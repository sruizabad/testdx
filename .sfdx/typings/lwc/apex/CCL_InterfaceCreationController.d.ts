declare module "@salesforce/apex/CCL_InterfaceCreationController.checkRequest" {
  export default function checkRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/CCL_InterfaceCreationController.createInterface" {
  export default function createInterface(param: {requestId: any}): Promise<any>;
}
