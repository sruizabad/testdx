declare module "@salesforce/apex/CCL_RequestController.getOrgs" {
  export default function getOrgs(): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.getOrg" {
  export default function getOrg(): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.getselectOptions" {
  export default function getselectOptions(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.getUserDetails" {
  export default function getUserDetails(): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.getObjectNames" {
  export default function getObjectNames(): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.getfieldNames" {
  export default function getfieldNames(param: {objectName: any}): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.inlineEdit" {
  export default function inlineEdit(param: {masterFields: any, changedFields: any}): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.checkData" {
  export default function checkData(param: {fieldsString: any}): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.addRequestFields" {
  export default function addRequestFields(param: {requestId: any, fieldsString: any}): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.sendEmail" {
  export default function sendEmail(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.sendRequest" {
  export default function sendRequest(param: {requestId: any}): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.loadInterfaceData" {
  export default function loadInterfaceData(param: {interfaceId: any}): Promise<any>;
}
declare module "@salesforce/apex/CCL_RequestController.loadMappingData" {
  export default function loadMappingData(param: {interfaceId: any, masterFields: any}): Promise<any>;
}
