({
    doInit: function(component, event, helper) {
        var reqStatus = component.get('v.requestStatus');
        var intName = component.get('v.interfaceName');
        if(reqStatus === 'New' || reqStatus === 'Transferred' ){
            if(intName){
                helper.checkRequest(component, component.get('v.requestId'));
            } else {
                component.set('v.errorMessage','Please fill in the Interface Name.');
                component.set('v.requestError', true);
            }
        } else {
            component.set('v.errorMessage','This request was already converted');
            component.set('v.requestError', true);
        }
    },
    
    goBack : function(component, event, helper){
        window.history.back();
    },
    
    goFurther : function(component, event, helper){
        component.set('v.continueMessage', '');
        component.set('v.continue', false);
        helper.createInterface(component, component.get('v.requestId'));
    },
    
    closeMessage : function(component, event, helper){
        var IntId = component.get('v.interfaceId');
        // SF1/Lightning Navigation
		if( (typeof sforce != 'undefined') && (sforce != null) ) {
     		sforce.one.navigateToSObject(IntId, 'detail');
		} 
		// Desktop/Classic Navigation
		else {
     		window.location.href = '/'+IntId;
		}
    }
})