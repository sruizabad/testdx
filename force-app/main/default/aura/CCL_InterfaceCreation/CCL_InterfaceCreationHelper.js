({
    checkRequest : function(component, requestId) {
        var action = component.get("c.checkRequest");
        action.setParams({
            "requestId": requestId
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                if(response.getReturnValue().check){
                    component.set("v.errorMessage", response.getReturnValue().message);
                    component.set("v.requestError", true);
                } else {
                    if(response.getReturnValue().goFurther){
                        this.createInterface(component, requestId);
                    } else {
                        component.set("v.continueMessage", response.getReturnValue().message);
                        component.set("v.continue", true);
                    }
                }
            };
        });
        $A.enqueueAction(action);
    },
    
    createInterface : function(component, requestId){
        var action = component.get("c.createInterface");
        action.setParams({
            "requestId": requestId
        });
        action.setCallback(this, function(response){
            if(response.getState() == "SUCCESS"){
                if(response.getReturnValue().goFurther){
                    component.set("v.interfaceId", response.getReturnValue().check);
                    component.set("v.completed", true);
                } else {
                    component.set("v.errorMessage", response.getReturnValue().message);
                    component.set("v.requestError", true);
                }
            };
        });
        $A.enqueueAction(action);
    }
    
})