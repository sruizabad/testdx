({
    doInit: function(component, event, helper) {
        component.find("requestRecordCreator").getNewRecord(
            "CCL_Request__c", // sObject type (objectApiName)
            null,      // recordTypeId
            false,     // skip cache?
            $A.getCallback(function() {
                var rec = component.get("v.newRequest");
                var error = component.get("v.newRequestError");
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                console.log("Record template initialized: " + rec.sobjectType);
                component.find('status').set("v.value", 'New');
                if(component.get("v.isInterfaceChange")){
                    component.find('type').set("v.value", 'Modification');
                } else {
                    component.find('type').set("v.value", 'New');
                }
            })
        );
        helper.fetchPickListVal(component, 'CCL_Operation__c', 'operationList');
        helper.fetchPickListVal(component, 'CCL_BI_Division__c', 'divisionList');
        helper.fetchObjects(component, 'objectList');
        helper.fetchOrg(component, 'org');
        helper.fetchUserDetails(component, 'reqby');
        if(component.get('v.isInterfaceChange')){
            helper.loadInterfaceData(component,component.get('v.interfaceId'));
        }
    },
    
    closeMessage: function(component, event, helper) {
        if(component.get('v.isInterfaceChange')){
            var intId = component.get('v.interfaceId');
            // SF1/Lightning Navigation
            if( (typeof sforce != 'undefined') && (sforce != null) ) {
                sforce.one.navigateToSObject(intId, 'detail');
            } 
            // Desktop/Classic Navigation
            else {
                window.location.href = '/' + intId.substring(0,3);
            }
        } else{
            window.location.reload();
        }
    },
    
    goBack: function(component, event, helper) {
        component.set("v.requestError", false);
    },
    
    //Submit request
    handleSubmitRequest: function(component, event, helper) {
        if(helper.validateRequestForm(component)) {
            if(helper.checkInlineEdit(component)) {
                helper.checkAllData(component,helper,component.find("fieldTable").getSelectedRows());
            } else {
                component.set("v.errorMessage","Please save the changes to the fields before submitting.");
                component.set("v.requestError", true);
            }
        } else {
            component.set("v.errorMessage","Please fill in all required fields.");
            component.set("v.requestError", true);
        }
    },  
    
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'edit_refType':
                helper.editRefType(component, row, action);
                break;
            default:
                //helper.showRowDetails(row);
                break;
        }
    },
    
    //Update number of fields field
    updateSelectedText: function (component, event) {
        var selectedRows = event.getParam("selectedRows");
        component.set("v.newRequestFields.CCL_Number_of_Fields__c", selectedRows.length);
    },
    
    //Inline Edit save
    saveRows: function (component, event, helper){       
        helper.inlineEdit(component,component.get("v.data"),event.getParam("draftValues"));
        component.find("fieldTable").set("v.draftValues", null);
    },
    
    //Get fields of selected Object
    selectFields: function(component, event, helper){
        var obj = component.find("objectList").get("v.value");
        helper.setColumns(component);
        helper.fetchData(component, obj);
    }
    
})