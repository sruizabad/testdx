({
    //Get picklist values
    fetchPickListVal: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find(elementId).set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Get org name
    fetchOrg: function(component, elementId) {
        var action = component.get("c.getOrg");
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var value = response.getReturnValue();
                component.find(elementId).set("v.value", value);
            };
        });
        $A.enqueueAction(action);
    },
    
    fetchUserDetails: function(component, elementId) {
        var action = component.get("c.getUserDetails");
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var value = response.getReturnValue();
                component.find(elementId).set("v.value", value);
            };
        });
        $A.enqueueAction(action);
    },
    
    //Get orgs from custom setting
    /*fetchOrgs: function(component, elementId) {
        var action = component.get("c.getOrgs");
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find(elementId).set("v.options", opts);
                //component.set(elementId,opts)
            }
        });
        $A.enqueueAction(action);
    },*/
    
    //Get available objects
    fetchObjects: function(component, elementId) {
        var action = component.get("c.getObjectNames");
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i].label,
                        value: allValues[i].apiName
                    });
                }
                component.find(elementId).set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Get fields of selected object
    fetchData: function (component, obj) {
        $A.util.removeClass(component.find("spinner"), "slds-hide"); 
        
        var action = component.get("c.getfieldNames");
        action.setParams({
            "objectName": obj
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        id: allValues[i].id,
                        label: allValues[i].label,
                        developerName: allValues[i].apiName,
                        dataType: allValues[i].dataType,
                        relatedTo: allValues[i].relatedTo,
                        actionLabel: allValues[i].refType,
                        required: false,
                        actionDisabled: allValues[i].actionDisabled,
                        extId: allValues[i].extId,
                    });
                }
                component.set('v.data', opts);
                if(component.get("v.isInterfaceChange")){
                    this.loadInterfaceMappingData(component,component.get("v.interfaceId"), component.get("v.data"));
                }
                
                $A.util.addClass(component.find("spinner"), "slds-hide"); 
            }
        });
        $A.enqueueAction(action);
    },
    
    //Validate required fields
    validateRequestForm: function(component) {
        var validRequest = true;
        var countryTeam = component.find("countryTeam").get("v.value");
        var divisionList = component.find("divisionList").get("v.value");
        var orgList = component.find("org").get("v.value");
        var objectList = component.find("objectList").get("v.value");
        var operationList = component.find("operationList").get("v.value");
        var batchSize = component.find("batchSize").get("v.value");
        var columnDelimiter = component.find("columnDelimiter").get("v.value");
        var textQualifier = component.find("textQualifier").get("v.value");
        var numberOfFields = component.find("numberOfFields").get("v.value");
        
        validRequest = (countryTeam && divisionList && orgList && objectList && operationList && batchSize && columnDelimiter && textQualifier && numberOfFields) ? true: false;
        
        return validRequest
    },
    
    //Check for pending inline changes
    checkInlineEdit: function(component) {
        var allSaved = true;       
        allSaved = (JSON.stringify(component.find("fieldTable").get("v.draftValues")) == '[]') ?  true: false;
        
        return allSaved
    },
    
    //Check All Data and create record if all ok
    checkAllData: function(component, helper, fieldsToAdd) {
        var action = component.get("c.checkData");
        var fields = [];
        for (var i = 0; i < fieldsToAdd.length; i++) {
            fields.push({
                label: fieldsToAdd[i].label,
                apiName: fieldsToAdd[i].developerName,
                dataType: fieldsToAdd[i].dataType,
                relatedTo: fieldsToAdd[i].relatedTo,
                required: fieldsToAdd[i].required,
                csvColumnName: fieldsToAdd[i].csvColumnName,
                refType: fieldsToAdd[i].actionLabel,
                extKey: fieldsToAdd[i].extKey
            });
        }
        action.setParams({
            "fieldsString": JSON.stringify(fields)
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                if(response.getReturnValue()){
                    component.find("requestRecordCreator").saveRecord(function(saveResult) {
                        if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                            // record is saved successfull
                            helper.createRequestFields(component,saveResult.recordId,component.find("fieldTable").getSelectedRows());
                            helper.sendEmail(component,saveResult.recordId);
                            //component.set("v.requestStatus", true); 
                        } else if (saveResult.state === "INCOMPLETE") {
                            // handle the incomplete state
                            console.log("User is offline, device doesn't support drafts.");
                        } else if (saveResult.state === "ERROR") {
                            // handle the error state
                            console.log("Problem saving contact, error: " + JSON.stringify(saveResult.error));
                        } else {
                            console.log("Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
                        }
                    });
                } else {
                    component.set("v.errorMessage","Please make sure that all csv column names are populated and unique.");
                    component.set("v.requestError", true);
                }
            }
        });
        $A.enqueueAction(action);    
    },
    
    //Create request field records
    createRequestFields: function(component, requestId, fieldsToAdd) {
        var action = component.get("c.addRequestFields");
        var fields = [];
        for (var i = 0; i < fieldsToAdd.length; i++) {
            fields.push({
                label: fieldsToAdd[i].label,
                apiName: fieldsToAdd[i].developerName,
                dataType: fieldsToAdd[i].dataType,
                relatedTo: fieldsToAdd[i].relatedTo,
                required: fieldsToAdd[i].required,
                csvColumnName: fieldsToAdd[i].csvColumnName,
                refType: fieldsToAdd[i].actionLabel,
                extKey: fieldsToAdd[i].extKey
            });
        }
        action.setParams({
            "requestId": requestId,
            "fieldsString": JSON.stringify(fields)
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                this.sendRequest(component,requestId);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Save inline edit changes
    inlineEdit: function(component, masterFields, changedFields) {
        var action = component.get("c.inlineEdit");
        var mfields = [];
        for (var i = 0; i < masterFields.length; i++) {
            mfields.push({
                id: masterFields[i].id,
                label: masterFields[i].label,
                apiName: masterFields[i].developerName,
                dataType: masterFields[i].dataType,
                relatedTo: masterFields[i].relatedTo,
                required: masterFields[i].required,
                csvColumnName: masterFields[i].csvColumnName,
                refType: masterFields[i].actionLabel,
                extKey: masterFields[i].extKey
            });
        }
        var cfields = [];
        for (var i = 0; i < changedFields.length; i++) {
            cfields.push({
                id: changedFields[i].id,
                label: changedFields[i].label,
                apiName: changedFields[i].developerName,
                dataType: changedFields[i].dataType,
                relatedTo: changedFields[i].relatedTo,
                required: changedFields[i].required,
                csvColumnName: changedFields[i].csvColumnName,
                refType: changedFields[i].actionLabel,
                extKey: changedFields[i].extKey
            });
        }
        action.setParams({
            "masterFields": JSON.stringify(mfields),
            "changedFields": JSON.stringify(cfields)
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        id: allValues[i].id,
                        label: allValues[i].label,
                        developerName: allValues[i].apiName,
                        dataType: allValues[i].dataType,
                        relatedTo: allValues[i].relatedTo,
                        required: allValues[i].required,
                        csvColumnName: allValues[i].csvColumnName,
                        actionLabel: allValues[i].refType,
                        extKey: allValues[i].extKey
                    });
                }
                component.set('v.data', opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Send Email with request specifications and csv attached
    sendEmail: function(component, requestId){
        var action = component.get("c.sendEmail");
        action.setParams({
            "requestId": requestId
        });
        $A.enqueueAction(action);
    },
    
    //Send Request to dev org
    sendRequest: function(component, requestId){
        var action = component.get("c.sendRequest");
        action.setParams({
            "requestId": requestId
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.requestStatus", true); 
            }
            
        });
        $A.enqueueAction(action);
    },
    
    //Change reference type
    editRefType: function (component, row) {
        var data = component.get('v.data');
        data = data.map(function(rowData) {
            if (rowData.id === row.id) {
                switch(row.actionLabel) {
                    case 'Salesforce Id':
                        //rowData.refType = 'External ID';
                        rowData.actionLabel = 'External Id';
                        break;
                    case 'External Id':
                        //rowData.refType = 'Salesforce Id';
                        rowData.actionLabel = 'Salesforce Id';
                        break;
                    default:
                        rowData.actionLabel = '';
                        rowData.actionDisabled = true;                        
                        break;
                }
            }
            return rowData;
        });
        component.set("v.data", data);
    },
    
    //Load existing interface data
    loadInterfaceData: function(component,interfaceId,masterFields){
        var action = component.get("c.loadInterfaceData");
        action.setParams({
            "interfaceId": interfaceId
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var interfaceData = response.getReturnValue();
                
                component.set('v.newRequestFields.CCL_Operation__c', interfaceData.CCL_Type_of_Upload__c);
                component.set('v.newRequestFields.CCL_Text_Qualifier__c', interfaceData.CCL_Text_Qualifier__c);
                component.set('v.newRequestFields.CCL_Column_Delimiter__c', interfaceData.CCL_Delimiter__c);
                component.set('v.newRequestFields.CCL_Batch_Size__c', interfaceData.CCL_Batch_Size__c);
                component.set('v.newRequestFields.CCL_Object__c', interfaceData.CCL_Selected_Object_Name__c);
                component.set('v.newRequestFields.CCL_Source_Interface_Name__c', interfaceData.CCL_Name__c);
                
                this.setColumns(component);
                this.fetchData(component, component.find("objectList").get("v.value"));                
            }
        });
        $A.enqueueAction(action);    
    },
    
    //Load existing interface mapping data
    loadInterfaceMappingData: function(component,interfaceId, masterFields){
        var action = component.get("c.loadMappingData");
        var mfields = [];
        for (var i = 0; i < masterFields.length; i++) {
            mfields.push({
                id: masterFields[i].id,
                label: masterFields[i].label,
                apiName: masterFields[i].developerName,
                dataType: masterFields[i].dataType,
                relatedTo: masterFields[i].relatedTo,
                required: masterFields[i].required,
                csvColumnName: masterFields[i].csvColumnName,
                refType: masterFields[i].actionLabel,
                extKey: masterFields[i].extKey,
                mapField: false
            });
        };
        var action = component.get("c.loadMappingData");
        action.setParams({
            "interfaceId": interfaceId,
            "masterFields": JSON.stringify(mfields)
        });
        var opts = [];
        var selected = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();                  
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        id: allValues[i].id,
                        label: allValues[i].label,
                        developerName: allValues[i].apiName,
                        dataType: allValues[i].dataType,
                        relatedTo: allValues[i].relatedTo,
                        required: allValues[i].required,
                        csvColumnName: allValues[i].csvColumnName,
                        actionLabel: allValues[i].refType,
                        extKey: allValues[i].extKey
                    });
                    if(allValues[i].mapField){
                        selected.push(allValues[i].id);
                    }
                }
                component.set('v.data', opts);          
                component.set("v.selectedRows",selected);
                component.set("v.newRequestFields.CCL_Number_of_Fields__c", selected.length);
            }
        });
        $A.enqueueAction(action);    
    },
    
    //Set column Names/types
    setColumns: function(component){
        component.set('v.columns', [
            {label: 'Field Label', fieldName: 'label', type: 'text'},
            {label: 'API Name', fieldName: 'developerName', type: 'text'},
            {label: 'CSV Column', fieldName: 'csvColumnName', type: 'text', editable: 'true'},
            {label: 'Data Type', fieldName: 'dataType', type: 'text'},
            {label: 'Required?', fieldName: 'required', type: 'boolean', editable: 'true'},
            {label: 'Related To', fieldName: 'relatedTo', type: 'text'},
            {label: 'Ref. Type', type: 'button', initialWidth: 150, typeAttributes:
             {label: { fieldName: 'actionLabel'}, name: 'edit_refType', title: 'Click to Change', disabled: {fieldName: 'actionDisabled'}, class: 'btn_next'}}, 
            {label: 'Ext. Key', fieldName: 'extKey', type: 'text', editable: {fieldName: 'extId'}},
        ]);
            },
            
            })