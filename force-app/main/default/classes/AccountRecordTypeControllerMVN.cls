/*
 * CaseRecordTypeControllerMVN
 * Created By:      Roman Lerman
 * Created Date:    2/14/2012
 * Description:     Displays Interaction or Request at the top of the Case Page based on whether or not the Case
 *                  is an Interaction or Request
 */
public with sharing class AccountRecordTypeControllerMVN {
    public String recordTypeName{get; set;}
    
    public AccountRecordTypeControllerMVN (ApexPages.StandardController controller){  
        recordTypeName = [select toLabel(RecordType.Name) from Account where Id = :controller.getRecord().Id].RecordType.Name;
    }
}