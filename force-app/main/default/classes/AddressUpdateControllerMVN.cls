/*
  *    AddressUpdateControllerMVN 
  *    Created By:     Kai Chen   
  *    Created Date:    September 6, 2013
  *    Description:     Controller for AddressUpdate component.  Used to update address fields on a Call record.
 */

public without sharing class AddressUpdateControllerMVN {

	public List<Address_vod__c> addresses{ get;set; }
	public Map<Id, Address_vod__c> addressesMap{ get;set; } 
	
	public Call2_vod__c call { get;set; }
	public Case caseRecord { get;set; }
	public Campaign_Target_vod__c campaignTargetRecord { get;set; }
	public Account account {get; set;}
	public Boolean forceLocked {get;set;}
	public String uniqueID {get;set;}

	public Address_vod__c newAddress { get;set; }
	public Boolean updateSuccessful {get;set;}
	public Boolean hasSaveError {get;set;}
	public Boolean testSAP {get;set;}
	public Call2_vod__c callToUpdate;
	
	public String addressShipTo {get;set;}
	public String addressLine1 {get;set;}
	public String addressLine2 {get;set;}
	public String addressLine3 {get;set;}
	public String addressLine4 {get;set;}
	public String addressCity  {get;set;}
	public String addressZip   {get;set;}
	public String shipToSalutation {get;set;}
	public Boolean showModal {get;set;}
	
	public String selectedAddress {get;set;}
	
	public final Integer SAPCHARLIMIT = 35;
	public String SELECTADDRESSOPTION {get{return 'select';}}

	private Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();
	
	public Boolean isSAP{
		get{
			return OrderUtilityMVN.SAPOrder;
		}
	}

	public String callId {
		get{
			if(!String.isBlank(uniqueID)){
				return uniqueID;
			} else if(call != null && call.Id != null){
				return call.Id;
			}
			return '';
		}
	}

	public Boolean callSubmitted {
		get{
			if(OrderUtilityMVN.userHasCreateEdit && !forceLocked) {
				if(call != null){
					return call.Status_vod__c == settings.Call_Submitted_Status_MVN__c;
				} else {
					return false;
				}
			} else {
				return true;
			}
		}
	}

	public List<SelectOption> getAddressOptions(){
		selectedAddress = '';
		Boolean callUsed = false;
		if(addresses != null){
			addressesMap = new Map<Id, Address_vod__c>(addresses);
		}
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(SELECTADDRESSOPTION, System.Label.Select_Address_Option));
		
		if(call.Ship_To_Address_vod__c != null){
			selectedAddress = call.Ship_To_Address_vod__c;
		} else {			
			selectedAddress = SELECTADDRESSOPTION;
		}

		if(addresses != null){
			for(Address_vod__c address : addresses){
				if(address.Id != call.Ship_To_Address_vod__c){
					options.add(UtilitiesMVN.getSelectOptionAddress(address));
				} else {
					options.add(new SelectOption(call.Ship_To_Address_vod__c, 
					(!String.isBlank(call.Ship_Address_Line_1_vod__c) ? call.Ship_Address_Line_1_vod__c : '') + 
					(!String.isBlank(call.Ship_City_vod__c) ? ', ' + call.Ship_City_vod__c : '') + 
					(!String.isBlank(call.Ship_Zip_vod__c) ? ', ' + call.Ship_Zip_vod__c : '')));
					callUsed = true;
				} 
			}
		}

		if(call.Ship_To_Address_vod__c == null && !String.isBlank(call.Ship_Address_Line_1_vod__c)){
			options.add(new SelectOption('custom', 
				call.Ship_Address_Line_1_vod__c+ 
				(!String.isBlank(call.Ship_City_vod__c) ? ', ' + call.Ship_City_vod__c : '') + 
				(!String.isBlank(call.Ship_Zip_vod__c) ? ', ' + call.Ship_Zip_vod__c : '')));
			
			selectedAddress = 'custom';
		} 
			
		options.add(new SelectOption('',System.Label.Enter_Address_Option));

		if (selectedAddress != SELECTADDRESSOPTION) {
			options.remove(0);
		}
		
		showModal = false;
		System.debug('SHOWMODAL: ' + showModal);
		System.debug('SELECTEDADDRESS: ' + selectedAddress); 
		return options;
		
	}

	public AddressUpdateControllerMVN() {
		hasSaveError = false;
		updateSuccessful = false;
		testSAP = false;
		showModal = false;
		selectedAddress = '';
		newAddress = new Address_vod__c();
	}

	public PageReference updateAddress(){
		hasSaveError = false;
		updateSuccessful = false;
		String requiredFieldName = '';
		if(String.isBlank(shipToSalutation) && isSap){
			requiredFieldName = System.Label.Letter_Salutation_Label;
		} else if(String.isBlank(addressShipTo)){
			requiredFieldName = System.Label.Address_Salutation_Label;
		} else if(String.isBlank(addressLine1)){
			requiredFieldName = System.Label.Street_Address_Label;
		} else if(String.isBlank(addressCity)){
			requiredFieldName = Schema.SObjectType.Call2_vod__c.fields.Ship_City_vod__c.getLabel();
		} else if(String.isBlank(addressZip)){
			requiredFieldName = Schema.SObjectType.Call2_vod__c.fields.Ship_Zip_vod__c.getLabel();
		} else if(String.isBlank(newAddress.Country_Code_BI__c)){
			requiredFieldName = Schema.SObjectType.Call2_vod__c.fields.Ship_Country_vod__c.getLabel();
		}
		if(requiredFieldName.length() > 0){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, requiredFieldName + ' ' + System.Label.New_Address_Required_Error));
    		showModal=true;
    		hasSaveError = true;
    		return null;
    	}
    	System.debug('SAP: ' + isSAP);
		if(isSap){
			String fieldName = '';
			if(addressShipTo.length() > SAPCHARLIMIT){
				fieldName = System.Label.Address_Salutation_Label;
            } else if(addressLine1 != null && addressLine1.length() > SAPCHARLIMIT){
				fieldName = System.Label.Street_Address_Label;
            } else if(addressLine2 != null && addressLine2.length() > SAPCHARLIMIT){
				fieldName = System.Label.Send_To_Label;
            } else if(addressLine3 != null && addressLine3.length() > SAPCHARLIMIT){
				fieldName = System.Label.Send_To_Label;
            } else if(addressLine4 != null && addressLine4.length() > SAPCHARLIMIT){
				fieldName = System.Label.Send_To_Label;
            } else if(addressCity != null && addressCity.length() > SAPCHARLIMIT){
				fieldName = Schema.SObjectType.Call2_vod__c.fields.Ship_City_vod__c.getLabel();
            } else if(addressZip != null && addressZip.length() > SAPCHARLIMIT){
				fieldName = Schema.SObjectType.Call2_vod__c.fields.Ship_Zip_vod__c.getLabel();
            } else if(newAddress.Country_Code_BI__c != null && newAddress.Country_Code_BI__c.length() > SAPCHARLIMIT){
				fieldName = Schema.SObjectType.Call2_vod__c.fields.Ship_Country_vod__c.getLabel();
            } 
            System.debug('FIELDNAME: ' + fieldName);
        	if(fieldName.length() > 0){

        		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, fieldName + ' ' + System.Label.Address_Length_Error + ' ' + SAPCHARLIMIT));
        		hasSaveError = true;
        		showModal=true;
        		return null;
        	}
		}
		Call2_vod__c tempCall = new Call2_vod__c();
		cloneCall(call, tempCall);
		call.Ship_To_Salutation_MVN__c = shipToSalutation;
		call.Ship_To_Name_MVN__c = addressShipTo;
		call.Ship_Address_Line_1_vod__c = addressLine1;
		call.Ship_Address_Line_2_vod__c  = addressLine2;
		call.Ship_Address_Line_3_MVN__c = addressLine3;
		call.Ship_Address_Line_4_MVN__c = addressLine4;
		call.Ship_City_vod__c = addressCity;
		call.Ship_Zip_vod__c = addressZip;
		call.Ship_Country_vod__c = newAddress.Country_Code_BI__c;

		Boolean rollbackCase = false;
		Boolean rollbackCampaignTarget = false;
		
		
		if(String.isBlank(selectedAddress) || selectedAddress == 'custom'){
			call.Ship_To_Address_vod__c = null;
		} else{
			call.Ship_To_Address_vod__c = Id.valueOf(selectedAddress);
		}
		Savepoint sp = Database.setSavepoint();
		try{
			upsert call;
			if(caseRecord != null && caseRecord.Order_MVN__c == null){
				caseRecord.Order_MVN__c = call.Id;
				rollbackCase = true;
				upsert caseRecord;
			}
			if(campaignTargetRecord != null && campaignTargetRecord.Order_MVN__c == null){
				campaignTargetRecord.Order_MVN__c = call.Id;
				rollbackCampaignTarget = true;
				upsert campaignTargetRecord;
			}
		} catch(EXCEPTION e){
			System.debug('EXCEPTION WHEN UPSERTING CALL: ' + e);
			if(rollbackCase){
				caseRecord.Order_MVN__c = null;
			}
			if(rollbackCampaignTarget){
				campaignTargetRecord.Order_MVN__c = null;
			}
			
			Database.rollback(sp);
			hasSaveError = true;
			showModal=true;
			updateSuccessful = false;
			System.debug('CLONING TEMP CALL');
			cloneCall(tempCall, call);
			System.debug('CLONING COMPLETE: ' + call);
			ApexPages.addMessages(e);
            return null;
        }
        System.debug('Call updated successfully');
		updateSuccessful = true;
		showModal = false;
		return null;
	}

	public void updateSelected(){
		if(selectedAddress != SELECTADDRESSOPTION){
			if(String.isBlank(selectedAddress) ){
				clearAddressFields();
				if(call.Account_vod__c != null){
					if(String.isBlank(account.PersonTitle)) {
						addressShipTo = account.Name;
					} else {
						addressShipTo = account.PersonTitle + ' ' + account.Name;
					}
				}
				newAddress.Country_Code_BI__c = call.Country_Code_BI__c;
			} else if (selectedAddress == 'custom') {
				copyCallFields(call);
			} else if(selectedAddress == call.Ship_To_Address_vod__c){
				copyCallFields(call);
			} else{
				Address_vod__c address = addressesMap.get(selectedAddress);
				if(address != null){
					copyAddressFields(address);
					if(selectedAddress == call.Ship_To_Address_vod__c){
						addressShipTo = call.Ship_to_Name_MVN__c;
					} else{
						if(String.isBlank(account.PersonTitle)) {
							addressShipTo = account.Name;
						} else {
							addressShipTo = account.PersonTitle + ' ' + account.Name;
						}
					}
				} 
			}
			showModal = true;
		}
		hasSaveError = false;
		updateSuccessful = false;
	}

	public void clearAddressFields(){
		populateSalutation();
		addressShipTo = '';
		addressLine1 = '';
		addressLine2 = '';
		addressLine3 = '';
		addressLine4 = '';
		addressCity = '';
		addressZip = '';
	}

	public void copyAddressFields(Address_vod__c address){
		populateSalutation();
		addressLine1 = address.Name;
		addressLine2 = address.Address_line_2_vod__c;
		addressLine3 = '';
		addressLine4 = '';
		addressCity = address.City_vod__c;
		addressZip = address.Zip_vod__c;
		newAddress.Country_Code_BI__c = address.Country_Code_BI__c;
	}

	public void copyCallFields(Call2_vod__c call){
		System.debug('COPYING CALL FIELDS: ' + call);
		populateSalutation();
		addressShipTo = call.Ship_to_Name_MVN__c;
		addressLine1 = call.Ship_Address_Line_1_vod__c;
		addressLine2 = call.Ship_Address_Line_2_vod__c;
		addressLine3 = call.Ship_Address_Line_3_MVN__c;
		addressLine4 = call.Ship_Address_Line_4_MVN__c;
		addressCity = call.Ship_City_vod__c;
		addressZip = call.Ship_Zip_vod__c;
		newAddress.Country_Code_BI__c = call.Ship_Country_vod__c;
	}

	public void cloneCall(Call2_vod__c originalCall, Call2_vod__c clonedCall){
		clonedCall.Ship_To_Salutation_MVN__c = originalCall.Ship_To_Salutation_MVN__c;
		clonedCall.Ship_To_Name_MVN__c = originalCall.Ship_To_Name_MVN__c;
		clonedCall.Ship_Address_Line_1_vod__c = originalCall.Ship_Address_Line_1_vod__c;
		clonedCall.Ship_Address_Line_2_vod__c  = originalCall.Ship_Address_Line_2_vod__c;
		clonedCall.Ship_Address_Line_3_MVN__c = originalCall.Ship_Address_Line_3_MVN__c;
		clonedCall.Ship_Address_Line_4_MVN__c = originalCall.Ship_Address_Line_4_MVN__c;
		clonedCall.Ship_City_vod__c = originalCall.Ship_City_vod__c;
		clonedCall.Ship_Zip_vod__c = originalCall.Ship_Zip_vod__c;
		clonedCall.Ship_Country_vod__c = originalCall.Ship_Country_vod__c;
		clonedCall.Ship_To_Address_vod__c = originalCall.Ship_To_Address_vod__c;
		clonedCall.Id = originalCall.Id;
	}

	public void populateSalutation(){
		if(!String.isBlank(call.Ship_To_Salutation_MVN__c) && !String.isBlank(selectedAddress) && (selectedAddress == 'custom' || selectedAddress == call.Ship_To_Address_vod__c)){
			shipToSalutation = call.Ship_To_Salutation_MVN__c;
		} else if(account != null && !String.isBlank(account.LastName) && !String.isBlank(account.OK_Stdopening_BI__c)){
			shipToSalutation = account.OK_Stdopening_BI__c + ' ' + account.LastName + ',';
		} else{
			shipToSalutation = System.Label.Letter_Salutation_Value;
		}
	}
}