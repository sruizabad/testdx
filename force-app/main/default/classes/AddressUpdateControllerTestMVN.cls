/*
  * AddressUpdateControllerTestMVN
  *    Created By:     Kai Chen   
  *    Created Date:    September 8, 2013
  *    Description:     Unit tests for AddressUpdateControllerMVN
 */
@isTest
private class AddressUpdateControllerTestMVN {
    static AddressUpdateControllerMVN controller = null;
    static User callCenterUser;
    // setup data
    static{
        Profile p = [select Id from Profile where Name='CRC - Customer Request Agent']; 
        
      //*************Modified by sathya**************
        //UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Germany'];
        List<UserRole> urList = [select Id from UserRole where DeveloperName = 'CRC_Germany'];
      //*************Modified by sathya**************
      
        callCenterUser = new User(Alias = 'heiditst', Email='heidibergen@oehringertest.com', 
                        EmailEncodingKey='UTF-8', FirstName = 'Heidi', LastName='Bergen', LanguageLocaleKey='de', 
                        LocaleSidKey='de', ProfileId = p.Id,  
                        TimeZoneSidKey='Europe/Berlin', UserName='heidibergen@oehringertest.com',
                        Country_Code_BI__c = 'DE', External_ID_BI__c='12345678');
                        
      //*************Modified by sathya**************
        if(urList.size() != 0)
            callCenterUser .UserRoleId = urList[0].Id;
      //*************Modified by sathya**************
        
        User adminUser = new User(alias='ccusysad', email= 'callcentertestusermvn@callcenter.com', External_ID_BI__c='123456789', Country_Code_BI__c = 'DE',
                      emailencodingkey='UTF-8', firstName='Reginald', lastname='Wellington', languagelocalekey='en_US', 
                      localesidkey='en_US', profileid = [select Id from Profile where Name = 'System Administrator'].Id, 
                      isActive = true, timezonesidkey='America/Los_Angeles', username='callcentertestusermvn@callcenter.com',
                      Default_Article_Language_MVN__c = KnowledgeArticleVersion.Language.getDescribe().getPicklistValues()[0].getLabel());

        System.runAs(adminUser){
        insert callCenterUser;
        }

        System.runAs(callCenterUser){
            TestDataFactoryMVN.createSettings();
        }
        
    }
    
    static testMethod void caseOnly(){
        System.runAs(callCenterUser){
            Call2_vod__c call = TestDataFactoryMVN.createSavedCall();
            controller = new AddressUpdateControllerMVN();
            controller.call = call;

            System.assertEquals(controller.getAddressOptions().size(), 2);
        }
    }

    //Scenario: Updating free form address on a Case with no Address lookup defined
    static testMethod void caseNoAddressSelectedFreeForm(){
        System.runAs(callCenterUser){
            Call2_vod__c call = TestDataFactoryMVN.createSavedCall();
            Account consumer = TestDataFactoryMVN.createTestConsumer();
            call.Account_vod__c = consumer.Id;
            update call;

            call = queryForCall(call.Id);
            consumer = queryForAccount(consumer.Id);

            Address_vod__c primaryAddress = TestDataFactoryMVN.createTestPrimaryAddress(consumer);
            Address_vod__c secondaryAddress = TestDataFactoryMVN.createTestAddress(consumer);
        
            List<Address_vod__c> addressList = new List<Address_vod__c>();
            addressList.add(primaryAddress);
            addressList.add(secondaryAddress);

            controller = new AddressUpdateControllerMVN();
            controller.addresses = addressList;
            controller.call = call;
            controller.account = consumer;
            controller.forceLocked = false;
            
            System.assertEquals(4, controller.getAddressOptions().size());

            System.assertEquals(controller.selectedAddress , 'select');

            controller.selectedAddress = '';

            controller.updateSelected();

            System.assertEquals(controller.addressShipTo, consumer.Name);
            System.assert(String.isBlank(controller.addressLine1));
            System.assert(String.isBlank(controller.addressLine2));
            System.assert(String.isBlank(controller.addressLine3));
            System.assert(String.isBlank(controller.addressLine4));
            System.assert(String.isBlank(controller.addressCity));
            System.assert(String.isBlank(controller.addressZip));   
            System.assertEquals(controller.newAddress.Country_Code_BI__c, call.Country_Code_BI__c);
            System.assertEquals(controller.addressShipTo, consumer.Name);
            System.assertEquals(controller.callId, call.Id);
            System.assertEquals(controller.callSubmitted, false);

            controller.addressShipTo = 'Test Consumer Modified';
            controller.addressLine1 = '123 Main St.';
            controller.addressLine2 = 'Apt. 1';
            controller.addressLine3 = '123';
            controller.addressLine4 = '456';
            controller.addressCity = 'Test City';
            controller.addressZip = '60606';
            controller.newAddress.Country_Code_BI__c = 'DE';
            
            controller.updateAddress();
        
            call = queryForCall(call.Id);

            System.assertEquals(call.Ship_to_Name_MVN__c, 'Test Consumer Modified');
            System.assertEquals(call.Ship_Address_Line_1_vod__c, '123 Main St.');
            System.assertEquals(call.Ship_Address_Line_2_vod__c, 'Apt. 1');
            System.assertEquals(call.Ship_Address_Line_3_MVN__c, '123');
            System.assertEquals(call.Ship_Address_Line_4_MVN__c, '456');
            System.assertEquals(call.Ship_City_vod__c, 'Test City');
            System.assertEquals(call.Ship_Zip_Vod__c, '60606');
            System.assertEquals(call.Ship_Country_vod__c, 'DE');
            System.assertEquals(call.Ship_To_Address_vod__c, null);
        }
        
    }

    //Scenario: Updating free form address on a Case with a Address lookup defined
    static testMethod void caseNoAddressSelectedExistingAddress(){
        System.runAs(callCenterUser){
            OrderUtilityMVN util = new OrderUtilityMVN();
            Call2_vod__c call = TestDataFactoryMVN.createSavedCall();
            Account consumer = TestDataFactoryMVN.createTestConsumer();
            call.Account_vod__c = consumer.Id;
            update call;
            
            Address_vod__c primaryAddress = TestDataFactoryMVN.createTestPrimaryAddress(consumer);
            Address_vod__c secondaryAddress = TestDataFactoryMVN.createTestAddress(consumer);
        
            List<Address_vod__c> addressList = new List<Address_vod__c>();
            addressList.add(primaryAddress);
            addressList.add(secondaryAddress);

            call = queryForCall(call.Id);
            consumer = queryForAccount(consumer.Id);

            controller = new AddressUpdateControllerMVN();
            controller.addresses = addressList;
            controller.call = call;
            controller.account = consumer;

            System.assertEquals(4, controller.getAddressOptions().size());

            System.assertEquals(controller.selectedAddress, 'select');

            controller.selectedAddress = primaryAddress.Id;

            controller.updateSelected();

            System.assertEquals(controller.addressLine1, primaryAddress.Name);
            System.assertEquals(controller.addressLine2, primaryAddress.Address_line_2_vod__c);
            System.assertEquals(controller.addressLine3, '');
            System.assertEquals(controller.addressLine4, '');
            System.assertEquals(controller.addressCity, primaryAddress.City_vod__c);
            System.assertEquals(controller.addressZip, primaryAddress.Zip_vod__c);
            System.assertEquals(controller.newAddress.Country_Code_BI__c, primaryAddress.Country_Code_BI__c);
            System.assertEquals(controller.selectedAddress, primaryAddress.Id);
            System.assertEquals(controller.addressShipTo, consumer.Name);

            controller.updateAddress();
            
            call = queryForCall(call.Id);

            System.assertEquals(call.Ship_To_Address_vod__c, primaryAddress.Id);
            
            controller.addressShipTo = 'Test Consumer Modified';
            controller.addressLine1 = '123 Main St.';
            controller.addressLine2 = 'Apt. 1';
            controller.addressLine3 = '123';
            controller.addressLine4 = '456';
            controller.addressCity = 'Test City';
            controller.addressZip = '60606';
            controller.newAddress.Country_Code_BI__c = 'DE';
            
            controller.updateAddress();

            System.assertEquals(3, controller.getAddressOptions().size());
            
            call = queryForCall(call.Id);

            System.assertEquals(call.Ship_to_Name_MVN__c, 'Test Consumer Modified');
            System.assertEquals(call.Ship_Address_Line_1_vod__c, '123 Main St.');
            System.assertEquals(call.Ship_Address_Line_2_vod__c, 'Apt. 1');
            System.assertEquals(call.Ship_Address_Line_3_MVN__c, '123');
            System.assertEquals(call.Ship_Address_Line_4_MVN__c, '456');
            System.assertEquals(call.Ship_City_vod__c, 'Test City');
            System.assertEquals(call.Ship_Zip_Vod__c, '60606');
            System.assertEquals(call.Ship_Country_vod__c, primaryAddress.Country_Code_BI__c);
            System.assertEquals(call.Ship_To_Address_vod__c, primaryAddress.Id);
        }
            
    }

    //Scenario: Updating free form address on a Case with a Address lookup defined
    static testMethod void caseSelectedAddressSelectedFreeForm(){
        System.runAs(callCenterUser){
            OrderUtilityMVN util = new OrderUtilityMVN();
            Call2_vod__c call = TestDataFactoryMVN.createSavedCall();
            Account consumer = TestDataFactoryMVN.createTestConsumer();

            Address_vod__c primaryAddress = TestDataFactoryMVN.createTestPrimaryAddress(consumer);
            Address_vod__c secondaryAddress = TestDataFactoryMVN.createTestAddress(consumer);
        
            List<Address_vod__c> addressList = new List<Address_vod__c>();
            addressList.add(primaryAddress);
            addressList.add(secondaryAddress);

            call = queryForCall(call.Id);
            consumer = queryForAccount(consumer.Id);

            call.Account_vod__c = consumer.Id;
            call.Ship_To_Address_vod__c = secondaryAddress.Id;
            call.Ship_Address_Line_1_vod__c = secondaryAddress.Name;
            call.Ship_Address_Line_2_vod__c = secondaryAddress.Address_line_2_vod__c;
            call.Ship_City_vod__c = secondaryAddress.City_vod__c;
            call.Ship_Zip_vod__c = secondaryAddress.Zip_vod__c;
            call.Ship_Country_vod__c = secondaryAddress.Country_Code_BI__c;
            call.Ship_to_Name_MVN__c = consumer.Name;
            update call;

            
            controller = new AddressUpdateControllerMVN();
            controller.addresses = addressList;
            controller.call = call;
            controller.account = consumer;

            
            System.assertEquals(3, controller.getAddressOptions().size());

            controller.updateSelected();

            System.assertEquals(controller.selectedAddress, secondaryAddress.Id);
            System.assertEquals(controller.addressLine1, call.Ship_Address_Line_1_vod__c);
            System.assertEquals(controller.addressLine2, secondaryAddress.Address_line_2_vod__c);
            System.assertEquals(controller.addressLine3, null);
            System.assertEquals(controller.addressLine4, null);
            System.assertEquals(controller.addressCity, secondaryAddress.City_vod__c);
            System.assertEquals(controller.addressZip, secondaryAddress.Zip_vod__c);
            System.assertEquals(controller.newAddress.Country_Code_BI__c, secondaryAddress.Country_Code_BI__c);
            System.assertEquals(controller.addressShipTo, call.Ship_to_Name_MVN__c);

            controller.selectedAddress = '';

            controller.updateSelected();

            System.assertEquals(controller.addressShipTo, consumer.Name);
            System.assert(String.isBlank(controller.addressLine1));
            System.assert(String.isBlank(controller.addressLine2));
            System.assert(String.isBlank(controller.addressLine3));
            System.assert(String.isBlank(controller.addressLine4));
            System.assert(String.isBlank(controller.addressCity));
            System.assert(String.isBlank(controller.addressZip));   
            System.assertEquals(controller.newAddress.Country_Code_BI__c, call.Country_Code_BI__c);
            System.assertEquals(controller.addressShipTo, consumer.Name);

            controller.addressShipTo = 'Test Consumer Modified';
            controller.addressLine1 = '123 Main St.';
            controller.addressLine2 = 'Apt. 1';
            controller.addressLine3 = '123';
            controller.addressLine4 = '456';
            controller.addressCity = 'Test City';
            controller.addressZip = '60606';
            controller.newAddress.Country_Code_BI__c = 'DE';
            
            controller.updateAddress();
        
            call = queryForCall(call.Id);

            System.assertEquals(call.Ship_to_Name_MVN__c, 'Test Consumer Modified');
            System.assertEquals(call.Ship_Address_Line_1_vod__c, '123 Main St.');
            System.assertEquals(call.Ship_Address_Line_2_vod__c, 'Apt. 1');
            System.assertEquals(call.Ship_Address_Line_3_MVN__c, '123');
            System.assertEquals(call.Ship_Address_Line_4_MVN__c, '456');
            System.assertEquals(call.Ship_City_vod__c, 'Test City');
            System.assertEquals(call.Ship_Zip_Vod__c, '60606');
            System.assertEquals(call.Ship_Country_vod__c, 'DE');
            System.assertEquals(call.Ship_To_Address_vod__c, null);
        }   
    }

    //Scenario: Updating free form address on a Case with a Address lookup defined
    static testMethod void caseSelectedAddressSelectedExistingAddress(){
        System.runAs(callCenterUser){
            OrderUtilityMVN util = new OrderUtilityMVN();
            Call2_vod__c call = TestDataFactoryMVN.createSavedCall();
            Account consumer = TestDataFactoryMVN.createTestConsumer();

            consumer = [select Name from Account where Id = :consumer.Id];
            Address_vod__c primaryAddress = TestDataFactoryMVN.createTestPrimaryAddress(consumer);
            Address_vod__c secondaryAddress = TestDataFactoryMVN.createTestAddress(consumer);
        
            List<Address_vod__c> addressList = new List<Address_vod__c>();
            addressList.add(primaryAddress);
            addressList.add(secondaryAddress);

            call.Account_vod__c = consumer.Id;
            call.Ship_To_Address_vod__c = secondaryAddress.Id;
            update call;

            call = queryForCall(call.Id);
            consumer = queryForAccount(consumer.Id);

            controller = new AddressUpdateControllerMVN();
            controller.addresses = addressList;
            controller.call = call;
            controller.account = consumer;
            
            System.assertEquals(3, controller.getAddressOptions().size());

            System.assertEquals(controller.selectedAddress, secondaryAddress.Id);

            controller.selectedAddress = primaryAddress.Id;

            controller.updateSelected();

            System.assertEquals(controller.addressLine1, primaryAddress.Name);
            System.assertEquals(controller.addressLine2, primaryAddress.Address_line_2_vod__c);
            System.assertEquals(controller.addressLine3, '');
            System.assertEquals(controller.addressLine4, '');
            System.assertEquals(controller.addressCity, primaryAddress.City_vod__c);
            System.assertEquals(controller.addressZip, primaryAddress.Zip_vod__c);
            System.assertEquals(controller.newAddress.Country_Code_BI__c, primaryAddress.Country_Code_BI__c);
            System.assertEquals(controller.selectedAddress, primaryAddress.Id);
            System.assertEquals(controller.addressShipTo, consumer.Name);

            controller.updateAddress();
            
            call = queryForCall(call.Id);

            System.assertEquals(call.Ship_To_Address_vod__c, primaryAddress.Id);
            
            controller.addressShipTo = 'Test Consumer Modified';
            controller.addressLine1 = '123 Main St.';
            controller.addressLine2 = 'Apt. 1';
            controller.addressLine3 = '123';
            controller.addressLine4 = '456';
            controller.addressCity = 'Test City';
            controller.addressZip = '60606';
            controller.newAddress.Country_Code_BI__c = 'DE';
            
            controller.updateAddress();

            List<SelectOption> options = controller.getAddressOptions();

            for(SelectOption option : options){
                if(option.getValue() == (String) primaryAddress.Id){
                    System.assert(option.getLabel().contains('123 Main St.'));
                }
            }
            
            call = queryForCall(call.Id);

            System.assertEquals(call.Ship_to_Name_MVN__c, 'Test Consumer Modified');
            System.assertEquals(call.Ship_Address_Line_1_vod__c, '123 Main St.');
            System.assertEquals(call.Ship_Address_Line_2_vod__c, 'Apt. 1');
            System.assertEquals(call.Ship_Address_Line_3_MVN__c, '123');
            System.assertEquals(call.Ship_Address_Line_4_MVN__c, '456');
            System.assertEquals(call.Ship_City_vod__c, 'Test City');
            System.assertEquals(call.Ship_Zip_Vod__c, '60606');
            System.assertEquals(call.Ship_Country_vod__c, primaryAddress.Country_Code_BI__c);
            System.assertEquals(call.Ship_To_Address_vod__c, primaryAddress.Id);
        }
            
    }


    //Scenario: A new call is created with the Case record passed in
    static testMethod void newCallFromCase(){
        System.runAs(callCenterUser){
            OrderUtilityMVN util = new OrderUtilityMVN();
            Call2_vod__c call = new Call2_vod__c();

            Case newCase = TestDataFactoryMVN.createNewTestCase(true);

            Account consumer = [select Name, Id from Account where Id = :newCase.AccountId];

            consumer = queryForAccount(consumer.Id);

            Address_vod__c primaryAddress = TestDataFactoryMVN.createTestPrimaryAddress(consumer);
            Address_vod__c secondaryAddress = TestDataFactoryMVN.createTestAddress(consumer);

            List<Address_vod__c> addressList = new List<Address_vod__c>();
            addressList.add(primaryAddress);
            addressList.add(secondaryAddress);

            call.Account_vod__c = consumer.Id;
            
            controller = new AddressUpdateControllerMVN();
            controller.addresses = addressList;
            controller.call = call;
            controller.caseRecord = newCase;
            controller.account = consumer;

            System.assertEquals('', controller.callId);
            
            controller.uniqueID = '1234';

            System.assertEquals('1234', controller.callId);

            controller.getAddressOptions();

            controller.selectedAddress = primaryAddress.Id;

            controller.updateSelected();

            controller.addressShipTo = 'Test Consumer Modified';
            controller.addressLine1 = '123 Main St.';
            controller.addressLine2 = 'Apt. 1';
            controller.addressCity = 'Test City';
            controller.addressZip = '60606';
            controller.newAddress.Country_Code_BI__c = 'DE';
            
            controller.updateAddress();

            call = queryForCall(call.Id);

            newCase = [select Id, Order_MVN__c from Case where Id = :newCase.Id];

            System.assertEquals(newCase.Order_MVN__c, call.Id);

            controller.getAddressOptions();
            controller.updateSelected();
        }
    }

    //Scenario: A new call is created with the Campaign Target record passed in
    static testMethod void newCallFromCampaignTarget(){
        System.runAs(callCenterUser){
            OrderUtilityMVN util = new OrderUtilityMVN();
            Call2_vod__c call = new Call2_vod__c();

            Campaign_vod__c campaign = TestDataFactoryMVN.createTestCampaign();

            Account consumer = TestDataFactoryMVN.createTestConsumer();
            consumer = queryForAccount(consumer.Id);

            Campaign_Target_vod__c target = TestDataFactoryMVN.createTestCampaignTarget(campaign, consumer);

            Address_vod__c primaryAddress = TestDataFactoryMVN.createTestPrimaryAddress(consumer);
            Address_vod__c secondaryAddress = TestDataFactoryMVN.createTestAddress(consumer);
        
            List<Address_vod__c> addressList = new List<Address_vod__c>();
            addressList.add(primaryAddress);
            addressList.add(secondaryAddress);

            call.Account_vod__c = consumer.Id;
            
            controller = new AddressUpdateControllerMVN();
            controller.addresses = addressList;
            controller.call = call;
            controller.campaignTargetRecord = target;
            controller.account = consumer;

            controller.getAddressOptions();

            controller.selectedAddress = primaryAddress.Id;

            controller.updateSelected();

            controller.addressShipTo = 'Test Consumer Modified';
            controller.addressLine1 = '123 Main St.';
            controller.addressLine2 = 'Apt. 1';
            controller.addressCity = 'Test City';
            controller.addressZip = '60606';
            controller.newAddress.Country_Code_BI__c = 'DE';
            
            controller.updateAddress();

            call = queryForCall(call.Id);

            target = [select Id, Order_MVN__c from Campaign_Target_vod__c where Id = :target.Id];

            System.assertEquals(target.Order_MVN__c, call.Id);
        }   
    }

    static testMethod void fieldValidations(){
        System.runAs(callCenterUser){
            OrderUtilityMVN util = new OrderUtilityMVN();
            Call2_vod__c call = TestDataFactoryMVN.createSavedCall();
            Account consumer = TestDataFactoryMVN.createTestConsumer();

            consumer = [select Name from Account where Id = :consumer.Id];
            call.Account_vod__c = consumer.Id;
            update call;

            call = queryForCall(call.Id);
            consumer = queryForAccount(consumer.Id);

            controller = new AddressUpdateControllerMVN();
            controller.call = call;
            controller.account = consumer;

            System.assertEquals(2, controller.getAddressOptions().size());

            controller.selectedAddress = '';

            controller.updateSelected();

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressLine1 = '123 Main St.';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressCity = 'Test City';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressZip = '12345';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.newAddress.Country_Code_BI__c = 'DE';

            controller.testSAP = true;

            controller.shipToSalutation = '';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.shipToSalutation = 'Test';
            controller.addressShipTo = 'TestTestTestTestTestTestTestTestTest';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressShipTo = 'Test';
            controller.addressLine2 = 'TestTestTestTestTestTestTestTestTest';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressLine2 = 'Test';
            controller.addressLine3 = 'TestTestTestTestTestTestTestTestTest';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressLine3 = 'Test';
            controller.addressLine4 = 'TestTestTestTestTestTestTestTestTest';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressLine4 = 'Test';
            controller.addressCity = 'TestTestTestTestTestTestTestTestTest';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressCity = 'Test';
            controller.addressZip = 'TestTestTestTestTestTestTestTestTest';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressZip = 'Test';
            controller.newAddress.Country_Code_BI__c = 'TestTestTestTestTestTestTestTestTest';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.newAddress.Country_Code_BI__c = 'Test';
            controller.addressShipTo = '';

            controller.updateAddress();

            System.assert(ApexPages.hasMessages());

            controller.addressShipTo = 'Test';

            Test.setReadOnlyApplicationMode(true);
            controller.updateAddress();

            System.assert(ApexPages.hasMessages());
        }
    }

    static Call2_vod__c queryForCall(Id callId){
        String query = 'select ' + OrderUtilityMVN.callQueryFields + ' from Call2_vod__c where Id = \'' + callId + '\'';
        return (Call2_vod__c) Database.query(query);
    }

    static Account queryForAccount(Id accountId){
        String query = 'select ' + OrderUtilityMVN.accountQueryFields + ' from Account where Id = \'' + accountId + '\'';
        return (Account) Database.query(query);
    }
      
}