public with sharing class AttachedArticlesControllerMITS {


  public Boolean  showAttachedArticles {get;set;}
  public Case   casearticle{get; set;}
  public Id   MITSattachedArticleId {get;set;} 
  public List<Case_Article_Data_MVN__c> attachedKnowledgeListMITS;
  
   private KnowledgeSearchUtilityMITS mitsknowledgeSearchUtility = new KnowledgeSearchUtilityMITS();
   
    public AttachedArticlesControllerMITS(){
      casearticle= [select Id, IsClosed from Case where Id =: ApexPages.CurrentPage().getparameters().get('id')];
    }
    public List<Case_Article_Data_MVN__c> getAttachedKnowledgeListMITS() {
         showAttachedArticles = false;
         
         attachedKnowledgeListMITS = mitsknowledgeSearchUtility.queryExistingCaseArticles(casearticle.Id);
        if(attachedKnowledgeListMITS .size() > 0){
          showAttachedArticles = true;
        }
       return attachedKnowledgeListMITS; 
    }
   public PageReference MITSremoveArticle() {
        for(Case_Article_Data_MVN__c cad : attachedKnowledgeListMITS) {
            if(cad.Id == MITSattachedArticleId) {
                mitsknowledgeSearchUtility.removeArticle(cad);
            }
        }

        return null;
    }
}