/*
 * AttachedArticlesControllerMVN
 * Created By:      Roman Lerman
 * Created Date:    4/25/2013
 * Description:     Displays the attached knowledge articles
 */
public with sharing class AttachedArticlesControllerMVN {
	public Boolean	showAttachedArticles {get;set;}
    public Case 	articleCase{get; set;}
    public Id   attachedArticleId {get;set;}
    public List<Case_Article_Data_MVN__c> attachedKnowledgeList;

    private KnowledgeSearchUtilityMVN knowledgeSearchUtility = new KnowledgeSearchUtilityMVN();
    
    public AttachedArticlesControllerMVN(){
    	articleCase = [select Id, IsClosed from Case where Id =: ApexPages.CurrentPage().getparameters().get('id')];
    }

    public List<Case_Article_Data_MVN__c> getAttachedKnowledgeList() {
    	showAttachedArticles = false;
    	
        attachedKnowledgeList = knowledgeSearchUtility.queryExistingCaseArticles(articleCase.Id);
        if(attachedKnowledgeList.size() > 0){
        	showAttachedArticles = true;
        }
       	
        return attachedKnowledgeList;
    }

    public PageReference removeArticle() {
        for(Case_Article_Data_MVN__c cad : attachedKnowledgeList) {
            if(cad.Id == attachedArticleId) {
                knowledgeSearchUtility.removeArticle(cad);
            }
        }

        return null;
    }
}