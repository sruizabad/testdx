/*
 * AttachedArticlesControllerTestMVN
 * Created By:      Roman Lerman
 * Created Date:    5/1/2013
 * Description:     Test class for the AttachedArticlesControllerMVN
 */
@isTest
private class AttachedArticlesControllerTestMVN {
    
    static Case request;
    static AttachedArticlesControllerMVN attachedArticlesController;
    static Case_Article_Data_MVN__c foundArticle;
    static List<Id> articleIds;
    static List<FAQ_MVN__kav> faqs;

    // Setup the data
    static {
        TestDataFactoryMVN.createSettings();
        articleIds = TestDataFactoryMVN.setupArticles();
        faqs = [select ArticleNumber, UrlName, Title, KnowledgeArticleId, Id, Language, Summary, ArticleType, VersionNumber, Indication_MVN__c, Category_MVN__c from FAQ_MVN__kav where Id in :articleIds];
    }

    //********************
    // Scenario 1:  Component lists attached articles when viewed
    @isTest static void knowledgeArticlesListed() {
        caseIsCreatedWithAttachedArticle();
        repViewsCase();
        allArticlesDisplayed();
    }

    static void caseIsCreatedWithAttachedArticle() {
        // Create a new request
        Case interaction = TestDataFactoryMVN.createTestCase();
        request = TestDataFactoryMVN.createTestRequest(interaction);

        foundArticle = KnowledgeSearchUtilityMVN.createCaseArticle(request.Id, faqs[1]);
        insert foundArticle;
    }

    static void repViewsCase() {
        ApexPages.currentPage().getParameters().put('id', request.id);
        attachedArticlesController = new attachedArticlesControllerMVN();
    }

    static void allArticlesDisplayed() {
        System.assertEquals(1,attachedArticlesController.getAttachedKnowledgeList().size());
        System.assertEquals(faqs[1].KnowledgeArticleId,attachedArticlesController.getAttachedKnowledgeList()[0].Knowledge_Article_ID_MVN__c);
    }

    //********************
    // Scenario 2:  Knowledge Articles should be removed
    @isTest static void knowledgeArticlesCanBeRemoved(){
        caseIsCreatedWithAttachedArticle();
        repViewsCase();
        removeTheArticle();
        repViewsCase();
        theArticleIsRemoved();
    }
    // And I remove the article
    static void removeTheArticle(){
        attachedArticlesController = new attachedArticlesControllerMVN();
        attachedArticlesController.getAttachedKnowledgeList();
        attachedArticlesController.attachedArticleId = foundArticle.Id;
        attachedArticlesController.removeArticle();
    }
    // The article is removed
    static void theArticleIsRemoved(){        
        System.assertEquals(0, attachedArticlesController.getAttachedKnowledgeList().size());
    }
    
}