public class BI_ATL_BO {
    private static BI_ATL_BO instance = new BI_ATL_BO();
    private BI_ATL_BO() {
    }

    public static BI_ATL_BO getInstance(){
        return instance;
    }

    public void createGASHistoryByATLChanged(List<Account_Territory_Loader_vod__c> lstATL, Map<Id, Account_Territory_Loader_vod__c> mapATLCompare){
        Map<String, String> dataToStoreGASHistory = new Map<String, String>();
        Map<String, Id> mapTerrId = new Map<String, Id>();
        Map<String, String> mapAccountUserFromDCR = new Map<String, String>();
        Map<String, Date> mapAccountDTRequestFromDCR = new Map<String, Date>();

        for(Account_Territory_Loader_vod__c atlToCheck : lstATL){
        	String newTerrs = atlToCheck.Territory_vod__c;
        	String oldTerrs = (mapATLCompare != null && mapATLCompare.get(atlToCheck.Id) != null) ? mapATLCompare.get(atlToCheck.Id).Territory_vod__c : '';
			String diffTerrs = String.isNotBlank(newTerrs) && String.isNotBlank(oldTerrs) ? newTerrs.substringAfter(oldTerrs) : newTerrs;
			diffTerrs = String.isNotBlank(diffTerrs) ? diffTerrs.replace(';', '') : '';

            if(diffTerrs != ''){
                String terrName = diffTerrs;
                dataToStoreGASHistory.put(atlToCheck.Account_vod__c, terrName);
                mapAccountUserFromDCR.put(atlToCheck.Account_vod__c, UserInfo.getUserId());
                mapAccountDTRequestFromDCR.put(atlToCheck.Account_vod__c, System.Today());
                mapTerrId.put(terrName, null);
            }
        }

        //GET USERS THAT CREATED DCR ABOUT THIS ALIGNMENT
        //BY ACCOUNT, DCR TYPE ACCOUNT, AND THE LAST CREATION, CHECKING TERRITORY OF USER
        for(Data_Change_Request_vod__c dcrCheck : [SELECT Account_vod__c, Status_vod__c, CreatedById, CreatedDate 
                                                     FROM Data_Change_Request_vod__c 
                                                    WHERE Status_vod__c = 'Processed_vod' 
                                                      AND Account_vod__c IN: dataToStoreGASHistory.keySet() 
                                                      AND Type_vod__c = 'New_vod'
                                                 ORDER BY CreatedDate DESC]){
            if(mapAccountUserFromDCR.containsKey(dcrCheck.Account_vod__c) && mapAccountUserFromDCR.get(dcrCheck.Account_vod__c) == UserInfo.getUserId()){
                mapAccountUserFromDCR.put(dcrCheck.Account_vod__c, dcrCheck.CreatedById);
                Date dcrCreatedDate = dcrCheck.CreatedDate.date();
                mapAccountDTRequestFromDCR.put(dcrCheck.Account_vod__c, dcrCreatedDate);
            }
        }

        if(!mapTerrId.keySet().isEmpty()){
            //GET ID'S OF TERRITORY
            for(Territory terrLoaded : [SELECT Id, Name 
                                          FROM Territory 
                                         WHERE Name IN: mapTerrId.keySet()]){
                mapTerrId.put(terrLoaded.Name, terrLoaded.Id);
            }

            List<GAS_History_BI__c> lstGASHistory = new List<GAS_History_BI__c>();

            for(String accCheck : dataToStoreGASHistory.keySet()){
                String terrName = dataToStoreGASHistory.get(accCheck);
                String terrId = mapTerrId.get(terrName);

                GAS_History_BI__c gasHistoryBI = new GAS_History_BI__c();
                if(String.isNotBlank(terrId)){
                	gasHistoryBI.Account_BI__c = accCheck;
	                gasHistoryBI.Operation_BI__c = 'add';
	                gasHistoryBI.Territory_ID_BI__c = terrId;
	                gasHistoryBI.User_Territory_Name_BI__c = terrName;
	                gasHistoryBI.GAS_Alignment_Date_BI__c = mapAccountDTRequestFromDCR.get(accCheck);
	                gasHistoryBI.GAS_Alignment_Made_By_BI__c = mapAccountUserFromDCR.get(accCheck);
	                lstGASHistory.add(gasHistoryBI);
                }
            }

            if(!lstGASHistory.isEmpty()){
                insert lstGASHistory;
            }
        }
    }
}