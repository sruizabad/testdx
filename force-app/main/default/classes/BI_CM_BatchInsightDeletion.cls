/**
 * Batch for insight deletion.
 */
global class BI_CM_BatchInsightDeletion 
    implements Database.Batchable<sObject>, Schedulable {
    
    String query;
    String INSIGHT_ARCHIVE_STATUS = 'Archive';

    /**
     * Batch constructor that make up the query which returns the items 
     * to be deactivated for the type indicated by parameter
     *
     * @param      artType  The article type
     *
     */
    global BI_CM_BatchInsightDeletion() {
        //Insights that have been manually set to be deleted
        //or insights in archive status with LastModifiedDate < 2 years (let's consider 730 days)
        query = 'SELECT Id FROM BI_CM_Insight__c' +
            ' WHERE (BI_CM_To_be_deleted__c = TRUE '+
            ' OR (BI_CM_Status__c = \''+INSIGHT_ARCHIVE_STATUS+'\' AND LastModifiedDate < LAST_N_DAYS:730)) LIMIT 50000';

    }
    
    /**
     * Batch start
     *
     * @param      BC    Database.BatchableContext
     *
     * @return     Database.QueryLocator
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('### - BI_CM_BatchInsightDeletion - getQueryLocator - query = ' + query);
        return Database.getQueryLocator(query);
    }

    /**
     * Batch execute
     *
     * @param      BC    Database.BatchableContext
     * @param      scope  The scope
     * 
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        delete scope;
    }
    
    /**
     * Batch finish
     *
     * @param      BC    Database.BatchableContext
     *
     */
    global void finish(Database.BatchableContext BC) {}
    
     /**
     * Batch schedule execute
     *
     * @param      sc    SchedulableContext
     *
     */
    global void execute(SchedulableContext sc){
        Database.executeBatch(new BI_CM_BatchInsightDeletion());
    }
}