@isTest
private class BI_CM_BatchInsightDeletion_Test {

	@Testsetup
	static void setUp() {
		User repUserBE = BI_CM_TestDataUtility.getSalesRepUser('BE', 0);
		User adminUserBE = BI_CM_TestDataUtility.getDataStewardUser('BE', 0);
		User adminUserGB = BI_CM_TestDataUtility.getDataStewardUser('GB', 1);
		List<BI_CM_Tag__c> newTags = new List<BI_CM_Tag__c>();

		System.runAs(repUserBE){
			List<BI_CM_Insight__c> draftInsights = BI_CM_TestDataUtility.newInsights('BE', 'Draft'); 
		}

		System.runAs(adminUserGB){
			List<BI_CM_Insight__c> draftInsights = BI_CM_TestDataUtility.newInsights('GB', 'Draft'); 
		}
		
	}

	
	private static testMethod void test_notToBeDeletedRightAdmin(){

		String CRON_EXP = '0 0 0/1 1/1 * ? *';
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name, BI_CM_Status__c
																		FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' 
																		AND BI_CM_Country_code__c = 'GB']);
		System.runAs(admin){
			for (BI_CM_Insight__c insight:insights){
				insight.BI_CM_Status__c = 'Archive';
			}
			update insights;
		}

		Test.startTest();
		//Schedule job
		system.schedule('job', CRON_EXP, new BI_CM_BatchInsightDeletion());
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'GB']);
	    System.assertEquals(201,insights.size());
	}

	private static testMethod void test_tobeDeletedRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name, BI_CM_To_be_deleted__c 
																		FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' 
																		AND BI_CM_Country_code__c = 'BE']);
		
		System.runAs(admin){
			for (BI_CM_Insight__c insight:insights){
				insight.BI_CM_To_be_deleted__c = TRUE;
			}
			update insights;
		}

		Test.startTest();
		//Execute batch without scheduling the job
	    Id batchID;
        BI_CM_BatchInsightDeletion c = new BI_CM_BatchInsightDeletion();
        batchID = Database.executeBatch(c,2000);
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE']);
	    System.assertEquals(0,insights.size());
	}

}