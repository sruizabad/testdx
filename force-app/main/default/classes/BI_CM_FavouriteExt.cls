/**
 *  Contains the method to return to the Favourite object page
 *
 @author    Omega CRM
 @created   2018-04-13
 @version   1.0
 @since     42.0 (Force.com ApiVersion)
 *
 @changelog
 * 2018-04-10 Omega CRM
 * - Created
 */

public with sharing class BI_CM_FavouriteExt {

    
    /////////////////////////////////// -=BEGIN CONSTRUCTOR=- /////////////////////////////////////
   /**
    * The constructor
    *
    @author     Omega CRM
    @created    2018-04-13
    @version    1.0
    @since  42.0 (Force.com ApiVersion)
    *
    @changelog
     * 2018-04-10 Omega CRM
    * - Created
    */
     public BI_CM_FavouriteExt(ApexPages.StandardController stdController) {

    }
    
     /////////////////////////////////// -=END CONSTRUCTOR=- ///////////////////////////////////////
    
    
    //********************************* -=BEGIN public methods=- **********************************
   /**
    * This method is used to return to the Favourite object page
    *
    @author  Omega CRM
    @created 2018-04-13
    @version 1.0
    @since   42.0 (Force.com ApiVersion)
    *
    @return     Pagereference
    *
    @changelog
    *  2018-04-10  Omega CRM
    * - Created
    */
   
    public PageReference returnFavourites{ 
        get { return new PageReference('/' + BI_CM_Favourite__c.sObjectType.getDescribe().getKeyPrefix());}
       
    }

	//********************************* -=END public methods=- **********************************
}