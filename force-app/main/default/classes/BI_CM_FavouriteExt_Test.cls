@isTest
private class BI_CM_FavouriteExt_Test {

	@Testsetup
	static void setUp() {
		User repUserBE = BI_CM_TestDataUtility.getSalesRepUser('BE', 0);
		User adminUserBE = BI_CM_TestDataUtility.getDataStewardUser('BE', 0);
		List<BI_CM_Tag__c> newTags = new List<BI_CM_Tag__c>();

		System.runAs(adminUserBE){
			newTags = BI_CM_TestDataUtility.newTags('BE', TRUE, 0); 
		}
	}


	static testmethod void returnToFavouriteObjectRight(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];

 		BI_CM_Tag__c tag = [SELECT Id FROM BI_CM_Tag__c WHERE BI_CM_Country_code__c = 'BE' LIMIT 1];
		BI_CM_Favourite__c favourite = new BI_CM_Favourite__c(BI_CM_Tag__c = tag.Id);

		Test.startTest();
		System.runAs(salesRep){
			//Create a record
			insert favourite;
			//Page reference to the VF Page
			PageReference pageRef = Page.BI_CM_FavouriteNew;
			Test.setCurrentPage(pageRef);
 			 
			//Pass necessary parameter (not necessary in our case)
			//pageRef.getParameters().put('Id',favourite.id);   
			
			//Pass the object to controller     
			ApexPages.StandardController stc = new ApexPages.StandardController(favourite);

			//Call controller
			BI_CM_FavouriteExt objCtrl = new BI_CM_FavouriteExt(stc);
			
			//Call pageRef method
			PageReference objPageRef = objCtrl.returnFavourites;

			//Expected result
			PageReference expectedPage = new PageReference('/' + BI_CM_Favourite__c.sObjectType.getDescribe().getKeyPrefix());

			System.assertNotEquals (null,objPageRef);
			System.assertEquals(expectedPage.getUrl(), objPageRef.getUrl());

		}
	    Test.stopTest();
	}

	static testmethod void returnToFavouriteObjectWrong(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];

 		BI_CM_Tag__c tag = [SELECT Id FROM BI_CM_Tag__c WHERE BI_CM_Country_code__c = 'BE' LIMIT 1];
		BI_CM_Favourite__c favourite = new BI_CM_Favourite__c(BI_CM_Tag__c = tag.Id);

		Test.startTest();
		System.runAs(salesRep){
			//Create a record
			insert favourite;
			//Page reference to the VF Page
			PageReference pageRef = new PageReference('/000');
			Test.setCurrentPage(pageRef);
 			 
			//Pass necessary parameter (not necessary in our case)
			//pageRef.getParameters().put('Id',favourite.id);   
			
			//Pass the object to controller     
			ApexPages.StandardController stc = new ApexPages.StandardController(favourite);

			//Call controller
			BI_CM_FavouriteExt objCtrl = new BI_CM_FavouriteExt(stc);
			
			//Call pageRef method
			PageReference objPageRef = objCtrl.returnFavourites;

			//Expected result
			PageReference expectedPage = new PageReference('/' + BI_CM_Favourite__c.sObjectType.getDescribe().getKeyPrefix());

			System.assertNotEquals (null,objPageRef);
			System.assertNotEquals (expectedPage, objPageRef);

		}
	    Test.stopTest();
	}

}