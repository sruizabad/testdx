public class BI_CM_InsightCommentTriggerHandler implements BI_CM_TriggerInterface {

	public map<String, map<Id, Boolean>> groupMembership = new map<String, map<Id, Boolean>>(); 
	public Map<String, Boolean> currentPermissions = new Map<String, Boolean> ();

	/**
     * Constructs the object.
     */
    public BI_CM_InsightCommentTriggerHandler() {}
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {

 		//Admin + Report builder users can't edit a comment
        if (Trigger.isInsert || Trigger.isUpdate) {

        	// The user must have access to the Insight Country Code 
            map<Id, Group> cimGroups = new map<Id, Group>([SELECT Id FROM Group WHERE DeveloperName LIKE 'BI_CM_ADMIN%' OR DeveloperName LIKE 'BI_CM_SALES%']);
            list<GroupMember> cimGroupMembers = new list<GroupMember>([SELECT Id, UserOrGroupId, Group.DeveloperName FROM GroupMember WHERE GroupId IN :cimGroups.KeySet()]);
            for(GroupMember gm : cimGroupMembers){
                if(groupMembership.get(gm.Group.DeveloperName) != null){
                    groupMembership.get(gm.Group.DeveloperName).put(gm.userOrGroupId, true);
                }else{
                    map<Id, Boolean> newEntry = new map<Id, Boolean>();
                    newEntry.put(gm.userOrGroupId, true);
                    groupMembership.put(gm.Group.DeveloperName, newEntry);
                }
            }
   
            System.debug('### - BI_CM_InsightTriggerHandler - bulkBefore - groupMembership = ' + groupMembership);

            currentPermissions = getCurrentUserPermissions();
            System.debug('### - BI_CM_InsightCommentTriggerHandler - bulkBefore - currentPermissions = ' + currentPermissions);
        }

    }

     /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter() {}
    
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     * 
     * @param      so    The SObject (BI_CM_Insight__c)
     * 
     */
    public void beforeInsert(SObject so) {}

    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     * 
     * @param      oldSo  The old SObject (BI_CM_Insight__c)
     * @param      so     The SObject (BI_CM_Insight__c)
     * 
     */
    public void beforeUpdate(SObject oldSo, SObject so) {

    	//Admin + Report builder users can't edit a comment of a country where they aren't allowed to work
    	if (currentPermissions.get('isREPORTBUILDER') == true && checkCountryCode(so) == false){
    		so.addError(Label.BI_CM_Flow_dont_have_permissions);
    	}
    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     * 
     * @param      so    The SObject (BI_CM_Insight__c)
     * 
     */
    public void beforeDelete(SObject so) {}

    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     * 
     * @param      so    The SObject (BI_CM_Insight__c)
     * 
     */
    public void afterInsert(SObject so) {}
    
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     * 
     * @param      oldSo  The old SObject (BI_CM_Insight__c)
     * @param      so     The SObject (BI_CM_Insight__c)
     * 
     */
    public void afterUpdate(SObject oldSo, SObject so) {}
    
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     * 
     * @param      so    The SObject (BI_CM_Insight__c)
     * 
     */
    public void afterDelete(SObject so) {}
    
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {}


	/**
       * Check if the insights Country code is correct
       *
       * @param      sObject      The insight
       * @param      String       To know if we want to compare with the onwer Id or the current user Id
       *
       * @return     Boolean True if the insight Country code is correct
    */
    public boolean checkCountryCode(sObject so){
        BI_CM_Insight_Comment__c currentInsightComment = (BI_CM_Insight_Comment__c)so;

        Id userId = userInfo.getUserId();
        System.debug('1 '+groupMembership.get('BI_CM_ADMIN_'+currentInsightComment.BI_CM_Country_code__c));
        System.debug('all '+groupMembership);
        //check in sales public groups
        if(groupMembership.get('BI_CM_SALES_'+currentInsightComment.BI_CM_Country_code__c) == null 
            || groupMembership.get('BI_CM_SALES_'+currentInsightComment.BI_CM_Country_code__c).get(userId) == null){
               
            //check in admin public groups
            if(groupMembership.get('BI_CM_ADMIN_'+currentInsightComment.BI_CM_Country_code__c) == null 
            || groupMembership.get('BI_CM_ADMIN_'+currentInsightComment.BI_CM_Country_code__c).get(userId) == null){
                return false;
            }
        }
        return true;
    }


	/**
      * Gets the current user permissions (is DataSteward, is Sales Rep, admin...)
      * It queries Profile and PermissionSet to get the inf
      *
      * @return     The current user permissions as a Map.
     */
     public static Map<String, Boolean> getCurrentUserPermissions() {
      List<PermissionSetAssignment> permSetAssign = new list<PermissionSetAssignment>([SELECT Id, PermissionSet.Name
              FROM PermissionSetAssignment
              WHERE Assignee.Id = : UserInfo.getUserId()]);

      User currentUser = new List<User>([SELECT Id, Country_Code_BI__c, Profile.PermissionsAuthorApex, Profile.Type FROM User WHERE Id = :UserInfo.getUserId()]).get(0);

      Boolean isSALES = false;
      Boolean isADMIN = false;
      Boolean isREPORTBUILDER = false;

      for (PermissionSetAssignment ass : permSetAssign) {
          String assPermissionSetName = (ass.PermissionSet.Name).touppercase();
          if (!isSALES) isSALES = Pattern.matches('BI_CM_SALES', assPermissionSetName);
          if (!isADMIN) isADMIN = Pattern.matches('BI_CM_ADMIN', assPermissionSetName);
          if (!isREPORTBUILDER) isREPORTBUILDER = Pattern.matches('BI_CM_REPORT_BUILDER', assPermissionSetName);
      }

      return new Map<String, Boolean> {
          'isSALES'          => isSALES,
          'isADMIN'          => isADMIN,
          'isREPORTBUILDER'  => isREPORTBUILDER
      };
    }

}