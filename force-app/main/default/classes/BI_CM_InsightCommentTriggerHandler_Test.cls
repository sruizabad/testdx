@isTest
public class BI_CM_InsightCommentTriggerHandler_Test {

	@Testsetup
	static void setUp() {
		User repUserBE = BI_CM_TestDataUtility.getSalesRepUser('BE', 0);
		User adminUserBE = BI_CM_TestDataUtility.getDataStewardUser('BE', 0);
		User adminUserGB = BI_CM_TestDataUtility.getDataStewardUser('GB', 1);
		User adminReportBuilderBUserBE = BI_CM_TestDataUtility.getDataStewardReportBuilderUser('BE', 0);
		User adminReportBuilderBUserGB = BI_CM_TestDataUtility.getDataStewardReportBuilderUser('GB', 1);

		System.runAs(repUserBE){
			List<BI_CM_Insight__c> draftInsights = BI_CM_TestDataUtility.newInsightsWithComments('BE', 'Draft'); 
		}

		System.runAs(adminUserGB){
			List<BI_CM_Insight__c> draftInsights = BI_CM_TestDataUtility.newInsightsWithComments('GB', 'Draft'); 
		}
	}

	//Admin+Report builder user trying to edit comments for a country where he is not allowed to work
	@isTest static void test_updateRepCommentsWrongAdminReportBuilder(){
		User admRepBuilder = [SELECT Id FROM User WHERE alias = 'admRep' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																								WHERE BI_CM_Country_code__c = 'BE']);

		try {
			Test.startTest();
			System.runAs(admRepBuilder){
		    	update comments;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Flow_dont_have_permissions), e.getMessage()); 
		}	
	}

	@isTest static void test_updateAdmCommentsWrongAdminReportBuilder(){
		User admRepBuilder = [SELECT Id FROM User WHERE alias = 'admRep' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																								WHERE BI_CM_Country_code__c = 'GB']);

		try {
			Test.startTest();
			System.runAs(admRepBuilder){
		    	update comments;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Flow_dont_have_permissions), e.getMessage()); 
		}	
	}

	//Admin+Report builder user edits comments for a country where he is allowed to work
	@isTest static void test_updateRepCommentsRightAdminReportBuilder(){
		User admRepBuilder = [SELECT Id FROM User WHERE alias = 'admRep' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																								WHERE BI_CM_Country_code__c = 'BE']);

		
		Test.startTest();
		System.runAs(admRepBuilder){
		   	update comments;
		}
		Test.stopTest();

		comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																	WHERE BI_CM_Country_code__c = 'BE']);
		System.assertEquals(201,comments.size());
		
	}


	//Admin user edits comments for a country where he is allowed to work
	@isTest static void test_updateRepCommentsRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'admRep' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																								WHERE BI_CM_Country_code__c = 'BE']);

		Test.startTest();
		System.runAs(admin){
		   	update comments;
		}
		Test.stopTest();

		comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																	WHERE BI_CM_Country_code__c = 'BE']);
		System.assertEquals(201,comments.size());
	}

	//Sales rep user edits comments for a country where he is allowed to work
	@isTest static void test_updateRepCommentsRightSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																								WHERE BI_CM_Country_code__c = 'BE']);

		Test.startTest();
		System.runAs(salesRep){
		   	update comments;
		}
		Test.stopTest();

		comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																	WHERE BI_CM_Country_code__c = 'BE']);
		System.assertEquals(201,comments.size());
	}

	//Admin user deletes comments
	@isTest static void test_deleteRepCommentsRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'admRep' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																								WHERE BI_CM_Country_code__c = 'BE']);

		Test.startTest();
		System.runAs(admin){
		   	delete comments;
		}
		Test.stopTest();

		comments = new List<BI_CM_Insight_Comment__c>([SELECT Name FROM BI_CM_Insight_Comment__c
																	WHERE BI_CM_Country_code__c = 'BE']);
		System.assertEquals(0,comments.size());
	}
}