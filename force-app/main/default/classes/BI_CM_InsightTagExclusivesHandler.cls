global class BI_CM_InsightTagExclusivesHandler implements Process.Plugin {

    global Process.PluginResult invoke(Process.PluginRequest request) { 
        String insightId = (String) request.inputParameters.get('insightId');
        String tagId = (String) request.inputParameters.get('tagId');
        String errorString = canInsertInsightTag(insightId, tagId);
        Boolean error = true;
        if(errorString==null){
            error = false;
        }

        Map<String,Object> result = new Map<String,Object>();
        result.put('errorString', errorString);
        result.put('error', error);
        return new Process.PluginResult(result);
    }

    global Process.PluginDescribeResult describe() { 
        Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 
        //result.Name = 'Look Up Account ID By Name';
        //result.Tag = 'Account Classes';
        result.inputParameters = new 
            List<Process.PluginDescribeResult.InputParameter>{ 
                new Process.PluginDescribeResult.InputParameter('insightId', 
                Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('tagId', 
                Process.PluginDescribeResult.ParameterType.STRING, true) 
            }; 
        result.outputParameters = new 
            List<Process.PluginDescribeResult.OutputParameter>{              
                new Process.PluginDescribeResult.OutputParameter('errorString', 
                Process.PluginDescribeResult.ParameterType.STRING),             
                new Process.PluginDescribeResult.OutputParameter('error', 
                Process.PluginDescribeResult.ParameterType.BOOLEAN)
            }; 
        return result; 
    }

    public static String canInsertInsightTag(String insightId, String tagId){

        Map<Id,Map<Id,Boolean>> map_Insight_Tag = new Map<Id,Map<Id,Boolean>>(); //insightId --> tagsIds (tagids, tagParentIds, tagGrandparentIds)
        Map<Id,Map<Id,Boolean>> map_Tag_TagExclusive = new Map<Id,Map<Id,Boolean>>(); //tagId --> exclusives tagsIds
        Map<Id, List<Id>> map_Tag_ChildParentGrandparentList = new Map<Id, List<Id>>(); //tagId --> tagid, tagParentId, tagGrandparentId
        Map<Id, BI_CM_Tag__c> tagMap = new Map<Id, BI_CM_Tag__c>(); //tagId --> TagObject info

        Set<Id> tagIds = new Set<Id>();
        tagIds.add(tagId);
        //add also all tags ids from existing insightTags related with the insights related in the news insightTags
        for(BI_CM_Insight_Tag__c insTag : [SELECT Id, BI_CM_Insight__c, BI_CM_Tag__c FROM BI_CM_Insight_Tag__c WHERE BI_CM_Insight__c = :insightId]){
            tagIds.add(insTag.BI_CM_Tag__c);
        }
        //System.debug('*******  tagIds: '+tagIds);

        //get all tags ids (tags in new insightTags, and its parents and grandparents)
        //complete the map "map_Tag_ChildParentGrandparentList"
        //complete the map "tagMap"
        Set<Id> existingTags_ChildParentGrandParent = new Set<Id>();
        for(BI_CM_Tag__c tag : [SELECT Id, Name, BI_CM_Hierarchy_level__c, BI_CM_Country_code__c, BI_CM_Active__c,
                                    BI_CM_Parent__c, BI_CM_Parent__r.Name, BI_CM_Parent__r.BI_CM_Hierarchy_level__c, BI_CM_Parent__r.BI_CM_Country_code__c, BI_CM_Parent__r.BI_CM_Active__c,
                                    BI_CM_Parent__r.BI_CM_Parent__c , BI_CM_Parent__r.BI_CM_Parent__r.Name, BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Hierarchy_level__c, BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Country_code__c, BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Active__c
                                    FROM BI_CM_Tag__c 
                                    WHERE Id IN :tagIds]){

            List<Id> parentGrandparentList = new List<Id>();
            //add tagId
            existingTags_ChildParentGrandParent.add(tag.Id);
            parentGrandparentList.add(tag.Id);
            BI_CM_Tag__c t1 = new BI_CM_Tag__c(Id = tag.Id, Name = tag.Name, BI_CM_Hierarchy_level__c = tag.BI_CM_Hierarchy_level__c, BI_CM_Country_code__c = tag.BI_CM_Country_code__c, BI_CM_Active__c = tag.BI_CM_Active__c);
            tagMap.put(tag.Id, t1);
            if(tag.BI_CM_Parent__c!=null){
                //if exist, add the parentTagId
                existingTags_ChildParentGrandParent.add(tag.BI_CM_Parent__c);
                parentGrandparentList.add(tag.BI_CM_Parent__c);                    
                BI_CM_Tag__c t2 = new BI_CM_Tag__c(Id = tag.BI_CM_Parent__c, Name = tag.BI_CM_Parent__r.Name, BI_CM_Hierarchy_level__c = tag.BI_CM_Parent__r.BI_CM_Hierarchy_level__c, BI_CM_Country_code__c = tag.BI_CM_Parent__r.BI_CM_Country_code__c, BI_CM_Active__c = tag.BI_CM_Parent__r.BI_CM_Active__c);
                tagMap.put(tag.BI_CM_Parent__c, t2);
                if(tag.BI_CM_Parent__r.BI_CM_Parent__c != null){
                    //if exist, add the grandparentTagId
                    existingTags_ChildParentGrandParent.add(tag.BI_CM_Parent__r.BI_CM_Parent__c);
                    parentGrandparentList.add(tag.BI_CM_Parent__r.BI_CM_Parent__c);                 
                    BI_CM_Tag__c t3 = new BI_CM_Tag__c(Id = tag.BI_CM_Parent__r.BI_CM_Parent__c, Name = tag.BI_CM_Parent__r.BI_CM_Parent__r.Name, BI_CM_Hierarchy_level__c = tag.BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Hierarchy_level__c, BI_CM_Country_code__c = tag.BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Country_code__c, BI_CM_Active__c = tag.BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Active__c);
                    tagMap.put(tag.BI_CM_Parent__r.BI_CM_Parent__c, t3);
                }
            }
            map_Tag_ChildParentGrandparentList.put(tag.Id, parentGrandparentList);
        }
        //System.debug('*******  existingTags_ChildParentGrandParent: '+existingTags_ChildParentGrandParent);            
        //System.debug('*******  map_Tag_ChildParentGrandparentList: '+map_Tag_ChildParentGrandparentList);  

        //for all tagsIds get in previous loop search its exclusives tags
        //complete the map "map_Tag_TagExclusive" (BI_CM_Excluded__c is a bidirectional relationship)
        for(BI_CM_Exclusive__c exc : [SELECT Id, BI_CM_Excluded__c, BI_CM_Tag__c FROM BI_CM_Exclusive__c WHERE BI_CM_Excluded__c IN :existingTags_ChildParentGrandParent OR BI_CM_Tag__c IN :existingTags_ChildParentGrandParent]){
            
            Map<Id,Boolean> map_TagExclusives = new Map<Id,Boolean>();
            if(map_Tag_TagExclusive.get(exc.BI_CM_Tag__c)!=null){
                map_TagExclusives = map_Tag_TagExclusive.get(exc.BI_CM_Tag__c);
            }
            map_TagExclusives.put(exc.BI_CM_Excluded__c, true);
            map_Tag_TagExclusive.put(exc.BI_CM_Tag__c, map_TagExclusives);

            //this lines exist because the relationship is bidirectional
            map_TagExclusives = new Map<Id,Boolean>();
            if(map_Tag_TagExclusive.get(exc.BI_CM_Excluded__c)!=null){
                map_TagExclusives = map_Tag_TagExclusive.get(exc.BI_CM_Excluded__c);
            }
            map_TagExclusives.put(exc.BI_CM_Tag__c, true);
            map_Tag_TagExclusive.put(exc.BI_CM_Excluded__c, map_TagExclusives);

        }
        //System.debug('*******  map_Tag_TagExclusive: '+map_Tag_TagExclusive);          

        //for all insights from new insightTags records get all tags ids (tags and its parents and grandparents)
        //complete the map "map_Insight_Tag"
        for(BI_CM_Insight_Tag__c insTag : [SELECT Id, BI_CM_Insight__c, BI_CM_Tag__c, BI_CM_Tag__r.BI_CM_Parent__c, BI_CM_Tag__r.BI_CM_Parent__r.BI_CM_Parent__c FROM BI_CM_Insight_Tag__c WHERE BI_CM_Insight__c = :insightId]){
            Map<Id, Boolean> map_Tags = new Map<Id, Boolean>();
            if(map_Insight_Tag.get(insTag.BI_CM_Insight__c) != null){
                map_Tags = map_Insight_Tag.get(insTag.BI_CM_Insight__c);
            }

            map_Tags.put(insTag.BI_CM_Tag__c, true);
            if(insTag.BI_CM_Tag__r.BI_CM_Parent__c!=null){
                map_Tags.put(insTag.BI_CM_Tag__r.BI_CM_Parent__c, true);
                if(insTag.BI_CM_Tag__r.BI_CM_Parent__r.BI_CM_Parent__c != null){
                    map_Tags.put(insTag.BI_CM_Tag__r.BI_CM_Parent__r.BI_CM_Parent__c, true);
                }
            }
            map_Insight_Tag.put(insTag.BI_CM_Insight__c, map_Tags);
        }
        //System.debug('*******  map_Insight_Tag: '+map_Insight_Tag); 

        Map<Id, Boolean> map_Tags = map_Insight_Tag.get(insightId);
        //System.debug('*******  map_Tags: '+map_Tags); 
        if(map_Tags != null){
            //get all exclusive tags from record tag and record parent tag and record grandparent tag
            Map<Id, Boolean> map_TagExclusive = new Map<Id, Boolean>(); 
            for(Id currentTagId : map_Tag_ChildParentGrandparentList.get(tagId)){
                if(map_Tag_TagExclusive.get(currentTagId) != null){
                    map_TagExclusive.putAll(map_Tag_TagExclusive.get(currentTagId));
                }
            }
            //System.debug('*******  map_TagExclusive: '+map_TagExclusive); 

            //search if the record can be inserted (don't exist any ther record for the insight with any tag, parentTag or grandparentTag exclusive with tag, parentTag or grandparentTag)
            if(!map_TagExclusive.isEmpty()){
                Boolean error = false;
                String errorString = '';
                for(Id currentTagId : map_TagExclusive.keySet()){
                    if(map_Tags.get(currentTagId) != null){
                        BI_CM_Tag__c t1 = tagMap.get(currentTagId);
                        errorString = '\n\n1) '+t1.Name+' ('
                                            //+Schema.BI_CM_Tag__c.fields.Id.getDescribe().getLabel()+' = '+t1.Id+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Hierarchy_level__c.getDescribe().getLabel()+' = '+getHierarchyPickListTranslate(t1.BI_CM_Hierarchy_level__c)+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Country_code__c.getDescribe().getLabel()+' = '+getHierarchyCuntryCodeTranslate(t1.BI_CM_Country_code__c)+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Active__c.getDescribe().getLabel()+' = '+t1.BI_CM_Active__c+') ';
                        for(Id tId : map_Tag_ChildParentGrandparentList.get(tagId)){
                            BI_CM_Tag__c t2 = tagMap.get(tId);
                            if(t2.BI_CM_Hierarchy_level__c == t1.BI_CM_Hierarchy_level__c){
                                errorString += '\n2) '+t2.Name+' ('
                                            //+Schema.BI_CM_Tag__c.fields.Id.getDescribe().getLabel()+' = '+t2.Id+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Hierarchy_level__c.getDescribe().getLabel()+' = '+getHierarchyPickListTranslate(t2.BI_CM_Hierarchy_level__c)+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Country_code__c.getDescribe().getLabel()+' = '+getHierarchyCuntryCodeTranslate(t2.BI_CM_Country_code__c)+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Active__c.getDescribe().getLabel()+' = '+t2.BI_CM_Active__c+')';
                            }
                        }
                        error = true;
                        break;
                    }
                }

                if(error){
                    //System.debug('+++++ errorString: ' + Label.BI_CM_Exclusive_Error+': '+errorString);
                    return Label.BI_CM_Exclusive_Error+': '+errorString; 
                }
            }
        }
        return null;
    }

    private static String getHierarchyPickListTranslate(String value){
        Schema.DescribeFieldResult fieldResult = BI_CM_Tag__c.BI_CM_Hierarchy_level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        String result = value;
        for( Schema.PicklistEntry f : ple){
            if(value.equalsIgnoreCase(f.getValue())){
                result = f.getLabel();
                break;
            }
        }       
        return result;
    }
    private static String getHierarchyCuntryCodeTranslate(String value){
        Schema.DescribeFieldResult fieldResult = BI_CM_Tag__c.BI_CM_Country_code__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        String result = value;
        for( Schema.PicklistEntry f : ple){
            if(value.equalsIgnoreCase(f.getValue())){
                result = f.getLabel();
                break;
            }
        }       
        return result;
    }
}