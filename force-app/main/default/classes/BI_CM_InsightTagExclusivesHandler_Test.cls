@isTest
private class BI_CM_InsightTagExclusivesHandler_Test{
    
    @testSetup
    static void setup(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', ProfileId = p.Id,
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='standarduser' + DateTime.now().getTime() + '@testorg.com');

        String myUserId = UserInfo.getUserId();
        System.runAs(u) {
            Group g = [SELECT Id FROM Group WHERE DeveloperName LIKE 'BI_CM_ADMIN_GB' LIMIT 1];
            GroupMember gm = new GroupMember(UserOrGroupId=myUserId, GroupId=g.Id);
            insert gm;
        }

        BI_CM_Tag__c tag1 = new BI_CM_Tag__c(Name= 'Level1', BI_CM_Active__c=true, BI_CM_Country_code__c='GB', BI_CM_Hierarchy_level__c='1');
        insert tag1;
        BI_CM_Tag__c tag21 = new BI_CM_Tag__c(Name= 'Level2.1', BI_CM_Active__c=true, BI_CM_Country_code__c='GB', BI_CM_Hierarchy_level__c='2', BI_CM_Parent__c=tag1.Id);
        insert tag21;
        BI_CM_Tag__c tag22 = new BI_CM_Tag__c(Name= 'Level2.2', BI_CM_Active__c=true, BI_CM_Country_code__c='GB', BI_CM_Hierarchy_level__c='2', BI_CM_Parent__c=tag1.Id);
        insert tag22;
        BI_CM_Tag__c tag23 = new BI_CM_Tag__c(Name= 'Level2.3', BI_CM_Active__c=true, BI_CM_Country_code__c='GB', BI_CM_Hierarchy_level__c='2', BI_CM_Parent__c=tag1.Id);
        insert tag23;
        BI_CM_Tag__c tag31 = new BI_CM_Tag__c(Name= 'Level3.1', BI_CM_Active__c=true, BI_CM_Country_code__c='GB', BI_CM_Hierarchy_level__c='3', BI_CM_Parent__c=tag21.Id);
        insert tag31;
        BI_CM_Tag__c tag32 = new BI_CM_Tag__c(Name= 'Level3.2', BI_CM_Active__c=true, BI_CM_Country_code__c='GB', BI_CM_Hierarchy_level__c='3', BI_CM_Parent__c=tag22.Id);
        insert tag32;

        BI_CM_Tag__c tag1b = new BI_CM_Tag__c(Name= 'Level1b', BI_CM_Active__c=true, BI_CM_Country_code__c='GB', BI_CM_Hierarchy_level__c='1');
        insert tag1b;
        BI_CM_Tag__c tag2b = new BI_CM_Tag__c(Name= 'Level2b', BI_CM_Active__c=true, BI_CM_Country_code__c='GB', BI_CM_Hierarchy_level__c='2', BI_CM_Parent__c=tag1b.Id);
        insert tag2b;

        BI_CM_Exclusive__c ex = new BI_CM_Exclusive__c(BI_CM_Excluded__c=tag21.Id, BI_CM_Tag__c=tag22.Id);
        insert ex;
        BI_CM_Exclusive__c ex2 = new BI_CM_Exclusive__c(BI_CM_Excluded__c=tag21.Id, BI_CM_Tag__c=tag23.Id);
        insert ex2;
        BI_CM_Exclusive__c ex3 = new BI_CM_Exclusive__c(BI_CM_Excluded__c=tag22.Id, BI_CM_Tag__c=tag23.Id);
        insert ex3;

        BI_CM_Insight__c ins = new BI_CM_Insight__c(BI_CM_Country_code__c='GB', BI_CM_Insight_description__c='Insight description', BI_CM_Status__c='Draft');
        insert ins;

        BI_CM_Insight_Tag__c iT = new BI_CM_Insight_Tag__c(BI_CM_Insight__c=ins.Id, BI_CM_Tag__c=tag2b.Id);
        insert iT;

    }

    static testmethod void testOk(){
        BI_CM_Insight__c ins = [SELECT Id FROM BI_CM_Insight__c LIMIT 1];
        BI_CM_Tag__c tg = [SELECT Id FROM BI_CM_Tag__c WHERE Name='Level3.1' LIMIT 1];

        BI_CM_InsightTagExclusivesHandler plugin = new BI_CM_InsightTagExclusivesHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = ins.Id;
        string tagId = tg.Id;
        
        inputParams.put('insightId', insightId);
        inputParams.put('tagId', tagId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        plugin.invoke(request);
        Process.PluginDescribeResult result = plugin.describe();
    }

    static testmethod void testWrong(){
        BI_CM_Insight__c ins = [SELECT Id FROM BI_CM_Insight__c LIMIT 1];
        BI_CM_Tag__c tg = [SELECT Id FROM BI_CM_Tag__c WHERE Name='Level3.2' LIMIT 1];
        BI_CM_Tag__c tgForIT = [SELECT Id FROM BI_CM_Tag__c WHERE Name='Level3.1' LIMIT 1];
        BI_CM_Insight_Tag__c iT = new BI_CM_Insight_Tag__c(BI_CM_Insight__c=ins.Id, BI_CM_Tag__c=tgForIT.Id);
        insert iT;

        BI_CM_InsightTagExclusivesHandler plugin = new BI_CM_InsightTagExclusivesHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = ins.Id;
        string tagId = tg.Id;
        
        inputParams.put('insightId', insightId);
        inputParams.put('tagId', tagId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        plugin.invoke(request);
        Process.PluginDescribeResult result = plugin.describe();
    }
}