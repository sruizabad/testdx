global class BI_CM_InsightTagHandler implements Process.Plugin {

	global Process.PluginResult invoke(Process.PluginRequest request) { 
		String insightId = (String) request.inputParameters.get('insightId');
		String errorString = canInsertInsightTag(insightId);
		Boolean error = true;
		if(errorString==null){
			error = false;
		}

		Map<String,Object> result = new Map<String,Object>();
		result.put('errorString', errorString);
		result.put('error', error);
		return new Process.PluginResult(result);
	}

	global Process.PluginDescribeResult describe() { 
		Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 

		result.inputParameters = new 
			List<Process.PluginDescribeResult.InputParameter>{ 
				new Process.PluginDescribeResult.InputParameter('insightId', 
				Process.PluginDescribeResult.ParameterType.STRING, true) 
			}; 
		result.outputParameters = new 
			List<Process.PluginDescribeResult.OutputParameter>{              
				new Process.PluginDescribeResult.OutputParameter('errorString', 
				Process.PluginDescribeResult.ParameterType.STRING),             
				new Process.PluginDescribeResult.OutputParameter('error', 
				Process.PluginDescribeResult.ParameterType.BOOLEAN)
			}; 
		return result; 
	}

	public static String canInsertInsightTag(String insightId){

		BI_CM_Insight__c insight = [SELECT Id, BI_CM_Status__c from BI_CM_Insight__c where id =:insightId];
		Map<String, Boolean> currentPermissions = getCurrentUserPermissions();
		//System.debug('*******  BI_CM_InsightTagHandler  >>> currentPermissions : '+currentPermissions); 
		
		if (currentPermissions.get('isREPORTBUILDER') == true && currentPermissions.get('isADMIN') == false
			&& currentPermissions.get('isSALES') == false) {
			return Label.BI_CM_Flow_dont_have_permissions;
		}
		else if(insight.BI_CM_Status__c != 'Draft' && currentPermissions.get('isADMIN') == false ){
            return Label.BI_CM_Action_not_allowed_for_active_insight;
        }        
       
        return null;
  	}

 	/**
      * Gets the current user permissions (is DataSteward, is Sales Rep, admin...)
      * It queries Profile and PermissionSet to get the inf
      *
      * @return     The current user permissions as a Map.
     */
     public static Map<String, Boolean> getCurrentUserPermissions() {
      List<PermissionSetAssignment> permSetAssign = new list<PermissionSetAssignment>([SELECT Id, PermissionSet.Name
              FROM PermissionSetAssignment
              WHERE Assignee.Id = : UserInfo.getUserId()]);

      User currentUser = new List<User>([SELECT Id, Country_Code_BI__c, Profile.PermissionsAuthorApex, Profile.Type FROM User WHERE Id = :UserInfo.getUserId()]).get(0);

      Boolean isSALES = false;
      Boolean isADMIN = false;
      Boolean isREPORTBUILDER = false;

      for (PermissionSetAssignment ass : permSetAssign) {
          String assPermissionSetName = (ass.PermissionSet.Name).touppercase();
          if (!isSALES) isSALES = Pattern.matches('BI_CM_SALES', assPermissionSetName);
          if (!isADMIN) isADMIN = Pattern.matches('BI_CM_ADMIN', assPermissionSetName);
          if (!isREPORTBUILDER) isREPORTBUILDER = Pattern.matches('BI_CM_REPORT_BUILDER', assPermissionSetName);
      }

      return new Map<String, Boolean> {
          'isSALES'          => isSALES,
          'isADMIN'          => isADMIN,
          'isREPORTBUILDER'  => isREPORTBUILDER
      };
  	}

}