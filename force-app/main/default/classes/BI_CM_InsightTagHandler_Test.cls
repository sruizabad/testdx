@isTest
private class BI_CM_InsightTagHandler_Test {

	@Testsetup
	static void setUp() {
		User repUserBE = BI_CM_TestDataUtility.getSalesRepUser('BE', 0);
		User adminUserBE = BI_CM_TestDataUtility.getDataStewardUser('BE', 0);
		User reportBuilderBUserBE = BI_CM_TestDataUtility.getReportBuilderUser('BE', 0);
		

		System.runAs(repUserBE){
			List<BI_CM_Insight__c> draftInsights = BI_CM_TestDataUtility.newInsights('BE', 'Draft'); 
		}

		System.runAs(adminUserBE){
			List<BI_CM_Insight__c> submittedInsights = BI_CM_TestDataUtility.newInsights('BE', 'Submitted'); 
			List<BI_CM_Insight__c> hiddenInsights = BI_CM_TestDataUtility.newInsights('BE', 'Hidden'); 
			List<BI_CM_Insight__c> archiveInsights = BI_CM_TestDataUtility.newInsights('BE', 'Archive'); 
		}

	}

	//Insert insight tag in Draft insight
	static testmethod void insertDraftInsightRightSalesRep(){
        User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(salesRep){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();

    }

    static testmethod void insertDraftInsightRightAdmin(){
        User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(admin){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();
    }

    static testmethod void insertDraftInsightWrongReportBuilder(){
        User repBuil = [SELECT Id FROM User WHERE alias = 'repBuil' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(repBuil){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();
    }

    //Insert insight tag in Submitted insight
	static testmethod void insertSubmittedInsightWrongSalesRep(){
        User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name, BI_CM_Status__c FROM BI_CM_Insight__c 
																WHERE BI_CM_Status__c = 'Draft' 
																AND BI_CM_Country_code__c = 'BE' LIMIT 1];

		insight.BI_CM_Status__c = 'Submitted';
		update insight;		

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(salesRep){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();

    }

    static testmethod void insertSubmittedInsightRightAdmin(){
        User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name, BI_CM_Status__c FROM BI_CM_Insight__c 
																WHERE BI_CM_Status__c = 'Submitted' 
																AND BI_CM_Country_code__c = 'BE' LIMIT 1];							

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(admin){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();
    }

    static testmethod void insertSubmittedInsightWrongReportBuilder(){
        User repBuil = [SELECT Id FROM User WHERE alias = 'repBuil' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(repBuil){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();
    }

    //Insert insight tag in Hidden insight
	static testmethod void insertHiddenInsightWrongSalesRep(){
        User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(salesRep){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();

    }

    static testmethod void insertHiddenInsightRightAdmin(){
        User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(admin){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();
    }

    static testmethod void insertHiddenInsightWrongReportBuilder(){
        User repBuil = [SELECT Id FROM User WHERE alias = 'repBuil' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(repBuil){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();
    }

    //Insert insight tag in Archive insight
	static testmethod void insertArchiveInsightWrongSalesRep(){
        User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(salesRep){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();

    }

    static testmethod void insertArchiveInsightRightAdmin(){
        User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
									
        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(admin){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();
    }

    static testmethod void insertArchiveInsightWrongReportBuilder(){
        User repBuil = [SELECT Id FROM User WHERE alias = 'repBuil' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

        BI_CM_InsightTagHandler plugin = new BI_CM_InsightTagHandler();
        Map<String, Object> inputParams = new Map<String, Object>();
        string insightId = insight.Id;
        
        inputParams.put('insightId', insightId);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        Test.startTest();
		System.runAs(repBuil){
		   	plugin.invoke(request);
		}
		Test.stopTest();

        Process.PluginDescribeResult result = plugin.describe();
    }
}