/**
 * Handler for BI_CM_Insight_tag__c Trigger.
 */
public without sharing class BI_CM_InsightTagTriggerHandler implements BI_CM_TriggerInterface {

    public list<BI_CM_Insight_subscription__c> isToInsert = new list<BI_CM_Insight_subscription__c>();
    public Map<String, Boolean> currentPermissions = new Map<String, Boolean> ();
    public list<BI_CM_Insight_subscription__c> insightSubscriptionToDeleteList;

    //IN FUTURE 
    //if is necessary controlling Exclusives from outside the Flow (dataLoader, detail page, ....) uncomment following
    /*
    public Map<Id,Map<Id,Boolean>> map_Insight_Tag = new Map<Id,Map<Id,Boolean>>(); //insightId --> tagsIds (tagids, tagParentIds, tagGrandparentIds)
    public Map<Id,Map<Id,Boolean>> map_Tag_TagExclusive = new Map<Id,Map<Id,Boolean>>(); //tagId --> exclusives tagsIds
    public Map<Id, List<Id>> map_Tag_ChildParentGrandparentList = new Map<Id, List<Id>>(); //tagId --> tagid, tagParentId, tagGrandparentId
    public Map<Id, BI_CM_Tag__c> tagMap = new Map<Id, BI_CM_Tag__c>(); //tagId --> TagObject info
    */
    
    /**
     * Constructs the object.
     */
    public BI_CM_InsightTagTriggerHandler() {}
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {

        //If insight != draf and user permission != admin -> user is not able to execute action
        if (Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete) {
            currentPermissions = getCurrentUserPermissions();
            System.debug('### - BI_CM_InsightTriggerHandler - bulkBefore - currentPermissions = ' + currentPermissions);
        }

        if (Trigger.isInsert) {
            //IN FUTURE 
            //if is necessary controlling Exclusives from outside the Flow (dataLoader, detail page, ....) uncomment following
            //generatedMapsToExclusivesControl(Trigger.new);
        }
    }
    
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter() {
        
        // If a subscription is assigned to the Tag, a BI_CM_Insight_subscription__c record must be created
        if (Trigger.isInsert) {
            set<Id> tagIds = new set<Id>();
            map<Id, set<Id>> tagSubscriptions = new map<Id, set<Id>>();
            
            for(sObject currentRecord : Trigger.new){
                BI_CM_Insight_tag__c it = (BI_CM_Insight_tag__c)currentRecord;
                tagIds.add(it.BI_CM_Tag__c);
            }
            
            System.debug('### - BI_CM_TagTriggerHandler - bulkAfter - tagIds = ' + tagIds);
            
            list<BI_CM_Subscription__c> mySubscriptions = new list<BI_CM_Subscription__c>([SELECT Id, BI_CM_Tag__c FROM BI_CM_Subscription__c WHERE BI_CM_Tag__c IN :tagIds]);
            
            System.debug('### - BI_CM_TagTriggerHandler - bulkAfter - mySubscriptions = ' + mySubscriptions);
            
            for(BI_CM_Subscription__c currentRecord : mySubscriptions){
                if(tagSubscriptions.get(currentRecord.BI_CM_Tag__c) == null){
                    set<Id> newSet = new set<Id>();
                    newSet.add(currentRecord.Id);
                    tagSubscriptions.put(currentRecord.BI_CM_Tag__c, newSet);
                }else{
                    tagSubscriptions.get(currentRecord.BI_CM_Tag__c).add(currentRecord.Id);
                }
            }
            
            System.debug('### - BI_CM_TagTriggerHandler - bulkAfter - tagSubscriptions = ' + tagSubscriptions);
            
            for(sObject currentRecord : Trigger.new){
                BI_CM_Insight_tag__c it = (BI_CM_Insight_tag__c)currentRecord;
                if(tagSubscriptions.get(it.BI_CM_tag__c) != null){
                    for(Id currentId : tagSubscriptions.get(it.BI_CM_tag__c)){
                        BI_CM_Insight_subscription__c newRecord = new BI_CM_Insight_subscription__c();
                        newRecord.BI_CM_Insight__c = it.BI_CM_Insight__c;
                        newRecord.BI_CM_Subscription__c = currentId;
                        isToInsert.add(newRecord);
                    }
                }
            }
            
            System.debug('### - BI_CM_TagTriggerHandler - bulkAfter - isToInsert = ' + isToInsert);
        }

        //If an insight tag is deleted, the corresponding insight subscriptions must be deleted
        if(Trigger.isDelete) {

            list<Id> insightList = new list<Id>();
            list<Id> tagList = new list<Id>();
            List<Id> subscriptionList = new list<Id>();

            for(sObject deletedRecord : Trigger.Old){
                BI_CM_Insight_Tag__c deletedInsighTag = (BI_CM_Insight_Tag__c)deletedRecord;
                insightList.add(deletedInsighTag.BI_CM_Insight__c);
                tagList.add(deletedInsighTag.BI_CM_Tag__c);
            }

            System.debug(LoggingLevel.ERROR, '### - BI_CM_InsightTagTriggerHandler - bulkAfter - insightList = ' + insightList);

            //Get the subscription id list for the deleted insight tags
            for(BI_CM_Subscription__c subs : [SELECT Id FROM BI_CM_Subscription__c WHERE BI_CM_Tag__c  IN :tagList]){
                subscriptionList.add(subs.Id);
            }


            System.debug(LoggingLevel.ERROR, '### - BI_CM_InsightTagTriggerHandler - bulkAfter - subscriptionList = ' + subscriptionList);


            insightSubscriptionToDeleteList = new list<BI_CM_Insight_subscription__c>([SELECT Id, BI_CM_Insight__c
                                                           FROM BI_CM_Insight_subscription__c 
                                                           WHERE BI_CM_Insight__c  IN :insightList AND BI_CM_Subscription__c IN :subscriptionList]); 
            
            System.debug(LoggingLevel.ERROR, '### - BI_CM_InsightTagTriggerHandler - bulkAfter - insightSubscriptionToDeleteList = ' + insightSubscriptionToDeleteList);
            
        }
    
    }
    
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     * 
     * @param      so    The SObject (BI_CM_Insight_tag__c)
     * 
     */
    public void beforeInsert(SObject so) {

         //If insight != draf and user permission != admin -> user is not able to execute action
         //insert is done through the Flow
       /* 
        BI_CM_Insight_tag__c currentInsightTag = (BI_CM_Insight_tag__c)so;
        if(currentInsightTag.BI_CM_Status__c!= 'Draft' && currentPermissions.get('isADMIN') == false){
            so.addError(Label.BI_CM_Action_not_allowed_for_active_insight);
        }*/

        //IN FUTURE 
        //if is necessary controlling Exclusives from outside the Flow (dataLoader, detail page, ....) uncomment following
        //exclusiveControlForInsert(so);
    }
    

    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     * 
     * @param      oldSo  The old SObject (BI_CM_Insight_tag__c)
     * @param      so     The SObject (BI_CM_Insight_tag__c)
     * 
     */
    public void beforeUpdate(SObject oldSo, SObject so) {

         //If insight != draf and user permission != admin -> user is not able to execute action
        BI_CM_Insight_tag__c currentInsightTag = (BI_CM_Insight_tag__c)so;
        if(currentInsightTag.BI_CM_Status__c!= 'Draft' && currentPermissions.get('isADMIN') == false){
            so.addError(Label.BI_CM_Action_not_allowed_for_active_insight);
        }

    }
    
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     * 
     * @param      so    The SObject (BI_CM_Insight_tag__c)
     * 
     */
    public void beforeDelete(SObject so) {

        //If insight != draf and user permission != admin -> user is not able to execute action
        BI_CM_Insight_tag__c currentInsightTag = (BI_CM_Insight_tag__c)so;
        System.debug('### - BI_CM_InsightTagTriggerHandler - beforeDelete - insight status = ' + currentInsightTag.BI_CM_Insight__r.BI_CM_Status__c);

        if(currentInsightTag.BI_CM_Status__c != 'Draft' && currentPermissions.get('isADMIN') == false){
            so.addError(Label.BI_CM_Action_not_allowed_for_active_insight);
        }
    }
    
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     * 
     * @param      so    The SObject (BI_CM_Insight_tag__c)
     * 
     */
    public void afterInsert(SObject so) {}
    
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     * 
     * @param      oldSo  The old SObject (BI_CM_Insight_tag__c)
     * @param      so     The SObject (BI_CM_Insight_tag__c)
     * 
     */
    public void afterUpdate(SObject oldSo, SObject so) {}
    
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     * 
     * @param      so    The SObject (BI_CM_Insight_tag__c)
     * 
     */
    public void afterDelete(SObject so) {}
    
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        // If a subscription is assigned to the Tag, a BI_CM_Insight_subscription__c record must be created
        if (Trigger.isInsert && Trigger.isAfter) {
            Database.insert(isToInsert, false);
        }

        //If an insight tag is deleted, the corresponding insight subscriptions must be deleted
        if(Trigger.isDelete && insightSubscriptionToDeleteList != null) {
            Database.delete(insightSubscriptionToDeleteList, false);
        }
    }

     /**
      * Gets the current user permissions (is DataSteward, is Sales Rep, admin...)
      * It queries Profile and PermissionSet to get the inf
      *
      * @return     The current user permissions as a Map.
     */
     public static Map<String, Boolean> getCurrentUserPermissions() {
      List<PermissionSetAssignment> permSetAssign = new list<PermissionSetAssignment>([SELECT Id, PermissionSet.Name
              FROM PermissionSetAssignment
              WHERE Assignee.Id = : UserInfo.getUserId()]);

      User currentUser = new List<User>([SELECT Id, Country_Code_BI__c, Profile.PermissionsAuthorApex, Profile.Type FROM User WHERE Id = :UserInfo.getUserId()]).get(0);

      Boolean isSALES = false;
      Boolean isADMIN = false;
      Boolean isREPORTBUILDER = false;

      for (PermissionSetAssignment ass : permSetAssign) {
          String assPermissionSetName = (ass.PermissionSet.Name).touppercase();
          if (!isSALES) isSALES = Pattern.matches('BI_CM_SALES', assPermissionSetName);
          if (!isADMIN) isADMIN = Pattern.matches('BI_CM_ADMIN', assPermissionSetName);
          if (!isREPORTBUILDER) isREPORTBUILDER = Pattern.matches('BI_CM_REPORT_BUILDER', assPermissionSetName);
      }

      return new Map<String, Boolean> {
          'isSALES'          => isSALES,
          'isADMIN'          => isADMIN,
          'isREPORTBUILDER'  => isREPORTBUILDER
      };
  }

    //IN FUTURE 
    //if is necessary controlling Exclusives from outside the Flow (dataLoader, detail page, ....) uncomment following
    /*
    public void generatedMapsToExclusivesControl(List<sObject> recordList){
        //get insight ids and tags ids from new insightTags records
        Set<Id> insightIds = new Set<Id>();
        Set<Id> tagIds = new Set<Id>();
        for(sObject currentRecord : recordList){
            BI_CM_Insight_tag__c it = (BI_CM_Insight_tag__c)currentRecord;
            insightIds.add(it.BI_CM_Insight__c);
            tagIds.add(it.BI_CM_Tag__c);
        }
        //System.debug('*******  insightIds: '+insightIds);
        //System.debug('*******  tagIds: '+tagIds);

        //add also all tags ids from existing insightTags related with the insights related in the news insightTags
        for(BI_CM_Insight_Tag__c insTag : [SELECT Id, BI_CM_Insight__c, BI_CM_Tag__c FROM BI_CM_Insight_Tag__c WHERE BI_CM_Insight__c IN :insightIds]){
            tagIds.add(insTag.BI_CM_Tag__c);
        }

        //get all tags ids (tags in new insightTags, and its parents and grandparents)
        //complete the map "map_Tag_ChildParentGrandparentList"
        //complete the map "tagMap"
        Set<Id> existingTags_ChildParentGrandParent = new Set<Id>();
        for(BI_CM_Tag__c tag : [SELECT Id, Name, BI_CM_Hierarchy_level__c, BI_CM_Country_code__c, BI_CM_Active__c,
                                    BI_CM_Parent__c, BI_CM_Parent__r.Name, BI_CM_Parent__r.BI_CM_Hierarchy_level__c, BI_CM_Parent__r.BI_CM_Country_code__c, BI_CM_Parent__r.BI_CM_Active__c,
                                    BI_CM_Parent__r.BI_CM_Parent__c , BI_CM_Parent__r.BI_CM_Parent__r.Name, BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Hierarchy_level__c, BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Country_code__c, BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Active__c
                                    FROM BI_CM_Tag__c 
                                    WHERE Id IN :tagIds]){

            List<Id> parentGrandparentList = new List<Id>();
            //add tagId
            existingTags_ChildParentGrandParent.add(tag.Id);
            parentGrandparentList.add(tag.Id);
            BI_CM_Tag__c t1 = new BI_CM_Tag__c(Id = tag.Id, Name = tag.Name, BI_CM_Hierarchy_level__c = tag.BI_CM_Hierarchy_level__c, BI_CM_Country_code__c = tag.BI_CM_Country_code__c, BI_CM_Active__c = tag.BI_CM_Active__c);
            tagMap.put(tag.Id, t1);
            if(tag.BI_CM_Parent__c!=null){
                //if exist, add the parentTagId
                existingTags_ChildParentGrandParent.add(tag.BI_CM_Parent__c);
                parentGrandparentList.add(tag.BI_CM_Parent__c);                    
                BI_CM_Tag__c t2 = new BI_CM_Tag__c(Id = tag.BI_CM_Parent__c, Name = tag.BI_CM_Parent__r.Name, BI_CM_Hierarchy_level__c = tag.BI_CM_Parent__r.BI_CM_Hierarchy_level__c, BI_CM_Country_code__c = tag.BI_CM_Parent__r.BI_CM_Country_code__c, BI_CM_Active__c = tag.BI_CM_Parent__r.BI_CM_Active__c);
                tagMap.put(tag.BI_CM_Parent__c, t2);
                if(tag.BI_CM_Parent__r.BI_CM_Parent__c != null){
                    //if exist, add the grandparentTagId
                    existingTags_ChildParentGrandParent.add(tag.BI_CM_Parent__r.BI_CM_Parent__c);
                    parentGrandparentList.add(tag.BI_CM_Parent__r.BI_CM_Parent__c);                 
                    BI_CM_Tag__c t3 = new BI_CM_Tag__c(Id = tag.BI_CM_Parent__r.BI_CM_Parent__c, Name = tag.BI_CM_Parent__r.BI_CM_Parent__r.Name, BI_CM_Hierarchy_level__c = tag.BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Hierarchy_level__c, BI_CM_Country_code__c = tag.BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Country_code__c, BI_CM_Active__c = tag.BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Active__c);
                    tagMap.put(tag.BI_CM_Parent__r.BI_CM_Parent__c, t3);
                }
            }
            map_Tag_ChildParentGrandparentList.put(tag.Id, parentGrandparentList);
        }
        //System.debug('*******  existingTags_ChildParentGrandParent: '+existingTags_ChildParentGrandParent);            
        //System.debug('*******  map_Tag_ChildParentGrandparentList: '+map_Tag_ChildParentGrandparentList);            

        //for all tagsIds get in previous loop search its exclusives tags
        //complete the map "map_Tag_TagExclusive" (BI_CM_Excluded__c is a bidirectional relationship)
        for(BI_CM_Exclusive__c exc : [SELECT Id, BI_CM_Excluded__c, BI_CM_Tag__c FROM BI_CM_Exclusive__c WHERE BI_CM_Excluded__c IN :existingTags_ChildParentGrandParent OR BI_CM_Tag__c IN :existingTags_ChildParentGrandParent]){
            
            Map<Id,Boolean> map_TagExclusives = new Map<Id,Boolean>();
            if(map_Tag_TagExclusive.get(exc.BI_CM_Tag__c)!=null){
                map_TagExclusives = map_Tag_TagExclusive.get(exc.BI_CM_Tag__c);
            }
            map_TagExclusives.put(exc.BI_CM_Excluded__c, true);
            map_Tag_TagExclusive.put(exc.BI_CM_Tag__c, map_TagExclusives);

            //this lines exist because the relationship is bidirectional
            map_TagExclusives = new Map<Id,Boolean>();
            if(map_Tag_TagExclusive.get(exc.BI_CM_Excluded__c)!=null){
                map_TagExclusives = map_Tag_TagExclusive.get(exc.BI_CM_Excluded__c);
            }
            map_TagExclusives.put(exc.BI_CM_Tag__c, true);
            map_Tag_TagExclusive.put(exc.BI_CM_Excluded__c, map_TagExclusives);
        }
        //System.debug('*******  map_Tag_TagExclusive: '+map_Tag_TagExclusive);          

        //for all insights from new insightTags records get all tags ids (tags and its parents and grandparents)
        //complete the map "map_Insight_Tag"
        for(BI_CM_Insight_Tag__c insTag : [SELECT Id, BI_CM_Insight__c, BI_CM_Tag__c, BI_CM_Tag__r.BI_CM_Parent__c, BI_CM_Tag__r.BI_CM_Parent__r.BI_CM_Parent__c FROM BI_CM_Insight_Tag__c WHERE BI_CM_Insight__c IN :insightIds]){
            Map<Id, Boolean> map_Tags = new Map<Id, Boolean>();
            if(map_Insight_Tag.get(insTag.BI_CM_Insight__c) != null){
                map_Tags = map_Insight_Tag.get(insTag.BI_CM_Insight__c);
            }

            map_Tags.put(insTag.BI_CM_Tag__c, true);
            if(insTag.BI_CM_Tag__r.BI_CM_Parent__c!=null){
                map_Tags.put(insTag.BI_CM_Tag__r.BI_CM_Parent__c, true);
                if(insTag.BI_CM_Tag__r.BI_CM_Parent__r.BI_CM_Parent__c != null){
                    map_Tags.put(insTag.BI_CM_Tag__r.BI_CM_Parent__r.BI_CM_Parent__c, true);
                }
            }
            map_Insight_Tag.put(insTag.BI_CM_Insight__c, map_Tags);
        }
        //System.debug('*******  map_Insight_Tag: '+map_Insight_Tag); 
    }

    public void exclusiveControlForInsert(sObject so){
        BI_CM_Insight_tag__c it = (BI_CM_Insight_tag__c)so;

        Map<Id, Boolean> map_Tags = map_Insight_Tag.get(it.BI_CM_Insight__c);
        //System.debug('*******  map_Tags: '+map_Tags); 
        if(map_Tags != null){
            //get all exclusive tags from record tag and record parent tag and record grandparent tag
            Map<Id, Boolean> map_TagExclusive = new Map<Id, Boolean>(); 
            for(Id tagId : map_Tag_ChildParentGrandparentList.get(it.BI_CM_Tag__c)){
                if(map_Tag_TagExclusive.get(tagId) != null){
                    map_TagExclusive.putAll(map_Tag_TagExclusive.get(tagId));
                }
            }
            //System.debug('*******  map_TagExclusive: '+map_TagExclusive); 

            //search if the record can be inserted (don't exist any ther record for the insight with any tag, parentTag or grandparentTag exclusive with tag, parentTag or grandparentTag)
            if(!map_TagExclusive.isEmpty()){
                Boolean error = false;
                String errorString = '';
                for(Id tagId : map_TagExclusive.keySet()){
                    if(map_Tags.get(tagId) != null){
                        BI_CM_Tag__c t1 = tagMap.get(tagId);
                        errorString = '\n\n1) '+t1.Name+' ('
                                            //+Schema.BI_CM_Tag__c.fields.Id.getDescribe().getLabel()+' = '+t1.Id+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Hierarchy_level__c.getDescribe().getLabel()+' = '+getHierarchyPickListTranslate(t1.BI_CM_Hierarchy_level__c)+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Country_code__c.getDescribe().getLabel()+' = '+getHierarchyCuntryCodeTranslate(t1.BI_CM_Country_code__c)+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Active__c.getDescribe().getLabel()+' = '+t1.BI_CM_Active__c+') ';
                        for(Id tId : map_Tag_ChildParentGrandparentList.get(it.BI_CM_Tag__c)){
                            BI_CM_Tag__c t2 = tagMap.get(tId);
                            if(t2.BI_CM_Hierarchy_level__c == t1.BI_CM_Hierarchy_level__c){
                                errorString += '\n2) '+t2.Name+' ('
                                            //+Schema.BI_CM_Tag__c.fields.Id.getDescribe().getLabel()+' = '+t2.Id+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Hierarchy_level__c.getDescribe().getLabel()+' = '+getHierarchyPickListTranslate(t2.BI_CM_Hierarchy_level__c)+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Country_code__c.getDescribe().getLabel()+' = '+getHierarchyCuntryCodeTranslate(t2.BI_CM_Country_code__c)+', '
                                            +Schema.BI_CM_Tag__c.fields.BI_CM_Active__c.getDescribe().getLabel()+' = '+t2.BI_CM_Active__c+')';
                            }
                        }
                        error = true;
                        break;
                    }
                }

                if(error){
                    //System.debug('+++++ errorString: ' + Label.BI_CM_Exclusive_Error+': '+errorString);
                    it.addError(Label.BI_CM_Exclusive_Error+': '+errorString); 
                }else{
                    //the tag isn't exclusive with the other tags of the insight
                    //if the record is valid, insert in map "map_Insight_Tag" the entries for the tag, parentTag and grandparentTag for next new records (bulk insert)
                    for(Id tagId : map_Tag_ChildParentGrandparentList.get(it.BI_CM_Tag__c)){
                        map_Tags.put(tagId, true);
                    }
                    map_Insight_Tag.put(it.BI_CM_Insight__c, map_Tags);
                }
            }else{
                //don't exists exclusives tags for the tag
                //if the record is valid, insert in map "map_Insight_Tag" the entries for the tag, parentTag and grandparentTag for next new records (bulk insert)
                for(Id tagId : map_Tag_ChildParentGrandparentList.get(it.BI_CM_Tag__c)){
                    map_Tags.put(tagId, true);
                }
                map_Insight_Tag.put(it.BI_CM_Insight__c, map_Tags);
            }
        }else{
            //don't exists tags for the insight
            //if the record is valid, insert in map "map_Insight_Tag" the entries for the tag, parentTag and grandparentTag for next new records (bulk insert)
            map_Tags = new Map<Id, Boolean>();
            for(Id tagId : map_Tag_ChildParentGrandparentList.get(it.BI_CM_Tag__c)){
                map_Tags.put(tagId, true);
            }
            map_Insight_Tag.put(it.BI_CM_Insight__c, map_Tags);
        }
    }    

    private String getHierarchyPickListTranslate(String value){
        Schema.DescribeFieldResult fieldResult = BI_CM_Tag__c.BI_CM_Hierarchy_level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        String result = value;
        for( Schema.PicklistEntry f : ple){
            if(value.equalsIgnoreCase(f.getValue())){
                result = f.getLabel();
                break;
            }
        }       
        return result;
    }
    private String getHierarchyCuntryCodeTranslate(String value){
        Schema.DescribeFieldResult fieldResult = BI_CM_Tag__c.BI_CM_Country_code__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        String result = value;
        for( Schema.PicklistEntry f : ple){
            if(value.equalsIgnoreCase(f.getValue())){
                result = f.getLabel();
                break;
            }
        }       
        return result;
    }
    */
}