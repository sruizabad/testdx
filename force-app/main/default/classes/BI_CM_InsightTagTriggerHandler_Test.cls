@isTest
public class BI_CM_InsightTagTriggerHandler_Test {

	@Testsetup
	static void setUp() {
		User repUserBE = BI_CM_TestDataUtility.getSalesRepUser('BE', 0);
		User adminUserBE = BI_CM_TestDataUtility.getDataStewardUser('BE', 0);
	/*	User repUserGB = BI_CM_TestDataUtility.getSalesRepUser('GB', 1);
		User adminUserGB = BI_CM_TestDataUtility.getDataStewardUser('GB', 1);
		User repUserNL = BI_CM_TestDataUtility.getSalesRepUser('NL', 2);
		User adminUserNL = BI_CM_TestDataUtility.getDataStewardUser('NL', 2);
		User adminReportBuilderBUserBE = BI_CM_TestDataUtility.getDataStewardReportBuilderUser('BE', 0);*/

		System.runAs(adminUserBE){
			BI_CM_Tag__c tag = BI_CM_TestDataUtility.newTag('BE'); 
		}

		System.runAs(repUserBE){
			BI_CM_Subscription__c subscription = BI_CM_TestDataUtility.newSubscription('BE');
			List<BI_CM_Insight_Tag__c> insightTags = BI_CM_TestDataUtility.newInsightTags('BE', 'Draft'); 
		}

	}

	//Update insight tags
	@isTest static void test_updateInsightTagForSubmittedInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Submitted';
		System.runAs(salesRep){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - REP UPDATE INSIGHT TAGS FOR SUBMITTED INSIGHT '+insightTags.size());
		
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	update insightTags;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_active_insight), e.getMessage()); 
		}	
	}

	@isTest static void test_updateInsightTagForHiddenInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Hidden';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - REP UPDATE INSIGHT TAGS FOR HIDDEN INSIGHT '+insightTags.size());
		
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	update insightTags;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_active_insight), e.getMessage()); 
		}	
	}

	@isTest static void test_updateInsightTagForArchiveInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Archive';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - REP UPDATE INSIGHT TAGS FOR ARCHIVE INSIGHT '+insightTags.size());
		
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	update insightTags;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_active_insight), e.getMessage()); 
		}	
	}

	@isTest static void test_updateInsightTagForDraftInsightRightSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - REP UPDATE INSIGHT TAGS FOR DRAFT INSIGHT '+insightTags.size());
		

		Test.startTest();
		System.runAs(salesRep){
		   	update insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(201,insightTags.size());
	}

	@isTest static void test_updateInsightTagForSubmittedInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Submitted';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - ADMIN UPDATE INSIGHT TAGS FOR SUBMITTED INSIGHT '+insightTags.size());
		
		Test.startTest();
		System.runAs(admin){
		   	update insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(201,insightTags.size());
	}

	@isTest static void test_updateInsightTagForHiddenInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Hidden';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - ADMIN UPDATE INSIGHT TAGS FOR HIDDEN INSIGHT '+insightTags.size());
		
		Test.startTest();
		System.runAs(admin){
		   	update insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(201,insightTags.size());
	}

	@isTest static void test_updateInsightTagForArchiveInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Archive';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - ADMIN UPDATE INSIGHT TAGS FOR ARCHIVE INSIGHT '+insightTags.size());
		
		Test.startTest();
		System.runAs(admin){
		   	update insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(201,insightTags.size());
	}

	@isTest static void test_updateInsightTagForDraftInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - ADMIN UPDATE INSIGHT TAGS FOR DRAFT INSIGHT '+insightTags.size());
		

		Test.startTest();
		System.runAs(admin){
		   	update insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(201,insightTags.size());
	}

	//Delete insight tags
	@isTest static void test_deleteInsightTagForSubmittedInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Submitted';
		System.runAs(salesRep){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - REP DELETE INSIGHT TAGS FOR SUBMITTED INSIGHT '+insightTags.size());
		
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	delete insightTags;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_active_insight), e.getMessage()); 
		}	
	}

	@isTest static void test_deleteInsightTagForHiddenInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Hidden';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - REP DELETE INSIGHT TAGS FOR HIDDEN INSIGHT '+insightTags.size());
		
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	delete insightTags;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_active_insight), e.getMessage()); 
		}	
	}

	@isTest static void test_deleteInsightTagForArchiveInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Archive';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - REP DELETE INSIGHT TAGS FOR ARCHIVE INSIGHT '+insightTags.size());
		
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	delete insightTags;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_active_insight), e.getMessage()); 
		}	
	}

	@isTest static void test_deleteInsightTagForDraftInsightRightSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - REP DELETE INSIGHT TAGS FOR DRAFT INSIGHT '+insightTags.size());
		

		Test.startTest();
		System.runAs(salesRep){
		   	delete insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(0,insightTags.size());

	    //HAY QUE ELIMINAR EL INSIGHT SUBSCRIPTION
	}

	@isTest static void test_deleteInsightTagForSubmittedInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Submitted';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - ADMIN DELETE INSIGHT TAGS FOR SUBMITTED INSIGHT '+insightTags.size());
		
		Test.startTest();
		System.runAs(admin){
		   	delete insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(0,insightTags.size());
	}

	@isTest static void test_deleteInsightTagForHiddenInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Hidden';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - ADMIN DELETE INSIGHT TAGS FOR HIDDEN INSIGHT '+insightTags.size());
		
		Test.startTest();
		System.runAs(admin){
		   	delete insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(0,insightTags.size());
	}

	@isTest static void test_deleteInsightTagForArchiveInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];
		
		insight.BI_CM_Status__c = 'Archive';
		System.runAs(admin){
			update insight;
		}

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		//System.debug('OMG - ADMIN DELETE INSIGHT TAGS FOR ARCHIVE INSIGHT '+insightTags.size());
		
		Test.startTest();
		System.runAs(admin){
		   	delete insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(0,insightTags.size());
	}

	@isTest static void test_deleteInsightTagForDraftInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																							WHERE BI_CM_Insight__c = :insight.Id]);
		
		List<Id> tagIds = new List<Id>();
		for (BI_CM_Insight_Tag__c insightTag:insightTags){
			tagIds.add(insightTag.Id);
		}
		
		//System.debug('OMG - ADMIN DELETE INSIGHT TAGS FOR DRAFT INSIGHT '+insightTags.size());		

		Test.startTest();
		System.runAs(admin){
		   	delete insightTags;
		}
		Test.stopTest();
		
		insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name FROM BI_CM_Insight_Tag__c
																WHERE BI_CM_Insight__c = :insight.Id]);
	    System.assertEquals(0,insightTags.size());
	}

/*	@isTest static void test_deleteInsightSubscriptionForDraftInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Insight__c insight = [SELECT Name FROM BI_CM_Insight__c 
												WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'BE' LIMIT 1];

		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>([SELECT Name, BI_CM_Tag__c FROM BI_CM_Insight_Tag__c
																										WHERE BI_CM_Insight__c = :insight.Id]);
		
		List<Id> tagIds = new List<Id>();
		for (BI_CM_Insight_Tag__c insightTag:insightTags){
			tagIds.add(insightTag.BI_CM_Tag__c);
		}
		System.debug('OMG - ADMIN DELETE INSIGHT SUBSCRIPTION - tagIds '+tagIds);
	   //	delete insightTags;
	    
	    //Delete insight subscription	
	    List<BI_CM_Subscription__c> subscriptions = new List<BI_CM_Subscription__c>([SELECT Id FROM BI_CM_Subscription__c 
	    																						WHERE BI_CM_Tag__c IN :tagIds]);
	    System.debug('OMG - ADMIN DELETE INSIGHT SUBSCRIPTION - subscriptions '+subscriptions);

	    List<Id> subsIds = new List<Id>();
		for (BI_CM_Subscription__c subscription:subscriptions){
			subsIds.add(subscription.Id);
		}
		System.debug('OMG - ADMIN DELETE INSIGHT SUBSCRIPTION - subsIds '+subsIds);

	    List<BI_CM_Insight_Subscription__c> insSubs = new List<BI_CM_Insight_Subscription__c>([SELECT Name FROM BI_CM_Insight_Subscription__c
																							WHERE BI_CM_Insight__c = :insight.Id
																							AND BI_CM_Subscription__c IN :subsIds]);
	    System.debug('OMG - ADMIN DELETE INSIGHT SUBSCRIPTION - insSub.size '+insSubs.size());
	    System.debug('OMG - ADMIN DELETE INSIGHT SUBSCRIPTION - insSubs '+insSubs);

	    Test.startTest();
		System.runAs(admin){
		   	delete insSubs;
		}
		Test.stopTest();
		
		insSubs = new List<BI_CM_Insight_Subscription__c>([SELECT Name FROM BI_CM_Insight_Subscription__c
																		WHERE BI_CM_Insight__c = :insight.Id
																		AND BI_CM_Subscription__c IN :subsIds]);
	    System.assertEquals(0,insSubs.size());
	}*/
}