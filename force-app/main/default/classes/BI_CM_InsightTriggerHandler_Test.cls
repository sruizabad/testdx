@isTest
public class BI_CM_InsightTriggerHandler_Test {

	@Testsetup
	static void setUp() {
		User repUserBE = BI_CM_TestDataUtility.getSalesRepUser('BE', 0);
		User adminUserBE = BI_CM_TestDataUtility.getDataStewardUser('BE', 0);
		User repUserGB = BI_CM_TestDataUtility.getSalesRepUser('GB', 1);
		User adminUserGB = BI_CM_TestDataUtility.getDataStewardUser('GB', 1);
		User repUserNL = BI_CM_TestDataUtility.getSalesRepUser('NL', 2);
		User adminUserNL = BI_CM_TestDataUtility.getDataStewardUser('NL', 2);
		User adminReportBuilderBUserBE = BI_CM_TestDataUtility.getDataStewardReportBuilderUser('BE', 0);

		System.runAs(repUserGB){
			List<BI_CM_Insight__c> draftInsights = BI_CM_TestDataUtility.newInsights('GB', 'Draft'); 
		}

		System.runAs(adminUserNL){
			List<BI_CM_Insight__c> draftInsights = BI_CM_TestDataUtility.newInsights('NL', 'Draft'); 
			List<BI_CM_Insight__c> submittedInsights = BI_CM_TestDataUtility.newInsights('NL', 'Submitted'); 
			List<BI_CM_Insight__c> hiddenInsights = BI_CM_TestDataUtility.newInsights('NL', 'Hidden'); 
			List<BI_CM_Insight__c> archiveInsights = BI_CM_TestDataUtility.newInsights('NL', 'Archive'); 
		}
	}

	//Insert insigths in Draft status for same and different country code
	@isTest static void test_insertDraftInsightRightCountrySalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		insertBEInsightsSuccess(salesRep, 'Draft');
	}

	@isTest static void test_insertDraftInsightWrongCountrySalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'GB'];
		insertBEInsightsError(salesRep, 'Draft');
	}

	@isTest static void test_insertDraftInsightRightCountryAdmin(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		insertBEInsightsSuccess(salesRep, 'Draft');
	}

	@isTest static void test_insertDraftInsightWrongCountryAdmin(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'GB'];
		insertBEInsightsError(salesRep, 'Draft');
	}

	//Insert insights in Submitted status for same country code
	@isTest static void test_insertSubmittedInsightRightCountrySalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		insertBEInsightsError(salesRep, 'Submitted');
	}

	@isTest static void test_insertSubmittedInsightRightCountryAdmin(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		insertBEInsightsSuccess(salesRep, 'Submitted');
	}

	//Insert insights in Hidden status for same country code
	@isTest static void test_insertHiddenInsightRightCountrySalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		insertBEInsightsError(salesRep, 'Hidden');
	}

	@isTest static void test_insertHiddenInsightRightCountryAdmin(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		insertBEInsightsSuccess(salesRep, 'Hidden');
	}

	//Insert insights in Archive status for same country code
	@isTest static void test_insertArchiveInsightRightCountrySalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		insertBEInsightsError(salesRep, 'Archive');
	}

	@isTest static void test_insertArchiveInsightRightCountryAdmin(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		insertBEInsightsSuccess(salesRep, 'Archive');
	}

	//Update insights in Draft status
	@isTest static void test_updateDraftInsightRightCountrySalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		Test.startTest();
		System.runAs(salesRep){
	    	update insights;
		}
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
	    System.assertEquals(201,insights.size());
	}

	@isTest static void test_updateDraftInsightWrongCountrySalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Country_code__c = 'BE';
		}

		try {
			Test.startTest();
			System.runAs(salesRep){
		    	update insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_User_not_assigned_to_group), e.getMessage()); 
		}	
	}

	@isTest static void test_updateDraftInsightRightCountryAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		Test.startTest();
		System.runAs(admin){
	    	update insights;
		}
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
	    System.assertEquals(201,insights.size());
	}

	@isTest static void test_updateDraftInsightWrongCountryAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Country_code__c = 'BE';
		}

		try {
			Test.startTest();
			System.runAs(admin){
		    	update insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_User_not_assigned_to_group), e.getMessage()); 
		}	
	}

	//Update insights to Submitted status
	@isTest static void test_updateSubmittedInsightRightSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		//System.debug('OMG - REP UPDATE DRAFT TO SUBMITTED '+insights.size());
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Submitted';
		}

		Test.startTest();
		System.runAs(salesRep){
	    	update insights;
		}
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'GB']);
	    System.assertEquals(201,insights.size());
	}

	@isTest static void test_updateSubmittedInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		//System.debug('OMG - ADMIN UPDATE DRAFT TO SUBMITTED '+insights.size());
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Submitted';
		}

		Test.startTest();
		System.runAs(admin){
	    	update insights;
		}
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'GB']);
	    System.assertEquals(201,insights.size());
	}

	//Update insights to Hidden status
	@isTest static void test_updateHiddenInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		//System.debug('OMG - REP UPDATE DRAFT TO HIDDEN '+insights.size());
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Hidden';
		}

		try {
			Test.startTest();
			System.runAs(salesRep){
		    	update insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}	
	}

	@isTest static void test_updateHiddenInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		//System.debug('OMG - ADMIN UPDATE DRAFT TO HIDDEN '+insights.size());
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Hidden';
		}
		
		Test.startTest();
		System.runAs(admin){
	    	update insights;
		}
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'GB']);
	    System.assertEquals(201,insights.size());
	}
	
	//Update insights to Archive status
	@isTest static void test_updateArchiveInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		//System.debug('OMG - REP UPDATE DRAFT TO ARCHIVE '+insights.size());
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Archive';
		}

		try {
			Test.startTest();
			System.runAs(salesRep){
		    	update insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}	
	}

	@isTest static void test_updateArchiveInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		//System.debug('OMG - ADMIN UPDATE DRAFT TO ARCHIVE '+insights.size());
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Archive';
		}

		Test.startTest();
		System.runAs(admin){
	    	update insights;
		}
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'GB']);
	    System.assertEquals(201,insights.size());
	}

	//Update Submitted insights
	@isTest static void test_updateEditSubmittedInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - SALES REP UPDATE SUBMITTED '+insights.size());
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	update insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}	
	}

	@isTest static void test_updateEditSubmittedInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN UPDATE SUBMITTED '+insights.size());
		Test.startTest();
		System.runAs(admin){
	    	update insights;
		}
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(201,insights.size());
	}

	@isTest static void test_updateEditSubmittedInsightWithCommentsRightSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - SALES REP UPDATE SUBMITTED '+insights.size());
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		
		Test.startTest();
		System.runAs(salesRep){
		   	insert comments;
		}
		Test.stopTest();

		insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(201,insights.size());
		
	}

	@isTest static void test_updateEditSubmittedInsightWithCommentsRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - SALES REP UPDATE SUBMITTED '+insights.size());
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		
		Test.startTest();
		System.runAs(admin){
		   	insert comments;
		}
		Test.stopTest();

		insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(201,insights.size());
		
	}

	@isTest static void test_updateEditSubmittedInsightWithCommentsWrongReportBuilder(){
		User admRepBuilder = [SELECT Id FROM User WHERE alias = 'admRep' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN REP BUILDER UPDATE SUBMITTED '+insights.size());
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		try {
			Test.startTest();
			System.runAs(admRepBuilder){
		    	insert comments;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Flow_dont_have_permissions), e.getMessage()); 
		}	
		
	}

	//Update Hidden insights
	@isTest static void test_updateEditHiddenInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - SALES REP UPDATE HIDDEN '+insights.size());
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	update insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}	
	}

	@isTest static void test_updateEditHiddenInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN UPDATE HIDDEN '+insights.size());
		Test.startTest();
		System.runAs(admin){
	    	update insights;
		}
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(201,insights.size());
	}

	@isTest static void test_updateEditHiddenInsightWithCommentsRightSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - SALES REP UPDATE HIDDEN '+insights.size());
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	insert comments;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}
		
	}

	@isTest static void test_updateEditHiddenInsightWithCommentsRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - SALES REP UPDATE HIDDEN '+insights.size());
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		
		Test.startTest();
		System.runAs(admin){
		   	insert comments;
		}
		Test.stopTest();

		insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(201,insights.size());
		
	}

	@isTest static void test_updateEditHiddenInsightWithCommentsWrongReportBuilder(){
		User admRepBuilder = [SELECT Id FROM User WHERE alias = 'admRep' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN REP BUILDER UPDATE HIDDEN '+insights.size());
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		try {
			Test.startTest();
			System.runAs(admRepBuilder){
		    	insert comments;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Flow_dont_have_permissions), e.getMessage()); 
		}	
		
	}

	//Update Hidden insights
	@isTest static void test_updateEditArchiveInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - SALES REP UPDATE ARCHIVE '+insights.size());
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	update insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}	
	}

	@isTest static void test_updateEditArchiveInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN UPDATE ARCHIVE '+insights.size());
		Test.startTest();
		System.runAs(admin){
	    	update insights;
		}
	    Test.stopTest();

	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(201,insights.size());
	}

	@isTest static void test_updateEditArchiveInsightWithCommentsRightSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - SALES REP UPDATE ARCHIVE '+insights.size());
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	insert comments;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}
		
	}

	@isTest static void test_updateEditArchiveInsightWithCommentsRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - SALES REP UPDATE HIDDEN '+insights.size());
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		
		Test.startTest();
		System.runAs(admin){
		   	insert comments;
		}
		Test.stopTest();

		insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(201,insights.size());
		
	}

	@isTest static void test_updateEditArchiveInsightWithCommentsWrongReportBuilder(){
		User admRepBuilder = [SELECT Id FROM User WHERE alias = 'admRep' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN REP BUILDER UPDATE ARCHIVE '+insights.size());
		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		try {
			Test.startTest();
			System.runAs(admRepBuilder){
		    	insert comments;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Flow_dont_have_permissions), e.getMessage()); 
		}	
		
	}
	
	//Delete insights
	@isTest static void test_deleteDraftInsightRightSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);

		//System.debug('OMG - SALES REP DELETE DRAFT '+insights.size());

		Test.startTest();
		System.runAs(salesRep){
		   	delete insights;
		}
		Test.stopTest();
	
		insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
	    System.assertEquals(0,insights.size());
	}

	@isTest static void test_deleteSubmittedInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Submitted';
		}
		update insights;
		//System.debug('OMG - SALES REP DELETE SUBMITTED '+insights.size());
	
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	delete insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}	
		
	}

	@isTest static void test_deleteHiddenInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Hidden';
		}
		update insights;
		//System.debug('OMG - SALES REP DELETE HIDDEN '+insights.size());
	
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	delete insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}	
		
	}

	@isTest static void test_deleteArchiveInsightWrongSalesRep(){
		User salesRep = [SELECT Id FROM User WHERE alias = 'testRep' and Country_Code_BI__c = 'GB'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'GB']);
		
		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Archive';
		}
		update insights;
		//System.debug('OMG - SALES REP DELETE ARCHIVE '+insights.size());
	
		try {
			Test.startTest();
			System.runAs(salesRep){
		    	delete insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}	
		
	}

	@isTest static void test_deleteDraftInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN DELETE DRAFT '+insights.size());
	
		Test.startTest();
		System.runAs(admin){
		   	delete insights;
		}
		Test.stopTest();

		insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Draft' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(0,insights.size());
		
	}

	@isTest static void test_deleteSubmittedInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN DELETE SUBMITTED '+insights.size());
	
		Test.startTest();
		System.runAs(admin){
		   	delete insights;
		}
		Test.stopTest();

		insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Submitted' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(0,insights.size());
		
	}

	@isTest static void test_deleteHiddenInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN DELETE HIDDEN '+insights.size());
	
		Test.startTest();
		System.runAs(admin){
		   	delete insights;
		}
		Test.stopTest();

		insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Hidden' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(0,insights.size());
		
	}

	@isTest static void test_deleteArchiveInsightRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
																		WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'NL']);
		
		//System.debug('OMG - ADMIN DELETE ARCHIVE '+insights.size());
	
		Test.startTest();
		System.runAs(admin){
		   	delete insights;
		}
		Test.stopTest();

		insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c
													WHERE BI_CM_Status__c = 'Archive' AND BI_CM_Country_code__c = 'NL']);
	    System.assertEquals(0,insights.size());
		
	}

	/**
     * Creates 201 new insights.
     *
     */
    public static void insertBEInsightsError(User testuser, String status) {
		//System.debug('OMG >> BI_CM_TestDataUtility >> Inserting 201 tags...');
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>();

        for(Integer i=0; i < 201; i++) {
            insights.add(new BI_CM_Insight__c(
            							BI_CM_Insight_description__c = 'Test insight ' + i, 
            							BI_CM_Status__c = status,
            							BI_CM_Country_code__c = 'BE'));
        }      

        try {
			Test.startTest();
			System.runAs(testuser){
		    	insert insights;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    if (status == 'Draft')
				System.assert(e.getMessage().contains(Label.BI_CM_User_not_assigned_to_group), e.getMessage()); 
			else
				System.assert(e.getMessage().contains(Label.BI_CM_Action_not_allowed_for_sales_rep), e.getMessage()); 
		}	

    } 

    /**
     * Creates 201 new insights.
     *
     */
    public static void insertBEInsightsSuccess(User testuser, String status) {
		//System.debug('OMG >> BI_CM_TestDataUtility >> Inserting 201 tags...');
		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>();

        for(Integer i=0; i < 201; i++) {
            insights.add(new BI_CM_Insight__c(
            							BI_CM_Insight_description__c = 'Test insight ' + i, 
            							BI_CM_Status__c = status,
            							BI_CM_Country_code__c = 'BE'));
        }      

		Test.startTest();
		System.runAs(testuser){
	    	insert insights;
		}
	    Test.stopTest();

	    //Check if the tree has been updated
	    insights = new List<BI_CM_Insight__c>([SELECT Name FROM BI_CM_Insight__c 
	    													WHERE BI_CM_Status__c = :status AND BI_CM_Country_code__c = 'BE']);
		System.assertEquals(201,insights.size());
			
    } 
}