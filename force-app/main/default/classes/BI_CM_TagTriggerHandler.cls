/**
 * Handler for BI_CM_Tag__c Trigger.
 */
public without sharing class BI_CM_TagTriggerHandler implements BI_CM_TriggerInterface {

    public map<String, map<Id, Boolean>> groupMembership = new map<String, map<Id, Boolean>>();
    public list<BI_CM_Tag__c> tagsToUpdateActiveStatusList;
  
    
    /**
     * Constructs the object.
     */
    public BI_CM_TagTriggerHandler() {}
    
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {
        
        if (Trigger.isInsert || Trigger.isUpdate) {
            
            // Tag owner must have access to the Tag Country Code 
            map<Id, Group> cimGroups = new map<Id, Group>([SELECT Id FROM Group WHERE DeveloperName LIKE 'BI_CM_ADMIN_%']);
            list<GroupMember> cimGroupMembers = new list<GroupMember>([SELECT Id, UserOrGroupId, Group.Name, Group.DeveloperName FROM GroupMember WHERE GroupId IN :cimGroups.KeySet()]);
            for(GroupMember gm : cimGroupMembers){
                if(groupMembership.get(gm.Group.DeveloperName) != null){
                    groupMembership.get(gm.Group.DeveloperName).put(gm.userOrGroupId, true);
                }else{
                    map<Id, Boolean> newEntry = new map<Id, Boolean>();
                    newEntry.put(gm.userOrGroupId, true);
                    groupMembership.put(gm.Group.DeveloperName, newEntry);
                }
            }
   
            System.debug('### - BI_CM_TagTriggerHandler - bulkBefore - groupMembership = ' + groupMembership);
        }
    }
    
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter() {
        if (Trigger.isInsert || Trigger.isUpdate) {

            // When Tag is deactivated or activated, the tags below it in the Tag Hierarchy must be activated or deactivated as well
            // When a Tag is activated, the tags above it in the Tag Hierarchy must be activated as well
            tagsToUpdateActiveStatusList = getTagsToChangeActiveStatus();
        }
    }
    
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     * 
     * @param      so    The SObject (BI_CM_Tag__c)
     * 
     */
    public void beforeInsert(SObject so) {
          
         // Tag external id calculation        
         calculateExternalId(so);
          
        // Tag owner must have access to the Tag Country Code 
        if(checkCountryCode(so) == false){
            so.addError(Label.BI_CM_User_not_assigned_to_group);
        }
    }

    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     * 
     * @param      oldSo  The old SObject (BI_CM_Tag__c)
     * @param      so     The SObject (BI_CM_Tag__c)
     * 
     */
    public void beforeUpdate(SObject oldSo, SObject so) {
        
        // Tag external id calculation       
        calculateExternalId(so);
        
        // Tag owner must have access to the Tag Country Code 
        if(checkCountryCode(so) == false){
            so.addError(Label.BI_CM_User_not_assigned_to_group);
        }
    }
    
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     * 
     * @param      so    The SObject (BI_CM_Tag__c)
     * 
     */
    public void beforeDelete(SObject so) {}
    
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     * 
     * @param      so    The SObject (BI_CM_Tag__c)
     * 
     */
    public void afterInsert(SObject so) {}
    
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     * 
     * @param      oldSo  The old SObject (BI_CM_Tag__c)
     * @param      so     The SObject (BI_CM_Tag__c)
     * 
     */
    public void afterUpdate(SObject oldSo, SObject so) {}
    
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     * 
     * @param      so    The SObject (BI_CM_Tag__c)
     * 
     */
    public void afterDelete(SObject so) {}
    
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        
            // When Tag is deactivated or activated, the tags below it in the Tag Hierarchy must be activated or deactivated as well
            Database.update(tagsToUpdateActiveStatusList, false);
        }
    }
    
    /**
       * Check if the tags Country code is correct
       *
       * @param      sObject      The tag
       *
       * @return     Boolean True if the tag Country code is correct
    */
    public boolean checkCountryCode(sObject so){
        BI_CM_Tag__c currentTag = (BI_CM_Tag__c)so;
        if(groupMembership.get('BI_CM_ADMIN_'+currentTag.BI_CM_Country_code__c) == null || groupMembership.get('BI_CM_ADMIN_'+currentTag.BI_CM_Country_code__c).get(currentTag.ownerId) == null){
            return false;
        }
        return true;
    }

    /**
     * Gets the tags to change active status.
     *
     * @return     The tags to change active status.
     */
    public list<BI_CM_Tag__c> getTagsToChangeActiveStatus(){

        // When Tag is deactivated, the tags below it in the Tag Hierarchy must be deactivated
        list<Id> deactivatedTagsList = new list<Id>();

        // When Tag is activated, the tags below it in the Tag Hierarchy must be activated
        list<Id> activatedTagsList = new list<Id>();

        // When a Tag is activated, the tags above it in the Tag Hierarchy must be activated as well
        list<BI_CM_Tag__c> tagsWithParentsToBeActivatedList;
        list<BI_CM_Tag__c> parentsToBeActivatedList = new list<BI_CM_Tag__c>();
        
        for(sObject currentRecord : Trigger.New){
            BI_CM_Tag__c currentTag = (BI_CM_Tag__c)currentRecord;
            
            if(currentTag.BI_CM_Active__c == FALSE && (Trigger.isInsert || ((BI_CM_Tag__c)Trigger.OldMap.get(currentTag.Id)).BI_CM_Active__c == TRUE)){
                deactivatedTagsList.add(currentTag.Id);

            }else if(currentTag.BI_CM_Active__c == TRUE && (Trigger.isInsert || ((BI_CM_Tag__c)Trigger.OldMap.get(currentTag.Id)).BI_CM_Active__c == FALSE)){
                activatedTagsList.add(currentTag.Id);
            }
        }


        
        System.debug(LoggingLevel.ERROR, '### - BI_CM_TagTriggerHandler - getTagsToChangeActiveStatus - deactivatedTagsList = ' + deactivatedTagsList);
        System.debug(LoggingLevel.ERROR, '### - BI_CM_TagTriggerHandler - getTagsToChangeActiveStatus - activatedTagsList = ' + activatedTagsList);
        
        tagsWithParentsToBeActivatedList = new list<BI_CM_Tag__c>([  SELECT BI_CM_Parent__r.BI_CM_Active__c, BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Active__c
                                                                    FROM BI_CM_Tag__c
                                                                    WHERE Id IN :activatedTagsList
                                                                        AND ((BI_CM_Parent__c != NULL AND BI_CM_Parent__r.BI_CM_Active__c = FALSE) OR
                                                                            (BI_CM_Parent__r.BI_CM_Parent__c != NULL AND BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Active__c = FALSE))]);
        
        for(BI_CM_Tag__c currentTag : tagsWithParentsToBeActivatedList){
            if(currentTag.BI_CM_Parent__r.BI_CM_Active__c == FALSE){
                BI_CM_Tag__c newTag = new BI_CM_Tag__c();
                newTag.Id = currentTag.BI_CM_Parent__c;
                newTag.BI_CM_Active__c = currentTag.BI_CM_Parent__r.BI_CM_Active__c;
                parentsToBeActivatedList.add(newTag);
            }
            if(currentTag.BI_CM_Parent__r.BI_CM_Parent__c != NULL && currentTag.BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Active__c == FALSE){
                BI_CM_Tag__c newTag = new BI_CM_Tag__c();
                newTag.Id = currentTag.BI_CM_Parent__r.BI_CM_Parent__c;
                newTag.BI_CM_Active__c = currentTag.BI_CM_Parent__r.BI_CM_Parent__r.BI_CM_Active__c;
                parentsToBeActivatedList.add(newTag);
            }
        }

        System.debug(LoggingLevel.ERROR, '### - BI_CM_TagTriggerHandler - getTagsToChangeActiveStatus - parentsToBeActivatedList = ' + parentsToBeActivatedList);

        tagsToUpdateActiveStatusList = new list<BI_CM_Tag__c>([SELECT Id, BI_CM_Active__c
                                                               FROM BI_CM_Tag__c 
                                                               WHERE (BI_CM_Active__c = TRUE AND 
                                                                    ((BI_CM_Parent__c  != NULL AND BI_CM_Parent__c IN :deactivatedTagsList) 
                                                                    OR (BI_CM_Parent__c != NULL AND BI_CM_Parent__r.BI_CM_Parent__c != NULL AND BI_CM_Parent__r.BI_CM_Parent__c IN :deactivatedTagsList)))
                                                                    /*OR
                                                                    (BI_CM_Active__c = FALSE AND 
                                                                    ((BI_CM_Parent__c  != NULL AND BI_CM_Parent__c IN :activatedTagsList) 
                                                                    OR (BI_CM_Parent__c != NULL AND BI_CM_Parent__r.BI_CM_Parent__c != NULL AND BI_CM_Parent__r.BI_CM_Parent__c IN :activatedTagsList)))*/]); 


        System.debug(LoggingLevel.ERROR, '### - BI_CM_TagTriggerHandler - getTagsToChangeActiveStatus - tagsToUpdateActiveStatusList = ' + tagsToUpdateActiveStatusList);
        tagsToUpdateActiveStatusList.addAll(parentsToBeActivatedList);

        for(BI_CM_Tag__c currentTag : tagsToUpdateActiveStatusList){
            if(currentTag.BI_CM_Active__c == TRUE)
                currentTag.BI_CM_Active__c = FALSE;
            else
                currentTag.BI_CM_Active__c = TRUE;
        }

        System.debug(LoggingLevel.ERROR, '### - BI_CM_TagTriggerHandler - getTagsToChangeActiveStatus - tagsToUpdateActiveStatusList = ' + tagsToUpdateActiveStatusList);

        return tagsToUpdateActiveStatusList;
    }

    /**
     * Calculates the external identifier.
     *
     * @param      so    { tag to calculate External Id }
     */
    public void calculateExternalId(sObject so){
        BI_CM_Tag__c currentRecord = (BI_CM_Tag__c)so;        
        currentRecord.BI_CM_External_id__c = currentRecord.Name + currentRecord.BI_CM_Country_code__c;
    }
}