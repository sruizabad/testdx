@isTest
public class BI_CM_TagTriggerHandler_Test {

	@Testsetup
	static void setUp() {

		User adminUserBE = BI_CM_TestDataUtility.getDataStewardUser('BE', 0);
		User adminUserNL = BI_CM_TestDataUtility.getDataStewardUser('NL', 1);

		System.runAs(adminUserNL){
			List<BI_CM_Tag__c> activeTags = BI_CM_TestDataUtility.newTags('NL', TRUE, 0);
			List<BI_CM_Tag__c> threeLevelTags = BI_CM_TestDataUtility.threeLevelTags('NL', FALSE); 
		}
		System.runAs(adminUserBE){
			List<BI_CM_Tag__c> inactiveTags1 = BI_CM_TestDataUtility.newTags('BE', FALSE, 1); 
			List<BI_CM_Tag__c> inactiveTags2 = BI_CM_TestDataUtility.newTags('BE', FALSE, 2); 
		}
	}

	//Insert tags
	@isTest static void test_insertTagSameCountryRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>();

        for(Integer i=0; i < 201; i++) {
            tags.add(new BI_CM_Tag__c(
            							Name = 'Test tag ' + i, 
            							BI_CM_Active__c = TRUE,
            							BI_CM_Country_code__c = 'BE',
            							BI_CM_Hierarchy_level__c = '1'));
        }      

		Test.startTest();
		System.runAs(admin){
	    	insert tags;
		}
	    Test.stopTest();

	    tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Active__c = TRUE AND BI_CM_Country_code__c = 'BE']);
		System.assertEquals(201,tags.size());
			
	}

	@isTest static void test_insertTagDifferentCountryWrongAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>();

        for(Integer i=0; i < 201; i++) {
            tags.add(new BI_CM_Tag__c(
            							Name = 'Test tag ' + i, 
            							BI_CM_Active__c = TRUE,
            							BI_CM_Country_code__c ='GB',
            							BI_CM_Hierarchy_level__c = '1'));
        }      

		try {
			Test.startTest();
			System.runAs(admin){
		    	insert tags;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_User_not_assigned_to_group), e.getMessage()); 
		}	
			
	}

	//Update tags
	@isTest static void test_updateTagSameCountryRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Active__c = TRUE AND BI_CM_Country_code__c = 'NL']);

        for (BI_CM_Tag__c tag:tags){
			tag.Name = tag.Name+' test';
		}   

		Test.startTest();
		System.runAs(admin){
	    	update tags;
		}
	    Test.stopTest();

	    tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Active__c = TRUE AND BI_CM_Country_code__c = 'NL']);
		System.assertEquals(201,tags.size());
			
	}

	@isTest static void test_updateTagDifferentCountryWrongAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Active__c = TRUE AND BI_CM_Country_code__c = 'NL']);

		for (BI_CM_Tag__c tag:tags){
			tag.BI_CM_Country_code__c = 'GB';
		}   

		try {
			Test.startTest();
			System.runAs(admin){
		    	update tags;
			}
		    Test.stopTest();
		} catch (DmlException e) {
		    //Assert Error Message
		    System.assert(e.getMessage().contains(Label.BI_CM_User_not_assigned_to_group), e.getMessage()); 
		}	
			
	}

	@isTest static void test_updateInactivateFromLevel1RightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Active__c = TRUE AND BI_CM_Country_code__c = 'NL' 
	    											AND BI_CM_Hierarchy_level__c = '1' AND Name LIKE 'Test 0%']);

        for (BI_CM_Tag__c tag:tags){
			tag.BI_CM_Active__c = FALSE;
		}   

		Test.startTest();
		System.runAs(admin){
	    	update tags;
		}
	    Test.stopTest();

	    tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Active__c = FALSE AND BI_CM_Country_code__c = 'NL'
	    											AND Name LIKE 'Test 0%']);
		System.assertEquals(201,tags.size());
			
	}

	@isTest static void test_updateActivateFromLevel1RightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Active__c = FALSE AND BI_CM_Country_code__c = 'BE' 
	    											AND BI_CM_Hierarchy_level__c = '1' AND Name LIKE 'Test 1%']);

        for (BI_CM_Tag__c tag:tags){
			tag.BI_CM_Active__c = TRUE;
		}   

		Test.startTest();
		System.runAs(admin){
	    	update tags;
		}
	    Test.stopTest();

	    tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Active__c = FALSE AND BI_CM_Country_code__c = 'BE' 
	    											AND Name LIKE 'Test 1%']);
		System.assertEquals(100,tags.size());
			
	}

	@isTest static void test_updateActivateFromLevel2RightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'BE'];
		BI_CM_Tag__c tag = [SELECT Name FROM BI_CM_Tag__c 
	    								WHERE BI_CM_Country_code__c = 'BE' AND BI_CM_Hierarchy_level__c = '2' 
	    								AND Name LIKE 'Test 2%' LIMIT 1];

		tag.BI_CM_Active__c = TRUE; 

		Test.startTest();
		System.runAs(admin){
	    	update tag;
		}
	    Test.stopTest();

	    List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    																WHERE BI_CM_Active__c = FALSE AND BI_CM_Country_code__c = 'BE' 
	    																AND Name LIKE 'Test 2%']);
		System.assertEquals(199,tags.size());
			
	}

	@isTest static void test_updateActivateFromLevel3RightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		BI_CM_Tag__c tag = [SELECT Name FROM BI_CM_Tag__c 
	    								WHERE BI_CM_Country_code__c = 'NL' AND BI_CM_Hierarchy_level__c = '3' 
	    								AND Name LIKE 'Level%' LIMIT 1];

		tag.BI_CM_Active__c = TRUE; 

		Test.startTest();
		System.runAs(admin){
	    	update tag;
		}
	    Test.stopTest();

	    List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    																WHERE BI_CM_Active__c = FALSE AND BI_CM_Country_code__c = 'NL' 
	    																AND Name LIKE 'Level%']);
		System.assertEquals(3,tags.size());
			
	}

	//Delete tags
	@isTest static void test_deleteTagsRightAdmin(){
		User admin = [SELECT Id FROM User WHERE alias = 'testAdm' and Country_Code_BI__c = 'NL'];
		List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Country_code__c = 'NL' AND Name LIKE 'Test%']);


		Test.startTest();
		System.runAs(admin){
	    	delete tags;
		}
	    Test.stopTest();

	    tags = new List<BI_CM_Tag__c>([SELECT Name FROM BI_CM_Tag__c 
	    											WHERE BI_CM_Country_code__c = 'NL' AND Name LIKE 'Test%']);
		System.assertEquals(0,tags.size());
			
	}

}