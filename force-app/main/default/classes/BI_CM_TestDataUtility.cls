//@IsTest
public class BI_CM_TestDataUtility {

	/**
     * Create a Sales Rep user.
     *
     * @param      countryCode  The country code
     * @param      numberCode   The number code
     *
     * @return     The Sales Rep user.
     */
    public static User getSalesRepUser(String countryCode, Decimal numberCode) {        
        Profile p = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

        //New user
        String testemail = 'testRep'+numberCode+'@example.com';
        User us = new User(username = testemail, ProfileId = p.Id, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='testRep', lastname='testRep', Country_Code_BI__c = countryCode,
                           EmployeeNumber = 'testRep'+numberCode, Street='Test Street');
       
        Database.insert(us);

        //Assign permission set
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'BI_CM_SALES'];
        insert new PermissionSetAssignment(AssigneeId = us.id, PermissionSetId = ps.Id);

        //Assign public group
        String developerName = 'BI_CM_SALES_'+countryCode;
        Group g = [SELECT Id, Name FROM Group WHERE DeveloperName = :developerName LIMIT 1];
        //System.debug('### - BI_CM_TestDataUtility - getSalesRepUser - public group = '+g.Name);
        GroupMember gm = new GroupMember(UserOrGroupId=us.id, GroupId=g.Id);
        insert gm;
        
        return us;
    }

    /**
     * Create a Data Steward user.
     *
     * @param      countryCode  The country code
     * @param      numberCode   The number code
     *
     * @return     The Data Steward user.
     */
    public static User getDataStewardUser(String countryCode, Decimal numberCode) {        
        Profile p = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

        //New user
        String testemail = 'testAdm'+numberCode+'@example.com';
        User us = new User(username = testemail, ProfileId = p.Id, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='testAdm', lastname='testAdm', Country_Code_BI__c = countryCode,
                           EmployeeNumber = 'testAdm'+numberCode, Street='Test Street');
       
        Database.insert(us);

        //Assign permission set
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'BI_CM_ADMIN'];
        insert new PermissionSetAssignment(AssigneeId = us.id, PermissionSetId = ps.Id);

        //Assign public group
        String developerName = 'BI_CM_ADMIN_'+countryCode;
        Group g = [SELECT Id, Name FROM Group WHERE DeveloperName = :developerName LIMIT 1];
       // System.debug('### - BI_CM_TestDataUtility - getDataStewardUser - public group = '+g.Name);
        GroupMember gm = new GroupMember(UserOrGroupId=us.id, GroupId=g.Id);
        insert gm;
        
        return us;
    }

    /**
     * Create a Data Steward and Report builder user.
     *
     * @param      countryCode  The country code
     * @param      numberCode   The number code
     *
     * @return     The user.
     */
    public static User getDataStewardReportBuilderUser(String countryCode, Decimal numberCode) {        
        Profile p = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

        //New user
        String testemail = 'admRep'+numberCode+'@example.com';
        User us = new User(username = testemail, ProfileId = p.Id, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='admRep', lastname='admRep', Country_Code_BI__c = countryCode,
                           EmployeeNumber = 'admRep'+numberCode, Street='Test Street');
       
        Database.insert(us);

        //Assign permission set
        List<PermissionSet> ps = [SELECT ID From PermissionSet WHERE Name IN ('BI_CM_ADMIN','BI_CM_REPORT_BUILDER')];
        for(PermissionSet s:ps){
			insert new PermissionSetAssignment(AssigneeId = us.id, PermissionSetId = s.Id);
        }
        //System.debug('OMG report builder permissions '+ps);
        //Assign public group
        String developerName = 'BI_CM_ADMIN_'+countryCode;
        Group g = [SELECT Id, Name FROM Group WHERE DeveloperName = :developerName LIMIT 1];
        //System.debug('### - BI_CM_TestDataUtility - getDataStewardReportBuilderUser - public group = '+g.Name);
        GroupMember gm = new GroupMember(UserOrGroupId=us.id, GroupId=g.Id);
        insert gm;
        
        return us;
    }

    /**
     * Create a Report Builder user.
     *
     * @param      countryCode  The country code
     * @param      numberCode   The number code
     *
     * @return     The Report Builder user.
     */
    public static User getReportBuilderUser(String countryCode, Decimal numberCode) {        
        Profile p = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

        //New user
        String testemail = 'repBuil'+numberCode+'@example.com';
        User us = new User(username = testemail, ProfileId = p.Id, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='repBuil', lastname='repBuil', Country_Code_BI__c = countryCode,
                           EmployeeNumber = 'repBuil'+numberCode, Street='Test Street');
       
        Database.insert(us);

        //Assign permission set
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'BI_CM_REPORT_BUILDER'];
        insert new PermissionSetAssignment(AssigneeId = us.id, PermissionSetId = ps.Id);

        //Assign public group
        String developerName = 'BI_CM_ADMIN_'+countryCode;
        Group g = [SELECT Id, Name FROM Group WHERE DeveloperName = :developerName LIMIT 1];
      //  System.debug('### - BI_CM_TestDataUtility - getReportBuilderUser - public group = '+g.Name);
        GroupMember gm = new GroupMember(UserOrGroupId=us.id, GroupId=g.Id);
        insert gm;
        
        return us;
    }
    
    /**
     * Creates 201 new insights.
     *
     * @return List of insights
     */
    public static List<BI_CM_Insight__c> newInsights(String countryCode, String status) {

		List<BI_CM_Insight__c> insights = new List<BI_CM_Insight__c>();

        for(Integer i=0; i < 201; i++) {
            insights.add(new BI_CM_Insight__c(
            							BI_CM_Insight_description__c = 'Test insight ' + i, 
            							BI_CM_Status__c = status,
            							BI_CM_Country_code__c = countryCode));
        }      

	   	insert insights;
	   	return insights;			
    } 

    /**
     * Update insights to submitted and create related comments
     *
     * @return  List of insights
     */
    public static List<BI_CM_Insight__c> updateInsightToSubmittedWithComments(List<BI_CM_Insight__c> insights){

		List<BI_CM_Insight_Comment__c> comments = new List<BI_CM_Insight_Comment__c>();

		for (BI_CM_Insight__c insight:insights){
			insight.BI_CM_Status__c = 'Submitted';

			comments.add(new BI_CM_Insight_Comment__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Confirm__c = TRUE));
		}

		update insights;
		insert comments;

		return insights;
    }

    /*
    * Creates 201 new insights with comments
    *
    * @return List of insights
     */
    public static List<BI_CM_Insight__c> newInsightsWithComments(String countryCode, String status){

    	List<BI_CM_Insight__c> insights = newInsights(countryCode, status);
    	insights = updateInsightToSubmittedWithComments(insights);

    	return insights;
    }

    /**
     * Creates a new insight with 201 insight tags
     *
     * @return List of insight tags
     */
    public static List<BI_CM_Insight_Tag__c> newInsightTags(String countryCode, String status) {
		
		//New insight
		BI_CM_Insight__c insight = new BI_CM_Insight__c(BI_CM_Insight_description__c = 'Test insight', 
            											BI_CM_Status__c = status,
            											BI_CM_Country_code__c = countryCode);

		insert insight;
		//System.debug('OMG >> BI_CM_TestDataUtility >> create new insight '+insight);

		//Retrieve tag
		BI_CM_Tag__c tag = [SELECT Id FROM BI_CM_Tag__c WHERE BI_CM_Country_code__c = :countryCode LIMIT 1];
		//System.debug('OMG >> BI_CM_TestDataUtility >> retrieve tag '+tag);
		
		//Retrieve subscription
		BI_CM_Subscription__c subs = [SELECT Id FROM BI_CM_Subscription__c WHERE BI_CM_Tag__c = :tag.Id LIMIT 1];
		System.debug('OMG >> BI_CM_TestDataUtility >> retrieve subscription '+subs);

		//New insight tags and insight subscriptions
		List<BI_CM_Insight_Tag__c> insightTags = new List<BI_CM_Insight_Tag__c>();
		List<BI_CM_Insight_Subscription__c> insightSubs = new List<BI_CM_Insight_Subscription__c>();

        for(Integer i=0; i < 201; i++) {
            insightTags.add(new BI_CM_Insight_Tag__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Tag__c = tag.Id));

            insightSubs.add(new BI_CM_Insight_Subscription__c(
            							BI_CM_Insight__c = insight.Id, 
            							BI_CM_Subscription__c = subs.Id));
        }      

	   	insert insightTags;
	   	insert insightSubs;
	   	//System.debug('OMG >> BI_CM_TestDataUtility >> new insight tags '+insightTags);
	   	System.debug('OMG >> BI_CM_TestDataUtility >> new insight subscriptions '+insightSubs);
	   		   	
	   	return insightTags;			
    } 

	/**
     * Creates a new tag
     *
     * @return The tag
     */
    public static BI_CM_Tag__c newTag(String countryCode){
    	BI_CM_Tag__c tag = new BI_CM_Tag__c(Name = 'Test tag',
											BI_CM_Active__c = TRUE,
											BI_CM_Country_code__c = countryCode,
											BI_CM_Hierarchy_level__c = '1');
		insert tag;
		return tag;
    }

    /**
     * Creates a new subscription (alert setting)
     *
     * @return The tag subscription
     */
    public static BI_CM_Subscription__c newSubscription(String countryCode){

    	BI_CM_Tag__c tag = [SELECT Id FROM BI_CM_Tag__c WHERE BI_CM_Country_code__c = :countryCode LIMIT 1];

    	BI_CM_Subscription__c subscription = new BI_CM_Subscription__c(BI_CM_Tag__c = tag.Id);
		insert subscription;
		return subscription;
    }

     /**
     * Creates 201 new tags.
     *
     * @return List of tags
     */
    public static List<BI_CM_Tag__c> newTags(String countryCode, boolean isActive, Integer testCounter) {

        List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>();
        BI_CM_Tag__c parentTag = new BI_CM_Tag__c(
                                        Name = 'Test '+testCounter+' tag 0 ', 
                                        BI_CM_Active__c = isActive,
                                        BI_CM_Country_code__c = countryCode,
                                        BI_CM_Hierarchy_level__c = '1');
        insert parentTag;

        for(Integer i=1; i < 101; i++) {
            tags.add(new BI_CM_Tag__c(
                                        Name = 'Test '+testCounter+' tag ' + i, 
                                        BI_CM_Active__c = isActive,
                                        BI_CM_Country_code__c = countryCode,
                                        BI_CM_Hierarchy_level__c = '1'));
        }  

        for(Integer i=101; i < 201; i++) {
            tags.add(new BI_CM_Tag__c(
                                        Name = 'Test '+testCounter+' tag ' + i, 
                                        BI_CM_Active__c = isActive,
                                        BI_CM_Country_code__c = countryCode,
                                        BI_CM_Hierarchy_level__c = '2',
                                        BI_CM_Parent__c = parentTag.Id));
        }    
        
        insert tags;
        return tags;            
    } 

    /**
     * Creates 201 new tags.
     *
     * @return List of tags
     */
    public static List<BI_CM_Tag__c> threeLevelTags(String countryCode, boolean isActive) {

        List<BI_CM_Tag__c> tags = new List<BI_CM_Tag__c>();
        BI_CM_Tag__c tag1 = new BI_CM_Tag__c(Name = 'Level1', 
                                            BI_CM_Active__c = isActive, 
                                            BI_CM_Country_code__c = countryCode, 
                                            BI_CM_Hierarchy_level__c = '1');
        insert tag1;
        tags.add(tag1);
        BI_CM_Tag__c tag21 = new BI_CM_Tag__c(Name = 'Level2.1', 
                                                BI_CM_Active__c = isActive, 
                                                BI_CM_Country_code__c = countryCode, 
                                                BI_CM_Hierarchy_level__c = '2', 
                                                BI_CM_Parent__c = tag1.Id);
        insert tag21;
        tags.add(tag21);
        BI_CM_Tag__c tag22 = new BI_CM_Tag__c(Name = 'Level2.2', 
                                                BI_CM_Active__c = isActive, 
                                                BI_CM_Country_code__c = countryCode, 
                                                BI_CM_Hierarchy_level__c = '2', 
                                                BI_CM_Parent__c = tag1.Id);
        insert tag22;
        tags.add(tag22);
        BI_CM_Tag__c tag23 = new BI_CM_Tag__c(Name = 'Level2.3', 
                                                BI_CM_Active__c = isActive, 
                                                BI_CM_Country_code__c = countryCode, 
                                                BI_CM_Hierarchy_level__c = '2', 
                                                BI_CM_Parent__c = tag1.Id);
        insert tag23;
        tags.add(tag23);
        BI_CM_Tag__c tag31 = new BI_CM_Tag__c(Name = 'Level3.1', 
                                                BI_CM_Active__c = isActive, 
                                                BI_CM_Country_code__c = countryCode, 
                                                BI_CM_Hierarchy_level__c = '3', 
                                                BI_CM_Parent__c = tag21.Id);
        insert tag31;
        tags.add(tag31);
        BI_CM_Tag__c tag32 = new BI_CM_Tag__c(Name = 'Level3.2', 
                                                BI_CM_Active__c = isActive, 
                                                BI_CM_Country_code__c = countryCode, 
                                                BI_CM_Hierarchy_level__c = '3', 
                                                BI_CM_Parent__c = tag22.Id);
        insert tag32;
        tags.add(tag32);
        
        return tags;            
    } 

}