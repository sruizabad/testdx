/**
 * Class TriggerFactory
 *
 * Used to instantiate and execute Trigger Handlers associated with sObjects.
 */
public without sharing class BI_CM_TriggerFactory {
    /**
     * Public static method to create and execute a trigger handler
     *
     * Arguments:   Schema.sObjectType soType - Object type to process (SObject.sObjectType) 
     *
     * Throws a TriggerException if no handler has been coded.
     */
    public static void createHandler(Schema.sObjectType soType) {
        // Get a handler appropriate to the object being processed
        BI_CM_TriggerInterface handler = getHandler(soType);

        // Make sure we have a handler registered, new handlers must be registered in the getHandler method.
        if (handler == null) { throw new BI_CM_Exception('No Trigger Handler registered for Object Type: ' + soType); }

        // Execute the handler to fulfil the trigger
        execute(handler);
    }

    /**
     * private static method to control the execution of the handler
     *
     * Arguments:   BI_CM_TriggerInterface handler - A Trigger Handler to execute
     */
    private static void execute(BI_CM_TriggerInterface handler) {
        // Before Trigger
        if (Trigger.isBefore) {
            // Call the bulk before to handle any caching of data and enable bulkification
            handler.bulkBefore();

            // Iterate through the records to be deleted passing them to the handler.
            if (Trigger.isDelete)
            {
                for (SObject so : Trigger.old)
                {
                    handler.beforeDelete(so);
                }
            }
            // Iterate through the records to be inserted passing them to the handler.
            if (Trigger.isInsert) {
                for (SObject so : Trigger.new) {
                    handler.beforeInsert(so);
                }
            }
            // Iterate through the records to be updated passing them to the handler.
            else if (Trigger.isUpdate) {
                for (SObject so : Trigger.old) {
                    handler.beforeUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        }

        // After Trigger
        if (Trigger.isAfter) {
            // Call the bulk before to handle any caching of data and enable bulkification
            handler.bulkAfter();

            // Iterate through the records deleted passing them to the handler.
            if (Trigger.isDelete)
            {
                for (SObject so : Trigger.old)
                {
                    handler.afterDelete(so);
                }
            }
            // Iterate through the records to be inserted passing them to the handler.
            if (Trigger.isInsert) {
                for (SObject so : Trigger.new) {
                    handler.afterInsert(so);
                }
            }
            // Iterate through the records to be updated passing them to the handler.
            else if (Trigger.isUpdate) {
                for (SObject so : Trigger.old) {
                    handler.afterUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        }  

        handler.andFinally();
    }

    /**
     * private static method to get the appropriate handler for the object type.
     * Modify this method to add any additional handlers.
     *
     * Arguments:   Schema.sObjectType soType - Object type tolocate (SObject.sObjectType)
     *
     * Returns:     BI_CM_TriggerInterface - A trigger handler if one exists or null.
     */
    private static BI_CM_TriggerInterface getHandler(Schema.sObjectType soType) {
        if (soType == BI_CM_Tag__c.sObjectType) {
            return new BI_CM_TagTriggerHandler(); 
        }else if(soType == BI_CM_Insight_tag__c.sObjectType){
            return new BI_CM_InsightTagTriggerHandler();
        }else if(soType == BI_CM_Insight__c.sObjectType){
            return new BI_CM_InsightTriggerHandler();
        }else if(soType == BI_CM_Insight_Comment__c.sObjectType){
            return new BI_CM_InsightCommentTriggerHandler();
        }
        return null;
    }
}