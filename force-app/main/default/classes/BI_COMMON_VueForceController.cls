global without sharing class BI_COMMON_VueForceController {

    //Wrapper to userinfo Class
    public String userInfoMap {get;set;}

    public static Set<String> notSupportedLists = new Set<String> {
        'ApexClass', 'Group'
    };

    public BI_COMMON_VueForceController () {
        userInfoMap = getUserInfo();
    }

    public String getUserInfo(){

        Map<String, String> userInfoMap = new Map<String, String> {
            'locale' => UserInfo.getLocale(),
            'timeZone' => String.valueOf(UserInfo.getTimeZone()),
            'id' => UserInfo.getUserId(),
            'sessionId' => UserInfo.getSessionId()
        };

        return JSON.serialize(userInfoMap);
    }




    // Wrapper Classes
    //global class ListViewInformationResponse {
    //    public ListViewWrapper metadata{get;set;}
    //    public List<ListView> listViewSelection {get;set;}
    //    public String listViewId {get;set;}
    //    public Map<Integer, String> objectIcons {get;set;}
    //    public String componentId {get;set;}
    //    public String message {get;set;}
    //}
    //
    
    global class RemoteResponse {
        public Object result {get;set;}
        public List<Object> records {get;set;}
        public sObject record {get;set;}
        public String query {get;set;}
        public Integer totalRecords {get;set;}
        public Integer totalPages {get;set;}
        public String firstIdOffset {get;set;}
        public String lastIdOffset {get;set;}
        public String componentId {get;set;}
        public String[] errors {get;set;}
        public String message {get;set;}
        public String status {get;set;}
        public Map<String, UserRecordAccess> visibility {get;set;}
        public LayoutWrapper layout {get;set;}
        public Object describe {get;set;}
        public Boolean hasErrors {
            get { return !errors.isEmpty();}
            set ;
        }

        public RemoteResponse() {
            errors = new List<String>();
            records = new List<sObject>();
        }
    }

  //  global class ListViewWrapper {

  //      public List<ColumnsWrapper> columns {get;set;}
  //      public String query {get;set;}
  //      public String sobjectType {get;set;}
        
  //  }

  //  global class ColumnsWrapper {
  //      public String ascendingLabel {get;set;}
  //      public String descendingLabel {get;set;}
  //      public String fieldNameOrPath {get;set;}
  //      public Boolean hidden {get;set;}
  //      public String label {get;set;}
  //      public String selectListItem {get;set;}
  //      public String sortDirection {get;set;}
  //      public Integer sortIndex {get;set;}
  //      public Boolean sortable {get;set;}
  //      public String type {get;set;}
  //  }
    global class LayoutWrapper {
        public String id {get; set;}
        public List<SectionWrapper> editLayoutSections {get; set;}
    }

    /**
     * Layout section wrapper
     */
    global class SectionWrapper {
        public Integer columns {get; set;}
        public String heading {get; set;}
        public Boolean useHeading {get;set;}
        public List<LayoutRowWrapper> layoutRows {get; set;}

    }

    /**
     * Layout row wrapper
     */
    global class LayoutRowWrapper {
        public List<LayoutItemsWrapper> layoutItems {get;set;}
        public Integer numItems {get; set;}
    }

    /**
     * Layout items wrapper
     */
    global class LayoutItemsWrapper {
        public String label {get;set;}
        public Boolean required {get;set;}
        public List<LayoutComponentWrapper> layoutComponents {get;set;}
        public Boolean isBlankSpace {
            get {
                return (label == null || String.isBlank(label));
            }
        }
        public String value {
            get {
                if(layoutComponents != null && !layoutComponents.isEmpty()) {
                    value = layoutComponents.get(0).value;
                }
                return value;
            }
            set;
        }

        public String type {
            get {
                if(layoutComponents != null && !layoutComponents.isEmpty()) {
                    //TODO Damage control
                    type = layoutComponents.get(0).details.type;
                }
                return type;
            }
            set;
        }
        

        public List<String> composedFields {
            get {
                if(layoutComponents != null 
                    && !layoutComponents.isEmpty()
                    && layoutComponents.get(0).components != null
                    && !layoutComponents.get(0).components.isEmpty()) {

                        composedFields = new List<String>();
                        for(FieldComponentsWrapper w : layoutComponents.get(0).components) {
                            composedFields.add(w.value);
                        }
                }
                return composedFields;
            } set;
        }
    }

    /**
     * Layout component wrapper
     */
    global class LayoutComponentWrapper {
        public String value {get;set;}
        public String fieldType {get;set;}
        public List<FieldComponentsWrapper> components {get; set;}
        public FieldComponentDetailWrapper details {get;set;}
    }

    /**
     * Layout cfield omponent wrapper
     */
    global class FieldComponentsWrapper {
        public FieldComponentDetailWrapper details {get;set;}
        public String value {get;set;}
    }

    /**
     * Layout field component detail wrapper
     */
    global class FieldComponentDetailWrapper {
        public String value {get;set;}
        public String soapType {get;set;}
        public String type {get;set;}
    }
      
    //This is the wrapper class the includes the job itself and a value for the percent complete
    global class BatchJob{
        public AsyncApexJob job {get; set;}
        public Integer percentComplete {get; set;}
    }

    global with sharing class SearcherWithSharing {
        public List<SObject> findSObjects(String query) {
            //System.debug(loggingLevel.Error, '*** Search with sharing: ');
            return Database.query(query);
        }

        public ApexPages.StandardSetController getWithSharingSetController(String query) {
            ApexPages.StandardSetController setController = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            return setController;
        }
    }


    /**
     * Gets the records.
     *
     * @param      listViewQuery    The list view query
     * @param      queryFilters     The query filters
     * @param      orderField       The order field
     * @param      orderDirection   The order direction
     * @param      currentPage      The current page
     * @param      recordsPerPage   The records per page
     * @param      lastIdOffset     The last identifier offset
     * @param      useSharingRules  The use sharing rules
     * @param      onlyCodes        The only codes
     * @param      getVisibility    The get visibility
     *
     * @return     The response with recrod information.
     */
    @RemoteAction
    @ReadOnly
    global static RemoteResponse getRecords(String listViewQuery,String queryFilters,
        String orderField,String orderDirection,Integer currentPage,
        Integer recordsPerPage, String lastIdOffset, Boolean useSharingRules,Boolean onlyCodes, Boolean getVisibility) {
        
        List<sObject> allRecord = new List<sObject>();
        ApexPages.StandardSetController setController;
        
        System.debug(logginglevel.ERROR, 'listViewQuery ---> ' + listViewQuery);
        System.debug(logginglevel.ERROR, 'queryFilters ---> ' + queryFilters);
        System.debug(logginglevel.ERROR, 'orderField ---> ' + orderField);
        System.debug(logginglevel.ERROR, 'orderDirection ---> ' + orderDirection);
        System.debug(logginglevel.ERROR, 'currentPage ---> ' + currentPage);
        System.debug(logginglevel.ERROR, 'recordsPerPage ---> ' + recordsPerPage);
        System.debug(logginglevel.ERROR, 'useSharingRules ---> ' + useSharingRules);
        //0 - Set inital values.
        String q = listViewQuery;
        String objectName = q.substringBetween('FROM', 'WHERE');
        if(orderDirection == null) orderDirection = 'DESC';
        if(currentPage == null) currentPage = 1;
        
        //1- Apply custom filters
        if(queryFilters != null && String.isNotBlank(queryFilters)) {
            if(q.containsIgnoreCase(' WHERE ')) {
                objectName = q.substringBetween('FROM', 'WHERE').trim();
                q = q.replace('WHERE', 'WHERE ' + queryFilters + ' AND ');
            } else {
                objectName = q.substringAfterLast('FROM').trim();
                q = q.replace('ORDER BY', 'WHERE ' + queryFilters + ' ORDER BY ');
            }
        }
        
        //2- Strip 'toLabels'
        if(onlyCodes) {
            q = q.replaceAll('toLabel\\(', '').replaceAll('\\),', ',');
            System.debug(loggingLevel.Error, '*** q WITHOUT TO LABEL: ' + q);
            
        }
        
        //3 - Add Order
        if(orderField != null && String.isNotBlank(orderField)) {
            if(q.containsIgnoreCase(' ORDER BY ')){
               q = q.replaceFirst('ORDER BY .*', ' ORDER BY ' + orderField + ' ' +orderDirection + ', Id ASC '); 
            } else {
                 q = q +' ORDER BY ' + orderField + ' ' +orderDirection + ', Id ASC ';
            }       
        }
        System.debug(loggingLevel.Error, '*** Final queryLocator: ' + q);

        //4 - Get totals
        String countQuery = q.replaceFirst('ORDER BY .*', '').replace(q.substringBetween('SELECT', 'FROM'), ' count() ');
        System.debug(loggingLevel.Error, '*** objectName: ' + objectName);
        System.debug(loggingLevel.Error, '*** countQuery: ' + countQuery);
        Integer totalRecords = Database.countQuery(countQuery);
        

        //5 - Get records
        List<sObject> records = new List<sObject>();
        if(totalRecords > 10000 || !notSupportedLists.contains(objectName)) {
            //We cannot instantiate an standard controller. Have to do it mannually
            //records = Database.query(paginateQuery(q, lastIdOffset));
            //q = q += ' LIMIT ' + recordsPerPage;
            System.debug(loggingLevel.Error, '*** q Final query custom: ' + q);
            records = Database.query(q);
        } else {
            try {

                //Use standardSetController stardardSetController
                //If with have to force sharing rules we have to use a inner "with sharing" class
                if(useSharingRules != null && !useSharingRules) {
                    setController = new ApexPages.StandardSetController(Database.getQueryLocator(q));          
                } else {
                    SearcherWithSharing s = new SearcherWithSharing();
                    setController = s.getWithSharingSetController(q);
                }
                setController.setPageSize(recordsPerPage);
                setController.setPageNumber(currentPage);
                //5 - WORKAROUND Page number hack
                for(Integer i = 1; i < currentPage; i++) {
                    setController.next();   
                }   
                records = setController.getRecords();
            } catch(Exception e) {
                records = Database.query(q);
            }
        }

        //5 - Get records


        
        //7 - Componse response
        RemoteResponse response = new RemoteResponse();
        response.totalRecords = totalRecords;
        response.totalPages = (Integer) Math.ceil( (Double) response.totalRecords / recordsPerPage );
        response.records = records;
        response.query = q;


        //EXTRA - Set visibility (limited to 200)
        if(getVisibility && recordsPerPage < 200) {

            Map<String, UserRecordAccess> visibility = new Map<String, UserRecordAccess>();               
            List<String> recordids = new List<String>();
            
            for(sObject rec : (List<sObject>)response.records) {
                recordids.add((String) rec.get('Id'));
            }
            
            for(UserRecordAccess acc : [SELECT RecordId, HasReadAccess, HasEditAccess FROM UserRecordAccess 
                                        WHERE UserId = :UserInfo.getUserId() AND RecordId IN :recordids]) {
                                            visibility.put(acc.RecordId, acc);
                                        }   
            
            System.debug(loggingLevel.Error, '*** visibility: ' + visibility);
            response.visibility = visibility;
        }
        
        
        
        
        
        System.debug(loggingLevel.Error, '*** response: ' + response);
       
        return response;

    }

    @RemoteAction
    global static RemoteResponse save(sObject[] records, Boolean allOrNone, String externalIdField){
        RemoteResponse response = new RemoteResponse();
        List<sObject> toUpdate = new List<sObject>();
        List<sObject> toInsert = new List<sObject>();

        Boolean aon = allOrNone != null ? allOrNone : true;

        for(sObject o : records) {
            System.debug(loggingLevel.Error, '*** sObject -> o --> ' + o);
            if(o.get('Id') != null) {
                toUpdate.add(o);
            } else {
                toInsert.add(o);
            }
        }
        
        // Iterate through each returned result
        for (Database.SaveResult sr : Database.insert(toInsert, aon)) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted object. object Id: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors()) {
                    response.errors.add(err.getMessage());
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('object fields that affected this error: ' + err.getFields());
                }
            }
        }


        // Iterate through each returned result
        for (Database.SaveResult sr : Database.update(toUpdate, aon)) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated object. object Id: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors()) {
                    response.errors.add(err.getMessage());
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('object fields that affected this error: ' + err.getFields());
                }
            }
        }

        response.totalRecords = records.size();
        response.records.addAll((List<Object>)toInsert);
        response.records.addAll((List<Object>)toUpdate);
        return response;
    }

    public static String paginateQuery(String query, String lastIdOffset) {
        return '';
    }

    @RemoteAction
    global static RemoteResponse getLookupRecords(String query, String offset) {
        return getRecords(query, null, null, null, 1, 200, offset, false, false, false);
    }
    
    @RemoteAction
    public static List<BatchJob> getBatchJobs(List<String> idsJobs, List<String> classNames, Boolean onlyMyBatches, String createdDate){
        //Create new list of BatchJobs, a wrapper class that includes the job and percent complete.
        List<BatchJob> batchJobs = new List<BatchJob>();

        //Generate query for retrieve batches information
        String query = 'SELECT TotalJobItems, Status, ExtendedStatus, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id, CreatedDate, CreatedById, CreatedBy.Name, CompletedDate, ApexClassId, ApexClass.Name FROM AsyncApexJob WHERE JobType = \'BatchApex\' AND';
    
        if(idsJobs != null && !idsJobs.isEmpty()) {
            query += ' Id IN (\'' + String.join(idsJobs, '\',\'') + '\')';
        } else if (classNames != null && !classNames.isEmpty()) {
            query += ' ApexClass.Name IN (\'' + String.join(classNames, '\',\'') + '\') AND CreatedDate > ' + createdDate;
            if(onlyMyBatches) {
                query += ' AND CreatedById = \'' + UserInfo.getUserId() + '\'';
            }
        }

        System.debug(loggingLevel.Error, '*** BI_PL_BatchStatusPollerCtrl -> getBatchJobs -> query: ' + query);

        //Query the Batch apex jobs
        if(!query.endsWith('AND')){
            for(AsyncApexJob a : (List<AsyncApexJob>) Database.query(query)){
                 
                Double itemsProcessed = a.JobItemsProcessed; 
                Double totalItems = a.TotalJobItems;
     
                BatchJob j = new BatchJob();
                j.job = a;
     
                //Determine the pecent complete based on the number of batches complete
                if(totalItems == 0){
                    //A little check here as we don't want to divide by 0.
                    j.percentComplete = 100;
                }else{
                    j.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
                }
     
                batchJobs.add(j);
            }
        }
        System.debug(loggingLevel.Error, '*** batchJobs: ' + batchJobs);
        return batchJobs;
    }

    @RemoteAction
    global static RemoteResponse getLayoutInfo(String recordId, String recordTypeId, String sObjectName) {
        System.debug(loggingLevel.Error, '*** BI_COMMON_VueForceController CONSTRUCTOR BEGIN');

        String stype;
        sObject currentRecord;
        String objectIcon;
        Map<Integer, String> objectIcons;
        LayoutWrapper layout;

        //String recordId = ApexPages.currentPage().getParameters().get('Id');
        //String recordTypeId = ApexPages.currentPage().getParameters().get('RecordTypeId');
        //String sObjectName = ApexPages.currentPage().getParameters().get('SObjectName');
        
        //We have Id, so we can know recordtype and sobject name doing a query to database
        if(recordId != null) {
            Id sObjId = Id.valueOf(recordId);
            stype = String.valueOf(sObjId.getSobjectType());
            System.debug(loggingLevel.Error, '*** stype: ' + stype);

            Boolean hasRecordType = false;
            String soql = 'SELECT ';

            Map<String, Schema.SObjectField> schemaMap = Schema.getGlobalDescribe().get(stype).getDescribe().fields.getMap();

            for(String fieldName : schemaMap.keySet()) {
                soql += fieldName + ', ';
                if(fieldName.toLowerCase().equals('recordtypeid')){
                    hasRecordType = true;
                }
                //get names of related objects (to fill lookups)
                if(schemaMap.get(fieldName).getDescribe().getType() == DisplayType.Reference) {
                    soql += schemaMap.get(fieldName).getDescribe().getRelationShipName() +'.Name' + ', ';
                }
            }
            soql = soql.left(soql.lastIndexOf(','));

            soql += ' FROM ' + stype + ' WHERE Id = \'' + sObjId + '\'' + ' LIMIT 1';

            System.debug(loggingLevel.Error, '*** soql: ' + soql);
            try {
                currentRecord = Database.query(soql);
                System.debug(loggingLevel.Error, '*** hasRecordType: ' + hasRecordType);
                //If the record doesnt have recordtypes (lack of recordtype field), we use master record type
                recordtypeId = hasRecordType && currentRecord.get('RecordTypeId') != null ? (String) currentRecord.get('RecordTypeId') : '012000000000000AAA';
            } catch(Exception e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'El registro no existe. El id es incorrecto o ya ha sido eliminado'));
            }

        }

        //We dont have Id, we should get sobjecttype and recordtype from URL
        else if(sObjectName != null) {
            //If the URL doent inform RecordType, we use master record type
            recordtypeId = recordTypeId != null ? recordTypeId : '012000000000000AAA';
            stype = sObjectName;
            currentRecord =  Schema.getGlobalDescribe().get(stype).newSObject();
            try {
                currentRecord.put('OwnerId', UserInfo.getUserId());
                //no permissions in master, so check if rt comes from url
                if(recordTypeId != null){
                    currentRecord.put('RecordTypeId',recordTypeId);
                }
                //currentRecord.put('RecordType',recordTypeId);
            } catch(Exception e) {
                System.debug(loggingLevel.Error, '*** e.getMessage(): ' + e.getMessage());
            }
        }


        System.debug(loggingLevel.Error, '*** currentRecord: ' + currentRecord);
        System.debug(loggingLevel.Error, '*** recordtypeId: ' + recordtypeId);
        System.debug(loggingLevel.Error, '*** stype: ' + stype);

        //we have the data, so lets query the layout
        if(recordtypeId != null && String.isNotBlank(recordtypeId) && stype != null && String.isNotBlank(stype)) {

            HttpRequest req = new HttpRequest();
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
            req.setHeader('Content-Type', 'application/json');

            String domainUrl = URL.getSalesforceBaseUrl().toExternalForm();
            String endpointUrl = domainUrl + '/services/data/v36.0/sobjects/' + stype + '/describe/layouts/'+recordtypeId;


            System.debug(LoggingLevel.ERROR, 'domain URL' + endpointUrl);
            req.setEndpoint(endpointUrl);
            req.setMethod('GET');

            Http h = new Http();
            HttpResponse res = h.send(req);
            //Map<String, Object>  root  = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            
            Type resultType = Type.forName('BI_COMMON_VueForceController.LayoutWrapper');
            //System.debug(loggingLevel.Error, '*** resultType: ' + resultType);
            //System.debug(loggingLevel.Error, '*** res.getBody(): ' + res.getBody());
            layout = (LayoutWrapper)JSON.deserialize(res.getBody(), resultType);
            System.debug(loggingLevel.Error, '*** layout: ' + layout);

            //Now we get the icon for this sObjectType
            List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
            List<Schema.DescribeTabResult> tabsList = new list<Schema.DescribeTabResult>();
        
            // Iterate through each tab set describe for each app and display the info
            for(Schema.DescribeTabSetResult tsr : tabSetDesc) {
                tabsList.addAll(tsr.getTabs());
            }

            objectIcons = new Map<Integer, String>();
            // Get more information for the Sales app            
            for(Schema.DescribeTabResult mytab : tabsList) { 
                //Comprobar que el sobjectname está en el keyset del resto de mapas
                if(mytab.getSobjectName().equals(stype)) {
                    System.debug(loggingLevel.Error, '*** ENCONTRADO TAB ' + mytab);
                    objectIcon = mytab.getIconUrl();
                    //System.debug(loggingLevel.Error, '*** objectIcon: ' + objectIcon);
                    for(Schema.DescribeIconResult icon : mytab.getIcons()) {
                        System.debug(loggingLevel.Error, '*** icon: ' + icon);
                        //Only images, exclude SVGs
                        if(icon.getHeight() > 0) {
                            objectIcons.put(icon.getHeight(), icon.getUrl().substringAfter('.com'));
                        }
                    }
                    break;
                }
            }

            //Default values for icons (in case of permission problem access)
            if(objectIcons.isEmpty()) {
                objectIcons.put(32, '/img/icon/custom51_100/pencil32.png');
                objectIcons.put(60, '/img/icon/custom51_100/pencil32.png');
                objectIcons.put(120, '/img/icon/custom51_100/pencil32.png');
            }

            //canShowForm = true;

            //autocompletamos campos provenientes de la URL. Se deben informar con f_[apiNameCampo]
            //list<String> paramKeys = new List<String>(ApexPages.currentPage().getParameters().keySet());
            //for(String key : paramKeys) {
            //    if(key.startsWith('f_')) {
            //        String fieldName = key.split('f_', 2)[1];
            //        String value = ApexPages.currentPage().getParameters().get(key);
            //        if(value != null && value != '' && value != 'undefined'){
            //            //TODO : Type safe assignments
                        
            //            if(value == 'true' || value == 'false') {
            //                currentRecord.put(fieldName, Boolean.valueOf(value));
            //            } else {
            //                currentRecord.put(fieldName, value);
            //            }
            //        }
            //    }
            //}

        } else {
            //ERRORS. Insuficient data
            //canShowForm = false;
            System.debug(loggingLevel.Error, '*** insufficient data: ');
        }
        RemoteResponse response = new RemoteResponse();
        response.record = currentRecord;
        response.layout = layout;
        return response;
    
    }
}