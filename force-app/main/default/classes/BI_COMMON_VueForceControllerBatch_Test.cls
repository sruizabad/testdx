global class BI_COMMON_VueForceControllerBatch_Test 
	implements Database.Batchable<sObject> {

	global final String query;

	global BI_COMMON_VueForceControllerBatch_Test(String q) {
		this.query=q;
	}
	global Database.QueryLocator start(Database.BatchableContext BC){
    	return Database.getQueryLocator(query);
   	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope){
    	for(sobject s : scope){     		
     	} 
    }

   	global void finish(Database.BatchableContext BC){
   	}
}