@isTest
global class BI_COMMON_VueForceController_Mock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"buttonLayoutSection":{"detailButtons":[{"behavior":null,"colors":[{"color":"0070d2","context":"primary","theme":"theme4"}],"content":null,"contentSource":null,"custom":false,"encoding":null,"height":null,"icons":[{"contentType":"image/svg+xml","height":0,"theme":"theme4","url":"https://eu11.salesforce.com/img/icon/t4v35/action/change_record_type.svg","width":0},{"contentType":"image/png","height":60,"theme":"theme4","url":"https://eu11.salesforce.com/img/icon/t4v35/action/change_record_type_60.png","width":60},{"contentType":"image/png","height":120,"theme":"theme4","url":"https://eu11.salesforce.com/img/icon/t4v35/action/change_record_type_120.png","width":120}],"label":"ChangeRecordType","menubar":false,"name":"ChangeRecordType","overridden":false,"resizeable":false,"scrollbars":false,"showsLocation":false,"showsStatus":false,"toolbar":false,"url":null,"width":null,"windowPosition":null}]}}');
        res.setStatusCode(200);
        return res;
    }
}