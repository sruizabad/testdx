@isTest
private class BI_COMMON_VueForceController_Test{

    @Testsetup
    static void setUp() {
        List<Contact> contactList = new List<Contact>();
        for(Decimal i = 0; i < 5; i++){
            Contact c = new Contact(FirstName = 'test_FirstName_'+i, LastName = 'test_LastName_'+i);
            contactList.add(c);
        }
        insert contactList;
    }

    @isTest static void vueForceController_initializeVars_Test(){
        BI_COMMON_VueForceController.RemoteResponse resp = new BI_COMMON_VueForceController.RemoteResponse();
        List<sObject> reList = (List<sObject> ) resp.records;
        sObject re = resp.record;
        String q = resp.query;
        Integer totRe = resp.totalRecords;
        Integer totPa = resp.totalPages;
        String lastId = resp.lastIdOffset;
        String cId = resp.componentId;
        List<String> err = resp.errors;
        String me = resp.message;
        String st = resp.status;
        Map<String, UserRecordAccess> vis = resp.visibility;
        BI_COMMON_VueForceController.LayoutWrapper lay = resp.layout;
        Object obj = resp.describe;
        Boolean hErr = resp.hasErrors;

        BI_COMMON_VueForceController.LayoutWrapper ly = new BI_COMMON_VueForceController.LayoutWrapper();
        String lId = ly.Id;
        List<BI_COMMON_VueForceController.SectionWrapper> sWr = ly.editLayoutSections;

        BI_COMMON_VueForceController.SectionWrapper secc = new BI_COMMON_VueForceController.SectionWrapper();
        Integer nCol = secc.columns;
        String he = secc.heading;
        Boolean uHe = secc.useHeading;
        List<BI_COMMON_VueForceController.LayoutRowWrapper> lrWr = secc.layoutRows;

        BI_COMMON_VueForceController.LayoutRowWrapper lyRaw = new BI_COMMON_VueForceController.LayoutRowWrapper();
        List<BI_COMMON_VueForceController.LayoutItemsWrapper> liWr = lyRaw.layoutItems;
        Integer nuIt = lyRaw.numItems;

        BI_COMMON_VueForceController.FieldComponentsWrapper fiComponents = new BI_COMMON_VueForceController.FieldComponentsWrapper();
        BI_COMMON_VueForceController.FieldComponentDetailWrapper deWr2 = fiComponents.details;
        String fiCoVal = fiComponents.value;

        BI_COMMON_VueForceController.FieldComponentDetailWrapper fiComponentDetails = new BI_COMMON_VueForceController.FieldComponentDetailWrapper();
        String fiCoDeVal = fiComponentDetails.value;
        String fiCoDeSoap = fiComponentDetails.soapType;
        String fiCoDeTy = fiComponentDetails.type;

        BI_COMMON_VueForceController.LayoutComponentWrapper lyComponents = new BI_COMMON_VueForceController.LayoutComponentWrapper();
        String cVal = lyComponents.value;
        String cFiTy = lyComponents.fieldType;
        List<BI_COMMON_VueForceController.FieldComponentsWrapper> comWr = lyComponents.components;
        comWr = new List<BI_COMMON_VueForceController.FieldComponentsWrapper>();
        comWr.add(fiComponents);
        lyComponents.components = comWr;
        BI_COMMON_VueForceController.FieldComponentDetailWrapper deWr = lyComponents.details;
        lyComponents.details = fiComponentDetails;

        BI_COMMON_VueForceController.LayoutItemsWrapper lyItems = new BI_COMMON_VueForceController.LayoutItemsWrapper();
        String la = lyItems.label;
        Boolean req = lyItems.required;
        Boolean bSp = lyItems.isBlankSpace;
        List<BI_COMMON_VueForceController.LayoutComponentWrapper> lcWr = lyItems.layoutComponents;
        lcWr = new List<BI_COMMON_VueForceController.LayoutComponentWrapper>();
        lcWr.add(lyComponents);
        lyItems.layoutComponents = lcWr;
        String iVal = lyItems.value;
        String iTy = lyItems.type;
        List<String> compFiList = lyItems.composedFields;

        BI_COMMON_VueForceController.BatchJob batchJob = new BI_COMMON_VueForceController.BatchJob();
        AsyncApexJob aaj = batchJob.job;
        Integer pc = batchJob.percentComplete;

        BI_COMMON_VueForceController.SearcherWithSharing searcher = new BI_COMMON_VueForceController.SearcherWithSharing();
        List<SObject> sO = searcher.findSObjects('SELECT Id, Name FROM Contact');
        ApexPages.StandardSetController sc = searcher.getWithSharingSetController('SELECT Id, Name FROM Contact');
    }

    @isTest static void vueForceController_Test(){
        BI_COMMON_VueForceController cont = new BI_COMMON_VueForceController();

        String userInfo = cont.getUserInfo();

        String paginate = BI_COMMON_VueForceController.paginateQuery('SELECT Id, Name FROM Contact', null);

        BI_COMMON_VueForceController.RemoteResponse rR = BI_COMMON_VueForceController.getLookupRecords('SELECT Id, Name FROM Contact', null);

        BI_COMMON_VueForceController.RemoteResponse rR1 = BI_COMMON_VueForceController.getRecords('SELECT Id, Name FROM Contact', null, null, null, 1, 200, null, false, true, false);
        BI_COMMON_VueForceController.RemoteResponse rR2 = BI_COMMON_VueForceController.getRecords('SELECT Id, Name FROM Contact', null, null, null, 1, 200, null, true, false, false);
        BI_COMMON_VueForceController.RemoteResponse rR3 = BI_COMMON_VueForceController.getRecords('SELECT Id, Name FROM Contact', null, null, null, 1, 100, null, false, false, true);
        BI_COMMON_VueForceController.RemoteResponse rR5 = BI_COMMON_VueForceController.getRecords('SELECT Id, Name FROM Contact', 'FirstName Like %test%', null, null, 1, 200, null, false, false, false);
        BI_COMMON_VueForceController.RemoteResponse rR6 = BI_COMMON_VueForceController.getRecords('SELECT Id, Name FROM Contact', null, 'FirstName', 'ASC', 1, 200, null, false, false, false);
        BI_COMMON_VueForceController.RemoteResponse rR7 = BI_COMMON_VueForceController.getRecords('SELECT Id, Name FROM Contact ORDER BY Id', null, 'FirstName', 'DESC', 1, 200, null, false, false, false);

        List<Contact> contactList = [Select Id, FirstName, LastName From Contact];
        for(Contact c : contactList){
            c.FirstName = c.FirstName+'edited';
        }
        BI_COMMON_VueForceController.RemoteResponse rR8 = BI_COMMON_VueForceController.save(contactList, true, null);
        List<Contact> contactList2 = new List<Contact>();
        contactList2.add(new Contact(FirstName='testNew1', LastName='testNew1Last'));
        BI_COMMON_VueForceController.RemoteResponse rR9 = BI_COMMON_VueForceController.save(contactList2, true, null);

        try{
        	Test.startTest();
            BI_COMMON_VueForceControllerBatch_Test bt = new BI_COMMON_VueForceControllerBatch_Test('SELECT Id, Name FROM Contact');
            String idbatch = DataBase.executeBatch(bt);
            List<BI_COMMON_VueForceController.BatchJob> bj = BI_COMMON_VueForceController.getBatchJobs(new List<String>{idbatch}, null, false, null);
            List<BI_COMMON_VueForceController.BatchJob> bj2 = BI_COMMON_VueForceController.getBatchJobs(new List<String>(), new List<String>{'test', 'test2'}, true, System.now()+'');
            Test.stopTest();
        }catch(Exception e){
        	Test.stopTest();
        }
        
        try{
            Test.setMock(HttpCalloutMock.class, new BI_COMMON_VueForceController_Mock());
            BI_COMMON_VueForceController.RemoteResponse rR10 = BI_COMMON_VueForceController.getLayoutInfo(null, null, 'Contact');
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, e);
        }
    }

    @isTest static void vueForceController_Test2(){
        BI_COMMON_VueForceController cont = new BI_COMMON_VueForceController();
       
        try{
        	Test.startTest();
            Test.setMock(HttpCalloutMock.class, new BI_COMMON_VueForceController_Mock());
            BI_COMMON_VueForceController.RemoteResponse rR = BI_COMMON_VueForceController.getLayoutInfo(null, null, 'Contact');
            Test.stopTest();
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, e);
            Test.stopTest();
        }
        
        try{
            Contact c = [SELECT Id, Name FROM Contact LIMIT 1];
            BI_COMMON_VueForceController.RemoteResponse rR1 = BI_COMMON_VueForceController.getLayoutInfo(c.Id, null, null);
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, e);
        }
    }

    @isTest static void vueForceController_Test3(){
        BI_COMMON_VueForceController cont = new BI_COMMON_VueForceController();
       
        try{
        	Test.startTest();
            Contact c = [SELECT Id, Name FROM Contact LIMIT 1];
            BI_COMMON_VueForceController.RemoteResponse rR = BI_COMMON_VueForceController.getLayoutInfo(c.Id, null, null);
            Test.stopTest();
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, e);
            Test.stopTest();
        }
    }
}