/*
INITIAL CODE : Code fetches interface details from Object and calls class and method based on the interface details
Author : Vinith Nair
Date : 10/14/15
*/

global class BI_CVCRM_SKYYVA_INTERFACE_SCHEDULER implements Schedulable 
{     
        /*** 
          *
          * This method starts the Skyvva interface scheduler -- It checks for interfaces that are ready to run and executed their APEX class/methods
          *
        /**/
        public id jobid = null;
        public Datetime chronTime = null;
        global void execute(SchedulableContext ctx) 
        {
                List<BI_SKYVVA_INTERFACE_SCHEDULER__c> listBISC = new List<BI_SKYVVA_INTERFACE_SCHEDULER__c>();
                for(BI_SKYVVA_INTERFACE_SCHEDULER__c BISC :[select id, Active_Flag__c, Frequency_Mins_BI__c, Interface_Name_BI__c, SKYVVA_Interface_Name__c, Last_run_datetime_BI__c, Last_Run_Status_BI__c, Country_code_BI__c from BI_SKYVVA_INTERFACE_SCHEDULER__c where (active_flag__c = true)])
                {
                     if(BISC.Last_run_datetime_BI__c != null && BISC.Last_run_datetime_BI__c.addminutes(integer.valueof(BISC.Frequency_Mins_BI__c)) < System.now())
                     {
                          BI_INT_CVCRM_Execute_Skyvva SOE = new BI_INT_CVCRM_Execute_Skyvva();
                          if(BISC.Interface_Name_BI__c != null && BISC.SKYVVA_Interface_Name__c != null)
                          {
                              {
                                  try
                                  {
//                                      system.debug('############################################ OUTBOUND INTERFACE CALLED' + '#### CHRON TIME =' + chronTime.getTime());
                                      SOE.executeInterface(BISC.Interface_Name_BI__c, BISC.SKYVVA_Interface_Name__c, BISC.id, BISC.Country_code_BI__c);
                                      BISC.Last_run_datetime_BI__c = system.now(); //( APPROXIMATE DATE TIME )
                                      BISC.Active_Flag__c = true;
                                      BISC.Last_Run_Status_BI__c = 'Success';
                                      BISC.errorMessage__c = '';
                                      listBISC.add(BISC);
                                  }
                                  catch(exception Ex)
                                  {
                                      system.debug('Exception Occurred');
                                      BISC.errorMessage__c = '' + Ex + '';
                                      BISC.Active_Flag__c = false;
                                      BISC.Last_Run_Status_BI__c = 'Failed';
                                      listBISC.add(BISC);
                                  }
                              }
                          }
                      } 
             } // END FOR
             
             // UPDATE THE OBJECT WITH VALUES CHANGED DURING EXECUTION
             update listBISC;
             
             
                //Rescheduling the class every five minutes
                chronTime = System.now().addMinutes(5);                           
                String strChronExpression = ' ' + chronTime.second() + ' ' + chronTime.minute() + ' ' + chronTime.hour() + ' ' + chronTime.day() + ' ' + chronTime.month() + ' ? ' + chronTime.year();                           
                BI_CVCRM_SKYYVA_INTERFACE_SCHEDULER SC = new BI_CVCRM_SKYYVA_INTERFACE_SCHEDULER ();
                jobid = System.schedule('BI_CVCRM_SKYYVA_INTERFACE_SCHEDULER' + chronTime , strChronExpression , SC);
        }// END METHOD
} // END CLASS