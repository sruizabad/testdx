public class BI_INT_CVCRM_Execute_Skyvva 
{
 
      /*AUTHOR : Vinith Nair-CapGemini for BI
      *
      *  THIS METHOD IS CALLED BY THE SCHEDULER DAEMON (BI_SKYYVA_INTERFACE_SCHEDULER)
      *
      * @param : strInterfaceName -- Name of the interface to call
      * @param : strSkyvvaInterfaceName -- Name of the Skyvva interface to invoke -- the interface ID will be queried from Skyvva internal table
      */
     public void executeInterface(String strInterfaceName ,String strSkyvvaInterfaceName, id idBISchedulerRecordid, String strCountryCode)
     {
      try
      { 

        system.debug('####################################### SKYVVA INTERFACE NAME strSkyvvaInterfaceName  = ' + strSkyvvaInterfaceName );
        
        // QUERY SKYVVA TABLE AND RETURN SKYVVA INTERFACE ID
        String strInterfaceId = [SELECT Id FROM skyvvasolutions__Interfaces__c WHERE skyvvasolutions__Name__c LIKE: strSkyvvaInterfaceName Limit 1].id;
        List<String> listCountryCodes = new List<String>(strCountryCode.split(';'));
        system.debug('###################### LIST OF COUNTRY CODES' + listCountrycodes);
        String InterfaceName = strInterfaceName;        
        system.debug('######################  +InterfaceName ' + InterfaceName);
                
        // MAKE SURE THE SKYVVA INTERFACE IS FOUND
        if (strInterfaceId == null || strInterfaceId == '')
        {
            throw new BIException('Skyvva interface Id not found for Interface Named : ' + strSkyvvaInterfaceName); 
        }
                // BASED ON THE INTERFACE NAME CALL THE CORRECT METHOD
                if(strInterfaceName == 'Orders')
                {          
                   pushOrders(strInterfaceId, listCountryCodes);
                   system.debug('############################################ OUTBOUND INTERFACE CALLED');
                }  
                else if(strInterfaceName == 'DirectOrders')
                {          
                   pushDirectOrders(strInterfaceId, listCountryCodes);
                   system.debug('############################################ OUTBOUND INTERFACE CALLED');
                }   
                else if(strInterfaceName == 'TransferOrders')
                {          
                   pushTransferOrders(strInterfaceId, listCountryCodes);
                   system.debug('############################################ OUTBOUND INTERFACE CALLED');
                }     
                else if (strInterfaceName  == 'DisplayKits')
                {
                    system.debug('########################### COUNTRY CODES =' + listCountryCodes + '##### INTERFACE ID' + strInterfaceId );
                    pushDisplayKits(strInterfaceId, listCountryCodes );
                }
                else if (strInterfaceName  == 'Accounts')
                {
                    system.debug('########################### COUNTRY CODES =' + listCountryCodes + '##### INTERFACE ID' + strInterfaceId );
                    pushAccounts(strInterfaceId, listCountryCodes );
                }
                else if (strInterfaceName  == 'Calls')
                {
                    system.debug('########################### COUNTRY CODES =' + listCountryCodes + '##### INTERFACE ID' + strInterfaceId );
                    pushCalls(strInterfaceId, listCountryCodes );
                }
                else if(strInterfaceName == 'MultichannelRec')
                {          
                   pushMultichannelRec(strInterfaceId, listCountryCodes);
                   system.debug('############################################ OUTBOUND INTERFACE CALLED');
                }
                else
                {
                    throw new BIException(strInterfaceName + ': Not a valid interface to call');
                }           
         }
         catch(exception Ex)
         {
                system.debug('Exception: ' + Ex);
                throw new BIException('##### Invalid Interface Name :  This interface does not exist ####');  
         }
    }
       
       
    /*
     * 
     * SENDS DISPLAY KITS TO PI FOR PUBLISH TO BPCS
     *
     */      
    public void pushDisplayKits(String strInterfaceId, List<String> listCountryCodes)
    {
        // THIS SOQL IS NOT LONGER VALID
        String strSOQL = 'Select Id from pricing_rule_vod__c Where recordtype.name = \'Limit_rule_vod\' and product_vod__r.Type_of_display_bi__c != \'\' and quantity_min_vod__c = 1  and country_code_bi__c in : listCountryCodes'; 
        
        List<ID> listPricingRuleIds = new List<ID>();
        
        For(Pricing_rule_vod__c P:Database.query(strSOQL))
        {
          listPricingRuleIds.add(P.Id);
        }
        
        if (listPricingRuleIds.size() > 0)
        { 
            skyvvasolutions.IServices.invokeCallout(strInterfaceId, listPricingRuleIds ,skyvvasolutions.IServicesUtilConst.BATCH); 
        }       
    }
 
    
  
    
     /**
      * 
      * PUSH ORDERS 
      *
      */
    public void pushOrders(String strInterfaceId, List<String> listCountryCodes)
    {       
//        String strSOQL = 'SELECT Id FROM Order_vod__c WHERE Delivery_Status_BI__c = \'\' AND Approval_Status_BI__c in (\'N/A\',\'Approved\') AND Status_vod__c = \'Submitted_vod\' AND Owner.Type = \'User\' AND Master_Order_vod__c = False AND KAM_MASTER_ORDER_BI__C = False AND BUYING_GROUP_MASTER_ORDER_BI__C = False AND OwnerId in (SELECT Id FROM User u WHERE u.country_code_bi__c in : listCountryCodes) limit 10'; 
        String strSOQL = 'SELECT Id FROM Order_vod__c WHERE Delivery_Status_BI__c = \'\' AND ((Order_Free_Goods_vod__c!=0) OR (Order_Total_Quantity_vod__c!=0)) AND  Approval_Status_BI__c in (\'N/A\',\'Approved\') AND Status_vod__c = \'Submitted_vod\' AND Owner.Type = \'User\' AND Master_Order_vod__c = False AND KAM_MASTER_ORDER_BI__C = False AND BUYING_GROUP_MASTER_ORDER_BI__C = False AND OwnerId in (SELECT Id FROM User u WHERE u.country_code_bi__c in : listCountryCodes) and account_vod__r.bi_external_id_2__c != \'\''; 
      
        List<ID> listOrderIds = new List<ID>();

        For(Order_vod__c O:Database.query(strSOQL))
        {
          listOrderIds.add(O.Id);
        }
        
        if (listOrderIds.size() > 0)
        {
        
            Savepoint savepointDataNotSent = Database.setSavepoint();
            List<Order_vod__c> listOrders = new List<Order_vod__c>();
            try
            {
                   System.debug('############################################# SKYVVA INTERFACE ID CALLS ' + strInterfaceId);
                   skyvvasolutions.IServices.invokeCallout(strInterfaceId, listOrderIds,skyvvasolutions.IServicesUtilConst.BATCH);        
                   
                   for(Order_vod__c O: [select id from Order_vod__c where id in: listOrderIds])
                   {
                            O.delivery_status_BI__C= 'Sent to ERP';
                            listOrders.add(O);
                   }
                   update listOrders;
             }
             catch(Exception ex)
             {
                    system.debug('##################### Sending failed');
                    Database.rollback(savepointDataNotSent);
                    throw ex;
             }
         }
        
    }
    
    
    /**
      * 
      * PUSH  DIRECT ORDERS ONLY
      *
      */
    public void pushDirectOrders(String strInterfaceId, List<String> listCountryCodes)
    {       
//        String strSOQL = 'SELECT Id FROM Order_vod__c WHERE RecordType.Name in(\'Direct_vod\',\'KAM Order\') AND Delivery_Status_BI__c = \'\' AND Approval_Status_BI__c in (\'N/A\',\'Approved\') AND Status_vod__c = \'Submitted_vod\' AND Owner.Type = \'User\' AND Master_Order_vod__c = False AND KAM_MASTER_ORDER_BI__C = False AND BUYING_GROUP_MASTER_ORDER_BI__C = False AND OwnerId in (SELECT Id FROM User u WHERE u.country_code_bi__c in : listCountryCodes) limit 10'; 
// and account_vod__r.bi_external_id_2__c != \'\'
        String strSOQL = 'SELECT Id FROM Order_vod__c WHERE RecordType.Name in(\'Direct_vod\',\'KAM Order\') AND Delivery_Status_BI__c = \'\' AND ((Order_Free_Goods_vod__c!=0) OR (Order_Total_Quantity_vod__c!=0)) AND Approval_Status_BI__c in (\'N/A\',\'Approved\') AND Status_vod__c = \'Submitted_vod\' AND Owner.Type = \'User\' AND Master_Order_vod__c = False AND KAM_MASTER_ORDER_BI__C = False AND BUYING_GROUP_MASTER_ORDER_BI__C = False AND OwnerId in (SELECT Id FROM User u WHERE u.country_code_bi__c in : listCountryCodes)  and account_vod__r.bi_external_id_2__c != \'\''; 
        List<ID> listOrderIds = new List<ID>();

        For(Order_vod__c O:Database.query(strSOQL))
        {
          listOrderIds.add(O.Id);
        }
        
        if (listOrderIds.size() > 0)
        {
        
            Savepoint savepointDataNotSent = Database.setSavepoint();
            List<Order_vod__c> listOrders = new List<Order_vod__c>();
            try
            {
                   System.debug('############################################# SKYVVA INTERFACE ID CALLS ' + strInterfaceId);
                   //skyvvasolutions.IServices.invokeCallout(strInterfaceId, listOrderIds,skyvvasolutions.IServicesUtilConst.BATCH);
                    skyvvasolutions.IServices.invokeCallout(strInterfaceId, listOrderIds,'AUTO');          
                   
                   for(Order_vod__c O: [select id from Order_vod__c where id in: listOrderIds])
                   {
                            O.delivery_status_BI__C= 'Sent to ERP';
                            listOrders.add(O);
                   }
                   update listOrders;
             }
             catch(Exception ex)
             {
                    system.debug('##################### Sending failed');
                    Database.rollback(savepointDataNotSent);
                    throw ex;
             }
         }
        
    }
    
    
    
    /**
      * 
      * PUSH TRANSFER ORDERS ONLY
      *
      */
    public void pushTransferOrders(String strInterfaceId, List<String> listCountryCodes)
    {       
        String strSOQL = 'SELECT Id FROM Order_vod__c WHERE RecordType.Name = \'Transfer_vod\' AND Delivery_Status_BI__c = \'\' AND ((Order_Free_Goods_vod__c!=0) OR (Order_Total_Quantity_vod__c!=0)) AND Approval_Status_BI__c in (\'N/A\',\'Approved\') AND Status_vod__c = \'Submitted_vod\' AND Owner.Type = \'User\' AND Master_Order_vod__c = False AND KAM_MASTER_ORDER_BI__C = False AND BUYING_GROUP_MASTER_ORDER_BI__C = False AND OwnerId in (SELECT Id FROM User u WHERE u.country_code_bi__c in : listCountryCodes) limit 10'; 
//        String strSOQL = 'SELECT Id FROM Order_vod__c WHERE RecordType.Name = \'Transfer_vod\' AND Delivery_Status_BI__c = \'\' AND Approval_Status_BI__c in (\'N/A\',\'Approved\') AND Status_vod__c = \'Submitted_vod\' AND Owner.Type = \'User\' AND Master_Order_vod__c = False AND KAM_MASTER_ORDER_BI__C = False AND BUYING_GROUP_MASTER_ORDER_BI__C = False AND OwnerId in (SELECT Id FROM User u WHERE u.country_code_bi__c in : listCountryCodes)  and account_vod__r.bi_external_id_2__c != \'\'';         
        List<ID> listOrderIds = new List<ID>();

        For(Order_vod__c O:Database.query(strSOQL))
        {
          listOrderIds.add(O.Id);
        }
        
        if (listOrderIds.size() > 0)
        {
        
            Savepoint savepointDataNotSent = Database.setSavepoint();
            List<Order_vod__c> listOrders = new List<Order_vod__c>();
            try
            {
                   System.debug('############################################# SKYVVA INTERFACE ID CALLS ' + strInterfaceId);
                   skyvvasolutions.IServices.invokeCallout(strInterfaceId, listOrderIds,skyvvasolutions.IServicesUtilConst.BATCH);        
                   
                   for(Order_vod__c O: [select id from Order_vod__c where id in: listOrderIds])
                   {
                            O.delivery_status_BI__C= 'Sent to ERP';
                            listOrders.add(O);
                   }
                   update listOrders;
             }
             catch(Exception ex)
             {
                    system.debug('##################### Sending failed');
                    Database.rollback(savepointDataNotSent);
                    throw ex;
             }
         }
        
    }
    
    
    /*
     * UTILITY FUNCTION TO PUSH SPECIFIC LIST OF ORDERS
     *
     */
    public void pushOrdersManual(String strInterfaceId,List<ID> listOrderIds)
    {

            Savepoint savepointDataNotSent = Database.setSavepoint();
            List<Order_vod__c> listOrders = new List<Order_vod__c>();
            try
            {
                   System.debug('############################################# SKYVVA INTERFACE ID CALLS ' + strInterfaceId);
                   skyvvasolutions.IServices.invokeCallout(strInterfaceId, listOrderIds,skyvvasolutions.IServicesUtilConst.BATCH);        
                   
                   for(Order_vod__c O: [select id from Order_vod__c where id in: listOrderIds])
                   {
                            O.delivery_status_BI__C= 'Sent to ERP';
                            listOrders.add(O);
                   }
                   update listOrders;
             }
             catch(Exception ex)
             {
                    system.debug('##################### Sending failed');
                    Database.rollback(savepointDataNotSent);
                    throw ex;
             }
         
    }
    
    /*VIN START*/
    /*
     * 
     * SENDS ACCOUNTS TO SAP/PI FOR PUBLISH TO BPCS
     *
     */      
    public void pushAccounts(String strInterfaceId, List<String> listCountryCodes)
    {
        String strSOQL = 'Select Id from Account where LastModifiedDate = LAST_N_DAYS:10 AND country_code_bi__c in : listCountryCodes AND ERP_Status_BI__c = \'Resend to ERP\' Limit 10'; 
        
        List<ID> listAccountIds = new List<ID>();
        List<Account> listAccount = new List<Account>();
        
        For(Account Acc:Database.query(strSOQL))
        {
          listAccountIds.add(Acc.Id);
        }
        
        if (listAccountIds.size() > 0)
        { 
            skyvvasolutions.IServices.invokeCallout(strInterfaceId, listAccountIds,skyvvasolutions.IServicesUtilConst.BATCH); 
        }
        
        For(Account AC : [select id, ERP_Status_BI__c from Account where id IN: listAccountIds]){
            AC.ERP_Status_BI__c = 'Sent to ERP';
            listAccount.add(AC);
        }
        update listAccount;
    }
 
    

    /*VIN END*/
    
    
     /*VIN START*/
    /*
     * 
     * NEW METHOD FOR CALL VOD RECORDS
     *
     */      
    public void pushCalls(String strInterfaceId, List<String> listCountryCodes)
    {
        String strSOQL = 'Select Id from Call2_vod__c where (NOT Status_vod__c LIKE \'Submitted%\') AND integration_status_BI__c = null AND integration_status_BI__c != \'Sent to ERP\' AND Call_Date_vod__c >= TODAY AND Account_vod__r.Country_Code_BI__c in :listCountryCodes'; 
        
        List<ID> listCallIds = new List<ID>();
        List<Call2_vod__c> listcalls = new List<Call2_vod__c>();
        
        For(Call2_vod__c Call:Database.query(strSOQL))
        {
          listCallIds.add(Call.Id);
        }
        
        if (listCallIds.size() > 0)
        { 
            skyvvasolutions.IServices.invokeCallout(strInterfaceId, listCallIds,skyvvasolutions.IServicesUtilConst.BATCH); 
        } 
        For(Call2_vod__c CV : [select id, integration_status_BI__c  from Call2_vod__c where id IN: listCallIds]){
            CV.integration_status_BI__c = 'Sent to ERP';
            listCalls.add(CV);
        }
        update listCalls;      
    }
    
     public void pushMultichannelRec(String strInterfaceId, List<String> listCountryCodes)
    {
        
        
List<Id> mccIds = new List<Id>();


/* Iterate through list of all Multichannel consent that have been cretated N days before */
/* -------------------------------------------------------------------------------------- */

        
for (Multichannel_Consent_vod__c mccp: [SELECT Id, LastModifiedDate FROM Multichannel_Consent_vod__c where LastModifiedDate = LAST_N_DAYS:1 AND LastModifiedDate < TODAY]){
    

mccIds.add(mccp.Id);


}       
        
if (mccIds.size() ==0){
        
System.debug('************************************************None of the records are getting picked');
}
        
if (mccIds.size() > 0)
{
System.debug('***********************************************There are records in the List: '+mccIds.size());
System.debug(mccIds);
System.debug('***********************************************Interface Id: '+strInterfaceId);
skyvvasolutions.IServices.invokeCallout(strInterfaceId, mccIds,skyvvasolutions.IServicesUtilConst.BATCH); 
}
  
}
 
    

    /*VIN END*/
}