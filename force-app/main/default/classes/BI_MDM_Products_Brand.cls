global with sharing class BI_MDM_Products_Brand extends skyvvasolutions.IProcessCustomAbs implements skyvvasolutions.GisOdbgIntegration.IProcess
{
/**
*
* Stores All email messages to be sent for TA creation/modification
*/
static List<Messaging.SingleEmailMessage> allEmails = new List<Messaging.SingleEmailMessage>();

private static Map<String,skyvvasolutions__IMessage__c> messagesToCancel =new Map<String,skyvvasolutions__IMessage__c>();
private static Map<String,skyvvasolutions__IMessage__c> messagesToProcess =new Map<String,skyvvasolutions__IMessage__c>();
public static Map<string,string> emailTexts = new map<string,string>();//<msgId, strMessageToSendEmail>
string Orgname;

public static boolean isExisting=false;
public static boolean isChanged=false;
public static String strMessageToSendEmail ='';
public static String UpdProdToSendEmail ='';
//public static map<string,string> MsgToMail = new map<string,string>();
//public static map<string,string> ExsToMail = new map<string,string>();
private static List<Product_vod__c> lp=new List<Product_vod__c>();
//private final static Map<Id,List<Integer>> mIndex=new Map<Id,List<Integer>>();

/**************************************************************************************************
 Method receives the SAP data into iResult and update in product Catalog object
*************************************************************************************************/
global override void doMap(skyvvasolutions.IServicesUtil.IMessageResult iResult)
{
    system.debug('===> BI_MDM_Products_Brand - doMap - iResult: ' + iResult);

    //Declarations
     //Map of country codes in Product_Country_Code__c custom setting
     Map<String, Product_Country_Code__c> mapCountry = Product_Country_Code__c.getAll();
     List<Product_Country_Code__c> lstProductCCode = Product_Country_Code__c.getall().values();
     //existing ids?
     List<string> existingid = new list<String>();
     //list of global products (to assign parent products)
     List<string> globalProductlist = new list<string>();
     Map<string,id> mapexternalId = new map<string,id>();
     Map<string,string> mapVexternalID = new map<string,string>(); //Add SUC
     Map<string,string> mapParentPrdExtID = new map<string,string>(); //Add SUC
     Map<string,string> mapIdExternalstring  = new map<string,string>();
     Map<string,string> mapnewProduct = new map<string,string>();
        String mapParentExternalID;
        String mapParentExt_ID;
        
     /*******************************************************************************************
        1. Extract all externalIds and populate the list of existing products local and global
     *******************************************************************************************/
         for(integer i=0;i<iResult.listMessage.size();i++)
         {
                 //Data from SAP
                 Map<String,String> mapRecord =iResult.listMapRecord.get(i);


                //filter by country code or global records only
                 if(mapRecord.get('country_code_bi__c') == null || mapCountry.containskey(mapRecord.get('country_code_bi__c')))
                    {
                                String externalID = mapRecord.get('external_id_vod__c');

                                if(mapRecord.get('manufacturer_vod__c')!=null || mapRecord.get('therapeutic_area_vod__c')!=null)
                                {
                                        externalID ='B_'+externalID ;
                                }
                                 if(mapRecord.get('recordtypeid')=='VIRTUAL')
                                 {
                                        externalID = 'P_' +externalID ;
                                 }
                                 existingid.add(externalID);
                                    mapParentExternalID = externalID; 
                                 //Code to add parent product
                                    if(mapRecord.get('brand_objectives_bi__c')!=null)
                                    {
                                             String globalProduct = 'B_' + mapRecord.get('brand_objectives_bi__c');
                                             globalProductlist.add(globalProduct);
                                             mapParentExt_ID = globalProduct;

                                    }//End for Brand Objectives
                    }//end if

            }//end for
            system.debug('===> BI_MDM_Products_Brand - doMap - 1. products: ' + existingid);

            /*******************************************************************************************
            2. If there are Products to process get the sf Id, vexternalId and parentexternalId.
         *******************************************************************************************/
      if(existingid!=null && existingid.size()>0)
      {
          for(Product_vod__c   erecs:[select id,external_ID_vod__c,VExternal_Id_vod__c,parent_product_vod__c, name from Product_vod__c  where external_ID_vod__c in:existingid])
          {
             mapexternalId.put(erecs.external_id_vod__c,erecs.Id);
             mapVexternalID.put(erecs.external_id_vod__c,erecs.VExternal_Id_vod__c);
             mapParentPrdExtID.put(erecs.external_id_vod__c,erecs.parent_product_vod__c);
          }
      }//end if 2
            system.debug('===> BI_MDM_Products_Brand - doMap - 2. mapexternalId: ' + mapexternalId + '<br> mapVexternalID' + mapVexternalID + '<br> mapParentPrdExtID' + mapParentPrdExtID);
            /***************************************************************************
            3. Query the Parent Id and assign to the Products
            ****************************************************************************/
            List<Product_vod__c> listProductIds = [select id,external_id_vod__c from Product_vod__c where external_id_vod__c in:globalProductlist]; //Shailesh : Put it outsde the for loop
            if(listProductIds!=null && listProductIds.size()>0)
            {
                 for(Product_vod__c prods:listProductIds)
                    {
                         mapIdExternalstring.put(prods.external_id_vod__c,prods.id);
                       // mapIdExternalstring.put(mapParentExternalID,prods.id);
                    }
            }
         system.debug('===> BI_MDM_Products_Brand - doMap - 3. mapIdExternalstring: ' + mapIdExternalstring);

         /*******************************************************************************************
            4. Second loop. Does the actual mapping
        *******************************************************************************************/
        for(integer i=0;i<iResult.listMessage.size();i++)
        {
                  //Data from SAP
                    Map<String,String> mapRecord =iResult.listMapRecord.get(i);
                    //filter by country codes or global products
                    if(mapRecord.get('country_code_bi__c') == null || mapCountry.containskey(mapRecord.get('country_code_bi__c')))
                    {
                        Product_vod__c Prod;
                        String externalID = mapRecord.get('external_id_vod__c');
                        if((mapRecord.get('recordtypeid')==NULL || (mapRecord.get('therapeutic_area_vod__c')!= NULL || mapRecord.get('manufacturer_vod__c')!=NULL)) || (mapRecord.get('recordtypeid')=='VIRTUAL'))
                        {

                                if(mapRecord.get('manufacturer_vod__c')!=null || mapRecord.get('therapeutic_area_vod__c')!=null)
                                    externalID ='B_'+mapRecord.get('external_id_vod__c');//never should do this
                                else if(mapRecord.get('recordtypeid')=='VIRTUAL')
                                    externalID = 'P_' +mapRecord.get('external_id_vod__c') ;

                                //Added the below for resolving duplicates -3
                                //Check if the records already exists

                                if(mapexternalId!=null && mapexternalId.keyset()!=null && mapexternalId.keyset().contains(externalID)) //externalid numeric to be considered - Suchitra
                                {
                                     //select existing product
                                     isExisting=true; //Check later - Suchitra
                                     Prod = [SELECT id,name, country_code_bi__c, description_vod__c,manufacturer_vod__c,therapeutic_area_vod__c,Parent_Product_vod__c,no_metrics_vod__c , no_details_vod__c, no_cycle_plans_vod__c, display_order_vod__c from Product_vod__c where id = :mapexternalId.get(externalID)];
                                }
                                else if(mapnewProduct==null ||!mapnewProduct.keyset().contains(externalID))
                                {
                                        //create new product
                                        Prod = new Product_vod__c();
                                        mapnewProduct.put(externalID,'1');
                                        isExisting=false;
                                        Prod.put('external_id_vod__c',externalID);
                                }else
                                continue; //skip to the next iteration of the loop
                                    //Added by Suchitra--Start
                                   // Organization myOrg = [Select Name From Organization];
                                   string name = UserInfo.getUserName().substringAfterLast('.');
                                   Orgname = ' of Brand in Org:' + name;
                                   // strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'ORG Name: ' + Orgname  + '<br>';
                                  
                                    //Added by Suchitra -End
                                    //Veeva Id, External ID, VExternal_Id_vod__c
                                    If (isExisting)
                                    {
                                        system.debug('==> isExisting product ' + Prod);
                                        
                                            //basic email template
                                        //Product name
                                        if (mapRecord.get('description_vod__c')!=null && (mapRecord.get('description_vod__c').left(80)!=Prod.name))
                                        {
                                            if(!isChanged)
                                                strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'Existing Product name : '+ Prod.Name;
                                                strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'MDM ID/external ID: ' + externalID;
                                                strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Veeva ID: ' + mapexternalId.get(externalID);
                                                UpdProdToSendEmail    = UpdProdToSendEmail + 'Product Name: ' +(mapRecord.get('description_vod__c').left(80));
                                                //strMessageToSendEmail = strMessageToSendEmail + '<br><br>' + 'Following Field(s) have changed:' +'<br>'+ 'Product Name: ' +(mapRecord.get('description_vod__c').left(80));
                                                Prod.put('Name', mapRecord.get('description_vod__c').left(80));
                                            isChanged=true;
                                        }
                                        else
                                        {           
                                                strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'Product Name: ' +(mapRecord.get('description_vod__c').left(80));
                                                strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'MDM ID/external ID: ' + externalID;
                                                strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Veeva ID: ' + mapexternalId.get(externalID);
                                        }
                                        
                                       
                                       // strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'V External ID: ' ;

                                        if(mapVexternalID.get(externalID)!=null)
                                            strMessageToSendEmail = strMessageToSendEmail + mapVexternalID.get(externalID);

                                        //1. country code
                                        if(mapRecord.get('country_code_bi__c')!=null && mapRecord.get('country_code_bi__c')!= Prod.country_code_bi__c)  //Modified by Suchitra
                                        {
                                             //strMessageToSendEmail = strMessageToSendEmail  + '<br>Existing Country Code: ' + Prod.Country_Code_BI__c +  '<br>' + 'Following Field(s) have changed:' +'<br>'+ 'Country Code : ' +mapRecord.get('country_code_bi__c');
                                             UpdProdToSendEmail = UpdProdToSendEmail + '<br>'+ 'Country Code : ' +mapRecord.get('country_code_bi__c');
                                             Prod.put('Country_Code_BI__c', mapRecord.get('country_code_bi__c'));
                                             isChanged=true;
                                        }
                                        else if(mapRecord.get('country_code_bi__c')==null && Prod.country_code_bi__c<>'GLOBAL')
                                        { //producto global
                                            // strMessageToSendEmail = strMessageToSendEmail  + '<br>Old Country Code: ' + Prod.Country_Code_BI__c + '<br><br>' + 'Following Field(s) have changed:' +'<br>'+ 'Country Code : ' + 'GLOBAL';
                                            UpdProdToSendEmail = UpdProdToSendEmail + '<br>'+ 'Country Code : ' + 'GLOBAL';
                                             Prod.put('Country_Code_BI__c', 'GLOBAL');
                                             isChanged=true;
                                        }
                                        else if(mapRecord.get('country_code_bi__c')!=null   ){
                                                strMessageToSendEmail = strMessageToSendEmail  + '<br>Country: ' +mapRecord.get('country_code_bi__c');
                                        }
                                        else
                                        {
                                            strMessageToSendEmail = strMessageToSendEmail  + '<br>Country: Global'  ;
                                        }
                                        //2. Product name
                                        /*if (mapRecord.get('description_vod__c')!=null && (mapRecord.get('description_vod__c').left(80)!=Prod.name))
                                        {
                                            if(!isChanged)
                                                strMessageToSendEmail = strMessageToSendEmail + '<br> Existing Product name : '+ Prod.Name + '<br><br>' + 'Following Field(s) have changed:' +'<br>'+ 'Product Name: ' +(mapRecord.get('description_vod__c').left(80));
                                            else
                                                strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'Product Name: ' +(mapRecord.get('description_vod__c').left(80));

                                            Prod.put('Name', mapRecord.get('description_vod__c').left(80));
                                            isChanged=true;
                                        }*/
                                        //3. Description
                                        if(mapRecord.get('description_vod__c')!=null && ((mapRecord.get('description_vod__c') !=Prod.description_vod__c) || Prod.description_vod__c == null || Prod.description_vod__c == ''))
                                        {
                                            Prod.put('description_vod__c', mapRecord.get('description_vod__c'));
                                              //strMessageToSendEmail = strMessageToSendEmail + '<br> <br>' + 'Following Field(s) have changed: '+'<br>'+ 'Product Description: ' +(mapRecord.get('description_vod__c'));
                                               UpdProdToSendEmail = UpdProdToSendEmail + '<br>'+ 'Product Description: ' +(mapRecord.get('description_vod__c'));                                         
                                               isChanged = true;
                                        }

                                        //4. manufacturer brands

                                        if(mapRecord.get('manufacturer_vod__c')!=null && ((mapRecord.get('manufacturer_vod__c') !=Prod.manufacturer_vod__c) || Prod.manufacturer_vod__c == null || Prod.manufacturer_vod__c == ''))
                                        {
                                            Prod.put('manufacturer_vod__c', mapRecord.get('manufacturer_vod__c'));
                                             //strMessageToSendEmail = strMessageToSendEmail + '<br> <br>' + 'Following Field(s) have changed: '+'<br>'+ 'Manufacturer: ' +(mapRecord.get('manufacturer_vod__c'));
                                             UpdProdToSendEmail = UpdProdToSendEmail + '<br>'+ 'Manufacturer: ' +(mapRecord.get('manufacturer_vod__c'));
                                             isChanged = true;
                                        }
                                        //5. Therapeutic Area
                                        if(mapRecord.get('therapeutic_area_vod__c')!=null && ((mapRecord.get('therapeutic_area_vod__c') !=Prod.therapeutic_area_vod__c) || Prod.therapeutic_area_vod__c == null || Prod.therapeutic_area_vod__c == ''))
                                        {
                                            Prod.put('therapeutic_area_vod__c', mapRecord.get('therapeutic_area_vod__c'));
                                              //strMessageToSendEmail = strMessageToSendEmail + '<br> <br>' + 'Following Field(s) have changed: '+'<br>'+ 'Therapeutic Area: ' +(mapRecord.get('therapeutic_area_vod__c'));
                                              UpdProdToSendEmail = UpdProdToSendEmail + '<br>'+ 'Therapeutic Area: ' +(mapRecord.get('therapeutic_area_vod__c'));
                                              isChanged = true;
                                        }
                                        //6. Display Order
                                        if(mapRecord.get('display_order_vod__c')!=null && ((mapRecord.get('display_order_vod__c') !=String.valueOf(Prod.display_order_vod__c)) || Prod.display_order_vod__c == null))
                                        {
                                            Prod.put('display_order_vod__c', Decimal.valueOf(mapRecord.get('display_order_vod__c')));
                                            //strMessageToSendEmail = strMessageToSendEmail + '<br> <br>' + 'Following Field(s) have changed: '+'<br>'+ 'Display Order: ' +(mapRecord.get('display_order_vod__c'));
                                             UpdProdToSendEmail = UpdProdToSendEmail + '<br>'+ 'Display Order: ' +(mapRecord.get('display_order_vod__c'));
                                            isChanged = true;                                       
                                        }

                                        system.debug('===> NoCheck1: ' + mapRecord.get('no_cycle_plans_vod__c'));                                       
                                        system.debug('===> NoCheck2: ' + Prod.no_cycle_plans_vod__c);
                                        system.debug('===> NoCheck3: ' + BI_MDM_Products_Utilities.getStringValue(Prod.no_cycle_plans_vod__c));
                                        
                                        //7. No Cycle Plans - boolean
                                        if((mapRecord.get('no_cycle_plans_vod__c')!=null && (mapRecord.get('no_cycle_plans_vod__c') !=BI_MDM_Products_Utilities.getStringValue(Prod.no_cycle_plans_vod__c)) || BI_MDM_Products_Utilities.getStringValue(Prod.no_cycle_plans_vod__c) == ''))
                                        {
                                            Prod.put('no_cycle_plans_vod__c', BI_MDM_Products_Utilities.getBooleanValue(mapRecord.get('no_cycle_plans_vod__c')));
                                            //strMessageToSendEmail = strMessageToSendEmail + '<br> <br>' + 'Following Field(s) have changed: '+'<br>'+ 'No Cycle Plans: ' +(mapRecord.get('no_cycle_plans_vod__c'));
                                             UpdProdToSendEmail = UpdProdToSendEmail +  '<br>'+ 'No Cycle Plans: ' +(mapRecord.get('no_cycle_plans_vod__c'));
                                            isChanged = true;
                                        }
                                        system.debug('===> NoCheck11: ' + mapRecord.get('no_cycle_plans_vod__c'));                                      
                                        system.debug('===> NoCheck21: ' + Prod.no_cycle_plans_vod__c);
                                        system.debug('===> NoCheck31: ' + BI_MDM_Products_Utilities.getStringValue(Prod.no_cycle_plans_vod__c));

                                        //8. No details
                                        if(mapRecord.get('no_details_vod__c')!=null && ((mapRecord.get('no_details_vod__c') !=BI_MDM_Products_Utilities.getStringValue(Prod.no_details_vod__c)) || Prod.no_details_vod__c == null || BI_MDM_Products_Utilities.getStringValue(Prod.no_details_vod__c) == ''))
                                        {
                                            Prod.put('no_details_vod__c', BI_MDM_Products_Utilities.getBooleanValue(mapRecord.get('no_details_vod__c')));
                                            //strMessageToSendEmail = strMessageToSendEmail + '<br> <br>' + 'Following Field(s) have changed: '+'<br>'+ 'No Details: ' +(mapRecord.get('no_details_vod__c'));
                                            UpdProdToSendEmail = UpdProdToSendEmail + '<br>'+ 'No Details: ' +(mapRecord.get('no_details_vod__c'));
                                            isChanged = true;
                                        }

                                        //9. No metrics
                                        if(mapRecord.get('no_metrics_vod__c')!=null && ((mapRecord.get('no_metrics_vod__c') !=BI_MDM_Products_Utilities.getStringValue(Prod.no_metrics_vod__c)) || Prod.no_metrics_vod__c == null || BI_MDM_Products_Utilities.getStringValue(Prod.no_metrics_vod__c) == ''))
                                        {
                                            Prod.put('no_metrics_vod__c', BI_MDM_Products_Utilities.getBooleanValue(mapRecord.get('no_metrics_vod__c')));
                                            //strMessageToSendEmail = strMessageToSendEmail + '<br> <br>' + 'Following Field(s) have changed: '+'<br>'+ 'No Metrics: ' +(mapRecord.get('no_metrics_vod__c'));
                                             UpdProdToSendEmail = UpdProdToSendEmail +  '<br>'+ 'No Metrics: ' +(mapRecord.get('no_metrics_vod__c'));
                                            isChanged = true;
                                        }
                                        system.debug('===> isExisting productchk3 ' + externalID);
                                        system.debug('===> ParentProdCheck1: ' + Prod.Parent_Product_vod__c);                                      
                                       // system.debug('===> ParentProdCheck2: ' + listProductIds[0].id);
                                        system.debug('===> ParentProdCheck3: ' + mapIdExternalstring);
                                        system.debug('===> ParentProdCheck5: ' + mapIdExternalstring.get('B_' + mapRecord.get('brand_objectives_bi__c')));
                                                
                                        //Update Parent product if null
                                           //  if(mapIdExternalstring!=null && mapIdExternalstring.keyset()!=null && (Prod.Parent_Product_vod__c ==null || Prod.Parent_Product_vod__c !=mapIdExternalstring.get('B_' + mapRecord.get('brand_objectives_bi__c'))))
                                           if(mapIdExternalstring!=null && Prod.country_code_bi__c<>'GLOBAL' && (Prod.Parent_Product_vod__c ==null || Prod.Parent_Product_vod__c !=mapIdExternalstring.get('B_' + mapRecord.get('brand_objectives_bi__c'))))
                                            {
                                                system.debug('===> ParentProdCheck6: ' + Prod.Parent_Product_vod__c);
                                                UpdProdToSendEmail = UpdProdToSendEmail + '<br>' +'Parent Veeva ID: '+mapIdExternalstring.get('B_' + mapRecord.get('brand_objectives_bi__c'));
                                               // strMessageToSendEmail = strMessageToSendEmail + '<br>' +'Parent External ID: '+externalID;
                                               UpdProdToSendEmail = UpdProdToSendEmail + '<br>' +'Parent External ID: '+'B_' + mapRecord.get('brand_objectives_bi__c');
                                                Prod.put('Parent_Product_vod__c', mapIdExternalstring.get('B_' + mapRecord.get('brand_objectives_bi__c')));
                                                isChanged = true;
                                                system.debug('===> ParentProdCheck7: ' + mapIdExternalstring.get('B_' + mapRecord.get('brand_objectives_bi__c')));
                                                
                                            }
                                            
                                            system.debug('==> GlobalParentProductCheck21 ' + isExisting);
                                        system.debug('==> GlobalParentProductCheck22 ' + isChanged);
                                        
                                        if(isChanged)
                                        strMessageToSendEmail = strMessageToSendEmail + '<br><br>' + 'Following Field(s) have changed:' +'<br>'+ UpdProdToSendEmail;
                                        
                                         UpdProdToSendEmail='';
                                    }
                                    if(!isExisting) //NEW PRODUCT CREATION, NO EXISTING
                                    {
                                        //Product Name & description
                                        if(mapRecord.get('description_vod__c')!=null)
                                        {
                                                strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'Product Name: '+ (mapRecord.get('description_vod__c').left(80));
                                                Prod.put('Name', mapRecord.get('description_vod__c').left(80));
                                                strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'Description: '+ (mapRecord.get('description_vod__c'));
                                                Prod.put('Description_vod__c', mapRecord.get('description_vod__c'));
                                        }

                                        //basic email template
                                        strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'MDM ID/external ID: '+ externalID;
                                        
                                        //1. country code
                                        if(mapRecord.get('country_code_bi__c')!=null)
                                        {
                                                strMessageToSendEmail = strMessageToSendEmail + '<br>Country: ' +(mapRecord.get('country_code_bi__c'));
                                                Prod.put('country_code_bi__c', mapRecord.get('country_code_bi__c'));
                                        }
                                        else
                                        {
                                             strMessageToSendEmail = strMessageToSendEmail + '<br>Country: ' + 'GLOBAL';
                                             Prod.put('Country_Code_BI__c', 'GLOBAL');
                                        }

                                        //2.Product Name & 3. description
                                        /*if(mapRecord.get('description_vod__c')!=null)
                                        {
                                                strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'Product Name: '+ (mapRecord.get('description_vod__c').left(80));
                                                Prod.put('Name', mapRecord.get('description_vod__c').left(80));
                                                strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'Description: '+ (mapRecord.get('description_vod__c'));
                                                Prod.put('Description_vod__c', mapRecord.get('description_vod__c'));
                                        }*/

                                        //4. Manufacturer BRANDS
                                        if(mapRecord.get('manufacturer_vod__c')!=null){
                                            strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'Manufacturer: '+ (mapRecord.get('description_vod__c'));
                                            Prod.put('manufacturer_vod__c', mapRecord.get('manufacturer_vod__c'));
                                        }

                                        //5. Therapeutic Area
                                        if(mapRecord.get('therapeutic_area_vod__c')!=null){
                                            Prod.put('therapeutic_area_vod__c', mapRecord.get('therapeutic_area_vod__c'));
                                            strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'Therapeutic Area: '+ (mapRecord.get('therapeutic_area_vod__c'));
                                        }
                                        //6. Product Identifier
                                        if(mapRecord.get('product_identifier_vod__c')=='SP')
                                        {
                                            Prod.put('Product_Type_vod__c', 'Sample');
                                        }else{
                                            Prod.put('Product_Type_vod__c', 'Detail');
                                        }

                                        if(mapIdExternalstring!=null && mapIdExternalstring.keyset()!=null && mapIdExternalstring.keyset().contains(externalID))
                                        {
                                                strMessageToSendEmail = strMessageToSendEmail + '<br>' +'Parent Veeva ID: '+mapIdExternalstring.get(externalID);
                                               // strMessageToSendEmail = strMessageToSendEmail + '<br>' +'Parent External ID: '+externalID;
                                               strMessageToSendEmail = strMessageToSendEmail + '<br>' +'Parent External ID: '+ mapParentExt_ID;
                                                Prod.put('Parent_Product_vod__c', mapIdExternalstring.get(externalID));
                                        }

                                        //7. Display Order
                                        if(mapRecord.get('display_order_vod__c')!=null)
                                        {
                                            strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'Display Order: '+ (mapRecord.get('display_order_vod__c'));
                                            Prod.put('display_order_vod__c', Decimal.ValueOf(mapRecord.get('display_order_vod__c')));
                                        }

                                        //8. No cycle plans
                                        if(mapRecord.get('no_cycle_plans_vod__c')!=null)
                                        {
                                            strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'No Cycle Plans: '+ (mapRecord.get('no_cycle_plans_vod__c'));
                                            Prod.put('no_cycle_plans_vod__c', BI_MDM_Products_Utilities.getBooleanValue(mapRecord.get('no_cycle_plans_vod__c')));
                                        }

                                        //9. No details
                                        if(mapRecord.get('no_details_vod__c')!=null)
                                        {
                                            strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'No Details: '+ (mapRecord.get('no_details_vod__c'));
                                            Prod.put('no_details_vod__c', BI_MDM_Products_Utilities.getBooleanValue(mapRecord.get('no_details_vod__c')));
                                        }

                                        //10. No metrics
                                        if(mapRecord.get('no_metrics_vod__c')!=null)
                                        {
                                            strMessageToSendEmail = strMessageToSendEmail +'<br>' + 'No Metrics: '+ (mapRecord.get('no_details_vod__c'));
                                            Prod.put('no_metrics_vod__c', BI_MDM_Products_Utilities.getBooleanValue(mapRecord.get('no_metrics_vod__c')));
                                        }
                                            system.debug('==> is NEW product ' + Prod);
                                    }//end if existing

                                        system.debug('==> GlobalParentProductCheck1 ' + isExisting);
                                        system.debug('==> GlobalParentProductCheck2 ' + isChanged);
                                        
                             if(!isExisting ||isChanged){
                                 lp.add(Prod);
                                 String msgId = iResult.listMessage.get(i).Id;
                                 emailTexts.put(msgId,strMessageToSendEmail);
                                 //add message present to mapped contact for update its status Failed/Compled
                                 messagesToProcess.put(iResult.listMessage.get(i).Id,iResult.listMessage.get(i));
                                 system.debug('Message to be processed: ' + Prod + ' mail ' + strMessageToSendEmail);
                             }else{
                                 messagesToCancel.put(iResult.listMessage.get(i).Id, iResult.listMessage.get(i));
                                  system.debug('Message to be cancelled: ' + Prod + ' mail '+ strMessageToSendEmail);
                             }

                             isExisting=false; //DEBUG
                             isChanged=false;   // DEBUG
                             strMessageToSendEmail='';
                            
                     }//end if clasifying
                }//end if filtering by country
                else //country not included - cancel message
                {
                    messagesToCancel.put(iResult.listMessage.get(i).Id, iResult.listMessage.get(i));
                    system.debug('Message to be cancelled: '+iResult.listMessage.get(i)+' mail '+ strMessageToSendEmail);
                }
        } //end for second loop 4.

}//doMap


global override List<skyvvasolutions.IServicesUtil.UpsertResult2> upsert2()
{
//upsert records

  Database.UpsertResult[] results=Database.upsert(lp,false);
    system.debug('==> upsert2 results ' +results );
    //NOEMI updateSkyvvaMessage(iResult, results);
    BI_MDM_Products_Utilities.updateSkyvvaMessageToProcess(messagesToProcess.values(), results,emailTexts, Orgname);
    BI_MDM_Products_Utilities.updateSkyvvaMessageToCancel(messagesToCancel.values(), 'MDM Product: Message Cancelled, no brand created or updated');
    return null;
}//End for global override List Method


}