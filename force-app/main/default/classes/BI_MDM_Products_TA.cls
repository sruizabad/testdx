/* 
Created Date: 24/08/2016
Purpose : Receives the Therapeutic Areas data from SAP MDM and upload it into the Product Catalog.
*/
global with sharing class BI_MDM_Products_TA extends skyvvasolutions.IProcessCustomAbs implements skyvvasolutions.GisOdbgIntegration.IProcess{

  private static Map<String,skyvvasolutions__IMessage__c> messagesToCancel =new Map<String,skyvvasolutions__IMessage__c>();
  private static Map<String,skyvvasolutions__IMessage__c> messagesToProcess =new Map<String,skyvvasolutions__IMessage__c>();
  public static Map<string,string> emailTexts = new map<string,string>();//<msgId, strMessageToSendEmail>
  public static List<string> listOfTAExternalID = new list<String>();
  string Orgname;

  /**
  *
  * Stores All email messages to be sent for TA creation/modification
  */
  //static List<Messaging.SingleEmailMessage> allEmails = new List<Messaging.SingleEmailMessage>();

  public static boolean isExisting=false;
  public static boolean isGlobalExisting=false;
  public static boolean isChanged=false;
  public static boolean isGlobalChanged=false;
  public static String strMessageToSendEmail ='';
  public static String UpdProdToSendEmail ='';
  private static List<Product_vod__c> lp=new List<Product_vod__c>();


  /**************************************************************************************************
   Method receives the SAP data into iResult and update in product Catalog object
  *************************************************************************************************/
  global override void doMap(skyvvasolutions.IServicesUtil.IMessageResult iResult)
  {
    system.debug('===> BI_MDM_Products_TA - doMap - iResult: ' + iResult);



    //Declarations
     //Map of country codes in Product_Country_Code__c custom setting
     Map<String, Product_Country_Code__c> mapCountry = Product_Country_Code__c.getAll();
     List<Product_Country_Code__c> lstProductCCode = Product_Country_Code__c.getall().values();

     //list of global products
     List<string> globalProductlist = new list<string>();
     //list of LocalTA ids
     List<string>localtherapeuticid= new list<String>();

     //maps ExteralId and Ids for the local TA and global
      Map<string,Product_vod__c> mapLocalTheraupetic = new map<string,Product_vod__c>();
     Map<string,Product_vod__c> mapGlobalTheraupetic = new map<string,Product_vod__c>();
     //maps externlId and vexternalIds for the local TA
     Map<string,string> mapVexternalID = new map<string,string>();
      Map<string,string> mapGlobalVexternalID = new map<string,string>();
     //maps external Id with name for local TA
     Map<string,string> mapParentPrdExtID = new map<string,string>();
      Map<string,string> mapParentPrdID = new map<string,string>(); //Suchitra
     //Stores the current external id for TA we are working with.
     String TAexternalID = '';
     String externalIDGlobal = '';
     //stores the created TA - to avoid multiples map externalid - number
     Map<string,string> mapLocalTheraupeticNew = new map<string,string>();
      Map<string,string> mapGlobalTheraupeticNew = new map<string,string>();
        String mapParentExternalID; //Added by for CR
      
         Map<string,string> mapIdExternalstring  = new map<string,string>(); //Added by Suchitra for CR
     //Assign value of param to its property inherited from the parent
     this.iResult=iResult;

   /*******************************************************************************************
    1. Extract all externalIds and populate the list of local TA exteranalids (one per country)
    and one as global product
   *******************************************************************************************/
     for(integer i=0;i<iResult.listMessage.size();i++)
     {
         //Data from SAP
         Map<String,String> mapRecord =iResult.listMapRecord.get(i);

       if(!mapRecord.get('external_id_vod__c').isNumeric() && mapRecord.get('description_vod__c')!=null) //ensure it is a TA
       {
          for(Product_Country_Code__c obj :lstProductCCode)
          {
            TAexternalID = obj.Country_Code__c +'_' + mapRecord.get('external_id_vod__c');
            //Add into the list of TA
             localtherapeuticid.add(TAexternalID);
          }
          globalProductlist.add(mapRecord.get('external_id_vod__c'));
       }//end if

   }//end first for which checks the iResult
     system.debug('===> BI_MDM_Products_TA - doMap - 1. localTAs: ' + localtherapeuticid);


   /*******************************************************************************************
    2. If there are TA to process get the sf Id, vexternalId and parentexternalId.
   *******************************************************************************************/
    if(localtherapeuticid!=null && localtherapeuticid.size()>0)
    {
        for(Product_vod__c provod:[select id,external_id_vod__c,VExternal_Id_vod__c,parent_product_vod__c,Description_vod__c, name from Product_vod__c  where external_id_vod__c in:localtherapeuticid])
        {
            mapLocalTheraupetic.put(provod.external_id_vod__c,provod);
            mapVexternalID.put(provod.external_id_vod__c,provod.VExternal_Id_vod__c);
            mapParentPrdExtID.put(provod.external_id_vod__c,provod.name);
             mapParentPrdID.put(provod.external_id_vod__c,provod.parent_product_vod__c);
             system.debug('===> ParentProductId: ' + provod.parent_product_vod__c);
        }
    }
    system.debug('===> BI_MDM_Products_TA - doMap - 2. mapLocalTheraupetic: ' + mapLocalTheraupetic + '<br> mapVexternalID' + mapVexternalID + '<br> mapParentPrdExtID' + mapParentPrdExtID + '<br> mapParentPrdID' + mapParentPrdID);
    
    /***************************************************************************
  If there are GLOBAL to process get the sf Id, vexternalId and parentexternalId.        
    ****************************************************************************/
  if(globalProductlist!=null && globalProductlist.size()>0)
    {
        list<Product_vod__c> listProductIds = [select id,external_id_vod__c,VExternal_Id_vod__c,parent_product_vod__c,description_vod__c, name from Product_vod__c where external_id_vod__c in:globalProductlist]; //Shailesh : Put it outsde the for loop
        if(listProductIds!=null && listProductIds.size()>0)
        {
           for(Product_vod__c prods:listProductIds)
            {
              // mapIdExternalstring.put(prods.external_id_vod__c,prods.id);
                mapGlobalTheraupetic.put(prods.external_id_vod__c,prods);
                mapGlobalVexternalID.put(prods.external_id_vod__c,prods.VExternal_Id_vod__c);
            }
        }
    }
      //Fetch Org Name
      string name = UserInfo.getUserName().substringAfterLast('.');
      Orgname = ' of TA in Org:' + name;
                                   
      /*******************************************************************************************
    Does the actual mapping for Global products
    *******************************************************************************************/
      //Added for global products--Start
    for(integer i=0; i<iResult.listMessage.size();i++){
      //current product
      Product_vod__c globalProd;
      Map<String,String> mapRecord =iResult.listMapRecord.get(i);

      if(!mapRecord.get('external_id_vod__c').isNumeric() && mapRecord.get('description_vod__c')!=null) //ensure we have required data
      {        
        externalIDGlobal = mapRecord.get('external_id_vod__c');
    if(mapGlobalTheraupetic!=null && mapGlobalTheraupetic.keyset()!=null && mapGlobalTheraupetic.keyset().contains(externalIDGlobal )){
            isGlobalExisting=true;
            //get global product data
            globalProd = mapGlobalTheraupetic.get(externalIDGlobal);                
    }
        else if(mapGlobalTheraupeticNew==null ||!mapGlobalTheraupeticNew.keyset().contains(externalIDGlobal)){
      //If mapLocalTheraupeticNew is empty or doesnot contain the TAexternalID
            isGlobalExisting=false;
            globalProd = new Product_vod__c();
            globalProd.put('external_id_vod__c',externalIDGlobal );
            //store the newly created TA in the map
            mapGlobalTheraupeticNew.put( externalIDGlobal,'1');
        }
    
    if(isGlobalExisting)
    {
            //set basic email
            //asign name & update email
            if(mapRecord.get('description_vod__c')!=null && (mapRecord.get('description_vod__c').left(80)!=globalProd.Name || globalProd.Name == null || globalProd.Name == ''))
            {       
                isGlobalChanged=true;
                strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Old Product Name : ' + globalProd.Name;
                globalProd.put('Name', mapRecord.get('description_vod__c').left(80));
                strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Country: GLOBAL';
                strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'MDM ID/external ID: '+ externalIDGlobal;
                 UpdProdToSendEmail = UpdProdToSendEmail + '<br>'+ 'Product Name: ' + (mapRecord.get('description_vod__c').left(80)); 
            }
            else
            {
             strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'Product Name: ' +(mapRecord.get('description_vod__c').left(80));
             strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Country: GLOBAL';
             strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'MDM ID/external ID: '+ externalIDGlobal;
            }
             system.debug('==> isGlobal product1 ' + externalIDGlobal);
             
              
           // strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Veeva ID: ';
            if(mapGlobalTheraupetic.get(externalIDGlobal )!=NULL)
             //  strMessageToSendEmail = strMessageToSendEmail + mapGlobalTheraupetic.get(externalIDGlobal );
           // strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'V External ID: ' ;
      if(mapGlobalVexternalID.get( mapRecord.get('external_id_vod__c'))!=null)
        //strMessageToSendEmail = strMessageToSendEmail + mapGlobalVexternalID.get( mapRecord.get('external_id_vod__c'));

        system.debug('==> isGlobal product2 ' + mapRecord.get('description_vod__c'));
             system.debug('==> isGlobal product3 ' + globalProd.description_vod__c);
             
      //if(mapRecord.get('description_vod__c') !=null && mapRecord.get('description_vod__c') !=globalProd.description_vod__c) 
      if( mapRecord.get('description_vod__c') !=null && ((mapRecord.get('description_vod__c') !=globalProd.description_vod__c) || globalProd.description_vod__c ==null || globalProd.description_vod__c ==''))
      {
        isGlobalChanged=true;
       //strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Old Product Description : ' + globalProd.description_vod__c +'<br><br>' + 'Following field(s) have changed: ' + '<br>' + 'Product Description: ' +(mapRecord.get('description_vod__c'));
       UpdProdToSendEmail = UpdProdToSendEmail + '<br>'+ 'Product Description: ' + mapRecord.get('description_vod__c'); 
        globalProd.put('description_vod__c', mapRecord.get('description_vod__c'));
      }
       if(isGlobalChanged)
       strMessageToSendEmail = strMessageToSendEmail + '<br><br>' + 'Following Field(s) have changed:' +'<br>'+ UpdProdToSendEmail;
       
       UpdProdToSendEmail = '';
    }
        else{ //is new Global - map and update email
              //asign country code
            globalProd.put('Country_Code_BI__c', 'GLOBAL');
              //asign name
            if(mapRecord.get('description_vod__c')!=null){
        globalProd.put('Name', mapRecord.get('description_vod__c').left(80));
                globalProd.put('description_vod__c', mapRecord.get('description_vod__c'));
            }
            globalProd.put('Product_Type_vod__c', 'Detail');

            //set basic Email
            strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Product Name: '+ (mapRecord.get('description_vod__c').left(80));
            strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Country: GLOBAL';
            strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'MDM ID/external ID: '+ externalIDGlobal;
            strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Description: ' +(mapRecord.get('description_vod__c'));
        }
    if(!isGlobalExisting || isGlobalChanged){
      lp.add(globalProd);
            String msgId = iResult.listMessage.get(i).Id;
            //emailTexts.put(msgId,strMessageToSendEmail);
      emailTexts.put(externalIDGlobal,strMessageToSendEmail);
            listOfTAExternalID.add(externalIDGlobal); 
            //add message present to mapped contact for update its status Failed/Compled
            System.debug('iResult.listMessage.get(i)>>>>'+iResult.listMessage.get(i));
            messagesToProcess.put(iResult.listMessage.get(i).Id,iResult.listMessage.get(i));
            System.debug('messagesToProcess>>>'+messagesToProcess);
        }
    else{
      messagesToCancel.put(iResult.listMessage.get(i).Id,iResult.listMessage.get(i));
        }
        Database.UpsertResult[] results=Database.upsert(lp,false);
        lp.clear();
        //reset global vars
        isGlobalExisting=false;
        isGlobalChanged=false;
        strMessageToSendEmail='';
      }
    }
   //Added for global products--End 
     
 
    /***************************************************************************      
  Query the Parent Id and assign to the Products
    ****************************************************************************/
        list<Product_vod__c> listProductIds = [select id,external_id_vod__c from Product_vod__c where external_id_vod__c in:globalProductlist]; //Shailesh : Put it outsde the for loop
        if(listProductIds!=null && listProductIds.size()>0)
        {
           for(Product_vod__c prods:listProductIds)
            {
               mapIdExternalstring.put(prods.external_id_vod__c,prods.id);
               // mapParentExternalID = prods.id;
            }
            System.debug('mapParentExternalID>>>>'+mapParentExternalID);
            for(Integer i=0;i<localtherapeuticid.size();i++){
               mapIdExternalstring.put(localtherapeuticid.get(i),mapParentExternalID);
            }
            System.debug('mapIdExternalstring>>>>'+mapIdExternalstring);
        }
      
    /*******************************************************************************************
    3. Second loop. Does the actual mapping
    *******************************************************************************************/
    for(integer i=0; i<iResult.listMessage.size();i++){
      //current product
      Product_vod__c Prod;
      Map<String,String> mapRecord =iResult.listMapRecord.get(i);

      if(!mapRecord.get('external_id_vod__c').isNumeric() && mapRecord.get('description_vod__c')!=null) //ensure we have required data
      {
        //for each country
        for(Product_Country_Code__c obj :lstProductCCode)
        {
            TAexternalID = obj.Country_Code__c +'_' + mapRecord.get('external_id_vod__c');
            //check if exists
            if(mapLocalTheraupetic!=null && mapLocalTheraupetic.keyset()!=null && mapLocalTheraupetic.keyset().contains(TAexternalID ))
            {
                isExisting=true;
                //get product data
                Prod = mapLocalTheraupetic.get(TAexternalID);
                // [SELECT id,name,description_vod__c,manufacturer_vod__c,therapeutic_area_vod__c,Country_Code_BI__c,Parent_Product_vod__c from Product_vod__c where id = :mapLocalTheraupetic.get(TAexternalID) LIMIT 1];
            }
            else if(mapLocalTheraupeticNew==null ||!mapLocalTheraupeticNew.keyset().contains(TAexternalID))
            {//If mapLocalTheraupeticNew is empty or doesnot contain the TAexternalID
                isExisting=false;
                Prod = new Product_vod__c();
                Prod.put('external_id_vod__c',TAexternalID );
                //store the newly created TA in the map
                 mapLocalTheraupeticNew.put( TAexternalID,'1');
            }

            //if is an existing TA
            if(isExisting)
            {
              //set basic email
              //asign name & update email
              if(mapRecord.get('description_vod__c')!=null && (mapRecord.get('description_vod__c').left(80)!=Prod.Name || Prod.Name == null || Prod.Name == ''))
              {
                Prod.put('Name', mapRecord.get('description_vod__c').left(80));
                isChanged=true;
                strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'Old Product Name : ' + Prod.Name;
                strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'Country: ' + obj.Country_Code__c;
                strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'MDM ID/external ID: '+ TAexternalID;
                UpdProdToSendEmail    = UpdProdToSendEmail + 'Product Name: ' +(mapRecord.get('description_vod__c').left(80));
                
              }
              else
              {
                strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'Product Name: ' + Prod.Name;
                strMessageToSendEmail = strMessageToSendEmail +  '<br>' + 'Country: ' + obj.Country_Code__c;
                strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'MDM ID/external ID: '+ TAexternalID;
              }
              
               //strMessageToSendEmail = strMessageToSendEmail + '<br>'+ 'Veeva ID: ';
               if(mapLocalTheraupetic.get(TAexternalID )!=NULL)
               //  strMessageToSendEmail = strMessageToSendEmail + mapLocalTheraupetic.get(TAexternalID );
              // strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'V External ID: ' ;
               if(mapVexternalID.get( mapRecord.get('external_id_vod__c'))!=null)
               //  strMessageToSendEmail = strMessageToSendEmail + mapVexternalID.get( mapRecord.get('external_id_vod__c'));

              //asign name & update email
              /*if(mapRecord.get('description_vod__c')!=null && (mapRecord.get('description_vod__c').left(80)!=Prod.Name || Prod.Name == null || Prod.Name == ''))
              {
                Prod.put('Name', mapRecord.get('description_vod__c').left(80));
                isChanged=true;
                strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Old Product Name : ' + Prod.Name +'<br><br> Following field(s) have changed:  <br>' + 'Product Name: ' +(mapRecord.get('description_vod__c').left(80));
              }*/
              //asign description & update email
              system.debug('==> islocal product1 ' + mapRecord.get('description_vod__c'));
             system.debug('==> islocal product2 ' + Prod.description_vod__c);
             
             
             if( mapRecord.get('description_vod__c')!=null && ((mapRecord.get('description_vod__c') !=Prod.description_vod__c) || Prod.description_vod__c ==null || Prod.description_vod__c ==''))
             {
                Prod.put('description_vod__c', mapRecord.get('description_vod__c'));
                isChanged=true;
               // strMessageToSendEmail = strMessageToSendEmail + '<br> Old Product Description : ' + Prod.description_vod__c +'<br><br> Following field(s) have changed:  <br>' + 'Product Description: ' +(mapRecord.get('description_vod__c'));
                  UpdProdToSendEmail = UpdProdToSendEmail +  '<br>'+ 'Product Description: ' +(mapRecord.get('description_vod__c'));

              }
              system.debug('==> isParent product1 ' + mapLocalTheraupetic.get(TAexternalID));
              system.debug('==> isParent product2 ' + Prod.Parent_Product_vod__c );
               system.debug('==> isParent product3 ' + TAexternalID);
                system.debug('==> isParent product4 ' + mapParentPrdID.get(TAexternalID));
                system.debug('==> isParent product5 ' + mapParentExternalID);
                   system.debug('===> isParent product6: ' + mapIdExternalstring.get(mapRecord.get('external_id_vod__c')));
              
              //to update parent product id in Local TA if it is null. added for CR by suchitra
               //if(mapIdExternalstring!=null && mapIdExternalstring.keyset()!=null && mapIdExternalstring.keyset().contains(TAexternalID) && (mapParentPrdID.get(TAexternalID) == null || mapParentPrdID.get(TAexternalID) !=mapParentExternalID))
               if(mapIdExternalstring!=null && mapIdExternalstring.keyset()!=null && (mapParentPrdID.get(TAexternalID) ==null || mapParentPrdID.get(TAexternalID) !=mapIdExternalstring.get(mapRecord.get('external_id_vod__c'))))
                  {
                      //strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Parent Veeva ID: '+mapIdExternalstring.get(TAexternalID);
                       UpdProdToSendEmail = UpdProdToSendEmail +  '<br>'+'Parent Veeva ID: '+ mapIdExternalstring.get(mapRecord.get('external_id_vod__c'));
                      //strMessageToSendEmail = strMessageToSendEmail + '<br>' +'Parent External ID: '+TAexternalID;
                     // strMessageToSendEmail = strMessageToSendEmail + '<br>' +'Parent External ID: '+mapRecord.get('external_id_vod__c');
                       UpdProdToSendEmail = UpdProdToSendEmail +  '<br>' + 'Parent External ID: '+mapRecord.get('external_id_vod__c');
                      Prod.put('Parent_Product_vod__c', mapIdExternalstring.get(mapRecord.get('external_id_vod__c')));
                       isChanged=true;
                  }
                  
                  if(isChanged)
                 strMessageToSendEmail = strMessageToSendEmail + '<br><br>' + 'Following Field(s) have changed:' +'<br>'+ UpdProdToSendEmail;
                 
                UpdProdToSendEmail = '';

            }
            else //is new TA - map and update email
            {
              //asign country code
              Prod.put('Country_Code_BI__c', obj.Country_Code__c);
              //asign name
              if(mapRecord.get('description_vod__c')!=null)
              {
                 Prod.put('Name', mapRecord.get('description_vod__c').left(80));
                Prod.put('description_vod__c', mapRecord.get('description_vod__c'));
              }
              Prod.put('Product_Type_vod__c', 'Detail');

              //set basic Email
              strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Product Name: '+ (mapRecord.get('description_vod__c').left(80));
              strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Country: ' + obj.Country_Code__c;
              strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'MDM ID/external ID: '+ TAexternalID;
              //strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Product Name: '+ (mapRecord.get('description_vod__c').left(80));
              strMessageToSendEmail = strMessageToSendEmail + '<br>' + 'Description: ' +(mapRecord.get('description_vod__c'));
            }
                  //Uncommented by Suchitra for CR
                 // if(mapIdExternalstring!=null && mapIdExternalstring.keyset()!=null && mapIdExternalstring.keyset().contains(TAexternalID) && !isExisting)
                  if(mapIdExternalstring!=null && mapIdExternalstring.keyset()!=null && !isExisting)
                  {
                      strMessageToSendEmail = strMessageToSendEmail + '<br>' +'Parent Veeva ID: '+mapIdExternalstring.get(mapRecord.get('external_id_vod__c'));
                      //strMessageToSendEmail = strMessageToSendEmail + '<br>' +'Parent External ID: '+TAexternalID;
                      strMessageToSendEmail = strMessageToSendEmail + '<br>' +'Parent External ID: '+mapRecord.get('external_id_vod__c');
                      Prod.put('Parent_Product_vod__c', mapIdExternalstring.get(mapRecord.get('external_id_vod__c')));
                  }
                  

                  system.debug('===> BI_MDM_Products_TA - doMap - 3. TA : ' + Prod + ' isChanged ' + isChanged + ' isExisting ' + isExisting);
                  system.debug('===> BI_MDM_Products_TA - doMap - 3. email : ' + strMessageToSendEmail);
                  system.debug('===> BI_MDM_Products_TA - doMap - 3. ParentProductNew1 : ' + mapIdExternalstring.get(mapRecord.get('external_id_vod__c')));

                  //ProdDetail and MailMsg addition to List/Map
                  if(!isExisting || isChanged){
                    //addProduct(iResult.listMessage.get(i).Id,Prod);
                    lp.add(Prod);
                    String msgId = iResult.listMessage.get(i).Id;
                    //emailTexts.put(msgId,strMessageToSendEmail);
          emailTexts.put(TAexternalID,strMessageToSendEmail);
                    listOfTAExternalID.add(TAexternalID); 
                    //add message present to mapped contact for update its status Failed/Compled
                    System.debug('iResult.listMessage.get(i)>>>>'+iResult.listMessage.get(i));
                    messagesToProcess.put(iResult.listMessage.get(i).Id,iResult.listMessage.get(i));
                      System.debug('messagesToProcess>>>'+messagesToProcess);
                  }else{
                    messagesToCancel.put(iResult.listMessage.get(i).Id,iResult.listMessage.get(i));
                  }
                  //reset global vars
                  isExisting=false;
                  isChanged=false;
                  strMessageToSendEmail='';

              }//end for each country
          }//End checking if
        }//end 3.for

}//end doMap

/*
static void addProduct(Id msgId, Product_vod__c p)
{
   try
  {
    system.debug('===> BI_MDM_Products_TA - addProduct - Product: ' + p);

    //1. Adds product to list
     lp.add(p);
     //index of product to upsert
      List<Integer> indexs = mIndex.get(msgId);
      if(indexs==null)indexs=new List<Integer>();
      indexs.add(lp.size()-1);//Start index 0
      mIndex.put(msgId, indexs);
      String MailIndex = msgId + '_' + (lp.size()-1);

      //This is for the email?
      system.debug('===> BI_MDM_Products_TA - addProduct - MailIndex: ' + MailIndex);
      if(!String.isEmpty(strMessageToSendEmail) && strMessageToSendEmail !='')
      {
         MsgToMail.put(MailIndex, strMessageToSendEmail);// DEBUG
         ExsToMail.put(MailIndex, 'true');// DEBUG
      }
  }//End 1st Try loop

   catch(Exception e)
  {
       System.debug('BI_MDM_Products_TA - addProduct --The following exception has occurred: ' + e.getMessage());
  }

}//End for static void addProduct

*/
global override List<skyvvasolutions.IServicesUtil.UpsertResult2> upsert2()
{
//upsert records
 Database.UpsertResult[] results=Database.upsert(lp,false);

  //NOEMI updateSkyvvaMessage(iResult, results);
  System.debug('Calling BI_MDM_Products_Utilities.updateSkyvvaMessageToProcess method');
    System.debug('messagesToProcess.values()>>>'+messagesToProcess.values());
    System.debug('results>>'+results);
    System.debug('emailTexts>>>'+emailTexts);
  //BI_MDM_Products_Utilities.updateSkyvvaMessageToProcess(messagesToProcess.values(), results,emailTexts);
  Boolean check = BI_MDM_Products_Utilities.updateSkyvvaMessageDetails(messagesToProcess.values(), results,emailTexts,listOfTAExternalID, Orgname);
  if(!check)
      BI_MDM_Products_Utilities.updateSkyvvaMessageToCancel(messagesToCancel.values(), 'MDM Therapeutic Area: Message Cancelled due to out of scope');
  return null;
}//End for global override List Method


/****************************************************************
 Update message status and comment based on upsert result

 @param: iResult Skyvva message
 @param: results Upsert result of Product
 *****************************************************************/
/*
static void updateSkyvvaMessage(skyvvasolutions.IServicesUtil.IMessageResult iResult, Database.UpsertResult[] results)
 {
     try
     {
     System.debug('>>>BI_MDM_Products_TA updateSkyvvaMessage: skyvva messages:'+iResult.listMessage.size() +'>Upsert Result of Product: '+results.size());

      for(Integer i=0;i<iResult.listMessage.size();i++)
         {
              System.debug('iResult value : ' + iResult.listMessage.size());
            //index of upserted records
             List<Integer> indexs=mIndex.get(iResult.listMessage.get(i).Id);
             if(indexs==null)continue;
             String relatedTo=null;
             Boolean isComplted=true;
             String cmt='';

             iResult.listMessage[i].skyvvasolutions__Status__c = 'Pending';
             iResult.listMessage[i].skyvvasolutions__Modification_Date__c = System.now();
                      System.debug('indexs value : ' + indexs);
             //index of upserted records == index of upsert result
             for(Integer j: indexs)
             {
                 Database.UpsertResult rs=results[i];
                 if(rs.isSuccess())
                 {
                     String MsgToMailRef = iResult.listMessage.get(i).Id + '_' + j;
                     system.debug('===================MsgToMailRef1====================' +MsgToMailRef);
                     String sEmailMsg = MsgToMail.get(MsgToMailRef);
                     if(String.isBlank(relatedTo))relatedTo=rs.getId();
                     if(rs.isCreated())                                                     //modified by Suchitra
                     {
                       cmt+='Creation of Product_vod__c:'+rs.getId()+',';
                        system.debug('===================SendEmailOnCreation1====================' +sEmailMsg);
                     }
                     else
                     {                                                                       //modified by Suchitra
                       cmt+='Modification of Product_vod__c:'+rs.getId()+',';
                       system.debug('===================SendEmailOnUpdation1====================' +sEmailMsg);
                     }
                      if((!String.isEmpty(sEmailMsg)))
                      {
                        //NCOTARELO SendEmail.SendEmail(false,sEmailMsg);
                        Messaging.SingleEmailMessage email = SendEmail.CreateEmail(false,sEmailMsg);
                        allEmails.add(email);
                      }
                 }
                 else
                 {
                      system.debug('==> failed ' );
                     isComplted=true;
                     if(String.isNotBlank(cmt))cmt=',';
                     for( Database.Error err:rs.getErrors())
                     {
                         cmt+='Failed' + err.getMessage()+',';
                     }
                     iResult.listMessage[i].skyvvasolutions__Status__c = 'Failed'; //new

                 }

             }//end for loop of indexes

              //Complete or Failed?
                iResult.listMessage[i].skyvvasolutions__Status__c = (isComplted? 'Completed': 'Failed');
                if(String.isNotBlank(cmt))
                 {
                     if(cmt.length()>255)iResult.listMessage[i].skyvvasolutions__Comment__c=cmt.substring(0,255);
                     else iResult.listMessage[i].skyvvasolutions__Comment__c=cmt.substring(0,(cmt.length()-1));//remove last
                 }
             if(isComplted)iResult.listMessage[i].skyvvasolutions__Related_To__c=relatedTo;
         }//end for loop of messages

   system.debug('==> send All emails : ' + allEmails);
   SendEmail.sendAllEmails(allEmails);

   update iResult.listMessage;
   System.debug('========updateSkyvvaMessage:updated message:Done=====');
     }//End 3rd Try loop
     catch(Exception e)
     {
          System.debug('The following exception has occurred while updating message: ' + e.getMessage());
     }
 }//End of Update Message Status

*/

}