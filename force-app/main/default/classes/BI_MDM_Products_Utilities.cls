public class BI_MDM_Products_Utilities {

    //Full list of messages to be sent
    static List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();


/*************************************************************************************/
/**                                     EMAILING UTILS                                                                                               */
/*************************************************************************************/

public static  Messaging.SingleEmailMessage CreateEmail(Boolean isCreated,String Message, String OrgName)
{
     // Step 1: Create a new Email
     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
     // Step 2: Set list of people who should get the email
       List<String> sendTo = new List<String>();

     //Recipients list from custom setting
      Map<String, MDM_Product_Recipients__c> mapRecipients= MDM_Product_Recipients__c.getAll();
     //loop through all values in recipients setting and add them to the sendTo List
       for (MDM_Product_Recipients__c rec:mapRecipients.values()){
           sendTo.add(rec.Recipients__c);
   }

    mail.setToAddresses(sendTo);

    // Step 3: Set who the email is sent from
     mail.setReplyTo('noreply@salesforce.com');
     mail.setSenderDisplayName('noreply@salesforce.com');

 //product was created
 if(isCreated == true)
  {
    mail.setSubject('Product MDM:Product Creation Notification' + OrgName);
    String body = '<br>\r\n The following Product has been created in Veeva CRM from Product MDM:</br>';
    body  +=  Message;
    mail.setHtmlBody(body);
  }else{
    mail.setSubject('Product MDM:Product Update Notification' + OrgName);
    String body = '\r\n The following Product has been updated  in Veeva CRM from Product MDM:';
    body +='<br> </br>'+ Message + '\n';
    mail.setHtmlBody(body);
    }

    system.debug('==> Create Email ' + isCreated + ' mail ' + mail);
  return mail;
}

public static void sendAllEmails(List<Messaging.SingleEmailMessage> allEmails)
{
    Messaging.SendEmail(allEmails);
}


/*************************************************************************************/
/**                                     SKYVVA MESSAGING UTILS                                                                               */
/*************************************************************************************/

public static void updateSkyvvaMessageToCancel(List<skyvvasolutions__IMessage__c>messagesToCancel, String comment){
    System.debug('Called updateSkyvvaMessageToCancel method');  
    for(Integer i=0; i<messagesToCancel.size();i++){
            messagesToCancel[i].skyvvasolutions__Status__c = 'Cancelled';
            messagesToCancel[i].skyvvasolutions__Modification_Date__c = System.now();
            messagesToCancel[i].skyvvasolutions__Comment__c = comment;
        }
        system.debug('==> updateSkyvvaMessageToCancel: ' + messagesToCancel + ' comment '+ comment);

        update messagesToCancel;
}


 public static void updateSkyvvaMessageToProcess( List<skyvvasolutions__IMessage__c> messagesToProcess, Database.UpsertResult[] results,Map<string,string> emailTexts, String OrgName)
 {
     System.debug('Called updateSkyvvaMessageToProcess method');

     try{
         String msgComment = '';
         Messaging.SingleEmailMessage email;
      //Assume that results.size=listMessage.size
         for(Integer i=0;i<results.size();i++){
            msgComment = '';
            Database.UpsertResult rs=results[i];
System.Debug('Result>>'+rs);
            messagesToProcess[i].skyvvasolutions__Status__c = 'Pending';
            messagesToProcess[i].skyvvasolutions__Modification_Date__c = System.now();
System.debug('checking whether record is success or not');
            if(rs.isSuccess()){
System.debug('record is success');
                messagesToProcess[i].skyvvasolutions__Status__c = 'Completed';
                messagesToProcess[i].skyvvasolutions__Related_To__c = rs.getId();
System.debug('checking whetehr record is created or not');
                //Recrod is create
                if(rs.isCreated()) {
                    System.debug('record is created');
                    if(String.isBlank(msgComment))
                        msgComment+='Creation of Product_vod__c:'+rs.getId();
                    else
                        msgComment+=' , Creation of Product_vod__c:'+rs.getId();
                    System.debug('msgComment>>>>'+msgComment);
                    System.debug('Calling createEmail method');
                    email = CreateEmail(true,emailTexts.get(messagesToProcess[i].Id), OrgName);
                    System.debug('Called createEmail method');
                }
                else {
                  if(String.isBlank(msgComment))
                        msgComment+='Update of Product_vod__c:'+rs.getId();
                    else
                        msgComment+=' , Update of Product_vod__c:'+rs.getId();

                 email = CreateEmail(false,emailTexts.get(messagesToProcess[i].Id), OrgName);
                }
                messagesToProcess[i].skyvvasolutions__Comment__c = msgComment;
                mails.add(email);
                messagesToProcess[i].skyvvasolutions__Retry_Counter__c  = messagesToProcess[i].skyvvasolutions__Retry_Counter__c + 1;
            }
            else{
                messagesToProcess[i].skyvvasolutions__Status__c = 'Failed';
                String errDsc='';
                for( Database.Error err:rs.getErrors()){
                    errDsc+=err.getMessage()+' ';
                }
                messagesToProcess[i].skyvvasolutions__Comment__c =errDsc;
                messagesToProcess[i].skyvvasolutions__Retry_Counter__c  = messagesToProcess[i].skyvvasolutions__Retry_Counter__c + 1;
            }

         }//end for
         System.debug('Sending e-mail now');
         System.debug('mails>>'+mails);
     sendAllEmails(mails);
         System.debug('After sending e-mail>>>mails>>'+mails);
         System.debug('Sent e-mail successfully');
     system.debug('==> updateSkyvvaMessageToProcess: ' + messagesToProcess );
    update messagesToProcess;

    }
    catch(Exception e)
    {
     System.debug('==>The following exception has occurred while processing: ' + e.getMessage());
    }

 }
    
    //Added for triggerring e-mail for TA--Start---By Saurav
    public static boolean updateSkyvvaMessageDetails( List<skyvvasolutions__IMessage__c> messagesToProcess, Database.UpsertResult[] results,Map<string,string> emailTexts, List<String> listTAExternalId, String OrgName)
 {
     System.debug('Called updateSkyvvaMessageToProcess method');
    Boolean returnValue = false;
     try{
         Boolean check = true;
         String msgComment = '';
         Messaging.SingleEmailMessage email;
      //Assume that results.size=listMessage.size
         for(Integer i=0;i<results.size();i++){
         if (i >= messagesToProcess.size())
            check = false;
            msgComment = '';
            Database.UpsertResult rs=results[i];
System.Debug('Result>>'+rs);
System.Debug('check>>'+check);
             if(check){
                messagesToProcess[i].skyvvasolutions__Status__c = 'Pending';
                messagesToProcess[i].skyvvasolutions__Modification_Date__c = System.now();
             }
System.debug('checking whether record is success or not');
            if(rs.isSuccess()){
System.debug('record is success');
                if(check){
                    messagesToProcess[i].skyvvasolutions__Status__c = 'Completed';
                    messagesToProcess[i].skyvvasolutions__Related_To__c = rs.getId();
                }
System.debug('checking whetehr record is created or not');
                //Recrod is create
                if(rs.isCreated()) {
                    System.debug('record is created');
                    if(String.isBlank(msgComment))
                        msgComment+='Creation of Product_vod__c:'+rs.getId();
                    else
                        msgComment+=' , Creation of Product_vod__c:'+rs.getId();
                    System.debug('msgComment>>>>'+msgComment);
                    System.debug('Calling createEmail method');
                    //email = CreateEmail(true,emailTexts.get(messagesToProcess[i].Id), OrgName);
                    email = CreateEmail(true,emailTexts.get(listTAExternalId[i]), OrgName);
                    System.debug('Called createEmail method');
                }
                else {
                  if(String.isBlank(msgComment))
                        msgComment+='Update of Product_vod__c:'+rs.getId();
                    else
                        msgComment+=' , Update of Product_vod__c:'+rs.getId();

                 //email = CreateEmail(false,emailTexts.get(messagesToProcess[i].Id));
                 email = CreateEmail(false,emailTexts.get(listTAExternalId[i]), OrgName);
                }
                if(check)
                    messagesToProcess[i].skyvvasolutions__Comment__c = msgComment;
                mails.add(email);
                if(check)
                    messagesToProcess[i].skyvvasolutions__Retry_Counter__c  = messagesToProcess[i].skyvvasolutions__Retry_Counter__c + 1;
                    returnValue = true;
            }
            else{
                if(check)
                    messagesToProcess[i].skyvvasolutions__Status__c = 'Failed';
                String errDsc='';
                for( Database.Error err:rs.getErrors()){
                    errDsc+=err.getMessage()+' ';
                }
                if(check){
                    messagesToProcess[i].skyvvasolutions__Comment__c =errDsc;
                    messagesToProcess[i].skyvvasolutions__Retry_Counter__c  = messagesToProcess[i].skyvvasolutions__Retry_Counter__c + 1;
                }
                returnValue = true;
            }
            
            
         }//end for
         System.debug('Sending e-mail now');
         System.debug('mails>>'+mails);
     sendAllEmails(mails);
         System.debug('After sending e-mail>>>mails>>'+mails);
         System.debug('Sent e-mail successfully');
     system.debug('==> updateSkyvvaMessageToProcess: ' + messagesToProcess );
    update messagesToProcess;
   
    }
    catch(Exception e)
    {
     System.debug('==>The following exception has occurred while processing: ' + e.getMessage());
    }
    return returnValue;
 }
//Added for triggerring e-mail for TA--End---By Saurav
 /**
 * This method will return Yes if the value is true and NO otherwise.
 * Needed due to values sent from SAP to Veeva not matching the standard data in Salesforce.
 */
 public static String getStringValue(Boolean field){
         //Yes o NO

         String ret = '';
         if(field==true){
             ret = 'Yes';
         }
         else {
             ret = 'No';
         }
             system.debug('==> getStringValue  ' + field + ' string ' + ret);
     return ret;

 }

 public static Boolean getBooleanValue(String field){
     boolean ret = false;
     if(field == null){
         ret = false;
     }else if(field.toLowerCase().compareTo('yes')==0){//it is yes, should return true
         ret = true;
     }
     system.debug('==> getBooleanValue  ' + field + ' boolean ' + ret);
     return ret;
 }


 }