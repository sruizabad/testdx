/* 
Name: BI_MM_BatchToUpdateBudgetOwnerId 
Requirement ID: Colloboration
Description: Batch to update ownerId in Budget records as per territory 
Version | Author-Email | Date | Comment 
1.0 | Mukesh Tiwari | 09.02.2016 | initial version 
*/
global without sharing class BI_MM_BatchToUpdateBudgetOwnerId implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        /* Get all the budgets */
        String strQuery = 'SELECT Id, Name, BI_MM_TerritoryName__c, BI_MM_TerritoryName__r.BI_MM_TerritoryId__c, BI_MM_ManagerTerritory__c, '+ 
                                 'BI_MM_ManagerTerritory__r.BI_MM_TerritoryId__c,BI_MM_ManagerName__c,BI_MM_UserName__c,BI_MM_UserName__r.manager.id FROM BI_MM_Budget__c LIMIT 50000';
        
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<BI_MM_Budget__c> lstBudget) {
         
        List<BI_MM_Budget__c> lstBudgetToUpdateOwnerForUserTerritory = new List<BI_MM_Budget__c>();
        List<BI_MM_Budget__c> lstBudgetToUpdateOwnerForMgrTerritory = new List<BI_MM_Budget__c>();
        
        Map<Id, Id> mapTerritoryIdToUserId = new Map<Id, Id>();
        
        Map<id,id> idToUserManagerIdMap = new Map<id,id>();
        
        Set<Id> setTerritoryIds = new Set<Id>();
        Set<Id> setMgrTerritoryIds = new Set<Id>();
        
        /* Iterate over the list of existing budget */          
        for(BI_MM_Budget__c objBudget : lstBudget) {
            
            if(null != objBudget.BI_MM_UserName__c && null != objBudget.BI_MM_UserName__r.manager.id)
            idToUserManagerIdMap.put(objBudget.id,objBudget.BI_MM_UserName__r.manager.id);
            
            if(objBudget.BI_MM_TerritoryName__c != null) {
                
                setTerritoryIds.add(objBudget.BI_MM_TerritoryName__r.BI_MM_TerritoryId__c);
            }
            
            if(objBudget.BI_MM_TerritoryName__c == null) {
                
                setMgrTerritoryIds.add(objBudget.BI_MM_ManagerTerritory__r.BI_MM_TerritoryId__c);
            }
        }
        
        List<UserTerritory> lstUserTerritory = [SELECT UserId, TerritoryId FROM UserTerritory WHERE TerritoryId IN: setTerritoryIds OR TerritoryId IN : setMgrTerritoryIds];
        
        
        /* Get user with respective to each territory */    
        if(lstUserTerritory != null)
        {
            for(UserTerritory objUserTerritory : lstUserTerritory) {
                
                mapTerritoryIdToUserId.put(objUserTerritory.TerritoryId, objUserTerritory.UserId);
                
                if(setMgrTerritoryIds.contains(objUserTerritory.TerritoryId)) {
                    
                   setMgrTerritoryIds.remove(objUserTerritory.TerritoryId); 
                }
            }
        }
        
        list<user> userIdList = [Select id,Manager.id from user where id in: mapTerritoryIdToUserId.values() ];
        /* Map<id,id> userToManagerIdMap = new Map<id,id>();
        
        for(user u : userIdList) {
            userToManagerIdMap.put(u.id,u.manager.id);
        }
        */
        
        /* Iterate over the list of existing budget and map OwnerId */ 
        for(BI_MM_Budget__c objBudget : lstBudget) {
            
            if(objBudget.BI_MM_TerritoryName__c != null && mapTerritoryIdToUserId.containsKey(objBudget.BI_MM_TerritoryName__r.BI_MM_TerritoryId__c)) {
                
                lstBudgetToUpdateOwnerForUserTerritory.add(new BI_MM_Budget__c(Id = objBudget.Id, BI_MM_ManagerName__c = idToUserManagerIdMap.containsKey(objBudget.Id)? idToUserManagerIdMap.get(objBudget.id) : null , OwnerId = mapTerritoryIdToUserId.get(objBudget.BI_MM_TerritoryName__r.BI_MM_TerritoryId__c)));
            }
            
            if(objBudget.BI_MM_TerritoryName__c == null && mapTerritoryIdToUserId.containsKey(objBudget.BI_MM_ManagerTerritory__r.BI_MM_TerritoryId__c)) {
                
                lstBudgetToUpdateOwnerForMgrTerritory.add(new BI_MM_Budget__c(Id = objBudget.Id,/* BI_MM_ManagerName__c=mapTerritoryIdToUserId.get(objBudget.BI_MM_ManagerTerritory__r.BI_MM_TerritoryId__c),*/ OwnerId = mapTerritoryIdToUserId.get(objBudget.BI_MM_ManagerTerritory__r.BI_MM_TerritoryId__c)));
            }
        }
        
        // Update Owner Id with User Territory User
        if(!lstBudgetToUpdateOwnerForUserTerritory.isEmpty())
            Database.update(lstBudgetToUpdateOwnerForUserTerritory,false);
        
        // Update Owner Id with Manager Territory User
        if(!lstBudgetToUpdateOwnerForMgrTerritory.isEmpty())
            Database.update(lstBudgetToUpdateOwnerForMgrTerritory,false);
    }   
    
    global void finish(Database.BatchableContext BC) {
        
        /* Do nothing */
    }
}