/* 
Name: BI_MM_BatchToUpdateBudgetOwnerIdTest 
Requirement ID: BudgetUpload
Description: Test class for BI_MM_BatchToUpdateBudgetOwnerId 
Version | Author-Email | Date | Comment 
1.0 | Mukesh Tiwari | 07.12.2016 | initial version
*/
@isTest
private class BI_MM_BatchToUpdateBudgetOwnerIdTest{
    
     private static testMethod void testBI_MM_BatchToUpdateBudgetOwnerIdTest() {
        BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest(); 

        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];  // Get BI_MM_ADMIN permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and  null Manager
        User testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, null);
        dataFactory.addPermissionSet(testUser, permissionAdmin);

        // Insert new Territories
        Territory territory = dataFactory.generateTerritory('Test Territory 1', null);

        System.runAs(testUser) {
            // Insert custom setting
            BI_MM_CountryCode__c countryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c);            
          
            // Insert Mirror Territory
            BI_MM_MirrorTerritory__c objMirrorTerritory = dataFactory.generateMirrorTerritory ('sr01Test', territory.Id);                        
            
            // Insert Product Catelog
            Product_vod__c objProduct = dataFactory.generateProduct('Test Product', 'Detail', null);  
            Product_vod__c objProduct1 = dataFactory.generateProduct('Test Product1', 'Detail', null); 
            Product_vod__c objProduct2 = dataFactory.generateProduct('Test Product2', 'Detail', null); 

            List<Product_vod__c> lstProduct = new List<Product_vod__c>{objProduct, objProduct1, objProduct2};                        
                                    
            Test.startTest();
                // Insert spain Budget 
                BI_MM_Budget__c objBudget  = dataFactory.generateBudgets(1, objMirrorTerritory.Id, objMirrorTerritory.Id, 'Micro Marketing', lstProduct[0], 4000, true).get(0);            
                
                                                                 
                BI_MM_Budget__c objBudget1  = dataFactory.generateBudgets(1, objMirrorTerritory.Id, objMirrorTerritory.Id, 'Micro Marketing', lstProduct[1], 4000, true).get(0);                        
                
                BI_MM_Budget__c objBud  = dataFactory.generateBudgets(1, null, objMirrorTerritory.Id, 'Micro Marketing', lstProduct[2], 4000, true).get(0);            
                     
                Id batchID;
           
                BI_MM_BatchToUpdateBudgetOwnerId c = new BI_MM_BatchToUpdateBudgetOwnerId();
                batchID = Database.executeBatch(c,2000);
            Test.stopTest();
        }
         
     }         
}