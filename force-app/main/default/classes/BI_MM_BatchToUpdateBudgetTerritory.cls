/* 
Name: BI_MM_BatchToUpdateBudgetTerritory 
Requirement ID: BudgetUpload
Description: Batch to get all the territo into newly created custom object i.e BI_MM_MirrorTerritory__c
Version | Author-Email | Date | Comment 
1.0 | Mukesh Tiwari | 07.12.2016 | initial version 
*/
global without sharing class BI_MM_BatchToUpdateBudgetTerritory implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        /* Get all the Territory */
        String strQuery = 'SELECT Id, Name FROM Territory LIMIT 50000';
        
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Territory> lstStandardTerritory) {
         
         List<BI_MM_MirrorTerritory__c> lstBadgetTerritoryToInsert = new List<BI_MM_MirrorTerritory__c>();
         List<BI_MM_MirrorTerritory__c> lstBadgetTerritoryToUpdate = new List<BI_MM_MirrorTerritory__c>();
         
         List<BI_MM_MirrorTerritory__c> lstBadgetTerritory = [SELECT Id, BI_MM_TerritoryId__c, Name FROM BI_MM_MirrorTerritory__c LIMIT 50000];
         
         Map<String, Id> mapBadgetTerritoryNameToId = new Map<String, Id>();
         
         /* Iterate over the list of existing Mirror territory */         
         for(BI_MM_MirrorTerritory__c objTerritory : lstBadgetTerritory) {
             
             mapBadgetTerritoryNameToId.put(objTerritory.Name, objTerritory.Id);
         }
         
        /* Iterate over the list of standard territory */         
        for(Territory objStandardTerritory : lstStandardTerritory) {
            
            /* Check if with the same name any Territory exist then update it with badget existing territory */ 
            if(mapBadgetTerritoryNameToId.containsKey(objStandardTerritory.Name)) {
                
                lstBadgetTerritoryToUpdate.add(new BI_MM_MirrorTerritory__c(Id = mapBadgetTerritoryNameToId.get(objStandardTerritory.Name), Name = objStandardTerritory.Name, BI_MM_TerritoryId__c = objStandardTerritory.Id)); 
            }
            else {
                
                /* Insert new territoty */ 
                lstBadgetTerritoryToInsert.add(new BI_MM_MirrorTerritory__c(Name = objStandardTerritory.Name, BI_MM_TerritoryId__c = objStandardTerritory.Id));                
            }
         }
        
        if(!lstBadgetTerritoryToInsert.isEmpty()) {
             
             DataBase.insert(lstBadgetTerritoryToInsert, false);
        }
        
        if(!lstBadgetTerritoryToUpdate.isEmpty()) {    
            
            DataBase.update(lstBadgetTerritoryToUpdate, false);
        }
    }   
    
    global void finish(Database.BatchableContext BC) {
        
        /* Do nothing */
    }
}