/* 
Name: BI_MM_BatchToUpdateBadgetTerritoryTest 
Requirement ID: BudgetUpload
Description: Test class for BI_MM_BatchToUpdateBudgetTerritoryTest 
Version | Author-Email | Date | Comment 
1.0 | Mukesh Tiwari | 07.12.2016 | initial version 
*/
@isTest
private class BI_MM_BatchToUpdateBudgetTerritoryTest {
    
    static List<Territory> lstTerritory = new List<Territory>();
    
    /*@isTest(SeeAllData=true)
    static void testWithAllDataAccess() {
        
        lstTerritory = [SELECT Id, Name FROM Territory LIMIT 10];
    }*/
    
    private static testMethod void testBI_MM_BatchToUpdateBudgetTerritoryTest() { 
        BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest(); 
        List<BI_MM_MirrorTerritory__c> lstMirrorTerritories = new List<BI_MM_MirrorTerritory__c>();
        List<Territory> lstTestTerritories = new List<Territory>();
        Integer sizeTerritories = 5;                
        
        Test.startTest();

            System.runAs(new User(Id = UserInfo.getUserId())){  // Run as current user to insert Territories
                for(Integer i = 0 ; i < sizeTerritories ; i++){
                    Territory territory = dataFactory.generateTerritory('Test Territory '+ i+1, null);
                    lstTestTerritories.add(territory);
                }
            }
            
            
            for(Integer i = 0 ; i < sizeTerritories ; i++){
                BI_MM_MirrorTerritory__c mt = dataFactory.generateMirrorTerritory ('mirror Test'+i, lstTestTerritories.get(i).Id);
                lstMirrorTerritories.add(mt);
            }
            
            BI_MM_BatchToUpdateBudgetTerritory c = new BI_MM_BatchToUpdateBudgetTerritory();
            Id batchID = Database.executeBatch(c,2000);
            //System.abortJob(batchID);

        Test.stopTest();
       
       
    }
    
    
}