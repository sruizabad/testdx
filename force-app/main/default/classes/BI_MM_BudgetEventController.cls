/*
Name: BI_MM_BudgetEventController
Requirement ID: collaboration
Description: This Class is used to handle collaboration functinality
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari | 01.01.2016 | initial version
*/
public without sharing class BI_MM_BudgetEventController {

    /* Public variables */
    public BI_MM_BudgetEvent__c objBudgetEvent                      {   get; set;   }

    public List<SelectOption> lstBudgetType                         {   get;set;    }
    public List<SelectOption> lstBudget                             {   get;set;    }
    public List<SelectOption> lstTerritories						{	get;set;	}

    public String selectedBudgetType                                {   get;set;    }
    public String selectedProduct                                   {   get;set;    }
    public String strProductName                                    {   get;set;    }
    public String strSelectedBudget                                 {   get;set;    }
    public String selectedTerritory									{	get;set;	}

    // public Date dtEventEndDate                                      {   get;set;    }
    // public Date dtEventEndDateLabel                                 {   get;set;    }
    public String strBudgetName                                     {   get;set;    }
    //public Decimal decAvailableBudget                               {   get;set;    }
    public BI_MM_Budget__c availableBudget                          {   get;set;    }

    /* Private variables */
    private String strCurrentRecordId;
    private Id mirrorTerritoryId;
    private Id productId;
    private Set<String> setProduct;
    public String strLoggedInUserMirrorTerritoryId {	get;set;	}// Selected territory
    private Set<Id> setLoggedInUserMirrorTerritoryId; //Set of the Mirror Territory Id of the user
    private Map<Id, BI_MM_Budget__c> mapAvailableAmount;

    private Set<String> setCollProgramsId;
    private Set<String> setCollProgramsExtId;
    private String strCountryCode;
    private Set<String> setCountryCode;

    /* Start - Constructor */
    public BI_MM_BudgetEventController(ApexPages.StandardController controller) {
        Initialize();
    }

    /* End - Construtor */

    /*
        @ Method Name : Initialize
        @ Description : This method is used toi initialize objects
        @ Parameter   : None
        25-Jan-2018 - Guillem Vivó - Updated - CR-1298
    */
    private void Initialize() {
        objBudgetEvent = new BI_MM_BudgetEvent__c();
        setProduct = new Set<String>();
        lstBudgetType = new List<SelectOption>();
        lstBudget = new List<SelectOption>();
        mapAvailableAmount = new Map<Id, BI_MM_Budget__c>();
        setCollProgramsId = new Set<String>();
        setCollProgramsExtId = new Set<String>();
        setCountryCode = new Set<String>();
        lstTerritories = new List<SelectOption>();
        setLoggedInUserMirrorTerritoryId = new Set<Id>();

        try{
            Set<Id> userTerritoryId = new Set<Id>();

            // Get the record Id from URL
            strCurrentRecordId  = ApexPages.CurrentPage().getparameters().get('id');

            // Geting Standard territory Id as per logged in Users
            for(UserTerritory objUserTerritory : [SELECT UserId, TerritoryId FROM UserTerritory WHERE UserId =: UserInfo.getUserId()]){
                userTerritoryId.add(objUserTerritory.TerritoryId);            	
            }

            // Geting Mirror territory Id as per logged in Users
            for(BI_MM_MirrorTerritory__c objMMTerritory : [SELECT Id, Name FROM BI_MM_MirrorTerritory__c WHERE BI_MM_TerritoryId__c IN: userTerritoryId]){
            	strLoggedInUserMirrorTerritoryId = objMMTerritory.Id;
            	setLoggedInUserMirrorTerritoryId.add(objMMTerritory.Id);
            	lstTerritories.add(new SelectOption(objMMTerritory.Id, objMMTerritory.Name));
            }
                

            for(BI_MM_BudgetEvent__c objEvent : [SELECT Id, Name, BI_MM_EventID__c, BI_MM_EventStatus__c,BI_MM_EventType__c, BI_MM_PlannedAmount__c , BI_MM_ProductID__c, BI_MM_IsBudgetLinked__c,
                                                        BI_MM_BudgetID__r.Name, BI_MM_BudgetID__c, BI_MM_PaidAmount__c, BI_MM_EndDateTime__c, BI_MM_BudgetType__c, BI_MM_Product__c,
                                                        BI_MM_BudgetID__r.BI_MM_AvailableBudget__c, BI_MM_BudgetID__r.BI_MM_Committed_amount__c , BI_MM_Event_ID_BI__c, BI_MM_EventID__r.of_BI_Employees_BI__c, BI_MM_StartDataTime__c, BI_MM_Committed_amount__c
                                                   FROM BI_MM_BudgetEvent__c
                                                  WHERE Id=: strCurrentRecordId]) {
                objBudgetEvent = objEvent;
                
                System.debug('*** BI_MM_BudgetEventController - Update MedicalEvent - primero - objBudgetEvent.BI_MM_EventID__c == ' + objBudgetEvent.BI_MM_EventID__c); //migart
                

                // if(objBudgetEvent.BI_MM_PlannedAmount__c == 0 || objBudgetEvent.BI_MM_PlannedAmount__c == null){
                if((objBudgetEvent.BI_MM_PlannedAmount__c == null? 0 : objBudgetEvent.BI_MM_PlannedAmount__c) == 0 &&
                objBudgetEvent.BI_MM_EventStatus__c == System.Label.BI_MM_EventStatusNew){

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, System.label.BI_MM_ReviewToProceed));

                }

                if(objBudgetEvent.BI_MM_PaidAmount__c == null){
                    objBudgetEvent.BI_MM_PaidAmount__c = 0;
                }

                if(objEvent.BI_MM_BudgetID__c != null) {

                    strBudgetName = objEvent.BI_MM_BudgetID__r.Name;
                    //decAvailableBudget = objEvent.BI_MM_BudgetID__r.BI_MM_AvailableBudget__c;
                    availableBudget = objEvent.BI_MM_BudgetID__r;
                }
            }

            for(Product_vod__c objProductCatlog : [SELECT Name FROM Product_vod__c WHERE Id =:objBudgetEvent.BI_MM_ProductID__c])
                strProductName = objProductCatlog.Name;

            for(BI_MM_Budget__c objBudget : [   SELECT Id, BI_MM_BudgetType__c, BI_MM_ProductName__c, BI_MM_TerritoryName__c, BI_MM_ProductName__r.name
                                                FROM BI_MM_Budget__c
                                                WHERE BI_MM_TerritoryName__c =: strLoggedInUserMirrorTerritoryId
                                                    AND BI_MM_ProductName__c =: objBudgetEvent.BI_MM_ProductID__c
                                                    AND BI_MM_Is_active__c = true]) {

                productId = objBudget.BI_MM_ProductName__c;
            }
            fetchBudgetType();

            // Get all program ids for Colloboration Type
            List<BI_MM_CollProgramId__c> lstPgrmCustomSetting = BI_MM_CollProgramId__c.getall().values();
            for(BI_MM_CollProgramId__c objPgrm : lstPgrmCustomSetting) setCollProgramsExtId.add(objPgrm.Name);
            List<Event_Program_BI__c> lstPGRM = new List<Event_Program_BI__c>();
            lstPGRM = [Select Id, External_ID_BI__c FROM Event_Program_BI__c WHERE External_ID_BI__c IN : setCollProgramsExtId];
            for(Event_Program_BI__c objPG : lstPGRM) setCollProgramsId.add(objPG.Id);

            // Get all available Countries for MyBudget
            List<BI_MM_CountryCode__c> lstCountryCode = BI_MM_CountryCode__c.getall().values();
            if(lstCountryCode != null)
                for(BI_MM_CountryCode__c objCountryCode : lstCountryCode)
                    setCountryCode.add(objCountryCode.name);

            // Get the country code for logged in Users
            List<User> lstUser = [SELECT Id, Country_Code_BI__c FROM User WHERE Id =:UserInfo.getUserId()];
            for(User objUser : lstUser) strCountryCode = objUser.Country_Code_BI__c;
        }
        catch(Exception ex) {
            System.debug('=====ex===='+ex);
        }
    }
    /* End - Initialize method */

    /**
    * @Description : This method is used to populate budget type
    *
    */
    public void populateBudgetType(){

        try {
            Set<String> setBudgetType = new Set<String>();

            lstBudgetType = new List<SelectOption>();
            lstBudgetType.add(new SelectOption(System.label.BI_MM_None, System.label.BI_MM_None));

            System.debug('*** BI_MM_BudgetEventController - populateBudgetType - objBudgetEvent.BI_MM_Product__c == ' + objBudgetEvent.BI_MM_Product__c);

            for(BI_MM_Budget__c objBudget : [SELECT Id, BI_MM_BudgetType__c, BI_MM_ProductName__c, BI_MM_TerritoryName__c
                                              FROM BI_MM_Budget__c
                                              WHERE BI_MM_TerritoryName__c =:strLoggedInUserMirrorTerritoryId
                                                    AND BI_MM_BudgetType__c != null
                                                    AND BI_MM_ProductName__c =: objBudgetEvent.BI_MM_Product__c
                                                    AND BI_MM_Is_active__c = true
                                              ORDER BY BI_MM_BudgetType__c]) {

                if(!setBudgetType.contains(objBudget.BI_MM_BudgetType__c)) {
                    setBudgetType.add(objBudget.BI_MM_BudgetType__c);
                    lstBudgetType.add(new SelectOption(objBudget.BI_MM_BudgetType__c, objBudget.BI_MM_BudgetType__c));
                }

            }
            //Reset Budgets
            fetchAllBudget();
        }
        catch(Exception ex) {
            System.debug('=======Exception=='+ex);
        }
    }

    /**
    * @Description : This method is used to get all budget type
    *
    * @Author: OmegaCRM
    */
    public void fetchBudgetType() {

        Set<String> setBudgetType = new Set<String>();

        try {
            lstBudgetType.add(new SelectOption(System.label.BI_MM_None, System.label.BI_MM_None));

            for(BI_MM_Budget__c objBudget : [SELECT Id, BI_MM_BudgetType__c, BI_MM_ProductName__c, BI_MM_TerritoryName__c
                                               FROM BI_MM_Budget__c
                                               WHERE BI_MM_TerritoryName__c =:strLoggedInUserMirrorTerritoryId
                                                    AND BI_MM_BudgetType__c != null
                                                    AND BI_MM_ProductName__c =: productId
                                                    AND BI_MM_Is_active__c = true
                                               ORDER BY BI_MM_BudgetType__c]) {

                if(!setBudgetType.contains(objBudget.BI_MM_BudgetType__c)) {
                    setBudgetType.add(objBudget.BI_MM_BudgetType__c);
                    lstBudgetType.add(new SelectOption(objBudget.BI_MM_BudgetType__c, objBudget.BI_MM_BudgetType__c));
                }
            }
        }
        catch(Exception ex) {
            System.debug('=======Exception=='+ex);
        }
    }

    /*
        @ Method Name : fetchAllBudget
        @ Description : This method is used to get all budget
                            03-11-2016 - OmegaCRM: Only Active Budgets will be retrieved
        @ Parameter   : None
    */
    public void fetchAllBudget() {
        lstBudget = new List<SelectOption>();
        availableBudget = new BI_MM_Budget__c();
        // List<String> setBudgetName = new List<String>();
        String strProductId =  (objBudgetEvent.BI_MM_ProductID__c != null) ? objBudgetEvent.BI_MM_ProductID__c : objBudgetEvent.BI_MM_Product__c;
        boolean first = true;
        try {
            for(BI_MM_Budget__c objBudget : [SELECT Id, Name, BI_MM_BudgetType__c, BI_MM_ProductName__c, BI_MM_TerritoryName__c, BI_MM_ProductName__r.name, BI_MM_AvailableBudget__c, BI_MM_Committed_amount__c
                                              FROM BI_MM_Budget__c
                                              WHERE BI_MM_TerritoryName__c =: strLoggedInUserMirrorTerritoryId
                                              AND BI_MM_ProductName__c =: strProductId
                                              AND BI_MM_BudgetType__c =:selectedBudgetType
                                              AND BI_MM_Is_active__c = true
                                              ORDER BY Name]) {
                // setBudgetName.add(objBudget.Name);
                lstBudget.add(new SelectOption(objBudget.Id, objBudget.Name));
                mapAvailableAmount.put(objBudget.Id, objBudget);
                //The first budget will be selected by default
                if(first){
                    //decAvailableBudget = objBudget.BI_MM_AvailableBudget__c;
                    availableBudget = objBudget;
                    first = false;
                }
            }
            System.debug('*** BI_MM_BudgetEventController - fetchAllBudget - lstBudget == ' + lstBudget);
        }
        catch(Exception ex) {
            System.debug('=====ex===='+ex);
        }
    }

    /**
     * @Description method to fill the Available Amount of the selected budget
     * @author  OmegaCRM
     */
    public void populateAvailableBudget(){
        if(strSelectedBudget != null && !String.isEmpty(strSelectedBudget)) availableBudget = mapAvailableAmount.get(strSelectedBudget);
        else availableBudget = new BI_MM_Budget__c();
    }

    /*
        @ Method Name : SaveRecord
        @ Description : This method is used to save budget Event
        @ Parameter   : None
        25-Jan-2018 - Guillem Vivó - Updated - CR-1298
    */
    public PageReference saveRecord() {
        System.debug('*** BI_MM_BudgetEventController - SaveRecord - Start ');
        List<BI_MM_Budget__c> lstBudget;
        List<Medical_Event_vod__c> lstMedicalEvenToUpdate = new List<Medical_Event_vod__c>();
        Map<Id, Double> mapBudgetIdToPlannedAmount = new Map<Id, Double>();
        Map<Id, Double> mapBudgetIdToPaidAmount = new Map<Id, Double>();

        try {
            //If budget is already linked we just want to save the budget event
            if(!objBudgetEvent.BI_MM_IsBudgetLinked__c){

                // List<Product_vod__c> lstProduct = [SELECT Id, Name FROM Product_vod__c WHERE Id =:objBudgetEvent.BI_MM_ProductID__c];
                List<Event_Expenses_BI__c> lstExpenses = [SELECT Id FROM Event_Expenses_BI__c
                    WHERE Event_Team_Member_BI__c IN (SELECT Id FROM Event_Team_Member_BI__c WHERE Event_Management_BI__c = :objBudgetEvent.BI_MM_EventID__c)];

                objBudgetEvent.BI_MM_BudgetID__c = strSelectedBudget;
                lstBudget = [SELECT Id, BI_MM_BudgetType__c,BI_MM_AvailableBudget__c,BI_MM_ProductName__r.Name, BI_MM_Country_code__c, CurrencyIsoCode
                               FROM BI_MM_Budget__c
                              WHERE Id =: objBudgetEvent.BI_MM_BudgetID__c];

                // Show error message if planned amount is less than zero
                if(objBudgetEvent.BI_MM_PlannedAmount__c < 0) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.BI_MM_PlannedAmtMsg));
                    return null;
                }
                else if(objBudgetEvent.BI_MM_BudgetID__c == null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.BI_MM_CollaborationMsg));
                    return null;
                }
                // Show Error message if there are no team Member
                else if(objBudgetEvent.BI_MM_EventID__r.of_BI_Employees_BI__c <= 0) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.BI_MM_TeamMemberMsg));
                    return null;
                }
                // Show error message if there are no expenses assigned to Event
                else if(lstExpenses == null || lstExpenses.size() == 0) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.BI_MM_EventExpenseMsg));
                    return null;
                }
                // Show error message if Event Currency is different than Budget Currency
                else if(objBudgetEvent.CurrencyIsoCode != lstBudget[0].CurrencyIsoCode) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.BI_MM_WrongCurrencyMsg));
                    return null;
                }
                else{
                    objBudgetEvent.BI_MM_BudgetType__c = selectedBudgetType;
                    objBudgetEvent.BI_MM_ProductName__c =  (lstBudget != null && lstBudget.size() > 0 ? lstBudget[0].BI_MM_ProductName__r.Name : '');
                    objBudgetEvent.BI_MM_BudgetID__c = (lstBudget != null && lstBudget.size() > 0 ? lstBudget[0].Id : null);
                    if(objBudgetEvent.BI_MM_ProductID__c == null) objBudgetEvent.BI_MM_ProductID__c = objBudgetEvent.BI_MM_Product__c;
                }          
           
           		System.debug('*** BI_MM_BudgetEventController - Update MedicalEvent - objBudgetEvent.BI_MM_EventID__c == ' + objBudgetEvent.BI_MM_EventID__c); //migart

                // Update MedicalEvent
                if(objBudgetEvent.BI_MM_EventID__c != null){

                    Map<Id, Medical_Event_vod__c> mapMedEventIdToMedEventObj = new Map<Id, Medical_Event_vod__c>();
                    Map<Id, Id> mapMedicalEventIdToProgramId = new map<Id, Id>();
                    Map<Id, String> mapProgramIdToProgramExtId = new map<Id, String>();

                    Medical_Event_vod__c objMedEvent = [SELECT Id, Program_BI__c, Event_Status_BI__c,RecordTypeId, Total_Cost_BI__c, Expense_Amount_vod__c, Expense_Post_Status_vod__c
                                                          FROM Medical_Event_vod__c WHERE Id = :objBudgetEvent.BI_MM_EventID__c];

                    if(objMedEvent.Program_BI__c != null){
                        mapMedicalEventIdToProgramId.put(objMedEvent.Id, objMedEvent.Program_BI__c);
                        mapMedEventIdToMedEventObj.put(objMedEvent.Id, objMedEvent);
                    }

                    Event_Program_BI__c objEventProgram = [SELECT Id, Name, External_ID_BI__c
                                                          FROM Event_Program_BI__c WHERE Id = :objMedEvent.Program_BI__c];
                    if(objEventProgram.External_ID_BI__c != null)
                        mapProgramIdToProgramExtId.put(objEventProgram.Id, objEventProgram.External_ID_BI__c);

                    //Not re-update if status is rejected
                    if(mapMedicalEventIdToProgramId.containskey(objBudgetEvent.BI_MM_EventID__c) &&
                    setCollProgramsId.contains(mapMedicalEventIdToProgramId.get(objBudgetEvent.BI_MM_EventID__c)) &&
                    setCountryCode.contains(strCountryCode) && objBudgetEvent.BI_MM_EventStatus__c != System.Label.BI_MM_Rejected){

                        //Get Planned and Paid new occurence
                        System.debug('*** BI_MM_BudgetEventController - SaveRecord - Status == ' + objBudgetEvent.BI_MM_IsEventPaid__c + ' - ' + objBudgetEvent.BI_MM_EventStatus__c);
                        Medical_Event_vod__c medEvent = new Medical_Event_vod__c(
                            Id = objBudgetEvent.BI_MM_EventID__c,
                            Product_BI__c = objBudgetEvent.BI_MM_ProductID__c,
                            Expense_Post_Status_vod__c = mapMedEventIdToMedEventObj.get(objBudgetEvent.BI_MM_EventID__c).Expense_Post_Status_vod__c,
                            Event_Status_BI__c = mapMedEventIdToMedEventObj.get(objBudgetEvent.BI_MM_EventID__c).Event_Status_BI__c
                        );

                        if(BI_MM_CollProgramId__c.getValues(mapProgramIdToProgramExtId.get(mapMedicalEventIdToProgramId.get(objBudgetEvent.BI_MM_EventID__c))).BI_MM_Remove_User_Attendees__c) {
                            //Set Expense post status once, then don't update the value anymore.
                            if(medEvent.Expense_Post_Status_vod__c == null && String.isEmpty(medEvent.Expense_Post_Status_vod__c))
                                medEvent.Expense_Post_Status_vod__c = Label.BI_MM_Pending;

                            medEvent.Expense_Amount_vod__c = mapMedEventIdToMedEventObj.get(objBudgetEvent.BI_MM_EventID__c).Total_Cost_BI__c;
                            System.debug('*** BI_MM_BudgetEventController - SaveRecord - Expense_Amount_vod__c == ' + medEvent.Expense_Amount_vod__c);
                        }
                        else {
                            medEvent.Expense_Amount_vod__c = 0;
                        }

                        //If the event is rejected, updating the Med Event will change its status to New/In Progress
                        if(medEvent.Event_Status_BI__c != System.Label.BI_MM_Rejected) lstMedicalEvenToUpdate.add(medEvent);
                    }
                }
            }
            if(objBudgetEvent != null) {
                System.debug('*** BI_MM_BudgetEventController - SaveRecord - objBudgetEventUpdated == ' + objBudgetEvent);
                update objBudgetEvent;
                if(!lstMedicalEvenToUpdate.isEmpty()) {
                    System.debug('*** BI_MM_BudgetEventController - SaveRecord - lstMedicalEvenToUpdate == ' + lstMedicalEvenToUpdate);
                    update lstMedicalEvenToUpdate;
                }
                PageReference objPR = new PageReference('/'+strCurrentRecordId);
                return objPR;
            }
            return null;
        }
        catch(DMLException ex){
            // Custom Exception for negative value
            System.debug('=====ex===='+ex);
            if(ex.getMessage().contains(System.Label.BI_MM_Budget_Exceeded) && lstBudget != null && lstBudget[0].BI_MM_Country_code__c != null){
                BI_MM_Negative_budget__c negativeValue = BI_MM_Negative_budget__c.getValues(lstBudget[0].BI_MM_Country_code__c);
                if(negativeValue == null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.BI_MM_Budget_Exceeded + '0%'));
                else
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.BI_MM_Budget_Exceeded + ' ' + negativeValue.BI_MM_Negative_percentage__c + '%'));
            }
            // Custom Exception for negative value in Master Budget
            else if(ex.getMessage().contains(System.Label.BI_MM_MasterBudget_Exceeded) && lstBudget != null && lstBudget[0].BI_MM_Country_code__c != null){
                BI_MM_Negative_budget__c negativeValue = BI_MM_Negative_budget__c.getValues(lstBudget[0].BI_MM_Country_code__c);
                if(negativeValue == null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.BI_MM_MasterBudget_Exceeded + '0%'));
                else
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.BI_MM_MasterBudget_Exceeded + ' ' + negativeValue.BI_MM_Negative_percentage__c + '%'));
            }
        }
        catch(Exception ex) {
            System.debug('=====ex===='+ex);
        }
        return null;
    }
}
/* End - BI_MM_BudgetEventController Class */