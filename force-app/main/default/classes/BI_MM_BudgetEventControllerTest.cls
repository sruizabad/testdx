/*
Name: BI_MM_BudgetEventControllerTest
Requirement ID: collaboration
Description: This is test class for BI_MM_BudgetEventController
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari | 01.01.2016 | initial version
*/
@isTest
private class BI_MM_BudgetEventControllerTest {

    static BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();
    static User testUser;
    static Territory territory;

    static void initUser(){

        List<Id> usersIds = new List<Id>();
        Map<Id, Id> mapUsersTerrs = new Map<Id, Id>();

        // Get BI_MM_SALES_MANAGER permission set
        PermissionSet permissionManager = [select Id from PermissionSet where Name = 'BI_MM_SALES_MANAGER' limit 1];

        // Get all users that have BI_MM_SALES_MANAGER permission set assigned
        for(PermissionSetAssignment permAssign :[select Id, AssigneeId from PermissionSetAssignment where PermissionSetId = :permissionManager.Id ]) {
            usersIds.add(permAssign.AssigneeId);
        }

        // Get UsersTerritorries for users that have BI_MM_SALES_MANAGER permission and  with a valid Territory
        for( UserTerritory usrTerr : [Select UserId, TerritoryId from UserTerritory where TerritoryId != null and UserId in :usersIds ]) {
            mapUsersTerrs.put(usrTerr.UserId, usrTerr.TerritoryId);
        }

        // select a user which is Active, has an assigned Territory and has a valid Country Code
        testUser = [select Id, Name, Country_Code_BI__c from User where isActive = true and Id in :mapUsersTerrs.keySet() and Country_Code_BI__c != null limit 1];

        // Select the territory assigned to to user
        territory = [select Id, Name from Territory where Id = :mapUsersTerrs.get(testUser.Id)];
    }

    private static testMethod void testBI_MM_BudgetEventController() {

        initUser();

        System.runAs(testUser) {

            // Insert Mirror Territory
            BI_MM_MirrorTerritory__c objMirrorTerritory = dataFactory.generateMirrorTerritory ('sr01Test', territory.Id);

            // Get record type id for Medical event
            RecordType colloborationRecordType = [select Id, Name, DeveloperName from RecordType where Sobjecttype='Medical_Event_vod__c' and DeveloperName = 'Medical_Event'][0];

            // Insert Country
            BI_MM_CountryCode__c objCountryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c, colloborationRecordType.DeveloperName);
			System.debug('*** BI_MM_BudgetEventControllerTest - Insert Country and record type : ' + testUser.Country_Code_BI__c + ' ' + colloborationRecordType.DeveloperName);//migart 

            // Insert Product Catelog
            Product_vod__c objProduct = dataFactory.generateProduct('Test Product', 'Detail', objCountryCode.Name);

            Date dt = system.Today();
            Date endDate = dt.addDays(6);

            Event_Program_BI__c objEventProgram = dataFactory.generateEventProgram('TestCollProgram', objCountryCode.Name,'', 'TestCollProgram');//migart

            BI_MM_CollProgramId__c objCollProgramId = dataFactory.generateCsProgram(objEventProgram.Id, objEventProgram.Name);

            // Insert Medical event record
            Medical_Event_vod__c objMedicalEvent = dataFactory.generateMedicalEvent(colloborationRecordType.Id, 'Test Collaboration Event Name', objProduct, objEventProgram,
                                                                                         'New / In Progress');

            // Insert spain Budget
            BI_MM_Budget__c objBudget  = dataFactory.generateBudgets(1, objMirrorTerritory.Id, null, 'Micro Marketing', objProduct, 4000, true).get(0);

            Event_Team_Member_BI__c objTM = new Event_Team_Member_BI__c(Event_Management_BI__c = objMedicalEvent.Id, User_BI__c = UserInfo.getUserId());
            insert objTM;

            List<BI_MM_BudgetEvent__c> lstBI_MM_BudgetEvent = [SELECT Id FROM BI_MM_BudgetEvent__c WHERE BI_MM_EventID__c =:objMedicalEvent.Id];
            System.assertEquals(1, lstBI_MM_BudgetEvent.size(), 'No Budget_Event was inserted for the Medical Event');

            if(lstBI_MM_BudgetEvent != null && lstBI_MM_BudgetEvent.size() > 0) {
                Test.startTest();
                    PageReference pageRef = Page.BI_MM_BudgetEvent;
                    Test.setCurrentPage(pageRef);
                    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(lstBI_MM_BudgetEvent[0]);
                    ApexPages.currentPage().getParameters().put('Id', lstBI_MM_BudgetEvent[0].id);

                    // Call public methods
                    BI_MM_BudgetEventController objBudgetEventCtr = new BI_MM_BudgetEventController(sc);
                    objBudgetEventCtr.SaveRecord();
                    objBudgetEventCtr.selectedProduct = objProduct.id;
                    objBudgetEventCtr.strSelectedBudget = objBudget.Id;
                    objBudgetEventCtr.selectedBudgetType = 'Micro Marketing';
                    objBudgetEventCtr.fetchAllBudget();
                    objBudgetEventCtr.SaveRecord();
                    objBudgetEventCtr.populateBudgetType();
                Test.stopTest();
                system.debug(':: Limit queries: ' + Limits.getLimitQueries() + ' - Queries: ' + Limits.getQueries());
            }
        }
    }

    private static testMethod void testExpenseException() {

        initUser();

        System.runAs(testUser) {

            // Insert Mirror Territory
            BI_MM_MirrorTerritory__c objMirrorTerritory = dataFactory.generateMirrorTerritory ('sr01Test', territory.Id);

            // Get record type id for Medical event
            RecordType colloborationRecordType = [select Id, Name, DeveloperName from RecordType where Sobjecttype='Medical_Event_vod__c' and DeveloperName = 'Medical_Event'][0];

            // Insert Country
            BI_MM_CountryCode__c objCountryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c, colloborationRecordType.DeveloperName);

            // Insert Product Catelog
            Product_vod__c objProduct = dataFactory.generateProduct('Test Product', 'Detail', objCountryCode.Name);

            Date dt = system.Today();
            Date endDate = dt.addDays(6);

            Event_Program_BI__c objEventProgram = dataFactory.generateEventProgram('TestCollProgram', objCountryCode.Name,'', 'TestCollProgram');//migart

            BI_MM_CollProgramId__c objCollProgramId = dataFactory.generateCsProgram(objEventProgram.Id, objEventProgram.Name);

            // Insert Medical event record
            Medical_Event_vod__c objMedicalEvent = dataFactory.generateMedicalEvent(colloborationRecordType.Id, 'Test Collaboration Event Name', objProduct, objEventProgram,
                                                                                         'New / In Progress');

            // Insert spain Budget
            BI_MM_Budget__c objBudget  = dataFactory.generateBudgets(1, objMirrorTerritory.Id, null, 'Micro Marketing', objProduct, 4000, true).get(0);

            Event_Team_Member_BI__c objTM = new Event_Team_Member_BI__c(Event_Management_BI__c = objMedicalEvent.Id, User_BI__c = UserInfo.getUserId());
            insert objTM;

            List<BI_MM_BudgetEvent__c> lstBI_MM_BudgetEvent = [SELECT Id, BI_MM_EventID__c  FROM BI_MM_BudgetEvent__c WHERE BI_MM_EventID__c =:objMedicalEvent.Id];
            System.assertEquals(1, lstBI_MM_BudgetEvent.size(), 'A Budget Event was not inserted for the Medical Event');

			System.debug('*** BI_MM_BudgetEventControllerTest - lstBI_MM_BudgetEvent : ' + lstBI_MM_BudgetEvent);//migart 

            if(lstBI_MM_BudgetEvent != null && lstBI_MM_BudgetEvent.size() > 0) {
                Test.startTest();
                    PageReference pageRef = Page.BI_MM_BudgetEvent;
                    Test.setCurrentPage(pageRef);
                    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(lstBI_MM_BudgetEvent[0]);
                    ApexPages.currentPage().getParameters().put('Id', lstBI_MM_BudgetEvent[0].id);

                    // Call public methods
                    BI_MM_BudgetEventController objBudgetEventCtr = new BI_MM_BudgetEventController(sc);
                    //objBudgetEventCtr.SaveRecord(); // migart
                    objBudgetEventCtr.selectedProduct = objProduct.id;
                    objBudgetEventCtr.strSelectedBudget = objBudget.Id;
                    objBudgetEventCtr.selectedBudgetType = 'Micro Marketing';
                    objBudgetEventCtr.fetchAllBudget();
                    objBudgetEventCtr.SaveRecord();
                    objBudgetEventCtr.populateBudgetType();
                Test.stopTest();
                system.debug(':: Limit queries: ' + Limits.getLimitQueries() + ' - Queries: ' + Limits.getQueries());
            }
        }
    }
}