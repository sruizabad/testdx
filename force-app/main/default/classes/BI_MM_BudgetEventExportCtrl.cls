/*
Name: BI_MM_BudgetEventExportCtrl
Requirement ID: CWT Template
Description: Controller Class for BI_MM_BudgetEventExport. Page to input remaining event information for CWT and exporting Excel form.
Version | Author-Email    | Date       | Comment
1.0     | Omega CRM       | 30.05.2017 | initial version
*/
public without sharing class BI_MM_BudgetEventExportCtrl{

    public BI_MM_BudgetEvent__c objBudgetEvent                  {   get; set;   }
    public BI_MM_Budget_Event_Export__c objBudgetEventExport    {   get; set;   }
    public User objUserApplicant                                {   get; set;   }
    public Medical_Event_vod__c objMedicalEvent                 {   get; set;   }

    public String strEventId                                    {   get;set;    }
    public Id idEventProgram                                    {   get;set;    }
    public String exportPage                                    {   get;set;    }

    public String strAppliTerr                                  {   get;set;    }
    public String strEventData                                  {   get;set;    }
    public String strPartType                                   {   get;set;    }

    public Boolean flgEventData                                 {   get;set;    }
    public Boolean flgPartType                                  {   get;set;    }
    public Boolean flgOneKey                                    {   get;set;    }
    public Boolean flgAgreg                                     {   get;set;    }
    public Boolean flgServ                                      {   get;set;    }
    public Boolean flgServTrans                                 {   get;set;    }
    public Boolean flgServAcomm                                 {   get;set;    }
    public Boolean flgServRegis                                 {   get;set;    }
    public Boolean flgServShort                                 {   get;set;    }
    public Boolean exportButton                                 {   get;set;    }
    public Boolean flgPartakerInfo                              {   get;set;    }

    public BI_MM_BudgetEventExportCtrl(ApexPages.StandardController controller) {

        System.debug('*** BI_MM_BudgetEventExportCtrl - Init');
        // System.debug('*** BI_MM_BudgetEventExportCtrl - currentpageUrl == ' + Apexpages.currentpage().getUrl());

        //Show the info messages:
        String strInfoMsg = Label.BI_MM_CWT_Important_1+'\n';
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, strInfoMsg));
        strInfoMsg += Label.BI_MM_CWT_Important_2;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, strInfoMsg));
        //Show the Territory and phone message
        String strErrorMsg = Label.BI_MM_Empty_territory;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, strErrorMsg));

        //If this is false the button is disabled
        exportButton= false;

        getEventData();
        checkEventProgram();

        flgEventData = false;
        flgPartType = false;
        flgOneKey = false;
        flgAgreg = false;
        flgServ = false;
        flgServTrans = false;
        flgServAcomm = false;
        flgServRegis = false;
        flgServShort = false;
    }

    /**
     * @Description get Event and User Data related to Budget Event
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void getEventData(){
        strEventId = Apexpages.currentpage().getparameters().get('BEvId');

        System.debug('***strEventId = '+strEventId);

        objBudgetEvent = [SELECT Id, Name, BI_MM_Event_ID_BI__c, BI_MM_Product__c, BI_MM_ProductName__c,
                                BI_MM_EventID__c, BI_MM_EventID__r.Total_Cost_BI__c, BI_MM_BudgetType__c, BI_MM_BudgetID__c, BI_MM_IsBudgetLinked__c,
                                CreatedById
                                FROM BI_MM_BudgetEvent__c
                                WHERE BI_MM_Event_ID_BI__c = :strEventId];

        objBudgetEventExport = new BI_MM_Budget_Event_Export__c(Name = objBudgetEvent.Name, BI_MM_Budget_Event__c = objBudgetEvent.Id);

        objUserApplicant = [SELECT Id, Name, FirstName, LastName, Phone, Email
                                FROM User
                                WHERE Id = :objBudgetEvent.CreatedById];

        objBudgetEventExport.BI_MM_Applicant__c = objUserApplicant.Id;

        objMedicalEvent = [SELECT Event_ID_BI__c, Event_Status_BI__c, Id, Name, Start_Date_vod__c, End_Date_vod__c, Program_BI__c
                            FROM Medical_Event_vod__c
                            WHERE Id = :objBudgetEvent.BI_MM_EventID__c];
        System.debug('***objMedicalEvent = '+objMedicalEvent);

        /* To review Later. List of Attendees and Partaker elements change when selecting Attendee
        for(Event_Attendee_vod__c objEventAttendee : [SELECT Id, Name, Attendee_Name_vod__c, User_vod__c
                                                FROM Event_Attendee_vod__c WHERE Medical_Event_vod__c = :objMedicalEvent.Id LIMIT 1])
            objPartaker = objEventAttendee;

        objUserPartaker = [SELECT Id, Name, FirstName, LastName, Phone, Email
                                FROM User
                                WHERE Id = :objPartaker.User_vod__c];*/
    }

    /**
     * @Description checks Event Program of related MedicalEvent and gets it to control Export page and availability
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void checkEventProgram(){
        idEventProgram = null;
        exportPage = null;
        List<BI_MM_Budget_Event_Export_Config__c> lstPgrmExport = BI_MM_Budget_Event_Export_Config__c.getall().values();
        for(BI_MM_Budget_Event_Export_Config__c objBudEventExport : lstPgrmExport){
            System.debug('*** BI_MM_BudgetEventExportCtrl - checkEventProgram - Program_BI__c == ' + objMedicalEvent.Program_BI__c);
            System.debug('*** BI_MM_BudgetEventExportCtrl - checkEventProgram - BI_MM_Event_Program_ID__c == ' + objBudEventExport.BI_MM_Event_Program_ID__c);
            if(objMedicalEvent.Program_BI__c == objBudEventExport.BI_MM_Event_Program_ID__c && idEventProgram == null){
                idEventProgram = objBudEventExport.BI_MM_Event_Program_ID__c;
                exportPage = objBudEventExport.BI_MM_Export_Page__c;
            }
        }
    }

    /**
     * @Description onChange functions for actions after changes in Page elements
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void onChangeApplTerr(){
        showFormSections();
        validateExport();
        // System.debug('*** BI_MM_BudgetEventExportCtrl - onChangeApplTerr - BI_MM_Applicant_Territory__c == ' + objBudgetEventExport.BI_MM_Applicant_Territory__c);
    }

    public void onChangeEventData(){
        showFormSections();
        validateExport();
    }

    public void onChangePartType(){
        showFormSections();
        validateExport();
    }

    public void onChangeServTrans(){
        showFormSections();
        validateExport();
    }

    public void onChangeServAccom(){
        showFormSections();
        validateExport();
    }

    public void onChangeServRegis(){
        showFormSections();
        validateExport();
    }

    public void onChangeServShort(){
        showFormSections();
        validateExport();
    }

    public void onChangeValidate(){
        onChangeServShort();
        validateExport();
    }

    public void onChangePartaker(){
        showFormSections();
        System.debug('***ONChangepartaker');
        validateExport();
    }

    /**
     * @Description sets flags that control visible sections of the form
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void showFormSections(){

        //Show the info messages:
        String strInfoMsg = Label.BI_MM_CWT_Important_1+'\n';
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, strInfoMsg));
        strInfoMsg += Label.BI_MM_CWT_Important_2;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, strInfoMsg));

        if (objBudgetEventExport.BI_MM_Applicant_Territory__c != null && objBudgetEventExport.BI_MM_Applicant_phone__c != null)
            flgEventData = true;
        else {
            flgEventData = false;
            flgPartType = false;
            flgServ = false;
            flgServTrans = false;
            flgServAcomm = false;
            flgServRegis = false;
            flgServShort = false;
        }

        if (flgEventData &&
            objBudgetEventExport.BI_MM_Event_Location__c != null &&
            objBudgetEventExport.BI_MM_Event_Location__c.trim() != null &&
            objBudgetEventExport.BI_MM_Event_Cost_Center__c != null)
            flgPartType = true;
        else {
            flgPartType = false;
            flgServ = false;
            flgServTrans = false;
            flgServAcomm = false;
            flgServRegis = false;
            flgServShort = false;
        }

        String strPartTypeOpt;
        if (objBudgetEventExport.BI_MM_Partaker_Type__c != null) strPartTypeOpt = objBudgetEventExport.BI_MM_Partaker_Type__c.left(1);
        if (flgPartType && strPartTypeOpt != null && strPartTypeOpt == '1')
            flgOneKey = true;
        else flgOneKey = false;
        if (flgPartType && strPartTypeOpt != null && strPartTypeOpt == '1' && objBudgetEventExport.BI_MM_Partaker_ONEKEY__c != null && objBudgetEventExport.BI_MM_Partaker_Category__c !=null)
            flgServ = true;
        else if (flgPartType && strPartTypeOpt != null && strPartTypeOpt != '1')
            flgServ = true;
        else {
            flgServ = false;
            flgServTrans = false;
            flgServAcomm = false;
            flgServRegis = false;
            flgServShort = false;
        }
        if (flgPartType && strPartTypeOpt != null && strPartTypeOpt == '5')
            flgAgreg = true;
        else flgAgreg = false;

        if (flgServ && objBudgetEventExport.BI_MM_Service_Transport__c)
            flgServTrans = true;
        else flgServTrans = false;
        if (flgServ && objBudgetEventExport.BI_MM_Service_Accommodation__c)
            flgServAcomm = true;
        else flgServAcomm = false;
        if (flgServ && objBudgetEventExport.BI_MM_Service_Registration__c)
            flgServRegis = true;
        else flgServRegis = false;
        if (flgServ && objBudgetEventExport.BI_MM_Service_Short_Meeting__c)
            flgServShort = true;
        else flgServShort = false;
    }

    /**
     * @Description Export procedure
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public String export(){
        PageReference pg;
        clearFields();
        if (validateExport()) {
            System.debug('***Validated');
            try{
                insert objBudgetEventExport;
                pg = redirectToExportPage();
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getDmlMessage(0)));
                pg = null;
            }
        } else {
            System.debug('***Not Validated');
            pg = null;
        }
        if(pg != null)
            return pg.getUrl();
        else{
            return null;
        }
    }

    /**
     * @Description cleans non-visible data from form before insert record
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void clearFields(){

        if (!flgEventData){
            objBudgetEventExport.BI_MM_Event_Location__c = null;
            objBudgetEventExport.BI_MM_Event_Cost_Center__c = null;
            objBudgetEventExport.BI_MM_Event_Delegate_Code_MSL__c = null;
            objBudgetEventExport.BI_MM_Event_project_ID_MSL__c = null;
        }
        if (!flgPartType){
            objBudgetEventExport.BI_MM_Partaker_Type__c = null;
            objBudgetEventExport.BI_MM_Partaker_first_name__c = null;
            objBudgetEventExport.BI_MM_Partaker_last_name__c = null;
            objBudgetEventExport.BI_MM_Partaker_email__c = null;
            objBudgetEventExport.BI_MM_Partaker_phone__c = null;
            objBudgetEventExport.BI_MM_Partaker_ONEKEY__c = null;
            objBudgetEventExport.BI_MM_Partaker_ID__c = null;
            objBudgetEventExport.BI_MM_Partaker_Specialty__c = null;
            objBudgetEventExport.BI_MM_Partaker_Work_Center__c = null;
            objBudgetEventExport.BI_MM_Partaker_Postal_Code__c = null;
            objBudgetEventExport.BI_MM_Partaker_City__c = null;
            objBudgetEventExport.BI_MM_Partaker_Province__c = null;
            objBudgetEventExport.BI_MM_Partaker_Advertisement__c = null;
        }
        if (!flgOneKey){
            objBudgetEventExport.BI_MM_Partaker_ONEKEY__c = null;
        }
        if (flgAgreg){
            objBudgetEventExport.BI_MM_Partaker_ID__c = null;
            objBudgetEventExport.BI_MM_Partaker_Specialty__c = null;
            objBudgetEventExport.BI_MM_Partaker_Work_Center__c = null;
            objBudgetEventExport.BI_MM_Partaker_Postal_Code__c = null;
            objBudgetEventExport.BI_MM_Partaker_City__c = null;
            objBudgetEventExport.BI_MM_Partaker_Province__c = null;
            objBudgetEventExport.BI_MM_Partaker_Advertisement__c = false;
            objBudgetEventExport.BI_MM_Service_Transport__c = false;
            flgServTrans = false;
            objBudgetEventExport.BI_MM_Service_Accommodation__c = false;
            flgServAcomm = false;
            objBudgetEventExport.BI_MM_Service_Registration__c = false;
            flgServRegis = false;
        }
        if (!flgServ){
            objBudgetEventExport.BI_MM_Service_Transport__c = false;
            flgServTrans = false;
            objBudgetEventExport.BI_MM_Service_Accommodation__c = false;
            flgServAcomm = false;
            objBudgetEventExport.BI_MM_Service_Registration__c = false;
            flgServRegis = false;
            objBudgetEventExport.BI_MM_Service_Short_Meeting__c = false;
            flgServShort = false;
            objBudgetEventExport.BI_MM_Service_Budget_Limit__c = null;
            objBudgetEventExport.BI_MM_Service_Comments__c = null;
        }
        if (!flgServTrans){
            objBudgetEventExport.BI_MM_Service_Transport_Type_Dep__c = null;
            objBudgetEventExport.BI_MM_Service_Transport_Type_Ret__c = null;
            objBudgetEventExport.BI_MM_Service_Transport_Ori_Dep__c = null;
            objBudgetEventExport.BI_MM_Service_Transport_Ori_Ret__c = null;
            objBudgetEventExport.BI_MM_Service_Transport_Des_Dep__c = null;
            objBudgetEventExport.BI_MM_Service_Transport_Des_Ret__c = null;
            objBudgetEventExport.BI_MM_Service_Transport_Date_Dep__c = null;
            objBudgetEventExport.BI_MM_Service_Transport_Date_Ret__c = null;
            objBudgetEventExport.BI_MM_Service_Transport_Comments__c = null;
            objBudgetEventExport.BI_MM_Service_Transport_Insurance__c = false;
        }
        if (!flgServAcomm){
            objBudgetEventExport.BI_MM_Service_Accommodation_Date_Arrival__c = null;
            objBudgetEventExport.BI_MM_Service_Accommodation_Date_Dep__c = null;
            objBudgetEventExport.BI_MM_Service_Accommodation_Type__c = null;
            objBudgetEventExport.BI_MM_Service_Accommodation_Comments__c = null;
        }
        if (!flgServRegis){
            objBudgetEventExport.BI_MM_Service_Registration_Type__c = null;
            objBudgetEventExport.BI_MM_Service_Registration_Comments__c = null;
        }
        if (!flgServShort){
            objBudgetEventExport.BI_MM_Service_Short_Meeting_Comments__c = null;
        }
    }

    /**
     * @Description performs validations before export operation
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public Boolean validateExport(){
        Boolean errorFlag = true;
        Boolean flgPartakerInfo = true;
        String strErrorMsg;
        if (objBudgetEventExport == null){
            strErrorMsg = Label.BI_MM_Empty_budget_event;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strErrorMsg));
            System.debug('*** BI_MM_BudgetEventExportCtrl - validateExport == ' + strErrorMsg);
            errorFlag = false;
        }
        else if (!flgEventData){
            strErrorMsg = Label.BI_MM_Empty_territory;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, strErrorMsg));
            System.debug('*** BI_MM_BudgetEventExportCtrl - validateExport == ' + strErrorMsg);
            errorFlag = false;
        }
        else if (!flgPartType){
            strErrorMsg = Label.BI_MM_Empty_location_center;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, strErrorMsg));
            System.debug('*** BI_MM_BudgetEventExportCtrl - validateExport == ' + strErrorMsg);
            errorFlag = false;
        }
        else if (!flgServ){
            strErrorMsg = Label.BI_MM_Partaker_type;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, strErrorMsg));
            System.debug('*** BI_MM_BudgetEventExportCtrl - validateExport == ' + strErrorMsg);
            errorFlag = false;
        }
        else if (idEventProgram == null){
            strErrorMsg = Label.BI_MM_Event_program;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strErrorMsg));
            System.debug('*** BI_MM_BudgetEventExportCtrl - validateExport == ' + strErrorMsg);
            errorFlag = false;
        }
        else if(!flgAgreg && (objBudgetEventExport.BI_MM_Partaker_first_name__c == null || objBudgetEventExport.BI_MM_Partaker_last_name__c == null
            || objBudgetEventExport.BI_MM_Partaker_phone__c == null || objBudgetEventExport.BI_MM_Partaker_email__c == null)){
                strErrorMsg = Label.BI_MM_Partaker_error;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strErrorMsg));
                errorFlag = false;
                flgPartakerInfo = false;
                System.debug('***Error partaker');
        }
        else if(flgServTrans){
            if(objBudgetEventExport.BI_MM_Service_Transport_Type_Dep__c == null ||
            objBudgetEventExport.BI_MM_Service_Transport_Type_Ret__c == null ||
            objBudgetEventExport.BI_MM_Service_Transport_Ori_Dep__c == null ||
            objBudgetEventExport.BI_MM_Service_Transport_Ori_Ret__c == null ||
            objBudgetEventExport.BI_MM_Service_Transport_Des_Dep__c == null ||
            objBudgetEventExport.BI_MM_Service_Transport_Des_Ret__c == null ||
            objBudgetEventExport.BI_MM_Service_Transport_Date_Dep__c == null ||
            objBudgetEventExport.BI_MM_Service_Transport_Date_Ret__c == null)
                errorFlag = false;
        }
        else if(flgServAcomm){
            if(objBudgetEventExport.BI_MM_Service_Accommodation_Date_Arrival__c == null ||
                objBudgetEventExport.BI_MM_Service_Accommodation_Date_Dep__c == null)
                errorFlag = false;
        }
        else if(flgServRegis){
            if(objBudgetEventExport.BI_MM_Service_Registration_Type__c == null)
                errorFlag = false;
        }

        //If there is no error, enable the button

        System.debug('***errorFlag = '+errorFlag);

        if(errorFlag == true){
            exportButton = true;
        } else{
            exportButton = false;
        }

        return errorFlag;
    }

    public pageReference redirectToExportPage() {
        PageReference pageRef = new PageReference('/apex/' + exportPage);
        pageRef.getParameters().put('Id', objBudgetEventExport.Id);
        pageRef.setRedirect(true);
        system.debug('*** BI_MM_BudgetEventExportCtrl - redirectToExportPage - pageRef == ' + pageRef);
        return pageRef;
    }

    /**
     * @Description Back button gets back to Budget Event page
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public pageReference goBack(){
        PageReference pageRef = new ApexPages.StandardController(objBudgetEvent).view();
        pageRef.setRedirect(true);

        return pageRef;
    }

}