/*
Name: BI_MM_BudgetEventExportCtrl_Test
Requirement ID: CWT Template
Description: Test for Controller Class BI_MM_BudgetEventExportCtrl.
Version | Author-Email    | Date       | Comment
1.0     | Omega CRM       | 30.05.2017 | initial version
*/
@isTest
private class BI_MM_BudgetEventExportCtrl_Test{
    static BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();
    static User thisUser;
    static User testUser;
    static List<BI_MM_BudgetEvent__c> lstBI_MM_BudgetEvent;
    static Territory territory;
    static Product_vod__c testProduct;
    static BI_MM_Budget__c objBudget;
    static Medical_Event_vod__c medEvent;
    static BI_MM_MirrorTerritory__c objMirrorTerritory;

    static void initUserTerritories(){
        List<Id> usersIds = new List<Id>();
        Map<Id, Id> mapUsersTerrs = new Map<Id, Id>();

        // Get BI_MM_SALES_MANAGER permission set
        PermissionSet permissionManager = [select Id from PermissionSet where Name = 'BI_MM_SALES_MANAGER' limit 1];  

        // Get all users that have BI_MM_SALES_MANAGER permission set assigned
        for(PermissionSetAssignment permAssign :[select Id, AssigneeId from PermissionSetAssignment where PermissionSetId = :permissionManager.Id ]) {
            usersIds.add(permAssign.AssigneeId);
        }

        // Get UsersTerritorries for users that have BI_MM_SALES_MANAGER permission and  with a valid Territory 
        for( UserTerritory usrTerr : [Select UserId, TerritoryId from UserTerritory where TerritoryId != null and UserId in :usersIds ]) {
            mapUsersTerrs.put(usrTerr.UserId, usrTerr.TerritoryId);
        }
        
        // select a user which is Active, has an assigned Territory and has a valid Country Code 
        testUser = [select Id, ProfileId, Name, Country_Code_BI__c from User where isActive = true and Id in :mapUsersTerrs.keySet() and Country_Code_BI__c != null limit 1];

        // Select the territory assigned to to user
        territory = [select Id, Name from Territory where Id = :mapUsersTerrs.get(testUser.Id)];
    }

    static void createData(){
    	// Get record type
    	Id colloborationRecordTypeId = [select Id from RecordType where Sobjecttype='Medical_Event_vod__c' and DeveloperName = 'Medical_Event'][0].Id;
    	
    	// Insert Mirror Territory
    	objMirrorTerritory = dataFactory.generateMirrorTerritory ('sr01Test', territory.Id);
    	
    	// Insert a custom setting for the country code of the current user
        BI_MM_CountryCode__c objCountryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c, 'Medical_Event'); //Migart
                
        // Insert Product Catelog   	
    	testProduct = dataFactory.generateProduct('TestProduct', 'Detail', testUser.Country_Code_BI__c);
    	
    	// Insert spain Budget
    	objBudget  = dataFactory.generateBudgets(1, objMirrorTerritory.Id, null, 'Micro Marketing', testProduct, 4000, true).get(0);
        BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();  
 
 		// insert Event Program
        Event_Program_BI__c objEventProgram = dataFactory.generateEventProgram('TestCollProgram', testUser.Country_Code_BI__c, 'Manager','TestCollProgram'); //Migart
        //Event_Program_BI__c objEventProgram = dataFactory.generateEventProgram('Test Program With Approve', testUser.Country_Code_BI__c, 'Manager','Test Program With Approve');
        
        // Insert new custom setting for the Events Programs
        BI_MM_CollProgramId__c objCollProgramId = dataFactory.generateCsProgram(objEventProgram.Id, objEventProgram.Name);
        dataFactory.generateCsApproval(testUser.Country_Code_BI__c);
        
		// Insert new custom setting for BI_MM_Profile_program__c to relate profile ids to programs @Migart
		BI_MM_Profile_program__c programProfile= dataFactory.generateProfileProgram('Test Program Profile With Approve', objEventProgram, testUser.ProfileId);

        medEvent = dataFactory.generateMedicalEvent(colloborationRecordTypeId, 'TestMedEvent', testProduct, objEventProgram, 'New / In Progress');
        //List<Event_Expenses_BI__c> eventExpenses = dataFactory.generateBulkEventExpenses(new List<Medical_Event_vod__c>{medEvent}, testUser, 100);
        Event_Team_Member_BI__c eventTeamMember = new Event_Team_Member_BI__c(Event_Management_BI__c = medEvent.Id, User_BI__c = UserInfo.getUserId());
        insert eventTeamMember;
        
        System.debug('***medEvent = '+ medEvent);

        // Query the list of Event Attendees related to the Medical Event
        List<Event_Attendee_vod__c> attendees = new List<Event_Attendee_vod__c>([select Id, Name, Attendee_Type_vod__c from Event_Attendee_vod__c 
                                                                                         where Medical_Event_vod__c = :medEvent.Id]);

        System.debug('***Attendees = '+ attendees);
        // Insert new Event Expense related to the Event Team Member
        Event_Expenses_BI__c eventExpense = new Event_Expenses_BI__c(Event_Team_Member_BI__c = eventTeamMember.Id, Expense_Type_BI__c ='Local Ticket', 
                                                                             Expense_Date_BI__c = System.today(), Amount_BI__c = 150);
        insert eventExpense;

        lstBI_MM_BudgetEvent = [SELECT Id, BI_MM_Event_ID_BI__c FROM BI_MM_BudgetEvent__c WHERE BI_MM_EventID__c =:medEvent.Id];
        System.debug('***lstBI_MM_BudgetEvent = '+lstBI_MM_BudgetEvent);
        
        // Insert custom setting for Event_Export_Config Detail
        BI_MM_Budget_Event_Export_Config__c CSEventExportDetail = new BI_MM_Budget_Event_Export_Config__c(Name = objEventProgram.Name , 
        BI_MM_Event_Program_ID__c = objEventProgram.Id, BI_MM_Export_Page__c = 'BI_MM_BudgetEventExportCWT');
        System.debug('***CSEventExportDetail = '+CSEventExportDetail);
        
        insert CSEventExportDetail;

    }

    static testMethod void BI_MM_BudgetEventExportCtrl_All(){
        initUserTerritories();

        System.runAs(testUser) {
            createData();
            
            Test.startTest();

                ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(lstBI_MM_BudgetEvent[0]);

                //first we link the budget
                PageReference pageRefLink = Page.BI_MM_BudgetEvent;
                Test.setCurrentPage(pageRefLink);
                ApexPages.Standardcontroller scLink = new ApexPages.Standardcontroller(lstBI_MM_BudgetEvent[0]);
                ApexPages.currentPage().getParameters().put('Id', lstBI_MM_BudgetEvent[0].id);

                // Call public methods
                BI_MM_BudgetEventController objBudgetEventCtr = new BI_MM_BudgetEventController(sc);
                objBudgetEventCtr.selectedProduct = testProduct.id;
                objBudgetEventCtr.strSelectedBudget = objBudget.Id;
                objBudgetEventCtr.selectedBudgetType = 'Micro Marketing';
                objBudgetEventCtr.fetchAllBudget();                
                objBudgetEventCtr.SaveRecord();

                PageReference pageRef = Page.BI_MM_BudgetEventExport;
                Test.setCurrentPage(pageRef);
                
                ApexPages.currentPage().getParameters().put('BEvId', lstBI_MM_BudgetEvent[0].BI_MM_Event_ID_BI__c);
                BI_MM_BudgetEventExportCtrl controller = new BI_MM_BudgetEventExportCtrl(sc);

                controller.export();
                controller.onChangeApplTerr();
                controller.objBudgetEventExport.BI_MM_Applicant_Territory__c = objMirrorTerritory.Id;
                controller.objBudgetEventExport.BI_MM_Applicant_phone__c='9155555555';//migart
                //controller.objBudgetEventExport.BI_MM_Partaker_Type__c = '1. Supply ONEKEY';//migart
                controller.objBudgetEventExport.BI_MM_Partaker_Type__c = '5. Several Partakers';//migart
                controller.objBudgetEventExport.BI_MM_Partaker_ONEKEY__c = 'ONEKEY';//migart
                controller.objBudgetEventExport.BI_MM_Partaker_Category__c = 'Assistant';
                controller.objBudgetEventExport.BI_MM_Service_Transport__c = true; //migart
                controller.objBudgetEventExport.BI_MM_Service_Accommodation__c = true; //migart
                controller.objBudgetEventExport.BI_MM_Service_Registration__c = true; //migart
                controller.objBudgetEventExport.BI_MM_Service_Short_Meeting__c = true; //migart
                controller.onChangeApplTerr();
                controller.objBudgetEventExport.BI_MM_Event_Location__c = 'testLoc';
                controller.onChangeEventData();
                controller.objBudgetEventExport.BI_MM_Event_Cost_Center__c = 1234;
                controller.onChangeEventData();
                controller.onChangePartType();
                controller.onChangeServTrans();
                controller.onChangeServAccom();
                controller.onChangeServRegis();
                controller.onChangeServShort();
                controller.export();
            // System.assertEquals(bdgBeforeTransfer.BI_MM_AvailableBudget__c + amount , bdgAfterTransfer.BI_MM_AvailableBudget__c, 'ERROR While transfering Budget.');

            Test.stopTest();
        }
    }
}