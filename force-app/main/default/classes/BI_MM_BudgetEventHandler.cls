/*
Name: BI_MM_BudgetEventHandler
Requirement ID: collaboration
Description: This Class is used to handle collaboration functionality
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari | 02.01.2016 | initial version
*/
public without sharing class BI_MM_BudgetEventHandler extends IMP_BI_TriggerHandler{

    private static Set<String> setCountryCode = new Set<String>();
    private static String strCountryCode;
    private static Map<String, String> mapMyFieldTranslationForStatus = new Map<String, String>();
    private static Set<String> setCollProgramsId = new Set<String>();

    //private static List<BI_MM_Budget__c> lstBudgetsToUpdate = new List<BI_MM_Budget__c>();

    /* Constructor */
    public BI_MM_BudgetEventHandler() {}

    Static{
        //this.setMaxLoopCount(3);
        List<BI_MM_CountryCode__c> lstCustomSetting = BI_MM_CountryCode__c.getall().values();
        if(lstCustomSetting != null) {
            for(BI_MM_CountryCode__c objCountryCode : lstCustomSetting)
                setCountryCode.add(objCountryCode.name);
        }
        List<User> lstUser = [SELECT Id, Country_Code_BI__c FROM User WHERE Id =:UserInfo.getUserId()];
        // Get the country code for logged in Users
        for(User objUser : lstUser)
            strCountryCode = objUser.Country_Code_BI__c;

        Schema.DescribeFieldResult objDesFieldRes = BI_MM_BudgetEvent__c.BI_MM_EventStatus__c.getDescribe();
        List<Schema.PicklistEntry> lstPLE = objDesFieldRes.getPicklistValues();

        // Iterate over the Picklist values
        for(Schema.PicklistEntry objPE : lstPLE)
            mapMyFieldTranslationForStatus.put(objPE.value, objPE.label);

        // Get all program ids for Colloboration Type
        List<BI_MM_CollProgramId__c> lstPgrmCustomSetting = BI_MM_CollProgramId__c.getall().values();

        for(BI_MM_CollProgramId__c objPgrm : lstPgrmCustomSetting)
            setCollProgramsId.add(objPgrm.Name);

        List<Event_Program_BI__c> lstPGRM = new List<Event_Program_BI__c>();
        lstPGRM = [Select Id FROM Event_Program_BI__c WHERE External_ID_BI__c IN : setCollProgramsId];
        setCollProgramsId = new Set<String>();
        for(Event_Program_BI__c objPG : lstPGRM)
            setCollProgramsId.add(objPG.Id);

        System.debug('*** BI_MM_BudgetEventHandler - Start - Queries == ' + Limits.getQueries());
        System.debug('*** BI_MM_BudgetEventHandler - Start - Limit queries == ' + Limits.getLimitQueries());
    }

    /*
    *@param mapOldBudgetEvent
    *@param mapNewBudgetEvent
    *@return
    *
    * @changelog
    * 25-Oct-2016 - Jefferson Escobar - Created
    * 15-Dec-2017 - Guillem Vivó - Updated
    */
    public override void beforeUpdate() {
        System.debug('*** BI_MM_BudgetEventHandler - beforeUpdate - Start ');
        // Iterate over the new inserted records
        List<BI_MM_BudgetEvent__c> budgetEvents = (List<BI_MM_BudgetEvent__c>) Trigger.new;
        Set<Id> setMedicalEventId = new Set<Id>();

      //Getting Medical Event reference
        for(BI_MM_BudgetEvent__c objBudgetEvent : budgetEvents) {
            // Get all the Medical Ids into Set
            if(objBudgetEvent.BI_MM_EventID__c != null) setMedicalEventId.add(objBudgetEvent.BI_MM_EventID__c);
        }
        //Getting key value of medical events related to budget events
        Map<Id, Medical_Event_vod__c> mapMedEventIdToMedEventObj = new Map<Id, Medical_Event_vod__c>([SELECT Id,Name, Event_Status_BI__c, Total_Cost_BI__c, Program_BI__r.Name
                                                                                                      FROM Medical_Event_vod__c WHERE Id IN :setMedicalEventId]);
        for(BI_MM_BudgetEvent__c objBudgetEvent : budgetEvents) {
            System.debug('*** BI_MM_BudgetEventHandler - beforeUpdate - objBudgetEvent == ' + objBudgetEvent);
            BI_MM_BudgetEvent__c oldBudgetEvent = (BI_MM_BudgetEvent__c) Trigger.oldMap.get(objBudgetEvent.Id);
            objBudgetEvent.BI_MM_IsBudgetLinked__c = (objBudgetEvent.BI_MM_BudgetID__c == null) ? false : true;

            Medical_Event_vod__c mEvent = mapMedEventIdToMedEventObj.get(objBudgetEvent.BI_MM_EventID__c);
            objBudgetEvent.BI_MM_EventStatus__c = mEvent.Event_Status_BI__c;
            String strStatus = mapMyFieldTranslationForStatus.get(mEvent.Event_Status_BI__c) == null ? mEvent.Event_Status_BI__c : mapMyFieldTranslationForStatus.get(mEvent.Event_Status_BI__c);

            System.debug('*** BI_MM_BudgetEventHandler - beforeUpdate - strStatus == ' + strStatus);

            // If the event is New or Under Approval, the amount is set to the Planned Amount
            if(strStatus == System.Label.BI_MM_New_InProgress || strStatus == System.Label.BI_MM_UnderApproval) {
                objBudgetEvent.BI_MM_PlannedAmount__c = mEvent.Total_Cost_BI__c;
                objBudgetEvent.BI_MM_Committed_Amount__c = 0;
                objBudgetEvent.BI_MM_PaidAmount__c = 0;
            }

            //if the event is approved, the amount is transfered to the Committed Amount
            if(strStatus == System.Label.BI_MM_Approved || strStatus == System.Label.BI_MM_Completed_Closed) {
                objBudgetEvent.BI_MM_PlannedAmount__c = 0;
                objBudgetEvent.BI_MM_Committed_Amount__c = mEvent.Total_Cost_BI__c;
                objBudgetEvent.BI_MM_PaidAmount__c = 0;
            }

            //If it is a col type 1 and it is not paid yet, the amount should be sent to Committed Amount since it is auto approved and is set to Completed/Closed
            // if(mEvent.Program_BI__r.Name.left(1) == '1' && objBudgetEvent.BI_MM_PaidDate__c == null && strStatus == System.Label.BI_MM_Completed_Closed){
            //     objBudgetEvent.BI_MM_Committed_Amount__c = mEvent.Total_Cost_BI__c;
            //     objBudgetEvent.BI_MM_PlannedAmount__c = 0;
            //     objBudgetEvent.BI_MM_PaidAmount__c = 0;
            // }

            //If the event is paid (Paid Date is populated), then the cost goes to paid amount and committed is set to 0
            if(objBudgetEvent.BI_MM_PaidDate__c != null){
                objBudgetEvent.BI_MM_PlannedAmount__c = 0;
                objBudgetEvent.BI_MM_Committed_Amount__c = 0;
                objBudgetEvent.BI_MM_PaidAmount__c = mEvent.Total_Cost_BI__c;
                objBudgetEvent.BI_MM_IsEventPaid__c = true;
            }
            // If status is Rejected then planned amount = Total_Cost_BI__c of Medical Event and and make Planned amount to zero and Budget planned amount to zero
            if(strStatus == System.Label.BI_MM_Rejected) {
                // Double amount = (objBudgetEvent.BI_MM_PlannedAmount__c != null ? objBudgetEvent.BI_MM_PlannedAmount__c : 0);
                objBudgetEvent.BI_MM_PlannedAmount__c = 0;
                objBudgetEvent.BI_MM_Committed_Amount__c = 0;
                objBudgetEvent.BI_MM_PaidAmount__c = 0;
            }

            // If status is Canceled then Amounts = 0, PaidDate is null, and Event flags are false
            if(strStatus == System.Label.BI_MM_Canceled) {
                if(objBudgetEvent.BI_MM_BudgetID__c != null){
                    //lstIdsBudgetsToUpdate.add(objBudgetEvent.BI_MM_BudgetID__c);
                    objBudgetEvent.BI_MM_BudgetID__c = null;
                }
                objBudgetEvent.BI_MM_PlannedAmount__c = 0;
                objBudgetEvent.BI_MM_Committed_Amount__c = 0;
                objBudgetEvent.BI_MM_PaidAmount__c = 0;
                objBudgetEvent.BI_MM_IsBudgetLinked__c = false;
                objBudgetEvent.BI_MM_IsEventPaid__c = false;
                objBudgetEvent.BI_MM_PaidDate__c = null;
            }
            System.debug('*** BI_MM_BudgetEventHandler - beforeUpdate - Status Before == ' + objBudgetEvent.BI_MM_IsEventPaid__c + ' - ' + objBudgetEvent.BI_MM_EventStatus__c);
        }
    }
    // This method is used to fire approval process on updation of budget Event records

    /*
    *@param mapOldBudgetEvent
    *@param mapNewBudgetEvent
    *@return
    *
    * @changelog
    * 25-Oct-2016 - Jefferson Escobar - Created
    * 15-Dec-2017 - Guillem Vivó - Updated
    */
    public override void afterUpdate() {
        System.debug('*** BI_MM_BudgetEventHandler - afterUpdate - Start ');

        List<BI_MM_BudgetEvent__c> budgetEvents = (List<BI_MM_BudgetEvent__c>) Trigger.new;
        List<Medical_Event_vod__c> lstMedEvent = new List<Medical_Event_vod__c>();

        Map<Id, BI_MM_Budget__c> mapBudgetToUpdate = new Map<Id, BI_MM_Budget__c>();
        Map<Id, Id> mapMedicalEventIdToProgramId = new map<Id, Id>();

        Set<Id> setMedicalEventId = new Set<Id>();

        // Identify Medical Event Ids and available Programs
        for(BI_MM_BudgetEvent__c objBudgetEvent : budgetEvents)
            if(objBudgetEvent.BI_MM_EventID__c != null) setMedicalEventId.add(objBudgetEvent.BI_MM_EventID__c);
        lstMedEvent = [SELECT Id, Program_BI__c, Event_Status_BI__c,RecordTypeId, Total_Cost_BI__c, Expense_Amount_vod__c, Expense_Post_Status_vod__c
                        FROM Medical_Event_vod__c WHERE Id IN :setMedicalEventId];
        for(Medical_Event_vod__c objMedEvent : lstMedEvent)
            if(objMedEvent.Program_BI__c != null) mapMedicalEventIdToProgramId.put(objMedEvent.Id, objMedEvent.Program_BI__c);

        // Identify Medical Event Record Type
        // Id colloborationRecordTypeId;
        // if(Schema.SObjectType.Medical_Event_vod__c.getRecordTypeInfosByName().containsKey(System.Label.BI_MM_CollaborationRecordType))
        //     colloborationRecordTypeId = Schema.SObjectType.Medical_Event_vod__c.getRecordTypeInfosByName().get(System.Label.BI_MM_CollaborationRecordType).getRecordTypeId();

        // Iterate over the updated records
        for(BI_MM_BudgetEvent__c objBudgetEvent : budgetEvents) {
            BI_MM_BudgetEvent__c oldBudgetEvent = (BI_MM_BudgetEvent__c) Trigger.oldMap.get(objBudgetEvent.Id);

            // Refresh Budget if there is change in Budget assignment
            if(mapMedicalEventIdToProgramId.containskey(objBudgetEvent.BI_MM_EventID__c) &&
                setCollProgramsId.contains(mapMedicalEventIdToProgramId.get(objBudgetEvent.BI_MM_EventID__c)) &&
                objBudgetEvent.BI_MM_EventID__c != null && setCountryCode.contains(strCountryCode)) {

                if(oldBudgetEvent.BI_MM_BudgetID__c != objBudgetEvent.BI_MM_BudgetID__c){
                    if(oldBudgetEvent.BI_MM_BudgetID__c != null)
                        mapBudgetToUpdate.put(oldBudgetEvent.BI_MM_BudgetID__c, new BI_MM_Budget__c (Id=oldBudgetEvent.BI_MM_BudgetID__c));
                    if(objBudgetEvent.BI_MM_BudgetID__c != null)
                        mapBudgetToUpdate.put(objBudgetEvent.BI_MM_BudgetID__c, new BI_MM_Budget__c (Id=objBudgetEvent.BI_MM_BudgetID__c));
                }
                else if(oldBudgetEvent.BI_MM_PlannedAmount__c != objBudgetEvent.BI_MM_PlannedAmount__c ||
                    oldBudgetEvent.BI_MM_Committed_Amount__c != objBudgetEvent.BI_MM_Committed_Amount__c ||
                    oldBudgetEvent.BI_MM_PaidAmount__c != objBudgetEvent.BI_MM_PaidAmount__c){
                    if(objBudgetEvent.BI_MM_BudgetID__c != null)
                        mapBudgetToUpdate.put(objBudgetEvent.BI_MM_BudgetID__c, new BI_MM_Budget__c (Id=objBudgetEvent.BI_MM_BudgetID__c));
                }

                // mapBudgetToUpdate.put(objBudgetEvent.BI_MM_BudgetID__c, new BI_MM_Budget__c (Id=objBudgetEvent.BI_MM_BudgetID__c));
            }
        }

        System.debug('*** BI_MM_BudgetEventHandler - afterUpdate - mapBudgetToUpdate == ' + mapBudgetToUpdate);

        if(!mapBudgetToUpdate.isEmpty()) {
            update mapBudgetToUpdate.values();
            System.debug('*** BI_MM_BudgetEventHandler - afterUpdate - mapBudgetToUpdate (updated) == ' + mapBudgetToUpdate);
        }
    }
}