/*
Name: BI_MM_BudgetEventHandlerTest
Requirement ID: collaboration
Description: This is test class for BI_MM_BudgetEventHandler
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari |  05/01/2016 | initial version
*/
@isTest
private class BI_MM_BudgetEventHandlerTest {

    /*
        * Method Name : testCollaborationBudgetEvent
        * Description : Created for unit testing of medical event trigger
        * Return Type : None
    */
    private static testMethod void testCollaborationBudgetEvent() {
      BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();

        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];  // Get BI_MM_ADMIN permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and  null Manager
        User testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, null);
        dataFactory.addPermissionSet(testUser, permissionAdmin);

        System.runAs(testUser) {
            // Insert custom setting
            Date dt = system.Today();
            Date endDate = dt.addDays(6);

            // Get record type id for Medical event
            RecordType colloborationRecordType = [select Id, Name, DeveloperName from RecordType where Sobjecttype='Medical_Event_vod__c' and DeveloperName = 'Medical_Event'][0];

            // Insert the custom setting countryCode for the corresponding country code of testUser
            BI_MM_CountryCode__c csCountryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c, colloborationRecordType.DeveloperName);

            // Insert new Event Program
            Event_Program_BI__c objEventProgram = dataFactory.generateEventProgram('TestCollProgram', csCountryCode.Name, null,'TestCollProgram');//migart

            BI_MM_CollProgramId__c objCollProgramId = dataFactory.generateCsProgram(objEventProgram.Id, objEventProgram.Name);
            new BI_MM_CollProgramId__c(Name = 'Type 1 Colloboration',Coll_Program_Id__c = objEventProgram.id);

            // Get User Territory
            UserTerritory objUserTerritory = [SELECT UserId, TerritoryId From UserTerritory WHERE TerritoryId != null limit 1];

            // Insert Mirror Territory
            BI_MM_MirrorTerritory__c objMirrorTerritory = dataFactory.generateMirrorTerritory ('sr01Test', objUserTerritory.Id);

            // Insert Product Catelog
            Product_vod__c objProduct = dataFactory.generateProduct('Test Product', 'Detail', csCountryCode.Name);

            test.startTest();
                // Insert spain Budget
                BI_MM_Budget__c objBudget = dataFactory.generateBudgets(1, objMirrorTerritory.Id, null, 'Micro Marketing', objProduct, 4000, true).get(0);

                // Insert Medical event records
                Medical_Event_vod__c objMedicalEvent = dataFactory.generateMedicalEvent(colloborationRecordType.Id, 'Test Collaboration Event Name', objProduct, objEventProgram,
                                                                                         'New / In Progress');

                BI_MM_RecursionHandler.isInsertTrigger = false;

                List<BI_MM_BudgetEvent__c> lstBudEvent = new List<BI_MM_BudgetEvent__c>([Select Id, BI_MM_BudgetID__c FROM BI_MM_BudgetEvent__c
                                                                                         WHERE BI_MM_EventID__c =: objMedicalEvent.Id]);

                System.assertEquals(1, lstBudEvent.size(), 'It should be one BudgetEvent object created when inserting a new medicalEvent object');

                objMedicalEvent.Event_Status_BI__c = 'Approved';
                update objMedicalEvent;
                lstBudEvent = new List<BI_MM_BudgetEvent__c>([select Id, BI_MM_EventStatus__c from BI_MM_BudgetEvent__c where BI_MM_EventID__c =: objMedicalEvent.Id]);
                System.assertEquals('Approved', lstBudEvent.get(0).BI_MM_EventStatus__c, 'The status of Budget Event should be the Same as the medicalEvent : "Approved"');
                
                //if the event is approved, the amount is transfered to the Committed Amount
                objMedicalEvent.Event_Status_BI__c = 'Closed'; //Migart
                update objMedicalEvent;
                
                //If the event is paid (Paid Date is populated), then the cost goes to paid amount and committed is set to 0
               /* BI_MM_BudgetEvent__c objBudgetEvent;
                objBudgetEvent.BI_MM_PaidDate__c = system.Today();//Migart
                update objBudgetEvent;*/
                // If status is Rejected then planned amount = Total_Cost_BI__c of Medical Event and and make Planned amount to zero and Budget planned amount to zero 117
                objMedicalEvent.Event_Status_BI__c = 'Rejected'; //Migart
                update objMedicalEvent;               

				// If status is Canceled then Amounts = 0, PaidDate is null, and Event flags are false 125
                objMedicalEvent.Event_Status_BI__c = 'Canceled'; //Migart
                update objMedicalEvent;       				

                BI_MM_RecursionHandler.isBool = true;

            test.stopTest();
        }

    }
}