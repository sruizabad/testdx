public class  BI_MM_BudgetExpenseHandler {
      
      public void onAfterUpdate(Map<Id, BI_MM_BudgetEvent__c> mapOldBudgetEvent, Map<Id, BI_MM_BudgetEvent__c> mapNewBudgetEvent){
          
          //Query for Link Budget Event Records
          list<BI_MM_BudgetEvent__c> budgetEventList = [select id,BI_MM_PaidAmount__c,BI_MM_EventID__c,BI_MM_IsEventPaid__c from BI_MM_BudgetEvent__c where id in : mapNewBudgetEvent.keyset()];
          
          Map<id,id> mapIdMedicalEventToBudget = new Map<id,id>();
          
          // Adding Link Budget Event id and Event management id of the record into map
          for(BI_MM_BudgetEvent__c medicalEventVar : budgetEventList ) {
          
                  if(!mapIdMedicalEventToBudget.containsKey(medicalEventVar.id))
                  
                  mapIdMedicalEventToBudget.put(medicalEventVar.id,medicalEventVar.BI_MM_EventID__c);
          }
          
          // Query for all the event team members related to the Event Management
          
          list<Event_Team_Member_BI__c> eventTeamMemberList = [select id,Event_Management_BI__c   from Event_Team_Member_BI__c where Event_Management_BI__c in: mapIdMedicalEventToBudget.values() ];
          
          Map<id,id> mapIdEventTeamMemberToEventManagement = new Map<id,id>();
          
           //Adding Event Team member id and Event Management id to map
           for(Event_Team_Member_BI__c teamMemberVar : eventTeamMemberList) {
           
                  if(!mapIdEventTeamMemberToEventManagement.containsKey(teamMemberVar.Event_Management_BI__c))
                   
                  mapIdEventTeamMemberToEventManagement.put(teamMemberVar.Event_Management_BI__c,teamMemberVar.id);
           }
          
          // Query all the Event Expenses related to event team member
          list<Event_Expenses_BI__c> eventExpenseList = [select id,Amount_BI__c,Event_Team_Member_BI__c from Event_Expenses_BI__c where Event_Team_Member_BI__c in: mapIdEventTeamMemberToEventManagement.Values()];
          
          Map<id,Event_Expenses_BI__c>  mapIdToEventExpense = new Map<id,Event_Expenses_BI__c>();
          
          // Adding event team member id and event expense Object to Map
          for(Event_Expenses_BI__c eventExpenseVar : eventExpenseList ) {
          
              if(!mapIdToEventExpense.containsKey(eventExpenseVar.Event_Team_Member_BI__c))
              
                  mapIdToEventExpense.put(eventExpenseVar.Event_Team_Member_BI__c , eventExpenseVar);
          
          }
          // List to be updated
          list<Event_Expenses_BI__c> expenseListToBeUpdated = new List<Event_Expenses_BI__c>();
          try{
          
              for(BI_MM_BudgetEvent__c budgetEventObj : budgetEventList){
                  // Only if isPaid is true
                  if(budgetEventObj.BI_MM_IsEventPaid__c){
                                                              
                      Event_Expenses_BI__c objToUpdate = mapIdToEventExpense.get(mapIdEventTeamMemberToEventManagement
                                                                            .get(mapIdMedicalEventToBudget
                                                                            .get(budgetEventObj.id)));
                      objToUpdate.Amount_BI__c = budgetEventObj.BI_MM_PaidAmount__c;                                                   
                      expenseListToBeUpdated.add(objToUpdate);    
                  }
              }
          
              if(!expenseListToBeUpdated.isEmpty())
              database.update(expenseListToBeUpdated,false);
   
          }catch(Exception Ex){
   
              system.debug('@@@@@@@@@ Exception @@@@@@@');
              system.debug('@@@@@@@@@ Exception Message @@@@@@@'+ex.getMessage());
              system.debug('@@@@@@@@@ Exception Line Number @@@@@@@'+ex.getLineNumber());
   
          }
      }
}