/*
Name: BI_MM_BudgetHandler
Requirement ID: Budget Trigger
Description: Trigger to update budget records distributed amount.
Version | Author-Email | Date | Comment
1.0 | Mukesh | 18.01.2016 | initial version
*/
public without sharing class BI_MM_BudgetHandler {

	public final string PERMISSION_SET = 'BI_MM_ADMIN';
	public final string PROFILE_NAME = 'ADMIN';

    /* Constructor */
    public BI_MM_BudgetHandler() {
        /* Do nothing */
    }

    public void onAfterInsert(Map<Id, BI_MM_Budget__c> mapOldBudget, Map<Id, BI_MM_Budget__c> mapNewBudget) {
		System.debug('*** BI_MM_BudgetHandler - onAfterInsert - Start');
        insertBudgetStageAudit(mapNewBudget);
        calculateDistributedAmount(mapNewBudget);
        calculateMasterAmount(mapNewBudget);
        //checkAvailableAmountOK(mapOldBudget, mapNewBudget);
    }

    public void onBeforeUpdate(Map<Id, BI_MM_Budget__c> mapNewBudget) {
		System.debug('*** BI_MM_BudgetHandler - onBeforeUpdate - Start');
        calculateBudgetAmounts(mapNewBudget);
    }

    public void onAfterUpdate(Map<Id, BI_MM_Budget__c> mapOldBudget, Map<Id, BI_MM_Budget__c> mapNewBudget) {
		System.debug('*** BI_MM_BudgetHandler - onAfterUpdate - Start');
        calculateDistributedAmount(mapNewBudget);
        calculateMasterAmount(mapNewBudget);
        checkAvailableAmountOK(mapOldBudget, mapNewBudget);
    }

    public void onAfterDelete(Map<Id, BI_MM_Budget__c> mapOldBudget, Map<Id, BI_MM_Budget__c> mapNewBudget) {
		System.debug('*** BI_MM_BudgetHandler - onAfterDelete - Start');
        deleteDistributedAmount(mapOldBudget);
        calculateMasterAmount(mapOldBudget);
    	System.debug('*** BI_MM_BudgetHandler - onAfterDelete - mapOldBudget == ' + mapOldBudget);
    	System.debug('*** BI_MM_BudgetHandler - onAfterDelete - mapNewBudget == ' + mapNewBudget);
    }

    // Calculate Distribute Amount of Updated Budgets' Parent Budget
    private void calculateDistributedAmount(Map<Id, BI_MM_Budget__c> mapNewBudget) {
        /* When a Budget is inserted/updated, Distributed Amount of Parent Budget can be affected.
            Function gets the Parent Budgets of the updated Budgets, and updates the Distributed Amount of Parent Budget
            using updated Budgets' and "brothers'" (children of the same Parent) Current Amounts.
        */

        // Change Key fields for Formulas
		System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - Start');

        Map<Id, Double> mapBudgetIdToDistribAmt = new Map<Id, Double>();
        Map<Id, List<BI_MM_Budget__c>> mapBudgetIdToDistributed = new Map<Id, List<BI_MM_Budget__c>>();

        Map<Id, BI_MM_Budget__c> mapParentBudget = new Map<Id, BI_MM_Budget__c>();
        Map<Id, BI_MM_Budget__c> mapChildrenBudget = new Map<Id, BI_MM_Budget__c>();

        List<BI_MM_Budget__c> lstNewBudget = new List<BI_MM_Budget__c>();
        List<BI_MM_Budget__c> lstParentBudget = new List<BI_MM_Budget__c>();
        List<BI_MM_Budget__c> lstUpdateBudget = new List<BI_MM_Budget__c>();
        List<BI_MM_Budget__c> lstChildBudget = new List<BI_MM_Budget__c>();
        List<BI_MM_Budget__c> lstParentBudgetToUpdate = new List<BI_MM_Budget__c>();

        /*  Create List of Parent Budgets of Updates (if needed, can be filtered by changes on Current Budget amount)
            Parent.BI_MM_Child_Key__c = Budget.BI_MM_Parent_Key__c
        */

        // Get Budgets from the Org with the Ids from mapNewBudget
        Set<String> parentKey = new Set<String>();
        Set<String> childKey = new Set<String>();

        lstNewBudget = [SELECT Id, Name, BI_MM_TerritoryName__c, BI_MM_ManagerTerritory__c, BI_MM_TimePeriod__c, BI_MM_BudgetType__c,
                        BI_MM_CurrentBudget__c, BI_MM_DistributedBudget__c, BI_MM_Child_Key__c, BI_MM_Parent_Key__c,
                        BI_MM_ProductName__r.Name, BI_MM_TerritoryName__r.Name, BI_MM_ManagerTerritory__r.Name, BI_MM_Is_active__c
                        FROM BI_MM_Budget__c
                        WHERE BI_MM_ManagerTerritory__c != null AND BI_MM_TerritoryName__c != null AND BI_MM_BudgetType__c != null
                        AND BI_MM_ProductName__c != null AND BI_MM_TimePeriod__c != null AND BI_MM_Is_active__c = true AND Id in : mapNewBudget.keySet()];
        System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - lstNewBudget == ' + lstNewBudget);

        for(BI_MM_Budget__c budget : lstNewBudget) {
            parentKey.add(budget.BI_MM_Parent_Key__c);
            childKey.add(budget.BI_MM_Child_Key__c);
        }
		System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - parentKey == ' + parentKey);
		System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - childKey == ' + childKey);

        lstParentBudget = [SELECT Id, Name, BI_MM_TerritoryName__c, BI_MM_ManagerTerritory__c, BI_MM_TimePeriod__c, BI_MM_BudgetType__c,
                            BI_MM_CurrentBudget__c, BI_MM_DistributedBudget__c, BI_MM_Child_Key__c, BI_MM_Parent_Key__c,
                            BI_MM_ProductName__r.Name, BI_MM_TerritoryName__r.Name, BI_MM_ManagerTerritory__r.Name, BI_MM_Is_active__c
                            FROM BI_MM_Budget__c
                            WHERE BI_MM_ManagerTerritory__c != null AND BI_MM_TerritoryName__c != null AND BI_MM_BudgetType__c != null
                            AND BI_MM_ProductName__c != null AND BI_MM_TimePeriod__c != null AND BI_MM_Is_active__c = true AND BI_MM_Child_Key__c in : parentKey];
        System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - lstParentBudget == ' + lstParentBudget);

        /* Using Parent List, Create List of children Budgets
            Children.BI_MM_Parent_Key__c = Budget.BI_MM_Child_Key__c
        */
        lstUpdateBudget = [SELECT Id, Name, BI_MM_TerritoryName__c, BI_MM_ManagerTerritory__c, BI_MM_TimePeriod__c, BI_MM_BudgetType__c,
                            BI_MM_CurrentBudget__c, BI_MM_DistributedBudget__c, BI_MM_Child_Key__c, BI_MM_Parent_Key__c,
                            BI_MM_ProductName__r.Name, BI_MM_TerritoryName__r.Name, BI_MM_ManagerTerritory__r.Name, BI_MM_Is_active__c
                            FROM BI_MM_Budget__c
                            WHERE BI_MM_ManagerTerritory__c != null AND BI_MM_TerritoryName__c != null AND BI_MM_BudgetType__c != null
                            AND BI_MM_ProductName__c != null AND BI_MM_TimePeriod__c != null AND BI_MM_Is_active__c = true AND BI_MM_Parent_Key__c in : parentKey];
		System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - lstUpdateBudget == ' + lstUpdateBudget);

        lstChildBudget = [SELECT Id, Name, BI_MM_TerritoryName__c, BI_MM_ManagerTerritory__c, BI_MM_TimePeriod__c, BI_MM_BudgetType__c,
                            BI_MM_CurrentBudget__c, BI_MM_DistributedBudget__c, BI_MM_Child_Key__c, BI_MM_Parent_Key__c,
                            BI_MM_ProductName__r.Name, BI_MM_TerritoryName__r.Name, BI_MM_ManagerTerritory__r.Name, BI_MM_Is_active__c
                            FROM BI_MM_Budget__c
                            WHERE BI_MM_ManagerTerritory__c != null AND BI_MM_TerritoryName__c != null AND BI_MM_BudgetType__c != null
                            AND BI_MM_ProductName__c != null AND BI_MM_TimePeriod__c != null AND BI_MM_Is_active__c = true AND BI_MM_Parent_Key__c in : childKey];
        System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - lstChildBudget == ' + lstChildBudget);

        // Using lists, create Maps for Parents and Children Budgets
        for(BI_MM_Budget__c parentBudget : lstParentBudget) mapParentBudget.put(parentBudget.id, parentBudget);
        for(BI_MM_Budget__c newBudget : lstNewBudget) mapParentBudget.put(newBudget.id, newBudget);
        for(BI_MM_Budget__c updateBudget : lstUpdateBudget) mapChildrenBudget.put(updateBudget.id, updateBudget);
        for(BI_MM_Budget__c childBudget : lstChildBudget) mapChildrenBudget.put(childBudget.id, childBudget);

        /* Using Parent and Updated Maps, Create Map of Parent to Lists of children Budgets
            Children.BI_MM_Parent_Key__c = Budget.BI_MM_Child_Key__c
        */
        for(BI_MM_Budget__c parentBudget : mapParentBudget.values()) {
            mapBudgetIdToDistributed.put(parentBudget.id, new List<BI_MM_Budget__c>{});
            for(BI_MM_Budget__c childBudget : mapChildrenBudget.values()){
                if (parentBudget.BI_MM_Child_Key__c == childBudget.BI_MM_Parent_Key__c)
                	mapBudgetIdToDistributed.get(parentBudget.id).add(childBudget);
            }
        }
		System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - mapBudgetIdToDistributed == ' + mapBudgetIdToDistributed);

        // Get map of Sums of Children Current values for updated budgets Distribute Amounts
        for(Id updateBudgetId : mapBudgetIdToDistributed.keySet()) {
            Double sumDistribution = 0;
            for(BI_MM_Budget__c objBudget : mapBudgetIdToDistributed.get(updateBudgetId))
                sumDistribution = sumDistribution + (objBudget.BI_MM_CurrentBudget__c != null ? objBudget.BI_MM_CurrentBudget__c : 0);
            mapBudgetIdToDistribAmt.put(updateBudgetId, sumDistribution);
        }
		System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - mapBudgetIdToDistribAmt == ' + mapBudgetIdToDistribAmt);

        // Create List of Budgets to update with Distributed Amounts
        for(String objUpdateBudgetId : mapBudgetIdToDistribAmt.keySet()) {

            lstParentBudgetToUpdate.add(new BI_MM_Budget__c(
            Id = objUpdateBudgetId, BI_MM_DistributedBudget__c = mapBudgetIdToDistribAmt.get(objUpdateBudgetId)));
        }

        // Update List of Parent Budgets
		System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - lstParentBudgetToUpdate == ' + lstParentBudgetToUpdate);
        if(!lstParentBudgetToUpdate.isEmpty()) {
            // database.update(lstParentBudgetToUpdate, true);
            database.update(lstParentBudgetToUpdate, false);
        }
    }

    // This method is used to calculate total Paid and Planned amounts for Budget
    private void calculateBudgetAmounts(Map<Id, BI_MM_Budget__c> mapNewBudget) {

		System.debug('*** BI_MM_BudgetHandler - calculateBudgetAmounts - Start');
        Map<Id, List<BI_MM_BudgetEvent__c>> mapBudgetIdToBudEvents = new Map<Id, List<BI_MM_BudgetEvent__c>>();
        Map<Id, Double> mapBudgetIdToPlannedAmount = new Map<Id, Double>();
        Map<Id, Double> mapBudgetIdToPaidAmount = new Map<Id, Double>();
        Map<Id, Double> mapBudgetIdToCommittedAmount = new Map<Id, Double>();

        List<BI_MM_Budget__c> lstBudget = new List<BI_MM_Budget__c>();
        // Get Budget Events of Budgets
        List<BI_MM_BudgetEvent__c> lstBudgetEvent = [SELECT Id, BI_MM_IsEventPaid__c, BI_MM_PaidAmount__c, BI_MM_PlannedAmount__c,BI_MM_ProductName__c,
                                                    BI_MM_BudgetID__c, BI_MM_BudgetID__r.BI_MM_ProductName__c,BI_MM_ProductID__c, BI_MM_Committed_Amount__c,
                                                    BI_MM_EventID__r.Event_Status_BI__c
                                                    FROM BI_MM_BudgetEvent__c
                                                    WHERE BI_MM_BudgetID__c IN : mapNewBudget.keySet()];

        // Get Map of Budget Events for each Budget
        if(lstBudgetEvent != null) {
            for(BI_MM_BudgetEvent__c objBuEvent : lstBudgetEvent) {
                if(!mapBudgetIdToBudEvents.containsKey(objBuEvent.BI_MM_BudgetID__c))
                mapBudgetIdToBudEvents.put(objBuEvent.BI_MM_BudgetID__c, new List<BI_MM_BudgetEvent__c>{objBuEvent});
                else mapBudgetIdToBudEvents.get(objBuEvent.BI_MM_BudgetID__c).add(objBuEvent);
            }
        }

        // Using Map of Budget Events for each Budget, obtain Totals Paid and Planned Amounts
        for(Id objBudgetId : mapNewBudget.keySet()) {

            Double sumPlanedAmount = 0;
            Double sumPaidAmount = 0;
            Double sumCommittedAmount = 0;
            if(mapBudgetIdToBudEvents.containskey(objBudgetId)) {

                for(BI_MM_BudgetEvent__c objBudgetEvent : mapBudgetIdToBudEvents.get(objBudgetId)) {
                    if(objBudgetEvent.BI_MM_ProductID__c == objBudgetEvent.BI_MM_BudgetID__r.BI_MM_ProductName__c) {
                        if(objBudgetEvent.BI_MM_IsEventPaid__c == false) {
                            sumPlanedAmount = sumPlanedAmount + (objBudgetEvent.BI_MM_PlannedAmount__c != null ? objBudgetEvent.BI_MM_PlannedAmount__c : 0);

                        }
                        sumCommittedAmount = sumCommittedAmount + (objBudgetEvent.BI_MM_Committed_Amount__c != null ? objBudgetEvent.BI_MM_Committed_Amount__c : 0);
                        if(objBudgetEvent.BI_MM_IsEventPaid__c == true) {
                            sumPaidAmount = sumPaidAmount + (objBudgetEvent.BI_MM_PaidAmount__c != null ? objBudgetEvent.BI_MM_PaidAmount__c : 0);
                        }
                    }
                }
                mapBudgetIdToPlannedAmount.put(objBudgetId, sumPlanedAmount);
                mapBudgetIdToPaidAmount.put(objBudgetId, sumPaidAmount);
                mapBudgetIdToCommittedAmount.put(objBudgetId, sumCommittedAmount);
            }
        }

        // Update Budgets with Totals Paid and Planned Amounts
        for(BI_MM_Budget__c objUpdateBudget : mapNewBudget.values()) {
            objUpdateBudget.BI_MM_PaidAmount__c = mapBudgetIdToPaidAmount.get(objUpdateBudget.Id) != null ? mapBudgetIdToPaidAmount.get(objUpdateBudget.Id) : 0;
            objUpdateBudget.BI_MM_PlannedAmount__c = mapBudgetIdToPlannedAmount.get(objUpdateBudget.Id) != null ? mapBudgetIdToPlannedAmount.get(objUpdateBudget.Id) : 0;
            objUpdateBudget.BI_MM_Committed_Amount__c = mapBudgetIdToCommittedAmount.get(objUpdateBudget.Id) != null ? mapBudgetIdToCommittedAmount.get(objUpdateBudget.Id) : 0;
        }
    }

    private void deleteDistributedAmount(Map<Id, BI_MM_Budget__c> mapOldBudget) {

        /* When a Budget is deleted, Distributed Amount of Parent Budget can be affected.
            Function gets the Parent Budgets of the updated Budgets, and updates the Distributed Amount of Parent Budget
            using "brothers'" (children of the same Parent) Current Amounts.
        */
		System.debug('*** BI_MM_BudgetHandler - deleteDistributedAmount - Start');
        Map<Id, Double> mapBudgetIdToDistribAmt = new Map<Id, Double>();
        Map<Id, List<BI_MM_Budget__c>> mapBudgetIdToDistributed = new Map<Id, List<BI_MM_Budget__c>>();

        List<BI_MM_Budget__c> lstParentBudget = new List<BI_MM_Budget__c>();
        List<BI_MM_Budget__c> lstUpdateBudget = new List<BI_MM_Budget__c>();
        List<BI_MM_Budget__c> lstParentBudgetToUpdate = new List<BI_MM_Budget__c>();

        /*  Create List of Parent Budgets of Updates
            Parent.BI_MM_Child_Key__c = Budget.BI_MM_Parent_Key__c
        */
        Set<String> setDeletedKey = new Set<String>();
        for(BI_MM_Budget__c budget : mapOldBudget.values()) setDeletedKey.add(budget.Id);
        Set<String> setChildKey = new Set<String>();
        for(BI_MM_Budget__c budget : mapOldBudget.values()) setChildKey.add(budget.BI_MM_Parent_Key__c);
        lstParentBudget = [SELECT Id, Name, BI_MM_TerritoryName__c, BI_MM_ManagerTerritory__c, BI_MM_TimePeriod__c, BI_MM_BudgetType__c, BI_MM_ProductName__r.Name,
                            BI_MM_CurrentBudget__c, BI_MM_DistributedBudget__c, BI_MM_Child_Key__c, BI_MM_Parent_Key__c
                            FROM BI_MM_Budget__c
                            WHERE BI_MM_ManagerTerritory__c != null AND BI_MM_TerritoryName__c != null AND BI_MM_BudgetType__c != null
                            AND BI_MM_ProductName__c != null AND BI_MM_TimePeriod__c != null AND BI_MM_Child_Key__c in : setChildKey
                            AND Id NOT in : setDeletedKey];

        /* Using Parent List, Create List of children Budgets
            Children.BI_MM_Parent_Key__c = Budget.BI_MM_Child_Key__c
        */
        lstUpdateBudget = [SELECT Id, Name, BI_MM_TerritoryName__c, BI_MM_ManagerTerritory__c, BI_MM_TimePeriod__c, BI_MM_BudgetType__c, BI_MM_ProductName__r.Name,
                            BI_MM_CurrentBudget__c, BI_MM_DistributedBudget__c, BI_MM_Child_Key__c, BI_MM_Parent_Key__c
                            FROM BI_MM_Budget__c
                            WHERE BI_MM_ManagerTerritory__c != null AND BI_MM_TerritoryName__c != null AND BI_MM_BudgetType__c != null
                            AND BI_MM_ProductName__c != null AND BI_MM_TimePeriod__c != null AND BI_MM_Parent_Key__c in : setChildKey
                            AND Id NOT in : setDeletedKey];

        /* Using Parent and Updated Lists, Create Map of Parent to Lists of children Budgets
            Children.BI_MM_Parent_Key__c = Budget.BI_MM_Child_Key__c
        */
        for(BI_MM_Budget__c parentBudget : lstParentBudget) {

            mapBudgetIdToDistributed.put(parentBudget.id, new List<BI_MM_Budget__c>{});

            for(BI_MM_Budget__c childBudget : lstUpdateBudget){
                if (parentBudget.BI_MM_Child_Key__c == childBudget.BI_MM_Parent_Key__c)
                mapBudgetIdToDistributed.get(parentBudget.id).add(childBudget);
            }
        }

        // Get map of Sums of Children Current values for updated budgets Distribute Amounts
        for(Id updateBudgetId : mapBudgetIdToDistributed.keySet()) {
            Double sumDistribution = 0;
            for(BI_MM_Budget__c objBudget : mapBudgetIdToDistributed.get(updateBudgetId))
            sumDistribution = sumDistribution + (objBudget.BI_MM_CurrentBudget__c != null ? objBudget.BI_MM_CurrentBudget__c : 0);
            mapBudgetIdToDistribAmt.put(updateBudgetId, sumDistribution);
        }

        // Create List of Budgets to update with Distributed Amounts
        for(String objUpdateBudgetId : mapBudgetIdToDistribAmt.keySet()) {
            lstParentBudgetToUpdate.add(new BI_MM_Budget__c(
            Id = objUpdateBudgetId, BI_MM_DistributedBudget__c = mapBudgetIdToDistribAmt.get(objUpdateBudgetId)));
        }

        // Update List of Parent Budgets
		System.debug('*** BI_MM_BudgetHandler - calculateDistributedAmount - lstParentBudgetToUpdate == ' + lstParentBudgetToUpdate);
        if(!lstParentBudgetToUpdate.isEmpty()) {
            database.update(lstParentBudgetToUpdate, true);
        }
    }


    /* This method insert BI_MM_BudgetStageAudit when new BI_MM_Budget__c records inserted with Initial indicator type */
    private void insertBudgetStageAudit(Map<Id, BI_MM_Budget__c> mapNewBudget) {

		System.debug('*** BI_MM_BudgetHandler - insertBudgetStageAudit - Start');
        List<BI_MM_BudgetStageAudit__c> lstBudgetStageAuditToInsert = new List<BI_MM_BudgetStageAudit__c>();

        for(BI_MM_Budget__c objSpainBudget : mapNewBudget.values()) {

            if(objSpainBudget.BI_MM_BudgetIndicator__c == System.Label.BI_MM_Initial) {

                lstBudgetStageAuditToInsert.add(new BI_MM_BudgetStageAudit__c(Name = objSpainBudget.Name,
                BI_MM_BudgetIndicator__c = objSpainBudget.BI_MM_BudgetIndicator__c,
                BI_MM_BudgetType__c = objSpainBudget.BI_MM_BudgetType__c,
                BI_MM_CurrentBudget__c = objSpainBudget.BI_MM_CurrentBudget__c,
                BI_MM_DistributedBudget__c = objSpainBudget.BI_MM_DistributedBudget__c,
                BI_MM_EndDate__c = objSpainBudget.BI_MM_EndDate__c,
                BI_MM_ManagerTerritory__c = objSpainBudget.BI_MM_ManagerTerritory__c,
                BI_MM_PaidAmount__c = objSpainBudget.BI_MM_PaidAmount__c,
                BI_MM_PlannedAmount__c = objSpainBudget.BI_MM_PlannedAmount__c,
                BI_MM_ProductName__c = objSpainBudget.BI_MM_ProductName__c,
                BI_MM_StartDate__c = objSpainBudget.BI_MM_StartDate__c,
                BI_MM_TimePeriod__c = objSpainBudget.BI_MM_TimePeriod__c,
                BI_MM_UserTerritory__c = objSpainBudget.BI_MM_TerritoryName__c,
				BI_MM_Budget_To__c = objSpainBudget.Id));
            }

        }
		System.debug('*** BI_MM_BudgetHandler - insertBudgetStageAudit - lstBudgetStageAuditToInsert == ' + lstBudgetStageAuditToInsert);
        if(!lstBudgetStageAuditToInsert.isEmpty()){
            List<Database.SaveResult>  sr = Database.insert(lstBudgetStageAuditToInsert, false);
			System.debug('*** BI_MM_BudgetHandler - insertBudgetStageAudit - Save Results == ' + sr);
        }
    }

    /**
    * @Description Method that checks if Budget Available Amount is correct, based on its
    *              negativity allowed per Country (CustomSetting: BI_MM_Negative_budget__c)
    *
    * @param       mapOldBudget  Trigger Old Map
    * @param       mapNewBudget  Trigger New Map
    *
    * @Author      OmegaCRM
	* 28-Abr-2017 OmegaCRM - Updated: Added Master Budget Available Amount validation
    */
    public void checkAvailableAmountOK(Map<Id, BI_MM_Budget__c> mapOldBudget, Map<Id, BI_MM_Budget__c> mapNewBudget){

        system.debug('*** BI_MM_BudgetHandler - checkAvailableAmountOK - INI');

        if(mapNewBudget != null){

			// Get Set of Master Budgets Ids
			Set<Id> setMasterIds = new Set<Id>();
	        for(BI_MM_Budget__c objBudget : mapNewBudget.values()) if(objBudget.BI_MM_Master_budget__c !=null) setMasterIds.add(objBudget.BI_MM_Master_budget__c);
			// Get Set of Master Budgets
	        List<BI_MM_Budget__c> lstMasterBudgets = [SELECT Id, BI_MM_CurrentBudget__c, BI_MM_DistributedBudget__c, BI_MM_PlannedAmount__c, BI_MM_PaidAmount__c, BI_MM_Committed_Amount__c FROM BI_MM_Budget__c
                WHERE Id IN :setMasterIds];
	        // Get Map of Master Budgets from Id
			Map<Id, BI_MM_Budget__c> mapMasterBudgets = new Map<Id, BI_MM_Budget__c>();
            for(BI_MM_Budget__c objMasterBudget : lstMasterBudgets) mapMasterBudgets.put(objMasterBudget.Id, objMasterBudget);

            for(BI_MM_Budget__c myBudget : mapNewBudget.values()){
                system.debug('*** BI_MM_BudgetHandler - checkAvailableAmountOK - myBudget = ' + myBudget);

                BI_MM_Negative_budget__c negativeValue = BI_MM_Negative_budget__c.getValues(myBudget.BI_MM_Country_code__c);

                Decimal myAvailableAmount = (myBudget.BI_MM_CurrentBudget__c == null? 0: myBudget.BI_MM_CurrentBudget__c) -
                (myBudget.BI_MM_DistributedBudget__c == null? 0: myBudget.BI_MM_DistributedBudget__c) -
                (myBudget.BI_MM_PlannedAmount__c == null? 0: myBudget.BI_MM_PlannedAmount__c) -
                (myBudget.BI_MM_PaidAmount__c == null? 0: myBudget.BI_MM_PaidAmount__c) -
                 (myBudget.BI_MM_Committed_Amount__c == null? 0: myBudget.BI_MM_Committed_Amount__c);
                System.debug('*** BI_MM_BudgetHandler - checkAvailableAmountOK - myAvailableAmount = ' + myAvailableAmount);

				Decimal myAvailableAmountMaster = 0;
				BI_MM_Budget__c objMasterBudget = new BI_MM_Budget__c();
				if (myBudget.BI_MM_Master_budget__c != null){
					objMasterBudget = mapMasterBudgets.get(myBudget.BI_MM_Master_budget__c);
					myAvailableAmountMaster = (objMasterBudget.BI_MM_CurrentBudget__c == null? 0: objMasterBudget.BI_MM_CurrentBudget__c) -
	                (objMasterBudget.BI_MM_DistributedBudget__c == null? 0: objMasterBudget.BI_MM_DistributedBudget__c) -
	                (objMasterBudget.BI_MM_PlannedAmount__c == null? 0: objMasterBudget.BI_MM_PlannedAmount__c) -
	                (objMasterBudget.BI_MM_PaidAmount__c == null? 0: objMasterBudget.BI_MM_PaidAmount__c) -
                    (objMasterBudget.BI_MM_Committed_Amount__c == null? 0: objMasterBudget.BI_MM_Committed_Amount__c);
				}
                System.debug('*** BI_MM_BudgetHandler - checkAvailableAmountOK - myAvailableAmountMaster = ' + myAvailableAmountMaster);

                // If no Country has been defined in this funcionality, the value is 0
                if(negativeValue == null){
					if(myAvailableAmount < 0 && !userIsAdmin())
                        throw new BI_MM_Exception(System.Label.BI_MM_Budget_Exceeded + ' 0%');
					else if(myAvailableAmountMaster < 0 && !userIsAdmin())
                        throw new BI_MM_Exception(System.Label.BI_MM_MasterBudget_Exceeded + ' 0%');
                }else{
					Double myNewAvailable = -(((myBudget.BI_MM_CurrentBudget__c == null? 0: myBudget.BI_MM_CurrentBudget__c) * negativeValue.BI_MM_Negative_percentage__c) / 100);

					Double myNewAvailableMaster = myAvailableAmountMaster;
					if (myBudget.BI_MM_Master_budget__c != null)
						myNewAvailableMaster = -(((objMasterBudget.BI_MM_CurrentBudget__c == null? 0: objMasterBudget.BI_MM_CurrentBudget__c) * negativeValue.BI_MM_Negative_percentage__c) / 100);

					system.debug('*** BI_MM_BudgetHandler - checkAvailableAmountOK - myNewAvailable = ' + myNewAvailable);
                    system.debug('*** BI_MM_BudgetHandler - checkAvailableAmountOK - myNewAvailableMaster = ' + myNewAvailableMaster);

                    if(myAvailableAmount < myNewAvailable && !userIsAdmin())
                        throw new BI_MM_Exception(System.Label.BI_MM_Budget_Exceeded + ' ' + negativeValue.BI_MM_Negative_percentage__c + '%');
					else if(myAvailableAmountMaster < myNewAvailableMaster && !userIsAdmin())
                        throw new BI_MM_Exception(System.Label.BI_MM_MasterBudget_Exceeded + ' ' + negativeValue.BI_MM_Negative_percentage__c + '%');
                }
            }
        }
    }

    /*
    *@Description Calculate current amount for a master based on its details
    *@param mapNewBudget
    *@return
    *
    * @changelog
    * 06-Feb-2017 Jefferson Escobar
    * - Created
    * 24-Abr-2017 OmegaCRM
    * - Updated: Current Amount of Master Budget is the Sum of Detail Budgets' Available Amount instead of its Current Amount
    */
    private void calculateMasterAmount(Map<Id, BI_MM_Budget__c> mapBudgets) {
		System.debug('*** BI_MM_BudgetHandler - calculateMasterAmount - Start');
        List<BI_MM_Budget__c> budgetMaster = new List<BI_MM_Budget__c>();
        Map<Id, Decimal> mapAmountMaster = new Map<Id, Decimal>();

        //Get Master Ids from budgets in operation
        Set<Id> setMasterIds = new Set<Id>();
        for(BI_MM_Budget__c b :  mapBudgets.values()){
            if(b.BI_MM_Master_budget__c !=null) setMasterIds.add(b.BI_MM_Master_budget__c);
        }
		System.debug('*** BI_MM_BudgetHandler - calculateMasterAmount - setMasterIds == ' + setMasterIds);
        //Check if budgets contain master associeted
        if(!setMasterIds.isEmpty()){
            //Get budget details
            List<BI_MM_Budget__c> budgetDetails = [SELECT Id, BI_MM_CurrentBudget__c, BI_MM_AvailableBudget__c, BI_MM_Master_budget__c
                                                    FROM BI_MM_Budget__c
                                                    WHERE BI_MM_Master_budget__c in :setMasterIds
                                                    AND BI_MM_Is_active__c = true];

            for(BI_MM_Budget__c bDetail : budgetDetails){
                Decimal currentAmount = bDetail.BI_MM_AvailableBudget__c != null ? bDetail.BI_MM_AvailableBudget__c : 0;
                if(!mapAmountMaster.containsKey(bDetail.BI_MM_Master_budget__c)){
                    mapAmountMaster.put(bDetail.BI_MM_Master_budget__c, currentAmount);
                }else if(mapAmountMaster.containsKey(bDetail.BI_MM_Master_budget__c)){
                    mapAmountMaster.put(bDetail.BI_MM_Master_budget__c, (mapAmountMaster.get(bDetail.BI_MM_Master_budget__c) + currentAmount));
                }
            }
			System.debug('*** BI_MM_BudgetHandler - calculateMasterAmount - mapAmountMaster == ' + mapAmountMaster);

			//Preparing master budget Ids to update
            for(Id masterId : mapAmountMaster.keySet()){
				budgetMaster.add(new BI_MM_Budget__c(Id = masterId, BI_MM_CurrentBudget__c = mapAmountMaster.get(masterId)));
            }

            if(!budgetMaster.isEmpty()){
                database.update(budgetMaster, true);
				System.debug('*** BI_MM_BudgetHandler - calculateMasterAmount - updated MasterBudgetAmountUpdated == ' + budgetMaster);
            }
        }
    }

    /*
    * @Description returns a flag whether the current User is Admin or not (Profile = 'System Administrator' OR Permission Set 'BI_MM_ADMIN')
    * @param none
    * @return Boolean
	*
	* @Author      OmegaCRM
    * @changelog
    * 14-Feb-2017 OmegaCRM - Created
    */
    private Boolean userIsAdmin() {
        Boolean isAdmin = false;
        // Get 'BI_MM_ADMIN' Assignments of the user
        List<PermissionSetAssignment> permSetAssign = new list<PermissionSetAssignment>(
            [SELECT Id
               FROM PermissionSetAssignment
              WHERE PermissionSet.Name = :PERMISSION_SET
                AND Assignee.Id =: UserInfo.getUserId()]);
        // Get 'System Administrator' ProfileId
        String profileName = [Select Name from Profile where Id = :UserInfo.getProfileId()].Name;

		System.debug('*** BI_MM_BudgetHandler - userIsAdmin - profileName == ' + profileName + ' - permSetAssign == ' + permSetAssign);
        if (permSetAssign.size() > 0 || profileName.toUpperCase().contains(PROFILE_NAME)) isAdmin = true;
        return isAdmin;
    }
}