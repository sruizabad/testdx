/*
Name: BI_MM_BudgetHandlerTest
Requirement ID: Upload Data
Description: This is test class for BI_MM_BudgetHandler
Version | Author-Email | Date | Comment patata
1.0 | Mukesh Tiwari |  27/01/2016 | initial version
*/
@isTest
private Class BI_MM_BudgetHandlerTest {

    /*
        * Method Name : testSpainBudget
        * Description : Created for unit testing of budget trigger
        * Return Type : None
    */
    private static testMethod void testSpainBudget() {
        BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();

        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];  // Get BI_MM_ADMIN permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and  null Manager
        User testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, null);
        dataFactory.addPermissionSet(testUser, permissionAdmin);

        // Insert new Territories
        Territory parentTerritory = dataFactory.generateTerritory('Test Territory 1', null);
        Territory childTerritory = dataFactory.generateTerritory('Test Territory 2', parentTerritory.Id);

        System.runAs(testUser) {
            // Insert custom setting for testUser country code
            BI_MM_CountryCode__c objCountryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c);

            // Insert Mirror Territory
            BI_MM_MirrorTerritory__c mt = dataFactory.generateMirrorTerritory ('sr01Test', childTerritory.Id);
            BI_MM_MirrorTerritory__c mt1 = dataFactory.generateMirrorTerritory ('dm01', parentTerritory.Id);

            // Insert Product Catelog
            Product_vod__c objProduct = dataFactory.generateProduct('Test Product', 'Detail', testUser.Country_Code_BI__c);
            Product_vod__c objProduct2 = dataFactory.generateProduct('Test Product2', 'Detail', testUser.Country_Code_BI__c);

            // Insert spain Budget
            BI_MM_Budget__c objBudget  = dataFactory.generateBudgets(2, mt.Id, mt1.Id, 'Micro Marketing', objProduct, 4000, true).get(0);
            BI_MM_Budget__c objBudget2  = dataFactory.generateBudgets(2, mt1.Id, mt.Id, 'Micro Marketing', objProduct, 4000, true).get(0);
            BI_MM_RecursionHandler.isInsertSpainTrigger = true;
            BI_MM_Budget__c objBudget3 = dataFactory.generateInitialBudgets(1, mt1.Id, mt.Id, 'Micro Marketing', objProduct2, 4000, true).get(0);

            objBudget.BI_MM_TimePeriod__c = '2015';
            update objBudget;
            delete objBudget;
        }

    }

    private static testMethod void testExpenseLoad() {
        BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();

        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];  // Get BI_MM_ADMIN permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and  null Manager
        User testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, null);
        dataFactory.addPermissionSet(testUser, permissionAdmin);

        System.runAs(testUser) {

            // Get record type id for Medical event
            RecordType colloborationRecordType = [select Id, Name, DeveloperName from RecordType where Sobjecttype='Medical_Event_vod__c' and DeveloperName = 'Medical_Event'][0];

            // Insert the cusston setting countryCode for the corresponding country code of testUser
            BI_MM_CountryCode__c csCountryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c, colloborationRecordType.DeveloperName);

            // Insert new Event Program
            Event_Program_BI__c objEventProgram = dataFactory.generateEventProgram('Type 1 Colloboration', csCountryCode.Name, null,'Type 1 Colloboration');//Migart 062018 Changed TestCollProgram to 1. Col. a ligui. gastos
            BI_MM_CollProgramId__c objCollProgramId = dataFactory.generateCsProgram(objEventProgram.Id, objEventProgram.Name);
            new BI_MM_CollProgramId__c(Name = 'Type 1 Colloboration',Coll_Program_Id__c = objEventProgram.id);

            // Get User Territory
            UserTerritory objUserTerritory1 = [SELECT UserId, TerritoryId From UserTerritory WHERE TerritoryId != null limit 2][0];
            UserTerritory objUserTerritory2 = [SELECT UserId, TerritoryId From UserTerritory WHERE TerritoryId != null limit 2][1];

            // Insert Mirror Territory
            BI_MM_MirrorTerritory__c objMirrorTerritory1 = dataFactory.generateMirrorTerritory ('sr01Test', objUserTerritory1.Id);
            BI_MM_MirrorTerritory__c objMirrorTerritory2 = dataFactory.generateMirrorTerritory ('sr02Test', objUserTerritory2.Id);

            // Insert Product Catelog
            Product_vod__c objProduct = dataFactory.generateProduct('Test Product', 'Detail', testUser.Country_Code_BI__c);

            Integer expenseAmount = 10;
            Integer numBudgets = 1;
            Integer expenseMult = 2;

            test.startTest();

            // Insert Budgets
            List<BI_MM_Budget__c> lstBudget = dataFactory.generateBudgets(numBudgets, objMirrorTerritory1.Id, objMirrorTerritory2.Id, 'Micro Marketing', objProduct, (2*numBudgets*expenseAmount), true);
            BI_MM_RecursionHandler.isInsertSpainTrigger = true;
            System.debug('*** BI_MM_BudgetHandlerTest - testExpenseLoad - lstBudget == ' + lstBudget);
            System.debug('*** BI_MM_BudgetHandlerTest - testExpenseLoad - BI_MM_PlannedAmount__c == ' +
                (lstBudget[0].BI_MM_PlannedAmount__c == null ? 0 : lstBudget[0].BI_MM_PlannedAmount__c));
            System.debug('*** BI_MM_BudgetHandlerTest - testExpenseLoad - BI_MM_PaidAmount__c == ' +
                (lstBudget[0].BI_MM_PaidAmount__c == null ? 0 : lstBudget[0].BI_MM_PaidAmount__c));

            // Insert MedicalEvents
            List<Medical_Event_vod__c> lstMedicalEvents =
                dataFactory.generateBulkMedicalEvents(numBudgets, colloborationRecordType.Id, 'TestCol', objProduct, objEventProgram, 'New / In Progress');

            // Insert eventTeamMembers & EventExpenses w/Cost X
            List<Event_Expenses_BI__c> lstEventExpenses = dataFactory.generateBulkEventExpenses(lstMedicalEvents, testUser, expenseAmount);

            // Assign BudEvents to Budgets
            List<BI_MM_BudgetEvent__c> lstBudgetEvents = new List<BI_MM_BudgetEvent__c>();
            Integer i = 0;
            for(BI_MM_BudgetEvent__c objBudgetEvent : [Select Id, BI_MM_BudgetID__c FROM BI_MM_BudgetEvent__c WHERE BI_MM_EventID__c IN :lstMedicalEvents]){
                objBudgetEvent.BI_MM_BudgetID__c = (lstBudget != null && lstBudget.size() > 0 ? lstBudget[i].Id : null);
                objBudgetEvent.BI_MM_ProductID__c = objProduct.Id;
                lstBudgetEvents.add(objBudgetEvent);
                i++;
                system.debug('***objBudget.BI_MM_objBudgetEvent = '+objBudgetEvent);
            }
            update lstBudgetEvents;

            Set<Id> setBudgetEvent = new Set<Id>();
            for(BI_MM_BudgetEvent__c objBudgetEvent : lstBudgetEvents) setBudgetEvent.add(objBudgetEvent.Id);

            // Update EventExpenses to Cost 2X
            List<Event_Expenses_BI__c> updEventExpenses = new List<Event_Expenses_BI__c>();
            for(Event_Expenses_BI__c objEventExpense : lstEventExpenses){
                objEventExpense.Amount_BI__c = expenseAmount*expenseMult;
                updEventExpenses.add(objEventExpense);
            }
            update updEventExpenses;

            // Check Limits and Assert Budgets
            Double sumAmounts = 0;
            Set<Id> setBudget = new Set<Id>();
            for(BI_MM_Budget__c objBudget : lstBudget) setBudget.add(objBudget.Id);
            for(BI_MM_Budget__c objBudget : [SELECT Id, Name, BI_MM_PlannedAmount__c, BI_MM_PaidAmount__c, BI_MM_Committed_amount__c FROM BI_MM_Budget__c WHERE Id IN :setBudget]){
                sumAmounts = (objBudget.BI_MM_PlannedAmount__c == null ? 0 : objBudget.BI_MM_PlannedAmount__c) +
                // BI_MM_Committed_amount__c
                             (objBudget.BI_MM_PaidAmount__c == null ? 0 : objBudget.BI_MM_PaidAmount__c);
                                System.debug('***expenseAmount*expenseMult = '+expenseAmount*expenseMult + ' = ' + sumAmounts);
                System.debug('***objBudget.BI_MM_PlannedAmount__c = '+objBudget.BI_MM_PlannedAmount__c);
                System.debug('***objBudget.BI_MM_Committed_amount__c = '+objBudget.BI_MM_Committed_amount__c);
                //System.assertEquals(expenseAmount*expenseMult, sumAmounts, 'Expense not properly updated in Budget' + objBudget.Name);

            }

            test.stopTest();
        }

    }
}