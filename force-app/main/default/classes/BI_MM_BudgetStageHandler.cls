/*
Name: BI_MM_BudgetStageHandler
Requirement ID: Budget Stage Trigger
Description: Trigger to update budget records.
Version | Author-Email | Date | Comment
1.0 | Mukesh | 14.12.2015 | initial version
*/
public without sharing class BI_MM_BudgetStageHandler {

    private Map<String, BI_MM_Budget__c> mapBudgetToUpdate  {   get; set;   }

    /* Contructor */
    public BI_MM_BudgetStageHandler() {

        /* Do nothing */
    }

    public void onBeforeInsert(List<BI_MM_BudgetStage__c> lstNewBudgetStage) {
        System.debug('*** BI_MM_BudgetStageHandler - onBeforeInsert - INI');

        List<BI_MM_BudgetStage__c> lstBudgetStageToDelete = [SELECT Id FROM BI_MM_BudgetStage__c LIMIT 10000];

        if(lstBudgetStageToDelete != null && !lstBudgetStageToDelete.isEmpty())
        {
            System.debug('*** BI_MM_BudgetStageHandler - onBeforeInsert - lstBudgetStageToDelete = '+lstBudgetStageToDelete);
            Database.delete(lstBudgetStageToDelete, false);
        }
    }

    // This method will be called after insert of budget stage records
    public void onAfterInsert(List<BI_MM_BudgetStage__c> lstNewBudgetStage) {
        System.debug('*** BI_MM_BudgetStageHandler - onAfterInsert - INI');
        updateBudgetOnUpdationOfBudgetStage(lstNewBudgetStage);
    }

    /* This method will called when user update current budget or Indicator fields */
    public void onAfterUpdate(Map<Id, BI_MM_BudgetStage__c> mapOldBudgetStage, Map<Id, BI_MM_BudgetStage__c> mapNewBudgetStage) {
        System.debug('*** BI_MM_BudgetStageHandler - onAfterUpdate - INI');
        List<BI_MM_BudgetStage__c> lstFilteredBudgetStage = new List<BI_MM_BudgetStage__c>();

        for(BI_MM_BudgetStage__c objBudgetStage : mapNewBudgetStage.values()) {

            if(mapOldBudgetStage.get(objBudgetStage.Id).BI_MM_CurrentBudget__c != mapNewBudgetStage.get(objBudgetStage.Id).BI_MM_CurrentBudget__c ||
               mapOldBudgetStage.get(objBudgetStage.Id).BI_MM_BudgetIndicator__c != mapNewBudgetStage.get(objBudgetStage.Id).BI_MM_BudgetIndicator__c) {

                lstFilteredBudgetStage.add(objBudgetStage);
                // System.debug('*** BI_MM_BudgetStageHandler - onAfterUpdate - objBudgetStage = '+objBudgetStage);
            }
        }

        // System.debug('*** - onAfterUpdate - lstFilteredBudgetStage = '+lstFilteredBudgetStage);
        if(!lstFilteredBudgetStage.isEmpty())
            updateBudgetOnUpdationOfBudgetStage(lstFilteredBudgetStage);
    }

    /* This method calculate the current budget i.e add or Subtract into budgets on insertion or updation of Budget stage records */
    private void updateBudgetOnUpdationOfBudgetStage(List<BI_MM_BudgetStage__c> lstNewBudgetStage){
        System.debug('*** BI_MM_BudgetStageHandler - updateBudgetOnUpdationOfBudgetStage - INI');

        String strUnique;
        List<BI_MM_BudgetStage__c> lstBudgetStageToAudit = new List<BI_MM_BudgetStage__c>();

        mapBudgetToUpdate = getListBudgetsToUpdate(lstNewBudgetStage);

        /* Iterate over the newly inserted budget stage records*/
        for(BI_MM_BudgetStage__c objStageBudget : lstNewBudgetStage) {

            // System.debug('*** BI_MM_BudgetStageHandler - updateBudgetOnUpdationOfBudgetStage - objStageBudget = '+objStageBudget);

            if(objStageBudget.Name != null && objStageBudget.BI_MM_BudgetType__c != null && objStageBudget.BI_MM_ProductName__c != null &&
                objStageBudget.BI_MM_UserTerritory__c != null && objStageBudget.BI_MM_TimePeriod__c != null &&
                (objStageBudget.BI_MM_BudgetIndicator__c == System.Label.BI_MM_Add || objStageBudget.BI_MM_BudgetIndicator__c == System.Label.BI_MM_Subtract)) {

                strUnique = getBudgetStageKey(objStageBudget);

                if(mapBudgetToUpdate.containskey(strUnique)) {
                    // Calculate current budget
                    Double budgetCurrentBudget = mapBudgetToUpdate.get(strUnique).BI_MM_CurrentBudget__c != null ? mapBudgetToUpdate.get(strUnique).BI_MM_CurrentBudget__c : 0;
                    Double stageCurrentBudget = objStageBudget.BI_MM_CurrentBudget__c != null ? objStageBudget.BI_MM_CurrentBudget__c : 0;
                    // System.debug('*** BI_MM_BudgetStageHandler - updateBudgetOnUpdationOfBudgetStage - Stage = '+
                    //     budgetCurrentBudget + ' ' + objStageBudget.BI_MM_BudgetIndicator__c + ' ' + stageCurrentBudget);

                    // apply Stage to Budget
                    mapBudgetToUpdate.get(strUnique).BI_MM_CurrentBudget__c = applyStageOperation(objStageBudget.BI_MM_BudgetIndicator__c, budgetCurrentBudget, stageCurrentBudget);
                    lstBudgetStageToAudit.add(objStageBudget);
                }
                else {
                    System.debug('*** BI_MM_BudgetStageHandler - updateBudgetOnUpdationOfBudgetStage - No available Budget for Key = '+strUnique);
                }
            }
        }

        /* Update budget records */
        System.debug('*** BI_MM_BudgetStageHandler - updateBudgetOnUpdationOfBudgetStage - mapBudgetToUpdate = '+mapBudgetToUpdate);
        if(!mapBudgetToUpdate.isEmpty()) {
            Database.update(mapBudgetToUpdate.values(), false);
        }

        /* Insert audit records */
        System.debug('*** BI_MM_BudgetStageHandler - updateBudgetOnUpdationOfBudgetStage - lstBudgetStageToAudit = '+lstBudgetStageToAudit);
        if(!lstBudgetStageToAudit.isEmpty()){
            insertBudgetStageAudit(lstBudgetStageToAudit);
        }
    }

    /**
     * [applyStageOperation given an indicator and CurrentBudget of a Budget and Stage, returns the updated CurrentBudget for the Budget]
     * @param  indicator            [Stage operation Indicator]
     * @param  budgetCurrentBudget  [Current Budget of the Budget to update]
     * @param  stageCurrentBudget   [Current Budget of the Budget Stage to apply]
     * @return                      [New CurrentBudget of the Budget to update]
     */
    public Double applyStageOperation(String indicator, Double budgetCurrentBudget, Double stageCurrentBudget) {
        // System.debug('*** BI_MM_BudgetStageHandler - applyStageOperation - INI');

        if(indicator.equalsIgnoreCase(System.Label.BI_MM_Add))
            budgetCurrentBudget =  budgetCurrentBudget + stageCurrentBudget;
        else if(indicator.equalsIgnoreCase(System.Label.BI_MM_Subtract))
            budgetCurrentBudget =  budgetCurrentBudget - stageCurrentBudget;

        System.debug('*** BI_MM_BudgetStageHandler - applyStageOperation - budgetCurrentBudget = '+budgetCurrentBudget);
        return budgetCurrentBudget;
    }

    /* This method insert BI_MM_BudgetStageAudit when new BI_MM_BudgetStage__c records inserted with Add or Subtract indicator type */
    private void insertBudgetStageAudit(List<BI_MM_BudgetStage__c> lstNewBudgetStage) {
        System.debug('*** BI_MM_BudgetStageHandler - insertBudgetStageAudit - INI');

        List<BI_MM_BudgetStageAudit__c> lstBudgetStageAuditToInsert = new List<BI_MM_BudgetStageAudit__c>();

        for(BI_MM_BudgetStage__c objStage : lstNewBudgetStage) {

            System.debug('*** BI_MM_BudgetStageHandler - insertBudgetStageAudit - objStage.Id = '+ objStage.Id);

            lstBudgetStageAuditToInsert.add(
                new BI_MM_BudgetStageAudit__c(
                    Name = objStage.Name,
                    BI_MM_BudgetIndicator__c = objStage.BI_MM_BudgetIndicator__c,
                    BI_MM_BudgetType__c = objStage.BI_MM_BudgetType__c,
                    BI_MM_CurrentBudget__c = objStage.BI_MM_CurrentBudget__c,
                    BI_MM_DistributedBudget__c = objStage.BI_MM_DistributedBudget__c,
                    BI_MM_EndDate__c = objStage.BI_MM_EndDate__c,
                    BI_MM_ManagerTerritory__c = objStage.BI_MM_ManagerTerritory__c,
                    BI_MM_PaidAmount__c = objStage.BI_MM_PaidAmount__c,
                    BI_MM_PlannedAmount__c = objStage.BI_MM_PlannedAmount__c,
                    BI_MM_ProductName__c = objStage.BI_MM_ProductName__c,
                    BI_MM_StartDate__c = objStage.BI_MM_StartDate__c,
                    BI_MM_TimePeriod__c = objStage.BI_MM_TimePeriod__c,
                    BI_MM_UserTerritory__c = objStage.BI_MM_UserTerritory__c,
                    BI_MM_Budget_To__c = mapBudgetToUpdate.get(getBudgetStageKey(objStage)).Id
                ));
        }
        System.debug('*** BI_MM_BudgetStageHandler - insertBudgetStageAudit - lstBudgetStageAuditToInsert = '+ lstBudgetStageAuditToInsert);

        if(!lstBudgetStageAuditToInsert.isEmpty()){
            Database.insert(lstBudgetStageAuditToInsert, false);
        }
    }

    /**
     * [applyStageOperation given a list of BudgetStage, returns the Map of affected Budgets]
     * @param   lstBudgetStage              [List of BudgetStage]
     * @return                              [Map of Key and Budget to update]
     */
    public Map<String, BI_MM_Budget__c> getListBudgetsToUpdate(List<BI_MM_BudgetStage__c> lstBudgetStage) {
        System.debug('*** BI_MM_BudgetStageHandler - getListBudgetsToUpdate - INI');

        Map<String, BI_MM_Budget__c> mapBudgetToUpdate = new Map<String, BI_MM_Budget__c>();
        Set<String> setBudgetType = new Set<String>();
        Set<String> setProductName = new Set<String>();
        Set<String> setTerritoryName = new Set<String>();
        Set<String> setTimePeriod = new Set<String>();
        Set<String> setBudgetStageKey = new Set<String>();
        String strUnique;

        // Get sets of values to obtain Budgets
        for(BI_MM_BudgetStage__c objBudgetStage : lstBudgetStage) {
            setBudgetType.add(objBudgetStage.BI_MM_BudgetType__c);
            setProductName.add(objBudgetStage.BI_MM_ProductName__c);
            setTerritoryName.add(objBudgetStage.BI_MM_UserTerritory__c);
            setTimePeriod.add(objBudgetStage.BI_MM_TimePeriod__c);
            setBudgetStageKey.add(getBudgetStageKey(objBudgetStage));
        }

        // Query for Budgets using sets of values from list of BudgetStage
        List<BI_MM_Budget__c> lstAllBudget = [SELECT Id, Name, BI_MM_BudgetType__c, BI_MM_TerritoryName__c, BI_MM_TerritoryName__r.Name,
                                                     BI_MM_ProductName__c, BI_MM_ProductName__r.Name, BI_MM_TimePeriod__c, BI_MM_CurrentBudget__c,
                                                     BI_MM_DistributedBudget__c
                                              FROM BI_MM_Budget__c
                                              WHERE Name != null AND BI_MM_BudgetType__c IN : setBudgetType AND BI_MM_ProductName__c IN : setProductName AND
                                                     BI_MM_TerritoryName__c IN : setTerritoryName AND BI_MM_TimePeriod__c IN : setTimePeriod
                                              LIMIT 10000];

        // Create Map of Budgets to update using key to filter map entries
        for(BI_MM_Budget__c objBudget : lstAllBudget) {
            strUnique = getBudgetKey(objBudget);
            if (setBudgetStageKey.contains(strUnique)) mapBudgetToUpdate.put(strUnique, objBudget);
        }

        return mapBudgetToUpdate;
    }

    /**
     * [getBudgetKey given a Budget, returns the Key used to associate the Budget with a BudgetStage]
     * @param  objBudget            [Budget to get Key from]
     * @return                      [New CurrentBudget of the Budget to update]
     */
    public String getBudgetKey(BI_MM_Budget__c objBudget) {

        String strUnique = objBudget.BI_MM_BudgetType__c + '_' + objBudget.BI_MM_ProductName__c + '_' +
                           objBudget.BI_MM_TerritoryName__c + '_' + objBudget.BI_MM_TimePeriod__c;
        // System.debug('*** BI_MM_BudgetStageHandler - getBudgetKey - strUnique = ' + strUnique);
        return strUnique;
    }

    /**
     * [getBudgetKey given a BudgetStage, returns the Key used to associate the BudgetStage with a Budget]
     * @param  objBudget            [Budget to get Key from]
     * @return                      [New CurrentBudget of the Budget to update]
     */
    public String getBudgetStageKey(BI_MM_BudgetStage__c objBudgetStage) {

        String strUnique = objBudgetStage.BI_MM_BudgetType__c + '_' + objBudgetStage.BI_MM_ProductName__c + '_' +
                           objBudgetStage.BI_MM_UserTerritory__c + '_' + objBudgetStage.BI_MM_TimePeriod__c;
        // System.debug('*** BI_MM_BudgetStageHandler - getBudgetStageKey - strUnique = ' + strUnique);
        return strUnique;
    }
}