/*
Name: BI_MM_BudgetStageHandlerTest
Requirement ID:
Description: Test class for BI_MM_BudgetStageHandler
Version | Author-Email | Date | Comment
1.0 | Venkata Madiri | 020.12.2015 | initial version
*/
@isTest
private class BI_MM_BudgetStageHandlerTest {

    static testMethod void testBI_MM_BudgetStageHandler() {
        BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();
        List<BI_MM_Budget__c> lstBudgetToInsert = new List<BI_MM_Budget__c>();
        List<BI_MM_BudgetStage__c> lstBudgetStageToInsert = new List<BI_MM_BudgetStage__c>();
        List<BI_MM_BudgetStage__c> lstBudgetStageToUpdate = new List<BI_MM_BudgetStage__c>();

        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];  // Get BI_MM_ADMIN permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and  null Manager
        User testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, null);
        dataFactory.addPermissionSet(testUser, permissionAdmin);

        // Insert new Territories
        Territory territory = dataFactory.generateTerritory('Test Territory 1', null);


        System.runAs(testUser) {
            Product_vod__c objProduct = dataFactory.generateProduct('Test Product', 'Detail', null);

            // Insert BI_MM_MirrorTerritory__c record
            BI_MM_MirrorTerritory__c objTerritory = dataFactory.generateMirrorTerritory ('sr01Test', territory.Id);

            Test.startTest();

            // Insert BI_MM_Budget__c records
            lstBudgetToInsert = dataFactory.generateBudgets(10, objTerritory.Id, null, 'Micro Marketing', objProduct, 4000, true);

            // Insert BI_MM_BudgetStage__c records
            lstBudgetStageToInsert = dataFactory.generateBudgetsStages(10, objTerritory.Id, null, 'Micro Marketing', objProduct, 1000, System.Label.BI_MM_Add);

            // Insert Multiple BI_MM_BudgetStage__c records again to test Delete and Budget repetition
            lstBudgetStageToInsert = dataFactory.generateMultipleBudgetStagesFromBudgets(lstBudgetToInsert, 5, 100, System.Label.BI_MM_Add);

            // update stage budget
            for(BI_MM_BudgetStage__c objBudgetStage : lstBudgetStageToInsert)
            {
                objBudgetStage.BI_MM_BudgetIndicator__c = System.Label.BI_MM_Subtract;
                objBudgetStage.BI_MM_CurrentBudget__c = 50;
                lstBudgetStageToUpdate.add(objBudgetStage);
            }
            if(!lstBudgetStageToUpdate.isEmpty())
            {
                update lstBudgetStageToUpdate;
            }

            Test.stopTest();
        }

    }
}