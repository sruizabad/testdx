/* 
Name: BI_MM_BudgetTransferControllerForAdminTest
Requirement ID: Budget Uploader Utility
Description: Test class for BI_MM_BudgetTransferControllerForAdmin
Version | Author-Email | Date | Comment 
1.0 | Venkata Madiri | 11.12.2015 | initial version 
*/

@isTest
private class BI_MM_BudgetTransControllerForAdminTest{
    static BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();    
    static User thisUser;    
    static User testUser;
    static Product_vod__c product;
    static BI_MM_Budget__c bgt;
    static BI_MM_Budget__c bgt1;
    static BI_MM_Budget__c bgtDistribute;
    static BI_MM_MirrorTerritory__c mirrorTerrChild1;
    static BI_MM_MirrorTerritory__c mirrorTerrParent;
    static BI_MM_MirrorTerritory__c mirrorTerrChild2; 
    static Territory parentTerr;
    static Territory territoryChild1;
    static Territory territoryChild2;



    static void initUser(){
        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];  // Get BI_MM_ADMIN permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and  null Manager
        testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, null);
        dataFactory.addPermissionSet(testUser, permissionAdmin);  
    }

    static void createTerritories() {
        // Create new territories
        parentTerr = dataFactory.generateTerritory('Test Parent Territory', null);
        territoryChild1 = dataFactory.generateTerritory('Test Child Territory 1', parentTerr.Id);
        territoryChild2 = dataFactory.generateTerritory('Test Child Territory 2', parentTerr.Id);
    }

    static void createData(){   
        List<BI_MM_Budget__c> budgets = new List<BI_MM_Budget__c>();
        String budgType = 'Micro Marketing1';

        thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        // Create mirror territories
        mirrorTerrChild1 = dataFactory.generateMirrorTerritory ('rm01', territoryChild1.Id);
        mirrorTerrParent = dataFactory.generateMirrorTerritory ('nm01', parentTerr.Id);
        mirrorTerrChild2 = dataFactory.generateMirrorTerritory ('rm02', territoryChild2.Id);
        
        // Create new Product catalogue
        product = dataFactory.generateProduct('TAUROLIN', 'Detail', testUser.Country_Code_BI__c); 
        
        // Create 3 Budgets: One parent and Two childs
        bgtDistribute = dataFactory.generateBudgets(1, mirrorTerrParent.Id, null, budgType, product, 4000, true).get(0);
        bgt = dataFactory.generateBudgets(1, mirrorTerrChild1.Id, mirrorTerrParent.Id, budgType, product, 1000, true).get(0);
        bgt1 = dataFactory.generateBudgets(1, mirrorTerrChild2.Id, mirrorTerrParent.Id, budgType, product, 1000, true).get(0);
            
        BI_MM_BudgetAudit__c budAud = dataFactory.generateBudgetAudit(bgt, bgt1, testUser, thisUser,Label.BI_MM_Transfer_Budget_Audit);   

        dataFactory.generateCountryCode('ES');         
    }    
    
    static testMethod void BI_MM_BudgetTransferControllerTransfer(){
        initUser();
        createTerritories();

        System.runAs(testUser) {
            Double amount = 500; 
            createData();            
            
            BI_MM_BudgetTransferControllerForAdmin bmtCtrl = new BI_MM_BudgetTransferControllerForAdmin();
            ////bmtCtrl.selectedUserName =  thisUser.id; Commented out for compilation error
            bmtCtrl.strSelectedMgrTerritory = parentTerr.Id;     
            bmtCtrl.selectedBudgetType =  'Micro Marketing1';     
            bmtCtrl.strSelectedProductId = 'TAUROLIN';
            //bmtCtrl.strSelectedTransfer  = 'Transfer Budget'; Migar 20180531 Changed to Label
            bmtCtrl.strSelectedTransfer  = Label.BI_MM_Transfer_Budget_Audit;
           // //bmtCtrl.selectedUserName1 =  thisUser.id; Commented out for compilation error
            bmtCtrl.selectedBudgetType1 =  'Micro Marketing1';     
            bmtCtrl.strSelectedProductId1 = 'TAUROLIN';
            bmtCtrl.selectedTerrName =mirrorTerrChild1.name;
            bmtCtrl.selectedTerrName1 =mirrorTerrChild2.name;
            bmtCtrl.strSelectedPeriod = System.now().format('yyyy/MM/d')+'-'+System.now().format('yyyy/MM/d');
            //bmtCtrl.BudjetType = 'Transfer';// Commented out for compilation error
            Test.startTest();

                bmtCtrl.transferSelection();
                bmtCtrl.terrlist1();
                bmtCtrl.terrlist();
               // //bmtCtrl.userlist();  Commented out for compilation error
               // //bmtCtrl.selectedUserName =  thisUser.id; Commented out for compilation error
                //bmtCtrl.readUser();  Commented out for compilation error
                bmtCtrl.selectedTerrName =mirrorTerrChild1.name;
                bmtCtrl.selectedBudgetType =  'Micro Marketing1';    
                bmtCtrl.products();
                bmtCtrl.touserlistAdd();
                bmtCtrl.availableAmtMethod();

                //bmtCtrl.selectedUserName =  thisUser.id; Commented out for compilation error
                bmtCtrl.selectedBudgetType =  'Micro Marketing1';       
                bmtCtrl.strSelectedProductId = 'TAUROLIN';
                bmtCtrl.selectedTerrName =mirrorTerrChild1.name;
                bmtCtrl.selectedTerrName1 =mirrorTerrChild2.name;
                bmtCtrl.products1();
                //bmtCtrl.selectedUserName1 =  thisUser.id; Commented out for compilation error
                bmtCtrl.strSelectedProductId1 = 'TAUROLIN';
                bmtCtrl.userlistAdd1();
                bmtCtrl.selectedBudgetType1 =  'Micro Marketing1';
                bmtCtrl.timeperiodAdd1();     
                bmtCtrl.timeperiodAdd();
                bmtCtrl.strSelectedPeriod = bmtCtrl.lstPeriods[1].getValue();
                bmtCtrl.strSelectedPeriod1 = bmtCtrl.lstPeriods1[1].getValue();
                bmtCtrl.availableAmtMethod();
                bmtCtrl.touserlistAdd();
                bmtCtrl.objBudget.BI_MM_TransferAmount__c = amount;
                System.debug('***objBudget'+bmtCtrl.objBudget);
                System.debug('***objBudget1'+bmtCtrl.objBudget1);
                BI_MM_Budget__c bdgBeforeTransfer = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :bgt1.Id];
                bmtCtrl.transfer();
                BI_MM_Budget__c bdgAfterTransfer = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :bgt1.Id];

                System.assertEquals(bdgBeforeTransfer.BI_MM_AvailableBudget__c + amount , bdgAfterTransfer.BI_MM_AvailableBudget__c, 'ERROR While transfering Budget.');
            
            Test.stopTest();
        }            
    }
    static testMethod void BI_MM_BudgetTransferControllerAdd(){
        initUser();
        createTerritories();

        System.runAs(testUser) {
            Double amount = 500;

            createData();
            BI_MM_BudgetTransferControllerForAdmin bmtCtrl = new BI_MM_BudgetTransferControllerForAdmin();
            //bmtCtrl.selectedUserName =  thisUser.id;
             bmtCtrl.strSelectedMgrTerritory = parentTerr.Id;    
            bmtCtrl.selectedBudgetType =  'Micro Marketing1';     
            bmtCtrl.strSelectedProductId = 'TAUROLIN';
            //bmtCtrl.strSelectedTransfer  = 'Distribute Budget'; Migart 20180531 Changed by label
            bmtCtrl.strSelectedTransfer  = Label.BI_MM_Distribute_Budget_Audit;
            bmtCtrl.selectedTerrName =mirrorTerrParent.name;
            bmtCtrl.selectedTerrName1 =mirrorTerrChild1.name;
            //bmtCtrl.selectedUserName1 =  thisUser.id;  Commented out for compilation error
            bmtCtrl.selectedBudgetType1 =  'Micro Marketing1';     
            bmtCtrl.strSelectedProductId1 = 'TAUROLIN';
            bmtCtrl.strSelectedPeriod = System.now().format('yyyy/MM/d')+'-'+System.now().format('yyyy/MM/d');
            //bmtCtrl.BudjetType = 'Add'; //Commented out for compilation error

            Test.startTest();
            
                bmtCtrl.transferSelection();
                bmtCtrl.selectedTerrName =mirrorTerrParent.name;
                bmtCtrl.selectedTerrName1 =mirrorTerrChild1.name;
                bmtCtrl.userlistAdd(); 
                bmtCtrl.touserlistAdd();
                bmtCtrl.redirectPopup();
                bmtCtrl.availableAmtMethod1();
                bmtCtrl.distributeListAdd();
                bmtCtrl.selectedBudgetType =  'Micro Marketing1';       
                bmtCtrl.strSelectedProductId = 'TAUROLIN';
                bmtCtrl.selectedTerrName =mirrorTerrParent.name;
                bmtCtrl.selectedTerrName1 =mirrorTerrChild1.name;
                bmtCtrl.products1();
                bmtCtrl.strSelectedProductId1 = 'TAUROLIN';
                bmtCtrl.selectedBudgetType1 =  'Micro Marketing1';
                bmtCtrl.timeperiodAdd();
                bmtCtrl.strSelectedPeriod = bmtCtrl.lstPeriods[1].getValue();
                bmtCtrl.availableAmtMethod();
                bmtCtrl.availableAmtMethod1();
                //bmtCtrl.selectedTerrName1 =null;
                bmtCtrl.objBudget.BI_MM_TransferAmount__c = amount;

                BI_MM_Budget__c bdgBeforeDistribute = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :bgt.Id];
                bmtCtrl.transfer();
                BI_MM_Budget__c bdgAfterDistribute = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :bgt.Id];

                System.assertEquals(bdgBeforeDistribute.BI_MM_AvailableBudget__c + amount , bdgAfterDistribute.BI_MM_AvailableBudget__c, 'ERROR While Distribuiting Budget.');
            
            Test.stopTest();
        }            
    }
    static testMethod void BI_MM_BudgetTransferControllerSub(){
        initUser();
        createTerritories();

        System.runAs(testUser) {
            Double amount = 500; 

            createData();
            BI_MM_BudgetTransferControllerForAdmin bmtCtrl = new BI_MM_BudgetTransferControllerForAdmin();
             bmtCtrl.strSelectedMgrTerritory = parentTerr.Id;   
            //bmtCtrl.selectedUserName =  thisUser.id;   Commented out for compilation error
            bmtCtrl.selectedBudgetType =  'Micro Marketing1';     
            bmtCtrl.strSelectedProductId = 'TAUROLIN';
            //bmtCtrl.strSelectedTransfer  = 'Revoke Budget'; Migart 20183105 Changed by label
            bmtCtrl.strSelectedTransfer  = Label.BI_MM_Revoke_Budget_Audit;
            bmtCtrl.strSelectedPeriod = System.now().format('yyyy/MM/d')+'-'+System.now().format('yyyy/MM/d');
            
            bmtCtrl.selectedTerrName =mirrorTerrChild1.name;
            bmtCtrl.selectedTerrName1 =mirrorTerrParent.name;
            //bmtCtrl.selectedUserName1 =  thisUser.id;  Commented out for compilation error
            bmtCtrl.selectedBudgetType1 =  'Micro Marketing1';     
            bmtCtrl.strSelectedProductId1 = 'TAUROLIN';
            Test.startTest();
                bmtCtrl.transferSelection();
                bmtCtrl.userlistAdd(); 
                bmtCtrl.selectedTerrName1 =mirrorTerrParent.name;
                bmtCtrl.redirectPopup();
                //bmtCtrl.BudjetType = 'Sub'; migart 20180530
                bmtCtrl.availableAmtMethod1();
                bmtCtrl.distributeListAdd();
                //bmtCtrl.selectedUserName =  thisUser.id;  Commented out for compilation error
                bmtCtrl.selectedBudgetType =  'Micro Marketing1';       
                bmtCtrl.strSelectedProductId = 'TAUROLIN';
                bmtCtrl.selectedTerrName =mirrorTerrChild1.name;
                bmtCtrl.selectedTerrName1 =mirrorTerrParent.name;
                //bmtCtrl.selectedUserName1 =  thisUser.id;  Commented out for compilation error
                bmtCtrl.strSelectedProductId1 = 'TAUROLIN';
                bmtCtrl.selectedBudgetType1 =  'Micro Marketing1';     
                bmtCtrl.timeperiodAdd();
                bmtCtrl.strSelectedPeriod = bmtCtrl.lstPeriods[1].getValue();
                bmtCtrl.availableAmtMethod();
                //bmtCtrl.selectedTerrName1 =null;
                bmtCtrl.objBudget.BI_MM_TransferAmount__c = amount;
                
                BI_MM_Budget__c bdgBeforeRevokeUser = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :bgt.Id];
                bmtCtrl.transfer();
                BI_MM_Budget__c bdgAfterRevokeUser = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :bgt.Id];

                System.assertEquals(bdgBeforeRevokeUser.BI_MM_AvailableBudget__c - amount, bdgAfterRevokeUser.BI_MM_AvailableBudget__c , 'ERROR While Revoking Budget for user.');

            Test.stopTest();
        }        
    }

    static testMethod void BI_MM_BudgetTransferControllerNone(){
        initUser();
        createTerritories();

        System.runAs(testUser) {
            createData();
            BI_MM_BudgetTransferControllerForAdmin bmtCtrl1 = new BI_MM_BudgetTransferControllerForAdmin();
            
            bmtCtrl1.strSelectedTransfer  = '--None--';
            bmtCtrl1.transferSelection();
            bmtCtrl1.transfer();
            
            BI_MM_BudgetTransferControllerForAdmin bmtCtrl2 = new BI_MM_BudgetTransferControllerForAdmin();
            bmtCtrl2.selectedBudgetType1 = 'Micro Marketing1'; 
            bmtCtrl2.transfer();
                        //bmtCtrl.BudjetType = 'Sub';// Commented out for compilation error
            bmtCtrl1.getTerritoryName();
            bmtCtrl1.getAllBudgetsMgrTerritory();
             bmtCtrl1.redirectTrasnferPage();
        }
    }
}