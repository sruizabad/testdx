/*
Name: BI_MM_BudgetTransferControllerTest
Requirement ID: Budget Uploader Utility
Description: Test class for BI_MM_BudgetTransferController
Version | Author-Email | Date | Comment
1.0 | Venkata Madiri | 11.12.2015 | initial version
*/
@isTest
private class BI_MM_BudgetTransferControllerTest{
    static BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();
    static User thisUser;
    static Product_vod__c product;
    static User testUser;
    static BI_MM_Budget__c budgetFrom;
    static BI_MM_Budget__c budgetTo;
    static BI_MM_Budget__c budgetDistribute;
    static BI_MM_MirrorTerritory__c mirrorTerrChild1;
    static BI_MM_MirrorTerritory__c mirrorTerrParent;
    static BI_MM_MirrorTerritory__c mirrorTerrChild2;
    static Territory parentTerr;
    static Territory territoryChild1;
    static Territory territoryChild2;
    

    static void initUserTerritories(){
        // Get userTerritory relationship
        UserTerritory objUserTerritory = [SELECT UserId, TerritoryId FROM UserTerritory limit 1];

        // get User and Parent Territory from UserTerritory
        testUser = [SELECT Id, Name,Username,UserRole.Name, Country_Code_BI__c FROM User WHERE Id = :objUserTerritory.UserId];
        parentTerr = [SELECT Id FROM Territory WHERE Id = :objUserTerritory.TerritoryId];

        // Create new child territories
        territoryChild1 = dataFactory.generateTerritory('Test Child Territory 1', parentTerr.Id);
        territoryChild2 = dataFactory.generateTerritory('Test Child Territory 2', parentTerr.Id);

        // Migart 20180524 - Commented -Get BI_MM_ADMIN permission set and assign it to the user if needed
        // PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];
        // if ([SELECT Id FROM PermissionSetAssignment WHERE AssigneeId = :testUser.Id AND PermissionSetId = :permissionAdmin.Id].size() == 0)
        //     dataFactory.addPermissionSet(testUser, permissionAdmin);
        // Migart 20180524 Get BI_MM_SALES_MANAGER (instead of BI_MM_ADMIN) permission set and assign it to the user if needed 
        PermissionSet permissionSalesMgr = [select Id from PermissionSet where Name = 'BI_MM_SALES_MANAGER' limit 1];
        if ([SELECT Id FROM PermissionSetAssignment WHERE AssigneeId = :testUser.Id AND PermissionSetId = :permissionSalesMgr.Id].size() == 0)
        dataFactory.addPermissionSet(testUser, permissionSalesMgr);
        // Migart 20180524 adding debug
        System.debug('*** BI_MM_BudgetTransferControllerTest - PermissionSet == ' + permissionSalesMgr + 'User== ' + testUser);
	    }

    static void createData(){
        String budgType = 'Micro Marketing1';

        thisUser = [select Id from User where Id = :UserInfo.getUserId()];

        // Create mirror territories
        mirrorTerrParent = dataFactory.generateMirrorTerritory ('nm01', parentTerr.Id);
        mirrorTerrChild1 = dataFactory.generateMirrorTerritory ('rm01', territoryChild1.Id);
        mirrorTerrChild2 = dataFactory.generateMirrorTerritory ('rm02', territoryChild2.Id);

        // Create new Product catalogue
        product = dataFactory.generateProduct('TAUROLIN', 'Detail', testUser.Country_Code_BI__c);

        // Create 3 Budgets: One parent and Two childs
        budgetDistribute = dataFactory.generateBudgets(1, mirrorTerrParent.Id, null, budgType, product, 4000, true).get(0);
        budgetFrom = dataFactory.generateBudgets(1, mirrorTerrChild1.Id, mirrorTerrParent.Id, budgType, product, 1000, true).get(0);
        budgetTo = dataFactory.generateBudgets(1, mirrorTerrChild2.Id, mirrorTerrParent.Id, budgType, product, 1000, true).get(0);
        //Migart 20180531 Added Labels.
        BI_MM_BudgetAudit__c budAudTrans = dataFactory.generateBudgetAudit(budgetFrom, budgetTo, testUser, thisUser, Label.BI_MM_Transfer_Budget_Audit);
        BI_MM_BudgetAudit__c budAudDistr = dataFactory.generateBudgetAudit(budgetFrom, budgetTo, testUser, thisUser, Label.BI_MM_Distribute_Budget_Audit);
        BI_MM_BudgetAudit__c budAudRevok = dataFactory.generateBudgetAudit(budgetFrom, budgetTo, testUser, thisUser, Label.BI_MM_Revoke_Budget_Audit);
        // Migart 20180524 adding debug
        System.debug('*** BI_MM_BudgetTransferControllerTest - product == ' + product);
        System.debug('*** BI_MM_BudgetTransferControllerTest - mirrorTerrParent == ' + mirrorTerrParent);
        System.debug('*** BI_MM_BudgetTransferControllerTest - mirrorTerrChild1 == ' + mirrorTerrChild1);
        System.debug('*** BI_MM_BudgetTransferControllerTest - Transfer_Budget_Audit == ' + budAudTrans);
        System.debug('*** BI_MM_BudgetTransferControllerTest - Distribute_Budget_Audit == ' + budAudDistr);
        System.debug('*** BI_MM_BudgetTransferControllerTest - Revoke_Budget_Audit == ' + budAudRevok);
        System.debug('*** BI_MM_BudgetTransferControllerTest - budgetFrom == ' + budgetFrom);
        System.debug('*** BI_MM_BudgetTransferControllerTest - budgetTo == ' + budgetTo);
        
        
    }

    static testMethod void BI_MM_BudgetTransferControllerTransfer(){
        initUserTerritories();

        System.runAs(testUser) {
            Double amount = 500;
            createData();

            BI_MM_BudgetTransferController bmtCtrl = new BI_MM_BudgetTransferController();

            Test.startTest();

            bmtCtrl.strSelectedTransfer = Label.BI_MM_Transfer_Budget_Audit;
            bmtCtrl.onChangeTransferType();
            bmtCtrl.selectedTerrNameFrom = mirrorTerrChild1.name;
            bmtCtrl.onChangeTerritoryFrom();
            bmtCtrl.strSelectedProductFrom = product.Name;
            bmtCtrl.onChangeProduct();
            bmtCtrl.selectedBudgetTypeFrom = budgetFrom.BI_MM_BudgetType__c;
            bmtCtrl.onChangeBudgetType();
            // TimePeriod obtained through formula field
            bmtCtrl.strSelectedPeriodFrom = [SELECT Id, BI_MM_TimePeriod__c FROM BI_MM_Budget__c WHERE Id = :budgetFrom.Id].BI_MM_TimePeriod__c;
            bmtCtrl.onChangeTimePeriod();
            bmtCtrl.selectedTerrNameTo = mirrorTerrChild2.name;
            bmtCtrl.onChangeTerritoryTo();

            bmtCtrl.objBudgetFrom.BI_MM_TransferAmount__c = amount;

            BI_MM_Budget__c bdgBeforeTransfer = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :budgetTo.Id];
            System.debug('*** BI_MM_BudgetTransferControllerTest - BI_MM_BudgetTransferControllerTransfer - objBudgetFrom == ' + bmtCtrl.objBudgetFrom);
            System.debug('*** BI_MM_BudgetTransferControllerTest - BI_MM_BudgetTransferControllerTransfer - objBudgetTo == ' + bmtCtrl.objBudgetTo);
            bmtCtrl.transfer();
            BI_MM_Budget__c bdgAfterTransfer = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :budgetTo.Id];

            System.assertEquals(bdgBeforeTransfer.BI_MM_AvailableBudget__c + amount , bdgAfterTransfer.BI_MM_AvailableBudget__c, 'ERROR While transfering Budget.');

            Test.stopTest();
        }
    }
    static testMethod void BI_MM_BudgetTransferControllerAdd(){
        initUserTerritories();

        System.runAs(testUser) {
            Double amount = 500;

            createData();
            BI_MM_BudgetTransferController bmtCtrl = new BI_MM_BudgetTransferController();

            Test.startTest();

            bmtCtrl.strSelectedTransfer = Label.BI_MM_Distribute_Budget_Audit;
            bmtCtrl.onChangeTransferType();
            bmtCtrl.selectedTerrNameFrom = mirrorTerrParent.Name;
            bmtCtrl.onChangeTerritoryFrom();
            bmtCtrl.strSelectedProductFrom = product.Name;
            bmtCtrl.onChangeProduct();
            bmtCtrl.selectedBudgetTypeFrom = budgetFrom.BI_MM_BudgetType__c;
            bmtCtrl.onChangeBudgetType();
            bmtCtrl.strSelectedPeriodFrom = [SELECT Id, BI_MM_TimePeriod__c FROM BI_MM_Budget__c WHERE Id = :budgetFrom.Id].BI_MM_TimePeriod__c;
            bmtCtrl.onChangeTimePeriod();
            bmtCtrl.selectedTerrNameTo = mirrorTerrChild1.Name;
            bmtCtrl.onChangeTerritoryTo();

            bmtCtrl.objBudgetFrom.BI_MM_TransferAmount__c = amount;

            BI_MM_Budget__c bdgBeforeDistribute = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :budgetFrom.Id];
            bmtCtrl.transfer();
            BI_MM_Budget__c bdgAfterDistribute = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :budgetFrom.Id];

            System.debug('*** BI_MM_BudgetTransferControllerAdd - bdgBeforeDistribute == ' + bdgBeforeDistribute);
            System.debug('*** BI_MM_BudgetTransferControllerAdd - bdgAfterDistribute == ' + bdgAfterDistribute);

            System.assertEquals(bdgBeforeDistribute.BI_MM_AvailableBudget__c + amount , bdgAfterDistribute.BI_MM_AvailableBudget__c, 'ERROR While Distribuiting Budget.');

            Test.stopTest();
        }
    }

    static testMethod void BI_MM_BudgetTransferControllerSub(){
        initUserTerritories();

        System.runAs(testUser) {
            Double amount = 500;

            createData();
            BI_MM_BudgetTransferController bmtCtrl = new BI_MM_BudgetTransferController();

            Test.startTest();

            bmtCtrl.strSelectedTransfer  = Label.BI_MM_Revoke_Budget_Audit;
            bmtCtrl.onChangeTransferType();
            bmtCtrl.selectedTerrNameFrom = mirrorTerrChild1.Name;
            bmtCtrl.onChangeTerritoryFrom();
            bmtCtrl.strSelectedProductFrom = product.Name;
            bmtCtrl.onChangeProduct();
            bmtCtrl.selectedBudgetTypeFrom = budgetFrom.BI_MM_BudgetType__c;
            bmtCtrl.onChangeBudgetType();
            bmtCtrl.strSelectedPeriodFrom = [SELECT Id, BI_MM_TimePeriod__c FROM BI_MM_Budget__c WHERE Id = :budgetFrom.Id].BI_MM_TimePeriod__c;
            bmtCtrl.onChangeTimePeriod();

            bmtCtrl.objBudgetFrom.BI_MM_TransferAmount__c = amount;

            BI_MM_Budget__c bdgBeforeRevokeUser = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :budgetFrom.Id];
            //BI_MM_Budget__c bdgBeforeRevokeManager = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :budgetDistribute.Id];
            bmtCtrl.transfer();
            BI_MM_Budget__c bdgAfterRevokeUser = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :budgetFrom.Id];
            //BI_MM_Budget__c bdgAfterRevokeManager = [select Id, BI_MM_AvailableBudget__c from BI_MM_Budget__c where id = :budgetDistribute.Id];

            System.assertEquals(bdgBeforeRevokeUser.BI_MM_AvailableBudget__c - amount, bdgAfterRevokeUser.BI_MM_AvailableBudget__c , 'ERROR While Revoking Budget.');
            // System.assertEquals(bdgBeforeRevokeManager.BI_MM_AvailableBudget__c + amount, bdgAfterRevokeManager.BI_MM_AvailableBudget__c , 'ERROR While Revoking Budget.');

            Test.stopTest();
        }
    }
    static testMethod void BI_MM_BudgetTransferControllerNone(){
        initUserTerritories();

        System.runAs(testUser){
            createData();
            BI_MM_BudgetTransferController bmtCtrl1 = new BI_MM_BudgetTransferController();
            //
            bmtCtrl1.strSelectedTransfer  = Label.BI_MM_None;
            // bmtCtrl1.transferSelection();
            bmtCtrl1.transfer();

            BI_MM_BudgetTransferController bmtCtrl2 = new BI_MM_BudgetTransferController();
            bmtCtrl2.selectedBudgetTypeTo = budgetFrom.BI_MM_BudgetType__c;
            bmtCtrl2.transfer();
        }
    }
    
       /* static testMethod void BI_MM_BudgetcreateBudgetAudit(){ //migart
        initUserTerritories();

        System.runAs(testUser){
            createData();
            BI_MM_BudgetTransferController bmtCtrl = new BI_MM_BudgetTransferController();
            bmtCtrl.selectedBudgetTypeFrom = budgetFrom.BI_MM_BudgetType__c;
		    bmtCtrl.createBudgetAudit(); //migart
            bmtCtrl.runTransfer(); //migart
            bmtCtrl.runDistribute(); //migart
            bmtCtrl.runRevoke(); //migart
        }
    }*/
    static testMethod void BI_MM_BudgetTransferControllerexception(){
        initUserTerritories();

        System.runAs(testUser) {
            createData();

            BI_MM_BudgetTransferController bmtCtrl = new BI_MM_BudgetTransferController();

            Test.startTest();

            bmtCtrl.strSelectedTransfer  = Label.BI_MM_Distribute_Budget;
            bmtCtrl.onChangeTransferType();
            bmtCtrl.selectedTerrNameFrom = mirrorTerrChild1.Name;
            bmtCtrl.onChangeTerritoryFrom();
            bmtCtrl.strSelectedProductFrom = product.Name;
            bmtCtrl.onChangeProduct();
            bmtCtrl.selectedBudgetTypeFrom = budgetFrom.BI_MM_BudgetType__c;
            bmtCtrl.onChangeBudgetType();
            bmtCtrl.strSelectedPeriodFrom = [SELECT Id, BI_MM_TimePeriod__c FROM BI_MM_Budget__c WHERE Id = :budgetFrom.Id].BI_MM_TimePeriod__c;
            bmtCtrl.onChangeTimePeriod();
            bmtCtrl.selectedTerrNameTo = null;
            bmtCtrl.onChangeTerritoryTo();

            bmtCtrl.strSelectedProductTo = Label.BI_MM_None;
            bmtCtrl.transfer();

            Test.stopTest();
        }
    }
}