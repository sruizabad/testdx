/*
Name: BI_MM_BudgetViewControllerForAdmin
Requirement ID: Budget View Utility
Description: This Class is used to view all Budget records based on Territories.
Version | Author-Email | Date | Comment
1.0 | Venkata Madiri | 15.12.2015 | initial version
*/
public without sharing class BI_MM_BudgetViewControllerForAdmin {

    public BI_MM_BudgetViewControllerForAdmin(ApexPages.StandardController controller) {
        initializeData();
    }

    public Boolean isEvent{get;set;}
    public Boolean isAudit{get;set;}
    public List<SelectOption> lstTerritory                          {   get;set;    }
    public List<SelectOption> lstProducts                           {   get;set;    }
    public List<SelectOption> lstBudgetType                         {   get;set;    }
    public List<SelectOption> lstPeriods                            {   get;set;    }
    public List<SelectOption> lstActives                            {   get;set;    }

    public List<BI_MM_Budget__c> lstTeamBudgetToDisplay             {   get;set;    }
    public Transient List<BI_MM_Budget__c> lstAllTeamBudget			{   get;set;    }
    public List<BI_MM_Budget__c> lstMasterBudgetsToDisplay          {   get;set;    }
    public Transient String selectedBudgetType                      {   get;set;    }
    public Transient String strSelectedProductId                    {   get;set;    }
    public Transient String strSelectedTerritory                    {   get;set;    }
    public Transient String strSelectedPeriod                       {   get;set;    }

    public Transient String totalDistributedBudget                  {   get;set;    }
    public Transient String totalCurrentBudget                      {   get;set;    }
    public Transient String totalPlannedAmount                      {   get;set;    }
    public Transient String totalCommittedAmount                    {   get;set;    }
    public Transient String totalPaidAmount                         {   get;set;    }
    public Transient String totalAvailableBudget                    {   get;set;    }
    public Transient String selectedBudgetActive                    {   get;set;    }
    public Transient Boolean allRecordsSelected                     {   get;set;    }

    // public BI_MM_Negative_budget__c negativeBudget                  {   get;set;    }
    public User currentUser                                         {   get;set;    }
    public String strCountryCode                                    {   get;set;    }
    public String strCurrencySymbol                                 {   get;set;    }
    public Set<String> setCountryCodes                              {   get;set;    }
    //public Transient String strRegionCode                           {   get;set;    }

    public integer offsetVar = 0;
    public integer currentPageNumber                                {   get;set;    }
    public integer totalPageNumber                                  {   get;set;    }
    public Boolean showMasterBudget                                 {   get;set;    }
    public Boolean showProjectCode                                  {   get;set;    }

    public String budgetAuditId                                     {   get;set;    }
    public BI_MM_Budget__c objBudgetAudit                           {   get;set;    }
    public List<BI_MM_BudgetAudit__c> lstBudgetAudit                {   get;set;    }
    public List<Medical_Event_vod__c> listMedicalEvent              {   get;set;    }
    public String totalCurrentBudgetAudit                           {   get;set;    }
    public String budgetAuditCurrency                               {   get;set;    }

    public final integer PAGE_SIZE = 50;

    /* Constructor */
    public BI_MM_BudgetViewControllerForAdmin() {
        initializeData();
    }

    /* This method is used to initialize data */
    @testVisible
    void initializeData() {

        try {
            List<User> lstuser = [SELECT Id, DefaultCurrencyIsoCode, Country_Code_BI__c FROM USER WHERE Id =: UserInfo.getUserId() LIMIT 1];
            currentUser = lstuser[0];

            showProjectCode = userIsInRegion();
            strCountryCode = (currentUser.Country_Code_BI__c != null ? currentUser.Country_Code_BI__c : 'None');
            strCurrencySymbol = (currentUser.DefaultCurrencyIsoCode != null ? currentUser.DefaultCurrencyIsoCode : '');

            // Get Countries from User Country Code and BI_MM_CountryCode__c Custom Setting
            setCountryCodes = new Set<String>();
            if(BI_MM_CountryCode__c.getValues(currentUser.Country_Code_BI__c).BI_MM_Region__c != null){
                for(BI_MM_CountryCode__c countryCode : BI_MM_CountryCode__c.getAll().values())
                    if(countryCode.BI_MM_Region__c == BI_MM_CountryCode__c.getValues(currentUser.Country_Code_BI__c).BI_MM_Region__c)
                        setCountryCodes.add(countryCode.Name);
            }
            else setCountryCodes.add(currentUser.Country_Code_BI__c);

            //selectedBudgetActive = false;
            showMasterBudget = false;
            clearPicklists();
            goFirst();
            populateTerritoryPicklist();
            populateProductPicklist();
            populateBudgetTypePicklist();
            populatePeriodPicklist();
            populateActivesPicklist();
        }
        catch(Exception ex) {
            System.debug('=====ex=Line=='+ex.getLineNumber());
            System.debug('=====ex==='+ex);
        }
    }

    /**
     * @Description Paginator method. fills content of Budget Lists
     * @param none
     * @return null
     * @Author OmegaCRM
     */
    public pageReference defaultPaginator(){
        try{
            budgetAuditId = '';
            system.debug('*** BI_MM_BudgetViewControllerForAdmin - defaultPaginator - Start');
            lstTeamBudgetToDisplay = getRefreshedBudgetType(strSelectedTerritory,selectedBudgetType,strSelectedProductId, strSelectedPeriod, true);
            lstMasterBudgetsToDisplay = getMasterBudgets(lstTeamBudgetToDisplay);
            checkAllInputsSelected();
        }
        catch(Exception ex){
            System.debug('=====ex=Line=='+ex.getLineNumber());
            System.debug('=====ex==='+ex);
        }
        return null;
    }

    /**
     * @Description Navigation methods called when navigating pages through the list of Budgets and selecting new search values.
     * @param none
     * @return null
     * @Author OmegaCRM
     */
    public pageReference goFirst(){
        currentPageNumber = 1;
        offsetVar = 0;
        defaultPaginator();
        return null;
    }
    public pageReference goLast(){
        integer listSize = getRefreshedBudgetType(strSelectedTerritory,selectedBudgetType,strSelectedProductId, strSelectedPeriod, false).size();
        offsetVar = PAGE_SIZE*(listSize/PAGE_SIZE);
        defaultPaginator();
        currentPageNumber = totalPageNumber;
        return null;
    }
    public pageReference goPrevious(){
        currentPageNumber--;
        if(offsetVar != 0) offsetVar -= PAGE_SIZE;
        defaultPaginator();
        return null;
    }
    public pageReference goNext(){
        currentPageNumber++;
        offsetVar += PAGE_SIZE;
        defaultPaginator();
        return null;
    }

    /**
     * @Description onChange methods for search picklists. Populate lists using selected search values
     * @param none
     * @return null
     * @Author OmegaCRM
     */
    public pageReference onChangePicklistTerr() {
        try{
            clearProducts();
            clearBudgetTypes();
            clearPeriods();
            goFirst();
            populateProductPicklist();
            populateBudgetTypePicklist();
            populatePeriodPicklist();
        } Catch(Exception ex){
            System.debug('=====ex=Line=='+ex.getLineNumber());
            System.debug('=====ex==='+ex);
        }
        return null;
    }
    public pageReference onChangePicklistProd() {
        try{
            clearBudgetTypes();
            clearPeriods();
            goFirst();
            populateBudgetTypePicklist();
            populatePeriodPicklist();
        } Catch(Exception ex){
            System.debug('=====ex=Line=='+ex.getLineNumber());
            System.debug('=====ex==='+ex);
        }
        return null;
    }
    public pageReference onChangePicklistType() {
        try{
            clearPeriods();
            goFirst();
            populatePeriodPicklist();
        } Catch(Exception ex){
            System.debug('=====ex=Line=='+ex.getLineNumber());
            System.debug('=====ex==='+ex);
        }
        return null;
    }
    public pageReference onChangePicklistPeriod() {
        try{
            goFirst();
        } Catch(Exception ex){
            System.debug('=====ex=Line=='+ex.getLineNumber());
            System.debug('=====ex==='+ex);
        }
        return null;
    }

    /**
     * @Description Generates query to search for available Budgets with provided restrictions on select lists. Also used for getting search list options and Pagination.
     * @param String subTerritory: Territory selected in search list
     * @param String budgetType: Budget Type selected in search list
     * @param String productName: Product selected in search list
     * @param String period: Time Period selected in search list
     * @param Boolean paginator: True, returns a partial list using PAGE_SIZE and Offset value. False, returns full list
     * @return List<BI_MM_Budget__c> List of Budgets obtained with generated query
     * @Author OmegaCRM
     * - 25-Abr-2017 OmegaCRM - Updated: Included fields BI_MM_WBS__c, BI_MM_Department__c, BI_MM_Expense_type__c and BI_MM_Expense_category__c in the query
     */
    public List<BI_MM_Budget__c> getRefreshedBudgetType(String subTerritory, String budgetType, String productName, String period, Boolean paginator){
        List<BI_MM_Budget__c> queryList = new List<BI_MM_Budget__c>();
        string queryString ='SELECT Id, Name, BI_MM_ProductName__c, BI_MM_ProductName__r.Name,BI_MM_StartDate__c, BI_MM_EndDate__c, BI_MM_TimePeriod__c, ' +
                                    'BI_MM_CurrentBudget__c, BI_MM_AvailableBudget__c, ' +
                                    'BI_MM_DistributedBudget__c, BI_MM_Committed_amount__c, BI_MM_PaidAmount__c, BI_MM_PlannedAmount__c, ' +
                                    'convertCurrency(BI_MM_CurrentBudget__c) convCurrent, convertCurrency(BI_MM_AvailableBudget__c) convAvailable, ' +
                                    'convertCurrency(BI_MM_DistributedBudget__c) convDistributed, convertCurrency(BI_MM_Committed_amount__c) convCommitted, ' +
                                    'convertCurrency(BI_MM_PaidAmount__c) convPaid, convertCurrency(BI_MM_PlannedAmount__c) convPlanned, ' +
                                    'BI_MM_TerritoryName__r.name, BI_MM_ManagerTerritory__c,BI_MM_ManagerTerritory__r.name,BI_MM_TerritoryName__c,' +
                                    'BI_MM_BudgetType__c, BI_MM_Is_active__c, BI_MM_Current_amount_percentage__c, BI_MM_Master_budget__c, BI_MM_Project_code__c, ' + /**BI_MM_Country_Code_BI__c '+*/
                                    'BI_MM_WBS__c, BI_MM_Department__c, BI_MM_Expense_type__c, BI_MM_Expense_category__c ' +
                             'FROM BI_MM_Budget__c '+
                             'WHERE BI_MM_TerritoryName__c != null AND BI_MM_ManagerTerritory__c != null AND BI_MM_Is_master_budget__c = false';

        // queryString += ' AND BI_MM_Country_Code_BI__c =\'' + string.escapeSingleQuotes(strCountryCode)+'\'' ;

        queryString += ' AND BI_MM_Country_code__c IN :setCountryCodes';

        if(subTerritory != null && subTerritory != Label.BI_MM_All)
            queryString += ' AND BI_MM_TerritoryName__c =\'' + string.escapeSingleQuotes(subTerritory)+'\'' ;
        if(productName != null && productName != Label.BI_MM_All)
            queryString += ' AND BI_MM_ProductName__r.Name =\'' + string.escapeSingleQuotes(productName)+'\'' ;
        if(budgetType != null && budgetType != Label.BI_MM_All)
            queryString += ' AND BI_MM_BudgetType__c =\'' + string.escapeSingleQuotes(budgetType)+'\'' ;
        if(period != null && period != Label.BI_MM_All)
            queryString += ' AND BI_MM_TimePeriod__c =\'' + string.escapeSingleQuotes(period)+'\'' ;

        // Add List of all Budgets
        try {
            lstAllTeamBudget = database.query(queryString);
        } catch (Exception ex) {
            System.debug('=====ex=Line=='+ex.getLineNumber());
            System.debug('=====ex==='+ex);
        }

        if(selectedBudgetActive == Label.BI_MM_Active)
            queryString += ' AND BI_MM_Is_active__c = true' ;
        else if(selectedBudgetActive == Label.BI_MM_Inactive)
            queryString += ' AND BI_MM_Is_active__c = false' ;

        queryString += ' ORDER BY Name';

        // Paginator control
        if (paginator){
            try {
                totalPageNumber=(database.query(queryString).size()/PAGE_SIZE)+1;
                if(totalPageNumber < 1) totalPageNumber = 1;
            } catch(Exception Ex){
                System.debug('=====ex=Line=='+ex.getLineNumber());
                System.debug('=====ex==='+ex);
            }
            queryString += ' LIMIT ' + PAGE_SIZE + ' offset '+string.valueOf(offsetvar);
        }
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getRefreshedBudgetType - Query String == '+queryString);
        try {
            queryList = database.query(queryString);
        }catch (Exception ex) {
            System.debug('=====ex=Line=='+ex.getLineNumber());
            System.debug('=====ex==='+ex);
        }

        calculateTotalAmounts(queryList);
        return queryList;
    }

    /**
     * @Description given a List of Budgets, calculate total values of Amounts
     * @param List<BI_MM_Budget__c> lstBudgets: List of Budgets
     * @Author OmegaCRM
     * - 22-feb-2018 OmegaCRM - Created
     */
    public void calculateTotalAmounts(List<BI_MM_Budget__c> lstBudgets){

        Double sumCurrentBudget = 0;
        Double sumDistributedBudget = 0;
        Double sumPlannedAmount = 0;
        Double sumCommittedAmount = 0;
        Double sumPaidAmount = 0;
        Double sumAvailableBudget = 0;

        for(BI_MM_Budget__c objBudget : lstBudgets){
            sumCurrentBudget += (objBudget.get('convCurrent') != null ? Double.valueOf(String.valueOf(objBudget.get('convCurrent'))) : 0);
            sumDistributedBudget += (objBudget.get('convDistributed') != null ? Double.valueOf(String.valueOf(objBudget.get('convDistributed'))) : 0);
            sumPlannedAmount += (objBudget.get('convPlanned') != null ? Double.valueOf(String.valueOf(objBudget.get('convPlanned'))) : 0);
            sumCommittedAmount += (objBudget.get('convCommitted') != null ? Double.valueOf(String.valueOf(objBudget.get('convCommitted'))) : 0);
            sumPaidAmount += (objBudget.get('convPaid') != null ? Double.valueOf(String.valueOf(objBudget.get('convPaid'))) : 0);
            sumAvailableBudget += (objBudget.get('convAvailable') != null ? Double.valueOf(String.valueOf(objBudget.get('convAvailable'))) : 0);
        }
        totalCurrentBudget = String.valueOf(sumCurrentBudget);
        totalDistributedBudget = String.valueOf(sumDistributedBudget);
        totalPlannedAmount = String.valueOf(sumPlannedAmount);
        totalCommittedAmount = String.valueOf(sumCommittedAmount);
        totalPaidAmount = String.valueOf(sumPaidAmount);
        totalAvailableBudget = String.valueOf(sumAvailableBudget);
    }

    /**
     * @Description Clear available options for the search lists
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void clearPicklists() {
        clearTerritories();
        clearProducts();
        clearBudgetTypes();
        clearPeriods();
    }
    public void clearTerritories(){
        lstTerritory = new List<SelectOption>();
        lstTerritory.add(new SelectOption(Label.BI_MM_All,Label.BI_MM_All));
        strSelectedTerritory = Label.BI_MM_All;
    }
    public void clearProducts(){
        lstProducts = new List<SelectOption>();
        lstProducts.add(new SelectOption(Label.BI_MM_All,Label.BI_MM_All));
        strSelectedProductId = Label.BI_MM_All;
    }
    public void clearBudgetTypes(){
        lstBudgetType = new List<SelectOption>();
        lstBudgetType.add(new SelectOption(Label.BI_MM_All,Label.BI_MM_All));
        selectedBudgetType = Label.BI_MM_All;
    }
    public void clearPeriods(){
        lstPeriods = new List<SelectOption>();
        lstPeriods.add(new SelectOption(Label.BI_MM_All,Label.BI_MM_All));
        strSelectedPeriod = Label.BI_MM_All;
    }

    /**
     * @Description Populates options in lstTerritory List to search Budgets using Territory
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void populateTerritoryPicklist(){

        Map<String, String> terrMap = new Map<String, String>();
        List<SelectOption> lstUnsortTerritory = new List<SelectOption>();

        lstTerritory = new List<SelectOption>();
        lstTerritory.add(new SelectOption(Label.BI_MM_All,Label.BI_MM_All));

        for(BI_MM_Budget__c objTerr : [SELECT Id, BI_MM_TerritoryName__r.Name, BI_MM_TerritoryName__c FROM BI_MM_Budget__c]){
            if(objTerr.BI_MM_TerritoryName__c != null)
                terrMap.put(objTerr.BI_MM_TerritoryName__c, objTerr.BI_MM_TerritoryName__r.Name);
        }
        for(String objTerr :terrMap.keySet() ) {
            lstUnsortTerritory.add(new SelectOption(objTerr,terrMap.get(objTerr)));
        }
        // Call method to sort Select option
        BI_MM_RecursionHandler.doSort(lstUnsortTerritory, BI_MM_RecursionHandler.FieldToSort.Label);

        for(SelectOption objSelectOption :lstUnsortTerritory ) {
            lstTerritory.add(objSelectOption);
        }
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - populateBudgetTypePicklist - lstTerritory == ' + lstTerritory);
    }

    /**
     * @Description Populates options in lstProducts List to search Budgets using Product
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void populateProductPicklist(){

        Set<String> productSet = new Set<String>();
        List<SelectOption> lstUnsortProducts = new List<SelectOption>();

        lstProducts = new List<SelectOption>();
        lstProducts.add(new SelectOption(Label.BI_MM_All,Label.BI_MM_All));

        for(BI_MM_Budget__c objProduct : lstAllTeamBudget){
            if(objProduct.BI_MM_ProductName__c != null)
                productSet.add(objProduct.BI_MM_ProductName__r.Name);
        }

        for(String objProduct :productSet ) {
            lstUnsortProducts.add(new SelectOption(objProduct, objProduct));
        }

        // Call method to sort Select option
        BI_MM_RecursionHandler.doSort(lstUnsortProducts, BI_MM_RecursionHandler.FieldToSort.Label);

        for(SelectOption objSelectOption :lstUnsortProducts ) {
            lstProducts.add(objSelectOption);
        }
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - populateBudgetTypePicklist - lstProducts == ' + lstProducts);
    }

    /**
     * @Description Populates options in lstBudgetType List to search Budgets using BudgetType
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void populateBudgetTypePicklist(){

        Set<String> budTypeSet =  new Set<String>();
        List<SelectOption> lstUnsortBudgetType = new List<SelectOption>();

        lstBudgetType = new List<SelectOption>();
        lstBudgetType.add(new SelectOption(Label.BI_MM_All,Label.BI_MM_All));

        for(BI_MM_Budget__c objBudget : lstAllTeamBudget) {
            if(objBudget.BI_MM_BudgetType__c != null)
                budTypeSet.add(objBudget.BI_MM_BudgetType__c);
        }
        for(String objBudType : budTypeSet){
            lstUnsortBudgetType.add(new SelectOption(objBudType,objBudType));
        }
        // Call method to sort Select option
        BI_MM_RecursionHandler.doSort(lstUnsortBudgetType, BI_MM_RecursionHandler.FieldToSort.Label);

        for(SelectOption objSelectOption :lstUnsortBudgetType )
            lstBudgetType.add(objSelectOption);
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - populateBudgetTypePicklist - lstBudgetType == ' + lstBudgetType);
    }

    /**
     * @Description Populates options in lstPeriods List to search Budgets using TimePeriod
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void populatePeriodPicklist(){

        Set<String> periodSet =  new Set<String>();
        List<SelectOption> lstUnsortPeriod = new List<SelectOption>();

        lstPeriods = new List<SelectOption>();
        lstPeriods.add(new SelectOption(Label.BI_MM_All,Label.BI_MM_All));

        for(BI_MM_Budget__c objBudget : lstAllTeamBudget) {
            if(objBudget.BI_MM_TimePeriod__c != null)
                periodSet.add(objBudget.BI_MM_TimePeriod__c);
        }
        for(String objPeriod : periodSet){
            lstUnsortPeriod.add(new SelectOption(objPeriod,objPeriod));
        }
        // Call method to sort Select option
        BI_MM_RecursionHandler.doSort(lstUnsortPeriod, BI_MM_RecursionHandler.FieldToSort.Label);

        for(SelectOption objSelectOption :lstUnsortPeriod )
            lstPeriods.add(objSelectOption);
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - populateBudgetTypePicklist - lstPeriods == ' + lstPeriods);
    }

    /**
     * @Description Populates options in lstActives List to search Budgets using Active/Inactive state
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public void populateActivesPicklist(){
        lstActives = new List<SelectOption>();
        lstActives.add(new SelectOption(Label.BI_MM_All, Label.BI_MM_All));
        lstActives.add(new SelectOption(Label.BI_MM_Inactive, Label.BI_MM_Inactive));
        lstActives.add(new SelectOption(Label.BI_MM_Active, Label.BI_MM_Active));
    }

    /**
     * @Description Save and update current data in List lstTeamBudgetToDisplay
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    public pageReference Save() {
        try{
            database.update(lstTeamBudgetToDisplay);
            goFirst();
        }
        Catch(Exception Ex) {}
        return null;
    }

    /**
     * @Description Creates URL and redirects to BI_MM_BudgetViewExcelReport page using current page parameters
     * @param none
     * @return pageReference
     * @Author OmegaCRM
     */
    public pageReference redirectIntoExcelReport() {
        PageReference pageRef = Page.BI_MM_BudgetViewExcelReport;
        pageRef.getParameters().put('Terr', strSelectedTerritory);
        pageRef.getParameters().put('Type', selectedBudgetType);
        pageRef.getParameters().put('Prod', strSelectedProductId);
        pageRef.getParameters().put('Peri', strSelectedPeriod);
        pageRef.getParameters().put('IsAc', selectedBudgetActive);
        pageRef.setRedirect(true);
        system.debug('*** BI_MM_BudgetViewControllerForAdmin - redirectIntoExcelReport - pageRef == ' + pageRef);
        return pageRef;
    }

    /**
     * @Description link to BI_MM_BudgetViewExcelReport with parameters for BudgetAudit Excel Report
     * @param N/A
     * @return pageReference pageRef: Returns pageReference link to BI_MM_BudgetViewExcelReport
     * @Author OmegaCRM
     */
    public pageReference redirectIntoExcelReportAudit() {
        PageReference pageRef = Page.BI_MM_BudgetViewExcelReport;
        pageRef.getParameters().put('AudB', budgetAuditId);
        pageRef.setRedirect(true);
        system.debug('*** BI_MM_BudgetViewControllerForAdmin - redirectIntoExcelReport - pageRef == ' + pageRef);
        return pageRef;
    }
    
    /**
     * @Description link to BI_MM_BudgetViewExcelReport with parameters for BudgetEvents Excel Report
     * @param N/A
     * @return pageReference pageRef: Returns pageReference link to BI_MM_BudgetViewExcelReportEvents
     * @Author OmegaCRM
     */
    public pageReference redirectIntoExcelReportEvents() {
        PageReference pageRef = Page.BI_MM_BudgetViewExcelReportEvents;
        pageRef.getParameters().put('AudB', budgetAuditId);
        pageRef.setRedirect(true);
        system.debug('*** BI_MM_BudgetViewControllerForAdmin - redirectIntoExcelReport - pageRef == ' + pageRef);
        return pageRef;
    }

    /**
     * @Description Method that select BI_MM_Is_active__c Budget field of all rendered records
     * @Author OmegaCRM
     */
    public void selectAllRecords(){
        showActivationMessage();

        for(BI_MM_Budget__c currentBudget : lstTeamBudgetToDisplay){
            currentBudget.BI_MM_Is_active__c = allRecordsSelected;
        }
    }

    /**
     * @Description Method that shows info message
     * @Author OmegaCRM
     */
    public void showActivationMessage(){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, Label.BI_MM_Active_applied_message));
    }

    /**
     * @Description Method triggered when BI_MM_Is_active__c is changed. It causes "Select all" checkbox update
     * @Author OmegaCRM
     */
    public void onChangeActive(){
        showActivationMessage();
        checkAllInputsSelected();
    }

    /**
     * @Description Method that automatically updates "Select all" checkbox based on BI_MM_Is_active__c values
     * @Author OmegaCRM
     */
    public void checkAllInputsSelected(){
        Boolean allSelected = true;

        for(BI_MM_Budget__c currentBudget : lstTeamBudgetToDisplay){
            if(currentBudget.BI_MM_Is_active__c == false)
                allSelected = false;
        }

        allRecordsSelected = allSelected;
    }

    /**
     * @Description Method that returns the List of Master Budgets. The List will include the Master Budgets of the Budgets visible in 'All Budgets' List
     * @param List<BI_MM_Budget__c> lstTeamBudgetToDisplay: List of Budgets available using the current search parameters
     * @return List<BI_MM_Budget__c> lstMasterBudgets: List of Master Budgets of provided Budget List
     * @Author OmegaCRM
     */
    public List<BI_MM_Budget__c> getMasterBudgets(List<BI_MM_Budget__c> lstTeamBudgetToDisplay){
        Set<Id> setMasterBudgets = new Set<Id>();
        List<BI_MM_Budget__c> lstMasterBudgets = new List<BI_MM_Budget__c>();
        for(BI_MM_Budget__c budget : lstTeamBudgetToDisplay){
            if(budget.BI_MM_Master_budget__c != null)
                setMasterBudgets.add(budget.BI_MM_Master_budget__c);
        }
        if(setMasterBudgets.size()>0){
            showMasterBudget = true;
            lstMasterBudgets = [SELECT Id, Name, BI_MM_StartDate__c, BI_MM_EndDate__c, BI_MM_TimePeriod__c, BI_MM_BudgetType__c, BI_MM_Is_active__c,
                                       BI_MM_CurrentBudget__c, BI_MM_AvailableBudget__c, BI_MM_DistributedBudget__c, BI_MM_Committed_amount__c, BI_MM_PlannedAmount__c, BI_MM_PaidAmount__c,
                                       BI_MM_ProductName__c, BI_MM_TerritoryName__c, BI_MM_ManagerTerritory__c,
                                       BI_MM_Project_code__c, BI_MM_WBS__c, BI_MM_Department__c, BI_MM_Expense_type__c, BI_MM_Expense_category__c
                                  FROM BI_MM_Budget__c WHERE Id IN: setMasterBudgets];
        }
        else showMasterBudget = false;

        return lstMasterBudgets;
    }

    /**
     * @Description Returns whether the user's CountryCode is in Custom Setting BI_MM_CountryCode__c and the country is in a Region
     * @param none
     * @return Boolean userInRegion: True if User's countryCode is in BI_MM_CountryCode__c and BI_MM_Region__c is not Null. False otherwise.
     * @Author OmegaCRM
     */
    public Boolean userIsInRegion() {
        Boolean userInRegion = false;
        if (BI_MM_CountryCode__c.getValues(currentUser.Country_Code_BI__c).BI_MM_Region__c != null) userInRegion = true;
        return userInRegion;
    }

    public void voidMethod(){}

    /**
     * @Description Change Budget selected for Audit Table and call to refresh Budget Audit Table content
     * @param None
     * @return None
     * @Author OmegaCRM
     */
    public pageReference selectBudgetAudit() {

        lstBudgetAudit = new List<BI_MM_BudgetAudit__c>();
        objBudgetAudit = new BI_MM_Budget__c();
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - selectBudgetAudit - budgetAuditId == ' + budgetAuditId);
        objBudgetAudit = getBudgetAudit(budgetAuditId);

        try {
            lstBudgetAudit = getBudgetAuditTable(objBudgetAudit);
        }catch(Exception ex) {
            System.debug('=====ex=Line=='+ex.getLineNumber());
            System.debug('=====ex==='+ex);
        }
        listMedicalEvent = getEventList(budgetAuditId);
        return null;
    }

    /**
     * @Description Get Budget selected for Audit Table from the Id
     * @param Id budAuditId: Id of Budget to get
     * @return None
     * @Author OmegaCRM
     */
    public BI_MM_Budget__c getBudgetAudit(String budAuditId) {

        BI_MM_Budget__c objGetBudgetAudit = [SELECT Id, Name, BI_MM_TerritoryName__c, BI_MM_TerritoryName__r.Name, BI_MM_ProductName__c, BI_MM_ProductName__r.Name,
                                 BI_MM_BudgetType__c, BI_MM_TimePeriod__c
                            FROM BI_MM_Budget__c WHERE Id =: budAuditId LIMIT 1];
        return objGetBudgetAudit;
    }

    /**
     * @Description Refresh Budget Audit Table content
     * @param BI_MM_Budget__c objBudget: Budget to get audit Data
     * @return List<BI_MM_BudgetAudit__c> lstNewBudgetAudit:
     * @Author OmegaCRM
     */
    public List<BI_MM_BudgetAudit__c> getBudgetAuditTable(BI_MM_Budget__c objBudget){

        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getBudgetAuditTable - START');
        List<BI_MM_BudgetAudit__c> lstNewBudgetAudit = new List<BI_MM_BudgetAudit__c>();
        // Get List of budgetAudit
        List<BI_MM_BudgetAudit__c> lstGetBudgetAudit =
            [SELECT Id, Name, BI_MM_SendingAmount__c, convertCurrency(BI_MM_SendingAmount__c) convSending,
                    BI_MM_Budget_Indicator__c, BI_MM_BudgetFrom__c, BI_MM_BudgetTo__c,
                    BI_MM_FromUserTerritoryName__c, BI_MM_ToUserTerritoryName__c, CreatedById, CreatedDate
               FROM BI_MM_BudgetAudit__c
              WHERE BI_MM_BudgetFrom__r.Id =: objBudget.Id
                 OR BI_MM_BudgetTo__r.Id =: objBudget.Id
              ORDER BY CreatedDate];

        // Get List of budgetStageAudit
        List<BI_MM_BudgetStageAudit__c> lstGetBudgetStageAudit =
            [SELECT Id, Name, BI_MM_CurrentBudget__c, convertCurrency(BI_MM_CurrentBudget__c) convCurrent,
                    BI_MM_BudgetIndicator__c, BI_MM_Budget_To__c, BI_MM_UserTerritory__c, CreatedById, CreatedDate
               FROM BI_MM_BudgetStageAudit__c
              WHERE BI_MM_Budget_To__r.Id =: objBudget.Id
              ORDER BY CreatedDate];

        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getBudgetAuditTable - lstGetBudgetAudit == ' + lstGetBudgetAudit);
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getBudgetAuditTable - lstGetBudgetStageAudit == ' + lstGetBudgetStageAudit);

        Double sumCurrentBudgetAudit = 0;
        Double dblCurrentBudgetAudit = 0;
        budgetAuditCurrency = objBudget.CurrencyIsoCode;
        Datetime dtBudgetAudit;
        Datetime dtBudgetStageAudit;
        BI_MM_BudgetAudit__c objNewBudgetAudit = new BI_MM_BudgetAudit__c();

        // Join records in Budgetaudit Table
        while (lstGetBudgetAudit.size() > 0 || lstGetBudgetStageAudit.size() > 0) {

            dtBudgetAudit = (lstGetBudgetAudit.size() > 0 ? lstGetBudgetAudit[0].CreatedDate : System.now());
            dtBudgetStageAudit = (lstGetBudgetStageAudit.size() > 0 ? lstGetBudgetStageAudit[0].CreatedDate : System.now());

            if (dtBudgetAudit < dtBudgetStageAudit){
                // System.debug('*** BI_MM_BudgetViewControllerForAdmin - getBudgetAuditTable - lstGetBudgetAudit[0] == ' + lstGetBudgetAudit[0]);
                dblCurrentBudgetAudit = (lstGetBudgetAudit[0].BI_MM_SendingAmount__c != null ? lstGetBudgetAudit[0].BI_MM_SendingAmount__c : 0);
                objNewBudgetAudit = new BI_MM_BudgetAudit__c(
                    BI_MM_BudgetType__c = lstGetBudgetAudit[0].CreatedDate.format(),
                    BI_MM_Budget_Indicator__c = (lstGetBudgetAudit[0].BI_MM_Budget_Indicator__c != null ? lstGetBudgetAudit[0].BI_MM_Budget_Indicator__c : Label.BI_MM_AuditList_Unknown),
                    BI_MM_SendingAmount__c = dblCurrentBudgetAudit,
                    BI_MM_ReceivingAmount__c = dblCurrentBudgetAudit,
                    BI_MM_BudgetFrom__c = lstGetBudgetAudit[0].BI_MM_BudgetFrom__c,
                    BI_MM_BudgetTo__c = lstGetBudgetAudit[0].BI_MM_BudgetTo__c,
                    BI_MM_FromUserTerritoryName__c = lstGetBudgetAudit[0].BI_MM_FromUserTerritoryName__c,
                    BI_MM_ToUserTerritoryName__c = lstGetBudgetAudit[0].BI_MM_ToUserTerritoryName__c,
                    BI_MM_ToUserName__c = lstGetBudgetAudit[0].CreatedById,
                    CurrencyIsoCode = budgetAuditCurrency
                );
                lstGetBudgetAudit.remove(0);
            }
            else {
                dblCurrentBudgetAudit = (lstGetBudgetStageAudit[0].BI_MM_CurrentBudget__c != null ? lstGetBudgetStageAudit[0].BI_MM_CurrentBudget__c : 0);
                objNewBudgetAudit = new BI_MM_BudgetAudit__c(
                    BI_MM_BudgetType__c = lstGetBudgetStageAudit[0].CreatedDate.format(),
                    BI_MM_Budget_Indicator__c = (lstGetBudgetStageAudit[0].BI_MM_BudgetIndicator__c != null ? lstGetBudgetStageAudit[0].BI_MM_BudgetIndicator__c : Label.BI_MM_AuditList_Unknown),
                    BI_MM_SendingAmount__c = dblCurrentBudgetAudit,
                    BI_MM_ReceivingAmount__c = dblCurrentBudgetAudit,
                    BI_MM_BudgetTo__c = lstGetBudgetStageAudit[0].BI_MM_Budget_To__c,
                    BI_MM_ToUserTerritoryName__c = lstGetBudgetStageAudit[0].BI_MM_UserTerritory__c,
                    BI_MM_ToUserName__c = lstGetBudgetStageAudit[0].CreatedById,
                    CurrencyIsoCode = budgetAuditCurrency
                );
                lstGetBudgetStageAudit.remove(0);
            }
            dblCurrentBudgetAudit = getAppliedAuditAmount(objNewBudgetAudit);
            objNewBudgetAudit.BI_MM_ReceivingAmount__c = dblCurrentBudgetAudit;
            objNewBudgetAudit.BI_MM_Budget_Indicator__c = getTranslatedIndicator(objNewBudgetAudit.BI_MM_Budget_Indicator__c);
            lstNewBudgetAudit.add(objNewBudgetAudit);
            sumCurrentBudgetAudit += dblCurrentBudgetAudit;
        }

        totalCurrentBudgetAudit = String.valueOf(sumCurrentBudgetAudit);

        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getBudgetAuditTable - sumCurrentBudgetAudit == ' + sumCurrentBudgetAudit);
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getBudgetAuditTable - lstNewBudgetAudit == ' + lstNewBudgetAudit);

        return lstNewBudgetAudit;
    }

    /**
     * @Description Given an Audit record, returns the Applied Amount to the selected Budget for Audit Table
     * @param BI_MM_BudgetAudit__c budgetAudit: BudgetAudit record
     * @return Double dblAppliedBudgetAudit: Applied Amount for Audit record
     * @Author OmegaCRM
     */
    public Double getAppliedAuditAmount(BI_MM_BudgetAudit__c budgetAudit){

        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getAppliedAuditAmount - START');

        Double dblAppliedBudgetAudit = budgetAudit.BI_MM_SendingAmount__c;

        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getAppliedAuditAmount - budgetAuditId = ' + budgetAuditId);
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getAppliedAuditAmount - budgetAudit.BI_MM_BudgetFrom__c = ' + budgetAudit.BI_MM_BudgetFrom__c);

        if (budgetAudit.BI_MM_BudgetFrom__c == budgetAuditId) {
            // FROM Distribution -> 0
            if (budgetAudit.BI_MM_Budget_Indicator__c == System.Label.BI_MM_Distribute_Budget_Audit){
                dblAppliedBudgetAudit = 0;
            }
            // FROM Revoke -> Negative
            // FROM Transfer -> Negative
            else if (budgetAudit.BI_MM_Budget_Indicator__c == System.Label.BI_MM_Revoke_Budget_Audit ||
                     budgetAudit.BI_MM_Budget_Indicator__c == System.Label.BI_MM_Transfer_Budget_Audit){
                dblAppliedBudgetAudit = dblAppliedBudgetAudit*(-1);
            }
        }
        else if (budgetAudit.BI_MM_BudgetTo__c == budgetAuditId) {
            // TO Revoke -> 0
            if (budgetAudit.BI_MM_Budget_Indicator__c == System.Label.BI_MM_Revoke_Budget_Audit){
                dblAppliedBudgetAudit = 0;
            }
            // TO Sub -> Negative
            else if (budgetAudit.BI_MM_Budget_Indicator__c == System.Label.BI_MM_Subtract){
                dblAppliedBudgetAudit = dblAppliedBudgetAudit*(-1);
            }
        }
        return dblAppliedBudgetAudit;
    }

    /**
     * @Description Given an Audit Indicator, returns the label with the translated values for that concept
     * @param String indicator: Indicator from BudgetAudit record
     * @return String trIndicator: Translated Indicator
     * @Author OmegaCRM
     */
    public String getTranslatedIndicator(String indicator){

        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getTranslatedIndicator - START');

        String trIndicator;

        if (indicator == System.Label.BI_MM_Transfer_Budget_Audit) trIndicator = System.Label.BI_MM_Transfer_Budget_View;
        else if (indicator == System.Label.BI_MM_Distribute_Budget_Audit) trIndicator = System.Label.BI_MM_Distribute_Budget_View;
        else if (indicator == System.Label.BI_MM_Revoke_Budget_Audit) trIndicator = System.Label.BI_MM_Revoke_Budget_View;
        else if (indicator == System.Label.BI_MM_Initial) trIndicator = System.Label.BI_MM_Initial_View;
        else if (indicator == System.Label.BI_MM_Add) trIndicator = System.Label.BI_MM_Add_View;
        else if (indicator == System.Label.BI_MM_Subtract) trIndicator = System.Label.BI_MM_Subtract_View;
        else trIndicator = indicator;
        System.debug('*** BI_MM_BudgetViewControllerForAdmin - getTranslatedIndicator - indicator => trIndicator = ' + indicator + ' => ' + trIndicator);
        return trIndicator;
    }
    
    public List<Medical_Event_vod__c> getEventList(String budAuditId){
        System.debug('***QUERY ' + budAuditId);
        List<BI_MM_BudgetEvent__c> listBudgetEvent = [SELECT Id, Name, BI_MM_EventID__c FROM BI_MM_BudgetEvent__c WHERE BI_MM_BudgetID__c =: budAuditId];
        List<String> listEventToReturnIds = new List<String>();
        for(BI_MM_BudgetEvent__c bugEv : listBudgetEvent){
            listEventToReturnIds.add(bugEv.BI_MM_EventID__c);
        }
        return [SELECT Id, Name,Start_Date_vod__c, End_Date_vod__c, Event_Status_BI__c, Expense_Amount_vod__c, Product_BI__r.Name, Event_ID_BI__c, Total_Cost_BI__c FROM Medical_Event_vod__c WHERE Id IN: listEventToReturnIds];
    }
}