/*
Name: BI_MM_BudgetViewControllerForAdminTest
Requirement ID: Budget View Utility
Description: BI_MM_BudgetViewControllerForAdmin
Version | Author-Email | Date | Comment
1.0 | Venkata Madiri | 07.12.2015 | initial version
*/
@isTest
private class BI_MM_BudgetViewControllerForAdminTest {
	
    static testMethod void testBudgetViewControllerForAdmin() {
	
        BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();

        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];  // Get BI_MM_ADMIN permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and  null Manager
        User testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, null);
        dataFactory.addPermissionSet(testUser, permissionAdmin);

        // Insert new Territories
        Territory parentTerritory = dataFactory.generateTerritory('Test Territory 1', null);
        Territory childTerritory = dataFactory.generateTerritory('Test Territory 2', parentTerritory.Id);
        Territory childTerritory2 = dataFactory.generateTerritory('Test Territory 3', parentTerritory.Id);//migart

        System.runAs(testUser){
            List<BI_MM_Budget__c> lstBudgetToInsert = new List<BI_MM_Budget__c>();
            List<Product_vod__c> lstProductToInsert = new List<Product_vod__c>();

            BI_MM_MirrorTerritory__c mt = dataFactory.generateMirrorTerritory ('sr01Test', childTerritory.Id);
            BI_MM_MirrorTerritory__c mt1 = dataFactory.generateMirrorTerritory ('dm01', parentTerritory.Id);
            BI_MM_MirrorTerritory__c mt2 = dataFactory.generateMirrorTerritory ('sr02Test', childTerritory2.Id);//migart

            Test.startTest();

                // Insert Products records
                for(Integer intNumber = 0; intNumber < 2; intNumber++) {
                    //Create your product
                    Product_vod__c objProduct = dataFactory.generateProduct('Product'+intNumber, 'Detail', testUser.Country_Code_BI__c);
                    lstProductToInsert.add(objProduct);
                }

                /*if(!lstProductToInsert.isEmpty())
                    insert lstProductToInsert;*/
                BI_MM_CountryCode__c objCountryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c);



                // Insert BI_MM_Budget__c records
                lstBudgetToInsert = dataFactory.generateBudgets(100, mt.Id, mt1.Id, 'Micro Marketing', lstProductToInsert[0], 4000, true);

                /*if(!lstBudgetToInsert.isEmpty())
                    insert lstBudgetToInsert;*/

                // Instantiate BI_MM_BudgetViewControllerForAdmin class
                BI_MM_BudgetViewControllerForAdmin objBI_MM_BudgetViewControllerForAdmin = new BI_MM_BudgetViewControllerForAdmin();
                objBI_MM_BudgetViewControllerForAdmin.selectedBudgetType = 'All';

                objBI_MM_BudgetViewControllerForAdmin.lstTeamBudgetToDisplay = objBI_MM_BudgetViewControllerForAdmin.getRefreshedBudgetType(
                                                                                    objBI_MM_BudgetViewControllerForAdmin.selectedBudgetType,
                                                                                    objBI_MM_BudgetViewControllerForAdmin.strSelectedProductId,
                                                                                    objBI_MM_BudgetViewControllerForAdmin.strSelectedTerritory,
                                                                                    objBI_MM_BudgetViewControllerForAdmin.strSelectedPeriod,
                                                                                    false);

                System.debug(LoggingLevel.ERROR, '***lstBudgetToInsert size = '+objBI_MM_BudgetViewControllerForAdmin.lstTeamBudgetToDisplay.size());
                // Deactivate the budget
                BI_MM_Budget__c bdgBeforeInactive = objBI_MM_BudgetViewControllerForAdmin.lstTeamBudgetToDisplay.get(0);
                bdgBeforeInactive.BI_MM_Is_active__c = false;
                objBI_MM_BudgetViewControllerForAdmin.save();
                BI_MM_Budget__c bdgAfterInactive = [select Id, BI_MM_Is_active__c from BI_MM_Budget__c where Id =:bdgBeforeInactive.Id];
                System.assertEquals(false, bdgAfterInactive.BI_MM_Is_active__c, 'Budget was not deactivated correctly');

                delete lstBudgetToInsert;

                // Insert Single BI_MM_Budget__c record
                BI_MM_Budget__c budget = dataFactory.generateBudgets(1, mt.Id, null, 'Micro Marketing', lstProductToInsert[0], 4000, true).get(0);
                
                //Generat Budget Audit registries - migart                 	
			    BI_MM_Budget__c budgetDistribute;//migart
			    BI_MM_Budget__c budgetFrom;//migart
				BI_MM_Budget__c budgetTo;//migart
				String budgType ='All';//migart
				User thisUser;//migart
                                
                thisUser = [select Id from User where Id = :UserInfo.getUserId()];
                budgetDistribute = dataFactory.generateBudgets(1, mt.Id, null, budgType, lstProductToInsert[0], 4000, true).get(0);
        		budgetFrom = dataFactory.generateBudgets(1, mt1.Id, mt.Id, budgType, lstProductToInsert[0], 1000, true).get(0);
        		budgetTo = dataFactory.generateBudgets(1, mt2.Id, mt.Id, budgType, lstProductToInsert[0], 1000, true).get(0);
                BI_MM_BudgetAudit__c budAudTrans = dataFactory.generateBudgetAudit(budgetFrom, budgetTo, testUser, thisUser, Label.BI_MM_Transfer_Budget_Audit);
                BI_MM_BudgetAudit__c budAudDistr = dataFactory.generateBudgetAudit(budgetFrom, budgetTo, testUser, thisUser, Label.BI_MM_Distribute_Budget_Audit);
        		BI_MM_BudgetAudit__c budAudRevok = dataFactory.generateBudgetAudit(budgetFrom, budgetTo, testUser, thisUser, Label.BI_MM_Revoke_Budget_Audit);
        		System.debug('*** BI_MM_BudgetViewControllerForAdmin_Test - budAudTrans = ' + budAudTrans);
        		System.debug('*** BI_MM_BudgetViewControllerForAdmin_Test - budAudTrans = ' + budAudDistr);
        		System.debug('*** BI_MM_BudgetViewControllerForAdmin_Test - budAudTrans = ' + budAudRevok);
        		objBI_MM_BudgetViewControllerForAdmin.budgetAuditId=budgetFrom.Id;
        		
        		pageReference pgbudgetAudit = objBI_MM_BudgetViewControllerForAdmin.selectBudgetAudit(); //migart
        		

                // Instantiate another BI_MM_BudgetViewControllerForAdmin class
                BI_MM_BudgetViewControllerForAdmin objBI_MM_BudgetViewControllerForAdmin2 = new BI_MM_BudgetViewControllerForAdmin();
                objBI_MM_BudgetViewControllerForAdmin2.selectedBudgetType = 'All';
                objBI_MM_BudgetViewControllerForAdmin2.strSelectedTerritory = 'Test';
                objBI_MM_BudgetViewControllerForAdmin2.selectedBudgetActive = 'Inactive';
                objBI_MM_BudgetViewControllerForAdmin2.goFirst();

                objBI_MM_BudgetViewControllerForAdmin2.selectedBudgetType = 'All';
                objBI_MM_BudgetViewControllerForAdmin2.strSelectedTerritory = 'All';
                objBI_MM_BudgetViewControllerForAdmin2.redirectIntoExcelReport();
                objBI_MM_BudgetViewControllerForAdmin2.strCurrencySymbol ='';
                objBI_MM_BudgetViewControllerForAdmin2.totalDistributedBudget = '';
                objBI_MM_BudgetViewControllerForAdmin2.totalCurrentBudget ='';
                objBI_MM_BudgetViewControllerForAdmin2.totalPlannedAmount ='';
                objBI_MM_BudgetViewControllerForAdmin2.totalPaidAmount ='';
                objBI_MM_BudgetViewControllerForAdmin2.totalAvailableBudget ='';
                objBI_MM_BudgetViewControllerForAdmin2.save();
                objBI_MM_BudgetViewControllerForAdmin2.selectedBudgetType='Micro Marketing';
                objBI_MM_BudgetViewControllerForAdmin2.selectedBudgetActive = 'Active';
                objBI_MM_BudgetViewControllerForAdmin2.strSelectedProductId ='Product';


                //Pagination code block
                objBI_MM_BudgetViewControllerForAdmin2.goFirst();
                objBI_MM_BudgetViewControllerForAdmin2.goLast();
                objBI_MM_BudgetViewControllerForAdmin2.goPrevious();
                objBI_MM_BudgetViewControllerForAdmin2.goNext();
                objBI_MM_BudgetViewControllerForAdmin2.selectAllRecords();
                objBI_MM_BudgetViewControllerForAdmin2.redirectIntoExcelReport();
                objBI_MM_BudgetViewControllerForAdmin2.onChangePicklistTerr();
                objBI_MM_BudgetViewControllerForAdmin2.onChangePicklistPeriod();
                objBI_MM_BudgetViewControllerForAdmin2.onChangePicklistProd();
                objBI_MM_BudgetViewControllerForAdmin2.onChangePicklistType();
                objBI_MM_BudgetViewControllerForAdmin2.onChangeActive();

                // objBI_MM_BudgetViewControllerForAdmin2.savePercentage();

                // objBI_MM_BudgetViewControllerForAdmin2.negativeBudget.BI_MM_Negative_percentage__c = 0.6;
                // objBI_MM_BudgetViewControllerForAdmin2.savePercentage();
                // objBI_MM_BudgetViewControllerForAdmin2.negativeBudget.BI_MM_Region__c = testUser.Country_Code_BI__c;
                // objBI_MM_BudgetViewControllerForAdmin2.savePercentage();
                // objBI_MM_BudgetViewControllerForAdmin2.voidMethod();

            Test.stopTest();
        }
    }
}