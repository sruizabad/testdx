/* 
Name: BI_MM_BudgetViewControllerTest 
Requirement ID: Budget View Utility
Description: BI_MM_BudgetViewController
Version | Author-Email | Date | Comment 
1.0 | Venkata Madiri | 07.12.2015 | initial version 
2.0 | OmegaCRM | 25.06.2018 | Increase coverage, added SeeAllData=true to see territories
*/
@isTest(SeeAllData=true) 
private class BI_MM_BudgetViewControllerTest {

    private static testMethod void testBudgetViewController() {
      BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest(); 

        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_SALES_MANAGER' limit 1];  // Get 'BI_MM_SALES_MANAGER'permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and  null Manager
        // migart - Changing Permision to Manager
        User testUserCreated = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, null);
        	
        //We are gonna user current user to run the test so it does have a territory
        User testUser = [SElect Id from User where Id =: UserInfo.getUserId()];
        
        Try //migart
        {
        dataFactory.addPermissionSet(testUser, permissionAdmin);
        } catch(Exception e){}

        // Insert new Territories
        Territory parentTerritory = dataFactory.generateTerritory('Test Territory 1', null);
        Territory childTerritory = dataFactory.generateTerritory('Test Territory 2', parentTerritory.Id);

        
        System.runAs(testUser) {
            //BI_MM_CountryCode__c countryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c);
             //BI_MM_CurrencyCode__c CurrCd = new BI_MM_CurrencyCode__c(name='EUR',currencyIsoCode='EUR');
             //insert CurrCd;
            
            List<BI_MM_Budget__c> lstBudgetToInsert = new List<BI_MM_Budget__c>();
            List<Product_vod__c> lstProductToInsert = new List<Product_vod__c>();

            
               Id userTerritoryId;
            // Geting Standard territory Id as per logged in Users
            for(UserTerritory objUserTerritory : [SELECT UserId, TerritoryId FROM UserTerritory WHERE UserId =: UserInfo.getUserId() LIMIT 1]){
            	userTerritoryId = objUserTerritory.TerritoryId;
            	System.debug('**** userTerritory = '+objUserTerritory);
            }
                
            System.debug('*** BI_MM_BudgetViewControllerTest - initializeData - userTerritoryId == ' + userTerritoryId);
            //BI_MM_MirrorTerritory__c mt = dataFactory.generateMirrorTerritory ('Test Territory 2', userTerritoryId);
            BI_MM_MirrorTerritory__c mt = [SELECT Id from BI_MM_MirrorTerritory__c where BI_MM_TerritoryId__c =: userTerritoryId LIMIT 1];
            BI_MM_MirrorTerritory__c mt1 = dataFactory.generateMirrorTerritory ('Test Territory 1', parentTerritory.Id);           
            
            Test.startTest();
            
              // Insert Product2 records 
                  
                  //Create your product
              Product_vod__c objProduct = dataFactory.generateProduct('Test Product', 'Detail', 'ES');                
              
              // Insert BI_MM_Budget__c records                  
              lstBudgetToInsert = dataFactory.generateBudgets(1, mt.Id, mt1.Id, 'Micro Marketing', objProduct, 4000, true);              
              
              // Instantiate BI_MM_BudgetViewController class
              BI_MM_BudgetViewController objBI_MM_BudgetViewController = new BI_MM_BudgetViewController();
              objBI_MM_BudgetViewController.selectedBudgetType = 'All';  
              
              string abc = mt.Id;
              Date today = System.today();            
              String period = String.valueOf(today.year()) + '/' + String.valueOf(today.month()) + '/' + String.valueOf(today.day());
              objBI_MM_BudgetViewController.strSelectedSubTerritory = abc;// migart - Changed from strSelectedTerritory to strSelectedSubTerritory
              objBI_MM_BudgetViewController.selectedBudgetType = 'Micro Marketing';
              objBI_MM_BudgetViewController.strSelectedProductId = 'Micro Marketing';
              objBI_MM_BudgetViewController.strSelectedPeriod = period + '-' + period;
              objBI_MM_BudgetViewController.getRefreshedMyBudgetType(objBI_MM_BudgetViewController.selectedBudgetType
                                                                     ,objBI_MM_BudgetViewController.strSelectedProductId
                                                                     ,objBI_MM_BudgetViewController.strSelectedSubTerritory
                                                                     ,objBI_MM_BudgetViewController.strSelectedPeriod);
              objBI_MM_BudgetViewController.getRefreshedTeamBudgetType(objBI_MM_BudgetViewController.selectedBudgetType
                                                                   ,objBI_MM_BudgetViewController.strSelectedProductId
                                                                   ,objBI_MM_BudgetViewController.strSelectedSubTerritory
                                                                   ,objBI_MM_BudgetViewController.strSelectedPeriod);
              objBI_MM_BudgetViewController.populatePicklists();
              /*objBI_MM_BudgetViewController.readTerritoryForSalesRep();
              objBI_MM_BudgetViewController.productsForSalesRep();
              objBI_MM_BudgetViewController.budgetsForSalesRep();*/
              delete lstBudgetToInsert;
              
              // Insert Single BI_MM_Budget__c record
              BI_MM_Budget__c budget = dataFactory.generateBudgets(1, mt.Id, null, 'Micro Marketing', objProduct, 4000, true).get(0); 
              BI_MM_Budget__c budget2 = dataFactory.generateBudgets(1, mt.Id, null, 'Micro Marketing2', objProduct, 4000, true).get(0); 

              // Instantiate another BI_MM_BudgetViewController class
              BI_MM_BudgetViewController objBI_MM_BudgetViewController2 = new BI_MM_BudgetViewController();
              objBI_MM_BudgetViewController2.selectedBudgetType = 'All';                                
              objBI_MM_BudgetViewController2.strSelectedSubTerritory = 'Test';                       
              objBI_MM_BudgetViewController2.populatePicklists();
              /*objBI_MM_BudgetViewController.initializeDataForsalesRep();
              objBI_MM_BudgetViewController.readTerritoryForSalesRep();
              objBI_MM_BudgetViewController.productsForSalesRep();
              objBI_MM_BudgetViewController.budgetsForSalesRep();*/
              
              objBI_MM_BudgetViewController2.selectedBudgetType = 'All';                                
              objBI_MM_BudgetViewController2.strSelectedSubTerritory = 'All';
              objBI_MM_BudgetViewController2.populatePicklists();
              /*objBI_MM_BudgetViewController.initializeDataForsalesRep();
              objBI_MM_BudgetViewController.readTerritoryForSalesRep();
              objBI_MM_BudgetViewController.productsForSalesRep();
              objBI_MM_BudgetViewController.budgetsForSalesRep();
              objBI_MM_BudgetViewController.redirectIntoStandardReport();
              objBI_MM_BudgetViewController.redirectIntoReport();*/
              objBI_MM_BudgetViewController.redirectIntoExcelReport();
              objBI_MM_BudgetViewController.totalDistributedBudget = '';                                    
              objBI_MM_BudgetViewController.totalCurrentBudget ='';                                      
              objBI_MM_BudgetViewController.totalPlannedAmount ='';                                      
              objBI_MM_BudgetViewController.totalPaidAmount   ='';                                       
              objBI_MM_BudgetViewController.totalAvailableBudget  ='';                                   
              
              objBI_MM_BudgetViewController.totalDistributedBudgetTeam   ='';                            
              objBI_MM_BudgetViewController.totalCurrentBudgetTeam     ='';                              
              objBI_MM_BudgetViewController.totalPlannedAmountTeam  ='';                                 
              objBI_MM_BudgetViewController.totalPaidAmountTeam     ='';                                 
              objBI_MM_BudgetViewController.totalAvailableBudgetTeam  ='';
              
              BI_MM_BudgetAudit__c budgetAudit = dataFactory.generateBudgetAudit(budget, budget2, testUserCreated, testUser, 'Transfer');
              
              try{
              	
              	PageReference pgRef = objBI_MM_BudgetViewController.selectBudgetAudit();
              } catch(Exception e){}
              
              try{
              List<BI_MM_BudgetAudit__c> lstBA = objBI_MM_BudgetViewController.getBudgetAuditTable(budget);
              } catch(Exception e){}
              
              objBI_MM_BudgetViewController.getTranslatedIndicator('Subtract');//migart
              
            Test.stopTest();
        }
        
    }
}