/*
Name: BI_MM_BudgetViewExcelReportController
Requirement ID: Colloboration
Description: Class to show excel report
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari | 17.02.2016 | initial version
*/
public without sharing class BI_MM_BudgetViewExcelReportController {

    public List<BI_MM_Budget__c> lstMyBudgetToDisplay           {   get; set;   }
    public List<BI_MM_Budget__c> lstTeamBudgetToDisplay         {   get; set;   }
    public List<BI_MM_Budget__c> lstMasterBudgetToDisplay       {   get; set;   }
    public List<BI_MM_BudgetAudit__c> lstBudgetAuditToDisplay   {   get; set;   }
    public BI_MM_Budget__c objBudgetAudit                       {   get; set;   }
    private String strTerritory;
    private String strSubTerritory;
    private String strBudgetType;
    private String strProduct;
    private String strPeriod;
    private String strIsActive;
    private String strAuditBud;
    public Boolean isAdmin                                  {   get; private set;   }
    public Boolean showProjectCode                                  {   get;set;    }
    public Boolean showPercentage                                   {   get;set;    }

    public final string PERMISSION_SET = 'BI_MM_ADMIN';
    // Controllers from requesting pages
    private BI_MM_BudgetViewControllerForAdmin viewAdminController;
    private BI_MM_BudgetViewController viewController;

    //Constructor
    public BI_MM_BudgetViewExcelReportController() {
        BudgetCSV();
    }

    /**
     * @Description Initialises and prepares Data for Excel Report using controllers from requesting page
     * @param none
     * @return none
     * @Author OmegaCRM
     */
    private void BudgetCSV() {
        lstMyBudgetToDisplay = new List<BI_MM_Budget__c>();
        lstTeamBudgetToDisplay = new List<BI_MM_Budget__c>();
        lstMasterBudgetToDisplay = new List<BI_MM_Budget__c>();
        lstBudgetAuditToDisplay = new List<BI_MM_BudgetAudit__c>();

        // Check if the current user has BI_MM_ADMIN Permission Set Assigned
        isAdmin = userIsAdmin();
        viewAdminController = new BI_MM_BudgetViewControllerForAdmin();
        viewController = new BI_MM_BudgetViewController();
        showProjectCode = viewAdminController.userIsInRegion();

        // Get Record ids of My & Team Budget Records
        system.debug('*** BI_MM_BudgetViewExcelReportController - BudgetCSV - currentpageUrl == ' + Apexpages.currentpage().getUrl());
        strTerritory = Apexpages.currentpage().getparameters().get('Terr');
        strSubTerritory = Apexpages.currentpage().getparameters().get('SubT');
        strBudgetType = Apexpages.currentpage().getparameters().get('Type');
        strProduct = Apexpages.currentpage().getparameters().get('Prod');
        strPeriod = Apexpages.currentpage().getparameters().get('Peri');
        strIsActive = Apexpages.currentpage().getparameters().get('IsAc');
        strAuditBud = Apexpages.currentpage().getparameters().get('AudB');

        // If strAuditBud is populated, request is for Budget Audit report
        If (strAuditBud != null && strAuditBud != ''){
            viewController.budgetAuditId = strAuditBud;
            objBudgetAudit = viewController.getBudgetAudit(strAuditBud);
            viewController.objBudgetAudit = viewController.getBudgetAudit(strAuditBud);
            lstBudgetAuditToDisplay = viewController.getBudgetAuditTable(viewController.objBudgetAudit);
        }
        // If strSubTerritory is populated, request is from BudgetViewController
        else If (strSubTerritory != null && strSubTerritory != ''){
            lstTeamBudgetToDisplay = viewController.getRefreshedTeamBudgetType(strSubTerritory, strBudgetType, strProduct, strPeriod);
            lstMyBudgetToDisplay = viewController.getRefreshedMyBudgetType(strTerritory, strBudgetType, strProduct, strPeriod);
        }
        // otherwise, request is from BudgetViewControllerForAdmin
        else{
            lstMyBudgetToDisplay = viewAdminController.getRefreshedBudgetType(strTerritory, strBudgetType, strProduct, strPeriod, false);
            lstMasterBudgetToDisplay = viewAdminController.getMasterBudgets(lstMyBudgetToDisplay);
            showPercentage = (!lstMasterBudgetToDisplay.isEmpty() ? true : false);
        }
        system.debug('*** BI_MM_BudgetViewExcelReportController - BudgetCSV - lstMyBudgetToDisplay == '+lstMyBudgetToDisplay);
    }

    /**
     * @Description returns whether the user has the Permission Set Admin for MyBudget
     * @param none
     * @return Boolean: True if user has the Permission Set Admin for MyBudget assigned. False otherwise.
     * @Author OmegaCRM
     */
    private Boolean userIsAdmin() {
        // Get 'BI_MM_ADMIN' Assignments of the user
        List<PermissionSetAssignment> permSetAssign = new list<PermissionSetAssignment>(
            [SELECT Id
               FROM PermissionSetAssignment
              WHERE PermissionSet.Name = :PERMISSION_SET
                AND Assignee.Id =: UserInfo.getUserId()]);

        System.debug('*** BI_MM_BudgetViewExcelReportController - userIsAdmin - permSetAssign == '+permSetAssign);
        return (!permSetAssign.isEmpty() ? true : false);
    }

}