/*
Name: BI_MM_BudgetViewExcelReportCtrTest
Requirement ID: Budget Upload
Description: Class to get excel report for budget
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari | 17.02.2016 | initial version
*/
@isTest
private Class BI_MM_BudgetViewExcelReportEventTest {

    private static testMethod void testBudgetViewController() {
        BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();

        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];  // Get BI_MM_ADMIN permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and  null Manager
        User testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, null);
        dataFactory.addPermissionSet(testUser, permissionAdmin);

        // Insert new Territories
        Territory parentTerr = dataFactory.generateTerritory('Test Territory 1', null);
        Territory childTerritory = dataFactory.generateTerritory('Test Territory 2', parentTerr.Id);

        System.runAs(testUser) {
            List<BI_MM_Budget__c> lstBudgetToInsert = new List<BI_MM_Budget__c>();
            List<Product_vod__c> lstProductToInsert = new List<Product_vod__c>();

            BI_MM_MirrorTerritory__c mt = dataFactory.generateMirrorTerritory ('sr01Test', childTerritory.Id);
            BI_MM_MirrorTerritory__c mt1 = dataFactory.generateMirrorTerritory ('nm01', parentTerr.Id);

            BI_MM_CountryCode__c countryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c);
            countryCode.BI_MM_Region__c = testUser.Country_Code_BI__c;

            Test.startTest();

                // Insert Product2 records
                for(Integer intNumber = 0; intNumber < 10; intNumber++) {
                    //Create your product
                    Product_vod__c objProduct = new Product_vod__c(Name = 'Product'+intNumber);
                    lstProductToInsert.add(objProduct);
                }
                if(!lstProductToInsert.isEmpty()) insert lstProductToInsert;

                // Insert BI_MM_Budget__c records
                lstBudgetToInsert = dataFactory.generateBudgets(10, mt.Id, mt1.Id, 'Micro Marketing', lstProductToInsert[0], 4000, true);

                // Call for report from BI_MM_BudgetViewController
                PageReference pageRef = Page.BI_MM_BudgetViewExcelReport;
                Test.setCurrentPage(pageRef);
                pageRef.getParameters().put('Terr', mt1.Name);
                pageRef.getParameters().put('SubT', mt.Name);
                pageRef.getParameters().put('Type', lstBudgetToInsert.get(0).BI_MM_BudgetType__c);
                pageRef.getParameters().put('Prod', lstProductToInsert[0].Name);
                pageRef.getParameters().put('Peri', lstBudgetToInsert.get(0).BI_MM_TimePeriod__c);
                pageRef.getParameters().put('IsAc', 'true');
                BI_MM_BudgetViewExcelReportEvents_Ctrl objExcelReport = new BI_MM_BudgetViewExcelReportEvents_Ctrl();

                // Call for report from BI_MM_BudgetViewControllerForAdmin
                pageRef = Page.BI_MM_BudgetViewExcelReport;
                Test.setCurrentPage(pageRef);
                pageRef.getParameters().put('Terr', mt1.Name);
                pageRef.getParameters().put('Type', lstBudgetToInsert.get(0).BI_MM_BudgetType__c);
                pageRef.getParameters().put('Prod', lstProductToInsert[0].Name);
                pageRef.getParameters().put('Peri', lstBudgetToInsert.get(0).BI_MM_TimePeriod__c);
                pageRef.getParameters().put('IsAc', 'true');
                objExcelReport = new BI_MM_BudgetViewExcelReportEvents_Ctrl();

            Test.stopTest();
        }
    }
}