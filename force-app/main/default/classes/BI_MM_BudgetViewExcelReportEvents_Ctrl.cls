public class BI_MM_BudgetViewExcelReportEvents_Ctrl {
    
    private String strAuditBud;
    private String strTerritory;
    private String strSubTerritory;
    private String strBudgetType;
    private String strProduct;
    private String strPeriod;
    private String strIsActive;
    public Boolean isAdmin                                  {   get; private set;   }
    public List<Medical_Event_vod__c> listMedicalEvents		{	get; set;	}
    public final string PERMISSION_SET = 'BI_MM_ADMIN';
    
    //Constructor
    public BI_MM_BudgetViewExcelReportEvents_Ctrl() {
        BudgetEventsCSV();
    }
    
    private void BudgetEventsCSV(){
        
        // Check if the current user has BI_MM_ADMIN Permission Set Assigned
        isAdmin = userIsAdmin();
        
        // Get Record ids of My & Team Budget Records
        system.debug('*** BI_MM_BudgetViewExcelReportController - BudgetCSV - currentpageUrl == ' + Apexpages.currentpage().getUrl());
        strTerritory = Apexpages.currentpage().getparameters().get('Terr');
        strSubTerritory = Apexpages.currentpage().getparameters().get('SubT');
        strBudgetType = Apexpages.currentpage().getparameters().get('Type');
        strProduct = Apexpages.currentpage().getparameters().get('Prod');
        strPeriod = Apexpages.currentpage().getparameters().get('Peri');
        strIsActive = Apexpages.currentpage().getparameters().get('IsAc');
        strAuditBud = Apexpages.currentpage().getparameters().get('AudB');
        
        listMedicalEvents = getEventList(strAuditBud);
    }
    
    public List<Medical_Event_vod__c> getEventList(String budAuditId){
        System.debug('***QUERY ' + budAuditId);
        List<BI_MM_BudgetEvent__c> listBudgetEvent = [SELECT Id, Name, BI_MM_EventID__c FROM BI_MM_BudgetEvent__c WHERE BI_MM_BudgetID__c =: budAuditId];
        List<String> listEventToReturnIds = new List<String>();
        for(BI_MM_BudgetEvent__c bugEv : listBudgetEvent){
            listEventToReturnIds.add(bugEv.BI_MM_EventID__c);
        }
        return [SELECT Id, Name,Start_Date_vod__c, End_Date_vod__c, Event_Status_BI__c, Expense_Amount_vod__c, Product_BI__r.Name, Event_ID_BI__c FROM Medical_Event_vod__c WHERE Id IN: listEventToReturnIds];
    }
    
    /**
     * @Description returns whether the user has the Permission Set Admin for MyBudget
     * @param none
     * @return Boolean: True if user has the Permission Set Admin for MyBudget assigned. False otherwise.
     * @Author OmegaCRM
     */
    private Boolean userIsAdmin() {
        // Get 'BI_MM_ADMIN' Assignments of the user
        List<PermissionSetAssignment> permSetAssign = new list<PermissionSetAssignment>(
            [SELECT Id
               FROM PermissionSetAssignment
              WHERE PermissionSet.Name = :PERMISSION_SET
                AND Assignee.Id =: UserInfo.getUserId()]);

        System.debug('*** BI_MM_BudgetViewExcelReportController - userIsAdmin - permSetAssign == '+permSetAssign);
        return (!permSetAssign.isEmpty() ? true : false);
    }

}