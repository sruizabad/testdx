/**
 * @Description Class created to generate the data needed by tests
 */
@isTest
public class BI_MM_DataFactoryTest {

    /**
     * @Description Method to create an user with Admin privileges
     * @return  User inserted
     * @Author OmegaCRM
     */
    public User generateUser(String alias, String email, String username, String lastname, String countryCode, Id idProfile, Id idManager) {
        // Insert new User
        User usr = new User(Alias = alias,
                      Email = email,
                      Username = username,
                      Lastname = lastname,
                      Country_Code_BI__c = countryCode,
                      ProfileId = idProfile,
                      ManagerId = idManager,
                      emailencodingkey='UTF-8',
                      languagelocalekey='en_US',
                      localesidkey='en_US',
                      country='Spain',
                      timezonesidkey='Europe/Paris',
                      isActive = true
                      );
        insert usr;

        return usr;
    }

    public void addPermissionSet(User user, PermissionSet permSet) {
        PermissionSetAssignment userPermission = new PermissionSetAssignment(AssigneeId = user.Id, PermissionSetId = permSet.Id);
        insert userPermission;
    }

    /**
     * Method that generates a new Territory
     * @param  name                 Name of the territory
     * @param  idParentTerritory    Parent Id of the territory
     * @return  The new Territory
     */
    public Territory generateTerritory(String name, Id idParentTerritory) {
        Territory territory = new Territory(Name = 'Test Child Territory 1', ParentTerritoryId = idParentTerritory);
         insert territory;

         return territory;
    }

    /**
     * @Description Insert a Mirror Territory
     * @return Mirror Territory inserted
     * @author OmegaCRM
     */
    public BI_MM_MirrorTerritory__c generateMirrorTerritory (String name, Id idTerritory) {
        BI_MM_MirrorTerritory__c objMirrorTerritory = new BI_MM_MirrorTerritory__c();

        objMirrorTerritory.name = name;
        objMirrorTerritory.CurrencyIsoCode = 'EUR';
        objMirrorTerritory.BI_MM_TerritoryId__c = idTerritory;
        insert objMirrorTerritory;

        return objMirrorTerritory;
    }

    /**
     * @Description Generates a custtom setting for Country Code
     * @return Country Code inserted
     * @author OmegaCRM
     */
    public BI_MM_CountryCode__c generateCountryCode(String name){
        BI_MM_CountryCode__c countryCode = new BI_MM_CountryCode__c(Name = name);
        insert countryCode;

        return countryCode;
    }

    /**
     * @Description Generates a custtom setting for Country Code with available Record Types and Expense Exception
     * @return Country Code inserted
     * @author OmegaCRM
     */
    public BI_MM_CountryCode__c generateCountryCode(String name, String recordTypes){
        BI_MM_CountryCode__c countryCode = new BI_MM_CountryCode__c(Name = name , BI_MM_RecordTypeList__c = recordTypes);
        insert countryCode;

        return countryCode;
    }

    /**
     * Method that creates a custtom setting for Event Program
     * @param  programId [description]
     * @param  name      [description]
     * @return           [description]
     */
    public BI_MM_CollProgramId__c generateCsProgram(Id programId, String name) {
        BI_MM_CollProgramId__c csProgram = new BI_MM_CollProgramId__c(Name = name , Coll_Program_Id__c = programId);
        insert csProgram;
        System.debug('*** BI_MM_DataFactoryTest - csProgram : ' + csProgram);
        return csProgram;
        
    }

    public void generateCsApproval(String countryCode){
        ProcessDefinition approval = [SELECT Id, DeveloperName FROM ProcessDefinition WHERE State = 'Active' AND TableEnumOrId = 'Medical_Event_vod__c'  Limit 1];

        BI_MM_Approval_process__c csApproval = new BI_MM_Approval_process__c(Name = countryCode,BI_MM_Process_name__c = approval.DeveloperName);
    
        insert csApproval;
        
        System.debug('*** BI_MM_DataFactoryTest - csApproval : ' + csApproval);
        
    }

    /**
     * @Description Generates a Product
     * @return Product inserted
     * @author OmegaCRM
     */
    public Product_vod__c generateProduct(String name, String type, String countryCode){
        Product_vod__c objProduct = new Product_vod__c(Name = name, Product_Type_vod__c = type, Country_Code_BI__c = countryCode);
        insert objProduct;

        return objProduct;
    }

    /**
     * @Description Generates a program that does not require approval and its custom settings
     * @return Program inserted
     * @author OmegaCRM
     */
    public Event_Program_BI__c generateEventProgram(String name, String countryCode, String requireApprovalFrom,String externalId){
        Event_Program_BI__c program = new Event_Program_BI__c(Name = name, Country_Code_BI__c = countryCode,Start_Date_BI__c=Date.today(), End_Date_BI__c = Date.today(),
                                            Requires_Approval_From_BI__c = requireApprovalFrom, External_ID_BI__c = externalId);
        insert program;

        return program;
    }

    /**
     * @Description Generates a Medical Event
     * @return Medical Event inserted
     * @author OmegaCRM
     */
    public Medical_Event_vod__c generateMedicalEvent(Id recordTypeId, String eventName, Product_vod__c product, Event_Program_BI__c program, String eventStatus){

        Medical_Event_vod__c objMedicalEvent = new Medical_Event_vod__c(RecordTypeId = recordTypeId, Name = eventName, Start_Date_Time_BI__c = System.now(), End_Date_Time_BI__c = System.now(),
                                                        Program_BI__c = program.Id, Product_BI__c = product.Id, Event_Status_BI__c = eventStatus,
                                                        Business_BI__c='AH', Event_Type_BI__c = 'Sponsorship to International Conference', Event_Subtype_BI__c = 'Grant');
        insert objMedicalEvent;

        return objMedicalEvent;
    }

    /**
     * @Description Generates a List of Medical Events
     * @param  Number of Events to generate
     * @return List of Medical Events inserted
     * @author OmegaCRM
     */
    public List<Medical_Event_vod__c> generateBulkMedicalEvents(Integer numEvents, Id recordTypeId, String eventName, Product_vod__c product, Event_Program_BI__c program, String eventStatus){
        List<Medical_Event_vod__c> lstMedicalEvents = new List<Medical_Event_vod__c>();

        for(Integer i = 1; i <= numEvents; i++){
            Medical_Event_vod__c objMedicalEvent = new Medical_Event_vod__c(
                RecordTypeId = recordTypeId, Name = eventName + ' - ' + i, Start_Date_Time_BI__c = System.now(), End_Date_Time_BI__c = System.now(),
                Program_BI__c = program.Id, Product_BI__c = product.Id, Event_Status_BI__c = eventStatus,
                Business_BI__c='AH', Event_Type_BI__c = 'Sponsorship to International Conference', Event_Subtype_BI__c = 'Grant');
            lstMedicalEvents.add(objMedicalEvent);
        }

        if(!lstMedicalEvents.isEmpty()){
            insert lstMedicalEvents;
        }

        return lstMedicalEvents;
    }

    /**
     * @Description Generates an Event Team Member and an Event Expense for every Medical Event given
     * @param  Number of Events to generate
     * @return List of Event Expenses generated
     * @author OmegaCRM
     */
    public List<Event_Expenses_BI__c> generateBulkEventExpenses(List<Medical_Event_vod__c> lstMedicalEvents, User teamMember, Integer expenseAmount){
        List<Event_Team_Member_BI__c> lstEventTeamMembers = new List<Event_Team_Member_BI__c>();
        List<Event_Expenses_BI__c > lstEventExpenses = new List<Event_Expenses_BI__c >();

        for(Medical_Event_vod__c objMedicalEvent : lstMedicalEvents){
            Event_Team_Member_BI__c objEventTeamMember = new Event_Team_Member_BI__c(Event_Management_BI__c = objMedicalEvent.Id, User_BI__c = teamMember.Id);
            lstEventTeamMembers.add(objEventTeamMember);
        }
        if(!lstEventTeamMembers.isEmpty()){
            insert lstEventTeamMembers;
        }

        for(Event_Team_Member_BI__c objEventTeamMember : lstEventTeamMembers){
            Event_Expenses_BI__c objExpense = new Event_Expenses_BI__c(Event_Team_Member_BI__c = objEventTeamMember.Id, Amount_BI__c = expenseAmount);
            lstEventExpenses.add(objExpense);
        }
        if(!lstEventExpenses.isEmpty()){
            insert lstEventExpenses;
        }

        return lstEventExpenses;
    }

    /**
     * @Description Generates an specified number of Budgets
     * @param  Number of Budgets to generate
     * @return List of inserted Budgets
     * @author OmegaCRM
     */
    public List<BI_MM_Budget__c> generateBudgets(Integer numBudgets, Id userTerritory, Id managerTerritory, String budgetType, Product_vod__c product, Double currentBudget, Boolean isActive){
        List<BI_MM_Budget__c> lstBudgets = new List<BI_MM_Budget__c>();

        for(Integer i = 1; i <= numBudgets; i++){
            BI_MM_Budget__c objBudget  = new BI_MM_Budget__c(Name = 'Test Budget '+i, BI_MM_StartDate__c = System.today(), BI_MM_EndDate__c = System.today().addMonths(i),
                                                         BI_MM_TerritoryName__c = userTerritory, BI_MM_ManagerTerritory__c = managerTerritory, BI_MM_BudgetType__c = budgetType,
                                                         BI_MM_ProductName__c = product.Id, BI_MM_CurrentBudget__c = currentBudget, BI_MM_Is_active__c = isActive);
            lstBudgets.add(objBudget);
        }

        if(!lstBudgets.isEmpty()){
            insert lstBudgets;
        }

        return lstBudgets;
    }

    /**
     * @Description Generates an specified number of Budgets With Budget Indicator = Initial
     * @param  Number of Budgets to generate
     * @return List of inserted Budgets
     * @author OmegaCRM
     */
    public List<BI_MM_Budget__c> generateInitialBudgets(Integer numBudgets, Id userTerritory, Id managerTerritory, String budgetType, Product_vod__c product, Double currentBudget, Boolean isActive){
        List<BI_MM_Budget__c> lstBudgets = new List<BI_MM_Budget__c>();

        for(Integer i = 1; i <= numBudgets; i++){
            BI_MM_Budget__c objBudget  = new BI_MM_Budget__c(Name = 'Test Budget '+i, BI_MM_StartDate__c = System.today(), BI_MM_EndDate__c = System.today().addMonths(i),
                                                         BI_MM_TerritoryName__c = userTerritory, BI_MM_ManagerTerritory__c = managerTerritory, BI_MM_BudgetType__c = budgetType,
                                                         BI_MM_ProductName__c = product.Id, BI_MM_CurrentBudget__c = currentBudget, BI_MM_Is_active__c = isActive,
                                                         BI_MM_BudgetIndicator__c = System.Label.BI_MM_Initial);
            lstBudgets.add(objBudget);
        }

        if(!lstBudgets.isEmpty()){
            insert lstBudgets;
        }

        return lstBudgets;
    }
    
//Migart 20180531 Commented as new function with field BI_MM_Budget_Indicator__c has been created below
  /*  public BI_MM_BudgetAudit__c generateBudgetAudit(BI_MM_Budget__c budgFrom, BI_MM_Budget__c budgTo, User userFrom, User userTo) {
        BI_MM_BudgetAudit__c budAud = new BI_MM_BudgetAudit__c();

        budAud.BI_MM_BudgetFrom__c = budgFrom.id;
        budAud.BI_MM_BudgetTo__c = budgTo.id;
        budAud.BI_MM_FromUserName__c = userFrom.id;
        budAud.BI_MM_ToUserName__c = userTo.id;
        insert budAud;

        return budAud;
    } */
    
    public BI_MM_BudgetAudit__c generateBudgetAudit(BI_MM_Budget__c budgFrom, BI_MM_Budget__c budgTo, User userFrom, User userTo, String indicator) {
        BI_MM_BudgetAudit__c budAud = new BI_MM_BudgetAudit__c();

        budAud.BI_MM_BudgetFrom__c = budgFrom.id;
        budAud.BI_MM_BudgetTo__c = budgTo.id;
        budAud.BI_MM_FromUserName__c = userFrom.id;
        budAud.BI_MM_ToUserName__c = userTo.id;
        budAud.BI_MM_Budget_Indicator__c = indicator;
        insert budAud;

        return budAud;
    }

    /**
     * [generateBudgetsStages description]
     * @param  numBudgets       [description]
     * @param  userTerritory    [description]
     * @param  managerTerritory [description]
     * @param  budgetType       [description]
     * @param  product          [description]
     * @param  currentBudget    [description]
     * @param  indicator        [description]
     * @return                  [description]
     */
    public List<BI_MM_BudgetStage__c> generateBudgetsStages(Integer numBudgets, Id userTerritory, Id managerTerritory, String budgetType, Product_vod__c product, Double currentBudget, String indicator){
        List<BI_MM_BudgetStage__c> lstBudgetsStages = new List<BI_MM_BudgetStage__c>();

        for(Integer i = 1; i <= numBudgets; i++){
            BI_MM_BudgetStage__c objBudget  = new BI_MM_BudgetStage__c(
                Name = 'Test Budget Stage '+i, BI_MM_StartDate__c = System.today(), BI_MM_EndDate__c = System.today().addMonths(i),
                BI_MM_UserTerritory__c = userTerritory, BI_MM_ManagerTerritory__c = managerTerritory, BI_MM_BudgetType__c = budgetType,
                BI_MM_ProductName__c = product.Id, BI_MM_CurrentBudget__c = currentBudget, BI_MM_BudgetIndicator__c = indicator,
                BI_MM_TimePeriod__c = System.today().year()+'/'+System.today().month()+'-'+System.today().addMonths(i).year()+'/'+System.today().addMonths(i).month());
            lstBudgetsStages.add(objBudget);
        }

        if(!lstBudgetsStages.isEmpty()){
            insert lstBudgetsStages;
        }

        return lstBudgetsStages;
    }

    /**
     * [generateMultipleBudgetStagesFromBudgets: given a List of Budgets, returns a List of several BudgetStages for each Budget]
     * @param  lstBudget        [description]
     * @param  numStages        [description]
     * @param  currentBudget    [description]
     * @param  indicator        [description]
     * @return                  [description]
     */
    public List<BI_MM_BudgetStage__c> generateMultipleBudgetStagesFromBudgets(List<BI_MM_Budget__c> lstBudget, Integer numStages, Double currentBudget, String indicator){
        List<BI_MM_BudgetStage__c> lstBudgetsStages = new List<BI_MM_BudgetStage__c>();

        Integer j = 1;
        for(BI_MM_Budget__c objBudget : lstBudget){
            for(Integer i = 1; i <= numStages; i++){
                BI_MM_BudgetStage__c objBudgetStage  = new BI_MM_BudgetStage__c(
                    Name = 'Test Budget Stage '+ objBudget.Name + ' - ' +i, BI_MM_StartDate__c = objBudget.BI_MM_StartDate__c, BI_MM_EndDate__c = objBudget.BI_MM_EndDate__c,
                    BI_MM_UserTerritory__c = objBudget.BI_MM_TerritoryName__c, BI_MM_ManagerTerritory__c = objBudget.BI_MM_ManagerTerritory__c,
                    BI_MM_BudgetType__c = objBudget.BI_MM_BudgetType__c,
                    BI_MM_ProductName__c = objBudget.BI_MM_ProductName__c, BI_MM_CurrentBudget__c = currentBudget, BI_MM_BudgetIndicator__c = indicator,
                    //BI_MM_TimePeriod__c = objBudget.BI_MM_TimePeriod__c);
                    BI_MM_TimePeriod__c = System.today().year()+'/'+System.today().month()+'-'+System.today().addMonths(j).year()+'/'+System.today().addMonths(j).month());
                lstBudgetsStages.add(objBudgetStage);
            }
            j++;
        }

        if(!lstBudgetsStages.isEmpty()){
            insert lstBudgetsStages;
        }

        return lstBudgetsStages;
    }
    
        /**
     * @Description Generates a custtom setting for in BI_MM_Profile_program__c
     * @return programProfile inserted
     * @author OmegaCRM Migart
     */
    public BI_MM_Profile_program__c generateProfileProgram(String name, Event_Program_BI__c program, Id objProfile){
        BI_MM_Profile_program__c programProfile = new BI_MM_Profile_program__c(Name = name, BI_MM_Program__c= program.Id, BI_MM_Profile__c=objProfile);
        insert programProfile;
        System.debug('*** BI_MM_DataFactoryTest - programProfile : ' + programProfile);
        return programProfile;
 
    }
}