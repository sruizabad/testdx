@isTest
private class BI_MM_EventManagMassEditUtility_Test {

	@isTest static void test_method_one() {
		BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();
		User testUser = new User();
		String result = null;

		Profile objProfile = [Select Id from Profile limit 1];		// Get a Profile
		PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];	// Get BI_MM_ADMIN permission set
		//System.debug(Schema.SObjectType.Medical_Event_vod__c.getRecordTypeInfosByName().get('Medical Event').getRecordTypeId());
		Id medicalEventRecordType = [select Id from RecordType where Sobjecttype='Medical_Event_vod__c' and DeveloperName = 'Medical_Event'][0].Id;

		// Create new test user with the Permission Set BI_MM_ADMIN
		testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, UserInfo.getUserId());
		dataFactory.addPermissionSet(testUser, permissionAdmin); 
				
		System.runAs(testUser) {
			
			// Get record type id for Medical event - migart
            RecordType colloborationRecordType = [select Id, Name, DeveloperName from RecordType where Sobjecttype='Medical_Event_vod__c' and DeveloperName = 'Medical_Event'][0];
			
			// Generate a custtom setting for country code of the Test user
			BI_MM_CountryCode__c countryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c,colloborationRecordType.DeveloperName);//migart

			// Generate a new Product Catalogue
			Product_vod__c product = dataFactory.generateProduct('Test product 1', 'Detail', countryCode.Name);

			// Generate Event Program
			Event_Program_BI__c eventProgram = dataFactory.generateEventProgram('Test program 1', countryCode.Name, null,'Test program 1');//migart
			// Create Custtom setting for the Event Program
			BI_MM_CollProgramId__c csProgram = dataFactory.generateCsProgram(eventProgram.Id, eventProgram.Name);		
			
			Test.startTest();

				// Create 2 Medical Event Records
				Medical_Event_vod__c medEvent1 = dataFactory.generateMedicalEvent(colloborationRecordType.Id, 'Test medical event 1', product, eventProgram, 'New / In Progress');//migart
				Medical_Event_vod__c medEvent2 = dataFactory.generateMedicalEvent(colloborationRecordType.Id, 'Test medical event 2', product, eventProgram, 'New / In Progress');//migart
				
				// Check for BudgetEvent
				List<BI_MM_BudgetEvent__c> lstBudgetEventWithout = [SELECT ID,BI_MM_EventID__c FROM BI_MM_BudgetEvent__c WHERE BI_MM_EventID__c =:medEvent1.Id limit 1];
				List<BI_MM_BudgetEvent__c> lstBudgetEventWith = [SELECT ID,BI_MM_EventID__c FROM BI_MM_BudgetEvent__c WHERE BI_MM_EventID__c =:medEvent2.Id limit 1];				
				System.assertEquals(1, lstBudgetEventWithout.size(), 'ERROR - BI_MM_EventManagMassEditUtility_Test : It has to be one BudgetEvent record inserted for the Medical Event');
				
				// call the web service
				List<String> ids = new List<String>();
				ids.add(lstBudgetEventWith[0].Id);
				ids.add(lstBudgetEventWithout[0].Id);				
				
				result = BI_MM_EventManagementMassEditUtility.cancelEvents(ids, null);								
				System.assertEquals(System.Label.BI_MM_Events_updated, result, 'ERROR : the Medical Event should be updated');

				result = BI_MM_EventManagementMassEditUtility.cancelEvents(ids, null);
				System.assertEquals(System.Label.BI_MM_Event_already_canceled, result, 'ERROR : the Medical Event should be canceled');

				result = BI_MM_EventManagementMassEditUtility.cancelEvents(new List<String>(), null);
				System.assertEquals(System.Label.BI_MM_Select_record, result, 'ERROR : No Medical Event Ids was passed to cancelEvents webservice method.');

			Test.stopTest();
		}		
	}
	
}