/**
 * @Description Class created to operate with MyBudget ListView actions
 */
global class BI_MM_EventManagementMassEditUtility{

	/**
	 * @Description Method called form ListView Custom BudgetEvent Button to updated selected records (Status = Canceled)
	 * @param      	ids   Selectd records ids
	 * @return     	Operation message
	 * 
	 * @Author OmegaCRM
	 */
    webservice static String cancelEvents(String[] ids, String reason){
    	System.debug('***reason = '+reason);
        if(!ids.isEmpty()){
	        // Check if the current user has BI_MM_ADMIN Permission Set Assigned
	        List<PermissionSetAssignment> permSetAssign = new list<PermissionSetAssignment>([SELECT Id
	                                                                                         FROM PermissionSetAssignment 
	                                                                                         WHERE PermissionSet.Name = 'BI_MM_ADMIN' 
	                                                                                            AND Assignee.Id =: UserInfo.getUserId()]);
	        if(permSetAssign.isEmpty()) {
	            return System.Label.BI_MM_Cancel_event_denied;
	        }

	        list<BI_MM_BudgetEvent__c> budgetEvents = new list<BI_MM_BudgetEvent__c>([	SELECT Id, BI_MM_EventID__c, BI_MM_Cancel_reason__c
	        																			FROM BI_MM_BudgetEvent__c
	        																			WHERE Id IN :ids]);
	        list<Id> medicalEventIds = new list<Id>();

	        for(BI_MM_BudgetEvent__c budgetEvent : budgetEvents){
	        	budgetEvent.BI_MM_Cancel_reason__c = reason;
	        	medicalEventIds.add(budgetEvent.BI_MM_EventID__c);
	        }

	        try{
	        	list<Medical_Event_vod__c> medicalEvents =  new list<Medical_Event_vod__c>([ 	SELECT Id, Name, Event_Status_BI__c, Expense_Post_Status_vod__c, Program_BI__r.Name 
	                                                                						FROM Medical_Event_vod__c 
	                                                                						WHERE Id = :medicalEventIds]);  
		        // If it not empty, it is only one, because where are searching by Id
		        if(!medicalEvents.isEmpty()){
		        	for(Medical_Event_vod__c medicalEvent : medicalEvents){

		        		// Event already Canceled
		        		if(medicalEvent.Event_Status_BI__c == 'Canceled'){
		        			if(medicalEvents.size() == 1){
		        				return System.Label.BI_MM_Single_event_already_canceled;
	        				}else{
	        					return System.Label.BI_MM_Event_already_canceled;
	        				}
		        		}

		        		// Cancel event
		        		else{
		        			medicalEvent.Event_Status_BI__c = 'Canceled';   // modify Status field
		        		}
		        	}
		            update budgetEvents;
		            update medicalEvents;
		            return System.Label.BI_MM_Events_updated;
		        }else{
		        	return System.Label.BI_MM_No_events_related;
		        }
		   	}catch(Exception e){
		   		System.debug(LoggingLevel.ERROR, '*** - BI_MM_EventManagementMassEditUtility - cancelEvents - Exception e = ' + e.getMessage());
		   		return System.Label.BI_MM_Problem_occurred;
		   	}
        }else{
        	return System.Label.BI_MM_Select_record;
        }
    }
}