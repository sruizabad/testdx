/* 
Name: BI_MM_FormatCurrencyController 
Requirement ID: Budget Upload
Description: Class to format currency 
Version | Author-Email | Date | Comment 
1.0 | Hari | 17.02.2016 | initial version 
*/
public without sharing class BI_MM_FormatCurrencyController {

    public String valueToFormat;
    public String valueToFormatISO{get;set;}
    Public String valueToDisplay {get;set;} 
    
    public void setvalueToFormat(String s){
        valueToFormat = getcurrency(s);
    }
    
    public String getvalueToFormat(){
        return valueToFormat;
    }
    
    public void setvalueToFormatISO(String s){
        valueToFormatISO = s;
    }
    
    public String getvalueToFormatISO(){
        return valueToFormatISO;
    }
    
    public String getcurrency(string i) {
        String s = ( Decimal.valueOf(i==null||i.trim()==''?'0':i).setScale(2) + 0.001 ).format();
        system.debug('Value To Display' + s);
        return s.substring(0,s.length()-1);
    }
}