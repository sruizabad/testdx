/* 
Name: BI_MM_FormatCurrencyControllerTest
Requirement ID: Budget Uploader Utility
Description: Test class for BI_MM_FormatCurrencyController
Version | Author-Email | Date | Comment 
1.0 | Hari| 11.12.2015 | initial version 
*/
@isTest
private class BI_MM_FormatCurrencyControllerTest{
     static testMethod void testFormatCurrencyControllerTest() {
         BI_MM_FormatCurrencyController controller = new BI_MM_FormatCurrencyController();
         controller.setvalueToFormat('1000');
         controller.setvalueToFormatISO('INR');
         controller.getvalueToFormatISO();
         controller.getvalueToFormat();
         system.assertEquals('INR',controller.getvalueToFormatISO());
     }
}