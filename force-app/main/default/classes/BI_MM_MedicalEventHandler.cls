/*
Name: BI_MM_MedicalEventHandler
Requirement ID: Collaboration Type 1
Description: This Class is used to create event budget on creation of Medical event.
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari | 29.12.2015 | initial version
*/
public without sharing class BI_MM_MedicalEventHandler {
    private Map<Id, String> recordTypeMap;
    private String strRecordTypeName;
    private String strLoginUsersCountryCode;
    private Set<String> setUsersCountryCode = new Set<String>();
    // private Set<String> setCollProgramsId = new Set<String>();
    private Map<String, String> mapProgramExtId = new Map<String, String>();
    private Map<String, String> mapMyFieldTransForEventType = new Map<String, String>();

    private static List<BI_MM_BudgetEvent__c> lstBudgetEventsToDelete = new List<BI_MM_BudgetEvent__c>();

    private Map<Id, BI_MM_BudgetEvent__c> mapMedEventIdToBudgetEveObj = new Map<Id, BI_MM_BudgetEvent__c>();
    private List<BI_MM_BudgetEvent__c> lstAllBudgetEvent = new List<BI_MM_BudgetEvent__c>();
    private Map<String, String> mapMyFieldTranslationForStatus = new Map<String, String>();

    /* Constructor */
    public BI_MM_MedicalEventHandler() {}

    public void getParams(){
        Schema.DescribeFieldResult objDesFieldRes = Medical_Event_vod__c.Event_Status_BI__c.getDescribe();
        List<Schema.PicklistEntry> lstPLE = objDesFieldRes.getPicklistValues();

        // Iterate over the Picklist values
        for(Schema.PicklistEntry objPE : lstPLE)
            mapMyFieldTranslationForStatus.put(objPE.value, objPE.label);

        // Get current login user
        List<User> lstUser = [SELECT Id, Country_Code_BI__c FROM User WHERE Id =:UserInfo.getUserId()];

        // Get the country code for logged in Users
        for(User objUser : lstUser)
            strLoginUsersCountryCode = objUser.Country_Code_BI__c;

        // Get all country code Custom setting values into set while getting available Event Rocord Types
        List<BI_MM_CountryCode__c> lstCustomSetting = BI_MM_CountryCode__c.getall().values();
        for(BI_MM_CountryCode__c objCountryCode : lstCustomSetting){
            setUsersCountryCode.add(objCountryCode.name);
            if (objCountryCode.name == strLoginUsersCountryCode) strRecordTypeName = objCountryCode.BI_MM_RecordTypeList__c;
        }

        System.debug('*** BI_MM_MedicalEventHandler - getParams - setUsersCountryCode : ' + setUsersCountryCode);
        System.debug('*** BI_MM_MedicalEventHandler - getParams - strRecordTypeName : ' + strRecordTypeName);

        // Get all record type and map of Medical_Event_vod__c objects
        // strRecordTypeName = System.Label.BI_MM_CollaborationRecordType;
        recordTypeMap = new Map<Id, String>();
        List<RecordType> lstRecordType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Medical_Event_vod__c'];
        for(RecordType objRecordType : lstRecordType){
            // if (strRecordTypeName.contains(objRecordType.Name)) recordTypeMap.put(objRecordType.Id,objRecordType.DeveloperName);
            if (strRecordTypeName != null && strRecordTypeName.contains(objRecordType.DeveloperName)){
                recordTypeMap.put(objRecordType.Id,objRecordType.DeveloperName);
            }
        }

        System.debug('*** BI_MM_MedicalEventHandler - getParams - recordTypeMap : ' + recordTypeMap);

        // Get all program ids for Colloboration Type
        Set<String> setCollProgramsExtId = new Set<String>();
        List<BI_MM_CollProgramId__c> lstPgrmCustomSetting = BI_MM_CollProgramId__c.getall().values();
        for(BI_MM_CollProgramId__c objPgrm : lstPgrmCustomSetting) setCollProgramsExtId.add(objPgrm.Name);

        System.debug('*** BI_MM_MedicalEventHandler - getParams - setCollProgramsExtId : ' + setCollProgramsExtId);

        List<Event_Program_BI__c> lstPGRM = new List<Event_Program_BI__c>();
        lstPGRM = [Select Id, External_ID_BI__c FROM Event_Program_BI__c WHERE External_ID_BI__c IN : setCollProgramsExtId];
        for(Event_Program_BI__c objPG : lstPGRM) mapProgramExtId.put(objPG.Id, objPG.External_ID_BI__c);

        // Added for field transalation for event type
        Schema.DescribeFieldResult fieldResult = Medical_Event_vod__c.Event_Type_BI__c.getDescribe();
        List<Schema.PicklistEntry> P = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry e : P) {
            mapMyFieldTransForEventType.put(e.value, e.label);
        }
    }

    /**
    *@Description This will check that the Collaboration Program chosen is correct
    *@param newMedicalEvent
    *@return
    *
    * - Created
    */
    public void onBeforeInsert(List<Medical_Event_vod__c> lstNewMedicalEvent){
        System.debug('*** BI_MM_MedicalEventHandler - onBeforeInsert - Start');
        checkEventProgram(lstNewMedicalEvent);
    }

    // This method will be called after insert of Medical_Event_vod__c records
    public void onAfterInsert(List<Medical_Event_vod__c> lstNewMedicalEvent) {
        System.debug('*** BI_MM_MedicalEventHandler - onAfterInsert - Start');
        List<BI_MM_BudgetEvent__c> lstBudgetEventToInsert = new List<BI_MM_BudgetEvent__c>();

        //get params usercode and program
        getParams();

        // Iterate over the newly inserted Medical event
        for(Medical_Event_vod__c objMedicalEvent : lstNewMedicalEvent) {

            System.debug('*** BI_MM_MedicalEventHandler - onAfterInsert - objMedicalEvent.Event_Type_BI__c : ' + objMedicalEvent.Event_Type_BI__c);
            System.debug('*** BI_MM_MedicalEventHandler - onAfterInsert - transalted Event_Type_BI__c : ' + mapMyFieldTransForEventType.get(objMedicalEvent.Event_Type_BI__c));
            System.debug('*** BI_MM_MedicalEventHandler - onAfterInsert - objMedicalEvent.Event_Status_BI__c : ' + objMedicalEvent.Event_Status_BI__c);

            // Check record type of newly inserted records of Type Medical AND Prgram
            if(checkEventManagement(objMedicalEvent)) {
                // Insert the new BI_MM_BudgetEvent__c records and populate the values of Medical_Event_vod__c records
                BI_MM_BudgetEvent__c objBudgetEvent = new BI_MM_BudgetEvent__c(
                    Name = objMedicalEvent.Name, BI_MM_EventID__c = objMedicalEvent.Id,
                    BI_MM_EventStatus__c = objMedicalEvent.Event_Status_BI__c,
                    BI_MM_EventType__c = mapMyFieldTransForEventType.get(objMedicalEvent.Event_Type_BI__c),
                    BI_MM_ProductID__c = objMedicalEvent.Product_BI__c,
                    BI_MM_StartDataTime__c = objMedicalEvent.Start_Date_Time_BI__c,
                    BI_MM_EndDateTime__c = objMedicalEvent.End_Date_Time_BI__c,
                    BI_MM_PlannedAmount__c = objMedicalEvent.Total_Cost_BI__c,
                    BI_MM_Event_ID_BI__c = objMedicalEvent.Event_ID_BI__c,
                    CurrencyIsoCode = objMedicalEvent.CurrencyIsoCode);
                System.debug('*** BI_MM_MedicalEventHandler - onAfterInsert - objBudgetEvent : ' + objBudgetEvent);
                lstBudgetEventToInsert.add(objBudgetEvent);
            }
        }

        if(!lstBudgetEventToInsert.isEmpty()) {
            DataBase.insert(lstBudgetEventToInsert, false);
            System.debug('*** BI_MM_MedicalEventHandler - onAfterInsert - lstBudgetEventToInsert : ' + lstBudgetEventToInsert);
        }
    }

    /**
     * @description
     * @param       mapOldMedicalEvent  The map old medical event
     * @param       mapNewMedicalEvent  The map new medical event
     */
    public void onBeforeUpdate(Map<Id, Medical_Event_vod__c> mapOldMedicalEvent, Map<Id, Medical_Event_vod__c> mapNewMedicalEvent) {
        //List<Medical_Event_vod__c> lstMedicalEvent = new List<Medical_Event_vod__c>();
        getParams();
        boolean isAdminUser = checkIsAdminUser();
        for(Medical_Event_vod__c objMedEvent : mapNewMedicalEvent.values()) {

            //We also check that the program is correct
            //lstMedicalEvent = new List<Medical_Event_vod__c>();
            if(objMedEvent.Program_BI__c != mapOldMedicalEvent.get(objMedEvent.Id).Program_BI__c){
                checkEventProgram(new List<Medical_Event_vod__c>{objMedEvent});
            }

            if(checkEventManagement(objMedEvent)) {
                // A Canceled collaboration cannot be edited
                if(objMedEvent.Event_Status_BI__c == 'Canceled'){
                    if(mapOldMedicalEvent.get(objMedEvent.Id).Event_Status_BI__c == objMedEvent.Event_Status_BI__c &&
                            !isAdminUser){
                        objMedEvent.addError(System.Label.BI_MM_Edit_cancel_collaboration);
                    }else{
                        // If new Status is Canceled and ExpensePostStatus is Pending, ExpensePostStatus needs to be empty
                        if(objMedEvent.Expense_Post_Status_vod__c == System.Label.BI_MM_Pending ||
                                objMedEvent.Expense_Post_Status_vod__c == 'Pending' ||
                                objMedEvent.Expense_Post_Status_vod__c == 'Pendiente'){
                            objMedEvent.Expense_Post_Status_vod__c = '';
                        }
                    }
                }
            }
        }
    }

    // This method will be called after insert of Medical_Event_vod__c records
    public void onAfterUpdate(Map<Id, Medical_Event_vod__c> mapOldMedicalEvent, Map<Id, Medical_Event_vod__c> mapNewMedicalEvent) {

        System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - Start');
        List<Id> lstApprovalProcessToReject = new List<Id>();
        List<BI_MM_BudgetEvent__c> lstBudgetEventToUpdate = new List<BI_MM_BudgetEvent__c>();
        Map<Id, Medical_Event_vod__c> mapMedicalEventToUpdate = new Map<Id, Medical_Event_vod__c>();
        List<Approval.ProcessSubmitRequest> autoApprovals = new List<Approval.ProcessSubmitRequest>();  // List where we put the medical Events with Program Type-1 to be auto-approved
        List<Id> medicalEventsIds = new List<Id>(); // List of Ids of medical events used to delete related "Event Attendees" of type 'User'
        String programExtId;

        Map<Id, BI_MM_BudgetEvent__c> mapMedEventIdToBudgetEveObj = new Map<Id, BI_MM_BudgetEvent__c>();

        List<BI_MM_BudgetEvent__c> lstAllBudgetEvent =
            [SELECT Id, Name, BI_MM_EventID__c, BI_MM_BudgetID__c
               FROM BI_MM_BudgetEvent__c
              WHERE BI_MM_EventID__c IN :mapNewMedicalEvent.keySet()];

        // Iterate over the list of all Budget Event records
        for(BI_MM_BudgetEvent__c objBudget : lstAllBudgetEvent)
        mapMedEventIdToBudgetEveObj.put(objBudget.BI_MM_EventID__c, objBudget);

        List<BI_MM_BudgetEvent__c> lstBEToInsert = new List<BI_MM_BudgetEvent__c>();

        // Iterate over the list of all Budget Event records
        //system.debug(':: Map: ' + (mapNewMedicalEvent != null ?  mapNewMedicalEvent.size() : 0));
        getParams();
        for(Medical_Event_vod__c objMedEvent : mapNewMedicalEvent.values()) {

            if(checkEventManagement(objMedEvent)) {
                // If record does not exist
                if(!mapMedEventIdToBudgetEveObj.containsKey(objMedEvent.Id)) {
                    lstBEToInsert.add(new BI_MM_BudgetEvent__c(
                        Name = objMedEvent.Name,
                        BI_MM_EventID__c = objMedEvent.Id,
                        BI_MM_EventStatus__c = objMedEvent.Event_Status_BI__c,
                        BI_MM_EventType__c = mapMyFieldTransForEventType.get(objMedEvent.Event_Type_BI__c),
                        BI_MM_ProductID__c = objMedEvent.Product_BI__c,
                        BI_MM_StartDataTime__c = objMedEvent.Start_Date_Time_BI__c,
                        BI_MM_EndDateTime__c = objMedEvent.End_Date_Time_BI__c,
                        BI_MM_PlannedAmount__c = objMedEvent.Total_Cost_BI__c,
                        BI_MM_Event_ID_BI__c = objMedEvent.Event_ID_BI__c,
                        CurrencyIsoCode = objMedEvent.CurrencyIsoCode));
                }

                // If record exist
                if(mapMedEventIdToBudgetEveObj.containsKey(objMedEvent.Id)) {

                    if(mapOldMedicalEvent.get(objMedEvent.Id).Event_Status_BI__c != objMedEvent.Event_Status_BI__c && objMedEvent.Event_Status_BI__c == 'Canceled'){
                        lstApprovalProcessToReject.add(objMedEvent.Id);
                    }

                    System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - objMedEvent.Event_Status_BI__c : ' + objMedEvent.Event_Status_BI__c);
                    BI_MM_BudgetEvent__c budgetEvent = mapMedEventIdToBudgetEveObj.get(objMedEvent.Id);
                    String strStatus = mapMyFieldTranslationForStatus.get(objMedEvent.Event_Status_BI__c);
                    // In case we get the status value translated, this if sentence solves the issue
                    if (strStatus == NULL) strStatus = objMedEvent.Event_Status_BI__c;
                    System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - strStatus : ' + strStatus);

                    System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - objMedEvent : ' + objMedEvent);
                    System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - budgetEvent.BI_MM_BudgetID__c : ' + budgetEvent.BI_MM_BudgetID__c);

                    // Code for calling Approval Process
                    if(strStatus == System.Label.BI_MM_EventStatusNew && objMedEvent.Expense_Amount_vod__c != null ) {

                        System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - Approval Process Call');
                        //Validate there is budget assign to the event
                        if(budgetEvent.BI_MM_BudgetID__c == null) objMedEvent.Submit_Expense_vod__c.addError(System.Label.BI_MM_Must_Budget_Assigned);

                        // Get Event Program External Id
                        programExtId = mapProgramExtId.get(objMedEvent.Program_BI__c);

                        // Create an approval request for the Medical_Event_vod__c
                        Approval.ProcessSubmitRequest objProcessSubmitRequest = new Approval.ProcessSubmitRequest();
                        objProcessSubmitRequest.setComments(System.Label.BI_MM_ApprovalProcessComments);
                        objProcessSubmitRequest.setObjectId(objMedEvent.Id);
                        // objProcessSubmitRequest.setProcessDefinitionNameOrId(BI_MM_Approval_process__c.getValues(strLoginUsersCountryCode).BI_MM_Process_name__c);
                        objProcessSubmitRequest.setProcessDefinitionNameOrId(BI_MM_CollProgramId__c.getValues(programExtId).BI_MM_Approval_Process__c);
                        objProcessSubmitRequest.setSkipEntryCriteria(true);

                        autoApprovals.add(objProcessSubmitRequest);
                        // Store the medicalEvent Id to delete later all "Event Attendees" with type user related with the medical Event
                        if (BI_MM_CollProgramId__c.getValues(programExtId).BI_MM_Remove_User_Attendees__c) medicalEventsIds.add(objMedEvent.Id);

                    }

                    // End - code for auto submit approval process

                    // Check whether fields are updated or not, if updated then update into link budget event
                    else {
                        if(mapOldMedicalEvent.containsKey(objMedEvent.Id) && mapOldMedicalEvent.get(objMedEvent.Id) != objMedEvent) {
                            System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - Inside Budget Event Update');
                            System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - objMedEvent.Total_Cost_BI__c : ' + objMedEvent.Total_Cost_BI__c);
                            System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - objMedEvent.Event_Status_BI__c : ' + objMedEvent.Event_Status_BI__c);
                            System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - strStatus : ' + strStatus);
                            BI_MM_BudgetEvent__c BudgetEventUpdatedToAdd = new BI_MM_BudgetEvent__c(
                                Id = mapMedEventIdToBudgetEveObj.get(objMedEvent.Id).Id,
                                Name = objMedEvent.Name,
                                BI_MM_EventID__c = objMedEvent.Id,
                                BI_MM_ProductID__c = objMedEvent.Product_BI__c,
                                BI_MM_EventType__c = mapMyFieldTransForEventType.get(objMedEvent.Event_Type_BI__c),
                                BI_MM_EventStatus__c = objMedEvent.Event_Status_BI__c,
                                BI_MM_StartDataTime__c = objMedEvent.Start_Date_Time_BI__c,
                                BI_MM_EndDateTime__c = objMedEvent.End_Date_Time_BI__c,
                                BI_MM_PlannedAmount__c = objMedEvent.Total_Cost_BI__c,
                                CurrencyIsoCode = objMedEvent.CurrencyIsoCode);

                            // Change status of updated Medical Event records with status 'Rejected' to 'New / In Progress'
                            // Change status of associated Budget Event
                            // Dissociate Budget and Budget Event removing Budget and Budget Type values from Budget Event
                            String statusOld = mapMyFieldTranslationForStatus.get(mapOldMedicalEvent.get(objMedEvent.Id).Event_Status_BI__c);
                            // In case we get the status value translated, this if sentence solves the issue
                            if (statusOld == NULL) statusOld = mapOldMedicalEvent.get(objMedEvent.Id).Event_Status_BI__c;

                            System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - statusOld : ' + statusOld);
                            System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - System.Label.BI_MM_Rejected : ' + System.Label.BI_MM_Rejected);
                            System.debug('*** BI_MM_MedicalEventHandler - onAfterUpdate - statusNew : ' + objMedEvent.Event_Status_BI__c);

                            if(statusOld == System.Label.BI_MM_Rejected && objMedEvent.Event_Status_BI__c != 'Canceled'){
                                //System.debug(':: Old Status: ' + mapOldMedicalEvent.get(objMedEvent.Id).Event_Status_BI__c + ' vs ' + objMedEvent.Event_Status_BI__c);
                                BudgetEventUpdatedToAdd.BI_MM_EventStatus__c = System.Label.BI_MM_New_InProgress;
                                BudgetEventUpdatedToAdd.BI_MM_BudgetID__c = NULL;
                                BudgetEventUpdatedToAdd.BI_MM_BudgetType__c = NULL;
                                // Remove BI_MM_Product__c relationship to Product
                                BudgetEventUpdatedToAdd.BI_MM_ProductName__c = NULL;
                                BudgetEventUpdatedToAdd.BI_MM_Product__c = NULL;
                                BudgetEventUpdatedToAdd.BI_MM_IsBudgetLinked__c = false;

                                Medical_Event_vod__c medicalEventToUpdate = new Medical_Event_vod__c(
                                    Id=objMedEvent.Id,
                                    Event_Status_BI__c = System.Label.BI_MM_New_InProgress,
                                    Expense_Amount_vod__c = NULL);
                                mapMedicalEventToUpdate.put(medicalEventToUpdate.Id, medicalEventToUpdate);
                            }
                            lstBudgetEventToUpdate.add(BudgetEventUpdatedToAdd);
                        }
                    }
                }
            }// End main if
        } // End for loop

        autoRejectCollaborations(lstApprovalProcessToReject, mapMedicalEventToUpdate);
        autoSubmitCollaborations(autoApprovals);

        if(!lstBEToInsert.isEmpty()) {
            insert lstBEToInsert;
        }

        if(!lstBudgetEventToUpdate.isEmpty()) {
            update lstBudgetEventToUpdate;
        }

        if(!mapMedicalEventToUpdate.isEmpty()) {
            try{
                update mapMedicalEventToUpdate.values();
            }catch(Exception e){
                System.debug('mapMedicalEventToUpdate - Exception e = ' + e.getMessage());
            }
        }

        if(!medicalEventsIds.isEmpty()) {
            autoRemoveAttendees(medicalEventsIds);
        }
    }

    /**
     * @Description Method that checks if current user is Admin
     * @return      True if current user is Admin
     */
    private boolean checkIsAdminUser(){
        List<Profile> profiles = new List<Profile>([SELECT Id
                                                    FROM Profile
                                                    WHERE Id = :UserInfo.getProfileId()
                                                        AND Name LIKE '%Admin%']);
        return (!profiles.isEmpty());
    }

    /**
     * @Description    Method that submit records for approval
     * @param       medicalEventsIds  List of Events to Remove User Attendees
     */
    private void autoRemoveAttendees(List<Id> medicalEventsIds){
        System.debug('*** BI_MM_MedicalEventHandler - autoRemoveAttendees - INI');
        System.debug('*** BI_MM_MedicalEventHandler - autoRemoveAttendees - medicalEventsIds = ' + medicalEventsIds);

        // Select "Event Attendees" with Type 'User' related to "Medical Events" that should be APPROVED AUTOMATICALLY
        List<Event_Attendee_vod__c> eventsAttendees = new List<Event_Attendee_vod__c>(
            [SELECT Id
               FROM Event_Attendee_vod__c
              WHERE Medical_Event_vod__c IN :medicalEventsIds
                AND Attendee_Type_vod__c = 'User']);

        if(!eventsAttendees.isEmpty()) {
            try {
                delete eventsAttendees;     // Delete Attendees
            } catch(DmlException ex) {
                System.debug(LoggingLevel.ERROR, '*** BI_MM_MedicalEventHandler - onAfterUpdate - Error on delete Event Attendees : '+ex.getMessage());
            }
        }
    }

    /**
     * @Description    Method that submit records for approval
     * @param       autoApprovals  The automatic approvals to be submitted
     */
    private void autoSubmitCollaborations(List<Approval.ProcessSubmitRequest> autoApprovals){
        System.debug('*** - autoSubmitCollaborations - INI');
        System.debug('*** - autoSubmitCollaborations - autoApprovals = ' + autoApprovals);

        if(!autoApprovals.isEmpty()) {  // if we have Medical Event to be auto-approved
            try {
                List<Approval.ProcessResult> results = Approval.process(autoApprovals, false);   //Submit the approval requests for the Medical_Event_vod__c
            } catch(Exception ex) {
                System.debug(LoggingLevel.ERROR, '*** BI_MM_MedicalEventHandler - onAfterUpdate - Error while Submitting Medical Event : '+ex.getMessage());
            }
        }
    }

    /**
     * @Description    Method that reject Approval Processes related to a Canceled Event Management
     * @param       lstApprovalProcessToReject  The list approval process to reject
     */
    private void autoRejectCollaborations(List<Id> lstApprovalProcessToReject, Map<Id, Medical_Event_vod__c> mapMedicalEventToUpdate){
        if(!lstApprovalProcessToReject.isEmpty()){
            Set<Id> pIds = (new Map<Id, ProcessInstance>([
                SELECT Id,Status,TargetObjectId
                  FROM ProcessInstance
                 WHERE Status='Pending'
                   AND TargetObjectId IN :lstApprovalProcessToReject])).keySet();

            Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([
                SELECT Id,ProcessInstanceId
                  FROM ProcessInstanceWorkitem
                 WHERE ProcessInstanceId IN :pIds])).keySet();

            List<Approval.ProcessWorkitemRequest> allReq = new List<Approval.ProcessWorkitemRequest>();

            if(!pInstanceWorkitems.isEmpty()){
                for (Id pInstanceWorkitemsId:pInstanceWorkitems){
                    Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                    req2.setComments(System.Label.BI_MM_Canceled_collaboration);
                    req2.setAction('Reject'); //to approve use 'Approve'
                    req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                    req2.setWorkitemId(pInstanceWorkitemsId);

                    // Add the request for approval
                    allReq.add(req2);
                }
                Approval.ProcessResult[] result =  Approval.process(allReq);

                for(Id medicalEventId : lstApprovalProcessToReject){
                    if(mapMedicalEventToUpdate.get(medicalEventId) == NULL){
                        mapMedicalEventToUpdate.put(medicalEventId, new Medical_Event_vod__c(Id=medicalEventId, Event_Status_BI__c='Canceled'));
                    }else{
                        mapMedicalEventToUpdate.get(medicalEventId).Event_Status_BI__c = 'Canceled';
                    }
                }
            }
        }
    }

    /**
      * @Description Method to retrieve the Budget Events related with the Medical Event to be deleted,
      * it is needed to get them in the before delete because the lookup field will be blank
      * in the after delete trigger
      * @param oldRecord List of Medical Events to be deleted
      * @author Omega CRM
      */
    public void onBeforeDelete(List<Medical_Event_vod__c> oldRecord){

        getParams();
        boolean isAdminUser = checkIsAdminUser();

        // Get Set of MedicalEvent Ids
        Set<Id> setMedicalEventId = new Set<Id>();
        for(Medical_Event_vod__c medicalEventOld : oldRecord){
            setMedicalEventId.add(medicalEventOld.Id);
        }

        // Get Map of MedicalEventId to BudgetEvent
        Map<Id, BI_MM_BudgetEvent__c> mapIdtoBudgetEvent = new Map<Id, BI_MM_BudgetEvent__c>();
        for(BI_MM_BudgetEvent__c objBudgetEvent : [SELECT Id, BI_MM_BudgetID__c, BI_MM_EventID__c FROM BI_MM_BudgetEvent__c WHERE BI_MM_EventID__c IN : setMedicalEventId]){
            mapIdtoBudgetEvent.put(objBudgetEvent.BI_MM_EventID__c, objBudgetEvent);
        }

        // Check if MedicalEvents are for MyBudget and prepare List of Deleted BudgetEvents
        for(Medical_Event_vod__c medicalEventOld : oldRecord){
            if(checkEventManagement(medicalEventOld)){
                try{
                    if(medicalEventOld.Event_Status_BI__c == 'Canceled' && !isAdminUser){
                        medicalEventOld.addError(System.Label.BI_MM_Delete_cancel_collaboration);
                    }
                    lstBudgetEventsToDelete.add(mapIdtoBudgetEvent.get(medicalEventOld.Id));
                }
                catch(Exception e){
                    System.debug(LoggingLevel.ERROR, '*** BI_MM_MedicalEventHandler - onBeforeDelete - Error while Deleting Medical Event : '+e.getMessage());
                }
            }
        }
    }

    /**
    * @Description Method to delete the Budget Events retrieved in the Before Delete Trigger
    * @param mapOldMedicalEvent List of Medical Events deleted
    * @author Omega CRM
    */
    public void onAfterDelete(Map<Id, Medical_Event_vod__c> mapOldMedicalEvent){

        // If this is not a MyBudget MedicalEvent, lstBudgetEventsToDelete.size() cannot be > 0
        if(!lstBudgetEventsToDelete.isEmpty()){
            System.debug('*** BI_MM_MedicalEventHandler - onAfterDelete - lstBudgetEventsToDelete : ' + lstBudgetEventsToDelete);
            List<BI_MM_Budget__c> lstBudgetsToUpdate = new List<BI_MM_Budget__c>();
            for(BI_MM_BudgetEvent__c be : lstBudgetEventsToDelete){
                System.debug('*** BI_MM_MedicalEventHandler - onAfterDelete - be : ' + be);
                if(be.BI_MM_BudgetID__c != null){
                    lstBudgetsToUpdate.add(new BI_MM_Budget__c(Id = be.BI_MM_BudgetID__c));
                }
            }
            Database.delete(lstBudgetEventsToDelete);
            Database.update(lstBudgetsToUpdate);
        }
    }

    /**
    *@Description This will check that the Collaboration Program chosen is correct
    *@param newMedicalEvent
    *@return
    *@author Omega CRM
    *
    * - Created
    */
    public void checkEventProgram(List<Medical_Event_vod__c> lstMedicalEvent){
        List<BI_MM_Profile_program__c> lstProgramProfile = BI_MM_Profile_program__c.getall().values();
        Map<Id, List<Id>> mapProgramProfile = new Map<Id, List<Id>>();

        //This creates a map that relates a Profile Id with the list of programs that can be used
        for(BI_MM_Profile_program__c profileProgram : lstProgramProfile){
            if(mapProgramProfile.containsKey(profileProgram.BI_MM_Profile__c)){
                mapProgramProfile.get(profileProgram.BI_MM_Profile__c).add(profileProgram.BI_MM_Program__c);
            }else{
                List<String> lstPrograms = new List<String>();
                lstPrograms.add(profileProgram.BI_MM_Program__c);
                mapProgramProfile.put(profileProgram.BI_MM_Profile__c, lstPrograms);
            }
        }

        if(mapProgramProfile.size()>0 && mapProgramProfile.keyset().contains(userinfo.getProfileid())){
            List<Id> programIds = mapProgramProfile.get(userinfo.getProfileid());
            for(Medical_Event_vod__c medEvent:lstMedicalEvent){
                boolean contained = false;

                for(Id program : programIds){
                    if(medEvent.Program_BI__c == program){
                        contained = true;
                    }
                }
                //If the allowed strings are not contained in the program name, throw error
                if(!contained){
                    medEvent.addError(System.Label.BI_MM_Wrong_program);
                }
            }
        }
    }

    /**
    *@Description Check if the Event Management record is available for MyBudget. Include Event Record Type, User Country and available Programs
    *@param objMedicalEvent
    *@return isMyBudget
    *@author Omega CRM
    *
    * 25-Jan-2018 - Guillem Vivó - Created - CR-1298
    */
    public Boolean checkEventManagement(Medical_Event_vod__c objMedicalEvent){

        Boolean isMyBudget = false;
        if(mapProgramExtId.Keyset().contains(objMedicalEvent.Program_BI__c) &&
        setUsersCountryCode.contains(strLoginUsersCountryCode) &&
        objMedicalEvent.recordTypeId != null &&
        recordTypeMap.containsKey(objMedicalEvent.recordTypeId)){
            isMyBudget = true;
        }
        System.debug('*** BI_MM_MedicalEventHandler - checkEventManagement - isMyBudget : ' + isMyBudget);
        return isMyBudget;
    }
}