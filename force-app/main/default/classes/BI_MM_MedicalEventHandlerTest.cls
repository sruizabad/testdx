/*
Name: BI_MM_MedicalEventHandlerTest
Requirement ID: Collaboration Type 1
Description: This Test class for Medical event handler.
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari | 29.12.2015 | initial version
*/
@isTest
private class BI_MM_MedicalEventHandlerTest {
    static BI_MM_DataFactoryTest dataFactory = new BI_MM_DataFactoryTest();
    static RecordType colloborationRecordType;
    static UserTerritory objUserTerritory;
    static User currentUser;
    static User testUser;
    static BI_MM_MirrorTerritory__c objMirrorTerritory;
    static Product_vod__c objProduct;
    static Event_Program_BI__c program_WithoutApprove;
    static Event_Program_BI__c program_WithApprove;
    static BI_MM_CollProgramId__c csProgram_WithoutApprove;
    static BI_MM_CollProgramId__c csProgram_WithApprove;
    static BI_MM_Budget__c objBudget;
    static final Integer dmlLoadSize = 100;
    static BI_MM_Profile_program__c programProfile;

    static void initUser(){
        currentUser = [select Id from User where Id = :UserInfo.getUserId()];

        Profile objProfile = [Select Id from Profile limit 1];      // Get a Profile
        PermissionSet permissionAdmin = [select Id from PermissionSet where Name = 'BI_MM_ADMIN' limit 1];  // Get BI_MM_ADMIN permission set

        // Create new test user with the Permission Set BI_MM_ADMIN and a Manager as the currentUser
        testUser = dataFactory.generateUser('TestUsr1', 'TestAdmin@Equifax.test1', 'TestAdmin@Equifax.test', 'Testing1', 'ES', objProfile.Id, currentUser.Id);
        dataFactory.addPermissionSet(testUser, permissionAdmin);
    }

    static void generateTestData() {
        // Get record type
        colloborationRecordType = [SELECT Id, Name, DeveloperName, Sobjecttype FROM RecordType
            WHERE Sobjecttype='Medical_Event_vod__c' and DeveloperName = 'Medical_Event'][0];

        // Get User Territory
        objUserTerritory = [SELECT UserId, TerritoryId FROM UserTerritory WHERE TerritoryId != null limit 1];

        // Insert Mirror Territory
        objMirrorTerritory = dataFactory.generateMirrorTerritory ('sr01Test', objUserTerritory.TerritoryId);

        // Insert a custom setting for the country code of the current user
        BI_MM_CountryCode__c countryCode = dataFactory.generateCountryCode(testUser.Country_Code_BI__c, colloborationRecordType.DeveloperName);

        // Insert Product Catelog
        objProduct = dataFactory.generateProduct('Test Product', 'Detail', countryCode.Name);

        // Insert spain Budget
        objBudget = dataFactory.generateBudgets(1, objMirrorTerritory.Id, null, 'Micro Marketing', objProduct, 4000, true).get(0);

        // insert Two Events Programs : One without Approval Required, and other one With Approval required from Manager
        program_WithoutApprove = dataFactory.generateEventProgram('Test Program Without Approve', countryCode.Name, '','Test Program Without Approve');
        program_WithApprove = dataFactory.generateEventProgram('Test Program With Approve', countryCode.Name, 'Manager', 'Test Program With Approve');

        // Insert new custom setting for the Events Programs
        csProgram_WithoutApprove = dataFactory.generateCsProgram(program_WithoutApprove.Id, program_WithoutApprove.Name);
        csProgram_WithApprove = dataFactory.generateCsProgram(program_WithApprove.Id, program_WithApprove.Name);
        dataFactory.generateCsApproval(testUser.Country_Code_BI__c);

		// Insert new custom setting for BI_MM_Profile_program__c to relate profile ids to programs @Migart
		programProfile= dataFactory.generateProfileProgram('Test Program Profile With Approve', program_WithApprove, testUser.ProfileId);
		programProfile= dataFactory.generateProfileProgram('Test Program Profile Without Approve', program_WithoutApprove, testUser.ProfileId);
		
		System.debug('*** BI_MM_MedicalEventHandler Test - CustomSetting - programProfile : ' + programProfile);
    }

    /*
        * Method Name : testMedicalEvent
        * Description : Created for unit testing of medical event trigger
        * Return Type : None
    */
    private static testMethod void testMedicalEvent() {
        initUser();

        System.runAs(testUser) {
            generateTestData();

            test.startTest();
                // Insert Medical event records
                
                Medical_Event_vod__c objMedicalEvent = dataFactory.generateMedicalEvent(colloborationRecordType.Id, 'test Collaboration Name', objProduct, program_WithoutApprove,
                                                                                         'New / In Progress');
              	delete objMedicalEvent;
                
                
                // Delete Medical event recods
                

            test.stopTest();
        }
    }

    /**
     * Test Method for the case of the Auto-Approved Medical Event
     * @return void
     */
    private static testMethod void testMedicalEvent_WithoutApprove(){
        initUser();

        System.runAs(testUser) {
            generateTestData();

            test.startTest();

                // Insert Medical Event auto-approved
                Medical_Event_vod__c objMedicalEvent = dataFactory.generateMedicalEvent(colloborationRecordType.Id, 'test Event AutoApproved', objProduct, program_WithoutApprove,
                                                                                         'New / In Progress');

                List<BI_MM_BudgetEvent__c> budgetsEvents = new List<BI_MM_BudgetEvent__c>([select Id, Name from BI_MM_BudgetEvent__c
                                                                                           where BI_MM_EventID__c = :objMedicalEvent.Id limit 1]);

                System.assertEquals(1, budgetsEvents.size(), 'ERROR - testMedicalEvent_WithoutApprove : It has to be one BudgetEvent record inserted for the Medical Event');

                // insert new Event Team Member related to the Medical Event
                Event_Team_Member_BI__c eventTeamMember = new Event_Team_Member_BI__c(Event_Management_BI__c = objMedicalEvent.Id, User_BI__c = UserInfo.getUserId());
                insert eventTeamMember;

                // Query the list of Event Attendees related to the Medical Event
                List<Event_Attendee_vod__c> attendees = new List<Event_Attendee_vod__c>([select Id, Name, Attendee_Type_vod__c from Event_Attendee_vod__c
                                                                                         where Medical_Event_vod__c = :objMedicalEvent.Id]);
                System.assertEquals(1, attendees.size(), 'ERROR - testMedicalEvent_WithoutApprove : It has to be one Event Attendee for each Event Team Member');

                // Insert new Event Expense related to the Event Team Member
                Event_Expenses_BI__c eventExpense = new Event_Expenses_BI__c(Event_Team_Member_BI__c = eventTeamMember.Id, Expense_Type_BI__c ='Local Ticket',
                                                                             Expense_Date_BI__c = System.today(), Amount_BI__c = 150);
                insert eventExpense;

                // Link Budget Event with the Budget
                budgetsEvents.get(0).BI_MM_BudgetType__c = objBudget.BI_MM_BudgetType__c;
                budgetsEvents.get(0).BI_MM_BudgetID__c = objBudget.Id;
                update budgetsEvents.get(0);
				System.debug('*** BI_MM_MedicalEventHandler Test - Auto-Approved Medical Event - budgetsEvents : ' + budgetsEvents);
 				System.debug('*** BI_MM_MedicalEventHandler Test - Auto-Approved Medical Event - program_WithoutApprove : ' + program_WithoutApprove);
                // Assert if the attendees type 'User' was deleted
                // Medical_Event_vod__c approvedMedicalEvent = [select Id, Name, Event_Status_BI__c from Medical_Event_vod__c where id = :objMedicalEvent.Id];
                // attendees = new List<Event_Attendee_vod__c>([select Id, Name from Event_Attendee_vod__c where Medical_Event_vod__c=:objMedicalEvent.Id and Attendee_Type_vod__c='User']);
                // System.assertEquals(0, attendees.size(), 'ERROR - testMedicalEvent_WithoutApprove : After The Medical Event has been auto-approved, all Event Attendees with type User shlould be deleted');

            test.stopTest();
        }
    }

    /**
     * Test Method for the case of Medical Event that require to be Approved
     * @return [description]
     */
    private static testMethod void testMedicalEvent_WithApprove() {
        initUser();

        System.runAs(testUser) {
            generateTestData();

            test.startTest();

                // Insert Medical Event that require Approval
                Medical_Event_vod__c objMedicalEvent = dataFactory.generateMedicalEvent(colloborationRecordType.Id, 'test Event Require Approve', objProduct, program_WithApprove, 'New / In Progress');

                List<BI_MM_BudgetEvent__c> budgetsEvents = new List<BI_MM_BudgetEvent__c>([select Id, Name from BI_MM_BudgetEvent__c
                                                                                           where BI_MM_EventID__c = :objMedicalEvent.Id limit 1]);
                System.assertEquals(1, budgetsEvents.size(), 'ERROR: It has to be one BudgetEvent record inserted for the Medical Event');

                // insert new Event Team Member related to the Medical Event
                Event_Team_Member_BI__c eventTeamMember = new Event_Team_Member_BI__c(Event_Management_BI__c = objMedicalEvent.Id, User_BI__c = UserInfo.getUserId());
                insert eventTeamMember;

                // Query the list of Event Attendees related to the Medical Event
                List<Event_Attendee_vod__c> attendees = new List<Event_Attendee_vod__c>([select Id, Name, Attendee_Type_vod__c from Event_Attendee_vod__c
                                                                                         where Medical_Event_vod__c = :objMedicalEvent.Id]);
                System.assertEquals(1, attendees.size(), 'ERROR : It has to be one Event Attendee for each Event Team Member');

                // Insert new Event Expense related to the Event Team Member
                Event_Expenses_BI__c eventExpense = new Event_Expenses_BI__c(Event_Team_Member_BI__c = eventTeamMember.Id, Expense_Type_BI__c ='Local Ticket',
                                                                             Expense_Date_BI__c = System.today(), Amount_BI__c = 150);
                insert eventExpense;

                // Link Budget Event with the Budget
                budgetsEvents.get(0).BI_MM_BudgetType__c = objBudget.BI_MM_BudgetType__c;
                budgetsEvents.get(0).BI_MM_BudgetID__c = objBudget.Id;
                update budgetsEvents.get(0);

                Medical_Event_vod__c approveMedicalEvent = [select Id, Name, Event_Status_BI__c from Medical_Event_vod__c where id = :objMedicalEvent.Id];

                // Approve Medical Event
                /*System.runAs(currentUser) { // Run as current user which is the Manager of the user : testUser
                    Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                    req.setComments('Approving request from Test class: BI_MM_MedicalEventHandlerTest');
                    req.setAction('Approve');
                    Id workItemId = [Select Id from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =: approveMedicalEvent.Id][0].Id;
                    req.setWorkitemId(workItemId);
                    Approval.ProcessResult result =  Approval.process(req);
                }*/

                approveMedicalEvent = [select Id, Name, Event_Status_BI__c from Medical_Event_vod__c where id = :objMedicalEvent.Id];

                attendees = new List<Event_Attendee_vod__c>([select Id, Name, Attendee_Type_vod__c from Event_Attendee_vod__c
                                                                                         where Medical_Event_vod__c = :objMedicalEvent.Id]);
                System.assertEquals(1, attendees.size(), 'ERROR : Attendees type "User" should not be deleted After Approving a "Medical Event" that is not Auto-Approved.');
				System.debug('*** BI_MM_MedicalEventHandler Test - Medical Event that require Approval - budgetsEvents : ' + budgetsEvents);
            	System.debug('*** BI_MM_MedicalEventHandler Test - Medical Event that require Approval - program_WithApprove : ' + program_WithApprove);

                // Reject Event @Migart
            	/*Medical_Event_vod__c rejectMedicalEvent = objMedicalEvent;
            	rejectMedicalEvent.Event_Status_BI__c = 'Rejected';
            	update rejectMedicalEvent;
            	System.debug('*** BI_MM_MedicalEventHandler Test - reject event : ' +rejectMedicalEvent); */            	
				
                // Event New-In Progress Event @Migart
            	Medical_Event_vod__c newMedicalEvent = objMedicalEvent;
            	newMedicalEvent.Event_Status_BI__c = 'New / In Progress';
            	update newMedicalEvent;

                // Cancel Event @Migart
            	Medical_Event_vod__c cancelMedicalEvent = objMedicalEvent;
            	cancelMedicalEvent.Event_Status_BI__c = 'Canceled';
            	update cancelMedicalEvent;
            	System.debug('*** BI_MM_MedicalEventHandler Test - Cancel event : ' +cancelMedicalEvent);         	
            test.stopTest();
        }
    }

    private static testMethod void testMedicalEventDML() {
        initUser();

        System.runAs(testUser) {
            generateTestData();

            test.startTest();

                // Create and Insert bulk of Medical event records
                List<Medical_Event_vod__c> lstMedicalEvents = dataFactory.generateBulkMedicalEvents(dmlLoadSize, colloborationRecordType.Id, 'test Collaboration Name', objProduct, program_WithoutApprove, 'New / In Progress');

                // Edit and update bulk
                for(Medical_Event_vod__c objMedicalEvent : lstMedicalEvents) {
                    objMedicalEvent.Business_BI__c='All';
                    objMedicalEvent.Expense_Amount_vod__c=50;
                }
                Try {
                Update lstMedicalEvents;
                }
                catch (exception e){}

                // Delete Medical event recods
                Delete lstMedicalEvents;

            test.stopTest();
        }
    }
    
}