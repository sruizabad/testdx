/*
Name: BI_MM_RecursionHandler
Requirement ID: Collaboration Type
Description: This class for Medical event handler.
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari | 31.12.2015 | initial version
*/
public without sharing Class BI_MM_RecursionHandler {

    public static Boolean isBool = true;
    public static Boolean isInsertTrigger = true;
    public static Boolean isBeforeUpdateBudgetTrigger = true;
    public static Boolean isAfterUpdateBudgetTrigger = true;
    public static Boolean isInsertSpainTrigger = true;
    public static Boolean isDeleteTrigger = true;
    public static Boolean isUpdateBudgetEvenTrigger = true;
    public static Boolean isUpdateMedicalEventTrigger = true;
    public static Integer entries = 0;

    /* Start - Added to sort selectoption on 25th May 2016 */
    /**
        Sort field to use in SelectOption i.e. Label or Value
    */
    public enum FieldToSort {
        Label, Value
    }

    public static void doSort(List<Selectoption> opts, FieldToSort sortField) {

        System.debug('========Before sort==='+opts);
        Map<String, Selectoption> mapping = new Map<String, Selectoption>();
        // Suffix to avoid duplicate values like same labels or values are in inbound list
        Integer suffix = 1;
        for (Selectoption opt : opts) {
            if (sortField == FieldToSort.Label) {
                mapping.put(    // Done this cryptic to save scriptlines, if this loop executes 10000 times
                                // it would every script statement would add 1, so 3 would lead to 30000.
                             (opt.getLabel() + suffix++), // Key using Label + Suffix Counter
                             opt);
            } else {
                mapping.put(
                             (opt.getValue() + suffix++), // Key using Label + Suffix Counter
                             opt);
            }
        }

        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();
        // clear the original collection to rebuilt it
        opts.clear();

        for (String key : sortKeys) {
            opts.add(mapping.get(key));
        }
        System.debug('========After sort==='+opts);
    }
}