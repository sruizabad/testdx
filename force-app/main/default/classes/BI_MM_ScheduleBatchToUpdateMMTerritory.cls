/* 
Name: BI_MM_ScheduleBatchToUpdateMMTerritory 
Requirement ID: Colloboration
Description: Scheduler class to Schedule batch BI_MM_BatchToUpdateBudgetTerritory
Version | Author-Email | Date | Comment 
1.0 | Mukesh Tiwari | 02.02.2016 | initial version 
*/
global class BI_MM_ScheduleBatchToUpdateMMTerritory implements Schedulable {
    
    global void execute(SchedulableContext SC) {
      
        DataBase.executeBatch(new BI_MM_BatchToUpdateBudgetTerritory(), 200);
    }
    
    /*
    //Code to Run from Developer console to schedule batch daily at 1 PM
    String CRON_EXP = '0 0 13 * * ?';
    System.schedule('Hourly Example Batch Schedule job', CRON_EXP, new BI_MM_ScheduleBatchToUpdateMMTerritory());
    */    
}