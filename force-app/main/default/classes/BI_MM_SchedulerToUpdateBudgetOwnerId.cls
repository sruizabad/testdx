/* 
Name: BI_MM_SchedulerToUpdateBudgetOwnerId 
Requirement ID: Colloboration
Description: Schedular to Schedule batch i.e BI_MM_BatchToUpdateBudgetOwnerId
Version | Author-Email | Date | Comment 
1.0 | Mukesh Tiwari | 17.02.2016 | initial version 
*/
global class BI_MM_SchedulerToUpdateBudgetOwnerId implements Schedulable {
    
    global void execute(SchedulableContext SC) {
      
        DataBase.executeBatch(new BI_MM_BatchToUpdateBudgetOwnerId(), 200);
    }
    
    /*
    //Code to Run from Developer console to schedule batch daily at 1 PM
    String CRON_EXP = '0 0 13 * * ?';
    System.schedule('Batch Schedule job to update ownerId', CRON_EXP, new BI_MM_SchedulerToUpdateBudgetOwnerId());
    */    
}