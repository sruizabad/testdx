public class BI_MX_CouponValidation {


    public String casenumber { get; set; }
   
    public boolean displayPopup {get; set;}         
    
    public PageReference closePopup() {        
        displayPopup = false; 
        //PageReference pg = new PageReference('/apex/BI_MX_CouponValidation');
      //  PageReference pg = new PageReference('/couponmanagement');
        PageReference pg = new PageReference('/');
        pg.setRedirect(true);
        return pg;   
    }
       
    public void showPopup() {
        casecreation();  
    }


   
    public String error2 { get; set; }
    public String error1 { get; set; }
    public String error3 { get; set; }
    public String errorLicense { get; set; }
    public String errorHCP { get; set; }
    public String errorCoupon { get; set; }    

    //Properties for coupon validation page
    public String coupon{get; set;}
    public String file {get; set;}
    public String check { get; set; }
    public String firstoption { get; set; }
    public String secondoption { get; set; }
    public String License { get; set; }
    public String docfirstname { get; set; }
    public String doclastname { get; set; }
    public Boolean selectcheckbox { get; set; }
    public String SiteTerm;
    public Id AccntId;
    public Id VASId;
    public Id StateId;
    public Date dt;
   
    public String Rquiredp {get;set;}
    public String Rquiredf {get;set;}
    public String Rquiredl {get;set;}
    public String Rquireds {get;set;}
    public String Rquiredn {get;set;}
    public String Rquiredc {get;set;}
    
    //Properties for personal details page
    public String Email { get; set; }
    public String Lname { get; set; }
    public String Fname { get; set; }
    public String BirthDate { get; set; }
    public String Phone { get; set; }
    public String Phone2 { get; set; }
    public String Addr1 { get; set; }
    public String Addr2 { get; set; }
    public String City { get; set; }
    public String State { get; set; }
    public String Zip { get; set; }
    public String Reference { get; set; }
    public String Product { get; set; }
    public String TimeConsumer { get; set; }
    public Id ProdId;
    
    //Required for case creation
    public String Email1 { get; set; }
    public String Lname1 { get; set; }
    public String Fname1 { get; set; }
    public String BirthDate1 { get; set; }
    public String Phone1 { get; set; }
    public String Phone21 { get; set; }
    public String Addr11 { get; set; }
    public String Addr21 { get; set; }
    public String City1 { get; set; }
    public String State1 { get; set; }
    public String Zip1 { get; set; }
    public String Reference1 { get; set; }
    public String Product1 { get; set; }
    public String Diseases1 { get; set; }
    public String TimeConsumer1 { get; set; }
    public String Gender1 { get; set; }
    List<recordtype> record1 = new List<recordtype>();
    List<recordtype> record12 = new List<recordtype>();
    List<recordtype> record = new List<recordtype>();
    
    
    //for Gender
    public String Gender { get; set; }
   
    //To dynamically display searchboxes based on radio button selection.
    public void getselecteditem() {
        if(secondoption == 'false')
        {
            check = 'false';
            license = null;
            Rquiredl = 'no Errors';
        }
        if(firstoption == 'true')
        {
            check = 'true';
            docfirstname = null;
            doclastname = null;
            Rquireds = 'no Errors';
            Rquiredn = 'no Errors';
        }
    }
    
    public BI_MX_CouponValidation(){
         check = 'true';
         error1 = 'false';
         error2 = 'false';
         error3 = 'false';
         errorLicense = 'false';
         errorHCP = 'false';
         errorCoupon = 'false';
          
         Rquiredp = 'no Errors';
         Rquiredf = 'no Errors';
         Rquiredl = 'no Errors';
         Rquireds = 'no Errors';
         Rquiredn = 'no Errors';
         Rquiredc = 'no Errors'; 
         
         Email1 = 'no Errors';
         Lname1 = 'no Errors';
         Fname1 = 'no Errors';
         BirthDate1 = 'no Errors';
         Phone1 = 'no Errors';
         Phone21 = 'no Errors';
         Addr11 = 'no Errors';
         Addr21 = 'no Errors';
         City1 = 'no Errors';
         State1 = 'no Errors';
         Zip1 = 'no Errors';
         Reference1 = 'no Errors';
         Product1 = 'no Errors';
         Diseases1 = 'no Errors';
         TimeConsumer1 = 'no Errors';
         Gender1 = 'no Errors';
         displayPopup = false;
         
         record = [select id from recordtype where name = 'Consumer'];
         record1 = [select id from recordtype where name = 'Interaction'];
         record12 = [select id from recordtype where name = 'Request']; 
        
      }
    
    //Fetch Product Id
    public void getProduct() {
        System.debug('ProdId5555555'+Product);
        List<Product_vod__c> prod = [Select Id from Product_vod__c where External_ID_vod__c=:Product];
        ProdId = prod.get(0).id;
        System.debug('ProdId'+ProdId);
    }
    
    //Fetch State
    public void getState() {
         List<Customer_Attribute_BI__c> attributes = [select Id, Name from Customer_Attribute_BI__c where Name=:State and OK_Country_ID_BI__c = 'MX'];
        System.debug('attributes'+attributes);
        StateId = attributes.get(0).Id;
    }
    
       
    //Coupon validation
    public PageReference validatecoupon() {
        
         String couponnumber1 = coupon + file;
    
        //Expired Coupon Validation
         list<vas_bi__c>activevaslist=[SELECT Active_BI__c,Id,Name,Product_BI__c,VAS_Code_BI__c FROM VAS_BI__c where Active_BI__c=true and VAS_Code_BI__c =: coupon];
        if(activevaslist.isEmpty()){
            error1 = 'true';
            errorCoupon = 'true';           
            return null;
        }
        
        //ExceptionList Coupon Validation
        list<Exception_List__c>exceptionlist=[select Id,Coupon_Number__c,VAS_CODE__c from Exception_List__c where VAS_CODE__r.Active_BI__c=true and Coupon_Number__c =: couponnumber1];
        if(!exceptionlist.isEmpty()){
            error1 = 'true';  
            errorCoupon = 'true';            
            return null;
        }
        
        
        //Used Coupon Validation
        list<Case>couponlist=[select Id,coupon__c from Case where Coupon__c = :couponnumber1 and Type='Coupon management' and (Owner.Profile.Name='CRC-Warehouse Agent' or Owner.Profile.Name='CRC - MX - Customer Request Agent' or Owner.Name='Warehouse Queue' or Owner.Name='MX Call Center Admin' or Owner.Profile.Name= 'CRC - VAS Admin'or Owner.Profile.Name='Coupon Management Profile')];
        
        if(!couponlist.isEmpty()){
            error1 = 'true';
            errorCoupon = 'true';            
            return null ;
        }
        else
        {
            coupon = coupon.touppercase();       
           List<VAS_BI__c> vascode1 = [SELECT Id FROM VAS_BI__c WHERE Active_BI__c=true and VAS_Code_BI__c =:coupon];
            VASId = vascode1.get(0).id;
        }
        
        return null;        
    }
    
    
    //HCP Validation
    public PageReference validatehcp() {
    //Required Check
        Rquiredp = 'no Error';
        Rquiredf = 'no Error';
        Rquiredl = 'no Error';
        Rquireds = 'no Error';
        Rquiredn = 'no Error';
        Rquiredc = 'no Error';
        error1 = 'false';
        error2 = 'false';
        error3 = 'false';
        errorLicense = 'false';
         errorHCP = 'false';
         errorCoupon = 'false';
         
        if(coupon == null || coupon == '')
        {
          Rquiredp = 'coupon';
        }
        
        if(file == null || file == '')
        {
          Rquiredf = 'file';
        }
        else if(!file.isNumeric())
        {
            Rquiredf = 'Wrong Data';
        }
        
        if(check == 'false' && ( docfirstname == '' || docfirstname == null || docfirstname == 'Ingrese nombre(s) del médico'))
        {
          Rquireds = 'license';
          Rquiredl = 'no Error';
        }
        
        if(check == 'false' && ( doclastname == '' || doclastname == null || doclastname == 'Ingrese apellidos del médico'))
        {
          Rquiredn = 'license';
          Rquiredl = 'no Error';
        }
        
        if(check  == 'true' && (license  == '' || license == null || license == 'Buscar médico por cédula profesional'))
        {
        
          Rquiredl = 'license';
          Rquireds = 'no Error';
          Rquiredn = 'no Error';
        }
        
        if(selectcheckbox == false) 
       {
           Rquiredc = 'checked';
           SiteTerm = 'Unchecked';
       }
  
    
    //Check if all required fields are filled
    if(Rquiredp == 'no Error' && Rquiredf == 'no Error' &&  Rquiredl == 'no Error' &&  Rquireds == 'no Error' &&  Rquiredn == 'no Error' && Rquiredc == 'no Error')
    {
        SiteTerm = 'Checked';
        
        if(coupon != null && coupon != ''&& file != null && file != '')
        {
            validatecoupon();
        }
                  
        List<Account> Acco = new List<Account>();
        if((license != null && license != '') && check == 'true')
        {
        
           if(license.length()==5)
                {
                   license = '000' + license;
                }
            if(license.length()==6)
                {
                   license = '00' + license;
                }
            if(license.length()==7)
                {
                   license = '0' + license;
                } 
                Acco=[select id,name from account where license_bi__c=: license and status_bi__c = 'Active' and recordtype.name='Professional_vod'];
                if(Acco.isEmpty())
                {
                 error1 = 'true';
                 errorLicense = 'true'; 
                 return null;                
                }
                if(Acco.size() > 1)
               {
                   error3 = 'true';
                   return null;
               } 
              AccntId = Acco.get(0).id;            
           
        }
        
        if(check == 'false' && (docfirstname!=null && doclastname!=null && docfirstname!='' && doclastname!=''))
        {
           string name = docfirstname+' '+doclastname;
           //string name;
           //if(name1.contains('i')){
              //name = name1.replace('í','i');
            //}
           //name.Replace('í','i');
           //name.Replace('é','e');
          //Lines added 299,301,302 and commented Line No: 304. Used SOSL in place of SOQL to Ignore special characters in validate
         List<list<sObject>> Acclist =new List<list<sObject>>();
        
      Acclist = [FIND :name IN ALL FIELDS RETURNING  Account(Id,name WHERE Status_BI__c ='Active' and recordtype.name='Professional_vod')];
   
           //List<Account>Acclist=[select id,name from account where name =:name and status_bi__c = 'Active' and recordtype.name='Professional_vod'];
           if(Acclist.isEmpty())
           {
                error1 = 'true';
                errorHCP = 'true';
               return null;
           }
           if(Acclist.size() > 1)
           {
               error2 = 'true';               
               return null;
           }     
           List<Account> accIds= [Select Id from Account where Id =: Acclist.get(0)];
           //AccntId = Acclist.get(0).Id;
           AccntId=  accIds[0].Id;
         }
        if(!selectcheckbox)
        {
            Rquiredc = 'checked';
            return null;
        }
          
          if(error1 == 'false' && error2 == 'false')
          {
               PageReference pg = new PageReference('/apex/BI_MX_CaseCreation');
               pg.getParameters().put('accntid',AccntId);
               String couponnumber = coupon + file;
               pg.getParameters().put('couponnumber',couponnumber );
               pg.getParameters().put('vid',VASId);
               pg.getParameters().put('SiteAcceptTerms',SiteTerm);
               return pg;
           }
           else
           {
               return null;
           } 
     }
      else
      {
          return null;
      }
    }
    
    public void casecreation() {
    //Required field check
         Email1 = 'no Errors';
         Lname1 = 'no Errors';
         Fname1 = 'no Errors';
         BirthDate1 = 'no Errors';
         Phone1 = 'no Errors';
         Phone21 = 'no Errors';
         Addr11 = 'no Errors';
         Addr21 = 'no Errors';
         City1 = 'no Errors';
         State1 = 'no Errors';
         Zip1 = 'no Errors';
         Reference1 = 'no Errors';
         Product1 = 'no Errors';
         TimeConsumer1 = 'no Errors';
         Gender1 = 'no Errors';
         displayPopup = false; 
         
         if(Fname == null || Fname == '')
         {
             Fname1 = 'Required';
         }  
         if(Lname == null || Lname == '')
         {
             Lname1 = 'Required';
         }
         
         if(Email == null || Email == '')
         {
             Email1 = 'Required';
         }
         else
         {
             //Check email Format
             String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
             Pattern MyPattern = Pattern.compile(emailRegex);
             Matcher MyMatcher = MyPattern.matcher(Email);
             if(!(MyMatcher.matches()))//Email Data Validation
             {
                 Email1 = 'WrongData';
             }
         }
         
        if(BirthDate == null || BirthDate == '')
         {
             BirthDate1 = 'Required';
         }
         else
         {
             Integer m1;
             Integer d1;
             Integer y1;
             Integer m2;
             Integer d2;
             Integer y2;
             try
             {
                 //String candDate    = BirthDate.substring(0,Math.min(10,BirthDate.length()));
                 //dt = Date.parse(candDate);
                 //dt = Date.parse(BirthDate);
                List <String> dateParts = BirthDate.split('/');
                m1 = Integer.valueOf(dateParts[1]);
                d1 = Integer.valueOf(dateParts[0]);
                y1 = Integer.valueOf(dateParts[2]);
                dt = Date.newInstance(y1, m1, d1);
                //dt = temp.addMonths(m1);
               //dt = temp.addDays(d1);
               //dt = temp.addYears(y1);
                //dt=Date.valueOf(BirthDate);
                Date dt1 = Date.Today();
                m2 = dt1.month();
                d2 = dt1.day();
                y2 = dt1.year();


             }
             catch(Exception e)
             {
                 BirthDate1 = 'WrongData';
             }
            // if(dt >= Date.Today())//Birth data validation
            if(y1>y2 || (y2==y1 && m1 > m2) || (y2==y1 && m2==m1 && d1 > d2))
             {
                 BirthDate1 = 'WrongData';
             }
         }
         if(Phone == null || Phone == '')
         {
             Phone1 = 'Required';
         }
         else
         if(Phone.length() != 10 || (!Phone.isNumeric()))//Phone data validation
         {
             Phone1 = 'WrongData';
         }
         if((Phone2 != null && Phone2 != '') && (Phone2.length() != 10 || (!Phone2.isNumeric())))//Phone data validation
         {
             Phone21 = 'WrongData';
         }

         
         if(Addr1 == null || Addr1 == '')
         {
             Addr11 = 'Required';
         }
         if(Addr2 == null || Addr2 == '')
         {
             Addr21 = 'Required';
         }
         if(City == null || City == '')
         {
             City1 = 'Required';
         }
         if(State == null || State == '')
         {
             State1 = 'Required';
         }
         if(Zip == null || Zip == '')
         {
             Zip1 = 'Required';
         }
         else
         if(Zip.length() != 5 || (!Zip.isNumeric()))//Phone data validation
         {
             Zip1 = 'WrongData';
         }
         
         if(Reference == null || Reference == '')
         {
             Reference1 = 'Required';
         }
         if(Product == null || Product == '')
         {
             Product1 = 'Required';
         }
         if(TimeConsumer == null || TimeConsumer == '')
         {   
             //DM-6407
             //TimeConsumer1 = 'Required';
         } 
         if(Gender == null || Gender == '')
         {
             Gender1 = 'Required';
         }
        
    if(Email1 == 'no Errors' && Lname1 == 'no Errors' && Fname1 == 'no Errors' && BirthDate1 == 'no Errors' && Phone1 == 'no Errors' && Phone21 == 'no Errors' && Addr11 == 'no Errors' && Addr21 == 'no Errors' && City1 == 'no Errors' && State1 == 'no Errors' && Zip1 == 'no Errors' &&  Reference1 == 'no Errors' && Product1 == 'no Errors' && TimeConsumer1 == 'no Errors' && Gender1 == 'no Errors')    
     {  
        getState();
        getProduct();
        
        Account accnt=new Account();
        accnt.FirstName = Fname;
        accnt.LastName = Lname;
        accnt.PersonEmail = Email;
        accnt.Primary_Email_BI__c = Email;
        accnt.Gender_vod__c = Gender;
        accnt.PersonBirthDate = dt;
        accnt.CRC_Phone_MVN__c = Phone.replaceAll('[^0-9]', '');
        accnt.CRC_Fax_MVN__c = Phone2.replaceAll('[^0-9]', '');
        accnt.PersonMobilePhone  = Phone.replaceAll('[^0-9]', '');
        accnt.Phone = Phone2.replaceAll('[^0-9]', '');
        accnt.RecordTypeId = record.get(0).id;
        accnt.Country_Code_BI__c = 'MX';
        accnt.OK_Status_Code_BI__c = 'Temporary';
        accnt.Site_Terms_Conditions_BI__c = ApexPages.currentPage().getparameters().get('SiteAcceptTerms');
        
        try{
        insert accnt;
        }
        catch(Exception Ex)
        {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Ex.getMessage()));
            
        }
        
        Address_vod__c addr = new Address_vod__c();
        addr.account_vod__c = accnt.id;
        addr.Primary_vod__c = true;
        addr.Name = Addr1;
        addr.City_vod__c = City;
        addr.Address_line_2_vod__c = Addr2;
        addr.OK_State_Province_BI__c= StateId;
        addr.Zip_vod__c = Zip;
        addr.Country_Code_BI__c = 'MX';
        try
        {
        insert addr;
        }
        catch(Exception Ex)
        {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Ex.getMessage()));
            
        }
        //Interaction Case
        case c = new case();
        c.recordtypeid = record1.get(0).id;
        c.Requester_Type_MVN__c = 'Patient';
        c.AccountId = accnt.id;
        c.Address_MVN__c = addr.Id;
        c.case_Country_MVN__c = 'MX';
        c.Country_MITS__c = 'MX';
        c.CurrencyIsoCode = 'MXN';
        c.User_Country_Code_MVN__c = 'MX';
        c.Origin = 'Web';
        c.case_Account_Email_MVN__c = accnt.PersonEmail;
        c.case_Account_Phone_MVN__c = accnt.CRC_Phone_MVN__c;
        c.case_Account_Fax_MVN__c = accnt.CRC_Fax_MVN__c;
       
        try
        {
       insert c;
        }
        catch(Exception Ex)
        {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Ex.getMessage()));
            
        }
        
        //Request Case
        case c2 = new case();
        c2.parentid = c.id;
        c2.recordtypeid = record12.get(0).id;
        c2.Requester_Type_MVN__c = 'Patient';
        c2.AccountId = accnt.id;
        c2.Type = 'Coupon Management';
        c2.Category_MVN__c = 'Coupon Management';
        c2.Interaction_Notes__c = Reference;
        c2.Request_MVN__c = TimeConsumer;
        c2.Coupon__c = ApexPages.currentPage().getparameters().get('couponnumber');       
        c2.Coupon_Code__c = ApexPages.currentPage().getparameters().get('vid');//vascode1.get(0).id;
        c2.account__c =  ApexPages.currentPage().getparameters().get('accntid');//Hcpid.get(0).id;
        c2.Product_MVN__c = Prodid;//prod.get(0).id;
        c2.Address_MVN__c = addr.Id;
        c2.case_Country_MVN__c = 'MX';
        c2.Country_MITS__c = 'MX';
        c2.CurrencyIsoCode = 'MXN';
        c2.User_Country_Code_MVN__c = 'MX';
        c2.Origin = 'Web';
        c2.case_Account_Email_MVN__c = accnt.PersonEmail;
        c2.case_Account_Phone_MVN__c = accnt.CRC_Phone_MVN__c;
        c2.case_Account_Fax_MVN__c = accnt.CRC_Fax_MVN__c;
        
        try
        {
        insert c2;
        }
        catch(Exception Ex)
        {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Ex.getMessage()));
        
        } 
        
       List<Case> caselist =   [select casenumber from case where id=:c.id]; 
        casenumber = caselist.get(0).casenumber;
        
        //Email to the patient
      List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      List<String> sendTo = new List<String>();
      sendTo.add(Email);
      mail.setToAddresses(sendTo);
      
      mail.setReplyTo('mktviviendosanoregistro.mex@boehringer-ingelheim.com');
      mail.setSenderDisplayName('Viviendo Sano');
     
      mail.setSubject('Notificación por correo Viviendo Sano');
      String body = 'Gracias por haberse registrado en nuestro sitio "Viviendo Sano, bienestar por ti"<br/><br/>';
      body += 'El número de referencia para dar seguimiento a su registro es: ' + casenumber + '.<br/><br/>';
      body += 'Usted recibirá los materiales del programa al cual su médico le invitó a inscribirse y participar en un término máximo de 15 días hábiles en la dirección que nos proporcionó para su entrega, de no llegar estos en el tiempo indicado le solicitamos nos llame por favor a nuestro Centro de Atención Inmediata al 01 800 911 22 55  en la República Mexicana, Área Metropolitana y CDMX  al 56 29 86 00  en días hábiles de  09:00 a 19:00 horas  y sábados de 09:00 a 14:00 horas.<br/><br/>';
      body += 'Por favor no responda a este mensaje. Este correo fue enviado desde una dirección solamente de notificaciones que no puede aceptar correos electrónicos. <br/><br/> ';
      
      mail.setHtmlBody(body);
      mails.add(mail);

      Messaging.sendEmail(mails);

        
        displayPopup = true; 
      }  
        
    }
}