@isTest
private class BI_MX_CouponValidationTest{
    static testmethod void testmethod1(){
        Test.startTest();
        List<VAS_BI__c> vaslist = new List<VAS_BI__c>();
        
        VAS_BI__c vascode1 = new VAS_BI__c();        
        vascode1.Name='testCoupon';
        vascode1.Active_BI__c=true;
        vascode1.VAS_Code_BI__c='123456';
        
        
        VAS_BI__c vascode2 = new VAS_BI__c();
        vascode2.Name='testCoupon1';
        vascode2.Active_BI__c=true;
        vascode2.VAS_Code_BI__c='456789';       
        
        vaslist.add(vascode1);
        vaslist.add(vascode2);
        
        insert vaslist;
        
        BI_MX_CouponValidation controller = new BI_MX_CouponValidation();
        //Required fields not filled
        controller.validateHCP();
        
        controller.check = 'false';
        controller.selectcheckbox = false;
        controller.validateHCP();
        
        //Expired Coupon
        controller.coupon = '789';
        controller.file = '1234';
        controller.license = '1234';
        controller.check = 'true';
        controller.selectcheckbox = true;
        controller.validateHCP();
        
        //Non numeric file number
        controller.file = '1234a';
        controller.validateHCP();
        
        //Exception List Coupon
        Exception_List__c c = new Exception_List__c();
        c.Coupon_Number__c = '4567891234';
        c.VAS_CODE__c = vascode2.id;
        insert c;
        
        controller.coupon = '456789';
        controller.file = '1234';
        controller.check = 'true';
        controller.license = '1234';
        controller.selectcheckbox = true;
        controller.validateHCP();
        
        //Valid Coupon
        controller.coupon = '123456';
        controller.file = '1234';
        controller.license = '1234';
        controller.check = 'true';
        controller.selectcheckbox = true;
        controller.validateHCP(); 
        
        //Valid Account
        Knipper_Settings__c ks = new Knipper_Settings__c();
        ks.Account_Detect_Change_FieldList__c = 'FirstName,Middle_vod__c,LastName,ME__c,External_ID_vod__c,Specialty_1_vod__c,Credentials_vod__c,PersonEmail';
        ks.Account_Detect_Changes_Record_Type_List__c = 'Professional_vod,KOL_vod,Staff';
        insert ks;
        
        List<Account> acclist = new List<Account>();
        
        Account acc = new Account();
         acc.FirstName='Test1';
         acc.LastName='Test1';
         acc.recordtypeId= [select id, Name, SobjectType from RecordType where Name LIKE 'Professional_vod' and SobjectType = 'Account' LIMIT 1].id;
         acc.License_BI__c='2345678';
         acc.status_bi__c='Active';
        acclist.add(acc);
        
        Account acc1 = new Account();
         acc1.FirstName='Test2';
         acc1.LastName='Test2';
         acc1.recordtypeId= [select id, Name, SobjectType from RecordType where Name LIKE 'Professional_vod' and SobjectType = 'Account' LIMIT 1].id;
         acc1.License_BI__c='2345678';
         acc1.status_bi__c='Active';
        acclist.add(acc1);
        
         Account acc2 = new Account();
         acc2.FirstName='Test3';
         acc2.LastName='Test3';
         acc2.recordtypeId= [select id, Name, SobjectType from RecordType where Name LIKE 'Professional_vod' and SobjectType = 'Account' LIMIT 1].id;
         acc2.License_BI__c='12345';
         acc2.status_bi__c='Active';
        acclist.add(acc2);
        
        Account acc3 = new Account();
         acc3.FirstName='Test2';
         acc3.LastName='Test2';
         acc3.recordtypeId= [select id, Name, SobjectType from RecordType where Name LIKE 'Professional_vod' and SobjectType = 'Account' LIMIT 1].id;
         acc3.License_BI__c='123456';
         acc3.status_bi__c='Active';
        acclist.add(acc3);
         
        insert acclist;
        
        //Search By HCP License, duplicate HCP
         controller.coupon = '123456';
        controller.file = '1234';
        controller.license = '2345678';
        controller.check = 'true';
        controller.selectcheckbox = true;
        controller.validateHCP();
        
        //Search By HCP License, valid HCP       
        controller.coupon = '123456';
        controller.file = '1234';
        controller.license = '12345';
        controller.check = 'true';
        controller.selectcheckbox = true;
        controller.validateHCP();
        
        //Search By HCP name, does not exist
        controller.coupon = '123456';
        controller.file = '1234';
        controller.docfirstname = 'Test1234';
        controller.doclastname = 'Test1234';
        controller.check = 'false';
        controller.selectcheckbox = true;
        controller.validateHCP();
        
        //Search By HCP name, duplicate HCP
        controller.coupon = '123456';
        controller.file = '1234';
        controller.docfirstname = 'Test2';
        controller.doclastname = 'Test2';
        controller.check = 'false';
        controller.selectcheckbox = true;
        controller.validateHCP();
        
        //Search By HCP name, valid HCP
        controller.coupon = '123456';
        controller.file = '1234';
        controller.docfirstname = 'Test3';
        controller.doclastname = 'Test3';
        controller.check = 'false';
        controller.selectcheckbox = true;
        controller.validateHCP();
        
        Test.stopTest();
    }
    
    static testmethod void testmethod2(){
        Test.startTest();
        BI_MX_CouponValidation controller = new BI_MX_CouponValidation();
        controller.Email='';
        controller.casecreation();
        
        controller.Email='a.b';
        controller.casecreation();
        
        controller.Phone='1234';
        controller.casecreation();
        
        controller.Phone2='1234';
        controller.casecreation();
        
        controller.Zip='1234';
        controller.casecreation();
        
        DateTime dt = Date.Today() + 30;
        controller.BirthDate = dt.format('dd/mm/yyyy');
        controller.casecreation();
        
        controller.Fname = 'Test Consumer';
        controller.Lname = 'Test Consumer';
        controller.Email = 'a.b@xyz.com';
        controller.Gender = 'M';        
        controller.BirthDate = '01/05/1980';        
        controller.Phone = '1234567890';
        controller.Phone2 = '1234567890'; 
        controller.Addr1 = 'AddrLine1';
        controller.City = 'City1';
        controller.Addr2 = 'AddrLine2';
        controller.Zip = '23456';
        controller.State = 'Jalisco';
        controller.Reference = 'Test Reference';
        controller.TimeConsumer = '1 month';
        
        Product_vod__c prod = new Product_vod__c();
        prod.Name = 'Test Product';
        prod.External_ID_vod__c = '123';
        prod.Product_Type_vod__c = 'Detail';
        insert prod;
        
      
        
        Customer_Attribute_BI__c attr = new Customer_Attribute_BI__c();
        attr.OK_Country_ID_BI__c = 'MX';
        attr.Name = 'Chihuahua';
        
        insert attr;
               
        VAS_BI__c vascode1 = new VAS_BI__c();        
        vascode1.Name='testCoupon';
        vascode1.Active_BI__c=true;
        vascode1.VAS_Code_BI__c='987101';
        insert vascode1;
        
        TestDataFactoryMVN.createSettings();
        
         Account acc2 = new Account();
         acc2.FirstName='Test5';
         acc2.LastName='Test5';
         acc2.recordtypeId= [select id, Name, SobjectType from RecordType where Name LIKE 'Professional_vod' and SobjectType = 'Account' LIMIT 1].id;
         acc2.License_BI__c='12346';
         acc2.status_bi__c='Active';
        insert acc2;
        
        PageReference pg = Page.BI_MX_CaseCreation;
        Test.setCurrentPage(pg);
        pg.getParameters().put('couponnumber', '9871012345');
        pg.getParameters().put('vid',vascode1.id);
        pg.getParameters().put('accntid',acc2.id);
        controller.Product = '123';
        controller.State = 'Chihuahua';
        controller.getProduct();
        controller.getState();
        controller.showPopup();
        controller.closePopup();

        Group testGroup = new Group(Name='MX Call Center Admin', Type='Queue');
        insert testGroup;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        } 
        Case c = [Select Id from Case where casenumber=: controller.casenumber];
        Case c1 = [select OwnerId from Case where parentid=: c.id];
        c1.OwnerId = testGroup.Id;
        update c1;
        
        //Used Coupon
        controller.coupon = '987101';
        controller.file = '2345';
        controller.license = '1234';
        controller.check = 'true';
        controller.selectcheckbox = true;
        controller.validateHCP();
        
        controller.firstoption = 'true';
        controller.getselecteditem();
        controller.secondoption = 'false';
        controller.getselecteditem();
        Test.stopTest();
    }
}