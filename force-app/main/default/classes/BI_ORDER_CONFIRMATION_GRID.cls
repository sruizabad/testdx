public class BI_ORDER_CONFIRMATION_GRID 
{
	public Order_vod__c childOrder {get; set;}
	public list<Order_Line_vod__c> childOrderLines {get; set;}
	
	public BI_ORDER_CONFIRMATION_GRID (Order_vod__c co)
	{
		childOrder = co;
		childOrderLines = new list<Order_Line_vod__c>();
	}
	
	public void addChild(list<Order_Line_vod__c> ol)
	{
		childOrderLines = ol;
	}
	
}