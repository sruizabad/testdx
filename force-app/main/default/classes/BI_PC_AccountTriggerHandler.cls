/***********************************************************************************************************
* @date 01/08/2018 (dd/mm/yyyy)
* @description PC Account Tigger Handler Class (MA-DOTS)
************************************************************************************************************/
public class BI_PC_AccountTriggerHandler {
    
    private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	public BI_PC_AccountTriggerLogic logic = null;
	
	public BI_PC_AccountTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
		logic = BI_PC_AccountTriggerLogic.getInstance();
	}
				
	/***********************************************************************************************************
	* @date 01/08/2018 (dd/mm/yyyy)
	* @description OnAfterUpdate logic calls
	* @param List<BI_PC_Account__c> newAccounts	:	list of updated accounts
	* @param Map<Id, BI_PC_Account__c> oldAccountMap	:	map of account old values
	* @return void
	************************************************************************************************************/
    public void OnAfterUpdate(List<BI_PC_Account__c> newAccounts, Map<Id, BI_PC_Account__c> oldAccountMap) {
        
        Map<Id, BI_PC_Account__c> accMap = new Map<Id, BI_PC_Account__c>();	//map of accounts with descriptions updated
        
        for(BI_PC_Account__c pcAcc : newAccounts) {
            
            if(pcAcc.BI_PC_Description__c != oldAccountMap.get(pcAcc.Id).BI_PC_Description__c) {
                accMap.put(pcAcc.Id, pcAcc);
            }
        }
        
        logic.setProposalAccountDescription(accMap);	//set proposal account descriptions
    }
}