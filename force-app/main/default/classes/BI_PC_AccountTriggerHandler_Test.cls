/***********************************************************************************************************
* @date 01/08/2018 (dd/mm/yyyy)
* @description Text class for the PC Account Tigger Handler Class
************************************************************************************************************/
@isTest
public class BI_PC_AccountTriggerHandler_Test {

    private static BI_PC_Account__c mcAcc;
	private static BI_PC_Proposal__c mcProposal;
    private static Id userId;
    private static User analyst;
    
	/******************************************************************************************************************
    * @date             01/08/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
	private static void dataSetUp() {
        
        userId = UserInfo.getUserId();
		        
        System.runAs(analyst) {  
            
            Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
            mcAcc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test Managed Care Account', new User(Id = userId), false);
            mcAcc.BI_PC_Description__c = 'Test1';
            insert mcAcc;
            
            Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');
            mcProposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, mcAcc.Id, BI_PC_ProposalSubmitApprovalExt.PV_Status_Prop_Development, Date.today().addDays(30), Date.today().addDays(40) , FALSE);
            mcProposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
            insert new List<BI_PC_Proposal__c>{mcProposal};
        }
    }
    
    /***********************************************************************************************************
	* @date 			01/08/2018 (dd/mm/yyyy)
	* @description 		Test description update
	************************************************************************************************************/
    public static testMethod void updateDescriptionTest() {
        
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_RAE_MC', 0);
        
        //create data
        dataSetUp();
        
        Test.startTest();
        
        System.runAs(analyst) {
            
            mcAcc.BI_PC_Description__c = 'Test2';
            update mcAcc;
        }
        
        Test.StopTest();
                
        List<BI_PC_Proposal__c> proposalsList = [SELECT BI_PC_Account_description__c FROM BI_PC_Proposal__c WHERE Id = :mcProposal.Id];	//get updated proposal
        
        System.assert(!proposalsList.isEmpty());
        System.assertEquals(mcAcc.BI_PC_Description__c, proposalsList[0].BI_PC_Account_description__c);
    }
}