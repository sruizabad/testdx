/***********************************************************************************************************
* @date 01/08/2018 (dd/mm/yyyy)
* @description PC Account Tigger Handler Class (MA-DOTS)
************************************************************************************************************/
public class BI_PC_AccountTriggerLogic {

    public static final String PV_Status_Prop_Development = 'Proposal in Development';
	public static BI_PC_AccountTriggerLogic instance = null;
	private BI_PC_AccountTriggerLogic() {}
    
	public static BI_PC_AccountTriggerLogic getInstance() {
        if(instance == null) {
            instance = new BI_PC_AccountTriggerLogic();
        } 
		return instance;
	}
    
    /***********************************************************************************************************
	* @date 01/08/2018 (dd/mm/yyyy)
	* @description Set the description of the account to the proposal
	* @param Map<Id, BI_PC_Account__c> accMap	:	map of updated accounts
	* @return void
	************************************************************************************************************/
    public void setProposalAccountDescription(Map<Id, BI_PC_Account__c> accMap) {
        
        List<BI_PC_Proposal__c> proposalsList = new List<BI_PC_Proposal__c>();	//proposals to be updated
        
        for(BI_PC_Proposal__c proposal : [SELECT Id, BI_PC_Account__c FROM BI_PC_Proposal__c WHERE BI_PC_Account__c IN :accMap.keySet() AND BI_PC_Status__c = :PV_Status_Prop_Development]) {
            
            proposal.BI_PC_Account_description__c = accMap.get(proposal.BI_PC_Account__c).BI_PC_Description__c;	//set account description
            proposalsList.add(proposal);
        }
        
        if(!proposalsList.isEmpty()) {
            update proposalsList;
        }
    }
}