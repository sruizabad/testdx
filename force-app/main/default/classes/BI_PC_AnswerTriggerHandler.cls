/***********************************************************************************************************
* @date 27/07/2018 (dd/mm/yyyy)
* @description PC Answer Tigger Handler Class (MA-DOTS)
************************************************************************************************************/
public class BI_PC_AnswerTriggerHandler {

    private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	public BI_PC_AnswerTriggerLogic logic = null;
	
	public BI_PC_AnswerTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
		logic = BI_PC_AnswerTriggerLogic.getInstance();
	}
	
	/***********************************************************************************************************
	* @date 27/07/2018 (dd/mm/yyyy)
	* @description OnBeforeInsert logic calls
	* @param List<BI_PC_Answer__c> newAnswersList	:	List with new values of Answers
	* @return void
	************************************************************************************************************/
    public void OnBeforeInsert(List<BI_PC_Answer__c> newAnswersList) {
        logic.checkExistingAnswersInsert(newAnswersList);
    }
      
	/***********************************************************************************************************
	* @date 27/07/2018 (dd/mm/yyyy)
	* @description OnAfterDelete logic calls
	* @param List<BI_PC_Answer__c> oldAnswersList	:	List with old values of Answers
	* @return void
	************************************************************************************************************/
    public void OnAfterDelete(List<BI_PC_Answer__c> oldAnswersList) {
        logic.checkExistingAnswersDelete(oldAnswersList);
    }  
}