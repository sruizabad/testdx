/***********************************************************************************************************
* @date 30/07/2018 (dd/mm/yyyy)
* @description Test class for the PC Answer Tigger Handler
************************************************************************************************************/
@isTest
public class BI_PC_AnswerTriggerHandler_Test {

    public static testMethod void checkFlagMainAnswerInsert_Test() {
        
        //create analyst
    	User analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
        
        //create account
        Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
        BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test Commercial Account', analyst, false);
        acc.BI_PC_Is_pool__c = FALSE;
        insert acc;

		//create proposal
        Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');
        BI_PC_Proposal__c proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40), false);
        proposal.BI_PC_Adjustment__c = 23;
        insert proposal;
               
        //create question
        BI_PC_Question__c newQuestion = BI_PC_TestMethodsUtility.getPCQuestion('Proposal', true);
        
        Test.startTest();
        
        //create answer
        BI_PC_Answer__c newAnswer = BI_PC_TestMethodsUtility.getPCAnswer(newQuestion.Id, proposal.Id, true);
        
        //create existing answer
        BI_PC_Answer__c existingAnswer = newAnswer.clone(false, true);
        insert existingAnswer;
        
        Test.stopTest();
        
        Integer mainAnswers = [SELECT COUNT() FROM BI_PC_Answer__c WHERE BI_PC_Is_main_answer__c = true];
        System.assertEquals(1, mainAnswers);
    }
    
    public static testMethod void checkFlagMainAnswerDelete_Test() {
        
        //create analyst
    	User analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
        
        //create account
        Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
        BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test Commercial Account', analyst, false);
        acc.BI_PC_Is_pool__c = FALSE;
        insert acc;

		//create proposal
        Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');
        BI_PC_Proposal__c proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40), false);
        proposal.BI_PC_Adjustment__c = 23;
        insert proposal;
               
        //create question
        BI_PC_Question__c newQuestion = BI_PC_TestMethodsUtility.getPCQuestion('Proposal', true);
        
        //create answer
        BI_PC_Answer__c newAnswer = BI_PC_TestMethodsUtility.getPCAnswer(newQuestion.Id, proposal.Id, true);
        
        //create existing answer
        BI_PC_Answer__c existingAnswer = newAnswer.clone(false, true);
        insert existingAnswer;
        
        Test.startTest();
        
        //delete the main answer
        delete newAnswer;
        
        Test.stopTest();
        
        Integer mainAnswers = [SELECT COUNT() FROM BI_PC_Answer__c WHERE BI_PC_Is_main_answer__c = true];
        System.assertEquals(1, mainAnswers);
    }
}