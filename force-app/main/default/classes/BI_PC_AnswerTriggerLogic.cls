/***********************************************************************************************************
* @date 27/07/2018 (dd/mm/yyyy)
* @description Logic for the PC Proposal Tigger Handler Class (MA-DOTS)
************************************************************************************************************/
public class BI_PC_AnswerTriggerLogic {

	public static BI_PC_AnswerTriggerLogic instance = null;
	private BI_PC_AnswerTriggerLogic() {}
	public static BI_PC_AnswerTriggerLogic getInstance() {
		if(instance == null) instance = new BI_PC_AnswerTriggerLogic();
		return instance;
	}
    
	/***********************************************************************************************************
	* @date 27/07/2018 (dd/mm/yyyy)
	* @description Check if the answers are new or already exist on creation
	* @param List<BI_PC_Answer__c> answersList	:	List with processed answers
	* @return void
	************************************************************************************************************/
    public void checkExistingAnswersInsert(List<BI_PC_Answer__c> answersList) {
        
        Set<Id> questionIdsSet = new Set<Id>();	//set of question ids
        Map<Id, Set<String>> existingAnswersMap = new Map<Id, Set<String>>();	//map of existing answers by question
        
        //store question ids
        for(BI_PC_Answer__c newAnswer : answersList) {
            questionIdsSet.add(newAnswer.BI_PC_Question__c);
            existingAnswersMap.put(newAnswer.BI_PC_Question__c, new Set<String>());	//initialize sets inside the map
        }
        System.debug(LoggingLevel.DEBUG, 'BI_PC_AnswerTriggerLogic.checkExistingAnswersInsert>>>>>questionIdsSet: ' + questionIdsSet);
        
        //store existing answers
        for(BI_PC_Answer__c existingAnswer : fetchAnswers(questionIdsSet, true)) {
            Set<String> answersSet = existingAnswersMap.get(existingAnswer.BI_PC_Question__c);
            answersSet.add(existingAnswer.BI_PC_Answer__c);
            existingAnswersMap.put(existingAnswer.BI_PC_Question__c, answersSet);
        }
        System.debug(LoggingLevel.DEBUG, 'BI_PC_AnswerTriggerLogic.checkExistingAnswersInsert>>>>>existingAnswersMap: ' + existingAnswersMap);
        
        for(BI_PC_Answer__c newAnswer : answersList) {
            
            Set<String> answersSet = existingAnswersMap.get(newAnswer.BI_PC_Question__c);
            
            if(!answersSet.contains(newAnswer.BI_PC_Answer__c)) {
                newAnswer.BI_PC_Is_main_answer__c = true;	//set to true if the answer is new
            }
        }
        System.debug(LoggingLevel.DEBUG, 'BI_PC_AnswerTriggerLogic.checkExistingAnswersInsert>>>>>answersList: ' + answersList);
    }
        
	/***********************************************************************************************************
	* @date 27/07/2018 (dd/mm/yyyy)
	* @description Check if the answers are new or already exist on deletion
	* @param List<BI_PC_Answer__c> answersList	:	List with processed answers
	* @return void
	************************************************************************************************************/
    public void checkExistingAnswersDelete(List<BI_PC_Answer__c> answersList) {
        
        List<BI_PC_Answer__c> mainAnswersList = new List<BI_PC_Answer__c>();	//list of main answers to be updated
        Set<Id> questionIdsSet = new Set<Id>();	//set of question ids
        Map<Id, BI_PC_Answer__c> oldAnswersMap = new Map<Id, BI_PC_Answer__c>();	//map of old answers by question
        
        //store old answers
        for(BI_PC_Answer__c oldAnswer : answersList) {
            if(oldAnswer.BI_PC_Is_main_answer__c) {
                questionIdsSet.add(oldAnswer.BI_PC_Question__c);
                oldAnswersMap.put(oldAnswer.BI_PC_Question__c, oldAnswer);	//initialize sets inside the map
            }
        }
        System.debug(LoggingLevel.DEBUG, 'BI_PC_AnswerTriggerLogic.checkExistingAnswersInsert>>>>>oldAnswersMap: ' + oldAnswersMap);
        
        if(!questionIdsSet.isEmpty()) {
            
            //get existing answers
            for(BI_PC_Answer__c existingAnswer : fetchAnswers(questionIdsSet, false)) {
                
                if(oldAnswersMap.containsKey(existingAnswer.BI_PC_Question__c)) {
                    
                    BI_PC_Answer__c oldAnswer = oldAnswersMap.get(existingAnswer.BI_PC_Question__c);	//get old answer related to the question
                                        
                    if(oldAnswer.BI_PC_Answer__c == existingAnswer.BI_PC_Answer__c) {
                        existingAnswer.BI_PC_Is_main_answer__c = true;	//set the answer as a main one
                        mainAnswersList.add(existingAnswer);	//add main answer to the list to be updated
                        oldAnswersMap.remove(existingAnswer.BI_PC_Question__c);	// remove the question from the map
                    }
                    
                    if(oldAnswersMap.isEmpty()) {
                        break;	//break the loop if all questions are processed
                    }
                }                
            }
            System.debug(LoggingLevel.DEBUG, 'BI_PC_AnswerTriggerLogic.checkExistingAnswersInsert>>>>>mainAnswersList: ' + mainAnswersList);
            
            if(!mainAnswersList.isEmpty()) {
                update mainAnswersList;
            }
        }
    }
    
	/***********************************************************************************************************
	* @date 27/07/2018 (dd/mm/yyyy)
	* @description Get the answers 
	* @param Set<Id> questionIdsSet	:	Set with Ids of the related questions
	* @param Boolean getMainAnswers	:	True if we get the main answers
	* @return List<BI_PC_Answer__c>
	************************************************************************************************************/
    private List<BI_PC_Answer__c> fetchAnswers(Set<Id> questionIdsSet, Boolean getMainAnswers) {
        
        return [SELECT Id, BI_PC_Answer__c, BI_PC_Question__c 
                FROM BI_PC_Answer__c 
                WHERE BI_PC_Question__c IN: questionIdsSet 
                	AND BI_PC_Is_main_answer__c = :getMainAnswers
               ORDER BY CreatedDate DESC LIMIT 30000];
    }
}