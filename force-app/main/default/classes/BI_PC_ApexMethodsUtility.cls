/***********************************************************************************************************
* @date 08/06/2018 (dd/mm/yyyy)
* @description Utility Apex Class which stores some iCon app methods
************************************************************************************************************/
public class BI_PC_ApexMethodsUtility {

	/***********************************************************************************************************
	* @date 08/06/2018 (dd/mm/yyyy)
	* @description This method parses different types of values into String values
	* @parameters: 
	*		- objValue: input object whose fields values need to parse
	*		- apiName: api name of the field we need to parse
	*		- fieldType: type of the field we need to parse
	*		- referenceMap: this map stores the ids grouped by field so we can query their names later.
	************************************************************************************************************/
	public static String parseToString(sObject objValue, String apiName, Schema.DisplayType fieldType){
		
		String parsedString;

		if(fieldType == Schema.DisplayType.Date){
			
			Date dateValue = (Date) objValue.get(apiName);
			parsedString = String.valueOf(dateValue);
			
		}else if(fieldType == Schema.DisplayType.Percent || fieldType == Schema.DisplayType.Currency || fieldType == Schema.DisplayType.Double){
			
			Decimal decimalValue = (Decimal) objValue.get(apiName);
			parsedString = String.valueOf(decimalValue);

		}/*else if(fieldType == Schema.DisplayType.Integer){
			
			Integer integerValue = (Integer) objValue.get(apiName);
			parsedString = String.valueOf(integerValue);

		}*/else if(fieldType == Schema.DisplayType.Boolean){
			
			Boolean booleanValue = (Boolean) objValue.get(apiName);
			parsedString = (booleanValue ? 'True' : 'False');

		}else if(fieldType == Schema.DisplayType.DateTime){
			
			DateTime dateTimeValue = (DateTime) objValue.get(apiName);
			parsedString = String.valueOf(dateTimeValue);

		}else{
			
			parsedString = (String) objValue.get(apiName);
		}

		

        return parsedString;
	}

	/***********************************************************************************************************
	* @date 07/08/2018 (dd/mm/yyyy)
	* @description This method is used to remove the "Read" permission for the RAM/NAD users 
	*			   when proposal changes to the next status
	* @parameters: 
	*		- proposals: List of proposal which changed to the next status
	************************************************************************************************************/
	@InvocableMethod(label='Remove RAM/NAD Sharing' description='Removes the proposal sharing records for RAM/NAD users when the proposal change to the next status')
	public static void removeSharingForRamNadUser(List<BI_PC_Proposal__c> proposals) {
  
		Map<Id,BI_PC_Proposal__c> propMap = new Map<Id,BI_PC_Proposal__c>(proposals);
		Set<Id> ramNadUserId = new Set<Id>();
		Map<Id,BI_PC_Proposal__Share> propShareMap = new Map<Id,BI_PC_Proposal__Share>();
		List<BI_PC_Proposal__Share> propSharingToDelete = new List<BI_PC_Proposal__Share>();

		//group ram/nad users by record
        for(BI_PC_Proposal__c p: [SELECT Id, BI_PC_Account__c, BI_PC_Account__r.BI_PC_Ram_nad__c, BI_PC_Account__r.BI_PC_Ram_nad__r.Email FROM BI_PC_Proposal__c WHERE Id IN :propMap.keySet()]){
            ramNadUserId.add(p.BI_PC_Account__r.BI_PC_Ram_nad__c);
        }

        //Remove the admin users from user map (we don't need to apply sharing settings for them)
        for (GroupMember gm : [SELECT Id, UserOrGroupId FROM GroupMember WHERE UserOrGroupId IN: ramNadUserId AND Group.DeveloperName = 'BI_PC_Admin']){
            ramNadUserId.remove(gm.UserOrGroupId);
        }

		//Check existing proposal share for RAM/NAD user
        for(BI_PC_Proposal__Share pShare : [SELECT Id, ParentId FROM BI_PC_Proposal__Share WHERE UserOrGroupId = : ramNadUserId]){
            propShareMap.put(pShare.ParentId,pShare);
        }
        
        //Check if there are some sharing records to delete
        for(Id propId: propMap.keySet()){
            if(propShareMap.containsKey(propId)){
                propSharingToDelete.add(propShareMap.get(propId));
            }
        }

        //return propSharingToDelete;

        
        if(!propSharingToDelete.isEmpty()){
            delete propSharingToDelete;
        }
    }
    
}