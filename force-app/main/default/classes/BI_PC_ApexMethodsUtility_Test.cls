/****************************************************************************************************
* @date 12/06/2018 (dd/mm/yyyy) 
* @description This is the test class for BI_PC_ApexMethodsUtility Apex Class
****************************************************************************************************/
@isTest
private class BI_PC_ApexMethodsUtility_Test {

    private static BI_PC_Proposal__c govProposal;
    private static User biManagement;
    private static User analyst;
    private static BI_PC_Account__c govAcc;
    private static BI_PC_Proposal__Share pShareToInsert;

    /******************************************************************************************************************
    * @date             07/08/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
    private static void dataSetUp() {
        
        biManagement = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_BI_Management', 1);
        GroupMember bmGroupMember = BI_PC_TestMethodsUtility.insertGroupMember('BI_PC_RAM_NAD', biManagement.Id);

        Id userId = UserInfo.getUserId();
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
                
        System.runAs(analyst) {  
            Id accGovernmentId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_government', 'BI_PC_Account__c');
            govAcc = BI_PC_TestMethodsUtility.getPCAccount(accGovernmentId, 'Test Government Account',analyst, FALSE);
            govAcc.BI_PC_Ram_nad__c = biManagement.Id;
            insert govAcc;
            
            Id governmentPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_government', 'BI_PC_Proposal__c');
            govProposal = BI_PC_TestMethodsUtility.getPCProposal(governmentPropId, govAcc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40) , FALSE);
            govProposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
            insert govProposal;  
        }

        
    }

    @isTest
    public static void BI_PC_ApexMethodsUtility(){

    	User analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);

        Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
        BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test Commercial Account', analyst, FALSE);
        acc.BI_PC_Is_pool__c = FALSE;
        insert acc;


        Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');      
        //BI_PC_Proposal__c proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40), analyst.Id , FALSE);
        BI_PC_Proposal__c proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40), FALSE);
        proposal.BI_PC_Adjustment__c = 23;
        insert proposal;

        Test.startTest();

        String dateTest = BI_PC_ApexMethodsUtility.parseToString(proposal, 'BI_PC_Due_date__c', BI_PC_Proposal__c.BI_PC_Due_date__c.getDescribe().getType());
        String dtTest = BI_PC_ApexMethodsUtility.parseToString(proposal, 'CreatedDate', BI_PC_Proposal__c.CreatedDate.getDescribe().getType());
        String decTest = BI_PC_ApexMethodsUtility.parseToString(proposal, 'BI_PC_Adjustment__c', BI_PC_Proposal__c.BI_PC_Adjustment__c.getDescribe().getType());
        //String decTest = BI_PC_ApexMethodsUtility.parseToString(proposal, 'BI_PC_Proposal_duration__c', BI_PC_Proposal__c.BI_PC_Proposal_duration__c.getDescribe().getType());
        String booleanTest = BI_PC_ApexMethodsUtility.parseToString(acc, 'BI_PC_Is_pool__c', BI_PC_Account__c.BI_PC_Is_pool__c.getDescribe().getType());
        String picklistTest = BI_PC_ApexMethodsUtility.parseToString(proposal, 'BI_PC_Status__c', BI_PC_Proposal__c.BI_PC_Status__c.getDescribe().getType());

        Test.stopTest();
    }

    @isTest
    public static void BI_PC_RemoveSharing_Test(){
        String ramStatus = 'Approval in Progress: MM Sales';
        String dccStatus = 'Approval in Progress: Dir Contract Compliance';

        dataSetUp();

        List<BI_PC_Proposal__Share> priorPropShare = new List<BI_PC_Proposal__Share>();
        List<BI_PC_Proposal__Share> finalPropShare = new List<BI_PC_Proposal__Share>();

        Test.startTest();

        System.runAs(analyst) {
            govProposal.BI_PC_Status__c = ramStatus;
            update govProposal;

            priorPropShare = [SELECT Id, ParentId FROM BI_PC_Proposal__Share WHERE UserOrGroupId = : biManagement.Id LIMIT 1];
                   
            govProposal.BI_PC_Status__c = dccStatus;
            update govProposal;

            finalPropShare = [SELECT Id, ParentId FROM BI_PC_Proposal__Share WHERE UserOrGroupId = : biManagement.Id LIMIT 1];
        }
        
        Test.stopTest();

        system.assertEquals(govProposal.Id,priorPropShare.get(0).ParentId);
        system.assertEquals(TRUE,finalPropShare.isEmpty());
    }
 
}