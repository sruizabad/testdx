/***********************************************************************************************************
* @date 04/09/2018 (dd/mm/yyyy)
* @description This is the test class for the BI_PC_CreateContentDocumentLinksBatch Batch class.
************************************************************************************************************/
@isTest
public class BI_PC_CreateContentDocLinksBatch_Test {
	private static BI_PC_Proposal__c govProposal;

    @isTest
	static void BI_PC_CreateContentDocumentLinksBatch_test(){
			Id userId = UserInfo.getUserId();
        	User adminUser = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
			Id accGovernmentId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_government', 'BI_PC_Account__c');
            BI_PC_Account__c govAcc = BI_PC_TestMethodsUtility.getPCAccount(accGovernmentId, 'Test Government Account',adminUser, TRUE);
            
            Id governmentPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_government', 'BI_PC_Proposal__c');
            govProposal = BI_PC_TestMethodsUtility.getPCProposal(governmentPropId, govAcc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40) , TRUE);

            System.runAs(adminUser) {
            	String propName = [SELECT Name FROM BI_PC_Proposal__c WHERE Id =: govProposal.Id LIMIT 1].Name;
	            ContentVersion cv = new ContentVersion(Title='testDoc', ExternalDocumentInfo1=propName, PathOnClient='test');
	            Blob b = Blob.valueOf('Unit Test Attachment Body');
				cv.versiondata = EncodingUtil.base64Decode('Unit Test Attachment Body');
	            insert cv;

	            Test.startTest();
	            Integer batchSize = 200;

	            Date endDate = Date.today().addDays(1);
	            Date startDate = Date.today().addDays(-1);

	            String endDateString = String.valueOf(endDate) + 'T00:00:00Z';
	            String startDateString = String.valueOf(startDate) + 'T00:00:00Z';
	            
	        	BI_PC_CreateContentDocumentLinksBatch myBatch = new BI_PC_CreateContentDocumentLinksBatch(adminUser.Id,startDateString,endDateString);
	        	ID batchprocessid = Database.executeBatch(myBatch,batchSize);
	            Test.stopTest();
	        }

	        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>([SELECT Id, ContentDocumentId, LinkedEntityId, ShareType FROM ContentDocumentLink WHERE LinkedEntityId =: govProposal.Id]);

	        system.assertEquals(1,cdl.size());
	}
}