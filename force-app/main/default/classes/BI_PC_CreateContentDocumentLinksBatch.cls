/***********************************************************************************************************
* @date 30/08/2018 (dd/mm/yyyy)
* @description Batch class used during the migration of files in order to create the ContentDocumentLinks
************************************************************************************************************/
global class BI_PC_CreateContentDocumentLinksBatch implements Database.Batchable<sObject> {

	global final string query;
	global final Id orgId;

	global BI_PC_CreateContentDocumentLinksBatch(Id userId, String startDate, String endDate) {
		//Exrtact all the ContentVersion created today
        query = 'SELECT ContentDocumentId, Title, ExternalDocumentInfo1 FROM ContentVersion WHERE CreatedDate >= ' + startDate + ' AND CreatedDate <= ' + endDate + ' AND CreatedById = \'' + userId + '\'';
    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('BI_PC_CreateContentDocumentLinksBatch:: start:: ' + query);       
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {     

		List<ContentDocumentLink> cdLinkList = new List<ContentDocumentLink>();
		//Map<String, Map<String,Id>> proposalContentMap = new Map<String, Map<String,Id>>();
		Map<String, Map<String,Id>> proposalContentMap = new Map<String, Map<String,Id>>();
        
        try {
        	for(Sobject s : scope){

        		ContentVersion cv = (ContentVersion) s;
        		//Store all the information by proposal name and by title of the document
        		if(!proposalContentMap.containsKey(cv.ExternalDocumentInfo1)){
        			proposalContentMap.put(cv.ExternalDocumentInfo1, new Map<String, Id>());
        		}

        		proposalContentMap.get(cv.ExternalDocumentInfo1).put(cv.Title, cv.ContentDocumentId);
        	}

        	System.debug('BI_PC_CreateContentDocumentLinksBatch:: proposalContentMap:: ' + proposalContentMap);  

        	//For each proposal we need to attach all the different files.
            for(BI_PC_Proposal__c proposal : [SELECT Id, Name FROM BI_PC_Proposal__c WHERE Name IN: proposalContentMap.keySet()]) {

            	Map<String, Id> titleContentMap = proposalContentMap.get(proposal.Name);

            	//loop every file for each proposal
            	for(String title: titleContentMap.keySet()){
            		//link to proposal
				    ContentDocumentLink cdlProp = new ContentDocumentLink();
				    cdlProp.ContentDocumentId = titleContentMap.get(title);
				    cdlProp.LinkedEntityId = proposal.Id;
				    cdlProp.ShareType = 'I';
				    cdLinkList.add(cdlProp);
				}
			}
			System.debug('BI_PC_CreateContentDocumentLinksBatch:: cdLinkList:: ' + cdLinkList);

			insert cdLinkList;
			
        } catch(exception e){           
            String errorMsg = 'Error at creating custom links: '+ e.getMessage();
        }
    }

    global void finish(Database.BatchableContext BC) {
        system.debug('Batch Job is Complete');
    }
}