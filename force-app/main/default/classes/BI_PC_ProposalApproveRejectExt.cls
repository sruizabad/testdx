/***********************************************************************************************************
* @date 24/07/2018 (dd/mm/yyyy)
* @description Extension class for the BI_PC_ProposalApproveReject visualforce page
************************************************************************************************************/
public class BI_PC_ProposalApproveRejectExt {
 
    public List<BI_PC_ScenariosApproveRejectWrp> scenariosWrpList {
        get {
                return this.scenariosWrpList;
        } 
        set;
    }   //list of scenarios to be displayed
    public static Boolean error {get; set;} //flag to disable functionality when any error occurs
    private Set<Id> scenariosIdSet; //set of the scenario ids in the approval process
    public static Id proposalId {
        get {
            return ApexPages.currentPage().getParameters().get('id');
        } 
        set;
    }   //id of the proposal
    
    //constants
    public static final String PV_Status_Approved = 'Approved';
    public static final String PV_Status_Rejected = 'Rejected';
    public static final String PV_Status_AiP_BM_DC_Development = 'Approval in Progress: Brand Marketing - Dir Contract Development';
    public static final String PV_Status_AiP_DC_Development = 'Approval in Progress: Dir Contract Development';
    public static final String PV_Status_AiP_BM_DC_Compliance = 'Approval in Progress: Brand Marketing - Dir Contract Compliance';
    public static final String PV_Status_AiP_DC_Compliance = 'Approval in Progress: Dir Contract Compliance';
    public static final String PV_Status_AiP_Gov_Compliance = 'Approval in Progress: Government Compliance';
    public static final String PV_Status_AiP_Pricing_Contracting = 'Approval in Progress: Pricing & Contracting';
    public static final String PV_Status_AiP_Market_Access = 'Approval in Progress: Market Access';
    public static final String PV_Status_AiP_MM_Sales = 'Approval in Progress: MM Sales';
    public static final String PV_Status_AiP_PTC_Sc = 'Approval in Progress: PTC Sc';
    public static final String PV_Status_AiP_PTC = 'Approval in Progress: PTC';
    public static final String Q_Brand_Marketing = 'BI_PC_Brand_marketing';
    public static final String Q_DC_Compliance = 'BI_PC_Director_Contract_Compliance';
    public static final String Q_DC_Development = 'BI_PC_Director_contract_dev';
    public static final String Q_Gov_compliance = 'BI_PC_Government_Compliance';
    public static final String Q_Executive_director = 'BI_PC_Executive_director';
    public static final String Q_VP_Market_Access = 'BI_PC_VP_Market_access';
    public static final String Q_PTC_Sc = 'BI_PC_PTC_Sub_committee';
    public static final String Q_RAM_NAD = 'BI_PC_RAM_NAD';
    public static final String PG_Admin = 'BI_PC_Admin';
    public static final String PG_Brand_Marketing = 'BI_PC_Brand_Marketing';
    public static final String PG_DC_Compliance = 'BI_PC_Director_Contract_Compliance';
    public static final String PG_DC_Dev = 'BI_PC_Director_Contract_Dev';
    public static final String PG_Executive_Director = 'BI_PC_Executive_Director';
    public static final String PG_Gov_Compliance = 'BI_PC_Government_Compliance';
    public static final String PG_PTC = 'BI_PC_PTC';
    public static final String PG_PTC_Sc = 'BI_PC_PTC_Sub_Committee';
    public static final String PG_RAM_NAD = 'BI_PC_RAM_NAD';
    public static final String PG_VP_Market_Access = 'BI_PC_VP_Market_Access';
    public static final String PG_VP_MM_Sales = 'BI_PC_VP_MM_Sales';
    public static final String RT_Prop_Government = 'BI_PC_Prop_government';
    public static final String RT_Prop_Managed_Care = 'BI_PC_Prop_m_care';
    
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Constructor method
    ************************************************************************************************************/
    public BI_PC_ProposalApproveRejectExt(ApexPages.StandardController ctrl) {
        
        error = false;  //initialize error flag
        
        validateUserCanWorkOnScenarios();   //validate that the user can approve or reject the scenarios
    }
    
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Validate that the user can work on the scenarios
    * @return void
    ************************************************************************************************************/
    private void validateUserCanWorkOnScenarios() {
        
        scenariosWrpList = new List<BI_PC_ScenariosApproveRejectWrp>(); //list of wrappers
        
        List<BI_PC_Scenario__c> scenariosList = fetchScenariosData(proposalId);
        System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt>>>>>scenariosList: ' + scenariosList);
        
        if(scenariosList != null && !scenariosList.isEmpty()) {
            List<BI_PC_Proposal__c> proposalList = fetchProposalData(proposalId);   //get proposal data
            System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.validateUserCanWorkOnScenarios>>>>>proposalList: ' + proposalList);
            
            if(proposalList != null && !proposalList.isEmpty() && isValidUser(proposalList[0])) {
                
                scenariosWrpList = new List<BI_PC_ScenariosApproveRejectWrp>(); //initialize wrapper list
                scenariosIdSet  = new Set<Id>();    //initialize set of scenario ids
                
                //add not aproved / rejected scenarios to the wrappers list
                for(BI_PC_Scenario__c scenario : scenariosList) {
                    if(scenario.BI_PC_Approval_status__c != PV_Status_Approved && scenario.BI_PC_Approval_status__c != PV_Status_Rejected) {
                        scenariosWrpList.add(new BI_PC_ScenariosApproveRejectWrp(scenario)); 
                        scenariosIdSet.add(scenario.Id);
                    }
                }
                System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.validateUserCanWorkOnScenarios>>>>>scenariosWrpList: ' + scenariosWrpList);
            } else {
                setError(Label.BI_PC_Approve_error_msg);    //no privileges error
            }
        } else {
            setError(Label.BI_PC_No_Scenarios_for_approval);    //no scenarios error
        }        
    }

    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Get scenarios data
    * @param Id proposalId  :   Id of the proposal
    * @return List<BI_PC_Scenario__c>   :   list with scenarios related to the proposal
    ************************************************************************************************************/
    private static List<BI_PC_Scenario__c> fetchScenariosData(Id proposalId) {
        return [SELECT Id, Name, BI_PC_Product_Description__c, BI_PC_Position__c, RecordType.DeveloperName, 
                    BI_PC_Guide_line__c, BI_PC_Out_of_GL__c, BI_PC_Price_prot__c, BI_PC_Proposal__c, 
                    BI_PC_Approval_status__c, BI_PC_RAM_NAD__c, BI_PC_Rate__c, BI_PC_Comment__c, BI_PC_Approval_comments__c
                FROM BI_PC_Scenario__c 
                WHERE BI_PC_Proposal__c = :proposalId 
                    AND RecordType.DeveloperName = 'BI_PC_Future'
                    AND BI_PC_Approval_status__c != :PV_Status_Rejected 
                    AND BI_PC_Approval_status__c != :PV_Status_Approved 
                    AND BI_PC_Approval_status__c != :''];
    }
    
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Get proposal data
    * @param Id proposalId  :   Id of the proposal
    * @return List<BI_PC_Proposal__c>   :   list with the proposal data
    ************************************************************************************************************/
    private static List<BI_PC_Proposal__c> fetchProposalData(Id proposalId) {
        return [SELECT BI_PC_Status__c, BI_PC_BM_processed__c, BI_PC_DC_processed__c, RecordType.DeveloperName, 
                    BI_PC_Account__r.BI_PC_Ram_nad__c 
                FROM BI_PC_Proposal__c 
                WHERE Id = :proposalId];
    }
    
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Check if is a valid user based on the group
    * @param BI_PC_Proposal__c proposal :   proposal data
    * @return Boolean   :   true if the user can approve the proposal in the current status
    ************************************************************************************************************/
    private static Boolean isValidUser(BI_PC_Proposal__c proposal) {
		return 0 < [SELECT COUNT() 
                    FROM GroupMember 
                    WHERE UserOrGroupId = : UserInfo.getUserId() 
                        AND Group.DeveloperName IN :getGroupDeveloperNames(proposal)];        
    }
    
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Get the developer names of the groups which can work on the scenarios
    * @param BI_PC_Proposal__c proposal :   proposal data
    * @return Set<String>   :   developer names of the groups that can approve the proposal in the current status
    ************************************************************************************************************/
    private static Set<String> getGroupDeveloperNames(BI_PC_Proposal__c proposal) {
        
        Set<String> groupDevNamesSet = new Set<String>();
        groupDevNamesSet.add(PG_Admin); //add admin group to the list
        
        //get the groups related to the current step
        if(proposal.BI_PC_Status__c == PV_Status_AiP_BM_DC_Development)  {
            
            //check if any role in this step has already process the scenarios
            if(!proposal.BI_PC_BM_processed__c) {
                groupDevNamesSet.add(PG_Brand_Marketing);
            }
            if(!proposal.BI_PC_DC_processed__c) {
                groupDevNamesSet.add(PG_DC_Dev);
            }
        } else if(proposal.BI_PC_Status__c == PV_Status_AiP_DC_Development) {
            groupDevNamesSet.add(PG_DC_Dev);            
        } else if(proposal.BI_PC_Status__c == PV_Status_AiP_BM_DC_Compliance) {
            
            //check if any role in this step has already process the scenarios
            if(!proposal.BI_PC_BM_processed__c) {
                groupDevNamesSet.add(PG_Brand_Marketing);
            }
            if(!proposal.BI_PC_DC_processed__c) {
                groupDevNamesSet.add(PG_DC_Compliance);
            }
        } else if(proposal.BI_PC_Status__c == PV_Status_AiP_DC_Compliance) {
            groupDevNamesSet.add(PG_DC_Compliance);
        } else if(proposal.BI_PC_Status__c == PV_Status_AiP_Gov_Compliance) {
            groupDevNamesSet.add(PG_Gov_Compliance);
        } else if(proposal.BI_PC_Status__c == PV_Status_AiP_Pricing_Contracting) {
            groupDevNamesSet.add(PG_Executive_Director);
        } else if(proposal.BI_PC_Status__c == PV_Status_AiP_Market_Access) {
            groupDevNamesSet.add(PG_VP_Market_Access);
        } else if(proposal.BI_PC_Status__c == PV_Status_AiP_MM_Sales) {
            
            //check the type of the proposal
            if(proposal.RecordType.DeveloperName == RT_Prop_Government) {
                groupDevNamesSet.add(PG_RAM_NAD);
            } else if(proposal.RecordType.DeveloperName == RT_Prop_Managed_Care) {
                groupDevNamesSet.add(PG_VP_MM_Sales);
            }         
        } else if(proposal.BI_PC_Status__c == PV_Status_AiP_PTC_Sc) {
            groupDevNamesSet.add(PG_PTC_Sc);
        } else if(proposal.BI_PC_Status__c == PV_Status_AiP_PTC) {
            groupDevNamesSet.add(PG_PTC);
        }
        System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.getGroupDeveloperNames>>>>>groupDevNamesSet: ' + groupDevNamesSet);
    
        return groupDevNamesSet;
    }
            
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Return to the proposal page layout
    * @return PageReference :   page reference with the proposal url
    ************************************************************************************************************/
    public PageReference returnToProposal() {
        PageReference pgRef = new PageReference('/' + ApexPages.currentPage().getParameters().get('id').substring(0,3));
        pgRef.setRedirect(true);
        System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.returnToProposal>>>>>URL: ' + pgRef.getUrl());
        
        return pgRef;
    }   
    
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Save approvals
    * @return PageReference :   page reference with the proposal url
    ************************************************************************************************************/
    public PageReference processSteps() {
        
        Savepoint sp = Database.setSavepoint(); //create savepoint

        try{
            List<BI_PC_Scenario__c> scToUpdateList = new List<BI_PC_Scenario__c>(); //List of the scenarios to update approval comments
            List<Approval.ProcessWorkitemRequest> allReq = new List<Approval.ProcessWorkitemRequest>(); //list of the approval steps to be processed            
            Map<Id, Id> pInstanceWorkItemScenarioIdMap = new map<Id, Id>(); //map of scenarios Ids and their approval steps            
            Set<Id> pIdsSet = (new Map<Id, ProcessInstance>([SELECT Id FROM ProcessInstance where Status = 'Pending' and TargetObjectId in :scenariosIdSet])).keySet(); //get process instance ids
            System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.processSteps>>>>>scenariosIdSet: ' + scenariosIdSet);
            System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.processSteps>>>>>pIdsSet: ' + pIdsSet);
            
            //get proposal data
            List<BI_PC_Proposal__c> proposalsList = fetchProposalData(ApexPages.currentPage().getParameters().get('id'));
            BI_PC_Proposal__c proposal = proposalsList[0];
            
            //Set<Id> actorIdsSet = getActorIds(proposal, scenariosWrpList[0].scenario.BI_PC_RAM_NAD__c); //get actor Ids
            Set<Id> actorIdsSet = getActorIds(proposal); //get actor Ids
            System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.processSteps>>>>>actorIdsSet: ' + actorIdsSet);
            
            Id queueId; //id of the queue related to the steps
            
            for(ProcessInstanceWorkitem piWitem : [SELECT Id, ProcessInstance.TargetObjectId, ActorId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIdsSet AND ActorId IN :actorIdsSet]) {
                pInstanceWorkItemScenarioIdMap.put(piWitem.ProcessInstance.TargetObjectId, piWitem.Id);
                queueId = piWitem.ActorId;
            }
            System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.processSteps>>>>>pInstanceWorkItemScenarioIdMap: ' + pInstanceWorkItemScenarioIdMap);
            
            //update the flag in proposal if the brand marketing or the director contract development is the first user working on the step
            Boolean updateProposal = false;
            for(GroupMember qMember : [SELECT Group.DeveloperName FROM GroupMember WHERE GroupId = :queueId]) {
                if(qMember.Group.DeveloperName == Q_Brand_Marketing && !proposal.BI_PC_DC_processed__c) {
                    proposal.BI_PC_BM_processed__c = true;
                    updateProposal = true;
                } else if((qMember.Group.DeveloperName == Q_DC_Compliance || qMember.Group.DeveloperName == Q_DC_Development) && !proposal.BI_PC_BM_processed__c) {
                    proposal.BI_PC_DC_processed__c = true;
                    updateProposal = true;
                }
            }        
            if(updateProposal) {
                update proposal;
            }
            System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.processSteps>>>>>proposal: ' + proposal);

            System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.processSteps>>>>>scenariosWrpList: ' + scenariosWrpList);
            
            //process the approval steps
            for(BI_PC_ScenariosApproveRejectWrp scenarioWrp : scenariosWrpList) {
                
                if(pInstanceWorkItemScenarioIdMap.containsKey(scenarioWrp.scenario.Id)) {

                    //instanciate step
                    Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                    req2.setComments(scenarioWrp.approvalComments); //store the comments
                    req2.setAction(scenarioWrp.actionSelected); //store the action
                    
                    //id of the step
                    req2.setWorkitemId(pInstanceWorkItemScenarioIdMap.get(scenarioWrp.scenario.Id));
                    
                    // Add the request for approval
                    allReq.add(req2);

                    //Add approval comments to the scenario
                    String scAppProccess = parseApprovalComments(scenarioWrp);
                    BI_PC_Scenario__c scToUpdate = new BI_PC_Scenario__c(Id = scenarioWrp.scenario.Id, BI_PC_Approval_comments__c = scAppProccess);
                    scToUpdateList.add(scToUpdate);
                }
            }
            Approval.process(allReq);

            if(!scToUpdateList.isEmpty()){
                update scToUpdateList;
            }
            
        } catch(Exception exp) {
            Database.rollback(sp);  //rollback if any error occurs
            setError('DML Error' + exp.getMessage());   //DML error
            return null;
        }
        
        return returnToProposal();
    }
     
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Get actor Ids
    * @param BI_PC_Proposal__c proposal :   proposal data
    * @param Id ramnadId    : RAM NAD Id
    * @return Set<Id>   :   Set with Ids of the actors that can approve the proposal in the current status
    ************************************************************************************************************/
    //private static Set<Id> getActorIds(BI_PC_Proposal__c proposal, Id ramnadId) {
    private static Set<Id> getActorIds(BI_PC_Proposal__c proposal) {
        
        Set<Id> actorIdsSet = new Set<Id>();    //set of actor ids
        
        Set<String> groupDevNamesSet = getGroupDeveloperNames(proposal);    //get the groups
        //groupDevNamesSet.remove(PG_Admin);  //remove the admin group
        System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.getActorIds>>>>>groupDevNamesSet: ' + groupDevNamesSet);
        
        if(!groupDevNamesSet.isEmpty()) {
            
            /*
            if(groupDevNamesSet.contains(PG_RAM_NAD)) {
                
                //if the user is a ram nad we need to get the user id
                actorIdsSet.add(ramnadId);       
            } else {
             */   
            Set<Id> groupIdSet = new Set<Id>(); //set of group ids
            
            //we need to get the id of the queue related to the user
            for(GroupMember gMember : [SELECT Id, GroupId FROM GroupMember WHERE Group.Type != 'Queue' AND Group.DeveloperName IN :groupDevNamesSet AND UserOrGroupId = :UserInfo.getUserId()]) {
                groupIdSet.add(gMember.GroupId);
            }
            System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosApproveRejectExt.getActorIds>>>>>groupIdSet: ' + groupIdSet);
            for(GroupMember qMember : [SELECT Id, GroupId FROM GroupMember WHERE Group.Type = 'Queue' AND UserOrGroupId IN :groupIdSet]) {
                actorIdsSet.add(qMember.GroupId);
            }
            //}
        }
        
        return actorIdsSet;
    }

    /***********************************************************************************************************
    * @date 02/08/2018 (dd/mm/yyyy)
    * @description Get actor Ids
    * @param BI_PC_ScenariosApproveRejectWrp scWrapper :   scenarioInfo
    * @return String   :   parseApprovalComments
    ************************************************************************************************************/
    private String parseApprovalComments(BI_PC_ScenariosApproveRejectWrp scWrapper){

        BI_PC_Scenario__c scenario = scWrapper.scenario;
        String aprvComments = '';
        String parsedAppComments = '';
        String action = (scWrapper.actionSelected == 'Approve')?Label.BI_PC_Approved:( (scWrapper.actionSelected == 'Reject')?Label.BI_PC_Rejected: '');

        aprvComments = String.format(Label.BI_PC_App_comments_scenario, new String[]{action, UserInfo.getName(), scWrapper.approvalComments});
        

        if(!String.isBlank(scenario.BI_PC_Approval_comments__c) && !String.isBlank(aprvComments)){
            String existingComments = scenario.BI_PC_Approval_comments__c; //get existing comments
        
            Integer totalLength = existingComments.length() + aprvComments.length(); //calculate total length
                
            if(totalLength < 32768) {
                parsedAppComments = existingComments + '\n' + aprvComments;
            }

        }else {
            
            if(aprvComments.length() < 32768) {
                parsedAppComments = aprvComments;
            }

        }
        
        return parsedAppComments;

    }
      
      
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Set error message
    * @param String errorMsg    :   Error message
    * @return void
    ************************************************************************************************************/
    private static void setError(String errorMsg) {
        error = true;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
        System.debug(LoggingLevel.ERROR, 'BI_PC_ScenariosApproveRejectExt.setError>>>>>errorMsg: ' + errorMsg);
    }
    
    /***********************************************************************************************************
    * @date 24/07/2018 (dd/mm/yyyy)
    * @description Wrapper class to store scenario and the approve or reject action selected
    ************************************************************************************************************/
    public class BI_PC_ScenariosApproveRejectWrp {
        public BI_PC_Scenario__c scenario {get; set;}
        public String actionSelected {get; set;}
        public List<SelectOption> actions {get; set;}
        public String approvalComments {get; set;}
        
         
        /***********************************************************************************************************
        * @date 24/07/2018 (dd/mm/yyyy)
        * @description Constructor of the BI_PC_ScenariosApproveRejectWrp class
        ************************************************************************************************************/
        public BI_PC_ScenariosApproveRejectWrp(BI_PC_Scenario__c scenario) {
            this.scenario = scenario;
            this.actionSelected = null;
            this.actions = new List<SelectOption>();
            this.actions.add(new SelectOption('Approve', Label.BI_PC_Approve));
            this.actions.add(new SelectOption('Reject', Label.BI_PC_Reject));
            this.approvalComments = '';
        }
    }
}