/***********************************************************************************************************
* @date 11/07/2018 (dd/mm/yyyy)
* @description Controller for the BI_PC_ProposalExportData VF page
************************************************************************************************************/
public class BI_PC_ProposalExportDataCtrl {

    public List<BI_PC_Proposal__c> proposalsList {get; set;}
    public List<BI_PC_Proposal_Product__c> proposalProductsList {get; set;}
    public List<BI_PC_Scenario_Wrp> scenariosList {get; set;}
    public List<BI_PC_Answer__c> answersList {get; set;}
    public Boolean isGovernment {get;set;}
    
    /***********************************************************************************************************
    * @date 11/07/2018 (dd/mm/yyyy)
    * @description Constructor method
    ************************************************************************************************************/
    public BI_PC_ProposalExportDataCtrl() {
        
        Id proposalId = ApexPages.currentPage().getParameters().get('id');
        
        //get proposal data
        proposalsList = fetchProposals(proposalId);
        
        //get proposal products data
        proposalProductsList = fetchproposalProducts(proposalId);
        
        //get scenarios data
        scenariosList = fetchScenarios(proposalId);
        
        //get answers data
        answersList = fetchAnswers(proposalId);
    }    
    
    /***********************************************************************************************************
    * @date 11/07/2018 (dd/mm/yyyy)
    * @description Method to get the proposals data
    * @return BI_PC_Proposal__c
    ************************************************************************************************************/
    private List<BI_PC_Proposal__c> fetchProposals(Id proposalId) {
        
        List<BI_PC_Proposal__c> propList = new List<BI_PC_Proposal__c>([SELECT Name, RecordTypeId, RecordType.DeveloperName, BI_PC_Account__c, BI_PC_Account_description__c, BI_PC_Adjustment__c, 
                                                                            BI_PC_Admin_fee__c, BI_PC_Calculation_method__c, BI_PC_Channel__c, BI_PC_Start_date__c, 
                                                                            BI_PC_End_date__c, BI_PC_Proposal_duration__c, BI_PC_Due_date__c, BI_PC_Priority_level__c, 
                                                                            BI_PC_Reason_for_expedited_bid__c, BI_PC_RX_Source__c, BI_PC_RX_lives__c, BI_PC_Tier_1__c,
                                                                            BI_PC_Tier_2__c, BI_PC_Tier_3__c, BI_PC_Tier_4__c, BI_PC_Tier_5__c, BI_PC_Tier_6__c, 
                                                                            BI_PC_Account__r.BI_PC_Group__c, CreatedById
                                                                        FROM BI_PC_Proposal__c WHERE Id = :proposalId]);

        if(!propList.isEmpty()){
            isGovernment = (propList.get(0).RecordType.DeveloperName == 'BI_PC_Prop_government');
        }

        return propList;
    }
    
    /***********************************************************************************************************
    * @date 11/07/2018 (dd/mm/yyyy)
    * @description Method to get proposal products data
    * @return List<BI_PC_Proposal_Product__c>
    ************************************************************************************************************/
    private List<BI_PC_Proposal_Product__c> fetchProposalProducts(Id proposalId) {
        
        return [SELECT RecordTypeId, BI_PC_Max_discount__c, BI_PC_Product__c, 
                    BI_PC_Price_prot__c, BI_PC_Product_description__c, BI_PC_Terms__c
                FROM BI_PC_Proposal_Product__c WHERE BI_PC_Proposal__c = :proposalId];
        
    }
    
    /***********************************************************************************************************
    * @date 11/07/2018 (dd/mm/yyyy)
    * @description Method to get scenarios data
    * @return List<BI_PC_Scenario__c>
    ************************************************************************************************************/
    private List<BI_PC_Scenario_Wrp> fetchScenarios(Id proposalId) {
        
        List<BI_PC_Scenario_Wrp> scenariosWrp = new List<BI_PC_Scenario_Wrp>();
        
        for(BI_PC_Scenario__c scenario : [SELECT RecordTypeId, BI_PC_Comment__c, BI_PC_Force_Out_of_GL__c, BI_PC_Max_discount__c, 
                                                BI_PC_Out_of_GL__c, BI_PC_Guide_line__c, BI_PC_Position__c, BI_PC_Price_prot__c, 
                                                BI_PC_Product_Description__c, BI_PC_Rate__c, BI_PC_Prop_product__r.BI_PC_Product__c, 
                                                BI_PC_Reset_terms__c, BI_PC_Guideline_quarter__c 
                                          FROM BI_PC_Scenario__c WHERE BI_PC_Proposal__c = :proposalId]) {
                scenariosWrp.add(new BI_PC_Scenario_Wrp(scenario, scenario.BI_PC_Out_of_GL__c, scenario.BI_PC_Force_Out_of_GL__c)); 
        }
        
        return scenariosWrp;
        
    }
    
    /***********************************************************************************************************
    * @date 11/07/2018 (dd/mm/yyyy)
    * @description Method to get answers data
    * @return List<BI_PC_Answer__c>
    ************************************************************************************************************/
    private List<BI_PC_Answer__c> fetchAnswers(Id proposalId) {
        
        return [SELECT BI_PC_Answer__c, BI_PC_Proposal_product__r.BI_PC_Product__c, BI_PC_Question_body__c,
                BI_PC_Proposal_product__r.BI_PC_Product_description__c, BI_PC_Question_type__c
                FROM BI_PC_Answer__c WHERE BI_PC_Proposal__c = :proposalId];
        
    }
        
    /***********************************************************************************************************
    * @date 11/07/2018 (dd/mm/yyyy)
    * @description Wrapper class to display Scenarios
    ************************************************************************************************************/
    public class BI_PC_Scenario_Wrp {
        
        public BI_PC_Scenario__c BI_PC_Scenario {get; set;}
        public String BI_PC_Out_of_GL {get; set;}
        public String BI_PC_Force_Out_of_GL {get; set;}
        
        public BI_PC_Scenario_Wrp(BI_PC_Scenario__c BI_PC_Scenario, String BI_PC_Out_of_GL, Boolean BI_PC_Force_Out_of_GL) {
            this.BI_PC_Scenario = BI_PC_Scenario;
            this.BI_PC_Out_of_GL = String.ValueOf(BI_PC_Scenario.BI_PC_Rate__c <= BI_PC_Scenario.BI_PC_Guide_line__c);
            this.BI_PC_Force_Out_of_GL = String.ValueOf(BI_PC_Force_Out_of_GL);
        }
    }
    
}