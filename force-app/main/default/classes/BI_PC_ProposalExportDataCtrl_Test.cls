/****************************************************************************************************
* @date 12/07/2018 (dd/mm/yyyy) 
* @description This is the test class for BI_PC_ProposalExportDataCtrl Apex Class
****************************************************************************************************/
@isTest
public class BI_PC_ProposalExportDataCtrl_Test {

    /***********************************************************************************************************
	* @date 12/07/2018 (dd/mm/yyyy)
	* @description Test method for the export data funtionality
	************************************************************************************************************/
    public static testMethod void testRedirectToExportData() {
        
        //create analyst
    	User analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
        
        //create account
        Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
        BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test Commercial Account', analyst, false);
        acc.BI_PC_Is_pool__c = FALSE;
        insert acc;

		//create proposal
        Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');
        BI_PC_Proposal__c proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40), false);
        proposal.BI_PC_Adjustment__c = 23;
        insert proposal;
                
		//create product
        Id commercialProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prod_m_care', 'BI_PC_Product__c');
        BI_PC_Product__c product = BI_PC_TestMethodsUtility.getPCProduct(commercialProdId, 'Test Product', false);
        insert product;
        
		//create guideline rate
        Id commercialGuidelineRateId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_GLR_m_care', 'BI_PC_Guide_line_rate__c');
        BI_PC_Guide_line_rate__c guidelineRate = BI_PC_TestMethodsUtility.getPCGuidelineRate(commercialGuidelineRateId, product.Id, false);
        insert guidelineRate;
        
		//create proposal product
        Id currentPropProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Current_position', 'BI_PC_Proposal_Product__c');
        BI_PC_Proposal_Product__c propProduct = BI_PC_TestMethodsUtility.getPCProposalProduct(currentPropProdId, proposal.Id, product.Id, false);
        insert propProduct;
        
		//create proposal product
        Id currentScenarioId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Current', 'BI_PC_Scenario__c');
        BI_PC_Scenario__c scenario = BI_PC_TestMethodsUtility.getPCScenario(currentScenarioId, proposal.Id, propProduct.Id, guidelineRate.Id, false);
        insert scenario;
        
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.BI_PC_ProposalExportData')); 
        System.currentPageReference().getParameters().put('id', proposal.Id);   
        BI_PC_ProposalExportDataCtrl ctrl = new BI_PC_ProposalExportDataCtrl();
                
        Test.stopTest();
        
        System.assert(!ctrl.proposalsList.isEmpty());
        System.assert(!ctrl.proposalProductsList.isEmpty());
        System.assert(!ctrl.scenariosList.isEmpty());
        
    }
}