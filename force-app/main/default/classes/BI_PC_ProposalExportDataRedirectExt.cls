/***********************************************************************************************************
* @date 11/07/2018 (dd/mm/yyyy)
* @description Extension for the BI_PC_ProposalExportDataRedirect VF page
************************************************************************************************************/
public with sharing class BI_PC_ProposalExportDataRedirectExt {
    
	private Id proposalId;    
    public static final String CP_Export_Data = 'BI_PC_Export_proposal_data';
    
    /***********************************************************************************************************
	* @date 11/07/2018 (dd/mm/yyyy)
	* @description Constructor method
	************************************************************************************************************/
    public BI_PC_ProposalExportDataRedirectExt(ApexPages.StandardController ctrl) {
		proposalId = ctrl.getId();        
    }
    
    /***********************************************************************************************************
	* @date 11/07/2018 (dd/mm/yyyy)
	* @description This method redirect the user to the export proposal data report
	* @return pageRef
	************************************************************************************************************/
    public PageReference redirectToExportDataReport() {
                
        if(FeatureManagement.checkPermission(CP_Export_Data) && proposalId != null) {
            
            //redirect to the report page
            PageReference pageRef = new PageReference('/apex/BI_PC_ProposalExportData');
            pageRef.getParameters().put('id', proposalId);
            pageRef.setRedirect(true);
            return pageRef;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.BI_PC_Export_data_error_msg));
            return null;
        }
    }
}