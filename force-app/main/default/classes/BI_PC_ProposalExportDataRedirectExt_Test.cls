/****************************************************************************************************
* @date 11/07/2018 (dd/mm/yyyy) 
* @description This is the test class for BI_PC_ProposalExportDataRedirectExt Apex Class
****************************************************************************************************/
@isTest
public class BI_PC_ProposalExportDataRedirectExt_Test {

    private static PageReference exportDataPR;
    
    /***********************************************************************************************************
	* @date 11/07/2018 (dd/mm/yyyy)
	* @description Test method for the redirect to export data page funtionality
	************************************************************************************************************/
    public static testMethod void testRedirectToExportData() {
        
        //create analyst
    	User analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_admin', 0);
        
        BI_PC_Proposal__c proposal;
        
        System.runAs(analyst) {  
            
            //create account
            Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
            BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test Commercial Account', analyst, FALSE);
            acc.BI_PC_Is_pool__c = FALSE;
            insert acc;
            
            //create proposal
            Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');
            proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40), FALSE);
            proposal.BI_PC_Adjustment__c = 23;
            insert proposal;
            
            Test.startTest();
            
            BI_PC_ProposalExportDataRedirectExt ctrl = new BI_PC_ProposalExportDataRedirectExt(new ApexPages.StandardController(proposal));
            exportDataPR = ctrl.redirectToExportDataReport();
            
            Test.stopTest();
        }
        
        PageReference expectedPR = new PageReference('/apex/BI_PC_ProposalExportData');
        expectedPR.getParameters().put('id', proposal.Id);
        System.assertEquals(expectedPR.getUrl(), exportDataPR.getUrl());
    }
    
    /***********************************************************************************************************
	* @date 11/07/2018 (dd/mm/yyyy)
	* @description Test method for the redirect to export data page functionality without permissions
	************************************************************************************************************/
    public static testMethod void testRedirectToExportDataNoPrivileges() {
        
        //create analyst
    	User analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
        
        System.runAs(analyst) {  
            
            //create account
            Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
            BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test Commercial Account', analyst, FALSE);
            acc.BI_PC_Is_pool__c = FALSE;
            insert acc;
            
            //create proposal
            Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');
            BI_PC_Proposal__c proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40), FALSE);
            proposal.BI_PC_Adjustment__c = 23;
            insert proposal;
            
            Test.startTest();            
            
            BI_PC_ProposalExportDataRedirectExt ctrl = new BI_PC_ProposalExportDataRedirectExt(new ApexPages.StandardController(proposal));
            exportDataPR = ctrl.redirectToExportDataReport();
            
            Test.stopTest();
        }
        
        System.assertEquals(null, exportDataPR);
    }
}