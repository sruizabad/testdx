/***********************************************************************************************************
* @date 03/08/2018 (dd/mm/yyyy)
* @description Controller for the BI_PC_ProposalNotifyApproversComp VF component
************************************************************************************************************/
public class BI_PC_ProposalNotifyApproversCtrl {

    public static String urlString {get; set;}
    
    public BI_PC_ProposalNotifyApproversCtrl() {
        
        urlString = '' + URL.getSalesforceBaseUrl().toExternalForm();
    }
}