/***********************************************************************************************************
* @date 13/07/2018 (dd/mm/yyyy)
* @description Extension class for the BI_PC_ProposalSubmitApproval visualforce page
************************************************************************************************************/
public class BI_PC_ProposalSubmitApprovalExt {

    private static Boolean isRae;
    private static Boolean isAnalyst;
    private static BI_PC_Proposal__c proposal;
    
    //Constants
    public static final String RT_Prop_Government = 'BI_PC_Prop_government';
    public static final String RT_Prop_Managed_Care = 'BI_PC_Prop_m_care';
    public static final String CP_Submit_RAE = 'BI_PC_Submit_proposal_by_rae';
    public static final String CP_Submit_Analyst = 'BI_PC_Submit_proposal_by_analyst';
    public static final String PV_Group_3_Account = 'Group 3';
    public static final String PV_Status_Prop_Development = 'Proposal in Development';
    public static final String PV_Status_BC_Development = 'Business Case in Development';
    public static final String PV_Status_AiP_DC_Development = 'Approval in Progress: Dir Contract Development';
    public static final String PV_Status_AiP_BM_DC_Development = 'Approval in Progress: Brand Marketing - Dir Contract Development';
    public static final String PV_Status_AiP_Gov_Compliance = 'Approval in Progress: Government Compliance';
    public static final String FLOW_Prod_Answers_Step = 'Step4';
    public static final String FLOW_RAE_Step = 'Step5';
    public static final String FLOW_ANALYST_Step = 'Step6';
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Constructor method
	************************************************************************************************************/
    public BI_PC_ProposalSubmitApprovalExt(ApexPages.StandardController ctrl) {
        
        Id proposalId = ctrl.getId();	//get the proposal id
        
        proposal = fetchProposal(proposalId);	//get the proposal data
        
        isRae = FeatureManagement.checkPermission(CP_Submit_RAE) 
            	&& proposal.OwnerId == UserInfo.getUserId() 
            	&& proposal.BI_PC_Status__c == PV_Status_Prop_Development;	//check if the user is the owner RAE and the proposal is in the correct status
        isAnalyst = FeatureManagement.checkPermission(CP_Submit_Analyst) && proposal.BI_PC_Status__c == PV_Status_BC_Development;	//check if the user is analyst and the proposal is in the correct status
        
        System.debug(LoggingLevel.DEBUG, 'BI_PC_ProposalSubmitApprovalExt>>>>> Proposal Status: ' + proposal.BI_PC_Status__c);
        System.debug(LoggingLevel.DEBUG, 'BI_PC_ProposalSubmitApprovalExt>>>>> Proposal Owner Id: ' + proposal.OwnerId);
    }
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Check what action to execute depending on the role
	* @param Boolean isRae	:	is Rae flag
	* @param Boolean isAnalyst	:	is Analyst flag
	************************************************************************************************************/
    public static void executeAction() {
        
        if(isAnalyst) {
            submitByAnalyst();	//execute Analyst actions
        } else if(isRae) {
            if(proposal.BI_PC_Flow_path__c == FLOW_Prod_Answers_Step) {
                submitByRAE();	//execute RAE actions
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.BI_PC_Complete_Steps_error_msg));	//complete steps first error
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.BI_PC_Submit_error_msg));	//no permissions error
        }
    }
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description submit for approval the scenarios related to the proposal
	************************************************************************************************************/
    private static void submitByAnalyst() {
        
        Savepoint sp = Database.setSavepoint();	//create savepoint

        try{            
            List<BI_PC_Scenario__c> scenariosList = new List<BI_PC_Scenario__c>();	//list of scenarios to submit
                        
            String status = '';
            
            if(proposal.RecordType.DeveloperName == RT_Prop_Government) {
                status = PV_Status_AiP_Gov_Compliance;
            } else if(proposal.RecordType.DeveloperName == RT_Prop_Managed_Care) {
                status = PV_Status_AiP_BM_DC_Development;
            }
            System.debug(LoggingLevel.DEBUG, 'BI_PC_ProposalSubmitApprovalExt.submitByAnalyst>>>>> New Proposal Status: ' + status);
            
            String proposalStatus = status;	// status to set to the proposal
            
            for(BI_PC_Scenario__c scenario : fetchScenarios()) {                
                
                String scenarioStatus = status;
                
                if(proposal.RecordType.DeveloperName == RT_Prop_Managed_Care && proposal.BI_PC_Account__r.BI_PC_Group__c == PV_Group_3_Account && scenario.BI_PC_Rate__c > scenario.BI_PC_Guide_line__c) {
                    scenarioStatus = PV_Status_AiP_DC_Development;
                    proposalStatus = PV_Status_AiP_DC_Development;
                } 
                System.debug(LoggingLevel.DEBUG, 'BI_PC_ProposalSubmitApprovalExt.submitByAnalyst>>>>> New Scenario Status: ' + scenarioStatus);
                
                //Update scenarios status
                scenario.BI_PC_Approval_status__c = scenarioStatus;
                
                scenariosList.add(scenario);
            }
            
            update scenariosList;
            
            proposal.BI_PC_Status__c = proposalStatus;
            proposal.BI_PC_Flow_path__c = FLOW_ANALYST_Step;
            update proposal;
            
            for(BI_PC_Scenario__c scenario : scenariosList) {
                
                // Create an approval request for the scenario
                Approval.ProcessSubmitRequest aprvReq = new Approval.ProcessSubmitRequest();
                aprvReq.setObjectId(scenario.id);
                
                // Submit on behalf of a specific submitter
                aprvReq.setSubmitterId(UserInfo.getUserId()); 
                
                // Submit the approval request for the scenario
                Approval.ProcessResult result = Approval.process(aprvReq);
            }
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.BI_PC_Submit_succeed_msg));	//submission succeed
            
        } catch(Exception exp) {
            Database.rollback(sp);	//rollback if any error occurs
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.BI_PC_Proposal_update_error_msg + exp.getMessage()));	//error
        }        
    }
    
    /***********************************************************************************************************
	* @date 18/07/2018 (dd/mm/yyyy)
	* @description Get the proposal data
	* @param Id proposalId	:	Id of the proposal
	* @return BI_PC_Proposal__c	:	proposal data
	************************************************************************************************************/
    private static BI_PC_Proposal__c fetchProposal(Id proposalId) {
        return [SELECT Id, OwnerId, BI_PC_Status__c, BI_PC_Account__c, RecordType.DeveloperName, 
                	BI_PC_Account__r.BI_PC_Group__c, BI_PC_Flow_path__c, (SELECT Id FROM Proposal_Products__r) 
                FROM BI_PC_Proposal__c 
                WHERE Id = :proposalId LIMIT 1];
    }  
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Get ids of the scenarios related to the proposal
	* @return List<BI_PC_Scenario__c>	:	list of scenarios related to the proposal
	************************************************************************************************************/
    private static List<BI_PC_Scenario__c> fetchScenarios() {
        return [SELECT Id, BI_PC_Rate__c, BI_PC_Guide_line__c 
                FROM BI_PC_Scenario__c 
                WHERE BI_PC_Proposal__c = : proposal.Id
                	AND RecordType.DeveloperName = 'BI_PC_Future'];
    }    
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Look for the analyst and updates the proposal
	************************************************************************************************************/
    private static void submitByRAE() {
        
        List<BI_PC_Account__c> accList = [SELECT BI_PC_Contract_analyst__c, BI_PC_Contract_analyst__r.IsActive FROM BI_PC_Account__c WHERE Id = :proposal.BI_PC_Account__c];	//get analyst
        
        if(accList != null && !accList.isEmpty() && accList[0].BI_PC_Contract_analyst__c != null && accList[0].BI_PC_Contract_analyst__r.IsActive) {
            
            if(scenariosRelatedToProposal()) {
                assignProposalToAnalyst(accList[0].BI_PC_Contract_analyst__c);
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.BI_PC_No_scenarios_error_msg));	//no proposal products or scenarios error
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.BI_PC_No_analyst_error_msg));	//no analyst error
        }
    }    
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Check if there are products and scenarios related to the proposal
	* @return Boolean	:	true if there are proposal products and scenarios related to the proposal
	************************************************************************************************************/
    private static Boolean scenariosRelatedToProposal() {
        
        Integer proposalProducts = [SELECT COUNT() FROM BI_PC_Proposal_Product__c WHERE BI_PC_Proposal__c = : proposal.Id];	//get the number of proposal products related
        Integer scenarios = [SELECT COUNT() FROM BI_PC_Scenario__c WHERE BI_PC_Proposal__c = : proposal.Id];	//get the number of scenarios related
        
        return ((proposalProducts > 0) && (scenarios > 0));
    }
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Execute the actions to assign the proposal to the analyst
	* @param Id analystId	:	Id of the analyst
	************************************************************************************************************/
    private static void assignProposalToAnalyst(Id analystId) {
        
        Savepoint sp = Database.setSavepoint();	//create savepoint

        try {
            Id originalOwnerId = proposal.OwnerId;	//store the original Owner to give read privileges
            
            updateProposal(analystId);	//update proposal
            
            updateProposalProducts(analystId);	//update proposal products
            
            shareProposalWithRAE(originalOwnerId);	//give read privileges to the RAE
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.BI_PC_Submit_succeed_msg));	//submission succeed
            
        } catch(Exception exp) {
            Database.rollback(sp);	//rollback if any error occurs
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.BI_PC_Proposal_update_error_msg + exp.getMessage()));	//error
        }
    }
    
    /***********************************************************************************************************
	* @date 18/07/2018 (dd/mm/yyyy)
	* @description Update the status and the owner of the proposal
	* @param Id analystId	:	Id of the analyst
	************************************************************************************************************/
    private static void updateProposal(Id analystId) {
        
        proposal.OwnerId = analystId;
        proposal.BI_PC_Status__c = PV_Status_BC_Development;
        proposal.BI_PC_Flow_path__c = FLOW_RAE_Step;
        update proposal;
        System.debug(LoggingLevel.DEBUG, 'BI_PC_ProposalSubmitApprovalExt.updateProposal>>>>> Updated Proposal: ' + proposal);
        
    }
    
    /***********************************************************************************************************
	* @date 18/07/2018 (dd/mm/yyyy)
	* @description Update the owner of the proposal products
	* @param Id analystId	:	Id of the analyst
	************************************************************************************************************/
    private static void updateProposalProducts(Id analystId) {
        
        List<BI_PC_Proposal_Product__c> propProdList = new List<BI_PC_Proposal_Product__c>();
        for(BI_PC_Proposal_Product__c propProd : proposal.Proposal_Products__r) {
            propProd.OwnerId = analystId;
            propProdList.add(propProd);
        }
        update propProdList;
        System.debug(LoggingLevel.DEBUG, 'BI_PC_ProposalSubmitApprovalExt.updateProposalProducts>>>>> Updated Proposal products: ' + propProdList);
        
    }
    
    /***********************************************************************************************************
	* @date 18/07/2018 (dd/mm/yyyy)
	* @description Give read access to the RAE
	* @param Id originalOwnerId	:	Id of the rae
	************************************************************************************************************/
    private static void shareProposalWithRAE(Id originalOwnerId) {
        
        Integer existingShare = [SELECT COUNT() FROM BI_PC_Proposal__Share WHERE ParentId = :proposal.Id AND UserOrGroupId = :originalOwnerId];	//check no share on the record exists for the rae
        
        if(existingShare == 0) {
            BI_PC_Proposal__Share proposalShare = new BI_PC_Proposal__Share();
            proposalShare.ParentId = proposal.Id;
            proposalShare.UserOrGroupId = originalOwnerId;
            proposalShare.AccessLevel = 'Read'; 
            proposalShare.RowCause = Schema.BI_PC_Proposal__Share.RowCause.BI_PC_Is_the_proposal_rae__c;
            insert proposalShare;
            System.debug(LoggingLevel.DEBUG, 'BI_PC_ProposalSubmitApprovalExt.shareProposalWithRAE>>>>> Proposal Share: ' + proposalShare);
        }
        
    }
}