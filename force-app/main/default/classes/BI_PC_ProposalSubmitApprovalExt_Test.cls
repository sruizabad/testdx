/***********************************************************************************************************
* @date 13/07/2018 (dd/mm/yyyy)
* @description Text class for the BI_PC_ProposalSubmitApprovalExt class
************************************************************************************************************/
@isTest
public class BI_PC_ProposalSubmitApprovalExt_Test {

    private static Id userId;
    private static User analyst;
	private static BI_PC_Scenario__c mcScenario;
	private static BI_PC_Proposal__c mcProposal;
	private static BI_PC_Proposal__c govProposal;
    private static BI_PC_Account__c mcAcc;
    private static BI_PC_Account__c govAcc;
    
	/******************************************************************************************************************
    * @date             12/06/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
	private static void dataSetUp() {
        
        userId = UserInfo.getUserId();
		        
        System.runAs(analyst) {  
            
            Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
            Id accGovernmentId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_government', 'BI_PC_Account__c');
            mcAcc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test Managed Care Account', new User(Id = userId), TRUE);
            govAcc = BI_PC_TestMethodsUtility.getPCAccount(accGovernmentId, 'Test Government Account', new User(Id = userId), TRUE);
            
            Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');
            Id governmentPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_government', 'BI_PC_Proposal__c');
            mcProposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, mcAcc.Id, BI_PC_ProposalSubmitApprovalExt.PV_Status_Prop_Development, Date.today().addDays(30), Date.today().addDays(40) , FALSE);
            mcProposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
            govProposal = BI_PC_TestMethodsUtility.getPCProposal(governmentPropId, govAcc.Id, BI_PC_ProposalSubmitApprovalExt.PV_Status_Prop_Development, Date.today().addDays(30), Date.today().addDays(40) , FALSE);
            govProposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
            insert new List<BI_PC_Proposal__c>{mcProposal, govProposal};  
            
            mcProposal.OwnerId = UserInfo.getUserId();
            govProposal.OwnerId = UserInfo.getUserId();
            update new List<BI_PC_Proposal__c>{mcProposal, govProposal};
        }
    }
    
    /******************************************************************************************************************
    * @date             12/06/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
    private static void dataSetUp2() {
        
        System.runAs(analyst) {  
            
            //create product
            Id commercialProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prod_m_care', 'BI_PC_Product__c');
            BI_PC_Product__c product = BI_PC_TestMethodsUtility.getPCProduct(commercialProdId, 'Test Product', false);
            insert product;
            
            //create guideline rate
            Id commercialGuidelineRateId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_GLR_m_care', 'BI_PC_Guide_line_rate__c');
            BI_PC_Guide_line_rate__c guidelineRate = BI_PC_TestMethodsUtility.getPCGuidelineRate(commercialGuidelineRateId, product.Id, false);
            insert guidelineRate;
            
            //create proposal product
            Id currentPropProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Proposed', 'BI_PC_Proposal_Product__c');
            BI_PC_Proposal_Product__c mcPropProduct = BI_PC_TestMethodsUtility.getPCProposalProduct(currentPropProdId, mcProposal.Id, product.Id, false);
            BI_PC_Proposal_Product__c govPropProduct = BI_PC_TestMethodsUtility.getPCProposalProduct(currentPropProdId, govProposal.Id, product.Id, false);
            insert new List<BI_PC_Proposal_Product__c>{mcPropProduct, govPropProduct};
            
            //create scenarios
            Id currentScenarioId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Future', 'BI_PC_Scenario__c');
            mcScenario = BI_PC_TestMethodsUtility.getPCScenario(currentScenarioId, mcProposal.Id, mcPropProduct.Id, guidelineRate.Id, false);
            mcScenario.BI_PC_Rate__c = 100;
            BI_PC_Scenario__c govScenario = BI_PC_TestMethodsUtility.getPCScenario(currentScenarioId, govProposal.Id, govPropProduct.Id, guidelineRate.Id, false);
            govScenario.BI_PC_Rate__c = 100;
            insert new List<BI_PC_Scenario__c>{mcScenario, govScenario};
        }
    }
        
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Test Rae submission
	************************************************************************************************************/
    public static testMethod void submitByRAE_Test() {
             
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_RAE_MC', 0);
        
        //create data
        dataSetUp();
        dataSetUp2();
        
        Test.startTest();
        
        System.runAs(analyst) {
            
            mcProposal.BI_PC_Flow_path__c = BI_PC_ProposalSubmitApprovalExt.FLOW_Prod_Answers_Step;	//set the correct path step
            update mcProposal;
            
            BI_PC_ProposalSubmitApprovalExt ctrl = new BI_PC_ProposalSubmitApprovalExt(new ApexPages.StandardController(mcProposal));
            BI_PC_ProposalSubmitApprovalExt.executeAction();
        }
        
        Test.StopTest();
        
        List<BI_PC_Proposal__c> proposalsList = [SELECT OwnerId FROM BI_PC_Proposal__c WHERE Id = :mcProposal.Id];	//get submitted proposal
        
        System.assert(!proposalsList.isEmpty());
        System.assertEquals(userId, proposalsList[0].OwnerId);
    }
          
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Test Rae submission without completing previous steps
	************************************************************************************************************/
    public static testMethod void submitByRAECompleteSteps_Test() {
             
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_RAE_MC', 0);
        
        //create data
        dataSetUp();
        dataSetUp2();
        
        Test.startTest();
        
        System.runAs(analyst) {
            
            BI_PC_ProposalSubmitApprovalExt ctrl = new BI_PC_ProposalSubmitApprovalExt(new ApexPages.StandardController(mcProposal));
            BI_PC_ProposalSubmitApprovalExt.executeAction();
        }
        
        Test.StopTest();
        
        List<BI_PC_Proposal__c> proposalsList = [SELECT OwnerId FROM BI_PC_Proposal__c WHERE Id = :mcProposal.Id];	//get submitted proposal
        
        System.assert(!proposalsList.isEmpty());
        System.assertEquals(analyst.Id, proposalsList[0].OwnerId);
    }
        
    /***********************************************************************************************************
	* @date 16/07/2018 (dd/mm/yyyy)
	* @description Test user with no permissions submission
	************************************************************************************************************/
    public static testMethod void submitByUserNoPermissions_Test() {
              
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
        
        //create data
        dataSetUp();
        dataSetUp2();
        
        Test.startTest();
        
        System.runAs(analyst) {
            BI_PC_ProposalSubmitApprovalExt ctrl = new BI_PC_ProposalSubmitApprovalExt(new ApexPages.StandardController(mcProposal));        
            BI_PC_ProposalSubmitApprovalExt.executeAction();
        }
        
        Test.StopTest();
        
        List<BI_PC_Proposal__c> proposalsList = [SELECT OwnerId FROM BI_PC_Proposal__c WHERE Id = :mcProposal.Id];	//get submitted proposal
        
        System.assert(!proposalsList.isEmpty());
        System.assertEquals(analyst.Id, proposalsList[0].OwnerId);
    }
    
    /***********************************************************************************************************
	* @date 16/07/2018 (dd/mm/yyyy)
	* @description Test rae submission with no active analyst
	************************************************************************************************************/
    public static testMethod void submitByRAENoAnalystError_Test() {
              
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_RAE_MC', 0);
        
        //create data
        dataSetUp();
        dataSetUp2();
        
        Test.startTest();
        
        System.runAs(analyst) {
            
            mcProposal.BI_PC_Flow_path__c = BI_PC_ProposalSubmitApprovalExt.FLOW_Prod_Answers_Step;	//set the correct path step
            update mcProposal;
            
            mcAcc.BI_PC_Contract_analyst__c = null;
            update mcAcc;
            
            BI_PC_ProposalSubmitApprovalExt ctrl = new BI_PC_ProposalSubmitApprovalExt(new ApexPages.StandardController(mcProposal));   
            BI_PC_ProposalSubmitApprovalExt.executeAction();     
        }
        
        Test.StopTest();
        
        List<BI_PC_Proposal__c> proposalsList = [SELECT OwnerId FROM BI_PC_Proposal__c WHERE Id = :mcProposal.Id];	//get submitted proposal
        
        System.assert(!proposalsList.isEmpty());
        System.assertEquals(analyst.Id, proposalsList[0].OwnerId);
    }
    
    /***********************************************************************************************************
	* @date 16/07/2018 (dd/mm/yyyy)
	* @description Test RAE submission with no scenarios error
	************************************************************************************************************/
    public static testMethod void submitByRAENoScenariosError_Test() {
              
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_RAE_MC', 0);
        
        //create data
        dataSetUp();
        dataSetUp2();
                
        Test.startTest();
        
        System.runAs(analyst) {
            
            mcProposal.BI_PC_Flow_path__c = BI_PC_ProposalSubmitApprovalExt.FLOW_Prod_Answers_Step;	//set the correct path step
            update mcProposal;
            
            delete [SELECT Id FROM BI_PC_Scenario__c];
            
            BI_PC_ProposalSubmitApprovalExt ctrl = new BI_PC_ProposalSubmitApprovalExt(new ApexPages.StandardController(mcProposal));  
            BI_PC_ProposalSubmitApprovalExt.executeAction();      
        }
        
        Test.StopTest();
        
        List<BI_PC_Proposal__c> proposalsList = [SELECT OwnerId FROM BI_PC_Proposal__c WHERE Id = :mcProposal.Id];	//get submitted proposal
        
        System.assert(!proposalsList.isEmpty());
        System.assertEquals(analyst.Id, proposalsList[0].OwnerId);
    }
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Test Analyst submission
	************************************************************************************************************/
    public static testMethod void submitByAnalyst_Test() {
                 
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_admin', 0);
        
        //create data
        dataSetUp();
        dataSetUp2();
            
        Test.startTest();
        
        System.runAs(analyst) {
            
            mcAcc.BI_PC_Group__c = BI_PC_ProposalSubmitApprovalExt.PV_Group_3_Account;
            update mcAcc;
            
            mcProposal.BI_PC_Status__c = BI_PC_ProposalSubmitApprovalExt.PV_Status_BC_Development;
            update mcProposal;
            
            BI_PC_ProposalSubmitApprovalExt ctrl = new BI_PC_ProposalSubmitApprovalExt(new ApexPages.StandardController(mcProposal));
            BI_PC_ProposalSubmitApprovalExt.executeAction();
        }
        
        Test.StopTest();
        
        List<BI_PC_Scenario__c> scenariosList = [SELECT BI_PC_Approval_status__c FROM BI_PC_Scenario__c WHERE BI_PC_Proposal__c = :mcProposal.Id];	//get submitted scenarios
        
        System.assert(!scenariosList.isEmpty());
        System.assertEquals(BI_PC_ProposalSubmitApprovalExt.PV_Status_AiP_DC_Development, scenariosList[0].BI_PC_Approval_status__c);
    }
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Test Analyst gov submission
	************************************************************************************************************/
    public static testMethod void submitByAnalystGov_Test() {
                  
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_admin', 0);
          
        //create data
        dataSetUp();
        dataSetUp2();
            
        Test.startTest();
        
        System.runAs(analyst) {
            
            govAcc.BI_PC_Group__c = BI_PC_ProposalSubmitApprovalExt.PV_Group_3_Account;
            update govAcc;
            
            govProposal.BI_PC_Status__c = BI_PC_ProposalSubmitApprovalExt.PV_Status_BC_Development;
            update govProposal;
            
            BI_PC_ProposalSubmitApprovalExt ctrl = new BI_PC_ProposalSubmitApprovalExt(new ApexPages.StandardController(govProposal));
            BI_PC_ProposalSubmitApprovalExt.executeAction();
        }
        
        Test.StopTest();
        
        List<BI_PC_Scenario__c> scenariosList = [SELECT BI_PC_Approval_status__c FROM BI_PC_Scenario__c WHERE BI_PC_Proposal__c = :govProposal.Id];	//get submitted scenarios
        
        System.assert(!scenariosList.isEmpty());
        System.assertEquals(BI_PC_ProposalSubmitApprovalExt.PV_Status_AiP_Gov_Compliance, scenariosList[0].BI_PC_Approval_status__c);
    }
    
    /***********************************************************************************************************
	* @date 13/07/2018 (dd/mm/yyyy)
	* @description Test Analyst submission with Error
	************************************************************************************************************/
    public static testMethod void submitByAnalyst_Error_Test() {
               
        analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 'BI_PC_MA_DOTS_admin', 0);
             
        //create data
        dataSetUp();
        dataSetUp2();
            
        Test.startTest();
        
        System.runAs(analyst) {
            
            mcAcc.BI_PC_Group__c = BI_PC_ProposalSubmitApprovalExt.PV_Group_3_Account;
            update mcAcc;
                                    
            // Create an approval request for the scenario to get an error when submitting again            
            mcProposal.BI_PC_Status__c = 'Approval in Progress: Dir Contract Development';
            update mcProposal;
            Approval.ProcessSubmitRequest aprvReq = new Approval.ProcessSubmitRequest();
            aprvReq.setObjectId(mcScenario.id);
            aprvReq.setSubmitterId(UserInfo.getUserId()); 
            Approval.ProcessResult result = Approval.process(aprvReq);
                        
            mcProposal.BI_PC_Status__c = BI_PC_ProposalSubmitApprovalExt.PV_Status_BC_Development;
            update mcProposal;
            
            BI_PC_ProposalSubmitApprovalExt ctrl = new BI_PC_ProposalSubmitApprovalExt(new ApexPages.StandardController(mcProposal));
            BI_PC_ProposalSubmitApprovalExt.executeAction();
        }
        
        Test.StopTest();
        
        List<BI_PC_Scenario__c> scenariosList = [SELECT BI_PC_Approval_status__c FROM BI_PC_Scenario__c WHERE BI_PC_Proposal__c = :mcProposal.Id];	//get submitted scenarios
        
        System.assert(!scenariosList.isEmpty());
        System.assertNotEquals(BI_PC_ProposalSubmitApprovalExt.PV_Status_AiP_DC_Development, scenariosList[0].BI_PC_Approval_status__c);
    }
}