/***********************************************************************************************************
* @date 08/06/2018 (dd/mm/yyyy)
* @description PC Proposal Tigger Handler Class (iCon)
************************************************************************************************************/
public class BI_PC_ProposalTriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	public BI_PC_ProposalTriggerLogic logic = null;
	private final String RAM_STATUS = 'Approval in Progress: MM Sales';
    public static Id mcRTId;
    public static Id govRTId;
	
	public BI_PC_ProposalTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
		logic = BI_PC_ProposalTriggerLogic.getInstance();
        mcRTId = Schema.SObjectType.BI_PC_Proposal__c.getRecordTypeInfosByDeveloperName().get('BI_PC_Prop_m_care').getRecordTypeId();	//get managed care record type id
        govRTId = Schema.SObjectType.BI_PC_Proposal__c.getRecordTypeInfosByDeveloperName().get('BI_PC_Prop_government').getRecordTypeId();	//get government record type id
	}
			
	/***********************************************************************************************************
	* @date 31/07/2018 (dd/mm/yyyy)
	* @description OnBeforeInsert logic calls
	************************************************************************************************************/
    public void OnBeforeInsert(List<BI_PC_Proposal__c> newProposals) {
        
        Set<Id> accIdSet = new Set<Id>();	//set with ids of accounts related to the proposal
        
        for(BI_PC_Proposal__c proposal : newProposals) {
            
            if(proposal.RecordTypeId == mcRTId) {
                accIdSet.add(proposal.BI_PC_Account__c);
            } else if(proposal.RecordTypeId == govRTId) {
                accIdSet.add(proposal.BI_PC_Account__c);
            }
        }
        
        logic.setProposalChannel(newProposals, accIdSet);	//set proposal channels
    }
	
	/***********************************************************************************************************
	* @date 12/07/2018 (dd/mm/yyyy)
	* @description OnBeforeUpdate logic calls
	************************************************************************************************************/
    public void OnBeforeUpdate(List<BI_PC_Proposal__c> newProposals, Map<Id, BI_PC_Proposal__c> oldProposalMap) {
        
        Set<Id> aprvProcessEndedPropIdSet = new Set<Id>();	//set with proposal Ids
        
        for(BI_PC_Proposal__c proposal : newProposals) {
            
            if(proposal.BI_PC_Status__c != oldProposalMap.get(proposal.Id).BI_PC_Status__c && (proposal.BI_PC_Status__c == 'Approved' || proposal.BI_PC_Status__c == 'Rejected')) {
                aprvProcessEndedPropIdSet.add(proposal.Id);
            }
        }
        
        if(!aprvProcessEndedPropIdSet.isEmpty()) {
            logic.setApprovalDataOnProposal(aprvProcessEndedPropIdSet, newProposals, oldProposalMap);
        }
    }
	
	public void OnAfterUpdate(List<BI_PC_Proposal__c> oldProposals, List<BI_PC_Proposal__c> updatedProposals, Map<ID, BI_PC_Proposal__c> proposalMap) {

		system.debug('BI_PC_ProposalTriggerHandler:: OnAfterUpdate:: starts');

		Id governmentID = Schema.SObjectType.BI_PC_Proposal__c.getRecordTypeInfosByDeveloperName().get('BI_PC_Prop_government').getRecordTypeId();
		Map<Id,BI_PC_Proposal__c> proposalsToShare = new Map<Id,BI_PC_Proposal__c>();
		Map<Id,BI_PC_Proposal__c> proposalsSharingToDelete = new Map<Id,BI_PC_Proposal__c>();

		
		logic.createProposalHistoryRecord(oldProposals, updatedProposals,proposalMap);

		for(BI_PC_Proposal__c oldProposal : oldProposals) {

			BI_PC_Proposal__c newProposal = proposalMap.get(oldProposal.Id);
            
            //Government proposal with status MM Sales must be checked by RAM/NAD User so we need to create sharing and send email
            if((newProposal.RecordTypeId == governmentID) && (newProposal.BI_PC_Status__c != oldProposal.BI_PC_Status__c) && (newProposal.BI_PC_Status__c == RAM_STATUS)) {
                proposalsToShare.put(newProposal.Id,newProposal);
            }

            //Government proposal with status MM Sales changes to other status, RAM/NAD user should not have access to the record anymore
            /*if((newProposal.RecordTypeId == governmentID) && (newProposal.BI_PC_Status__c != oldProposal.BI_PC_Status__c) && (oldProposal.BI_PC_Status__c == RAM_STATUS)) {
                proposalsSharingToDelete.put(newProposal.Id, newProposal);
            }*/
        }

        //logic.sharingAndEmailForRamNadUser(proposalsToShare,proposalsSharingToDelete);
        logic.sharingForRamNadUser(proposalsToShare);
		
		system.debug('BI_PC_ProposalTriggerHandler:: OnAfterUpdate:: ends');

	}
}