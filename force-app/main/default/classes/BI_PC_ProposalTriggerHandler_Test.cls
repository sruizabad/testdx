/****************************************************************************************************
* @date 12/06/2018 (dd/mm/yyyy) 
* @description This is the test class for BI_PC_ProposalTriggerHandler Apex Class
****************************************************************************************************/
@isTest
public class BI_PC_ProposalTriggerHandler_Test {

    private static BI_PC_Scenario__c scenario;
	private static BI_PC_Proposal__c proposal;
	private static BI_PC_Proposal__c govProposal;
	private static BI_PC_Account__c newAccount;
	private static User newAnalyst;
	private static User newOwner;
	private static User analyst;
	
	@isTest
	private static void updateProposal_test(){

		dataSetUp();

		Test.startTest();
		
		Id governmentPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_government', 'BI_PC_Proposal__c');
		
		proposal.RecordTypeId = governmentPropId;
		proposal.BI_PC_Due_date__c = Date.today().addDays(20);
		proposal.BI_PC_Status__c = 'Business Case in Development';
		proposal.BI_PC_Account__c = newAccount.Id;
		proposal.BI_PC_Start_date__c = Date.today().addDays(1);
		proposal.BI_PC_End_date__c = Date.today().addDays(15);
		proposal.OwnerId = newOwner.Id;
		proposal.BI_PC_Admin_fee__c = 15;
		proposal.BI_PC_Analyst__c = newAnalyst.FirstName + ' ' + newAnalyst.LastName;
		proposal.BI_PC_Priority_level__c = 'High';
		update proposal;

		Test.stopTest();

		List<BI_PC_Prop_history__c> phList = new List<BI_PC_Prop_history__c>();
		phList = [SELECT Id, BI_PC_Field_API_name__c, BI_PC_Field_label__c, BI_PC_New_value__c, BI_PC_Old_value__c, BI_PC_Proposal__c FROM BI_PC_Prop_history__c];
		system.assertEquals(10, phList.size());

		for(BI_PC_Prop_history__c ph: phList){
			switch on ph.BI_PC_Field_API_name__c {
				when 'RecordTypeId' {

					system.assertEquals('Managed Care', ph.BI_PC_Old_value__c);
					system.assertEquals('Government', ph.BI_PC_New_value__c);
				}  
				when 'BI_PC_Due_date__c' {

					system.assertEquals(String.valueOf(Date.today().addDays(40)), ph.BI_PC_Old_value__c);
					system.assertEquals(String.valueOf(Date.today().addDays(20)), ph.BI_PC_New_value__c);
				}
				when 'BI_PC_Status__c' {
					system.assertEquals('Proposal in Development', ph.BI_PC_Old_value__c);
					system.assertEquals('Business Case in Development', ph.BI_PC_New_value__c);
				}
				when 'BI_PC_Account__c' {
					system.assertEquals('Test Managed Care Account', ph.BI_PC_Old_value__c);
					system.assertEquals('Test Government Account', ph.BI_PC_New_value__c);
				}
				when 'BI_PC_Start_date__c' {
					system.assertEquals(String.valueOf(Date.today()), ph.BI_PC_Old_value__c);
					system.assertEquals(String.valueOf(Date.today().addDays(1)), ph.BI_PC_New_value__c);
				}
				when 'BI_PC_End_date__c' {
					system.assertEquals(String.valueOf(Date.today().addDays(30)), ph.BI_PC_Old_value__c);
					system.assertEquals(String.valueOf(Date.today().addDays(15)), ph.BI_PC_New_value__c);
				}
				when 'BI_PC_Admin_fee__c' {
					system.assertEquals(String.valueOf(20.000), ph.BI_PC_Old_value__c);
					system.assertEquals(String.valueOf(15.000), ph.BI_PC_New_value__c);
				}
				when 'BI_PC_Analyst__c' {
					system.assertEquals(analyst.FirstName + ' ' + analyst.LastName, ph.BI_PC_Old_value__c);
					system.assertEquals(newAnalyst.FirstName + ' ' + newAnalyst.LastName, ph.BI_PC_New_value__c);
				}
				when 'BI_PC_Priority_level__c' {
					system.assertEquals('Normal', ph.BI_PC_Old_value__c);
					system.assertEquals('High', ph.BI_PC_New_value__c);
				}
				
			}
		}

	}


	/******************************************************************************************************************
    * @date             12/06/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
	private static void dataSetUp(){
		analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
		
		Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
		Id accGovernmentId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_government', 'BI_PC_Account__c');
		BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test Managed Care Account', analyst, TRUE);
		newAccount = BI_PC_TestMethodsUtility.getPCAccount(accGovernmentId, 'Test Government Account', analyst, TRUE);
		
		newAnalyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 1);
		newOwner = BI_PC_TestMethodsUtility.insertUser('System Administrator', 2);

		Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');
		proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40) , FALSE);
		proposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
		insert proposal;     

		Id govPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_government', 'BI_PC_Proposal__c');
		govProposal = BI_PC_TestMethodsUtility.getPCProposal(govPropId, newAccount.Id, 'Proposal in Development', Date.today().addDays(30), Date.today().addDays(40) , FALSE);
		govProposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
		insert govProposal; 
		   
	}
	
	/******************************************************************************************************************
    * @date             12/07/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
    private static void dataSetUp2() {
                
		//create product
        Id commercialProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prod_m_care', 'BI_PC_Product__c');
        BI_PC_Product__c product = BI_PC_TestMethodsUtility.getPCProduct(commercialProdId, 'Test Product', false);
        insert product;
        
		//create guideline rate
        Id commercialGuidelineRateId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_GLR_m_care', 'BI_PC_Guide_line_rate__c');
        BI_PC_Guide_line_rate__c guidelineRate = BI_PC_TestMethodsUtility.getPCGuidelineRate(commercialGuidelineRateId, product.Id, false);
        insert guidelineRate;
        
		//create proposal product
        Id currentPropProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Proposed', 'BI_PC_Proposal_Product__c');
        BI_PC_Proposal_Product__c propProduct = BI_PC_TestMethodsUtility.getPCProposalProduct(currentPropProdId, proposal.Id, product.Id, false);
        insert propProduct;
        
		//create scenario
        Id currentScenarioId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Future', 'BI_PC_Scenario__c');
        scenario = BI_PC_TestMethodsUtility.getPCScenario(currentScenarioId, proposal.Id, propProduct.Id, guidelineRate.Id, false);
        insert scenario;
    }
    
	/******************************************************************************************************************
    * @date             12/07/2018 (dd/mm/yyyy)  
    * @description      Test set approval comments in proposal 
    *******************************************************************************************************************/
    public static testMethod void setApprovalDataOnProposal_test() {
        
        dataSetUp();	//create data
        dataSetUp2();	//create data
        
        //update proposal status
        proposal.BI_PC_Status__c = BI_PC_ProposalSubmitApprovalExt.PV_Status_AiP_BM_DC_Development;
        update proposal;
                
        // Create an approval request for the scenario
        Approval.ProcessSubmitRequest aprvReq = new Approval.ProcessSubmitRequest();
        aprvReq.setComments('Submitting request for approval.');
        aprvReq.setObjectId(scenario.id);
        
        // Submit on behalf of a specific submitter
        aprvReq.setSubmitterId(UserInfo.getUserId()); 
        
        // Submit the approval request for the scenario
        Approval.ProcessResult result = Approval.process(aprvReq);
        
        // Verify the result
        System.assert(result.isSuccess());        
        System.assertEquals('Pending', result.getInstanceStatus());
        
        // Approve the submitted request
        // First, get the ID of the newly created item
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest workItemReq = new Approval.ProcessWorkitemRequest();
        workItemReq.setComments('Approving request for ' + scenario.id);
        workItemReq.setAction('Approve');
        workItemReq.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Use the ID from the newly created item to specify the item to be worked
        workItemReq.setWorkitemId(newWorkItemIds.get(0));
        
        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(workItemReq);

        Test.startTest();
        
        //approve proposal
        proposal.BI_PC_Status__c = 'Approved';
        update proposal;
                
        Test.stopTest();
        
        List<BI_PC_Proposal__c> approvedProposals = [SELECT BI_PC_Approval_comments__c FROM BI_PC_Proposal__c WHERE Id = :proposal.Id];
        System.assert(!approvedProposals.isEmpty());
        System.assert(approvedProposals[0].BI_PC_Approval_comments__c != null && approvedProposals[0].BI_PC_Approval_comments__c.contains(scenario.id), approvedProposals[0].BI_PC_Approval_comments__c);
    }

    /******************************************************************************************************************
    * @date             12/07/2018 (dd/mm/yyyy)  
    * @description      Test set ram/nad visibility 
    *******************************************************************************************************************/
    public static testMethod void sharingAndEmailForRamNadUser_test() {

    	String ramStatus = 'Approval in Progress: MM Sales';
    	dataSetUp();	//create data
        dataSetUp2();	//create data

        //Update government proposal status to MM Sales
        Test.startTest();

        List<BI_PC_Proposal__Share> priorPropShare = new List<BI_PC_Proposal__Share>([SELECT Id, ParentId FROM BI_PC_Proposal__Share WHERE UserOrGroupId = : analyst.Id LIMIT 1]);

    	govProposal.BI_PC_Status__c = ramStatus;
    	update govProposal;

    	BI_PC_Proposal__Share existingPropShare = [SELECT Id, ParentId FROM BI_PC_Proposal__Share WHERE UserOrGroupId = : analyst.Id LIMIT 1];
    	
		Test.stopTest();

		system.assertEquals(TRUE,priorPropShare.isEmpty());
		system.assertEquals(govProposal.Id,existingPropShare.ParentId);
	}

}