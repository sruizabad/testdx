/***********************************************************************************************************
* @date 08/06/2018 (dd/mm/yyyy)
* @description PC Proposal Tigger Handler Class (iCon)
************************************************************************************************************/
public class BI_PC_ProposalTriggerLogic {

	public static BI_PC_ProposalTriggerLogic instance = null;
	private BI_PC_ProposalTriggerLogic() {}
    
	public static BI_PC_ProposalTriggerLogic getInstance() {
        if(instance == null) {
            instance = new BI_PC_ProposalTriggerLogic();
        } 
		return instance;
	}
    
    /***********************************************************************************************************
	* @date 31/07/2018 (dd/mm/yyyy)
	* @description Set the channel of the account to the proposal
	* @param List<BI_PC_Proposal__c> newProposals	:	list of new proposals
	* @param Set<Id> accIdSet	:	set of acc ids related to proposals
	* @return void
	************************************************************************************************************/
    public void setProposalChannel(List<BI_PC_Proposal__c> newProposals, Set<Id> accIdSet) {
        
        Map<Id, BI_PC_Account__c> accMap = new Map<Id, BI_PC_Account__c>([SELECT BI_PC_Channel__c, BI_PC_RAM_NAD__c FROM BI_PC_Account__c WHERE Id IN :accIdSet]);	//get account channels
        
        for(BI_PC_Proposal__c proposal : newProposals) {
            
            if(accMap.containsKey(proposal.BI_PC_Account__c)) {
                if(proposal.RecordTypeId == BI_PC_ProposalTriggerHandler.mcRTId) {
                    proposal.BI_PC_Channel__c = accMap.get(proposal.BI_PC_Account__c).BI_PC_Channel__c;	//set account channel
                } else if(proposal.RecordTypeId == BI_PC_ProposalTriggerHandler.govRTId) {
                    proposal.BI_PC_RAM_NAD__c = accMap.get(proposal.BI_PC_Account__c).BI_PC_RAM_NAD__c;	//set account RAM NAD
                }
            }
        }
    }
    
    /***********************************************************************************************************
	* @date 12/07/2018 (dd/mm/yyyy)
	* @description OnBeforeUpdate logic calls
	************************************************************************************************************/
    public void setApprovalDataOnProposal(Set<Id> proposalIdSet, List<BI_PC_Proposal__c> newProposals, Map<Id, BI_PC_Proposal__c> oldProposalMap) {
        
        Map<Id, String> proposalCommentsMap = new Map<Id, String>();	//map of proposal ids and their approval comments
        Set<Id> aprvProcessIdSet = new Set<Id>();	//set of approval processes ids
        Map<Id, ProcessInstanceStep> aprvProcessDataMap = new Map<Id, ProcessInstanceStep>();	//map of proposal ids and their approval processes data
        
        //get scenarios map with Ids and their data
        Map<Id, BI_PC_Scenario__c>  scenarioIdMap = new Map<Id, BI_PC_Scenario__c>([SELECT Id, Name, BI_PC_Proposal__c FROM BI_PC_Scenario__c WHERE BI_PC_Proposal__c IN :proposalIdSet]);
        
        System.debug(LoggingLevel.INFO, 'BI_PC_ProposalTriggerLogic.setApprovalDataOnProposal>>>>> scenarioIdMap: ' + scenarioIdMap);
        
        //get approval processes Ids
        for(ProcessInstance aprvProcess : [SELECT Id FROM ProcessInstance WHERE TargetObjectId IN :scenarioIdMap.keySet()]) {
            aprvProcessIdSet.add(aprvProcess.Id);
        }        
        System.debug(LoggingLevel.INFO, 'BI_PC_ProposalTriggerLogic.setApprovalDataOnProposal>>>>> aprvProcessIdSet: ' + aprvProcessIdSet);

        //get approval processes data
        for(ProcessInstanceStep aprvProcessData : [SELECT Id, ProcessInstance.TargetObjectId, Comments, Actor.Name, StepStatus FROM ProcessInstanceStep WHERE ProcessInstanceId IN :aprvProcessIdSet AND Comments != null]) {
            
            BI_PC_Scenario__c scenario = scenarioIdMap.get(aprvProcessData.ProcessInstance.TargetObjectId);	//get scenario data
            
            String aprvComments = String.format(Label.BI_PC_Approval_comments_format, new String[]{scenario.Name, aprvProcessData.StepStatus, aprvProcessData.Actor.Name, aprvProcessData.Comments});
            
            if(proposalCommentsMap.containsKey(scenario.BI_PC_Proposal__c)) {
                
                String existingComments = proposalCommentsMap.get(scenario.BI_PC_Proposal__c);	//get existing comments from the proposal map
                
                Integer totalLength = existingComments.length() + aprvComments.length();	//calculate total length
                
                if(totalLength < 32768) {
                    proposalCommentsMap.put(scenario.BI_PC_Proposal__c, existingComments + '\n' + aprvComments);
                }
            } else {
                if(aprvComments.length() <= 32768) {
                    proposalCommentsMap.put(scenario.BI_PC_Proposal__c, aprvComments);
                }
            }
        }  
        System.debug(LoggingLevel.INFO, 'BI_PC_ProposalTriggerLogic.setApprovalDataOnProposal>>>>> proposalCommentsMap: ' + proposalCommentsMap);
        
        //store all comments in the proposal record
        for(BI_PC_Proposal__c proposal : newProposals) {
            
            if(proposalCommentsMap.containsKey(proposal.Id)) {
                proposal.BI_PC_Approval_comments__c = proposalCommentsMap.get(proposal.Id);
            }
            
            //Change the step in the flow field
            proposal.BI_PC_Flow_path__c = 'Step7';
        }
        System.debug(LoggingLevel.INFO, 'BI_PC_ProposalTriggerLogic.setApprovalDataOnProposal>>>>> newProposals: ' + newProposals);
    }

    /***********************************************************************************************************
    * @date 08/06/2018 (dd/mm/yyyy)
    * @description OnAfterUpdate logic calls
    ************************************************************************************************************/
    public void createProposalHistoryRecord(List<BI_PC_Proposal__c> oldProposals, List<BI_PC_Proposal__c> updatedProposals, Map<ID, BI_PC_Proposal__c> proposalMap) {
        //08-06-2018. 
        //Insert Proposal History - Start
        //When a proposal is modified, we check if the fields stored in the field set changed. Then we create a Proposal History Record tracking these changes.
        List<Schema.FieldSetMember> fieldsToTrack = Schema.SObjectType.BI_PC_Proposal__c.fieldSets.BI_PC_Prop_fields_tracked.getFields();
        List<BI_PC_Prop_history__c> propHistToInsert = new List<BI_PC_Prop_history__c>();
        List<BI_PC_Prop_history__c> propHistToCheck = new List<BI_PC_Prop_history__c>();
        Set<Id> accountsToQuery = new Set<Id>();
        Set<Id> usersToQuery = new Set<Id>();
        Set<Id> recordTypeToQuery = new Set<Id>();

        for(BI_PC_Proposal__c oldProp: oldProposals){

            BI_PC_Proposal__c newProp = proposalMap.get(oldProp.Id);

            for(Schema.FieldSetMember fs: fieldsToTrack){

                String apiName = fs.getFieldPath();
                
                if(oldProp.get(apiName) != newProp.get(apiName)){
                    
                    BI_PC_Prop_history__c propHist = new BI_PC_Prop_history__c();
                    propHist.BI_PC_Field_API_name__c = apiName;
                    propHist.BI_PC_Field_label__c = fs.getLabel();
                    propHist.BI_PC_New_value__c = BI_PC_ApexMethodsUtility.parseToString(newProp, apiName, fs.getType());

                    if(oldProp.get(apiName) != NULL){
                        propHist.BI_PC_Old_value__c = BI_PC_ApexMethodsUtility.parseToString(oldProp, apiName, fs.getType());
                    }
                    
                    propHist.BI_PC_Proposal__c = newProp.Id;

                    //If the field what changed is a lookup, we need to retrieve the name.
                    if(fs.getType() != Schema.DisplayType.Reference){
                        propHistToInsert.add(propHist);
                    }else{

                        propHistToCheck.add(propHist);
                        
                        //Get the referenced object name using the ID of the record.
                        String sObjName;
                        if(propHist.BI_PC_New_value__c != NULL){
                            sObjName = ((Id) propHist.BI_PC_New_value__c).getSObjectType().getDescribe().getName();
                        }else{
                            sObjName = ((Id) propHist.BI_PC_Old_value__c).getSObjectType().getDescribe().getName();
                        }

                        //We store the ids in different lists depending on the object type.
                        if(sObjName == 'BI_PC_Account__c'){

                            if(propHist.BI_PC_New_value__c != NULL) accountsToQuery.add(propHist.BI_PC_New_value__c);
                            if(propHist.BI_PC_Old_value__c != NULL) accountsToQuery.add(propHist.BI_PC_Old_value__c);
                        }
                        if(sObjName == 'User'){

                            if(propHist.BI_PC_New_value__c != NULL) usersToQuery.add(propHist.BI_PC_New_value__c);
                            if(propHist.BI_PC_Old_value__c != NULL) usersToQuery.add(propHist.BI_PC_Old_value__c);
                        }
                        if(sObjName == 'RecordType'){
                            if(propHist.BI_PC_New_value__c != NULL) recordTypeToQuery.add(propHist.BI_PC_New_value__c);
                            if(propHist.BI_PC_Old_value__c != NULL) recordTypeToQuery.add(propHist.BI_PC_Old_value__c);
                        }
                        
                    }
                }
            }
        }

        if(!accountsToQuery.isEmpty()){
            for(BI_PC_Account__c acc: [SELECT Id, Name FROM BI_PC_Account__c WHERE Id IN: accountsToQuery]){

                //We check the list of prop history objects associated to the account
                for(BI_PC_Prop_history__c ph: propHistToCheck){

                    //Check if the field we need to update is the old value or the new one
                    if(ph.BI_PC_New_value__c == (String) acc.Id){
                        ph.BI_PC_New_value__c = acc.Name;
                    }
                    if(ph.BI_PC_Old_value__c == (String) acc.Id){
                        ph.BI_PC_Old_value__c = acc.Name;
                    }
                }
            }
        }
        if(!usersToQuery.isEmpty()){
            for(User u: [SELECT Id, Name FROM User WHERE Id IN: usersToQuery]){

                //We check the list of prop history objects associated to the account
                for(BI_PC_Prop_history__c ph: propHistToCheck){

                    //Check if the field we need to update is the old value or the new one
                    if(ph.BI_PC_New_value__c == (String) u.Id){
                        ph.BI_PC_New_value__c = u.Name;
                    }
                    if(ph.BI_PC_Old_value__c == (String) u.Id){
                        ph.BI_PC_Old_value__c = u.Name;
                    }
                    
                }
            }
        }
        if(!recordTypeToQuery.isEmpty()){
            for(RecordType rt: [SELECT Id, Name FROM RecordType WHERE Id IN: recordTypeToQuery]){

                //We check the list of prop history objects associated to the account
                for(BI_PC_Prop_history__c ph: propHistToCheck){

                    //Check if the field we need to update is the old value or the new one
                    if(ph.BI_PC_New_value__c == (String) rt.Id){
                        ph.BI_PC_New_value__c = rt.Name;
                    }
                    if(ph.BI_PC_Old_value__c == (String) rt.Id){
                        ph.BI_PC_Old_value__c = rt.Name;
                    }
                }
            }
        }
        
        if(!propHistToCheck.isEmpty()){
            propHistToInsert.addAll(propHistToCheck);
        }
        
        if(!propHistToInsert.isEmpty()){
            system.debug('BI_PC_ProposalTriggerHandler:: OnAfterUpdate:: Insert Prop History:: ' + propHistToInsert.size());
            insert propHistToInsert;
        }
        
        //Insert Proposal History - End

    }

    /***********************************************************************************************************
    * @date 18/07/2018 (dd/mm/yyyy)
    * @description OnAfterUpdate logic calls
    ************************************************************************************************************/
    //public void sharingAndEmailForRamNadUser(Map<Id,BI_PC_Proposal__c> proposalsToShareMap, Map<Id,BI_PC_Proposal__c> propSharingToDeleteMap) {
    public void sharingForRamNadUser(Map<Id,BI_PC_Proposal__c> proposalsToShareMap) {
        Set<Id> propIds = new Set<Id>();
        Map<Id,Id> propRamUserIds = new Map<Id,Id>();
        Map<Id,BI_PC_Proposal__Share> existingPropShareMap = new Map<Id,BI_PC_Proposal__Share>();
        List<BI_PC_Proposal__Share> propSharingToDelete = new List<BI_PC_Proposal__Share>();
        List<BI_PC_Proposal__Share> propSharingToInsert = new List<BI_PC_Proposal__Share>();
        Map<Id,User> propRamUserMap = new Map<Id,User>();
        Map<Id,User> ramUserMap = new Map<Id,User>();
        List<User> ramUserList = new List<User>();
        
        propIds.addAll(proposalsToShareMap.keySet());
        //propIds.addAll(propSharingToDeleteMap.keySet());


              
        //group ram/nad users by record
        for(BI_PC_Proposal__c p: [SELECT Id, BI_PC_Account__c, BI_PC_Account__r.BI_PC_Ram_nad__c, BI_PC_Account__r.BI_PC_Ram_nad__r.Email FROM BI_PC_Proposal__c WHERE Id IN :propIds]){
            propRamUserIds.put(p.Id, p.BI_PC_Account__r.BI_PC_Ram_nad__c);
            ramUserMap.put(p.BI_PC_Account__r.BI_PC_Ram_nad__c, new User(Id=p.BI_PC_Account__r.BI_PC_Ram_nad__c, Email = p.BI_PC_Account__r.BI_PC_Ram_nad__r.Email));
        }

        //Remove the admin users from user map (we don't need to apply sharing settings for them)
        for (GroupMember gm : [SELECT Id, UserOrGroupId FROM GroupMember WHERE UserOrGroupId IN: ramUserMap.keySet() AND Group.DeveloperName = 'BI_PC_Admin']){
            ramUserMap.remove(gm.UserOrGroupId);
        }

        System.debug(LoggingLevel.INFO, 'BI_PC_ProposalTriggerLogic.sharingAndEmailForRamNadUser>>>>> propRamUserIds: ' + propRamUserIds);
        System.debug(LoggingLevel.INFO, 'BI_PC_ProposalTriggerLogic.sharingAndEmailForRamNadUser>>>>> ramUserMap: ' + ramUserMap);

        //Check existing proposal share for RAM/NAD user
        for(BI_PC_Proposal__Share existingPropShare : [SELECT Id, ParentId FROM BI_PC_Proposal__Share WHERE UserOrGroupId = : ramUserMap.keySet()]){
            existingPropShareMap.put(existingPropShare.ParentId,existingPropShare);
        }
        
        for(Id createPropSharing: proposalsToShareMap.keySet()){
            //check no share on the record exists for the RAM/NAD user
            if(!existingPropShareMap.containsKey(createPropSharing)){
                Id userId = propRamUserIds.get(createPropSharing);


                //Add sharing just to users which are not Admin
                if(ramUserMap.containsKey(userId)){

                 
                    BI_PC_Proposal__Share newPropSharing = new BI_PC_Proposal__Share();
                    newPropSharing.ParentId = createPropSharing;
                    newPropSharing.UserOrGroupId = userId;
                    newPropSharing.AccessLevel = 'Read';
                    newPropSharing.RowCause = Schema.BI_PC_Proposal__Share.RowCause.BI_PC_Is_the_prop_ram_nad__c;
                    propSharingToInsert.add(newPropSharing);

                }
            }
        }

        //System.debug(LoggingLevel.INFO, 'BI_PC_ProposalTriggerLogic.sharingAndEmailForRamNadUser>>>>> propSharingToDelete: ' + propSharingToDelete);
        System.debug(LoggingLevel.INFO, 'BI_PC_ProposalTriggerLogic.sharingAndEmailForRamNadUser>>>>> propSharingToInsert: ' + propSharingToInsert);

        if(!propSharingToInsert.isEmpty()){
            insert propSharingToInsert;
        }
	}
}