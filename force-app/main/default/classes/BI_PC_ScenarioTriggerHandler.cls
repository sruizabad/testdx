/***********************************************************************************************************
* @date 17/07/2018 (dd/mm/yyyy)
* @description PC Scenario Tigger Handler Class (iCon)
************************************************************************************************************/
public class BI_PC_ScenarioTriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	public BI_PC_ScenarioTriggerLogic logic = null;
	
	public BI_PC_ScenarioTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
		logic = BI_PC_ScenarioTriggerLogic.getInstance();
	}
	
		
	//public void OnBeforeInsert(List<BI_PC_Scenario__c> newScenarios){}
	
	//public void OnAfterInsert(List<BI_PC_Scenario__c> newScenariosList){}

	//@future public static void OnAfterInsertAsync(Set<ID> newScenarioIDs){}
	
	///public void OnBeforeUpdate(List<BI_PC_Scenario__c> oldScenarios, List<BI_PC_Scenario__c> updatedScenarios, Map<ID, BI_PC_Scenario__c> newScenarioMap){}
	
	public void OnAfterUpdate(List<BI_PC_Scenario__c> oldScenarios, List<BI_PC_Scenario__c> updatedScenarios, Map<ID, BI_PC_Scenario__c> scenarioMap){

		system.debug('BI_PC_ScenarioTriggerHandler:: OnAfterUpdate:: starts');

		List<Id> proposalToCheck = new List<Id>();
		//Once all the status scenarios changed, proposal status must change
		for(BI_PC_Scenario__c oldScenario: oldScenarios){
			BI_PC_Scenario__c newScenario = ScenarioMap.get(oldScenario.Id);
			if(oldScenario.BI_PC_Approval_status__c != newScenario.BI_PC_Approval_status__c){
				proposalToCheck.add(newScenario.BI_PC_Proposal__c);
			}
		}

		logic.checkRelatedScenariosByProposal(proposalToCheck);		

		system.debug('BI_PC_ScenarioTriggerHandler:: OnAfterUpdate:: ends');

	}
	
	//@future public static void OnAfterUpdateAsync(Set<ID> updatedScenarioIDs){}
	
	//public void OnBeforeDelete(List<BI_PC_Scenario__c> ScenariosToDelete, Map<ID, BI_PC_Scenario__c> ScenarioMap){}
	
	//public void OnAfterDelete(List<BI_PC_Scenario__c> deletedScenarios, Map<ID, BI_PC_Scenario__c> ScenarioMap){}
	
	//@future public static void OnAfterDeleteAsync(Set<ID> deletedScenarioIDs){}
	
	//public void OnUndelete(List<BI_PC_Scenario__c> restoredScenarios){}
	/*
	public boolean IsTriggerContext{
		get{ return m_isExecuting;}
	}
	*/
	/*
	public boolean IsVisualforcePageContext{
		get{ return !IsTriggerContext;}
	}
	*/
	/*
	public boolean IsWebServiceContext{
		get{ return !IsTriggerContext;}
	}
	*/
	/*
	public boolean IsExecuteAnonymousContext{
		get{ return !IsTriggerContext;}
	}
	*/
	//public String parseToString(BI_PC_Scenario__c propValue, String apiName, Schema.DisplayType fieldType){

}