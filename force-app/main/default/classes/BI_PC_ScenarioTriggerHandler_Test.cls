/****************************************************************************************************
* @date 23/07/2018 (dd/mm/yyyy) 
* @description This is the test class for BI_PC_ScenarioTriggerHandler Apex Class
****************************************************************************************************/
@isTest
public class BI_PC_ScenarioTriggerHandler_Test {

	private static BI_PC_Scenario__c scenario;
	private static BI_PC_Scenario__c scenario2;
	private static BI_PC_Proposal__c proposal;
	private static User analyst;

	/******************************************************************************************************************
    * @date             23/07/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
	private static void dataSetUpMC(){
		analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
		
		Id accComercialId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_m_care', 'BI_PC_Account__c');
		BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accComercialId, 'Test MC Account', analyst, TRUE);
				
		Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_m_care', 'BI_PC_Proposal__c');
		proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Approval in Progress: Brand Marketing - Dir Contract Development', Date.today().addDays(30), Date.today().addDays(40) , FALSE);
		proposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
		insert proposal;     

		//create product
        Id commercialProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prod_m_care', 'BI_PC_Product__c');
        BI_PC_Product__c product = BI_PC_TestMethodsUtility.getPCProduct(commercialProdId, 'Test Product', false);
        insert product;

        BI_PC_Product__c product2 = BI_PC_TestMethodsUtility.getPCProduct(commercialProdId, 'Test Product 2', false);
        insert product2;
        
		//create guideline rate
        Id commercialGuidelineRateId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_GLR_m_care', 'BI_PC_Guide_line_rate__c');
        BI_PC_Guide_line_rate__c guidelineRate = BI_PC_TestMethodsUtility.getPCGuidelineRate(commercialGuidelineRateId, product.Id, false);
        insert guidelineRate;

        //create proposal product
        Id proposedPropProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Proposed', 'BI_PC_Proposal_Product__c');
        BI_PC_Proposal_Product__c propProduct = BI_PC_TestMethodsUtility.getPCProposalProduct(proposedPropProdId, proposal.Id, product.Id, false);
        insert propProduct;

         BI_PC_Proposal_Product__c propProduct2 = BI_PC_TestMethodsUtility.getPCProposalProduct(proposedPropProdId, proposal.Id, product2.Id, false);
        insert propProduct2;
        
		//create scenarios
        Id futureScenarioId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Future', 'BI_PC_Scenario__c');
        scenario = BI_PC_TestMethodsUtility.getPCScenario(futureScenarioId, proposal.Id, propProduct.Id, guidelineRate.Id, false);
        scenario.BI_PC_Approval_status__c = 'Approval in Progress: Brand Marketing - Dir Contract Development';
        insert scenario;

        scenario2 = BI_PC_TestMethodsUtility.getPCScenario(futureScenarioId, proposal.Id, propProduct2.Id, guidelineRate.Id, false);
        scenario2.BI_PC_Approval_status__c = 'Approval in Progress: Brand Marketing - Dir Contract Development';
        insert scenario2;   
	}

	/******************************************************************************************************************
    * @date             23/07/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
	private static void dataSetUpGov(){
		analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
		
		Id accGovernmentId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_government', 'BI_PC_Account__c');
		BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accGovernmentId, 'Test MC Account', analyst, TRUE);
				
		Id commercialPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_government', 'BI_PC_Proposal__c');
		proposal = BI_PC_TestMethodsUtility.getPCProposal(commercialPropId, acc.Id, 'Approval in Progress: Government Compliance', Date.today().addDays(30), Date.today().addDays(40) , FALSE);
		proposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
		insert proposal;     

		//create product
        Id commercialProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prod_government', 'BI_PC_Product__c');
        BI_PC_Product__c product = BI_PC_TestMethodsUtility.getPCProduct(commercialProdId, 'Test Product', false);
        insert product;

        BI_PC_Product__c product2 = BI_PC_TestMethodsUtility.getPCProduct(commercialProdId, 'Test Product 2', false);
        insert product2;
        
		//create guideline rate
        Id commercialGuidelineRateId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_GLR_government', 'BI_PC_Guide_line_rate__c');
        BI_PC_Guide_line_rate__c guidelineRate = BI_PC_TestMethodsUtility.getPCGuidelineRate(commercialGuidelineRateId, product.Id, false);
        insert guidelineRate;

        //create proposal product
        Id proposedPropProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Proposed', 'BI_PC_Proposal_Product__c');
        BI_PC_Proposal_Product__c propProduct = BI_PC_TestMethodsUtility.getPCProposalProduct(proposedPropProdId, proposal.Id, product.Id, false);
        insert propProduct;

         BI_PC_Proposal_Product__c propProduct2 = BI_PC_TestMethodsUtility.getPCProposalProduct(proposedPropProdId, proposal.Id, product2.Id, false);
        insert propProduct2;
        
		//create proposal product
        Id futureScenarioId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Future', 'BI_PC_Scenario__c');
        scenario = BI_PC_TestMethodsUtility.getPCScenario(futureScenarioId, proposal.Id, propProduct.Id, guidelineRate.Id, false);
        scenario.BI_PC_Approval_status__c = 'Approval in Progress: Government Compliance';
        insert scenario;

        scenario2 = BI_PC_TestMethodsUtility.getPCScenario(futureScenarioId, proposal.Id, propProduct2.Id, guidelineRate.Id, false);
        scenario2.BI_PC_Approval_status__c = 'Approval in Progress: Government Compliance';
        insert scenario2;   
	}
	
	
    @isTest
	private static void updateScenarioStatusMC_test(){
		dataSetUpMC();

		String statusP1 = 'Approval in Progress: Pricing & Contracting';
		String statusP2 = 'Approval in Progress: Market Access';
		String statusP3 = 'Approval in Progress: PTC';

		Test.startTest();
		scenario.BI_PC_Approval_status__c = statusP1;
		update scenario;

		scenario2.BI_PC_Approval_status__c = statusP2;
		update scenario2;

		Test.stopTest();

		BI_PC_Proposal__c proposal = [SELECT Id, BI_PC_Status__c FROM BI_PC_Proposal__c LIMIT 1];
		system.assertEquals(statusP1, proposal.BI_PC_Status__c);

		scenario.BI_PC_Approval_status__c = statusP3;
		update scenario;

		proposal = [SELECT Id, BI_PC_Status__c FROM BI_PC_Proposal__c LIMIT 1];
		system.assertEquals(statusP2, proposal.BI_PC_Status__c);

		scenario2.BI_PC_Approval_status__c = statusP3;
		update scenario2;

		proposal = [SELECT Id, BI_PC_Status__c FROM BI_PC_Proposal__c LIMIT 1];
		system.assertEquals(statusP3, proposal.BI_PC_Status__c);

	}

	@isTest
	private static void updateScenarioStatusGOV_test(){
		dataSetUpGOV();

		String statusP1 = 'Approval in Progress: Dir Contract Compliance';
		String statusP2 = 'Approval in Progress: Pricing & Contracting';
		String statusP3 = 'Approval in Progress: PTC Sc';

		Test.startTest();
		scenario.BI_PC_Approval_status__c = statusP1;
		update scenario;

		scenario2.BI_PC_Approval_status__c = statusP2;
		update scenario2;

		Test.stopTest();

		BI_PC_Proposal__c proposal = [SELECT Id, BI_PC_Status__c FROM BI_PC_Proposal__c LIMIT 1];
		system.assertEquals(statusP1, proposal.BI_PC_Status__c);

		scenario.BI_PC_Approval_status__c = statusP3;
		update scenario;

		proposal = [SELECT Id, BI_PC_Status__c FROM BI_PC_Proposal__c LIMIT 1];
		system.assertEquals(statusP2, proposal.BI_PC_Status__c);

		scenario2.BI_PC_Approval_status__c = statusP3;
		update scenario2;

		proposal = [SELECT Id, BI_PC_Status__c FROM BI_PC_Proposal__c LIMIT 1];
		system.assertEquals(statusP3, proposal.BI_PC_Status__c);

	}


}