/***********************************************************************************************************
* @date 17/07/2018 (dd/mm/yyyy)
* @description PC Proposal Tigger Handler Class (iCon)
************************************************************************************************************/
public class BI_PC_ScenarioTriggerLogic {
	public static BI_PC_ScenarioTriggerLogic instance = null;
	private BI_PC_ScenarioTriggerLogic() {}
	public static BI_PC_ScenarioTriggerLogic getInstance() {
		if(instance == null) instance = new BI_PC_ScenarioTriggerLogic();
		return instance;
	}

    private final string REJECTED = 'Rejected';

    private Map<String,String> statusMap{
    	get{
    		if(statusMap == null){
    			statusMap = new Map<String,String>();
    			statusMap.put('Approval in Progress: Brand Marketing - Dir Contract Development','BM_DC_Develop');
				statusMap.put('Approval in Progress: PTC','PTC');
				statusMap.put('Approval in Progress: Dir Contract Development','Dir_Contract_Development');
				statusMap.put('Approval in Progress: Government Compliance','Government_Compliance');
				statusMap.put('Approval in Progress: MM Sales','MM_Sales');
				statusMap.put('Approval in Progress: Dir Contract Compliance','Dir_Contract_Compliance');
				statusMap.put('Approval in Progress: Brand Marketing - Dir Contract Compliance','BM_DC_Compliance');
				statusMap.put('Approval in Progress: Brand Marketing','Brand_Marketing');
				statusMap.put('Approval in Progress: Pricing & Contracting','Pricing_Contracting');
				statusMap.put('Approval in Progress: Market Access','Market_Access');
				statusMap.put('Approval in Progress: PTC Sc','PTC_Sc');
				statusMap.put('Rejected','Rejected');
				statusMap.put('Approved','Approved');
				statusMap.put('BM_DC_Develop','Approval in Progress: Brand Marketing - Dir Contract Development');
				statusMap.put('PTC','Approval in Progress: PTC');
				statusMap.put('Dir_Contract_Development','Approval in Progress: Dir Contract Development');
				statusMap.put('Government_Compliance','Approval in Progress: Government Compliance');
				statusMap.put('MM_Sales','Approval in Progress: MM Sales');
				statusMap.put('Dir_Contract_Compliance','Approval in Progress: Dir Contract Compliance');
				statusMap.put('BM_DC_Compliance','Approval in Progress: Brand Marketing - Dir Contract Compliance');
				statusMap.put('Brand_Marketing','Approval in Progress: Brand Marketing');
				statusMap.put('Pricing_Contracting','Approval in Progress: Pricing & Contracting');
				statusMap.put('Market_Access','Approval in Progress: Market Access');
				statusMap.put('PTC_Sc','Approval in Progress: PTC Sc');
			}
			return statusMap;
        }
        set;
    }
	
	/***********************************************************************************************************
	* @date 17/07/2018 (dd/mm/yyyy)
	* @description OnAfterUpdate logic calls
	************************************************************************************************************/
    public void checkRelatedScenariosByProposal(List<Id> proposalIds) {
    	 
		Map<Id,List<BI_PC_Scenario__c>> scenariosByProposal = new Map<Id,List<BI_PC_Scenario__c>>();
		List<BI_PC_Proposal__c> proposalsToUpdate = new List<BI_PC_Proposal__c>();
		Map<String,BI_PC_Proposal_status_order__mdt> psoByStatusMap = new Map<String,BI_PC_Proposal_status_order__mdt>();
		Map<Integer,String> managedCareByPosition = new Map<Integer,String>();
		Map<Integer,String> govByPosition = new Map<Integer,String>();


		for(BI_PC_Proposal_status_order__mdt pso: [SELECT Id, DeveloperName, BI_PC_Government_position__c, BI_PC_Managed_care_position__c FROM BI_PC_Proposal_status_order__mdt]){
			psoByStatusMap.put(pso.DeveloperName, pso);
			managedCareByPosition.put(Integer.valueOf(pso.BI_PC_Managed_care_position__c),pso.DeveloperName);
			govByPosition.put(Integer.valueOf(pso.BI_PC_Government_position__c),pso.DeveloperName);
		}

		//Retrieve all the scenarios related to the proposal and group them by proposal
		for(BI_PC_Scenario__c scenario: [SELECT Id, BI_PC_Proposal__c, BI_PC_Proposal__r.BI_PC_Status__c, BI_PC_Proposal__r.RecordType.DeveloperName, BI_PC_Approval_status__c FROM BI_PC_Scenario__c WHERE BI_PC_Proposal__c IN :proposalIds AND RecordType.DeveloperName = 'BI_PC_Future']){

			if(scenariosByProposal.get(scenario.BI_PC_Proposal__c) == NULL){
				scenariosByProposal.put(scenario.BI_PC_Proposal__c, new List<BI_PC_Scenario__c>());
			}
				scenariosByProposal.get(scenario.BI_PC_Proposal__c).add(scenario);
		}
		System.debug(LoggingLevel.INFO, 'BI_PC_ScenarioTriggerLogic.checkRelatedScenariosByProposal>>>>> scenariosByProposal: ' + scenariosByProposal);

		//We go through all the scenarios by proposal
		for(Id proposalId: scenariosByProposal.keySet()){

			Boolean changeStatus = TRUE;
			String proposalStatus = scenariosByProposal.get(proposalId).get(0).BI_PC_Proposal__r.BI_PC_Status__c;
			String propRT = scenariosByProposal.get(proposalId).get(0).BI_PC_Proposal__r.RecordType.DeveloperName;
			Integer statusPosition = returnStatusPosition(proposalStatus,psoByStatusMap,propRT);
			Integer lowerPosition = 0;

			for(BI_PC_Scenario__c sc: scenariosByProposal.get(proposalId)){

				Integer scenarioPosition = returnStatusPosition(sc.BI_PC_Approval_status__c,psoByStatusMap,propRT);
				//If we find any scenario with a status of the same position or lower, we don't change the proposal status
				if(scenarioPosition <= statusPosition){
					changeStatus = FALSE;
					break;
				//If we find any scenario with a high position status, we store the position and status which is lower in order to update the proposal
				}else if((lowerPosition == 0) || (scenarioPosition < lowerPosition)){
					lowerPosition = scenarioPosition;
				}
			}
			System.debug(LoggingLevel.INFO, 'BI_PC_ScenarioTriggerLogic.checkRelatedScenariosByProposal>>>>> changeStatus: ' + changeStatus);
			System.debug(LoggingLevel.INFO, 'BI_PC_ScenarioTriggerLogic.checkRelatedScenariosByProposal>>>>> lowerPosition: ' + lowerPosition);

			if(changeStatus && lowerPosition > 0){
				proposalStatus = getStatusByPosition(lowerPosition,managedCareByPosition,govByPosition,propRT);
				BI_PC_Proposal__c proposalToUpdate = new BI_PC_Proposal__c(Id = proposalId, BI_PC_Status__c = proposalStatus);
				proposalsToUpdate.add(proposalToUpdate);
			}

			System.debug(LoggingLevel.INFO, 'BI_PC_ScenarioTriggerLogic.checkRelatedScenariosByProposal>>>>> proposalsToUpdate: ' + proposalsToUpdate);
		
			if(!proposalsToUpdate.isEmpty()){
				update proposalsToUpdate;
			}
			
		}
	}

	
	/***********************************************************************************************************
	* @date 23/07/2018 (dd/mm/yyyy)
	* @description This method returns the status position when the status value is provided
	************************************************************************************************************/
    public Integer returnStatusPosition(String status, Map<String,BI_PC_Proposal_status_order__mdt> csMap, String devName){
    	String cmName = (statusMap.get(status) != NULL) ? statusMap.get(status) : '';

    	BI_PC_Proposal_status_order__mdt pso;
    	if(csMap.containsKey(cmName)){
    		pso = new BI_PC_Proposal_status_order__mdt();
    		pso = csMap.get(cmName);
    	}

    	if(pso != NULL && devName == 'BI_PC_Prop_government'){
    		return Integer.valueOf(pso.BI_PC_Government_position__c);
    	} else if((pso != NULL) && (devName == 'BI_PC_Prop_m_care')){
    		return Integer.valueOf(pso.BI_PC_Managed_care_position__c);
    	} else{
    		return 0;
    	}
    }

    /***********************************************************************************************************
	* @date 23/07/2018 (dd/mm/yyyy)
	* @description This method returns the status value when the status position is provided
	************************************************************************************************************/
    public String getStatusByPosition(Integer position, Map<Integer,String> managedCareMap, Map<Integer,String> govMap, String devName){
    	
    	String cmName = '';

    	if(devName == 'BI_PC_Prop_government' && govMap.containsKey(position)){
    		cmName = govMap.get(position);
    	} else if(devName == 'BI_PC_Prop_m_care' && govMap.containsKey(position)){
    		cmName = managedCareMap.get(position);
    	}

    	System.debug(LoggingLevel.INFO, 'BI_PC_ScenarioTriggerLogic.getStatusByPosition>>>>> cmName: ' + cmName);

    	if(!String.isBlank(cmName)){
    		return (statusMap.get(cmName) != NULL) ? statusMap.get(cmName) : '';
    	} else{
    		return '';
    	}

    }
}