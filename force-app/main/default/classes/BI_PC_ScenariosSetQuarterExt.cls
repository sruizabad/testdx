/***********************************************************************************************************
* @date 30/07/2018 (dd/mm/yyyy)
* @description Extension class for the BI_PC_ScenariosSetQuarter visualforce page
************************************************************************************************************/
public class BI_PC_ScenariosSetQuarterExt {

	public static Boolean scenarioError {get; set;}	//flag to show scenarios error message
    public static String scenarioErrorMessage {get; set;}	//error text
    public String infoMessage {get;set;} //info text
    private Set<String> prodWithGL; //list of products with any guideline
    public static Boolean loadCurrentScenarios = FALSE; //flag used to know when to search scenarios in database


    public Id currentUserId {
    	get{
    		return UserInfo.getUserId();
    	}
    	set;
    }
    private Map<Id, List<BI_PC_Scenario__c>> scenariosMap{ //All the scenarios grouped by product
        get {
                return this.scenariosMap;
        } 
        set;
    }
    private Map<Id, BI_PC_Scenario__c> currentScenariosMap; //current scenarios with the scenario id as key
    public static Id proposalId {
        get {
            return ApexPages.currentPage().getParameters().get('id');
        } 
        set;
    }	//id of the proposal

    public List<SelectOption> quarterOptions {//picklist with the available quarters
        get {
            
            return this.quarterOptions;
        } 
        set;
    }	//list of quarters to be displayed

    //List of current scenarios which will be displayed
    public List<BI_PC_Scenario__c> currentScenariosList {
        get {

        	if(currentScenariosList == NULL || loadCurrentScenarios){

        		//initialize variables
        		//infoMessage = '';
        		//prodWithGL = new Set<String>();
	    		currentScenariosList = new List<BI_PC_Scenario__c>();
		        scenariosMap = new Map<Id, List<BI_PC_Scenario__c>>();
		        currentScenariosMap = new Map<Id,BI_PC_Scenario__c>();
	            
	            //Retrieve all the scenarios related to the proposals
	            List<BI_PC_Scenario__c> scList= new List<BI_PC_Scenario__c>([SELECT Id, Name, BI_PC_Product_Description__c, BI_PC_Position__c, RecordType.DeveloperName, 
															                	BI_PC_Guide_line__c, BI_PC_Out_of_GL__c, BI_PC_Price_prot__c, BI_PC_Proposal__c, BI_PC_Rate__c, BI_PC_Comment__c, BI_PC_Guideline_quarter__c,
															                	BI_PC_Prop_product__c, BI_PC_Prop_product__r.BI_PC_Product__c, BI_PC_GL_assigned__c,BI_PC_GL_rate__c
															                FROM BI_PC_Scenario__c 
															                WHERE BI_PC_Proposal__c = :proposalId]);

	            
            	for(BI_PC_Scenario__c sc: scList){

            		//Store the current scenarios which still weren't assigned to any guideline
            		if(sc.RecordType.DeveloperName == 'BI_PC_Current'){
		        		currentScenariosList.add(sc);
		        		currentScenariosMap.put(sc.BI_PC_Prop_product__r.BI_PC_Product__c,sc);

			        	//Store the list of products whose scenarios already have any guideline in order to show them in the info message
			        	/*if(sc.RecordType.DeveloperName == 'BI_PC_Current' && sc.BI_PC_GL_assigned__c == TRUE){
			        		prodWithGL += sc.BI_PC_Product_Description__c + ', ';
			        	}*/
		        	}

		        	//Store scenarios grouped by product
		        	if(!scenariosMap.containsKey(sc.BI_PC_Prop_product__r.BI_PC_Product__c)){
		        		scenariosMap.put(sc.BI_PC_Prop_product__r.BI_PC_Product__c,new List<BI_PC_Scenario__c>());
		        	}
		        	scenariosMap.get(sc.BI_PC_Prop_product__r.BI_PC_Product__c).add(sc);       
		        }
		    }

		    //If there are some products with guideline, an info message will be displayed
		    /*
		    if(!String.isBlank(prodWithGL)){
		    	infoMessage = Label.BI_PC_ScenariosInfoMessage + ' ' + prodWithGL.substring(0,prodWithGL.length()-2);
		    }*/
		    
		    System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosSetQuarterExt>>>>>currentScenariosList: ' + currentScenariosList);
            
	        return currentScenariosList;
        } 
        set;
    }

    //Flag to know if there are some available scenarios
    public Boolean emptyList {
    	get {
    		if(currentScenariosList == NULL || currentScenariosList.isEmpty()){
    			return true;
    		}else{
    			return false;
    		}

    	}
    	set;
    }

    //Number of quarters to be displayed in the quarter picklist
    private static final Integer QUARTERS = 5;

    /***********************************************************************************************************
	* @date 30/07/2018 (dd/mm/yyyy)
	* @description Constructor method
	************************************************************************************************************/
    public BI_PC_ScenariosSetQuarterExt(ApexPages.StandardController ctrl) {
        
        scenarioError = false;	//initialize error flag
        
        
        quarterOptions = getQuarterOptions();
        
    }

    /***********************************************************************************************************
	* @date 31/07/2018 (dd/mm/yyyy)
	* @description This method retrieve the picklist with the different quarters
	* @return List<SelectOption> options
	************************************************************************************************************/
    private static List<SelectOption> getQuarterOptions() {
    		Date today = Date.today();

    		//get current quarter and year
     		Integer currentYear = today.year();
            Integer currentQuarter = (today.month() / 3) + 1;

            List<SelectOption> options = new List<SelectOption>();

            //Starting from the current quarter, we store the 4 previous quarters
            //First we retrieve the previous quarters from the current year
            Integer counter = 0;
           	for(Integer i = currentQuarter; i > 0 ; i-- ){
            	if(counter == QUARTERS){ //Max number of quarters that we want to retrieve (current quarter + 4)
            		break;
            	}

            	String sOption = String.valueOf(i) + 'Q' + String.valueOf(currentYear);
            	options.add(new SelectOption(sOption,sOption));

            	counter++;
			}

			//Once we get all the previous quarters from the current year, in case we still don't have the max number of quarters, we go through one year before
			Integer quarterCounter = 4;
			for(Integer i = counter; i < QUARTERS; i++){

				String sOption = String.valueOf(quarterCounter) + 'Q' + String.valueOf(currentYear-1);
            	options.add(new SelectOption(sOption,sOption));
            	quarterCounter --;
			}

            return options;
	}

	/***********************************************************************************************************
	* @date 31/07/2018 (dd/mm/yyyy)
	* @description Save quarters and assign guidelines
	* @return void
	************************************************************************************************************/
    public void submitQuarter() {
    	
    	Map<Id,BI_PC_Scenario__c> scenariosToUpdate = new Map<Id,BI_PC_Scenario__c>();
    	Set<Id> prodIds = new Set<Id>();
    	Map<Id,String> errorProductSet = new Map<Id,String>();
    	Map<Id,BI_PC_Scenario__c> errorScToUpdate = new Map<Id,BI_PC_Scenario__c>();
    	Set<String> quarterSet = new Set<String>();
    	Set<String> positionSet = new Set<String>();
    	Map<String,Id> glMap = new Map<String,Id>();
    	infoMessage = '';
        prodWithGL = new Set<String>();

    	try{
    		//Go through all the current scenarios
	    	for(BI_PC_Scenario__c cs: currentScenariosList){
	    		quarterSet.add(cs.BI_PC_Guideline_quarter__c);
	    		//assign the quarter to all the related scenarios
	    		for(BI_PC_Scenario__c sToUpd: scenariosMap.get(cs.BI_PC_Prop_product__r.BI_PC_Product__c)){
	    			sToUpd.BI_PC_Guideline_quarter__c = cs.BI_PC_Guideline_quarter__c;
	    			system.debug('sToUpd: '+sToUpd);
	    			scenariosToUpdate.put(sToUpd.Id,sToUpd);
	    			positionSet.add(sToUpd.BI_PC_Position__c);
	    			prodIds.add(cs.BI_PC_Prop_product__r.BI_PC_Product__c);
				}
	    	}

	    	System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosSetQuarterExt>>>>>submitQuarter:positionSet ' + positionSet);
	    	System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosSetQuarterExt>>>>>submitQuarter:quarterSet ' + quarterSet);
	    	System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosSetQuarterExt>>>>>submitQuarter:prodIds ' + prodIds);

	    	//Fetch guidelines related to that quarter, product and position
	    	for(BI_PC_Guide_line_rate__c guideLine: [SELECT Id, BI_PC_Position__c, BI_PC_Quarter__c, BI_PC_Product__c 
	    											 FROM BI_PC_Guide_line_rate__c 
	    											 WHERE Recordtype.DeveloperName = 'BI_PC_GLR_government' 
	    											 AND BI_PC_Position__c IN: positionSet
	    											 AND BI_PC_Quarter__c IN: quarterSet
	    											 AND BI_PC_Product__c IN: prodIds]){

	    		String uniqueKey = guideLine.BI_PC_Product__c + guideLine.BI_PC_Position__c + guideLine.BI_PC_Quarter__c;

	    		//group guideline by product + position + quarter
	    		glMap.put(uniqueKey,guideLine.Id);
	    	}

	    	
	    	//assign guideline
	    	for(BI_PC_Scenario__c sc: scenariosToUpdate.values()){

	    		//Update just future scenarios
	    		if(sc.RecordType.DeveloperName == 'BI_PC_Future'){
	    			String uniqueKey = sc.BI_PC_Prop_product__r.BI_PC_Product__c + sc.BI_PC_Position__c + sc.BI_PC_Guideline_quarter__c;
	    			System.debug(LoggingLevel.INFO, 'BI_PC_ScenariosSetQuarterExt>>>>>submitQuarter:uniqueKey ' + uniqueKey);
	    			//If we find a guideline, we assign it to the future scenarios
		    		if(glMap.containsKey(uniqueKey)){
		    			sc.BI_PC_GL_rate__c = glMap.get(uniqueKey);

		    			//If we assigned a guideline, we search the related current scenario and select the "Guideline assigned" flag
		    			Id currentScenarioId = currentScenariosMap.get(sc.BI_PC_Prop_product__r.BI_PC_Product__c).Id;
		    			scenariosToUpdate.get(currentScenarioId).BI_PC_GL_assigned__c = TRUE;

		    			prodWithGL.add(scenariosToUpdate.get(currentScenarioId).BI_PC_Product_Description__c);

		    		//If we cannot find a guideline don't update any of the scenarios related to that product
		    		} else {

		    			//If the scenario had a guideline added previously, we remove it
		    			Id currentScenarioId = currentScenariosMap.get(sc.BI_PC_Prop_product__r.BI_PC_Product__c).Id;
		    			
						//if(scenariosToUpdate.get(currentScenarioId).BI_PC_GL_assigned__c == TRUE || sc.BI_PC_GL_rate__c != NULL){
						if(!String.isBlank(sc.BI_PC_Guideline_quarter__c)){

							//Clean previous guideline data
		    				scenariosToUpdate.get(currentScenarioId).BI_PC_GL_assigned__c = FALSE;
		    				scenariosToUpdate.get(currentScenarioId).BI_PC_Guideline_quarter__c = '';

		    				//Store the error records (no guideline) that need to be updated
		    				errorScToUpdate.put(currentScenarioId,scenariosToUpdate.get(currentScenarioId));
		    				sc.BI_PC_Guideline_quarter__c='';
		    				sc.BI_PC_GL_rate__c = NULL;
		    				errorScToUpdate.put(sc.Id,sc);
		    			}		    			

		    			errorProductSet.put(sc.BI_PC_Prop_product__r.BI_PC_Product__c, sc.BI_PC_Product_Description__c);
		    		}
	    		}
	    	}

	    	//If there are some scenarios with no guideline (it wasn't found), we show a warning message
	    	if(!errorProductSet.isEmpty()){
	    		loadCurrentScenarios = TRUE;
	    		scenarioErrorMessage = Label.BI_PC_NoGuidelineFound;
		    	scenarioError = true;
		    		
		    	//Remove the scenarios whith no changes from the list to be updated
	    		for(Id prodId: errorProductSet.keySet()){
	    			scenarioErrorMessage += ' ' + errorProductSet.get(prodId) + ',';
	    			for(BI_PC_Scenario__c sError: scenariosMap.get(prodId)){

	    				//If the scenario didn't have any guideline assigned before so don't need to be updated
	    				if(!errorScToUpdate.containsKey(sError.Id)){
	    					scenariosToUpdate.remove(sError.Id);
	    				}
		    		}
				}

				scenarioErrorMessage = scenarioErrorMessage.substring(0,scenarioErrorMessage.length()-1);
			}

			if(!prodWithGL.isEmpty()){
				infoMessage = Label.BI_PC_ScenariosInfoMessage + ' ' + prodWithGL;
				infoMessage = infoMessage.replace('{','').replace('}','');
			}

  		    if(!scenariosToUpdate.isEmpty()){
  		    	loadCurrentScenarios = TRUE;
	    		update scenariosToUpdate.values();
	    	}

	    	
	    } catch(Exception exp) {
	    	system.debug('ERROR:: '+exp.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'submitQuarter Error' + exp.getMessage()));
        }
    }

    

}