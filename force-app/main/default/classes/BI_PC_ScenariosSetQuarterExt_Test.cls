/***********************************************************************************************************
* @date 26/07/2018 (dd/mm/yyyy)
* @description Test class for the BI_PC_ScenariosApproveRejectExt class
************************************************************************************************************/
@isTest
public class BI_PC_ScenariosSetQuarterExt_Test {

	private static BI_PC_Scenario__c currentScenario1;
	private static BI_PC_Scenario__c currentScenario2;
	private static BI_PC_Scenario__c currentScenario3;
	private static BI_PC_Scenario__c futureScenario1;
	private static BI_PC_Scenario__c futureScenario2;
	private static BI_PC_Proposal__c proposal;
	private static BI_PC_Proposal__c emptyProposal;
	private static BI_PC_Guide_line_rate__c guidelineRate1;
	private static BI_PC_Guide_line_rate__c guidelineRate2;
	private static User analyst;

	/******************************************************************************************************************
    * @date             23/07/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
	private static void dataSetUpEmptyProp(){
		analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
		
		Id accgovId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_government', 'BI_PC_Account__c');
		BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accgovId, 'Test Account', analyst, TRUE);
				
		Id govPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_government', 'BI_PC_Proposal__c');
		emptyProposal = BI_PC_TestMethodsUtility.getPCProposal(govPropId, acc.Id, 'Business Case in Development', Date.today().addDays(30), Date.today().addDays(40) , FALSE);
		emptyProposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
		insert emptyProposal;     
	}
	/******************************************************************************************************************
    * @date             23/07/2018 (dd/mm/yyyy)  
    * @description      Setup the test data. 
    *******************************************************************************************************************/
	private static void dataSetUp(){

		analyst = BI_PC_TestMethodsUtility.insertUser('System Administrator', 0);
		
		Id accgovId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Acc_government', 'BI_PC_Account__c');
		BI_PC_Account__c acc = BI_PC_TestMethodsUtility.getPCAccount(accgovId, 'Test Account', analyst, TRUE);
				
		Id govPropId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prop_government', 'BI_PC_Proposal__c');
		proposal = BI_PC_TestMethodsUtility.getPCProposal(govPropId, acc.Id, 'Business Case in Development', Date.today().addDays(30), Date.today().addDays(40) , FALSE);
		proposal.BI_PC_Analyst__c = analyst.FirstName + ' ' + analyst.LastName;
		insert proposal;     
		
		//create product
        Id govProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Prod_government', 'BI_PC_Product__c');
        BI_PC_Product__c product = BI_PC_TestMethodsUtility.getPCProduct(govProdId, 'Test Product', false);
        insert product;

        BI_PC_Product__c product2 = BI_PC_TestMethodsUtility.getPCProduct(govProdId, 'Test Product 2', false);
        insert product2;

        BI_PC_Product__c product3 = BI_PC_TestMethodsUtility.getPCProduct(govProdId, 'Test Product 3', false);
        insert product3;
        
		//create guideline rate
        Id govRateId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_GLR_government', 'BI_PC_Guide_line_rate__c');
        guidelineRate1 = BI_PC_TestMethodsUtility.getPCGuidelineRate(govRateId, product.Id, false);
        guidelineRate1.BI_PC_Quarter__c = '1Q2018';
        insert guidelineRate1;

        guidelineRate2 = BI_PC_TestMethodsUtility.getPCGuidelineRate(govRateId, product2.Id, false);
        guidelineRate2.BI_PC_Quarter__c = '2Q2018';
        insert guidelineRate2;

        
        //create proposal product
        Id currentProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Current_position', 'BI_PC_Proposal_Product__c');
        BI_PC_Proposal_Product__c currProduct1 = BI_PC_TestMethodsUtility.getPCProposalProduct(currentProdId, proposal.Id, product.Id, false);
        insert currProduct1;

        BI_PC_Proposal_Product__c currProduct2 = BI_PC_TestMethodsUtility.getPCProposalProduct(currentProdId, proposal.Id, product2.Id, false);
        insert currProduct2;

        BI_PC_Proposal_Product__c currProduct3 = BI_PC_TestMethodsUtility.getPCProposalProduct(currentProdId, proposal.Id, product3.Id, false);
        insert currProduct3;

        //create proposal product
        Id proposedPropProdId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Proposed', 'BI_PC_Proposal_Product__c');
        BI_PC_Proposal_Product__c propProduct = BI_PC_TestMethodsUtility.getPCProposalProduct(proposedPropProdId, proposal.Id, product.Id, false);
        insert propProduct;

         BI_PC_Proposal_Product__c propProduct2 = BI_PC_TestMethodsUtility.getPCProposalProduct(proposedPropProdId, proposal.Id, product2.Id, false);
        insert propProduct2;
        
        //create current scenarios
        Id currentScenarioId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Current', 'BI_PC_Scenario__c');
        currentScenario1 = BI_PC_TestMethodsUtility.getPCScenario(currentScenarioId, proposal.Id, currProduct1.Id, NULL, false);
        currentScenario1.BI_PC_Guideline_quarter__c = '1Q2018';
        currentScenario1.BI_PC_Position__c = 'On PDL';
        insert currentScenario1;

        currentScenario2 = BI_PC_TestMethodsUtility.getPCScenario(currentScenarioId, proposal.Id, currProduct2.Id, NULL, false);
        currentScenario2.BI_PC_Guideline_quarter__c = '4Q2017';
        currentScenario2.BI_PC_Position__c = 'On PDL';
        insert currentScenario2;

        //current scenario assigned to a guideline
        currentScenario3 = BI_PC_TestMethodsUtility.getPCScenario(currentScenarioId, proposal.Id, currProduct3.Id, NULL, false);
        currentScenario3.BI_PC_GL_assigned__c = TRUE;
        currentScenario3.BI_PC_Guideline_quarter__c = '3Q2018';
        currentScenario3.BI_PC_Position__c = 'On PDL';
        insert currentScenario3;

        
		//create future scenarios
        Id futureScenarioId = BI_PC_TestMethodsUtility.getRecordTypeByDeveloperName('BI_PC_Future', 'BI_PC_Scenario__c');
        futureScenario1 = BI_PC_TestMethodsUtility.getPCScenario(futureScenarioId, proposal.Id, propProduct.Id, NULL, false);
        futureScenario1.BI_PC_Position__c = 'On PDL';
        insert futureScenario1;

        futureScenario2 = BI_PC_TestMethodsUtility.getPCScenario(futureScenarioId, proposal.Id, propProduct2.Id, NULL, false);
        futureScenario2.BI_PC_Position__c = 'On PDL';
        insert futureScenario2;   
	}

	@isTest
	private static void scenariosSetQuarterController_test(){
		dataSetUp();
		
		Test.startTest();

		PageReference pageRef = Page.BI_PC_ScenariosSetQuarter;
		pageRef.getParameters().put('id', String.valueOf(proposal.Id));
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController sc = new ApexPages.StandardController(proposal);
		BI_PC_ScenariosSetQuarterExt sQContr = new BI_PC_ScenariosSetQuarterExt(sc);

		sQContr.submitQuarter();

		BI_PC_Scenario__c sctest = [SELECT Id, BI_PC_GL_rate__c FROM BI_PC_Scenario__c WHERE Id =: futureScenario1.ID];
		BI_PC_Scenario__c sctest2 = [SELECT Id, BI_PC_GL_rate__c FROM BI_PC_Scenario__c WHERE Id =: futureScenario2.ID];
		system.assertEquals(guidelineRate1.Id, sctest.BI_PC_GL_rate__c);
		system.assertEquals(NULL, sctest2.BI_PC_GL_rate__c);
		
		Test.stopTest();

		
	}

	@isTest
	private static void submitQuarterException_test(){
		dataSetUp();
		
		Test.startTest();

		PageReference pageRef = Page.BI_PC_ScenariosSetQuarter;
		pageRef.getParameters().put('id', String.valueOf(proposal.Id));
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController sc = new ApexPages.StandardController(proposal);
		BI_PC_ScenariosSetQuarterExt sQContr = new BI_PC_ScenariosSetQuarterExt(sc);

		List<BI_PC_Scenario__c> sList = new List<BI_PC_Scenario__c>();
		BI_PC_Scenario__c scenario = new BI_PC_Scenario__c(Id =currentScenario1.Id, BI_PC_Guideline_quarter__c = '2');
		sList.add(scenario);
		sQContr.currentScenariosList = sList;

		sQContr.submitQuarter();

		system.assertEquals(FALSE,sQcontr.emptyList);
		BI_PC_Scenario__c sctest = [SELECT Id, BI_PC_GL_rate__c FROM BI_PC_Scenario__c WHERE Id =: futureScenario1.ID];
		BI_PC_Scenario__c sctest2 = [SELECT Id, BI_PC_GL_rate__c FROM BI_PC_Scenario__c WHERE Id =: futureScenario2.ID];
		system.assertEquals(NULL, sctest.BI_PC_GL_rate__c);
		system.assertEquals(NULL, sctest2.BI_PC_GL_rate__c);

		Test.stopTest();
	}

	@isTest
	private static void otherParameters_test(){
		dataSetUpEmptyProp();

		Test.startTest();

		PageReference pageRef = Page.BI_PC_ScenariosSetQuarter;
		pageRef.getParameters().put('id', String.valueOf(emptyProposal.Id));
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController sc = new ApexPages.StandardController(emptyProposal);
		BI_PC_ScenariosSetQuarterExt sQContr = new BI_PC_ScenariosSetQuarterExt(sc);

		//Test Empty List
		sQContr.submitQuarter();
		system.assertEquals(TRUE,sQcontr.emptyList);

		//Test user parameter
		Id userId = sQContr.currentUserId;
		system.assertEquals(userId,UserInfo.getUserId());
		//Test quarter picklist
		List<SelectOption> qOpt = sQContr.quarterOptions;
		system.assertEquals(5, qOpt.size());
		
		Test.stopTest();
	}
	

}