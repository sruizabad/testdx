/*********************************************************************************************************************
* @date 12/06/2018 (dd/mm/yyyy)  
* @description This class contains sample records for each SObject type in order to pool apex TestMethod creation
***********************************************************************************************************************/
@isTest
public class BI_PC_TestMethodsUtility {

	/******************************************************************************************************************
    * @date             12/06/2018 (dd/mm/yyyy)  
    * @description      The method retrieves the account record. This record will be inserted directly 
    					depending on the insertAcc flag. 
    *******************************************************************************************************************/   
	public static BI_PC_Account__c getPCAccount(Id rtId, String name, User analyst, Boolean insertAcc){
		BI_PC_Account__c acc = new BI_PC_Account__c();
		acc.Name = name;
		acc.RecordTypeId = rtId;
		acc.BI_PC_Group__c = 'Group 1';
		acc.BI_PC_Contract_analyst__c = analyst.Id;
        acc.BI_PC_Ram_nad__c = analyst.Id;
		if(insertAcc){
			insert acc;
		}

		return acc; 
	}

	/******************************************************************************************************************
    * @date             30/07/2018 (dd/mm/yyyy)  
    * @description      The method creates a new question. This record will be inserted directly 
    					depending on the insertRecord flag.  
    *******************************************************************************************************************/   
    public static BI_PC_Question__c getPCQuestion(String qType, Boolean insertRecord) {
        
        BI_PC_Question__c newQuestion = new BI_PC_Question__c(BI_PC_Question_type__c = qType, BI_PC_Question__c = 'Test Question');
        
        if(insertRecord){
			insert newQuestion;
		}
        
        return newQuestion;
    }
    
	/******************************************************************************************************************
    * @date             30/07/2018 (dd/mm/yyyy)  
    * @description      The method creates a new answer. This record will be inserted directly 
    					depending on the insertRecord flag.  
    *******************************************************************************************************************/   
    public static BI_PC_Answer__c getPCAnswer(Id questionId, Id proposalId, Boolean insertRecord) {
                
        BI_PC_Answer__c newAnswer = new BI_PC_Answer__c(BI_PC_Answer__c = 'Test answer', BI_PC_Question__c = questionId, BI_PC_Proposal__c = proposalId);
        
        if(insertRecord){
			insert newAnswer;
		}
        
        return newAnswer;
    }
    
	/******************************************************************************************************************
    * @date             12/06/2018 (dd/mm/yyyy)  
    * @description      The method retrieves the proposal record. This record will be inserted directly 
    					depending on the insertProp flag. 
    *******************************************************************************************************************/
	public static BI_PC_Proposal__c getPCProposal(Id rtId, Id accountId, String status, Date endDate, Date dueDate, Boolean insertProp){
		BI_PC_Proposal__c prop = new BI_PC_Proposal__c();
		prop.RecordTypeId = rtId;
		prop.BI_PC_Due_date__c = dueDate;
		prop.BI_PC_Status__c = status;
		prop.BI_PC_Account__c = accountId;
		prop.BI_PC_Start_date__c = Date.today(); 
		prop.BI_PC_End_date__c = endDate;
		prop.BI_PC_Admin_fee__c = 20;
		prop.BI_PC_Priority_level__c = 'Normal';


		if(insertProp){
			insert prop;
		}

		return prop; 
	}
    
	/******************************************************************************************************************
    * @date             12/07/2018 (dd/mm/yyyy)  
    * @description      The method retrieves the product record. This record will be inserted directly 
    					depending on the insertRecord flag. 
    *******************************************************************************************************************/
	public static BI_PC_Product__c getPCProduct(Id rtId, String prodName, Boolean insertRecord){
		BI_PC_Product__c product = new BI_PC_Product__c();
		product.RecordTypeId = rtId;
        product.Name = prodName;
        product.BI_PC_Long_description__c = prodName;
        product.BI_PC_NDC_code__c = prodName;
        product.BI_PC_Brand_payer_lead__c = prodName;


		if(insertRecord){
			insert product;
		}

		return product; 
	}

	/******************************************************************************************************************
    * @date             12/07/2018 (dd/mm/yyyy)  
    * @description      The method retrieves the guideline rate record. This record will be inserted directly 
    					depending on the insertRecord flag. 
    *******************************************************************************************************************/
	public static BI_PC_Guide_line_rate__c getPCGuidelineRate(Id rtId, Id productId, Boolean insertRecord){
        
        String rtName = [SELECT DeveloperName FROM RecordType WHERE Id = :rtId].DeveloperName;
        
		BI_PC_Guide_line_rate__c guidelineRate = new BI_PC_Guide_line_rate__c();
		guidelineRate.RecordTypeId = rtId;
        guidelineRate.BI_PC_Product__c = productId;
        guidelineRate.BI_PC_Max_guideline_rebate__c = 90;
        guidelineRate.BI_PC_Position__c = (rtName == 'BI_PC_GLR_government') ? 'On PDL' : 'Other';
        guidelineRate.BI_PC_Group__c = 'Group 1';
        guidelineRate.BI_PC_Quarter_start_date__c = System.Today();
        guidelineRate.BI_PC_Quarter_end_date__c = System.Today().addDays(30);
        
        

		if(insertRecord){
			insert guidelineRate;
		}

		return guidelineRate; 
	}
	/******************************************************************************************************************
    * @date             12/07/2018 (dd/mm/yyyy)  
    * @description      The method retrieves the proposal product record. This record will be inserted directly 
    					depending on the insertRecord flag. 
    *******************************************************************************************************************/
	public static BI_PC_Proposal_Product__c getPCProposalProduct(Id rtId, Id proposalId, Id productId, Boolean insertRecord){
		BI_PC_Proposal_Product__c propProduct = new BI_PC_Proposal_Product__c();
		propProduct.RecordTypeId = rtId;
		propProduct.BI_PC_Proposal__c = proposalId;
        propProduct.BI_PC_Product__c = productId;
        propProduct.BI_PC_Position__c = 'On PDL';

		if(insertRecord){
			insert propProduct;
		}

		return propProduct; 
	}
    
	/******************************************************************************************************************
    * @date             12/07/2018 (dd/mm/yyyy)  
    * @description      The method retrieves the scenario record. This record will be inserted directly 
    					depending on the insertRecord flag. 
    *******************************************************************************************************************/
	public static BI_PC_Scenario__c getPCScenario(Id rtId, Id proposalId, Id propProductId, Id guidelineRateId, Boolean insertRecord){

        BI_PC_Scenario__c scenario = new BI_PC_Scenario__c();
		scenario.RecordTypeId = rtId;
		scenario.BI_PC_Proposal__c = proposalId;
        scenario.BI_PC_Prop_product__c = propProductId;
        scenario.BI_PC_GL_rate__c = guidelineRateId;

        if(insertRecord){
			insert scenario;
		}

		return scenario; 
	}
    
	/******************************************************************************************************************
    * @date             12/06/2018 (dd/mm/yyyy)  
    * @description      The method inserts the user record. Once the user was inserted, he is added to a permission set.
    *******************************************************************************************************************/
	public static User insertUser(String profName, String permissionSetName, Integer counter) {

        String profileId = [SELECT Id FROM Profile WHERE Name = :profName LIMIT 1].Id;

         User userInfo = new User(FirstName = 'Test' + counter,LastName = 'Test',Alias = 'AU' + counter, Email = 'test.apex' + counter + '@email.es', Username = 'testAdmin' + counter + '@email.es', EmailEncodingKey='ISO-8859-1', TimeZoneSidKey = 'Europe/Paris',
                             LocaleSidKey = 'en_GB', ProfileId = profileId, LanguageLocaleKey = 'en_US');

        insert userInfo;

        List<PermissionSet> pSet = new List<PermissionSet>([SELECT Id, Name FROM PermissionSet WHERE Name =: permissionSetName]);
        if(!pSet.isEmpty()){
        	Id pSetId = pSet.get(0).Id;

        	PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = pSetId, AssigneeId = userInfo.Id);
			insert psa; 
        }

        return userInfo;
    }

    /******************************************************************************************************************
    * @date             12/06/2018 (dd/mm/yyyy)  
    * @description      The method inserts the user record (with no permission set)
    *******************************************************************************************************************/
	public static User insertUser(String profName, Integer counter) {

        String profileId = [SELECT Id FROM Profile WHERE Name = :profName LIMIT 1].Id;

        User userInfo = new User(FirstName = 'Test' + counter,LastName = 'Test',Alias = 'AU' + counter, Email = 'test.apex' + counter + '@email.es', Username = 'testAdmin' + counter + '@email.es', EmailEncodingKey='ISO-8859-1', TimeZoneSidKey = 'Europe/Paris',
                             LocaleSidKey = 'en_GB', ProfileId = profileId, LanguageLocaleKey = 'en_US');

        insert userInfo;

        return userInfo;
        
    }

    /******************************************************************************************************************
    * @date             26/07/2018 (dd/mm/yyyy)  
    * @description      The method inserts the GroupMember record
    *******************************************************************************************************************/
    public static GroupMember insertGroupMember(String groupName, Id userId) {

        Id groupId = [SELECT Id FROM Group WHERE Type != 'Queue' AND DeveloperName = :groupName LIMIT 1].Id;
        
        GroupMember grMember = new GroupMember(GroupId = groupId, UserOrGroupId = userId);
        
        insert grMember;
        
        return grMember;
    }
    
	/******************************************************************************************************************
    * @date             12/06/2018 (dd/mm/yyyy)  
    * @description      The method retrieves the recordtype Id of a given record type identified providing the 
                        RecordType DeveloperName and associate sObject Name. Using DeveloperName instead of Name 
                        avoids issues due to translations or Name changes
    *******************************************************************************************************************/
    public static Id getRecordTypeByDeveloperName(String devName, String objectName) {
        Map<String, RecordType> recordTypesForSpecificObject;
        if (!recordTypesByObjectByDevName.containsKey(objectName)) {
            recordTypesForSpecificObject = new Map<String, RecordType>(); 
            
            List<RecordType> recTypes = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType= :objectName];
            for (RecordType rt:recTypes) {
                recordTypesForSpecificObject.put(rt.DeveloperName, rt);  
            }                  
            recordTypesByObjectByDevName.put(objectName, recordTypesForSpecificObject);
        } else {
            recordTypesForSpecificObject = recordTypesByObjectByDevName.get(objectName);
        }
        
        if(recordTypesForSpecificObject.containsKey(devName)) {
            return recordTypesForSpecificObject.get(devName).Id;  
        }
        return null;  
    }
    //Cache of the RecordTypes
    private static Map<String,Map<String, RecordType>> recordTypesByObjectByDevName = new Map<String,Map<String, RecordType>>();

    /******************************************************************************************************************
    * @date             23/07/2018 (dd/mm/yyyy)  
    * @description      The method creates BI_PC_Proposal_status_order__mdt records
    *******************************************************************************************************************/
    /*public static void insertProposalStatusOrderObject(){

        List<BI_PC_Proposal_status_order__mdt> psoList = new List<BI_PC_Proposal_status_order__mdt>();

        BI_PC_Proposal_status_order__mdt pso1 = new BI_PC_Proposal_status_order__mdt();
        pso1.BI_PC_Managed_care_position__c = 2;
        pso1.Label = 'BM & DC Develop';
        pso1.DeveloperName = 'BM_DC_Develop';
        psoList.add(pso1);

        BI_PC_Proposal_status_order__mdt pso2 = new BI_PC_Proposal_status_order__mdt();
        pso2.BI_PC_Managed_care_position__c = 4;
        pso2.BI_PC_Government_position__c = 6;
        pso2.Label = 'Pricing & Contracting';
        pso2.DeveloperName = 'Pricing_Contracting';
        psoList.add(pso2);

        BI_PC_Proposal_status_order__mdt pso3 = new BI_PC_Proposal_status_order__mdt();
        pso3.BI_PC_Managed_care_position__c = 5;
        pso3.BI_PC_Government_position__c = 7;
        pso3.Label = 'Market Access';
        pso3.DeveloperName = 'Market_Access';
        psoList.add(pso3);

        BI_PC_Proposal_status_order__mdt pso4 = new BI_PC_Proposal_status_order__mdt();
        pso4.BI_PC_Managed_care_position__c = 8;
        pso4.Label = 'PTC';
        pso4.DeveloperName = 'PTC';
        psoList.add(pso4);



    }
    */


}