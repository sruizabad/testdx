public without sharing class BI_PL_AddNewTargetModalCtrl {

    /**
     * Returns the accounts (HCP and HCO) that match with the search term.
     *
     * @param      nameSearchTerm  Search accounts by this string
     * @param      countryCode     Preparation's country code
     *
     * @return     The possible accounts to add.
     */
    @RemoteAction
    @ReadOnly
    public static List<BI_PL_PreparationExt.PlanitTargetModel> searchAccounts(String nameSearchTerm, String city, String state, String preparationId, String countryCode, Boolean includeHCOs, String positionName, Boolean globalSearch, String typeAccount, String fieldForce) {

        Boolean searchUsingAffiliation = false;

        if(fieldForce == 'HSBS' && typeAccount == '0'){
            searchUsingAffiliation = true;
        }

        nameSearchTerm = String.escapeSingleQuotes(nameSearchTerm);
        city = String.escapeSingleQuotes(city);
        state = String.escapeSingleQuotes(state);

        // Retrieve the specialty field set for the country:
        String specialtyField = BI_PL_TargetPreparationHandler.getSpecialtyFieldForCountry(countryCode);

        List<BI_PL_PreparationExt.PlanitTargetModel> output = new List<BI_PL_PreparationExt.PlanitTargetModel>();
        Map<String, BI_PL_PreparationExt.PlanitTargetModel> outputMap = new Map<String, BI_PL_PreparationExt.PlanitTargetModel>();

        Boolean addressFilterSet = String.isNotBlank(city) || String.isNotBlank(state);

        // The final query looks similar to this one (as a reference):
        /*
                SELECT Id, IsPersonAccount, Name, FirstName, LastName, External_ID_vod__c, Specialty_1_vod__c,
                    (SELECT Id, Name, City_vod__c, Zip_vod__c, State_vod__c
                    FROM Address_vod__r
                    WHERE Primary_vod__c = true
                    AND City_vod__c LIKE 'Orlando%'
                    AND State_vod__c LIKE 'Fl%')
                FROM Account
                WHERE Country_Code_BI__c = 'US'
                AND Name LIKE 'Cvs%'
                AND IsPersonAccount = false
                AND Id IN
                    (SELECT Account_vod__c
                    FROM Address_vod__c
                    WHERE Primary_vod__c = true
                    AND City_vod__c LIKE 'Orlando%'
                    AND State_vod__c LIKE 'Fl%')
                LIMIT 501
        */
        // The "Inner SELECT Query" is used to retrieve the address data to display.
        // The "Inner WHERE Query" is used to filter the accounts by the address fields.

        String query = 'SELECT Id, IsPersonAccount, Name, FirstName, LastName, External_ID_vod__c, ' + specialtyField;

        // Address_vod__c queries. Both should be the same except for the selected fields and the Address_vod__c / Address_vod__r difference:
        String innerSelectQuery = 'SELECT Id, Name, City_vod__c, Zip_vod__c, State_vod__c FROM Address_vod__r WHERE Primary_vod__c = true';
        String innerWhereQuery = 'SELECT Account_vod__c FROM Address_vod__c WHERE Primary_vod__c = true';

        List<String> conditions = new List<String>();

        String addressConditions = '';

        if (String.isNotBlank(city))
            addressConditions += ' AND City_vod__c LIKE \'%' + city + '%\'';

        if (String.isNotBlank(state))
            addressConditions += ' AND State_vod__c LIKE \'%' + state + '%\'';

        innerSelectQuery += addressConditions;
        innerWhereQuery += addressConditions;

        // If the user sets any address field query Address_vod__c
        query += ', (' + innerSelectQuery;

        //if(searchUsingAffiliation){

        //    Set<Id> affAccounts = new Set<Id>();

        //    for(BI_PL_Affiliation__c aff : [SELECT BI_PL_Customer__c FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = 'SearchAccounts' AND BI_PL_Field_force__c = : fieldForce]){

        //        affAccounts.add(aff.BI_PL_Customer__c);
        //    }

        //    query += ') FROM Account WHERE Country_Code_BI__c = \'' + countryCode + '\' AND Id IN :affAccounts';

        //    if (typeAccount == '1')
        //        query += ' AND IsPersonAccount = true';
        //    else if (typeAccount == '0')
        //        query += ' AND IsPersonAccount = false';


        //    System.debug('preparationId: ' + preparationId + ' - positionName: ' + positionName);
        //} 
        //else 
        if (globalSearch) {
            query += ') FROM Account WHERE Country_Code_BI__c = \'' + countryCode + '\'';

            if (!includeHCOs)
                query += ' AND IsPersonAccount = true';
            else {
                if (typeAccount == '1')
                    query += ' AND IsPersonAccount = true';
                else if (typeAccount == '0')
                    query += ' AND IsPersonAccount = false';
            }

        }else {
            // === Accounts by territory ===
            Set<Id> accountIds = new Set<Id>();

            for (AccountShare a : [SELECT AccountId, RowCause, UserOrGroupId, Account.BI_Specialty_BI__r.Name, Account.External_ID_vod__c, Account.Name
                                   FROM AccountShare
                                   WHERE AccountId NOT IN (SELECT BI_PL_Target_Customer__c FROM BI_PL_Target_Preparation__c WHERE BI_PL_Header__c = :preparationId)
                                   AND (RowCause = 'TerritoryManual' OR RowCause = 'Territory' OR RowCause = 'Territory Management')
                                   AND UserOrGroupId IN (SELECT Id FROM Group WHERE Type = 'Territory' AND Related.Name = :positionName)]) {
                accountIds.add(a.AccountId);
            }

            query += ') FROM Account WHERE Country_Code_BI__c = \'' + countryCode + '\' AND Id IN :accountIds';

            if (typeAccount == '1')
                query += ' AND IsPersonAccount = true';
            else if (typeAccount == '0')
                query += ' AND IsPersonAccount = false';

            System.debug('preparationId: ' + preparationId + ' - positionName: ' + positionName);

            // =============================
        }

        if (String.isNotBlank(nameSearchTerm))
                query += ' AND Name LIKE \'%' + nameSearchTerm + '%\'';

        if (String.isNotBlank(addressConditions))
            query += ' AND Id IN (' + innerWhereQuery + ')';

        query += ' LIMIT 501';

        System.debug('searchAccounts query:' + query);

        // Iterate over the results.
        try {
            Map<Id, sObject> result = new Map<Id, sObject>(Database.query(query));
            Set<String> affAccounts = new Set<String>();

            if(searchUsingAffiliation){


                for(BI_PL_Affiliation__c aff : [SELECT BI_PL_Customer__c 
                    FROM BI_PL_Affiliation__c 
                    WHERE BI_PL_Type__c = 'SearchAccounts' 
                    AND BI_PL_Field_force__c = : fieldForce
                    AND BI_PL_Customer__c IN : result.keySet()]){

                    affAccounts.add(aff.BI_PL_Customer__c);
                }
            } 



            for (Account account : (list<Account>) result.values()) {

                BI_PL_PreparationExt.PlanitTargetModel target = new BI_PL_PreparationExt.PlanitTargetModel(new BI_PL_Target_Preparation__c(
                            BI_PL_Target_customer__c = account.Id,
                            BI_PL_Target_customer__r = account,
                            BI_PL_Header__c = preparationId,
                            BI_PL_Added_reason__c = '',
                            BI_PL_Specialty__c = (String)BI_PL_TargetPreparationHandler.getFieldValue(account, specialtyField)));

                if (account.Address_vod__r != null)
                    target.addAddresses(account.Address_vod__r);

                if (!addressFilterSet || addressFilterSet && account.Address_vod__r.size() > 0)
                    output.add(target);

                if(searchUsingAffiliation){
                    if(affAccounts.contains(account.Id)){
                        target.setValidToAdd(true);
                    } else {
                        //Exists , but not available in "SearchAccount"
                        target.setValidToAdd(false);
                    }
                }else {
                    target.setValidToAdd(true);
                }

            }
        } catch (QueryException e) {
            throw new BI_PL_Exception(Label.BI_PL_Specialty_field_not_properly_countries + ' ' + countryCode + '. ' + Label.BI_PL_Contact_system_admin);
            BI_PL_ErrorHandlerUtility.addPlanitServerError('BI_PL_AddNewTargetModalCtrl', 'Search accounts to add', e.getMessage(), e.getStackTraceString());
        }
        return output;
    }

    @RemoteAction
    public static List<PicklistOption> getAddedReasonOptions(){
        List<PicklistOption> pickListValuesList = new List<PicklistOption>();
        Schema.DescribeFieldResult fieldResult = BI_PL_Preparation_action__c.BI_PL_Added_reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for ( Schema.PicklistEntry pickListVal : ple) {
            if(isAddedManuallyReasonValue(pickListVal.getValue()))
                pickListValuesList.add(new PicklistOption(pickListVal.getValue(), pickListVal.getLabel()));
        }

        return pickListValuesList; 
    }
    
    private static Boolean isAddedManuallyReasonValue(String value){
        return !value.startsWith('Share') && !value.startsWith('Transfer');
    }

    public class PicklistOption {
        public String label;
        public String value;

        public PicklistOption (String value, String label) {
            this.label = label;
            this.value = value;
        }
    }

    /**
     * Returns the related overlap data if there's product overlap for the specified account.
     *
     * @param      accountId              The account identifier
     * @param      countryCode            The country code
     * @param      positionName           The position name
     * @param      cycleId                The cycle identifier
     * @param      preparationProductIds  The preparation product identifiers
     *
     * @return     True if there's overlap.
     */
    @ReadOnly
    @RemoteAction
    public static List<BI_PL_PreparationExt.PlanitRelatedTargetModel> getRelatedTargets(Id accountId, String countryCode, String positionName, Id cycleId, Map<String, BI_PL_PreparationExt.PlanitProductPair> preparationProducPairs) {

        List<BI_PL_PreparationExt.PlanitRelatedTargetModel> output = new List<BI_PL_PreparationExt.PlanitRelatedTargetModel>();

        List<BI_PL_PreparationExt.PlanitRelatedTargetModel> relatedTargets = BI_PL_PreparationExt.getRelatedTargets(new Set<String> {accountId}, countryCode, positionName, cycleId);

        for (BI_PL_PreparationExt.PlanitRelatedTargetModel related : relatedTargets) {
            for (BI_PL_PreparationExt.PlanitProductPair productPair : related.productsPairs) {
                if (preparationProducPairs.containsKey(productPair.Id)) {
                    output.add(related);
                    break;
                }
            }
        }
        return output;
    }


    /**
     * Returns the affiliation sales info grouped by account and product.
     *
     * @param      accountIdList              Account identifier list
     * @param      productIdList              Product identifier list
     *
     * @return     Map.
     */
    @ReadOnly
    @RemoteAction
    public static Map<String, Map<String, SalesInfoWrapper>> getAffiliationSalesInfo(List<Id> accountIdList, List<Id> productIdList) {

        Map<String, Map<String, SalesInfoWrapper>> result = new Map<String, Map<String, SalesInfoWrapper>>();

        for(BI_PL_Affiliation__c aff : [SELECT Id, BI_PL_Product_Sales__c, BI_PL_Customer__c, BI_PL_Product__c 
                                                FROM BI_PL_Affiliation__c 
                                                    WHERE BI_PL_Customer__c IN :accountIdList 
                                                    AND BI_PL_Product__c IN :productIdList]){
            Map<String, SalesInfoWrapper> auxProdInfo = result.get(aff.BI_PL_Customer__c);
            if(auxProdInfo == null){
                auxProdInfo = new Map<String, SalesInfoWrapper>();
            }
            SalesInfoWrapper auxSalesInfo = auxProdInfo.get(aff.BI_PL_Product__c);
            if(auxSalesInfo == null){
                auxSalesInfo = new SalesInfoWrapper();
            }
            auxSalesInfo.productSales += (aff.BI_PL_Product_Sales__c == null ? 0 : aff.BI_PL_Product_Sales__c);
            auxProdInfo.put(aff.BI_PL_Product__c, auxSalesInfo);
            result.put(aff.BI_PL_Customer__c, auxProdInfo);
        }

        return result;
    }

    public class SalesInfoWrapper{
        public Decimal productSales {get;set;}

        public SalesInfoWrapper(){
            this.productSales=0;
        }
    }
}