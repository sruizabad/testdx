@isTest
private class BI_PL_AddNewTargetModalCtrlTest
{

	@testSetup
	static void Setup()
	{
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		User thisUser = [SELECT Id,Country_Code_BI__c from User where Id = :UserInfo.getUserId() ];
		String countryCode = thisUser.Country_code_BI__c;
		list<Account> accs = BI_PL_TestDataFactory.createTestAccounts(5,countryCode);
		BI_PL_TestDataFactory.createCycleStructure(countryCode);
		List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_ID__c from BI_PL_Position_Cycle__c];
		String cycleId = [SELECT Id from BI_PL_cycle__c LIMIT 1].Id;
		//list<Account> accs = [SELECT id,External_ID_vod__c from account];
		
		List<Product_vod__c> products = BI_PL_TestDataFactory.createTestProduct(3,countryCode);
		//List<Product_vod__c> products = [SELECT Id,External_ID_vod__c from products];

		BI_PL_TestDataFactory.createPreparations(countryCode, posCycles, accs, products);
		List<Account> paccs = BI_PL_TestDataFactory.createTestPersonAccounts(3,countryCode);
		
		//
		List<BI_PL_Affiliation__c> affrel = BI_PL_TestDataFactory.createAffiliationRelationship(accs, paccs[0], countryCode);

		system.debug('aff' + affrel );
		


		Database.executeBatch(new BI_PL_GenerateSearchHCOAffiliationsBatch(cycleId));	
		
		List<BI_PL_Affiliation__c> aff_search = [SELECT id from bi_pl_affiliation__C where bi_pl_type__c = 'searchAccounts'];
		system.debug('aff_search' + aff_search );		

		List<String> lCycles = new List<String>(); 
		for (BI_PL_cycle__c a : [SELECT Id from BI_PL_cycle__c])
            lCycles.add(a.Id);

		Database.executeBatch(new BI_PL_UpdateUsersInPLANITBatch(countryCode, lCycles));
	}


	@isTest
	static void test(){
		User thisUser = [ select Id,Country_Code_BI__c from User where Id = :UserInfo.getUserId() ];
		String countryCode = thisUser.Country_code_BI__c;
		String acc = [SELECT Name from account where ispersonaccount = true LIMIT 1].Name;
		BI_PL_Position__c position = [SELECT Id, Name from BI_PL_Position__c LIMIT 1];
		BI_PL_Preparation__c prep = [SELECT Id from BI_PL_Preparation__c LIMIT 1];
		BI_PL_AddNewTargetModalCtrl.searchAccounts('test','','',prep.id,countryCode,false,position.Name,true,'1','');
		BI_PL_AddNewTargetModalCtrl.searchAccounts('test','','',prep.id,countryCode,true,position.Name,true,'1','');
		BI_PL_AddNewTargetModalCtrl.searchAccounts('test','CityTest','stateFull',prep.id,countryCode,false,position.Name,false,'1','');
		BI_PL_AddNewTargetModalCtrl.searchAccounts('test','CityTest','stateFull',prep.id,countryCode,true,position.Name,true,'0','HSBS');
		BI_PL_AddNewTargetModalCtrl.searchAccounts(acc,'CityTest','stateFull',prep.id,countryCode,true,position.Name,false,'1','HSBS');
		BI_PL_AddNewTargetModalCtrl.searchAccounts(acc,'CityTest','stateFull',prep.id,countryCode,false,position.Name,false,'1','HSBS');
		BI_PL_AddNewTargetModalCtrl.searchAccounts('test','CityTest','stateFull',prep.id,countryCode,true,position.Name,false,'0','HSBS');
		//searchAccounts(String nameSearchTerm, String city, String state, String preparationId, String countryCode, Boolean includeHCOs, String positionName, Boolean globalSearch, String typeAccount, String fieldForce) {

		BI_PL_AddNewTargetModalCtrl.getAddedReasonOptions();

		List<Id> accountIdList = new List<Id>(); 
		List<Id> productIdList = new List<Id>(); 
		List<Product_vod__c> productList = new List<Product_vod__c>();

		for (account a : [SELECT Id from account])
            accountIdList.add(a.Id);

        for (Product_vod__c o : [SELECT Id,Name from Product_vod__c]){
            productIdList.add(o.Id);
            productList.add(o);
        }


		BI_PL_AddNewTargetModalCtrl.getAffiliationSalesInfo(accountIdList, productIdList);

		Id cycleId = [SELECT Id from BI_PL_cycle__c LIMIT 1].Id;
		Id accountId = [SELECT Id from account LIMIT 1].Id;

		BI_PL_PreparationExt.PlanitProductPair productPair = new BI_PL_PreparationExt.PlanitProductPair(productList.get(0), productList.get(1));
		map<String, BI_PL_PreparationExt.PlanitProductPair> mapProductPair = new map<String, BI_PL_PreparationExt.PlanitProductPair>();
		String prodPairId = productList.get(0).Id + '' + productList.get(1).Id;
		mapProductPair.put(prodPairId, productPair);

		BI_PL_AddNewTargetModalCtrl.getRelatedTargets(accountId, countryCode,position.Name, cycleId, mapProductPair);



		//(Id accountId, String countryCode, String positionName, Id cycleId, Map<String, BI_PL_PreparationExt.PlanitProductPair> preparationProducPairs) {

		//Id cycleId = [SELECT Id from BI_PL_cycle__c LIMIT 1].Id;

		//Map<String, BI_PL_PreparationExt.PlanitProductPair> preparationProducPairs = Map<String, BI_PL_PreparationExt.PlanitProductPair> ();

		//BI_PL_AddNewTargetModalCtrl.getRelatedTargets(accountIdList[0].Id,countryCode,position.Name,cycleId,preparationProducPairs);
		
	}
}