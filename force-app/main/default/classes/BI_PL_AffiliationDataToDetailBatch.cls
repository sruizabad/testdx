/**
 *	IMPORTANT:
 *	Affiliation records must be properly filled (just the required fields) in order for this batch to work properly.
 *	For example, DO NOT ADD A PRODUCT TO A Affiliation record of type 'Batch'.
 *
 *	Dump Affiliation fields to detail and target records.
 *	Values will be filled only if the field is empty (null) (no override done).
 *	@author	OMEGA CRM
 */

global without sharing class BI_PL_AffiliationDataToDetailBatch extends BI_PL_NightSubbatch {

	private static final String QUERIED_RECORD_LOG_ENTRY = 'Queried records';
	private static final String UPDATE_RECORD_LOG_ENTRY = 'U';
	private static final String TARGET_ERROR_LOG_ENTRY = 'TE';
	private static final String GENERIC_ERROR_LOG_ENTRY = 'GE';
	private static final String DETAIL_ERROR_LOG_ENTRY = 'DE';

	public static final Decimal HEAPSIZE_LIMIT_PERCENTAGE = 0.7;
	public static final Integer MAX_FILE_SIZE = 4500000;

	public static final String LOG_FILE_NAME = 'PLANiT_(Affiliation_to_detail)';

	private static final String AFFILIATION_TYPE_SALES = 'Sales';
	private static final String AFFILIATION_TYPE_INFO = 'Information';
	private static final String AFFILIATION_TYPE_BATCH = 'Batch';

	private static final String COMPOSED_KEY_SEPARATOR = '###';

	/*
		private List<String> genericErrors = new List<String> {};
		private List<String> targetErrors = new List<String> {};
		private List<String> detailErors = new List<String> {};
	*/
	// ======================================================================================== //

	// Instead of sets use maps to increase performance:
	/*Map<Id, Boolean> recordsToUpdateId = new Map<Id, Boolean>();
	Map<Id, Boolean> queriedDetailRecordsId = new Map<Id, Boolean>();
	Map<Id, Boolean> affiliationRecordsQueried = new Map<Id, Boolean>();
	*/

	Boolean outputAffiliationRecordIds = false;

	String logBody = '';

	List<Id> generatedDocumentsId = new List<Id>();

	Boolean errorHappened = false;
	Boolean pendingDocumentToGenerate = false;

	BI_PL_Country_settings__c oCountrySetting;

	/*
	* Set here the mapping between the 'affiliation field' (value) and the 'detail fields' (key).
	* Set here the mapping between the 'affiliation field' (value) and the 'target fields' (key).
	*/
	private void setupMapping() {
		// DETAIL
		// Information
		addMapping('BI_PL_Segment__c', 'BI_PL_Segment__c', true, AFFILIATION_TYPE_INFO, false);
		addMapping('BI_PL_Strategic_segment__c', 'BI_PL_Strategic_segment__c', true, AFFILIATION_TYPE_INFO, false);
		addMapping('BI_PL_Segment__c', 'BI_PL_Secondary_segment__c', true, AFFILIATION_TYPE_INFO, true);
		addMapping('BI_PL_Strategic_segment__c', 'BI_PL_Secondary_strategic_segment__c', true, AFFILIATION_TYPE_INFO, true);

		// Sales
		addMapping('BI_PL_Market__c', 'BI_PL_Market__c', true, AFFILIATION_TYPE_SALES, false);
		//addMapping('BI_PL_Net_sales_TRx__c', 'BI_PL_Net_sales_TRx__c', true, AFFILIATION_TYPE_SALES, false);
		addMapping('BI_PL_Sales_rx__c', 'BI_PL_Net_sales_TRx__c', true, AFFILIATION_TYPE_SALES, false);
		addMapping('BI_PL_Market_Sales__c', 'BI_PL_Market_Sales__c', true, AFFILIATION_TYPE_SALES, false);
		addMapping('BI_PL_Product_Sales__c', 'BI_PL_Product_Sales__c', true, AFFILIATION_TYPE_SALES, false);
		addMapping('BI_PL_Product_decile__c', 'BI_PL_Product_decile__c', true, AFFILIATION_TYPE_SALES, false);
		addMapping('BI_PL_Market_decile__c', 'BI_PL_Market_decile__c', true, AFFILIATION_TYPE_SALES, false);
		addMapping('BI_PL_Unrestricted_aggregate_access__c', 'BI_PL_Unrestricted_aggregate_access__c', true, AFFILIATION_TYPE_SALES, false);
		addMapping('BI_PL_Volume_TRx__c', 'BI_PL_Volume_TRx__c', true, AFFILIATION_TYPE_SALES, false);

		//MLG --> LOOKUPS
		addMapping('PRIMARY_SALES'+COMPOSED_KEY_SEPARATOR+'Id', 'BI_PL_Primary_sales_affiliation__c', true, AFFILIATION_TYPE_SALES, false);
		addMapping('SECONDARY_SALES'+COMPOSED_KEY_SEPARATOR+'Id', 'BI_PL_Secondary_sales_affiliation__c', true, AFFILIATION_TYPE_SALES, true);
		addMapping('PRIMARY_INFO'+COMPOSED_KEY_SEPARATOR+'Id', 'BI_PL_Primary_info_affiliation__c', true, AFFILIATION_TYPE_INFO, false);
		addMapping('SECONDARY_INFO'+COMPOSED_KEY_SEPARATOR+'Id', 'BI_PL_Secondary_info_affiliation__c', true, AFFILIATION_TYPE_INFO, true);

		// TARGET
		// Batch
		addMapping('BI_PL_HCP_Access__c', 'BI_PL_HCP_access__c', false, AFFILIATION_TYPE_BATCH, false);
	}

	// ======================================================================================== //

	Map<String, MappingAffiliationField> mappingAffiliationToDetail = new Map<String, MappingAffiliationField> ();

	Set<String> detailFields = new Set<String> {'BI_PL_Product__c', 'BI_PL_Secondary_Product__c'};
	Set<String> targetFields = new Set<String> {'BI_PL_Target_customer__c'};
	Set<String> affiliationFields = new Set<String> {'BI_PL_Customer__c', 'BI_PL_Product__c', 'BI_PL_Type__c'};
	Set<String> affiliationTypes = new Set<String>();

	String query;


	global BI_PL_AffiliationDataToDetailBatch(String countryCode, BI_PL_NightSubbatch nextBatchToExecute) {
		super(countryCode, nextBatchToExecute);

		oCountrySetting = getCountrySetting();
		//outputAffiliationRecordIds = getOutputAffiliationRecordIds();
		outputAffiliationRecordIds = oCountrySetting.BI_PL_Aff_to_detail_output_affs__c;

		setupMapping();

		// Delete previous logs
		delete [SELECT Id FROM Document WHERE Name LIKE :generateLogName(false) + '%' AND FolderId = : getAdministratorId()];

		try {
			//Date tmp = System.today();
			query = 'SELECT Id, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c'; 
			query += getStringsConcat(detailFields) + getTargetFieldString();
			query += ' FROM BI_PL_Detail_preparation__c ';
			query += 'WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Country_code__c = \'' + countryCode + '\' ';
			query += 'AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c > today';/*: \''+tmp+'\'';*/

			System.debug(loggingLevel.Error, '*** query: ' + query);
			initializeLogBody();
		} catch (exception e) {
			//addGenericError('Error while getting the details data: ' + e.getMessage());
			initializeLogBody();

			log_addGenericError('Error while getting the details data: ' + e.getMessage());

			createNewDocument();
		}
	}

	private BI_PL_Country_settings__c getCountrySetting(){

		return [SELECT Id, BI_PL_Aff_to_detail_output_affs__c, BI_PL_Segment__c, BI_PL_Use_Segment__c, BI_PL_Use_secondary_product__c 
				FROM BI_PL_Country_settings__c 
				WHERE BI_PL_Country_code__c = :countryCode];
	}

	/*
	private Boolean getOutputAffiliationRecordIds() {
		return [SELECT Id, BI_PL_Aff_to_detail_output_affs__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = :countryCode].BI_PL_Aff_to_detail_output_affs__c;
	}
	*/

	private String getTargetFieldString() {
		Set<String> targetFieldsWithPrefix = new Set<String>();
		for (String field : targetFields)
			targetFieldsWithPrefix.add('BI_PL_Channel_detail__r.BI_PL_Target__r.' + field);

		return getStringsConcat(targetFieldsWithPrefix);
	}

	global override Database.QueryLocator start(Database.BatchableContext BC) {
		Database.QueryLocator queryLocator = null;
		try {
			queryLocator = Database.getQueryLocator(query);
		} catch (exception e) {
			initializeLogBody();

			log_addGenericError('Error on START: ' + e.getMessage());

			createNewDocument();
		}
		return queryLocator;
	}

	global override void execute(Database.BatchableContext BC, List<sObject> scope) {
		// Map<CustomerId, Map<Type, ProductId>> AffiliationRecord.
		Set<String> cycles = new Set<String>();

		Map<Id, Map<String, Map<Id, BI_PL_Affiliation__c>>> affiliationsMap = new Map<Id, Map<String, Map<Id, BI_PL_Affiliation__c>>>();

		// DETAIL FIELDS
		List<BI_PL_Detail_preparation__c> details = (List<BI_PL_Detail_preparation__c>)scope;
		for(BI_PL_Detail_preparation__c d : details){
			cycles.add(d.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c);
		}
		System.debug('CYCLES: ' + cycles);
		Map<Id, BI_PL_Target_preparation__c> targets = new Map<Id, BI_PL_Target_preparation__c>();

		// 1.- Retrieve Affiliation data:
		try {
			retrieveAffiliationData(details, affiliationsMap);
			System.debug('AffiliationMap: ' + affiliationsMap.size());
		} catch (exception e) {
			log_addGenericError('Error while retrieving the affiliation data: ' + e.getMessage());
			//addGenericError('Error while retrieving the affiliation data: ' + e.getMessage());
			return;
		}

		Map<String, BI_PL_Detail_preparation__c> detailsToUpdate = new Map<String, BI_PL_Detail_preparation__c>();

		system.debug('## Country Set: ' + oCountrySetting);
		system.debug('## BI_PL_Segment: ' + oCountrySetting.BI_PL_Segment__c);

		log_addLimitsLine();
		addLogLine(QUERIED_RECORD_LOG_ENTRY+'('+details.size()+'):');
		
		Object value;
		Object value2;
		
		for (BI_PL_Detail_preparation__c detail : details)
			log_addQueriedDetailRecord(detail.Id, detail.BI_PL_Channel_detail__r.BI_PL_Target__c);

		for (BI_PL_Detail_preparation__c detail : details){

			//queriedDetailRecordsId.put(detail.Id, true);
			//queriedDetailRecordsId.put(detail.BI_PL_Channel_detail__r.BI_PL_Target__c, true);
			Boolean updateDetail = false;

			//Check Country use Affiliations
			if (oCountrySetting.BI_PL_Use_Segment__c){
				// 2.- Fill detail and target records:
				for (MappingAffiliationField mapping : mappingAffiliationToDetail.values()) {
					//System.debug('mapping ' + mapping);

					value = null;
					value2 = null;
					// a) Get the affiliation field data (in case the detail / record field is empty):
					try {
						if ((mapping.toDetail() && !mapping.isSecondary()) || !mapping.toDetail()){
							value = getAffiliationFieldForDetail(mapping, detail, detail.BI_PL_Product__c, affiliationsMap);
							//GLOS-1011 - Check Segment field to initialize with default value in Custom Setting.
							system.debug('## BEFORE value: ' + value);
							value = getDefaultSegmentValue(value, mapping.getAffiliationField(), oCountrySetting.BI_PL_Segment__c);
							system.debug('## AFTER value: ' + value);					
						}
						//MLG
						//if (mapping.toDetail() && mapping.isSecondary() && detail.get(mapping.getDestinationField()) == null && detail.BI_PL_Secondary_product__c != null) {
						if (mapping.toDetail() && mapping.isSecondary() && detail.BI_PL_Secondary_product__c != null){ 
							value2 = getAffiliationFieldForDetail(mapping, detail, detail.BI_PL_Secondary_product__c, affiliationsMap);
							//GLOS-1011 - Check Segment field to initialize with default value in Custom Setting
							system.debug('## BEFORE SEC value: ' + value2);
							value2 = getDefaultSegmentValue(value2, mapping.getAffiliationField(), oCountrySetting.BI_PL_Segment__c);
							//System.debug(loggingLevel.Error, '*** detail with secondary product --> value2: ' + value2);
							system.debug('## AFTER SEC value: ' + value2);

						}

					} catch (exception e) {
						//addGenericError(generateFieldError('Error while getting the affiliation data', mapping, e));
						log_addGenericError(generateFieldError('Error while getting the affiliation data', mapping, e));
					}
					// b) If the affiliation field has data, clone it to the detail / target:
					try {
						if (value != null) {
							if (mapping.toDetail()) {
								detail.put(mapping.getDestinationField(), value);
								updateDetail = true;
							} else {
								if (!targets.containsKey(detail.BI_PL_Channel_detail__r.BI_PL_Target__c))
									targets.put(detail.BI_PL_Channel_detail__r.BI_PL_Target__c, detail.BI_PL_Channel_detail__r.BI_PL_Target__r);

								targets.get(detail.BI_PL_Channel_detail__r.BI_PL_Target__c).put(mapping.getDestinationField(), value);
								//recordsToUpdateId.add(detail.BI_PL_Channel_detail__r.BI_PL_Target__c);
								//recordsToUpdateId.put(detail.BI_PL_Channel_detail__r.BI_PL_Target__c, true);
								log_addRecordToUpdate(detail.BI_PL_Channel_detail__r.BI_PL_Target__c);
							}
						}
						//MLG
						if(value2 != null && mapping.toDetail()){
							detail.put(mapping.getDestinationField(), value2);
							updateDetail = true;
						}

					} catch (exception e) {
						//addGenericError(generateFieldError('Error while updating the details and targets (' + detail.Id + ')', mapping, e));
						log_addGenericError(generateFieldError('Error while updating the details and targets (' + detail.Id + ')', mapping, e));
					}
				}
			}else{
				//GLOS-1011 - Check Segment field to initialize with default value in Custom Setting
				if (String.isBlank(detail.BI_PL_Segment__c) ){
					detail.BI_PL_Segment__c = oCountrySetting.BI_PL_Segment__c;
					updateDetail = true;
				}
			}
			if (updateDetail) {
				System.debug('Detail to Update: ' + detail);
				detailsToUpdate.put(detail.Id, detail);
				//recordsToUpdateId.add(detail.Id);
				//recordsToUpdateId.put(detail.Id, true);
				log_addRecordToUpdate(detail.Id);
			}
		}

		// TARGETS UPDATE
		List<BI_PL_Target_preparation__c> targetsList = new List<BI_PL_Target_preparation__c>(targets.values());

		Database.SaveResult[] updateResults = Database.update(targets.values(), false);

		for (Integer i = 0; i < updateResults.size(); i++) {
			Database.SaveResult sr = updateResults.get(i);
			if (!sr.isSuccess()) {
				for (Database.Error err : sr.getErrors()) {
					log_addTargetError(SObjectType.BI_PL_Target_preparation__c.getLabel() + ' \'(\'' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + targetsList.get(i).Id + '\') ' + targetsList.get(i).Id + '\' : ' + err.getStatusCode() + ': ' + err.getMessage() + ' Fields : ' + err.getFields());

					//targetErrors.add(SObjectType.BI_PL_Target_preparation__c.getLabel() + ' \'<a href=\'' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + targetsList.get(i).Id + '\' >' + targetsList.get(i).Id + '</a>\' : ' + err.getStatusCode() + ': ' + err.getMessage() + ' Fields : ' + err.getFields());
				}
			}
		}

		// DETAILS UPDATE
		updateResults = Database.update(detailsToUpdate.values(), false);

		for (Integer i = 0; i < updateResults.size(); i++) {
			Database.SaveResult sr = updateResults.get(i);
			if (!sr.isSuccess()) {
				for (Database.Error err : sr.getErrors()) {
					log_addDetailError(SObjectType.BI_PL_Detail_preparation__c.getLabel() + ' \'(\'' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + details.get(i).Id + '\' >' + details.get(i).Id + ')\' : ' + err.getStatusCode() + ': ' + err.getMessage() + ' Fields : ' + err.getFields());

					//detailErors.add(SObjectType.BI_PL_Detail_preparation__c.getLabel() + ' \'<a href=\'' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + details.get(i).Id + '\' >' + details.get(i).Id + '</a>\' : ' + err.getStatusCode() + ': ' + err.getMessage() + ' Fields : ' + err.getFields());
				}
			}
		}

		pendingDocumentToGenerate = true;

		// Check if save document:
		if ((Decimal)Limits.getHeapSize() / (Decimal)Limits.getLimitHeapSize() > HEAPSIZE_LIMIT_PERCENTAGE || logBody.length() > MAX_FILE_SIZE) {
			createNewDocument();
		}
	}

	@TestVisible
	private Id saveDocument() {
		log_addLimitsLine();

		Document d = new Document();
		d.Name = generateLogName(true);
		//String myContent = generateDocumentBody();
		d.Body = Blob.valueOf(logBody);
		d.ContentType = 'text/plain';
		d.Type = 'txt';
		d.FolderId = getAdministratorId();

		insert d;

		pendingDocumentToGenerate = false;

		initializeLogBody();

		//genericErrors.clear();
		//targetErrors.clear();
		//detailErors.clear();
		//recordsToUpdateId.clear();
		//queriedDetailRecordsId.clear();
		//affiliationRecordsQueried.clear();

		return d.Id;
	}

	@TestVisible
	private void initializeLogBody() {
		logBody = '-- Affiliation to detail --\r\n\r\n';

		log_addTimeLine();
		log_addQueryLine();

		log_addDetailFields();
		log_addTargetFields();
		log_addAffiliationFields();

		log_addLimitsLine();
	}

	/*private void addGenericError(String error) {
		errorHappened = true;
		genericErrors.add(error);
	}*/
	@TestVisible
	private String generateFieldError(String prefix, MappingAffiliationField mapping,  exception e) {
		return prefix + ': (Source field: ' + mapping.getDestinationField() + ', Destination field: ' + mapping.getAffiliationField() + ') : ' + e.getTypeName() + ' ' + e.getStackTraceString() + ' - [' + e.getMessage() + ']';
	}

	public class MyException extends Exception {}

	@TestVisible
	private void createNewDocument() {
		generatedDocumentsId.add(saveDocument());
	}

	global override void finish(Database.BatchableContext BC) {
		super.finish(BC);

		if (pendingDocumentToGenerate)
			createNewDocument();

		if (errorHappened) {
			sendErrorEmail();
		}
	}

	// =================== LOG WRITE METHODS ===================== //

	private void log_addTimeLine() {
		addLogLine('Time: ' + Datetime.now());
	}
	private void log_addQueryLine() {
		addLogLine('Query: ' + query);
	}
	private void log_addLimitsLine() {
		addLogLine('Limits');
		addLogLine('Heap: ' + (Decimal)Limits.getHeapSize() + '/' + (Decimal)Limits.getLimitHeapSize() + ' (' +
		           ((Decimal)Limits.getHeapSize() / (Decimal)Limits.getLimitHeapSize() * 100) + '%)'
		           + ' LogBody: ' + logBody.length());
	}
	@TestVisible
	private void log_addAffiliationQueryLine(String q) {
		addLogLine('Aff. query: ' + q);
	}
	private void log_addDetailFields() {
		logBody += '\r\nDetail fields (' + detailFields.size() + '): \r\n';
		for (String i : detailFields)
			logBody += i + ', ';
	}
	private void log_addTargetFields() {
		logBody += '\r\nTarget fields (' + targetFields.size() + '): \r\n';
		for (String i : targetFields)
			logBody += i + ', ';
	}
	private void log_addAffiliationFields() {
		logBody += '\r\nAff. fields (' + affiliationFields.size() + '): \r\n';
		for (String i : affiliationFields)
			logBody += i + ', ';
	}

	private void log_addQueriedDetailRecord(Id recordId, Id targetId) {
		logBody += getShortId(recordId) + '(' + targetId + '),';
	}
	@TestVisible
	private void log_addRecordToUpdate(Id recordId) {
		addLogLine(UPDATE_RECORD_LOG_ENTRY + ':' + getShortId(recordId));
	}
	@TestVisible
	private void log_addAffiliationRecord(Id recordId) {
		logBody += getShortId(recordId) + ',';
	}
	@TestVisible
	private void log_addGenericError(String s) {
		errorHappened = true;
		addLogLine(GENERIC_ERROR_LOG_ENTRY + ':' + s);
	}
	@TestVisible
	private void log_addTargetError(String s) {
		errorHappened = true;
		addLogLine(TARGET_ERROR_LOG_ENTRY + ':' + s);
	}
	@TestVisible
	private void log_addDetailError(String s) {
		errorHappened = true;
		addLogLine(DETAIL_ERROR_LOG_ENTRY + ':' + s);
	}

	private void addLogLine(String line) {
		logBody += '\r\n' + line;
	}

	private String getShortId(Id i) {
		return  ((String)i).substring(0, 15);
	}

	/*private String generateDocumentBody() {
		String output = '<h2>Affiliation to detail</h2>';
		//output += '\r\n<b>Time: </b>' + Datetime.now();
		//output += '\r\n\r\n<b>Query: </b>' + query;
		/*
				output += '\r\n\r\n<b>Detail fields (' + detailFields.size() + ') </b>: \r\n';
				for (String i : detailFields)
					output += i + ', ';
		*//*
output += '\r\n\r\n<b>Target fields (' + targetFields.size() + ') </b>: \r\n';
for (String i : targetFields)
output += i + ', ';*/
		/*
				output += '\r\n\r\n<b>Affiliation fields (' + affiliationFields.size() + ') </b>: \r\n';
				for (String i : affiliationFields)
					output += i + ', ';
		*/
		// RECORDS AFFECTED
		/*
				output += '\r\n\r\n<b>Queried detail record ids (' + queriedDetailRecordsId.size() + ') </b>: \r\n';
				for (Id i : queriedDetailRecordsId.keySet())
					output += i + ', ';
		*/
		/*
				output += '\r\n\r\n<b>Records to update (' + recordsToUpdateId.size() + ')</b>: \r\n';
				for (Id i : recordsToUpdateId.keySet())
					output += i + ', ';
		*//*
output += '\r\n\r\n<b>Affiliation records (' + affiliationRecordsQueried.size() + ')</b>: \r\n';
for (Id i : affiliationRecordsQueried.keySet())
output += i + ', ';
*/
		// ERRORS
		/*
				output += '\r\n\r\n<b>Generic errors (' + genericErrors.size() + ')</b>: \r\n';
				if (genericErrors.size() > 0)
					output += generateListErrors(genericErrors, null);

				output += '\r\n\r\n<b>Target errors (' + targetErrors.size() + ')</b>: \r\n';
				if (targetErrors.size() > 0)
					output += generateListErrors(targetErrors, SObjectType.BI_PL_Target_preparation__c.getLabel());

				output += '\r\n\r\n<b>Detail errors (' + detailErors.size() + ')</b>: \r\n';
				if (detailErors.size() > 0)
					output += generateListErrors(detailErors, SObjectType.BI_PL_Detail_preparation__c.getLabel());
		*/
		/*return output;
	}*/
	@TestVisible
	protected override String generateErrorEmailBody() {
		String body = '';
		body = 'The process finished with errors. Check the Salesforce documents folder to find more information.';
		return body;
	}

	/*private String generateListErrors(List<String> errors, String objectLabel) {
		String output = '';
		if (objectLabel != null) {
			output += '<h3>' + objectLabel + ' errors:</h3>';
		}
		output += '\r\n<ul>';
		for (String s : errors) {
			output += '<li/>' + s + '</li>';
		}
		output += '</ul>';
		return output;
	}*/
	@TestVisible
	protected override String generateErrorEmailSubject() {
		return Label.BI_PL_Affiliation_data_to_detail_batch_subject;
	}

	/**
	 *	Queries the affiliation data based on the batch details.
	 */
	private void retrieveAffiliationData(List<BI_PL_Detail_preparation__c> details, Map<Id, Map<String, Map<Id, BI_PL_Affiliation__c>>> affiliationsMap) {
		Set<Id> accountsId = new Set<Id>();
		Set<Id> productsId = new Set<Id>();

		for (BI_PL_Detail_preparation__c detail : details) {
			accountsId.add(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c);
			productsId.add(detail.BI_PL_Product__c);
			//MLG
			if(detail.BI_PL_Secondary_product__c != null)
				productsId.add(detail.BI_PL_Secondary_product__c);

		}

		String affiliationQuery = 'SELECT Id ' + getStringsConcat(affiliationFields) + ' FROM BI_PL_Affiliation__c WHERE BI_PL_Customer__c IN :accountsId AND (BI_PL_Product__c IN :productsId OR BI_PL_Product__c = null) AND BI_PL_Type__c IN :affiliationTypes';

		if (outputAffiliationRecordIds)
			log_addAffiliationQueryLine(affiliationQuery);

		if (outputAffiliationRecordIds)
			addLogLine('Aff. records: ');

		for (BI_PL_Affiliation__c a : Database.query(affiliationQuery)) {
			System.debug('Afiliation: ' + a);
			addAffiliation(a.BI_PL_Customer__c, a.BI_PL_Type__c, a.BI_PL_Product__c, a, affiliationsMap);
			//affiliationRecordsQueried.put(a.Id, true);
			if (outputAffiliationRecordIds)
				log_addAffiliationRecord(a.Id);
		}

	}

	private Object getAffiliationFieldForRecord(Id customer, String affType, Id product, String affiliationField, Map<Id, Map<String, Map<Id, BI_PL_Affiliation__c>>> affiliationsMap) {

		if (!affiliationsMap.containsKey(customer))
			return null;
		if (!affiliationsMap.get(customer).containsKey(affType))
			return null;

		return getFieldValue(getAffiliationRecord(customer, affType, product, affiliationsMap), affiliationField);
	}
	//MLG --> added parameter product
	private Object getAffiliationFieldForDetail(MappingAffiliationField mapping, BI_PL_Detail_preparation__c detail, String productId, Map<Id, Map<String, Map<Id, BI_PL_Affiliation__c>>> affiliationsMap) {
		return getAffiliationFieldForRecord(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c, mapping.getAffiliationType(), (mapping.toDetail()) ? productId : null, mapping.getAffiliationField(), affiliationsMap);
	}
	/**
	* This method returns the value of any object field specified with a String.
	*/
	@TestVisible
	private Object getFieldValue(SObject o, String field) {
		if (o == null) {return null;}
		if (field.contains('.')) {
			String nextField = field.substringAfter('.');
			String relation = field.substringBefore('.');
			return getFieldValue((SObject)o.getSObject(relation), nextField);
		} else {
			return o.get(field);
		}
	}
	@TestVisible
	private void addAffiliation(Id customer, String affType, Id product, BI_PL_Affiliation__c record, Map<Id, Map<String, Map<Id, BI_PL_Affiliation__c>>> affiliationsMap) {
		if (!affiliationsMap.containsKey(customer))
			affiliationsMap.put(customer, new Map<String, Map<Id, BI_PL_Affiliation__c>>());

		if (!affiliationsMap.get(customer).containsKey(affType))
			affiliationsMap.get(customer).put(affType, new Map<Id, BI_PL_Affiliation__c>());

		affiliationsMap.get(customer).get(affType).put(product, record);
	}

	private BI_PL_Affiliation__c getAffiliationRecord(Id customer, String affType, Id product, Map<Id, Map<String, Map<Id, BI_PL_Affiliation__c>>> affiliationsMap) {
		return affiliationsMap.get(customer).get(affType).get(product);
	}

	//MLG
	private void addMapping(String affiliationField, String destinationField, Boolean toDetail, String affiliationType, Boolean isSecondary) {

		//MLG
		if(affiliationField.contains(COMPOSED_KEY_SEPARATOR)) {
			mappingAffiliationToDetail.put(destinationField, new MappingAffiliationField('Id', destinationField, toDetail, affiliationType, isSecondary));
		} else {
			mappingAffiliationToDetail.put(destinationField, new MappingAffiliationField(affiliationField, destinationField, toDetail, affiliationType, isSecondary));
			affiliationFields.add(affiliationField);
		}


		if (toDetail) {
			detailFields.add(destinationField);
		} else {
			targetFields.add(destinationField);
		}

		affiliationTypes.add(affiliationType);
	}

	private String getStringsConcat(Set<String> s) {
		String output = '';
		for (String field : s)
			output += ', ' + field;
		return output;
	}

	private string generateLogName(Boolean withDate) {
		String output = LOG_FILE_NAME + '_' + countryCode + '_';
		if (withDate)
			output += Datetime.now();
		return output;
	}

	/*
	* GLOS-1011 - Check affiliation field to initialize with default value.
	*/
	private Object getDefaultSegmentValue(Object oOriginalValue, String sAffiliation, String sDefault){

		system.debug('## sAffiliation: ' + sAffiliation);
		system.debug('## oOriginalValue: ' + oOriginalValue);
		system.debug('## sDefault: ' + sDefault);
		return (sAffiliation.equals('BI_PL_Segment__c') || sAffiliation.equals('BI_PL_Secondary_segment__c') ) 
				&& oOriginalValue == null ? sDefault : oOriginalValue;
	}


	@TestVisible
	private class MappingAffiliationField {

		private String affiliationField;
		private String destinationField;
		private String affiliationType;
		private Boolean toDetail;
		private Boolean isSecondary;

		public MappingAffiliationField(String affiliationField, String destinationField, Boolean toDetail, String affiliationType, Boolean isSecondary) {
			this.affiliationField = affiliationField;
			this.destinationField = destinationField;
			this.affiliationType = affiliationType;
			this.toDetail = toDetail;
			this.isSecondary = isSecondary;
		}

		public String getAffiliationField() {
			return affiliationField;
		}
		public String getDestinationField() {
			return destinationField;
		}
		public String getAffiliationType() {
			return affiliationType;
		}

		public Boolean toDetail() {
			return toDetail;
		}
		public Boolean isSecondary() {
			return isSecondary;
		}
	}
	@TestVisible
	private class AffiliationWrapper {
		private BI_PL_Affiliation__c record;

		public AffiliationWrapper(BI_PL_Affiliation__c record) {
			this.record = record;
		}

		public String getType() {
			return record.BI_PL_Type__c;
		}

		public String getProductId() {
			return this.record.BI_PL_Product__c;
		}
	}

}