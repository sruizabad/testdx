@isTest
private class BI_PL_AffiliationDataToDetailBatch_Test {
	
	@isTest static void test_method_one() {
		String userCountryCode = [SELECT Id, Country_Code_BI__c from User where Id =: UserInfo.getUserId()].Country_Code_BI__c;
		Id larId = 'a7qV00000008vTB';
		User txt = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);

		BI_PL_TestDataFactory.createCountrySetting();
		BI_PL_TestDataFactory.createCustomSettings();

		Map<Id, Map<String, Map<Id, BI_PL_Affiliation__c>>> affiliationsMap = new Map<Id, Map<String, Map<Id, BI_PL_Affiliation__c>>>();

		BI_PL_TestDataFactory.createCycleStructure(userCountryCode, Date.today() + 1, Date.today() + 30 );
		List<BI_PL_Position_cycle__c> positionCycles = [SELECT Id, BI_PL_External_ID__c FROM BI_PL_Position_cycle__c];

		List<Account> listAccounts = BI_PL_TestDataFactory.createTestAccounts(2,userCountryCode);
		List<Product_vod__c> listProducts = BI_PL_TestDataFactory.createTestProduct(2,userCountryCode);

		List<BI_PL_Preparation__c> listPreparations = BI_PL_TestDataFactory.createPreparations(userCountryCode,positionCycles, listAccounts, listProducts);

		List<Product_vod__c> products = BI_PL_TestDataUtility.createProducts();

		List<BI_PL_Affiliation__c> affiliations = BI_PL_TestDataFactory.createAffiliationSI('Sales', listAccounts, products, userCountryCode);
		List<BI_PL_Affiliation__c> affiliationsI = BI_PL_TestDataFactory.createAffiliationSI('Information', listAccounts, products, userCountryCode);

		Test.startTest();

		BI_PL_affiliationDataToDetailBatch affiliationBatch = new BI_PL_affiliationDataToDetailBatch(userCountryCode,null);
		Database.executeBatch(affiliationBatch);

		affiliationBatch.saveDocument();
		affiliationBatch.initializeLogBody();
		affiliationBatch.createNewDocument();
		affiliationBatch.log_addRecordToUpdate(larId);
		affiliationBatch.log_addAffiliationRecord(larId);
		affiliationBatch.log_addGenericError('test');
		affiliationBatch.log_addTargetError('test');
		affiliationBatch.log_addDetailError('test');
		affiliationBatch.generateErrorEmailBody();
		affiliationBatch.generateErrorEmailSubject();
		affiliationBatch.getFieldValue(null, 'asdasd');
		affiliationBatch.getFieldValue(products[0], 'External_ID_vod__c');
		affiliationBatch.getFieldValue(affiliations[0], 'BI_PL_Customer__r.External_ID_vod__c');
		affiliationBatch.log_addAffiliationQueryLine('aweq');

		BI_PL_affiliationDataToDetailBatch.AffiliationWrapper exd = new BI_PL_affiliationDataToDetailBatch.AffiliationWrapper(affiliations[0]);
		exd.getType();
		exd.getProductId();

		BI_PL_affiliationDataToDetailBatch.MappingAffiliationField mapstr = new BI_PL_affiliationDataToDetailBatch.MappingAffiliationField('affiliationField', 'destinationField', true, 'affiliationType', true);
		mapstr.getDestinationField();

		BI_PL_affiliationDataToDetailBatch.MyException asdew = new BI_PL_affiliationDataToDetailBatch.MyException();

		affiliationBatch.generateFieldError('test', mapstr, asdew);

		//affiliationBatch.retrieveAffiliationData(, affiliationsMap);

		Test.stopTest();
	}
	
	/*@isTest static void test_method_two() {

		String userCountryCode = [SELECT Id, Country_Code_BI__c from User where Id =: UserInfo.getUserId()].Country_Code_BI__c;
		Id larId = 'a7qV00000008vTB';
		User txt = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);

		BI_PL_TestDataFactory.createCountrySetting();
		BI_PL_TestDataFactory.createCustomSettings();

		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);
		List<BI_PL_Position_cycle__c> positionCycles = [SELECT Id, BI_PL_External_ID__c FROM BI_PL_Position_cycle__c];

		List<Account> listAccounts = BI_PL_TestDataFactory.createTestAccounts(2,userCountryCode);
		List<Product_vod__c> listProducts = BI_PL_TestDataFactory.createTestProduct(2,userCountryCode);

		List<BI_PL_Preparation__c> listPreparations = BI_PL_TestDataFactory.createPreparations(userCountryCode,positionCycles, listAccounts, listProducts);

		List<Product_vod__c> products = BI_PL_TestDataUtility.createProducts();


		List<BI_PL_Affiliation__c> affiliations = BI_PL_TestDataFactory.createAffiliationSI('Sales', listAccounts, products, userCountryCode);
		List<BI_PL_Affiliation__c> affiliationsI = BI_PL_TestDataFactory.createAffiliationSI('Information', listAccounts, products, userCountryCode);
		//affiliationsMap
		Test.startTest();

		BI_PL_affiliationDataToDetailBatch affiliationBatch1 = new BI_PL_affiliationDataToDetailBatch(userCountryCode,null);
		Database.executeBatch(affiliationBatch1);

		Test.stopTest();
	}*/
}