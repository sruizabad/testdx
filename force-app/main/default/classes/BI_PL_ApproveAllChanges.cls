/**
*  26/09/2018
*  Batch to approve all changes
*  @author Ferran Garcia Omega CRM BCN
*/
global class BI_PL_ApproveAllChanges extends BI_PL_PlanitProcess implements Database.Batchable<sObject>/*, Database.Stateful */ {

 	global String cycle;
    global String hierarchyName;

    //Constructor
    global BI_PL_ApproveAllChanges() {    }

	//Constructor
	/*global BI_PL_ApproveAllChanges(List<String> cycle, List<String> hierar) {
		this.cycle = cycle;
        this.hierarchyName = hierar;
	}*/
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		//this.cycle = (this.cycle == null) ? this.cycleIds[0] : this.cycle;
		//this.hierarchyName = (this.hierarchyName == null) ? this.hierarchyNames[0] : this.hierarchyName;

		String q = 'SELECT Id FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c IN :cycleIds AND BI_PL_Edited__c = true'+ 
					' AND BI_PL_Reviewed__c = false AND BI_PL_Rejected__c = false';
		if(hierarchyName != null){
			q += ' AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN :hierarchyNames';
		}
        return Database.getQueryLocator(q); 
	}

   	global void execute(Database.BatchableContext BC, List<BI_PL_Channel_detail_preparation__c> listObj) {
   		System.debug('LIST BI_PL_Channel_detail_preparation__c :: ' + listObj.size());
		for(BI_PL_Channel_detail_preparation__c det : listObj){
			det.BI_PL_Reviewed__c = true;
		}
		update listObj;	
	}
	
	global void finish(Database.BatchableContext BC) {
		Database.executeBatch(new BI_PL_ApproveUpperChanges(cycleIds, hierarchyNames));
	}
}