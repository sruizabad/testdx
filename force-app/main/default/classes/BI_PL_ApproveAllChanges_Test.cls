@isTest
private class BI_PL_ApproveAllChanges_Test {

	private static Account account1;
	private static Account account2;
	private static Account account3;
	private static Account account4;
	private static Account account5;

	@isTest static void test_method_one() {
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = testUser.Country_Code_BI__c;
		BI_PL_TestDataUtility.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		account1 = new Account(Name = 'Account1', External_ID_vod__c = 'testExtAccount1');
		account2 = new Account(Name = 'Account2', External_ID_vod__c = 'testExtAccount2');
		account3 = new Account(Name = 'Account3', External_ID_vod__c = 'testExtAccount3');
		account4 = new Account(Name = 'Account4', External_ID_vod__c = 'testExtAccount4');
		account5 = new Account(Name = 'Account5', External_ID_vod__c = 'testExtAccount5');

		insert new List<Account> {account1, account2, account3, account4, account5};

		List<Account> acc = [SELECT Id, External_ID_vod__c FROM Account];

		BI_PL_TestDataFactory.createCycleStructure('US');
		List<Product_vod__c> prod = BI_PL_TestDataFactory.createTestProduct(2,'US');
		List<BI_PL_Position_cycle__c> positionCycles = [SELECT Id, BI_PL_External_id__c FROM BI_PL_Position_cycle__c];
		BI_PL_TestDataFactory.createPreparations('US', positionCycles, acc, prod);

		List<String> cyclesW = new List<String>();
		List<String> herarW = new List<String>();


		List<BI_PL_Cycle__c> es = [SELECT Id FROM BI_PL_Cycle__c];
		for(BI_PL_Cycle__c x : es){
			cyclesW.add(String.valueOf(x.Id));
		}

		herarW.add('KAM/AM Hierarchy');
		herarW.add('hierarchyTest');

		List<BI_PL_Preparation__c> es1 = [SELECT Id FROM BI_PL_Preparation__c];

		BI_PL_TestDataFactory.createTestPreparationAction(String.valueOf(account1.Id), es1[0], es1[1], 'rep_detail_only', true);

		List<BI_PL_Preparation_action_item__c> es2 = [SELECT Id, BI_PL_Status__c FROM BI_PL_Preparation_action_item__c];
		for(BI_PL_Preparation_action_item__c exce : es2){
			exce.BI_PL_Status__c = 'Pending';
		}
		update es2;

		BI_PL_ApproveAllChanges b = new BI_PL_ApproveAllChanges();
		b.init('US', cyclesW, herarW, null, null);
		Database.executeBatch(b, 200);

	}
}