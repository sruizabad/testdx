/**
*  26/09/2018
*  Batch to approve all changes
*  @author Ferran Garcia Omega CRM BCN
*/
global class BI_PL_ApproveUpperChanges extends BI_PL_PlanitProcess implements Database.Batchable<sObject> {
	
	global List<String> cycle;
    global List<String> hierarchyName;

    //Constructor
    global BI_PL_ApproveUpperChanges() {    }

	//Constructor
	global BI_PL_ApproveUpperChanges(List<String> cycle, List<String> hierar) {
		this.cycle = cycle;
        this.hierarchyName = hierar;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String q = 'SELECT Id, BI_PL_Confirmed__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c IN :cycle';
		if(hierarchyName != null){
			q += ' AND BI_PL_Hierarchy__c IN :hierarchyName';
		}
        return Database.getQueryLocator(q); 
	}

   	global void execute(Database.BatchableContext BC, List<BI_PL_Position_cycle__c> listObj) {
   		System.debug('LIST BI_PL_Position_cycle__c :: ' + listObj.size());
		Set<Id> cyc = new Set<Id>();
		for(BI_PL_Position_cycle__c det : listObj){
			det.BI_PL_Confirmed__c = false;
			cyc.add(det.Id);		
		}
		update listObj;	

		String pres = 'SELECT Id, BI_PL_Status__c FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__c IN :listObj';

		List<BI_PL_Preparation__c> postn = Database.query(pres);
		System.debug('LIST BI_PL_Preparation__c :: ' + postn.size());
		for(BI_PL_Preparation__c esde : postn){
			esde.BI_PL_Status__c = 'Approved';
		}
		update postn;

		//-----------------------------------Para transfer and shares entre la misma jerarquia y que recibe de otra jerarquia  --------------------------------------
		Set<Id> actions = new Set<Id>();

		String actit = 'SELECT Id, BI_PL_Status__c, BI_PL_Parent__c, BI_PL_Rejected_reason__c FROM BI_PL_Preparation_action_item__c WHERE BI_PL_Target_preparation__c IN :postn AND BI_PL_Status__c = \'Pending\'';
		List<BI_PL_Preparation_action_item__c> actit1 = Database.query(actit);
		System.debug('LIST BI_PL_Preparation_action_item__c :: ' + actit1.size());
		for(BI_PL_Preparation_action_item__c esdex : actit1){
			esdex.BI_PL_Status__c = 'Canceled';
			esdex.BI_PL_Rejected_reason__c = Label.BI_PL_Transfer_Cancel;
			actions.add(esdex.BI_PL_Parent__c);
		}
		update actit1;

		Set<Id> noupdate = new Set<Id>();

		String uper = 'SELECT Id, BI_PL_Canceled__c FROM BI_PL_Preparation_action__c WHERE Id IN :actions AND BI_PL_Canceled__c= false AND BI_PL_All_approved__c = false';
		List<BI_PL_Preparation_action__c> uper1 = Database.query(uper);
		System.debug('LIST BI_PL_Preparation_action__c :: ' + uper1.size());
		for(BI_PL_Preparation_action__c esdez : uper1){
			esdez.BI_PL_Canceled__c = true;
			noupdate.add(esdez.Id);
		}
		
		//update uper1;
		//--------------------------------------------------------------------------------------------------------------------------------------------------------

		//-----------------------------------Para transfer and shares que se envían a otra jerarquia   --------------------------------------
		Set<Id> actionitemsout = new Set<Id>();

		String serv1 = 'SELECT Id, BI_PL_Canceled__c FROM BI_PL_Preparation_action__c WHERE BI_PL_Source_preparation__c IN :postn AND BI_PL_Canceled__c= false AND BI_PL_All_approved__c = false';
		List<BI_PL_Preparation_action__c> serv2 = Database.query(serv1);
		List<BI_PL_Preparation_action__c> serv3 = new List<BI_PL_Preparation_action__c>();
		System.debug('LIST BI_PL_Preparation_action__c :: ' + serv2.size());
		for(BI_PL_Preparation_action__c esdez1 : serv2){
			if(!noupdate.contains(esdez1.Id)){
				esdez1.BI_PL_Canceled__c = true;
				serv3.add(esdez1);
			}
			actionitemsout.add(esdez1.Id);
		}

		String postr = 'SELECT Id, BI_PL_Status__c, BI_PL_Parent__c, BI_PL_Rejected_reason__c FROM BI_PL_Preparation_action_item__c WHERE BI_PL_Parent__c IN :actionitemsout AND BI_PL_Status__c = \'Pending\'';
		List<BI_PL_Preparation_action_item__c> postr1 = Database.query(postr);
		System.debug('LIST BI_PL_Preparation_action_item__c :: ' + postr1.size());
		for(BI_PL_Preparation_action_item__c esdex1 : postr1){
			esdex1.BI_PL_Status__c = 'Canceled';
			esdex1.BI_PL_Rejected_reason__c = Label.BI_PL_Transfer_Cancel;
			
		}
		update postr1;		
		update serv3;
		update uper1;
		

		//--------------------------------------------------------------------------------------------------------------------------------------------------------

		for(BI_PL_Position_cycle__c det : listObj){
			det.BI_PL_Confirmed__c = true;
			cyc.add(det.Id);		
		}
		update listObj;
	}
	
	global void finish(Database.BatchableContext BC) {
	}
}