/**
 *  12/09/2017
 *  - Minor changes
 *  08/09/2017
 *  - New external id methods.
 *  21/08/2017
 *   - fixed a bug in generating external_id, replaced Y for y.
 *  08/08/2017
 *  - generateDetailPreparationExternalId() now takes into consideration the secondary product.
 *  04/07/2017
 *  - Exceptions in Custom labels.
 *
 *  13-06-2017
 *  Contains utility methods to copy a BITMAN hierarchy into PL.
 *  @author OMEGA CRM
 */
public without sharing class BI_PL_BITMANtoPLANiTImportUtility {

    private static Map<Id, BI_PL_Position__c> positionsByTerritoryId;
    private static Map<Id, BI_PL_Position_cycle__c> positionCyclesByTerritoryId;

    /**
     *  Creates the needed PL records to copy a BITMAN hierarchy into PL.
     *  @author OMEGA CRM
     */
    public static BITMANtoPLANiTImportResults importFromBitman(Id alignmentId, Date snapshotDate, Id cycleId, List<BI_TM_Position_relation__c> prs) {
        positionsByTerritoryId = new Map<Id, BI_PL_Position__c>();
        positionCyclesByTerritoryId = new Map<Id, BI_PL_Position_cycle__c>();

        System.debug('importFromBitman: ' + alignmentId + ' - ' + snapshotDate + ' - ' + cycleId);

        BI_TM_Alignment__c alignment = [SELECT Id, Name, BI_TM_Country_Code__c, BI_TM_Start_date__c, BI_TM_End_date__c, BI_TM_FF_type__c FROM BI_TM_Alignment__c WHERE Id = :alignmentId];

        System.debug('Alignment ' + alignment);

        String hierarchyName = alignment.Name;

        // 1.- BI_PL_Cycle__c
        BI_PL_Cycle__c cycle = [SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Field_force__c, BI_PL_Field_force__r.Name, BI_PL_Country_code__c FROM BI_PL_Cycle__c WHERE Id = :cycleId LIMIT 1];

        System.debug('Cycle ' + cycle);

        // 2.- BI_PL_Position__c
        // Get all BI_TM_Territory__c from BI_TM_Position_relation__c for the provided BI_TM_Alignment__c.
        /*List<BI_TM_Position_relation__c> prs = [SELECT Id, Name, BI_TM_Position__c, BI_TM_Position__r.Name, BI_TM_Parent_Position__r.Name, BI_TM_Parent_Position__r.BI_TM_FF_type__c, BI_TM_Parent_Position__c, BI_TM_Position__r.BI_TM_FF_type__c
                                                FROM BI_TM_Position_relation__c
                                                WHERE BI_TM_Alignment_Cycle__c = : alignmentId
                                                        AND BI_TM_Position__r.BI_TM_Start_date__c <= : snapshotDate
                                                        AND (BI_TM_Position__r.BI_TM_End_date__c = null OR BI_TM_Position__r.BI_TM_End_date__c >= : snapshotDate)];
        */
        Map<String, BI_PL_Position__c> positionsNotRepeat = new Map<String, BI_PL_Position__c>();

        for (BI_TM_Position_relation__c pr : prs) {
            //System.debug('BI_PL_BITMANtoPLANiTImportUtility BI_TM_Position_relation__c ' + pr);
            // Create BI_PL_Position__c:
            if (!positionsByTerritoryId.containsKey(pr.BI_TM_Position__c)) {
                BI_PL_Position__c position = generatePosition(pr.BI_TM_Position__r.Name, pr.BI_TM_Position__r.BI_TM_FF_type__r.Name, alignment.BI_TM_Country_Code__c);

                positionsByTerritoryId.put(pr.BI_TM_Position__c, position);
                positionsNotRepeat.put(position.BI_PL_External_id__c, position);
            }
            
            // Create parent BI_PL_Position__c:
            if (String.isNotBlank(pr.BI_TM_Parent_Position__c) && !positionsByTerritoryId.containsKey(pr.BI_TM_Parent_Position__c)) {
                BI_PL_Position__c position = generatePosition(pr.BI_TM_Parent_Position__r.Name, pr.BI_TM_Parent_Position__r.BI_TM_FF_type__r.Name, alignment.BI_TM_Country_Code__c);

                positionsByTerritoryId.put(pr.BI_TM_Parent_Position__c, position);
                positionsNotRepeat.put(position.BI_PL_External_id__c, position);
            }
        }


        System.debug('>>insert Positions');
        System.debug('BI_PL_BITMANtoPLANiTImportUtility positionsByTerritoryId.values() ' + positionsByTerritoryId.values());

        upsert positionsNotRepeat.values() BI_PL_External_id__c;

        // 3.- BI_PL_Position_cycle__c
        Set<BI_PL_Position_cycle__c> positionCycles = new Set<BI_PL_Position_cycle__c>();
        for (BI_TM_Position_relation__c pr : prs) {
            System.debug('**pr' + pr.BI_TM_Position__c +' ' + pr.BI_TM_Position__r.Name + ' ' + pr.BI_TM_Parent_Position__r.Name + ' ' + pr.BI_TM_Parent_Position__r.BI_TM_FF_type__r.Name + ' ' + pr.BI_TM_Parent_Position__c + ' ' + pr.BI_TM_Position__r.BI_TM_FF_type__r.Name);

            BI_PL_Position_cycle__c pc = new BI_PL_Position_cycle__c(BI_PL_Position__c = getPositionId(pr.BI_TM_Position__c),
                    BI_PL_Parent_position__c = getPositionId(pr.BI_TM_Parent_Position__c),
                    BI_PL_Cycle__c = cycle.Id,
                    BI_PL_Hierarchy__c = hierarchyName);

            pc.BI_PL_External_Id__c = generatePositionCycleExternalId(cycle, pc.BI_PL_Hierarchy__c, getPositionName(pr.BI_TM_Position__c));
            System.debug('** external Id ' +pc.BI_PL_External_Id__c );
            positionCyclesByTerritoryId.put(pr.BI_TM_Position__c, pc);
        }

        System.debug('>>insert position cycles');

        //insert positionCyclesByTerritoryId.values();
        upsert positionCyclesByTerritoryId.values() BI_PL_External_id__c;

        // 4.- BI_PL_Position_cycle_user
        // Get BI_TM_User_territory__c records which are inside (fully or partially) of the cycle's period.
        System.debug('BI_PL_Position_cycle_user');

        Set<BI_PL_Position_cycle_user__c> pcus = new Set<BI_PL_Position_cycle_user__c>();

        //Map<String, List<BI_PL_Position_cycle_user__c>> mapToCheckEqualPositionCycleUsers = new Map<String, List<BI_PL_Position_cycle_user__c>>();

        Set<String> aux = new Set<String>();

        Date cycleStartDate = cycle.BI_PL_Start_date__c;

        for (BI_TM_User_territory__c ut : [SELECT Id, Name, BI_TM_Territory1__c, BI_TM_Start_date__c, BI_TM_End_date__c, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c,
                                           BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__r.External_ID__c
                                           FROM BI_TM_User_territory__c WHERE BI_TM_Territory1__c IN: positionCyclesByTerritoryId.keySet()
                                           AND (BI_TM_Start_date__c <= : cycleStartDate AND (BI_TM_End_date__c >= : cycleStartDate OR BI_TM_End_date__c = null))]) {

            if (String.isNotBlank(ut.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c) && String.isNotBlank(ut.BI_TM_Territory1__c)) {
                BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = getPositionCycleIdByTerritory(ut.BI_TM_Territory1__c), BI_PL_User__c = ut.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);

                aux.add(ut.BI_TM_Territory1__c);

                //System.debug('BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__r.UserName ' + ut + ' - ' + ut.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__r.UserName);
                /*
                            if (String.isBlank(ut.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c))
                                throw new BI_PL_Exception(Label.BI_PL_User_not_defined_for_user_territory + ' ' + ut.Name);

                            if (String.isBlank(ut.BI_TM_Territory1__c))
                                throw new BI_PL_Exception(Label.BI_PL_Territory_not_defined_for_user_territory + ' ' + ut.Name);
                */
                pcu.BI_PL_External_Id__c = generatePositionCycleUserExternalId(cycle, getPositionCycleByTerritory(ut.BI_TM_Territory1__c).BI_PL_Hierarchy__c, getPositionName(ut.BI_TM_Territory1__c), ut.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__r.External_ID__c);
                pcus.add(pcu);
            }

            /*
                        if (!mapToCheckEqualPositionCycleUsers.containsKey(pcu.BI_PL_External_Id__c))
                            mapToCheckEqualPositionCycleUsers.put(pcu.BI_PL_External_Id__c, new List<BI_PL_position_cycle_user__c>());

                        mapToCheckEqualPositionCycleUsers.get(pcu.BI_PL_External_Id__c).add(pcu);*/
        }

        System.debug('number of territories. ' + aux.size());

        /*
        // Duplicated position cycle users that are not inserted:
        for (String k : mapToCheckEqualPositionCycleUsers.keySet()) {
            if (mapToCheckEqualPositionCycleUsers.get(k).size() > 1)
                System.debug(mapToCheckEqualPositionCycleUsers.get(k));
        }
        */
        System.debug('>>insert Position cycle users');

        upsert new List<BI_PL_position_cycle_user__c>(pcus) BI_PL_External_id__c;

        System.debug(positionCyclesByTerritoryId.size() + ' - ' + pcus.size() + ' - ' + positionsByTerritoryId.size());

        return new BITMANtoPLANiTImportResults(positionCyclesByTerritoryId.size(), pcus.size(), positionsByTerritoryId.size());
    }

    public class BITMANtoPLANiTImportResults {
        public Integer nPositionCycles {get; set;}
        public Integer nPositionCycleUsers {get; set;}
        public Integer nPositions {get; set;}

        public BITMANtoPLANiTImportResults (Integer nPositionCycles, Integer nPositionCycleUsers, Integer nPositions) {
            this.nPositionCycles = nPositionCycles;
            this.nPositionCycleUsers = nPositionCycleUsers;
            this.nPositions = nPositions;
        }
    }
    private static BI_PL_Position__c generatePosition(String name, String fieldForceName, String countryCode) {
        BI_PL_Position__c aux = new BI_PL_Position__c(Name = name, BI_PL_Field_force__c = fieldForceName, BI_PL_Country_code__c = countryCode);
        aux.BI_PL_External_id__c = generatePositionExternalId(aux);
        return aux;
    }

    private static Id getPositionId(Id bitmanPositionId) {
        if (bitmanPositionId != null)
            return positionsByTerritoryId.get(bitmanPositionId).Id;

        return null;
    }

    private static String getPositionName(Id bitmanPositionId) {
        if (bitmanPositionId != null)
            return positionsByTerritoryId.get(bitmanPositionId).Name;

        return null;
    }


    private static Id getPositionCycleIdByTerritory(Id bitmanPositionId) {
        if (!positionCyclesByTerritoryId.containsKey(bitmanPositionId))
            throw new BI_PL_Exception(Label.BI_PL_New_position_cycle_not_found_for_territory + bitmanPositionId);
        return positionCyclesByTerritoryId.get(bitmanPositionId).Id;
    }

    private static BI_PL_Position_cycle__c getPositionCycleByTerritory(Id bitmanPositionId) {
        return positionCyclesByTerritoryId.get(bitmanPositionId);
    }

    // EXTERNAL ID STRING METHODS

    public static String getDateString(Date d) {
        return DateTime.newInstance(d.year(), d.month(), d.day()).format('yyyyMMdd');
    }

    public static String generateCycleExternalId(BI_PL_Cycle__c cycle) {
        if (String.isBlank(cycle.BI_PL_Country_code__c))
            throw new BI_PL_Exception(Label.BI_PL_Country_code_null);

        if (cycle.BI_PL_Start_date__c == null)
            throw new BI_PL_Exception(Label.BI_PL_Start_date_null);

        if (cycle.BI_PL_End_date__c == null)
            throw new BI_PL_Exception(Label.BI_PL_End_date_null);

        // Either BI_PL_Field_force__r.Name / BI_PL_Field_force_name__c
        String fieldForce;
        try {
            // If the field "BI_PL_Field_force__r.Name" is not queried an exception is thrown.
            // Also, if the field is empty "BI_PL_Field_force__r.Name" an exception is thrown.
            System.debug('///////// Field force Name: ' + cycle.BI_PL_Field_force__r.Name);

            if (String.isBlank(cycle.BI_PL_Field_force__r.Name))
                throw new BI_PL_Exception(Label.BI_PL_Field_force_name_null);
            fieldForce = cycle.BI_PL_Field_force__r.Name;
            System.debug('//// ff : '+ fieldForce);
        } catch (exception e) {
            // If the field "BI_PL_Field_force_name__c" is not queried an exception is thrown.
            // Also, if the field is empty "BI_PL_Field_force_name__c" an exception is thrown.
            if (String.isBlank(cycle.BI_PL_Field_force_name__c))
                throw new BI_PL_Exception(Label.BI_PL_Field_force_name_null);
            fieldForce = cycle.BI_PL_Field_force_name__c;
        }

        return generateCycleExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, fieldForce);
    }

    public static String generateCycleExternalId(String countryCode, Date startDate, Date endDate, String fieldForceName) {
        // COUNTRYCODE_STARTDATE_ENDDATE_FIELDFORCE
        if (String.isBlank(countryCode))
            throw new BI_PL_Exception(Label.BI_PL_Country_code_null);

        if (startDate == null)
            throw new BI_PL_Exception(Label.BI_PL_Start_date_null);

        if (endDate == null)
            throw new BI_PL_Exception(Label.BI_PL_End_date_null);

        if (String.isBlank(fieldForceName))
            throw new BI_PL_Exception(Label.BI_PL_Field_force_name_null);

        return countryCode + '_' + getDateString(startDate) + '_' + getDateString(endDate) + '_' + fieldForceName;
    }

    public static String generatePositionExternalId(BI_PL_Position__c position) {
        // COUNTRYCODE_POSITIONNAME
        if (String.isBlank(position.BI_PL_Country_code__c))
            throw new BI_PL_Exception(Label.BI_PL_Country_code_null);

        if (String.isBlank(position.Name))
            throw new BI_PL_Exception(Label.BI_PL_Position_name_null);

        return position.BI_PL_Country_code__c + '_' + position.Name;
    }

    public static String generatePositionCycleExternalId(BI_PL_Cycle__c cycle, String hierarchy, String positionName) {
        return generatePositionCycleExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, cycle.BI_PL_Field_force__r.Name, hierarchy, positionName);
    }
    public static String generatePositionCycleExternalId(String countryCode, Date startDate, Date endDate, String fieldForceName, String hierarchy, String positionName) {
        // EXTERNALIDCYCLE_HIERACHYNAME_POSITIONNAME
        if (String.isBlank(hierarchy))
            throw new BI_PL_Exception(Label.BI_PL_Hierarchy_null);

        if (String.isBlank(positionName))
            throw new BI_PL_Exception(Label.BI_PL_Position_name_null);

        return generateCycleExternalId(countryCode, startDate, endDate, fieldForceName) + '_' + hierarchy + '_' + positionName;
    }
    public static String generatePositionCycleExternalId(BI_PL_Position_cycle__c positionCycle) {
        return generatePositionCycleExternalId(positionCycle.BI_PL_Cycle__r.BI_PL_Country_code__c, positionCycle.BI_PL_Cycle__r.BI_PL_Start_date__c, positionCycle.BI_PL_Cycle__r.BI_PL_End_date__c, positionCycle.BI_PL_Cycle__r.BI_PL_Field_force_name__c, positionCycle.BI_PL_Hierarchy__c, positionCycle.BI_PL_Position_name__c);
    }

    public static String generatePositionCycleUserExternalId(BI_PL_Cycle__c cycle, String hierarchy, String positionName, String userExternalId) {
        return generatePositionCycleUserExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, cycle.BI_PL_Field_force__r.Name, hierarchy, positionName, userExternalId);
    }
    public static String generatePositionCycleUserExternalId(String countryCode, Date startDate, Date endDate, String fieldForceName, String hierarchy, String positionName, String userExternalId) {
        // EXTERNALOIDPOSITIONCYCLE_userExternalId
        if (String.isBlank(positionName))
            throw new BI_PL_Exception(Label.BI_PL_Position_name_null);

        if (String.isBlank(userExternalId))
            throw new BI_PL_Exception(Label.BI_PL_User_external_id_null);

        return generatePositionCycleExternalId(countryCode, startDate, endDate, fieldForceName, hierarchy, positionName) + '_' + userExternalId;
    }

    public static String generatePreparationExternalId(String countryCode, Date startDate, Date endDate, String hierarchyName, String positionName, String cycleFieldForce) {
        // COUNTRYCODE_CYCLESTARTDATE_CYCLEENDDATE_HIERARCHYNAME_POSITIONNAME
        if (String.isBlank(countryCode))
            throw new BI_PL_Exception(Label.BI_PL_Country_code_null);

        if (startDate == null)
            throw new BI_PL_Exception(Label.BI_PL_Start_date_null);

        if (endDate == null)
            throw new BI_PL_Exception(Label.BI_PL_End_date_null);

        if (String.isBlank(hierarchyName))
            throw new BI_PL_Exception(Label.BI_PL_Hierarchy_null);

        if (String.isBlank(positionName))
            throw new BI_PL_Exception(Label.BI_PL_Position_name_null);

        return generatePositionCycleExternalId(countryCode, startDate, endDate, cycleFieldForce, hierarchyName, positionName);
    }
    public static String generatePreparationExternalId(BI_PL_Preparation__c preparation) {
        return generatePreparationExternalId(preparation.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c, preparation.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c, preparation.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c, preparation.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, preparation.BI_PL_Position_cycle__r.BI_PL_Position_name__c, preparation.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force_name__c);
    }


    public static String generateTargetPreparationExternalId(BI_PL_Cycle__c cycle, String hierarchyName, String positionName, String accountExternalId, String accountId, String cycleFieldForce) {
        // EXTERNALIDPREP_ACCOUNTEXTID
        return generateTargetPreparationExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchyName, positionName, accountExternalId, accountId, cycleFieldForce);
    }

    public static String generateTargetPreparationExternalId(String countryCode, Date startDate, Date endDate, String hierarchyName, String positionName, String accountExternalId, String accountId, String cycleFieldForce) {
        // EXTERNALIDPREP_ACCOUNTEXTID
        return generatePreparationExternalId(countryCode, startDate, endDate, hierarchyName, positionName, cycleFieldForce) + '_' + generateAccountExternalId(accountExternalId, accountId);
    }
    public static String generateTargetPreparationExternalId(BI_PL_Target_preparation__c target) {
        return generateTargetPreparationExternalId(target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c,
                target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c,
                target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                target.BI_PL_Target_customer__r.External_ID_vod__c,
                target.BI_PL_Target_customer__c,
                target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force_name__c);
    }


    public static String generateAccountExternalId(String externalId, String accountId) {
        return String.isNotBlank(externalId) ? externalId : accountId;
    }

    public static String generateChannelDetailPreparationExternalId(String countryCode, Date startDate, Date endDate, String hierarchyName, String positionName, String accountExternalId, String accountId, String channel, String cycleFieldForce) {
        // TARGETEXTID_CHANNEL
        String aux = generateTargetPreparationExternalId(countryCode, startDate, endDate, hierarchyName, positionName, accountExternalId, accountId, cycleFieldForce) + '_' + channel;
        return aux;
    }

    public static String generateChannelDetailPreparationExternalId(BI_PL_Channel_detail_preparation__c channelDetailPrep) {
        return generateChannelDetailPreparationExternalId(channelDetailPrep.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c,
                channelDetailPrep.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                channelDetailPrep.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c,
                channelDetailPrep.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                channelDetailPrep.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                channelDetailPrep.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c,
                channelDetailPrep.BI_PL_Target__r.BI_PL_Target_customer__c,
                channelDetailPrep.BI_PL_Channel__c,
                channelDetailPrep.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force_name__c);
    }


    public static String generateDetailPreparationExternalId(BI_PL_Cycle__c cycle, String hierarchyName, String positionName, String accountExternalId, String accountId, String channel, String productExternalId, String productId, String secondaryProductExternalId, String secondaryProductId, String cycleFieldForce) {
        return generateDetailPreparationExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchyName, positionName, accountExternalId, accountId, channel, productExternalId, productId, secondaryProductExternalId, secondaryProductId, cycleFieldForce);
    }

    public static String generateDetailPreparationExternalId(String countryCode, Date startDate, Date endDate, String hierarchyName, String positionName, String accountExternalId, String accountId, String channel, String productExternalId, String productId, String secondaryProductExternalId, String secondaryProductId, String cycleFieldForce) {
        // CHANDETEXTID_EXTIDPRODUCT
        return generateChannelDetailPreparationExternalId(countryCode, startDate, endDate, hierarchyName, positionName, accountExternalId, accountId, channel, cycleFieldForce) + '_' + generateProductExternalId(productExternalId, productId) + generateProductExternalId(secondaryProductExternalId, secondaryProductId);
    }

    public static String generateDetailPreparationExternalId(BI_PL_Detail_preparation__c detail) {
        return generateDetailPreparationExternalId(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c,
                detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c,
                detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c,
                detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,
                detail.BI_PL_Channel_detail__r.BI_PL_Channel__c,
                detail.BI_PL_Product__r.External_ID_vod__c,
                detail.BI_PL_Product__c,
                detail.BI_PL_Secondary_product__r.External_ID_vod__c,
                detail.BI_PL_Secondary_product__c,
                detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force_name__c);
    }

    public static String generateProductExternalId(String productExternalId, String productId) {
        if (String.isBlank(productExternalId) && String.isBlank(productId))
            return '';
        return String.isNotBlank(productExternalId) ? productExternalId : productId;
    }


}