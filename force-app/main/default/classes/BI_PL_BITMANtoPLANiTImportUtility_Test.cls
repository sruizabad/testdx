@isTest
public with sharing class BI_PL_BITMANtoPLANiTImportUtility_Test {

	static Profile profile;

	@isTest
	public static void test() {
		
		BI_PL_TestDataUtility.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting('US');

		profile = [SELECT Id FROM Profile WHERE Name LIKE 'US_%' LIMIT 1];


		User user1 = createUser('user1.omega');
		User user2 = createUser('user2.omega');
		User user3 = createUser('user3.omega');
		User user4 = createUser('user4.omega');

		insert new List<User> {user1, user2, user3, user4};
		

		// Data creation
		BI_TM_User_mgmt__c um1 = createUserManagement(user1);
		BI_TM_User_mgmt__c um2 = createUserManagement(user2);
		BI_TM_User_mgmt__c um3 = createUserManagement(user3);
		BI_TM_User_mgmt__c um4 = createUserManagement(user4);

		insert new List<BI_TM_User_mgmt__c> {um1, um2, um3, um4};

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'FieldForceName', BI_TM_Country_Code__c = 'US');

		insert fieldForce;

		BI_TM_Alignment__c alignment = new BI_TM_Alignment__c(BI_TM_Country_Code__c = 'US', BI_TM_FF_type__c = fieldForce.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1);

		insert alignment;

		BI_PL_Cycle__c selectedCycle = new BI_PL_Cycle__c(BI_PL_Start_date__c = alignment.BI_TM_Start_date__c, BI_PL_End_date__c = alignment.BI_TM_End_date__c, BI_PL_Field_force__c = alignment.BI_TM_FF_type__c, BI_PL_Country_code__c = alignment.BI_TM_Country_Code__c);

		insert selectedCycle;

		BI_TM_Territory__c territory1 = new BI_TM_Territory__c(Name = 'territory1', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1);

		insert territory1;

		BI_TM_Territory__c territory2 = new BI_TM_Territory__c(Name = 'territory2', BI_TM_Parent_Position__c = territory1.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1);
		BI_TM_Territory__c territory3 = new BI_TM_Territory__c(Name = 'territory3', BI_TM_Parent_Position__c = territory1.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1);

		insert new List<BI_TM_Territory__c> {territory2, territory3};

		BI_TM_Territory__c territory4 = new BI_TM_Territory__c(Name = 'territory4', BI_TM_Parent_Position__c = territory2.Id, 
			BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1);
		BI_TM_Territory__c territory5 = new BI_TM_Territory__c(Name = 'territory5', BI_TM_Parent_Position__c = territory2.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today() + 20, BI_TM_End_date__c = Date.today() + 21);
		BI_TM_Territory__c territory7 = new BI_TM_Territory__c(Name = 'territory7', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 21);
		
		
		insert new List<BI_TM_Territory__c> {territory4, territory5,  territory7};
		BI_TM_Territory__c territory6 = new BI_TM_Territory__c(Name = 'territory6', BI_TM_Parent_Position__c = territory7.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 21);
		insert territory6;

		BI_TM_Position_relation__c pr1 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory1.BI_TM_Parent_Position__c, BI_TM_Position__r = territory1, BI_TM_Position__c = territory1.Id);
		BI_TM_Position_relation__c pr2 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory2.BI_TM_Parent_Position__c, BI_TM_Position__r = territory2, BI_TM_Position__c = territory2.Id);
		BI_TM_Position_relation__c pr3 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory3.BI_TM_Parent_Position__c, BI_TM_Position__r = territory3, BI_TM_Position__c = territory3.Id);
		BI_TM_Position_relation__c pr4 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory4.BI_TM_Parent_Position__c, BI_TM_Position__r = territory4, BI_TM_Position__c = territory4.Id);

		BI_TM_Position_relation__c pr5 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory5.BI_TM_Parent_Position__c, BI_TM_Position__r = territory5, BI_TM_Position__c = territory5.Id);

		BI_TM_Position_relation__c pr6 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory7.id, BI_TM_Parent_Position__r = territory7, BI_TM_Position__r = territory6, BI_TM_Position__c = territory6.Id);

		

		pr1.BI_TM_Position__r.Name = 'territory1';
		pr2.BI_TM_Position__r.Name = 'territory2';
		pr3.BI_TM_Position__r.Name = 'territory3';
		pr4.BI_TM_Position__r.Name = 'territory4';
		pr5.BI_TM_Position__r.Name = 'territory5';
		pr6.BI_TM_Position__r.Name = 'territory6';

		List<BI_TM_Position_relation__c> lstPositionRelation = new List<BI_TM_Position_relation__c> {pr1, pr2, pr3, pr4, pr5, pr6};

		insert lstPositionRelation;


		BI_TM_User_territory__c pcu1 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory1.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user1.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um1.Id, BI_TM_External_Id__c = 'test1');
		BI_TM_User_territory__c pcu2 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory2.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user2.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um2.Id, BI_TM_External_Id__c = 'test2');
		BI_TM_User_territory__c pcu3 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory3.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user3.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um3.Id, BI_TM_External_Id__c = 'test3');
		BI_TM_User_territory__c pcu4 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory4.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user4.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um4.Id, BI_TM_External_Id__c = 'test4');

		BI_TM_User_territory__c pcu5 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory4.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user3.Id, BI_TM_Start_date__c = Date.today() + 20, BI_TM_End_date__c = Date.today() + 21, BI_TM_User_mgmt_tm__c = um4.Id, BI_TM_External_Id__c = 'test5');
		
		
		insert new List<BI_TM_User_territory__c> {pcu1, pcu2, pcu3, pcu4, pcu5};

		Test.startTest();

		BI_PL_Cycle__c cycle = [SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Field_force__c, BI_PL_Field_force__r.Name, BI_PL_Country_code__c FROM BI_PL_Cycle__c LIMIT 1];

		BI_TM_Position_relation__c[] positionRelations = [SELECT Id, Name, BI_TM_Position__c, BI_TM_Position__r.Name, BI_TM_Parent_Position__r.Name, BI_TM_Parent_Position__r.BI_TM_FF_type__r.Name, BI_TM_Parent_Position__c, BI_TM_Position__r.BI_TM_FF_type__r.Name FROM BI_TM_Position_relation__c
		        WHERE BI_TM_Alignment_Cycle__c = : alignment.Id
		                AND BI_TM_Position__r.BI_TM_Start_date__c <= TODAY
		                AND (BI_TM_Position__r.BI_TM_End_date__c = null OR BI_TM_Position__r.BI_TM_End_date__c >= TODAY)];

		BI_PL_BITMANtoPLANiTImportUtility.importFromBitman(alignment.Id, Date.today(), cycle.Id, positionRelations);
		String externalId = '';
		try {
			externalId = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId(selectedCycle);
		} catch (Exception e) {}

		Test.stopTest();

		// Checking
		// Position
		List<BI_PL_Position__c> positions = [SELECT Id, Name, BI_PL_Field_Force__c, BI_PL_External_id__c, BI_PL_Country_code__c FROM BI_PL_Position__c ORDER BY Id];

		System.debug('>>>>positions ' + positions);

		System.assertEquals(6, positions.size(), 'Number of BI_PL_Position__c unexpected.');

		checkPosition(fieldForce.Name, positions.get(0));
		checkPosition(fieldForce.Name, positions.get(1));
		checkPosition(fieldForce.Name, positions.get(2));
		checkPosition(fieldForce.Name, positions.get(3));

		String testExtId = BI_PL_BITMANtoPLANiTImportUtility.generatePositionExternalId(positions.get(0));

		// Position Cycle
		List<BI_PL_Position_cycle__c> positionCycles = [SELECT Id, BI_PL_Position__c, BI_PL_Parent_position__c, BI_PL_Cycle__c, BI_PL_Hierarchy__c, BI_PL_External_id__c FROM BI_PL_Position_cycle__c ORDER BY Id];

		System.assertEquals(5, positionCycles.size(), 'Number of BI_PL_Position_cycle__c unexpected.');

		checkPositionCycle(cycle, positionCycles.get(0), positions.get(0), null);
		checkPositionCycle(cycle, positionCycles.get(1), positions.get(1), positions.get(0).Id);
		checkPositionCycle(cycle, positionCycles.get(2), positions.get(2), positions.get(0).Id);
		checkPositionCycle(cycle, positionCycles.get(3), positions.get(3), positions.get(1).Id);

		// Position Cycle User
		/*List<BI_PL_Position_cycle_user__c> pcus = [SELECT Id, BI_PL_Position_cycle__c, BI_PL_User__c, BI_PL_External_id__c, BI_PL_User__r.External_id__c FROM BI_PL_Position_cycle_user__c ORDER BY Id];

		System.assertEquals(4, pcus.size(), 'Number of BI_PL_Position_cycle_user__c unexpected.');

		checkPositionCycleUser(cycle, pcus.get(0), positionCycles.get(0), user1.Id, positions.get(0).Name, user1.UserName);
		checkPositionCycleUser(cycle, pcus.get(1), positionCycles.get(1), user2.Id, positions.get(1).Name, user2.UserName);
		checkPositionCycleUser(cycle, pcus.get(2), positionCycles.get(2), user3.Id, positions.get(2).Name, user3.UserName);
		checkPositionCycleUser(cycle, pcus.get(3), positionCycles.get(3), user4.Id, positions.get(3).Name, user4.UserName);*/

		//Errors
		try {
			BI_PL_Cycle__c wrongCycle1 = new BI_PL_Cycle__c(BI_PL_Start_date__c = alignment.BI_TM_Start_date__c, BI_PL_End_date__c = alignment.BI_TM_End_date__c, BI_PL_Field_force__c = alignment.BI_TM_FF_type__c, BI_PL_Country_code__c = '');
			Id idNull1 = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId(wrongCycle1);
		} catch (Exception e) {
			Boolean expectedExceptionThrown =  e.getMessage().contains(Label.BI_PL_Country_code_null) ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
		try {
			BI_PL_Cycle__c wrongCycle2 = new BI_PL_Cycle__c(BI_PL_Start_date__c = null, BI_PL_End_date__c = alignment.BI_TM_End_date__c, BI_PL_Field_force__c = alignment.BI_TM_FF_type__c, BI_PL_Country_code__c = 'US');
			Id idNull2 = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId(wrongCycle2);
		} catch (Exception e) {
			Boolean expectedExceptionThrown =  e.getMessage().contains(Label.BI_PL_Start_date_null) ? true : false;
			System.debug('********exception start date null :');
			System.debug('********'+e.getMessage());
			System.AssertEquals(expectedExceptionThrown, true);
		}

		try{
			BI_PL_Cycle__c wrongCycle3 = new BI_PL_Cycle__c(BI_PL_Start_date__c = alignment.BI_TM_Start_date__c, BI_PL_End_date__c = null, BI_PL_Field_force__c = alignment.BI_TM_FF_type__c, BI_PL_Country_code__c = 'US');
			Id idNull6 = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId(wrongCycle3);
		}catch(Exception e){
			Boolean expectedExceptionThrown =  e.getMessage().contains(Label.BI_PL_End_date_null) ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}

		try {
			Id idNull3 = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId('',  alignment.BI_TM_Start_date__c,  alignment.BI_TM_End_date__c, alignment.BI_TM_FF_type__c);
		} catch (Exception e) {}
		try {
			Id idNull4 = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId('US',  null,  alignment.BI_TM_End_date__c, alignment.BI_TM_FF_type__c);
		} catch (Exception e) {}

		try {
			Id idNull5 = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId('US',  alignment.BI_TM_Start_date__c,  null, alignment.BI_TM_FF_type__c);
		} catch (Exception e) {}
		try{
			Id idNUll7 = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId('US', alignment.BI_TM_Start_date__c, alignment.BI_TM_End_date__c, '');
		}catch(Exception e){
			Boolean expectedExceptionThrown =  e.getMessage().contains(Label.BI_PL_Field_force_name_null) ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
		BI_TM_FF_type__c fieldForce2 = new BI_TM_FF_type__c(Name = 'FieldForceNameD', BI_TM_Country_Code__c = 'BR');

		insert fieldForce2;


		//correct generation of cycle External Id
		BI_PL_Cycle__c correctCycle = new BI_PL_Cycle__c(BI_PL_Start_date__c = alignment.BI_TM_Start_date__c, BI_PL_End_date__c = alignment.BI_TM_End_date__c, BI_PL_Field_force__c = fieldForce2.id,  BI_PL_Country_code__c = 'BR');
		insert correctCycle;

		correctCycle = [SELECT id, BI_PL_Field_force__c, BI_PL_Field_Force_name__c, BI_PL_Cycle__c.BI_PL_Country_code__c, BI_PL_End_date__c, BI_PL_Start_date__c, BI_PL_Field_force__r.Name FROM BI_PL_Cycle__c WHERE BI_PL_Field_force__c = :fieldForce2.id LIMIT 1];
		System.debug('******fieldforcecycle:' + correctCycle.BI_PL_Field_force__c + ' field force name; ' + correctCycle.BI_PL_Field_force__r.Name);
		String cycleExternalID = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId(correctCycle);
		Boolean cycleEiBlank = String.isBlank(cycleExternalID);
		System.assertEquals(cycleEiBlank, false);

		//errors generatre position external id
		try{
			BI_PL_Position__c wrongPosition1 = new BI_PL_Position__c(Name = 'Position12345', BI_PL_Country_code__c = '');
			BI_PL_BITMANtoPLANiTImportUtility.generatePositionExternalId(wrongPosition1);
		}catch(Exception e){
			Boolean expectedExceptionThrown =  e.getMessage().contains(Label.BI_PL_Country_code_null) ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}

		try{
			BI_PL_Position__c wrongPosition2 = new BI_PL_Position__c(Name = '', BI_PL_Country_code__c = 'US');
			BI_PL_BITMANtoPLANiTImportUtility.generatePositionExternalId(wrongPosition2);
		}catch(Exception e){
			Boolean expectedExceptionThrown =  e.getMessage().contains(Label.BI_PL_Position_name_null) ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
		
		//error generate position cycle external id
		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId('US', date.today(),date.today()+1,'ff','','position');
		}catch(Exception e){
			Boolean expectedExceptionThrown =  e.getMessage().contains(Label.BI_PL_Hierarchy_null) ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}

		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId('US', date.today(),date.today()+1,'ff','hierarcgy','');
		}catch(Exception e){
			Boolean expectedExceptionThrown = e.getMessage().contains(Label.BI_PL_Position_name_null) ? true : false;
			System.assertEquals(expectedExceptionThrown,true);
		}

		//error generate position cycle user external id

		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId('US',date.today(),date.today()+2,'ff','hierarchy','','userEI');
		}catch(Exception e){
			Boolean expectedExceptionThrown = e.getMessage().contains(Label.BI_PL_Position_name_null);
			System.assertEquals(expectedExceptionThrown,true);
		}
		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId('US',date.today(),date.today()+2,'ff','hierarchy','posname','');
		}catch(Exception e){
			Boolean expectedExceptionThrown = e.getMessage().contains(Label.BI_PL_User_external_id_null);
			System.assertEquals(expectedExceptionThrown,true);
		}
		
		//errors generate preparation external id
		BI_PL_Position_cycle__c pc = positionCycles.get(0);
		BI_PL_Preparation__c prep = new BI_PL_Preparation__c();
		prep.BI_PL_Position_cycle__c = pc.id;
		prep.BI_PL_Country_code__c = 'US';
		prep.BI_PL_External_id__c = 'ExternalIdPreparationTest1983247';

		insert prep;

		prep = [SELECT id from BI_PL_Preparation__c LIMIT 1];
		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId(prep);
		}catch(Exception e){}

		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('', date.today(), date.today()+3, 'hierarchy', 'positionname', 'ff');
		}catch(Exception e){
			Boolean expectedExceptionThrown = e.getMessage().contains(Label.BI_PL_Country_code_null);
			System.assertEquals(expectedExceptionThrown,true);
		}

		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('US', null, date.today()+3, 'hierarchy', 'positionname', 'ff');
		}catch(Exception e){
			Boolean expectedExceptionThrown = e.getMessage().contains(Label.BI_PL_Start_date_null);
			System.assertEquals(expectedExceptionThrown,true);
		}
		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('US', date.today(), null, 'hierarchy', 'positionname', 'ff');
		}catch(Exception e){
			Boolean expectedExceptionThrown = e.getMessage().contains(Label.BI_PL_End_date_null);
			System.assertEquals(expectedExceptionThrown,true);
		}
		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('US', date.today(), date.today()+3, '', 'positionname', 'ff');
		}catch(Exception e){
			Boolean expectedExceptionThrown = e.getMessage().contains(Label.BI_PL_Hierarchy_null);
			System.assertEquals(expectedExceptionThrown,true);
		}
		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('US', date.today(), date.today()+3, 'hierarchy', '', 'ff');
		}catch(Exception e){
			Boolean expectedExceptionThrown = e.getMessage().contains(Label.BI_PL_Position_name_null);
			System.assertEquals(expectedExceptionThrown,true);
		}

		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('US', date.today(), date.today()+3, 'hierarchy', 'positionname', 'ff');
		}catch(Exception e){
			
		}

		List<Account> listacc = BI_PL_TestDataFactory.createTestAccounts(1,'US');
		//target preparation external id
		BI_PL_Target_Preparation__c target = new BI_PL_Target_Preparation__c();
		target.BI_PL_Header__c = prep.id;
		target.BI_PL_Target_customer__c = listacc.get(0).id;
		target.BI_PL_External_id__c = 'TargaetPreparationeTESTExternalId';
		insert target;

		try{
			BI_PL_BITMANtoPLANiTImportUtility.generateTargetPreparationExternalId(target);
		}catch(Exception e){}

		try{
			BI_PL_BITMANtoPLANiTImportUtility.generateTargetPreparationExternalId(cycle,'hi','position','accId','acci','ff');
		}catch(Exception e){}

		//account external id
		BI_PL_BITMANtoPLANiTImportUtility.generateAccountExternalId('testExId',listacc.get(0).id);

		//channel detail preparation external id
		BI_PL_Channel_detail_preparation__c cdp = new BI_PL_Channel_detail_preparation__c(BI_PL_Channel__c = 'rep_clm', BI_PL_target__c = target.id, BI_PL_External_id__c = 'cdp External Id 21421');
		insert cdp;
		try{
			String cdpExId = BI_PL_BITMANtoPLANiTImportUtility.generateChannelDetailPreparationExternalId(cdp);
		}catch(Exception e){
			Boolean expectedExceptionThrown = e.getMessage().contains(Label.BI_PL_Country_code_null);
			System.assertEquals(expectedExceptionThrown,true);
		}

		//detail preparation exeternal id
		BI_PL_Detail_preparation__c detail = new BI_PL_Detail_preparation__c(
						BI_PL_Adjusted_details__c = 1,
					    BI_PL_External_id__c = 'detalpreparartionExternalID',
					    BI_PL_Planned_details__c = 5,
					    BI_PL_Segment__c = 'No Segmentation',
					    BI_PL_Channel_detail__r = new BI_PL_Channel_detail_preparation__c(BI_PL_External_Id__c = 'chanelBI_PL_External_Id__c'),
					    BI_PL_Product__r = new Product_vod__c(External_ID_vod__c = 'prod_External_ID_vod__c'));
		try{
			String detailPreparationExternalId = BI_PL_BITMANtoPLANiTImportUtility.generateDetailPreparationExternalId(detail);
		}catch(Exception e){}
		
		try{
			BI_PL_BITMANtoPLANiTImportUtility.generateDetailPreparationExternalId(cycle, 'hh','position','accountexid','accid','chanel','productei','secondaryProductExid','secprodid','test','ff');
		}catch(Exception e){}

		//product external id
		String productExID = BI_PL_BITMANtoPLANiTImportUtility.generateProductExternalId('','');
		System.assertEquals(productExID,'');

		try{
			BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(pc);
		}catch(Exception e){}

		

	}

	private static void checkPosition(String territoryFieldForce, BI_PL_Position__c position) {
		System.assertEquals(territoryFieldForce, position.BI_PL_Field_Force__c, 'Position Field force not expected');

		System.assertEquals(BI_PL_BITMANtoPLANiTImportUtility.generatePositionExternalId(position), position.BI_PL_External_id__c, 'External ID does not match.');
	}

	private static void checkPositionCycle(BI_PL_Cycle__c cycle, BI_PL_Position_cycle__c positionCycle, BI_PL_Position__c position, Id parentPosition) {
		System.assertEquals(positionCycle.BI_PL_Position__c, position.Id, 'Position Field for position cycle \'' + positionCycle + '\' not expected');
		System.assertEquals(positionCycle.BI_PL_Parent_Position__c, parentPosition, 'Parent Position Field for position cycle \'' + positionCycle + '\' not expected');
		String testExtID = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, positionCycle.BI_PL_Hierarchy__c, position.Name);
		System.assertEquals(BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, positionCycle.BI_PL_Hierarchy__c, position.Name), positionCycle.BI_PL_External_id__c, 'External ID does not match.');
	}

	private static void checkPositionCycleUser(BI_PL_Cycle__c cycle, BI_PL_Position_cycle_user__c pcu, BI_PL_Position_cycle__c positionCycle, Id userId, String positionName, String userName) {
		System.assertEquals(pcu.BI_PL_Position_cycle__c, positionCycle.Id, 'Position cycle Field for position cycle \'' + pcu + '\' not expected');
		System.assertEquals(pcu.BI_PL_User__c, userId, 'User Field for position cycle user \'' + pcu + '\' not expected');
		String testExtID = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, cycle.BI_PL_Field_Force__r.Name, positionCycle.BI_PL_Hierarchy__c, positionName, pcu.BI_PL_User__r.External_id__c);
		System.assertEquals(BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, cycle.BI_PL_Field_Force__r.Name, positionCycle.BI_PL_Hierarchy__c, positionName, pcu.BI_PL_User__r.External_id__c), pcu.BI_PL_External_id__c, 'External ID does not match.');
	}

	private static User createUser(String name) {
		return new User(Alias = name.substring(0, 5), Email = name + '@testorg.com',
		                EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US',
		                LocaleSidKey = 'en_US', ProfileId = profile.Id, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = name + '_External_id_Test');
	}
	private static BI_TM_User_mgmt__c createUserManagement(User user) {
		return new BI_TM_User_mgmt__c(BI_TM_Currency__c = 'U.S. Dollar', BI_TM_UserId_Lookup__c = user.Id, BI_TM_LanguageLocaleKey__c = user.LanguageLocaleKey,
		                              BI_TM_LocaleSidKey__c = user.LocaleSidKey, BI_TM_Username__c = user.UserName, BI_TM_TimeZoneSidKey__c = user.TimeZoneSidKey);

	}

}