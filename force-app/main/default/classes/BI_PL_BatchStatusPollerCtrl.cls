/**
 * Controlle class for bi_pl_batch_status_poller component.
 */
public with sharing class BI_PL_BatchStatusPollerCtrl {

    @RemoteAction
    public static List<BatchJob> getBatchJobs(List<String> idsJobs, List<String> classNames, Boolean onlyMyBatches, String createdDate){
        //Create new list of BatchJobs, a wrapper class that includes the job and percent complete.
        List<BatchJob> batchJobs = new List<BatchJob>();

        //Generate query for retrieve batches information
        String query = 'SELECT TotalJobItems, Status, ExtendedStatus, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id, CreatedDate, CreatedById, CreatedBy.Name, CompletedDate, ApexClassId, ApexClass.Name FROM AsyncApexJob WHERE JobType = \'BatchApex\' AND';
    
        if(idsJobs != null && !idsJobs.isEmpty()) {
            query += ' Id IN (\'' + String.join(idsJobs, '\',\'') + '\')';
        } else if (classNames != null && !classNames.isEmpty()) {
            query += ' ApexClass.Name IN (\'' + String.join(classNames, '\',\'') + '\') AND CreatedDate > ' + createdDate;
            if(onlyMyBatches) {
                query += ' AND CreatedById = \'' + UserInfo.getUserId() + '\'';
            }
        }

        System.debug(loggingLevel.Error, '*** BI_PL_BatchStatusPollerCtrl -> getBatchJobs -> query: ' + query);

        //Query the Batch apex jobs
        if(!query.endsWith('AND')){
            for(AsyncApexJob a : (List<AsyncApexJob>) Database.query(query)){
                 
                Double itemsProcessed = a.JobItemsProcessed; 
                Double totalItems = a.TotalJobItems;
     
                BatchJob j = new BatchJob();
                j.job = a;
     
                //Determine the pecent complete based on the number of batches complete
                if(totalItems == 0){
                    //A little check here as we don't want to divide by 0.
                    j.percentComplete = 100;
                }else{
                    j.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
                }
     
                batchJobs.add(j);
            }
        }
        System.debug(loggingLevel.Error, '*** batchJobs: ' + batchJobs);
        return batchJobs;
    }
 
    //This is the wrapper class the includes the job itself and a value for the percent complete
    public class BatchJob{
        public AsyncApexJob job {get; set;}
        public Integer percentComplete {get; set;}
    }
}