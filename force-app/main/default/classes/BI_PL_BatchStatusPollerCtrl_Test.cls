@Istest(SeeAllData=true)
public class BI_PL_BatchStatusPollerCtrl_Test {
	static testMethod void testPoller() {
		//BI_PL_BatchStatusPollerCtrl.getBatchJobs(null);
		List<AsyncApexJob> js = [SELECT Id FROM AsyncApexJob];
		String bId = !js.isEmpty() ? js.get(0).Id : '';
		try{
			BI_PL_BatchStatusPollerCtrl.getBatchJobs(new List<String>{bId}, null, null, null);	
		}catch(Exception e){}
		try{
			BI_PL_BatchStatusPollerCtrl.getBatchJobs(null, new List<String>{'testClass'}, null, null);
		}catch(Exception e){}
		try{
			BI_PL_BatchStatusPollerCtrl.getBatchJobs(null, new List<String>{'testClass'}, true, null);
		}catch(Exception e){}
	}
}