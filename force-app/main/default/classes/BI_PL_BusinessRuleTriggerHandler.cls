public with sharing class BI_PL_BusinessRuleTriggerHandler implements BI_PL_TriggerInterface  {

	Set<Id> specialtyIds;
	Map<Id,Customer_Attribute_BI__c> customerAtrMap;
	Map<Id,Product_vod__c> producCat;
	Map<Id,Product_vod__c> secondaryProducCat;
	Map<Id,BI_PL_Position__c> positionMap;
	//Constructor
	public BI_PL_BusinessRuleTriggerHandler(){

	}


	public void bulkBefore() {
		//specialtyIds = new Set<Id>();

		Set<Id> productIds = new Set<Id>();
		Set<Id> secProducIds = new Set<Id>();
		Set<Id> positionIds = new Set<Id>();
		for(BI_PL_Business_rule__c bs : (List<BI_PL_Business_rule__c>)Trigger.new){
			//specialtyIds.add(bs.BI_PL_Specialty__c);
			productIds.add(bs.BI_PL_Product__c);
			secProducIds.add(bs.BI_PL_Secondary_product__c);
			positionIds.add(bs.BI_PL_Position__c);


		}

		//customerAtrMap = new Map<Id, Customer_Attribute_BI__c> ([SELECT External_ID_BI__c FROM Customer_Attribute_BI__c WHERE Id IN :specialtyIds]);
		producCat = new Map<Id, Product_vod__c> ([SELECT External_ID_vod__c FROM Product_vod__c WHERE Id IN :productIds]);
		secondaryProducCat = new Map<Id, Product_vod__c>([SELECT External_ID_vod__c FROM Product_vod__c WHERE Id IN :secProducIds]);
		positionMap = new Map<Id, BI_PL_Position__c>([SELECT BI_PL_External_id__c FROM BI_PL_Position__c WHERE Id IN :positionIds]);


	}
	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkAfter() {
	}

	/**
	 * beforeInsert
	 *
	 * This method is called iteratively for each record to be inserted during a BEFORE
	 * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
	 */
	public void beforeInsert(SObject so) {

		BI_PL_Business_rule__c bs = (BI_PL_Business_rule__c)so;
		String ruleType = bs.BI_PL_Type__c;
		if(ruleType == BI_PL_BusinessRuleUtility.CUSTOMER_SPECIALTY){
		
			String specialtyName = bs.BI_PL_Specialty__c;//customerAtrMap.get(bs.BI_PL_Specialty__c).External_ID_BI__c;

			String productExId = producCat.get(bs.BI_PL_Product__c).External_ID_vod__c;


			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.CUSTOMER_SPECIALTY + '_' + specialtyName + '_'+ productExId + '_' + 'BR';
		}else if(ruleType == BI_PL_BusinessRuleUtility.SEGMENTATION){

			String productExId = producCat.get(bs.BI_PL_Product__c).External_ID_vod__c;

			if(secondaryProducCat.get(bs.BI_PL_Secondary_product__c) != null){
				String secProducExId = secondaryProducCat.get(bs.BI_PL_Secondary_product__c).External_ID_vod__c;

				bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.SEGMENTATION + '_' + bs.BI_PL_Field_force__c + '_' + bs.BI_PL_Criteria__c + '_' + productExId + '_' + secProducExId + '_' + 'BR';
			}else{
				bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.SEGMENTATION + '_' + bs.BI_PL_Field_force__c + '_' + bs.BI_PL_Criteria__c + '_' + productExId + '_' + '_'+'BR';
			}

			
		}else if(ruleType == BI_PL_BusinessRuleUtility.ACCOUNT_DROP_PERCENTAGE){
			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.ACCOUNT_DROP_PERCENTAGE + '_' + bs.BI_PL_Field_force__c + '_' + bs.BI_PL_Percentage__c + '_' +'BR';

		}else if(ruleType == BI_PL_BusinessRuleUtility.ACCOUNT_SHARE_PERCENTAGE){
			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.ACCOUNT_SHARE_PERCENTAGE + '_' + bs.BI_PL_Field_force__c + '_' + bs.BI_PL_Percentage__c + '_' +'BR';

		} else if(ruleType == BI_PL_BusinessRuleUtility.ACCOUNT_TRANSFER_PERCENTAGE){
			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.ACCOUNT_TRANSFER_PERCENTAGE + '_' + bs.BI_PL_Field_force__c + '_' + bs.BI_PL_Percentage__c + '_' +'BR';

		} else if(ruleType == BI_PL_BusinessRuleUtility.THRESHOLD){

			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.THRESHOLD + '_' + bs.BI_PL_Field_force__c + '_' + bs.BI_PL_Visits_per_day__c + '_' + bs.BI_PL_Required_capacity__c + '_' + 'BR';
		}else if(ruleType == BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY){

			String productExId = producCat.get(bs.BI_PL_Product__c).External_ID_vod__c;
			String positionExId = positionMap.get(bs.BI_PL_Position__c).BI_PL_External_id__c;

			if(secondaryProducCat.get(bs.BI_PL_Secondary_product__c) != null){

				String secProducExId = secondaryProducCat.get(bs.BI_PL_Secondary_product__c).External_ID_vod__c;

				bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY + '_' + positionExId + '_' + productExId + '_' + secProducExId + '_' + 'BR';
			}else{
				bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY + '_' + positionExId + '_' + productExId + '_' +  '_' + 'BR';
			}

			
		}else if(ruleType == BI_PL_BusinessRuleUtility.FORBIDDEN_PRODUCT){
			
			String specialtyName = bs.BI_PL_Specialty__c;//customerAtrMap.get(bs.BI_PL_Specialty__c).External_ID_BI__c;

			String productExId = producCat.get(bs.BI_PL_Product__c).External_ID_vod__c;

			
			if(secondaryProducCat.get(bs.BI_PL_Secondary_product__c) != null){

				String secProducExId = secondaryProducCat.get(bs.BI_PL_Secondary_product__c).External_ID_vod__c;
				bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.FORBIDDEN_PRODUCT + '_' + specialtyName + '_' + productExId + '_'+ secProducExId + '_' + 'BR';

			}else{
				bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.FORBIDDEN_PRODUCT + '_' + specialtyName + '_' + productExId + '_' +'_'+ 'BR';
				
			}
		}else if(ruleType == BI_PL_BusinessRuleUtility.MUST_ADD_PRODUCT){
			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.MUST_ADD_PRODUCT + '_' + bs.BI_PL_Field_force__c + '_' + 'BR';
		}else if(ruleType == BI_PL_BusinessRuleUtility.FIELD_FORCE_PRODUCTS){
			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.FIELD_FORCE_PRODUCTS + '_' + bs.BI_PL_Field_force__c + '_' + 'BR';
		}else if(ruleType == BI_PL_BusinessRuleUtility.TGT_FLAG){
			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.TGT_FLAG + '_' + bs.BI_PL_Field_force__c + '_' + 'BR';
		//GLOS-1026 - Workload
		}else if(ruleType == BI_PL_BusinessRuleUtility.WORKLOAD){
			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.WORKLOAD + '_' + bs.BI_PL_Field_force__c + '_' + 'BR';

		}else if(ruleType == BI_PL_BusinessRuleUtility.FREQUENCY_VARIATION){
			bs.BI_PL_External_id__c = bs.BI_PL_Country_code__c + '_' + BI_PL_BusinessRuleUtility.FREQUENCY_VARIATION + '_' + bs.BI_PL_Field_force__c + '_' + 'BR';
		}
		
	}

	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
	 */
	public void beforeUpdate(SObject oldSo, SObject so) {
	}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
	 */
	public void beforeDelete(SObject so) {
	}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
	 */
	public void afterInsert(SObject so) {
	}


	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
	 */
	public void afterUpdate(SObject oldSo, SObject so) {
	}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 */
	public void afterDelete(SObject so) {
	}

	/**
	 * andFinally
	 *
	 * This method is called once all records have been processed by the trigger. Use this
	 * method to accomplish any final operations such as creation or updates of other records.
	 */
	public void andFinally() {

	}


}