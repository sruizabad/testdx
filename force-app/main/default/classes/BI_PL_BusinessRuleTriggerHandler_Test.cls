@isTest
private class BI_PL_BusinessRuleTriggerHandler_Test {
	
	@isTest static void test_method_one() {

        Id currentUserId = UserInfo.getUserId();

        String userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = : currentUserId LIMIT 1].Country_Code_BI__c;

        BI_PL_Position__c position = new BI_PL_Position__c(BI_PL_Country_code__c = 'US', BI_PL_External_Id__c = 'PositionExBRTest', BI_PL_Field_force__c = 'Cardiology', Name = 'positionBRtest');
		insert position;
		
		List<Product_vod__c> products = BI_PL_TestDataFactory.createTestProduct(2, userCountryCode);
		Test.startTest();
		BI_PL_TestDataFactory.insertBusinessRules(products, userCountryCode, position);

		BI_PL_Business_rule__c br1 = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Channel__c = 'rep_detail_only',BI_PL_Position__c = position.id,BI_PL_Product__c = products.get(0).id,
																BI_PL_Secondary_product__c = products.get(1).id, BI_PL_Type__c = 'Customer Speciality', BI_PL_Active__c = true);
		insert br1;
		BI_PL_Business_rule__c br2 = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Channel__c = 'rep_detail_only',BI_PL_Position__c = position.id,BI_PL_Product__c = products.get(0).id,
																BI_PL_Secondary_product__c = products.get(1).id, BI_PL_Type__c = 'Segmentation', BI_PL_Active__c = true);
		insert br2;
		BI_PL_Business_rule__c br21 = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Channel__c = 'rep_detail_only',BI_PL_Position__c = position.id,BI_PL_Product__c = products.get(0).id,
																BI_PL_Secondary_product__c = null, BI_PL_Type__c = 'Segmentation', BI_PL_Active__c = true);
		insert br21;
		BI_PL_Business_rule__c br3 = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Channel__c = 'rep_detail_only',BI_PL_Position__c = position.id,BI_PL_Product__c = products.get(0).id,
																BI_PL_Secondary_product__c = products.get(1).id, BI_PL_Type__c = 'Account Share percentage', BI_PL_Active__c = true);
		insert br3;
		BI_PL_Business_rule__c br4 = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Channel__c = 'rep_detail_only',BI_PL_Position__c = position.id,BI_PL_Product__c = products.get(0).id,
																BI_PL_Secondary_product__c = products.get(1).id, BI_PL_Type__c = 'Forbidden Product', BI_PL_Active__c = true);
		insert br4;
		BI_PL_Business_rule__c br41 = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Channel__c = 'rep_detail_only',BI_PL_Position__c = position.id,BI_PL_Product__c = products.get(0).id,
																BI_PL_Secondary_product__c = null, BI_PL_Type__c = 'Forbidden Product', BI_PL_Active__c = true);
		insert br41;
		BI_PL_Business_rule__c br5 = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Channel__c = 'rep_detail_only',BI_PL_Position__c = position.id,BI_PL_Product__c = products.get(0).id,
																BI_PL_Secondary_product__c = products.get(1).id, BI_PL_Type__c = 'Must Add Product', BI_PL_Active__c = true);
		insert br5;
		BI_PL_Business_rule__c br6 = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Channel__c = 'rep_detail_only',BI_PL_Position__c = position.id,BI_PL_Product__c = products.get(0).id,
																BI_PL_Secondary_product__c = products.get(1).id, BI_PL_Type__c = 'Field Force products', BI_PL_Active__c = true);
		insert br6;
		BI_PL_Business_rule__c br7 = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Channel__c = 'rep_detail_only',BI_PL_Position__c = position.id,BI_PL_Product__c = products.get(0).id,
																BI_PL_Secondary_product__c = null, BI_PL_Type__c = 'Primary and Secondary', BI_PL_Active__c = true);
		insert br7;

		br7.BI_PL_Secondary_product__c = products.get(1).id;
		update br7;
		
		Test.stopTest();
	}
}