public class BI_PL_BusinessRuleUtility {
	// Types
	public static final String CUSTOMER_SPECIALTY = 'Customer Speciality';
	public static final String SEGMENTATION = 'Segmentation';
	public static final String ACCOUNT_DROP_PERCENTAGE = 'Account Drop percentage';
	public static final String FORBIDDEN_PRODUCT = 'Forbidden Product';
	public static final String PRIMARY_AND_SECONDARY = 'Primary and Secondary';
	public static final String ACCOUNT_TRANSFER_PERCENTAGE = 'Account Transfer percentage';
	public static final String ACCOUNT_SHARE_PERCENTAGE = 'Account Share percentage';
	public static final String THRESHOLD = 'Threshold';
	public static final String MUST_ADD_PRODUCT = 'Must Add Product';
	public static final String FIELD_FORCE_PRODUCTS = 'Field Force products';
	public static final String TGT_FLAG = 'Tgt Flag';
	public static final String WORKLOAD = 'Workload'; //GLOS-1026 - Workload
	public static final String FREQUENCY_VARIATION = 'Frequency Variation'; //GLOS-1025 - Frequency Variation


	/**
	 *	Returns all the business rules for the Country that match the provided fields.
	 *
	 *	The "positionsId" and "targetsSpecialty" parameters are used in the query with an OR operand, meaning that
	 *	the retrieved business rules won't have to have both fields filled. Made this way because no business rule needs
	 *	both fields set at the same time.
	 */
	public static List<BI_PL_Business_rule__c> getBusinessRulesForCountry(String countryCode, List<String> types, List<String> positionsId, List<String> targetsSpecialty, List<BI_PL_PreparationExt.PlanitProductPair> productPairs) {

		return getBusinessRulesForCountry(countryCode, types, positionsId, targetsSpecialty, productPairs, null, null);
	}



	public static List<BI_PL_Business_rule__c> getBusinessRulesForCountry(String countryCode, List<String> types, List<String> positionsId, 
		List<String> targetsSpecialty, List<BI_PL_PreparationExt.PlanitProductPair> productPairs, String fieldForce, String channel) {


		//all products (available in plan and all product to be added). If filtering by positions
		List<BI_PL_PreparationExt.PlanitProductPair> allProductPairs = productPairs != null ? productPairs : new List<BI_PL_PreparationExt.PlanitProductPair>();
		if(positionsId != null && !positionsId.isEmpty()) {
			for(BI_PL_Business_rule__c br : [SELECT Id,BI_PL_Secondary_product__c, BI_PL_Secondary_product__r.Name, BI_PL_Product__c, BI_PL_Product__r.Name 
				FROM BI_PL_Business_rule__c 
				WHERE BI_PL_Position__c IN :positionsId 
				AND BI_PL_Country_code__c = :countryCode
				AND BI_PL_Type__c = :BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY
				AND BI_PL_Active__c = true]){
				allProductPairs.add(new BI_PL_PreparationExt.PlanitProductPair(br.BI_PL_Product__r, br.BI_PL_Secondary_product__r));
			}
		} 

		String query = 'SELECT Id, BI_PL_Max_value__c, '+
				' BI_PL_Workload_value__c, '+
				' BI_PL_Field_Force__c, '+
				' BI_PL_Min_value__c, '+
				' BI_PL_Type__c, '+
				' BI_PL_Product__c, '+
				' BI_PL_Product__r.Name, '+
				' BI_PL_Secondary_product__c, '+
				' BI_PL_Secondary_product__r.Name, '+
				' BI_PL_Specialty__c, '+
				' BI_PL_Criteria__c, '+
				' BI_PL_Channel__c, '+ 
				' BI_PL_Country_code__c, '+
				' BI_PL_Position__c, '+
                ' BI_PL_Visits_per_day__c, '+
                ' BI_PL_Percentage__c, '+
                ' BI_PL_Required_capacity__c, '+
				' BI_PL_Position__r.Name FROM BI_PL_Business_rule__c WHERE BI_PL_Active__c = true';

		if (String.isNotBlank(countryCode))
			query += ' AND BI_PL_Country_code__c = \'' + countryCode + '\'';

		if (String.isNotBlank(fieldForce))
			query += ' AND (BI_PL_Field_force__c = \'' + fieldForce + '\' OR BI_PL_Field_force__c = null)';

		if (String.isNotBlank(channel))
			query += ' AND (BI_PL_Channel__c = \'' + channel + '\' OR BI_PL_Channel__c = null)';

		if (types != null && types.size() > 0)
			query += ' AND BI_PL_Type__c IN (\'' + String.join(types, '\',\'') + '\')';

		List<String> typesUseProd = typesUsingProducts(types);
		if (!typesUseProd.isEmpty() && allProductPairs != null && allProductPairs.size() > 0 ) {
			String tps = String.join(typesUseProd, '\',\'');
			query += ' AND (((';
			for (Integer i = 0; i < allProductPairs.size(); i++) {
				BI_PL_PreparationExt.PlanitProductPair productPair = allProductPairs.get(i);
				query += ' (BI_PL_Product__c = \''+productPair.primary.Id+'\'';
				if (productPair.hasSecondary)
					query += ' AND BI_PL_Secondary_product__c = \''+productPair.secondary.Id+'\'';

				query += ' )';

				if (i < allProductPairs.size() - 1)
					query += ' OR ';
			}
			query += ') AND BI_PL_Type__c IN (\'' +tps+'\' )) OR  BI_PL_Product__c = null)';
		}

		//if ((positionsId != null && positionsId.size() > 0) && (targetsSpecialty != null && targetsSpecialty.size() > 0)) {
		//	query += ' AND (BI_PL_Position__c IN (\'' + String.join(positionsId, '\',\'') + '\') OR BI_PL_Position__c = null OR BI_PL_Specialty__c IN (\'' + String.join(targetsSpecialty, '\',\'') + '\') OR BI_PL_Specialty__c = null)';
		//} else {
			if (positionsId != null && positionsId.size() > 0)
				query += ' AND (BI_PL_Position__c IN (\'' + String.join(positionsId, '\',\'') + '\') OR BI_PL_Position__c = null)';

			if (targetsSpecialty != null && targetsSpecialty.size() > 0)
				query += ' AND (BI_PL_Specialty__c IN (\'' + String.join(targetsSpecialty, '\',\'') + '\') OR BI_PL_Specialty__c = null)';
		//}

		//throw new BI_PL_Exception(query);
		System.debug('getBusinessRulesForCountry Query: '+query);

		return Database.query(query);
	}

	public static List<String> typesUsingProducts(List<String> selectedTypes) {
		Set<String> selectedTypesSet = new Set<String>(selectedTypes);
		List<String> ts = new List<String>();
		if(selectedTypesSet.contains(BI_PL_BusinessRuleUtility.CUSTOMER_SPECIALTY)) ts.add(BI_PL_BusinessRuleUtility.CUSTOMER_SPECIALTY);
		if(selectedTypesSet.contains(BI_PL_BusinessRuleUtility.FORBIDDEN_PRODUCT)) ts.add(BI_PL_BusinessRuleUtility.FORBIDDEN_PRODUCT);
		if(selectedTypesSet.contains(BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY)) ts.add(BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY);
		if(selectedTypesSet.contains(BI_PL_BusinessRuleUtility.SEGMENTATION)) ts.add(BI_PL_BusinessRuleUtility.SEGMENTATION);
		return ts;
	}

}