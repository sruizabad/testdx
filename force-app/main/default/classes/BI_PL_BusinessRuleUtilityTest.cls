@isTest
private class BI_PL_BusinessRuleUtilityTest
{
	@testSetup static void Setup(){

		User thisUser = [ select Id,Country_Code_BI__c from User where Id = :UserInfo.getUserId() ];
		String countryCode = thisUser.Country_code_BI__c;

		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		BI_PL_Position__c position = new BI_PL_Position__c(Name = 'D1234',BI_PL_Country_code__c = countryCode, BI_PL_Field_force__c = 'diab', BI_PL_External_id__c = 'EXID' );
		insert position;
		List<Product_vod__c> listProd = BI_PL_TestDataFactory.createTestProduct(4,countryCode);

		BI_PL_TestDataFactory.insertBusinessRules(listProd, countryCode, position);


	}



	@isTest
	static void test()
	{
		User thisUser = [ select Id,Country_Code_BI__c from User where Id = :UserInfo.getUserId() ];
		String countryCode = thisUser.Country_code_BI__c;

		BI_PL_Position__c position =[select id, Name from BI_PL_Position__c limit 1];
		List<String> listPos = new List<String>();
		listPos.add(position.id);
		List<String> specialtys = new List<String> {'Allergy'};
		List<Product_vod__c> prodlist = [Select id, Name from Product_vod__c ];
		List<String> types = new List<String>{'Customer Speciality','Segmentation','Threshold','Account Drop percentage','Primary and Secondary','Forbidden Product','Account Transfer percentage','Account Share percentage'};
		BI_PL_PreparationExt.PlanitProductPair productPair = new BI_PL_PreparationExt.PlanitProductPair(prodlist.get(0),prodlist.get(1));
		List<BI_PL_PreparationExt.PlanitProductPair> listProductPair = new List<BI_PL_PreparationExt.PlanitProductPair>();
		listProductPair.add(productPair);

		BI_PL_BusinessRuleUtility.getBusinessRulesForCountry(countryCode,types,listPos,specialtys,listProductPair);

		productPair = new BI_PL_PreparationExt.PlanitProductPair(prodlist.get(2),prodlist.get(3));

		listProductPair.add(productPair);

		List<String> emptyList = new List<String>();


		BI_PL_BusinessRuleUtility.getBusinessRulesForCountry(countryCode,types,listPos,emptyList,listProductPair);
		BI_PL_BusinessRuleUtility.getBusinessRulesForCountry(countryCode,types,emptyList,specialtys,listProductPair);
	}
}