/**
 * Controller for BI_PL_BusinessRules page.
 *
 *	19/09/2017
 *	- BI_PL_Specialty__c from BusinessRule now is a picklist instead of a Lookup.
 * Date: 22/11/2016
 * @author Omega CRM Consulting
 * @version 1.0
*/
public with sharing class BI_PL_BusinessRulesCtrl {
	
	/*-----------------------------------*/
	/*         PUBLIC VARIABLES          */
	/*-----------------------------------*/
	
	public String user_country_code {get; set;}												//Retrieved country code from current user.
	public list<String> lSegmentNames {get; set;}											//list of segment names values from picklist.
	public map<String,map<String,map<String,BI_PL_Business_rule__c>>> mSegmentationBR {get; set;}		//map of maps to store all segmentation rules.
	public map<String,list<String>> mSpecialtyBR {get; set;}								//map of list to store all specialty rules.
	public Boolean hasMin {get; set;}														//boolean to indicate if any segmentation rule has defined a min value.
	public Boolean hasMax {get; set;}														//boolean to indicate if any segmentation rule has defined a min value.
	public Boolean hasSegmentationBR{get; set;}
	public Boolean hasSpecialtyBR{get; set;}
	public String selectedChannel {get;set;}												//Current channel selected
	public List<SelectOption> channelOptions {
		get {
			if(channelOptions == null) {
				channelOptions = new List<SelectOption>();
		        for (Schema.PicklistEntry entry : BI_PL_Channel_detail_preparation__c.BI_PL_Channel__c.getDescribe().getPicklistValues()){
		            channelOptions.add(new SelectOption(entry.getValue(), entry.getLabel()));
		        }
			}
			return channelOptions;
		}
		set;
	}
	
	/*-----------------------------------*/
	/*            CONSTRUCTOR            */
	/*-----------------------------------*/
	
    public BI_PL_BusinessRulesCtrl(){
    	//default channel TODO: move from here??
    	selectedChannel = 'rep_detail_only';
    	setUserCountryCode();
    	setBusinessRules();
    }
    
    
    /*-----------------------------------*/
	/*          PRIVATE METHODS          */
	/*-----------------------------------*/
	
    /**
	 * Method to get the user country code.
	 *
	 * @return void
	*/
    private void setUserCountryCode(){
    	//Retrieve the current user record.
    	User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
    	//Storing user country code value.
    	user_country_code = currentUser.Country_Code_BI__c;
    	system.debug('###user CC: ' + user_country_code);
    }
    
    /**
	 * Main method to set all business rules.
	 *
	 * @return void
	*/
    
    public void setBusinessRules(){
    	//Calling all setters methods. 
    	setSegmentationBR();
    	setSpecialtyBR();
    }
    
    /**
	 * This method extract all values from Segment picklist.
	 *
	 * @return void
	*/
    
    private void setSegmentNames(){
    	//Initialization variables
    	lSegmentNames = new list<String>();
    	
    	//Retrieving picklist values.
    	list<Schema.PicklistEntry> picklistEntries = SAP_Detail_Preparation_BI__c.Segment_BI__c.getDescribe().getPicklistValues();
    	//Storing values.
    	for(Schema.PicklistEntry pE: picklistEntries){
    		lSegmentNames.add(pE.getValue());
    	}
    	system.debug('#### LIST SEGMENT NAMES: ' + lSegmentNames);
    }

    
    /**
	 * Method to retrieve, process and set all segmentation rules by country code.
	 *
	 * @return void
	*/
    private void setSegmentationBR(){
    	//Initialization variables.
    	hasMin = false;
    	hasMax = false;
    	mSegmentationBR = new map<String,map<String,map<String,BI_PL_Business_rule__c>>>();
    	
    	//Retrieving all segment names values.
    	setSegmentNames();
    	//Retrieving all segmentation business rules using a general method getBusinessRulesByType.
    	//map<String,list<BI_PL_Business_rule__c>> mSegmentationRulesByProduct = BI_PL_PreparationServiceUtility.getBusinessRulesByType(user_country_code,
    	//																								'Segmentation',
    	//																								'BI_PL_Product__r.Name',
    	//																								selectedChannel);


		map<String,map<String,list<BI_PL_Business_rule__c>>> mSegmentationRules = getSegmentationBusinessRules(user_country_code);

    	hasSegmentationBR = (mSegmentationRules.size()>0);
		system.debug('#### has Segmentation BR: ' + hasSegmentationBR);
    	//processing rules and making structures.

		for(String ffName: mSegmentationRules.keySet()){
			system.debug('#### ffName: ' + mSegmentationRules.get(ffName));
			map<String,map<String,BI_PL_Business_rule__c>> mapAux = new map<String,map<String,BI_PL_Business_rule__c>>();
			for(String prodName: mSegmentationRules.get(ffName).keySet()){
				Boolean contains = mapAux.containsKey(prodName);
				map<String,BI_PL_Business_rule__c> mBRbyCriteria = (contains) ? mapAux.get(prodName) : new map<String,BI_PL_Business_rule__c>();
				for(String segmentName: lSegmentNames){
					mBRbyCriteria.put(segmentName,new BI_PL_Business_rule__c());
				} 
				for(BI_PL_Business_rule__c BR:  mSegmentationRules.get(ffName).get(prodName)){
					hasMin = (!hasMin)? BR.BI_PL_Min_value__c != null : hasMin;
					hasMax = (!hasMax)? BR.BI_PL_Max_value__c != null : hasMax;
					mBRbyCriteria.put(BR.BI_PL_Criteria__c,BR);
				}
				if(!contains) mapAux.put(prodName,mBRbyCriteria);
			}
			mSegmentationBR.put(ffName,mapAux);
		}
    }
    
    /**
	 * Method to retrieve, process and set all specialty rules by country code.
	 *
	 * @return void
	*/
	private void setSpecialtyBR(){
		mSpecialtyBR = new map<String,list<String>>();
		map<String,list<BI_PL_Business_rule__c>> mSpecialtyRulesByProduct = BI_PL_PreparationServiceUtility.getBusinessRulesByType(user_country_code,
																									'Customer Speciality',
																									'BI_PL_Product__r.Name',
																									selectedChannel);	
		hasSpecialtyBR = (mSpecialtyRulesByProduct.size()>0);
		system.debug('#### has Specialty BR: ' + hasSpecialtyBR);
		system.debug('#### BR retrieved: ' + mSpecialtyRulesByProduct);														
		for(String prodName: mSpecialtyRulesByProduct.keySet()){
			set<String> sBRbyProduct = new set<String>();
			for(BI_PL_Business_rule__c BR: mSpecialtyRulesByProduct.get(prodName)){
				if(BR.BI_PL_Specialty__c!= null) sBRbyProduct.add(BR.BI_PL_Specialty__c);
			}
			mSpecialtyBR.put(prodName,new list<String>(sBRbyProduct));
		}
		system.debug('#### MAP SPECIALITIES RULES: ' + mSpecialtyBR);
	}


	private map<String,map<String,list<BI_PL_Business_rule__c>>> getSegmentationBusinessRules(String countryCode){
		list<BI_PL_Business_rule__c> lBusinessRules = new list<BI_PL_Business_rule__c>();
		try{

			lBusinessRules = [SELECT Id, BI_PL_Max_value__c, 
							BI_PL_Field_Force__c,
							BI_PL_Min_value__c, 
							BI_PL_Type__c, 
							BI_PL_Product__c, 
							BI_PL_Product__r.Name,
							//BI_PL_Specialty__c,
							BI_PL_Criteria__c
							FROM BI_PL_Business_rule__c 
							WHERE BI_PL_Active__c = true 
							AND BI_PL_Country_code__c =: countryCode
							AND BI_PL_Channel__c =: selectedChannel
							AND BI_PL_Type__c = 'Segmentation'];			
			
		}catch(Exception e){
			system.debug('Error while retrieving business rules: ' + e.getMessage());
		}
	   
		map<String,map<String,list<BI_PL_Business_rule__c>>> fieldForceMap = new map<String,map<String,list<BI_PL_Business_rule__c>>>();
		
		
		for (BI_PL_Business_rule__c BR: lBusinessRules){
			map<String,list<BI_PL_Business_rule__c>> rulesMap = new map<String,list<BI_PL_Business_rule__c>>();
			list<BI_PL_Business_rule__c> lBRDetail = new list<BI_PL_Business_rule__c>();

			System.debug('####fieldforce: ' + BR.BI_PL_Field_Force__c);

			if (fieldForceMap.containsKey(BR.BI_PL_Field_Force__c)) rulesMap = fieldForceMap.get(BR.BI_PL_Field_Force__c);
			
			System.debug('####product: ' + BR.BI_PL_Product__r.Name);
			Boolean contains = rulesMap.containsKey(BR.BI_PL_Product__r.Name);			
			if(contains) lBRDetail = rulesMap.get(BR.BI_PL_Product__r.Name);			
			lBRDetail.add(BR);
			if(!contains) rulesMap.put(BR.BI_PL_Product__r.Name,lBRDetail);
			
			fieldForceMap.put(BR.BI_PL_Field_Force__c, rulesMap);
		}
		System.debug('####detailsRulesMap: ' + fieldForceMap);
		
		return fieldForceMap;
	}

	/**
	* This method returns the value of any object field specified with a String.
	*/
	public static Object getFieldValue(SObject o,String field){
		
		system.debug('%%% field: ' + field);
	
		if(o == null){return null;}
		if(field.contains('.')){
			String nextField = field.substringAfter('.');
			String relation = field.substringBefore('.');
			return getFieldValue((SObject)o.getSObject(relation),nextField);
		}else{
			return o.get(field);
		}	
	}	

	/**
	 * This method creates new business rules of customer specialty
	 */
	@RemoteAction
	public static void createCustomerSpecialtyBR(String channel, String countryCode, String product, String specialties){
		System.debug('@@@@ specialties: ' + specialties);
		List<String> specialtiesList = (List<String>)JSON.deserialize(specialties, List<String>.class);
		Map<String, Boolean> result = new Map<String, Boolean>();
		String recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Customer Specialty' LIMIT 1].Id;
		String extProd = [SELECT Id, External_id_vod__c FROM Product_vod__c WHERE Id = :product LIMIT 1].External_id_vod__c;

		List<BI_PL_Business_rule__c> businessRules = new List<BI_PL_Business_rule__c>();

		for(String specialty: specialtiesList){
			if(specialty!=''){
				String externalID = countryCode + '_Customer Speciality_' + specialty + '_' + extProd +'_BR';
				businessRules.add(
					new BI_PL_Business_rule__c(
						BI_PL_External_id__c = externalID,
						RecordTypeId = recordTypeId,
						BI_PL_Active__c = true,
						BI_PL_Channel__c = channel,
						BI_PL_Country_code__c = countryCode,
						BI_PL_Product__c = product,
						BI_PL_Specialty__c = specialty,
						BI_PL_Type__c = 'Customer Speciality')
				);
				System.debug('ID BusinessRule :: ' + externalID);
			}
		}
		Schema.SObjectField field = BI_PL_Business_rule__c.Fields.BI_PL_External_id__c;
		Database.UpsertResult[] srList = Database.upsert(businessRules, field,false);
	}
}