@isTest
private class BI_PL_BusinessRulesCtrl_Test {

	@testSetup static void setup(){
		BI_PL_TestDataFactory.createCustomSettings();

		Product_vod__c product1 = new Product_vod__c(
        	Name = 'TestProductBR',
        	RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(),
        	Product_Type_vod__c = 'Detail',
        	External_ID_vod__c = 'TestProduct1_ExtIdBusRule'
        );

        Product_vod__c product2 = new Product_vod__c(
        	Name = 'TestProductBR2',
        	RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(),
        	Product_Type_vod__c = 'Detail',
        	External_ID_vod__c = 'Product1_ExtIdBusRule2'
        );

        insert product1;
        insert product2;
        List<Product_vod__c> listProd = [SELECT id FROM Product_vod__c];

        Id currentUserId = UserInfo.getUserId();

        String userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = : currentUserId LIMIT 1].Country_Code_BI__c;

		BI_PL_Business_rule__c businessRule = new BI_PL_Business_rule__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Active__c = true, BI_PL_Product__c = product1.Id, BI_PL_Type__c = 'Segmentation',
																		BI_PL_Channel__c = 'rep_detail_only', BI_PL_External_Id__c= 'TestBusinessRuleExt_ID_1');
		insert businessRule;

		List<Account> listAcc = BI_PL_TestDataFactory.createTestAccounts(1,userCountryCode);

		Customer_Attribute_BI__c specialty = new Customer_Attribute_BI__c(Name = 'Cardio', External_Id_BI__c = 'CardioExId', Type_BI__c = 'ACCT_BI_Specialty');

		insert specialty;
		BI_PL_Position__c position = new BI_PL_Position__c(BI_PL_Country_code__c = 'US', BI_PL_External_Id__c = 'PositionExBRTest', BI_PL_Field_force__c = 'Cardiology', Name = 'positionBRtest');
		insert position;

		BI_PL_TestDataFactory.insertBusinessRules(listProd,'US',position);
	}
	
	@isTest static void test_method_one() {
	
		BI_PL_BusinessRulesCtrl controller = new BI_PL_BusinessRulesCtrl();
		List<SelectOption> options = controller.channelOptions;

	}
	
	@isTest static void test_method_two() {
		// Implement test code
		try{
			BI_PL_BusinessRulesCtrl.getFieldValue(null,'test');
		}catch(Exception e){}

		Product_vod__c product1 = [SELECT id from Product_vod__c LIMIT 1];

		BI_PL_BusinessRulesCtrl.getFieldValue(product1,'id');
		BI_PL_Business_rule__c br = [SELECT id, BI_PL_Product__r.Name FROM BI_PL_Business_rule__c WHERE BI_PL_Type__c = 'Primary and Secondary' LIMIT 1];
		BI_PL_BusinessRulesCtrl.getFieldValue(br,'BI_PL_Product__r.Name');
	}

	@isTest static void testCreateSpecialtyBR(){
		
        String userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
		List<String> specialtiesList = new List<String>();
		specialtiesList.add('Speciality1');
		specialtiesList.add('Speciality2');
		String serializeList = JSON.serialize(specialtiesList);


		BI_PL_BusinessRulesCtrl.createCustomerSpecialtyBR('rep_detail_only', userCountryCode, [SELECT Id FROM Product_vod__c LIMIT 1].Id, serializeList);

		List<BI_PL_Business_rule__c> brList = [SELECT Id FROM BI_PL_Business_rule__c WHERE BI_PL_Type__c = 'Customer Speciality'];
		System.assertEquals(specialtiesList.size(),brList.size());
	}
}