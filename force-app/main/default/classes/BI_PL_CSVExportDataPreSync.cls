/**
 *  05/08/2018
 *  Batch to export data as a previous step before sync to veeva
 *  @author Ferran Garcia Omega CRM BCN
 */
 public class BI_PL_CSVExportDataPreSync implements Database.Batchable<BI_PL_WrapObjectCSVPreSync>, Database.Stateful{
	
	private static final String SFILENAME = 'PreExportVeeva_';
	private String cycle;
	private Attachment att;
	public List<BI_PL_WrapObjectCSVPreSync> wrapdetails = new list<BI_PL_WrapObjectCSVPreSync>();

    //List<Attachment> lstAttach;
    String attId;
    Set<String> setAllAtt;
	String query;
	
	public BI_PL_CSVExportDataPreSync(String cycle, List<BI_PL_WrapObjectCSVPreSync> wrap){
		System.debug('BI_PL_CSVExportDataPreSync Cycle :: ' + cycle);
        this.cycle = cycle;
		//this.extension = '.csv';
		this.wrapdetails = wrap;
	}
	
	public Iterable<BI_PL_WrapObjectCSVPreSync> start(Database.BatchableContext BC) {
		//setAllAtt = new set<String>();
        Attachment att = new Attachment(
                                Name = SFILENAME + this.cycle + '_' + Datetime.now().format('yyyyMMdd') + '.csv', 
                                ParentId = this.cycle, 
                                Body = Blob.valueOf(getDocHeaderRow()+'\r\n'));
        insert att;
        this.attId = att.Id;
        //setAllAtt.add(att.Id);
		return wrapdetails;
	}

   	public void execute(Database.BatchableContext BC, List<BI_PL_WrapObjectCSVPreSync> wrapdetails) {
        String row, newRow = '';
        List<Attachment> lstAttach = new List<Attachment>();
        Boolean bHeapSize = false;

        //Load attachment where export file is saving
        Attachment workingDoc = [SELECT Id, Body FROM Attachment WHERE Id = :attId LIMIT 1];
        lstAttach.add(workingDoc);

        for(BI_PL_WrapObjectCSVPreSync lin2 : wrapdetails){
	        newRow += '"' + lin2.SAP_External_ID2 + '",' 
	                + '"' + lin2.Start_Date + '",' 
	                + '"' + lin2.End_Date + '",' 
	                + '"' + lin2.Territory + '",'
	                + '"' + lin2.Status + '",' 
	                + '"' + lin2.Country_Code_BI + '",' 
	                + '"' + lin2.Target_External_ID2 + '",'
	                + '"' + lin2.Cycle_Plan_Account + '",' 
	                + '"' + lin2.Planned_Calls + '",' 
	                + '"' + lin2.Detail_External_ID + '",'
	                + '"' + lin2.Product + '",' 
	                + '"' + lin2.Planned_Details + '",' 
	                + '"' + lin2.PM_Segmentation_BI + '",'
	                + '"' + lin2.PM_Potential_BI + '",'
	                + '"' + lin2.PM_Intimacy_BI + '",' 
	                + '"' + lin2.Primary_Goal_BI + '",' 
	                + '"' + lin2.Other_Goal_BI + '",' 
	                + '"' + lin2.PM_Strategic_Segment_BI + '"\r\n';

	        if(((Limits.getHeapSize() + 2500000) > Limits.getLimitHeapSize() && !bHeapSize) || Test.isRunningTest()){

                bHeapSize = true;
                row = workingDoc.Body.toString() + newRow;
                workingDoc.Body = Blob.valueOf(row);
                newRow = '';

                //Create new Attachment
                workingDoc = new Attachment(Name = SFILENAME + this.cycle + '_' + Datetime.now().format('yyyyMMdd') + '.csv', 
				                             ParentId = this.cycle, 
				                             Body = Blob.valueOf(getDocHeaderRow()+'\r\n'));
                lstAttach.add(workingDoc);
            }
        }
        
        if (String.isNotBlank(newRow)){
            row = workingDoc.Body.toString() + newRow;
            //System.debug('Heap Size BLOB :: ' + Limits.getHeapSize() );
            workingDoc.Body = Blob.valueOf(row);
        }

        upsert lstAttach;

        this.attId = workingDoc.Id;
	}
	
	public void finish(Database.BatchableContext BC) {
		
	}

	private String getDocHeaderRow(){
        return '"""SAP_External_ID2","Start_Date","End_Date","Territory","Status","Country_Code_BI","Target_External_ID2","Cycle_Plan_Account","Planned_Calls","Detail_External_ID","Product","Planned_Details","PM_Segmentation_BI","PM_Potential_BI","PM_Intimacy_BI","Primary_Goal_BI","Other_Goal_BI","PM_Strategic_Segment_BI"';
    }
}