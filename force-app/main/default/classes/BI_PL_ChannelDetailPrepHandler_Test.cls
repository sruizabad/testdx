@isTest
private class BI_PL_ChannelDetailPrepHandler_Test {

	
	
	@isTest public static void testWithBr() {

		BI_PL_TestDataFactory.createFieldForces();
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		List<Product_vod__c> listProducts = BI_PL_TestDataFactory.createTestProduct(2,'US');
		List<Account> listAccounts = BI_PL_TestDataFactory.createTestAccounts(2,'US');




		BI_PL_TestDataFactory.createCycleStructure('US');

		BI_PL_Position__c position = [SELECT ID, NAme, BI_PL_Field_force__c FROM BI_PL_Position__c WHERE NAME = 'Test5'];

		BI_PL_TestDataFactory.insertBusinessRules(listProducts,'US',position);

		List<BI_PL_Position_cycle__c> positionCycleRep = [SELECT id, BI_PL_External_id__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Position__r.Name IN ('Test4','Test5','Pos4','Pos5') AND BI_PL_Position__r.BI_PL_Country_code__c = 'US'];

		

		List<BI_PL_Preparation__c> preparations = BI_PL_TestDataFactory.createPreparations('US',positionCycleRep,listAccounts,listProducts);


	


	}


	@isTest public static void testWithoutBR() {

		BI_PL_TestDataFactory.createFieldForces();
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		List<Product_vod__c> listProducts = BI_PL_TestDataFactory.createTestProduct(2,'US');
		List<Account> listAccounts = BI_PL_TestDataFactory.createTestAccounts(2,'US');

		




		BI_PL_TestDataFactory.createCycleStructure('US');

		BI_PL_Position__c position = [SELECT ID, NAme, BI_PL_Field_force__c FROM BI_PL_Position__c WHERE NAME = 'Test5'];

	
		List<BI_PL_Position_cycle__c> positionCycleRep = [SELECT id, BI_PL_External_id__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Position__r.Name IN ('Test4','Test5','Pos4','Pos5') AND BI_PL_Position__r.BI_PL_Country_code__c = 'US'];

		

		List<BI_PL_Preparation__c> preparations = BI_PL_TestDataFactory.createPreparations('US',positionCycleRep,listAccounts,listProducts);




	}

	
	
}