public without sharing class BI_PL_ChannelDetailPreparationHandler
	implements BI_PL_TriggerInterface {


	Map<Id, Boolean> addedManuallyByTargetId = new Map<Id, Boolean>();

	Map<Id, String> countryCodeByTarget = new Map<Id, String>();
	Map<String, Boolean> dontFillTGTByCountryCode = new Map<String, Boolean>();
	Map<String, List<String>> fieldforceTgTByCountry = new Map<String, List<String>>();
	Map<Id, String> fieldForceByTarget = new Map<Id, String>();
	Map<Id, Boolean> noSeListByTarget = new Map<Id, Boolean>();
	Map<Id, Boolean> nextBestByTarget = new Map<Id, Boolean>();
	Map<String,Map<String,List<String>>> productsForFF = new Map<String,Map<String,List<String>>>();
	Map<String,BI_PL_Detail_preparation__c> detailsToInsert = new Map<String,BI_PL_Detail_preparation__c>();


	public BI_PL_ChannelDetailPreparationHandler() {
	}

	public void bulkAfter() {

		
		Set<Id> targetIds = new Set<Id>();

		if (Trigger.isInsert || Trigger.isUpdate) {
			for (sObject record : Trigger.new)
				targetIds.add(((BI_PL_Channel_detail_preparation__c)record).BI_PL_Target__c);

			for (BI_PL_Target_preparation__c target : [SELECT Id, BI_PL_Added_manually__c, BI_PL_Header__r.BI_PL_Country_code__c, BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c, BI_PL_No_see_list__c, BI_PL_Next_Best_account__c FROM BI_PL_Target_preparation__c WHERE Id IN:targetIds]) {				
				countryCodeByTarget.put(target.Id, target.BI_PL_Header__r.BI_PL_Country_code__c);
				fieldForceByTarget.put(target.Id, target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c);
			}

			productsForFF = getProductsBussinesRules(countryCodeByTarget.values(),fieldForceByTarget.values());
			
		}



	}

	/**
	 * bulkBefore
	 *
	 * This method is called prior to execution of a BEFORE trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkBefore() {
		Set<Id> targetIds = new Set<Id>();

		if (Trigger.isInsert || Trigger.isUpdate) {
			for (sObject record : Trigger.new)
				targetIds.add(((BI_PL_Channel_detail_preparation__c)record).BI_PL_Target__c);

			for (BI_PL_Target_preparation__c target : [SELECT Id, BI_PL_Added_manually__c, BI_PL_Header__r.BI_PL_Country_code__c, BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c, BI_PL_No_see_list__c, BI_PL_Next_Best_account__c FROM BI_PL_Target_preparation__c WHERE Id IN:targetIds]) {
				addedManuallyByTargetId.put(target.Id, target.BI_PL_Added_manually__c);
				noSeListByTarget.put(target.Id, target.BI_PL_No_see_list__c);
				nextBestByTarget.put(target.Id, target.BI_PL_Next_Best_account__c);
				countryCodeByTarget.put(target.Id, target.BI_PL_Header__r.BI_PL_Country_code__c);
				fieldForceByTarget.put(target.Id, target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c);
			}
		}

		// Get for each channel detail country if 'BI_PL_Do_not_fill_tgt_flag__c' is checked.
		for (BI_PL_Country_settings__c setting : [SELECT Id, BI_PL_Country_code__c, BI_PL_Do_not_fill_tgt_flag__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = :countryCodeByTarget.values()]) {
			dontFillTGTByCountryCode.put(setting.BI_PL_Country_code__c, setting.BI_PL_Do_not_fill_tgt_flag__c);
		}




		for (BI_PL_Business_rule__c br : [SELECT Id, BI_PL_Country_code__c, BI_PL_Field_force__c FROM BI_PL_Business_rule__c WHERE BI_PL_Country_code__c =: countryCodeByTarget.values() AND BI_PL_Type__c =: BI_PL_BusinessRuleUtility.TGT_FLAG AND BI_PL_Active__c = true]){
			if(!fieldforceTgTByCountry.containsKey(br.BI_PL_Country_code__c)){
				fieldforceTgTByCountry.put(br.BI_PL_Country_code__c, new List<String>());
				fieldforceTgTByCountry.get(br.BI_PL_Country_code__c).add(br.BI_PL_Field_force__c);
			}
			else{
				fieldforceTgTByCountry.get(br.BI_PL_Country_code__c).add(br.BI_PL_Field_force__c);
			}
		}

	}


	/*private Boolean isFillTGTDisabledForChannelDetail(BI_PL_Channel_detail_preparation__c channelDetail) {
		return dontFillTGTByCountryCode.get(countryCodeByTarget.get(channelDetail.BI_PL_Target__c));
	}*/

	private Boolean isSpecialFieldForceForTgt(BI_PL_Channel_detail_preparation__c channelDetail){
		if(fieldforceTgTByCountry.containsKey(countryCodeByTarget.get(channelDetail.BI_PL_Target__c))){
			List<String> fieldForces = fieldforceTgTByCountry.get(countryCodeByTarget.get(channelDetail.BI_PL_Target__c));
			if(fieldForces.contains(fieldForceByTarget.get(channelDetail.BI_PL_Target__c))){
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}



	}

	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */

	/**
	 * beforeInsert
	 *
	 * This method is called iteratively for each record to be inserted during a BEFORE
	 * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
	 */
	public void beforeInsert(SObject so) {
		BI_PL_Channel_detail_preparation__c ch = (BI_PL_Channel_detail_preparation__c)so;
			
		if(!isSpecialFieldForceForTgt((BI_PL_Channel_detail_preparation__c)so)){
			if (Trigger.isInsert || Trigger.isUpdate) {					
				ch.BI_PL_MSL_flag__c = setMSLFlag((BI_PL_Channel_detail_preparation__c)so);					
			}
		}
		
	}


	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
	 */
	public void beforeUpdate(SObject oldSo, SObject so) {
		BI_PL_Channel_detail_preparation__c ch = (BI_PL_Channel_detail_preparation__c)so;
		if(!isSpecialFieldForceForTgt((BI_PL_Channel_detail_preparation__c)so)){
			if (Trigger.isInsert || Trigger.isUpdate) {				
				ch.BI_PL_MSL_flag__c = setMSLFlag((BI_PL_Channel_detail_preparation__c)so);
				
			}
		}
		
	}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
	 */
	public void beforeDelete(SObject so) {
	}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
	 */
	public void afterInsert(SObject so) {
		BI_PL_Channel_detail_preparation__c ch = (BI_PL_Channel_detail_preparation__c)so;

		String cc = countryCodeByTarget.get(ch.BI_PL_Target__c);
		String fieldForce = fieldForceByTarget.get(ch.BI_PL_Target__c);
		if(productsForFF != null){
			if(productsForFF.containsKey(cc)){
				if(productsForFF.get(cc).containsKey(fieldForce)){
					for(String productId : productsForFF.get(cc).get(fieldForce)){
						BI_PL_Detail_preparation__c detail = new BI_PL_Detail_preparation__c(BI_PL_Channel_detail__c = ch.Id, BI_PL_Planned_details__c = 0, BI_PL_Adjusted_details__c = 0, BI_PL_Product__r = new Product_vod__c(External_id_vod__c = productId), BI_PL_External_id__c = ch.BI_PL_External_id__c + '_' + productId);
						detailsToInsert.put(ch.BI_PL_External_id__c + '_' + productId,detail);
					}
				}

			}
		}


	}


	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
	 */
	public void afterUpdate(SObject oldSo, SObject so) {
		BI_PL_Channel_detail_preparation__c ch = (BI_PL_Channel_detail_preparation__c)so;

		String cc = countryCodeByTarget.get(ch.BI_PL_Target__c);
		String fieldForce = fieldForceByTarget.get(ch.BI_PL_Target__c);
		if(productsForFF != null){
			if(productsForFF.containsKey(cc)){
				if(productsForFF.get(cc).containsKey(fieldForce)){
					for(String productId : productsForFF.get(cc).get(fieldForce)){
						BI_PL_Detail_preparation__c detail = new BI_PL_Detail_preparation__c(BI_PL_Channel_detail__c = ch.Id, BI_PL_Planned_details__c = 0, BI_PL_Adjusted_details__c = 0, BI_PL_Product__r = new Product_vod__c(External_id_vod__c = productId), BI_PL_External_id__c = ch.BI_PL_External_id__c + '_' + productId);
						detailsToInsert.put(ch.BI_PL_External_id__c + '_' + productId,detail);
					}
				}

			}
		}

	}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 */
	public void afterDelete(SObject so) {
	}

	/**
	 * andFinally
	 *
	 * This method is called once all records have been processed by the trigger. Use this
	 * method to accomplish any final operations such as creation or updates of other records.
	 */
	public void andFinally() {
		if(detailsToInsert.size() > 0){
			System.debug('detailsToInsert' + detailsToInsert);

			upsert detailsToInsert.values() BI_PL_External_id__c;
		}

	}


	private Boolean setMSLFlag(BI_PL_Channel_detail_preparation__c channelDetail) {
		Boolean reviewed = channelDetail.BI_PL_Reviewed__c; //aprove	
		Boolean removed = channelDetail.BI_PL_Removed__c; //drop
		Boolean rejected = channelDetail.BI_PL_Rejected__c; 
		Boolean edited = channelDetail.BI_PL_Edited__c; 
		Boolean addedManually = addedManuallyByTargetId.get(channelDetail.BI_PL_Target__c); //From GAS or No see lit or Next Best Account
		Boolean noSeeList = noSeListByTarget.get(channelDetail.BI_PL_Target__c);
		Boolean nextBestAccount = nextBestByTarget.get(channelDetail.BI_PL_Target__c);

		if((noSeeList || nextBestAccount) && !addedManually){
			return false;
		}

		if(removed && !rejected){
			return false;
		}

		if(rejected && !removed && addedManually){
			return false;
		}

		return true;
		//channelDetail.BI_PL_MSL_flag__c = BI_PL_SynchronizeSAPDataBatch.getToVeevaAddValue(reviewed, removed, rejected, edited, addedManually, 0, 0) != -1;
		
	}

	private Map<String,Map<String,List<String>>> getProductsBussinesRules(List<String> countryCodes, List<String> fieldForces){
		//CountryCode, Field Force, Products List
		Map<String,Map<String,List<String>>> mapProducts = new Map<String,Map<String,List<String>>>();

		List<BI_PL_Business_rule__c> listbr = [SELECT Id, BI_PL_Products__c, BI_PL_Country_code__c, BI_PL_Field_force__c FROM BI_PL_Business_rule__c WHERE BI_PL_Type__c =: BI_PL_BusinessRuleUtility.FIELD_FORCE_PRODUCTS AND BI_PL_Country_code__c IN: countryCodes AND BI_PL_Field_force__c IN: fieldForces AND BI_PL_Active__c = true];

		for(BI_PL_Business_rule__c br : listbr){
			if(!mapProducts.containsKey(br.BI_PL_Country_code__c)){
				mapProducts.put(br.BI_PL_Country_code__c,new Map<String,List<String>>());				
			}

			mapProducts.get(br.BI_PL_Country_code__c).put(br.BI_PL_Field_force__c,br.BI_PL_Products__c.split(';'));

/*
			Map<String, String> mp = mapProducts.get(br.BI_PL_Country_code__c);

			if(!mp.containsKey(br.BI_PL_Field_force__c)){
				mp.put(br.BI_PL_Field_force__c, br.BI_PL_Products__c.split(';'));
			}else{
				mp.put(br.BI_PL_Field_force__c, br.BI_PL_Products__c);
			}*/
		}


		if(mapProducts.size() > 0){
			
			return mapProducts;

		}else{
			return null;
		}
	}

}