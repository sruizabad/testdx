global class BI_PL_ClearPlanSegmentationBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject> {
	
	String query;
	
	global BI_PL_ClearPlanSegmentationBatch() {}

	private String joinToQuery(List<String> toJoin){
		return '\'' + String.join(tojoin, '\',\'') + '\'';
	}

	/**
	 * Generate query filters based on calss porperties (inherited from BI_PL_PlanitProcess)
	 *
	 * @return     The filters.
	 */
	private String getFilters(){
		String queryFilters = '';
        if(this.cycleIds != null && this.cycleIds.size() > 0){
            queryFilters += ' BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c IN (' + this.joinToQuery(this.cycleIds) + ')';
        }
        if(this.hierarchyNames != null && this.hierarchyNames.size() > 0){
            queryFilters += ' AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN (' + this.joinToQuery(this.hierarchyNames) + ')';
        }
        if(this.channels != null && this.channels.size() > 0){
           queryFilters += ' AND  BI_PL_Channel_detail__r.BI_PL_Channel__c IN (' + this.joinToQuery(this.channels) + ')';
        }
        System.debug(loggingLevel.Error, '*** queryFilters: ' + queryFilters);
        return queryFilters;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id,  BI_PL_Segment__c FROM BI_PL_Detail_preparation__c WHERE ' + getFilters();
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		for(BI_PL_Detail_preparation__c det : (List<BI_PL_Detail_preparation__c>) scope) {
			det.BI_PL_Segment__c = null;
		}
		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}