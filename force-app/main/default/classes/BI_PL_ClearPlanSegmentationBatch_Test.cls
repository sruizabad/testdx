@isTest
public with sharing class BI_PL_ClearPlanSegmentationBatch_Test {
	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        //BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);
    }


    @isTest
    public static void test() {

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        String[] hierarchies = new String[]{'hierarchyTest'};
        String[] channels =  new String[]{'rep_detail_only'};
        String[] cycles = new String[]{cycle.Id};
        

        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

        Test.startTest();
        BI_PL_ClearPlanSegmentationBatch b = new BI_PL_ClearPlanSegmentationBatch();
        b.init(cycles, hierarchies, channels, null);

        Database.executeBatch(b);
        Test.stopTest();

        List<BI_PL_Detail_preparation__c> details = [SELECT ID, BI_PL_Segment__c 
        	FROM BI_PL_Detail_preparation__c
        	WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c IN : cycles
        	AND  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN :hierarchies
        	AND  BI_PL_Channel_detail__r.BI_PL_Channel__c IN :channels];

        for(BI_PL_Detail_preparation__c det : details) {
        	System.assertEquals(null, det.BI_PL_Segment__c);
        }

    }
}