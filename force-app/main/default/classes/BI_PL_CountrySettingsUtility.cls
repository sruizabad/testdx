public without sharing class BI_PL_CountrySettingsUtility {
	// Columns
	public static final String PARENT_AFFILIATION = Label.BI_PL_Country_column_ParentAff;
	public static final String NTL = Label.BI_PL_Country_column_NTL;
	public static final String PRIORITY_GROUP = Label.BI_PL_Country_column_Group;
	public static final String MAX_AP = Label.BI_PL_Country_column_MaxAP;
	public static final String SUM_AP = Label.BI_PL_Country_column_SumAP;
	public static final String MARKET_INFO = Label.BI_PL_Country_column_MarketInfo;
	public static final String VEEVA_AFF = Label.BI_PL_Country_column_VeevaAff;
	public static final String HYPER_TARGET = Label.BI_PL_Country_column_HyperTarget;
	public static final String MUST_WIN = Label.BI_PL_Country_column_MustWin;

    public static final String COUNTRY_COLUMNS_SEPARATOR = ';';

    // Add new target modes
    public static final String ADD_NEW_TARGET_NONE = Label.BI_PL_Add_new_target_none;
    public static final String ADD_NEW_TARGET_LOCAL = Label.BI_PL_Add_new_target_local;
    public static final String ADD_NEW_TARGET_GLOBAL = Label.BI_PL_Add_new_target_global;


	public static BI_PL_Country_settings__c getCountrySettingsForCurrentUser() {
		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		return getCountrySettingsForCountry(currentUser.Country_Code_BI__c);
	}

	@RemoteAction
	public static BI_PL_Country_settings__c saveCountrySetting(BI_PL_Country_settings__c setting) {
		update setting;
		return setting;
	}

	public static BI_PL_Country_settings__c getCountrySettingsForCountry(String countryCode) {
		return [SELECT Id, 
			BI_PL_Add_target_mode__c,
			BI_PL_Admin_user_name__c,
			BI_PL_Aff_to_detail_output_affs__c,
			BI_PL_Allow_all_products_default__c,
			BI_PL_Allow_dropping_targets__c,
			BI_PL_AMA_Products__c,
			BI_PL_AMA_view_fieldforces__c,
			BI_PL_Auto_save_interval__c,
			BI_PL_Auto_save_targets__c,
			BI_PL_Channel__c,
			BI_PL_Clone__c,
			BI_PL_Columns__c,
			BI_PL_Country_code__c,
			BI_PL_Custom_name_column__c,
			BI_PL_Dashboard_Information__c,
			BI_PL_Default_owner_user_name__c,
			BI_PL_Default_time_zone__c,
			BI_PL_Delete_error_logs_night_process__c,
			BI_PL_Do_not_fill_tgt_flag__c,
			BI_PL_Edit_preparation_by_default__c,
			BI_PL_Execute_night_process__c,
			BI_PL_Hierarchy_conf_notification__c,
			BI_PL_Import_from_BITMAN__c,
			BI_PL_Import_from_IMMPACT__c,
			BI_PL_Individual_Approval__c,
			BI_PL_Levels__c,
			BI_PL_MSL_view_fieldforces__c,
			BI_PL_Multichannel__c,
			BI_PL_Must_win_hyper_target_fieldforces__c,
			BI_PL_No_see_list__c,
			BI_PL_Number_of_pull_through__c,
			BI_PL_Planit_pilot__c,
			BI_PL_Preparation_collapsed_sections__c,
			BI_PL_Search_Accounts_by_FF__c,
			BI_PL_Show_display_new_product__c,
			BI_PL_Show_Hierarchy_Tree__c,
			BI_PL_Show_veeva_affiliation__c,
			BI_PL_Show_Workload__c,
			BI_PL_Specialty_field__c,
			BI_PL_Split_cycle__c,
			BI_PL_Synchronize_autoname__c,
			BI_PL_Transfer_and_share_fieldforces__c,
			BI_PL_Transfer_share_notification__c,
			BI_PL_Use_HCO__c,
			BI_PL_Use_secondary_product__c,
			BI_PL_Use_sum__c,
			BI_PL_Segment__c,
			BI_PL_Use_workload_and_capacity__c,
			BI_PL_Use_specialty_business_rule__c,
			BI_PL_Affiliation_to_detail_night__c,
			BI_PL_Veeva_interactions_night__c,
			BI_PL_Users_bitman_night__c
        FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = :countryCode];
	}

	public static Boolean isNotifyTransferAndShareEnabled() {
		return BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser().BI_PL_Transfer_share_notification__c;
	}
	public static Boolean isNotifyHierarchyEnabled() {
		return BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser().BI_PL_Hierarchy_conf_notification__c;
	}
	public static Boolean hasCountryColumn(String column){
		String columns = BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser().BI_PL_Columns__c;
		if(String.isBlank(columns))
			return false;
		
		return new Set<String>(columns.split(COUNTRY_COLUMNS_SEPARATOR)).contains(column);
	}
	public static Boolean isTransferAndShareEnabledForFieldforce(String fieldforce){
		String fieldforces = BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser().BI_PL_Transfer_and_share_fieldforces__c;
		if(String.isBlank(fieldforces))
			return false;
		
		return new Set<String>(fieldforces.split(COUNTRY_COLUMNS_SEPARATOR)).contains(fieldforce);
	}
}