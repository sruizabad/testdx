@isTest
private class BI_PL_CountrySettingsUtility_Test {

	@isTest static void test_method_one (){

		BI_PL_TestDataFactory.createCustomSettings();
		createCountrySetting();

		BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser();
		BI_PL_CountrySettingsUtility.isNotifyTransferAndShareEnabled();
		BI_PL_CountrySettingsUtility.isNotifyHierarchyEnabled();
		BI_PL_CountrySettingsUtility.hasCountryColumn('MAX_AP');
		BI_PL_CountrySettingsUtility.isTransferAndShareEnabledForFieldforce('fieldforce');
		
	}	
	@isTest static void test_method_two (){
		//test countrysetting where BI_PL_Columns__c & BI_PL_Transfer_and_share_fieldforces__c is blank
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		
		BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser();
		BI_PL_CountrySettingsUtility.isNotifyTransferAndShareEnabled();
		BI_PL_CountrySettingsUtility.isNotifyHierarchyEnabled();
		BI_PL_CountrySettingsUtility.hasCountryColumn('MAX_AP');
		BI_PL_CountrySettingsUtility.isTransferAndShareEnabledForFieldforce('fieldforce');

	}

	@isTest static void createCountrySetting(){

		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		String userCountryCode = testUser.Country_Code_BI__c;
		
		insert new BI_PL_Country_Settings__c(Name = 'BR' + testUser.Username, BI_PL_Country_Code__c = userCountryCode, BI_PL_Columns__c='MAX_AP', BI_PL_Default_owner_user_name__c = testUser.UserName, BI_PL_Specialty_field__c = 'Specialty_1_vod__c', BI_PL_MSL_view_fieldforces__c = 'MSL;HEOR;AMA;KEE;MSLCNS;MSLCVMET;MSLIMBIO;MSLONCO;MSLRESP', BI_PL_AMA_view_fieldforces__c = '', BI_PL_Dashboard_Information__c = 'summary_sales_table;summary_product_table;attainment_chart;attainment_by_product_chart;planit_veeva_table;frequency_chart;summary_by_p1_p2;overlap_table',
			BI_PL_Transfer_and_share_fieldforces__c = 'fieldforce', BI_PL_Transfer_share_notification__c = true);
	}
}