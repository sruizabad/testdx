/**
 *  12-06-2017
 *  @author OMEGA CRM
 */
public without sharing class BI_PL_CycleTriggerHandler
    implements BI_PL_TriggerInterface {

    public static final String DEFAULT_FIELD_FORCE_NAME = 'Default';

    private String userCountryCode;
    private Id countryDefaultFieldForceId;
    private String countryDefaultFieldForceName;

    private Map<BI_PL_Cycle__c, Id> fieldForceIdByCycle = new Map<BI_PL_Cycle__c, Id>();
    private Map<Id, String> fieldForceNamesById = new Map<Id, String>();
    private Map<String, Id> fieldForceIdByCountryCode = new Map<String, Id>();

    private Set<BI_PL_Cycle__c> cyclesWithErrors = new Set<BI_PL_Cycle__c>();

    // Constructor
    public BI_PL_CycleTriggerHandler() {

    }

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {
        System.debug('BULK BEFORE');
        // Store field force names for the cycle external id generation:
        if (Trigger.isInsert) {

            // 1.- Get all cycle field forces Id. In case the cycle has no field force, it will retrieve the default one.
            Set<String> countryCodesWithoutFieldForce = new Set<String>();
            for (BI_PL_Cycle__c c : (List<BI_PL_Cycle__c>)Trigger.new) {
                // If the country code is not set retrieve the user's country code:
                if (String.isBlank(c.BI_PL_Country_code__c)) {
                    userCountryCode = retrieveUserCountryCode((List<BI_PL_Cycle__c>)Trigger.new);
                }
                // If the field force is not defined store the country code to later look for the country's default field force.
                if (String.isBlank(c.BI_PL_Field_force__c)) {
                    if (String.isBlank(c.BI_PL_Country_code__c)) {
                        countryCodesWithoutFieldForce.add(userCountryCode);
                    } else {
                        countryCodesWithoutFieldForce.add(c.BI_PL_Country_code__c);
                    }
                } else {
                    fieldForceIdByCycle.put(c, c.BI_PL_Field_force__c);
                }
            }

            System.debug('userCountryCode ' + userCountryCode);
            System.debug('countryCodesWithoutFieldForce ' + countryCodesWithoutFieldForce);
            System.debug('fieldForceIdByCycle ' + fieldForceIdByCycle);

            // 2.- Get field forces for the cycle's with the field force field empty and also get all the used field force Names:
            for (BI_TM_FF_type__c ff : [SELECT Id, BI_TM_Country_Code__c, Name FROM BI_TM_FF_type__c
                                        WHERE (BI_TM_Country_Code__c IN:countryCodesWithoutFieldForce AND Name = : DEFAULT_FIELD_FORCE_NAME)
                                        OR ID IN:fieldForceIdByCycle.values()]) {
                System.debug('ff.Name ' + ff.Name + ' - ' + DEFAULT_FIELD_FORCE_NAME);
                if (ff.Name == DEFAULT_FIELD_FORCE_NAME)
                    fieldForceIdByCountryCode.put(ff.BI_TM_Country_Code__c, ff.Id);

                fieldForceNamesById.put(ff.Id, ff.Name);
            }
        }
    }
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter() {
    }

    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(SObject so) {
        if (!hasError((BI_PL_Cycle__c)so)) {
            BI_PL_Cycle__c cycle = (BI_PL_Cycle__c)so;

            Boolean defaultFieldForceName = String.isBlank(cycle.BI_PL_Field_force__c);

            // 1.-  If the cycle has no country code set then it's filled with the user's one.
            //           (if the country is null display an error)
            // 2.-  If the cycle has no field force set then fill it with the default one for the country.
            //           (if the default field force is not found display an error)

            setCountryCodeIfNotDefined(cycle);

            setFieldForceIfNotDefined(cycle);

            System.debug('SET EXTERNAL ID');
            setExternalId(cycle, defaultFieldForceName);

            System.debug('beforeInsert ' + cycle);
        }
    }
    /**
     * GloSAP GLOS-297
     * Fills the cycle's country code with the user's country code in case it's empty.
     * @author OMEGA CRM
     */
    private void setCountryCodeIfNotDefined(BI_PL_Cycle__c cycle) {
        System.debug('setCountryCodeIfNotDefined ' + cycle);

        if (String.isBlank(cycle.BI_PL_Country_code__c))
            cycle.BI_PL_Country_code__c = userCountryCode;
    }
    /**
     * GloSAP GLOS-297
     *  Adds an error to te cycle record.
     *  @author OMEGA CRM
     */
    private void addError(BI_PL_Cycle__c cycle, String message) {
        cyclesWithErrors.add(cycle);
        cycle.addError(message);
    }
    /**
     * GloSAP GLOS-297
     *  Returns true if the record has one or more errors.
     *  @author OMEGA CRM
     */
    private Boolean hasError(BI_PL_Cycle__c cycle) {
        return cyclesWithErrors.contains(cycle);
    }
    /**
     *  Gets the user country code. If not defined, add an error to all the cycle records to abort the trigger execution.
     *  @author OMEGA CRM
    */
    private String retrieveUserCountryCode(BI_PL_Cycle__c[] cycles) {
        System.debug('retrieveUserCountryCode');
        String output = [SELECT Country_Code_BI__c FROM User WHERE Id = : UserInfo.getUserId()].Country_Code_BI__c;
        if (String.isBlank(output)) {
            for (BI_PL_Cycle__c c : cycles) {
                System.debug('Error country');
                addError(c, Label.BI_PL_User_country_code_undefined);
            }
        }
        return output;
    }

    /**
     * GloSAP GLOS-297
     * Fills the cycle's country code with the user's country code in case it's empty.
     * @author OMEGA CRM
     */
    private void setFieldForceIfNotDefined(BI_PL_Cycle__c cycle) {
        System.debug('setFieldForceIfNotDefined ' + String.isBlank(cycle.BI_PL_Field_force__c) + ' - ' + cycle);
        System.debug('fieldForceIdByCountryCode ' + fieldForceIdByCountryCode);

        if (String.isBlank(cycle.BI_PL_Field_force__c)) {
            if (!fieldForceIdByCountryCode.containsKey(cycle.BI_PL_Country_code__c))
                cycle.addError(Label.BI_PL_Field_force_country_not_found + ' (' + cycle.BI_PL_Country_code__c + ')');

            cycle.BI_PL_Field_force__c = fieldForceIdByCountryCode.get(cycle.BI_PL_Country_code__c);

            System.debug('cycle.BI_PL_Field_force__c ' + cycle + ' - ' + fieldForceIdByCountryCode.get(cycle.BI_PL_Country_code__c));
        }
    }
    /**
     * GloSAP GLOS-297
     * Sets the cycle's external id.
     * @author OMEGA CRM
     */
    private void setExternalId(BI_PL_Cycle__c cycle, Boolean defaultFieldForceName) {
        cycle.BI_PL_External_Id__c = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, getFieldForceName(cycle, defaultFieldForceName));
        System.debug('setExternalId ' + cycle);
    }
    /**
     * GloSAP GLOS-297
     *  Returns the field force name.
     *  - If the field force is set returns its name.
     *  - If the field force is not set returns the default field force name for the country (supposed to be always DEFAULT_FIELD_FORCE_NAME).
     *  @author OMEGA CRM
     */
    private String getFieldForceName(BI_PL_Cycle__c cycle, Boolean defaultFieldForceName) {
        if (defaultFieldForceName) {
            return DEFAULT_FIELD_FORCE_NAME;
        } else {
            return fieldForceNamesById.get(cycle.BI_PL_Field_force__c);
        }
    }

    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(SObject oldSo, SObject so) {
    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so) {
    }

    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(SObject so) {
    }


    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(SObject oldSo, SObject so) {
    }

    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(SObject so) {
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {

    }

}