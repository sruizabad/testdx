/**
 *	12-06-2017
 *	@author OMEGA CRM
 */
@isTest
public with sharing class BI_PL_CycleTriggerHandler_Test {

	static Profile profile = [SELECT Id FROM Profile WHERE Name LIKE 'BR_%' LIMIT 1];

	@isTest
	public static void test() {

		User userBR = createUser('UserBR', 'BR');

		insert userBR;

		BI_TM_FF_type__c fieldForce1 = new BI_TM_FF_type__c(Name = 'FieldForceTest1', BI_TM_Country_Code__c = 'BR');
		BI_TM_FF_type__c fieldForce2 = new BI_TM_FF_type__c(Name = 'FieldForceTest2', BI_TM_Country_Code__c = 'BR');

		Date date1 = Date.newInstance(2017, 05, 17);
		Date date2 = Date.newInstance(2017, 05, 18);
		Date date3 = Date.newInstance(2017, 05, 19);
		Date date4 = Date.newInstance(2017, 05, 20);

		System.runAs(userBR) {
			insert new List<BI_TM_FF_type__c> {fieldForce1, fieldForce2};

			BI_PL_Cycle__c cycle1 = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'BR', BI_PL_Start_date__c = date1, BI_PL_End_date__c = date2, BI_PL_Field_force__c = fieldForce1.Id);
			BI_PL_Cycle__c cycle2 = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'BR', BI_PL_Start_date__c = date2, BI_PL_End_date__c = date3, BI_PL_Field_force__c = fieldForce2.Id);
            BI_PL_Cycle__c cycle3 = new BI_PL_Cycle__c(BI_PL_Country_code__c = '', BI_PL_Start_date__c = date1, BI_PL_End_date__c = date3, BI_PL_Field_force__c = fieldForce2.Id);
            //BI_PL_Cycle__c cycle4 = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'BR', BI_PL_Start_date__c = date2, BI_PL_End_date__c = date4, BI_PL_Field_force__c = null);
            //BI_PL_Cycle__c cycle5 = new BI_PL_Cycle__c(BI_PL_Country_code__c = '', BI_PL_Start_date__c = date1, BI_PL_End_date__c = date4, BI_PL_Field_force__c = null);


			insert new List<BI_PL_Cycle__c> {cycle1, cycle2, cycle3};

			List<BI_PL_Cycle__c> cycles = [SELECT Id, BI_PL_Field_force__r.Name, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_External_id__c FROM BI_PL_Cycle__c WHERE Id IN:new List<Id> {cycle1.Id, cycle2.Id} ORDER BY Id];

			checkCycleExternalId(cycles.get(0), '20170517', '20170518', fieldForce1.Name);
			checkCycleExternalId(cycles.get(1), '20170518', '20170519', fieldForce2.Name);

			cycle1.BI_PL_Start_date__c = Date.newInstance(2017, 05, 16);

			BI_PL_CycleTriggerHandler cycleTrigger = new BI_PL_CycleTriggerHandler();
			cycleTrigger.bulkAfter();
			cycleTrigger.beforeUpdate(cycle1, cycle2);
			cycleTrigger.beforeDelete(cycle1);
			cycleTrigger.afterInsert(cycle1);
			cycleTrigger.afterDelete(cycle1);

			update new List<BI_PL_Cycle__c> {cycle1, cycle2};

			delete new List<BI_PL_Cycle__c> {cycle1, cycle2};
		}
	}

	@isTest
	public static void testCheckDefaultCountryFieldForce() {

		User userBR = createUser('UserBR', 'BR');

		insert userBR;

		BI_TM_FF_type__c fieldForceDefaultBR = new BI_TM_FF_type__c(Name = BI_PL_CycleTriggerHandler.DEFAULT_FIELD_FORCE_NAME, BI_TM_Country_code__c = 'BR');
		BI_TM_FF_type__c fieldForce2 = new BI_TM_FF_type__c(Name = 'FieldForceTest2',BI_TM_Country_code__c = 'US');
		BI_TM_FF_type__c fieldForceDefaultDE = new BI_TM_FF_type__c(Name = BI_PL_CycleTriggerHandler.DEFAULT_FIELD_FORCE_NAME, BI_TM_Country_code__c = 'DE');

		Date date1 = Date.newInstance(2017, 05, 17);
		Date date2 = Date.newInstance(2017, 05, 18);
		Date date3 = Date.newInstance(2017, 05, 19);

		insert new List<BI_TM_FF_type__c> {fieldForceDefaultBR, fieldForce2, fieldForceDefaultDE};

		BI_PL_Cycle__c cycleWithoutFieldForce = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'BR', BI_PL_Start_date__c = date1, BI_PL_End_date__c = date2);
		BI_PL_Cycle__c cycleWithoutCountryAndFieldForce = new BI_PL_Cycle__c(BI_PL_Start_date__c = date2, BI_PL_End_date__c = date3);

		System.runAs(userBR) {
			insert cycleWithoutCountryAndFieldForce;
		}
		insert cycleWithoutFieldForce;

		List<BI_PL_Cycle__c> cycles = [SELECT Id, BI_PL_Field_force__c, BI_PL_Field_force__r.Name, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_External_id__c FROM BI_PL_Cycle__c WHERE Id IN:new List<Id> {cycleWithoutFieldForce.Id, cycleWithoutCountryAndFieldForce.Id} ORDER BY Id];

		// cycleWithoutFieldForce
		System.assertEquals('BR', cycles.get(0).BI_PL_Country_code__c, 'The country code is not right for the cycle.');
		System.assertEquals(fieldForceDefaultBR.Id, cycles.get(0).BI_PL_Field_force__c, 'The field force is not right for the cycle.');
		// cycleWithoutCountryAndFieldForce
		System.assertEquals('BR', cycles.get(1).BI_PL_Country_code__c, 'The country code is not right for the cycle.');
		System.assertEquals(fieldForceDefaultBR.Id, cycles.get(1).BI_PL_Field_force__c, 'The field force is not right for the cycle.');

	}

	private static User createUser(String name, String country) {
		return new User(Alias = name.substring(0, 5), Country_Code_BI__c = country, Email = name + '@testorg.com',
		                EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US',
		                LocaleSidKey = 'en_US', ProfileId = profile.Id, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = name + '_External_id_Test');
	}

	private static void checkCycleExternalId(BI_PL_Cycle__c c, String startDate, String endDate, String fieldForceName) {
		System.assertEquals(c.BI_PL_Country_code__c + '_' + startDate + '_' + endDate + '_' + fieldForceName, c.BI_PL_External_id__c);
	}

	@isTest
	public static void testUserNoCountryCodeandFF(){
		String name = 'testCycleNoCC';
		User u = new User(Alias = name.substring(0, 5),  Email = name + '@testorg.com',
		                EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US',
		                LocaleSidKey = 'en_US', ProfileId = profile.Id, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = name + '_External_id_Test_no_cc');
		insert u;
	
		System.runAs(u){
			Date date1 = Date.newInstance(2017, 05, 17);
			Date date2 = Date.newInstance(2017, 05, 18);

			BI_TM_FF_type__c fieldForce1 = new BI_TM_FF_type__c(Name = 'FieldForceTest1', BI_TM_Country_code__c='AA');
			insert fieldForce1;
			BI_PL_Cycle__c cycle1 = new BI_PL_Cycle__c( BI_PL_Start_date__c = date1, BI_PL_End_date__c = date2, BI_PL_Field_force__c = fieldForce1.Id);
			BI_PL_CycleTriggerHandler cth = new BI_PL_CycleTriggerHandler();
			BI_PL_Cycle__c cycle2 = new BI_PL_Cycle__c( BI_PL_Start_date__c = date1, BI_PL_End_date__c = date2, BI_PL_Country_code__c = 'AJSHS');


			try{
				insert cycle1;
			}catch(Exception e){
				Boolean expectedExceptionThrown =  e.getMessage().contains(Label.BI_PL_User_country_code_undefined) ? true : false;
				System.AssertEquals(expectedExceptionThrown, true);
			}
			try{
				insert cycle2;
			}catch(Exception e){
				Boolean expectedExceptionThrown =  e.getMessage().contains(Label.BI_PL_Field_force_country_not_found + ' (' + cycle2.BI_PL_Country_code__c + ')') ? true : false;
				System.AssertEquals(expectedExceptionThrown, true);
			}
			
		}

	}
}