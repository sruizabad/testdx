/**
 *  17/10/2017
 *  - GLOS-427: Single specialty field at target preparation level
 *  07/09/2017
 *  - Change BI_PL_Primary_specialty__c with BI_PL_Specialty__c
 *  25/07/2017
 *  - GLOS-329: Add the Adjusted capacity to the Interactions adjusted vs planned by territory chart.
 *              Create a new bar chart with the Percentage of allocation (Approved) and achievement. 
 *	06/07/2017
 *  - Now the Global Capacity is at Cycle level, so the preparation gets its value from a formula field (no field filling through APEX anymore).
 */
public with sharing class BI_PL_DashboardsCtrl {
	public BI_PL_DashboardsCtrl() {
		
	}

	public class SetupModel{
        public Map<String, Boolean> permissions;
        public List<String> planitCountries;
        public String currentUserCountry;
        public List<BI_PL_Cycle__c> cycles;
        public List<Schema.PicklistEntry> channels;
        public List<Schema.PicklistEntry> segments;

        
        //public map<String,list<String>> salesPositionsBySM;
        //public list<String> selectSMOptions;
        //public map<Id,list<String>> mSubRoleNameByRoleId;
        //public Integer MAX_ITERATIONS = 99;
        //public Integer currentIterations = 0;
        //public map<String, list<String>> smInRole;
        public SetupModel() {
        	planitCountries = new List<String>();
        }
    }

    /**
     * Gets the setup data, such as cycles, countries and permissions for current user
     *
     * @return     The setup data.
     */
	@RemoteAction
	public static SetupModel getSetupData() {
		SetupModel model = new SetupModel();

		//Channels adn segments
		model.channels = BI_PL_Channel_detail_preparation__c.fields.BI_PL_Channel__c.getDescribe().getPicklistValues();
		model.segments = BI_PL_Detail_preparation__c.fields.BI_PL_Segment__c.getDescribe().getPicklistValues();

		//Countries
        for(BI_PL_Country_Settings__c cs : BI_PL_Country_Settings__c.getall().values()){
            model.planitCountries.add(cs.BI_PL_Country_Code__c);
        }

        //Current user country
        model.currentUserCountry = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId()].Country_Code_BI__c;

        //permissions
        model.permissions = BI_PL_PreparationServiceUtility.getCurrentUserPermissions();

        //Cycles
        model.cycles = [SELECT Id, Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE BI_PL_Country_Code__c = :model.currentUserCountry ORDER BY BI_PL_Start_date__c DESC];

        return model;
	}

	/**
	 * Gets the hierarchies for a give cycle.
	 *
	 * @param      cycleId  The cycle identifier
	 *
	 * @return     The hierarchies for cycle.
	 */
	@RemoteAction
	public static Set<String> getHierarchiesForCycle(String cycleId) {
		Set<String> hierarchies = new Set<String>();

		/*for(AggregateResult ag : [SELECT BI_PL_hierarchy__c 
				FROM BI_PL_position_cycle__c 
				WHERE BI_PL_cycle__c =: cycleId 
				GROUP BY BI_PL_hierarchy__c]) {
			hierarchies.add(String.valueOf(ag.get('BI_PL_hierarchy__c')));
		}*/

		List<BI_PL_Position_cycle_user__c> hierarchyList = [SELECT BI_PL_User__c, BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c FROM BI_PL_Position_cycle_user__c WHERE BI_PL_User__c =:UserInfo.getUserId() AND  BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycleId];
        
	    for(BI_PL_Position_cycle_user__c hierarchy : hierarchyList)
	    {
	            hierarchies.add(String.valueOf(hierarchy.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c));
	    }
	   
		return hierarchies;
	}

	/**
	 * Gets the positions for a fiven cycle and hierarchy name
	 *
	 * @param      cycleId        The cycle identifier
	 * @param      hierarchyName  The hierarchy name
	 *
	 * @return     The positions.
	 */
	@RemoteAction
	public static List<BI_PL_Position__c> getPositions(String cycleId, String hierarchyName) {
		Set<String> posIds = new Set<String>();
		//System.debug(loggingLevel.Error, '*** cycleId: ' + cycleId);
		//System.debug(loggingLevel.Error, '*** hierarchyName: ' + hierarchyName);
		for(BI_PL_position_cycle__c ag : [SELECT BI_PL_Position__c 
				FROM BI_PL_position_cycle__c 
				WHERE BI_PL_cycle__c =: cycleId 
				AND BI_PL_Hierarchy__c = :hierarchyName]) {

			posIds.add(ag.BI_PL_Position__c);
		}


		return [SELECT Id, Name FROM BI_PL_Position__c WHERE Id IN :posIds];
	}

	/**
     *  Returns a map where each position is a hierarchy node visible for the current user (grouped by position id) and the
     *  hierarchy root node.
     *  @author OMEGA CRM
     */
    @RemoteAction
    public static PLANiTHierarchyNodesWrapper getHierarchyNodes(String countryCode, String cycleId, String hierarchyName) {
        Map<String, PLANiTTreeNode> hierarchy = new Map<String, PLANiTTreeNode>();
        //Get current cycle
        //BI_PL_Cycle__c[] cycles = new List<BI_PL_Cycle__c>([SELECT Id FROM BI_PL_Cycle__c WHERE BI_PL_Start_date__c > TODAY
        //        AND BI_PL_Country_code__c = :countryCode ORDER BY BI_PL_Start_date__c LIMIT 1]);
        //String currentCycleId = cycles.get(0).Id;

        Map<String, BI_PL_PositionCycleWrapper> tree = new BI_PL_VisibilityTree(cycleId, hierarchyName).getTree();

        String rootNodeId;
        List<String> treeList = new List<String>(tree.keySet());

        for (Integer i = 0; i < treeList.size(); i++) {
            System.debug('tree size: ' + treeList.size() + ' positionId: ' + treeList.get(i));
            String positionId =  treeList.get(i);
            PLANiTTreeNode node = new PLANiTTreeNode(tree.get(positionId));
            hierarchy.put(positionId, node);

            if (node.isCurrentUsersNode)
                rootNodeId = node.positionId;
        }
        System.debug('rootNodeId: ' + rootNodeId);

        return new PLANiTHierarchyNodesWrapper(hierarchy, rootNodeId);
    }

	@RemoteAction
	public static List<BI_PL_Business_Rule__c> getThresholdBusinessRules(String countryCode) {
		return [SELECT Id, BI_PL_Country_code__c,
                            BI_PL_Visits_per_day__c,
							BI_PL_Required_capacity__c,
                            BI_PL_Field_Force__c
                            FROM BI_PL_Business_rule__c 
                            WHERE BI_PL_Active__c = true 
                            AND BI_PL_Country_code__c =: countryCode
                            AND BI_PL_Type__c = 'Threshold'];
	}

	/**
	 * Class for grouped result model.
	 */
	public class GroupedResultModel {
		List<BI_PL_Channel_detail_preparation__c> channelDetails;
		List<BI_PL_Detail_preparation__c> details;
		String lastOffsetId;
	}

	/**
	 * Get the records (of type BI_PL_Channel_detail_preparation__c) needed for 
	 * all the preparations of a given contry, cycle and hierarchy.
	 * 
	 * This methods can be called multiple times due to large dataset. Paginated and ordered by Id
	 *
	 * @param      countryCode   The country code
	 * @param      cycleId       The cycle identifier
	 * @param      hierarchy     The hierarchy
	 * @param      lastIdOffset  The last identifier offset
	 *
	 * @return     The grouped data.
	 */
	@RemoteAction
	public static GroupedResultModel getGroupedData(String countryCode, String cycleId, String hierarchy, String territory, String lastIdOffset) {

		GroupedResultModel model = new GroupedResultModel();

		System.debug(loggingLevel.Error, '*** getGroupedData countryCode: ' + countryCode);
		System.debug(loggingLevel.Error, '*** getGroupedData cycleId: ' + cycleId);
		System.debug(loggingLevel.Error, '*** getGroupedData hierarchy: ' + hierarchy);
        String idOffset = lastIdOffset != null ? lastIdOffset : '';
        List<BI_PL_Channel_detail_preparation__c> result = new List<BI_PL_Channel_detail_preparation__c>();

        if(territory!='')
        {
        	result = new List<BI_PL_Channel_detail_preparation__c>([
			SELECT 	Id,
					BI_PL_Max_adjusted_interactions__c, 
					BI_PL_Max_planned_interactions__c, 
					BI_PL_Sum_adjusted_interactions__c, 
					BI_PL_Sum_planned_interactions__c,
					BI_PL_Channel__c,
					BI_PL_Number_details__c,
					BI_PL_Removed__c,
					BI_PL_Target__r.BI_PL_Target_customer__r.Name,
					BI_PL_Target__r.BI_PL_Removed__c,
					BI_PL_Target__r.BI_PL_Added_manually__c,
					BI_PL_Target__r.BI_PL_Next_best_account__c,
					BI_PL_Target__r.BI_PL_No_see_list__c,
					BI_PL_Target__r.BI_PL_Header__c,
					BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c,
					BI_PL_Target__r.BI_PL_Header__r.BI_PL_Field_force__c,
					BI_PL_Target__r.BI_PL_Header__r.BI_PL_Final_adjusted_capacity__c,
					BI_PL_Target__r.BI_PL_Header__r.BI_PL_Total_capacity__c,
					BI_PL_Target__r.BI_PL_Specialty__c,
					BI_PL_Reviewed__c,
					BI_PL_Edited__c,
					(SELECT Id, BI_PL_Segment__c, BI_PL_Adjusted_details__c, BI_PL_Planned_details__c
					FROM BI_PL_Details_Preparation__r)
			FROM BI_PL_Channel_detail_preparation__c
			WHERE BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
			AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchy
			AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Country_code__c = :countryCode
			AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__c = :territory
			AND Id > : idOffset
            ORDER BY Id ASC
            LIMIT 4500
			]);
        }
        else
        {
        	result = new List<BI_PL_Channel_detail_preparation__c>([
			SELECT 	Id,
					BI_PL_Max_adjusted_interactions__c, 
					BI_PL_Max_planned_interactions__c, 
					BI_PL_Sum_adjusted_interactions__c, 
					BI_PL_Sum_planned_interactions__c,
					BI_PL_Channel__c,
					BI_PL_Number_details__c,
					BI_PL_Removed__c,
					BI_PL_Target__r.BI_PL_Target_customer__r.Name,
					BI_PL_Target__r.BI_PL_Removed__c,
					BI_PL_Target__r.BI_PL_Added_manually__c,
					BI_PL_Target__r.BI_PL_Next_best_account__c,
					BI_PL_Target__r.BI_PL_No_see_list__c,
					BI_PL_Target__r.BI_PL_Header__c,
					BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c,
					BI_PL_Target__r.BI_PL_Header__r.BI_PL_Field_force__c,
					BI_PL_Target__r.BI_PL_Header__r.BI_PL_Final_adjusted_capacity__c,
					BI_PL_Target__r.BI_PL_Header__r.BI_PL_Total_capacity__c,
					BI_PL_Target__r.BI_PL_Specialty__c,
					BI_PL_Reviewed__c,
					BI_PL_Edited__c,
					(SELECT Id, BI_PL_Segment__c, BI_PL_Adjusted_details__c, BI_PL_Planned_details__c
					FROM BI_PL_Details_Preparation__r)
			FROM BI_PL_Channel_detail_preparation__c
			WHERE BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
			AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchy
			AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Country_code__c = :countryCode 
			AND Id > : idOffset
            ORDER BY Id ASC
            LIMIT 4500
			]);

        }
		

		model.channelDetails = result;
		model.lastOffsetId = result.size() >= 4500 ? result.get(result.size() - 1).Id : null;

		System.debug(loggingLevel.Error, '*** result: ' + result);

		return model;

	}

	/**
	 * Calls a batch process that approves all the preparations for a country, cycle and hierarchy
	 *
	 * @param      countryCode    The country code
	 * @param      cycleId        The cycle identifier
	 * @param      hierarchyName  The hierarchy name
	 *
	 * @return     Id of the batch job
	 */
	@RemoteAction
	public static String approveAll(String countryCode, String cycleId, String hierarchyName) {
		String batchid = Database.executeBatch(new BI_PL_PreparationChangeStatusBatch(countryCode, cycleId, hierarchyName, 'Approved'));
		return batchid;
	}
	
	@RemoteAction
	public static String approveAllAdjustments(String countryCode, String cycleId, String hierarchyName) {
		String batchid = Database.executeBatch(new BI_PL_PreparationApproveAdjBatch(countryCode, cycleId, hierarchyName));
		return batchid;
	}
	
	@RemoteAction
	public static String reopenAll(String countryCode, String cycleId, String hierarchyName) {
		String batchid = Database.executeBatch(new BI_PL_PreparationChangeStatusBatch(countryCode, cycleId, hierarchyName, 'Under Review'));
		return batchid;
	}


	 public class PLANiTHierarchyNodesWrapper {

        public Map<String, PLANiTTreeNode> hierarchy {get; set;}
        public String rootNodeId {get; set;}

        public PLANiTHierarchyNodesWrapper(Map<String, PLANiTTreeNode> hierarchy, String rootNodeId) {
            this.rootNodeId = rootNodeId;
            this.hierarchy = hierarchy;
        }
    }

    public class PLANiTTreeNode {
        public Id repId;
        public String repName;

        public String positionName;

        public Id positionId;
        public Id parentPositionId;

        public List<String> childrenIds = new List<String>();

        public Boolean isCurrentUsersNode = false;
        public Boolean isConfirmed;
        public String rejectedReason;

        public Id positionCycleId;

        public PLANiTTreeNode (BI_PL_PositionCycleWrapper pcw) {
            if (pcw.positionCycleUsersExclusiveNode.size() > 0) {
                this.repId = pcw.positionCycleUsersExclusiveNode.get(0).BI_PL_User__c;
                this.repName = pcw.positionCycleUsersExclusiveNode.get(0).BI_PL_User__r.Name;
            }

            for (BI_PL_Position_cycle_user__c pcu : pcw.positionCycleUsersExclusiveNode) {
                if (pcu.BI_PL_User__c == UserInfo.getUserId()) {
                    isCurrentUsersNode = true;
                    break;
                }
            }
            this.positionCycleId = pcw.positionCycleId;
            this.positionName = pcw.position.record.Name;
            this.positionId = pcw.position.recordId;
            this.parentPositionId = pcw.parentPosition.recordId;
            this.isConfirmed = pcw.isConfirmed;
            this.rejectedReason = pcw.rejectedReason;

            for (BI_PL_PositionCycleWrapper child : pcw.getChildren())
                this.childrenIds.add(child.position.recordId);
        }
    }
}