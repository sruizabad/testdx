@isTest
private class BI_PL_DashboardsCtrl_Test {
	private static String userCountryCode;
	private static String hierarchy = 'hierarchyTest';

	@isTest static void test_method_one() {
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		userCountryCode = testUser.Country_Code_BI__c;
		
        BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();
		BI_PL_TestDataFactory.usersCreation(userCountryCode);
		BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
		BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

		testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
		List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

		
		List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
		System.debug('prods*+*'+listProd);

		BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
		List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];

        Date currentDate = Date.today();
             

        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

		BI_PL_DashboardsCtrl controller = new BI_PL_DashboardsCtrl();
		BI_PL_DashboardsCtrl.SetupModel setupModel = BI_PL_DashboardsCtrl.getSetupData();

		Set<String>  hierarchies = BI_PL_DashboardsCtrl.getHierarchiesForCycle(setupModel.cycles[0].Id);
		BI_PL_DashboardsCtrl.PLANiTHierarchyNodesWrapper   hierarchyNodes = BI_PL_DashboardsCtrl.getHierarchyNodes(userCountryCode, cycle.Id, hierarchy);
		//List<String> lstHierarchies = new List<String>(hierarchies);
		List<BI_PL_Position__c> lstPositions =  BI_PL_DashboardsCtrl.getPositions(cycle.Id, hierarchy);
		System.debug('lstPositions:'+lstPositions);
		List<BI_PL_Business_Rule__c> businessRules = BI_PL_DashboardsCtrl.getThresholdBusinessRules(userCountryCode);
		String idApproveAll = BI_PL_DashboardsCtrl.approveAll(userCountryCode, cycle.Id, hierarchy);
		String idApproveAllAdjustments = BI_PL_DashboardsCtrl.approveAllAdjustments(userCountryCode, cycle.Id, hierarchy);
		String idReopenAll = BI_PL_DashboardsCtrl.reopenAll(userCountryCode, cycle.Id, hierarchy);
        BI_PL_DashboardsCtrl.GroupedResultModel resultModel = BI_PL_DashboardsCtrl.getGroupedData(userCountryCode, cycle.Id, hierarchy,lstPositions[0].Id,'');
        BI_PL_DashboardsCtrl.GroupedResultModel resultModelWithoutTerritory = BI_PL_DashboardsCtrl.getGroupedData(userCountryCode,cycle.Id,hierarchy,'','');
	}
	
	
}