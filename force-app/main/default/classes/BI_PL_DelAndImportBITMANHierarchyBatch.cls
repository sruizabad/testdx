/**
 *	04/07/2017
 *	- Exceptions in Custom labels.
 * 
 *	Deletes the records related to the alignment and cycle for the specified date an calls the import from bitman batch.
 *	@author Omega CRM
 */
global class BI_PL_DelAndImportBITMANHierarchyBatch implements Database.Batchable<sObject>, Database.Stateful {

	private String hierarchyName;

	private Id alignmentId;
	private Date snapshotDate;
	private Id cycleId;


	global BI_PL_DelAndImportBITMANHierarchyBatch(Id alignmentId, Date snapshotDate, Id cycleId) {
		if (snapshotDate == null)
			throw new BI_PL_Exception(Label.BI_PL_Snapshot_date_null);

		if(String.isBlank(alignmentId))
			throw new BI_PL_Exception(Label.BI_PL_Alignment_null);

		if(String.isBlank(cycleId))
			throw new BI_PL_Exception(Label.BI_PL_Cycle_null);

		System.debug('BI_PL_DelAndImportBITMANHierarchyBatch Constructor');
		BI_TM_Alignment__c alignment = [SELECT Id, Name, BI_TM_Country_Code__c, BI_TM_Start_date__c, BI_TM_End_date__c, BI_TM_FF_type__c FROM BI_TM_Alignment__c WHERE Id = :alignmentId LIMIT 1];

		hierarchyName = alignment.Name;
		this.alignmentId = alignmentId;
		this.snapshotDate = snapshotDate;
		this.cycleId = cycleId;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {

		System.debug('BI_PL_DelAndImportBITMANHierarchyBatch START ' + cycleId + ' - ' + hierarchyName);
		// Delete position cycles where cycle.Id = selected && hierarchy = alignment.name && BITMAN = true
		//String query = 'SELECT Id, BI_PL_Position__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = \'' + cycleId + '\' AND BI_PL_Hierarchy__c =\'' + hierarchyName + '\' AND BI_PL_From_Bitman__c = true';
		
		return Database.getQueryLocator([SELECT Id, BI_PL_Position__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c =: cycleId AND BI_PL_Hierarchy__c =: hierarchyName]);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		try {
			System.debug('BI_PL_DelAndImportBITMANHierarchyBatch execute');

			List<Id> positionCycleIds = new List<Id>();
			List<Id> positionIds = new List<Id>();

			List<BI_PL_Position_cycle__c> pcs = new List<BI_PL_Position_cycle__c>();
			for (SObject o : scope) {
				positionCycleIds.add(((BI_PL_Position_cycle__c)o).Id);
				positionIds.add(((BI_PL_Position_cycle__c)o).BI_PL_Position__c);
			}

			// 1.- Delete position cycle users that reference the position cycles:
			System.debug('delete positionCycleIds ' + positionCycleIds);
			delete [SELECT Id FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c IN: positionCycleIds];

			// 2.- Delete position cycles:
			delete [SELECT Id FROM BI_PL_Position_cycle__c WHERE Id IN: positionCycleIds];

			// 3.- Delete positions referenced by the position cycles:
			// Database.delete with allOrNone = false because there may be other records pointing to the same position, so we want to kepp them.
			System.debug('delete positionCycleIds ' + positionIds);

			Database.delete([SELECT Id FROM BI_PL_Position__c WHERE Id IN: positionIds], false);
		} catch (exception e) {
			throw e;
		}
	}

	global void finish(Database.BatchableContext BC) {
		// Once all imported records are deleted, import the hierarchy from BITMAN:
		if(!Test.isRunningTest())
			Database.executeBatch(new BI_PL_ImportBITMANHierarchyBatch(alignmentId, snapshotDate, cycleId));
	}

}