global class BI_PL_DeleteAffiliationBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject> {
	
	String query;
	
	global BI_PL_DeleteAffiliationBatch() {
		System.debug(loggingLevel.Error, '*** BATCHcontrcutpr: ');
	}

	private String getFilters(){
		String queryFilters = '';
        if(this.params != null && this.params.containsKey('type')){
        	String t = String.valueOf(this.params.get('type'));
            queryFilters += 'WHERE  BI_PL_Type__c = \'' + t + '\'';
        }
        if(this.countryCode == null) {
        	String c = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
        	queryFilters +=  ' AND BI_PL_Country_code__c = \'' + c + '\'';
        }else{
        	queryFilters +=  ' AND BI_PL_Country_code__c = \'' + this.countryCode + '\'';

        }
        return queryFilters;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id FROM BI_PL_Affiliation__c ' + getFilters();
		System.debug(loggingLevel.Error, '*** country Code que llega del : ' + this.countryCode);
		System.debug(loggingLevel.Error, '*** BATCH START: ');
		System.debug(loggingLevel.Error, '*** query: ' + query);

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		delete scope;
	}
	
	global void finish(Database.BatchableContext BC) {
	}
}