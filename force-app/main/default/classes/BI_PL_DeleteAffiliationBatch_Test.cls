@isTest
public with sharing class BI_PL_DeleteAffiliationBatch_Test {
	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        BI_PL_TestDataFactory.createAffiliationSI('Sales', listAcc, listProd, userCountryCode);
        BI_PL_TestDataFactory.createAffiliationRelationship(listAcc, listAcc[0], userCountryCode);
    }


    @isTest
    public static void test() {

    	//Asert current records size
        List<BI_PL_Affiliation__c> beforeAff = [SELECT Id FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = 'Sales'];
        System.assertEquals(16, beforeAff.size());

        Test.startTest();
        BI_PL_DeleteAffiliationBatch b = new BI_PL_DeleteAffiliationBatch();
        b.init(null, null, null, new Map<String, Object>{ 'type' => 'Sales'});
        Database.executeBatch(b);
        Test.stopTest();


    	//Asert after batch execution records size. Should be zero
        List<BI_PL_Affiliation__c> afterAff = [SELECT Id FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = 'Sales'];
        System.assertEquals(0, afterAff.size());


    }
}