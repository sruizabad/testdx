/**
*   - Delete:
*        - Position Cycle User 
*        - Position Cycle
*        - Cycle
*   
*   - Set all call plans as pending to be deleted
*   - Change Call Plans Owner to default country owner
*           
*   @author Omega CRM
*/
global class BI_PL_DeleteCycleBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    private Id cycleId;
    private Id defaultOwnerId;
    private String countryCode;
    public Set<String> cyclesToDelete = new Set<String>();
    
    global BI_PL_DeleteCycleBatch() {}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator([SELECT Id, BI_PL_Position__c, BI_PL_Cycle__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__r.BI_PL_Type__c = 'tobedeleted']);
    }
    
 
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('BI_PL_DeleteCycleBatch execute');
        
        Id cycleId = ((BI_PL_Position_cycle__c)scope.get(0)).BI_PL_Cycle__c;
        cyclesToDelete.add(cycleId);
        
        List<Id> positionCycleIds = new List<Id>();
        
        for (SObject o : scope)
            positionCycleIds.add(((BI_PL_Position_cycle__c)o).Id);
        
        // 1.- Delete position cycle users that reference the position cycles:
        System.debug('delete positionCycleIds ' + positionCycleIds);
        Database.delete([SELECT Id FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c IN: positionCycleIds], true);
        
        // 2.- Delete preparations:
        Database.delete([SELECT Id FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__c IN: positionCycleIds], true);
        
        // 3.- Delete position cycles:
        Database.delete([SELECT Id FROM BI_PL_Position_cycle__c WHERE Id IN: positionCycleIds], true);

    }

    global void finish(Database.BatchableContext BC) {
        /// 4.- Delete cycle
        Database.delete([SELECT Id FROM BI_PL_Cycle__c WHERE Id IN :cyclesToDelete], true);
    }
    
}