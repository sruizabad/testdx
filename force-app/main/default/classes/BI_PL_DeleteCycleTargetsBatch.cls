/**
*      This batch deletes:
*
*            Targets 
*               |
*   Channels Detail Preparation
*               |
*       Details Preparation
*   
*        
*	@author Omega CRM
*/
global class BI_PL_DeleteCycleTargetsBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {
    
    private Id cycleId;
    
    global BI_PL_DeleteCycleTargetsBatch() {}

    global BI_PL_DeleteCycleTargetsBatch(Id cycleId) {
        if (String.isBlank(cycleId))
            throw new BI_PL_Exception(Label.BI_PL_Cycle_null);
        
        this.cycleId = cycleId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
	

        this.cycleId = (!this.cycleIds.isEmpty() ? this.cycleIds.get(0) : this.cycleId);

        return Database.getQueryLocator([SELECT Id FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycleId]);

    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        delete scope;
       
    }
    global void finish(Database.BatchableContext BC) {

        BI_PL_Country_settings__c countrySetting = [SELECT BI_PL_Default_owner_user_name__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c =: this.countryCode];
        String defaultOwnerId = null;
        if(countrySetting!=null){
            String defaulOwnerUsername = countrySetting.BI_PL_Default_owner_user_name__c;
            defaultOwnerId = [SELECT Id FROM User WHERE Username =: defaulOwnerUsername].Id;
        }

        List<BI_PL_Preparation__c> preparationList = [SELECT Id, OwnerId FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycleId];
        
        for(BI_PL_Preparation__c callPlan: preparationList){
            if(defaultOwnerId != null)
                callPlan.OwnerId = defaultOwnerId;
        }
        update preparationList;
    
        BI_PL_Cycle__c cycle = [SELECT Id, BI_PL_Type__c FROM BI_PL_Cycle__c WHERE Id =: cycleId];
        cycle.BI_PL_Type__c = 'tobedeleted';
        update cycle;

        
    	//Database.executeBatch( new BI_PL_DeleteCycleBatch(this.cycleId, this.countryCode));

    }
}