/**
 * Deletes Error logs older than 2 weeks ago and not set to be kept.
 * @author     OMEGA CRM
 */
global without sharing class BI_PL_DeleteOldErrorLogs extends BI_PL_PlanitProcess implements Schedulable, Database.Batchable<sObject> {

	public static String SCHEDULED_JOB_NAME = 'Planit - Delete old error logs ';
	private final Integer NUMBER_OF_DAYS_PER_WEEK = 7;

	private String countryCode;
	private Boolean deleteAll;

	global BI_PL_DeleteOldErrorLogs(String countryCode) {
		this.countryCode = countryCode;
	}

	global BI_PL_DeleteOldErrorLogs() {
		this.countryCode = getCurrentUserCountryCode();
	}

	private static String getCurrentUserCountryCode() {
        return [SELECT Id, Country_Code_BI__c FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
		this.deleteAll = this.params.containsKey('deleteAll') ? Boolean.valueOf(this.params.get('deleteAll')) : false;
		
		if(!this.deleteAll) {
			return Database.getQueryLocator([SELECT Id FROM BI_COMMON_Error__c WHERE BI_COMMON_Do_not_delete__c = false AND CreatedDate <= :Date.today() - NUMBER_OF_DAYS_PER_WEEK * 2 ORDER BY CreatedDate]);
		} else {
			return Database.getQueryLocator([SELECT Id FROM BI_COMMON_Error__c WHERE BI_COMMON_Do_not_delete__c = false]);
		}
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		delete scope;
	}

	global void finish(Database.BatchableContext BC) {

	}

	global void execute(SchedulableContext sc) {

		BI_PL_Country_settings__c countrySetting = [SELECT Id, BI_PL_Delete_error_logs_night_process__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = :countryCode];

		if (countrySetting.BI_PL_Delete_error_logs_night_process__c)
			Database.executeBatch(new BI_PL_DeleteOldErrorLogs(countryCode));
	}

	/**
	 *	Schedules the Delete logs process for the specified country.
	 *	@author	OMEGA CRM
	 */
	public static void scheduleNightProcess(String countryCode, Integer hours, Integer minutes) {
		system.schedule(SCHEDULED_JOB_NAME + countryCode, '0 ' + minutes + ' ' + hours + ' * * ?', new BI_PL_DeleteOldErrorLogs(countryCode));
	}
}