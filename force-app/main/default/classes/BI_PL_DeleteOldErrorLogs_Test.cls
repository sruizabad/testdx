@isTest
private class BI_PL_DeleteOldErrorLogs_Test {
	
	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        //BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);
    }

    @isTest
    public static void testFromCOnsole() {

    	List<BI_COMMON_Error__c> errs = new List<BI_COMMON_Error__c>();

    	for(Integer i = 0; i < 50; i++) {
    		errs.add(new BI_COMMON_Error__c(BI_COMMON_Application__c = 'PLANIT',
		                              BI_COMMON_Error_message__c = 'TEST' + i,
		                              BI_COMMON_Trace__c = 'TEST' + i,
		                              BI_COMMON_Component_name__c = 'TEST' + i,
		                              BI_COMMON_Functionality__c = 'TEST' + i,
		
		                              BI_COMMON_Server_side__c = true,
		                              BI_COMMON_User__c = UserInfo.getUserId()));
    	}

    	insert errs;

        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        //Map<String, Object> params = new Map<String, Object> { 'deleteAll' => true };

        Test.startTest();
        
        BI_PL_DeleteOldErrorLogs b = new BI_PL_DeleteOldErrorLogs(userCountryCode);
        Database.executeBatch(b);

        Test.stopTest();

        List<BI_COMMON_Error__c> errors = new List<BI_COMMON_Error__c>([SELECT ID  FROM BI_COMMON_Error__c ]);

        System.assertEquals(50, errors.size());
        

    }

    @isTest
    public static void testFromAdminPage() {

    	List<BI_COMMON_Error__c> errs = new List<BI_COMMON_Error__c>();

    	for(Integer i = 0; i < 50; i++) {
    		errs.add(new BI_COMMON_Error__c(BI_COMMON_Application__c = 'PLANIT',
		                              BI_COMMON_Error_message__c = 'TEST' + i,
		                              BI_COMMON_Trace__c = 'TEST' + i,
		                              BI_COMMON_Component_name__c = 'TEST' + i,
		                              BI_COMMON_Functionality__c = 'TEST' + i,
		
		                              BI_COMMON_Server_side__c = true,
		                              BI_COMMON_User__c = UserInfo.getUserId()));
    	}

    	insert errs;

        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        Map<String, Object> params = new Map<String, Object> { 'deleteAll' => true };

        Test.startTest();
        
        BI_PL_DeleteOldErrorLogs b = new BI_PL_DeleteOldErrorLogs();
        b.init(null, null, null, params);

        Database.executeBatch(b);
        Test.stopTest();

        List<BI_COMMON_Error__c> errors = new List<BI_COMMON_Error__c>([SELECT ID  FROM BI_COMMON_Error__c ]);

        System.assertEquals(0, errors.size());
        

    }
	
	@isTest
    public static void testSchedule() {

    	User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

    	Test.startTest();
        //BI_PL_DeleteOldErrorLogs b = new BI_PL_DeleteOldErrorLogs(userCountryCode);
        BI_PL_DeleteOldErrorLogs.scheduleNightProcess(userCountryCode, 5, 5); 
        //String chron = '0 0 23 * * ?';        
        //System.schedule('Test Sched', chron, b);
        Test.stopTest();

    }
}