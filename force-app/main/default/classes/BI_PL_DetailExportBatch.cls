global class BI_PL_DetailExportBatch implements Database.Batchable<sObject>, Database.Stateful {
	
	String query;
	//Document doc;
	String attId;
	String cycleId;
	String separator;
	String extension;
	String dataEncapsulator;
	BI_PL_View__c selectedView;
	
	global BI_PL_DetailExportBatch(String viewType, String countryCode, String cycleId, String hierarchy, String channel, String position, String planId) {
		this.cycleId = cycleId;
		this.selectedView = BI_PL_ViewService.getDetailsView(null, countryCode, viewType);
		this.separator = (selectedView.BI_PL_Export_separator__c == null || selectedView.BI_PL_Export_separator__c == '') ? '' : selectedView.BI_PL_Export_separator__c;
		this.extension = (selectedView.BI_PL_Export_file_extension__c == null || selectedView.BI_PL_Export_file_extension__c == '') ? 'csv' : selectedView.BI_PL_Export_file_extension__c;
		this.dataEncapsulator = (selectedView.BI_PL_Export_data_encapsulator__c == null || selectedView.BI_PL_Export_data_encapsulator__c == '') ? '' : selectedView.BI_PL_Export_data_encapsulator__c;
		this.query = BI_PL_PreparationServiceUtility.getPaginatedDetailsQueryWithDirection(selectedView.BI_PL_Query_locator__c, null, true,  cycleId, hierarchy, channel,  position,planId, null, null, null, false);
	}
	
	global Database.QueryLocator start(Database.BatchableContext bc) {
		//1. Create the final document
   		List<BI_PL_ViewService.PLanitColumn> columns = BI_PL_ViewService.getViewColumns(selectedView);
		String headerRow = BI_PL_DetailTableCtrl.getHeaderRowFromColumns(columns, this.separator, this.dataEncapsulator);

		Attachment att = new Attachment(Name = 'ExportDetails_' + bc.getJobId() + '.' + this.extension, ParentId = cycleId, Body = Blob.valueOf(headerRow));
		insert att;

		this.attId = att.Id;
		System.debug(loggingLevel.Error, '*** this.query: ' + this.query);
		return Database.getQueryLocator(this.query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		Attachment workingDoc = [SELECT Id, Body FROM Attachment WHERE Id = :attId LIMIT 1];
   		List<BI_PL_ViewService.PLanitColumn> columns = BI_PL_ViewService.getViewColumns(selectedView);

		System.debug(loggingLevel.Error, '*** scope: ' + scope);
		System.debug(loggingLevel.Error, '*** columns: ' + columns);

		for(sObject det : scope) {

			BI_PL_Detail_preparation__c deto = (BI_PL_Detail_preparation__c) det;

			System.debug(loggingLevel.Error, '*** deto.BI_PL_product__r.Name: ' + deto.BI_PL_product__r.Name);
			String newRow = BI_PL_DetailTableCtrl.getDetailRowFromColumns(det, columns, this.separator, this.dataEncapsulator);
			String row = workingDoc.Body.toString() + newRow;
			workingDoc.Body = Blob.valueOf(row);
		}

		update workingDoc;
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug(loggingLevel.Error, '*** finish export!!: ');
		
	}

	
	
}