/**
 *  12-06-2017
 *  @author OMEGA CRM
 */
public without sharing class BI_PL_DetailPreparationHandler
	implements BI_PL_TriggerInterface {

	//private Map<String,Map<String, Integer>> veevaInteractionsByProductAndCycle;
	//private Map<String,DetailWrapper> detailsWrapper;

	// Constructor
	public BI_PL_DetailPreparationHandler() {

	}

	/**
	 * bulkBefore
	 *
	 * This method is called prior to execution of a BEFORE trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkBefore() {
		// Retrieve records required data:
		//if(Trigger.isUpdate){
			//detailsWrapper = getDetailWrappersForVeevaInteractions((List<BI_PL_Detail_preparation__c>)Trigger.new);
			//// Retrieve veeva data based on the trigger records:
			//fillVeevaInteractionsDataMap(detailsWrapper.values());

			//// Update records:
			//for (DetailWrapper detail : detailsWrapper.values())
			//	updateVeevaInterationsDataForDetail(detail);


			//System.debug('veeva Interactions' + veevaInteractionsByProductAndCycle);
			//for(BI_PL_Detail_preparation__c d : (List<BI_PL_Detail_preparation__c>)Trigger.new){
				

			//	String key = createKeyWrapper(d.BI_PL_Channel_detail__c,d.BI_PL_Product__c , d.BI_PL_Secondary_product__c);

			//	System.debug('wrapper' + detailsWrapper.get(key));
			//	System.debug('interactions b' + d.BI_PL_Primary_interactions__c);
			//	d.BI_PL_Primary_interactions__c = detailsWrapper.get(key).record.BI_PL_Primary_interactions__c;
			//	d.BI_PL_Secondary_interactions__c = detailsWrapper.get(key).record.BI_PL_Secondary_interactions__c;
			//	System.debug('interactions a' + d.BI_PL_Primary_interactions__c);
			//}

			
			/*for(DetailWrapper detail : detailsWrapper){
				BI_PL_Detail_preparation__c record = detail.getRecord();
				record.BI_PL_Primary_interactions__c = det
			}*/

			

			// Apply changes to the trigger records:
			/*for (Integer i = 0; i < detailsWrapper.size(); i++) {
				BI_PL_Detail_preparation__c d = ((BI_PL_Detail_preparation__c)Trigger.new.get(i));
				d.BI_PL_Primary_interactions__c = detailsWrapper.get(i).record.BI_PL_Primary_interactions__c;
				d.BI_PL_Secondary_interactions__c = detailsWrapper.get(i).record.BI_PL_Secondary_interactions__c;
			}*/
		//}

	}
	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkAfter() {
	}

	/**
	 * beforeInsert
	 *
	 * This method is called iteratively for each record to be inserted during a BEFORE
	 * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
	 */
	public void beforeInsert(SObject so) {
	}
	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
	 */
	public void beforeUpdate(SObject oldSo, SObject so) {
	}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
	 */
	public void beforeDelete(SObject so) {
	}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
	 */
	public void afterInsert(SObject so) {
	}


	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
	 */
	public void afterUpdate(SObject oldSo, SObject so) {
	}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 */
	public void afterDelete(SObject so) {
	}

	/**
	 * andFinally
	 *
	 * This method is called once all records have been processed by the trigger. Use this
	 * method to accomplish any final operations such as creation or updates of other records.
	 */
	public void andFinally() {
		//if(Trigger.isUpdate){
		//veevaInteractionsByProductAndCycle.clear();
		//}
	}

	//private void updateVeevaInterationsDataForDetail(DetailWrapper detailWrapper) {
	//	Id cycleId = detailWrapper.cycle.Id;

	//	if (detailWrapper.getPrimaryProductId() != null) {
	//		detailWrapper.record.BI_PL_Primary_interactions__c = getVeevaInteractionsForCycleAndProduct(cycleId, detailWrapper.getPrimaryProductName(), detailWrapper.getPositionName(),detailWrapper.getCustomerId());
	//	} else {
	//		detailWrapper.record.BI_PL_Primary_interactions__c = 0;
	//	}

	//	if (detailWrapper.getSecondaryProductId() != null) {
	//		detailWrapper.record.BI_PL_Secondary_interactions__c = getVeevaInteractionsForCycleAndProduct(cycleId, detailWrapper.getSecondaryProductName(), detailWrapper.getPositionName(),detailWrapper.getCustomerId());
	//	} else {
	//		detailWrapper.record.BI_PL_Secondary_interactions__c = 0;
	//	}
	//}

	//private Integer getVeevaInteractionsForCycleAndProduct(String cycleId, String productName, String position, String targetId) {
	//	String key = generateMapKey(position, productName, targetId);
	//	if (!veevaInteractionsByProductAndCycle.containsKey(cycleId))
	//		return 0;
	//	if(!veevaInteractionsByProductAndCycle.get(cycleId).containsKey(key))
	//		return  0;

	//	return veevaInteractionsByProductAndCycle.get(cycleId).get(key);
	//}

	//private String generateMapKey(String position, String productName, String targetId) {
	//	return position + targetId+productName;
	//}

	//private class IterarionsVeevaWrapper {
	//	Map<String, Map<String, Map<String, Integer>>> mapInteractionsPrimary;
	//	Map<String, Map<String, Map<String, Integer>>> mapInteractionsSecondary;

	//	public IterarionsVeevaWrapper(Map<String, Map<String, Map<String, Integer>>> mapInteractionsPrimary, Map<String, Map<String, Map<String, Integer>>> mapInteractionsSecondary) {
	//		this.mapInteractionsPrimary = mapInteractionsPrimary;
	//		this.mapInteractionsSecondary = mapInteractionsSecondary;
	//	}

	//	public Map<String, Map<String, Map<String, Integer>>> getPrimaryIterations() {
	//		return this.mapInteractionsPrimary;
	//	}

	//	public Map<String, Map<String, Map<String, Integer>>> getSecondaryIterations() {
	//		return this.mapInteractionsSecondary;
	//	}
	//}

	//private Map<String,DetailWrapper> getDetailWrappersForVeevaInteractions(List<BI_PL_Detail_preparation__c> details) {
	//	Map<String,DetailWrapper> output = new Map<String,DetailWrapper>();

	//	Set<Id> productsId = new Set<Id>();

	//	if (Trigger.isInsert) {
	//		// If trigger is insert retrieve the related data through the channel detail:
	//		Set<Id> channelDetailsId = new Set<Id>();
	//		for (BI_PL_Detail_preparation__c d : details) {
	//			if (d.Id == null)
	//				channelDetailsId.add(d.BI_PL_Channel_detail__c);

	//			if (d.BI_PL_Product__c != null)
	//				productsId.add(d.BI_PL_Product__c);
	//			if (d.BI_PL_Secondary_product__c != null)
	//				productsId.add(d.BI_PL_Secondary_product__c);
	//		}

	//		Map<Id, Product_vod__c> detailProducts = new Map<Id, Product_vod__c>([SELECT Id, Name FROM Product_vod__c WHERE Id IN :productsId]);

	//		Map<Id, BI_PL_Channel_detail_preparation__c> channelDetails = new Map<Id, BI_PL_Channel_detail_preparation__c>([SELECT Id,
	//		        BI_PL_Target__c,
	//		        BI_PL_Target__r.BI_PL_Target_customer__c,
	//		        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c,
	//		        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
	//		        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c FROM BI_PL_Channel_detail_preparation__c WHERE Id IN :channelDetailsId]);

	//		for (BI_PL_Detail_preparation__c d : details) {
	//			String key = createKeyWrapper(d.BI_PL_Channel_detail__c, d.BI_PL_Product__c, d.BI_PL_Secondary_product__c);
	//			output.put(key,new DetailWrapper(d,
	//			                             channelDetails.get(d.BI_PL_Channel_detail__c).BI_PL_Target__r,
	//			                             detailProducts.get(d.BI_PL_Product__c).Name,
	//			                             (d.BI_PL_Secondary_product__c != null) ? detailProducts.get(d.BI_PL_Secondary_product__c).Name : null));
	//		}
	//	} else if (Trigger.isUpdate) {
	//		// If trigger is update retrieve the data directly from the existing detail records:

	//		Set<Id> detailsId = new Set<Id>();
	//		for (BI_PL_Detail_preparation__c d : details) {
	//			detailsId.add(d.Id);
	//		}

	//		for (BI_PL_Detail_preparation__c d : [SELECT Id,
	//		                                      BI_PL_Product__c,
	//		                                      BI_PL_Product__r.Name,
	//		                                      BI_PL_Secondary_product__c,
	//		                                      BI_PL_Secondary_product__r.Name,

	//		                                      BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,
	//		                                      BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c,
	//		                                      BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
	//		                                      BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c
	//		                                      FROM BI_PL_Detail_preparation__c WHERE Id IN: detailsId]) {
	//			DetailWrapper aux = new DetailWrapper(d);
	//			String key = createKeyWrapper(d.BI_PL_Channel_detail__c, d.BI_PL_Product__c, d.BI_PL_Secondary_product__c);
	//			output.put(key,aux);
	//		}

	//	}

	//	return output;
	//}

	//private void fillVeevaInteractionsDataMap(List<DetailWrapper> details) {
	//	veevaInteractionsByProductAndCycle = new Map<String,Map<String, Integer>>();

	//	Set<String> uniqueAccounts = new Set<String>();
	//	Set<String> uniqueTerritories = new Set<String>();
	//	Set<String> primaryProducts = new Set<String>();
	//	Set<String> secondaryProducts = new Set<String>();

	//	Map<Id, BI_PL_Cycle__c> cyclesMap = new Map<Id, BI_PL_Cycle__c>();

	//	Date minDate;
	//	Date maxDate;

	//	for (DetailWrapper detail : details) {
	//		if (detail.getCustomerId() != null)
	//			uniqueAccounts.add(detail.getCustomerId());

	//		if (detail.getPositionName() != null)
	//			uniqueTerritories.add(detail.getPositionName());

	//		if (detail.getPrimaryProductId() != null)
	//			primaryProducts.add(detail.getPrimaryProductName());
	//		if (detail.getSecondaryProductId() != null)
	//			secondaryProducts.add(detail.getSecondaryProductName());

	//		// Min start date:
	//		Date startDate = detail.cycle.BI_PL_Start_date__c;
	//		if (minDate > startDate || minDate == null)
	//			minDate = startDate;

	//		// Max end date:
	//		Date endDate = detail.cycle.BI_PL_End_date__c;
	//		if (maxDate < endDate || maxDate == null)
	//			maxDate = endDate;

	//		cyclesMap.put(detail.cycle.Id, detail.cycle);

	//	}

	//	System.debug('call' + minDate +maxDate+ uniqueAccounts+uniqueTerritories+primaryProducts);
	//	// Retrieve call2 records between the minimum and maximum dates found in detail records:
	//	for (Call2_vod__c callRecord : [SELECT  Account_vod__c, CP1__c, CP2__c, Territory_vod__c, Call_Date_vod__c
	//	                                FROM Call2_vod__c
	//	                                WHERE Call_Date_vod__c >= :minDate
	//	                                AND Call_Date_vod__c <= :maxDate
	//	                                AND Status_vod__c = 'Submitted_vod'
	//	                                        AND Account_vod__c IN :uniqueAccounts
	//	                                        AND Territory_vod__c IN :uniqueTerritories
	//	                                        AND (CP1__c IN :primaryProducts OR CP2__c IN :secondaryProducts)]) {


	//		System.debug('callRecord '+ callRecord);
	//		// Get affected cycles:
	//		List<Id> involvedCyclesId = new List<Id>();
	//		for (BI_PL_Cycle__c cycle : cyclesMap.values()) {
	//			if (cycle.BI_PL_Start_date__c < callRecord.Call_Date_vod__c && cycle.BI_PL_End_date__c > callRecord.Call_Date_vod__c) {
	//				involvedCyclesId.add(cycle.Id);
	//			}
	//		}

	//		// For each cycle and product increase the number of interactions:
	//		for (Id cycleId : involvedCyclesId) {
	//			increaseInteractionsForCycleAndProduct(cycleId, callRecord.CP1__c, callRecord.Territory_vod__c, callRecord.Account_vod__c);
	//			increaseInteractionsForCycleAndProduct(cycleId, callRecord.CP2__c, callRecord.Territory_vod__c, callRecord.Account_vod__c);
	//		}
	//	}
	//}

	//private void increaseInteractionsForCycleAndProduct(Id cycleId, String productId, String position, String targetId) {

	//	if (productId != null) {
	//		String key = generateMapKey(position, productId, targetId);
	//		if (!veevaInteractionsByProductAndCycle.containsKey(cycleId)){
				
	//			veevaInteractionsByProductAndCycle.put(cycleId, new Map<String, Integer>());
	//			veevaInteractionsByProductAndCycle.get(cycleId).put(key, 1);
	//		}else if (!veevaInteractionsByProductAndCycle.get(cycleId).containsKey(key)){
	//			// Increase the number of interactions for the cycle and product:
	//			veevaInteractionsByProductAndCycle.get(cycleId).put(key, 1);
	//		}else{
	//			veevaInteractionsByProductAndCycle.get(cycleId).put(key,veevaInteractionsByProductAndCycle.get(cycleId).get(key)+1);
	//		}

			
	//	}
	//}

	//private String createKeyWrapper(String channel, String productId , String secondaryId){

	//	String key = channel+productId;
	//	if(secondaryId != null)
	//		key += secondaryId;

	//	return key;

	//}

	//private class DetailWrapper {

	//	public BI_PL_Detail_preparation__c record;
	//	public BI_PL_Cycle__c cycle;
	//	private String positionName;
	//	private String targetCustomer;

	//	private String primaryProductName;
	//	private String secondaryProductName;

	//	public DetailWrapper(BI_PL_Detail_preparation__c record) {
	//		this.record = record;
	//		this.positionName = record.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c;
	//		this.targetCustomer = record.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c;
	//		this.cycle = record.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r;
	//		this.primaryProductName = record.BI_PL_Product__r.Name;
	//		this.secondaryProductName = record.BI_PL_Secondary_product__r.Name;
	//	}
	//	public DetailWrapper(BI_PL_Detail_preparation__c record, BI_PL_Target_preparation__c target, String primaryProductName, String secondaryProductName) {
	//		this.record = record;
	//		this.positionName = target.BI_PL_Header__r.BI_PL_Position_name__c;
	//		this.targetCustomer = target.BI_PL_Target_customer__c;
	//		this.cycle = target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r;

	//		this.primaryProductName = primaryProductName;
	//		this.secondaryProductName = secondaryProductName;
	//	}

	//	public String getPositionName() {
	//		return this.positionName;
	//	}
	//	public String getPrimaryProductId() {
	//		return BI_PL_DetailPreparationHandler.getShortId(record.BI_PL_Product__c);
	//	}
	//	public String getSecondaryProductId() {
	//		return BI_PL_DetailPreparationHandler.getShortId(record.BI_PL_Secondary_product__c);
	//	}
	//	public String getPrimaryProductName() {
	//		if (this.primaryProductName != null)
	//			return this.primaryProductName;
	//		if (record.BI_PL_Product__c != null)
	//			return record.BI_PL_Product__r.Name;
	//		return null;
	//	}
	//	public String getSecondaryProductName() {
	//		if (this.secondaryProductName != null)
	//			return this.secondaryProductName;
	//		if (record.BI_PL_Secondary_product__c != null)
	//			return record.BI_PL_Secondary_product__r.Name;
	//		return null;
	//	}
	//	public Id getCustomerId() {
	//		return targetCustomer;
	//	}

	//	public BI_PL_Detail_preparation__c getRecord() {
	//		return this.record;
	//	}
	//}

	//public static String getShortId(Id i) {
	//	if (i == null)
	//		return null;
	//	return ((String)i).substring(0, 15);
	//}

}