public without sharing class BI_PL_DetailTableCtrl {

	public static final String COLUMN_BRAND_ADOPTION_DECILE = 'Brand Adoption Decile';
    public static final String COLUMN_NET_SALES_TRX = 'Net Sales Per TR';
    public static final String COLUMN_PRODUCT_NET_SALES = 'Product Net Sales';
    public static final String COLUMN_MARKET_NET_SALES = 'Market net sales';
    public static final String COLUMN_MARKET_DECILE = 'Market Decile';
    public static final String COLUMN_AGG_ACCESS = 'Aggregate Access';
    public static final String COLUMN_AGG_ACCESS_FUTURE = 'Aggregate Access Future';

	public BI_PL_DetailTableCtrl() {
		
	}

    /**
     * Inner class used to use WITH SHARING when querying details
     */
    public with sharing class BI_PL_DetailTableCtrlWithSharing {
        public BI_PL_DetailTableCtrlWithSharing(){}
        public BI_COMMON_VueforceController.RemoteResponse getDetailsPageWithSharingInner(String fieldSet, String cycleId, String hierarchyName, String channel, String singlePosition, String planId, Integer pageSize, String lastIdOffset, Map<String, String> ordenation, Boolean isNextPage) {
            System.debug(loggingLevel.ERROR, cycleId);
            System.debug(loggingLevel.ERROR, hierarchyName);
            System.debug(loggingLevel.ERROR, channel);
            System.debug(loggingLevel.ERROR, singlePosition);
            System.debug(loggingLevel.ERROR, planId);
            
            //1. query a ciclo
            //1. sacar todos los detalles para el filtro dado (paginados). oofset va a ser de details
            //2. para los accounts encontraods, buscar sus Call2_vod__c en las fechas (ciclo) dadas
            String userCountryCode = BI_PL_PreparationServiceUtility.getCurrentUser().Country_Code_BI__c;
            BI_PL_Cycle__c cycle = [SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE Id = :cycleId];
            BI_PL_View__c selectedView = BI_PL_ViewService.getDetailsView(null, userCountryCode, fieldSet);

            String firstDetailId = null;
            String lastDetailId = null;
            //Set<String> pageAccountIds = new Set<String>();
            //Map<String, PlanitDetailRow> rowMap = new Map<String, PlanitDetailRow>();
            Map<String, BI_PL_Detail_preparation__c> rowMap = new Map<String, BI_PL_Detail_preparation__c>();

            //String customFilter = ' AND BI_PL_Channel_detail__r.BI_PL_Removed__c = false ';
            String customFilter = ' ';
            String locator = selectedView.BI_PL_Query_locator__c != '' ? selectedView.BI_PL_Query_locator__c : BI_PL_PreparationServiceUtility.getFullSelectDetailQuery();

            Integer totalInQuery = Database.countQuery(BI_PL_PreparationServiceUtility.getCountDetailsQuery(locator, customFilter, cycleId, hierarchyName, channel, singlePosition, planId));

            //String query = BI_PL_DetailTableCtrl.getBaseQueryForFieldSet(fieldSet);
            String paginatedQuery = BI_PL_PreparationServiceUtility.getPaginatedDetailsQueryWithDirection(locator, customFilter, true, cycleId, hierarchyName, channel, singlePosition, planId, pageSize, lastIdOffset, ordenation, isNextPage);

            System.debug(loggingLevel.ERROR, paginatedQuery);
            //throw new BI_PL_Exception(paginatedQuery);
            //Fisrts fill map with existing details
            for(BI_PL_Detail_preparation__c det : (List<BI_PL_Detail_preparation__c>) Database.query(paginatedQuery)){
                
                //PlanitDetailRow row = new PlanitDetailRow(det);
                //PlanitDetailRow row = BI_PL_DetailTableCtrl.getRowInstanceForFieldSet(fieldSet, det);
                //rowMap.put(row.key(), row);
                rowMap.put(det.Id, det);
                //pageAccountIds.add(det.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c);
                
                if(isNextPage){
                    if(firstDetailId==null){
                        firstDetailId = det.Id;
                    }
                    lastDetailId = det.Id;
                }else{
                    if(lastDetailId==null){
                        lastDetailId = det.Id;
                    }
                    firstDetailId = det.Id;
                }
            }

            //Planit Versus VV - Then find interactions in Veeva for accounts found in page and sum into the models
            //if(fieldSet == 'PVV'){
            //    for(Call2_vod__c c: [SELECT Name, Id, Account_vod__c, Account_vod__r.Name, CP1__c, CP2__c, Territory_vod__c 
            //        FROM Call2_vod__c 
            //        WHERE Call_Date_vod__c >= :cycle.BI_PL_Start_date__c
            //        AND Call_Date_vod__c <= :cycle.BI_PL_End_date__c 
            //        AND Status_vod__c = 'Submitted_vod' 
            //        AND Account_vod__c IN :pageAccountIds ]){

            //        String vkey = BI_PL_DetailTableCtrl.keyFromVeevaInteraction(c);
            //        System.debug(loggingLevel.Error, '*** vkey: ' + vkey);

            //        if(rowMap.containsKey(vkey)){
            //            rowMap.get(vkey).addVeevaInteraction();
            //        }else {
            //            //TODO What esle
            //            System.debug(loggingLevel.Error, '*** vkey NOT found in rows: ' + vkey);
            //        }
            //    }
            //}


            //build the final response
            BI_COMMON_VueforceController.RemoteResponse response = new BI_COMMON_VueforceController.RemoteResponse();
            response.records = rowMap.values();
            response.firstIdOffset = firstDetailId;
            response.lastIdOffset = lastDetailId;
            response.totalRecords = totalInQuery;
            response.totalPages = Decimal.valueOf(Double.valueOf(response.totalRecords) / pageSize).round(RoundingMode.CEILING).intValue();
            return response;
        }

        //public String getLastIndexOfPageWithSharing(String fieldSet, String cycleId, String hierarchyName, String channel, String singlePosition, Integer pageSize, String lastIdOffset, Map<String, String> ordenation) {
        //    String lastDetailId = null;

        //    String query = 'SELECT ID FROM BI_PL_Detail_preparation__c ';
        //    String customFilter = ' AND BI_PL_Channel_detail__r.BI_PL_Removed__c = false ';
        //    String paginatedQuery = BI_PL_PreparationServiceUtility.getPaginatedDetailsQuery(query, customFilter, true, cycleId, hierarchyName, channel, singlePosition, pageSize, lastIdOffset, ordenation);
        //    //Fisrts fill map with existing details
        //    List<BI_PL_Detail_preparation__c> detailList = (List<BI_PL_Detail_preparation__c>) Database.query(paginatedQuery);

        //    if(detailList.size()>0){
        //        lastDetailId = detailList.get(detailList.size()-1).Id;
        //    }

        //    return lastDetailId;
        //}
    }

    /**
    * Generates al key based on Veeva Interaction fields (TERRITORY - ACCOUNT - PRODUCT)
    *
    * @param      call  The call
    *
    * @return     The key generated
    */
    //public static String keyFromVeevaInteraction(Call2_vod__c call){
    //    String productkey = call.CP2__c == null ? call.CP1__c : call.CP1__c + ' ' + call.CP2__c;
    //    return call.Territory_vod__c+call.Account_vod__c+productkey;
    //}

    /**
     * Runs detail export batch for the selected view
     *
     * @param      fieldSet       The field set
     * @param      cycleId        The cycle identifier
     * @param      hierarchyName  The hierarchy name
     * @param      channel        The channel
     * @param      positionId     The position identifier
     *
     * @return     The batch process Id
     */
    @ReadOnly
    @RemoteAction
    public static String runDetailExportBatch(String fieldSet, String cycleId, String hierarchyName, String channel, String positionId, String planId) {
        String countryCode = BI_PL_PreparationServiceUtility.getCurrentUser().Country_Code_BI__c;
        String batchId = Database.executeBatch(new BI_PL_DetailExportBatch(fieldSet, countryCode, cycleId, hierarchyName, channel, positionId, planId));
        return batchId;
        //return 'a';
    }


    public class ExportChunkStatus {
        public String attachmentId;
        public String lastOffsetId;
        public Integer rowCount;
        public ExportChunkStatus(String att, String off, Integer c){
            this.attachmentId = att;
            this.lastOffsetId = off;
            this.rowCount = c;
        }
    }

    /**
     * Creates or updates an existing Attachment with the result of the export.
     * Export configuration is retrieved form View record.
     * 
     * Getr the following chunk  from parameter
     *
     * @param      fieldSet        The field set
     * @param      cycleId         The cycle identifier
     * @param      hierarchyName   The hierarchy name
     * @param      channel         The channel
     * @param      singlePosition  The single position
     * @param      pageSize        The page size
     * @param      lastIdOffset    The last identifier offset
     * @param      attId           The att identifier
     *
     * @return     { description_of_the_return_value }
     */
    @RemoteAction
    public static ExportChunkStatus generateExportChunk(String fieldSet, String cycleId, String hierarchyName, String channel, String singlePosition, String planId, Integer pageSize, String lastIdOffset, String attId) {

            String userCountryCode = BI_PL_PreparationServiceUtility.getCurrentUser().Country_Code_BI__c;
            BI_PL_Cycle__c cycle = [SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, Name FROM BI_PL_Cycle__c WHERE Id = :cycleId];

            BI_PL_View__c selectedView = BI_PL_ViewService.getDetailsView(null, userCountryCode, fieldSet);
            List<BI_PL_ViewService.PLanitColumn> columns = BI_PL_ViewService.getViewColumns(selectedView);
            Attachment currentAtt;

            //first call
            if(lastIdOffset == null || attId == null){
                currentAtt = new Attachment(Name = 'ExportRemote_' + Userinfo.getUserId() +'.' + selectedView.BI_PL_Export_file_extension__c, ParentId = cycleId);
                currentAtt.Body = Blob.valueOf(getHeaderRowFromColumns(columns, selectedView.BI_PL_Export_separator__c, selectedView.BI_PL_Export_data_encapsulator__c));
                //insert currentAtt;
            } else {
                currentAtt = [SELECT Id, Name, Body FROM Attachment WHERE Id = :attId LIMIT 1];
            }

            //TODO CHECK Attachment size
            String lastDetailId = null;
            String customFilter = ' AND BI_PL_Channel_detail__r.BI_PL_Removed__c = false ';
            String locator = selectedView.BI_PL_Query_locator__c != '' ? selectedView.BI_PL_Query_locator__c : BI_PL_PreparationServiceUtility.getFullSelectDetailQuery();
            String paginatedQuery = BI_PL_PreparationServiceUtility.getPaginatedDetailsQueryWithDirection(locator, customFilter, true, cycleId, hierarchyName, channel, singlePosition, planId,  pageSize, lastIdOffset, null, true);
            String currentBody = '';
            Integer count = 0;

            for(BI_PL_Detail_preparation__c det : (List<BI_PL_Detail_preparation__c>) Database.query(paginatedQuery)){
                currentBody += getDetailRowFromColumns(det, columns, selectedView.BI_PL_Export_separator__c, selectedView.BI_PL_Export_data_encapsulator__c);
                lastDetailId = det.Id;
                count++;
            }

            currentAtt.Body = Blob.valueOf(currentAtt.Body.toString() + currentBody);
            upsert currentAtt;

            //System.debug(loggingLevel.Error, '*** currentAtt.Id: ' + currentAtt.Id);
            //System.debug(loggingLevel.Error, '*** lastDetailId: ' + lastDetailId);
            //System.debug(loggingLevel.Error, '*** count: ' + count);

            ExportChunkStatus response = new ExportChunkStatus(currentAtt.Id, lastDetailId, count);
            return response;
        }

    /**
     * Gets the view for the given fieldSet.
     *
     * @param      fieldSet  The field set
     *
     * @return     The view.
     */
    @ReadOnly
    @RemoteAction
    public static List<BI_PL_ViewService.PlanitColumn> getView(String fieldSet) {

        String userCountryCode = BI_PL_PreparationServiceUtility.getCurrentUser().Country_Code_BI__c;
        BI_PL_View__c selectedView = BI_PL_ViewService.getDetailsView(null, userCountryCode, fieldSet);
        List<BI_PL_ViewService.PlanitColumn> columns =  BI_PL_ViewService.getViewColumns(selectedView);

        return columns;
    }

    @RemoteAction
        public static Attachment getAttachment(String cycleid, String batchid) {
        String search = '%'+batchid+'%';
        return [SELECT Id, Name FROM Attachment WHERE ParentId = :cycleId AND Name LIKE :search];
    }

    @RemoteAction
    public static String getSalesforceDetailReportURL(String cycleId, String hierarchyName, String channel, String positionId, String planId) {

        BI_PL_Cycle__c cycle = [SELECT Id, Name FROM BI_PL_Cycle__c WHERE Id = :cycleId];

        Report rep;

        if(!Test.isRunningTest()) { rep = [SELECT Id, DeveloperName, Name FROM Report WHERE DeveloperName = 'BI_PL_Cycle_plan_data' ORDER BY CreatedDate DESC LIMIT 1]; }


        String finalurl = '/' + (rep != null ? rep.Id : '00OU0000001cCIQ') + '?pv0=' + cycle.Name + '&pv1=' + hierarchyName + '&pv2=' + channel;

        if(positionId != null){
            finalurl += '&pv2=' + positionId;
        }

        if(planId != null){
            finalurl += '&pn3=eq&pv3=' + planId.substring(0, 15);
        }

        return finalurl;
    }

	@RemoteAction
	@ReadOnly
	public static BI_COMMON_VueforceController.RemoteResponse getDetailsPage(String fieldSet, String cycleId, String hierarchyName, String channel, String singlePosition, String planId, Integer pageSize, String lastIdOffset, Map<String, String> ordenation, Boolean isNextPage) {
       //return new BI_PL_DetailTableCtrlWithSharing().getDetailsPageWithSharing(fieldSet, cycleId, hierarchyName, channel, singlePosition, pageSize, lastIdOffset, ordenation, isNextPage);
	   return getDetailsPageWithSharing(fieldSet, cycleId, hierarchyName, channel, singlePosition, planId, pageSize, lastIdOffset, ordenation, isNextPage);
	}

    public static BI_COMMON_VueforceController.RemoteResponse getDetailsPageWithSharing(String fieldSet, String cycleId, String hierarchyName, String channel, String singlePosition, String planId, Integer pageSize, String lastIdOffset, Map<String, String> ordenation, Boolean isNextPage) {
       return new BI_PL_DetailTableCtrlWithSharing().getDetailsPageWithSharingInner(fieldSet, cycleId, hierarchyName, channel, singlePosition, planId, pageSize, lastIdOffset, ordenation, isNextPage);
    }

    //@RemoteAction
    //@ReadOnly
    //public static String getLastIndexOfPage(String fieldSet, String cycleId, String hierarchyName, String channel, String singlePosition, Integer pageSize, String lastIdOffset, Map<String, String> ordenation) {
    //   return new BI_PL_DetailTableCtrlWithSharing().getLastIndexOfPageWithSharing(fieldSet, cycleId, hierarchyName, channel, singlePosition, pageSize, lastIdOffset, ordenation);
    //}

    public static String getHeaderRowFromColumns(List<BI_PL_ViewService.PLanitColumn> columns, String separator, String dataEncapsulator) {
        String result = '';
        for(BI_PL_ViewService.PLanitColumn col : columns) {
            result += dataEncapsulator + col.label + dataEncapsulator + separator;
        }
        result = result.removeEnd(separator);
        result += '\r\n';

        return result;
    }

    public static String getDetailRowFromColumns(sObject det, List<BI_PL_ViewService.PLanitColumn> columns, String separator, String dataEncapsulator) {
        String result = '';
        for(BI_PL_ViewService.PLanitColumn col : columns) {
            if(col.field.contains('.')) {
                result += dataEncapsulator + getValueFromRelationship(det, col.field) + dataEncapsulator + separator;
            } else {
                result += dataEncapsulator + det.get(col.field) + dataEncapsulator + separator;
            }
        }
        result = result.removeEnd(separator);
        result += '\r\n';

        return result;
    }

    public static String getValueFromRelationship(sObject det, String fieldPath) {
        System.debug(loggingLevel.Error, '*** getValueFromRelationship detail: ' + det);
        System.debug(loggingLevel.Error, '*** getValueFromRelationship fieldPath: ' + fieldPath);
        String result = '';
        sObject aux = det;

        List<String> fieldPathArr = fieldPath.split('\\.');
        System.debug(loggingLevel.Error, '*** fieldPathArr: ' + fieldPathArr);

        for(Integer i = 0; i < fieldPathArr.size() - 1; i++ ){

            aux = aux.getSObject(fieldPathArr[i]);
            
            if(aux == null){
                break;
            }
        }

        if(aux != null){
            result = String.valueOf(aux.get(fieldPathArr[fieldPathArr.size() - 1]));
        } 

        System.debug(loggingLevel.Error, '*** getValueFromRelationship result: ' + result);

        return result;
    }
}