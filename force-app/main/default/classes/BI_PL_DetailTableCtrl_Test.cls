@isTest
private class BI_PL_DetailTableCtrl_Test {

	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

        BI_PL_TestDataFactory.createPlanitViews(userCountryCode);
        //BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);

    }

    @isTest static void testSalesforceReport() {
    	//Test constructor
    	BI_PL_DetailTableCtrl ctrl = new BI_PL_DetailTableCtrl();

		String cycleId = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1].Id;
		String hierarchyName = [SELECT Id, BI_PL_Hierarchy__c FROM BI_PL_Position_Cycle__c WHERE BI_PL_Cycle__c = :cycleId LIMIT 1].BI_PL_Hierarchy__c;
		String positionId = [SELECT Id, BI_PL_Position__c FROM BI_PL_Position_Cycle__c  WHERE BI_PL_Cycle__c = :cycleId AND BI_PL_Hierarchy__c = :hierarchyName  LIMIT 1].BI_PL_Position__c;
		String channel = 'rep_detail_only'; 
		String fieldSet = 'BI_PL_Detail_view_lift';

		String salesforeceReportURL = BI_PL_DetailTableCtrl.getSalesforceDetailReportURL(cycleId, hierarchyName, channel, positionId, null);

	}
	
	@isTest static void testGetInfo() {
		String cycleId = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1].Id;
		String hierarchyName = [SELECT Id, BI_PL_Hierarchy__c FROM BI_PL_Position_Cycle__c WHERE BI_PL_Cycle__c = :cycleId LIMIT 1].BI_PL_Hierarchy__c;
		String channel = 'rep_detail_only'; 
		String fieldSet = 'BI_PL_Detail_view_lift';

		// Implement test code
		List<BI_PL_ViewService.PlanitColumn> columns = BI_PL_DetailTableCtrl.getView(fieldSet);
		BI_COMMON_VueforceController.RemoteResponse resp = BI_PL_DetailTableCtrl.getDetailsPage(fieldSet, cycleId, hierarchyName, channel,  null, null, 10, null, null, true);

		//Paginate results
		sObject lastRec = (sObject) resp.records.get(resp.records.size() - 1);
		String lastId = String.valueOf(lastRec.get('Id'));
		resp = BI_PL_DetailTableCtrl.getDetailsPage(fieldSet, cycleId, hierarchyName, channel,  null, null,  10, lastId, null, true);

		Test.startTest();
		resp = BI_PL_DetailTableCtrl.getDetailsPage(fieldSet, cycleId, hierarchyName, channel,  null, null,  10, lastId, null, false);
		Test.stopTest();

	}


	@isTest static void testExportRemoting() {
		// Implement test code
		String cycleId = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1].Id;
		String hierarchyName = [SELECT Id, BI_PL_Hierarchy__c FROM BI_PL_Position_Cycle__c WHERE BI_PL_Cycle__c = :cycleId LIMIT 1].BI_PL_Hierarchy__c;
		String channel = 'rep_detail_only'; 
		String fieldSet = 'BI_PL_Detail_view_lift';

		// Implement test code
		List<BI_PL_ViewService.PlanitColumn> columns = BI_PL_DetailTableCtrl.getView(fieldSet);
		BI_COMMON_VueforceController.RemoteResponse resp = BI_PL_DetailTableCtrl.getDetailsPage(fieldSet, cycleId, hierarchyName, channel,  null, null, 10, null, null, true);

		Test.startTest();
		BI_PL_DetailTableCtrl.ExportChunkStatus status = BI_PL_DetailTableCtrl.generateExportChunk(fieldSet, cycleId, hierarchyName, channel, null, null,  10, null, null);
		status = BI_PL_DetailTableCtrl.generateExportChunk(fieldSet, cycleId, hierarchyName, channel, null, null, 10, status.lastOffsetId, status.attachmentId);
		Test.stopTest();



	}
	
	@isTest static void testExportBatch() {
		// Implement test code
		String cycleId = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1].Id;
		String hierarchyName = [SELECT Id, BI_PL_Hierarchy__c FROM BI_PL_Position_Cycle__c WHERE BI_PL_Cycle__c = :cycleId LIMIT 1].BI_PL_Hierarchy__c;
		String channel = 'rep_detail_only'; 
		String fieldSet = 'BI_PL_Detail_view_lift';

		// Implement test code
		List<BI_PL_ViewService.PlanitColumn> columns = BI_PL_DetailTableCtrl.getView(fieldSet);
		BI_COMMON_VueforceController.RemoteResponse resp = BI_PL_DetailTableCtrl.getDetailsPage(fieldSet, cycleId, hierarchyName, channel, null,  null, 10, null, null, true);
		Test.startTest();
		String bId = BI_PL_DetailTableCtrl.runDetailExportBatch(fieldSet, cycleId, hierarchyName, channel, null, null);
		Test.stopTest();

		try {
			Attachment att = BI_PL_DetailTableCtrl.getAttachment(cycleId, bId);
		} catch(Exception e) {

		}

	}
	
}