public without sharing class BI_PL_EmailNotificationUtility {

	public static String CYCLE_PLAN_SUBMITED = 'BI_PL_Cycle_Plan_Submited';
	public static String UNSUBMIT_POSITION = 'BI_PL_Unsubmit_Notification';
	public static String TRANSFER_AND_SHARE = 'BI_PL_Transfer_or_Share_Request';
	/**
	 *	Sends an email to each pair (targetId, relatedToId).
	 *	@author OMEGA CRM
	 */
	public static void sendEmail(String templateName, Set<EmailNotificationIdPair> idPairs) {
		System.debug('BI_PL_EmailNotificationUtility sendEmail ' + templateName + ' - ' + idPairs);
		//OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress limit 1];
		EmailTemplate templateId;
		try {
			templateId = [Select id from EmailTemplate where DeveloperName = :templateName];
		} catch (exception e) {
			throw new BI_PL_Exception('The email template \'' + templateName + '\' was not found.');
		}
		List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();

		for (EmailNotificationIdPair pair : idPairs) {
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setTargetObjectId(pair.targetId);
			mail.setTemplateID(templateId.Id);
			mail.setSaveAsActivity(false);
			mail.setWhatId(pair.objectId);
			//mail.setOrgWideEmailAddressId(owa.id);
			allmsg.add(mail);
		}
		System.debug('Messaging.sendEmail ' + allmsg);
		if (allmsg.size() > 0)
			Messaging.sendEmail(allmsg, false);

	}

	public static Set<EmailNotificationIdPair> generateEmailNotificationIdPairs(Map<Id, Id> ids) {
		Set<EmailNotificationIdPair> output = new Set<EmailNotificationIdPair>();

		for (Id key : ids.keySet()) {
			output.add(new EmailNotificationIdPair(key, ids.get(key)));
		}
		return output;
	}

	public class EmailNotificationIdPair {
		public Id targetId;
		public Id objectId;

		public EmailNotificationIdPair(Id targetId, Id objectId) {
			this.targetId = targetId;
			this.objectId = objectId;
		}
	}
	public static Messaging.SingleEmailMessage setPlainHTMLMailWithReplacements(Messaging.SingleEmailMessage email, Map<String, String> replacements) {

		String finalBody = '';
		String finalSubject = '';

		//Are the contents on the template or is custom body
		if (email.getTemplateId() != null) {
			List<EmailTemplate> emailTemplates = new List<EmailTemplate>([SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE Id = :email.getTemplateId() LIMIT 1]);
			if (!emailTemplates.isEmpty()) {
				finalBody = emailTemplates.get(0).HTMLValue;
				finalSubject = emailTemplates.get(0).Subject;
			}
		} else {
			finalBody = email.getHTMLBody();
			finalSubject = email.getSubject();
		}

		if (!String.isBlank(finalBody)) {
			for (String key : replacements.keySet()) {
				finalBody = finalBody.replace(key, replacements.get(key));
				finalSubject = finalSubject.replace(key, replacements.get(key));
			}
		}

		//Sets final body
		email.setTemplateId(null);
		email.setSubject(finalSubject);
		email.setHtmlBody(finalBody);

		return email;
	}

	public static Messaging.SingleEmailMessage composeEmail(String templateName, String orgAddress, List<String> toAddresses) {
		//mail to be returned
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		List<EmailTemplate> emailTemplates = new List<EmailTemplate>([SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE DeveloperName = :templateName LIMIT 1]);
		List<OrgWideEmailAddress> orgAddr = new List<OrgWideEmailAddress>([SELECT Id, Address FROM OrgWideEmailAddress WHERE Address = : orgAddress OR Id = :orgAddress]);

		//Compose the email template
		if (!emailTemplates.isEmpty()) {
			mail.setTemplateId(emailTemplates.get(0).Id);
		}

		//Use address
		if (!orgAddr.isEmpty()) {
			mail.setOrgWideEmailAddressId(orgAddr.get(0).Id);
		}

		if (toAddresses != null && !toAddresses.isEmpty()) {
			mail.setToAddresses(toAddresses);
		}

		return mail;
	}
}