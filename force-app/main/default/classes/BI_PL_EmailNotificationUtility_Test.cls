@isTest
private class BI_PL_EmailNotificationUtility_Test {
	
	@isTest static void test_method_one() {
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();
		String template = 'BI_PL_Unsubmit_Notification';
		User testUser = [SELECT Id, Country_Code_BI__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
		String countryCode = testUser.Country_Code_BI__c;
		List<User> idList = BI_PL_TestDataFactory.createtestusers(2, countryCode);
		map<Id, Id> idMap = new map<Id, Id>();
		idMap.put(idList.get(0).Id, idList.get(1).Id);
		Set<BI_PL_EmailNotificationUtility.EmailNotificationIdPair> emailPairSet = BI_PL_EmailNotificationUtility.generateEmailNotificationIdPairs(idMap);
		BI_PL_EmailNotificationUtility.sendEmail(template, emailPairSet);

		try{
			BI_PL_EmailNotificationUtility.sendEmail('', emailPairSet);
		}catch(Exception e){}

		
		List<String> toAdresses = New List<String> {'a','b','c'};
		Messaging.SingleEmailMessage testEmail = BI_PL_EmailNotificationUtility.composeEmail(template,TestUser.Id,toAdresses);

		Map<string,string> replacements = new Map<String, String>{'a' => 'b', 'c' => 'd'};

		BI_PL_EmailNotificationUtility.setPlainHTMLMailWithReplacements(testEmail,replacements);

		Messaging.SingleEmailMessage testEmailnull = BI_PL_EmailNotificationUtility.composeEmail('',TestUser.Id,toAdresses);
		BI_PL_EmailNotificationUtility.setPlainHTMLMailWithReplacements(testEmailnull,replacements);
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}