/**
 * 28/07/2017
 *  - New method to allow email body customization from APEX.
 *
 * @author OMEGA CRM
*/
public class BI_PL_EmailUtility {
    public static void sendEmailWithTemplate(String templateName, list<String> addresses) {
        sendEmailWithTemplate(null, templateName, addresses);
    }

    public static void sendEmailWithTemplate(list<String> addresses, String subject, String body) {
        Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();

        emailToSend.setToAddresses(addresses);
        emailToSend.setHTMLBody(body);
        emailToSend.setSubject(subject);

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {emailToSend});
    }

    public static void sendEmailWithTemplate(Id whatID, String templateName, list<String> addresses) {
        EmailTemplate eT = [select Id from EmailTemplate where Name = : templateName];

        Contact cnt = [select id
                       from Contact
                       where email != null limit 1];
        System.debug('contact-->' + cnt);
        List<Messaging.SingleEmailMessage> msgList = new List<Messaging.SingleEmailMessage>();

        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setTemplateId(eT.Id);
        msg.setSaveAsActivity(false);
        msg.setWhatId(whatID);
        msg.setTargetObjectId(cnt.id);
        msg.setToAddresses(addresses);

        msgList.add(msg);

        system.debug('#####msglist: ' + msgList);
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(msgList);
        Database.rollback(sp);

        List<Messaging.SingleEmailMessage> msgListToBeSend = new List<Messaging.SingleEmailMessage>();

        for (Messaging.SingleEmailMessage email : msgList) {
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            emailToSend.setToAddresses(email.getToAddresses());
            emailToSend.setPlainTextBody(email.getPlainTextBody());
            emailToSend.setHTMLBody(email.getHTMLBody());
            emailToSend.setSubject(email.getSubject());
            msgListToBeSend.add(emailToSend);
        }
        system.debug('#######Message to be send: ' + msgListToBeSend);
        Messaging.sendEmail(msgListToBeSend);
    }

}