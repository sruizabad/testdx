@isTest
private class BI_PL_EmailUtility_Test {

	@testSetup static void Setup(){
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createTestAccounts(1, 'US');
		Account acc = BI_PL_TestDataFactory.listAcc.get(0);
	} 
	
	@isTest static void test_method_one() {

		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs ( thisUser ) {
			Account acc = [SELECT Id FROM Account Limit 1];
			
			Contact c = new Contact(LastName = 'testContact', Email = 'testContact@test.test');
        	insert c;

	        List<String> listString = new List<String>();
	        String test1 = 'testContact@test.test';
	        listString.add(test1);
	        String subject = 'subject';
	        String body = 'body';

	        EmailTemplate e = new EmailTemplate(developerName = 'test', TemplateType = 'Text', Name  = 'testTemplate', FolderId = UserInfo.getUserId(), isActive = true);
        	insert e;

       		BI_PL_EmailUtility.sendEmailWithTemplate('testTemplate', listString);
        	BI_PL_EmailUtility.sendEmailWithTemplate(listString, subject, body);

		}
	}
	
}