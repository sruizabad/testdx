public without sharing class BI_PL_ErrorHandlerUtility {

	public static final String PLANIT = 'PLANiT';

	public static final String FUNCTIONALITY_TRANSFER_AND_SHARE = 'Transfer and Share';
	public static final String FUNCTIONALITY_SPLIT_CYCLE = 'Split Cycle Batch';
	public static final String FUNCTIONALITY_AGGBATCH = 'AGG Night Batch';

	private static final Integer LONG_TEXT_SIZE = 32768;
	private static final Integer NAME_FUNCTIONALTY_LENGTH = 255;


	@RemoteAction
	public static void addPlanitVueError(String vueComponent, String functionality, String error, String trace) {
		/*insert new BI_COMMON_Error__c(BI_COMMON_Application__c = PLANIT,
		                              BI_COMMON_Error_message__c = error,
		                              BI_COMMON_Trace__c = trace,
		                              BI_COMMON_Component_name__c = vueComponent,
		                              BI_COMMON_Functionality__c = functionality,
		                              BI_COMMON_Server_side__c = false,
		                              BI_COMMON_User__c = UserInfo.getUserId());*/

		saveError(error, trace, vueComponent, functionality, false);
	}
	@RemoteAction
	public static void addPlanitServerError(String apexClass, String functionality, String error, String trace) {
		/*insert new BI_COMMON_Error__c(BI_COMMON_Application__c = PLANIT,
		                              BI_COMMON_Error_message__c = error,
		                              BI_COMMON_Trace__c = trace,
		                              BI_COMMON_Component_name__c = apexClass,
		                              BI_COMMON_Functionality__c = functionality,
		                              BI_COMMON_Server_side__c = true,
		                              BI_COMMON_User__c = UserInfo.getUserId());*/

		saveError(error, trace, apexClass, functionality, true);
	}


	public static void saveError(String errorMessage, String trace, String componentName, String functionality, Boolean serverSide){
		insert new BI_COMMON_Error__c(BI_COMMON_Application__c = PLANIT,
		                              BI_COMMON_Error_message__c = truncateString(errorMessage, LONG_TEXT_SIZE),
		                              BI_COMMON_Trace__c = truncateString(trace, LONG_TEXT_SIZE),
		                              BI_COMMON_Component_name__c = truncateString(componentName, NAME_FUNCTIONALTY_LENGTH),
		                              BI_COMMON_Functionality__c = truncateString(functionality, NAME_FUNCTIONALTY_LENGTH),
		
		                              BI_COMMON_Server_side__c = serverSide,
		                              BI_COMMON_User__c = UserInfo.getUserId());
	}

	private static String truncateString(String value, Integer length) {
		if (value.length() > length)
			return value.substring(0, length);
		return value;
	}

}