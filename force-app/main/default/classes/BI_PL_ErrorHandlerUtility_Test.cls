@isTest
private class BI_PL_ErrorHandlerUtility_Test {
	
	@isTest static void testClientError() {
		// Implement test code
		Test.startTest();
		BI_PL_ErrorHandlerUtility.addPlanitVueError('<vf-table>', 'Add new target', 'Invalid porperty "test" of "undefined"','test stack trace');
		Test.stopTest();
		System.assertEquals(1, [SELECT Id FROM BI_COMMON_Error__c].size());
	}
	
	@isTest static void testServerError() {
		Test.startTest();
		String longDesc = 'Add new target with a veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery  veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery  veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery  veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery  veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery veeeeeeeeeeeeeeery loing desc';
		BI_PL_ErrorHandlerUtility.addPlanitServerError('BI_PL_PreparationExt', longDesc, 'Invalid porperty "test" of "undefined"',' test stack trace');
		Test.stopTest();
		System.assertEquals(1, [SELECT Id FROM BI_COMMON_Error__c].size());
	}
	
}