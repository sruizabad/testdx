global class BI_PL_ExportProdMetricsBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {
	
    private static final String SFILENAME = 'ProductMetricsToDelete_';

	Set<String> accountProductIds = new Set<String>();
	List<String> accountIds;
	List<String> productIds;

	Integer nToDelete;
    String cycle;
    String cycleName;
	String extension;
	
    String attId;
    Set<String> setAllAtt = new Set<String>();

	global BI_PL_ExportProdMetricsBatch() {
		nToDelete = 0;
	}
	
	global BI_PL_ExportProdMetricsBatch(String cycle, List<String> accountIds, List<String> productIds, Set<String> accountProduct) {
		extension = 'csv';
		this.cycle = cycle;
		this.cycleName = [SELECT Id, Name FROM BI_PL_Cycle__c WHERE Id = :cycle].Name;
		this.accountIds = accountIds;
		this.productIds = productIds;
		this.accountProductIds = accountProduct;
		nToDelete = 0;

	}
	
	global Database.QueryLocator start(Database.BatchableContext bc) {

		Attachment att = new Attachment(Name = SFILENAME + this.cycleName + '_' + Datetime.now().format('YYYYMMDD') + '.' + this.extension, ParentId = this.cycle, Body = Blob.valueOf(getDocHeaderRow()+'\r\n'));
		insert att;

		this.attId = att.Id;
		setAllAtt.add(att.Id);

        String query = 'SELECT Id, Account_vod__c, Account_vod__r.Name, Products_vod__c, Products_vod__r.Name, Segmentation_BI__c, Strategic_Segment_BI__c FROM Product_Metrics_vod__c WHERE Account_vod__c in :accountIds';
		
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

        String row, newRow = '';
   		Boolean bHeapSize = false;
   		List<Attachment> lstAttach = new List<Attachment>();

   		/*List<String> scopeProductIds = new List<String>();
   		for(Product_Metrics_vod__c pMetric: (List<Product_Metrics_vod__c>)scope){
   			scopeProductIds.add(pMetric.Product_vod__c);
   		}

   		List<Product_Metrics_vod__c> filteredScope = [SELECT Id, Account_vod__c, Account_vod__r.Name, Products_vod__c, Products_vod__r.Name, Segmentation_BI__c, Strategic_Segment_BI__c FROM Product_Metrics_vod__c WHERE Products_vod__c in :scopeProductIds];*/

   		//Load attachment where export file is saving
        Attachment workingDoc = [SELECT Id, Body FROM Attachment WHERE Id = :attId LIMIT 1];
        lstAttach.add(workingDoc);

   		Map<String, Product_Metrics_vod__c> mapProductMetrics = new Map<String, Product_Metrics_vod__c>(); 

   		//for(Product_Metrics_vod__c pm: filteredScope){
   		Integer ind = 1;
   		for(Product_Metrics_vod__c pm: (List<Product_Metrics_vod__c>)scope){

   			if(accountProductIds.contains(''+pm.Account_vod__c+pm.Products_vod__c)){

   				nToDelete++;
	   			newRow += '"""' + pm.Id + '"","""' + pm.Account_vod__c + '","' + pm.Account_vod__r.Name + '","' + pm.Products_vod__c + '","' + pm.Products_vod__r.Name + '","' + pm.Segmentation_BI__c + '","' + pm.Strategic_Segment_BI__c + '"\r\n';

		        if(((Limits.getHeapSize() + 1000000) > Limits.getLimitHeapSize() && !bHeapSize) || Test.isRunningTest()){

					bHeapSize = true;
	                row = workingDoc.Body.toString() + newRow;
	                workingDoc.Body = Blob.valueOf(row);
	                newRow = '';

	   				//Create new Attachment
                	workingDoc = new Attachment(Name = SFILENAME + this.cycleName + '_' + Datetime.now().format('YYYYMMDD') + '_' + ind + '.' + this.extension, ParentId = this.cycle, Body = Blob.valueOf(getDocHeaderRow()+'\r\n'));
                	ind++;
                	lstAttach.add(workingDoc);


	   			}

   			}
   		}

   		if (String.isNotBlank(newRow)){
            row = workingDoc.Body.toString() + newRow;
            workingDoc.Body = Blob.valueOf(row);
        }

        upsert lstAttach;

        for (Attachment inAtt : lstAttach){
            setAllAtt.add(inAtt.Id);
        }

        this.attId = workingDoc.Id;
	   		
	}
	
	global void finish(Database.BatchableContext BC) {

		System.debug(loggingLevel.Error,'*** finish export!!: ');

		String links = '';
        for (String sAtt : setAllAtt){
            links += System.URL.getSalesforceBaseURL().toExternalForm() + '/' + sAtt +'\r\n';
        }

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
       	String[] toAddresses = new String[] {UserInfo.getUserEmail()};
        mail.setToAddresses(toAddresses);  
        mail.setSubject('PLANiT | Product Metrics');  
        mail.setPlainTextBody('Product Metrics records to delete:  ' + nToDelete);  
	    mail.setPlainTextBody('The product metrics\' export has finished correctly, you can download it here: ' + links);  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		
	}

	private String getDocHeaderRow(){
		return'"""Id"","""Account Id","Account Name","Product Id","Product Name","Segment","Strategic Segment"';
	}


}