@isTest
public with sharing class BI_PL_ExportProdMetricsBatch_Test {

    @testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        Date startDate = Date.today()-60;
        Date endDate =  Date.today()-30;
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);

    }

    @isTest
    public static void test() {

        String[] hierarchy = new String[]{'hierarchyTest'};
        String channel = 'rep_detail_only';

        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        
        List<BI_PL_Cycle__c> cycles = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c ORDER BY BI_PL_Start_date__c ASC];

        
        List<String> accountsId = new List<String>();
        for(Account acc: [SELECT Id FROM Account]){
            accountsId.add(acc.Id);
        }

        List<String> productsId = new List<String>();
        for(Product_vod__c prod: [SELECT Id FROM Product_vod__c]){
            productsId.add(prod.Id);
        }

        Set<String> accountproducts = new Set<String>();
        for(String prod: productsId){
            for(String acc : accountsId){
                accountproducts.add(''+acc+prod);
            }
        }


        Test.startTest();

        Map<String, Object> params = new Map<String, Object>();
        params.put('lastCycle', cycles[0].Id);
        params.put('currentCycle', cycles[1].Id);

        BI_PL_ProductMetricsMinusBatch pmmb = new BI_PL_ProductMetricsMinusBatch();
        pmmb.init(userCountryCode,new List<String>{cycles[0].Id,cycles[1].Id},new List<String>(),new List<String>(),params);
        Database.executeBatch(pmmb);

        Database.executeBatch(new BI_PL_ProductMetricsMinusBatch(cycles[0].Id,cycles[1].Id));        

        Test.stopTest();

    }

}