global class BI_PL_FillChildCyclesBatch implements Database.Batchable<sObject>, Database.Stateful {


    global Id cycleId;

    global boolean cyclesCreated;
    global List<BI_PL_Cycle__c> cycles;
    global Map<Integer, List<Integer>> frequencies;
    global Map<Id, List<BI_PL_Position_cycle__c>> positionsCycles; //Key Cycle Id
    static private Integer MONTHSTOSPLIT = 12;
    global List<Database.Error> error;
    global Set<String> mapErrors;
    global String cycleName;
    global List<String> hierarchies;
    global String productId;


    public BI_PL_FillChildCyclesBatch (Id cycle, Map<Integer, List<Integer>> freq, List<BI_PL_Cycle__c> cycles, Map<Id, List<BI_PL_Position_cycle__c>> positionsCycles, String name, Set<String> hierarch, String pId) {

        this.cycleId = cycle;
        this.cyclesCreated = false;
        this.cycles = cycles;
        this.frequencies = freq;
        this.positionsCycles = positionsCycles;
        this.mapErrors = new set<String>();
        this.cycleName = name;
        this.hierarchies = new List<String>(hierarch);
        this.productId = pId;


    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('### - BI_PL_SplitCycleBatch - start');
        System.debug('### - Positions Cycle ' + this.positionsCycles);
        System.debug('### - Cyclee ' + this.cycleId);
        System.debug('### - Cycles ' + this.cycles);
        System.debug('### - frequencies ' + this.frequencies);
        return Database.getQueryLocator([
                                            SELECT Id,
                                            BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__c,
                                            BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name,
                                            BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_External_id__c,
                                            BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_cycle_Start_date__c,
                                            BI_PL_Header__r.BI_PL_Country_code__c,
                                            BI_PL_Header__r.BI_PL_Start_date__c,
                                            BI_PL_Header__r.BI_PL_End_date__c,
                                            BI_PL_Header__r.BI_PL_Status__c,
                                            BI_PL_Header__r.Name,

                                            //BI_PL_Header__r.BI_PL_Global_workload__c,
                                            //BI_PL_Header__r.BI_PL_Capacity__c,
                                            //BI_PL_Header__r.BI_PL_Workload_adjustment__c,
                                            //BI_PL_Header__r.BI_PL_Workload_approved__c,
                                            //BI_PL_Header__r.BI_PL_Global_workload__c,

                                            BI_PL_Header__r.BI_PL_Global_capacity__c,
                                            BI_PL_Header__r.BI_PL_Rep_capacity__c,
                                            BI_PL_Header__r.BI_PL_Capacity_adjustment__c,
                                            BI_PL_Header__r.BI_PL_Capacity_adjustment_approved__c,
                                            BI_PL_Header__r.BI_PL_Final_adjusted_capacity__c,
                                            BI_PL_Header__r.BI_PL_External_id__c,

                                            BI_PL_Header__r.BI_PL_Capacity__c,
                                            BI_PL_Header__r.BI_PL_Position_cycle__c,
                                            BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                                            BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Parent_position__c,
                                            BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Parent_position__r.Name,
                                            BI_PL_Header__c,
                                            BI_PL_Added_manually__c,
                                            BI_PL_Added_reason__c,
                                            BI_PL_Ntl_value__c,
                                            BI_PL_Removed__c,
                                            BI_PL_Removed_reason__c,
                                            BI_PL_Target_customer__r.External_ID_vod__c,
                                            BI_PL_Target_customer__c,
                                            BI_PL_External_id__c,
                                            BI_PL_Specialty__c,
                                            BI_PL_next_best_Account__c,
                                            BI_PL_No_see_list__c

                                            FROM BI_PL_Target_preparation__c
                                            WHERE BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId

                                        ]);
    }

    global void execute(Database.BatchableContext BC, List<BI_PL_Target_preparation__c> originalTargets) {
        System.debug('### - BI_PL_FillChildCyclesBatch - execute');
        try {
            Set<Id> targetIDs = new Set<Id>();
            for (BI_PL_Target_preparation__c t : originalTargets)
                targetIDs.add(t.Id);

            System.debug('***originalTargets size ' + originalTargets.size());

            Map<Id, BI_PL_Channel_detail_preparation__c> originalChannelDetails = new Map<Id, BI_PL_Channel_detail_preparation__c>([
                        SELECT Id,
                        BI_PL_Target__c,
                        BI_PL_Channel__c,
                        BI_PL_Max_adjusted_interactions_input__c,
                        BI_PL_Reviewed__c,
                        BI_PL_Rejected__c,
                        BI_PL_Rejected_Reason__c,
                        BI_PL_Removed__c,
                        BI_PL_Removed_reason__c,
                        BI_PL_Removed_date__c,
                        BI_PL_External_id__c,
                        BI_PL_Edited__c,
                        BI_PL_Mccp_channel_criteria__c,
                        BI_PL_MSL_flag__c,
                        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c,
                        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c,
                        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name,
                        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_External_id__c,

                        BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c,
                        BI_PL_Target__r.BI_PL_Target_customer__c,
                        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Capacity__c,
                        BI_PL_Target__r.BI_PL_Header__r.BI_PL_External_id__c,
                        BI_PL_Target__r.BI_PL_Header__r.BI_PL_Start_date__c,
                        BI_PL_Target__r.BI_PL_Header__c
                        FROM BI_PL_Channel_detail_preparation__c
                        WHERE BI_PL_Target__c IN:targetIDs
                    ]);

            /*Map<Id, BI_PL_Channel_detail_preparation__c> originalChannelDetailsByTargetId = new Map<Id, BI_PL_Channel_detail_preparation__c>();

            for (BI_PL_Channel_detail_preparation__c c : originalChannelDetails.values())
                originalChannelDetailsByTargetId.put(c.BI_PL_Target__c, c);*/


            Map<Id, BI_PL_Detail_preparation__c> originalDetails = new Map<Id, BI_PL_Detail_preparation__c>([
                        SELECT Id,
                        BI_PL_Channel_detail__c,
                        BI_PL_Planned_details__c,
                        BI_PL_Adjusted_details__c,
                        BI_PL_Channel_detail__r.BI_PL_Edited__c,
                        BI_PL_Channel_detail__r.BI_PL_Reviewed__c,
                        BI_PL_Channel_detail__r.BI_PL_Rejected__c,
                        BI_PL_Channel_detail__r.BI_PL_Removed__c,
                        BI_PL_Channel_detail__r.BI_PL_Removed_Reason__c,
                        BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c,
                        BI_PL_Added_manually__c,
                        BI_PL_Business_rule_ok__c,
                        BI_PL_Column__c,
                        BI_PL_Commit_target__c,
                        BI_PL_Cvm__c,
                        BI_PL_Detail_priority__c,
                        BI_PL_Fdc__c,
                        BI_PL_Ims_id__c,
                        BI_PL_Market_target__c,
                        BI_PL_Other_goal__c,
                        BI_PL_Poa_objective__c,
                        BI_PL_Prescribe_target__c,
                        BI_PL_Primary_goal__c,
                        BI_PL_Row__c,
                        BI_PL_Segment__c,
                        BI_PL_Specialty_code__c,
                        BI_PL_Strategic_segment__c,
                        BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Start_date__c,
                        BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name,
                        BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Parent_position__r.Name,
                        BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                        BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                        BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c,
                        BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,
                        BI_PL_Channel_detail__r.BI_PL_Channel__c,
                        BI_PL_Product__r.External_ID_vod__c,
                        BI_PL_Product__c,
                        BI_PL_Secondary_product__r.External_ID_vod__c,
                        BI_PL_Secondary_product__c,
                        BI_PL_External_id__c
                        FROM BI_PL_Detail_preparation__c
                        WHERE BI_PL_Channel_detail__c IN:originalChannelDetails.keySet()
                    ]);

            Map<String, BI_PL_Preparation__c> newPreparationsByOriginalId = new Map<String, BI_PL_Preparation__c>();
            for (BI_PL_Target_preparation__c originalTarget : originalTargets) {
                try {
                    if (!newPreparationsByOriginalId.containsKey(originalTarget.BI_PL_Header__r.BI_PL_External_id__c)) {
                        newPreparationsByOriginalId.put(originalTarget.BI_PL_Header__r.BI_PL_External_id__c, originalTarget.BI_PL_Header__r);
                    }
                } catch (Exception e) {
                    System.debug(LoggingLevel.ERROR, '### - BI_PL_FillChildCyclesBatch - error on Prep creation');
                    System.debug(LoggingLevel.ERROR, '### - BI_PL_FillChildCyclesBatch - PrepCreation - e.getMessage() = ' + e.getMessage());
                }

            }


            // ======================= PREPARATIONS ========================= //

            Map<String, BI_PL_Preparation__c> listPrep = new Map<String, BI_PL_Preparation__c>();

            for (String key : this.positionsCycles.keySet()) {
                List<BI_PL_Position_cycle__c> listPC = this.positionsCycles.get(key);
                for (BI_PL_Position_cycle__c poscycle : listPC) {
                    for (String preparationExternalId : newPreparationsByOriginalId.keySet()) {
                        String position = newPreparationsByOriginalId.get(preparationExternalId).BI_PL_Position_cycle__r.BI_PL_Position__r.Name;
                        String hierarchy = newPreparationsByOriginalId.get(preparationExternalId).BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
                        if (poscycle.BI_PL_External_id__c.contains(hierarchy + '_' + position)) {
                            BI_PL_Preparation__c preparation = createPreparation(newPreparationsByOriginalId.get(preparationExternalId), poscycle);
                            listPrep.put(preparation.BI_PL_External_id__c, preparation);
                        }
                    }
                }
            }

            Schema.SObjectField field = BI_PL_Preparation__c.Fields.BI_PL_External_id__c;
            Database.UpsertResult [] cr = Database.upsert(listPrep.values(), field, false);
            for (Database.UpsertResult theResult : cr) {
                if (!theResult.isSuccess()) {
                    for (Database.Error error : theResult.getErrors()) {
                        mapErrors.add(error.getMessage());

                    }
                }
            }

            // ======================= TARGETS ========================= //

            Map<String, BI_PL_Target_preparation__c> listTg = new Map<String, BI_PL_Target_preparation__c>();

            for (BI_PL_Target_preparation__c target : originalTargets) {
                String hierarchy = target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
                String position = target.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name;
                for (BI_PL_Preparation__c preparation : listPrep.values()) {

                    if (preparation.BI_PL_External_id__c.contains(hierarchy + '_' + position)) {
                        BI_PL_Target_preparation__c newTarget = createTarget(target, preparation);
                        listTg.put(newTarget.BI_PL_External_id__c, newTarget);
                    }
                }
            }

            field = BI_PL_Target_preparation__c.Fields.BI_PL_External_id__c;
            cr = Database.upsert(listTg.values(), field, false);
            for (Database.UpsertResult theResult : cr) {
                if (!theResult.isSuccess()) {
                    for (Database.Error error : theResult.getErrors()) {

                        mapErrors.add(error.getMessage());



                    }
                }
            }

            // ======================= CHANNEL DETAILS ========================= //

            Map<String, BI_PL_Channel_detail_preparation__c> listCH = new Map<String, BI_PL_Channel_detail_preparation__c>();

            for (Id key : originalChannelDetails.keySet()) {
                BI_PL_Channel_detail_preparation__c ch = originalChannelDetails.get(key);

                String hierarchy = ch.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
                String position = ch.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name;
                String account = ch.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c;
                for (BI_PL_Target_preparation__c tg : listTg.values()) {
                    if (tg.BI_PL_External_id__c.contains(hierarchy + '_' + position + '_' + account)) {
                        BI_PL_Channel_detail_preparation__c newChannelDetail = createChannel(ch, tg);
                        listCH.put(newChannelDetail.BI_PL_External_id__c, newChannelDetail);
                    }
                }

            }

            field = BI_PL_Channel_detail_preparation__c.Fields.BI_PL_External_id__c;
            cr = Database.upsert(listCH.values(), field, false);
            for (Database.UpsertResult theResult : cr) {
                if (!theResult.isSuccess()) {
                    for (Database.Error error : theResult.getErrors()) {
                        mapErrors.add(error.getMessage());
                    }
                }
            }

            // ======================= DETAILS ========================= //

            Map<String, BI_PL_Detail_preparation__c> listDetail = new Map<String, BI_PL_Detail_preparation__c>();
            for (Id key : originalDetails.keySet()) {
                BI_PL_Detail_preparation__c detail = originalDetails.get(key);
                String hierarchy = detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
                String position = detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name;
                String account = detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c;
                String channel = detail.BI_PL_Channel_detail__r.BI_PL_Channel__c;
                //String parentPosition = detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Parent_position__r.Name;
                for (BI_PL_Channel_detail_preparation__c ch : listCH.values()) {
                    if (ch.BI_PL_External_Id__c.contains(hierarchy + '_' + position + '_' + account + '_' + channel)) {
                        BI_PL_Detail_preparation__c newDetail = createDetail(detail, ch);
                        if (newDetail != null) {
                            listDetail.put(newDetail.BI_PL_External_id__c, newDetail);
                        }
                    }
                }
            }

            //Set<BI_PL_Detail_preparation__c> setDetail = new Set<BI_PL_Detail_preparation__c>(listDetail);
            //List<BI_PL_Detail_preparation__c> listToUpsert = new List<BI_PL_Detail_preparation__c>(setDetail);
            field = BI_PL_Detail_preparation__c.Fields.BI_PL_External_id__c;
            cr = Database.upsert(listDetail.values(), field, false);

            for (Database.UpsertResult theResult : cr) {
                if (!theResult.isSuccess()) {
                    for (Database.Error error : theResult.getErrors()) {

                        mapErrors.add(error.getMessage());

                    }
                }
            }
        } catch (Exception error) {
            mapErrors.add(error.getMessage());
            BI_PL_ErrorHandlerUtility.addPlanitServerError('BI_PL_FillChildCyclesBatch', BI_PL_ErrorHandlerUtility.FUNCTIONALITY_SPLIT_CYCLE, error.getMessage(), error.getStackTraceString());

        }

    }

    global void finish(Database.BatchableContext BC) {

        System.debug('### - BI_PL_FillChildCyclesBatch - finish');
        System.debug('DML: ' + String.valueOf(Limits.getLimitDMLRows() - Limits.getDMLRows()));
        createEmail();

        /*if(mapErrors.size()==0 && this.cycles.size()>0){

            String firstCycle = this.cycles.get(0).Id;

            List<String> nextCycles = new List<String>();
            for(Integer i = 1; i<this.cycles.size();i++){

                nextCycles.add(this.cycles.get(i).Id);
            }



           // Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(firstCycle, this.hierarchies.get(0), false,  nextCycles, this.hierarchies));
        }*/

    }


    public void createEmail() {
        System.debug('### creatin Email');
        String subject;
        String body = '';
        if (mapErrors.size() > 0) {
            System.debug('### process failed');
            subject = Label.BI_PL_split_cycle_subject_failed + ' ' + this.cycleName;
            body += '<p>' + Label.BI_PL_split_cycle_body_failed + '</p>';

            for (String message : mapErrors) {
                body += '<p>' + message + '</p>';
            }

        } else {
            System.debug('### process okey');
            subject = Label.BI_PL_Split_cycle_subject_ok;

            body += '<p>' + Label.BI_PL_The_cycle + ' ' + this.cycleName + ' ' +  Label.BI_PL_successfully_split + ' </p>';
            body += '<p>' + Label.BI_PL_Go_to_cycle_page + '</p>';
        }


        sendEmail(subject, body);
    }

    public void sendEmail(String subject, String body) {
        System.debug('### sending email');
        try {
            sendEmailWithTemplate(subject, body);
        } catch (exception e) {
            System.debug('###Unable to send email: ' + e.getMessage());
        }
    }

    private BI_PL_Preparation__c createPreparation(BI_PL_preparation__c original, BI_PL_Position_cycle__c positionCycle) {


        return new BI_PL_Preparation__c(
                   BI_PL_Country_code__c = original.BI_PL_Country_code__c,
                   BI_PL_External_id__c = positionCycle.BI_PL_External_id__c,
                   //BI_PL_Position_cycle__r = positionCycle,
                   BI_PL_Position_cycle__c = positionCycle.id,
                   BI_PL_Capacity__c = original.BI_PL_Capacity__c,
                   //BI_PL_Global_workload__c = original.BI_PL_Header__r.BI_PL_Global_workload__c,
                   //BI_PL_Workload_adjustment__c = original.BI_PL_Header__r.BI_PL_Workload_adjustment__c,
                   //BI_PL_Workload_approved__c = original.BI_PL_Header__r.BI_PL_Workload_approved__c

                   BI_PL_Capacity_adjustment__c = original.BI_PL_Capacity_adjustment__c,
                   BI_PL_Capacity_adjustment_approved__c = original.BI_PL_Capacity_adjustment_approved__c,
                   BI_PL_Final_adjusted_capacity__c = original.BI_PL_Final_adjusted_capacity__c
               );
    }

    private BI_PL_Target_preparation__c createTarget(BI_PL_Target_preparation__c targetOld, BI_PL_preparation__c preparation) {
        return new BI_PL_Target_preparation__c(
                   BI_PL_Added_manually__c = targetOld.BI_PL_Added_manually__c,
                   BI_PL_Added_reason__c = targetOld.BI_PL_Added_reason__c,
                   BI_PL_Target_customer__c = targetOld.BI_PL_Target_customer__c,
                   BI_PL_External_id__c = preparation.BI_PL_External_id__c + '_' + targetOld.BI_PL_Target_customer__r.External_ID_vod__c,
                   BI_PL_Header__c = preparation.id,
                   //BI_PL_Header__r = preparation,
                   BI_PL_Next_best_account__c = targetOld.BI_PL_next_best_Account__c,
                   BI_PL_No_see_list__c = targetOld.BI_PL_No_see_list__c,
                   BI_PL_Ntl_value__c = targetOld.BI_PL_Ntl_value__c,
                   BI_PL_Removed__c = targetOld.BI_PL_Removed__c,
                   BI_PL_Removed_reason__c = targetOld.BI_PL_Removed_reason__c,
                   BI_PL_Specialty__c = targetOld.BI_PL_Specialty__c
               );
    }

    private BI_PL_Channel_detail_preparation__c createChannel(BI_PL_Channel_detail_preparation__c channel, BI_PL_Target_preparation__c target) {
        return new BI_PL_Channel_detail_preparation__c(
                   BI_PL_Channel__c = channel.BI_PL_Channel__c,
                   BI_PL_Edited__c = channel.BI_PL_Edited__c,
                   BI_PL_External_id__c = target.BI_PL_External_id__c + '_' + channel.BI_PL_Channel__c,
                   BI_PL_Max_adjusted_interactions_input__c = channel.BI_PL_Max_adjusted_interactions_input__c,
                   BI_PL_Mccp_channel_criteria__c = channel.BI_PL_Mccp_channel_criteria__c,
                   BI_PL_MSL_flag__c = channel.BI_PL_MSL_flag__c,
                   BI_PL_Target__c = target.id,
                   //BI_PL_Target__r = target,
                   BI_PL_Rejected__c = channel.BI_PL_Rejected__c,
                   BI_PL_Rejected_Reason__c = channel.BI_PL_Rejected_Reason__c,
                   BI_PL_Removed__c = channel.BI_PL_Removed__c,
                   BI_PL_Removed_date__c = channel.BI_PL_Removed_date__c,
                   BI_PL_Removed_reason__c = channel.BI_PL_Removed_reason__c,
                   BI_PL_Reviewed__c = channel.BI_PL_Reviewed__c

               );
    }

    private BI_PL_Detail_preparation__c createDetail(BI_PL_Detail_preparation__c detail, BI_PL_Channel_detail_preparation__c channel) {

        if(this.productId == '' || this.productId == detail.BI_PL_Product__c){
            System.debug('//// startDate ' + channel.BI_PL_External_id__c.substring(7, 9));
            String startDate = channel.BI_PL_External_id__c.substring(7, 9);
            Integer month = Integer.valueOf(startDate);
            System.debug('/// sttratDate int: ' + month);
            Integer frequency;

            Decimal value = BI_PL_SynchronizeSAPDataBatch.getToVeevaAddValue(channel.BI_PL_Reviewed__c,channel.BI_PL_Removed__c, channel.BI_PL_Rejected__c, channel.BI_PL_Edited__c, channel.BI_PL_Target__r.BI_PL_Added_manually__c, detail.BI_PL_Planned_details__c, detail.BI_PL_Adjusted_details__c);
            
            if(value == -1) value = detail.BI_PL_Planned_details__c;

            System.debug('value ' + value);
            if (value >= 0) {
                frequency = frequencies.get((Integer) value).get(month - 1);
                BI_PL_Detail_preparation__c det = new BI_PL_Detail_preparation__c(
                    BI_PL_Adjusted_details__c = frequency,
                    BI_PL_Added_manually__c = detail.BI_PL_Added_manually__c,
                    BI_PL_Business_rule_ok__c = detail.BI_PL_Business_rule_ok__c,
                    BI_PL_Column__c = detail.BI_PL_Column__c,
                    BI_PL_Commit_target__c = detail.BI_PL_Commit_target__c,
                    BI_PL_Cvm__c = detail.BI_PL_Cvm__c,
                    BI_PL_Detail_priority__c = detail.BI_PL_Detail_priority__c,
                    BI_PL_External_id__c = channel.BI_PL_External_id__c + '_' + detail.BI_PL_Product__r.External_ID_vod__c,
                    BI_PL_Fdc__c = detail.BI_PL_Fdc__c,
                    BI_PL_Ims_id__c = detail.BI_PL_Ims_id__c,
                    BI_PL_Market_target__c = detail.BI_PL_Market_target__c,
                    BI_PL_Other_goal__c = detail.BI_PL_Other_goal__c,
                    BI_PL_Planned_details__c = frequency,
                    BI_PL_Channel_detail__c = channel.id,
                    //BI_PL_Channel_detail__r = channel,
                    BI_PL_Poa_objective__c = detail.BI_PL_Poa_objective__c,
                    BI_PL_Prescribe_target__c = detail.BI_PL_Prescribe_target__c,
                    BI_PL_Primary_goal__c = detail.BI_PL_Primary_goal__c,
                    BI_PL_Product__c = detail.BI_PL_Product__c,
                    BI_PL_Row__c = detail.BI_PL_Row__c,
                    BI_PL_Segment__c = detail.BI_PL_Segment__c,
                    BI_PL_Specialty_code__c = detail.BI_PL_Specialty_code__c,
                    BI_PL_Strategic_segment__c = detail.BI_PL_Strategic_segment__c
                );

                if (detail.BI_PL_Secondary_product__c != null) {
                    det.BI_PL_External_id__c = det.BI_PL_External_id__c + detail.BI_PL_Secondary_product__r.External_ID_vod__c;
                    return det;
                } else {
                    return det;

                }
            } else {
                return null;
            }
        }else{
            return null;
        }

    }

    public void sendEmailWithTemplate(String subject, String body) {
        Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
        emailToSend.setTargetObjectId( UserInfo.getUserId() );
        emailToSend.setSaveAsActivity( false );
        emailToSend.setHTMLBody(body);
        emailToSend.setSubject(subject);

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {emailToSend});
    }

    



}