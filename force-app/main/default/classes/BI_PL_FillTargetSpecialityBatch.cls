/**
 *  Fills the target's specialty field with the one in account defined by the country custom setting.
 *
 *  @author OMEGA CRM
 */
global class BI_PL_FillTargetSpecialityBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {
    
    private Map<String, String> mapSpecilatyCodeMapping = new Map<String, String>();

    global BI_PL_FillTargetSpecialityBatch(){}
    global BI_PL_FillTargetSpecialityBatch(List<String> sCycle, List<String> lstHierarchyNames) {
        this.cycleIds = sCycle;
        this.hierarchyNames = lstHierarchyNames;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        system.debug('## START BI_PL_FillTargetSpecialityBatch');

        //Load all Specialty Codes 
        for(BI_PL_Specialty_code_mapping__mdt specialtyCodeMap :[SELECT MasterLabel, BI_PL_Code__c 
                                                                 FROM BI_PL_Specialty_code_mapping__mdt
                                                                 LIMIT 10000] ){
            mapSpecilatyCodeMapping.put(specialtyCodeMap.MasterLabel, specialtyCodeMap.BI_PL_Code__c);
        }

        return Database.getQueryLocator(
            [SELECT Id, Name, BI_PL_Target_customer__c, BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Header__r.BI_PL_Country_code__c, 
                    BI_PL_Specialty__c, BI_PL_Specialty_code__c   
                FROM BI_PL_Target_preparation__c
                WHERE BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleIds
                    AND BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN :hierarchyNames
                    ORDER BY BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c]
        );
    }

    global void execute(Database.BatchableContext BC, List<BI_PL_Target_preparation__c> lstTarget) {
    
        String sSpecialtyField, sSpecialtyValue, sSpecialtyCode;    
        set<String> setCountries = new set<String>();
        set<Id> setAccountsIds = new set<Id>();
        set<BI_PL_Target_preparation__c> setTargetPrepToUpdate = new set<BI_PL_Target_preparation__c>();

        system.debug('##lstTarget: ' + lstTarget);
        for (BI_PL_Target_preparation__c inTarget : lstTarget){
            setCountries.add(inTarget.BI_PL_Header__r.BI_PL_Country_code__c);
            setAccountsIds.add(inTarget.BI_PL_Target_customer__c);
        }

        map<String,String> mapSpecialtybyCountry = new map<String,String>();
        map<String,Boolean> mapSpecialtyCodeByCountry = new map<String,Boolean>();


        //Load Custom Setting by Countries in Target Preparation
        for (BI_PL_Country_settings__c inSetField : [SELECT BI_PL_Country_code__c, BI_PL_Specialty_field__c, BI_PL_Specialty_code__c  
                                                    FROM BI_PL_Country_settings__c 
                                                    WHERE BI_PL_Country_code__c IN :setCountries]){
            mapSpecialtybyCountry.put(inSetField.BI_PL_Country_code__c, inSetField.BI_PL_Specialty_field__c);
            mapSpecialtyCodeByCountry.put(inSetField.BI_PL_Country_code__c, inSetField.BI_PL_Specialty_code__c);
        }
        if (mapSpecialtybyCountry.isEmpty()) throw new BI_PL_Exception('ERROR BI_PL_FillTargetSpecialityBatch - No Fields in Country Settings');

        system.debug('## CountriesCode: ' + mapSpecialtybyCountry.keyset());
        system.debug('## Specialties: ' + mapSpecialtybyCountry.values());

        map<Id,Account> mapAccounts = getAccountsWithSpecialtyField(mapSpecialtybyCountry, setAccountsIds);

        for (BI_PL_Target_preparation__c inTarget : lstTarget){
            if (mapAccounts.containsKey(inTarget.BI_PL_Target_customer__c) && mapSpecialtybyCountry.containsKey(inTarget.BI_PL_Header__r.BI_PL_Country_code__c) ){

                //Get Api Field Name based on Country Code
                sSpecialtyField = mapSpecialtybyCountry.get(inTarget.BI_PL_Header__r.BI_PL_Country_code__c);
                //get Specialty field value from Field name
                sSpecialtyValue = (String)BI_PL_TargetPreparationHandler.getFieldValue( mapAccounts.get(inTarget.BI_PL_Target_customer__c), sSpecialtyField );

                system.debug('## sSpecialtyField: ' + sSpecialtyField);
                system.debug('## sSpecialtyValue: ' + sSpecialtyValue);

                //check picklist value
                sSpecialtyValue = checkPicklistValue(sSpecialtyValue, sSpecialtyField);

                //Only change when it's not the same value
                if ( String.isNotBlank(sSpecialtyValue) && sSpecialtyValue != inTarget.BI_PL_Specialty__c) setTargetPrepToUpdate.add(inTarget);
                //inTarget.BI_PL_Specialty__c = sSpecialtyValue;

                //check Country has Specialty Code to load in Target Preparation
                if(mapSpecialtyCodeByCountry.get(inTarget.BI_PL_Header__r.BI_PL_Country_code__c) && mapSpecilatyCodeMapping.containsKey(sSpecialtyValue)){
                    sSpecialtyCode = mapSpecilatyCodeMapping.get(sSpecialtyValue);
                    //Only change when it's not the same value
                    if (String.isNotBlank(sSpecialtyCode) && sSpecialtyCode != inTarget.BI_PL_Specialty_code__c ) setTargetPrepToUpdate.add(inTarget);
                    //inTarget.BI_PL_Specialty_code__c = sSpecialtyCode;                        
                }
            }
        }

        if (!setTargetPrepToUpdate.isEmpty()) update new List<BI_PL_Target_preparation__c>( setTargetPrepToUpdate );
    }
    
    global void finish(Database.BatchableContext BC) {
        system.debug(' ## FINISH BI_PL_FillTargetSpecialityBatch');
    }

    /*
    * Load from Custom Setting Countries and its Specialty Field
    */
    private map<String,String> getSpecialtyFieldbyCountry(set<String> setCountryCodes){

        Map<String,String> mapCountry = new Map<String,String>();
        for (BI_PL_Country_settings__c inSetField : [SELECT BI_PL_Country_code__c, BI_PL_Specialty_field__c, BI_PL_Specialty_code__c 
                                                    FROM BI_PL_Country_settings__c 
                                                    WHERE BI_PL_Country_code__c IN :setCountryCodes]){
            mapCountry.put(inSetField.BI_PL_Country_code__c, inSetField.BI_PL_Specialty_field__c);
        }

        return mapCountry;
    }

    /*
    * get All Accounts with Specialty values
    */
    private map<Id,Account> getAccountsWithSpecialtyField(map<String,String> mapSpecialties, set<Id> accountsIds){

        map<Id,Account> mapAccounts = new map<Id,Account>();

        set<String> alreadyAddedFields = new set<String>();
        String squery = 'SELECT Id, Country_Code_BI__c';

        for (String field : mapSpecialties.values()) {
            if (!alreadyAddedFields.contains(field)) {
                squery += ', ' + field;
                alreadyAddedFields.add(field);
            }
        }

        System.debug('## Query Account: ' + squery);

        squery += ' FROM Account WHERE Id IN :accountsIds';

        for (Account inAcc : Database.query(squery) ){
            mapAccounts.put(inAcc.Id, inAcc);
        }

        return mapAccounts;
    }   

    /*
    * 
    * Check if value is from Picklist value. Done this way because business rules' Specialty are set in the country's language.
    */
    private String checkPicklistValue(String sValue, String sSpecialtyField){

        Schema.SObjectField picklistField = BI_PL_TargetPreparationHandler.getPicklistFieldIfIsPicklist(sSpecialtyField);

        //Return the picklist label instead of the value
        return (picklistField != null) ? getPicklistLabel(picklistField, sValue) : sValue;
    }

    private String getPicklistLabel(Schema.SObjectField picklistField, String value) {
        Schema.DescribeFieldResult fieldResult = picklistField.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for (Schema.PicklistEntry f : ple) {
            if (f.getValue() == value)
                return f.getLabel();
        }
        return null;
    }
}