@isTest
private class BI_PL_FillTargetSpecialityBatch_Test {
	
	private static List<String> hierarchy = new List<String>{BI_PL_TestDataFactory.hierarchy};
	private static List<Account> accList;
	private static List<Product_vod__c> prodList;
	private static List<BI_PL_Position_Cycle__c> posCycList;

	@testSetup static void setup() {

		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		String countryCode = testUser.Country_Code_BI__c;

		accList = BI_PL_TestDataFactory.createTestAccounts(2, countryCode);
		prodList = BI_PL_TestDataFactory.createTestProduct(2, countryCode);

		BI_PL_TestDataFactory.createCycleStructure(countryCode);

		posCycList = [SELECT Id, BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];

		BI_PL_TestDataFactory.createPreparations(countryCode, posCycList, accList, prodList);
	}

	@isTest static void test_method_one() {		

		accList = [SELECT Id, Name, Specialty_1_vod__c, External_ID_vod__c FROM Account];

		system.debug('## accList: ' + accList);

		for (Account inAcc : accList){
			inAcc.Specialty_1_vod__c = 'TheSpecialty';
		}
		update accList;

		posCycList = [SELECT Id, BI_PL_Cycle__c, BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];

		BI_PL_Cycle__c oCycle = [SELECT Id, BI_PL_External_Id__c FROM BI_PL_Cycle__c LIMIT 1];
		system.debug('## oCycle: ' + oCycle);
		system.assertNotEquals(oCycle,null);

		BI_PL_FillTargetSpecialityBatch o = new BI_PL_FillTargetSpecialityBatch();
		Test.startTest();

			Id batchId = Database.executeBatch( new BI_PL_FillTargetSpecialityBatch(new List<String>{posCycList.get(0).BI_PL_Cycle__c}, hierarchy), 200 );

		Test.stopTest();

		System.assertNotEquals([SELECT Id FROM CronTrigger WHERE Id = :batchId], null);
	}
	
}