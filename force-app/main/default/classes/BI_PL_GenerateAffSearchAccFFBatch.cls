global class BI_PL_GenerateAffSearchAccFFBatch implements Database.Batchable<sObject> {

	Map<String, String> mapping = new Map<String, String> {
		'KAM/AM' => 'HSBS'
	};
	String cycle;
	String countryCode;

	global BI_PL_GenerateAffSearchAccFFBatch(String cycleId, String countryCode) {
		System.debug('Generate Affiliation Search FF');

		this.cycle = cycleId;
		this.countryCode = countryCode;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT BI_PL_Target_customer__c, BI_PL_Target_customer__r.External_id_vod__c, BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c, BI_PL_Country_code__c FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c IN: mapping.keySet() AND BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : this.cycle]);
	}

	global void execute(Database.BatchableContext BC, List<BI_PL_Target_preparation__c> scope) {
		List<BI_PL_Affiliation__c> 	listToInsert = new List<BI_PL_Affiliation__c>();

		for (BI_PL_Target_preparation__c tp : scope) {

			BI_PL_Affiliation__c newAff = new BI_PL_Affiliation__c();
			newAff.BI_PL_Customer__c = tp.BI_PL_Target_customer__c;
			newAff.BI_PL_Field_force__c = mapping.get(tp.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c);
			newAff.BI_PL_Type__c = BI_PL_GenerateSearchHCOAffiliationsBatch.AFFILIATION_TYPE_SEARCH_ACCOUNTS;
			newAff.BI_PL_External_Id__c = tp.BI_PL_Target_customer__r.External_id_vod__c + '_' + tp.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c + '_' + BI_PL_GenerateSearchHCOAffiliationsBatch.AFFILIATION_TYPE_SEARCH_ACCOUNTS;
			newAff.BI_PL_Country_code__c = tp.BI_PL_Country_code__c;
			listToInsert.add(newAff);


		}

		Set<BI_PL_Affiliation__c> setInsert = new Set<BI_PL_Affiliation__c>(listToInsert);

		List<BI_PL_Affiliation__c> listAff = new List<BI_PL_Affiliation__c>(setInsert);

		upsert listAff BI_PL_External_Id__c;


	}

	global void finish(Database.BatchableContext BC) {
		Database.executeBatch(new BI_PL_GenerateAffSearchAccChildFFBatch(countryCode));
	}

}