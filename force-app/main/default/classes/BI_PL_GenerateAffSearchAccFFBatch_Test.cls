@isTest
private class BI_PL_GenerateAffSearchAccFFBatch_Test {
	
	private static final String SAFFILIATION_TYPE_SEARCH_ACCOUNTS = BI_PL_TestDataFactory.SAFFILIATION_TYPE_SEARCH_ACCOUNTS;
	private static String userCountryCode;
	private static BI_PL_Cycle__c cycle;

	@testSetup static void setup() {

        userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
        
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);

        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

        cycle = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

	}
	
	@isTest static void test_GenerateAffSearchAccFFBatch() {

		cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
		userCountryCode = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()].Country_Code_BI__c;

        
        Test.startTest();

        Database.executeBatch(new BI_PL_GenerateAffSearchAccFFBatch(cycle.Id,userCountryCode));

        Test.stopTest();

		System.assertNotEquals([Select Id FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = :SAFFILIATION_TYPE_SEARCH_ACCOUNTS].size(),0);

	}
	

	@isTest static void test_GenerateAffSearchAccChildFFBatch() {	
		
		cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
		userCountryCode = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()].Country_Code_BI__c;

		List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

		BI_PL_TestDataFactory.createAffiliationRelationship(listAcc, listAcc.get(0), userCountryCode);

		List<BI_PL_Affiliation__c> 	listToInsert = new List<BI_PL_Affiliation__c>();

		BI_PL_Affiliation__c newAff = new BI_PL_Affiliation__c(
			BI_PL_Customer__c = listAcc.get(0).Id,
			BI_PL_Type__c = SAFFILIATION_TYPE_SEARCH_ACCOUNTS,
			BI_PL_Country_code__c = userCountryCode,
			BI_PL_Field_force__c = 'FieldForce ' + listAcc.get(0).Id
		);
		insert newAff;

		Test.startTest();

        Database.executeBatch(new BI_PL_GenerateAffSearchAccChildFFBatch(userCountryCode));

        Test.stopTest();

        System.debug('## acc: ' + newAff);
        //After Batch all Child BI_PL_Affiliation have the Parent's BI_PL_Field_force__c field name
        For (BI_PL_Affiliation__c inAff : [Select Id, BI_PL_Customer__c, BI_PL_Field_force__c, BI_PL_Type__c, BI_PL_External_Id__c 
        									FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = :SAFFILIATION_TYPE_SEARCH_ACCOUNTS]){
        	System.debug('## res: ' + inAff);
        	System.assertEquals(inAff.BI_PL_Field_force__c, newAff.BI_PL_Field_force__c);
        }

	}


	@isTest static void test_GenerateSearchHCOAffiliationsBatch(){
		cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
		userCountryCode = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()].Country_Code_BI__c;

		List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

		BI_PL_TestDataFactory.createAffiliationRelationship(listAcc, listAcc.get(0), userCountryCode);

		BI_PL_Affiliation__c newAff = new BI_PL_Affiliation__c(
			BI_PL_Customer__c = listAcc.get(0).Id,
			BI_PL_Type__c = SAFFILIATION_TYPE_SEARCH_ACCOUNTS,
			BI_PL_Country_code__c = userCountryCode,
			BI_PL_Field_force__c = 'FieldForce ' + listAcc.get(0).Id
		);

		insert newAff;
        Test.startTest();
		BI_PL_GenerateSearchHCOAffiliationsBatch gs = new BI_PL_GenerateSearchHCOAffiliationsBatch();
        Database.executeBatch(new BI_PL_GenerateSearchHCOAffiliationsBatch(cycle.Id));

        Test.stopTest();

	}

}