/**
 * Execute this batch to execute the whole process of "HCO Affiliation Search records creation".
 * 1.- Delete existing 'SearchAccounts' records for the country.
 * 2.- Generate parent records.
 * 3.- Generate children records.
 */
global class BI_PL_GenerateSearchHCOAffiliationsBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject> {

	public static final String AFFILIATION_TYPE_SEARCH_ACCOUNTS = 'SearchAccounts';

	String cycle;
	String countryCode;

	global BI_PL_GenerateSearchHCOAffiliationsBatch() {}

	global BI_PL_GenerateSearchHCOAffiliationsBatch(String cycleId) {
		System.debug('Generate Affiliation Search FF');
		this.cycle = cycleId;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		cycle = this.cycleIds.isEmpty() ? cycle : this.cycleIds.get(0);
		countryCode = [SELECT BI_PL_Country_code__c FROM BI_PL_Cycle__c WHERE Id = : cycle LIMIT 1].BI_PL_Country_code__c;
		return Database.getQueryLocator([SELECT id FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = : AFFILIATION_TYPE_SEARCH_ACCOUNTS AND BI_PL_Country_code__c = : countryCode]);
	}

	global void execute(Database.BatchableContext BC, List<BI_PL_Affiliation__c> scope) {
		delete scope;
	}

	global void finish(Database.BatchableContext BC) {
		Database.executeBatch(new BI_PL_GenerateAffSearchAccFFBatch(cycle, countryCode));
	}

}