global class BI_PL_GenerateVCOBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global String query;
    global BI_PL_Cycle__c cycle;

    global String flag;
    global String countryCode;
    global String productId;
    global Set<String> mapErrors;
    global BI_PL_Affiliation__c planitVCO;
    global List<BI_PL_Affiliation__c> listPlanitVco;

    global Map<String, String> pairFlagProduct;
    global List<String> fieldForces;

    Map<String, String> mapping = new Map<String, String>{
        'Jar flag' => 'BI_PL_Jar_flag__c',
        'Spiriva flag' => 'BI_PL_Spir_flag__c',
        'Stio flag' => 'BI_PL_Stio_flag__c'

    };
    //MAP flag product
    global BI_PL_GenerateVCOBatch (BI_PL_Cycle__c cyc, String cc, List<BI_PL_Affiliation__c> arr) {
        this.planitVCO = arr.remove(0);
        this.listPlanitVco = arr;
        this.cycle = cyc;
        this.flag = this.planitVCO.BI_PL_Selected_VCO_Flag__c;
        this.countryCode = cc;
        this.productId = this.planitVCO.BI_PL_Product__c;
        this.mapErrors = new set<String>();
        this.fieldForces = this.planitVCO.BI_PL_Field_force__c.split(';');


        //String flagAPI = mapping.get(flag);
        //query to get the affiliation records that meet the criteria //' + flagAPI + '= true

        this.query = 'SELECT Id, BI_PL_Customer__c, BI_PL_Product__c, BI_PL_VCO_flag__c FROM BI_PL_Affiliation__c WHERE  BI_PL_Type__c = \'Information\' AND BI_PL_Country_code__c = \''+ countryCode +'\' AND BI_PL_Product__c = \'' + productId + '\' AND BI_PL_VCO_flag__c includes(\''+flag+'\')';

        System.debug('query: ' + query);

        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<BI_PL_Affiliation__c> scope) {
        System.debug('SIZE: ' + scope.size());
        System.debug('scope: ' + scope);

        List<Id> accountsId =  new List<Id>();
        String cycleId = this.cycle.Id;
        Set<String> uniqueFlags = new Set<String>();
        //Get all the different accounbts
        for(BI_PL_Affiliation__c aff :  scope){
            accountsId.add(aff.BI_PL_Customer__c);           
        }

        Id recordTypeId = Schema.SObjectType.Call_Objective_vod__c.getRecordTypeInfosByName().get('Call Objective').getRecordTypeId();

        Map<String, Call_Objective_vod__c> mapVeevaCallObjective = new Map<String, Call_Objective_vod__c>(); 

        System.debug('Product_vod__c: ' + productId + ' flag; ' + flag);
        /*//Get the data to create the VCO in Veeva
        BI_PL_Affiliation__c planitVCO = [SELECT Id, BI_PL_Selling_message__c, BI_PL_Vco_name__c, BI_PL_Product__c, BI_PL_Selected_VCO_Flag__c FROM BI_PL_Affiliation__c WHERE BI_PL_Country_code__c =: countryCode AND BI_PL_Type__c = 'VCO' AND BI_PL_Selected_VCO_Flag__c =: flag LIMIT 1];       */    

        String startDate = BI_PL_BITMANtoPLANiTImportUtility.getDateString(cycle.BI_PL_Start_date__c);
        String endDate = BI_PL_BITMANtoPLANiTImportUtility.getDateString(cycle.BI_PL_End_date__c);


        //Create Veeva Call objective for every account that meet the criteria (having the product and the flag a true), FRom date -> cycle start date, To date -> cycle end date, recordtype -> call objective, name -> name in planit vco, on default -> true, 
        // ownerId -> owner of the preparation, recurring -> true, selling message -> selling message in planit vco, external id -> generated

        for(BI_PL_Detail_preparation__c detail : [SELECT Id, BI_PL_Product__c, BI_PL_Product__r.External_Id_vod__c,BI_PL_Secondary_product__r.External_Id_vod__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c,  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.OwnerId,BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name FROM BI_PL_Detail_preparation__c WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c =: cycleId AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c IN: accountsId AND (BI_PL_Product__c =: productId OR BI_PL_Secondary_product__c =: productId) AND BI_PL_Channel_detail__r.BI_PL_Msl_flag__c = true AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_HCP__c = true AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c IN: fieldForces]){

            String primaryOrSecondaryId;

            if(detail.BI_PL_Product__c == productId){
                primaryOrSecondaryId = detail.BI_PL_Product__r.External_Id_vod__c;
            }else{
                primaryOrSecondaryId = detail.BI_PL_Secondary_product__r.External_Id_vod__c;

            }

             //External id -> Contry code_Target External Id_cycle Start Date_cycle End date_product external id_Position name
            String externalId = countryCode + '_' + detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c + '_' + startDate + '_' + endDate +'_'+ primaryOrSecondaryId + '_'+this.flag+'_' + detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name;

            System.debug('EXTENAL ID: ' + externalId);


            Call_Objective_vod__c co = new Call_Objective_vod__c(Account_vod__c = detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,
                                                        Product_vod__c = productId,
                                                        Country_Code_BI__c = countryCode,
                                                        From_Date_vod__c = cycle.BI_PL_Start_date__c,
                                                        To_Date_vod__c = cycle.BI_PL_End_date__c,
                                                        RecordTypeId = recordTypeId,
                                                        Name_vod__c = planitVCO.BI_PL_Vco_name__c,
                                                        On_By_Default_vod__c = true,
                                                        OwnerId = detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.OwnerId,
                                                        Recurring_vod__c = true,
                                                        Selling_Message__c = planitVCO.BI_PL_Selling_message__c,
                                                        External_ID_BI__c = externalId );

            mapVeevaCallObjective.put(externalId, co);


        }


        Schema.SObjectField field = Call_Objective_vod__c.Fields.External_ID_BI__c;
        Database.UpsertResult [] cr = Database.upsert(mapVeevaCallObjective.values(), field, false);
       
        for (Database.UpsertResult theResult : cr) {
            if (!theResult.isSuccess()) {
                for (Database.Error error : theResult.getErrors()) {
                    mapErrors.add(error.getMessage());

                }
            }
        }

    }
    
    global void finish(Database.BatchableContext BC) {

        createEmail();

        if(this.listPlanitVco.size() > 0){
            Database.executeBatch(new BI_PL_GenerateVCOBatch(this.cycle, this.countryCode,this.listPlanitVco));
        }
    }

    public void createEmail() {
        System.debug('### creating Email');
        String subject;
        String body = '';
        if (mapErrors.size() > 0) {
            System.debug('### process failed');
            subject = 'Failed process to create VCO for the cycle ' + this.cycle.Name;
            body += '<p>' + 'Product: '+ this.planitVCO.BI_PL_Product__r.Name+ ' </p>';
            body += '<p>' + 'Flag: '+ this.planitVCO.BI_PL_Selected_VCO_Flag__c+ ' </p>';
            body += '<p>' + 'The records failed with the following message: '+ '</p>';

            for (String message : mapErrors) {
                body += '<p>' + message + '</p>';
            }

        } else {
            System.debug('### process okey');
            subject = 'Ceated VCO succesfully';

            body += '<p>' + 'The VCO has been succesfully created for the cycle '+ this.cycle.Name+ ' </p>';
            body += '<p>' + 'Product: '+ this.planitVCO.BI_PL_Product__r.Name+ ' </p>';
            body += '<p>' + 'Flag: '+ this.planitVCO.BI_PL_Selected_VCO_Flag__c+ ' </p>';
            body += '<p>' + 'Field Forces: ';
            for(Integer i = 0;i<this.fieldForces.size();i++){
                if(i == this.fieldForces.size()-1){
                    body+= ''+this.fieldForces.get(i);
                }else{

                    body+= this.fieldForces.get(i) + ', ';
                }
            }
            /*body += '<p>' + Label.BI_PL_Go_to_cycle_page + '</p>';*/
        }


        sendEmail(subject, body);
    }

    public void sendEmail(String subject, String body) {
        System.debug('### sending email');
        try {
            sendEmailWithTemplate(subject, body);
        } catch (exception e) {
            System.debug('###Unable to send email: ' + e.getMessage());
        }
    }

    public void sendEmailWithTemplate(String subject, String body) {
        Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
        emailToSend.setTargetObjectId( UserInfo.getUserId() );
        emailToSend.setSaveAsActivity( false );
        emailToSend.setHTMLBody(body);
        emailToSend.setSubject(subject);

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {emailToSend});
    }

    
    
}