@isTest
private class BI_PL_GenerateVCOBatch_Test {

	private static String userCountryCode;
    public static final String SCHANNEL = 'rep_detail_only';

	
	@isTest static void test_method_one() {
		User testUser = [SELECT Id, Country_Code_BI__c, UserName, External_ID__c from User where Id =: UserInfo.getUserId()];
		userCountryCode = testUser.Country_Code_BI__c;
		
        BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();
		BI_PL_TestDataFactory.usersCreation(userCountryCode);
		BI_PL_TestDataFactory.createTestPersonAccounts(4, userCountryCode);
		BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

		Date startDate = Date.today();
		Date endDate = Date.today() + 7;

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'fielfforcename', BI_TM_Country_code__c = userCountryCode);

		List<Account> accountList = [SELECT Id, External_Id_vod__c FROM Account];
		List<Product_vod__c> productList = [SELECT Id, External_Id_vod__c FROM Product_vod__c];

		insert fieldForce;
		fieldForce = [SELECT Id, Name FROM BI_TM_FF_type__c LIMIT 1];
		BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(Name = 'Cycle Test', BI_PL_Start_date__c = startDate, BI_PL_End_date__c = endDate, BI_PL_Field_force__c = fieldForce.Id, BI_PL_Country_code__c = userCountryCode);
		insert cycle;

		cycle =[SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Country_code__c, BI_PL_Field_force__r.Name, Name FROM BI_PL_Cycle__c LIMIT 1];

		BI_PL_Position__c position = new BI_PL_Position__c(Name = 'position1', BI_PL_Country_code__c = userCountryCode, BI_PL_Field_force__c = 'CARD');
		insert position;

		BI_PL_Position_cycle__c posCycle = new BI_PL_Position_cycle__c(BI_PL_Position__c = position.Id, BI_PL_Parent_position__c = null, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = 'CARD');
		posCycle.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, posCycle.BI_PL_Hierarchy__c, position.Name);

		insert posCycle;

		BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = posCycle.Id, BI_PL_User__c = testUser.Id);
		pcu.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycle, posCycle.BI_PL_Hierarchy__c, position.Name, testUser.External_Id__c);
		insert pcu;


		BI_PL_Preparation__c prep = new BI_PL_Preparation__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Position_cycle__c = posCycle.Id, BI_PL_Status__c = 'Under Review', BI_PL_External_id__c = posCycle.BI_PL_External_id__c);
		insert prep;

		List<BI_PL_Detail_preparation__c> listDetails = new List<BI_PL_Detail_preparation__c>();
		List<BI_PL_Target_preparation__c> listTarget = new List<BI_PL_Target_preparation__c>();
		List<BI_PL_Channel_detail_preparation__c> listChannel = new List<BI_PL_Channel_detail_preparation__c>();
		List<BI_PL_Affiliation__c> listAff = new List<BI_PL_Affiliation__c>();

		for(Account acc : accountList){
			BI_PL_Target_preparation__c tgtPrep = new BI_PL_Target_preparation__c(
    			BI_PL_Target_customer__r = new Account(External_ID_vod__c = acc.External_Id_vod__c),
			  	BI_PL_Header__r = new BI_PL_Preparation__c(BI_PL_External_id__c = prep.BI_PL_External_ID__c),
			  	BI_PL_External_Id__c = prep.BI_PL_External_Id__c + '_' + acc.External_Id_vod__c
			  
			);
			listTarget.add(tgtPrep);



			


			BI_PL_Channel_detail_preparation__c chanDetail = new BI_PL_Channel_detail_preparation__c(
				BI_PL_Channel__c = SCHANNEL,
	    		BI_PL_Target__r = new BI_PL_Target_preparation__c(BI_PL_External_Id__c = tgtPrep.BI_PL_External_Id__c),
	    		BI_PL_External_Id__c = tgtPrep.BI_PL_External_Id__c + '_' + SCHANNEL,
	    		BI_PL_MSL_flag__c = true
			);

			listChannel.add(chanDetail);

			for(Product_vod__c prod : productList){
				BI_PL_Detail_preparation__c detail = new BI_PL_Detail_preparation__c(
					BI_PL_Product__r = new Product_vod__c(External_ID_vod__c = prod.External_ID_vod__c),
					BI_PL_Adjusted_details__c = 1,
				    BI_PL_External_id__c = chanDetail.BI_PL_External_Id__c + '_' + prod.External_ID_vod__c+'_'+Math.random(),
				    BI_PL_Planned_details__c = 5,
				    BI_PL_Segment__c = 'No Segmentation',
				    //BI_PL_Added_Manually__c = (Math.random() > 0.5) ? true : false,				   
				    BI_PL_Channel_detail__r = new BI_PL_Channel_detail_preparation__c(BI_PL_External_Id__c = chanDetail.BI_PL_External_Id__c)
				    
				);

				listDetails.add(detail);

				BI_PL_Affiliation__c aff = new BI_PL_Affiliation__c(BI_PL_Customer__r = new Account(External_Id_vod__c = acc.External_id_vod__c), BI_PL_Product__r = new Product_vod__c(External_Id_vod__c = prod.External_Id_vod__c), BI_PL_Type__c = 'Information', BI_PL_External_id__c = acc.External_id_vod__c + '_' + prod.External_Id_vod__c + '_Information', BI_PL_Country_code__c = userCountryCode, BI_PL_VCO_Flag__c = 'AARP Part D');
				listAff.add(aff);


			}
		}

		
		for(Product_vod__c prod : productList){
			BI_PL_Affiliation__c aff = new BI_PL_Affiliation__c(BI_PL_Product__r =  new Product_vod__c(External_Id_vod__c = prod.External_Id_vod__c), BI_PL_Selected_vco_flag__c = 'AARP Part D', BI_PL_Vco_name__c = 'VCO Name Test', BI_PL_Selling_message__c = 'Selling Message test', BI_PL_Field_force__c = 'CARD', BI_PL_Type__c = 'VCO', BI_PL_External_Id__c = 'AARP Part D' + prod.External_Id_vod__c + '_VCO', BI_PL_Country_code__c = userCountryCode);
			listAff.add(aff);
		}


		insert listTarget;
		insert listChannel;
		insert listDetails;
		insert listAff;

		List<BI_PL_Affiliation__c> vcos = [SELECT Id, BI_PL_Product__c, BI_PL_Selling_message__c, BI_PL_Vco_name__c,BI_PL_Selected_VCO_Flag__c, BI_PL_Field_force__c,BI_PL_Product__r.Name FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = 'VCO' ];

		Test.startTest();
		Database.executeBatch(new BI_PL_GenerateVCOBatch(cycle,userCountryCode, vcos));
		Test.stopTest();
	


	}

		
}