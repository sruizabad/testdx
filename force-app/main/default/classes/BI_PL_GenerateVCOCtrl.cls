public without sharing class BI_PL_GenerateVCOCtrl {

    @RemoteAction
    public static List<BI_PL_Affiliation__c> getSetupData(String countryCode){

        List<BI_PL_Affiliation__c> aff =[SELECT Id, BI_PL_Selling_message__c, BI_PL_Vco_name__c, BI_PL_Product__c, BI_PL_Selected_VCO_Flag__c,BI_PL_Product__r.External_ID_vod__c, BI_PL_Product__r.Name, BI_PL_Field_force__c FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = 'VCO' AND BI_PL_Country_code__c =: countryCode];

        return aff;


    }

    @RemoteAction
    public static String generateVCO(BI_PL_Cycle__c cycleId, String countryCode, List<BI_PL_Affiliation__c> aff){

        BI_PL_GenerateVCOBatch gvb = new BI_PL_GenerateVCOBatch(cycleId, countryCode, aff);

       String batchId = Database.executeBatch(gvb);
       return batchId;
    }

    @RemoteAction
    public static List<SelectOption> getFlagValues()
    {
      List<SelectOption> options = new List<SelectOption>();
            
       Schema.DescribeFieldResult fieldResult =  BI_PL_Affiliation__c.BI_PL_Selected_VCO_Flag__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
       for( Schema.PicklistEntry f : ple)
       {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       return options;
    }


    @ReadOnly
    @RemoteAction
    public static List<Product_vod__c> getProducts(String cc){



        return [SELECT Id, Name,External_ID_vod__c FROM Product_vod__c WHERE Product_Type_vod__c = 'Detail' AND Country_Code_BI__c =: cc ];
    }

    @RemoteAction
    public static Boolean createVCO(Product_vod__c product, String flag, String sellingMessage, String vcoName, String cc, String fieldForces){

        BI_PL_Affiliation__c aff = new BI_PL_Affiliation__c();
        aff.BI_PL_Selling_message__c = sellingMessage;
        aff.BI_PL_Vco_name__c = vcoName;
        aff.BI_PL_Product__c = product.Id;
        aff.BI_PL_Selected_VCO_Flag__c = flag;
        aff.BI_PL_Type__c = 'VCO';
        aff.BI_PL_Country_code__c = cc;
        aff.BI_PL_External_Id__c = cc+'_VCO_'+product.External_Id_vod__c+'_'+flag;
        aff.BI_PL_Field_force__c = fieldForces;

        upsert aff BI_PL_External_Id__c;

        return true;
    }


    @RemoteAction
    public static Boolean deleteVco(List<BI_PL_Affiliation__c> listAff){
        
        delete listAff;

        return true;
            
    }
    
}