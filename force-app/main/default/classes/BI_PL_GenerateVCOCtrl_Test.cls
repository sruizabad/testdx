@isTest
private class BI_PL_GenerateVCOCtrl_Test {
	private static String userCountryCode;

	@isTest static void test_method_one() {
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		userCountryCode = testUser.Country_Code_BI__c;
		
        BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();
		BI_PL_TestDataFactory.usersCreation(userCountryCode);
		BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
		BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

		testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
		List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

		
		List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
	

		BI_PL_Cycle__c cycle = [SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, Name FROM BI_PL_Cycle__c LIMIT 1];
		List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];

    
             

        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

       /* List<BI_PL_Affiliation__c> affiliationToInsert = new List<BI_PL_Affiliation__c>();
        for(Account a : listAcc){
        	for(Product_vod__c p : listProd){
        		BI_PL_Affiliation__c newAff = new BI_PL_Affiliation__c(BI_PL_Type__c = 'Information', BI_PL_Country_code__c = userCountryCode, BI_PL_Customer__c = a.Id, BI_PL_Product__c = p.Id, BI_PL_VCO_Flag__c = 'AARP');
        		affiliationToInsert.add(newAff);
        	}
        }

        insert affiliationToInsert;*/

        BI_PL_GenerateVCOCtrl.createVCO(listProd.get(0),'AARP Part D','SM Test','VCO Name Test',userCountryCode,'CARD');

        List<BI_PL_Affiliation__c> listVCo = BI_PL_GenerateVCOCtrl.getSetupData(userCountryCode);
        System.assertEquals(1,listVCo.size());


        List<Product_vod__c> testProd = BI_PL_GenerateVCOCtrl.getProducts(userCountryCode);
        System.assertEquals(4,testProd.size());

        List<SelectOption> flagValues = BI_PL_GenerateVCOCtrl.getFlagValues();
        Test.startTest();
        BI_PL_GenerateVCOCtrl.generateVCO(cycle, userCountryCode, listVCo);
        Test.stopTest();

        BI_PL_GenerateVCOCtrl.deleteVco(listVCo);
       
        


	
	}
	
	
	
}