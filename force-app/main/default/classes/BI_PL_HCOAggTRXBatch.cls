/**
 *	Sum for each HCO the Net sales data for each product.
 *
 *	Used for merged accounts.
 *	@author	OMEGA CRM
 */

global without sharing class BI_PL_HCOAggTRXBatch extends BI_PL_NightSubbatch {

	public static final String TYPE_SUMTRX = 'SumTRX';
	public static final String TYPE_RELATIONSHIP = 'Relationship';
	public static final String TYPE_SALES = 'Sales';
	public static final String TYPE_SUMNETSALES = 'SumNetSales';

	private static final String TRX_AFFILIATION_FIELD = 'BI_PL_Sales_rx__c';
	private static final String NET_SALES_AFFILIATION_FIELD = 'BI_PL_Product_Sales__c';

	Map<String, String> mappingTypeByField = new Map<String, String> {
		TRX_AFFILIATION_FIELD => TYPE_SUMTRX,
		NET_SALES_AFFILIATION_FIELD => TYPE_SUMNETSALES
	};

	List<String> errors = new List<String>();

	global BI_PL_HCOAggTRXBatch(String countryCode, BI_PL_NightSubbatch nextBatchToExecute) {
		super(countryCode, nextBatchToExecute);
	}

	global override Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('SELECT Id, BI_PL_Customer__c, BI_PL_Parent__c FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = \'' + TYPE_RELATIONSHIP + '\' AND BI_PL_Parent__c != null AND BI_PL_Country_code__c = \'' + countryCode + '\' AND BI_PL_Customer__r.IsPersonAccount = true');
	}

	global override void execute(Database.BatchableContext BC, List<sObject> scope) {

		try {
			// 1.- For each HCO find its HCP:
			Map<Id, Id> hcoByHCP = new Map<Id, Id>();
			for (BI_PL_Affiliation__c aff : (List<BI_PL_Affiliation__c>)scope) {
				hcoByHCP.put(aff.BI_PL_Customer__c, aff.BI_PL_Parent__c);
			}

			// 2.- Retrieve the HCP's Affiliation Sales records to get the TRX value per product and HCP:
			/*Map<Id, Map<String, Decimal>> sumByProductAndHCOTrx = new Map<Id, Map<String, Decimal>>();
			Map<String, Set<Id>> alreadyAddedHCPsByProductTrx = new Map<String, Set<Id>>();

			Map<Id, Map<String, Decimal>> sumByProductAndHCONetSales = new Map<Id, Map<String, Decimal>>();
			Map<String, Set<Id>> alreadyAddedHCPsByProductNetSales = new Map<String, Set<Id>>();*/


			Map<String, Map<Id, Map<String, Decimal>>> sumByProductAndHCOAndField = new Map<String, Map<Id, Map<String, Decimal>>>();
			Map<String, Map<String, Set<Id>>> alreadyAddedHCPsByProductField = new Map<String, Map<String, Set<Id>>>();


			/*List<BI_PL_Affiliation__c> salesAffs = Database.query('SELECT Id, ' + TRX_AFFILIATION_FIELD + ', ' + NET_SALES_AFFILIATION_FIELD + ',BI_PL_Customer__c, BI_PL_Customer__r.Name, BI_PL_Product__c, BI_PL_Product__r.Name FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = \''+TYPE_SALES+'\' AND BI_PL_Customer__c IN : hcoByHCP.keySet() AND (' + TRX_AFFILIATION_FIELD + ' != null OR ' + NET_SALES_AFFILIATION_FIELD + '!=null)');

			Set<Id> productIds = new Set<Id>();

			for (BI_PL_Affiliation__c a : salesAffs) {
				productIds.add(a.BI_PL_Product__c);
			}*/


			List<Id> hcpIds = new List<Id>(hcoByHCP.keySet());

			// Build the sales affiliation query:
			String query = 'SELECT Id';

			for (String field : mappingTypeByField.keySet()) {
				query += ', ' + field;
			}

			query += ', BI_PL_Customer__c, BI_PL_Customer__r.Name, BI_PL_Product__c, BI_PL_Product__r.Name FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = \'' + TYPE_SALES + '\'';
			query += ' AND BI_PL_Customer__c IN : hcpIds AND';

			query += ' (';

			List<String> fields = new List<String>(mappingTypeByField.keySet());

			for (Integer i = 0; i < fields.size(); i++) {
				query += fields.get(i) + ' != null';
				if (i < fields.size() - 1)
					query += ' OR ';
			}

			query += ')';


			System.debug('Query: ' + query);


			//Database.query('SELECT Id, ' + TRX_AFFILIATION_FIELD + ', ' + NET_SALES_AFFILIATION_FIELD + ', BI_PL_Customer__c, BI_PL_Customer__r.Name, BI_PL_Product__c, BI_PL_Product__r.Name FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = \'' + TYPE_SALES + '\' AND BI_PL_Customer__c IN : hcoByHCP.keySet() AND (' + TRX_AFFILIATION_FIELD + ' != null OR ' + NET_SALES_AFFILIATION_FIELD + ' != null)')

			for (BI_PL_Affiliation__c affiliation : Database.query(query)) {

				String productKey = affiliation.BI_PL_Product__c;
				String hcoId = hcoByHCP.get(affiliation.BI_PL_Customer__c);

				if (hcoId != null && productKey != null) {
					/*
									if (a.get(TRX_AFFILIATION_FIELD) != null) {

										if (!sumByProductAndHCOTrx.containsKey(hcoId))
											sumByProductAndHCOTrx.put(hcoId, new Map<String, Decimal>());

										if (!sumByProductAndHCOTrx.get(hcoId).containsKey(productKey))
											sumByProductAndHCOTrx.get(hcoId).put(productKey, 0);

										// Increase the value:
										// Do not increase the value if already added for the product and HCP:

										if (alreadyAddedHCPsByProductTrx.containsKey(productKey) && alreadyAddedHCPsByProductTrx.get(productKey).contains(a.BI_PL_Customer__c)) {
											errors.add('There\'s duplicated sales TRX data for the customer \'' + a.BI_PL_Customer__r.Name + '\' and the product \'' + a.BI_PL_Product__r.Name + '\'');
											continue;
										}

										sumByProductAndHCOTrx.get(hcoId).put(productKey, sumByProductAndHCOTrx.get(hcoId).get(productKey) + (Decimal)a.get(TRX_AFFILIATION_FIELD));

										if (!alreadyAddedHCPsByProductTrx.containsKey(productKey))
											alreadyAddedHCPsByProductTrx.put(productKey, new Set<Id>());

										alreadyAddedHCPsByProductTrx.get(productKey).add(a.BI_PL_Customer__c);
									}*/

					incrementFieldMap(TRX_AFFILIATION_FIELD, sumByProductAndHCOAndField, alreadyAddedHCPsByProductField, productKey, hcoId, affiliation);

					incrementFieldMap(NET_SALES_AFFILIATION_FIELD, sumByProductAndHCOAndField, alreadyAddedHCPsByProductField, productKey, hcoId, affiliation);
					/*
									if (a.get(NET_SALES_AFFILIATION_FIELD) != null) {

										if (!sumByProductAndHCONetSales.containsKey(hcoId))
											sumByProductAndHCONetSales.put(hcoId, new Map<String, Decimal>());

										if (!sumByProductAndHCONetSales.get(hcoId).containsKey(productKey))
											sumByProductAndHCONetSales.get(hcoId).put(productKey, 0);

										// Increase the value:
										// Do not increase the value if already added for the product and HCP:

										if (alreadyAddedHCPsByProductNetSales.containsKey(productKey) && alreadyAddedHCPsByProductNetSales.get(productKey).contains(a.BI_PL_Customer__c)) {
											errors.add('There\'s duplicated Net Sales data for the customer \'' + a.BI_PL_Customer__r.Name + '\' and the product \'' + a.BI_PL_Product__r.Name + '\'');
											continue;
										}

										sumByProductAndHCONetSales.get(hcoId).put(productKey, sumByProductAndHCONetSales.get(hcoId).get(productKey) + (Decimal)a.get(NET_SALES_AFFILIATION_FIELD));

										if (!alreadyAddedHCPsByProductNetSales.containsKey(productKey))
											alreadyAddedHCPsByProductNetSales.put(productKey, new Set<Id>());

										alreadyAddedHCPsByProductNetSales.get(productKey).add(a.BI_PL_Customer__c);
									}*/
				}
			}

			// 3.- Update the Sum TRX for the HCOs:
			List<BI_PL_Affiliation__c> toUpdate = new List<BI_PL_Affiliation__c>();

			for (String field : mappingTypeByField.keySet()) {
				for (Id hco : sumByProductAndHCOAndField.get(field).keySet()) {
					for (String productKey : sumByProductAndHCOAndField.get(field).get(hco).keySet()) {
						String external = hco + '_' + productKey + '_' + mappingTypeByField.get(field);

						BI_PL_Affiliation__c aff = new BI_PL_Affiliation__c(BI_PL_External_id__c = external,
						        BI_PL_Customer__c = hco,
						        //BI_PL_HCO_Agg_TRX__c = sumByProductAndHCOAndField.get(field).get(hco).get(productKey),
						        BI_PL_Country_code__c = countryCode,
						        BI_PL_Product__c = productKey,
						        BI_PL_Type__c = mappingTypeByField.get(field));

						aff.put(field, getSum( field,  hco , productKey,  sumByProductAndHCOAndField));

						toUpdate.add(aff);

					}
				}

				/*
				for (Id hco : sumByProductAndHCONetSales.keySet()) {
					for (String productKey : sumByProductAndHCONetSales.get(hco).keySet()) {
						String external = hco + '_' + productKey + '_' + TYPE_SUMNETSALES;

						toUpdate.add(new BI_PL_Affiliation__c(BI_PL_External_id__c = external,
						                                      BI_PL_Customer__c = hco,
						                                      BI_PL_HCO_Agg_Net_sales__c = sumByProductAndHCONetSales.get(hco).get(productKey),
						                                      BI_PL_Country_code__c = countryCode,
						                                      BI_PL_Product__c = productKey,
						                                      BI_PL_Type__c = TYPE_SUMNETSALES));
					}
				}
				*/
			}

			upsert toUpdate BI_PL_External_id__c;



		} catch (Exception error) {

			BI_PL_ErrorHandlerUtility.addPlanitServerError('BI_PL_HCOAggTRXBatch', BI_PL_ErrorHandlerUtility.FUNCTIONALITY_AGGBATCH, error.getMessage(), error.getStackTraceString());
		}
		// ----------------------------- //













		/*

				List<Account> hcos = (List<Account>)scope;
				List<Id> hcoIds = new List<Id>();

				for (Account a : hcos)
					hcoIds.add(a.Id);

				// 1.- For each HCO find its HCP:
				Map<Id, Id> hcoByHCP = new Map<Id, Id>();

				for (BI_PL_Affiliation__c a : [SELECT Id, BI_PL_Customer__c, BI_PL_Parent__c FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = :TYPE_RELATIONSHIP AND BI_PL_Parent__c IN: hcoIds]) {
					hcoByHCP.put(a.BI_PL_Customer__c, a.BI_PL_Parent__c);
				}

				List<Id> hcpIds = new List<Id>(hcoByHCP.keySet());

				// 2.- Retrieve the HCP's Affiliation Sales records to get the TRX value per product and HCP:

				Map<Id, Map<String, Decimal>> sumByProductAndHCOTrx = new Map<Id, Map<String, Decimal>>();
				Map<String, Set<Id>> alreadyAddedHCPsByProductTrx = new Map<String, Set<Id>>();

				Map<Id, Map<String, Decimal>> sumByProductAndHCONetSales = new Map<Id, Map<String, Decimal>>();
				Map<String, Set<Id>> alreadyAddedHCPsByProductNetSales = new Map<String, Set<Id>>();

				String q = 'SELECT Id, ' + TRX_AFFILIATION_FIELD + ', ' + NET_SALES_AFFILIATION_FIELD + ',BI_PL_Customer__c, BI_PL_Customer__r.Name, BI_PL_Product__c, BI_PL_Product__r.Name FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c = :TYPE_SALES AND BI_PL_Customer__c IN : hcpIds AND (' + TRX_AFFILIATION_FIELD + ' != null OR ' + NET_SALES_AFFILIATION_FIELD + '!=null)';

				for (BI_PL_Affiliation__c a : Database.query(q)) {

					String productKey = a.BI_PL_Product__c;
					String hcoId = hcoByHCP.get(a.BI_PL_Customer__c);

					if (hcoId != null && productKey != null) {

						if (a.get(TRX_AFFILIATION_FIELD) != null) {

							if (!sumByProductAndHCOTrx.containsKey(hcoId))
								sumByProductAndHCOTrx.put(hcoId, new Map<String, Decimal>());

							if (!sumByProductAndHCOTrx.get(hcoId).containsKey(productKey))
								sumByProductAndHCOTrx.get(hcoId).put(productKey, 0);

							// Increase the value:
							// Do not increase the value if already added for the product and HCP:

							if (alreadyAddedHCPsByProductTrx.containsKey(productKey) && alreadyAddedHCPsByProductTrx.get(productKey).contains(a.BI_PL_Customer__c)) {
								errors.add('There\'s duplicated sales TRX data for the customer \'' + a.BI_PL_Customer__r.Name + '\' and the product \'' + a.BI_PL_Product__r.Name + '\'');
								continue;
							}

							sumByProductAndHCOTrx.get(hcoId).put(productKey, sumByProductAndHCOTrx.get(hcoId).get(productKey) + (Decimal)a.get(TRX_AFFILIATION_FIELD));

							if (!alreadyAddedHCPsByProductTrx.containsKey(productKey))
								alreadyAddedHCPsByProductTrx.put(productKey, new Set<Id>());

							alreadyAddedHCPsByProductTrx.get(productKey).add(a.BI_PL_Customer__c);
						}

						if (a.get(NET_SALES_AFFILIATION_FIELD) != null) {

							if (!sumByProductAndHCONetSales.containsKey(hcoId))
								sumByProductAndHCONetSales.put(hcoId, new Map<String, Decimal>());

							if (!sumByProductAndHCONetSales.get(hcoId).containsKey(productKey))
								sumByProductAndHCONetSales.get(hcoId).put(productKey, 0);

							// Increase the value:
							// Do not increase the value if already added for the product and HCP:

							if (alreadyAddedHCPsByProductNetSales.containsKey(productKey) && alreadyAddedHCPsByProductNetSales.get(productKey).contains(a.BI_PL_Customer__c)) {
								errors.add('There\'s duplicated Net Sales data for the customer \'' + a.BI_PL_Customer__r.Name + '\' and the product \'' + a.BI_PL_Product__r.Name + '\'');
								continue;
							}

							sumByProductAndHCONetSales.get(hcoId).put(productKey, sumByProductAndHCONetSales.get(hcoId).get(productKey) + (Decimal)a.get(NET_SALES_AFFILIATION_FIELD));

							if (!alreadyAddedHCPsByProductNetSales.containsKey(productKey))
								alreadyAddedHCPsByProductNetSales.put(productKey, new Set<Id>());

							alreadyAddedHCPsByProductNetSales.get(productKey).add(a.BI_PL_Customer__c);
						}

					} else {
						// 'hcoId' is null, which means the HCP has no parent HCO
						// or
						// 'productKey' is null, which mean the Affiliation record is not properly filled.

					}
				}

				// 3.- Update the Sum TRX for the HCOs:
				List<BI_PL_Affiliation__c> toUpdate = new List<BI_PL_Affiliation__c>();

				for (Id hco : sumByProductAndHCOTrx.keySet()) {
					for (String productKey : sumByProductAndHCOTrx.get(hco).keySet()) {
						String external = hco + '_' + productKey + '_' + TYPE_SUMTRX;

						toUpdate.add(new BI_PL_Affiliation__c(BI_PL_External_id__c = external,
						                                      BI_PL_Customer__c = hco,
						                                      BI_PL_HCO_Agg_TRX__c = sumByProductAndHCOTrx.get(hco).get(productKey),
						                                      BI_PL_Country_code__c = countryCode,
						                                      BI_PL_Product__c = productKey,
						                                      BI_PL_Type__c = TYPE_SUMTRX));
					}
				}

				for (Id hco : sumByProductAndHCONetSales.keySet()) {
					for (String productKey : sumByProductAndHCONetSales.get(hco).keySet()) {
						String external = hco + '_' + productKey + '_' + TYPE_SUMNETSALES;

						toUpdate.add(new BI_PL_Affiliation__c(BI_PL_External_id__c = external,
						                                      BI_PL_Customer__c = hco,
						                                      BI_PL_HCO_Agg_Net_sales__c = sumByProductAndHCONetSales.get(hco).get(productKey),
						                                      BI_PL_Country_code__c = countryCode,
						                                      BI_PL_Product__c = productKey,
						                                      BI_PL_Type__c = TYPE_SUMNETSALES));
					}
				}




				upsert toUpdate BI_PL_External_id__c;*/
	}

	private void incrementFieldMap(String fieldName, Map<String, Map<Id, Map<String, Decimal>>> aggregatedMap, Map<String, Map<String, Set<Id>>> alreadyAddedHCPsByProduct, String productKey, String hcoId, BI_PL_Affiliation__c affiliation) {
		if (affiliation.get(fieldName) != null) {

			// Check if there's data for the specified field:
			if (!aggregatedMap.containsKey(fieldName))
				aggregatedMap.put(fieldName, new Map<Id, Map<String, Decimal>>());

			// Check if there's HCO data for the current field:
			if (!hasHCOData(hcoId, fieldName, aggregatedMap))
				aggregatedMap.get(fieldName).put(hcoId, new Map<String, Decimal>());

			// Check if there's product Data for the current HCO and field:
			if (!hasProductData(hcoId, fieldName, productKey, aggregatedMap))
				aggregatedMap.get(fieldName).get(hcoId).put(productKey, 0);

			// Increase the value:
			// Do not increase the value if already added for the product and HCP:

			if (!alreadyAddedHCPsByProduct.containsKey(fieldName))
				alreadyAddedHCPsByProduct.put(fieldName, new Map<String, Set<Id>>());

			if (alreadyAddedHCPsByProduct.get(fieldName).containsKey(productKey) && alreadyAddedHCPsByProduct.get(fieldName).get(productKey).contains(affiliation.BI_PL_Customer__c)) {
				errors.add('There\'s duplicated sales TRX data for the customer \'' + affiliation.BI_PL_Customer__r.Name + '\' and the product \'' + affiliation.BI_PL_Product__r.Name + '\'');
				return;
			}

			aggregatedMap.get(fieldName).get(hcoId).put(productKey, aggregatedMap.get(fieldName).get(hcoId).get(productKey) + (Decimal)affiliation.get(fieldName));

			if (!alreadyAddedHCPsByProduct.get(fieldName).containsKey(productKey))
				alreadyAddedHCPsByProduct.get(fieldName).put(productKey, new Set<Id>());

			alreadyAddedHCPsByProduct.get(fieldName).get(productKey).add(affiliation.BI_PL_Customer__c);
		}
	}

	private Boolean hasHCOData(Id hcoId, String field, Map<String, Map<Id, Map<String, Decimal>>> aggregatedMap) {
		return aggregatedMap.containsKey(field) && aggregatedMap.get(field).containsKey(hcoId);
	}
	private Boolean hasProductData(Id hcoId, String field, String product, Map<String, Map<Id, Map<String, Decimal>>> aggregatedMap) {
		return aggregatedMap.containsKey(field) && aggregatedMap.get(field).containsKey(hcoId) && aggregatedMap.get(field).get(hcoId).containsKey(product);
	}


	global override void finish(Database.BatchableContext BC) {
		super.finish(BC);

		if (errors.size() > 0) {
			sendErrorEmail();
		}
	}

	protected override String generateErrorEmailBody() {
		String body = '';
		body = Label.BI_PL_Process_finished_following_errors;
		body += '<br/><ul>';
		for (String s : errors) {
			body += '<li/>' + s + '</li>';
		}
		body += '</ul>';
		body = '<h2>' + generateErrorEmailSubject() + '</h2>' + '<br/><br/>' + body;

		return body;
	}

	protected override String generateErrorEmailSubject() {
		return Label.BI_PL_Duplicated_TRX_data;
	}

	private Decimal getSum(String field, Id hco , String productKey, Map<String, Map<Id, Map<String, Decimal>>> sumByProductAndHCOAndField ) {

		if (sumByProductAndHCOAndField.containsKey(field)) {
			if (sumByProductAndHCOAndField.get(field).containsKey(hco)) {
				if (sumByProductAndHCOAndField.get(field).get(hco).containsKey(productKey)) {
					return sumByProductAndHCOAndField.get(field).get(hco).get(productKey);
				}
			}
		}
		return null;
	}
}