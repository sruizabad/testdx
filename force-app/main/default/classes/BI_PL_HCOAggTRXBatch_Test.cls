@isTest
private class BI_PL_HCOAggTRXBatch_Test {
	
	private static final String STYPERELATIONSHIP = BI_PL_HCOAggTRXBatch.TYPE_RELATIONSHIP;
	private static final String STYPE_SALES = BI_PL_HCOAggTRXBatch.TYPE_SALES;
	private static final String STYPE_SUMTRX = BI_PL_HCOAggTRXBatch.TYPE_SUMTRX;
	private static final String STYPE_SUMNETSALES = BI_PL_HCOAggTRXBatch.TYPE_SUMNETSALES;

	private static String userCountryCode;

	@testSetup  static void setup() {

	    userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);

        List<Product_vod__c> listProd = BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
        List<Account> listAcc = BI_PL_TestDataFactory.createTestPersonAccounts(5, userCountryCode);

		BI_PL_TestDataFactory.createAffiliationSI(STYPE_SALES, listAcc, listProd, userCountryCode);		
		BI_PL_TestDataFactory.createAffiliationRelationship(listAcc, listAcc.get(0), userCountryCode);
	}

	@isTest static void test_method_one() {
		
		userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
		
        List<BI_PL_Affiliation__c> lstAff = [SELECT Id, BI_PL_External_id__c, BI_PL_Customer__r.Name, BI_PL_Country_code__c, BI_PL_Product__c, BI_PL_Type__c FROM BI_PL_Affiliation__c];
        system.debug('## lstAff: ' + lstAff);
        System.Assert(lstAff != null && !lstAff.isEmpty() );

        Test.startTest();

        Database.executeBatch(new BI_PL_HCOAggTRXBatch(userCountryCode, null ) );

        Test.stopTest();

        List<BI_PL_Affiliation__c> lstAffAfterBatch = [SELECT Id, BI_PL_External_id__c, BI_PL_Customer__r.Name, BI_PL_Country_code__c, BI_PL_Product__c, BI_PL_Type__c FROM BI_PL_Affiliation__c];
        
        System.Assert(lstAffAfterBatch != null && !lstAffAfterBatch.isEmpty() );
        System.Assert(lstAffAfterBatch.size() > lstAff.size());

        set<String> setAffType = new set<String>();
        for (BI_PL_Affiliation__c inAff : lstAffAfterBatch){
        	setAffType.add(inAff.BI_PL_Type__c);
        }

        system.assert(setAffType.contains(STYPE_SUMTRX) );
        system.assert(setAffType.contains(STYPE_SUMNETSALES) );
	}
}