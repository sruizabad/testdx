/**
 *	Sum for each HCO the Net sales data for each product.
 *
 *	Used for merged accounts.
 *	@author	OMEGA CRM
 */

global without sharing class BI_PL_HCOAggregatedBatch extends BI_PL_NightSubbatch {
	String query;

	//Planit proccess
	global BI_PL_HCOAggregatedBatch() {

	}

	//Night sub-batch
	global BI_PL_HCOAggregatedBatch(String countryCode, BI_PL_NightSubbatch nextBatchToExecute) {
		super(countryCode, nextBatchToExecute);
	}

	global override Database.QueryLocator start(Database.BatchableContext BC) {
		//Planit process
		countryCode = this.params.containsKey('countryCode') ? (String) this.params.get('countryCode') : countryCode;

		query = 'SELECT Id FROM BI_PL_Affiliation__c WHERE BI_PL_Type__c IN (\'' + BI_PL_HCOAggTRXBatch.TYPE_SUMTRX+'\',\''+ BI_PL_HCOAggTRXBatch.TYPE_SUMNETSALES+ '\') AND BI_PL_Country_code__c = \'' + countryCode + '\'';
		return Database.getQueryLocator(query);
	}

	global override void execute(Database.BatchableContext BC, List<sObject> scope) {
		delete scope;
	}

	global override void finish(Database.BatchableContext BC) {
		super.finish(BC);

		Database.executeBatch(new BI_PL_HCOAggTRXBatch(countryCode, nextBatchToExecute), 10);
	}

}