@isTest
private class BI_PL_HCOAggregatedBatch_Test {

	private static String userCountryCode;

	@testSetup static void setup() {

        userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
        
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);

        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

	}

	@isTest static void BI_PL_HCOAggregatedBatch(){


		List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];
		userCountryCode = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()].Country_Code_BI__c;

		BI_PL_TestDataFactory.createAffiliationRelationship(listAcc, listAcc.get(0), userCountryCode);

		List<BI_PL_Affiliation__c> 	listToInsert = new List<BI_PL_Affiliation__c>();

		BI_PL_Affiliation__c newAff = new BI_PL_Affiliation__c(
			BI_PL_Customer__c = listAcc.get(0).Id,
			BI_PL_Type__c = BI_PL_HCOAggTRXBatch.TYPE_SUMTRX,
			BI_PL_Country_code__c = userCountryCode,
			BI_PL_Field_force__c = 'FieldForce ' + listAcc.get(0).Id
		);
		insert newAff;

		Test.startTest();
		BI_PL_HCOAggregatedBatch hcoTest = new BI_PL_HCOAggregatedBatch();
		Database.executeBatch(new BI_PL_HCOAggregatedBatch(userCountryCode,null));
		Test.stopTest();
	}


}