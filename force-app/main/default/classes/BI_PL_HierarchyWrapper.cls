public with sharing class BI_PL_HierarchyWrapper {

	
    public static BI_PL_HierarchyNodeWrapper getUserHierarchy(String cycleId, String hierarchy, String userId) {
        return getHierarchy(cycleId, hierarchy).getUserSubhierarchy(userId);
    }

    public static BI_PL_HierarchyNodeWrapper getPositionHierarchy(String cycleId, String hierarchy, String position) {
        return getHierarchy(cycleId, hierarchy).getPositionSubhierarchy(position);
    }

    public static BI_PL_HierarchyNodeWrapper getHierarchy(String cycleId, String hierarchy) {
        return getHierarchy( cycleId,  hierarchy,  true);
    }

    public static BI_PL_HierarchyNodeWrapper getHierarchy(String cycleId, String hierarchy, Boolean withPerm) {
        BI_PL_HierarchyNodeWrapper root = new BI_PL_HierarchyNodeWrapper();
        Map<String, BI_PL_HierarchyNodeWrapper> nodesById = new Map<String, BI_PL_HierarchyNodeWrapper>();
        Map<String, String> perms = new Map<String, String>();

        if(withPerm)
            for (PermissionSetAssignment psa : [SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment 
                WHERE AssigneeId IN (SELECT BI_PL_User__c FROM BI_PL_Position_cycle_user__c 
                    WHERE BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId AND BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchy)
                AND PermissionSet.Name LIKE 'BI_PL_%']) {
                if(!perms.containsKey(psa.PermissionSet.Name))
                    perms.put(psa.AssigneeId, psa.PermissionSet.Name);
            }

        for(BI_PL_Position_cycle__c posc : [SELECT Id, BI_PL_Position__c, BI_PL_Position__r.Name, BI_PL_Parent_position__c, BI_PL_Parent__c, BI_PL_Hierarchy_level__c, BI_PL_Confirmed__c,
            (SELECT Id, BI_PL_User__c,BI_PL_User__r.Name, BI_PL_Preparation_owner__c FROM BI_PL_Position_cycle_users__r WHERE BI_PL_User__r.IsActive = true),
            (SELECT Id, Owner.Name, BI_PL_Status__c FROM PLANiT_Preparations__r)
            FROM BI_PL_Position_cycle__c 
            WHERE BI_PL_Cycle__c = :cycleId 
            AND BI_PL_Hierarchy__c = :hierarchy
            ORDER BY BI_PL_Hierarchy_level__c ASC NULLS FIRST]) {

            if(posc.BI_PL_Hierarchy_level__c == 1) {
                if(root.record != null) {
                    throw new BI_PL_Exception('Hierarchy has more than one root nodes');
                }
                root = new BI_PL_HierarchyNodeWrapper(posc);
                if(withPerm) root.repPermission = perms.get(root.repId);
                nodesById.put(posc.Id, root);
            } else {
                BI_PL_HierarchyNodeWrapper node = new BI_PL_HierarchyNodeWrapper(posc);
                if(withPerm) node.repPermission = perms.get(node.repId);
                nodesById.put(posc.Id, node); 
                BI_PL_HierarchyNodeWrapper parentNode = nodesById.get(posc.BI_PL_Parent__c);
                if(parentNode != null) parentNode.addChild(node);
            }

        }
        return root;
    }

    //public static List<BI_PL_position_cycle__c> getPositionCylesBelowOfPosition(String posId) {
    //    return [SELECT Id, BI_PL_Position__c FROM BI_PL_position_cycle__c 
    //        WHERE BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Position__c = :posId
    //        OR BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Parent__r.BI_PL_Position__c = :posId]
    //}


    public class BI_PL_HierarchyNodeWrapper {
        //For lookups
        public String Name;
        public Id repId;
        public String repPermission;
        public Id positionId;
        public String repName;

        public BI_PL_Position_cycle__c record;
        public List<BI_PL_HierarchyNodeWrapper> children;
        public Boolean isCurrentUsersNode = false;
        //public String positionName;
        //public Id parentPositionId;
        //public Boolean isConfirmed;
        //public String rejectedReason;
        //public Id positionCycleId;
        //public List<String> childrenIds = new List<String>();

        public BI_PL_HierarchyNodeWrapper getPositionSubhierarchy(String positionId) {
            if(positionId == null) return this;
            System.debug(loggingLevel.Error, '*** getPositionSubhierarchy positionId: ' + positionId);
            if(this.record.BI_PL_Position__c == positionId){
                return this;
            } else if(!this.children.isEmpty()) {
                for(BI_PL_HierarchyNodeWrapper child : this.children) {
                    BI_PL_HierarchyNodeWrapper found = child.getPositionSubhierarchy(positionId);
                    System.debug(loggingLevel.Error, '*** getPositionSubhierarchy found: ' + found);
                    if(found != null) return found;
                }
            }

            return null;

        }

        public BI_PL_HierarchyNodeWrapper getUserSubhierarchy(String userId) {
            if(userId == null) return this;
            System.debug(loggingLevel.Error, '*** getSubhierarchy userId: ' + userId);
            if(this.repId == userId){
                return this;
            } else if(!this.children.isEmpty()) {
                for(BI_PL_HierarchyNodeWrapper child : this.children) {
                    BI_PL_HierarchyNodeWrapper found = child.getUserSubhierarchy(userId);
                    System.debug(loggingLevel.Error, '*** getSubhierarchy found: ' + found);
                    if(found != null) return found;
                }
            }

            return null;

        }

        public List<String> getPositionIdList() {

            List<String> toret = new List<String>();

            toRet.add(this.record.BI_PL_Position__c);

            if(!this.children.isEmpty()) {
                for(BI_PL_HierarchyNodeWrapper child : this.children) {
                    toRet.addAll(child.getPositionIdList());
                }
            }

            return toRet;
        }

        public BI_PL_HierarchyNodeWrapper () {
            this.record = null;
        }

        public BI_PL_HierarchyNodeWrapper (BI_PL_Position_cycle__c pc) {
            this.children = new List<BI_PL_HierarchyNodeWrapper>();
            this.record = pc;
            this.positionId = pc.BI_PL_Position__c;
            for(BI_PL_Position_cycle_user__c pcu : pc.BI_PL_Position_cycle_users__r) {
                this.repId = pcu.BI_PL_User__c;
                this.repName = pcu.BI_PL_User__r.Name;
                if(pcu.BI_PL_Preparation_owner__c) {
                    break;
                }

            }
            this.Name = pc.BI_PL_Position__r.Name + ' ' + this.repName;
            this.isCurrentUsersNode = (this.repId == UserInfo.getUserId());
        }

        public void addChild(BI_PL_HierarchyNodeWrapper childNode) {
            this.children.add(childNode);
        }
    }
}