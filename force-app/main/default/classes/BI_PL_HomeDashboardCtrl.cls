/**
 *  17/10/2017
 *  - GLOS-427: Single specialty field at target preparation level
 *	@author	OMEGA CRM
 */

public without sharing class BI_PL_HomeDashboardCtrl {
	public BI_PL_HomeDashboardCtrl() {

	}

	/**
	 * @author	OMEGA CRM
	 * @return  The setup data.
	 */
	@RemoteAction
	public static PlanitSetupModel getSetupData() {
		PlanitSetupModel setup = new PlanitSetupModel();

		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		setup.userCountryCode = currentUser.Country_Code_BI__c;

		BI_PL_Cycle__c[] cycles = new List<BI_PL_Cycle__c>([SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, Name  FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :setup.userCountryCode ORDER BY BI_PL_Start_date__c]);

		//Get current cycle
		BI_PL_Cycle__c[] cycle = new List<BI_PL_Cycle__c>([SELECT Id FROM BI_PL_Cycle__c WHERE BI_PL_Start_date__c > TODAY AND BI_PL_Country_code__c = :setup.userCountryCode ORDER BY BI_PL_Start_date__c LIMIT 1]);
		String currentCycleId = '';
		if (cycle.size() > 0)
			currentCycleId = cycle.get(0).Id;

		//Permissions and permission sets
		Map<String, Boolean> permissionsMap = BI_PL_PreparationServiceUtility.getCurrentUserPermissions();
		setup.isSR = permissionsMap.get('isSR');
		setup.isSM = permissionsMap.get('isSM');
		setup.isDS = permissionsMap.get('isDS');
		setup.cycles = cycles;
		setup.currentCycleId = currentCycleId;


		return setup;
	}

	/**
	 *	Returns a map where each position is a hierarchy node visible for the current user (grouped by position id) and the
	 *	hierarchy root node.
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static PLANiTHierarchyNodesWrapper getHierarchyNodes(String countryCode, String cycleId, String hierarchyName) {
		Map<String, PLANiTTreeNode> hierarchy = new Map<String, PLANiTTreeNode>();
		//Get current cycle
		//BI_PL_Cycle__c[] cycles = new List<BI_PL_Cycle__c>([SELECT Id FROM BI_PL_Cycle__c WHERE BI_PL_Start_date__c > TODAY
		//        AND BI_PL_Country_code__c = :countryCode ORDER BY BI_PL_Start_date__c LIMIT 1]);
		//String currentCycleId = cycles.get(0).Id;

		Map<String, BI_PL_PositionCycleWrapper> tree = new BI_PL_VisibilityTree(cycleId, hierarchyName).getTree();

		String rootNodeId;
		List<String> treeList = new List<String>(tree.keySet());

		for (Integer i = 0; i < treeList.size(); i++) {
			String positionId =  treeList.get(i);
			PLANiTTreeNode node = new PLANiTTreeNode(tree.get(positionId));
			hierarchy.put(positionId, node);

			if (node.isCurrentUsersNode)
				rootNodeId = node.positionId;
		}

		return new PLANiTHierarchyNodesWrapper(hierarchy, rootNodeId);
	}

	/**
	 *	Returns:
	 *	- The future closest cycle.
	 *	- The preparations visible for the current user for the closest future cycle.
	 *	- The hierarchy options available for the cycle and the user's visibility.
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static PLANiTPreparationsWrapper getPreparations(String countryCode, String cycleId) {
		//Get current cycle
		//BI_PL_Cycle__c[] cycles = new List<BI_PL_Cycle__c>([SELECT Id FROM BI_PL_Cycle__c WHERE BI_PL_Start_date__c > TODAY
		//        AND BI_PL_Country_code__c = :countryCode ORDER BY BI_PL_Start_date__c LIMIT 1]);
		//String currentCycleId = cycles.get(0).Id;


		Set<SelectOption> hierarchyOptions = new Set<SelectOption>();
		String linkedHierarchy = '';

		List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();
		for (BI_PL_Preparation__c p : [SELECT Id, Name, BI_PL_Position_cycle__r.BI_PL_Position__c,
		                               BI_PL_Position_cycle__r.BI_PL_Hierarchy__c
		                               FROM BI_PL_Preparation__c
		                               WHERE BI_PL_Country_code__c = :countryCode
		                                       AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId]) {
			preparations.add(p);
			//hierarchyOptions.add(new SelectOption(p.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, p.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c));
			
		}

		List<BI_PL_Position_cycle_user__c> hierarchies = [SELECT BI_PL_User__c, BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c FROM BI_PL_Position_cycle_user__c WHERE BI_PL_User__c =:UserInfo.getUserId() AND  BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycleId LIMIT 1];
        
        for(BI_PL_Position_cycle_user__c hierarchy : hierarchies)
        {
            hierarchyOptions.add(new SelectOption(hierarchy.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, hierarchy.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c));
        }
         
        if(hierarchies.size() > 0)
        {
            linkedHierarchy =  hierarchies.get(0).BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
            //hierarchyOptions.add(new SelectOption(p.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, p.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c));
        }

		return new PLANiTPreparationsWrapper(preparations, hierarchyOptions, linkedHierarchy);

	}

	/**
	 *	Returns the aggregated data for the preparation.
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static PlanitDataWrapper getTargets(List<BI_PL_Preparation__c> preparationsId, String lastIdOffset) {
		//List of preparations Ids
		Map<String, BI_PL_Preparation__c> prepIds = new Map<String, BI_PL_Preparation__c>();
		for (BI_PL_Preparation__c prep : preparationsId) {
			prepIds.put(prep.Id, prep);
		}
		Set<Id> accountsId = new Set<Id>();

		//Offset of Ids to paginate records
		String idOffset = lastIdOffset != null ? lastIdOffset : '';
		Map<Id, BI_PL_Target_Preparation__c> targets = new Map<Id, BI_PL_Target_Preparation__c>([SELECT Id, CreatedDate, BI_PL_Top_parent__c, BI_PL_Primary_Parent__c, BI_PL_NTL_Value__c, BI_PL_Specialty__c,
		        BI_PL_Target_Customer__c, BI_PL_Target_Customer__r.Name, BI_PL_Target_Customer__r.External_ID_vod__c,
		        BI_PL_External_ID__c,
		        BI_PL_Target_Customer__r.PersonMobilePhone, BI_PL_Target_Customer__r.Phone, BI_PL_Target_Customer__r.Fax, BI_PL_Added_reason__c,
		        BI_PL_Added_Manually__c, BI_PL_Parent_External_Id__c
		        FROM BI_PL_Target_Preparation__c
		        WHERE BI_PL_Header__c IN :prepIds.keySet()
		        AND BI_PL_Target_Customer__r.Name != null
		        AND Id > : idOffset
		        ORDER BY Id ASC
		        LIMIT 500]);


		//Create a map with channel-details wrapper keyed by target preparation Id.
		//At same time, store the different products for details and different channels
		Map<String, List<PlanitChannelDetailModel>> targetChannelsByTarget = new Map<String, List<PlanitChannelDetailModel>>();

		for (BI_PL_Channel_detail_preparation__c tgtChannel : [SELECT Id, BI_PL_Channel__c, BI_PL_Max_adjusted_interactions__c, BI_PL_Max_adjusted_interactions_input__c, BI_PL_Max_planned_interactions__c,
		        BI_PL_Edited__c, BI_PL_Reviewed__c, BI_PL_Rejected__c, BI_PL_Rejected_Reason__c,
		        BI_PL_Target__c, BI_PL_Target_account__c, BI_PL_Target__r.BI_PL_Target_customer__r.Name, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__c,
		        BI_PL_Sum_adjusted_interactions__c, BI_PL_Sum_planned_interactions__c, BI_PL_Removed__c, BI_PL_Removed_reason__c,
		        (SELECT Id, BI_PL_Product__c, BI_PL_Secondary_product__c, BI_PL_Secondary_product__r.Name, BI_PL_Secondary_product__r.External_ID_vod__c, BI_PL_Column__c, BI_PL_Row__c, BI_PL_Product__r.Name, BI_PL_Product__r.External_ID_vod__c, BI_PL_Planned_Details__c,
		         BI_PL_Adjusted_Details__c, BI_PL_Channel_detail__c, BI_PL_Segment__c,
		         BI_PL_Segment_Color__c,
		         BI_PL_External_ID__c, BI_PL_Added_Manually__c ,
		         BI_PL_Business_rule_ok__c, BI_PL_Commit_target__c, BI_PL_Cvm__c, BI_PL_Detail_priority__c, BI_PL_Fdc__c,
		         BI_PL_Ims_id__c, BI_PL_Market_target__c, BI_PL_Other_goal__c, BI_PL_Poa_objective__c, BI_PL_Prescribe_target__c, BI_PL_Primary_goal__c, BI_PL_Strategic_segment__c
		         FROM BI_PL_Details_Preparation__r
		         ORDER BY BI_PL_Product__r.Name)
		        FROM BI_PL_Channel_detail_preparation__c
		        WHERE BI_PL_Target__c IN :targets.keySet()]) {



			List<PlanitChannelDetailModel> tgchList = new List<PlanitChannelDetailModel>();

			for (BI_PL_Detail_preparation__c detail : tgtChannel.BI_PL_Details_Preparation__r) {

				PlanitChannelDetailModel tgchModel = new PlanitChannelDetailModel(tgtChannel,
				        tgtChannel.BI_PL_Target__r.BI_PL_Target_customer__r.Name,
				        tgtChannel.BI_PL_Target_account__c,
				        tgtChannel.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c,
				        tgtChannel.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__c);

				Decimal plannedDetails = (detail.BI_PL_Planned_details__c != null) ? detail.BI_PL_Planned_details__c : 0;
				Decimal adjustedDetails = (detail.BI_PL_Adjusted_details__c != null) ? detail.BI_PL_Adjusted_details__c : 0;
				
				Decimal valueToVeeva = getToVeevaAddValue(tgtChannel.BI_PL_Reviewed__c, tgtChannel.BI_PL_Removed__c, tgtChannel.BI_PL_Rejected__c, tgtChannel.BI_PL_Edited__c, detail.BI_PL_Added_Manually__c, plannedDetails, adjustedDetails);
				PlanitChannelDetails fullDetail = new PlanitChannelDetails(detail, valueToVeeva);
				tgchModel.addDetail(fullDetail);
				//Add to the map
				tgchList.add(tgchModel);
				accountsId.add(tgtChannel.BI_PL_Target_account__c);




			}


			if (targetChannelsByTarget.containsKey(tgtChannel.BI_PL_Channel__c)) {
				System.debug(System.LoggingLevel.DEBUG, 'contains channel');
				targetChannelsByTarget.get(tgtChannel.BI_PL_Channel__c).addAll(tgchList);
			} else {
				targetChannelsByTarget.put(tgtChannel.BI_PL_Channel__c, tgchList);
			}

		}

		String lastId = '';
		Integer datalength = targets.size();
		Integer count = 0;

		if (datalength >= 500) {
			List<Id> targetList = new  List<Id>(targets.keySet());
			lastId = targetList.get(targetList.size() - 1);
		}

		PlanitDataWrapper planitData = new PlanitDataWrapper(targetChannelsByTarget, lastId, accountsId);
		return planitData;


	}

	/**
	 *	Returns the veeva data for a cycle.
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static VeevaDataWrapper getVeevaDetails(String startDateStr, String endDateStr, String lastIdOffset, String countryCode, List<id> accounts) {

		System.debug(System.LoggingLevel.DEBUG, 'getVeevaDetails');

		datetime  startDatetime = datetime.newInstance(long.valueOf(startDateStr));

		DateTime startDate = DateTime.newInstance(Long.valueOf(startDateStr)) ;
		DateTime endDate = DateTime.newInstance(Long.valueOf(endDateStr));
		String idOffset = lastIdOffset != null ? lastIdOffset : '';

		List<Cycle_Plan_Detail_vod__c> veevadata = new List<Cycle_Plan_Detail_vod__c>([SELECT Name, Id, Actual_Details_vod__c,
		        Total_Actual_Details_vod__c,
		        Planned_Details_vod__c,
		        Total_Planned_Details_vod__c,
		        Product_vod__c, Cycle_Plan_Target_vod__r.Cycle_Plan_Account_vod__r.Id,
		        Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.Territory_vod__c, Cycle_Plan_Target_vod__r.Id  
                                                                                       FROM Cycle_Plan_Detail_vod__c WHERE Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.Start_Date_vod__c = :startDate.dateGMT()
                                                                                       AND Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.End_Date_vod__c = : endDate.dateGMT()  AND  Cycle_Plan_Target_vod__r.Cycle_Plan_Account_vod__r.Id IN :accounts AND  Country_Code_BI__c = :countryCode  AND Name > : idOffset ORDER BY Name ASC LIMIT 5000]);

		String lastId = '';
		Integer datalength = veevadata.size();
		//Integer count = 0;
		if (datalength >= 5000) {
			//List<Id> targetList = new  List<Id>(targets.keySet());
			lastId = veevadata.get(veevadata.size() - 1).Name;
		}

		VeevaDataWrapper data = new VeevaDataWrapper(veevadata, lastId);
		return data;

	}


	public class VeevaDataWrapper {
		List<Cycle_Plan_Detail_vod__c> data;
		String idOffset;
		public VeevaDataWrapper(List<Cycle_Plan_Detail_vod__c> data, String idOffset) {
			this.data = data;
			this.idOffset = idOffset;
		}
	}

	public class PlanitDataWrapper {
		Map<String, List<PlanitChannelDetailModel>> data;
		String idOffset;
		Set<Id> accountsId;
		public PlanitDataWrapper(Map<String, List<PlanitChannelDetailModel>> data, String idOffset, Set<Id> accountsId) {
			this.data = data;
			this.idOffset = idOffset;
			this.accountsId = accountsId;
		}
	}



	public class PlanitChannelDetailModel {
		List<PlanitChannelDetails> details;
		String accountName {get; set;}
		Id accountId {get; set;}
		String positionName {get; set;}
		String positionId {get; set;}
		


		// Invented field. Not used in Apex, but required to not crash.
		Integer TgtA;

		public PlanitChannelDetailModel(BI_PL_Channel_detail_preparation__c tgtChannel, String accountName, Id accountId, String positionName, String positionId) {
			details = new List<PlanitChannelDetails>();
			this.accountName = accountName;
			this.accountId = accountId;
			this.positionName = positionName;
			this.positionId = positionId;

		}

		public void addDetail(PlanitChannelDetails det) {
			details.add(det);
		}
	}

	public class PlanitChannelDetails {
		BI_PL_Detail_preparation__c detail;
		Decimal valueToVeeva {get; set;}

		public PlanitChannelDetails(BI_PL_Detail_preparation__c detail, Decimal valueToVeeva) {
			this.valueToVeeva = valueToVeeva;
			this.detail = detail;

		}
	}

	private static Decimal getToVeevaAddValue (Boolean reviewed, Boolean removed, Boolean rejected, Boolean edited, Boolean addedManually, Decimal planned, Decimal adjusted) {
		if (reviewed) {
			if (!removed) {
				if (!rejected && !edited) {
					return planned;
				} else {
					return adjusted;
				}
			} else {
				return 0;
			}
		} else {
			if (addedManually) {
				return 0;
			} else {
				return planned;
			}
		}
	}



	public class PLANiTPreparationsWrapper {

		public List<BI_PL_Preparation__c> preparations {get; set;}

		public Set<SelectOption> hierarchyOptions {get; set;}
		public String linkedHierarchy {get; set;}

		public PLANiTPreparationsWrapper() {
			preparations = new List<BI_PL_Preparation__c>();
			hierarchyOptions = new Set<SelectOption>();
		}

		public PLANiTPreparationsWrapper (List<BI_PL_Preparation__c> preparations, Set<SelectOption> hierarchyOptions, String linkedHierarchy) {
			this.preparations = preparations;
			this.hierarchyOptions = hierarchyOptions;
			this.linkedHierarchy = linkedHierarchy;
		}

	}
	public class PlanitSetupModel {

		public Boolean isDS;
		public Boolean isSM;
		public Boolean isSR;

		public String userCountryCode;
		public List<BI_PL_Cycle__c> cycles {get; set;}
		public String currentCycleId;

		public PlanitSetupModel() {
		}
	}

	public class PLANiTHierarchyNodesWrapper {

		public Map<String, PLANiTTreeNode> hierarchy {get; set;}
		public String rootNodeId {get; set;}

		public PLANiTHierarchyNodesWrapper(Map<String, PLANiTTreeNode> hierarchy, String rootNodeId) {
			this.rootNodeId = rootNodeId;
			this.hierarchy = hierarchy;
		}
	}

	public class PLANiTTreeNode {
		public Id repId;
		public String repName;

		public String positionName;

		public Id positionId;
		public Id parentPositionId;

		public List<String> childrenIds = new List<String>();

		public Boolean isCurrentUsersNode = false;
		public Boolean isConfirmed;
		public String rejectedReason;

		public Id positionCycleId;

		public PLANiTTreeNode (BI_PL_PositionCycleWrapper pcw) {
			if (pcw.positionCycleUsersExclusiveNode.size() > 0) {
				this.repId = pcw.positionCycleUsersExclusiveNode.get(0).BI_PL_User__c;
				this.repName = pcw.positionCycleUsersExclusiveNode.get(0).BI_PL_User__r.Name;
			}

			for (BI_PL_Position_cycle_user__c pcu : pcw.positionCycleUsersExclusiveNode) {
				if (pcu.BI_PL_User__c == UserInfo.getUserId()) {
					isCurrentUsersNode = true;
					break;
				}
			}
			this.positionCycleId = pcw.positionCycleId;
			this.positionName = pcw.position.record.Name;
			this.positionId = pcw.position.recordId;
			this.parentPositionId = pcw.parentPosition.recordId;
			this.isConfirmed = pcw.isConfirmed;
			this.rejectedReason = pcw.rejectedReason;

			for (BI_PL_PositionCycleWrapper child : pcw.getChildren())
				this.childrenIds.add(child.position.recordId);
		}
	}








}