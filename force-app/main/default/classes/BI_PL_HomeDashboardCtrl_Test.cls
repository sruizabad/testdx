@isTest
private class BI_PL_HomeDashboardCtrl_Test {

	private static String userCountryCode;
	private static User testUser;
	private static String hierarchy = 'hierarchyTest';
    private static List<BI_PL_Position_Cycle__c> posCycles;

	
	@isTest static void test() {
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		userCountryCode = testUser.Country_Code_BI__c;
		

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

       testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);

        	
    	List<Account> listAcc = new List<Account>([SELECT id, External_Id_vod__c FROM Account LIMIT 250]);
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];

		BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

		//System.debug('******** countrycode: ' + userCountryCode);

		List<BI_PL_Position_cycle__c> listPc = new List<BI_PL_Position_cycle__c>([SELECT id FROM BI_PL_Position_cycle__c]);

		
		BI_PL_HomeDashboardCtrl.PlanitSetupModel setup = BI_PL_HomeDashboardCtrl.getSetupData();
		System.debug('** setup '+setup);
		setup.currentCycleId = [SELECT id FROM BI_PL_Cycle__c LIMIT 1].id;

		BI_PL_HomeDashboardCTrl.PLANiTHierarchyNodesWrapper hiherachynodes = BI_PL_HomeDashboardCtrl.getHierarchyNodes(userCountryCode, setup.currentCycleId,hierarchy );
		System.debug('********* hyerarchyNodesWrapper: ' + hiherachynodes);
		BI_PL_HomeDashboardCTrl.PLANiTPreparationsWrapper preparationWrap = BI_PL_HomeDashboardCtrl.getPreparations(userCountryCode,setup.currentCycleId);
		System.debug('********* preparationsWrapper: ' + preparationWrap);
		List<BI_PL_Preparation__c> preparations = preparationWrap.preparations;
		System.debug('********* preparations: ' + preparations);

		BI_PL_HomeDashboardCTrl.PlanitDataWrapper datWrap = BI_PL_HomeDashboardCtrl.getTargets(preparations,'');

		System.debug('********* data wrapper: ' + datWrap);

		

	

		BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
			
		Date startDate = cycle.BI_PL_Start_date__c;
		Date endDate = cycle.BI_PL_End_date__c;

		Datetime startTimeDT = Datetime.newInstance(startDate.year(), startDate.month(), startDate.day());
		Datetime endDateDT = Datetime.newInstance(endDate.year(), endDate.month(), endDate.day());

		String startDateStr = String.valueOf(startTimeDT.getTime());
		String endDateStr = String.valueOf(endDateDT.getTime());
		
		List<Id> listAcc2 = new List<Id>();
		
		List<BI_PL_Target_preparation__c> mapAcc = new List<BI_PL_Target_preparation__c>([SELECT BI_PL_Target_customer__c FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__c = :cycle.id]);
		for(BI_PL_Target_preparation__c tp : mapAcc){
			listAcc2.add(tp.id);
		}
		BI_PL_HomeDashboardCtrl.VeevaDataWrapper veevaData = BI_PL_HomeDashboardCtrl.getVeevaDetails(startDateStr, endDateStr, '', userCountryCode, listAcc2);

		

		


	}
	
}