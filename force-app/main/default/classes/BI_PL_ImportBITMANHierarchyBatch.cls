global class BI_PL_ImportBITMANHierarchyBatch implements Database.Batchable<sObject>, Database.Stateful {

	private Id alignmentId;
	private Date snapshotDate;
	private Id cycleId;


	global BI_PL_ImportBITMANHierarchyBatch(Id alignmentId, Date snapshotDate, Id cycleId) {

		this.alignmentId = alignmentId;
		this.snapshotDate = snapshotDate;
		this.cycleId = cycleId;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, Name, BI_TM_Position__c, BI_TM_Position__r.Name, BI_TM_Parent_Position__r.Name, BI_TM_Parent_Position__r.BI_TM_FF_type__r.Name, BI_TM_Parent_Position__c, BI_TM_Position__r.BI_TM_FF_type__r.Name
		                                 FROM BI_TM_Position_relation__c
		                                 WHERE BI_TM_Alignment_Cycle__c = : alignmentId
		                                         AND BI_TM_Position__r.BI_TM_Start_date__c <= : snapshotDate
		                                         AND (BI_TM_Position__r.BI_TM_End_date__c = null OR BI_TM_Position__r.BI_TM_End_date__c >= : snapshotDate)]);
	}

	global void execute(Database.BatchableContext BC, List<BI_TM_Position_relation__c> scope) {
		BI_PL_BITMANtoPLANiTImportUtility.importFromBitman(alignmentId, snapshotDate, cycleId, scope);
	}

	global void finish(Database.BatchableContext BC) {
	}
}