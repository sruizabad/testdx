@isTest
private class BI_PL_ImportBITMANHierarchyBatch_Test {
    
    static Profile profile;
    private static String hierarchy;

    @isTest static void test_method_one() {
        profile = [SELECT Id FROM Profile WHERE Name LIKE 'BR_%' LIMIT 1];

        User user1 = createUser('bituser1');
        User user2 = createUser('bituser2');
        User user3 = createUser('bituser3');
        User user4 = createUser('bituser4');

        insert new List<User> {user1, user2, user3, user4};

        // Data creation
        BI_TM_User_mgmt__c um1 = createUserManagement(user1);
        BI_TM_User_mgmt__c um2 = createUserManagement(user2);
        BI_TM_User_mgmt__c um3 = createUserManagement(user3);
        BI_TM_User_mgmt__c um4 = createUserManagement(user4);

        insert new List<BI_TM_User_mgmt__c> {um1, um2, um3, um4};

        BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name='FieldForceName', BI_TM_Country_Code__c = 'BR', BI_TM_Business__c = 'PM');

        insert fieldForce;

        BI_TM_Alignment__c alignment = new BI_TM_Alignment__c(Name = 'testHierarchy', BI_TM_Country_Code__c = 'BR', BI_TM_FF_type__c = fieldForce.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c = 'PM');
        hierarchy = alignment.Name;

        insert alignment;
        
        BI_TM_Position_Type__c posType = new BI_TM_Position_Type__c(Name = 'PosType1', BI_TM_Business__c='PM', BI_TM_Country_Code__c = 'BR');
        insert posType;

        BI_PL_Cycle__c selectedCycle = new BI_PL_Cycle__c(BI_PL_Start_date__c = alignment.BI_TM_Start_date__c, BI_PL_End_date__c = alignment.BI_TM_End_date__c,BI_PL_Field_force__r = fieldforce, BI_PL_Field_force__c = alignment.BI_TM_FF_type__c, BI_PL_Country_code__c = alignment.BI_TM_Country_Code__c);
        selectedCycle.BI_PL_Field_force__r.Name = 'FieldForceName';
        insert selectedCycle;

        BI_TM_Territory__c territory1 = new BI_TM_Territory__c(Name = 'territory1', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = true, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');

        insert territory1;

        BI_TM_Territory__c territory2 = new BI_TM_Territory__c(Name = 'territory2', BI_TM_Parent_Position__c = territory1.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');
        BI_TM_Territory__c territory3 = new BI_TM_Territory__c(Name = 'territory3', BI_TM_Parent_Position__c = territory1.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');

        insert new List<BI_TM_Territory__c> {territory2, territory3};

        BI_TM_Territory__c territory4 = new BI_TM_Territory__c(Name = 'territory4', BI_TM_Parent_Position__c = territory2.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');
        BI_TM_Territory__c territory5 = new BI_TM_Territory__c(Name = 'territory5', BI_TM_Parent_Position__c = territory2.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today() + 20, BI_TM_End_date__c = Date.today() + 21, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');

        insert new List<BI_TM_Territory__c> {territory4, territory5};

        BI_TM_Position_relation__c pr1 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory1.BI_TM_Parent_Position__c, BI_TM_Position__r = territory1, BI_TM_Position__c = territory1.Id, BI_TM_Business__c = 'PM');
        BI_TM_Position_relation__c pr2 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory2.BI_TM_Parent_Position__c, BI_TM_Position__r = territory2, BI_TM_Position__c = territory2.Id, BI_TM_Business__c = 'PM');
        BI_TM_Position_relation__c pr3 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory3.BI_TM_Parent_Position__c, BI_TM_Position__r = territory3, BI_TM_Position__c = territory3.Id, BI_TM_Business__c = 'PM');
        BI_TM_Position_relation__c pr4 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory4.BI_TM_Parent_Position__c, BI_TM_Position__r = territory4, BI_TM_Position__c = territory4.Id, BI_TM_Business__c = 'PM');

        BI_TM_Position_relation__c pr5 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory5.BI_TM_Parent_Position__c, BI_TM_Position__r = territory5, BI_TM_Position__c = territory5.Id, BI_TM_Business__c = 'PM');

        pr1.BI_TM_Position__r.Name = 'territory1';
        pr2.BI_TM_Position__r.Name = 'territory2';
        pr3.BI_TM_Position__r.Name = 'territory3';
        pr4.BI_TM_Position__r.Name = 'territory4';
        pr5.BI_TM_Position__r.Name = 'territory5';

        List<BI_TM_Position_relation__c> lstPositionRelation = new List<BI_TM_Position_relation__c> {pr1, pr2, pr3, pr4, pr5};

        insert lstPositionRelation;
        //New
        List<BI_PL_Position__c> positionsM = new List<BI_PL_Position__c>();
        List<BI_PL_Position_cycle_user__c> positionCycleUsers = new List<BI_PL_Position_cycle_user__c>();
        List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();

        BI_PL_Position__c positionRoot = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = 'BR');
        BI_PL_Position__c positionChild1 = new BI_PL_Position__c(Name = 'Test2', BI_PL_Country_code__c = 'BR');
        BI_PL_Position__c positionChild2 = new BI_PL_Position__c(Name = 'Test3', BI_PL_Country_code__c = 'BR');
        BI_PL_Position__c positionChild21 = new BI_PL_Position__c(Name = 'Test4', BI_PL_Country_code__c = 'BR');

        positionsM.add(positionRoot);
        positionsM.add(positionChild1);
        positionsM.add(positionChild2);
        positionsM.add(positionChild21);

        insert positionsM;


        BI_PL_Position_cycle__c root = new BI_PL_Position_cycle__c(/*Name = 'root', */BI_PL_Position__c = positionRoot.Id, BI_PL_Parent_position__c = null, BI_PL_Cycle__c = selectedCycle.Id, BI_PL_Hierarchy__c = hierarchy);

        root.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(selectedCycle, root.BI_PL_Hierarchy__c, positionRoot.Name);

        insert root;

        BI_PL_Position_cycle__c child1 = new BI_PL_Position_cycle__c(/*Name = 'child1', */BI_PL_Position__c = positionChild1.Id, BI_PL_Parent_position__c = positionRoot.Id, BI_PL_Cycle__c = selectedCycle.Id, BI_PL_Hierarchy__c = hierarchy);

        child1.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(selectedCycle, child1.BI_PL_Hierarchy__c, positionChild1.Name);

        insert child1;

        BI_PL_Position_cycle__c child2 = new BI_PL_Position_cycle__c(/*Name = 'child2', */BI_PL_Position__c = positionChild2.Id, BI_PL_Parent_position__c = positionRoot.Id, BI_PL_Cycle__c = selectedCycle.Id, BI_PL_Hierarchy__c = hierarchy);

        child2.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(selectedCycle, child2.BI_PL_Hierarchy__c, positionChild2.Name);

        insert child2;

        BI_PL_Position_cycle__c child21 = new BI_PL_Position_cycle__c(/*Name = 'child21', */BI_PL_Position__c = positionChild21.Id, BI_PL_Parent_position__c = positionChild2.Id, BI_PL_Cycle__c = selectedCycle.Id, BI_PL_Hierarchy__c = hierarchy);

        child21.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(selectedCycle, child21.BI_PL_Hierarchy__c, positionChild21.Name);

        insert child21;
        //End new

        BI_TM_User_territory__c pcu1 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory1.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user1.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um1.Id, BI_TM_External_Id__c = 'test1');
        BI_TM_User_territory__c pcu2 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory2.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user2.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um2.Id, BI_TM_External_Id__c = 'test2');
        BI_TM_User_territory__c pcu3 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory3.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user3.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um3.Id, BI_TM_External_Id__c = 'test3');
        BI_TM_User_territory__c pcu4 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory4.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user4.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um4.Id, BI_TM_External_Id__c = 'test4');
        BI_TM_User_territory__c pcu5 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory4.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user3.Id, BI_TM_Start_date__c = Date.today() + 20, BI_TM_End_date__c = Date.today() + 21, BI_TM_User_mgmt_tm__c = um4.Id, BI_TM_External_Id__c = 'test5');

        insert new List<BI_TM_User_territory__c> {pcu1, pcu2, pcu3, pcu4, pcu5};
        
        BI_PL_ImportBITMANHierarchyBatch toExecute = new BI_PL_ImportBITMANHierarchyBatch(alignment.Id, alignment.BI_TM_Start_date__c, selectedCycle.Id);
        Test.startTest();
        try{
            Id batchId = Database.executeBatch(toExecute, 10);
            BI_PL_ImportBITMANHierarchyCtrl.checkBatchStatus(batchId);
        }catch (Exception e){
        }
        Test.stopTest();
    }

    private static User createUser(String name) {
        return new User(Alias = name.substring(0, 5), Email = name + '@testorg.com',
                        EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US', ProfileId = profile.Id, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = name+'_External_ID_Test_');
    }
    private static BI_TM_User_mgmt__c createUserManagement(User user) {
        return new BI_TM_User_mgmt__c(BI_TM_Currency__c = 'USD', BI_TM_UserId_Lookup__c = user.Id, BI_TM_LanguageLocaleKey__c = user.LanguageLocaleKey,
                                      BI_TM_LocaleSidKey__c = user.LocaleSidKey, BI_TM_Username__c = user.UserName, BI_TM_TimeZoneSidKey__c = user.TimeZoneSidKey,BI_TM_UserCountryCode__c='US',
                                      BI_TM_Alias__c = 'Alias',
                                      BI_TM_Business__c = 'PM', 
                                      BI_TM_COMMUNITYNICKNAME__c = 'CName'+generaNumeroAleatorio(3), 
                                      BI_TM_Email__c = 'email'+generaNumeroAleatorio(3)+'@mail.com', 
                                      BI_TM_Last_name__c = 'LName', 
                                      BI_TM_Profile__c = 'US_AMA', 
                                      BI_TM_Start_date__c = Date.parse(system.today().format()));

    }
    
    private static String generaNumeroAleatorio(Integer nposiciones) {
       integer lower= 0;
       integer upper= 9;

       if( nposiciones == null || nposiciones <= 0 || nposiciones >= 20)
           nposiciones = 3;

       string alea= '';
       for( integer i=1; i<=nposiciones; i++) {
           alea += (Integer) (Math.floor(Math.random() * (upper - lower)) + lower);
       }

       return alea;
   }
    
}