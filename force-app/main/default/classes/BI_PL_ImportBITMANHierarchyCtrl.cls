/**
 *	14-06-2017
 *	BI_SAP_LoadBITMANHierarchy component controller
 *	@author OMEGA CRM
 */
public without sharing class BI_PL_ImportBITMANHierarchyCtrl {

	public static final String const_reimportAvailable = 'REIMPORT_AVAILABLE';
	public String REIMPORT_AVAILABLE {
		get{
			return const_reimportAvailable;
		}
		set;
	}

	public static final String const_success = 'SUCCESS';
	public String SUCCESS_MESSAGE {
		get{
			return const_success;
		}
		set;
	}

	public static Map<String, Schema.SObjectField> M = Schema.SObjectType.BI_TM_Alignment__c.fields.getMap();

	public String countryCode {get; set;}
	public Id alignment {get; set;}
	public Id fieldForce {get; set;}
	public Id cycleId {get; set;}

	public Boolean reimporting {get; set;}
	public Boolean completed {get; set;}

	public Id batchJobId {get; set;}

	public Boolean reimportEnabled {get; set;}

	public Date snapshotDate {get; set;}
	public String snapshotDateString {get; set;}

	public Boolean dateSelected {
		get {
			return isDateSelected();
		}
		set;
	}

	public String errors {get; set;}
	public BI_PL_BITMANtoPLANiTImportUtility.BITMANtoPLANiTImportResults result {get; set;}

	public Boolean loading {get; set;}

	public List<BI_TM_Alignment__c> alignments {
		get{
			return alignmentsMap.values();
		} set;
	}

	public Map<Id, BI_TM_Alignment__c> alignmentsMap {get; set;}

	public List<SelectOption> alignmentOptions {get; set;}
	public List<SelectOption> cycleOptions {get; set;}
	public Map<Id, BI_PL_Cycle__C> cycles {get; set;}
	public List<SelectOption> fieldforceOptions {get; set;}

	public BI_PL_ImportBITMANHierarchyCtrl() {
		reimporting = false;
		countryCode = BI_PL_ImportBITMANHierarchyCtrl.getUserCountryCode();

		alignments = new List<BI_TM_Alignment__c>();
		snapshotDate = Date.today();

		getAlignments();

		loadCycleOptions();
	}

	public class ResponseWrapper {
		public List<BI_TM_Alignment__c> alignments;
		public Map<String, String> fieldForces;

		public ResponseWrapper(Map<String, String> ffs, List<BI_TM_Alignment__c> al) {
			fieldForces = ffs;
			alignments = al;
		}
	}

	private static String getUserCountryCode() {
		return [SELECT Country_Code_BI__c FROM USER WHERE Id = :UserInfo.getUserId()].Country_Code_BI__c;
	}

	@RemoteAction
	@ReadOnly
	public static String checkBatchStatus(String batchId) {
		return [SELECT Status FROM AsyncApexJob WHERE Id = : batchId].Status;
	}

	/**
	 *	Retrieves alingments from the DB depending on the selected snapshotDate.
	 *	@author OMEGA CRM
	 */
	@RemoteAction
	@ReadOnly
	public static ResponseWrapper getAlignmentsRemote(Date snapShotDate, String countryCode) {

		String ccode = countryCode == null ? getUserCountryCode() : countryCode;
		//Date snapShotDate = Date.parse(dat);

		Map<String, String> fieldForces = new Map<String, String>();

		List<BI_TM_Alignment__c> alignments = new List<BI_TM_Alignment__c>([SELECT Id, Name, BI_TM_Start_date__c, BI_TM_End_date__c, BI_TM_FF_type__c, BI_TM_FF_type__r.Name, BI_TM_Country_Code__c, BI_TM_Status__c
		        FROM BI_TM_Alignment__c
		        WHERE BI_TM_Start_date__c <= :snapshotDate AND BI_TM_End_date__c >= :snapshotDate
		        AND BI_TM_Country_Code__c = :ccode]);

		for (BI_TM_Alignment__c a : alignments) {
			if (!fieldForces.containsKey(a.BI_TM_FF_type__c)) {
				fieldForces.put(a.BI_TM_FF_type__c, a.BI_TM_FF_type__r.Name);
			}
		}

		return new ResponseWrapper(fieldForces, alignments);
	}

	@RemoteAction
	@ReadOnly
	public static List<BI_PL_Cycle__c> getCycles(String countryCode) {
		String ccode = countryCode == null ? getUserCountryCode() : countryCode;

		return new List<BI_PL_Cycle__c>([SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Field_force__r.Name, Name FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = : ccode ORDER BY Name]);

	}

	/**
	 *	Executes the BITMAN import process. In case something goes wrong, display the error messages using the "errors" variable.
	 *	@author OMEGA CRM
	 */
	@RemoteAction
	public static String startImportFromBitman(String alignmentId, Date snapshotDate, String cycleId) {
		System.debug('BI_PL_ImportBITMANHierarchyCtrl startImportFromBitman');

		if (snapshotDate == null)
			throw new BI_PL_Exception(Label.BI_SAP_DM_Choose_a_date);

		if (String.isBlank(alignmentId))
			throw new BI_PL_Exception(Label.BI_PL_Invalid_alignment);

		if (String.isBlank(cycleId))
			throw new BI_PL_Exception(Label.BI_PL_Invalid_cycle);

		if (isHierarchyAlreadyImported(alignmentId, cycleId)) {
			if (hasHierarchyPreparations(cycleId, alignmentId))
				return 'hierarchyAlreadyWithPreps';
			//throw new BI_PL_Exception(Label.BI_PL_Hierarchy_already_PLANiT_with_preps);
			return 'hierarchyAlreadyReimport';
		} else {
			Database.executeBatch(new BI_PL_ImportBITMANHierarchyBatch(alignmentId, snapshotDate, cycleId));
		}

		return null;
	}

	@RemoteAction
	public static void runDeleteAndImportBatchJob(String alignmentId, Date snapshotDate, String cycleId) {
		Database.executeBatch(new BI_PL_DelAndImportBITMANHierarchyBatch(alignmentId, snapshotDate, cycleId));
	}


	/**
	 *	Retrieves alingments from the DB depending on the selected snapshotDate.
	 *	@author OMEGA CRM
	 */
	private void getAlignments() {
		alignmentsMap = new Map<Id, BI_TM_Alignment__c>([SELECT Id, Name, BI_TM_Start_date__c, BI_TM_End_date__c, BI_TM_FF_type__c, BI_TM_FF_type__r.Name, BI_TM_Status__c FROM BI_TM_Alignment__c WHERE BI_TM_Start_date__c <= :snapshotDate AND BI_TM_End_date__c >= :snapshotDate AND BI_TM_Country_Code__c = :countryCode]);

		System.debug('getAlignments: ' + snapshotDate + ' - ' + countryCode + ' - ' + alignmentsMap);

		fieldforceOptions = new List<SelectOption>();
		Set<Id> alreadyAdded = new Set<Id>();
		if (isDateSelected()) {
			for (BI_TM_Alignment__c a : alignments) {
				if (!alreadyAdded.contains(a.BI_TM_FF_type__c)) {
					fieldforceOptions.add(new SelectOption(a.BI_TM_FF_type__c, a.BI_TM_FF_type__r.Name));
					alreadyAdded.add(a.BI_TM_FF_type__c);
				}
			}
		}
		if (!fieldforceOptions.isEmpty()) {
			fieldForce = fieldforceOptions.get(0).getValue();
			refreshAlignmentsOptions();
		} else {
			alignmentOptions = new List<SelectOption>();
			fieldForce = null;
			alignment = null;
		}
	}

	/**
	 *	Executed when the force field input has changed its value.
	 *	@author OMEGA CRM
	 */
	public PageReference forceFieldChanged() {
		refreshAlignmentsOptions();
		return null;
	}

	/**
	 *	Updates the available options in the alignments picklist.
	 *	@author OMEGA CRM
	 */
	private void refreshAlignmentsOptions() {
		alignmentOptions = new List<SelectOption>();
		for (BI_TM_Alignment__c a : alignments) {
			if (a.BI_TM_FF_type__c == fieldForce) {
				String dateInterval = alignmentsMap.get(a.Id).BI_TM_Start_date__c.format() + ' - ' + alignmentsMap.get(a.Id).BI_TM_End_date__c.format();
				alignmentOptions.add(new SelectOption(a.Id, a.Name + ' (' + dateInterval + ')'));
			}
		}
		if (!alignmentOptions.isEmpty()) {
			alignment = alignmentOptions.get(0).getValue();
		} else {
			alignment = null;
		}
	}

	private void loadCycleOptions() {
		cycleOptions = new List<SelectOption>();

		cycles = new Map<Id, BI_PL_Cycle__c>([SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Field_force__r.Name, Name FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = : countryCode]);

		for (BI_PL_Cycle__c c : cycles.values()) {
			cycleOptions.add(new SelectOption(c.Id, c.Name));
		}

		if (!cycleOptions.isEmpty()) {
			cycleId = cycleOptions.get(0).getValue();
		}
	}
	/**
	 *	Snapshotdate has changed.
	 *	@author OMEGA CRM
	 */
	public PageReference dateChanged() {
		System.debug('dateChanged ' + snapshotDateString);

		List<String> snapshotDateSplit = new List<String>();
		if (String.isNotBlank(snapshotDateString)) {
			snapshotDateSplit = snapshotDateString.split('-');
		}
		if (snapshotDateSplit.size() > 0) {
			snapshotDate = date.newInstance(integer.valueof(snapshotDateSplit.get(0)), integer.valueof(snapshotDateSplit.get(1)), integer.valueof(snapshotDateSplit.get(2)));
		} else {
			snapshotDate = null;
		}
		System.debug('snapshotDate ' + snapshotDate);
		if (isDateSelected()) {
			getAlignments();
		} else {
			alignment = null;
		}
		return null;
	}

	/**
	 *	Checks if the BITMAN hierarchy has been already imported into PLANiT.
	 *	@author OMEGA CRM
	 */
	private static Boolean isHierarchyAlreadyImported(Id alignment, Id cycleId) {
		String hierarchyName = [SELECT Name FROM BI_TM_Alignment__c WHERE Id = : alignment].Name;
		System.debug('**hierarchyName ' + hierarchyName);
		return new List<BI_PL_Position_cycle__c>([SELECT Id FROM BI_PL_Position_cycle__c WHERE BI_PL_Hierarchy__c = : hierarchyName AND BI_PL_Cycle__c = : cycleId]).size() > 0;
	}

	/**
	 *	Checks if there's any Preparation for the provided Cycle and Hierarchy.
	 *	@author OMEGA CRM
	 */
	private static Boolean hasHierarchyPreparations(String cycleId, String alignment) {
		String hierarchyName = [SELECT Name FROM BI_TM_Alignment__c WHERE Id = : alignment].Name;
		return new List<BI_PL_Preparation__c>([SELECT Id FROM BI_PL_Preparation__c
		                                       WHERE BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
		                                               AND BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchyName LIMIT 1]).size() > 0;
	}
	public void cancelReimport() {
		reimportEnabled = false;
	}

	public void startLoading() {
		loading = true;
		errors = null;
		completed = false;
	}

	public void endLoading() {
		loading = false;
	}
	public void empty() {

	}

	private Boolean isDateSelected() {
		return snapshotDate != null;
	}

	private void clearErrors() {
		errors = null;
	}
}