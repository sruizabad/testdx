@isTest
public with sharing class BI_PL_ImportBITMANHierarchyCtrl_Test {
	@isTest
	public static void test() {

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(BI_TM_Country_Code__c = 'BR',BI_TM_Business__c='PM');
		insert fieldForce;

		BI_TM_Alignment__c alignment = new BI_TM_Alignment__c(Name = 'testHierarchy', BI_TM_Country_Code__c = 'BR', BI_TM_FF_type__c = fieldForce.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c='PM');
		String hierarchy = alignment.Name;
		insert alignment;

		BI_PL_Cycle__c selectedCycle = new BI_PL_Cycle__c(BI_PL_Start_date__c = alignment.BI_TM_Start_date__c, BI_PL_End_date__c = alignment.BI_TM_End_date__c,BI_PL_Field_force__r = fieldforce, BI_PL_Field_force__c = alignment.BI_TM_FF_type__c, BI_PL_Country_code__c = alignment.BI_TM_Country_Code__c);
		selectedCycle.BI_PL_Field_force__r.Name = 'FieldForceName';
		insert selectedCycle;

		BI_PL_Cycle__c selectedCycle2 = new BI_PL_Cycle__c(BI_PL_Start_date__c = alignment.BI_TM_Start_date__c, BI_PL_End_date__c = alignment.BI_TM_End_date__c,BI_PL_Field_force__r = fieldforce, BI_PL_Field_force__c = alignment.BI_TM_FF_type__c, BI_PL_Country_code__c = [SELECT Country_Code_BI__c FROM USER WHERE Id = :UserInfo.getUserId()].Country_Code_BI__c);
		selectedCycle.BI_PL_Field_force__r.Name = 'FieldForceName2';
		insert selectedCycle2;

		BI_PL_DelAndImportBITMANHierarchyBatch toExecute = new BI_PL_DelAndImportBITMANHierarchyBatch(alignment.Id, alignment.BI_TM_Start_date__c, selectedCycle.Id);	

		BI_PL_ImportBITMANHierarchyCtrl controllerModified = new BI_PL_ImportBITMANHierarchyCtrl();

		BI_PL_ImportBITMANHierarchyCtrl.runDeleteAndImportBatchJob(alignment.Id, alignment.BI_TM_Start_date__c, selectedCycle.Id);

		try{
			controllerModified.snapshotDate = null;
			PageReference pageDate = controllerModified.dateChanged();
		}catch(Exception e){}

		BI_PL_ImportBITMANHierarchyCtrl controller = new BI_PL_ImportBITMANHierarchyCtrl();

		Boolean testDS = controller.dateSelected;

		String aux = controller.REIMPORT_AVAILABLE;
		aux = controller.SUCCESS_MESSAGE;

		controller.forceFieldChanged();

		controller.dateChanged();

		controller.alignment = alignment.Id;
		controller.snapshotDate = Date.today();
		controller.cycleId = selectedCycle.Id;

		controller.cancelReimport();
		controller.startLoading();
		controller.endLoading();
		controller.empty();
		BI_PL_ImportBITMANHierarchyCtrl.getAlignmentsRemote(Date.today(), 'US');
		BI_PL_ImportBITMANHierarchyCtrl.getCycles('US');

		try{
			String batchId = BI_PL_ImportBITMANHierarchyCtrl.startImportFromBitman(alignment.Id, null,selectedCycle.Id);	
			BI_PL_ImportBITMANHierarchyCtrl.checkBatchStatus(batchId);
		}catch(Exception e){}

		try{
			String batchId = BI_PL_ImportBITMANHierarchyCtrl.startImportFromBitman(alignment.Id, Date.today(),null);	
			BI_PL_ImportBITMANHierarchyCtrl.checkBatchStatus(batchId);
		}catch(Exception e){}

		try{
			String batchId = BI_PL_ImportBITMANHierarchyCtrl.startImportFromBitman(null, Date.today(),selectedCycle.Id);
			BI_PL_ImportBITMANHierarchyCtrl.checkBatchStatus(batchId);	
		}catch(Exception e){}

		try{
			String batchId = BI_PL_ImportBITMANHierarchyCtrl.startImportFromBitman(alignment.Id, Date.today(),selectedCycle.Id);
			BI_PL_ImportBITMANHierarchyCtrl.checkBatchStatus(batchId);	
		}catch(Exception e){}
		
		
		//BI_PL_ImportBITMANHierarchyCtrl.checkBatchStatus(batchId);
		
		BI_PL_BITMANtoPLANiTImportUtility.BITMANtoPLANiTImportResults results = controller.result;


		}
}