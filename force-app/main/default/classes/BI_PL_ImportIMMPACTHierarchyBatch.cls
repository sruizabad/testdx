/**
 *  08/08/2017
 *  - generateDetailPreparationExternalId() now takes into consideration the secondary product.
 *  04/07/2017
 *  - Exceptions in custom labels.
 * 
 *  15/06/2017
 *  Loads data from IMMPACT into PLANiT.
 * 0.- The batch works with Target_Account_BI__c records having:
 *  - Target_Calls_BI__c > 0
 *  - Version_Comparison_BI__c = 'Active'
 *  - Portfolio_BI__r.Country_Code_BI__c = : countryCode
 *  - Portfolio_BI__r.Active_SAP__c = true
 *  - The same start and end target cycle dates for the portfolio's dates.
 *
 * 1.- Create a BI_PL_Preparation from the target account data.
 *  - BI_PL_Country_code__c: the portfolio's country code.
 *  - BI_PL_Position_cycle__c: the one pointing to the user's position for the provided hierarchy.
 *  - BI_PL_External_id__c: countryCode, targetCycleStartDate, targetCycleEndDate, targetHierarchy, territory (based on Target_Account_BI__r.Account_BI__c)
 *
 * 2.- For each Target_Account_BI__c create a BI_PL_Target_preparation__c:
 *  - BI_PL_Target_customer__c = Target_Account_BI__c.Account_BI__c,
 *  - BI_PL_Ntl_value__c = Target_Account_BI__c.NTL_Value_BI__c,
 *  - BI_PL_Header__c = newPreparationId (from the ones created previously),
 *  - BI_PL_External_id__c = countryCode, targetCycleStartDate, targetCycleEndDate, targetHierarchy, positionName, Target_Account_BI__c.Account_BI__r.External_ID_vod__c,  * Target_Account_BI__c.Account_BI__c
 *
 *      - countryCode: batch param.
 *      - targetCycleStartDate: batch param.
 *      - targetCycleEndDate: batch param.
 *      - targetHierarchy: batch param.
 *      - positionName: userPositionName.
 *
 * 3.-  For each Target_Account_BI__c create a BI_PL_Channel_detail_preparation__c:
 *  - BI_PL_Target__c = targetPreparationId,
 *  - BI_PL_Channel__c = DEFAULT_CHANNEL,
 *  - BI_PL_External_id__c = countryCode, startDate, endDate, hierarchy, positionName, customerExternalId, customerId, DEFAULT_CHANNEL;
 *
 *      - countryCode: batch param.
 *      - startDate: batch param.
 *      - endDate: batch param.
 *      - hierarchy: batch param.
 *      - positionName: userPositionName.
 *      - customerExternalId: targetAccount.Account_BI__r.External_ID_vod__c.
 *      - customerId: targetAccount.Account_BI__c.
 *
 * 4.- Retrieve Target_Detail_BI__c records having:
 *  - Target_Account_BI__c IN :targetAccountsId.
 *  - Version_Comparison_BI__c = 'Active'.
 *  - Portfolio_BI__r.Active_SAP__c = true.
 *
 *  For each of those Target_Detail_BI__c create a BI_PL_Detail_preparation__c:
 *  - BI_PL_Planned_details__c = targetDetail.Target_Details_BI__c,
 *  - BI_PL_Adjusted_details__c = targetDetail.Target_Details_BI__c,
 *  - BI_PL_Column__c = targetDetail.Column_BI__c,
 *  - BI_PL_Product__c = targetDetail.Product_Catalog_BI__c,
 *  - BI_PL_Segment__c = targetDetail.Segment_BI__c,
 *  - BI_PL_Row__c = targetDetail.Row_BI__c,
 *  - BI_PL_Channel_detail__c = channelDetailId,
 *  - BI_PL_External_id__c = countryCode, targetCycleStartDate, targetCycleEndDate, targetHierarchy, positionName, targetDetail.Target_Account_BI__r.Account_BI__r.External_ID_vod__c,  * targetDetail.Target_Account_BI__r.Account_BI__c, DEFAULT_CHANNEL, targetDetail.Product_Catalog_BI__r.External_ID_vod__c, targetDetail.Product_Catalog_BI__c.
 *
 *      - channelDetailId: from the previously created records.
 *      - countryCode: batch param.
 *      - targetCycleStartDate: batch param.
 *      - targetCycleEndDate: batch param.
 *
 *  @author OMEGA CRM
 */
global without sharing class BI_PL_ImportIMMPACTHierarchyBatch implements Database.Batchable<sObject>, Database.Stateful {

    private static final String PRODUCT_TYPE_DETAIL = 'Detail';
    private static final String GROUP_TERRITORY_TYPE = 'Territory';
    private static final String DEFAULT_CHANNEL = 'rep_detail_only';
    private String STATUS_UNDER_REVIEW = 'Under Review';

    private Map<Id, BI_PL_Target_preparation__c> newTargetPreparationByAccountId;
    private Map<Id, BI_PL_Channel_detail_preparation__c> newChannelDetailByAccountId;

    private String countryCode;
    private Id targetCycle;
    private String targetHierarchy;
    private Date targetCycleStartDate;
    private Date targetCycleEndDate;
    private String targetCycleFieldForce;

    private String userPositionName;


    // newPreparationIdByOriginalPreparationId stores: key = originalPreparationId, value = newPreparationId
    private Map<Id, Id> newPreparationIdByOriginalPreparationId = new Map<Id, Id>();


    global BI_PL_ImportIMMPACTHierarchyBatch(Id targetCycle, String targetHierarchy) {
        // Precondition:
        // The running user is at least in one position cycle user for the target cycle and hierarchy.

        BI_PL_Cycle__c cycle = [SELECT BI_PL_Field_Force__r.Name, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Country_code__c FROM BI_PL_Cycle__c WHERE Id = :targetCycle];

        this.targetCycle = targetCycle;
        this.targetHierarchy = targetHierarchy;
        this.countryCode = cycle.BI_PL_Country_code__c;
        this.targetCycleStartDate = cycle.BI_PL_Start_date__c;
        this.targetCycleEndDate = cycle.BI_PL_End_date__c;
        this.targetCycleFieldForce = cycle.BI_PL_Field_Force__r.Name;

        List<BI_PL_Position_cycle_user__c> pcus = new List<BI_PL_Position_cycle_user__c>([SELECT BI_PL_Position_cycle__r.BI_PL_Position_name__c
                FROM BI_PL_Position_cycle_user__c
                WHERE BI_PL_User__c = :UserInfo.getUserId()
                                      AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = :targetCycle
                                              AND BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :targetHierarchy]);

        if (pcus.size() > 0) {
            this.userPositionName = pcus.get(0).BI_PL_Position_cycle__r.BI_PL_Position_name__c;
        }

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        // If the userPositionName was set properly:
        if (userPositionName != null) {
            Id groupId = getGroupId();

            System.debug('BI_PL_ImportIMMPACTHierarchyBatch start');
            System.debug('BI_PL_ImportIMMPACTHierarchyBatch countryCode: ' + countryCode);
            System.debug('BI_PL_ImportIMMPACTHierarchyBatch targetCycleStartDate: ' + targetCycleStartDate);
            System.debug('BI_PL_ImportIMMPACTHierarchyBatch targetCycleEndDate: ' + targetCycleEndDate);
            System.debug('BI_PL_ImportIMMPACTHierarchyBatch groupId: ' + groupId);

            return Database.getQueryLocator([SELECT
                                             Id,
                                             Account_BI__c,
                                             NTL_Value_BI__c,
                                             Account_BI__r.Name,
                                             Account_BI__r.External_ID_vod__c
                                             FROM Target_Account_BI__c
                                             WHERE Account_BI__c IN (SELECT AccountId FROM AccountShare WHERE UserOrGroupId = :groupId)
                                             AND Target_Calls_BI__c > 0
                                             AND Version_Comparison_BI__c = 'Active'
                                                     AND Portfolio_BI__r.Country_Code_BI__c = : countryCode
                                                             AND Portfolio_BI__r.Active_SAP__c = true
                                                                     AND Portfolio_BI__r.Cycle_BI__r.Start_Date_BI__c = :targetCycleStartDate
                                                                             AND Portfolio_BI__r.Cycle_BI__r.End_Date_BI__c = :targetCycleEndDate]);
        }else{
            // If the userPositionName was not set properly the batch can't continue.
            System.abortJob(BC.getJobId());
        }
        return null;
    }

    /**
     *  Loads all the existing groups related to the territories.
     *  @author OMEGA CRM
     */
    private Id getGroupId() {
        try {
            return [SELECT Id FROM Group WHERE Type = : GROUP_TERRITORY_TYPE AND RelatedId IN (SELECT Id FROM Territory WHERE Name = : userPositionName)].Id;
        } catch (exception e) {
            throw new BI_PL_Exception(Label.BI_PL_No_group_for_territory+' \'' + userPositionName + '\'');
        }

        return null;
    }

    global void execute(Database.BatchableContext BC, List<Target_Account_BI__c> originalTargetAccounts) {
        System.debug('BI_PL_ImportIMMPACTHierarchyBatch execute: ' + originalTargetAccounts);

        //  Portfolio_BI__c <-- Target_Account_BI__c <-- Target_Detail_BI__c
        //  BI_PL_Preparation__c <-- BI_PL_Target_preparation__c <-- BI_PL_Channel_detail_preparation__c (single channel) <-- BI_PL_Detail_preparation__c

        Set<Id> targetAccountsId = new Set<Id>();

        for (Target_Account_BI__c t : originalTargetAccounts)
            targetAccountsId.add(t.Id);

        // 1.- Create the BI_PL_Preparation__c based on the preparation information from the Target_Account_BI__c:
        BI_PL_Position_cycle__c pos = new BI_PL_Position_cycle__c(BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(countryCode, targetCycleStartDate, targetCycleEndDate, targetCycleFieldForce, targetHierarchy, userPositionName));

        BI_PL_Preparation__c newPreparation = new BI_PL_Preparation__c(
            BI_PL_Country_code__c = countryCode,
            BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId(countryCode, targetCycleStartDate, targetCycleEndDate, targetHierarchy, userPositionName, targetCycleFieldForce),
            BI_PL_Position_cycle__r = pos,
            BI_PL_Status__c = STATUS_UNDER_REVIEW);

        System.debug('BI_PL_ImportIMMPACTHierarchyBatch execute: ' + newPreparation);

        upsert newPreparation BI_PL_External_id__c;

        // 2.- Target preparations creation:
        newTargetPreparationByAccountId = new Map<Id, BI_PL_Target_preparation__c>();
        newChannelDetailByAccountId = new Map<Id, BI_PL_Channel_detail_preparation__c>();

        for (Target_Account_BI__c targetAccount : originalTargetAccounts) {
            newTargetPreparationByAccountId.put(targetAccount.Account_BI__c,
                                                new BI_PL_Target_preparation__c(
                                                    BI_PL_Target_customer__c = targetAccount.Account_BI__c,
                                                    BI_PL_Ntl_value__c = targetAccount.NTL_Value_BI__c,
                                                    BI_PL_Header__c = newPreparation.Id,
                                                    BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generateTargetPreparationExternalId(countryCode, targetCycleStartDate, targetCycleEndDate, targetHierarchy, userPositionName, targetAccount.Account_BI__r.External_ID_vod__c, targetAccount.Account_BI__c, targetCycleFieldForce)
                                                )
                                               );
        }

        upsert newTargetPreparationByAccountId.values() BI_PL_External_id__c;

        // 3.- Channel details creation (single channel, one record per target):
        for (Target_Account_BI__c targetAccount : originalTargetAccounts) {
            if (newTargetPreparationByAccountId.containsKey(targetAccount.Account_BI__c)) {
                newChannelDetailByAccountId.put(targetAccount.Account_BI__c, createChannelDetail(countryCode, targetCycleStartDate, targetCycleEndDate, targetHierarchy, userPositionName, targetAccount.Account_BI__r.External_ID_vod__c, targetAccount.Account_BI__c, newTargetPreparationByAccountId.get(targetAccount.Account_BI__c).Id));
            }
        }
        upsert newChannelDetailByAccountId.values() BI_PL_External_id__c;

        // 4.- Detail preparations creation:
        List<BI_PL_Detail_preparation__c> detailPreparations = new List<BI_PL_Detail_preparation__c>();

        // For each target detail create a new one:
        for (Target_Detail_BI__c targetDetail : [SELECT Id, Target_Account_BI__c, Target_Account_BI__r.Account_BI__r.Name, Target_Account_BI__r.Account_BI__c, Target_Account_BI__r.Account_BI__r.External_ID_vod__c, Product_Catalog_BI__c, Segment_BI__c, Row_BI__c, Product_Catalog_BI__r.External_ID_vod__c, Target_Details_BI__c, Column_BI__c
                FROM Target_Detail_BI__c
                WHERE Target_Account_BI__c IN :targetAccountsId
                AND Product_Catalog_BI__r.Product_Type_vod__c = :PRODUCT_TYPE_DETAIL
                AND Version_Comparison_BI__c = 'Active'
                                               AND Portfolio_BI__r.Active_SAP__c = true]) {

            // Only create details for the created channel details:
            if (newChannelDetailByAccountId.containsKey(targetDetail.Target_Account_BI__r.Account_BI__c)) {
                BI_PL_Detail_preparation__c detailPreparation = createDetailPreparation(targetDetail, userPositionName, newChannelDetailByAccountId.get(targetDetail.Target_Account_BI__r.Account_BI__c).Id);

                detailPreparations.add(detailPreparation);
            }
        }

        upsert detailPreparations BI_PL_External_id__c;
    }

    global void finish(Database.BatchableContext BC) {
        // Once all records have been created refresh the preparations visibility:
        if (!Test.isRunningTest())
            Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(targetCycle, targetHierarchy, false), 10);
    }

    /**
     *  Returns a new BI_PL_Channel_detail_preparation__c record.
     *  @author OMEGA CRM
     */
    private BI_PL_Channel_detail_preparation__c createChannelDetail(String countryCode, Date startDate, Date endDate, String hierarchy, String positionName, String customerExternalId, Id customerId, Id targetPreparationId) {
        return new BI_PL_Channel_detail_preparation__c(
                   BI_PL_Target__c = targetPreparationId,
                   BI_PL_Channel__c = DEFAULT_CHANNEL,
                   BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generateChannelDetailPreparationExternalId(countryCode, startDate, endDate, hierarchy, positionName, customerExternalId, customerId, DEFAULT_CHANNEL, targetCycleFieldForce));
    }
    /**
     *  Returns a new BI_PL_Detail_preparation__c record.
     *  @author OMEGA CRM
     */
    private BI_PL_Detail_preparation__c createDetailPreparation(Target_Detail_BI__c targetDetail, String positionName, Id channelDetailId) {
        if (String.isBlank(targetDetail.Product_Catalog_BI__c))
            throw new BI_PL_Exception(Label.BI_PL_No_product_for_target_detail);

        return new BI_PL_Detail_preparation__c(
                   BI_PL_Planned_details__c = targetDetail.Target_Details_BI__c,
                   BI_PL_Adjusted_details__c = targetDetail.Target_Details_BI__c,
                   BI_PL_Column__c = targetDetail.Column_BI__c,
                   BI_PL_Product__c = targetDetail.Product_Catalog_BI__c,
                   BI_PL_Segment__c = targetDetail.Segment_BI__c,
                   BI_PL_Row__c = targetDetail.Row_BI__c,
                   BI_PL_Channel_detail__c = channelDetailId,
                   BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generateDetailPreparationExternalId(countryCode,
                                          targetCycleStartDate,
                                          targetCycleEndDate,
                                          targetHierarchy,
                                          positionName,
                                          targetDetail.Target_Account_BI__r.Account_BI__r.External_ID_vod__c,
                                          targetDetail.Target_Account_BI__r.Account_BI__c,
                                          DEFAULT_CHANNEL,
                                          targetDetail.Product_Catalog_BI__r.External_ID_vod__c,
                                          targetDetail.Product_Catalog_BI__c,
                                          null,
                                          null,
                                          targetCycleFieldForce));

    }

}