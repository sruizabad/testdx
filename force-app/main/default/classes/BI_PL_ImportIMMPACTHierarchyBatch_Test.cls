@isTest(seeAllData = false)
public with sharing class BI_PL_ImportIMMPACTHierarchyBatch_Test {

    private static Profile profile;

    @isTest
    public static void test() {

        Test.startTest();

        Date startDate = Date.newInstance(2017, 2, 13);
        Date endDate = Date.newInstance(2018, 2, 13);

     
        BI_PL_TestDataUtility.createCustomSettings();


        profile = [SELECT Id FROM Profile WHERE Name LIKE 'BR_%' LIMIT 1];

        String countryCode = 'BR';
        String hierarchy = 'Hierarchy1';

        String territoryName1 = 'Territory1';
        String territoryName2 = 'Territory2';
        String territoryName3 = 'Territory3';
        String territoryName4 = 'Territory4';

        User defaultOwner = createUser('defaultOwnerte');
        User user1 = createUser('user1te');
        User user2 = createUser('user2te');
        User user3 = createUser('user3te');
        User user4 = createUser('user4te');

        List<User> usersList = new List<User> {defaultOwner, user1, user2, user3, user4};
        insert usersList;

        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name = 'BI_PL_SALES'];

        for (User u : usersList) {
            System.runAs(new User(Id = Userinfo.getUserId(), UserName = 'username_' + Math.random() + '@u.com', Country_Code_BI__c = countryCode)) {
                insert createPermissionSetAssignment(u.Id, permissionSet.Id);
            }
        }

        insert new BI_PL_Country_Settings__c(Name = 'Brazil', BI_PL_Country_Code__c = countryCode, BI_PL_Default_owner_user_name__c = defaultOwner.UserName, BI_PL_Specialty_field__c = 'Specialty_1_vod__c');

        Account account1 = new Account(Name = 'Account1');
        Account account2 = new Account(Name = 'Account2');
        Account account3 = new Account(Name = 'Account3');
        Account account4 = new Account(Name = 'Account4');
        Account account5 = new Account(Name = 'Account5');

        insert new List<Account> {account1, account2, account3, account4, account5};

        Territory territory1;
        Territory territory2;
        Territory territory3;
        Territory territory4;

        System.runAs(user1) {
            territory1 = new Territory(Name = territoryName1);
            territory2 = new Territory(Name = territoryName2);
            territory3 = new Territory(Name = territoryName3);
            territory4 = new Territory(Name = territoryName4);

            insert new List<Territory> {territory1, territory2, territory3, territory4};
        }

        Group group1 = [SELECT Id FROM Group WHERE RelatedId = :territory1.Id AND Type = 'Territory']; // account1 + account5
        Group group2 = [SELECT Id FROM Group WHERE RelatedId = :territory2.Id AND Type = 'Territory']; // account2
        Group group3 = [SELECT Id FROM Group WHERE RelatedId = :territory3.Id AND Type = 'Territory']; // account3
        Group group4 = [SELECT Id FROM Group WHERE RelatedId = :territory4.Id AND Type = 'Territory']; // account4

        AccountShare accountShare1 = new AccountShare(AccountId = account1.Id, UserOrGroupId = group1.Id);
        AccountShare accountShare2 = new AccountShare(AccountId = account2.Id, UserOrGroupId = group2.Id);
        AccountShare accountShare3 = new AccountShare(AccountId = account3.Id, UserOrGroupId = group3.Id);
        AccountShare accountShare4 = new AccountShare(AccountId = account4.Id, UserOrGroupId = group4.Id);
        AccountShare accountShare5 = new AccountShare(AccountId = account5.Id, UserOrGroupId = group1.Id);

        insert new List<AccountShare> {accountShare1, accountShare2, accountShare3, accountShare4, accountShare5};


        BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'FieldForceName', BI_TM_Country_Code__c = countryCode);

        insert fieldForce;
        // PLANIT RECORDS CREATION:

        BI_PL_Cycle__c targetCycle = new BI_PL_Cycle__c(BI_PL_Field_force__c = fieldForce.Id, BI_PL_Country_code__c = countryCode, BI_PL_Start_date__c = startDate, BI_PL_End_date__c = endDate);

        insert targetCycle;

        BI_PL_Position__c position1 = new BI_PL_Position__c(BI_PL_Country_code__c = countryCode, Name = territoryName1);
        BI_PL_Position__c position2 = new BI_PL_Position__c(BI_PL_Country_code__c = countryCode, Name = territoryName2);
        BI_PL_Position__c position3 = new BI_PL_Position__c(BI_PL_Country_code__c = countryCode, Name = territoryName3);

        insert new List<BI_PL_Position__c> {position1, position2, position3};

        BI_PL_Position_cycle__c positionCycle = new BI_PL_Position_cycle__c(BI_PL_Position__c = position1.Id, BI_PL_Cycle__c = targetCycle.Id, BI_PL_Hierarchy__c = hierarchy);

        positionCycle.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(countryCode, targetCycle.BI_PL_Start_date__c, targetCycle.BI_PL_End_date__c, fieldForce.Name, hierarchy, position1.Name);

        insert new List<BI_PL_Position_cycle__c> {positionCycle};

        BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_User__c = user1.Id,
                BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(countryCode, targetCycle.BI_PL_Start_date__c, targetCycle.BI_PL_End_date__c, fieldForce.Name, hierarchy, position1.Name, user1.External_id__c));

        insert pcu;

        // IMMPACT OBJECTS      

        Country_BI__c country = new Country_BI__c(Country_Code_BI__c = countryCode);

        insert country;

        Cycle_BI__c cycleBI = new Cycle_BI__c(Country_Lkp_BI__c = country.Id, Start_Date_BI__c = startDate, End_Date_BI__c = endDate);

        insert cycleBI;

        Portfolio_BI__c portfolio = new Portfolio_BI__c(Cycle_BI__c = cycleBI.Id, Country_Code_BI__c = countryCode, Active_SAP__c = true, Version_BI__c = 1);

        insert portfolio;

        // Target_Account_BI__c
        Target_Account_BI__c targetAccount1a = new Target_Account_BI__c(Account_BI__c = account1.Id, Portfolio_BI__c = portfolio.Id, Target_Calls_BI__c = 1, Version_BI__c = 1);
        Target_Account_BI__c targetAccount1b = new Target_Account_BI__c(Account_BI__c = account5.Id, Portfolio_BI__c = portfolio.Id, Target_Calls_BI__c = 1, Version_BI__c = 1);
        Target_Account_BI__c targetAccount2 = new Target_Account_BI__c(Account_BI__c = account2.Id, Portfolio_BI__c = portfolio.Id, Target_Calls_BI__c = 1, Version_BI__c = 1);
        Target_Account_BI__c targetAccount3 = new Target_Account_BI__c(Account_BI__c = account3.Id, Portfolio_BI__c = portfolio.Id, Target_Calls_BI__c = 1, Version_BI__c = 1);
        Target_Account_BI__c targetAccount4 = new Target_Account_BI__c(Account_BI__c = account4.Id, Portfolio_BI__c = portfolio.Id, Target_Calls_BI__c = 1, Version_BI__c = 1);

        insert new List<Target_Account_BI__c> {targetAccount1a, targetAccount1b, targetAccount2, targetAccount3, targetAccount4};

        Product_vod__c product1 = new Product_vod__c(Name = 'product1', Product_Type_vod__c = 'Detail');
        Product_vod__c product2 = new Product_vod__c(Name = 'product2', Product_Type_vod__c = 'Detail');
        Product_vod__c product3 = new Product_vod__c(Name = 'product3', Product_Type_vod__c = 'Detail');
        Product_vod__c product4 = new Product_vod__c(Name = 'product4', Product_Type_vod__c = 'Detail');

        insert new List<Product_vod__c> {product1, product2, product3, product4};

        // Target_Detail_BI__c
        Target_Detail_BI__c targetDetail1a = new Target_Detail_BI__c(Target_Account_BI__c = targetAccount1a.Id, Version_BI__c = 1, Portfolio_BI__c = portfolio.Id, Product_Catalog_BI__c = product1.Id);
        Target_Detail_BI__c targetDetail1b = new Target_Detail_BI__c(Target_Account_BI__c = targetAccount1a.Id, Version_BI__c = 1, Portfolio_BI__c = portfolio.Id, Product_Catalog_BI__c = product2.Id);
        Target_Detail_BI__c targetDetail1c = new Target_Detail_BI__c(Target_Account_BI__c = targetAccount1b.Id, Version_BI__c = 1, Portfolio_BI__c = portfolio.Id, Product_Catalog_BI__c = product3.Id);
        Target_Detail_BI__c targetDetail2 = new Target_Detail_BI__c(Target_Account_BI__c = targetAccount2.Id, Version_BI__c = 1, Portfolio_BI__c = portfolio.Id, Product_Catalog_BI__c = product2.Id);
        Target_Detail_BI__c targetDetail3 = new Target_Detail_BI__c(Target_Account_BI__c = targetAccount3.Id, Version_BI__c = 1, Portfolio_BI__c = portfolio.Id, Product_Catalog_BI__c = product3.Id);
        Target_Detail_BI__c targetDetailNotIncluded = new Target_Detail_BI__c(Target_Account_BI__c = targetAccount4.Id, Version_BI__c = 1, Portfolio_BI__c = portfolio.Id, Product_Catalog_BI__c = product4.Id);

        insert new List<Target_Detail_BI__c> {targetDetail1a, targetDetail1b, targetDetail1c, targetDetail2, targetDetail3, targetDetailNotIncluded};

        System.runAs(user1) {
            Database.executeBatch(new BI_PL_ImportIMMPACTHierarchyBatch(targetCycle.Id, hierarchy));
        }
        Test.stopTest();

        // ASSERTIONS

        // BI_PL_Preparation__c
        BI_PL_Preparation__c[] preparations = [SELECT Id, BI_PL_Position_cycle__c FROM BI_PL_Preparation__c ORDER BY BI_PL_Position_cycle__r.BI_PL_Position_name__c];

        System.assertEquals(1, preparations.size(), 'The number of created BI_PL_Preparation__c is not expected');

        BI_PL_Preparation__c preparation = preparations.get(0);

        ////throw new BI_PL_Exception(preparations + ' - (' + positionCycle1.Id + ' - ' + positionCycle2.Id + ' - ' + positionCycle3.Id);

        System.assertEquals(preparation.BI_PL_Position_cycle__c, positionCycle.Id, 'The position cycle does not match for the preparation.');

        //// BI_PL_Target_preparation__c
        BI_PL_Target_preparation__c[] targetPreparations = [SELECT Id, BI_PL_Header__c FROM BI_PL_Target_preparation__c ORDER BY BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c];

        // The accounts associated with the user that executed the process are account1 & account5.
        System.assertEquals(2, targetPreparations.size(), 'The number of created BI_PL_Target_preparation__c is not expected');

        System.assertEquals(targetPreparations.get(0).BI_PL_Header__c, preparation.Id, 'The preparation does not match for the target preparation.');
        System.assertEquals(targetPreparations.get(1).BI_PL_Header__c, preparation.Id, 'The preparation does not match for the target preparation.');

        //// BI_PL_Channel_detail_preparation__c
        BI_PL_Channel_detail_preparation__c[] channelDetails = [SELECT Id, BI_PL_Target__c FROM BI_PL_Channel_detail_preparation__c ORDER BY BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c];

        System.assertEquals(2, channelDetails.size(), 'The number of created BI_PL_Channel_detail_preparation__c is not expected');

        System.assertEquals(channelDetails.get(0).BI_PL_Target__c, targetPreparations.get(0).Id, 'The target preparation does not match for the channel detail.');
        System.assertEquals(channelDetails.get(1).BI_PL_Target__c, targetPreparations.get(1).Id, 'The target preparation does not match for the channel detail.');

        Map<Id, Id> targetByChannelDetail = new Map<Id, Id>();
        for(BI_PL_Channel_detail_preparation__c c : channelDetails)
            targetByChannelDetail.put(c.Id, c.BI_PL_Target__c);

        //// BI_PL_Detail_preparation__c
        BI_PL_Detail_preparation__c[] details = [SELECT Id, BI_PL_Product_name__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c, BI_PL_Channel_detail__c FROM BI_PL_Detail_preparation__c ORDER BY BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c];


        System.assertEquals(3, details.size(), 'The number of created BI_PL_Detail_preparation__c is not expected');

    }

    @isTest
    public static void test2(){
       upsert new Knipper_Settings__c(
            Account_Detect_Change_FieldList__c='FirstName,Middle_vod__c',
            Account_Detect_Changes_Record_Type_List__c='Professional_vod',
            External_ID__c = UserInfo.getProfileId(),
            SetupOwnerId = Userinfo.getOrganizationId()
        ) External_ID__c;
        User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String countryCode = testUser.Country_Code_BI__c;
        System.debug('***country code: ' + countryCode);
        insert new BI_PL_Country_Settings__c(Name = countryCode + testUser.Username, BI_PL_Country_Code__c = countryCode, BI_PL_Default_owner_user_name__c = testUser.UserName, BI_PL_Specialty_field__c = 'Specialty_1_vod__c');

        BI_PL_TestDataFactory.createTestAccounts(1, 'BR');
        String hierarchy = BI_PL_TestDataFactory.hierarchy;
        Account acc1 = BI_PL_TestDataFactory.listAcc.get(0);

        String userCountryCode = 'BR';

        BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'FieldForceName', BI_TM_Country_Code__c = 'BR');
        insert fieldForce;

        BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Start_date__c = Date.today(), BI_PL_End_date__c = Date.today() + 1, BI_PL_Field_force__c = fieldForce.Id);
        insert cycle;

        BI_PL_Cycle__c cycle2 = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];

        String cycleId = cycle2.Id;

        Country_BI__c country = new Country_BI__c(Country_Code_BI__c = 'BR');

        insert country;

        Cycle_BI__c cycleBI = new Cycle_BI__c(Country_Lkp_BI__c = country.Id, Start_Date_BI__c = Date.today(), End_Date_BI__c = Date.today() + 10);

        insert cycleBI;
        
        Portfolio_BI__c portfolio = new Portfolio_BI__c(Cycle_BI__c = cycleBI.Id, Country_Code_BI__c = 'BR', Active_SAP__c = true, Version_BI__c = 1);
        insert portfolio;
        
        Product_vod__c product1 = new Product_vod__c(Name = 'product1', Product_Type_vod__c = 'Detail');
        insert product1;
        
        BI_PL_TestDataFactory.createTestUsers(1, 'BR');

        BI_PL_Cycle__c targetCycle = new BI_PL_Cycle__c(BI_PL_Field_force__c = fieldForce.Id, BI_PL_Country_code__c = 'BR', BI_PL_Start_date__c = Date.today(), BI_PL_End_date__c = Date.today() + 10);
        insert targetCycle;
        
        Target_Account_BI__c targetAccount = new Target_Account_BI__c(Account_BI__c = acc1.Id, Portfolio_BI__c = portfolio.Id, Target_Calls_BI__c = 1, Version_BI__c = 1);
        insert targetAccount;
        Target_Detail_BI__c targetDetail = new Target_Detail_BI__c(Target_Account_BI__c = targetAccount.Id, Version_BI__c = 1, Portfolio_BI__c = portfolio.Id, Product_Catalog_BI__c = null);
        insert targetDetail;
        User testUser2 = BI_PL_TestDataFactory.listUser.get(0);
        System.runAs(testUser){
            Database.executeBatch(new BI_PL_ImportIMMPACTHierarchyBatch(targetCycle.Id, hierarchy));
        }
    }

    private static User createUser(String name) {
        return new User(Alias = name.substring(0, 5), Email = name + '@testorg.com', External_id__c = name,
                        EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US', ProfileId = profile.Id, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles');
    }
    private static PermissionSetAssignment createPermissionSetAssignment(Id userId, Id permissionSet) {
        return new PermissionSetAssignment(PermissionSetId = permissionSet, AssigneeId = userId);
    }
}