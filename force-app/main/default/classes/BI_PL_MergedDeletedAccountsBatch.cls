/**
 *	For all 'TargetPreparation' records, finds the ones with no account (removed)
 *	and fills the 'BI_PL_Target__c' empty field with the 'Account_Merge_History_vod__c.Account_vod__c' field
 *
 *	Used for merged accounts.
 *	@author	OMEGA CRM
 */

global without sharing class BI_PL_MergedDeletedAccountsBatch extends BI_PL_NightSubbatch {

	private List<String> errors = new List<String> {};
	private Integer numberOfTargetsProcessed = 0;
	String query;

	global BI_PL_MergedDeletedAccountsBatch(String countryCode, BI_PL_NightSubbatch nextBatchToExecute) {
		super(countryCode, nextBatchToExecute);

		query = 'SELECT Id, Name, BI_PL_Target_customer_id__c FROM BI_PL_Target_preparation__c WHERE BI_PL_Target_customer__c = null AND BI_PL_Country_code__c = \'' + countryCode + '\'';
	}

	global override Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global override void execute(Database.BatchableContext BC, List<sObject> scope) {
		try {
			List<BI_PL_Target_preparation__c> targets = (List<BI_PL_Target_preparation__c>)scope;

			Set<String> removedAccountsId = new Set<String>();

			for (BI_PL_Target_preparation__c target : targets) {
				removedAccountsId.add(((BI_PL_Target_preparation__c)target).BI_PL_Target_customer_id__c);
			}
			// Map<OldId, NewId> :
			Map<String, Id> oldToNew = new Map<String, Id>();

			// Prepare the old to new map:
			for (Account_Merge_History_vod__c entry : [SELECT Id, Name, Account_vod__c FROM Account_Merge_History_vod__c WHERE Name IN :removedAccountsId]) {
				oldToNew.put(entry.Name, entry.Account_vod__c);
			}
			// Fill targets with the new account based on the old one:
			for (BI_PL_Target_preparation__c target : targets) {
				if (!oldToNew.containsKey(target.BI_PL_Target_customer_id__c)) {
					// There's no mapping [old - new] for this account:
					errors.add(SObjectType.BI_PL_Target_preparation__c.getLabel()+' \'<a href=\'' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + target.Id + '\' >' + target.Name + '</a>\' : The record has no new target assigned in the Account Merge History Object');
				} else {
					target.BI_PL_Target_customer__c = oldToNew.get(target.BI_PL_Target_customer_id__c);
				}
			}

			Database.SaveResult[] updateResults = Database.update(targets, false);

			for (Integer i = 0; i < updateResults.size(); i++) {
				Database.SaveResult sr = updateResults.get(i);
				if (!sr.isSuccess()) {
					for (Database.Error err : sr.getErrors()) {
						errors.add(SObjectType.BI_PL_Target_preparation__c.getLabel()+' \'<a href=\'' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + targets.get(i).Id + '\' >' + targets.get(i).Name + '</a>\' : ' + err.getStatusCode() + ': ' + err.getMessage() + ' Fields : ' + err.getFields());
					}
				}
			}
		} catch (exception e) {
			errors.add(e.getMessage());
		}
		numberOfTargetsProcessed += scope.size();
	}

	global override void finish(Database.BatchableContext BC) {
		super.finish(BC);

		if (errors.size() > 0) {
			sendErrorEmail();
		}
	}

	protected override String generateErrorEmailBody() {
		String body = '';
		body = Label.BI_PL_Process_finished_following_errors;
		body += '<br/><ul>';
		for (String s : errors) {
			body += '<li/>' + s + '</li>';
		}
		body += '</ul>';
		body += '<p>' + (numberOfTargetsProcessed - errors.size()) + ' / ' + numberOfTargetsProcessed + ' targets updated successfully.</p>';

		body = '<h2>' + generateErrorEmailSubject() + '</h2>' + '<br/><br/>' + body;

		return body;
	}

	protected override String generateErrorEmailSubject() {
		return Label.BI_PL_Merge_deleted_accounts_batch_subject;
	}
}