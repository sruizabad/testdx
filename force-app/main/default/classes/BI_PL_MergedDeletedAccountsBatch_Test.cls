@isTest
private class BI_PL_MergedDeletedAccountsBatch_Test {

	@isTest static void test_method_one() {
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id = : UserInfo.getUserId()];
		String countryCode = testUser.Country_Code_BI__c;

		BI_PL_TestDataFactory.createTestAccounts(2, countryCode);
		BI_PL_TestDataFactory.createTestProduct(2, countryCode);
		BI_PL_TestDataFactory.createCycleStructure(countryCode);

		List<BI_PL_Position_Cycle__c> posCycList = [SELECT Id, BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
		List<Account> accList = [SELECT Id, External_ID_vod__c FROM Account];
		List<Product_vod__c> prodList = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

		BI_PL_TestDataFactory.createPreparations(countryCode, posCycList, accList, prodList);

		// Targets to null
		Account deletedAccount = [SELECT Id FROM Account WHERE Name = 'testAccountDF0'];

		String deletedId = deletedAccount.Id;
		delete deletedAccount;

		//insert new Account_Merge_History_vod__c(Name = deletedId, Account_vod__c = [SELECT Id FROM Account WHERE Name = 'testAccountDF1'].Id);

		BI_PL_MergedDeletedAccountsBatch toExecute = new BI_PL_MergedDeletedAccountsBatch(countryCode, null);

		Test.startTest();

		Database.executeBatch(toExecute);

		Test.stopTest();
	}

}