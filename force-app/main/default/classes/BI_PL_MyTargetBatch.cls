/**
*   Delete targets for specific cycle and hierarchy and copy new values in veeva object
*   @author Omega CRM
*/
global class BI_PL_MyTargetBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {
     private String cycle;
     private List<String> hierarchy;

     //Required for dynamic instantiation
     global BI_PL_MyTargetBatch (){}


     global BI_PL_MyTargetBatch (String cycle, List<String> hierarchy){
        System.debug('BI_PL_MyTargetBatch ' + cycle + ' - ' + hierarchy);
        this.cycle = cycle;
        this.hierarchy = hierarchy;     
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('BI_PL_DeleteCycleBatch START ' + cycle );

        //Get data from admin page if any (MLG)
        this.cycle = (!this.cycleIds.isEmpty() ? this.cycleIds.get(0) : this.cycle);
        this.hierarchy = (!this.hierarchyNames.isEmpty() ? this.hierarchyNames : this.hierarchy);
        /*
        return Database.getQueryLocator([SELECT Id, BI_PL_Country_code__c, BI_PL_Position_name__c, BI_PL_Start_date__c,
                                         BI_PL_End_date__c, OwnerId, BI_PL_External_id__c, BI_PL_Position_cycle__c
                                         FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN : hierarchy AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycle]);
        */
        return Database.getQueryLocator(
                [SELECT Id, Name, BI_PL_MSL_flag__c, BI_PL_Removed__c, 
                /* FROM BI_PL_PREPARATION */
                BI_PL_Target__r.BI_PL_Header__r.BI_PL_Country_code__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c, 
                /* FROM BI_PL_Target_preparation__c*/
                BI_PL_Target__c, BI_PL_Target__r.BI_PL_Target_customer__c, BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c, BI_PL_Target__r.BI_PL_Header__r.CurrencyIsoCode 
                FROM BI_PL_Channel_detail_preparation__c 
                WHERE BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN : hierarchy 
                    AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycle                     
                    AND BI_PL_Target__r.BI_PL_Target_customer__c != null /* Need an Account to work process */
                    AND BI_PL_MSL_flag__c = true 
                    AND BI_PL_Removed__c = false
                ORDER BY BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c]
        );
    }

    global void execute(Database.BatchableContext BC, List<BI_PL_Channel_detail_preparation__c> channelDetails) {
        System.debug('***EXECUTE');

        transferTargets(ChannelDetails);
        
    }

    public void transferTargets(List<BI_PL_Channel_detail_preparation__c> lstChannelDetails){
        
        List<Id> accountIds = new List<Id>();
        map<Id,BI_PL_Channel_detail_preparation__c> maptargets = new map<Id, BI_PL_Channel_detail_preparation__c>();

        //group channel detail by its Target
        for(BI_PL_Channel_detail_preparation__c inChannelDetail : lstChannelDetails)
        {
            if (inChannelDetail.BI_PL_Target__c != null && !maptargets.containsKey(inChannelDetail.BI_PL_Target__c))
                maptargets.put(inChannelDetail.BI_PL_Target__c, inChannelDetail);

            accountIds.add(inChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__c);
            system.debug('## accountIds: ' + inChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__c);
        }

        List<TSF_vod__c> tsfToAdd = new List<TSF_vod__c>();
        List<TSF_vod__c> tsfToDelete = new List<TSF_vod__c>();
        set<String> setExternal = new set<String>();

        //Generate list of tsf to be added
        for (BI_PL_Channel_detail_preparation__c sChannelDetail : maptargets.values() )
        {            
            if (!setExternal.contains(sChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c)){
                TSF_vod__c tsf = new TSF_vod__c();
                tsf.Account_vod__c = sChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__c;
                tsf.Country_Code_BI__c = sChannelDetail.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Country_code__c;
                tsf.External_Id_vod__c = sChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c; // Account Id + Territory Name
                tsf.Territory_vod__c = sChannelDetail.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c; //Territory Name
                tsf.Name = sChannelDetail.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c; //TSF Name the same as territory name
                tsf.My_Target_vod__c = true;
                tsf.CurrencyIsoCode = sChannelDetail.BI_PL_Target__r.BI_PL_Header__r.CurrencyIsoCode;

                tsfToAdd.add(tsf);
                setExternal.add(sChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c);
            }
            system.debug('## sChannelDetail: ' + sChannelDetail);
        }

        //Get list of tfs for the accounts which belong to current targets
        Map<String, Map<String,TSF_vod__c>> tsfList = new Map<String, Map<String,TSF_vod__c>>();

        for(TSF_vod__c tsfElement : [SELECT Id, Account_vod__c, Territory_vod__c 
                                     FROM TSF_vod__c 
                                     WHERE Account_vod__c IN :accountIds])
        {
            if (!tsfList.containsKey(tsfElement.Account_vod__c))
                tsfList.put(tsfElement.Account_vod__c, new Map<String,TSF_vod__c>());

            if (!tsfList.get(tsfElement.Account_vod__c).containsKey(tsfElement.Territory_vod__c))
                tsfList.get(tsfElement.Account_vod__c).put(tsfElement.Territory_vod__c, tsfElement);
        }

        System.debug('>>>> List of TSF: '+tsfList);

         //Find those tsf whose account and territory are in current targets

        for (BI_PL_Channel_detail_preparation__c sChannelDetail : maptargets.values() )
        {
            if(tsfList.containsKey(sChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__c) 
                && tsfList.get(sChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__c).containsKey(sChannelDetail.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c))
            {
                tsfToDelete.add(
                    tsfList.get(sChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__c).get(sChannelDetail.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c)
                );
            }
        }

        System.debug('>>>> List of TSF to add: '+tsfToAdd);
        System.debug('>>>> List of TSF to remove: '+tsfToDelete);

        //Delete tsf
        delete tsfToDelete;
        System.debug('TSF REMOVED');

        //Insert tsf
        insert tsfToAdd;
        System.debug('TSF ADDED');
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('BI_PL_MyTargetBatch finish');
    }

}