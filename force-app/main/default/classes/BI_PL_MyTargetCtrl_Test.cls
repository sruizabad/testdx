@isTest
private class BI_PL_MyTargetCtrl_Test {

    private static String userCountryCode;
    private static String hierarchy = BI_PL_TestDataFactory.hierarchy;
    private static String sChannel = BI_PL_TestDataFactory.SCHANNEL;
    private static User testUser;
    private static String sProfileName;
    
    private static void getData(){
        testUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        userCountryCode = testUser.Country_Code_BI__c;

        sProfileName = userCountryCode + '_HOME_OFFICE';
        testUser = [SELECT Id, ProfileId FROM User WHERE Country_Code_BI__c = :userCountryCode AND Profile.Name = :sProfileName AND IsActive = true LIMIT 1];
    }

    @testSetup static void setup() {

        testUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        userCountryCode = testUser.Country_Code_BI__c;
        BI_PL_TestDataFactory.createTestUsers(1,userCountryCode);
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
    }
    

    @isTest static void test_method_one() {

        getData();

        String currentCycleId = [SELECT id FROM BI_PL_CYCLE__c LIMIT 1].Id;
        
        System.runAs(testUser){

            BI_PL_MyTargetCtrl controllerMyTarget = new BI_PL_MyTargetCtrl();


            controllerMyTarget.selectedCycle = currentCycleId;
            controllerMyTarget.selectedHierarchy  = hierarchy;
            controllerMyTarget.userCountryCode = userCountryCode;
            controllerMyTarget.cycleChanged();
            controllerMyTarget.selectedHierarchy  = hierarchy;

            Test.startTest();
            controllerMyTarget.myTarget();
            
            Test.stopTest();
            controllerMyTarget.checkBatchStatus();

            System.assertEquals(controllerMyTarget.errors, null);
                       
            Boolean refreshAllowed = controllerMyTarget.refreshAllowed;

            System.assertEquals(refreshAllowed, true);
            
        }
    }

    @isTest static void test_method_two() {    

        getData();

        System.runAs(testUser){

            BI_PL_MyTargetCtrl controllerMyTarget = new BI_PL_MyTargetCtrl();

            Test.startTest();
                controllerMyTarget.hierarchyChanged();
                controllerMyTarget.exportMyTarget();    
            Test.stopTest();

            system.assertNotEquals([SELECT Id From AsyncApexJob], null);
        }
    }
}