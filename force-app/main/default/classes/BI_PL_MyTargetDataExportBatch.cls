global class BI_PL_MyTargetDataExportBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    private static final String SFILENAME = 'MyTargetToDelete_';
    private String cycle;
    private set<Id> setAccounts;
    private set<String> setTerritories;

    private String extension;
    private Attachment att;
    
    String sCycleName;
    List<Attachment> lstAttach;
    String attId;
    set<String> setAllAtt;
    
    global BI_PL_MyTargetDataExportBatch(set<Id> setAccounts, set<String> setTerritories, String cycleId){
        System.debug('BI_PL_MyTargetDataExportBatch ' + setAccounts.size() + ' - ' + setTerritories.size());
        this.setAccounts = setAccounts;
        this.setTerritories = setTerritories;
        this.cycle = cycleId;
        this.extension = '.csv';        
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        this.sCycleName = [SELECT Id, Name FROM BI_PL_Cycle__c WHERE Id = :cycle].Name;
        
        setAllAtt = new set<String>();
        //Create Attachment File that save export dile
        Attachment att = new Attachment(
                                Name = SFILENAME + this.sCycleName + '_' + Datetime.now().format('YYYYMMDD') + this.extension, 
                                ParentId = this.cycle, 
                                Body = Blob.valueOf(getHeaderRow()+'\r\n')
                            );
        insert att;

        this.attId = att.Id;
        setAllAtt.add(att.Id);
        
        return Database.getQueryLocator(
            [SELECT Id, Account_vod__c, Territory_vod__c, Country_Code_BI__c, CurrencyIsoCode 
             FROM TSF_vod__c 
             WHERE Account_vod__c NOT IN :setAccounts 
                AND Territory_vod__c IN :setTerritories
                ORDER BY Territory_vod__c]
        );
    }

    global void execute(Database.BatchableContext BC, List<TSF_vod__c> lstTSFvod) {
    
        system.debug('## lstTSFvod: ' + lstTSFvod.size() );
        system.debug('## attId: ' + attId);

        String row, newRow = '';
        boolean bHeapSize = false;
        
        lstAttach = new List<Attachment>();

        //Load attachment where export file is saving
        Attachment workingDoc = [SELECT Id, Body FROM Attachment WHERE Id = :attId LIMIT 1];
        lstAttach.add(workingDoc);

        for(TSF_vod__c tsfElement : lstTSFvod) {
            
            System.debug('Heap Size: ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());

            newRow += '"""' + tsfElement.Id + '"",' 
                    + '"""' + tsfElement.Territory_vod__c + '",' 
                    + '"' + tsfElement.Account_vod__c + '",' 
                    + '"' + tsfElement.Country_Code_BI__c + '",' 
                    + '"' + tsfElement.CurrencyIsoCode + '"\r\n';

            System.debug('Heap Size: ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());

            //check Heap Size is not coming over To Limit
            if ( ((Limits.getHeapSize() + 7500000) > Limits.getLimitHeapSize() && !bHeapSize) || Test.isRunningTest() ){

                bHeapSize = true;
                system.debug('## New Attachment Heap Size: ' + Limits.getHeapSize() );
                row = workingDoc.Body.toString() + newRow;
                workingDoc.Body = Blob.valueOf(row);
                
                newRow = '';

                //Create new Attachment
                workingDoc = new Attachment(
                             Name = SFILENAME + this.sCycleName + '_' + Datetime.now().format('YYYYMMDD') + this.extension, 
                             ParentId = this.cycle, 
                             Body = Blob.valueOf(getHeaderRow()+'\r\n')
                        );
                
                lstAttach.add(workingDoc);
                System.debug('Heap Size: ' + Limits.getHeapSize() );
            }

            system.debug('## tsfElement: ' + tsfElement);
        }
        
        if ( String.isNotBlank(newRow) ){
            row = workingDoc.Body.toString() + newRow;
            workingDoc.Body = Blob.valueOf(row);
        }

        upsert lstAttach;

        for (Attachment inAtt : lstAttach){
            setAllAtt.add(inAtt.Id);
        }

        this.attId = workingDoc.Id;

    }

    private String getHeaderRow(){
        
        return '"""Id","Territory","AccountId","Country","Currency"';
    }

    global void finish(Database.BatchableContext BC) {
        System.debug('BI_PL_MyTargetDataExportBatch finish');

        String sMailText = '';
        for (String sAtt : setAllAtt){
            sMailText += System.URL.getSalesforceBaseURL().toExternalForm() + '/' + sAtt +'\r\n';
        }
        //Send an email with link with the generated export file
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        String[] toAddresses = new String[] {UserInfo.getUserEmail()};
        mail.setToAddresses(toAddresses);  
        mail.setSubject('Planit - My Targets to Delete');
        
        mail.setPlainTextBody(sMailText);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}