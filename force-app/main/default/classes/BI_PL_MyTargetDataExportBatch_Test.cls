@isTest
private class BI_PL_MyTargetDataExportBatch_Test
{

    private static final String STERRITORY = 'TerritoryName';
    private static String userCountryCode;
    private static List<String> hierarchy = new List<String>{BI_PL_TestDataFactory.hierarchy};
    private static List<Account> lstNewAccount;

	@testSetup  static void setup() {
        
        Integer iNumAccounts = 6;
        list<TSF_vod__c> lstSFvod = new list<TSF_vod__c>();
        TSF_vod__c tsf;

        User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        userCountryCode = testUser.Country_Code_BI__c;
        
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);

        lstNewAccount = BI_PL_TestDataFactory.createTestAccounts(iNumAccounts, userCountryCode);

        for(Integer i = 0; i < iNumAccounts/2; i++){
            tsf = new TSF_vod__c(
                Account_vod__c = lstNewAccount.get(i).Id,
                Country_Code_BI__c = userCountryCode,
                External_Id_vod__c = lstNewAccount.get(i).Id + '_' + STERRITORY, // Account Id + Territory Name
                Territory_vod__c = STERRITORY, //Territory Name
                My_Target_vod__c = true,
                CurrencyIsoCode = 'EUR'
            );
            lstSFvod.add(tsf);
        }

        insert lstSFvod;

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);
        
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        List<BI_PL_Position__c> positions = [SELECT Id FROM BI_PL_Position__c WHERE Name LIKE 'Pos1%'];

        BI_PL_TestDataFactory.createTestProduct(1, userCountryCode);
        
        List<Account> listAcc = new List<Account>();
        for(Integer i = iNumAccounts/2; i < iNumAccounts; i++){
            system.debug('## Account: ' + lstNewAccount.get(i) );
            listAcc.add(lstNewAccount.get(i));
            //setAccounts.add(lstNewAccount.get(i).Id);
        }
        
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        
        BI_PL_TestDataFactory.createTestTerritoryField(userCountryCode, listAcc.get(0), positions.get(0));
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);        
    }


	@isTest
	static void test_MyTargetFlagBatch()
	{
		
		BI_PL_Cycle__c cycle = [SELECT Id, Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        
        Test.startTest();
        
        Database.executeBatch(new BI_PL_MyTargetMSLFlagBatch(cycle.Name, hierarchy));        

        Test.stopTest();
	}

    @isTest
    static void test_ExportMethod()
    {
        
        set<Id> setAccounts = new set<Id>();
        set<String> setTerritories = new set<String>{BI_PL_TestDataFactory.hierarchy,STERRITORY};

        BI_PL_Cycle__c cycle = [SELECT Id, Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];

        for(Account inAccount : [SELECT Id FROM Account LIMIT 2] ){
            setAccounts.add(inAccount.Id);
        }


        Test.startTest();

        Database.executeBatch(new BI_PL_MyTargetDataExportBatch(setAccounts, setTerritories, cycle.Id) );

        Test.stopTest();
    }
}