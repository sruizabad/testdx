global class BI_PL_MyTargetMSLFlagBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {
	
	private String cycle;
	private List<String> hierarchy;

	set<Id> accountIds;
    set<String> territoryIds;
	
	global BI_PL_MyTargetMSLFlagBatch() { }

	global BI_PL_MyTargetMSLFlagBatch(String cycle, List<String> hierarchy) {
		System.debug('BI_PL_MyTargetMSLFlagBatch ' + cycle + ' - ' + hierarchy);
        this.cycle = cycle;
        this.hierarchy = hierarchy;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		//Get data from admin page if any (MLG)
        this.cycle = (!this.cycleIds.isEmpty() ? this.cycleIds.get(0) : this.cycle);
        this.hierarchy = (!this.hierarchyNames.isEmpty() ? this.hierarchyNames : this.hierarchy);
        System.debug(loggingLevel.Error, '*** cycleIds: ' + cycleIds);
        System.debug(loggingLevel.Error, '*** cycle: ' + cycle);
        System.debug(loggingLevel.Error, '*** hierarchyNames: ' + hierarchyNames);
        System.debug(loggingLevel.Error, '*** hierarchy: ' + hierarchy);
		
		accountIds = new set<Id>();
		territoryIds = new set<String>();

		return Database.getQueryLocator(
			[SELECT Id, Name, BI_PL_MSL_flag__c, BI_PL_Removed__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c, 
                    BI_PL_Target__r.BI_PL_Target_customer__c, BI_PL_Target__r.BI_PL_Target_customer__r.Name 
                FROM BI_PL_Channel_detail_preparation__c 
                WHERE BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN : hierarchy 
                    AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycle 
                    AND BI_PL_Target__r.BI_PL_Target_customer__c != null /* Need an Account to work process */
                    AND BI_PL_MSL_flag__c = true 
                    AND BI_PL_Removed__c = false 
                ORDER BY BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c]
		);
	}

   	global void execute(Database.BatchableContext BC, List<BI_PL_Channel_detail_preparation__c> lstChannelDetails) {

		system.debug('## lstChannelDetails: ' + lstChannelDetails.size() );

		//Get all Accounts and Territory in set
		for(BI_PL_Channel_detail_preparation__c inChannelDetail : lstChannelDetails) {
            accountIds.add(inChannelDetail.BI_PL_Target__r.BI_PL_Target_customer__c);
            territoryIds.add(inChannelDetail.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c);
        }
	
	}
	
	global void finish(Database.BatchableContext BC) {
		
		system.debug('## accountIds: ' + accountIds);
		system.debug('## territoryIds: ' + territoryIds);
		//Execute Batch Process if there are some account
		if (!accountIds.isEmpty() && !territoryIds.isEmpty())
			Database.executeBatch(new BI_PL_MyTargetDataExportBatch(accountIds, territoryIds, this.cycle), 100);
	}
	
}