/**
 *	Set several batches to be executed.
 *	@author	OMEGA CRM
 */
global class BI_PL_NightProcess implements Schedulable {

	public static String SCHEDULED_JOB_NAME = 'Planit - Night process ';
	public static String SCHEDULED_JOB_TYPE = '7';

	String countryCode;

	public BI_PL_NightProcess(String countryCode) {
		this.countryCode = countryCode;
	}

	global void execute(SchedulableContext sc) {

		BI_PL_Country_settings__c countrySetting = [SELECT Id, BI_PL_Execute_night_process__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = :countryCode];

		if (countrySetting.BI_PL_Execute_night_process__c) {
			//BI_PL_HCOAggregatedBatch hcoAggTRXBatch = new BI_PL_HCOAggregatedBatch(countryCode, null);
			BI_PL_AffiliationDataToDetailBatch affiliationDataToDetailBatch = new BI_PL_AffiliationDataToDetailBatch(countryCode, null);
			BI_PL_UpdateVeevaInteractions updateVeevaInteractions = new BI_PL_UpdateVeevaInteractions(countryCode);
			BI_PL_RefreshUsersFromBITMANBatch refreshUsersFromBitman = new BI_PL_RefreshUsersFromBITMANBatch(countryCode);
			Database.executeBatch(affiliationDataToDetailBatch);
			//Database.executeBatch(hcoAggTRXBatch,100);
			Database.executeBatch(updateVeevaInteractions,50);
			Database.executeBatch(refreshUsersFromBitman);
		}
	}
	/**
	 *	Schedules a Night process for the specified country.
	 * 	The defined hour and minutes are translated to the country's specified time zone.
	 * 	Check the supported timezones here: https://help.salesforce.com/articleView?id=admin_supported_timezone.htm&type=5
	 *	@author	OMEGA CRM
	 */
	public static void scheduleNightProcessForAllPlanitCountries(Integer hour, Integer minutes) {
		// The process is scheduled for the countries having a PLANiT Country Settings record and having the 'BI_PL_Execute_night_process__c' check set to true.
		for (BI_PL_Country_settings__c countrySetting : [SELECT Id, BI_PL_Default_time_zone__c, BI_PL_Execute_night_process__c, BI_PL_Country_code__c FROM BI_PL_Country_settings__c]) {
			String countrySettingTimezone = countrySetting.BI_PL_Default_time_zone__c;

			if (countrySetting.BI_PL_Execute_night_process__c && String.isNotBlank(countrySettingTimezone)) {
				Date t = date.today();
				DateTime dateT = DateTime.newInstance(t.year(), t.month(), t.day(), hour, minutes, 0);
				List<String> theTime = dateT.format('HH mm', countrySettingTimezone).split(' ');

				Integer hours = Integer.valueOf(theTime.get(0));
				Integer mins = Integer.valueOf(theTime.get(1));

				scheduleNightProcess(countrySetting.BI_PL_Country_code__c, hours, mins);
			}
		}
	}

	/**
	 *	Schedules a Night process for the specified country.
	 *	@author	OMEGA CRM
	 */
	public static void scheduleNightProcess(String countryCode, Integer hours, Integer minutes) {
		system.schedule(SCHEDULED_JOB_NAME + countryCode, '0 ' + minutes + ' ' + hours + ' * * ?', new BI_PL_NightProcess(countryCode));
	}

}