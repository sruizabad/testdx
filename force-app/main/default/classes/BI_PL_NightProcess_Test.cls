@isTest
private class BI_PL_NightProcess_Test {
	
	@isTest static void test_method_one() {
		BI_PL_TestDataFactory.createCountrySetting();
		BI_PL_TestDataFactory.createCycleStructure('US');
		BI_PL_NightProcess series = new BI_PL_NightProcess('US');

		//BI_PL_NightProcess.scheduleNightProcess('US', 10, 30);
		BI_PL_Country_settings__c countrySetting = [SELECT Id, BI_PL_Default_time_zone__c, BI_PL_Execute_night_process__c, BI_PL_Country_code__c FROM BI_PL_Country_settings__c LIMIT 1];
		countrySetting.BI_PL_Default_time_zone__c = 'US';
		countrySetting.BI_PL_Country_code__c = 'US';
		countrySetting.BI_PL_Execute_night_process__c = true;
		update countrySetting;
		BI_PL_NightProcess.scheduleNightProcessForAllPlanitCountries(10,30);
		/*Test.startTest();
		series.execute();
		Test.stopTest();*/
	}	
}