global virtual class BI_PL_NightSubbatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {

	protected BI_PL_NightSubbatch nextBatchToExecute;
	protected String countryCode;

	global BI_PL_NightSubbatch(){}

	global BI_PL_NightSubbatch(String countryCode, BI_PL_NightSubbatch nextBatchToExecute) {
		this.nextBatchToExecute = nextBatchToExecute;
		this.countryCode = countryCode;
	}

	global virtual Database.QueryLocator start(Database.BatchableContext BC) {
		return null;
	}

	global virtual void execute(Database.BatchableContext BC, List<sObject> scope) {

	}

	global virtual void finish(Database.BatchableContext BC) {
		if (nextBatchToExecute != null)
			Database.executeBatch(nextBatchToExecute);
	}

	protected String getAdministratorEmail() {
		return [SELECT Email FROM User WHERE UserName = : BI_PL_CountrySettingsUtility.getCountrySettingsForCountry(countryCode).BI_PL_Admin_user_name__c].Email;
	}
	protected Id getAdministratorId() {
		return [SELECT Id FROM User WHERE UserName = : BI_PL_CountrySettingsUtility.getCountrySettingsForCountry(countryCode).BI_PL_Admin_user_name__c].Id;
	}

	protected void sendErrorEmail() {
		String email = '';
		String ex;
		try {
			email = getAdministratorEmail();
		} catch (exception e) {
			ex = e.getMessage();
		}

		if(String.isBlank(email))
			throw new BI_PL_Exception('There was an error while retrieving the country default user email' + ((ex != null) ? ': ' + ex : '.'));

		try {
			BI_PL_EmailUtility.sendEmailWithTemplate(new List<String> {email}, generateErrorEmailSubject(), generateErrorEmailBody());
		} catch (exception e) {
			System.debug('Unable to send email: ' + e.getMessage());
		}
	}

	protected virtual String generateErrorEmailBody() {
		return null;
	}
	protected virtual String generateErrorEmailSubject() {
		return null;
	}

}