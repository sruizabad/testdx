/**
 *   10/07/2017
 *  - GLOS-343: display a warning message if the country custom setting is not set.
*/
global with sharing class BI_PL_PlanitAdminCtrl {

    /*
        Preparation Clonation and Synchronization Variables
    */
    public list<BI_PL_Preparation__c> preparationsToClone {get; set;}

    //public list<BI_PL_Preparation__c> preparationList {get; set;}
    public String preparationsToCloneJSON {get; set;}
    //public map<Id, String> ownerNamesByPreparationId {get; set;}

    public BI_PL_PlanitAdminCtrlWrapper controller {
        get{
            return new BI_PL_PlanitAdminCtrlWrapper(this);
        }
        set;
    }
    /*
        Synchronization Variables
    */
    public transient list<BI_PL_Preparation__c> preparationsToSync {get; set;} //REVIEW
    public transient list<BI_PL_Preparation__c> lSyncPreparations {get; set;}
    public List<BI_PL_Preparation__c> lstToShowPreparations {get; set;}
    public String preparationsToSyncJSON {get; set;}
    public transient map<Id, String> ownerNamesBySyncPreparationId {get; set;} //NOT USED
    public List<SelectOption> lstHierarchies {get; set;}
    public List<SelectOption> lstCycles {get; set;}
    public List<SelectOption> lstCyclesSync {get; set;}
    public String selectedHierarchy {get; set;}
    public String selectedCycle {get; set;}
    public String selectedFieldForce {get; set;}
    public Date selectedStartDate {get; set;}
    public Date selectedEndDate {get; set;}
    public Map<Id, String> mapFieldForce {get; set;}
    public Map<Id, Date> mapStartDates {get; set;}
    public Map<Id, Date> mapEndDates {get; set;}
    public transient Map<BI_PL_Cycle__c, Set<String>> mapCycles {get; set;} //NOT USED
    public transient Map<String, List<SelectOption>> mapCycleHierarchy {get; set;} //NOT USED
    public Map<Id, Boolean> canSyncCycle {get; set;}
    public String canSyncCycleJSON {get; set;}
    public Map<Id, String> syncedHierarchy {get; set;}
    public Boolean syncModal {get; set;}

    public void showModal() {
        syncModal = true;
    }
    public void closeModel() {
        syncModal = false;
    }

    // This boolean indicates us if we have an available cycle to which
    // we shall clone
    public boolean canClone {get; set;}
    public boolean canSync {get; set;}
    public boolean canSyncError {get; set;}
    public String userCountryCode {get; set;}
    public String noneLabel {get; set;}
    public String approvedLabel {get; set;}

    /**
     * Actions report
     */
    public String selectedChannel {get; set;}
    public List<SelectOption> activeChannels {
        get {
            if (activeChannels == null) {
                activeChannels = new List<SelectOption>();
                for (Schema.PicklistEntry pl : BI_PL_Channel_detail_preparation__c.fields.BI_PL_Channel__c.getDescribe().getPicklistValues()) {
                    activeChannels.add(new SelectOption(pl.getValue(), pl.getLabel()));
                }
            }
            return activeChannels;
        }
        set;
    }
    public transient String actionReport {get; set;}
    public transient String actionReportDocumentId {get; set;}
    public BI_PL_Channel_detail_preparation__c channelFilter {get; set;}


    private BI_PL_Country_settings__c planitCountrySettings;

    public Boolean isPlanitCountrySettingSet {
        get {
            return planitCountrySettings != null;
        }
    }

    public Boolean isCloneAllowed {
        get {
            if (planitCountrySettings == null)
                loadPlanitSettings();
            return planitCountrySettings.BI_PL_Clone__c;
        }
    }
    public Boolean isImportFromIMMPACTAllowed {
        get {
            if (planitCountrySettings == null)
                loadPlanitSettings();
            return planitCountrySettings.BI_PL_Import_from_IMMPACT__c;
        }
    }

    public Boolean isImportFromBITMANAllowed {
        get {
            if (planitCountrySettings == null)
                loadPlanitSettings();
            return planitCountrySettings.BI_PL_Import_from_BITMAN__c;
        }
    }

    public Boolean isSplitCycleAllowed {
        get {
            if (planitCountrySettings == null)
                loadPlanitSettings();
            return planitCountrySettings.BI_PL_Split_cycle__c;
        }
    }


    public Map<Id, BI_PL_Cycle__c> cycles {get; set;}

    /*
        Common Variables
    */


    /*****************
     ** Constructor **
     *****************/

    public BI_PL_PlanitAdminCtrl() {
        // Setting common data
        system.debug('%%% setUserCountryCode()');
        setUserCountryCode();

        //system.debug('%%% setPreparationsToSync()');
        //setPreparationsToSync();
        //lstToShowPreparations = new List<BI_PL_Preparation__c>();

        noneLabel = Label.Common_none_vod;
        System.debug('***mapCycles = ' + mapCycles);
        System.debug('***lstHierarchies = ' + lstHierarchies);

        if (planitCountrySettings == null)
            loadPlanitSettings();

        //loadCycleOptions();
        //loadPreparations();
    }

    private void loadPlanitSettings() {
        try {
            planitCountrySettings = [SELECT BI_PL_Channel__c, BI_PL_Multichannel__c, BI_PL_Clone__c, BI_PL_Import_from_BITMAN__c, BI_PL_Import_from_IMMPACT__c, BI_PL_Split_cycle__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = : userCountryCode LIMIT 1];
        } catch (exception e) {

        }
    }

    /**
     *  Loads the cycle options for the picklist.
     *  @author OMEGA CRM
     */
    private void loadCycleOptions() {
        lstCycles = new List<SelectOption>();
        lstCycles.add(new SelectOption('', Label.Common_None_vod));

        cycles = new Map<Id, BI_PL_Cycle__c>([SELECT Id, Name, BI_PL_Field_force__r.Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :userCountryCode ORDER BY Name]);
        for (BI_PL_Cycle__c c : cycles.values()) {
            lstCycles.add(new SelectOption(c.Id, c.Name));
        }
    }

    @RemoteAction
    public static List<sObject> getAffiliationRecordTypes() {
        String query = 'SELECT Id, Name FROM RecordType WHERE SObjectType = \'BI_PL_Affiliation__c\'';
        return Database.query(query);
    }

    @RemoteAction
    public static List<sObject> getCycles(String countryCode,Boolean onlyActives, String additionalConditions) {

        String cCode = getCurrentUserCountryCode();
        if(countryCode != null) {
            cCode = countryCode;
        }

        String query = 'SELECT Id, Name, BI_PL_Field_force__r.Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c  WHERE BI_PL_Country_code__c = \'' + cCode + '\'';
        if (onlyActives) {
            query += ' AND BI_PL_Type__c = \'active\' ';
        }
        if (String.isNotBlank(additionalConditions))
            query += ' AND ' + additionalConditions + ' ';

        query += ' ORDER BY Name ';

        return Database.query(query);
    }

    @RemoteAction
    @Readonly
    public static Set<String> getHierarchies(String cycleId, String userId) {
        Set<String> hierarchies = new Set<String>();
        String query = 'SELECT BI_PL_Hierarchy__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = :cycleId';
        if(userId != null) {
            query += ' AND Id IN (SELECT BI_PL_Position_cycle__c FROM BI_PL_Position_cycle_user__c WHERE BI_PL_User__c = :userId AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId)';
        }
        for (BI_PL_Position_cycle__c c : Database.query(query)) {
            hierarchies.add(c.BI_PL_Hierarchy__c);
        }
        if(hierarchies.size() == 0 && userId != null) {
            throw new BI_PL_Exception('User has not available hierarchies in selected cycle');
        }
        return hierarchies;
    }

    @RemoteAction
    public static String runProcess(String countryCode, List<String> cycleIds, List<String> hierarchyNames, List<String> channels, String className, Map<String, Object> params, Integer batchSize) {
        Type processType = Type.forName(className);
        BI_PL_PlanitProcess process = (BI_PL_PlanitProcess) processType.newInstance();

        process.init(countryCode, cycleIds, hierarchyNames, channels, params);

        String batchId = Database.executeBatch((Database.Batchable<sObject>) process, batchSize);

        return batchId;

    }

    @RemoteAction
    @Readonly
    public static Map<String, Object> getChannels(String cycleId, List<String> hierarchies, Boolean searchInHierarchy) {

        Boolean doSearch = searchInHierarchy == null ? false : searchInHierarchy;

        Map<String, Object> totals = new Map<String, Object>{
            'plans' => 0,
            'details' => 0
        };

        Map<String, Object> channels = new Map<String, Object>();
        Set<String> channelsInHierarchy = new Set<String>();

        String userCountryCode = getCurrentUserCountryCode();

        System.debug(logginglevel.ERROR, '//countrycode ' + userCountryCode);
        System.debug(logginglevel.ERROR, '//selectedHierarchy ' + hierarchies);
        System.debug(logginglevel.ERROR, '//selectedCycle ' + cycleId);

        if (doSearch) {
            Map<Id, BI_PL_Preparation__c> preparations = new Map<Id, BI_PL_Preparation__c>([SELECT Id
                    FROM BI_PL_Preparation__c
                    WHERE BI_PL_Country_code__c = :userCountryCode
                                                  AND BI_PL_position_cycle__r.BI_PL_hierarchy__c IN :hierarchies
                                                  AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
                                                          ORDER BY Name]);

            System.debug(logginglevel.ERROR, '**preparation' +  preparations);
            totals.put('plans', preparations.size());

            for(AggregateResult ag : [SELECT sum(BI_PL_Number_details__c) totalDetails, count(Id) totalChannelDetails, BI_PL_Channel__c channel FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Target__r.BI_PL_Header__c  IN: preparations.keySet() AND BI_PL_MSL_Flag__c = true GROUP BY BI_PL_Channel__c ]) {
                totals.put('details',  Integer.valueOf(totals.get('details')) + Integer.valueOf(ag.get('totalDetails')));
                channelsInHierarchy.add(String.valueOf(ag.get('channel')));
            }

            //for (BI_PL_Channel_detail_preparation__c channelDetail : [SELECT BI_PL_Channel__c, BI_PL_Number_details__c
            //        FROM BI_PL_Channel_detail_preparation__c
            //        WHERE BI_PL_Target__r.BI_PL_Header__c = :preparations.keySet()]) {

            //    totals.put('details',  Integer.valueOf(totals.get('details')) + channelDetail.BI_PL_Number_details__c);
            //    channelsInHierarchy.add(channelDetail.BI_PL_Channel__c);
            //}
        }


        //Get all values from describe
        for (Schema.PicklistEntry pl : BI_PL_Channel_detail_preparation__c.fields.BI_PL_Channel__c.getDescribe().getPicklistValues()) {
            if ((doSearch && channelsInHierarchy.contains(pl.getValue())) || !doSearch) {
                channels.put(pl.getValue(), pl.getLabel());
            }
        }

        //get totals
        

        channels.put('__totals__', totals);
        return channels;
    }

    @RemoteAction
    @Readonly
    public static List<BI_PL_Preparation__c> getPlans(String cycleId, String hierarchyName, String position) {
        if(cycleId == null) throw new BI_PL_Exception('Cycle is mandatory to get plans');
        if(hierarchyName == null) throw new BI_PL_Exception('Hierarchy is mandatory to get plans');

        List<String> positions = BI_PL_HierarchyWrapper.getHierarchy(cycleId, hierarchyName).getPositionSubhierarchy(position).getPositionIdList();
        String query = 'SELECT Id, Owner.Name FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__r.BI_PL_Position__c IN :positions ' +
            ' AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId '+ 
            ' AND BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchyName ';

        return (List<BI_PL_Preparation__c>) Database.query(query);

    }

    @RemoteAction
    @Readonly
    public static BI_PL_HierarchyWrapper.BI_PL_HierarchyNodeWrapper getPositions(String cycleId, String hierarchyName, String userId) {
        return BI_PL_HierarchyWrapper.getUserHierarchy(cycleId, hierarchyName, userId);
    }


    private static String getCurrentUserCountryCode() {
        return [SELECT Id, Country_Code_BI__c FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
    }

    /**
     *  Loads the hierarchy options for the picklist.
     *  @author OMEGA CRM
     */
    private void loadHierarchyOptions() {
        lstHierarchies = new List<SelectOption>();
        lstHierarchies.add(new SelectOption('', Label.Common_None_vod));

        Set<String> hierarchies = new Set<String>();
        for (BI_PL_Position_cycle__c c : [SELECT BI_PL_Hierarchy__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = :selectedCycle]) {
            if (!hierarchies.contains(c.BI_PL_Hierarchy__c)) {
                lstHierarchies.add(new SelectOption(c.BI_PL_Hierarchy__c, c.BI_PL_Hierarchy__c));
                hierarchies.add(c.BI_PL_Hierarchy__c);
            }
        }
    }

    /**
     * Retrieves the preparations to be shown according to the selected hierarchy and cycle.
     * @author OmegaCRM
     */
    public void loadPreparations() {
        lstToShowPreparations = [SELECT Id, Name, BI_PL_Country_code__c, BI_PL_Position_name__c, toLabel(BI_PL_Status__c), BI_PL_Start_date__c, BI_PL_End_date__c, OwnerId,
                                 BI_PL_position_cycle__r.BI_PL_hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name,
                                 BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                                 BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c, BI_PL_Position_cycle__r.BI_PL_Synchronized__c, Owner.Name
                                 FROM BI_PL_Preparation__c
                                 WHERE BI_PL_Country_code__c = : userCountryCode
                                         AND BI_PL_position_cycle__r.BI_PL_hierarchy__c = : selectedHierarchy
                                                 AND BI_PL_position_cycle__r.BI_PL_Cycle__c = : selectedCycle];
    }


    /***************************
     ** Getting Preparations to Sync **
     **************************/
    public List<BI_PL_Preparation__c> getPreparationsToSync() {
        return [SELECT Id, Name, BI_PL_Country_code__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Type__c, BI_PL_Position_name__c, toLabel(BI_PL_Status__c), BI_PL_Start_date__c, BI_PL_End_date__c, OwnerId,
                BI_PL_position_cycle__r.BI_PL_hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name,
                BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c, BI_PL_Position_cycle__r.BI_PL_Synchronized__c, BI_PL_position_cycle__r.BI_PL_Cycle__r.BI_PL_External_Id__c
                FROM BI_PL_Preparation__c
                WHERE BI_PL_Country_code__c = : userCountryCode
                                              AND BI_PL_Start_date__c != null
                                              AND BI_PL_End_date__c != null
                                              ORDER BY BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name];
    }

    public void setPreparationsToSync() {
        Date currentDate = Date.today();

        // We will filter all Preparations wich status is currently approved, from future and current cycles

        //lSyncPreparations = [SELECT Id, Name, BI_PL_Country_code__c, BI_PL_Position_name__c, toLabel(BI_PL_Status__c), BI_PL_Start_date__c, BI_PL_End_date__c, OwnerId,
        //                     BI_PL_position_cycle__r.BI_PL_hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name,
        //                     BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
        //                     BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c, BI_PL_Position_cycle__r.BI_PL_Synchronized__c, BI_PL_position_cycle__r.BI_PL_Cycle__r.BI_PL_External_Id__c
        //                     FROM BI_PL_Preparation__c
        //                     WHERE BI_PL_Country_code__c = : userCountryCode
        //                             AND BI_PL_Start_date__c != null
        //                             AND BI_PL_End_date__c != null];
        // AND BI_PL_End_date__c >= :currentDate];
        //

        lSyncPreparations = getPreparationsToSync();

        System.debug('***lSyncPreparations = ' + lSyncPreparations);

        preparationsToSync = new list<BI_PL_Preparation__c>();
        ownerNamesBySyncPreparationId = new map<Id, String>();
        set<Id> ownerIds = new set<Id>();
        map<Id, Id> OwnerIdByPreparationId = new map<Id, Id>();

        try {
            BI_PL_Preparation__c prepApproved = [SELECT Id, toLabel(BI_PL_Status__c) FROM BI_PL_Preparation__c WHERE
                                                 BI_PL_Status__c = 'Approved' LIMIT 1];
            approvedLabel = prepApproved.BI_PL_Status__c;
        } catch (Exception e) {
            approvedLabel = '';
        }

        fillSyncMaps();
        for (BI_PL_Preparation__c prep : lSyncPreparations) {
            if (prep.BI_PL_Status__c == approvedLabel) {
                preparationsToSync.add(prep);
            }
            System.debug('*** preparationsToSync = ' + preparationsToSync);
            ownerIds.add(prep.OwnerId);
            OwnerIdByPreparationId.put(prep.Id, prep.OwnerId);
        }

        list<User> lUsers = [SELECT Id, Name FROM User WHERE Id IN :ownerIds];
        map <Id, String> userNamesByUserId = new map <Id, String>();

        for (User u : lUsers) {
            userNamesByUserId.put(u.Id, u.Name);
        }

        for (BI_PL_Preparation__c prep : lSyncPreparations) {
            ownerNamesBySyncPreparationId.put(prep.Id, userNamesByUserId.get(OwnerIdByPreparationId.get(prep.Id)));
        }

        canSync = false;
    }

    /**
     * This method will fill the maps, sets and Lists for the Sync Tab
     *
     * @param lstPreparations list of PLANiT Preparations records shown
     * @author OmegaCRM
     */

    public void fillSyncMaps() {
        Set<BI_PL_Cycle__c> setCycles = new Set<BI_PL_Cycle__c>();
        Set<String> setHierarchies = new Set<String>();
        Set<String> setHierarchiesToShow = new Set<String>();
        mapCycles = new Map<BI_PL_Cycle__c, Set<String>>();
        Map<String, List<BI_PL_Preparation__c>> mapHierarchies = new Map<String, List<BI_PL_Preparation__c>>();
        mapFieldForce = new Map<Id, String>();
        mapStartDates = new Map<Id, Date>();
        mapEndDates = new Map<Id, Date>();

        //MLG : viestate
        lSyncPreparations = getPreparationsToSync();

        for (BI_PL_Preparation__c prep : lSyncPreparations) {
            if (!setCycles.contains(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__r)) {
                Set<String> setH = new Set<String>();
                setH.add(prep.BI_PL_Position_cycle__r.BI_PL_hierarchy__c);
                mapCycles.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__r, setH);
                setCycles.add(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__r);
                if (prep.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name != null)
                    mapFieldForce.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, prep.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name);
                else {
                    mapFieldForce.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, ' ');
                }
                mapStartDates.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, prep.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c);
                mapEndDates.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, prep.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c);
            } else {
                mapCycles.get(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__r).add(prep.BI_PL_position_cycle__r.BI_PL_hierarchy__c);
            }

            if (!setHierarchies.contains(prep.BI_PL_position_cycle__r.BI_PL_hierarchy__c + prep.BI_PL_position_cycle__r.BI_PL_Cycle__r.BI_PL_External_Id__c)) {
                List<BI_PL_Preparation__c> lstPreparations = new List<BI_PL_Preparation__c>();
                lstPreparations.add(prep);
                mapHierarchies.put(prep.BI_PL_position_cycle__r.BI_PL_hierarchy__c + prep.BI_PL_position_cycle__r.BI_PL_Cycle__r.BI_PL_External_Id__c, lstPreparations);
                setHierarchies.add(prep.BI_PL_position_cycle__r.BI_PL_hierarchy__c + prep.BI_PL_position_cycle__r.BI_PL_Cycle__r.BI_PL_External_Id__c);
                setHierarchiesToShow.add(prep.BI_PL_position_cycle__r.BI_PL_hierarchy__c);
            } else {
                mapHierarchies.get(prep.BI_PL_position_cycle__r.BI_PL_hierarchy__c + prep.BI_PL_position_cycle__r.BI_PL_Cycle__r.BI_PL_External_Id__c).add(prep);
            }
        }

        lstCyclesSync = new List<SelectOption>();
        lstCyclesSync.add(new SelectOption('', Label.Common_None_vod));
        for (BI_PL_Cycle__c c : setCycles) {
            if (c.BI_PL_Type__c == BI_PL_VisibilityTree.CYCLE_STATUS_ACTIVE)
                lstCyclesSync.add(new SelectOption(c.Id, c.Name));
        }
        mapCycleHierarchy = new Map<String, List<SelectOption>>();


        lstHierarchies = new List<SelectOption>();
        lstHierarchies.add(new SelectOption('', Label.Common_None_vod));
        for (String h : setHierarchiesToShow) {
            lstHierarchies.add(new SelectOption(h, h));
        }

        canSyncError = !canSyncHierarchies(mapHierarchies);
        System.debug('***lSyncPreparations = ' + lSyncPreparations);
        canSynchronizeCycle(lSyncPreparations);

    }

    /**
     * This method check if there is any hierarchy with all preparations approved
     *
     * @param  mapHierarchies
     * @return true if all are approved
     * @author OmegaCRM
     */

    public boolean canSyncHierarchies(Map<String, List<BI_PL_Preparation__c>> mapHierarchies) {

        List<String> approvedHierarchies = new List<String>();
        for (String hierarchy : mapHierarchies.keySet()) {
            Boolean allApproved = true;
            for (BI_PL_Preparation__c prep : mapHierarchies.get(hierarchy)) {
                if (prep.BI_PL_Status__c != approvedLabel)
                    allApproved = false;
            }
            if (allApproved)
                approvedHierarchies.add(hierarchy);
        }
        system.debug('***approvedHierarchies = ' + approvedHierarchies);

        return (approvedHierarchies.size() > 0);

    }

    /**
     * This method checks if there is any synchronized hierarchy in the cycle, if there is, it cannot be synchronized
     * @param lstPreparations
     * @author OmegaCRM
     */

    public void canSynchronizeCycle(List<BI_PL_Preparation__c> lstPreparations) {
        Map<Id, Boolean> canSyncCycle = new Map<Id, Boolean>();
        Map<Id, String> canSyncCycleHierarchy = new Map<Id, String>();
        Set<Id> addedCycles = new Set<Id>();
        syncedHierarchy = new Map<Id, String>();

        for (BI_PL_Preparation__c prep : lstPreparations) {
            if (addedCycles.contains(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c)) {
                if (prep.BI_PL_Position_cycle__r.BI_PL_Synchronized__c) {
                    canSyncCycleHierarchy.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, prep.BI_PL_Position_cycle__r.BI_PL_hierarchy__c);
                    canSyncCycle.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, false);
                    syncedHierarchy.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, prep.BI_PL_Position_cycle__r.BI_PL_hierarchy__c);
                }
            } else {
                syncedHierarchy.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, ' ');
                if (prep.BI_PL_Position_cycle__r.BI_PL_Synchronized__c) {
                    canSyncCycle.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, false);
                    canSyncCycleHierarchy.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, prep.BI_PL_Position_cycle__r.BI_PL_hierarchy__c);
                    syncedHierarchy.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, prep.BI_PL_Position_cycle__r.BI_PL_hierarchy__c);
                } else {
                    canSyncCycle.put(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c, true);
                }
                addedCycles.add(prep.BI_PL_Position_cycle__r.BI_PL_Cycle__c);
            }
        }
        System.debug('***syncedHierarchy = ' + syncedHierarchy);
        canSyncCycleJSON = JSON.serialize(canSyncCycleHierarchy);
        System.debug('***CansyccycleJSON = ' + canSyncCycleJSON);
    }

    /**
     * Retrieves the preparations to be shown according to the selected hierarchy and cycle.
     * @author OmegaCRM
     */

    public void showPreparations() {
        System.debug('***showPreparations');
        Date currentDate = Date.today();
        lstToShowPreparations = [SELECT Id, Name, BI_PL_Country_code__c, BI_PL_Position_name__c, toLabel(BI_PL_Status__c), BI_PL_Start_date__c, BI_PL_End_date__c, OwnerId,
                                 BI_PL_position_cycle__r.BI_PL_hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name,
                                 BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                                 BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c, BI_PL_Position_cycle__r.BI_PL_Synchronized__c, Owner.Name
                                 FROM BI_PL_Preparation__c
                                 WHERE BI_PL_Country_code__c = : userCountryCode
                                         AND BI_PL_position_cycle__r.BI_PL_hierarchy__c = : selectedHierarchy
                                                 AND BI_PL_position_cycle__r.BI_PL_Cycle__c = : selectedCycle];
        System.debug(logginglevel.DEBUG, '***lstToShowPreparations = ' + lstToShowPreparations);

    }

    /**
     * This method clears the list of preparations shown and the selected hierarchy when the user changes the cycle selected
     * @author OmegaCRM
     */

    public void cycleChanged() {
        if(lstToShowPreparations!=null && !lstToShowPreparations.isEmpty()) lstToShowPreparations.clear();
        selectedHierarchy = '';
        loadHierarchyOptions();
    }

    public void hierarchyChanged() {
        loadPreparations();
    }


    /****************************************
     ** Preparationo Synchronization Code **
     ***************************************/


    @RemoteAction
    public static Boolean synchronizePreparations(String preparationName, List<String> preparationsToSyncronize) {
        Boolean canSynchronize = false;
        System.debug('***preparationsToSyncronize = ' + preparationsToSyncronize);

        Id currentUserId = UserInfo.getUserId();
        try {
            String userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = : currentUserId LIMIT 1].Country_Code_BI__c;

            BI_PL_Country_settings__c countrySettings = [SELECT BI_PL_Channel__c, BI_PL_Multichannel__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = : userCountryCode LIMIT 1];

            //If there are preparations to syncronize and all are approved.
            if (preparationsToSyncronize.size() > 0 && countrySettings != null) {
                system.debug('## synchronize process started!!');
                canSynchronize = true;
                if (!countrySettings.BI_PL_Multichannel__c) {
                    BI_PL_SynchronizeSAPDataBatch syncDataBatch = new BI_PL_SynchronizeSAPDataBatch(preparationsToSyncronize, preparationName, countrySettings.BI_PL_Channel__c);
                    Id batchJobId = Database.executeBatch(syncDataBatch, 2);
                } else {
                    BI_PL_SynchronizeMCCPDataBatch syncDataBatch = new BI_PL_SynchronizeMCCPDataBatch(preparationsToSyncronize);
                    Id batchJobId = Database.executeBatch(syncDataBatch, 2);
                }
            }
        } catch (Exception e) {
            System.debug('***Exception = ' + e.getMessage());
        }

        return canSynchronize;
    }

    /*****************
     ** Common Code **
     *****************/

    private void setUserCountryCode() {
        Id currentUserId = UserInfo.getUserId();

        userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = : currentUserId LIMIT 1].Country_Code_BI__c;

        System.debug('%%% userCountryCode: ' + userCountryCode);
    }

    @RemoteAction
    @Readonly
    public static String getActionTransferShareReport(String selectedCycle, String selectedHierarchy, String selectedChannel) {
        return BI_PL_TransferAndShareUtility.generateActionTextReportBody(selectedCycle, selectedHierarchy);
    }

    @RemoteAction
    @Readonly
    public static String getActionAddReport(String selectedCycle, String selectedHierarchy, String selectedChannel) {
        return BI_PL_TransferAndShareUtility.generateAddsTextReportBody(selectedCycle, selectedHierarchy, selectedChannel);
    }

    @RemoteAction
    @Readonly
    public static String getActionDropReport(String selectedCycle, String selectedHierarchy, String selectedChannel) {
        return BI_PL_TransferAndShareUtility.generateDropsTextReportBody(selectedCycle, selectedHierarchy, selectedChannel);
    }

    @RemoteAction
    @Readonly
    public static Map<String, String> getDetailsReport(String selectedCycle, String selectedHierarchy, String selectedChannel, String  poa, String  off) {
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedCycle: ' + selectedCycle);
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedHierarchy: ' + selectedHierarchy);
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedChannel: ' + selectedChannel);
        return BI_PL_TransferAndShareUtility.generateDetailsTextReportBody(selectedCycle, selectedHierarchy, selectedChannel, poa, off);
    }

    public void getActionTransferShareReport() {
        this.actionReport = BI_PL_TransferAndShareUtility.generateActionTextReportBody(selectedCycle, selectedHierarchy);
    }

    public void getActionAddReport() {
        this.actionReport = BI_PL_TransferAndShareUtility.generateAddsTextReportBody(selectedCycle, selectedHierarchy, selectedChannel);
    }
    public void getActionDropReport() {
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedCycle: ' + selectedCycle);
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedHierarchy: ' + selectedHierarchy);
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedChannel: ' + selectedChannel);
        this.actionReport = '';
    }

    public void getPlannedDetailsReport() {
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedCycle: ' + selectedCycle);
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedHierarchy: ' + selectedHierarchy);
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedChannel: ' + selectedChannel);
        this.actionReport = '';
    }

    public void getAdjustedDetailsReport() {
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedCycle: ' + selectedCycle);
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedHierarchy: ' + selectedHierarchy);
        System.debug(loggingLevel.Error, '*** getActionDropReport selectedChannel: ' + selectedChannel);
        this.actionReport = '';
    }



    /**************************
     ** Common Inner Classes **
     **************************/

    global class PreparationCycleWrapper {
        public Date startDate;
        public Date endDate;

        public PreparationCycleWrapper(Date startDate, Date endDate) {
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }

    /*************************
    ** Product Metrics *******
    *************************/

    /**
     *  Loads the cycle options for the picklist.
     *  @author OMEGA CRM
     */
    @RemoteAction
    public static List<BasicWrapper> getCycleOptions(String userCountryCode) {
        

        List<BasicWrapper> cyclesList = new List<BasicWrapper>();
        cyclesList.add(new BasicWrapper('', Label.Common_None_vod));
        
        List<BI_PL_Cycle__c> cycles = new List<BI_PL_Cycle__c>([SELECT Id, Name FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :userCountryCode ORDER BY Name]);

        for (BI_PL_Cycle__c c : cycles) {
            cyclesList.add(new BasicWrapper(c.Id, c.Name));
        }

        return cyclesList;
    }

    /**
     * Loads previous cycle options.
     */
    @RemoteAction
    public static List<BasicWrapper> getPreviousCycleOptions(String currentCycle, String userCountryCode) {
        
        List<BasicWrapper> cyclesList = new List<BasicWrapper>();

        if(currentCycle!=''){

            Date currCycleStartDate = [SELECT Id, BI_PL_Start_date__c FROM BI_PL_Cycle__c WHERE Id = :currentCycle].BI_PL_Start_date__c;

            Map<Id, BI_PL_Cycle__c> prevCycles = new Map<Id, BI_PL_Cycle__c>([SELECT Id, Name, BI_PL_Field_force__r.Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :userCountryCode 
                AND BI_PL_Start_date__c <= :currCycleStartDate 
                AND Id != :currentCycle ORDER BY Name]);

            if(!prevCycles.isEmpty()){

                cyclesList.add(new BasicWrapper('', Label.Common_None_vod));
                for (BI_PL_Cycle__c c : prevCycles.values()) {
                    cyclesList.add(new BasicWrapper(c.Id, c.Name));
                }
            }
            else{
                cyclesList.add(new BasicWrapper('', Label.BI_PL_No_Prev_Cycles));
            }

        }

        return cyclesList;
    }

    @RemoteAction
    public static String getReportFolderURL(){
        List<Folder> lFolder = [Select Id From Folder Where DeveloperName = 'BI_PLANiT_Reports'];
        if(!lFolder.isEmpty()){
            return System.URL.getSalesforceBaseURL().toExternalForm() + '/' + lFolder[0].Id;
        }return '';
    }

    global class BasicWrapper {
        public String label;
        public String value;

        public BasicWrapper(String value, String label){
            this.label = label;
            this.value = value;
        }
    }


}