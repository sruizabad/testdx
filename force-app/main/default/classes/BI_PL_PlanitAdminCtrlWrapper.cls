public with sharing class BI_PL_PlanitAdminCtrlWrapper {
	public BI_PL_PlanitAdminCtrl controller {get; set;}

	public BI_PL_PlanitAdminCtrlWrapper(BI_PL_PlanitAdminCtrl controller) {
		this.controller = controller;
	}

}