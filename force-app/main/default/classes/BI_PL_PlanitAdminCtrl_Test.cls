@isTest
private class BI_PL_PlanitAdminCtrl_Test {
    @testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(3, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(3, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        //BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);
    }

    Static Date currentDate;
    Static String countryCode;
    
    @isTest static void test_method_one() {

        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_PlanitAdminCtrl control = new BI_PL_PlanitAdminCtrl();
        list<BI_PL_Preparation__c> lisPrepTest = control.preparationsToClone;
        String jsonPrep = control.preparationsToCloneJSON;
        String jsonSinc = control.preparationsToSyncJSON;
        String selectedFF = control.selectedFieldForce;
        Date selectedSD = control.selectedStartDate;
        Date selectedED = control.selectedEndDate;
        Map<Id, Boolean> canSyncCycTest = control.canSyncCycle;
        Boolean canClone = control.canClone;
        Boolean countrySet = control.isPlanitCountrySettingSet;
        BI_PL_PlanitAdminCtrlWrapper con = control.controller;
        //PermissionSetAssignment permSetAssign = [SELECT AssigneeId from PermissionSetAssignment Where PermissionSet.Name = 'BI_PL_ADMIN' LIMIT 1];

        /*
        String l = 'BR' + '_%';
        Profile p = [SELECT Id FROM Profile WHERE Name LIKE :l LIMIT 1];

        User testUser = new User(Alias = 'user546', Email = 'testuser54' + '@testorg.com', Country_Code_BI__c = 'BR',
                        EmailEncodingKey = 'UTF-8', LastName = 'test', LanguageLocaleKey = 'en_US', IsActive = true,
                        LocaleSidKey = 'en_US', ProfileId = p.id, UserName = 'test234' + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = 'testest23_Test_External_ID');

        insert testUser;
         
        PermissionSet permSet = [SELECT Id FROM PermissionSet WHERE Name = 'BI_PL_ADMIN' LIMIT 1];
        PermissionSetAssignment perSetAss = new PermissionSetAssignment(AssigneeId = testUser.Id, PermissionSetId = permSet.Id);
        insert perSetAss; 
        */

        BI_PL_TestDataFactory.createTestUsers(1,'BR');
        User testUser = [SELECT id, LastName, Country_Code_BI__c FROM User WHERE Country_Code_BI__c = 'BR' AND LastName = 'TestUserDatafactory0'];

        countryCode = testUser.Country_Code_BI__c;

        //BI_PL_TestDataFactory.createCycleStructure('BR');
        
        //BI_PL_Preparation__c preparation = BI_PL_TestDataFactory.preparation5;


        //BI_PL_Position_Cycle__c posCycId = BI_PL_TestDataFactory.ro


        BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'Cardiology',BI_TM_Country_code__c = countryCode);
        insert fieldForce;

        //Insert Cycle
        BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = countryCode, BI_PL_Start_date__c = Date.today(), BI_PL_End_date__c = Date.today() + 1, BI_PL_Field_force__c = fieldForce.Id);
        insert cycle;

        cycle = [SELECT Id, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c, Name, BI_PL_Field_force__r.Name FROM BI_PL_Cycle__c WHERE Id = : cycle.Id];

        List<BI_PL_Position__c> positions = new List<BI_PL_Position__c>();
        List<BI_PL_Position_cycle_user__c> positionCycleUsers = new List<BI_PL_Position_cycle_user__c>();
        List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();

        //Insert Positions

        BI_PL_Position__c positionRoot = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = countryCode);
        
        positions.add(positionRoot);
        
        insert positions;

        //Insert Positions Cycle

        BI_PL_Position_cycle__c posCycId = new BI_PL_Position_cycle__c(/*Name = 'root', */BI_PL_Position__c = positionRoot.Id, BI_PL_Parent_position__c = null, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = 'testHierarchy');

        posCycId.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, posCycId.BI_PL_Hierarchy__c, positionRoot.Name);

        insert posCycId;

        BI_PL_Preparation__c prep = new BI_PL_Preparation__c(
                BI_PL_Country_code__c = countryCode,
                BI_PL_Position_cycle__c = posCycId.Id,
                BI_PL_Status__c = 'Approved',
                BI_PL_External_Id__c = posCycId.BI_PL_External_ID__c + Math.random().intValue()
            );
        insert prep;


        //Id posId = BI_PL_TestDataFactory.preparation5.BI_PL_Position_cycle__c;
        Id posId = prep.BI_PL_Position_cycle__c;
        BI_PL_PlanitAdminCtrl controller = new BI_PL_PlanitAdminCtrl();
        controller.selectedCycle = [SELECT BI_PL_Cycle__r.Name FROM BI_PL_Position_cycle__c WHERE Id =: posId LIMIT 1].BI_PL_Cycle__r.Name;
        controller.cycleChanged();
        controller.selectedHierarchy = prep.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
        controller.hierarchyChanged();
        controller.showPreparations();
        
        
        BI_PL_Country_settings__c countrySettings = new BI_PL_Country_settings__c();

        System.runAs(testUser){
            EmailTemplate e = new EmailTemplate(developerName = 'test', TemplateType = 'Text', Name  = 'SAP Synchronize Notification', FolderId = UserInfo.getUserId(), isActive = true);
            insert e;
            Contact c = new Contact(LastName = 'testContact', Email = 'testContact@test.test');
            insert c;

            countrySettings = new BI_PL_Country_settings__c(
                    Name = 'TestCountrySettingsBR',
                    BI_PL_Channel__c = 'rep_detail_only',
                    BI_PL_Specialty_field__c = 'Specialty_1_vod__c',
                    BI_PL_Multichannel__c = true,
                    BI_PL_Country_code__c = countryCode
                );
            insert countrySettings;

            BI_PL_Mccp__c settingMCCP = new BI_PL_Mccp__c(Name = 'rep_detail_only', BI_PL_Mccp_channel__c='Call2_vod');
            insert settingMCCP;

        }
        List<String> prepsToSync = new List<String>();
        prepsToSync.add(prep.Id);

        controller.showModal();
       
        BI_PL_PlanitAdminCtrl.synchronizePreparations('TestSync',prepsToSync);
        controller.closeModel();

        System.runAs(testUser){
            countrySettings.BI_PL_Multichannel__c = false;
            update countrySettings;
        }
        BI_PL_PlanitAdminCtrl.synchronizePreparations('TestSync',prepsToSync);

        //BI_PL_Country_settings__c countryList = new BI_PL_Country_settings__c(Name = 'BRA', BI_PL_Country_code__c = 'BR');

        //insert countryList;

        BI_PL_Country_settings__c countrySettings2 = new BI_PL_Country_settings__c(
                    Name = 'TestCountrySettingsBR2',
                    BI_PL_Channel__c = 'rep_detail_only',
                    BI_PL_Specialty_field__c = 'Specialty_1_vod__c',
                    BI_PL_Multichannel__c = true,
                    BI_PL_Country_code__c = countryCode
                );
            insert countrySettings2;

        Boolean cloneAllowed = controller.isCloneAllowed;
        Boolean importImpact = controller.isImportFromIMMPACTAllowed;
        Boolean importFromBitman = controller.isImportFromBITMANAllowed;

        BI_PL_PlanitAdminCtrl.getActionTransferShareReport(controller.selectedCycle, controller.selectedHierarchy, controller.selectedChannel);
        BI_PL_PlanitAdminCtrl.getActionAddReport(controller.selectedCycle, controller.selectedHierarchy, controller.selectedChannel);
        BI_PL_PlanitAdminCtrl.getActionDropReport(controller.selectedCycle, controller.selectedHierarchy, controller.selectedChannel);
        BI_PL_PlanitAdminCtrl.getDetailsReport(controller.selectedCycle, controller.selectedHierarchy, controller.selectedChannel,'planned','100');

    }


    @isTest static void test_cycle_hierachy_selection() {
        //We get the cycles
        List<sObject> affTypes = BI_PL_PlanitAdminCtrl.getAffiliationRecordTypes();

        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

        List<sObject> cycles = BI_PL_PlanitAdminCtrl.getCycles(userCountryCode, true, null);

        //Select one
        String selectedCycleId = (String) cycles.get(0).get('Id');
        System.debug(loggingLevel.Error, '*** cycles: ' + cycles);

        List<String> hs = new List<String> (BI_PL_PlanitAdminCtrl.getHierarchies(selectedCycleId, null));
        System.debug(loggingLevel.Error, '*** hs: ' + hs);

        Map<String, Object> chs =  BI_PL_PlanitAdminCtrl.getChannels(selectedCycleId, hs, true);
        System.debug(loggingLevel.Error, '*** chs: ' + chs);

        Test.startTest();

        List<String> selectedChannels = new List<String>(chs.keySet());
        List<String> cys = new List<String>{selectedCycleid};
        Map<String, Object> params = new Map<String, Object> { 'type' => 'Sales'};

        String batchId = BI_PL_PlanitAdminCtrl.runProcess(userCountryCode, cys, hs, selectedChannels, 'BI_PL_DeleteAffiliationBatch', params, 200);

        Test.stopTest();
    }
}