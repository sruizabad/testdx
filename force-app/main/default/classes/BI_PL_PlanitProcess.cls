public virtual class BI_PL_PlanitProcess {
	protected String countryCode = null;
	protected List<String> cycleIds = new List<String>();
	protected List<String> hierarchyNames = new List<String>();
	protected List<String> channels = new List<String>();
	protected  Map<String, Object> params =  new Map<String, Object>();
	//Define the method signature to be implemented in classes that implements the interface
	//Example method
	public virtual void init(String cc, List<String> cycleIds, List<String> hierarchyNames, List<String> channels, Map<String, Object> params) {
		this.countryCode = cc;
		this.cycleIds = cycleIds;
		this.hierarchyNames = hierarchyNames;
		this.channels = channels;
		if(params!= null){
			this.params = params;
		}
	}


	public virtual void init( List<String> cycleIds, List<String> hierarchyNames, List<String> channels, Map<String, Object> params) {
		this.countryCode = [SELECT ID, Country_Code_Bi__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_Bi__c;
		this.cycleIds = cycleIds;
		this.hierarchyNames = hierarchyNames;
		this.channels = channels;
		if(params!= null){
			this.params = params;
		}
	}
}