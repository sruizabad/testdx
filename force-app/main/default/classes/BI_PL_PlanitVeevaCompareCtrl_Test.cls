@isTest
public class BI_PL_PlanitVeevaCompareCtrl_Test {
	
 	private static String userCountryCode;
    private static List<String> hierarchy = new List<String>{BI_PL_TestDataFactory.hierarchy};

    @testSetup  static void setup() {
        BI_PL_SummaryReviewCtrl countroller = new BI_PL_SummaryReviewCtrl();
        User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        userCountryCode = testUser.Country_Code_BI__c;

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        List<BI_PL_Position__c> positions = [SELECT Id FROM BI_PL_Position__c];
        BI_PL_TestDataFactory.createTestTerritoryField(userCountryCode, listAcc[0], positions[0]);
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
      

    }

    @isTest
    public static void test_method() {
        
        Test.startTest();

        Test.stopTest();
    }



}