/**
 * Batch that clones Position Cycles and Position Cycle User records for a given Hierarchy and Cycle
 */
global class BI_PL_PositionCycleCloneBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    private String cycle;
    private String hierarchy;
    private Id targetCycleId;
    private BI_PL_Cycle__c targetCycleRecord;
    private String targetHierarchy;
        
    global BI_PL_PositionCycleCloneBatch(String cycle, String hierarchy, Id targetCycleId, String targetHierarchy) {
        System.debug('### - BI_PL_PositionCycleCloneBatch ' + cycle + ' - ' + hierarchy);
        this.cycle = cycle;
        this.hierarchy = hierarchy;
        this.targetCycleId = targetCycleId;
        this.targetHierarchy = targetHierarchy;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        System.debug('### - BI_PL_PositionCycleCloneBatch - start');

        this.targetCycleRecord = [SELECT Id, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Field_force__r.Name 
                                    FROM BI_PL_Cycle__c 
                                      WHERE Id = : targetCycleId];

        return Database.getQueryLocator([SELECT Id,
                                            BI_PL_Cycle__c,
                                            BI_PL_Position__c,
                                            BI_PL_Position__r.Name,
                                            BI_PL_Parent_position__c,
                                            BI_PL_Hierarchy__c,
                                            BI_PL_External_id__c,
                                            BI_PL_Workload_percent__c
    
                                            FROM BI_PL_Position_cycle__c
                                            
                                            WHERE BI_PL_Cycle__c = :cycle
                                                AND BI_PL_Hierarchy__c = :hierarchy]);
    }

    global void execute(Database.BatchableContext BC, List<BI_PL_Position_cycle__c> originalPosCycles) {
        try{
            System.debug('### - BI_PL_PositionCycleCloneBatch - execute');

            map<Id, String> matchPositionCycleCloned = new map<Id, String>();

            list<BI_PL_Position_cycle__c> posCycleToCreate = new list<BI_PL_Position_cycle__c>();
            list<BI_PL_Position_cycle_user__c> posCycleUserToCreate = new list<BI_PL_Position_cycle_user__c>();

            // 1.- Create BI_PL_Position_cycle__c records:
            for (BI_PL_Position_cycle__c originalPosCycle : originalPosCycles) {
                BI_PL_Position_cycle__c pc = createPositionCycle(originalPosCycle);
                System.debug('### - BI_PL_PositionCycleCloneBatch - execute - pc = ' + pc);
                posCycleToCreate.add(pc);

                matchPositionCycleCloned.put(originalPosCycle.Id, pc.BI_PL_External_id__c);
            }

            upsert posCycleToCreate BI_PL_External_id__c;

            // 2.- Create BI_PL_Position_cycle_user__c records:
            list<BI_PL_Position_cycle_user__c> originalPosCycleUsers = new list<BI_PL_Position_cycle_user__c>([ SELECT Id, BI_PL_User__c, BI_PL_Position_cycle__c,BI_PL_Position_cycle__r.BI_PL_Position__r.Name, 
                                                                                                                    BI_PL_User__r.External_ID__c, BI_PL_External_id__c
                                                                                                                FROM BI_PL_Position_cycle_user__c
                                                                                                                WHERE BI_PL_Position_cycle__c IN :matchPositionCycleCloned.keySet()]);

            for(BI_PL_Position_cycle_user__c originalPosCycleUser: originalPosCycleUsers){
                BI_PL_Position_cycle_user__c pcu = createPositionCycleUser(matchPositionCycleCloned.get(originalPosCycleUser.BI_PL_Position_cycle__c), originalPosCycleUser);
                posCycleUserToCreate.add(pcu);
            }

            System.debug('### - BI_PL_PositionCycleCloneBatch - execute - posCycleUserToCreate = ' + posCycleUserToCreate);

            upsert posCycleUserToCreate BI_PL_External_id__c;

        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, '### e.getMessage() = ' + e.getMessage());
            System.debug(LoggingLevel.ERROR, '### e.getMessage() = ' + e.getStackTraceString());
        }
        
    }

    global void finish(Database.BatchableContext BC) {
        System.debug('### - BI_PL_PositionCycleCloneBatch - finish');
        //MLG - If not target hierachy selected, for example when cloning a cycle vs a cycle without any hierarchy nodes
        if(targetHierarchy == null || targetHierarchy == '') {
          targetHierarchy = hierarchy;
        }

        BI_PL_PreparationCloneBatch cloneDataBatch = new BI_PL_PreparationCloneBatch(cycle, hierarchy, targetCycleId, targetHierarchy);
        Database.executeBatch(cloneDataBatch);
    }

    /**
     * Creates a position cycle.
     *
     * @param      positionCycle  The position cycle
     *
     * @return     Cloned PositionCycleRecord
     */
    private BI_PL_Position_cycle__c createPositionCycle(BI_PL_Position_cycle__c positionCycle) {

        System.debug('### - createPositionCycle - targetCycleRecord = ' + targetCycleRecord);

        return new BI_PL_Position_cycle__c(
                   BI_PL_Cycle__c = targetCycleId,
                   BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(targetCycleRecord, positionCycle.BI_PL_Hierarchy__c, positionCycle.BI_PL_Position__r.Name),
                   BI_PL_Position__c = positionCycle.BI_PL_Position__c,
                   BI_PL_Workload_percent__c = positionCycle.BI_PL_Workload_percent__c,
                   BI_PL_Hierarchy__c = hierarchy,
                   BI_PL_Parent_position__c = positionCycle.BI_PL_Parent_position__c);
    }

    /**
     * Creates a position cycle user.
     *
     * @param      newPositionCycleExternalId  The new position cycle external identifier
     * @param      original                    The original
     *
     * @return     Cloned PositionCycleUser record
     */
    private BI_PL_Position_cycle_user__c createPositionCycleUser(String newPositionCycleExternalId, BI_PL_Position_cycle_user__c original) {
      System.debug(LoggingLevel.ERROR, '### - 2.1');
      System.debug(LoggingLevel.ERROR, '### - newPositionCycleExternalId = ' + newPositionCycleExternalId);

      BI_PL_Position_cycle__c pos = new BI_PL_Position_cycle__c(BI_PL_External_id__c = newPositionCycleExternalId);
      
      System.debug(LoggingLevel.ERROR, '### - 2.2');

      return new BI_PL_Position_cycle_user__c(
                    BI_PL_Position_cycle__r = pos,
                    BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(targetCycleRecord, hierarchy, original.BI_PL_Position_cycle__r.BI_PL_Position__r.Name, original.BI_PL_User__r.External_ID__c),
                    BI_PL_User__c = original.BI_PL_User__c);
    }
}