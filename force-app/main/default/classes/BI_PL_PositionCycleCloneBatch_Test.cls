@isTest
private class BI_PL_PositionCycleCloneBatch_Test {
	
	@isTest static void testBatch() {

		BI_PL_TestDataFactory.createCountrySetting();
		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		String userCountryCode = currentUser.Country_Code_BI__c;

		
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);
		BI_PL_Cycle__c cycle = [SELECT id, Name, BI_PL_Start_date__c, BI_PL_End_date__c from BI_PL_Cycle__c limit 1];

	
		Test.startTest();

		BI_PL_PositionCycleCloneBatch positionCycleCloneBatch = new BI_PL_PositionCycleCloneBatch(cycle.id,'hierarchyTest',cycle.id,'hierarchyTest');
		Database.executeBatch(positionCycleCloneBatch);

		Test.stopTest();



		
	}
	
	
	
}