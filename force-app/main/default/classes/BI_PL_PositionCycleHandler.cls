public without sharing class BI_PL_PositionCycleHandler
    implements BI_PL_TriggerInterface {


    // Constructor
    Map<String, String> pcIdsByPosition;
    
    List<BI_PL_Position_cycle__c> toUpdate = new List<BI_PL_Position_cycle__c>();

    // Constructor
    public BI_PL_PositionCycleHandler() {}

    public void bulkAfter() {

        if(trigger.isInsert || trigger.isUpdate){
            pcIdsByPosition = new Map<String, String>();

            Set<String> cs = new Set<String>();
            Set<String> hs = new Set<String>();

            for(BI_PL_Position_cycle__c pc : (List<BI_PL_Position_cycle__c>) trigger.new) {
                cs.add(pc.BI_PL_Cycle__c);
                hs.add(pc.BI_PL_Hierarchy__c);  
                
                             
            }



            for(BI_PL_Position_cycle__c pc : [SELECT Id, BI_PL_Position__c, BI_PL_Hierarchy__c, BI_PL_Cycle__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c IN : cs AND BI_PL_Hierarchy__c IN  :hs ]) {
                pcIdsByPosition.put(pc.BI_PL_Position__c+pc.BI_PL_Hierarchy__c+pc.BI_PL_Cycle__c, pc.Id);
            }

            System.debug(loggingLevel.Error, '*** pcIdsByPosition: ' + pcIdsByPosition);
        }



        if(trigger.isUpdate){

            //GLOS-966 - Check PC has changed Confirmed field
            List<BI_PL_Position_Cycle__c> lstPositionCycle = new List<BI_PL_Position_Cycle__c>();
            //Check for every Position Cycle has changed confirmed field
            for(Integer i=0;i<trigger.new.size();i++){
                if(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Confirmed__c != ((BI_PL_Position_Cycle__c)trigger.old.get(i)).BI_PL_Confirmed__c)
                    lstPositionCycle.add((BI_PL_Position_Cycle__c)trigger.new.get(i));
            }

            system.debug('## lstPositionCycle: ' + lstPositionCycle.size());
            if (!lstPositionCycle.isEmpty()) getLastActionforPreparation(lstPositionCycle);

            /*
            *Commented till functionality be tested in full //
            */
            /*
            Set<Id> listPar = new Set<Id>();
            List<BI_PL_Position_Cycle__c> listChild = new List<BI_PL_Position_Cycle__c>();
            map<String, List<BI_PL_Position_Cycle__c>> parentPair = new map<String, List<BI_PL_Position_Cycle__c>>();
            map<Id, Id> sendList = new map<Id, Id>();
            List<BI_PL_Preparation__c> prepOwners = new List<BI_PL_Preparation__c>();

            List<BI_PL_Position_Cycle_User__c> prepOwnersWithNoPrep = new List<BI_PL_Position_Cycle_User__c>();
            Map<String, Id> parentPositionCycleIdMap = new Map<String, Id>();
            List<Id> noPrep = new List<Id>();

            Map<id, List<String>> mapHierarchy = new Map<id, List<String>>();

            map<Id, Id> pairToSend = new map<Id, Id>();
            list<Id> listUnconfirmedNodes = new List<Id>();
            List<BI_PL_Position_Cycle_User__c> listUnconfirmedNodesUsers = new List<BI_PL_Position_Cycle_User__c>();

            for(Integer i=0;i<trigger.new.size();i++){
                if(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Confirmed__c == true && ((BI_PL_Position_Cycle__c)trigger.old.get(i)).BI_PL_Confirmed__c == false){
                    listPar.add(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Parent_Position__c);
                    if(mapHierarchy.containsKey(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Parent_Position__c)){
                        mapHierarchy.get(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Parent_Position__c).add(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Hierarchy__c);
                    }else{
                        mapHierarchy.put(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Parent_Position__c,new List<String>());
                        mapHierarchy.get(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Parent_Position__c).add(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Hierarchy__c);
                    }

                }
            }

            if(listPar.size() > 0){

                listChild = [SELECT Id, BI_PL_Parent_Position__c,BI_PL_Confirmed__c, BI_PL_Cycle__c, BI_PL_Hierarchy__c, BI_PL_Position__c FROM BI_PL_Position_Cycle__c WHERE BI_PL_Parent_Position__c IN: listPar OR BI_PL_Position__c IN :listPar];

                for(BI_PL_Position_Cycle__c chi : listChild){

                    if(mapHierarchy.containsKey(chi.BI_PL_Position__c)){
                        for(String hierarchy : mapHierarchy.get(chi.BI_PL_Position__c)){
                            if(hierarchy == chi.BI_PL_Hierarchy__c){

                                    String key = '' + chi.BI_PL_Position__c + chi.BI_PL_Cycle__c + chi.BI_PL_Hierarchy__c;

                                    if(!parentPositionCycleIdMap.containsKey(key))
                                        parentPositionCycleIdMap.put(key, chi.Id);

                                    if(!parentPair.containsKey(key)){
                                        parentPair.put(key, new List<BI_PL_Position_Cycle__c>());
                                    }
                            }
                        }
                    }else{
                        for(String hierarchy : mapHierarchy.get(chi.BI_PL_Parent_Position__c)){
                            if(hierarchy == chi.BI_PL_Hierarchy__c){

                                    String key = '' + chi.BI_PL_Parent_Position__c + chi.BI_PL_Cycle__c + chi.BI_PL_Hierarchy__c;

                                    if(parentPair.containsKey(key)){
                                        parentPair.get(key).add(chi);
                                    }
                                    else{
                                        parentPair.put(key, new List<BI_PL_Position_Cycle__c>());
                                        parentPair.get(key).add(chi);
                                    }
                            }
                        }
                    }
                }

                Boolean allTrue = true;

                Set<Id> parentPositionCycleIds = new Set<Id>();

                for(String key : parentPair.keySet()){
                    for(BI_PL_Position_Cycle__c chil : parentPair.get(key)){
                        if(!chil.BI_PL_Confirmed__c) {
                            allTrue = false;
                            break;
                        }
                        
                    }
                    if(allTrue)
                        parentPositionCycleIds.add(parentPositionCycleIdMap.get(key));
                    allTrue = true;
                }

                Boolean check = false;

                prepOwners = [SELECT Owner.Id, BI_PL_Position_Cycle__c FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__c IN :parentPositionCycleIds];
                for(Id parPos : parentPositionCycleIds){
                    for(BI_PL_Preparation__c pre : prepOwners){
                        if(pre.BI_PL_Position_Cycle__c == parPos) check = true;
                    }
                    if(!check) noPrep.add(parPos);
                    check = false;  
                }
                for(BI_PL_Position_Cycle_User__c noPrepOwn : [SELECT BI_PL_User__c, BI_PL_Position_Cycle__c FROM BI_PL_Position_Cycle_User__c WHERE BI_PL_Position_Cycle__c IN: noPrep AND BI_PL_Preparation_Owner__c = true]){
                    sendList.put(noPrepOwn.BI_PL_User__c, noPrepOwn.BI_PL_Position_Cycle__c);
                }
                for(BI_PL_Preparation__c prepOwn : prepOwners){
                    sendList.put(prepOwn.Owner.Id, prepOwn.BI_PL_Position_Cycle__c);
                }

                System.debug('sendList ' + sendList);

                if (BI_PL_CountrySettingsUtility.isNotifyHierarchyEnabled()){

                    BI_PL_EmailNotificationUtility.sendEmail(BI_PL_EmailNotificationUtility.CYCLE_PLAN_SUBMITED, BI_PL_EmailNotificationUtility.generateEmailNotificationIdPairs(sendList));
                }
            }

            for(Integer i=0;i<trigger.new.size();i++){
                if(((BI_PL_Position_Cycle__c)trigger.new.get(i)).BI_PL_Confirmed__c == false && ((BI_PL_Position_Cycle__c)trigger.old.get(i)).BI_PL_Confirmed__c == true){
                    listUnconfirmedNodes.add(trigger.new.get(i).Id);
                }
            }

            System.debug('**listUnconfirmedNodes ' + listUnconfirmedNodes);

            if(listUnconfirmedNodes.size() > 0){

                listUnconfirmedNodesUsers = [SELECT BI_PL_User__c, BI_PL_Position_Cycle__c FROM BI_PL_Position_Cycle_User__c WHERE BI_PL_Position_Cycle__c IN: listUnconfirmedNodes];

                System.debug('**listUnconfirmedNodesUsers ' + listUnconfirmedNodesUsers);

                for(Id nod : listUnconfirmedNodes){
                    for(BI_PL_Position_Cycle_User__c us : listUnconfirmedNodesUsers){
                        if(us.BI_PL_Position_Cycle__c == nod) pairToSend.put(us.BI_PL_User__c, nod);
                    }
                }

                System.debug('**pairToSend ' + pairToSend);

                if (BI_PL_CountrySettingsUtility.isNotifyHierarchyEnabled()){

                    BI_PL_EmailNotificationUtility.sendEmail(BI_PL_EmailNotificationUtility.UNSUBMIT_POSITION, BI_PL_EmailNotificationUtility.generateEmailNotificationIdPairs(pairToSend));
                }
            }
            */
        }
    }

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {

    }
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */

    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(SObject so) {

    }


    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(SObject oldSo, SObject so) {
        updateParentLookup((BI_PL_Position_cycle__c)oldSo,(BI_PL_Position_cycle__c)so);
    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so) {
    }

    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(SObject so) {
        fillParentLookup(so);
    }


    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(SObject oldSo, SObject so) {
        fillParentLookup(so);

    }

    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(SObject so) {
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        System.debug(loggingLevel.Error, '*** toUpdate: ' + toUpdate);
        if(!toUpdate.isEmpty()) {
            update toUpdate;
        }
    }

    public void updateParentLookup(BI_PL_Position_cycle__c oldSo, BI_PL_Position_cycle__c so){
        if(oldSo.BI_PL_Parent_position__c != so.BI_PL_Parent_position__c){
            so.BI_PL_Parent__c = null;
        }
    }


    public void fillParentLookup(sObject so) {
        BI_PL_Position_cycle__c pc = (BI_PL_Position_cycle__c) so.clone(true, true, false, false);
        if(pcIdsByPosition.containsKey(pc.BI_PL_Parent_position__c+pc.BI_PL_Hierarchy__c+pc.BI_PL_Cycle__c) && pc.BI_PL_Parent__c == null && pc.BI_PL_Parent_position__c != null) {
            pc.BI_PL_Parent__c = pcIdsByPosition.get(pc.BI_PL_Parent_position__c+pc.BI_PL_Hierarchy__c+pc.BI_PL_Cycle__c);
            toUpdate.add(pc);
        }
    }

    /**
     *  Generates Last Action for preparations.
     *  @author Omega CRM
     *  @return Map where key = positionId.
     */
    private void getLastActionforPreparation(List<BI_PL_Position_Cycle__c> lstPositionCycle){

        String sApproved = 'Approved'; 
        String sUnapproved = 'UnApproved'; 
        String sSubmit = 'Submit';
        String sUnsubmit = 'unSubmit';

        map<String,list<BI_PL_Position_Cycle__c> > mapTriggerPosCycle = new map<String,list<BI_PL_Position_Cycle__c> >();
        map<String,map<String,String> > mapAllPSbyHierch = new map<String, map<String,String> >();
        list<BI_PL_Position_Cycle__c> lstNewPosCycle = new list<BI_PL_Position_Cycle__c>();

        set<String> setCycleId = new set<String>();
        set<String> setCycle = new set<String>();

        //get all hierachies and create map will all PositionCycle order by hierarchy
        for (BI_PL_Position_Cycle__c inPC : lstPositionCycle){
            setCycleId.add(inPC.BI_PL_Cycle__c);
            lstNewPosCycle = new list<BI_PL_Position_Cycle__c>();

            if (!mapTriggerPosCycle.containsKey(inPC.BI_PL_Hierarchy__c)) mapTriggerPosCycle.put(inPC.BI_PL_Hierarchy__c, lstNewPosCycle);
            
            lstNewPosCycle = mapTriggerPosCycle.get(inPC.BI_PL_Hierarchy__c);
            lstNewPosCycle.add(inPC);
            mapTriggerPosCycle.put(inPC.BI_PL_Hierarchy__c,lstNewPosCycle);
        }

        /*system.debug('## mapTriggerPosCycle: ' + mapTriggerPosCycle);
        system.debug('## setCycleId: ' + setCycleId);*/

        set<Id> setNotPrep = new set<Id>(); //var to get all Position not Prep type
        map<String,String> mapPS_ChildParent = new map<String,String>();
        //Search for all Positions Cycles in every Hierarchy and Cycle
        for (BI_PL_Position_Cycle__c inPC : [SELECT Id, Name, BI_PL_Hierarchy__c, BI_PL_Cycle__c, BI_PL_Position__c, BI_PL_Parent_position__c 
                                             FROM BI_PL_Position_Cycle__c 
                                             WHERE BI_PL_Hierarchy__c IN :mapTriggerPosCycle.keyset()  
                                                    AND BI_PL_Cycle__c IN :setCycleId]){

            mapPS_ChildParent = new map<String,String>();
            if (mapAllPSbyHierch.containsKey(inPC.BI_PL_Hierarchy__c)) mapPS_ChildParent = mapAllPSbyHierch.get(inPC.BI_PL_Hierarchy__c);
            else mapAllPSbyHierch.put(inPC.BI_PL_Hierarchy__c, mapPS_ChildParent);

            mapPS_ChildParent.put(InPC.BI_PL_Position__c, InPC.BI_PL_Parent_position__c);
            mapAllPSbyHierch.put(inPC.BI_PL_Hierarchy__c, mapPS_ChildParent);
            if (InPC.BI_PL_Parent_position__c != null) //data stewart has not parent
                setNotPrep.add(InPC.BI_PL_Parent_position__c);
        }

        /*system.debug('## mapAllPSbyHierch: ' + mapAllPSbyHierch);*/

        //get all Cycle Names
        for (BI_PL_Cycle__c inCycle : [SELECT Name 
                                        FROM BI_PL_Cycle__c 
                                        WHERE Id IN : setCycleId]){
            setCycle.add(inCycle.Name);
        }
       /* system.debug('## setCycleName: ' + setCycle);*/

        //List woith Preparation to be updated with Last Action info.
        list<BI_PL_Preparation__c> lstPreparation = new list<BI_PL_Preparation__c>();
        

        //Search for all Preparations related to herarchies
        for (BI_PL_Preparation__c inPrep : [SELECT Id, BI_PL_Position_Cycle__c, BI_PL_Position_Cycle__r.BI_PL_Position__c, BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c, 
                                                    BI_PL_Last_Action__c, BI_PL_Territory_Last_Action__c 
                                                    FROM BI_PL_Preparation__c 
                                                    WHERE BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c IN :mapTriggerPosCycle.keyset() 
                                                        AND BI_PL_Cycle_name__c IN :setCycle]){ 

            /*system.debug('## inPrep.BI_PL_Position_Cycle__c: ' + inPrep);
            system.debug('## Hierarchy: ' + inPrep.BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c);
            system.debug('## map get: ' + mapTriggerPosCycle.get(inPrep.BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c));
*/
            if (inPrep.BI_PL_Position_Cycle__c != null && mapTriggerPosCycle.containsKey(inPrep.BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c)){
                for (BI_PL_Position_Cycle__c inPosCycle : mapTriggerPosCycle.get(inPrep.BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c) ){
                    
                    //if(mapAllPSbyHierch.containsKey(inPosCycle.BI_PL_Hierarchy__c)){
                    //get All Position Cycles by the Hierarchy
                    mapPS_ChildParent = mapAllPSbyHierch.get((inPosCycle.BI_PL_Hierarchy__c));
                    /*system.debug('## mapPS_ChildParent: ' + mapPS_ChildParent);
                    system.debug('## PosCycle: ' + inPosCycle.BI_PL_Position__c);
                    system.debug('## inPrep.BI_PL_Position_Cycle__c: ' + inPrep.BI_PL_Position_Cycle__r.BI_PL_Position__c);*/

                    //Check Preparation belongs to current Position Cycle
                    if (mapPS_ChildParent.containskey(inPosCycle.BI_PL_Position__c) ){
                        //system.debug('## IN ##');
                        //check BI_PL_Position_Cycle__r.BI_PL Position__c of Preparation is PS in Trigger -> Prep
                        if(inPosCycle.BI_PL_Position__c.equals(inPrep.BI_PL_Position_cycle__r.BI_PL_Position__c)) {
                            //if (mapPS_ChildParent.containskey(inPrep.BI_PL_Position_Cycle__r.BI_PL_Position__c) ){
                            //system.debug('## Prep: ' + inPosCycle.BI_PL_Position__c);
                            inPrep.BI_PL_Last_Action__c = inPosCycle.BI_PL_Confirmed__c ? sSubmit : sUnsubmit;
                            inPrep.BI_PL_Territory_Last_Action__c = inPosCycle.BI_PL_Position_name__c;
                            lstPreparation.add(inPrep);
                            //break;
                            //}
                        }
                        //Check PositionCycle is Manager or data stewart
                        else if (setNotPrep.contains(inPosCycle.BI_PL_Position__c) ){
                            if (mapPS_ChildParent.get(inPosCycle.BI_PL_Position__c) != null){ //Manager
                                //system.debug('## Manag: ' + inPosCycle.BI_PL_Position__c);
                                inPrep.BI_PL_Last_Action__c = inPosCycle.BI_PL_Confirmed__c ? sSubmit : sUnsubmit;
                                inPrep.BI_PL_Territory_Last_Action__c = inPosCycle.BI_PL_Position_name__c;
                                lstPreparation.add(inPrep);
                            }
                            else{ //data stewart (it has not a parent)
                                //system.debug('## datastewart: ' + inPosCycle.BI_PL_Position__c);
                                inPrep.BI_PL_Last_Action__c = inPosCycle.BI_PL_Confirmed__c ? sApproved : sUnapproved;
                                inPrep.BI_PL_Territory_Last_Action__c = inPosCycle.BI_PL_Position_name__c;
                                lstPreparation.add(inPrep);
                            }
                        }
                    }
                }                        
            }
        }
        if(!lstPreparation.isEmpty()){

            Set<BI_PL_Preparation__c> setList = new Set<BI_PL_Preparation__c>(lstPreparation);
            List<BI_PL_Preparation__c> newlist = new List<BI_PL_Preparation__c>(setList);

            update newlist;
        } 
    }
}