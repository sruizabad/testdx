@isTest
private class BI_PL_PositionCycleHandler_Test {
	
    private static final String SSUBMIT = 'Submit';

	private static String userCountryCode;
	private static list<BI_PL_Position_Cycle__c> lstPosCycles;
	private static set<Id> setCycleId = new set<Id>();
	private static set<String> setCycle = new set<String>();
	private static set<String> setHierarchy = new set<String>();
	private static set<Id> setRep = new set<Id>();
    private static set<Id> setMang = new set<Id>();
    private static set<Id> setDataStew = new set<Id>();

	private static void getData(){
		set<String> setParents = new set<String>();

		lstPosCycles = [SELECT Id, BI_PL_Position__c, BI_PL_Position__r.Name, BI_PL_Parent_position__c, BI_PL_Parent_position__r.Name, 
								BI_PL_Confirmed__c, BI_PL_Rejected_reason__c, BI_PL_Hierarchy__c, BI_PL_Cycle__c 
						FROM BI_PL_Position_Cycle__c];

		system.assert(lstPosCycles.size() > 0);		

		for (BI_PL_Position_Cycle__c inPosCycle : lstPosCycles){			
			
			system.debug('## PosCycle: ' + inPosCycle);

			setHierarchy.add(inPosCycle.BI_PL_Hierarchy__c);
			setCycleId.add(inPosCycle.BI_PL_Cycle__c);
			if (inPosCycle.BI_PL_Parent_position__c != null) 
				setParents.add(inPosCycle.BI_PL_Parent_position__c);
		}

		system.assert(!setHierarchy.isEmpty());
		system.assert(!setCycleId.isEmpty());
		system.assert(!setParents.isEmpty());

		//Order Position Cycle
        for (BI_PL_Position_Cycle__c inPosCycle : lstPosCycles){
        	if (!setParents.contains(inPosCycle.BI_PL_Position__c)){
        		setRep.add(inPosCycle.BI_PL_Position__c); //sales rep        		
        	}
        	else if (inPosCycle.BI_PL_Parent_position__c != null)
        		setMang.add(inPosCycle.BI_PL_Position__c); //manager
        	else if (inPosCycle.BI_PL_Parent_position__c == null )
        		setDataStew.add(inPosCycle.BI_PL_Position__c); //datastewart
        }

		system.assert(!setRep.isEmpty());
		system.assert(!setMang.isEmpty());
		system.assert(!setDataStew.isEmpty());

        for (BI_PL_Cycle__c inCycle : [SELECT Name 
                                FROM BI_PL_Cycle__c 
                                WHERE Id IN : setCycleId]){
            setCycle.add(inCycle.Name);
        }

        system.assert(!setCycle.isEmpty());
	}

	@testSetup  static void setup() {

		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        userCountryCode = testUser.Country_Code_BI__c;

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        BI_PL_TestDataFactory.createTestAccounts(1, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(1, userCountryCode);
        
		List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
	}

	@isTest static void test_Rep() {
		
		getData();
        
		Test.startTest();

		for (BI_PL_Position_Cycle__c inPosCycle : lstPosCycles){
			if (setRep.contains(inPosCycle.BI_PL_Position__c) )
				inPosCycle.BI_PL_Confirmed__c = true;
		}
        update lstPosCycles;

        Test.stopTest();

		for (BI_PL_Preparation__c inPrep : [SELECT Id, BI_PL_Cycle_name__c, BI_PL_Position_Cycle__c, BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c, 
													BI_PL_Position_Cycle__r.BI_PL_Position__c, BI_PL_Last_Action__c, BI_PL_Territory_Last_Action__c
												FROM BI_PL_Preparation__c 
												WHERE BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c IN :setHierarchy 
                                                	AND BI_PL_Cycle_name__c IN :setCycle
                                                	AND BI_PL_Position_Cycle__r.BI_PL_Position__c IN :setRep]){
			system.debug('## Prep: ' + inPrep);
			system.assert(String.isNotBlank(inPrep.BI_PL_Last_Action__c) );
			system.assert(String.isNotBlank(inPrep.BI_PL_Territory_Last_Action__c) );
			system.assertEquals(inPrep.BI_PL_Last_Action__c,SSUBMIT);
		}
	}
	
	@isTest static void test_Manag() {
		
		getData();
        
		Test.startTest();

		for (BI_PL_Position_Cycle__c inPosCycle : lstPosCycles){
			if (setMang.contains(inPosCycle.BI_PL_Position__c) )
				inPosCycle.BI_PL_Confirmed__c = true;
		}
        update lstPosCycles;

        Test.stopTest();

		for (BI_PL_Preparation__c inPrep : [SELECT Id, BI_PL_Cycle_name__c, BI_PL_Position_Cycle__c, BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c, 
													BI_PL_Position_Cycle__r.BI_PL_Position__c, BI_PL_Last_Action__c, BI_PL_Territory_Last_Action__c
												FROM BI_PL_Preparation__c 
												WHERE BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c IN :setHierarchy 
                                                	AND BI_PL_Cycle_name__c IN :setCycle]){

			system.assert(String.isNotBlank(inPrep.BI_PL_Last_Action__c) );
			system.assert(String.isNotBlank(inPrep.BI_PL_Territory_Last_Action__c) );
			system.assertEquals(inPrep.BI_PL_Last_Action__c,SSUBMIT);
		}
	}

	@isTest static void test_dataStew() {
		
		getData();
        
		Test.startTest();

		for (BI_PL_Position_Cycle__c inPosCycle : lstPosCycles){
			if (setDataStew.contains(inPosCycle.BI_PL_Position__c) )
				inPosCycle.BI_PL_Confirmed__c = true;
		}
        update lstPosCycles;

        Test.stopTest();

        for (BI_PL_Preparation__c inPrep : [SELECT Id, BI_PL_Cycle_name__c, BI_PL_Position_Cycle__c, BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c, 
													BI_PL_Position_Cycle__r.BI_PL_Position__c, BI_PL_Last_Action__c, BI_PL_Territory_Last_Action__c
												FROM BI_PL_Preparation__c 
												WHERE BI_PL_Position_Cycle__r.BI_PL_Hierarchy__c IN :setHierarchy 
                                                	AND BI_PL_Cycle_name__c IN :setCycle]){

			system.debug('## Last Action: ' + inPrep.BI_PL_Last_Action__c);
			system.debug('## Terridtory: ' + inPrep.BI_PL_Territory_Last_Action__c);
			system.assert(String.isNotBlank(inPrep.BI_PL_Last_Action__c) );
			system.assert(String.isNotBlank(inPrep.BI_PL_Territory_Last_Action__c) );			
		}
	}
}