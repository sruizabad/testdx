public class BI_PL_PositionCycleUserHandler implements BI_PL_TriggerInterface {


	private Map<Id,BI_PL_Position_cycle__c> mapPositionsCycle;
	private Map<Id, User> mapUsers;

	public BI_PL_PositionCycleUserHandler(){}

	/**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */

	public void bulkAfter() {}


    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */

	public void bulkBefore() {

		if(Trigger.isInsert){

		Set<Id> pcId = new Set<Id>();
		Set<Id> userId = new Set<Id>();
		for(BI_PL_Position_cycle_user__c u : (List<BI_PL_Position_cycle_user__c>)trigger.new){
			pcId.add(u.BI_PL_Position_cycle__c);
			userId.add(u.BI_PL_User__c);
		}

		mapPositionsCycle = new Map<Id,BI_PL_Position_cycle__c>([SELECT id, BI_PL_External_id__c FROM BI_PL_Position_cycle__c WHERE Id = :pcId]);
		mapUsers = new Map<Id, User>([SELECT id, External_ID__c FROM User WHERE Id = :userId]);

		}
    }

    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(SObject so) {
    	BI_PL_Position_cycle_user__c user = (BI_PL_Position_cycle_user__c)so;

    	user.BI_PL_External_id__c = mapPositionsCycle.get(user.BI_PL_Position_cycle__c).BI_PL_External_id__c + '_' + mapUsers.get(user.BI_PL_User__c).External_ID__c;

    }

    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(SObject oldSo, SObject so) {

    }

     /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so) {
    }

    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(SObject so) {
    }

    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(SObject oldSo, SObject so) {
    }

    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(SObject so) {
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
    }

}