/**
 *	09/10/2017
 *	- GLOS-424 : Set the Position Cycle (Preparation) Owner at Position Cycle User level
 *	14/08/2017
 *	- Summary Review page requirements.
 *	11/08/2017
 *	- "children" getter.
 *	Stores the data of a  BI_PL_Position_cycle__c.
 *	Used by:
 *	- BI_PL_PositionWrapper
 *	- BI_PL_PreparationVisibilityUtility
 *	- BI_PL_VisibilityTree
 *	@author Omega CRM
 */
public without sharing class BI_PL_PositionCycleWrapper {

	public BI_PL_PositionWrapper position {get; set;}
	public BI_PL_PositionWrapper parentPosition {get; set;}

	public Boolean isConfirmed {get; set;}
	public String rejectedReason {get; set;}
	public Id positionCycleId {get; set;}

	public BI_PL_PositionCycleWrapper parent {get; set;}
	public Set<BI_PL_PositionCycleWrapper> children {get; set;}
	public List<BI_PL_Position_Cycle_User__c> positionCycleUsers {get; set;}
	public List<BI_PL_Position_Cycle_User__c> positionCycleUsersExclusiveNode {get; set;}
	public Id preparationOwner {get; set;}
	public Integer level {get; set;}

	public BI_PL_PositionCycleWrapper(Id positionCycleId, BI_PL_PositionWrapper position, BI_PL_PositionWrapper parentPosition, BI_PL_PositionCycleWrapper parent, Boolean isConfirmed, String rejectedReason, Integer level) {
		this.positionCycleId = positionCycleId;
		this.children = new Set<BI_PL_PositionCycleWrapper>();
		this.position = position;
		this.parentPosition = parentPosition;
		this.rejectedReason = rejectedReason;
		this.level = level;

		

		if (parent != null) {
			this.parent = parent;
			parent.addChild(this, parent.level + 1);
			
		} else {
			System.debug('### Parent null');
		}
		positionCycleUsers = new List<BI_PL_Position_Cycle_User__c>();
		positionCycleUsersExclusiveNode = new List<BI_PL_Position_Cycle_User__c>();

		this.isConfirmed = isConfirmed;
	}
	public void addPositionCycleUser(BI_PL_Position_Cycle_User__c p, Boolean exclusiveNode) {
		positionCycleUsers.add(p);
		if (children != null) {
			for (BI_PL_PositionCycleWrapper c : children) {
				c.addPositionCycleUser(p, false);
			}
		}
		if (exclusiveNode) {
			positionCycleUsersExclusiveNode.add(p);

			// Set the first found user with the check set to true as the owner:
			if (preparationOwner == null && p.BI_PL_Preparation_owner__c)
				preparationOwner = p.BI_PL_User__c;
		}

	}

	public void addChild(BI_PL_PositionCycleWrapper child, Integer level) {
		child.level = level;
		this.children.add(child);
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	public Set<BI_PL_PositionCycleWrapper> getChildren() {
		return children;
	}

	public List<BI_PL_PositionCycleWrapper> getChildrenList() {
		return new List<BI_PL_PositionCycleWrapper> (children);
	}

	public Map<String, BI_PL_PositionWrapper> getChildrenPositionSet() {
		Map<String, BI_PL_PositionWrapper> result = new Map<String, BI_PL_PositionWrapper>();
		
		result.put(this.position.recordId, this.position);
		
		for(BI_PL_PositionCycleWrapper pcw : this.getChildrenList()) {
			System.debug(loggingLevel.Error, '*** getChildrenPositionSet pcw: ' + pcw);
			System.debug(loggingLevel.Error, '*** getChildrenPositionSet pcw.getChildrenPositionSet: ' + pcw.getChildrenPositionSet());
			result.putAll(pcw.getChildrenPositionSet());
		}
		System.debug(loggingLevel.Error, '*** getChildrenPositionSet: ' + result);
		return result;
	}

	public String getChildrenPositionSetForQuery() {
		//List<String> positionIds = new List<String>();
		//for(String p : getChildrenPositionSet().keySet()) {
		//	positionIds.add(p);
		//}
		//System.debug(loggingLevel.Error, '*** getChildrenPositionSetForQuery: ' + positionIds);
		return '\'' +String.join(new List<String>(getChildrenPositionSet().keySet()),  '\',\'') + '\'';
	}

	public Boolean hasExclusiveNodes(){
		return !positionCycleUsersExclusiveNode.isEmpty();
	}

	public Id getOwnerId() {
		if (preparationOwner == null && hasExclusiveNodes()){
			return positionCycleUsersExclusiveNode.get(0).BI_PL_User__c;
		}else{
			return preparationOwner;
		}
	}

}