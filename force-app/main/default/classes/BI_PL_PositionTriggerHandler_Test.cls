@isTest
public with sharing class BI_PL_PositionTriggerHandler_Test {
	@isTest
	public static void test() {

		BI_PL_Position__c position1 = new BI_PL_Position__c(Name = 'TestPosition', BI_PL_Country_code__c = 'BR');
		BI_PL_Position__c position2 = new BI_PL_Position__c(Name = 'TestPosition2', BI_PL_Country_code__c = 'BR');

		insert new List<BI_PL_Position__c> {position1, position2};

		List<BI_PL_Position__c> positions = [SELECT BI_PL_Country_code__c, Name, BI_PL_External_id__c FROM BI_PL_Position__c ORDER BY Id];

		System.assertEquals(positions.get(0).BI_PL_Country_code__c + '_' + positions.get(0).Name, positions.get(0).BI_PL_External_id__c);
		System.assertEquals(positions.get(1).BI_PL_Country_code__c + '_' + positions.get(1).Name, positions.get(1).BI_PL_External_id__c);


		BI_PL_PositionTriggerHandler handler = new BI_PL_PositionTriggerHandler();
		handler.bulkAfter();
		handler.beforeUpdate(positions.get(0), positions.get(1));
		handler.afterUpdate(positions.get(0), positions.get(1));
		handler.afterDelete(positions.get(0));
		handler.beforeDelete(positions.get(0));
		handler.afterInsert(positions.get(0));
		handler.andFinally();

		update new List<BI_PL_Position__c> {position1, position2};

		delete new List<BI_PL_Position__c> {position1, position2};

		try{
			BI_PL_TriggerFactory.createHandler(Schema.Account.sObjectType);
		} catch (Exception e){

		}

	}
}