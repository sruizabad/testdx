/**
 *	Wrapper class for a BI_PL_Position__c record.
 *	Used by:
 *	- BI_PL_PositionCycleWrapper
 *	- BI_PL_PreparationVisibilityUtility
 *	- BI_PL_PreparationWrapper
 *	- BI_PL_VisibilityTree
 *	@author OMEGA CRM
 */
public with sharing class BI_PL_PositionWrapper {

	public BI_PL_Position__c record {get; set;}
	public String recordId {get; set;}
	public String name {get;set;}
	public Map<String, Integer> fieldForceCount {get;set;}
	public Integer total {get;set;}
	public Integer overlap {get;set;}
	public Integer pctg {get;set;}

	public BI_PL_PositionWrapper(BI_PL_Position__c record, String recordId) {
		this.record = record;
		this.recordId = recordId;
	}


	public BI_PL_PositionWrapper(BI_PL_Target_preparation__c tgt){
		this.fieldForceCount = new Map<String, Integer>();
		this.name = tgt.BI_PL_header__r.BI_PL_position_cycle__r.BI_PL_position_name__c;
		this.total = 0;
		this.aggregate(tgt);
	}

	public void addOverlap(Integer overlap){
		this.overlap = overlap;
		this.pctg = (this.overlap / this.total) * 100;
	}

	public void aggregate(BI_PL_Target_preparation__c tgt) {
		String ff = tgt.BI_PL_header__r.BI_PL_position_cycle__r.BI_PL_position__r.BI_PL_field_force__c;

		if(this.fieldForceCount.containsKey(ff)) {
			this.fieldForceCount.put(ff, this.fieldForceCount.get(ff) + 1);
		} else {
			this.fieldForceCount.put(ff, 1);
		}
		this.total += 1;
	}
}