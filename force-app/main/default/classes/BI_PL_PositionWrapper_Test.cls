@isTest private class BI_PL_PositionWrapper_Test {
	
	
	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        Date startDate = Date.today()-60;
        Date endDate =  Date.today()-30;
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);

    }

	@isTest static void test_method_one() {

		BI_PL_PositionWrapper pw = new BI_PL_PositionWrapper(null,'recordId');
		
		pw.total = 50;
		pw.addOverlap(2);
		BI_PL_Target_preparation__c targetPreparation = [SELECT Id, 
				BI_PL_header__r.BI_PL_position_cycle__r.BI_PL_position_name__c,
				BI_PL_header__r.BI_PL_position_cycle__r.BI_PL_position__r.BI_PL_field_force__c
				 FROM BI_PL_Target_preparation__c LIMIT 1];
        pw = new BI_PL_PositionWrapper(targetPreparation);
		//pw.aggregate(targetPreparation);

	}

}