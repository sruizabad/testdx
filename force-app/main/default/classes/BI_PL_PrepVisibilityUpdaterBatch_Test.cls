@isTest
private class BI_PL_PrepVisibilityUpdaterBatch_Test {

	private static String userCountryCode;
	private static String hierarchy = 'hierarchyTest';
	private static String hierarchy2 = 'hierarchyTest2';
	
	@isTest static void test_method_one() {
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		userCountryCode = currentUser.Country_Code_BI__c;

		BI_PL_TestDataFactory.createTestAccounts(6,userCountryCode);
		BI_PL_TestDataFactory.createTestProduct(2, userCountryCode);
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'test1',BI_TM_Country_code__c =userCountryCode );

		insert fieldForce;

		BI_PL_Cycle__c cycle2 = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];

		String cycleId = cycle2.Id;
		
		BI_PL_PreparationVisibilityUpdaterBatch toExecute = new BI_PL_PreparationVisibilityUpdaterBatch(cycleId, hierarchy, true);

		BI_PL_PreparationVisibilityUpdaterBatch toExecute2 = new BI_PL_PreparationVisibilityUpdaterBatch(cycleId, hierarchy, false);

		List<String> listHierarchies = new list<String> ();
		listHierarchies.add(hierarchy);
		
		List<String> nextCycles = new list<String> ();
		nextCycles.add(cycleId);

		BI_PL_PreparationVisibilityUpdaterBatch toExecute3 = new BI_PL_PreparationVisibilityUpdaterBatch(cycleId, hierarchy, false, nextCycles, listHierarchies);
		
		List<String> nextCyclesnull = new List<String> ();

		listHierarchies.add(hierarchy2);

		BI_PL_PreparationVisibilityUpdaterBatch toExecute4 = new BI_PL_PreparationVisibilityUpdaterBatch(cycleId, hierarchy, false, null, listHierarchies);

		List<String> listHierarchiesnull = new List<String> ();

		BI_PL_PreparationVisibilityUpdaterBatch toExecute5 = new BI_PL_PreparationVisibilityUpdaterBatch(cycleId, hierarchy, false, nextCycles, listHierarchiesnull);

		BI_PL_PreparationVisibilityUpdaterBatch toExecute6 = new BI_PL_PreparationVisibilityUpdaterBatch(cycleId, hierarchy, true, nextCycles, listHierarchies);

		Test.startTest();
		try{
			Id batchId = Database.executeBatch(toExecute);
			Id batchId2 = Database.executeBatch(toExecute2);
			Id batchId3 = Database.executeBatch(toExecute3);
			Id batchId4 = Database.executeBatch(toExecute4);
			//Id batchId5 = Database.executeBatch(toExecute5);
			Id batchId6 = Database.executeBatch(toExecute6);
		}catch (Exception e){
		}
		Test.stopTest();

	}
	
}