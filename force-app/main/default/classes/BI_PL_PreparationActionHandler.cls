public without sharing class BI_PL_PreparationActionHandler
    implements BI_PL_TriggerInterface {


    Map<String, Map<Id, List<BI_PL_Channel_detail_preparation__c>>> interactionsByPrepAndAccount;
    //Map<String, BI_PL_Target_preparation__c> sourceTargets;
    Map<String, BI_PL_Target_preparation__c> targetTargetsByAccount = new Map<String, BI_PL_Target_preparation__c>();
    Map<String, List<BI_PL_Preparation_action_item__c>> itemsByAction;
    List<BI_PL_PreparationExt.PlanitTargetModel> modelsToSave = new List<BI_PL_PreparationExt.PlanitTargetModel>();
    //List<BI_PL_Channel_detail_preparation__c> channelDetailsToUpdate = new List<BI_PL_Channel_detail_preparation__c>();
    Map<Id, BI_PL_Channel_detail_preparation__c> channelDetailsToUpdate = new Map<Id, BI_PL_Channel_detail_preparation__c>();
    /**
     * List of details to be updated. Filled when a share (PUSH)
     */
    Map<Id, BI_PL_Detail_preparation__c> detailsToUpdate = new Map<Id, BI_PL_Detail_preparation__c>();

    // Constructor
    public BI_PL_PreparationActionHandler() {}

    public void bulkAfter() {

        interactionsByPrepAndAccount = new Map<String, Map<Id, List<BI_PL_Channel_detail_preparation__c>>>();
        itemsByAction = new Map<String, List<BI_PL_Preparation_action_item__c>>();

        //Only work on update
        if (!Trigger.isUpdate) return;

        System.debug(loggingLevel.Error, '*** bulk after update: ');

        Set<String> actions = new Set<String>();
        Set<String> channels = new Set<String>();
        Set<String> sourceAccounts = new Set<String>();
        Set<String> sourcePreparations = new Set<String>();
        Set<String> targetPreparations = new Set<String>();
        Integer requestCounter = 0;
        Integer noRequestCounter = 0;

        for (BI_PL_Preparation_action__c act : (List<BI_PL_Preparation_action__c>) Trigger.new) {
            actions.add(act.Id);
            channels.add(act.BI_PL_Channel__c);
            sourceAccounts.add(act.BI_PL_Account__c);
            sourcePreparations.add(act.BI_PL_Source_preparation__c);
        }

        for (BI_PL_Preparation_action_item__c item : [SELECT Id,
                BI_PL_Target_preparation__c,
                BI_PL_Request_response__c,
                BI_PL_Target_preparation__r.BI_PL_External_id__c,
                BI_PL_Target_adjustment__c,
                BI_PL_Source_adjustment__c,
                BI_PL_Product__c,
                BI_PL_Secondary_Product__c,
                BI_PL_Parent__c,
                BI_PL_Parent__r.BI_PL_Added_reason__c,
                BI_PL_Parent__r.BI_PL_Account__r.External_Id_vod__c,
                BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_External_id__c
                FROM BI_PL_Preparation_action_item__c WHERE BI_PL_Parent__c IN :actions ]) {
            targetPreparations.add(item.BI_PL_Target_preparation__c);
            if (itemsByAction.containsKey(item.BI_PL_Parent__c)) {
                itemsByAction.get(item.BI_PL_Parent__c).add(item);
            } else {
                itemsByAction.put(item.BI_PL_Parent__c, new List<BI_PL_Preparation_action_item__c> {item});
            }
        }

        System.debug(loggingLevel.Error, '*** actions: ' + actions);
        System.debug(loggingLevel.Error, '*** channels: ' + channels);
        System.debug(loggingLevel.Error, '*** targetPreparations: ' + targetPreparations);
        System.debug(loggingLevel.Error, '*** sourceAccounts: ' + sourceAccounts);
        System.debug(loggingLevel.Error, '*** sourcePreparations: ' + sourcePreparations);
        System.debug(loggingLevel.Error, '*** itemsByAction: ' + itemsByAction);

        for (BI_PL_Channel_detail_preparation__c chD : [SELECT Id,
                BI_PL_Channel__c,
                BI_PL_Removed__c,
                BI_PL_Target__r.BI_PL_Header__c,
                BI_PL_Target__r.BI_PL_Added_manually__c,
                BI_PL_Target__r.BI_PL_Next_best_account__c,
                BI_PL_Target_account__c,
                (SELECT Id, BI_PL_Product__c,
                 BI_PL_Secondary_product__c,
                 BI_PL_Secondary_product__r.Name,
                 BI_PL_Secondary_product__r.External_ID_vod__c,
                 BI_PL_Column__c,
                 BI_PL_Row__c, BI_PL_Product__r.Name,
                 BI_PL_Product__r.External_ID_vod__c,
                 BI_PL_Planned_Details__c,
                 BI_PL_Adjusted_Details__c, BI_PL_Segment__c,
                 BI_PL_Segment_Color__c,
                 BI_PL_External_ID__c, BI_PL_Added_Manually__c,
                 BI_PL_Business_rule_ok__c,
                 BI_PL_Commit_target__c,
                 BI_PL_Cvm__c,
                 BI_PL_Detail_priority__c,
                 BI_PL_Fdc__c,
                 BI_PL_Ims_id__c,
                 BI_PL_Market_target__c,
                 BI_PL_Other_goal__c,
                 BI_PL_Poa_objective__c,
                 BI_PL_Prescribe_target__c,
                 BI_PL_Primary_goal__c,
                 BI_PL_Strategic_segment__c
                 FROM BI_PL_Details_Preparation__r
                 ORDER BY BI_PL_Product__r.Name)
                FROM BI_PL_Channel_detail_preparation__c
                WHERE BI_PL_Target__r.BI_PL_Target_customer__c IN :sourceAccounts
                //AND BI_PL_Channel__c IN :channels
                //AND (BI_PL_Target__r.BI_PL_Header__c IN :sourcePreparations OR BI_PL_Target__r.BI_PL_Header__c IN :targetPreparations)
                AND (BI_PL_Target__r.BI_PL_Header__c IN :sourcePreparations OR BI_PL_Target__r.BI_PL_Header__c IN :targetPreparations)
                                                       ]) {
            System.debug(loggingLevel.Error, '*** chD: ' + chD);
            if (interactionsByPrepAndAccount.containsKey(chD.BI_PL_Target__r.BI_PL_Header__c)) {
                if (interactionsByPrepAndAccount.get(chD.BI_PL_Target__r.BI_PL_Header__c).containsKey(chD.BI_PL_Target_account__c)) {
                    interactionsByPrepAndAccount.get(chD.BI_PL_Target__r.BI_PL_Header__c).get(chD.BI_PL_Target_account__c).add(chD);
                } else {
                    interactionsByPrepAndAccount.get(chD.BI_PL_Target__r.BI_PL_Header__c).put(chD.BI_PL_Target_account__c, new List<BI_PL_Channel_detail_preparation__c> {chD});
                }
            } else {
                interactionsByPrepAndAccount.put(chD.BI_PL_Target__r.BI_PL_Header__c, new Map<Id, List<BI_PL_Channel_detail_preparation__c>> {chD.BI_PL_Target_account__c => new List<BI_PL_Channel_detail_preparation__c> {chD}});
            }

        }

        for (BI_PL_Target_preparation__c tgt : [SELECT Id,
                BI_PL_Added_manually__c,
                BI_PL_No_see_list__c,
                BI_PL_Next_best_account__c,
                BI_PL_Target_customer__c
                
                FROM BI_PL_Target_preparation__c
                WHERE BI_PL_Target_customer__c IN :sourceAccounts
                AND BI_PL_Header__c IN :targetPreparations ]) {

            if (!targetTargetsByAccount.containsKey(tgt.BI_PL_Target_customer__c)) {
                targetTargetsByAccount.put(tgt.BI_PL_Target_customer__c, tgt);
            }
        }

        System.debug(loggingLevel.Error, '*** interactionsByPrepAndAccount: ' + interactionsByPrepAndAccount);


    }

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {

    }
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */

    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(SObject so) {
        //System.debug(loggingLevel.Error, '*** BI_PL_PreparationActionHandler -> beforeInsert -->: ' + so);

    }


    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(SObject oldSo, SObject so) {
        //System.debug(loggingLevel.Error, '*** BI_PL_PreparationActionHandler -> beforeUpdate --> oldSo : ' + oldSo);
        //System.debug(loggingLevel.Error, '*** BI_PL_PreparationActionHandler -> beforeUpdate --> so: ' + so);

    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so) {
    }

    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(SObject so) {
    }


    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(SObject oldSo, SObject so) {
        BI_PL_Preparation_action__c actionOld = (BI_PL_Preparation_action__c) oldSo;
        BI_PL_Preparation_action__c action = (BI_PL_Preparation_action__c) so;

        // New targets / targets to update map:
        // key = preparation id, value = TargetModel
        Map<String, BI_PL_PreparationExt.PlanitTargetModel> targetsModelByPreparation = new Map<String, BI_PL_PreparationExt.PlanitTargetModel>();

        Map<String, Map<String, BI_PL_PreparationExt.PlanitChannelDetailModel>> modelsMapByTargetPrep = new Map<String, Map<String, BI_PL_PreparationExt.PlanitChannelDetailModel>>();

        if (action.BI_PL_All_approved__c) {

            //PUSH actions
            if (action.BI_PL_Type__c == 'Transfer' || action.BI_PL_Type__c == 'Share') {

                System.debug(loggingLevel.Error, '*** Transfer action: ' );
                //Move the source target preparation to the target preparation
                BI_PL_PreparationExt.PlanitTargetModel model;

                List<BI_PL_Channel_detail_preparation__c> sourceChannelDetailList = interactionsByPrepAndAccount.containsKey(action.BI_PL_Source_preparation__c) && interactionsByPrepAndAccount.get(action.BI_PL_Source_preparation__c).containsKey(action.BI_PL_Account__c)
                        ? interactionsByPrepAndAccount.get(action.BI_PL_Source_preparation__c).get(action.BI_PL_Account__c)  : null;

                System.debug(loggingLevel.Error, '*** sourceChannelDetailList: ' + sourceChannelDetailList);

                BI_PL_Channel_detail_preparation__c sourceChannelDetails;
                //BI_PL_Channel_detail_preparation__c targetChannelDetails;

                if (sourceChannelDetailList == null) return;

                //Search form channel details in action channel
                for (BI_PL_Channel_detail_preparation__c sourceDet : sourceChannelDetailList) {
                    if (action.BI_PL_Channel__c == sourceDet.BI_PL_Channel__c) {
                        sourceChannelDetails = sourceDet;
                        break;
                    }
                }

                System.debug(loggingLevel.Error, '***afterUpdate -> itemsByAction.get(action.Id): ' + itemsByAction.get(action.Id));
                //Compose the target and channel details
                for (BI_PL_Preparation_action_item__c child : itemsByAction.get(action.Id)) {

                    //first time, we created the model
                    if (!targetsModelByPreparation.containsKey(child.BI_PL_Target_preparation__c)) {
                        targetsModelByPreparation.put(child.BI_PL_Target_preparation__c,
                                                      new BI_PL_PreparationExt.PlanitTargetModel(
                                                          new BI_PL_Target_preparation__c(
                                                              BI_PL_Header__r = new BI_PL_Preparation__c(BI_PL_External_id__c = child.BI_PL_Target_preparation__r.BI_PL_External_id__c),
                                                              BI_PL_Target_customer__r = new Account(External_Id_vod__c = child.BI_PL_Parent__r.BI_PL_Account__r.External_Id_vod__c),
                                                              BI_PL_External_id__c = null
                                                          )
                                                      )
                                                     );

                        //Calculate 'Added manually' flag
                        List<BI_PL_Channel_detail_preparation__c> existingChannelDetailsInTarget = null;
                        model = targetsModelByPreparation.get(child.BI_PL_Target_preparation__c);
                        try {
                            existingChannelDetailsInTarget = (interactionsByPrepAndAccount != null 
                                && interactionsByPrepAndAccount.containsKey(child.BI_PL_Target_preparation__c) 
                                && interactionsByPrepAndAccount.get(child.BI_PL_Target_preparation__c).containsKey(child.BI_PL_Parent__r.BI_PL_Account__c)) ? interactionsByPrepAndAccount.get(child.BI_PL_Target_preparation__c).get(child.BI_PL_Parent__r.BI_PL_Account__c) : null;

                            // Two conditions to set the destination target to "Added manually":
                            // - No channel details found in the target preparation.
                            // - The target is a Next Best Account and there are existing channels.
                            if (existingChannelDetailsInTarget == null 
                                || existingChannelDetailsInTarget != null 
                                && existingChannelDetailsInTarget.get(0).BI_PL_Target__r.BI_PL_Next_best_account__c) {
                                model.record.BI_PL_Added_manually__c = true;
                                //model.record.BI_PL_Transferred_Shared__c = true;
                                model.record.BI_PL_Added_reason__c = child.BI_PL_Parent__r.BI_PL_Added_reason__c;
                            }

                            System.debug(loggingLevel.Error, '*** targetTargetsByAccount: ' + targetTargetsByAccount);
                            if(targetTargetsByAccount.containsKey(child.BI_PL_Parent__r.BI_PL_Account__c) 
                                && (targetTargetsByAccount.get(child.BI_PL_Parent__r.BI_PL_Account__c).BI_PL_No_see_list__c == true
                                    || targetTargetsByAccount.get(child.BI_PL_Parent__r.BI_PL_Account__c).BI_PL_Next_best_account__c == true)) {
                                model.record.BI_PL_Added_manually__c = true;
                                model.record.BI_PL_Added_reason__c = child.BI_PL_Parent__r.BI_PL_Added_reason__c;

                            }
                        } catch (Exception e) {
                            System.debug(loggingLevel.Error, '*** Error when getting: ' + e);
                            BI_PL_ErrorHandlerUtility.addPlanitServerError('BI_PL_PreparationActionHandler', 'Transfer and share', e.getMessage(), e.getStackTraceString());
                        }
                    }

                    //model = targetsModelByPreparation.get(child.BI_PL_Target_preparation__c);
                    /*
                    if (model == null) {

                        model = new BI_PL_PreparationExt.PlanitTargetModel(new BI_PL_Target_preparation__c(
                                    BI_PL_Header__r = new BI_PL_Preparation__c(BI_PL_External_id__c = child.BI_PL_Target_preparation__r.BI_PL_External_id__c),
                                    BI_PL_Target_customer__r = new Account(External_Id_vod__c = child.BI_PL_Parent__r.BI_PL_Account__r.External_Id_vod__c),
                                    BI_PL_External_id__c = null
                                ));


                        //Calculate 'Added manually' flag
                        List<BI_PL_Channel_detail_preparation__c> exitingChannelDetailsInTarget = null;
                        try {
                            exitingChannelDetailsInTarget = interactionsByPrepAndAccount.get(child.BI_PL_Target_preparation__c).get(child.BI_PL_Parent__r.BI_PL_Account__c);
                            if (exitingChannelDetailsInTarget == null) {
                                model.record.BI_PL_Added_manually__c = true;
                                model.record.BI_PL_Added_reason__c = child.BI_PL_Parent__r.BI_PL_Added_reason__c;
                            }
                        } catch (Exception e) {
                            System.debug(loggingLevel.Error, '*** Error when getting: ');
                        }
                    }
                    */
                    BI_PL_PreparationExt.PlanitChannelDetailModel targetChannelDetailModel;

                    // Create a position in the map for the target preparation:
                    if (!modelsMapByTargetPrep.containsKey(child.BI_PL_Target_preparation__c))
                        modelsMapByTargetPrep.put(child.BI_PL_Target_preparation__c, new Map<String, BI_PL_PreparationExt.PlanitChannelDetailModel>());

                    // Get the 'targetChannelDetailModel' for the current target preparation and channel:
                    if (modelsMapByTargetPrep.get(child.BI_PL_Target_preparation__c).containsKey(sourceChannelDetails.BI_PL_Channel__c)) {
                        targetChannelDetailModel = modelsMapByTargetPrep.get(child.BI_PL_Target_preparation__c).get(sourceChannelDetails.BI_PL_Channel__c);
                        System.debug(loggingLevel.Error, '*** Cahnnel detail from map ');
                    } else {
                        //Clone source channel-detail and details to target (clear keys and relations with target and channel-details). Basically account and channel
                        targetChannelDetailModel = new BI_PL_PreparationExt.PlanitChannelDetailModel(BI_PL_TransferAndShareUtility.cloneChannelDetailWithoutKeys(sourceChannelDetails));
                        targetChannelDetailModel.record.BI_PL_MSL_Flag__c = true;
                        targetChannelDetailModel.record.BI_PL_Removed__c = false;
                        targetChannelDetailModel.record.BI_PL_Edited__c = true;
                        modelsMapByTargetPrep.get(child.BI_PL_Target_preparation__c).put(sourceChannelDetails.BI_PL_Channel__c, targetChannelDetailModel);
                    }

                    BI_PL_Detail_preparation__c forCurrentItem;

                    for (BI_PL_Detail_preparation__c sourceDetail : sourceChannelDetails.BI_PL_Details_Preparation__r) {
                        //If it is a transfer, just clone all the details
                        if (action.BI_PL_Type__c == 'Transfer') {
                            targetChannelDetailModel.addDetail(BI_PL_TransferAndShareUtility.cloneDetailWithoutKeys(sourceDetail));

                            //If is a share, clone the detail and adjust the values
                        } else if (action.BI_PL_Type__c == 'Share') {

                            System.debug(loggingLevel.Error, '*** Share action for source detail: ' + sourceDetail);
                            System.debug(loggingLevel.Error, '*** Share action item: ' + child);
                            //System.debug(loggingLevel.Error, '*** Primary product match: ' + sourceDetail.BI_PL_Product__c.equals(child.BI_PL_Product__c));
                            //System.debug(loggingLevel.Error, '*** Seconday product match: ' + sourceDetail.BI_PL_Secondary_Product__c == child.BI_PL_Secondary_Product__c);

                            if (sourceDetail.BI_PL_Product__c == child.BI_PL_Product__c &&
                                    (( sourceDetail.BI_PL_Secondary_Product__c == null && child.BI_PL_Secondary_Product__c == null) || (sourceDetail.BI_PL_Secondary_Product__c != null && child.BI_PL_Secondary_Product__c != null && sourceDetail.BI_PL_Secondary_Product__c == child.BI_PL_Secondary_Product__c)) ) {

                                System.debug(loggingLevel.Error, '*** PRODUCT LINE MATCH Adj --> :' + child.BI_PL_Target_adjustment__c);
                                forCurrentItem = BI_PL_TransferAndShareUtility.cloneDetailWithoutKeys(sourceDetail);
                                forCurrentItem.BI_PL_Planned_Details__c = 0;
                                forCurrentItem.BI_PL_Adjusted_Details__c = child.BI_PL_Target_adjustment__c;
                                targetChannelDetailModel.addDetail(forCurrentItem);

                                //Update source detail
                                System.debug(loggingLevel.Error, '*** Changing source detail adjusted value from : ' + sourceDetail.BI_PL_Adjusted_Details__c + '  to ' +  child.BI_PL_Source_adjustment__c );
                                sourceDetail.BI_PL_Adjusted_Details__c = child.BI_PL_Source_adjustment__c;
                                //if (detailsToUpdate.containsKey(sourceDetail.Id)) {
                                //    //TODO : what to do if we visited this detail before?
                                //} else {
                                detailsToUpdate.put(sourceDetail.Id, sourceDetail);
                                //}
                            }
                        }
                    }

                    //if(!modelsMap.containsKey(sourceChannelDetails.BI_PL_Channel__c))
                    model.addTargetChannels(new List<BI_PL_PreparationExt.PlanitChannelDetailModel> {targetChannelDetailModel});
                    System.debug(loggingLevel.Error, '*** targetChannelDetailModel for item: ' + targetChannelDetailModel);


                    modelsToSave.add(model);
                }

                //Be sure to delete the source in case of a transfer
                if (action.BI_PL_Type__c == 'Transfer') {
                    sourceChannelDetails.BI_PL_MSL_Flag__c = false;
                    sourceChannelDetails.BI_PL_Removed__c = true;
                    sourceChannelDetails.BI_PL_Edited__c = true;
                    sourceChannelDetails.BI_PL_Removed_date__c = Datetime.now();
                    sourceChannelDetails.BI_PL_Removed_reason__c = 'Transferred';
                    //channelDetailsToUpdate.add(sourceChannelDetails);
                    channelDetailsToUpdate.put(sourceChannelDetails.Id, sourceChannelDetails);
                }

                /*
                //Final model for the action
                System.debug(loggingLevel.Error, '*** afterUpdate Transfer -> final model: ' + model);
                modelsToSave.add(model);
                */
                //PULL action
            } else if (action.BI_PL_Type__c == 'Request') {

                BI_PL_PreparationExt.PlanitTargetModel model;

                //map to store channel details processed and avoid to create duplicates
                Map<String, BI_PL_PreparationExt.PlanitChannelDetailModel> processedChannelDetails = new Map<String, BI_PL_PreparationExt.PlanitChannelDetailModel>();

                for (BI_PL_Preparation_action_item__c child : itemsByAction.get(action.Id)) {

                    //Create new target model in source preparation
                    if (model == null) {
                        model = new BI_PL_PreparationExt.PlanitTargetModel(new BI_PL_Target_preparation__c(
                                    BI_PL_Header__r = new BI_PL_Preparation__c(BI_PL_External_id__c = child.BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_External_id__c),
                                    BI_PL_Target_customer__r = new Account(External_Id_vod__c = child.BI_PL_Parent__r.BI_PL_Account__r.External_Id_vod__c),
                                    BI_PL_Added_manually__c = true,
                                    BI_PL_External_id__c = null,
                                    BI_PL_Added_reason__c = child.BI_PL_Parent__r.BI_PL_Added_reason__c
                                ));
                    }

                    BI_PL_Channel_detail_preparation__c toClone;
                    List<BI_PL_Channel_detail_preparation__c> targetChannelDetailList = interactionsByPrepAndAccount.containsKey(child.BI_PL_Target_preparation__c) && interactionsByPrepAndAccount.get(child.BI_PL_Target_preparation__c).containsKey(action.BI_PL_Account__c)
                            ? interactionsByPrepAndAccount.get(child.BI_PL_Target_preparation__c).get(action.BI_PL_Account__c) : null;

                    if (targetChannelDetailList == null) break;


                    //Search from channel details in action channel
                    for (BI_PL_Channel_detail_preparation__c sourceDet : targetChannelDetailList) {
                        if (action.BI_PL_Channel__c == sourceDet.BI_PL_Channel__c) {
                            toClone = sourceDet;
                            break;
                        }
                    }

                    //Get working channel-detail model. If we already processed the channel detail, dont create a new one
                    BI_PL_PreparationExt.PlanitChannelDetailModel targetChannelDetailModel;

                    if (processedChannelDetails.containsKey(toClone.BI_PL_Channel__c)) {
                        targetChannelDetailModel = processedChannelDetails.get(toClone.BI_PL_Channel__c);
                    } else {
                        //Clone source channel-detail  (clear keys and relations with target)
                        targetChannelDetailModel = new BI_PL_PreparationExt.PlanitChannelDetailModel(BI_PL_TransferAndShareUtility.cloneChannelDetailWithoutKeys(toClone));
                        targetChannelDetailModel.record.BI_PL_MSL_Flag__c = true;
                        targetChannelDetailModel.record.BI_PL_Removed__c = false;
                        targetChannelDetailModel.record.BI_PL_Edited__c = true;
                        processedChannelDetails.put(toClone.BI_PL_Channel__c, targetChannelDetailModel);
                    }



                    for (BI_PL_Detail_preparation__c sourceDetail : toClone.BI_PL_Details_Preparation__r) {
                        System.debug(loggingLevel.Error, '*** sourceDetail ' + sourceDetail);

                        if (sourceDetail.BI_PL_Product__c == child.BI_PL_Product__c &&
                                (( sourceDetail.BI_PL_Secondary_Product__c == null && child.BI_PL_Secondary_Product__c == null) || (sourceDetail.BI_PL_Secondary_Product__c != null && child.BI_PL_Secondary_Product__c != null && sourceDetail.BI_PL_Secondary_Product__c == child.BI_PL_Secondary_Product__c)) ) {
                            System.debug(loggingLevel.Error, '*** added to  1' + sourceDetail);
                            System.debug(loggingLevel.Error, '*** added to  1 bis' + child);
                            targetChannelDetailModel.addDetail(BI_PL_TransferAndShareUtility.cloneDetailWithoutKeys(sourceDetail));
                            System.debug(loggingLevel.Error, '*** added to  1 bis bis' + targetChannelDetailModel);
                        }
                        //TODO: In case of share, set adjusted details?

                        ////If is a share, clone the detail and adjust the values
                        //if(action.BI_PL_Type__c == 'Share'){
                        //    if(sourceDetail.BI_PL_Product__c == child.BI_PL_Product__c && sourceDetail.BI_PL_Secondary_Product__c == child.BI_PL_Secondary_Product__c) {
                        //        forCurrentItem = BI_PL_TransferAndShareUtility.cloneDetailWithoutKeys(sourceDetail);
                        //        forCurrentItem.BI_PL_Adjusted_Details__c = child.BI_PL_Target_adjustment__c;
                        //        targetChannelDetailModel.addDetail(forCurrentItem);
                        //    }
                        //}
                    }
                    model.addTargetChannels(new List<BI_PL_PreparationExt.PlanitChannelDetailModel> {targetChannelDetailModel});

                    System.debug(loggingLevel.Error, '*** targetChannelDetailModel for item: ' + targetChannelDetailModel);


                    if (child.BI_PL_Request_response__c == 'Transfer') {
                        //add to delete target channel-detail
                        toClone.BI_PL_MSL_Flag__c = false;
                        toClone.BI_PL_Removed__c = true;
                        toClone.BI_PL_Edited__c = true;
                        toClone.BI_PL_Removed_date__c = Datetime.now();
                        toClone.BI_PL_Removed_reason__c = 'Transferred';
                        //channelDetailsToUpdate.add(toClone);
                        channelDetailsToUpdate.put(toClone.Id, toClone);

                    } else if (child.BI_PL_Request_response__c == 'Share') {
                        //TODO : what to do with the target?
                    }

                }

                System.debug(loggingLevel.Error, '*** afterUpdate Request -> final model: ' + model);
                modelsToSave.add(model);

            }
        }


    }

    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(SObject so) {
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        System.debug(loggingLevel.Error, '*** andFinally -> save Models: ' + modelsToSave);
        BI_PL_PreparationExt.saveModels(modelsToSave, null);
        //update channelDetailsToUpdate;
        update channelDetailsToUpdate.values();
        System.debug(loggingLevel.Error, '*** andFinally -> detailsToUpdate: ' + detailsToUpdate);

        update detailsToUpdate.values();
    }

}