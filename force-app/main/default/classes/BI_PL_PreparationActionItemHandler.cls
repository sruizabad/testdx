public without sharing class BI_PL_PreparationActionItemHandler
    implements BI_PL_TriggerInterface {

    private Map<Id, Id> itemIdByUserId = new Map<Id, Id>();
    private List<BI_PL_Preparation_action__Share> parentShares = new List<BI_PL_Preparation_action__Share>();

    // Constructor
    public BI_PL_PreparationActionItemHandler() {
    }

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {
    }
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter() {
    }

    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public void beforeInsert(SObject so) {
        System.debug(loggingLevel.Error, '*** BI_PL_PreparationActionItemHandler -> beforeInsert -->: ' + so);
    }


    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    public void beforeUpdate(SObject oldSo, SObject so) {
        System.debug(loggingLevel.Error, '*** BI_PL_PreparationActionItemHandler -> beforeUpdate --> oldSo : ' + oldSo);
        System.debug(loggingLevel.Error, '*** BI_PL_PreparationActionItemHandler -> beforeUpdate --> so: ' + so);
    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so) {
    }

    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public void afterInsert(SObject so) {
        //We have to give visibility to the parent record
        BI_PL_Preparation_action_item__c item = (BI_PL_Preparation_action_item__c) so;
        parentShares.add(new BI_PL_Preparation_action__Share(AccessLevel = 'Edit', RowCause = 'Manual', ParentId = item.BI_PL_Parent__c, UserOrGroupId = item.BI_PL_Target_preparation_owner__c));

        itemIdByUserId.put(item.BI_PL_Target_preparation_owner__c, item.Id);
    }


    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    public void afterUpdate(SObject oldSo, SObject so) {
    }

    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    public void afterDelete(SObject so) {
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        //insert parentShares;
        Database.insert(parentShares, false);
        // Notify users
        System.debug('AND FINALLY ' +BI_PL_CountrySettingsUtility.isNotifyTransferAndShareEnabled()+ ' - '+ BI_PL_EmailNotificationUtility.generateEmailNotificationIdPairs(itemIdByUserId));
        if (BI_PL_CountrySettingsUtility.isNotifyTransferAndShareEnabled() && itemIdByUserId.size() > 0)
            BI_PL_EmailNotificationUtility.sendEmail(BI_PL_EmailNotificationUtility.TRANSFER_AND_SHARE, BI_PL_EmailNotificationUtility.generateEmailNotificationIdPairs(itemIdByUserId));
}

}