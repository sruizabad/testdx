@isTest
private class BI_PL_PreparationActionTrigger_Test {
	
	@isTest static void test_method_one() {
		// Implement test code

		User user = [SELECT Id, Country_code_BI__c from User where Id = :Userinfo.getUserId()];
		String countryCode = user.Country_code_BI__c;
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();
		BI_PL_TestDataFactory.createCycleStructure(countryCode);
		List<Account> listAcc = BI_PL_TestDataFactory.createTestAccounts(3,countryCode);
		List<Product_vod__c> listProd = BI_PL_TestDataFactory.createTestProduct(3,countryCode);
		List<BI_PL_Position_cycle__c> listpc = [SELECT id, BI_PL_External_id__c FROM BI_PL_Position_cycle__c where Bi_pl_position__r.Name in ('Test4','Test5')];
		List<BI_PL_Preparation__c> listPrep = BI_PL_TestDataFactory.createPreparations(countryCode, listpc, listAcc, listProd);


		List<BI_PL_Preparation_action__c> listPrepAction = new List<BI_PL_Preparation_action__c>();
		List<BI_PL_Preparation_action_item__c> listPrepActionItem = new List<BI_PL_Preparation_action_item__c>();
		//Test Share
		BI_PL_Preparation_action__c prepAction = new BI_PL_Preparation_action__c();
		prepAction.BI_PL_Account__c = listAcc.get(0).id;
		prepAction.BI_PL_Added_reason__c = 'Influencer';
		prepAction.BI_PL_Channel__c = 'rep_detail_only';
		prepAction.BI_PL_Source_preparation__c = listPrep.get(0).id;
		prepAction.BI_PL_Type__c = 'Share';
		listPrepAction.add(prepAction);
		//insert prepAction;

		

		//Test transfer
		BI_PL_Preparation_action__c prepAction3 = new BI_PL_Preparation_action__c();
		prepAction3.BI_PL_Account__c = listAcc.get(1).id;
		prepAction3.BI_PL_Added_reason__c = 'Influencer';
		prepAction3.BI_PL_Channel__c = 'rep_detail_only';
		prepAction3.BI_PL_Source_preparation__c = listPrep.get(0).id;
		prepAction3.BI_PL_Type__c = 'Transfer';
		listPrepAction.add(prepAction3);
		//insert prepAction3;



	

		BI_PL_Preparation_action__c prepAction4 = new BI_PL_Preparation_action__c();
		prepAction4.BI_PL_Account__c = listAcc.get(2).id;
		prepAction4.BI_PL_Added_reason__c = 'Influencer';
		prepAction4.BI_PL_Channel__c = 'rep_detail_only';
		prepAction4.BI_PL_Source_preparation__c = listPrep.get(0).id;
		prepAction4.BI_PL_Type__c = 'Request';
		listPrepAction.add(prepAction4);
		//insert prepAction4;

		insert listPrepAction;

		BI_PL_Preparation_action_item__c prepActionItem = new BI_PL_Preparation_action_item__c();
		prepActionItem.BI_PL_Parent__c = listPrepAction.get(0).id;
		prepActionItem.BI_PL_Target_preparation__c =listPrep.get(1).id;
		prepActionItem.BI_PL_Product__c = listProd.get(0).id;
		prepActionItem.BI_PL_Status__c = 'Approved';
		listPrepActionItem.add(prepActionItem);
		
		BI_PL_Preparation_action_item__c prepActionItem2 = new BI_PL_Preparation_action_item__c();
		prepActionItem2.BI_PL_Parent__c = listPrepAction.get(0).id;
		prepActionItem2.BI_PL_Target_preparation__c =listPrep.get(1).id;
		prepActionItem2.BI_PL_Product__c = listProd.get(1).id;
		prepActionItem2.BI_PL_Status__c = 'Approved';

		listPrepActionItem.add(prepActionItem2);	


		BI_PL_Preparation_action_item__c prepActionItem3 = new BI_PL_Preparation_action_item__c();
		prepActionItem3.BI_PL_Parent__c = listPrepAction.get(1).id;
		prepActionItem3.BI_PL_Target_preparation__c =listPrep.get(1).id;
		prepActionItem3.BI_PL_Product__c = listProd.get(0).id;
		prepActionItem3.BI_PL_Status__c = 'Approved';
	
		listPrepActionItem.add(prepActionItem3);


		BI_PL_Preparation_action_item__c prepActionItem4 = new BI_PL_Preparation_action_item__c();
		prepActionItem4.BI_PL_Parent__c = listPrepAction.get(2).id;
		prepActionItem4.BI_PL_Target_preparation__c =listPrep.get(1).id;
		prepActionItem4.BI_PL_Product__c = listProd.get(2).id;
		prepActionItem4.BI_PL_Status__c = 'Approved';
		prepActionItem4.BI_PL_Request_response__c = 'Transfer';

		listPrepActionItem.add(prepActionItem4);


		insert listPrepActionItem;

		



	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}