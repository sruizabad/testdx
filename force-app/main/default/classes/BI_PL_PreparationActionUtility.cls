public with sharing class BI_PL_PreparationActionUtility {
	public static final String DROP = 'Drop';
	public static final String SHARE = 'Share';
	public static final String REQUEST = 'Request';
	public static final String TRANSFER = 'Transfer';

}