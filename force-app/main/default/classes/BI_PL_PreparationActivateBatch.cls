/**
 *  13/09/2017
 *  - Added secondary product to the query and added in the object PLANiT Detail Preparation.
 *  06/07/2017
 *  - Now the Global Capacity is at Cycle level, so the preparation gets its value from a formula field (no field filling through APEX anymore).
 * Batch class that manages initial PLANiT Preparations activation after loading PLANiT Staging Preparation records
 */
global class BI_PL_PreparationActivateBatch implements Database.Batchable<sObject>, Database.Stateful{

    public String logBody;
    public String logTitle;
    public String cycleId;
    public BI_PL_Country_settings__c countrySettings;

    public BI_PL_PreparationActivateBatch(){
        System.debug('### - BI_PL_PreparationActivateBatch - INI');

        this.logTitle = 'BI_PL_PreparationActivateBatch ' + String.valueOf(Datetime.now());
        this.logBody = 'Errors will show below : \r\n\n';

        this.countrySettings = BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser();
      
    }


    global Database.QueryLocator start(Database.BatchableContext BC){
        String userCountryCode = [SELECT Id, Country_Code_BI__c 
                                    FROM User 
                                        WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;



        return Database.getQueryLocator('SELECT Id, BI_PL_Capacity__c, BI_PL_Column__c, BI_PL_Country_code__c, BI_PL_Target_customer__r.External_ID_vod__c, ' + 
                                                'BI_PL_Detail_preparation_external_id__c, BI_PL_Ntl_value__c, BI_PL_Planned_details__c, BI_PL_Error__c, BI_PL_Error_log__c,' +   
                                                'BI_PL_Preparation_external_id__c, BI_PL_Product__c,BI_PL_Product__r.External_ID_vod__c, BI_PL_Row__c, BI_PL_Channel__c, BI_PL_Mccp_channel_criteria__c,' + 
                                                'BI_PL_Segment__c, BI_PL_Target_preparation_external_id__c, BI_PL_Secondary_Product__c, BI_PL_Secondary_Product__r.External_ID_vod__c,' +
                                                'BI_PL_Position_cycle__r.BI_PL_External_id__c, BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle_start_date__c, BI_PL_Position_cycle__r.BI_PL_Cycle_end_date__c, BI_PL_Position_cycle__r.BI_PL_Position_name__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_Force__r.Name,' +

                                                'BI_PL_Commit_target__c, BI_PL_Cvm__c, BI_PL_Fdc__c, BI_PL_Ims_id__c, BI_PL_Market_target__c, BI_PL_Other_goal__c, BI_PL_Poa_objective__c, BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c,' + 
                                                'BI_PL_Prescribe_target__c, BI_PL_Primary_goal__c, BI_PL_Specialty_code__c, BI_PL_Strategic_segment__c,' +
                                                'BI_PL_Next_best_account__C, BI_PL_No_see_list__c, BI_PL_Target_flag__c, bi_pl_must_win__c, BI_PL_HyperTarget__c, BI_PL_Sub_Segment__c  ' +
                                            ' FROM BI_PL_Staging__c ' + 
                                            ' WHERE BI_PL_Country_code__c = :userCountryCode');
    }
    
    global void execute(Database.BatchableContext BC, List<BI_PL_Staging__c> scope){
        System.debug('### - BI_PL_PreparationActivateBatch - execute - scope = ' + scope);
        
        this.cycleId = scope.get(0).BI_PL_Position_cycle__r.BI_PL_Cycle__c;
        insertPreparationsStructure(scope);
    }
    
    global void finish(Database.BatchableContext BC){
        /**
         * Write logs
         */
        try {
            Document log = new Document();
            log.Body = Blob.valueOf(logBody + '\r\n\n END LOG');
            log.FolderId = UserInfo.getUserId();
            log.Name = logTitle;
            log.Type = 'txt';
            insert log;
        } catch(Exception e) {
            System.debug(loggingLevel.Error, '*** finihs exception on log creation: ' + e.getMessage());
            System.debug(loggingLevel.Error, '*** finihs exception on log creation: ' + e.getStackTraceString());
        }
        
        //Database.executeBatch(new BI_PL_StagingDeleteBatch());


       /* if(this.countrySettings.BI_PL_Search_Accounts_by_FF__c){

            Database.executeBatch(new BI_PL_GenerateSearchHCOAffiliationsBatch(this.cycleId));
        }*/

    }

    /**
     * Method that 
     *
     * @param      stgRecords  SAP Staging Preparation records that will be inserted in the system as 
     *                   - SAP Preparation
     *                   - SAP Target Preparation
     *                   - SAP Target-Channel
     *                   - SAP Detail Preparation
     */
    private void insertPreparationsStructure(list<BI_PL_Staging__c> stgRecords){
        System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - INI');

        Map<String, BI_PL_Staging__c> stagingMap = new Map<String, BI_PL_Staging__c>(); 
        List<stagingRecords> staginWrapperList= new List<stagingRecords>();
        Map<String, BI_PL_Preparation__c> preparations = new Map<String, BI_PL_Preparation__c>();
        Map<String, BI_PL_Target_preparation__c> targetPreparations = new Map<String, BI_PL_Target_preparation__c>();
        Map<String, BI_PL_Channel_detail_preparation__c> targetChannelPreparations = new Map<String, BI_PL_Channel_detail_preparation__c>();
        Map<String,BI_PL_Detail_preparation__c> detailPreparations = new Map<String,BI_PL_Detail_preparation__c>();
        List<BI_PL_Affiliation__c> affiliations = new List<BI_PL_Affiliation__c>();
        String fieldforce;
        List<BI_PL_Staging__c> cleanStaging = new List<BI_PL_Staging__c>();
        for(BI_PL_Staging__c stgRecord : stgRecords){
/*
            String key = stgRecord.BI_PL_Position_cycle__r.BI_PL_External_id__c + ''+ stgRecord.BI_PL_Target_customer__r.External_ID_vod__c + stgRecord.BI_PL_Channel__c + stgRecord.BI_PL_Product__r.External_ID_vod__c;
            if(stgRecord.BI_PL_Secondary_Product__c != null) key = key + stgRecord.BI_PL_Secondary_Product__r.External_ID_vod__c;


            stagingMap.put(key,stgRecord);*/
            if(stgRecord.BI_PL_Error__c){
                stgRecord.BI_PL_Error__c = false;
                stgRecord.BI_PL_Error_log__c = '';
                cleanStaging.add(stgRecord);
            }

            stagingRecords stgWrapper = new stagingRecords(stgRecord);
            stgWrapper.addPositionCycle(stgRecord.BI_PL_Position_cycle__c);

            fieldforce = stgRecord.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c;

            stgRecord.BI_PL_Preparation_external_id__c = BI_PL_PreparationServiceUtility.generatePreparationExternalId(stgRecord.BI_PL_Country_code__c,
                                                                                                    stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_start_date__c, 
                                                                                                    stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_end_date__c, 
                                                                                                    stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_Force__r.Name,
                                                                                                    stgRecord.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                                                                                                   stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c);

            stgRecord.BI_PL_Target_preparation_external_id__c = stgRecord.BI_PL_Preparation_external_id__c+'_'+stgRecord.BI_PL_Target_customer__r.External_ID_vod__c;


            // The preparation has not been analysed yet
            if(preparations.get(stgRecord.BI_PL_Preparation_external_id__c) == null){
                System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - insertPreparation');
                BI_PL_Preparation__c currentRecord = new BI_PL_Preparation__c();


                System.debug(loggingLevel.Error, '*** insertPreparationsStructure - Generated external id for preparation: ' + stgRecord.BI_PL_Preparation_external_id__c);

                currentRecord.BI_PL_External_id__c =  stgRecord.BI_PL_Preparation_external_id__c;

                BI_PL_Position_cycle__c pos = new BI_PL_Position_cycle__c(BI_PL_External_id__c = stgRecord.BI_PL_Position_cycle__r.BI_PL_External_id__c);
                currentRecord.BI_PL_Position_cycle__r = pos;

                //currentRecord.BI_PL_Status__c = stgRecord.BI_PL_Status__c;
                currentRecord.BI_PL_Status__c = 'Under Review';
                if(stgRecord.BI_PL_Capacity__c != null){
                    currentRecord.BI_PL_Total_capacity__c = stgRecord.BI_PL_Capacity__c;
                    currentRecord.BI_PL_Total_capacity_manual__c = true;
                }
                
                currentRecord.BI_PL_Country_code__c = stgRecord.BI_PL_Country_code__c;

                preparations.put(stgRecord.BI_PL_Preparation_external_id__c, currentRecord);
                stgWrapper.addPrep(stgRecord.BI_PL_Preparation_external_id__c);
            }

            // The Target preparation has not been analysed yet
            if(targetPreparations.get(stgRecord.BI_PL_Target_preparation_external_id__c) == null){
                System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - insertTarget');
                BI_PL_Target_preparation__c currentRecord = new BI_PL_Target_preparation__c();
                //generate target external id
                System.debug(loggingLevel.Error, '*** insertPreparationsStructure - Generated external id for target: ' + stgRecord.BI_PL_Target_preparation_external_id__c);

                currentRecord.BI_PL_External_id__c = stgRecord.BI_PL_Target_preparation_external_id__c;

                BI_PL_Preparation__c prep = new BI_PL_Preparation__c(BI_PL_External_id__c = stgRecord.BI_PL_Preparation_external_id__c);
                currentRecord.BI_PL_Header__r = prep;

                Account acc = new Account(External_ID_vod__c = stgRecord.BI_PL_Target_customer__r.External_ID_vod__c);
                currentRecord.BI_PL_Target_customer__r = acc;

                currentRecord.BI_PL_NTL_value__c = stgRecord.BI_PL_Ntl_value__c;

                currentRecord.BI_PL_Next_best_account__c = stgRecord.BI_PL_Next_best_account__c;
                currentRecord.BI_PL_No_see_list__c = stgRecord.BI_PL_No_see_list__c;
                currentRecord.BI_PL_Must_win__c = stgRecord.BI_PL_Must_win__c;
                currentRecord.BI_PL_HyperTarget__c = stgRecord.BI_PL_HyperTarget__c;
               


                targetPreparations.put(stgRecord.BI_PL_Target_preparation_external_id__c, currentRecord);
                stgWrapper.addTarget(stgRecord.BI_PL_Target_preparation_external_id__c);
            }

            
            // The Target-Channel preparation has not been analysed yet
            if(targetChannelPreparations.get(stgRecord.BI_PL_Target_preparation_external_id__c +'_'+ stgRecord.BI_PL_Channel__c) == null){
                System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - insert targetChannel');
                BI_PL_Channel_detail_preparation__c currentRecord = new BI_PL_Channel_detail_preparation__c();
                currentRecord.BI_PL_External_id__c = stgRecord.BI_PL_Target_preparation_external_id__c +'_'+stgRecord.BI_PL_Channel__c;

                BI_PL_Target_preparation__c tar = new BI_PL_Target_preparation__c(BI_PL_External_id__c = stgRecord.BI_PL_Target_preparation_external_id__c);
                currentRecord.BI_PL_Target__r = tar;
                currentRecord.BI_PL_MSL_flag__c = stgRecord.BI_PL_Target_flag__c;
                currentRecord.BI_PL_Channel__c = stgRecord.BI_PL_Channel__c;
                currentRecord.BI_PL_Mccp_channel_criteria__c = stgRecord.BI_PL_Mccp_channel_criteria__c;
                targetChannelPreparations.put(stgRecord.BI_PL_Target_preparation_external_id__c +'_'+ stgRecord.BI_PL_Channel__c, currentRecord);
                stgWrapper.addChannel(stgRecord.BI_PL_Target_preparation_external_id__c +'_'+ stgRecord.BI_PL_Channel__c);
            }


            if(stgRecord.BI_PL_Product__c != null)
            {
            
                System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - insertDetail');
                BI_PL_Detail_preparation__c currentRecord = new BI_PL_Detail_preparation__c();

                //generate external id for detail
               
                if(stgRecord.BI_PL_Secondary_Product__c != null){
                    String secProdExId = stgRecord.BI_PL_Secondary_Product__r.External_ID_vod__c;
                    stgRecord.BI_PL_Detail_preparation_external_id__c = stgRecord.BI_PL_Target_preparation_external_id__c +'_'+stgRecord.BI_PL_Channel__c+'_'+stgRecord.BI_PL_Product__r.External_ID_vod__c+secProdExId;
                    Product_vod__c secProd = new Product_vod__c(External_ID_vod__c = stgRecord.BI_PL_Secondary_Product__r.External_ID_vod__c);
                    currentRecord.BI_PL_Secondary_product__r = secProd;
                }else{
                    stgRecord.BI_PL_Detail_preparation_external_id__c = stgRecord.BI_PL_Target_preparation_external_id__c +'_'+stgRecord.BI_PL_Channel__c+'_'+stgRecord.BI_PL_Product__r.External_ID_vod__c;
                }

                
                currentRecord.BI_PL_External_id__c = stgRecord.BI_PL_Detail_preparation_external_id__c;
                System.debug('//**// detail external id: ' + currentRecord.BI_PL_External_id__c);
                BI_PL_Channel_detail_preparation__c tarChan = new BI_PL_Channel_detail_preparation__c(BI_PL_External_id__c = stgRecord.BI_PL_Target_preparation_external_id__c +'_'+ stgRecord.BI_PL_Channel__c);
                currentRecord.BI_PL_Channel_detail__r = tarChan;

                Product_vod__c prod = new Product_vod__c(External_ID_vod__c = stgRecord.BI_PL_Product__r.External_ID_vod__c);
                currentRecord.BI_PL_Product__r = prod;
              
                currentRecord.BI_PL_Adjusted_details__c = stgRecord.BI_PL_Planned_details__c;
                currentRecord.BI_PL_Planned_details__c = stgRecord.BI_PL_Planned_details__c;
                currentRecord.BI_PL_Column__c = stgRecord.BI_PL_Column__c;
                currentRecord.BI_PL_Row__c = stgRecord.BI_PL_Row__c;
                currentRecord.BI_PL_Segment__c = stgRecord.BI_PL_Segment__c;
                currentRecord.BI_PL_Sub_Segment__c = stgRecord.BI_PL_Sub_Segment__c;

                // US fields
                currentRecord.BI_PL_Commit_target__c = stgRecord.BI_PL_Commit_target__c;
                currentRecord.BI_PL_Cvm__c = stgRecord.BI_PL_Cvm__c;
                currentRecord.BI_PL_Fdc__c = stgRecord.BI_PL_Fdc__c;
                currentRecord.BI_PL_Ims_id__c = stgRecord.BI_PL_Ims_id__c;
                currentRecord.BI_PL_Market_target__c = stgRecord.BI_PL_Market_target__c;
                currentRecord.BI_PL_Other_goal__c = stgRecord.BI_PL_Other_goal__c;
                currentRecord.BI_PL_Poa_objective__c = stgRecord.BI_PL_Poa_objective__c;
                currentRecord.BI_PL_Prescribe_target__c = stgRecord.BI_PL_Prescribe_target__c;
                currentRecord.BI_PL_Primary_goal__c = stgRecord.BI_PL_Primary_goal__c;
                currentRecord.BI_PL_Specialty_code__c = stgRecord.BI_PL_Specialty_code__c;
                currentRecord.BI_PL_Strategic_segment__c = stgRecord.BI_PL_Strategic_segment__c;

                detailPreparations.put(currentRecord.BI_PL_External_id__c,currentRecord);
                stgWrapper.addDetail(currentRecord.BI_PL_External_id__c);
                

            }
            System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - preparations = ' + preparations);
            System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - targetPreparations = ' + targetPreparations);
            System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - targetChannelPreparations = ' + targetChannelPreparations);
            System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - detailPreparations = ' + detailPreparations);

            
            staginWrapperList.add(stgWrapper);
            
            
            
        }

        update cleanStaging;

        if(!preparations.isEmpty()){
            List<BI_PL_Preparation__c> prepList = preparations.values();
            Database.UpsertResult[] results = Database.upsert(prepList, BI_PL_Preparation__c.Fields.BI_PL_External_id__c,false);
            System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - Preparations upserted');
            Integer size = results.size();
            for(Integer index = 0;  index < size; index++){
                if(!results[index].isSuccess()){
                    // Operation failed, so get all errors
                    List<stagingRecords> stgR = findStagingWrapper(staginWrapperList, prepList[index].BI_PL_External_id__c, 1);
                    for(Database.Error err : results[index].getErrors()) {
                        //System.debug(LoggingLevel.ERROR, '### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - Preparations error: ' + err.getMessage());
                        this.logBody += '### Preparations error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                        String message = '### Preparations error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                        for(stagingRecords s : stgR){                            
                            if(s.checkMessageEmpty()){                                
                                s.changeErrorMessage(message);
                            }else{
                                 s.addErrorMessage(message);                            
                            }
                            s.recordChanged();
                        }

                    }
                }
            }
        }

        if(!targetPreparations.isEmpty()){
            List<BI_PL_Target_preparation__c> targetsList = targetPreparations.values();
            Database.UpsertResult[] results = Database.upsert(targetsList, BI_PL_Target_preparation__c.Fields.BI_PL_External_id__c,false);
            System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - Target Preparations upserted');
            Integer size = results.size();
            for(Integer index = 0;  index < size; index++){
                if(!results[index].isSuccess()){
                    List<stagingRecords> stgR = findStagingWrapper(staginWrapperList, targetsList[index].BI_PL_External_id__c, 2);
                    for(Database.Error err : results[index].getErrors()) {
                        //System.debug(LoggingLevel.ERROR, '### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - Targets error: ' + err.getMessage());
                        this.logBody += '### Targets error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                        String message = '### Targets error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                        for(stagingRecords s : stgR){                            
                            if(s.checkMessageEmpty()){                                
                                s.changeErrorMessage(message);
                            }else{
                                 s.addErrorMessage(message);                            
                            }
                            s.recordChanged();
                        }
                    }
                }
            }
        }

        if(!targetChannelPreparations.isEmpty()){
            List<BI_PL_Channel_detail_preparation__c> chList = targetChannelPreparations.values();
            Database.UpsertResult[] results = Database.upsert(chList, BI_PL_Channel_detail_preparation__c.Fields.BI_PL_External_id__c, false);
            System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - Target-Channel Preparations upserted');
            Integer size = results.size();
            for(Integer index = 0;  index < size; index++){
                if(!results[index].isSuccess()){
                    List<stagingRecords> stgR = findStagingWrapper(staginWrapperList, chList[index].BI_PL_External_id__c, 3);
                    for(Database.Error err : results[index].getErrors()) {
                        //System.debug(LoggingLevel.ERROR, '### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - Channel-detail error: ' + err.getMessage());
                        this.logBody += '### Channel-detail error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                        String message = '### Channel-detail error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                        for(stagingRecords s : stgR){                            
                            if(s.checkMessageEmpty()){                                
                                s.changeErrorMessage(message);
                            }else{
                                 s.addErrorMessage(message);                            
                            }
                            s.recordChanged();
                        }
                    }
                }
            }
        }

        if(!detailPreparations.isEmpty()){
            List<BI_PL_Detail_preparation__c> detailList = detailPreparations.values();
            Database.UpsertResult[] results = Database.upsert(detailList, BI_PL_Detail_preparation__c.Fields.BI_PL_External_id__c, false);
            System.debug('### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - Detail Preparations upserted');
            Integer size = results.size();
            for(Integer index = 0;  index < size; index++){
                if(!results[index].isSuccess()){
                    List<stagingRecords> stgR = findStagingWrapper(staginWrapperList, detailList[index].BI_PL_External_id__c, 4);                
                    for(Database.Error err : results[index].getErrors()) {
                        //System.debug(LoggingLevel.ERROR, '### - BI_PL_PreparationActivateBatch - insertPreparationsStructure - Detail error: ' + err.getMessage());
                        this.logBody += '### Detail error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                        String message = '### Detail error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                        for(stagingRecords s : stgR){                           
                            if(s.checkMessageEmpty()){
                                
                                s.changeErrorMessage(message);
                            }else{
                                 s.addErrorMessage(message);                            
                            }
                            s.recordChanged();
                        }
                    }
                }
            }
        }

        List<BI_PL_Staging__c> stgToUpdate = new List<BI_PL_Staging__c>();
        for(stagingRecords s : staginWrapperList){
            if(s.checkRecordChange()){
               
                stgToUpdate.add(s.stgRecord());
            }
        }

        update stgToUpdate;
        
    }

    private class stagingRecords {
        BI_PL_Staging__c stgRecord;
        Id pcRecord;
        String preparationId;
        String targetId;
        String channelId;
        String detailId;
        Boolean change = false;

        public stagingRecords(BI_PL_Staging__c stg){
            this.stgRecord = stg;

        }

        public BI_PL_Staging__c stgRecord(){
            return this.stgRecord;
        }
        public void addPositionCycle(Id pc){
            this.pcRecord = pc;
        }

        public void addPrep(String prep){
            this.preparationId = prep;
        }

        public void addTarget(String tgt){
            this.targetId = tgt;
        }

        public void addDetail(String detail){
            this.detailId = detail;
        }

        public void addChannel(String ch){
            this.channelId = ch;
        }

        public void changeErrorMessage(String message){
            this.stgRecord.BI_PL_Error_log__c = message;
            System.debug('***Message: ' + this.stgRecord.BI_PL_Error_log__c);
        }

        public void addErrorMessage(String message){
            this.stgRecord.BI_PL_Error_log__c += message;

            System.debug('***Message: ' + this.stgRecord.BI_PL_Error_log__c);
        }

        public Boolean checkMessageEmpty(){
            if(this.stgRecord.BI_PL_Error_log__c == null){
                return true;
            }else{
                return false;
            }
          
        }

        public String getPreparationId(){
            return this.preparationId;
        }
        public String getTargetId(){
            return this.targetId;
        }
        public String getChannelId(){
            return this.channelId;
        }
        public String getDetailId(){
            return this.detailId;
        }

        public void recordChanged(){
            this.stgRecord.BI_PL_Error__c = true;
            this.change = true;
        }
        public Boolean checkRecordChange(){
            return this.change;
        }

    }

    private List<stagingRecords> findStagingWrapper(List<stagingRecords> listStgRecords, String externalId, Integer numberType){

        List<stagingRecords> stgToReturn = new List<stagingRecords>();
        //System.debug('*/*/*findStagingWrapper exId: ' + externalId);
        for(stagingRecords stgRecord : listStgRecords){
            if(numberType == 1){
                if(stgRecord.getPreparationId() == externalId){
                    stgToReturn.add(stgRecord);
                }
            }else if(numberType == 2){
                if(stgRecord.getTargetId() == externalId){
                    stgToReturn.add(stgRecord);
                }

            }else if(numberType == 3){
                if(stgRecord.getChannelId() == externalId){
                    stgToReturn.add(stgRecord);
                }
            }else if(numberType == 4){

                if(stgRecord.getDetailId() == externalId){
                    stgToReturn.add(stgRecord);
                }
            }
        }

        return stgToReturn;
    }

}