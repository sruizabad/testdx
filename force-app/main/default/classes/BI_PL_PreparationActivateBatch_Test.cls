@isTest
private class BI_PL_PreparationActivateBatch_Test {

	private static final String SCHANNEL = BI_PL_TestDataFactory.SCHANNEL;
	private static String countryCodeUser;

	
	@testSetUp static void setUp() {
		//THIS?
		
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();
		BI_PL_Country_settings__c countrySettings = BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser();
		countrySettings.BI_PL_AMA_Products__c = 'Pradaxa_test1ExtID;Spiriva_test1ExtId';
        countrySettings.BI_PL_Search_Accounts_by_FF__c = true;
        update countrySettings;
		countryCodeUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;

		//Accounts
		List<Account> accs = new List<Account> {
			new Account(Name = 'House', External_Id_vod__c = 'House_Ext_Id'),
			new Account(Name = 'Critiano Ronaldo', External_Id_vod__c = 'CR_Ext_Id'),
			new Account(Name = 'Fernando Suarez', External_Id_vod__c = 'FSUA_Ext_Id'),
			new Account(Name = 'Fernando Salazar', External_Id_vod__c = 'FSAL_Ext_Id'),
			new Account(Name = 'Cabueñes', External_Id_vod__c = 'Caba_Ext_Id'),
			new Account(Name = 'Carlos Tévez', External_Id_vod__c = 'Carlos_Ext_Id')
		};

		insert accs;

		//Products
		List<Product_vod__c> prods = new List<Product_vod__c> {
			new Product_vod__c(Name = 'Pradaxa', External_Id_vod__c = 'Pradaxa_test1ExtID', RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(), Product_Type_vod__c = 'Detail'),
			new Product_vod__c(Name = 'Spiriva', External_Id_vod__c = 'Spiriva_test1ExtId',RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(),Product_Type_vod__c = 'Detail')
		};
		insert prods;

		//Countries and Field forces
		
		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'KAM', BI_TM_Country_Code__c = countryCodeUser);
        insert fieldForce;

		//Cycles
		List<BI_PL_Cycle__c> cycles = new List<BI_PL_Cycle__c> {
			new BI_PL_Cycle__c(
	        	BI_PL_Country_code__c = countryCodeUser,
	        	BI_PL_Start_date__c = Date.newInstance(2018, 1, 1),
	        	BI_PL_End_date__c = Date.newInstance(2018, 1, 31),
	        	BI_PL_External_id__c = 'BR_20180101_20180131_KAM',
	        	BI_PL_Field_force__c = fieldForce.Id
	        ),
	        new BI_PL_Cycle__c(
	        	BI_PL_Country_code__c = countryCodeUser,
	        	BI_PL_Start_date__c = Date.newInstance(2018, 2, 2),
	        	BI_PL_End_date__c = Date.newInstance(2018, 2, 28),
	        	BI_PL_External_id__c = 'BR_20180202_20180228_KAM',
	        	BI_PL_Field_force__c = fieldForce.Id
	        )
		};
		insert cycles;

		//Positions
		BI_PL_Position__c position = new BI_PL_Position__c(
        	Name = 'SSP-000003',
        	BI_PL_Country_code__c = countryCodeUser,
        	BI_PL_Field_force__c = 'TestFieldForce'
        );

        insert position;

		//Position cycles
		List<BI_PL_Position_cycle__c> posCycles = new List<BI_PL_Position_cycle__c>{
			new BI_PL_Position_cycle__c(
	        	BI_PL_Cycle__c = cycles.get(0).Id,
	        	BI_PL_External_id__c = cycles.get(0).BI_PL_External_id__c + '_testHierarchy_SSP-000003',
	        	BI_PL_Hierarchy__c = 'testHierarchy',
	        	BI_PL_Position__c = position.Id
	        ),
	        new BI_PL_Position_cycle__c(
	        	BI_PL_Cycle__c = cycles.get(1).Id,
	        	BI_PL_External_id__c = cycles.get(1).BI_PL_External_id__c + '_testHierarchy_SSP-000003',
	        	BI_PL_Hierarchy__c = 'testHierarchy',
	        	BI_PL_Position__c = position.Id
	        )
		};
		insert posCycles;

		//Business rules
		List<BI_PL_Business_rule__c> brs = new list<BI_PL_Business_rule__c>{
			new BI_PL_Business_rule__c(
				BI_PL_Country_code__c = countryCodeUser
				//BI_PL_Global_workload__c = 300
			)
		};
		insert brs;
	}

	static void getUserCode(){
		countryCodeUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
	}

	static void createStagingRecords() {
		getUserCode();

		List<BI_PL_Staging__c> staging = new List<BI_PL_Staging__c> {
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='Caba_Ext_Id'),
				BI_PL_Planned_details__c =2,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Pradaxa_test1ExtID'),
				BI_PL_Secondary_Product__r = new Product_vod__c(External_id_vod__c ='Spiriva_test1ExtId'), 
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180101_20180131_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 10,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323798',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'Build',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 2',
				BI_PL_Sub_segment__c = 'sub seg 2'
			),

			new BI_PL_Staging__c(
				BI_PL_Country_code__c =countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='CR_Ext_Id'),
				BI_PL_Planned_details__c =3,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Spiriva_test1ExtId'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180101_20180131_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 20,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323799',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'No Segmentation',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 3',
				BI_PL_Sub_segment__c = 'sub seg 3'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='FSAL_Ext_Id'),
				BI_PL_Planned_details__c =4,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Pradaxa_test1ExtID'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180101_20180131_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 30,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323800',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'Gain',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 4',
				BI_PL_Sub_segment__c = 'sub seg 4'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='FSUA_Ext_Id'),
				BI_PL_Planned_details__c =5,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Pradaxa_test1ExtID'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180101_20180131_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 40,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323801',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'Build',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 5',
				BI_PL_Sub_segment__c = 'sub seg 5'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='Carlos_Ext_Id'),
				BI_PL_Planned_details__c =3,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Pradaxa_test1ExtID'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180101_20180131_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 50,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323802',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'Gain',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 6',
				BI_PL_Sub_segment__c = 'sub seg 6'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='House_Ext_Id'),
				BI_PL_Planned_details__c =2,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Spiriva_test1ExtId'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180101_20180131_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 60,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323803',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'Build',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 7',
				BI_PL_Sub_segment__c = 'sub seg 7'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='Caba_Ext_Id'),
				BI_PL_Planned_details__c =1,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Pradaxa_test1ExtID'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180101_20180131_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 70,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323804',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'Gain',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 8',
				BI_PL_Sub_segment__c = 'sub seg 8'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='Caba_Ext_Id'),
				BI_PL_Planned_details__c =1,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Spiriva_test1ExtId'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180202_20180228_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 80,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323805',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'Gain',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 9',
				BI_PL_Sub_segment__c = 'sub seg 9'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='CR_Ext_Id'),
				BI_PL_Planned_details__c =2,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Pradaxa_test1ExtID'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180202_20180228_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 90,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323806',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'Build',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 10',
				BI_PL_Sub_segment__c = 'sub seg 10'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='FSAL_Ext_Id'),
				BI_PL_Planned_details__c =4,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Pradaxa_test1ExtID'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180202_20180228_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 100,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323807',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'No Segmentation',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 11',
				BI_PL_Sub_segment__c = 'sub seg 11'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='Caba_Ext_Id'),
				BI_PL_Planned_details__c =5,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Spiriva_test1ExtId'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180202_20180228_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 110,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323808',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'Gain',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 12',
				BI_PL_Sub_segment__c = 'sub seg 12'
			),
			new BI_PL_Staging__c(
				BI_PL_Country_code__c = countryCodeUser,
				BI_PL_Channel__c = SCHANNEL,
				BI_PL_Target_customer__r = new Account(External_id_vod__c='CR_Ext_Id'),
				BI_PL_Planned_details__c =6,
				BI_PL_Product__r =  new Product_vod__c(External_id_vod__c='Pradaxa_test1ExtID'),
				BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180202_20180228_KAM_testHierarchy_SSP-000003'),
				BI_PL_Status__c = 'Under Review',
				BI_PL_Capacity__c = 120,
				BI_PL_Mccp_Channel_criteria__c = '',
				BI_PL_Column__c = 1,
				BI_PL_Commit_target__c = False,
				BI_PL_CVM__c = 3,
				BI_PL_Fdc__c = False,
				BI_PL_IMS_Id__c = 'INS00323809',
				BI_PL_Market_target__c = False,
				BI_PL_NTL_value__c = 2,
				BI_PL_Other_goal__c = 3,
				BI_PL_POA_Objective__c = 3,
				BI_PL_Prescribe_target__c = false,
				BI_PL_Primary_goal__c = 2,
				BI_PL_Row__c = 2,
				BI_PL_Segment__C = 'No Segmentation',
				BI_PL_Specialty_code__c = 'PCC',
				BI_PL_Strategic_segment__c = 'Gain 13',
				BI_PL_Sub_segment__c = 'sub seg 13'
			)

		};

		upsert staging;
	}
    
	@isTest static void testBatchResults() {
		List<BI_PL_Position_cycle__c> posc = new List<BI_PL_Position_cycle__c>([SELECT id , BI_PL_external_id__c from BI_PL_Position_cycle__c]);
		System.debug(loggingLevel.Error, '*** posc: ' + posc);

		// Insert some staging records
		createStagingRecords();
		/*Profile p = [SELECT Id FROM Profile WHERE Name LIKE 'BR_%' LIMIT 1];
		String name = 'testUserPreparationActiveBatch';
		User u = new User(Alias = name.substring(0, 5), Email = name + '@testorg.com',
                        EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US', Country_Code_BI__c = 'BR',
                        LocaleSidKey = 'en_US', ProfileId = p.id, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles');
		*/
	
			Test.startTest();
			BI_PL_PreparationActivateBatch pab = new BI_PL_PreparationActivateBatch();
	    	Database.executeBatch(pab);
			Test.stopTest();
		
	}
	
	@isTest static void testBatch_withError() {

		getUserCode();

		// 1. Create Producto_vod__c without External_Id_vod__c
		List<Product_vod__c> lstProds = new List<Product_vod__c> {
			new Product_vod__c(Name = 'Pradaxa', /*External_Id_vod__c = 'Pradaxa_test1ExtID', */RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(), Product_Type_vod__c = 'Detail'),
			new Product_vod__c(Name = 'Spiriva', /*External_Id_vod__c = 'Spiriva_test1ExtId',*/RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(),Product_Type_vod__c = 'Detail')
		};
		insert lstProds;

		//2.Create Staging with lookup to products
		List<BI_PL_Staging__c> lstStaging = new List<BI_PL_Staging__c>();
		for (integer i = 0; i < lstProds.size(); i++){
			lstStaging.add(
				new BI_PL_Staging__c(
					BI_PL_Country_code__c = countryCodeUser,
					BI_PL_Channel__c = SCHANNEL,
					BI_PL_Target_customer__r = new Account(External_id_vod__c='CR_Ext_Id'),
					BI_PL_Planned_details__c =6,
					BI_PL_Product__c =  lstProds.get(i).Id,
					BI_PL_Position_cycle__r = new BI_PL_Position_cycle__c(BI_PL_External_id__c = 'BR_20180202_20180228_KAM_testHierarchy_SSP-000003'),
					BI_PL_Status__c = 'Under Review',
					BI_PL_Capacity__c = 120,
					BI_PL_Mccp_Channel_criteria__c = '',
					BI_PL_Column__c = 1,
					BI_PL_Commit_target__c = False,
					BI_PL_CVM__c = 3,
					BI_PL_Fdc__c = false,
					BI_PL_IMS_Id__c = 'INS0032380' + i,
					BI_PL_Market_target__c = false,
					BI_PL_NTL_value__c = 2,
					BI_PL_Other_goal__c = 3,
					BI_PL_POA_Objective__c = 3,
					BI_PL_Prescribe_target__c = false,
					BI_PL_Primary_goal__c = 2,
					BI_PL_Row__c = 2,
					BI_PL_Segment__C = 'No Segmentation',
					BI_PL_Specialty_code__c = 'PCC',
					BI_PL_Strategic_segment__c = 'Gain ' + i,
					BI_PL_Sub_segment__c = 'sub seg ' + i
				)
			);
		}

		insert lstStaging;

		Test.startTest();
			BI_PL_PreparationActivateBatch pab = new BI_PL_PreparationActivateBatch();
    		Database.executeBatch(pab);
		Test.stopTest();

		//Check every Staging has an error
		for (BI_PL_Staging__c inStage : [SELECT Id, BI_PL_Product__c, BI_PL_Error_log__c FROM BI_PL_Staging__c] ){
			System.Assert(String.isNotBlank(inStage.BI_PL_Error_log__c) );
			System.Assert (inStage.BI_PL_Error_log__c.contains('External_ID_vod__c not specified'));
		}
	}


}