/**
 *  07/09/2017
 *  - Change Removed field from target to channel
 */
global class BI_PL_PreparationApproveAdjBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global BI_PL_PreparationApproveAdjBatch(String countryCode, String cycleId, String hierarchyName) {
		query = 'SELECT Id, BI_PL_Reviewed__c '+
			'FROM BI_PL_Channel_detail_preparation__c '+
			'WHERE BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = \''+cycleId+'\' '+ 
			'AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = \''+hierarchyName+'\' AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Country_code__c =  \''+countryCode+'\' '+ 
			'AND (BI_PL_Edited__c = true OR BI_PL_Target__r.BI_PL_Added_manually__c = true OR BI_PL_Removed__c = true)';
		System.debug(loggingLevel.Error, '*** query: ' + query);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		for(sObject chdet : scope) {
			chdet.put('BI_PL_Reviewed__c', true);
		}
		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}