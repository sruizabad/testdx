global class BI_PL_PreparationChangeStatusBatch implements Database.Batchable<sObject>, Database.Stateful {
	
	String query;
	String newStatus;
	
	global BI_PL_PreparationChangeStatusBatch(String countryCode, String cycleId, String hierarchyName, String targetStatus) {
		System.debug(loggingLevel.Error, '*** countryCode: ' + countryCode);
		System.debug(loggingLevel.Error, '*** cycleId: ' + cycleId);
		System.debug(loggingLevel.Error, '*** hierarchyName: ' + hierarchyName);
		System.debug(loggingLevel.Error, '*** targetStatus: ' + targetStatus);
		query = 'SELECT Id, BI_PL_Status__c FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__r.BI_PL_Cycle__c = \''+cycleId+'\' AND BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = \''+hierarchyName+'\' AND BI_PL_Country_code__c = \''+countryCode+'\'';
		newStatus = targetStatus;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		for(sObject prep : scope) {
			prep.put('BI_PL_Status__c', newStatus);
   		}
   		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}