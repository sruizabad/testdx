@isTest
private class BI_PL_PreparationChangeStatusBatch_Test {
	
	private static final Integer MANUAL_TOTAL_CAPACITY = 10;
	private static final Integer CAPACITY_ADJUSTMENT = 4;
	private static final Integer GLOBAL_CAPACITY = 10;


	@isTest static void test_method_one() {
		// Implement test code
		String hierarchy = 'testHierarchy';

		BI_PL_Business_rule__c br = new BI_PL_Business_rule__c(BI_PL_Visits_per_day__c = 20,
		        BI_PL_Country_code__c = 'BR',
		        BI_PL_Type__c = BI_PL_PreparationUtility.THRESHOLD_RULE_TYPE,
		        BI_PL_Active__c = true);

		insert br;

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'KAM', BI_TM_Country_Code__c = 'BR');
		insert fieldForce;

		BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'BR',
		        BI_PL_Global_capacity__c = GLOBAL_CAPACITY,
		        BI_PL_Start_date__c = Date.newInstance(2018, 1, 1),
		        BI_PL_End_date__c = Date.newInstance(2018, 1, 31),
		        BI_PL_Field_force__c = fieldForce.Id);

		insert cycle;

		BI_PL_Position__c position1 = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = 'BR');
		BI_PL_Position__c position2 = new BI_PL_Position__c(Name = 'Test2', BI_PL_Country_code__c = 'BR');
		BI_PL_Position__c position3 = new BI_PL_Position__c(Name = 'Test3', BI_PL_Country_code__c = 'BR');

		insert new List<BI_PL_Position__c> {position1, position2, position3};

		BI_PL_Position_cycle__c positionCycle1 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position1.Id,
		        BI_PL_External_id__c = 'test1', BI_PL_Hierarchy__c = hierarchy);
		BI_PL_Position_cycle__c positionCycle2 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position2.Id,
		        BI_PL_External_id__c = 'test2', BI_PL_Hierarchy__c = hierarchy);
		BI_PL_Position_cycle__c positionCycle3 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position3.Id,
		        BI_PL_External_id__c = 'test3', BI_PL_Hierarchy__c = hierarchy);

		insert new List<BI_PL_Position_cycle__c> {positionCycle1, positionCycle2, positionCycle3};

		BI_PL_Preparation__c preparation1 = new BI_PL_Preparation__c(
		    BI_PL_Country_code__c = 'BR',
		    BI_PL_External_id__c = 'Test',
		    BI_PL_Position_cycle__c = positionCycle1.Id,
		    BI_PL_Capacity_adjustment__c = CAPACITY_ADJUSTMENT,
		    BI_PL_Total_capacity__c = MANUAL_TOTAL_CAPACITY,
		    BI_PL_Total_capacity_manual__c = false,
		    BI_PL_Capacity_adjustment_approved__c = false,
		    BI_PL_Status__c = 'Under Review');

		BI_PL_Preparation__c preparation2 = new BI_PL_Preparation__c(
		    BI_PL_Country_code__c = 'BR',
		    BI_PL_External_id__c = 'Test2',
		    BI_PL_Position_cycle__c = positionCycle2.Id,
		    BI_PL_Capacity_adjustment__c = CAPACITY_ADJUSTMENT,
		    BI_PL_Total_capacity__c = MANUAL_TOTAL_CAPACITY,
		    BI_PL_Total_capacity_manual__c = false,
		    BI_PL_Capacity_adjustment_approved__c = true,
		    BI_PL_Status__c = 'Under Review');

		BI_PL_Preparation__c preparation3 = new BI_PL_Preparation__c(
		    BI_PL_Country_code__c = 'BR',
		    BI_PL_External_id__c = 'Test3',
		    BI_PL_Capacity_adjustment_approved__c = false,
		    BI_PL_Position_cycle__c = positionCycle3.Id,
		    BI_PL_Capacity_adjustment__c = CAPACITY_ADJUSTMENT,
		    BI_PL_Total_capacity__c = MANUAL_TOTAL_CAPACITY,
		    BI_PL_Total_capacity_manual__c = true,
		    BI_PL_Status__c = 'Under Review');

		insert new List<BI_PL_Preparation__c> {preparation1, preparation2, preparation3};

		Test.startTest();

		Database.executeBatch(new BI_PL_PreparationChangeStatusBatch('BR', cycle.id, hierarchy, 'Approved'));

		Test.stopTest();
		for(BI_PL_Preparation__c prep : [SELECT id, BI_PL_Status__c FROM BI_PL_Preparation__c]){

			System.assertEquals('Approved',prep.BI_PL_Status__c);
		}

	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}