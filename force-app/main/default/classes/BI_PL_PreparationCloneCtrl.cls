/**
 *	08/09/2017
 *	- Source cycle and target cycles now are not restricted by dates.
 *	- Now the component uses Vue.js.
 *	16-06-2016
 *	@author	OMEGA CRM
 */
public without sharing class BI_PL_PreparationCloneCtrl {

	@RemoteAction
	public static List<BI_PL_Preparation__c> getPreparations(String selectedHierarchy, String selectedCycle) {
		User currentUser = [SELECT Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

		return [SELECT Id, Name, BI_PL_Country_code__c, BI_PL_Position_name__c, BI_PL_Status__c, BI_PL_Start_date__c, BI_PL_End_date__c, OwnerId,
		        BI_PL_position_cycle__r.BI_PL_hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name,
		        BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
		        BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c, BI_PL_Position_cycle__r.BI_PL_Synchronized__c, Owner.Name
		        FROM BI_PL_Preparation__c
		        WHERE BI_PL_Country_code__c = : currentUser.Country_Code_BI__c
		                                      AND BI_PL_position_cycle__r.BI_PL_hierarchy__c = : selectedHierarchy
		                                              AND BI_PL_position_cycle__r.BI_PL_Cycle__c = : selectedCycle];
	}

	public class PositionCycleWrapper {
		public String PositionCycleExternalId;
		public String PositionName;

		public PositionCycleWrapper (String PositionCycleExternalId, String PositionName) {
			this.PositionCycleExternalId = PositionCycleExternalId;
			this.PositionName = PositionName;
		}
	}

	@RemoteAction
	public static Map<String, PositionCycleWrapper> getTargetPositionCycles(String selectedHierarchy, String selectedCycle) {
		Map<String, PositionCycleWrapper> output = new Map<String, PositionCycleWrapper>();


		Set<String> positionsInUse = new Set<String>();

		// 1.- Retrieve the position names that are in use by the preparations:
		for (BI_PL_Preparation__c p : [SELECT BI_PL_Position_cycle__r.BI_PL_Position_name__c FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = : selectedHierarchy AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = : selectedCycle]) {
			positionsInUse.add(p.BI_PL_Position_cycle__r.BI_PL_Position_name__c);
		}

		// 2.- Get all the positions in the hierarchy and exclude the ones previously found:
		for (BI_PL_position_cycle__c pc : [SELECT BI_PL_External_id__c, BI_PL_Position_name__c
		                                   FROM BI_PL_Position_cycle__c
		                                   WHERE BI_PL_hierarchy__c = : selectedHierarchy
		                                           AND BI_PL_Cycle__c = : selectedCycle
		                                                   ORDER BY BI_PL_Position_name__c]) {
			if(!positionsInUse.contains(pc.BI_PL_Position_name__c))
				output.put(pc.BI_PL_Position_name__c, new PositionCycleWrapper(pc.BI_PL_External_id__c, pc.BI_PL_Position_name__c));
		}

		return output;
	}

	@RemoteAction
	public static Map<Id, BI_PL_Cycle__c> getCycles() {
		User currentUser = [SELECT Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

		return new Map<Id, BI_PL_Cycle__c>([SELECT Id, Name, BI_PL_Field_force__r.Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :currentUser.Country_Code_BI__c ORDER BY Name]);
	}

	@RemoteAction
	public static Set<String> getHierarchies(String cycleId) {
		Set<String> hierarchies = new Set<String>();
		for (BI_PL_Position_cycle__c c : [SELECT BI_PL_Hierarchy__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = :cycleId ORDER BY BI_PL_Hierarchy__c]) {
			if (!hierarchies.contains(c.BI_PL_Hierarchy__c)) {
				hierarchies.add(c.BI_PL_Hierarchy__c);
			}
		}
		return hierarchies;
	}

	/**
	 *	newPreparationPositionsMap : [key = original preparation Id, value = target hierarchy position cycle Id]
	 *
	 *	@author OMEGA CRM
	 */
	@RemoteAction
	public static void executeClone(String selectedCycle, String selectedHierarchy, String selectedTargetCycle, String selectedTargetHierarchy, Boolean mergeHierarchies, Map<String, String> newPreparationPositionsMap) {
		if (mergeHierarchies) {
			// Do not clone the hierarchy, just the preparations:
			BI_PL_PreparationCloneBatch cloneDataBatch = new BI_PL_PreparationCloneBatch(selectedCycle, selectedHierarchy, selectedTargetCycle, selectedTargetHierarchy, newPreparationPositionsMap);
				Database.executeBatch(cloneDataBatch);
		} else {
			// Clone both hierarchy and preparations:
			BI_PL_PositionCycleCloneBatch cloneDataBatch = new BI_PL_PositionCycleCloneBatch(selectedCycle, selectedHierarchy, selectedTargetCycle, selectedTargetHierarchy);
			Database.executeBatch(cloneDataBatch);
		}
	}
}