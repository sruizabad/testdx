@isTest
public with sharing class BI_PL_PreparationCloneCtrl_Test {

    private static Map<String, User> users;
    private static Map<Id, User> usersById;
    private static String hierarchy;
    private static BI_PL_Cycle__c cycle;

    private static BI_PL_Position__c positionRoot;
    private static BI_PL_Position__c positionChild1;
    private static BI_PL_Position__c positionChild2;
    private static BI_PL_Position__c positionChild21;

    private static BI_PL_Position_cycle__c root;
    private static BI_PL_Position_cycle__c child1;
    private static BI_PL_Position_cycle__c child2;
    private static BI_PL_Position_cycle__c child21;

    private static BI_PL_Preparation__c preparation1;
    private static BI_PL_Preparation__c preparation2;
    private static BI_PL_Preparation__c preparation3;
    private static BI_PL_Preparation__c preparation4;

    private static Map<String, List<BI_PL_Position_cycle_user__c>> positionCycleUsersByPosition = new Map<String, List<BI_PL_Position_cycle_user__c>>();
    private static Map<Id, BI_PL_Preparation__c> preparationsMap = new Map<Id, BI_PL_Preparation__c>();


    @isTest
    public static void test1() {

        basicTestRecordsCreation();

        BI_PL_TestDataFactory.createTestUsers(1, 'BR');
        User user1 = BI_PL_TestDataFactory.listUser.get(0);
        System.runAs(user1){
            Map<Id, BI_PL_Cycle__c> mapCycles= BI_PL_PreparationCloneCtrl.getCycles();

            Set<Id> cyclesSet = mapCycles.keySet();
            List<Id> cyclesId = new List<Id>(cyclesSet);
            BI_PL_PreparationCloneCtrl.PositionCycleWrapper positionCycle = new BI_PL_PreparationCloneCtrl.PositionCycleWrapper('Stirng', 'String');
            Set<String> hierarchies = BI_PL_PreparationCloneCtrl.getHierarchies(cyclesId.get(0));
            List<BI_PL_Preparation__c> preps = BI_PL_PreparationCloneCtrl.getPreparations('TestHierarchy',cyclesId.get(0));
            Map<String, BI_PL_PreparationCloneCtrl.PositionCycleWrapper> mapPositions = BI_PL_PreparationCloneCtrl.getTargetPositionCycles('TestHierarchy',cyclesId.get(0));
            Map<String,String> mapPC = new Map<String, String>();
            BI_PL_PreparationCloneCtrl.executeClone(cyclesId.get(0),'TestHierarchy',cyclesId.get(0),'TestHierarchy', false, mapPC);
            BI_PL_PreparationCloneCtrl.executeClone(cyclesId.get(0),'TestHierarchy',cyclesId.get(0),'TestHierarchy', true, mapPC);

        }
    }

    @isTest
    public static void test2(){
        /*basicTestRecordsCreation();
        preparation1.BI_PL_Status__c = 'Approved';
        preparation2.BI_PL_Status__c = 'Approved';
        preparation3.BI_PL_Status__c = 'Approved';
        preparation4.BI_PL_Status__c = 'Approved';

        BI_PL_PreparationCloneCtrl controller = new BI_PL_PreparationCloneCtrl();
        controller.adminControllerWrapper = new BI_PL_PlanitAdminCtrlWrapper(new BI_PL_PlanitAdminCtrl());

        BI_PL_TestDataFactory.createTestUsers(1, 'BR');
        User user1 = BI_PL_TestDataFactory.listUser.get(0);
        System.runAs(user1){
            controller.selectedCycle = cycle.Id;
            controller.selectedHierarchy = hierarchy;
            controller.selectedTargetCycle = cycle.Id;

            controller.loadPreparations();
        }*/

    }
    private static void basicTestRecordsCreation() {
        users = usersCreation();
        System.debug('users: ' + users);

        insert new BI_PL_Country_Settings__c(Name = 'Brazil', BI_PL_Country_Code__c = 'BR', BI_PL_Default_owner_user_name__c = getUser('RootUser', users).UserName, BI_PL_Specialty_field__c = 'Specialty_1_vod__c');

        hierarchy = 'TestHierarchy';

        BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'TestFieldForce', BI_TM_Country_code__c = 'BR');

        insert fieldForce;

        cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'BR', BI_PL_Start_date__c = Date.today(), BI_PL_End_date__c = Date.today() + 1, BI_PL_Field_force__c = fieldForce.Id);

        insert cycle;

        List<BI_PL_Position__c> positions = new List<BI_PL_Position__c>();
        List<BI_PL_Position_cycle_user__c> positionCycleUsers = new List<BI_PL_Position_cycle_user__c>();
        List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();

        positionRoot = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = 'BR');
        positionChild1 = new BI_PL_Position__c(Name = 'Test2', BI_PL_Country_code__c = 'BR');
        positionChild2 = new BI_PL_Position__c(Name = 'Test3', BI_PL_Country_code__c = 'BR');
        positionChild21 = new BI_PL_Position__c(Name = 'Test4', BI_PL_Country_code__c = 'BR');

        positions.add(positionRoot);
        positions.add(positionChild1);
        positions.add(positionChild2);
        positions.add(positionChild21);

        insert positions;

        System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());

        cycle = [SELECT Id, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c, Name, BI_PL_Field_force__r.Name FROM BI_PL_Cycle__c WHERE Id =: cycle.Id];


        root = new BI_PL_Position_cycle__c(/*Name = 'root', */BI_PL_Position__c = positionRoot.Id, BI_PL_Parent_position__c = null, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = hierarchy);

        root.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, root.BI_PL_Hierarchy__c, positionRoot.Name);

        insert root;

        child1 = new BI_PL_Position_cycle__c(/*Name = 'child1', */BI_PL_Position__c = positionChild1.Id, BI_PL_Parent_position__c = positionRoot.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = hierarchy);

        child1.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child1.BI_PL_Hierarchy__c, positionChild1.Name);

        insert child1;

        child2 = new BI_PL_Position_cycle__c(/*Name = 'child2', */BI_PL_Position__c = positionChild2.Id, BI_PL_Parent_position__c = positionRoot.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = hierarchy);
        
        child2.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child2.BI_PL_Hierarchy__c, positionChild2.Name);

        insert child2;

        child21 = new BI_PL_Position_cycle__c(/*Name = 'child21', */BI_PL_Position__c = positionChild21.Id, BI_PL_Parent_position__c = positionChild2.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = hierarchy);

        child21.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child21.BI_PL_Hierarchy__c, positionChild21.Name);

        insert child21;

        addPositionCycleUser(cycle, root, 'RootUser', positionRoot.Name);
        addPositionCycleUser(cycle, child1, 'PositionChild1User', positionChild1.Name);
        addPositionCycleUser(cycle, child2, 'PositionChild2User', positionChild2.Name);
        addPositionCycleUser(cycle, child21, 'PositionChild21User', positionChild21.Name);

        insert getPositionCycleUsers();

        System.debug('2.Number of Queries used in this apex code so far: ' + Limits.getQueries());


        System.debug('TESTETTSET hierarchy: ' + hierarchy + ' cycle: ' + cycle.Id);
        System.debug('PCU: ' + [SELECT BI_PL_Position_cycle__r.BI_PL_Position__c,
                                BI_PL_User__c,
                                BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                                BI_PL_Position_cycle__r.BI_PL_Parent_position__c,
                                BI_PL_Position_cycle__r.BI_PL_Position__r.Name
                                FROM BI_PL_Position_cycle_user__c
                                WHERE BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchy AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycle.Id]);

        preparation1 = new BI_PL_Preparation__c(BI_PL_Country_code__c ='BR', BI_PL_Position_cycle__c = root.Id);
        preparation2 = new BI_PL_Preparation__c(BI_PL_Country_code__c ='BR', BI_PL_Position_cycle__c = child2.Id);
        preparation3 = new BI_PL_Preparation__c(BI_PL_Country_code__c ='BR', BI_PL_Position_cycle__c = child21.Id);
        preparation4 = new BI_PL_Preparation__c(BI_PL_Country_code__c ='BR', BI_PL_Position_cycle__c = child1.Id);

        preparation1.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('BR', cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchy, positionRoot.Name, 'KAM');
        preparation2.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('BR', cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchy, positionChild2.Name, 'KAM');
        preparation3.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('BR', cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchy, positionChild21.Name, 'KAM');
        preparation4.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('BR', cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchy, positionChild1.Name, 'KAM');

        preparations.add(preparation1);
        preparations.add(preparation2);
        preparations.add(preparation3);
        preparations.add(preparation4);

        insert preparations;

        System.debug('3.Number of Queries used in this apex code so far: ' + Limits.getQueries());

    }

    private static List<BI_PL_Position_cycle_user__c> getPositionCycleUsers() {
        List<BI_PL_Position_cycle_user__c> output = new List<BI_PL_Position_cycle_user__c>();
        for (List<BI_PL_Position_cycle_user__c> l : positionCycleUsersByPosition.values())
            output.addAll(l);
        return output;
    }

    private static BI_PL_Position_cycle_user__c addPositionCycleUser(BI_PL_Cycle__c cycle, BI_PL_Position_cycle__c positionCycle, String userId, String positionName) {
        if (!positionCycleUsersByPosition.containsKey(positionCycle.Id))
            positionCycleUsersByPosition.put(positionCycle.Id, new List<BI_PL_Position_cycle_user__c>());

        BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_User__c = getUserId(userId, users));

        pcu.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycle, positionCycle.BI_PL_Hierarchy__c, positionName, getUser(userId, users).UserName);

        positionCycleUsersByPosition.get(positionCycle.Id).add(pcu);
        return pcu;
    }
    private static User getUser(String lastName, Map<String, User> u) {
        return users.get(lastName);
    }
    private static String getUserId(String lastName, Map<String, User> u) {
        return String.valueOf(getUser(lastName, users).Id).substring(0, 15);
    }

    private static Map<String, User> usersCreation() {
        usersById = new Map<Id, User>();
        Map<String, User> output = new Map<String, User>();

        Profile p = [SELECT Id FROM Profile WHERE Name LIKE 'BR_%' LIMIT 1];

        List<User> usersList = new List<User>();
        usersList.add(createUser('RootUser', p.Id));
        usersList.add(createUser('PositionChild1User', p.Id));
        usersList.add(createUser('PositionChild2User', p.Id));
        usersList.add(createUser('PositionChild21User', p.Id));
        usersList.add(createUser('PositionChild3User', p.Id));

        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name = 'BI_PL_SALES'];
        insert usersList;

        for (User u : usersList) {
            output.put(u.LastName, u);
            usersById.put(u.Id, u);

            System.runAs(new User(Id = Userinfo.getUserId(), UserName = 'username_' + Math.random() + '@u.com', Country_Code_BI__c = 'BR')) {
                insert createPermissionSetAssignment(u.Id, permissionSet.Id);
            }
        }

        return output;
    }

    private static User createUser(String name, Id profileId) {
        return new User(Alias = name.substring(0, 5), Email = name + '@testorg.com',
                        EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US', ProfileId = profileId, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles');
    }
    private static PermissionSetAssignment createPermissionSetAssignment(Id userId, Id permissionSet) {
        return new PermissionSetAssignment(PermissionSetId = permissionSet, AssigneeId = userId);
    }
}