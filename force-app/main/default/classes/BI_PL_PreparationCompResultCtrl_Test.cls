@isTest
private class BI_PL_PreparationCompResultCtrl_Test {
	
	Static Date currentDate;
	Static String countryCode;
	
	@isTest static void test_method_one() {

		User testUser = [SELECT Id, Country_Code_BI__c from User where Id =: UserInfo.getUserId()];
		countryCode = testUser.Country_Code_BI__c;
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_Preparation__c preparation = createPreparation();	

		apexpages.currentpage().getparameters().put('ids' , preparation.id);
		apexpages.currentpage().getparameters().put('channel' , 'rep_detail_only');
		BI_PL_PreparationComparatorResultCtrl controller = new BI_PL_PreparationComparatorResultCtrl();
	}
	
	static BI_PL_Preparation__c createPreparation() {
    	currentDate = Date.today();
        BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(
        	BI_TM_Country_Code__c = countryCode
        );

        insert fieldForce;

        BI_PL_Cycle__c	cycle = new BI_PL_Cycle__c(
        	BI_PL_Country_code__c = countryCode,
        	BI_PL_Start_date__c = currentDate,
        	BI_PL_End_date__c = currentDate.addMonths(1),
        	BI_PL_External_id__c = 'BR_Cycle_Test_Ext_Id',
        	BI_PL_Field_force__c = fieldForce.Id
        );

        insert cycle;

        BI_PL_Position__c position = new BI_PL_Position__c(
        	Name = 'testPosition',
        	BI_PL_Country_code__c = countryCode,
        	BI_PL_Field_force__c = 'TestFieldForce'
        );

        insert position;

      
        BI_PL_TestDataUtility.createCustomSettings();


        Account acc1 = new Account(
        	Name = 'TestAccount1',
       		External_Id_vod__c = 'TestAccount1_Ext_Id'
        	);

        Database.Upsert(acc1, Account.fields.External_Id_vod__c);

        Account acc2 = new Account(
        	Name = 'TestAccount2',
        	External_Id_vod__c = 'TestAccount2_Ext_Id'
        	);
        
        Database.Upsert(acc2, Account.fields.External_Id_vod__c);

        BI_PL_Position_cycle__c positionCycle= new BI_PL_Position_cycle__c(
        	BI_PL_Cycle__c = cycle.Id,
        	BI_PL_External_id__c = cycle.BI_PL_External_id__c+'PosCycle',
        	BI_PL_Hierarchy__c = 'testHierarchy',
        	BI_PL_Position__c = position.Id
        );

        insert positionCycle;

        Product_vod__c product1 = new Product_vod__c(
        	Name = 'TestProduct1',
        	RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(),
        	Product_Type_vod__c = 'Detail',
        	External_ID_vod__c = 'TestProduct1_ExtId'
        );

        insert product1;

        Product_vod__c product2 = new Product_vod__c(
        	Name = 'TestProduct2',
        	Product_Type_vod__c = 'Detail',
        	External_ID_vod__c = 'TestProduct2_ExtId'
        );

        insert product2;

        BI_PL_Preparation__c prep = new BI_PL_Preparation__c(
			BI_PL_Country_code__c = countryCode,
		    BI_PL_Position_cycle__c = positionCycle.Id,
		    BI_PL_Status__c = 'Under Review',
		    BI_PL_External_Id__c = 'External_ID_Prep1'
		);

		insert prep;

		BI_PL_Target_preparation__c tP1 = new BI_PL_Target_preparation__c(
    			BI_PL_Target_customer__c = acc1.Id,
			  	BI_PL_Header__c = prep.Id,
			  	BI_PL_External_Id__c = 'External_ID_Target11');
		insert tP1;

		BI_PL_Target_preparation__c tP2 = new BI_PL_Target_preparation__c(
    			BI_PL_Target_customer__c = acc2.Id,
			  	BI_PL_Header__c = prep.Id,
			  	BI_PL_External_Id__c = 'External_ID_Target12');
		insert tP2;

		BI_PL_Channel_detail_preparation__c tc1 = new BI_PL_Channel_detail_preparation__c(
			BI_PL_Channel__c = 'rep_detail_only',
    		BI_PL_Target__c = tP1.Id,
    		BI_PL_Reviewed__c = true,
    		BI_PL_Edited__c = true,
    		BI_PL_External_Id__c = 'External_ID_ChannelDetail1'
		);

		insert tc1;

		System.debug('***tc1 = '+tc1);

		BI_PL_Channel_detail_preparation__c tc2 = new BI_PL_Channel_detail_preparation__c(
			BI_PL_Channel__c = 'rep_detail_only',
		    BI_PL_Target__c = tP2.Id,
		    BI_PL_Reviewed__c = true,
		    BI_PL_Edited__c = true,
		    BI_PL_External_Id__c = 'External_ID_ChannelDetail2'
		);

		insert tc2;

		BI_PL_Detail_preparation__c dp1 = new BI_PL_Detail_preparation__c(
			BI_PL_Adjusted_details__c = 1,
		    BI_PL_External_id__c = 'ExternalIdTestDP1',
		    BI_PL_Planned_details__c = 5,
		    BI_PL_Segment__c = 'No Segmentation',
		    BI_PL_Channel_detail__c = tc1.Id,
		    BI_PL_Product__c = product1.Id
		);

		insert dp1;

		BI_PL_Detail_preparation__c dp2 = new BI_PL_Detail_preparation__c(
			BI_PL_Adjusted_details__c = 1,
		    BI_PL_External_id__c = 'ExternalIdTestDP2',
		    BI_PL_Planned_details__c = 5,
		    BI_PL_Segment__c = 'No Segmentation',
		    BI_PL_Channel_detail__c = tc2.Id,
		    BI_PL_Product__c = product2.Id
		);

		insert dp2;

        MC_Cycle_vod__c mcCycle = new MC_Cycle_vod__c(
        	Name = 'BI_PL_TestMCCycle',
        	Start_date_vod__c = currentDate,
        	End_date_vod__c = currentDate.addMonths(1),
        	Country_code_BI__c = countryCode,
        	External_Id_vod__c = 'BI_PL_TestMCCycle'
        );

        insert mcCycle;

        return prep;
    }

    public with sharing class BI_PL_PreparationComparatorWrapper {

        public BI_PL_Preparation__c record {get; set;}

        public String sumAdjInteractions {get; set;}
        public String sumPlanInteractions {get; set;}
        public String sumMaxAdjInteractions {get; set;}
        public String sumMaxPlanInteractions {get; set;}

        public String plannedTargets {get; set;}
        public String adjustedTargets {get; set;}
        public String addedTargets {get; set;}
        public String addedTargetsApproved {get; set;}
        public String removedTargets {get; set;}
        public String removedTargetsApproved {get; set;}
        public String targetsToSendToVeeva {get; set;}

        public BI_PL_PreparationComparatorWrapper(BI_PL_Preparation__c record, Decimal sumAdjInteractions, Decimal sumPlanInteractions, Decimal sumMaxAdjInteractions, Decimal sumMaxPlanInteractions, Decimal plannedTargets, Decimal adjustedTargets, Decimal addedTargets, Decimal addedTargetsApproved, Decimal removedTargets, Decimal removedTargetsApproved, Decimal targetsToSendToVeeva) {

            this.record = record;

            this.sumAdjInteractions = convertToString(sumAdjInteractions);
            this.sumPlanInteractions = convertToString(sumPlanInteractions);
            this.sumMaxAdjInteractions = convertToString(sumMaxAdjInteractions);
            this.sumMaxPlanInteractions = convertToString(sumMaxPlanInteractions);

            this.plannedTargets = convertToString(plannedTargets);
            this.adjustedTargets = convertToString(adjustedTargets);
            this.addedTargets = convertToString(addedTargets);
            this.addedTargetsApproved = convertToString(addedTargetsApproved);
            this.removedTargets = convertToString(removedTargets);
            this.removedTargetsApproved = convertToString(removedTargetsApproved);
            this.targetsToSendToVeeva = convertToString(targetsToSendToVeeva);
        }
        public BI_PL_PreparationComparatorWrapper(BI_PL_Preparation__c record) {
            this.record = record;
        }

        private String convertToString(Decimal value) {
            if (value == null)
                return '-';
            return value.format();
        }
    }
	
}