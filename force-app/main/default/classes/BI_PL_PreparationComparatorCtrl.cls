/**
 *  08-06-2017
 *  @author OMEGA CRM
*/
public with sharing class BI_PL_PreparationComparatorCtrl {

    public String countryCode {get; set;}
    public Id cycle {get; set;}
    public String hierarchy {get; set;}
    public String channel {get; set;}
    public Id fieldForce {get; set;}
    public Id currentPreparation {get; set;}
    public String userid{get;set;}

    public SelectOption notAvailableOption {
        get{
            if (notAvailableOption == null)
                notAvailableOption = new SelectOption('-1', 'No values available');

            return notAvailableOption;
        }
        set;
    }


    public Map<Id, BI_PL_Preparation__c> preparations {get; set;}

    public String selectedPreparations {get; set;}

    public List<SelectOption> cycleOptions {get; set;}
    public List<SelectOption> countryOptions {get; set;}

    public Boolean loading {get; set;}

    /**
     * Constructs the object.
     */
    public BI_PL_PreparationComparatorCtrl() {
        countryOptions = new List<SelectOption>();
        userid = Userinfo.getUserId();

        init();
    }

    public void init() {
        System.debug('BI_PL_PreparationComparatorCtrl CONSTRUCTOR');
        System.debug('BI_PL_PreparationComparatorCtrl cycle ' + cycle);
        System.debug('BI_PL_PreparationComparatorCtrl cycle ' + hierarchy);

        countryCode = getUserCountryCode();

        if (checkViewAllPreparations())
            loadCountryOptions();

        preparations = new Map<Id, BI_PL_Preparation__c>();
        loading = false;

        loadCycleOptions();

    }

    /**
     * Loads the available countries for the appp.
     */
    public void loadCountryOptions() {

        countryOptions = new List<SelectOption>();
        System.debug('Country Settings ' + [SELECT BI_PL_Country_code__c FROM BI_PL_Country_settings__c]);
        for (BI_PL_Country_settings__c c : BI_PL_Country_settings__c.getall().values()) {
            countryOptions.add(new SelectOption(c.BI_PL_Country_code__c, c.Name));
        }

        String userCountryCode = getUserCountryCode();

        // If the user has no country code assigned set the first one in the list:
        if (String.isBlank(userCountryCode)) {
            if (countryOptions.size() > 0)
                countryCode = countryOptions.get(0).getValue();
        } else {
            countryCode = userCountryCode;
        }
    }

    private String getUserCountryCode() {
        System.debug('getUserCountryCode : '+[SELECT Country_Code_BI__c FROM USER WHERE Id = :UserInfo.getUserId()]);
        return [SELECT Country_Code_BI__c FROM USER WHERE Id = :UserInfo.getUserId()].Country_Code_BI__c;
    }
    /**
     * Loads cycle options.
     */
    public void loadCycleOptions() {

        System.debug('loadCycleOptions ' + countryCode);

        cycleOptions = new List<SelectOption>();
        for (BI_PL_Cycle__c c : [SELECT Id, Name FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = : countryCode]) {
            cycleOptions.add(new SelectOption(c.Id, c.Name));
        }
    }

    public Boolean checkViewAllPreparations() {
        list<ObjectPermissions> pmAssigned = new list<ObjectPermissions>([  SELECT Id
                FROM ObjectPermissions WHERE ParentId IN
                (SELECT PermissionSetId
                 FROM PermissionSetAssignment
                 WHERE AssigneeId = : UserInfo.getUserId())
                AND SObjectType = 'SAP_Preparation_BI__c' AND PermissionsViewAllRecords = true]);


        return (!pmAssigned.isEmpty());
    }
    /**
     * { function_description }
     *
     * @return     { description_of_the_return_value }
     */
    public PageReference cycleChanged() {
        System.debug('cycleChanged: ' + cycle);
        return null;
    }


    /**
     * { function_description }
     *
     * @return     { description_of_the_return_value }
     */
    public PageReference countryCodeChanged() {
        System.debug('countryCodeChanged: ' + countryCode);
        loadCycleOptions();
        return null;
    }




    /**
     * { function_description }
     *
     * @return     { description_of_the_return_value }
     */
    public PageReference hierarchyChanged() {
        return null;
    }
    /**
     * { function_description }
     *
     * @return     { description_of_the_return_value }
     */
    public PageReference compare() {
        PageReference pg = Page.BI_PL_PreparationComparatorResult;
        pg.setRedirect(true);
        pg.getParameters().put('ids', selectedPreparations);
        pg.getParameters().put('channel', channel);

        return pg;
    }

    public void empty() {

    }

}