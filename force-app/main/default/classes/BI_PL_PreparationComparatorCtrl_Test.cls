@isTest
public with sharing class BI_PL_PreparationComparatorCtrl_Test {
	@isTest
	public static void test() {

		User currentUser = [SELECT Id, Country_Code_BI__c from User where Id =:UserInfo.getUserId()];

		String countryCode = currentUser.Country_Code_BI__c;

		Date currentDate = Date.today();
        BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(
        	BI_TM_Country_Code__c = countryCode
        );

        insert fieldForce;

		BI_PL_Cycle__c	cycle = new BI_PL_Cycle__c(
        	BI_PL_Country_code__c = countryCode,
        	BI_PL_Start_date__c = currentDate,
        	BI_PL_End_date__c = currentDate.addMonths(1),
        	BI_PL_External_id__c = 'BR_Cycle_Test_Ext_Id',
        	BI_PL_Field_force__c = fieldForce.Id
        );

        insert cycle;

		BI_PL_PreparationComparatorCtrl controller = new BI_PL_PreparationComparatorCtrl();

		controller.init();
		controller.loadCountryOptions();
		controller.loadCycleOptions();
		controller.checkViewAllPreparations();
		controller.cycleChanged();
		controller.countryCodeChanged();
		controller.hierarchyChanged();
		controller.compare();
		controller.empty();
		PageReference comp = controller.compare();

		SelectOption notOption = controller.notAvailableOption;

		//Next, we do the same for a user with no Country Code set

		Profile p = [SELECT Id FROM Profile WHERE Name LIKE 'BR_%' LIMIT 1];
		User currentUser2 = new User(Alias = 'User2', Email =  'currentUser2@testorg.com',
		                EmailEncodingKey = 'UTF-8', LastName = 'CurrentUser2', LanguageLocaleKey = 'en_US',Country_Code_BI__c='BR',
		                LocaleSidKey = 'en_US', ProfileId = p.Id, UserName = 'CurrentUser2' + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = 'CurrentUser2' +'_Test_External_ID');
		String countryCode2 = currentUser2.Country_Code_BI__c;
		Date currentDate2 = Date.today();
        	BI_TM_FF_type__c fieldForce2 = new BI_TM_FF_type__c(
        		BI_TM_Country_Code__c = countryCode2
        	);

        	insert fieldForce2;

        BI_PL_Cycle__c	cycle2 = new BI_PL_Cycle__c(
        		BI_PL_Country_code__c = countryCode,
        		BI_PL_Start_date__c = currentDate2,
        		BI_PL_End_date__c = currentDate2.addMonths(1),
        		BI_PL_External_id__c = 'BR_Cycle2_Test_Ext_Id',
        		BI_PL_Field_force__c = fieldForce2.Id
        	);

        	insert cycle2;

        	BI_PL_Country_settings__c countryList = new BI_PL_Country_settings__c(Name = 'USA', BI_PL_Country_code__c = 'US', BI_PL_Specialty_field__c = 'Specialty_1_vod__c');

        	insert countryList;

		System.runas(currentUser2){

			

			
			BI_PL_PreparationComparatorCtrl controller2 = new BI_PL_PreparationComparatorCtrl();

			Id fieldforce3 = controller2.fieldforce;
			Id currPrep = controller2.currentPreparation;

			controller2.init();
			controller2.loadCountryOptions();
			controller2.loadCycleOptions();
			controller2.checkViewAllPreparations();
			controller2.cycleChanged();
			controller2.countryCodeChanged();
			controller2.hierarchyChanged();
			controller2.compare();
			controller2.empty();
			PageReference comp2 = controller2.compare();

			SelectOption notOption2 = controller2.notAvailableOption;
		}
	}
}