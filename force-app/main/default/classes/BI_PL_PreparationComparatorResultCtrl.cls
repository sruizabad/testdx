/**
 *  12-06-2017
 *  Added the following rows:
 *  Planned targets, Adjusted targets, Added targets, Added targets approved, Removed targets, Removed targets approve, Targets to send to Veeva.
 *  @author OMEGA CRM
*/
public with sharing class BI_PL_PreparationComparatorResultCtrl {

    private Map<Id, BI_PL_PreparationComparatorWrapper> data;

    public List<BI_PL_PreparationComparatorWrapper> preparationsData {
        get{
            return data.values();
        }
        set;
    }

    /**
     * Constructs the object.
     */
    public BI_PL_PreparationComparatorResultCtrl() {
        String ids =  ApexPages.currentPage().getParameters().get('ids');
        String channel =  ApexPages.currentPage().getParameters().get('channel');

        List<String> idsList = ids.split(',');

        Map<Id, BI_PL_Preparation__c> preparations = new Map<Id, BI_PL_Preparation__c>([SELECT Id, name, BI_PL_Global_capacity__c, BI_PL_Capacity__c, BI_PL_Number_of_targets__c, Owner.Name, BI_PL_Status__c, BI_PL_Position_cycle__r.BI_PL_Confirmed__c, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Position_cycle__r.BI_PL_Position_name__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name
                // TODO Ver con Marcos estos campos
                /*, Total_Planned_Details_BI__c, Total_Planned_Interactions_BI__c, Total_Adjusted_Details_BI__c, Total_Adjusted_Interactions_BI__c */
                FROM BI_PL_Preparation__c
                WHERE Id IN :idsList]);

        System.debug(preparations);


        Map<Id, BI_PL_Channel_detail_preparation__c> channelDetails = new Map<Id, BI_PL_Channel_detail_preparation__c>();

        // By
        Map<Id, Decimal> sumAdjustedInteractions = new Map<Id, Decimal>();
        Map<Id, Decimal> sumPlannedInteractions = new Map<Id, Decimal>();
        Map<Id, Decimal> maxAdjustedInteractions = new Map<Id, Decimal>();
        Map<Id, Decimal> maxPlannedInteractions = new Map<Id, Decimal>();

        // By target
        Map<Id, Decimal> plannedTargets = new Map<Id, Decimal>();
        Map<Id, Decimal> adjustedTargets = new Map<Id, Decimal>();
        Map<Id, Decimal> addedTargets = new Map<Id, Decimal>();
        Map<Id, Decimal> addedTargetsApproved = new Map<Id, Decimal>();
        Map<Id, Decimal> removedTargets = new Map<Id, Decimal>();
        Map<Id, Decimal> removedTargetsApproved = new Map<Id, Decimal>();
        Map<Id, Decimal> targetsToSendToVeeva = new Map<Id, Decimal>();

        Map<Id, Decimal> addedManuallyByPreparation = new Map<Id, Decimal>();

        data = new Map<Id, BI_PL_PreparationComparatorWrapper>();

        Set<Id> targetsAlreadyCalculated = new Set<Id>();

        List<Id> planTar = new List<Id>();
        List<Id> actuTar = new List<Id>();
        List<Id> addTar = new List<Id>();
        List<Id> remTar = new List<Id>();
        map<Id, String> tarLis = new map<Id, String>();
        List<BI_PL_Channel_detail_preparation__c> channelDetailList = new List<BI_PL_Channel_detail_preparation__c>();

        for (BI_PL_Channel_detail_preparation__c channelDetail : [SELECT
                BI_PL_Target__c,

                BI_PL_Reviewed__c,

                BI_PL_Target__r.BI_PL_Added_manually__c,
                BI_PL_Target__r.BI_PL_Next_best_account__c,
                BI_PL_Target__r.BI_PL_No_see_list__c,
                BI_PL_Removed__c,
                BI_PL_Target__r.BI_PL_Header__c,

                BI_PL_Sum_adjusted_interactions__c,
                BI_PL_Sum_planned_interactions__c,
                BI_PL_Max_adjusted_interactions__c,
                BI_PL_Max_planned_interactions__c

                FROM BI_PL_Channel_detail_preparation__c
                WHERE BI_PL_Channel__c = : channel
                                         AND BI_PL_Target__r.BI_PL_Header__c IN :idsList]) {

            channelDetailList.add(channelDetail);

            Id preparationId = channelDetail.BI_PL_Target__r.BI_PL_Header__c;
            Id targetId = channelDetail.BI_PL_Target__c;

            System.debug('preparationId ' + preparationId);

            Boolean removed = channelDetail.BI_PL_Removed__c;
            Boolean addedManually = channelDetail.BI_PL_Target__r.BI_PL_Added_manually__c;
            Boolean nextbest = channelDetail.BI_PL_Target__r.BI_PL_Next_best_account__c;
            Boolean noseelist = channelDetail.BI_PL_Target__r.BI_PL_No_see_list__c;

            //GLOS 667 - Next best account and no see list have planned interactions don't add it.
            if(!addedManually && !nextbest && !noseelist){
                addValueToMap(sumPlannedInteractions, preparationId, getValueIfNotEmpty(channelDetail.BI_PL_Sum_planned_interactions__c));
                addValueToMap(maxPlannedInteractions, preparationId, getValueIfNotEmpty(channelDetail.BI_PL_Max_planned_interactions__c));
            }

            //GLOS 667 - No add interactions from removed accounts, no see list and nextbest account that no added manually.
            if((addedManually && nextbest && !removed) || (!removed && !nextbest && !noseelist)){           
                addValueToMap(sumAdjustedInteractions, preparationId, getValueIfNotEmpty(channelDetail.BI_PL_Sum_adjusted_interactions__c));
                addValueToMap(maxAdjustedInteractions, preparationId, getValueIfNotEmpty(channelDetail.BI_PL_Max_adjusted_interactions__c));
            }

            if (!targetsAlreadyCalculated.contains(targetId)) {

                //GLOS 667 - removed target from call plan and not added manually
                Boolean removed_notadd = channelDetail.BI_PL_Removed__c && !channelDetail.BI_PL_Target__r.BI_PL_Added_manually__c;
                
                //if addeed and then dropped the bi_pl_added_mannually__c is true
                Boolean addedManually_notdrop = channelDetail.BI_PL_Target__r.BI_PL_Added_manually__c && !channelDetail.BI_PL_Removed__c;
                
                Boolean reviewed = channelDetail.BI_PL_Reviewed__c;
                Boolean plannedTarget = !addedManually && !nextbest && !noseelist;
                Boolean addedTargetApproved = addedManually_notdrop && reviewed;
                Boolean removedTargetApproved = removed_notadd && reviewed;

                addValueToMap(plannedTargets, preparationId, plannedTarget);
                addValueToMap(addedTargets, preparationId, addedManually_notdrop);
                addValueToMap(addedTargetsApproved, preparationId, addedTargetApproved);
                addValueToMap(removedTargets, preparationId, removed_notadd);
                addValueToMap(removedTargetsApproved, preparationId, removedTargetApproved);

                targetsAlreadyCalculated.add(targetId);
            }
        }

        for(BI_PL_Detail_Preparation__c detPrep : [SELECT Id, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c,BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c,BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c, BI_PL_Channel_detail__r.BI_PL_Removed__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__r.Name, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__r.IsPersonAccount FROM BI_PL_Detail_Preparation__c WHERE BI_PL_Channel_detail__c IN: channelDetailList]){

            if(detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.IsPersonAccount){
                Id accountId = detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c;
                /*System.debug('**DropId ' + detPrep.Id);
                System.debug('**DropName ' + detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__r.Name);
                System.debug('**accountsDrop ' + detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Removed__c);*/

                //if(!detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c && !detPrep.BI_PL_Channel_detail__r.BI_PL_Removed__c && !detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c && !detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c){
                if(!detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c && !detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c && !detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c){
                    planTar.add(accountId);
                    actuTar.add(accountId);
                    tarLis.put(accountId, 'Planned and Actual');
                }
                else if(detPrep.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c && !detPrep.BI_PL_Channel_detail__r.BI_PL_Removed__c){
                    actuTar.add(accountId);
                    addTar.add(accountId);
                    tarLis.put(accountId, 'Actual and Added');
                }
                else if(detPrep.BI_PL_Channel_detail__r.BI_PL_Removed__c){
                        remTar.add(accountId);
                        tarLis.put(accountId, 'Dropped');
                }
            }
        }


        // Values computed using the previously calculated ones:
        for (String prepId : plannedTargets.keySet()) {
            addValueToMap(adjustedTargets, prepId, plannedTargets.get(prepId) + addedTargets.get(prepId) - removedTargets.get(prepId));
            addValueToMap(targetsToSendToVeeva, prepId, plannedTargets.get(prepId) + addedTargetsApproved.get(prepId) - removedTargetsApproved.get(prepId));
        }


        for (Id prepId : preparations.keySet()) {

            List<Decimal> sales = getSalesByPrep(prepId, tarLis, planTar, actuTar, addTar, remTar);


            System.debug('prepID??? ' + prepId + ' - ' + plannedTargets.containsKey(prepId));
            if (plannedTargets.containsKey(prepId)) {
                data.put(prepId, 
                         new BI_PL_PreparationComparatorWrapper(
                             preparations.get(prepId),
                             sumAdjustedInteractions.get(prepId),
                             sumPlannedInteractions.get(prepId),
                             maxAdjustedInteractions.get(prepId),
                             maxPlannedInteractions.get(prepId),
                             plannedTargets.get(prepId),
                             adjustedTargets.get(prepId),
                             addedTargets.get(prepId),
                             addedTargetsApproved.get(prepId),
                             removedTargets.get(prepId),
                             removedTargetsApproved.get(prepId),
                             targetsToSendToVeeva.get(prepId), 
                             sales.get(0),
                             sales.get(1),
                             sales.get(2),
                             sales.get(3),
                             sales.get(4),
                             sales.get(5),
                             sales.get(6),
                             sales.get(7)
                             )
                        );

            } else {
                data.put(prepId, new BI_PL_PreparationComparatorWrapper(preparations.get(prepId)));
            }
        }



        System.debug('data ' + data);

        System.debug('sumAdjustedInteractions ' + sumAdjustedInteractions);
        System.debug('sumPlannedInteractions ' + sumPlannedInteractions);
        System.debug('maxAdjustedInteractions ' + maxAdjustedInteractions);
        System.debug('maxPlannedInteractions ' + maxPlannedInteractions);

        System.debug('plannedTargets ' + plannedTargets);
        System.debug('adjustedTargets ' + adjustedTargets);
        System.debug('addedTargets ' + addedTargets);
        System.debug('addedTargetsApproved ' + addedTargetsApproved);
        System.debug('removedTargets ' + removedTargets);
        System.debug('removedTargetsApproved ' + removedTargetsApproved);
    }
    /**
     *  Returns the value if it's not empty. Otherwise, returns 0.
     *  @author OMEGA CRM
     */
    private Decimal getValueIfNotEmpty(Decimal value) {
        if (value == null)
            return 0;
        return value;
    }

    /**
    *   Adds the provided value to the specified map.
    *   @author OMEGA CRM
    */
    private void addValueToMap(Map<Id, Decimal> m, Id theId, Decimal value) {
        if (!m.containsKey(theId))
            m.put(theId, 0);

        m.put(theId, m.get(theId) + value);
    }
    /**
    *   Adds 1 to the specified map if value is true.
    *   @author OMEGA CRM
    */
    private void addValueToMap(Map<Id, Decimal> m, Id theId, Boolean value) {
        addValueToMap(m, theId, (value) ? 1 : 0);
    }

    /**
     * Class for bi pl preparation comparator wrapper.
     */
    public with sharing class BI_PL_PreparationComparatorWrapper {

        public BI_PL_Preparation__c record {get; set;}

        public String sumAdjInteractions {get; set;}
        public String sumPlanInteractions {get; set;}
        public String sumMaxAdjInteractions {get; set;}
        public String sumMaxPlanInteractions {get; set;}

        public String plannedTargets {get; set;}
        public String adjustedTargets {get; set;}
        public String addedTargets {get; set;}
        public String addedTargetsApproved {get; set;}
        public String removedTargets {get; set;}
        public String removedTargetsApproved {get; set;}
        public String targetsToSendToVeeva {get; set;}

        public String planSales {get; set;}
        public String actuSales {get; set;}
        public String addSales {get; set;}
        public String dropSales {get; set;}
        public String planMarketSales {get; set;}
        public String actuMarketSales {get; set;}
        public String addMarketSales {get; set;}
        public String dropMarketSales {get; set;}



        public BI_PL_PreparationComparatorWrapper(BI_PL_Preparation__c record, Decimal sumAdjInteractions, Decimal sumPlanInteractions, Decimal sumMaxAdjInteractions, Decimal sumMaxPlanInteractions, Decimal plannedTargets, Decimal adjustedTargets, Decimal addedTargets, Decimal addedTargetsApproved, Decimal removedTargets, Decimal removedTargetsApproved, Decimal targetsToSendToVeeva, Decimal planSales, Decimal actuSales, Decimal addSales, Decimal dropSales, Decimal planMarketSales, Decimal actuMarketSales, Decimal addMarketSales, Decimal dropMarketSales) {

            this.record = record;

            this.sumAdjInteractions = convertToString(sumAdjInteractions);
            this.sumPlanInteractions = convertToString(sumPlanInteractions);
            this.sumMaxAdjInteractions = convertToString(sumMaxAdjInteractions);
            this.sumMaxPlanInteractions = convertToString(sumMaxPlanInteractions);

            this.plannedTargets = convertToString(plannedTargets);
            this.adjustedTargets = convertToString(adjustedTargets);
            this.addedTargets = convertToString(addedTargets);
            this.addedTargetsApproved = convertToString(addedTargetsApproved);
            this.removedTargets = convertToString(removedTargets);
            this.removedTargetsApproved = convertToString(removedTargetsApproved);
            this.targetsToSendToVeeva = convertToString(targetsToSendToVeeva);

            this.planSales = convertToString(planSales);
            this.actuSales = convertToString(actuSales);
            this.addSales = convertToString(addSales);
            this.dropSales = convertToString(dropSales);
            this.planMarketSales = convertToString(planMarketSales);
            this.actuMarketSales = convertToString(actuMarketSales);
            this.addMarketSales = convertToString(addMarketSales);
            this.dropMarketSales = convertToString(dropMarketSales);

        }
        public BI_PL_PreparationComparatorWrapper(BI_PL_Preparation__c record) {
            this.record = record;
        }

        private String convertToString(Decimal value) {
            if (value == null)
                return '-';
            return value.format();
        }
    }

    public List<String> getProductsByPrep(String prepId){
        List<BI_PL_Detail_preparation__c> prodNames = new List<BI_PL_Detail_preparation__c>([SELECT BI_PL_Product__r.External_Id_vod__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__r.External_Id_vod__c FROM BI_PL_Detail_preparation__c 
            WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__c =: prepId]);
        List<String> prodString = new List<String>();
        for(BI_PL_Detail_preparation__c det : prodNames)
            prodString.add(det.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__r.External_Id_vod__c + '_' + det.BI_PL_Product__r.External_Id_vod__c + '_Sales');

        return prodString;
    }   

    public List<BI_PL_Affiliation__c> getAffiByPrep(String prepId, map<Id, String> tarList, List<String> prods){
        List<Id> idTar = new List<Id>();
        for(Id key : tarList.keySet()){
            idTar.add(key);
        }
        List<BI_PL_Affiliation__c> aff = new List<BI_PL_Affiliation__c>([SELECT Id, Name, BI_PL_Product__c, BI_PL_Customer__c, BI_PL_Product_Sales__c, BI_PL_Market_Sales__c FROM BI_PL_Affiliation__c WHERE BI_PL_External_Id__c IN: prods]);
        return aff;

    }

    public List<Decimal> getSalesByPrep(String prepId, map<Id, String> tarList, List<Id> planTar, List<Id> actuTar, List<Id> addTar, List<Id> remTar){
        List<String> listProd = getProductsByPrep(prepId);
        List<BI_PL_Affiliation__c> affi = getAffiByPrep(prepId, tarList, listProd);
        Decimal planSales = 0;
        Decimal actuSales = 0;
        Decimal addSales = 0;
        Decimal dropSales = 0;
        Decimal planMarketSales = 0;
        Decimal actuMarketSales = 0;
        Decimal addMarketSales = 0;
        Decimal dropMarketSales = 0;
        System.debug('**Target List ' + tarList.size());
        System.debug('**Products ' + listProd.size());

        for(BI_PL_Affiliation__c af : affi){
            Id tar = af.BI_PL_Customer__c;
            //for(Id tar : tarList.keySet()){
                //if(af.BI_PL_Customer__c == tar){
                    if(af.BI_PL_Product_Sales__c != null && af.BI_PL_Market_Sales__c != null){
                        if(tarList.get(tar) == 'Planned and Actual'){
                            
                            planSales += af.BI_PL_Product_Sales__c;
                            actuSales += af.BI_PL_Product_Sales__c;
                            planMarketSales += af.BI_PL_Market_Sales__c;
                            actuMarketSales += af.BI_PL_Market_Sales__c;
                        

                        }
                        else if(tarList.get(tar) == 'Actual and Added'){
                            actuSales += af.BI_PL_Product_Sales__c;
                            addSales += af.BI_PL_Product_Sales__c;
                            actuMarketSales += af.BI_PL_Market_Sales__c;
                            addMarketSales += af.BI_PL_Market_Sales__c;
                        }
                        
                        else if(tarList.get(tar) == 'Dropped'){
                            dropSales += af.BI_PL_Product_Sales__c;
                            actusales -= af.BI_PL_Product_Sales__c;
                            dropMarketSales += af.BI_PL_Market_Sales__c;
                            actuMarketSales -= af.BI_PL_Market_Sales__c;
                        }
                }
                //}
            //}
        }


        System.debug('**planSales ' + planSales);

        List<Decimal> totSales = new List<Decimal>();
        totSales.add(planSales);
        totSales.add(actuSales);
        totSales.add(addSales);
        totSales.add(dropSales);
        totSales.add(planMarketSales);
        totSales.add(actuMarketSales);
        totSales.add(addMarketSales);
        totSales.add(dropMarketSales);

        for(Integer i=0;i<8;i++){
            if(totSales[i] < 0)
                totSales[i] = 0;
        }


        return totSales;
    }

   
}