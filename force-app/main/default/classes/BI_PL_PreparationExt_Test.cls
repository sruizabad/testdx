@isTest
private class BI_PL_PreparationExt_Test {

	Static Date currentDate;
	Static String countryCode;

	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

        BI_PL_TestDataFactory.createPlanitViews(userCountryCode);
        //BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);
    }

	private Static List<BI_PL_Affiliation__c> affiliations;
	private Static List<BI_PL_Affiliation__c> affiliationsI;


	private static List<BI_PL_Preparation__c> preps;
	private static List<Account> accounts;
	private static List<Product_vod__c> products;

	private static final String POSITION_ROOT_NAME = 'Test1';
	private static final String POSITION_TEST2_NAME = 'Test2';

	private static final String CHANNEL_REP_DETAIL_ONLY = 'rep_detail_only';

	private static final String FIELDFORCE = 'TestFieldForce';

	private static final Integer NACCOUNTS = 5;
	private static final Integer NPRODUCTS = 5;


	@isTest static void testLoadData_NormalPlan() {

		//SALESFORCE URL GIVES US THI INFO IN PRACTICE
		BI_PL_Preparation__c plan = [SELECT Id, BI_PL_Field_force__c, BI_PL_Country_code__c, BI_PL_Position_name__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Position__c, BI_PL_External_Id__c FROM BI_PL_Preparation__c LIMIT 1];

		//STEP 1 - LOAD CONFIG DATA BASED ON FIELD-FORCE
		String serializedSetupDate = BI_PL_PreparationExt.getSetupData(plan.BI_PL_Field_force__c);
		
		//STEP 2 (OLD) - LOADS PICKLIST VALUES FORM SOME FIELDS (TODO NOT_NEEDED!!)
		//BI_PL_RemotingServiceExt.getTargetAddedReasonOptions()

		//STEP 2 - GETS PLANS DATA
		BI_PL_PreparationExt.PlanitPreparationModel prep = BI_PL_PreparationExt.getPreparation(plan.Id); 

		//STEP 3 - GET VIEWS FOR TABLES
		BI_PL_PreparationExt.getTargetsView(plan.BI_PL_Field_force__c, plan.BI_PL_Country_code__c);

		//STEP 4 - GET TARGETS AND ALL ITS CHILDREN DATA IN CHUNKS BY ID. TARGET IDS COMES IN BI_PL_PreparationExt.PlanitPreparationModel MODEL
		String firstId = String.valueOf(prep.sortedTargetsId.get(0));
		String lastId = String.valueOf(prep.sortedTargetsId.get(prep.sortedTargetsId.size() - 1));
		BI_PL_PreparationExt.PlanitPreparationModel modelWithTargets = BI_PL_PreparationExt.prepareTargetsData(plan.Id, firstId, lastId);

		//STEP 5 - QUERY BUSINESS RULES
		// List<BI_PL_Business_rule__c> rules = BI_PL_PreparationExt.getCountryBusinessRules(plan.BI_PL_Country_code__c, plan.BI_PL_Field_force__c, plan.BI_PL_Position_cycle__r.BI_PL_Position__c);


		//////NOW THE PAGE LOAD IS COMPLETE. BEGIN USER ACTIONS

		// ACTION 1 - ADD NEW TARGET
		/// 1.1 Search for valid accounts
		List<BI_PL_PreparationExt.PlanitTargetModel> validAccounts = BI_PL_AddNewTargetModalCtrl.searchAccounts('test', '', '', plan.Id, plan.BI_PL_Country_code__c, true, plan.BI_PL_Position_name__c, true, '0', plan.BI_PL_Field_force__c);
		System.debug(loggingLevel.Error, '*** validAccounts: ' + validAccounts);
		BI_PL_PreparationExt.PlanitTargetModel tgtModel = validAccounts.get(0);	
		/// 1.2 Save in plan (there is not overlap)
		tgtModel = BI_PL_PreparationExt.saveNewTarget(tgtModel,  plan.BI_PL_Country_code__c, plan.BI_PL_Position_name__c, plan.BI_PL_Position_cycle__r.BI_PL_Cycle__c, plan.BI_PL_External_Id__c);

		//Test cobertura Sprint 5 OMEGA

		BI_PL_PreparationExt.getSetupDataMSL('test');
		//BI_PL_PreparationExt.PlanitChannelDetailModel.resetRelated();
		BI_PL_PreparationExt.getListOfColumns('Test');
		BI_PL_PreparationExt.getPrimaryAndSecondaryProductsForPosition('test1', 'test2');
		
		List<String> acc = new List<String>();
		for(Account act : [SELECT Id, External_ID_vod__c FROM Account]){
			acc.add(String.valueOf(act.Id));
		}
		BI_PL_PreparationExt.getPlanitAffiliationSiblingsAndChildren(acc, '');
		
		BI_PL_Target_Preparation__c asd = [SELECT Id, BI_PL_Target_Customer__r.External_ID_vod__c, BI_PL_Target_Customer__r.Id, BI_PL_External_id__c, BI_PL_Parent_external_id__c, BI_PL_Header__r.BI_PL_external_id__c FROM BI_PL_Target_Preparation__c LIMIT 1];
		BI_PL_Affiliation__c aff = [SELECT Id FROM BI_PL_Affiliation__c LIMIT 1];
		BI_PL_Channel_detail_preparation__c chandet = [SELECT Id FROM BI_PL_Channel_detail_preparation__c LIMIT 1];
		BI_PL_Preparation__c prepst = [SELECT Id,BI_PL_External_id__c, BI_PL_Position_cycle__c, BI_PL_position_Cycle__r.BI_PL_Confirmed__c FROM BI_PL_Preparation__c LIMIT 1];
		BI_PL_Cycle__c cycle2 = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];

		BI_PL_PreparationExt.PlanitTargetModel aler = new BI_PL_PreparationExt.PlanitTargetModel(asd);
		aler.getChannel('test');
		//aler.addTargetChannel(null);
		aler.addAffiliation(null);
		aler.resetAffiliationsPlanit();
		//aler.addAffiliationPlanit(aff);

		List<BI_PL_PreparationExt.PlanitTargetModel> aqua = new List<BI_PL_PreparationExt.PlanitTargetModel>();
		aqua.add(aler);
		Test.startTest();
		BI_PL_PreparationExt.saveChanges(prepst, aqua, prepst.BI_PL_External_id__c);
		BI_PL_PreparationExt.submitPreparation(String.valueOf(prepst.Id));
		Test.stopTest();


		Map<String, String> mapstri = new Map<String, String>();
		ApexPages.StandardController sc = new ApexPages.StandardController(new BI_PL_Preparation__c());
		BI_PL_PreparationExt est = new BI_PL_PreparationExt(sc);
		est.preparation_id = 'test';
		est.showEdit = true;
		est.multipleTerritories = true;
		est.sap_country_code = 'test';
		est.territory_selected = 'test';
		est.fieldForce = 'test';
		est.sapExternalId = 'test';
		est.edit_mode = true;
		est.detail_mode = true;
		est.create_mode = true;
		est.isDSM = true;
		est.isDS = true;
		est.segmentsColorMap = mapstri;
		est.segmentsMap = mapstri;
		est.deleteOptsMap = null;
		BI_PL_PreparationExt.getKAMTargetsForCycle(cycle2.Id, prepst.Id);
		BI_PL_PreparationExt.getRAENADTargetsForCycle(cycle2.Id, prepst.Id);
		
		est.sap_preparation = prepst;
		mapstri = est.deleteOptsMap;







		/*User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id = : UserInfo.getUserId()];
		countryCode = testUser.Country_Code_BI__c;

		Test.startTest();
		createTestData(countryCode);
		Test.stopTest();


		String cycleId = [SELECT Id FROM BI_PL_Cycle__c].Id;
		String currentPreparationId = preps.get(0).Id;
		String positionId = [SELECT BI_PL_Position_cycle__r.BI_PL_Position__c FROM BI_PL_Preparation__c WHERE Id = :currentPreparationId].BI_PL_Position_cycle__r.BI_PL_Position__c;

		BI_PL_PreparationExt.getSetupDataMSL(null);
		BI_PL_PreparationExt.getSetupData(null);

		// ========================================================== //
		// TEST : BI_PL_PreparationExt.getPrimaryAndSecondaryProductsForPosition
		// Position with no product pairs:
		String productPairId = products.get(0).Id + '' + products.get(1).Id;

		Map<String, BI_PL_PreparationExt.PlanitProductPair> productPairs = BI_PL_PreparationExt.getPrimaryAndSecondaryProductsForPosition([SELECT Id FROM BI_PL_Position__c WHERE Name = : POSITION_TEST2_NAME].Id, countryCode);
		System.assertEquals(0, productPairs.size(), 'The preparation shouldn\'t have products.');

		// Position with product pairs:
		productPairs = BI_PL_PreparationExt.getPrimaryAndSecondaryProductsForPosition([SELECT Id FROM BI_PL_Position__c WHERE Name = : POSITION_ROOT_NAME].Id, countryCode);
		System.assertEquals(productPairId, productPairs.get(productPairId).Id, 'The products for the position are wrong.');

		// ========================================================== //
		// TEST : BI_PL_PreparationExt.prepareTargetsData2
		BI_PL_PreparationExt.PlanitPreparationModel planitPreparationModel = BI_PL_PreparationExt.prepareTargetsData2(currentPreparationId, null);

		System.assertEquals(NACCOUNTS, planitPreparationModel.targets.size(), 'Wrong number of targets.');

		// TARGETS:
		for (Integer accountIndex = 0; accountIndex < NACCOUNTS; accountIndex++) {
			BI_PL_PreparationExt.PlanitTargetModel target = planitPreparationModel.targets.get(accountIndex);
			System.assertEquals(accounts.get(accountIndex).Id, target.record.BI_PL_Target_customer__c, 'Wrong account for target ' + accountIndex + '.');

			// CHANNELS:
			//System.assertEquals(1, target.targetChannels.size(), 'Wrong number of channels for target ' + target);

			// DETAILS
			for (String key : target.targetChannels.keySet()) {
				BI_PL_PreparationExt.PlanitChannelDetailModel channelDetail = target.targetChannels.get(key);
				System.assertEquals(NPRODUCTS, channelDetail.details.size(), 'Wrong number of details for channel ' + channelDetail);
			}
		}

		// ========================================================== //
		// TEST : BI_PL_PreparationExt.prepareTargetsData2

		Set<String> accountIds = new Set<String> ();

		for (Account a : accounts) {
			accountIds.add(a.Id);
		}

		List<BI_PL_PreparationExt.PlanitRelatedTargetModel> relateds = BI_PL_PreparationExt.getRelatedTargets(accountIds, countryCode, POSITION_ROOT_NAME, cycleId);
		// ========================================================== //
		// TEST : BI_PL_PreparationExt.getPlanitAffiliationSiblingsAndChildren
		BI_PL_TestDataFactory.createAffiliationRelationship(new List<Account> {accounts.get(1)}, accounts.get(0), countryCode);

		Map<String, List<Account>> childrenByParent = BI_PL_PreparationExt.getPlanitAffiliationSiblingsAndChildren(new List<String>(accountIds));

		// ========================================================== //
		// TEST : BI_PL_PreparationExt.getKAMTargetsForCycle

		BI_PL_PreparationExt.getKAMTargetsForCycle(cycleId, currentPreparationId);
		// ========================================================== //
		// TEST : BI_PL_PreparationExt.getTransferAndShareTargets

		List<Id> targetsId = new List<Id>();
		for (BI_PL_PreparationExt.PlanitTargetModel t : planitPreparationModel.targets)
			targetsId.add(t.record.Id);

		BI_PL_PreparationExt.getTransferAndShareTargets(targetsId, CHANNEL_REP_DETAIL_ONLY);
		// ========================================================== //
		// TEST : BI_PL_PreparationExt.getCountryBusinessRules
		BI_PL_PreparationExt.getCountryBusinessRules(countryCode, FIELDFORCE, positionId);
		// ========================================================== //
		// TEST : BI_PL_PreparationExt.saveChanges
		//BI_PL_PreparationExt.saveChanges(preps.get(0), planitPreparationModel.targets, null);
		// ========================================================== //
		// TEST : BI_PL_PreparationExt.saveNewTarget
		BI_PL_PreparationExt.saveNewTarget(new BI_PL_PreparationExt.PlanitTargetModel(new BI_PL_Target_preparation__c()), countryCode, POSITION_ROOT_NAME, cycleId, null);
*/
	}

	/*static BI_PL_Preparation__c createPreparation() {
		/*currentDate = Date.today();

		BI_PL_TestDataUtility.createCustomSettings();

		MC_Cycle_vod__c mcCycle = new MC_Cycle_vod__c(
		    Name = 'BI_PL_TestMCCycle',
		    Start_date_vod__c = currentDate,
		    End_date_vod__c = currentDate.addMonths(1),
		    Country_code_BI__c = countryCode,
		    External_Id_vod__c = 'BI_PL_TestMCCycle'
		);

		insert mcCycle;

		return preps.get(0);
		//return null;
	}*/

}