/**
 *  14-06-2017
 *  @author OMEGA CRM
 */
public with sharing class BI_PL_PreparationFromImmpactCtrl {

    public BI_PL_PlanitAdminCtrlWrapper adminControllerWrapper {get; set;}

    public List<SelectOption> lstHierarchies {get; set;}
    public List<SelectOption> lstCycles {get; set;}

    public Map<Id, BI_PL_Cycle__c> cycles {get; set;}

    public String selectedTargetCycle {get; set;}
    public String selectedTargetHierarchy {get; set;}

    public Boolean importAllowed {
        get{
            return String.isNotBlank(selectedTargetCycle) && String.isNotBlank(selectedTargetHierarchy);
        }
    }
    private BI_PL_Country_settings__c planitCountrySettings;

    public Boolean isImportFromIMMPACTAllowed {
        get {
            if (planitCountrySettings == null)
                loadPlanitSettings();
            return planitCountrySettings.BI_PL_Import_from_IMMPACT__c;
        }
    }
    private void loadPlanitSettings() {
        planitCountrySettings = [SELECT BI_PL_Channel__c, BI_PL_Multichannel__c, BI_PL_Clone__c, BI_PL_Import_from_BITMAN__c, BI_PL_Import_from_IMMPACT__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = : userCountryCode LIMIT 1];
    }


    private Id batchJobId;

    public String errors {get; set;}
    public Boolean loading {get; set;}
    public Boolean completed {get; set;}

    public String userCountryCode {get; set;}

    public BI_PL_PreparationFromImmpactCtrl() {
        // Setting common data
        system.debug('%%% setUserCountryCode()');
        setUserCountryCode();

        loadCycleOptions();
    }


    public void importFromIMMPACT() {
        completed = false;
        loading = true;
        errors = null;

        batchJobId = Database.executeBatch(new BI_PL_ImportIMMPACTHierarchyBatch(selectedTargetCycle, selectedTargetHierarchy));
    }

    /**
     *  Gets the user's country code.
     *  @author OMEGA CRM
     */
    private void setUserCountryCode() {
        Id currentUserId = UserInfo.getUserId();

        userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = : currentUserId LIMIT 1].Country_Code_BI__c;

        System.debug('%%% userCountryCode: ' + userCountryCode);
    }

    /**
     *  Loads the cycle options for the picklist.
     *  @author OMEGA CRM
     */
    private void loadCycleOptions() {
        lstCycles = new List<SelectOption>();
        lstCycles.add(new SelectOption('', Label.Common_None_vod));

        // Cycles that are active (start date < TODAY < end date)
        cycles = new Map<Id, BI_PL_Cycle__c>([SELECT Id, Name, BI_PL_Field_force__r.Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :userCountryCode]);

        for (BI_PL_Cycle__c c : cycles.values()) {
            lstCycles.add(new SelectOption(c.Id, c.Name));
        }
    }

    /**
     *  Loads the hierarchy options for the picklist.
     *	The hierarchies displayed are the ones that belong to the selected cycle and also the ones where the user is assigned (position cycle user)
     *  @author OMEGA CRM
     */
    private void loadHierarchyOptions() {
        lstHierarchies = new List<SelectOption>();
        lstHierarchies.add(new SelectOption('', Label.Common_None_vod));

        Set<String> hierarchies = new Set<String>();


		for(BI_PL_Position_cycle_user__c pcu : [SELECT BI_PL_Position_cycle__r.BI_PL_Hierarchy__c FROM BI_PL_Position_cycle_user__c
												WHERE BI_PL_User__c = :UserInfo.getUserId()
												  AND BI_PL_Position_cycle__r.BI_PL_Cycle__c =: selectedTargetCycle]){
			if (!hierarchies.contains(pcu.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c)) {
                lstHierarchies.add(new SelectOption(pcu.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, pcu.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c));
                hierarchies.add(pcu.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c);
            }
		}
/*
        for (BI_PL_Position_cycle__c c : [SELECT BI_PL_Hierarchy__c FROM BI_PL_Position_cycle__c
        									WHERE BI_PL_Cycle__c = :selectedTargetCycle
        									  AND BI_PL_Position_]) {
            if (!hierarchies.contains(c.BI_PL_Hierarchy__c)) {
                lstHierarchies.add(new SelectOption(c.BI_PL_Hierarchy__c, c.BI_PL_Hierarchy__c));
                hierarchies.add(c.BI_PL_Hierarchy__c);
            }
        }*/
    }
    /**
     * This method clears the list of preparations shown.
     * @author OmegaCRM
     */

    public void cycleChanged() {
        loadHierarchyOptions();
        selectedTargetHierarchy = null;
        completed = false;
        errors = null;
    }

    /**
     *  Loads the preparations as soon as the hierarchy is changed.
     *  @author OMEGA CRM
     */
    public void hierarchyChanged() {
        System.debug('hierarchyChanged ' + selectedTargetHierarchy);
        completed = false;
        errors = null;

    }

    /**
     *  Checks the status of the refresh visibiliy batch to display the results in the view.
     *  @author OMEGA CRM
     */
    public String checkBatchStatus() {
        if (batchJobId != null) {
            AsyncApexJob aaj = [SELECT Id, Status, ExtendedStatus From AsyncApexJob where Id = :batchJobId LIMIT 1];
            String batchStatus = aaj.Status;

            if (batchStatus == 'Completed') {
                loading = false;

                if (String.isNotBlank(aaj.ExtendedStatus)) {
                    errors = Label.BI_PL_Error_during_process;
                } else {
                    completed = true;
                }
            } else if (batchStatus == 'Failed' || batchStatus == 'Aborted') {
                errors = Label.BI_PL_Error_during_process;
                loading = false;
            }

        }
        return null;
    }
}