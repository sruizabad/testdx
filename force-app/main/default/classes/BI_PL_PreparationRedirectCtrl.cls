/**
 *	09/10/2017
 *	- Created date.
 *
 *	Redirects to a different Preparation page depending on the set MSL Fieldforces for the PLANiT country setting.
 *
 *	@author	OMEGA CRM
 */
public with sharing class BI_PL_PreparationRedirectCtrl {

	private static String ORIGINAL_PREPARATION_URL = '/apex/BI_PL_Preparation?id=';
	private static String MSL_PREPARATION_URL = '/apex/BI_PL_MSLPreparation?id=';
	private static String AMA_PREPARATION_URL = '/apex/BI_PL_AMAPreparation?id=';

	public BI_PL_Preparation__c preparation {get; set;}

	public BI_PL_PreparationRedirectCtrl(ApexPages.StandardController controller) {
		System.debug('BI_PL_PreparationRedirectCtrl');
		preparation = [SELECT Id, BI_PL_Country_code__c, BI_PL_Field_force__c FROM BI_PL_Preparation__c WHERE Id = :((BI_PL_Preparation__c)controller.getRecord()).Id];

		List<BI_PL_Preparation__c> prepList = [SELECT Id, BI_PL_Country_code__c, BI_PL_Field_force__c FROM BI_PL_Preparation__c];

		System.debug('**prepList ' + prepList);
	}

	public PageReference redirect() {
		System.debug('Redirect');

		//PageReference pageReference;
		//System.debug('**prep ' + preparation);
		//BI_PL_Country_settings__c settings = [SELECT BI_PL_MSL_view_fieldforces__c, BI_PL_AMA_view_fieldforces__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = :preparation.BI_PL_Country_code__c LIMIT 1];

		//System.debug('**preparation.BI_PL_Field_force__c ' + preparation.BI_PL_Field_force__c);
		//System.debug('**settings.BI_PL_AMA_view_fieldforces__c ' + settings);

		//if (isFieldforceInList(preparation.BI_PL_Field_force__c, settings.BI_PL_MSL_view_fieldforces__c))
		//	pageReference = new PageReference(MSL_PREPARATION_URL + preparation.Id);
		//else if (isFieldforceInList(preparation.BI_PL_Field_force__c, settings.BI_PL_AMA_view_fieldforces__c))
		//	pageReference = new PageReference(AMA_PREPARATION_URL + preparation.Id);
		//else
		//	pageReference = new PageReference(ORIGINAL_PREPARATION_URL + preparation.Id);

		//System.debug(pageReference);
		
		//pageReference.setRedirect(true);
		PageReference pageReference = new PageReference('/apex/BI_PL_Preparation?id='+preparation.Id);
		return pageReference;
	}

	private Boolean isFieldforceInList(String fieldForce, String fieldForces) {
		if (String.isBlank(fieldForces))
			return false;
		Set<String> fieldforcesList = new Set<String>(fieldForces.split(';'));
		return fieldforcesList.contains(fieldForce);
	}
}