@isTest
private class BI_PL_PreparationRedirectCtrl_Test {

	@isTest static void test_method_one() {
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting('US');

		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		String userCountryCode = 'US';

		BI_PL_TestDataFactory.createTestAccounts(6,userCountryCode);
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

		BI_PL_Preparation__c prep = BI_PL_TestDataFactory.preparation1;

		ApexPages.StandardController sc = new ApexPages.StandardController(prep);

		BI_PL_PreparationRedirectCtrl controller = new BI_PL_PreparationRedirectCtrl(sc);
		controller.redirect();
	}

	@isTest static void test_method_two(){
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting('US');

		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		String userCountryCode = 'US';

		BI_PL_TestDataFactory.createTestAccounts(6,userCountryCode);

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'MSL', BI_TM_Country_code__c = 'US');

		insert fieldForce;

		String l = usercountryCode + '_%';
		Integer numUser = 1;
		String name = 'TestUserDatafactory';
		List<User> listUser = new List<User>();

		Profile p = [SELECT Id FROM Profile WHERE Name LIKE :l LIMIT 1];

		for(Integer i = 0; i<numUser; i++){
			User u = new User(Alias = name.substring(0, 5)+i, Email = name + '@testorg.com', Country_Code_BI__c = usercountryCode,
		                EmailEncodingKey = 'UTF-8', LastName = name+i, LanguageLocaleKey = 'en_US', IsActive = true,
		                LocaleSidKey = 'en_US', ProfileId = p.id, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = name+'_Test_External_ID');
			listUser.add(u);
		}

		insert listUser;

		//Insert Cycle
		BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = usercountryCode, BI_PL_Start_date__c = Date.today(), BI_PL_End_date__c = Date.today() + 1, BI_PL_Field_force__c = fieldForce.Id);
		insert cycle;

		cycle = [SELECT Id, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c, Name, BI_PL_Field_force__r.Name FROM BI_PL_Cycle__c WHERE Id = : cycle.Id];

		System.debug('**data cycle ' + cycle);

		List<BI_PL_Position__c> positions = new List<BI_PL_Position__c>();
		List<BI_PL_Position_cycle_user__c> positionCycleUsers = new List<BI_PL_Position_cycle_user__c>();
		List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();

		//Insert Positions

		BI_PL_Position__c positionRoot2 = new BI_PL_Position__c(Name = 'Test12', BI_PL_Country_code__c = usercountryCode, BI_PL_Field_force__c = 'MSL');
		positions.add(positionRoot2);
		
		insert positions;

		//Insert Positions Cycle

		BI_PL_Position_cycle__c root = new BI_PL_Position_cycle__c(/*Name = 'root', */BI_PL_Position__c = positionRoot2.Id, BI_PL_Parent_position__c = null, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = 'testhierarchy');

		root.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, root.BI_PL_Hierarchy__c, positionRoot2.Name);

		insert root;
		

		//ADD Position Cycle Users
		//BI_PL_TestDataFactory.addPositionCycleUser(cycle, root, 'RootUser', positionRoot.Name);

		//insert getPositionCycleUsers();

		//Insert Preparations

		BI_PL_Preparation__c preparation1 = new BI_PL_Preparation__c(BI_PL_Country_code__c = usercountryCode, BI_PL_Position_cycle__c = root.Id);

		preparation1.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId(usercountryCode, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, 'testhierarchy', positionRoot2.Name,'MSL');

		preparations.add(preparation1);

     	insert preparations;

     	ApexPages.StandardController sc = new ApexPages.StandardController(preparation1);
		BI_PL_PreparationRedirectCtrl controller = new BI_PL_PreparationRedirectCtrl(sc);

		controller.redirect();
	}
	
}