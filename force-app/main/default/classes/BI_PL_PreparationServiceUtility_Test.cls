/**
 *	19/09/2017
 *	- BI_PL_Specialty__c from BusinessRule now is a picklist instead of a Lookup.
*/
@isTest
private class BI_PL_PreparationServiceUtility_Test {

	public static String countryCode = 'US';
	private static Account account15;
	
	@isTest
	static void testGetCurrentUserPermissions(){

		List<User> oTest = BI_PL_TestDataFactory.CreateTestUsersProfile(1, 'US', '%SALES_MANAGER');
		System.runas(oTest.get(0)){
		List<PermissionSet> ps = [SELECT Id,IsOwnedByProfile,Label, Name FROM PermissionSet WHERE NAME like 'BI_PL_SALES_MANAGER'];
		PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps.get(0).Id, AssigneeId = UserInfo.getUserId());
		insert psa; 
		Map<String, Boolean> perm = BI_PL_PreparationServiceUtility.getCurrentUserPermissions();


		System.assertEquals(perm.get('isSM'), true);

		}

	}
	@isTest
	static void testfindFieldForces(){
		String countrycode2;
		BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(countryCode);
        BI_PL_TestDataFactory.createCycleStructure(countryCode);
        List<Account> listAcc = BI_PL_TestDataFactory.createTestAccounts(4,countryCode);
        List<Product_vod__c> listProd = BI_PL_TestDataFactory.createTestProduct(3,countryCode);
        list<BI_PL_Position_cycle__c> posCycles = [select id, BI_PL_External_id__c from BI_PL_Position_cycle__c];
        BI_PL_TestDataFactory.createPreparations(countryCode,posCycles, listAcc, listProd);

		BI_PL_Cycle__c cycle = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];
		BI_PL_Position_cycle__c hierarchyName = [select Id, Name, BI_PL_Hierarchy__c from BI_PL_Position_cycle__c limit 1];

		BI_PL_PreparationServiceUtility.findFieldForcesInCycle(countryCode, cycle.Id, hierarchyName.BI_PL_Hierarchy__c);
		BI_PL_PreparationServiceUtility.findFieldForcesInCycle(countrycode2, cycle.Id, hierarchyName.BI_PL_Hierarchy__c);

		BI_PL_PreparationServiceUtility.findFieldForcesInBITMAN(countryCode);
		BI_PL_PreparationServiceUtility.findFieldForcesInBITMAN(countrycode2);
		//BI_PL_PreparationServiceUtility.findSpecialtiesInCycle(countryCode, cycle.Id, hierarchyName.BI_PL_Hierarchy__c);
		//BI_PL_PreparationServiceUtility.findSpecialtiesInCycle(countrycode2, cycle.Id, hierarchyName.BI_PL_Hierarchy__c);

		BI_PL_PreparationServiceUtility.findSpecialtiesInVeeva('fieldPath',countryCode);
		BI_PL_PreparationServiceUtility.findSpecialtiesInVeeva('fieldPath',countryCode2);
		BI_PL_PreparationServiceUtility.getCurrentUser();

		
		List<BI_PL_Detail_preparation__c> listpreps = [SELECT Id, BI_PL_Channel_detail__r.BI_PL_Reviewed__c, BI_PL_Channel_detail__r.BI_PL_Removed__c, BI_PL_Channel_detail__r.BI_PL_Rejected__c, 
                BI_PL_Channel_detail__r.BI_PL_Edited__c, BI_PL_Added_Manually__c, BI_PL_Planned_details__c, BI_PL_Adjusted_details__c FROM BI_PL_Detail_preparation__c];
		
		System.debug('LISTPREPS SIZE :: ' + listpreps.size() + ' :: ' + listpreps);
		BI_PL_PreparationServiceUtility.getToVeevaAddValue(listpreps[0]);
		BI_PL_PreparationServiceUtility.getToVeevaAddValue(true, false, false, false, true, 1.0, 1.0);
		BI_PL_PreparationServiceUtility.getToVeevaAddValue(true, false, false, true, true, 1.0, 1.0);
		BI_PL_PreparationServiceUtility.getToVeevaAddValue(true, true, false, true, true, 1.0, 1.0);
		BI_PL_PreparationServiceUtility.getToVeevaAddValue(false, false, false, false, true, 1.0, 1.0);
		BI_PL_PreparationServiceUtility.getToVeevaAddValue(false, false, false, false, false, 1.0, 1.0);
		
		BI_PL_Channel_detail_preparation__c targ = [SELECT Id, BI_PL_Target__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__c FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycle.Id LIMIT 1];
		String targety = targ.BI_PL_Target__c;
		String callplan = targ.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__c;

		//BI_PL_PreparationServiceUtility.getCountDetailsQuery('SELECT Id FROM', '', string.valueof(cycle.Id), hierarchyName.BI_PL_Hierarchy__c,'rep_detail_only', targety, callplan);
		BI_PL_PreparationServiceUtility.getDetailFilters(string.valueof(cycle.Id), hierarchyName.BI_PL_Hierarchy__c,'rep_detail_only', null, null);
		BI_PL_PreparationServiceUtility.getTargetFilters(string.valueof(cycle.Id), hierarchyName.BI_PL_Hierarchy__c,'rep_detail_only', targety);
		//BI_PL_PreparationServiceUtility.getDifferentProductsInCycle(string.valueof(cycle.Id), hierarchyName.BI_PL_Hierarchy__c,'rep_detail_only', targety, callplan);

		//string xmple = ' SELECT Id, BI_PL_Product__c, BI_PL_Secondary_product__c, BI_PL_Adjusted_details__c, BI_PL_Planned_details__c FROM BI_PL_Detail_preparation__c ';
		Map<String, String> maps = new Map<String, String>();
		//BI_PL_PreparationServiceUtility.getPaginatedDetailsQuery('SELECT Id FROM BI_PL_Cycle__c WHERE ', '', true, string.valueof(cycle.Id), hierarchyName.BI_PL_Hierarchy__c,'rep_detail_only', targety, callplan,2, null, maps);
		//BI_PL_PreparationServiceUtility.getPaginatedDetailsQueryWithDirection(null, '', true, string.valueof(cycle.Id), hierarchyName.BI_PL_Hierarchy__c,'rep_detail_only', targety, callplan,2, null, maps, false);
		//BI_PL_PreparationServiceUtility.getPaginatedDetailsQueryWithDirection(null, '', true, string.valueof(cycle.Id), hierarchyName.BI_PL_Hierarchy__c,'rep_detail_only', targety, callplan,2, 'qwer', maps, false);
		//BI_PL_PreparationServiceUtility.getPaginatedDetailsQueryWithDirection(null, '', true, string.valueof(cycle.Id), hierarchyName.BI_PL_Hierarchy__c,'rep_detail_only', targety, callplan,2, 'qwer', maps, true);
		//BI_PL_PreparationServiceUtility.getPaginatedDetailsQueryWithDirection(null, '', true, string.valueof(cycle.Id), hierarchyName.BI_PL_Hierarchy__c,'rep_detail_only', targety, callplan,2, null, maps, true);
		//BI_PL_PreparationServiceUtility.updateDetailsWithVeevaIterations(listpreps);
	}

	@isTest 
	static void testGenerationExternalId() {
		String countryCode2 = 'BR';
		String hierarchy = 'h1';
		String positionName = 'pos1';
		String fieldforce = 'fieldforce';
		Date startDate1 = Date.newInstance(2017, 01, 01);
		Date endDate1 = Date.newInstance(2017, 01, 31);

		//with zeros
		String extid1 = BI_PL_PreparationServiceUtility.generatePreparationExternalId(countryCode2, startDate1, endDate1, fieldforce, hierarchy, positionName);
		System.assertEquals('BR_20170101_20170131_fieldforce_h1_pos1', extid1);

		//Without zeros
		Date startDate2 = Date.newInstance(2017, 1, 1);
		Date endDate2 = Date.newInstance(2017, 1, 31);
		String extid2 = BI_PL_PreparationServiceUtility.generatePreparationExternalId(countryCode2, startDate2, endDate2, fieldforce, hierarchy, positionName);
		System.assertEquals('BR_20170101_20170131_fieldforce_h1_pos1', extid2);
		Contact c = new Contact(LastName = 'testContact', Email = 'testContact@test.test');
		Object resultC = BI_PL_PreparationServiceUtility.getFieldValue(c, 'Name');
		Object resultC2 = BI_PL_PreparationServiceUtility.getFieldValue(c, 'Account.Email');
	}

	@isTest 
	static void testGetActiveBusinessRules(){
		String ff = '';
		BI_PL_Position__c position1 = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = countryCode);
		String positionId = position1.Id;
		BI_PL_PreparationServiceUtility.getActiveBusinessRules(countryCode, ff, positionId);
	}

	@isTest
	static void testGetBusinessRulesByType(){
		String ruleType = 'Customer Speciality';
		String ruleType2 = 'Forbidden Product';
		String ruleType3 = 'Primary and Secondary';
		String indexBy = 'Id';
		String channel = 'rep_detail_only';
		String channel2 = '';
		List<String> ruleTypeList = new List<String>();
		ruleTypeList.add(ruleType);
		//ruleTypeList.add(ruleType2);
		List<String> ruleTypeList2 = new List<String>();

		BI_PL_Position__c position1 = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = countryCode);
		insert position1;
		String positionId = position1.Id;

		List<Product_vod__c> pradaxa = BI_PL_TestDataFactory.createTestProduct(1, 'US');

		Customer_Attribute_BI__c customer = new Customer_Attribute_BI__c(Name = 'Customer Speciality', External_Id_BI__c = 'oTest_Ext_Id_br', Type_BI__c = 'ACCT_BI Specialty');
		insert customer;

		Customer_Attribute_BI__c forbProd = new Customer_Attribute_BI__c(Name = 'Forbidden Product', External_Id_BI__c = 'oTest_Ext_Id_br_nf', Type_BI__c = 'ACCT_IND Type');
		insert forbProd;		

		Customer_Attribute_BI__c primSec = new Customer_Attribute_BI__c(Name = 'Primary and Secondary', External_Id_BI__c = 'oTest_Ext_Id_br_ps', Type_BI__c = 'ACCT_IND Type');
		insert primSec;

		BI_PL_Business_Rule__c br = new BI_PL_Business_Rule__c(BI_PL_Active__c = true, BI_PL_Type__c = 'Customer Speciality', BI_PL_Country_code__c = 'US', BI_PL_Channel__c = 'rep_detail_only', BI_PL_Product__c = pradaxa.get(0).Id, BI_PL_Specialty__c = customer.Name, BI_PL_External_Id__c = 'br_Test_ExtId_Test');
		insert br;

		/*BI_PL_Business_Rule__c br2 = new BI_PL_Business_Rule__c(BI_PL_Active__c = true, BI_PL_Type__c = 'Forbidden Product', BI_PL_Country_code__c = 'US', BI_PL_Channel__c = 'rep_detail_only', BI_PL_Product__c = pradaxa.get(0).Id, BI_PL_Specialty__c = forbProd.Id, BI_PL_External_Id__c = 'br_Test_ExtId_Test2');
		insert br2;*/		

		BI_PL_Business_Rule__c br3 = new BI_PL_Business_Rule__c(BI_PL_Active__c = true, BI_PL_Type__c = 'Primary and Secondary', BI_PL_Country_code__c = 'US', BI_PL_Channel__c = 'rep_detail_only', BI_PL_Product__c = pradaxa.get(0).Id, BI_PL_Specialty__c = primSec.Name, BI_PL_External_Id__c = 'br_Test_ExtId_Test3', BI_PL_Position__c = positionId);
		insert br3;

		BI_PL_PreparationServiceUtility.getBusinessRulesByType(countryCode, ruleType, indexBy, channel, positionId);
		BI_PL_PreparationServiceUtility.getBusinessRulesByType(countryCode, ruleType, indexBy, channel);
		BI_PL_PreparationServiceUtility.getBusinessRulesByType(countryCode, ruleTypeList, indexBy, channel2, positionId);
		BI_PL_PreparationServiceUtility.getBusinessRulesDefinedAndNotForbidden(countryCode, channel2, positionId);
		BI_PL_PreparationServiceUtility.getBusinessRulesDefinedAndNotForbidden(countryCode, channel, positionId);
	}
	
}