public with sharing class BI_PL_PreparationTargetAdjModalCtrl {

	private static final String NO_SEGMENTATION = 'No Segmentation';
	private static final String SEGMENT_COLOR = 'Grey';


	@RemoteAction
	public static BI_PL_PreparationExt.PlanitChannelDetailModel addNewDetail(String channel, BI_PL_PreparationExt.PlanitTargetModel target, String preparationExtId, String channelDetailExternalId, Product_vod__c primaryProduct, Product_vod__c secondaryProduct, Integer actual) {

		System.debug(loggingLevel.Error, '*** target: ' + target);

		
		BI_PL_PreparationExt.PlanitChannelDetailModel channelModel = new BI_PL_PreparationExt.PlanitChannelDetailModel(new BI_PL_Channel_detail_preparation__c(BI_PL_Channel__c = channel, BI_PL_Edited__c = true));
		
		channelModel.addDetail( new BI_PL_Detail_preparation__c(
		    BI_PL_Business_rule_ok__c = true,
		    BI_PL_Added_manually__c = true,
		    BI_PL_Adjusted_details__c = actual,
		    BI_PL_Planned_details__c = 0,
		    BI_PL_Product__c = primaryProduct.Id,
		    BI_PL_Product__r = primaryProduct,
		    BI_PL_Secondary_product__c = (secondaryProduct != null) ? secondaryProduct.Id : null,
		    BI_PL_Secondary_product__r = (secondaryProduct != null) ? secondaryProduct : null,
		    BI_PL_Column__c = 0,
		    BI_PL_Row__c = 0,
		   
		    BI_PL_Market_target__c = false,
		    BI_PL_Fdc__c = false,
		    BI_PL_Commit_target__c = false,
		    BI_PL_Prescribe_target__c = false
		 ));

		//BI_PL_PreparationExt.PlanitTargetModel model = new BI_PL_PreparationExt.PlanitTargetModel(new BI_PL_Target_Preparation__c(Id = targetId));
		BI_PL_PreparationExt.PlanitTargetModel model = new BI_PL_PreparationExt.PlanitTargetModel(target.record);
		model.addTargetChannel(channelModel);

		BI_PL_PreparationExt.saveModels(new List<BI_PL_PreparationExt.PlanitTargetModel> {model}, preparationExtId);
		System.debug('MODEL ' + model);
		//BI_PL_Detail_preparation__c detail = new BI_PL_Detail_preparation__c(
		//    BI_PL_Channel_detail__c = channelDetailId,
		//    BI_PL_Business_rule_ok__c = true,
		//    BI_PL_Added_manually__c = true,
		//    BI_PL_Adjusted_details__c = 0,
		//    BI_PL_Planned_details__c = 0,
		//    BI_PL_Product__c = primaryProduct.Id,
		//    BI_PL_Product__r = primaryProduct,
		//    BI_PL_Secondary_product__c = (secondaryProduct != null) ? secondaryProduct.Id : null,
		//    BI_PL_Secondary_product__r = (secondaryProduct != null) ? secondaryProduct : null,
		//    BI_PL_Column__c = 0,
		//    BI_PL_Row__c = 0,
		//    BI_PL_Segment__c = NO_SEGMENTATION,
		//    BI_PL_Market_target__c = false,
		//    BI_PL_Fdc__c = false,
		//    BI_PL_Commit_target__c = false,
		//    BI_PL_Prescribe_target__c = false);

		//if (secondaryProduct == null) {
		//	detail.BI_PL_External_id__c = channelDetailExternalId + '_' + primaryProduct.Id;
		//} else {
		//	detail.BI_PL_External_id__c = channelDetailExternalId + '_' + primaryProduct.Id + secondaryProduct.Id;
		//}

		//insert detail;

		channelModel.addDetails(target.currentTargetChannel.returnDetails());

		return channelModel;
		//return new BI_PL_PreparationExt.PlanitChannelDetailModel(BI_PL_PreparationExt.getChannelDetails(new Set<Id> {model.record.Id}, channel).get(0), true);
	}
}