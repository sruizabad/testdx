@isTest
private class BI_PL_PreparationTargetAdjModalCtrl_Test {
	
	@isTest static void test_method_one() {
		String channel = 'rep_detail_only';
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		String countryCode = testUser.Country_Code_BI__c;
		List<Account> accList = BI_PL_TestDataFactory.createTestAccounts(2, countryCode);
		List<Product_vod__c> prodList = BI_PL_TestDataFactory.createTestProduct(2, countryCode);
		BI_PL_TestDataFactory.createCycleStructure(countryCode);
		List<BI_PL_Position_Cycle__c> posCycList = [SELECT Id, BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
		BI_PL_TestDataFactory.createPreparations(countryCode, posCycList, accList, prodList);
		BI_PL_Target_Preparation__c targetPrep = [SELECT Id,BI_PL_Target_customer__c, BI_PL_Target_customer__r.Id, BI_PL_External_id__c FROM BI_PL_Target_Preparation__c LIMIT 1];
		String targetId = targetPrep.Id;
		String channelDetailId = [SELECT Id FROM BI_PL_Channel_detail_Preparation__c WHERE BI_PL_Target__c =: targetId LIMIT 1].Id;
		String channelDetailExternalId = [SELECT Id, BI_PL_External_Id__c FROM BI_PL_Channel_detail_Preparation__c WHERE BI_PL_Target__c =: targetId LIMIT 1].BI_PL_External_Id__c;
		BI_PL_PreparationExt.PlanitTargetModel model = new BI_PL_PreparationExt.PlanitTargetModel(targetPrep);
		BI_PL_PreparationTargetAdjModalCtrl.addNewDetail(channel, model, channelDetailId, channelDetailExternalId, prodList.get(0), prodList.get(1), 0);
		BI_PL_PreparationTargetAdjModalCtrl.addNewDetail(channel, model, channelDetailId, channelDetailExternalId, prodList.get(0), null, 0);


	}
	
}