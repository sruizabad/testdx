/**
 *	Utility methods for the Preparation object.
 *	@author	OMEGA CRM
 */
public with sharing class BI_PL_PreparationUtility {
	public static final String THRESHOLD_RULE_TYPE = 'Threshold';
	
	/**
	 *	Returns the calculated total capacity for the Preparation.
	 *	@author	OMEGA CRM
	 */
	public static Decimal calculateTotalCapacity(BI_PL_Preparation__c preparation, Decimal businessRuleVisitsPerDay) {
		Decimal repCapacity = 0;

		if (preparation.BI_PL_Capacity_adjustment_approved__c) {
			repCapacity = preparation.BI_PL_Global_capacity__c - preparation.BI_PL_Capacity_adjustment__c;
		} else {
			repCapacity = preparation.BI_PL_Global_capacity__c;
		}

		return businessRuleVisitsPerDay * repCapacity;
	}

	/**
	 *	Returns the visits per day business rule value for the provided country.
	 *	@author	OMEGA CRM
	 */
	public static Integer getVisitsPerDayPerThreshold(String countryCode) {
		List<BI_PL_Business_rule__c> brs = new List<BI_PL_Business_rule__c>([SELECT Id, BI_PL_Visits_per_day__c, BI_PL_Field_force__c
		        FROM BI_PL_Business_rule__c
		        WHERE BI_PL_Active__c = true
		                                AND BI_PL_Country_code__c = :countryCode
		                                        AND BI_PL_Type__c = :THRESHOLD_RULE_TYPE]);

		System.debug('brs: ' + brs.size() + ' cc:' + countryCode);


		if (brs.size() == 0) {
			throw new BI_PL_Exception(Label.BI_PL_No_Threshold_Business_rules);
		}else if (brs.size() == 1) {
			return (Integer)brs.get(0).BI_PL_Visits_per_day__c;
		}
		
		throw new BI_PL_Exception(Label.BI_PL_Multiple_Threshold_Business_rules);
		
		return null;
	}

	/**
	 *	Returns the visits per day business rule value for the provided country and Field Force.
	 *	@author	OMEGA CRM
	 */

	public static Map<String, Decimal> getVisitsPerDayPerThreshold(String countryCode, Set<String> ff) {
		Map<String, Decimal> visitsByFF = new Map<String, Decimal>();

		for(BI_PL_Business_rule__c br : [SELECT Id, BI_PL_Visits_per_day__c, BI_PL_Field_force__c
		        FROM BI_PL_Business_rule__c
		        WHERE BI_PL_Active__c = true
                AND BI_PL_Country_code__c = :countryCode
                AND BI_PL_Type__c = :THRESHOLD_RULE_TYPE
                AND BI_PL_Field_force__c IN :ff]) {

			visitsByFF.put(br.BI_PL_Field_force__c, br.BI_PL_Visits_per_day__c);
		}

		return visitsByFF;
	}
}