@isTest
public with sharing class BI_PL_PreparationUtility_Test {
	@isTest
	public static void test() {

		BI_PL_Business_rule__c br = new BI_PL_Business_rule__c(BI_PL_Visits_per_day__c = 20,
		        BI_PL_Country_code__c = 'BR',
		        BI_PL_Type__c = BI_PL_PreparationUtility.THRESHOLD_RULE_TYPE,
		        BI_PL_Active__c = true,
		        BI_PL_Field_force__c='KAM');

		insert br;

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'KAM', BI_TM_Country_Code__c = 'BR');
		insert fieldForce;

		BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'BR',
		        BI_PL_Global_capacity__c = 10,
		        BI_PL_Start_date__c = Date.newInstance(2018, 1, 1),
		        BI_PL_End_date__c = Date.newInstance(2018, 1, 31),
		        BI_PL_Field_force__c = fieldForce.Id);

		insert cycle;

		BI_PL_Position__c position = new BI_PL_Position__c(Name = 'Test', BI_PL_Country_code__c = 'BR');

		insert position;

		BI_PL_Position_cycle__c positionCycle = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position.Id,
		        BI_PL_External_id__c = 'test', BI_PL_Hierarchy__c = 'test');

		insert positionCycle;

		BI_PL_Preparation__c preparation = new BI_PL_Preparation__c(BI_PL_Country_code__c = 'BR', BI_PL_External_id__c = 'Test',
		        BI_PL_Capacity_adjustment_approved__c = false,
		        BI_PL_Position_cycle__c = positionCycle.Id,
		        BI_PL_Capacity_adjustment__c = 10);

		insert preparation;

		Integer businessRuleVisitsPerDay = (Integer)br.BI_PL_Visits_per_day__c;

		System.assertEquals(br.BI_PL_Visits_per_day__c, BI_PL_PreparationUtility.getVisitsPerDayPerThreshold('BR'), 'The visits per day are wrong');

	
		preparation = [SELECT Id, BI_PL_Capacity_adjustment_approved__c,
		               BI_PL_Global_capacity__c,
		               BI_PL_Capacity_adjustment__c FROM BI_PL_Preparation__c WHERE Id = : preparation.Id];

		System.assertEquals(preparation.BI_PL_Global_capacity__c * businessRuleVisitsPerDay,
		                    BI_PL_PreparationUtility.calculateTotalCapacity(preparation, businessRuleVisitsPerDay), 'The total capacity is wrong.');

		preparation.BI_PL_Capacity_adjustment_approved__c = true;

		update preparation;

		System.assertEquals((preparation.BI_PL_Global_capacity__c - preparation.BI_PL_Capacity_adjustment__c) * businessRuleVisitsPerDay,
		                    BI_PL_PreparationUtility.calculateTotalCapacity(preparation, businessRuleVisitsPerDay), 'The total capacity is wrong.');

		Set<String> setff = new Set<String>{'KAM'};
		Map<String, Decimal> visitsByFF = new Map<String, Decimal>{fieldforce.Name=>br.BI_PL_Visits_per_day__c};
				
		System.assertEquals(visitsByFF, BI_PL_PreparationUtility.getVisitsPerDayPerThreshold('BR',setff), 'The visits per day are wrong');
		
	}
}