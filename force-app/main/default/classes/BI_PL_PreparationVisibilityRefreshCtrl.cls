/**
 *  15-06-2017
 *  @author OMEGA CRM
 */
public without sharing class BI_PL_PreparationVisibilityRefreshCtrl {

    public BI_PL_PlanitAdminCtrlWrapper adminControllerWrapper {get; set;}

    public List<BI_PL_Preparation__c> preparations {
        get{
            return adminControllerWrapper.controller.lstToShowPreparations;
        }
        set {
            adminControllerWrapper.controller.lstToShowPreparations = value;
        }
    }

    public List<SelectOption> lstHierarchies {get; set;}
    public List<SelectOption> lstCycles {get; set;}
    public Map<Id, BI_PL_Cycle__c> cycles {get; set;}
    public String selectedHierarchy {get; set;}
    public String selectedCycle {get; set;}

    private Id refreshVisibilityBatchId;

    public String errors {get; set;}
    public Boolean loading {get; set;}
    public Boolean completed {get; set;}

    public String userCountryCode {get; set;}

    public Boolean refreshAllowed {
        get{
            return String.isNotBlank(selectedCycle) && String.isNotBlank(selectedHierarchy) && preparations.size() > 0;
        }
    }

    public BI_PL_PreparationVisibilityRefreshCtrl() {
        // Setting common data
        system.debug('%%% setUserCountryCode()');
        setUserCountryCode();

        loadCycleOptions();
    }

    /**
     *  Calls the refresh visibility batch.
     * @author Omega CRM Consulting
     */
    public void refreshVisibility() {
        System.debug('refreshVisibility');
        completed = false;
        loading = true;
        errors = null;
        refreshVisibilityBatchId = Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(selectedCycle, selectedHierarchy, false), 10);
    }

    /**
     *  Gets the user's country code.
     *  @author OMEGA CRM
     */
    private void setUserCountryCode() {
        Id currentUserId = UserInfo.getUserId();

        userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = : currentUserId LIMIT 1].Country_Code_BI__c;

        System.debug('%%% userCountryCode: ' + userCountryCode);
    }

    /**
     *  Loads the cycle options for the picklist.
     *  @author OMEGA CRM
     */
    private void loadCycleOptions() {
        lstCycles = new List<SelectOption>();
        lstCycles.add(new SelectOption('', Label.Common_None_vod));

        cycles = new Map<Id, BI_PL_Cycle__c>([SELECT Id, Name, BI_PL_Field_force__r.Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :userCountryCode ORDER BY Name]);

        for (BI_PL_Cycle__c c : cycles.values()) {
            lstCycles.add(new SelectOption(c.Id, c.Name));
        }
    }

    /**
     *  Loads the hierarchy options for the picklist.
     *  @author OMEGA CRM
     */
    private void loadHierarchyOptions() {
        lstHierarchies = new List<SelectOption>();
        lstHierarchies.add(new SelectOption('', Label.Common_None_vod));

        Set<String> hierarchies = new Set<String>();
        for (BI_PL_Position_cycle__c c : [SELECT BI_PL_Hierarchy__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = :selectedCycle ORDER BY BI_PL_Hierarchy__c]) {
            if (!hierarchies.contains(c.BI_PL_Hierarchy__c)) {
                lstHierarchies.add(new SelectOption(c.BI_PL_Hierarchy__c, c.BI_PL_Hierarchy__c));
                hierarchies.add(c.BI_PL_Hierarchy__c);
            }
        }

    }
    /**
     * This method clears the list of preparations shown.
     * @author OmegaCRM
     */

    public void cycleChanged() {
        if(!preparations.isEmpty()) preparations.clear();
        loadHierarchyOptions();
        completed = false;
        errors = null;
        selectedHierarchy = null;
    }

    /**
     *  Loads the preparations as soon as the hierarchy is changed.
     *  @author OMEGA CRM
     */
    public void hierarchyChanged() {
        System.debug('hierarchyChanged ' + selectedHierarchy);
        loadPreparations();
        completed = false;
        errors = null;
    }

    /**
     * Retrieves the preparations to be shown according to the selected hierarchy and cycle.
     * @author OmegaCRM
     */
    public void loadPreparations() {
        preparations = [SELECT Id, Name, BI_PL_Country_code__c, BI_PL_Position_name__c, BI_PL_Status__c, BI_PL_Start_date__c, BI_PL_End_date__c, OwnerId,
                        BI_PL_position_cycle__r.BI_PL_hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name,
                        BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                        BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c, BI_PL_Position_cycle__r.BI_PL_Synchronized__c, Owner.Name
                        FROM BI_PL_Preparation__c
                        WHERE BI_PL_Country_code__c = : userCountryCode
                                AND BI_PL_position_cycle__r.BI_PL_hierarchy__c = : selectedHierarchy
                                        AND BI_PL_position_cycle__r.BI_PL_Cycle__c = : selectedCycle];
    }

    /**
     *  Checks the status of the refresh visibiliy batch to display the results in the view.
     *  @author OMEGA CRM
     */
    public String checkBatchStatus() {
        if (refreshVisibilityBatchId != null) {
            AsyncApexJob aaj = [SELECT Id, Status, ExtendedStatus From AsyncApexJob where Id = :refreshVisibilityBatchId LIMIT 1];
            String batchStatus = aaj.Status;

            if (batchStatus == 'Completed') {
                loading = false;

                if (String.isNotBlank(aaj.ExtendedStatus)) {
                    errors = Label.BI_PL_Error_during_process;
                } else {
                    completed = true;
                }
            } else if (batchStatus == 'Failed' || batchStatus == 'Aborted') {
                errors = Label.BI_PL_Error_during_process;
                loading = false;
            }
        }
        return null;
    }
}