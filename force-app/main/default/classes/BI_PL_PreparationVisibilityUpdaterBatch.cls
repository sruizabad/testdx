/**
 * 27/07/2017
 *	- Send an email notifying about the failed preparations and assign the default country owner to them.
 * @author OMEGA CRM
 */
global class BI_PL_PreparationVisibilityUpdaterBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {

	private String cycle;
	private String hierarchy;
	private Boolean autoSync = false;
	private List<String> nextCycles;
	private List<String> listHierarchies;

	global List<BI_PL_Preparation__c> failedPreps = new List<BI_PL_Preparation__c>();
	global Integer nPreparations = 0;

	global BI_PL_PreparationVisibilityUpdaterBatch() {}

	global BI_PL_PreparationVisibilityUpdaterBatch(String cycle, String hierarchy, Boolean autoSync) {
		System.debug('BI_PL_PreparationVisibilityUpdaterBatch ' + cycle + ' - ' + hierarchy);
		this.cycle = cycle;
		this.hierarchy = hierarchy;
		this.autoSync = autoSync;
		this.nextCycles = null;
		this.listHierarchies = null;
	}

	global BI_PL_PreparationVisibilityUpdaterBatch(String cycle, String hierarchy, Boolean autoSync, List<String> nextCycles, List<String> listHierarchies) {
		System.debug('BI_PL_PreparationVisibilityUpdaterBatch ' + cycle + ' - ' + hierarchy);
		this.cycle = cycle;
		this.hierarchy = hierarchy;
		this.autoSync = autoSync;
		this.nextCycles = nextCycles;
		this.listHierarchies = listHierarchies;
	}
	global Database.QueryLocator start(Database.BatchableContext BC) {
		this.cycle = this.cycle != null ? this.cycle : this.cycleIds[0];
		this.hierarchy = this.hierarchy != null ? this.hierarchy : this.hierarchyNames[0];
		this.listHierarchies = this.listHierarchies != null 
			? this.listHierarchies 
			: (this.hierarchyNames.isEmpty() ? null : this.hierarchyNames);

		return Database.getQueryLocator(BI_PL_PreparationVisibilityUtility.generatePreparationsQuery(this.cycle, this.hierarchy));
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('**scope size ' + scope.size());
		System.debug('BI_PL_PreparationVisibilityUpdaterBatch ' + cycle + ' - ' + hierarchy);
		failedPreps.addAll(BI_PL_PreparationVisibilityUtility.updateSharingSettingsForPreparationsSet(new Set<BI_PL_Preparation__c>((List<BI_PL_Preparation__c>)scope)));
		nPreparations += scope.size();
	}

	global void finish(Database.BatchableContext BC) {
		System.debug('BI_PL_PreparationVisibilityUpdaterBatch finish ' + autoSync + ' - ' + this.hierarchy + ' - ' + this.cycle);

		// Send an email notifying about the failed preparations.
		sendEmailWithPreps(failedPreps);

		if (autoSync &&  !Test.isRunningTest() ) {
			Set<String> preparationsId = new Set<String>();
			for (BI_PL_Preparation__c p : [SELECT Id FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = : hierarchy AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycle]) {
				preparationsId.add(p.Id);
			}
			BI_PL_PlanitAdminCtrl.synchronizePreparations('', new List<String>(preparationsId));
		}

		
		if (this.nextCycles != null && !Test.isRunningTest() ) {
			if (this.nextCycles.size() > 0) {
				String lastHierarchy = this.listHierarchies.get(this.listHierarchies.size() - 1);

				if (this.hierarchy == lastHierarchy) {
					String newCycle = this.nextCycles.remove(0);
					Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(newCycle, this.listHierarchies.get(0), false, this.nextCycles, this.listHierarchies));
				} else {
					Integer index = this.listHierarchies.indexOf(this.hierarchy);
					if (index != -1) {
						Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(this.cycle, this.listHierarchies.get(index + 1), false, this.nextCycles, this.listHierarchies));
					}
				}
			}else{
				if ( this.listHierarchies.size() > 0) {

					String lastHierarchy = this.listHierarchies.get(this.listHierarchies.size() - 1);

					Integer index = this.listHierarchies.indexOf(this.hierarchy);

					if (index != -1 && this.listHierarchies.get(index) != lastHierarchy) {
						Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(this.cycle, this.listHierarchies.get(index + 1), false, this.nextCycles, this.listHierarchies));
					}
				}

			}

		} else if (this.listHierarchies != null &&  !Test.isRunningTest() ) {
			if ( this.listHierarchies.size() > 0) {

				String lastHierarchy = this.listHierarchies.get(this.listHierarchies.size() - 1);

				Integer index = this.listHierarchies.indexOf(this.hierarchy);

				if (index != -1 && this.listHierarchies.get(index) != lastHierarchy) {
					Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(this.cycle, this.listHierarchies.get(index + 1), false, this.nextCycles, this.listHierarchies));
				}
			}
		}

	}

	public void sendEmailWithPreps(List<BI_PL_Preparation__c> errorPreparations) {
		System.debug('sendEmailWithPreps cycle: ' + cycle + ' hierarchy: ' + hierarchy + ' errorPreparations ' + errorPreparations);
		String subject;
		String body = '<p>Cycle Id: ' + cycle + ', Hierarchy: ' + hierarchy + '</p>';
		if (errorPreparations.size() > 0) {
			subject = 'Update owner failed for some preparations';
			body += 'The ownership of the following preparations couldn\'t be updated with the user specified in the hierarchy: ';
			for (BI_PL_Preparation__c p : errorPreparations) {
				body += '<br/><ul>';
				body += '<li/><a href=\'' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + p.Id + '\' >' + p.Name + '</a> <a href=\'' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + p.BI_PL_Position_cycle__c + '\' > (' + p.BI_PL_Position_cycle__r.BI_PL_Position__r.Name + ') </a></li>';
				body += '</ul>';
			}
			body += '<p>The owner of all previous preparations was set to the default country owner.</p>';
		} else {
			subject = 'Update owner process completed successfully';
		}
		body += '<p>' + (nPreparations - errorPreparations.size()) + ' / ' + nPreparations + ' preparations updated successfully.</p>';

		body = '<h2>' + subject + '</h2>' + '<br/>' + body;
		sendEmail(subject, body);
	}

	public void sendEmail(String subject, String body) {
		try {
			BI_PL_EmailUtility.sendEmailWithTemplate(new List<String> {UserInfo.getUserEmail()}, subject, body);
		} catch (exception e) {
			System.debug('Unable to send email: ' + e.getMessage());
		}
	}

}