/**
 *	19/10/2017
 *	- "Database.insert" instead of "insert"
 *	27/07/2017
 *	- GLOS-353 : when a user has no visibility over the PLANiT objects, set instead the default country owner and notify through email.
 *	04/07/2017
 *	- Exceptions in Custom labels.
 *
 *	Contains the main functionality to keep the Preparations visibility according to the hierarchies and cycles.
 *	@author Omega CRM
 */
public without sharing class BI_PL_PreparationVisibilityUtility {

	public static final String ACCESS_LEVEL_EDIT = 'Edit';
	public static final String ROW_CAUSE_MANUAL = 'Manual';

	private static final String CYCLE_TYPE_ACTIVE = 'active';

	private static List<BI_PL_Preparation__c> failedPreparations = new List<BI_PL_Preparation__c>();

	/**
	 * Updates the preparations' sharing settings for the cycle and hierarchy specified.
	 * @return Returns the failed preparations to update the ownership.
	 * @author Omega CRM
	*/
	public static List<BI_PL_Preparation__c> updateSharingSettingsForPreparations(Id cycle, String hierarchy) {
		// TODO : Check if used, otherwise remove.
		return updateSharingSettingsForPreparationsSet(new Set<BI_PL_Preparation__c>([SELECT Id, Name, BI_PL_Position_cycle__c, BI_PL_Position_cycle__r.BI_PL_Position__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Type__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Position__c, BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, OwnerId FROM BI_PL_Preparation__c
		        WHERE BI_PL_Position_Cycle__r.BI_PL_Cycle__c = :cycle
		                AND BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchy ]));
	}

	public static String generatePreparationsQuery(String cycle, String hierarchy) {
		if (String.isBlank(cycle))
			throw new BI_PL_Exception(Label.BI_PL_Cycle_null);

		if (String.isBlank(hierarchy))
			throw new BI_PL_Exception(Label.BI_PL_Hierarchy_null);

		return 'SELECT Id, Name, BI_PL_Position_cycle__c, BI_PL_Position_cycle__r.BI_PL_Position__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Type__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Position__c, BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, OwnerId FROM BI_PL_Preparation__c WHERE BI_PL_Position_Cycle__r.BI_PL_Cycle__c = \'' + cycle + '\' AND BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = \'' + hierarchy + '\'';
	}
	/**
	 * Updates the sharing settings for the specified preparations.
	 * @return Returns the failed preparations to update the ownership.
	 * @author Omega CRM
	*/
	public static List<BI_PL_Preparation__c> updateSharingSettingsForPreparationsSet(Set<BI_PL_Preparation__c> preparations) {
		Map<Id, Map<String, Set<BI_PL_Preparation__c>>> preparationsByHierarchyAndCycle = new Map<Id, Map<String, Set<BI_PL_Preparation__c>>>();

		for (BI_PL_Preparation__c p : preparations) {
			if (!preparationsByHierarchyAndCycle.containsKey(p.BI_PL_Position_cycle__r.BI_PL_Cycle__c)) {
				preparationsByHierarchyAndCycle.put(p.BI_PL_Position_cycle__r.BI_PL_Cycle__c, new Map<String, Set<BI_PL_Preparation__c>>());
			}

			if (!preparationsByHierarchyAndCycle.get(p.BI_PL_Position_cycle__r.BI_PL_Cycle__c).containsKey(p.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c)) {
				preparationsByHierarchyAndCycle.get(p.BI_PL_Position_cycle__r.BI_PL_Cycle__c).put(p.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, new Set<BI_PL_Preparation__c>());

			}
			preparationsByHierarchyAndCycle.get(p.BI_PL_Position_cycle__r.BI_PL_Cycle__c).get(p.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c).add(p);
		}

		System.debug('updateSharingSettingsForPreparations preparations' + preparations);

		Set<Id> preparationsId = new Set<Id>();
		for (BI_PL_Preparation__c p : preparations) {
			preparationsId.add(p.Id);
		}

		if (!preparationsId.isEmpty()) {
			deletePreparationsSharing(preparationsId);
		}

		Set<BI_PL_Preparation__c> preparationsToUpdate = new Set<BI_PL_Preparation__c>();
		List<BI_PL_Preparation__Share> sharesToInsert = new List<BI_PL_Preparation__Share>();

		Id defaultCountryOwnerId = null;
		BI_PL_VisibilityTree tree;

		for (Id cycle : preparationsByHierarchyAndCycle.keySet()) {
			for (String hierarchy : preparationsByHierarchyAndCycle.get(cycle).keySet()) {
				tree = BI_PL_PreparationVisibilityUtility.generateVisibilityTree(hierarchy, cycle);

				Set<BI_PL_Preparation__c> preps = preparationsByHierarchyAndCycle.get(cycle).get(hierarchy);

				preparationsToUpdate.addAll(BI_PL_PreparationVisibilityUtility.changePreparationsOwnership(preps, tree, false));
				// Insert the new permissions:
				sharesToInsert.addAll(new List<BI_PL_Preparation__Share>(generateSharingSettingsForPreparations(preps, tree)));
			}
		}

		updatePreparations(new List<BI_PL_Preparation__c>(preparationsToUpdate), tree.defaultCountryOwnerId);
		Database.insert(sharesToInsert, false);

		return failedPreparations;
	}

	/**
	 *	Returns the BI_PL_Preparation__Share settings related to the specified preparations based on the provided visibility tree.
	 *	@author Omega CRM
	 *	@return Set of all BI_PL_Preparation__Share to insert.
	 */
	public static Set<BI_PL_Preparation__Share> generateSharingSettingsForPreparations(Set<BI_PL_Preparation__c> preparations, BI_PL_VisibilityTree tree) {
		System.debug('generateSharingSettingsForPreparations');
		Set<BI_PL_Preparation__Share> sharingSettings = new Set<BI_PL_Preparation__Share>();
		for (BI_PL_Preparation__c p : preparations) {
			sharingSettings.addAll(generateSharingSettingsForPreparation(p, tree));
		}
		System.debug('sharingSettings' + sharingSettings);

		return sharingSettings;
	}

	// ----------------- DATA RETRIEVING FROM GENERATED TREE ------------------ //

	/**
	 * Generates the sharing settings for a single preparation.
	 * @author Omega CRM
	 * @return Set of BI_PL_Preparation__Share for the preparation.
	*/
	public static Set<BI_PL_Preparation__Share> generateSharingSettingsForPreparation(BI_PL_Preparation__c preparation, BI_PL_VisibilityTree tree) {
		BI_PL_PositionWrapper position = new BI_PL_PositionWrapper(null, preparation.BI_PL_Position_cycle__r.BI_PL_Position__c);
		BI_PL_PreparationWrapper p = new BI_PL_PreparationWrapper(preparation.Id, preparation, position);
		return generateSharingSettingsForPreparation(p, tree);
	}
	/**
	 * Generates the sharing settings for a single preparation.
	 * @author Omega CRM
	 * @return Set of BI_PL_Preparation__Share for the preparation.
	*/
	public static Set<BI_PL_Preparation__Share> generateSharingSettingsForPreparation(BI_PL_PreparationWrapper preparation, BI_PL_VisibilityTree tree) {
		return generateSharingSettingsForPreparation(preparation, tree.getNode(preparation.position.recordId));
	}

	/**
	 * Generates the sharing settings for a single preparation from the root position cycle "w".
	 * @author Omega CRM
	 * @return Set of BI_PL_Preparation__Share for the preparation.
	*/
	private static Set<BI_PL_Preparation__Share> generateSharingSettingsForPreparation(BI_PL_PreparationWrapper preparation, BI_PL_PositionCycleWrapper w) {
		System.debug('w ' + w);
		Set<BI_PL_Preparation__Share> output = new Set<BI_PL_Preparation__Share>();

		if (w != null && w.positionCycleUsers != null) {
			for (BI_PL_Position_Cycle_User__c pcu : w.positionCycleUsers) {

				Boolean isCycleHidden = preparation.record.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Type__c != CYCLE_TYPE_ACTIVE;

				if (String.isBlank(pcu.BI_PL_User__c))
					throw new BI_PL_Exception(Label.BI_PL_User_for_position_cycle_null + ' ' + pcu.Id);

				System.debug('preparation.record.OwnerId ' + preparation.record.OwnerId + ' - ' + pcu.BI_PL_User__c + ' - ' + (preparation.record.OwnerId == pcu.BI_PL_User__c));

				if (preparation.record.OwnerId != pcu.BI_PL_User__c && !isCycleHidden) {
					output.add(new BI_PL_Preparation__Share(ParentId = preparation.recordId, UserOrGroupId = pcu.BI_PL_User__c, AccessLevel = ACCESS_LEVEL_EDIT, RowCause = ROW_CAUSE_MANUAL));
				}
			}
		}
		return output;
	}
	// ---------------------- TREE GENERATION -------------------------- //

	/**
	 *	Generates the visibility tree for the provided hierarchy and cycle, where each node is a BI_PL_PositionCycleWrapper.
	 *	@author Omega CRM
	 *	@return Map where key = positionId.
	 */
	public static BI_PL_VisibilityTree generateVisibilityTree(String hierarchy, Id cycle) {
		return generateTree(hierarchy, cycle);
	}

	/**
	 *	Generates the visibility tree where each node is a BI_PL_PositionCycleWrapper for the provided position and cycle.
	 *	@author Omega CRM
	 *	@return BI_PL_VisibilityTree for the cycle and hierarchy
	 */
	public static BI_PL_VisibilityTree generateTree(String hierarchy, Id cycle) {
		System.debug('generateTree hierarchy: ' + hierarchy + ' cycle: ' + cycle);

		return new BI_PL_VisibilityTree(cycle, hierarchy);
	}

	/**
	 *	Changes the ownership of the preparations based on the rep for each tree node.
	 *	@author Omega CRM
	 *	@return All the received Preparations, but with the ownerId changed.
	 */
	public static Set<BI_PL_Preparation__c> changePreparationsOwnership(Set<BI_PL_Preparation__c> preparations, BI_PL_VisibilityTree tree, Boolean dmlUpdate) {
		for (BI_PL_Preparation__c p : preparations) {

			Boolean isCycleHidden = p.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Type__c != CYCLE_TYPE_ACTIVE;

			System.debug('****************changePreparationsOwnership' + p + ' - ' + p.Name);
			System.debug('****************p.BI_PL_Position__c ' + p.BI_PL_Position_cycle__r.BI_PL_Position__c);

			// If there's no rep set the country default owner:
			String newOwnerId = tree.getPositionRep(p.BI_PL_Position_cycle__r.BI_PL_Position__c);
			System.debug('****************changePreparationsOwnership newOwnerId ' + newOwnerId);
			System.debug('****************changePreparationsOwnership defaultCountryOwnerId ' + tree.defaultCountryOwnerId);

			if (isCycleHidden) {
				p.OwnerId = tree.defaultCountryOwnerId;
			} else {
				p.OwnerId = newOwnerId;
			}

			if (p.OwnerId == null)
				throw new BI_PL_Exception(Label.BI_PL_Preparation_owner_null_default_country);
		}

		if (dmlUpdate)
			updatePreparations(new List<BI_PL_Preparation__c>(preparations), tree.defaultCountryOwnerId);

		return preparations;
	}

	/**
	 *	Updates the preparations list and fills the failedPreparations with the records that have failed in the process.
	 *	@author	OMEGA CRM
	 */
	public static void updatePreparations(List<BI_PL_Preparation__c> preparations, Id defaultCountryOwnerId) {
		failedPreparations = new List<BI_PL_Preparation__c>();

		List<Database.SaveResult> results = Database.update(preparations, false);
		for (Integer i = 0; i < results.size(); i++) {
			Database.SaveResult result = results[i];
			if (!result.isSuccess()) {
				// For the failed records assign the default country owner:
				BI_PL_Preparation__c origRecord = preparations.get(i);
				origRecord.OwnerId = defaultCountryOwnerId;
				failedPreparations.add(origRecord);
			}
		}
		update failedPreparations;
	}

	/**
	 *	Deletes the preparation sharing settings for the specified preparations.
	 *	@author Omega CRM
	 */
	private static void deletePreparationsSharing(Set<Id> preparationsId) {
		delete [SELECT Id FROM BI_PL_Preparation__share WHERE ParentId IN: preparationsId AND RowCause = 'Manual'];
	}

	/**
	*	Wraps the BI_PL_Preparation__c record.
	*	@author Omega CRM
	*/
	class BI_PL_PreparationWrapper {

		public BI_PL_Preparation__C record {get; set;}
		public String recordId {get; set;}
		public BI_PL_PositionWrapper position {get; set;}

		public BI_PL_PreparationWrapper(String recordId, BI_PL_Preparation__C record, BI_PL_PositionWrapper position) {
			this.recordId = recordId;
			this.record = record;
			this.position = position;
		}
	}
}