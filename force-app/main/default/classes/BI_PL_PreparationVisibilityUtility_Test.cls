@isTest
public with sharing class BI_PL_PreparationVisibilityUtility_Test {

	private static Map<String, User> users;
	private static Map<Id, User> usersById;
	private static String hierarchy;
	private static BI_PL_Cycle__c cycle;
	private static String userCountryCode;

	private static BI_PL_Position__c positionRoot;
	private static BI_PL_Position__c positionChild1;
	private static BI_PL_Position__c positionChild2;
	private static BI_PL_Position__c positionChild21;

	private static BI_PL_Position_cycle__c root;
	private static BI_PL_Position_cycle__c child1;
	private static BI_PL_Position_cycle__c child2;
	private static BI_PL_Position_cycle__c child21;

	private static BI_PL_Preparation__c preparation1;
	private static BI_PL_Preparation__c preparation2;
	private static BI_PL_Preparation__c preparation3;
	private static BI_PL_Preparation__c preparation4;

	private static Map<String, List<BI_PL_Position_cycle_user__c>> positionCycleUsersByPosition = new Map<String, List<BI_PL_Position_cycle_user__c>>();
	private static Map<Id, BI_PL_Preparation__c> preparationsMap = new Map<Id, BI_PL_Preparation__c>();

	@isTest
	public static void disabledUserNotIncluded() {

		//-----------------------------//
		// >> Hierarchy
		//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(1)
		//-----------------------------//

		// This method tests that disabled users are not considered in the refresh visibility process:

		basicTestRecordsCreation();


		System.assertEquals(2, [SELECT Id FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c = : child21.Id].size(), 'The number of position cycle users for the position cycle "child21" is not expected.');

		// Delete the position child 21 user to leave the PositionChild21InactiveUser alone in the node.
		delete [SELECT Id FROM BI_PL_Position_cycle_user__c WHERE BI_PL_User__c = : getUser('PositionChild21User', users).Id];

		System.assertEquals(1, [SELECT Id FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c = : child21.Id].size(), 'The number of position cycle users for the position cycle "child21" is not expected.');

		// Check if the only user for the position cycle is the inactive:
		System.assertEquals(getUserId('PositionChild21InactiveUser', users), [SELECT BI_PL_User__c FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c = : child21.Id LIMIT 1][0].BI_PL_User__c, 'The position cycle user is not the expected one.');

		Test.startTest();
		Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycle.Id, hierarchy, false));
		Test.stopTest();

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(4);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		// Now the default country user is the owner (because the user 'PositionChild21InactiveUser' is inactive in the position):
		//checkOwnership([SELECT Id, UserName from User where Id =: UserInfo.getUserId() LIMIT 1].Username, [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {});

	}
	@isTest
	public static void positionCycleUserUpdateTest() {
		//-----------------------------//
		// >> Hierarchy
		//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(1)
		//-----------------------------//
		// In this test method we switch the users in child21 with the ones in child2.

		basicTestRecordsCreation();

		BI_PL_PreparationVisibilityUtility.updateSharingSettingsForPreparations(cycle.Id, hierarchy);

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(4);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation3});

		// Change visibility between child21 and child2:
		List<BI_PL_Position_cycle_user__c> pcus = [SELECT Id, BI_PL_Position_cycle__c FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c = : child2.Id OR BI_PL_Position_cycle__c = : child21.Id];


		for (BI_PL_Position_cycle_user__c pcu : pcus) {
			if (pcu.BI_PL_Position_cycle__c == child2.Id) {
				pcu.BI_PL_Position_cycle__c = child21.Id;
			} else {
				pcu.BI_PL_Position_cycle__c = child2.Id;
			}
		}
		update pcus;


		Test.startTest();
		Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycle.Id, hierarchy, false));
		Test.stopTest();

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(4);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation2, preparation3});

	}

	@isTest
	public static void positionCycleUserDeleteTest() {
		//-----------------------------//
		// >> Hierarchy
		//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(1)
		//-----------------------------//
		// In this test method we delete the user assigned to child21.

		basicTestRecordsCreation();

		BI_PL_PreparationVisibilityUtility.updateSharingSettingsForPreparations(cycle.Id, hierarchy);

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(4);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation3});


		delete [SELECT Id, BI_PL_Position_cycle__c FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c = : child21.Id];


		Test.startTest();
		Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycle.Id, hierarchy, false));
		Test.stopTest();

		BI_PL_VisibilityTree tree = BI_PL_PreparationVisibilityUtility.generateVisibilityTree(hierarchy, cycle.Id);

		refreshPreparationsMap();
		// Sharing settings checking:
		//checkNumberOfManualAndOwnerShares(3);

		// The default countryOwner is 'RootUser'
		Id countryDefaultOwner = BI_PL_PreparationVisibilityUtility.generateTree(hierarchy, cycle.Id).defaultCountryOwnerId;

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership(countryDefaultOwner, [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {});

	}

	@isTest
	public static void positionCycleUserInsertTest() {
		//-----------------------------//
		// >> Hierarchy
		//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(1)
		//-----------------------------//

		basicTestRecordsCreation();

		BI_PL_PreparationVisibilityUtility.updateSharingSettingsForPreparations(cycle.Id, hierarchy);

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(4);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation3});
		checkUserVisibility('PositionChild3User', new List < BI_PL_Preparation__c > {});

		// INSERT a new user in the child21 node:
		insert addPositionCycleUser(cycle, child21, 'PositionChild3User', positionChild21.Name);

		Test.startTest();
		Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycle.Id, hierarchy, false));
		Test.stopTest();

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(5);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation3});
		checkUserVisibility('PositionChild3User', new List < BI_PL_Preparation__c > {preparation3});

	}

	@isTest
	public static void positionCycleUpdateTest() {
		//-----------------------------//
		// >> Original hierarchy
		//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(1)
		//-----------------------------//
		// In this test method we change the tree structure by updating the child2 node parent.

		basicTestRecordsCreation();

		BI_PL_PreparationVisibilityUtility.updateSharingSettingsForPreparations(cycle.Id, hierarchy);

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(4);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation3});

		// UPDATE
		//
		//			root(1)
		//		    /
		//		child1(1)
		//			\
		//        child2(1)
		//		      \
		//          child21(1)
		//-----------------------------//
		child2.BI_PL_Parent_position__c = positionChild1.Id;
		update child2;

		Test.startTest();
		Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycle.Id, hierarchy, false));
		Test.stopTest();

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(6);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation3});
	}

	@isTest
	public static void positionCycleDeleteTest() {
		//-----------------------------//
		// >> Original hierarchy
		//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(1)
		//-----------------------------//
		// In this test method we change the tree structure by updating the child2 node parent.

		basicTestRecordsCreation();

		BI_PL_PreparationVisibilityUtility.updateSharingSettingsForPreparations(cycle.Id, hierarchy);

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(4);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation3});

		// UPDATE
		//
		//			root(1)
		//		    /
		//		child1(1)
		//			\
		//        child2(1)
		//		      \
		//          child21(1)
		//-----------------------------//
		child2.BI_PL_Parent_position__c = positionChild1.Id;
		update child2;

		// DELETE
		//
		//			root(1)
		//		    /
		//		child1(0)
		//			\
		//        child2(1)
		//-----------------------------//

		delete new List<BI_PL_Preparation__c> {preparation3, preparation4};
		delete positionCycleUsersByPosition.get(child21.Id);
		delete child21;

		Test.startTest();
		Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycle.Id, hierarchy, false));
		Test.stopTest();

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(2);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation2});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2});

	}

	@isTest
	public static void preparationUpdateTest() {
		//-----------------------------//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(1)
		//-----------------------------//

		basicTestRecordsCreation();

		// Switch the child21 preparation with the one in child1

		preparation3.BI_PL_Position_cycle__c = child1.Id;
		preparation4.BI_PL_Position_cycle__c = child21.Id;
		update new List<BI_PL_Preparation__c> {preparation3, preparation4};

		Test.startTest();
		Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycle.Id, hierarchy, false));
		Test.stopTest();

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(4);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation3});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation4});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation4});

	}

	@isTest
	public static void preparationDeleteTest() {
		//-----------------------------//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(1)
		//-----------------------------//

		basicTestRecordsCreation();

		// DELETE preparation3
		//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(0)
		//-----------------------------//

		delete preparation3;

		Test.startTest();
		Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycle.Id, hierarchy, false));
		Test.stopTest();

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(2);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {});
	}

	@isTest
	public static void databaseRecordsVisibilityTest() {
		//-----------------------------//
		// >> Hierarchy
		//
		//			root(1)
		//		    /     \
		//	   child1(1)  child2(1)
		//		              \
		//                  child21(1)
		//-----------------------------//

		basicTestRecordsCreation();

		Test.startTest();
		Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycle.Id, hierarchy, false));
		Test.stopTest();

		refreshPreparationsMap();
		// Sharing settings checking:
		checkNumberOfManualAndOwnerShares(4);

		// Preparations ownership checking:
		checkOwnership('RootUser', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation1.Id].OwnerId);
		checkOwnership('PositionChild2User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation2.Id].OwnerId);
		checkOwnership('PositionChild1User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation4.Id].OwnerId);
		checkOwnership('PositionChild21User', [SELECT OwnerId FROM BI_PL_Preparation__c WHERE Id = :preparation3.Id].OwnerId);

		// User visibility checking
		checkUserVisibility('RootUser', new List < BI_PL_Preparation__c > {preparation1, preparation2, preparation3, preparation4});
		checkUserVisibility('PositionChild1User', new List < BI_PL_Preparation__c > {preparation4});
		checkUserVisibility('PositionChild2User', new List < BI_PL_Preparation__c > {preparation2, preparation3});
		checkUserVisibility('PositionChild21User', new List < BI_PL_Preparation__c > {preparation3});

	}

	private static void checkNumberOfManualAndOwnerShares(Integer manual) {
		List<BI_PL_Preparation__share> manualShares = [SELECT Id, ParentId, UserOrGroupId FROM BI_PL_Preparation__share WHERE RowCause = 'Manual'];
		List<BI_PL_Preparation__share> ownerShares = [SELECT Id, ParentId, UserOrGroupId FROM BI_PL_Preparation__share WHERE RowCause = 'Owner'];

		System.assertEquals(manual, manualShares.size(), 'Wrong number of \'Manual\' share records.');
		System.assertEquals(preparationsMap.size(), ownerShares.size(), 'Wrong number of \'Owner\' share records.');
	}

	private static void basicTestRecordsCreation() {
		users = usersCreation();

		BI_PL_TestDataFactory.createCountrySetting();



		//insert new BI_PL_Country_Settings__c(Name = 'Brazil', BI_PL_Country_Code__c = 'BR', BI_PL_Default_owner_user_name__c = getUser('RootUser', users).UserName, BI_PL_Specialty_field__c = 'Specialty_1_vod__c');
		//insert new BI_PL_Country_Settings__c(Name = 'US', BI_PL_Country_Code__c = 'US', BI_PL_Default_owner_user_name__c = getUser('RootUser', users).UserName, BI_PL_Specialty_field__c = 'Specialty_1_vod__c');
		//BI_PL_TestDataFactory.createCountrySetting();

		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        userCountryCode = userCountry.Country_Code_BI__c;
		hierarchy = 'TestHierarchy';


		/*BI_PL_Country_Settings__c countrySettings = [SELECT Id, BI_PL_Default_owner_user_name__c FROM BI_PL_Country_Settings__c WHERE BI_PL_Country_Code__c = :userCountryCode LIMIT 1];
		countrySettings.BI_PL_Default_owner_user_name__c = 'RootUser';
		update countrySettings;*/



		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'TestFieldForce', BI_TM_Country_code__c = userCountryCode);

		insert fieldForce;

		cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'userCountryCode', BI_PL_Start_date__c = Date.today(), BI_PL_End_date__c = Date.today() + 1, BI_PL_Field_force__c = fieldForce.Id);

		insert cycle;

		List<BI_PL_Position__c> positions = new List<BI_PL_Position__c>();
		List<BI_PL_Position_cycle_user__c> positionCycleUsers = new List<BI_PL_Position_cycle_user__c>();
		List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();

		positionRoot = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = userCountryCode);
		positionChild1 = new BI_PL_Position__c(Name = 'Test2', BI_PL_Country_code__c = userCountryCode);
		positionChild2 = new BI_PL_Position__c(Name = 'Test3', BI_PL_Country_code__c = userCountryCode);
		positionChild21 = new BI_PL_Position__c(Name = 'Test4', BI_PL_Country_code__c = userCountryCode);

		positions.add(positionRoot);
		positions.add(positionChild1);
		positions.add(positionChild2);
		positions.add(positionChild21);

		insert positions;


		cycle = [SELECT Id, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c, Name, BI_PL_Field_force__r.Name FROM BI_PL_Cycle__c WHERE Id = : cycle.Id];


		root = new BI_PL_Position_cycle__c(/*Name = 'root', */BI_PL_Position__c = positionRoot.Id, BI_PL_Parent_position__c = null, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = hierarchy);

		root.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, root.BI_PL_Hierarchy__c, positionRoot.Name);

		insert root;

		child1 = new BI_PL_Position_cycle__c(/*Name = 'child1', */BI_PL_Position__c = positionChild1.Id, BI_PL_Parent_position__c = positionRoot.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = hierarchy);

		child1.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child1.BI_PL_Hierarchy__c, positionChild1.Name);

		insert child1;

		child2 = new BI_PL_Position_cycle__c(/*Name = 'child2', */BI_PL_Position__c = positionChild2.Id, BI_PL_Parent_position__c = positionRoot.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = hierarchy);

		child2.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child2.BI_PL_Hierarchy__c, positionChild2.Name);

		insert child2;

		child21 = new BI_PL_Position_cycle__c(/*Name = 'child21', */BI_PL_Position__c = positionChild21.Id, BI_PL_Parent_position__c = positionChild2.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = hierarchy);

		child21.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child21.BI_PL_Hierarchy__c, positionChild21.Name);

		insert child21;

		addPositionCycleUser(cycle, root, 'RootUser', positionRoot.Name);
		addPositionCycleUser(cycle, child1, 'PositionChild1User', positionChild1.Name);
		addPositionCycleUser(cycle, child2, 'PositionChild2User', positionChild2.Name);
		addPositionCycleUser(cycle, child21, 'PositionChild21User', positionChild21.Name);
		addPositionCycleUser(cycle, child21, 'PositionChild21InactiveUser', positionChild21.Name);

		insert getPositionCycleUsers();

		preparation1 = new BI_PL_Preparation__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Position_cycle__c = root.Id);
		preparation2 = new BI_PL_Preparation__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Position_cycle__c = child2.Id);
		preparation3 = new BI_PL_Preparation__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Position_cycle__c = child21.Id);
		preparation4 = new BI_PL_Preparation__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Position_cycle__c = child1.Id);

		preparation1.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId(userCountryCode, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchy, positionRoot.Name,'TestFieldForce');
		preparation2.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId(userCountryCode, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchy, positionChild2.Name,'TestFieldForce');
		preparation3.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId(userCountryCode, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchy, positionChild21.Name,'TestFieldForce');
		preparation4.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId(userCountryCode, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, hierarchy, positionChild1.Name,'TestFieldForce');

		preparations.add(preparation1);
		preparations.add(preparation2);
		preparations.add(preparation3);
		preparations.add(preparation4);

		insert preparations;


	}

	private static void refreshPreparationsMap() {
		preparationsMap = new Map<Id, BI_PL_Preparation__c>([SELECT Id, Name, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Position__c, OwnerId FROM BI_PL_Preparation__c]);
	}

	private static List<BI_PL_Position_cycle_user__c> getPositionCycleUsers() {
		List<BI_PL_Position_cycle_user__c> output = new List<BI_PL_Position_cycle_user__c>();
		for (List<BI_PL_Position_cycle_user__c> l : positionCycleUsersByPosition.values())
			output.addAll(l);
		return output;
	}

	private static BI_PL_Position_cycle_user__c addPositionCycleUser(BI_PL_Cycle__c cycle, BI_PL_Position_cycle__c positionCycle, String userId, String positionName) {
		if (!positionCycleUsersByPosition.containsKey(positionCycle.Id))
			positionCycleUsersByPosition.put(positionCycle.Id, new List<BI_PL_Position_cycle_user__c>());

		BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_User__c = getUserId(userId, users));

		pcu.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycle, positionCycle.BI_PL_Hierarchy__c, positionName, getUser(userId, users).External_id__c);

		positionCycleUsersByPosition.get(positionCycle.Id).add(pcu);
		return pcu;
	}

	private static void checkOwnership(String u, Id ownerId) {
		System.assertEquals(getUserId(u, users), ownerId, 'Preparation owner wrong. [[Expected: ' + u + ' | Actual: ' + usersById.get(ownerId) + ']]');
	}
	private static void checkOwnership(Id u, Id ownerId) {
		System.assertEquals(u, ownerId, 'Preparation owner wrong. [[Expected: ' + u + ' | Actual: ' + ownerId + ']]');
	}

	private static void checkUserVisibility(String u, List<BI_PL_Preparation__c> expectedPreparations) {
		System.runAs(getUser(u, users)) {
			Map<Id, BI_PL_Preparation__c> userPreparations = new Map<Id, BI_PL_Preparation__c>([SELECT Id, Name, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Position__c FROM BI_PL_Preparation__c]);

			Integer matchedPreparations = 0;
			for (BI_PL_Preparation__c p : expectedPreparations) {
				if (userPreparations.containsKey(p.Id))
					matchedPreparations++;
			}
			System.assertEquals(expectedPreparations.size(), userPreparations.size(), 'The number of preparations that ' + u + ' sees is not expected.');
			System.assertEquals(userPreparations.size(), matchedPreparations, 'The preparations that ' + u + ' sees are not right.');
		}
	}

	private static User getUser(String lastName, Map<String, User> u) {
		return users.get(lastName);
	}
	private static String getUserId(String lastName, Map<String, User> u) {
		return String.valueOf(getUser(lastName, users).Id).substring(0, 15);
	}

	private static Map<String, User> usersCreation() {
		usersById = new Map<Id, User>();
		Map<String, User> output = new Map<String, User>();
		String profileName = 'BR' +'_%';
		System.debug('******* profileName: ' + profileName);
		Profile p = [SELECT Id FROM Profile WHERE Name LIKE :profileName LIMIT 1];

		List<User> usersList = new List<User>();
		usersList.add(createUser('RootUser', p.Id));
		usersList.add(createUser('PositionChild1User', p.Id));
		usersList.add(createUser('PositionChild2User', p.Id));
		usersList.add(createUser('PositionChild21User', p.Id));
		usersList.add(createUser('PositionChild3User', p.Id));


		PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name = 'BI_PL_SALES'];
		insert usersList;

		for (User u : usersList) {
			output.put(u.LastName, u);
			usersById.put(u.Id, u);

			System.runAs(new User(Id = Userinfo.getUserId(), UserName = 'username_' + Math.random() + '@u.com', Country_Code_BI__c = 'BR')) {
				insert createPermissionSetAssignment(u.Id, permissionSet.Id);
			}
		}

		User inactiveUser = createUser('PositionChild21InactiveUser', p.Id);
		output.put(inactiveUser.LastName, inactiveUser);


		insert inactiveUser;

		System.runAs(new User(Id = Userinfo.getUserId(), UserName = 'username_' + Math.random() + '@u.com', Country_Code_BI__c = 'BR')) {
			insert createPermissionSetAssignment(inactiveUser.Id, permissionSet.Id);
			inactiveUser.IsActive = false;
			update inactiveUser;
		}


		usersById.put(inactiveUser.Id, inactiveUser);

		return output;
	}

	private static User createUser(String name, Id profileId) {
		return new User(Alias = name.substring(0, 5), Email = name + '@testorg.com',
		                EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US',
		                LocaleSidKey = 'en_US', ProfileId = profileId, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = name+'_Test_External_ID');
	}
	private static PermissionSetAssignment createPermissionSetAssignment(Id userId, Id permissionSet) {
		return new PermissionSetAssignment(PermissionSetId = permissionSet, AssigneeId = userId);
	}
}