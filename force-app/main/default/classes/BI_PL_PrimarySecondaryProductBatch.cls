/**
 * This process creates a business rule for each position for the selected cycle-hierarchy-channel
 */
global class BI_PL_PrimarySecondaryProductBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful{
	
	private String primaryProduct;
	private String secondaryProduct = null;
	private Set<Id> processedPositions = new Set<Id>();
	private Map<Id,Boolean> resultList = new Map<Id,Boolean>();
	private String brRecordTypeId;
	private String BRTYPE = 'Primary and Secondary';
	private List<String> errors = new List<String>();

	global BI_PL_PrimarySecondaryProductBatch(){}

	global Database.QueryLocator start(Database.BatchableContext BC) {

		this.primaryProduct = String.valueOf(this.params.get('primaryProduct'));
		if(this.params.get('secondaryProduct')!=null)
			this.secondaryProduct = String.valueOf(this.params.get('secondaryProduct'));
		this.brRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'PrimaryAndSecondary' LIMIT 1].Id;

		String cycle = this.cycleIds[0];

		String query = 'SELECT BI_PL_Position__c,BI_PL_Position__r.BI_PL_External_id__c, BI_PL_Cycle__r.BI_PL_Country_code__c ' +
            'FROM BI_PL_Position_cycle__c ' +
            'WHERE BI_PL_Cycle__c = :cycle ';
        
        if(!hierarchyNames.isEmpty()){
            query += ' AND BI_PL_Hierarchy__c IN : hierarchyNames';
        }

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		List<BI_PL_Position_cycle__c> positionsCycle = (List<BI_PL_Position_cycle__c>)scope;
		Map<String,BI_PL_Business_rule__c> brToCreate = new Map<String,BI_PL_Business_rule__c>();

		List<String> products = new List<String>();
		products.add(this.primaryProduct);
		if(this.secondaryProduct != null) products.add(this.secondaryProduct);
		Map<String, String> mapProduct = new Map<String, String>(); 
		for(Product_vod__c p : [SELECT Id, External_id_vod__c FROM Product_vod__c WHERE Id IN: products]){
			mapProduct.put(p.Id,p.External_id_vod__c);
		}
		
		for(BI_PL_Position_cycle__c positionCycle: positionsCycle){

			if(!processedPositions.contains(positionCycle.BI_PL_Position__c)){
				System.debug('BI_PL_Position__r.Name :: ' + positionCycle.BI_PL_Position__r.BI_PL_External_id__c);
				System.debug('primaryProduct :: ' + this.primaryProduct);
				System.debug('secondaryProduct :: ' + this.secondaryProduct);
				String externalID = positionCycle.BI_PL_Cycle__r.BI_PL_Country_code__c + '_Primary and Secondary_' + positionCycle.BI_PL_Position__r.BI_PL_External_id__c + '_'+mapProduct.get(this.primaryProduct) +'_';

				if(this.secondaryProduct != null){
					externalID += mapProduct.get(this.secondaryProduct) + '_BR';
				}else{
					externalID += '_BR';
				}


				brToCreate.put( externalID,
					new BI_PL_Business_rule__c(
						BI_PL_External_id__c = externalID,
						RecordTypeId = brRecordTypeId,
						BI_PL_Active__c = true,
						BI_PL_Channel__c = this.channels[0],
						BI_PL_Country_code__c = positionCycle.BI_PL_Cycle__r.BI_PL_Country_code__c,
						BI_PL_Product__c = this.primaryProduct,
						BI_PL_Secondary_product__c = this.secondaryProduct,
						BI_PL_Type__c = this.BRTYPE,
						BI_PL_Position__c = positionCycle.BI_PL_Position__c)
					);
			}

			processedPositions.add(positionCycle.BI_PL_Position__c);

		}

		System.debug('br size: ' + brToCreate.size());

		
		
		Schema.SObjectField field = BI_PL_Business_rule__c.Fields.BI_PL_External_id__c;

		Database.UpsertResult[] srList = Database.upsert(brToCreate.values(), field, false);

		System.debug('srList :: ' + srList);

		List<String> insertedBRId = new List<String>();

		for (Database.UpsertResult sr : srList) {
		    if (sr.isSuccess()) {
		        resultList.put(sr.getId(),true);
		        insertedBRId.add(sr.getId());
		    }else {
		        for(Database.Error err : sr.getErrors()) {                 
		            errors.add(err.getStatusCode() + ': ' + err.getMessage());
		        }
		    }
		}

		/*System.debug('ERRORS :: ' + errors);
		System.debug('ERRORS size:: ' + errors.size());
		System.debug('SUCCES :: ' + insertedBRId);
		System.debug('SUCCES size:: ' + insertedBRId.size());*/
	}

	global void finish(Database.BatchableContext BC){}

	/*global void finish(Database.BatchableContext BC) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
       	String[] toAddresses = new String[] {UserInfo.getUserEmail()};
        mail.setToAddresses(toAddresses);  
        mail.setSubject('PLANiT | Primary and Secondary Product Batch'); 
        String listError = '';
        for(String error: errors){
        	listError += error;
        } 
	    mail.setPlainTextBody(listError);  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}*/

}