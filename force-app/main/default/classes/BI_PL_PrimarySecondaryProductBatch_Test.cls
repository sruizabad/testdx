@isTest
private class BI_PL_PrimarySecondaryProductBatch_Test {

	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);



    }

    @isTest
    static void test() {

        String[] hierarchy = new String[]{'hierarchyTest'};
        String[] channel = new String[]{'rep_detail_only'};

        
        List<String> cycleIds = new List<String>();
        for(BI_PL_Cycle__c cycle: [SELECT Id FROM BI_PL_Cycle__c]){
        	cycleIds.add(cycle.Id);
        }

        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

        Map<String, Object> mapProducts = new Map<String, Object>();
        mapProducts.put('primaryProduct',[SELECT Id, External_ID_vod__c FROM Product_vod__c LIMIT 1].Id);
        mapProducts.put('secondaryProduct',[SELECT Id, External_ID_vod__c FROM Product_vod__c LIMIT 1].Id);

        Test.startTest();
        BI_PL_PrimarySecondaryProductBatch psBatch = new BI_PL_PrimarySecondaryProductBatch();
        psBatch.init(cycleIds,hierarchy,channel,mapProducts);
        Database.executeBatch(psBatch);
        Test.stopTest();
    }
}