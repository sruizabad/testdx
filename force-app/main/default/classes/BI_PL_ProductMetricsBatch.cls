/**
 *  29/09/2017
 *  @author OMEGA CRM
 */
global class BI_PL_ProductMetricsBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {
     
    public String cycleId;
    public String channelId;
    
    public BI_PL_ProductMetricsBatch(){}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        this.cycleId = this.cycleIds[0];
        this.channelId = this.channels[0];

        System.debug('BI_PL_ProductMetricsBatch START ' + cycleId );

        /*String query = 'SELECT Id ' + 
                        'FROM BI_PL_Target_preparation__c ' +
                        'WHERE BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycle AND ' +
                                'BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c = : countryCode';
                                //'BI_PL_Header__r.BI_PL_Country_code__c = : countryCode';
        if(!hierarchy.isEmpty()){
            query += ' AND BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN : hierarchy';
        }*/


        String query = 'SELECT Id, BI_PL_Product__c, BI_PL_Segment__c, BI_PL_Strategic_segment__c, BI_PL_Channel_detail__r.BI_PL_Target_account__c ' +
                        'FROM BI_PL_Detail_preparation__c ' +
                        'WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycleId ' +
                        'AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c = : countryCode ' +
                        'AND BI_PL_Channel_detail__r.BI_PL_MSL_flag__c = true';
        
        if(!this.hierarchyNames.isEmpty()){
            query += ' AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN : hierarchyNames';
        }

        if(!this.channels.isEmpty()){
            query += ' AND BI_PL_Channel_detail__r.BI_PL_Channel__c IN :channels';
        }

        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<BI_PL_Detail_preparation__c> detailPreparations) {
        System.debug('***EXECUTE');

        transferProductMetrics(detailPreparations);
        
    }

    public void transferProductMetrics(List<BI_PL_Detail_preparation__c> lstALLDetails)
    {
        /*String query = 'SELECT Id, BI_PL_Product__c, BI_PL_Segment__c, BI_PL_Strategic_segment__c, BI_PL_Channel_detail__r.BI_PL_Target_account__c ' +
                        'FROM BI_PL_Detail_preparation__c ' +
                        'WHERE BI_PL_Channel_detail__r.BI_PL_Target__c in :targetPreparations AND BI_PL_Channel_detail__r.BI_PL_MSL_flag__c = true ';

        if(channel!=null && channel!=''){
            query += ' AND BI_PL_Channel_detail__r.BI_PL_Channel__c = :channel';
        }*/

        /*List<BI_PL_Detail_preparation__c> lstALLDetails = (List<BI_PL_Detail_preparation__c>)Database.query(query);*/
        
        Set<Id> accounts = new Set<Id>();
        Set<Id> products = new Set<Id>();
        for(BI_PL_Detail_preparation__c detail: lstALLDetails){
            products.add(detail.BI_PL_Product__c);
            accounts.add(detail.BI_PL_Channel_detail__r.BI_PL_Target_account__c);
        }

        //Get ProductMetric Data
        List<Product_Metrics_vod__c> productMetricsList  = new  List<Product_Metrics_vod__c>([SELECT Id, Account_vod__c, Products_vod__c, Segmentation_BI__c, Strategic_Segment_BI__c FROM Product_Metrics_vod__c WHERE Account_vod__c in :accounts AND Products_vod__c in :products]);

        List<Product_Metrics_vod__c> productsmetricsToUpdate = new List<Product_Metrics_vod__c>();
        List<Product_Metrics_vod__c> newProdumetrics = new List<Product_Metrics_vod__c>();

        for(Integer i = 0; i< lstALLDetails.size(); i++)
        {
            //Check if the record exist in Product Metrics
            BI_PL_Detail_preparation__c  detail = lstALLDetails.get(i);
            String productKey = detail.BI_PL_Product__c;
            String segmentKey = detail.BI_PL_Segment__c;
            String accountKey = detail.BI_PL_Channel_detail__r.BI_PL_Target_account__c;
            System.debug('detail '+ detail);
            System.debug('productKey '+ productKey + ' - ' + segmentKey + ' - '+ accountKey);

            Product_Metrics_vod__c productmetric = findProductMetric(productMetricsList,accountKey,productKey); 
            
           
            if(productmetric!= null)
            {
                //Check if product mettic is already in the list to be updated
                if(!listContains(productsmetricsToUpdate,productmetric))
                {
                    //Update Record
                    productmetric.Country_Code_BI__c = countryCode;
                    if(detail.BI_PL_Segment__c!=null){
                        productmetric.Segmentation_BI__c = detail.BI_PL_Segment__c;
                        productmetric.Strategic_Segment_BI__c = detail.BI_PL_Strategic_segment__c;   
                    }
                    productsmetricsToUpdate.add(productmetric);
                }
               
            }
            else
            {
                //Create New Record
                Product_Metrics_vod__c newproductmetric = new Product_Metrics_vod__c();
                newproductmetric.Country_Code_BI__c  = countryCode;
                newproductmetric.Account_vod__c = accountKey;
                newproductmetric.Products_vod__c = productKey;
                newproductmetric.Segmentation_BI__c = detail.BI_PL_Segment__c;
                newproductmetric.Strategic_Segment_BI__c = detail.BI_PL_Strategic_segment__c;
                newProdumetrics.add(newproductmetric);
                System.debug(newProdumetrics);
            }

        }

        update productsmetricsToUpdate;
        insert newProdumetrics;
        
        System.debug('ProductMetrics to insert: ' + productsmetricsToUpdate.size() + ' ; New ProductMetrics: ' + newProdumetrics.size());

    }

    private Boolean listContains(Object[] source, Object target) {
        return (new Set<Object>(source)).contains(target);
    }

    global void finish(Database.BatchableContext BC) {
        System.debug('BI_PL_ProductMetricsBatch finish');
        Database.executeBatch(new BI_PL_ProductMetricsSecondaryBatch(this.cycleIds[0],this.hierarchyNames,this.channels[0],this.countryCode));
    }


    private static Product_Metrics_vod__c findProductMetric(List<Product_Metrics_vod__c> productMetrics, String account, String product)
    {
        for(Integer i = 0; i< productMetrics.size(); i++)
        {
            Product_Metrics_vod__c obj = productMetrics.get(i);
            if(obj.Account_vod__c == account && obj.Products_vod__c == product)
            {
                return obj;
            }
        }
        return null;
    }

}