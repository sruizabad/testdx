global class BI_PL_ProductMetricsMinusBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {
		
	private String lastCycle;
	private String currentCycle;

    List<String> accountsId;
    List<String> productsId;
    Set<String> accountProduct;
	
  global BI_PL_ProductMetricsMinusBatch() {
  }

	global BI_PL_ProductMetricsMinusBatch(String lastCycle, String currentCycle) {
		this.lastCycle = lastCycle;
		this.currentCycle = currentCycle;
		accountsId = new List<String>();
		productsId = new List<String>();
		accountProduct = new Set<String>();
	}
	
	global Database.QueryLocator start(Database.BatchableContext bc) {

        if(this.params != null && this.params.containsKey('lastCycle') && this.params.containsKey('currentCycle')){
          this.lastCycle = String.valueOf(this.params.get('lastCycle'));
          this.currentCycle = String.valueOf(this.params.get('currentCycle'));

          accountsId = new List<String>();
          productsId = new List<String>();
          accountProduct = new Set<String>();
        
        }/*else{
          throw new BI_PL_Exception(Label.BI_PL_Cycle_null);  
        }*/
    
		
        String query = 'SELECT Id, BI_PL_Product__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c ' + 
        	'FROM BI_PL_Detail_preparation__c ' + 
        	'WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : lastCycle ';

        if(Test.isRunningTest()){
        	query += ' LIMIT 1';
        }
		
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		List<BI_PL_Detail_preparation__c> details = (List<BI_PL_Detail_preparation__c>)scope;
   		List<String> accounts = new List<String>();
   		List<String> products = new List<String>();
   		Map<String, BI_PL_Detail_preparation__c> detailsMap = new Map<String, BI_PL_Detail_preparation__c>();

   		for(BI_PL_Detail_preparation__c detail: details){
   			accounts.add(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c);
   			products.add(detail.BI_PL_Product__c);
   			detailsMap.put(''+detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c+detail.BI_PL_Product__c,detail);
   		}

   		/*String currentCycleDetailsQuery = 'SELECT count(Id), BI_PL_Product__c prod, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c acc ' + 
        	'FROM BI_PL_Detail_preparation__c ' + 
        	'WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : currentCycle ' +
        	'AND (BI_PL_Product__c IN :products OR BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c IN :accounts) '; 

        currentCycleDetailsQuery += 'GROUP BY BI_PL_Product__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c';


        for(AggregateResult ag: Database.query(currentCycleDetailsQuery)){
   			String key = String.valueOf(ag.get('acc'))+String.valueOf(ag.get('prod'));
   			detailsMap.remove(key);
   		}*/

   		for(BI_PL_Detail_preparation__c detailPreparationCurrentCycle: [SELECT BI_PL_Product__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c
   						FROM BI_PL_Detail_preparation__c
   						WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : currentCycle 
			   			AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c IN :accounts]){
   		//				AND (BI_PL_Product__c IN :products OR BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c IN :accounts)]){
   			String key = String.valueOf(detailPreparationCurrentCycle.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c)+String.valueOf(detailPreparationCurrentCycle.BI_PL_Product__c);
   			detailsMap.remove(key);
   		}

   		if(!detailsMap.isEmpty()){

	   		for(BI_PL_Detail_preparation__c det: detailsMap.values()){
	   			accountsId.add(det.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c);
	   			productsId.add(det.BI_PL_Product__c);
	   			accountProduct.add(''+det.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c+det.BI_PL_Product__c);
	   		}

   		}
   		
	}
	
	global void finish(Database.BatchableContext BC) {

		System.debug(loggingLevel.Error,'*** Finish export!!');

		/*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
       	String[] toAddresses = new String[] {UserInfo.getUserEmail()};
        mail.setToAddresses(toAddresses);  
        mail.setSubject('Minus');  
        mail.setPlainTextBody('Finish. Details size: ' + accountsId.size());  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/

        Database.executeBatch(new BI_PL_ExportProdMetricsBatch(currentCycle,accountsId,productsId,accountProduct));
		
	}


}