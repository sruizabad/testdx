/**
*   Delete Detail Preparation which Products are in Restricted Products fields from Account
*   @author Omega CRM
*/
global class BI_PL_ProductsDeleteBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject> {
	
	//String cycleId;
	global BI_PL_ProductsDeleteBatch(){}
	global BI_PL_ProductsDeleteBatch(List<String> sCycle) {
		this.cycleIds = sCycle;	
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('## START BI_PL_ProductsDeleteBatch');
		return Database.getQueryLocator([SELECT Id, Name, BI_PL_Target__r.BI_PL_Target_customer__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c, 
											BI_PL_Target__r.BI_PL_Header__r.BI_PL_Country_code__c, BI_PL_Target__r.BI_PL_Specialty__c, BI_PL_Target__r.BI_PL_Specialty_code__c, 
											BI_PL_Removed__c, BI_PL_Edited__c, BI_PL_Number_details__c, BI_PL_External_Id__c, BI_PL_Removed_reason__c, BI_PL_Removed_date__c
								            FROM BI_PL_Channel_detail_preparation__c
								            WHERE BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c IN :cycleIds AND BI_PL_Target__r.BI_PL_Target_customer__c != null
								            ORDER BY BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c]
		);
	}

   	global void execute(Database.BatchableContext BC, List<BI_PL_Channel_detail_preparation__c> lstTargetPrep) {
	
		List<BI_PL_Channel_detail_preparation__c> lstChannel_Update = new List<BI_PL_Channel_detail_preparation__c>();
		List<BI_PL_Detail_preparation__c> lstDetail_Update = new List<BI_PL_Detail_preparation__c>();
		List<BI_PL_Detail_preparation__c> lstDetail_Delete = new List<BI_PL_Detail_preparation__c>();

		Map<Id,BI_PL_Channel_detail_preparation__c> mapTargetPrep = new Map<Id,BI_PL_Channel_detail_preparation__c>();
		Map<Id,Account> mapRestrictedAccounts = new Map<Id,Account>();
		Map<Id,Id> mapAccountTarget = new Map<Id,Id>(); //put Account Id related to Channel

		Map<Id, List<BI_PL_Detail_preparation__c>> channelDetails = new Map<Id, List<BI_PL_Detail_preparation__c>>(); //map where control and list every detail from ther channel

		System.debug('## lstTargetPrep: ' + lstTargetPrep);
		for (BI_PL_Channel_detail_preparation__c inTargetPrep : lstTargetPrep){
			mapTargetPrep.put(inTargetPrep.Id, inTargetPrep);
			if (inTargetPrep.BI_PL_Target__r.BI_PL_Target_customer__c != null) 
				mapAccountTarget.put(inTargetPrep.BI_PL_Target__r.BI_PL_Target_customer__c, inTargetPrep.Id);
		}

		//Get only Accounts with Restricted Products
		mapRestrictedAccounts = getAccountsWithRestrictedProducts(mapAccountTarget.keyset(), mapRestrictedAccounts);
		Set<Id> channelsWithAccountRestricted = new Set<Id>();
		for(BI_PL_Channel_detail_preparation__c ch : mapTargetPrep.values()){
			if(mapRestrictedAccounts.containsKey(ch.BI_PL_Target__r.BI_PL_Target_customer__c)){
				channelsWithAccountRestricted.add(ch.Id);
			}
		}

		System.debug('## mapRestrictedAccounts: ' + mapRestrictedAccounts);
		//Check there is some Account with Restricted Product
		if (!mapRestrictedAccounts.isEmpty()){

			//Get Restricted Product List by Account and related to Target
			Map<Id,Set<String> > mapTargetProducts = getRestrictedProducts(mapRestrictedAccounts.values());
			//System.debug('## mapTargetProducts: ' + mapTargetProducts);
			//System.debug('## mapTargetPrep: ' + mapTargetPrep);

			List<BI_PL_Detail_preparation__c> lstDetailPrep = [SELECT Id, BI_PL_Channel_detail__c, BI_PL_Product__c, BI_PL_Product__r.Name, BI_PL_Channel_detail__r.BI_PL_Target__c,
																BI_PL_Secondary_product__c, BI_PL_Secondary_product__r.Name, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c, BI_PL_External_Id__c
																, BI_PL_Product__r.External_id_vod__c, BI_PL_Secondary_product__r.External_id_vod__c
																FROM BI_PL_Detail_preparation__c 
																WHERE BI_PL_Channel_detail__c IN :channelsWithAccountRestricted AND BI_PL_Product__c != null
																ORDER BY BI_PL_Channel_detail__c];	

			Map<Id, List<BI_PL_Detail_preparation__c>> detailPerChannel = getDetailsPerChannel(channelsWithAccountRestricted, lstDetailPrep);
			//get Channel and its Primary Products
			Map<Id,set<String>> mapChannelProd = getProductsbyChannel(lstDetailPrep);

			Boolean bPrimary, bSecondary, notSecondary;
			for (Id inDetail : channelsWithAccountRestricted){
				//Prepare Data
				BI_PL_Channel_detail_preparation__c chan = mapTargetPrep.get(inDetail);
				Decimal numDetails = chan.BI_PL_Number_details__c;
				List<BI_PL_Detail_preparation__c> detailsTo = detailPerChannel.get(chan.Id);
				List<String> detailsExternalsId = new List<String>();
				for(BI_PL_Detail_preparation__c d : detailsTo){
					detailsExternalsId.add(d.BI_PL_External_Id__c);
				}

				List<BI_PL_Detail_preparation__c> detailsDubt = new List<BI_PL_Detail_preparation__c>();
				Integer details_Del = 0;

				for(BI_PL_Detail_preparation__c det : detailsTo){
					//Booleans to know if the detail has to be deleted
					bPrimary = mapTargetProducts.get(det.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c).contains(det.BI_PL_Product__r.Name) ? true : false;
					bSecondary = det.BI_PL_Secondary_product__c != null && 
									mapTargetProducts.get(det.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c).contains(det.BI_PL_Secondary_product__r.Name) ? true : false;
					notSecondary = (det.BI_PL_Secondary_product__c == null ) ? true : false;
					if(bPrimary && (bSecondary || notSecondary)){
							detailsDubt.add(det);
					}else{ 
						if(bSecondary==false && bPrimary==true && notSecondary==false){
							//Promoted as Primary if there isn't Channel with that Product
							String externalId = chan.BI_PL_External_Id__c + '_' + det.BI_PL_Secondary_product__r.External_id_vod__c;
							if(detailsExternalsId.contains(externalId)){
								detailsDubt.add(det);
							}else{
								det.BI_PL_Product__c = det.BI_PL_Secondary_product__c;
								det.BI_PL_Secondary_product__c = null;
								det.BI_PL_External_Id__c = externalId;

								lstDetail_Update.add(det);
								detailsExternalsId.add(externalId);
							}
						}else{
							if(bSecondary==true && bPrimary==false && notSecondary==false){
								//Delete Secondary relationship
								String externalId = chan.BI_PL_External_Id__c + '_' + det.BI_PL_Product__r.External_id_vod__c;
								if(detailsExternalsId.contains(externalId)){
									detailsDubt.add(det);
								}else{
									det.BI_PL_Secondary_product__c = null;
									det.BI_PL_External_Id__c = externalId;
									lstDetail_Update.add(det);
									detailsExternalsId.add(externalId);
								}
							}
						}
					}
				}
				if(numDetails == detailsDubt.size()){
					chan.BI_PL_Edited__c = true;
					chan.BI_PL_Removed__c = true;
					chan.BI_PL_Removed_reason__c = 'Excluded due to Specialty';
					chan.BI_PL_Removed_date__c = System.today();
					lstChannel_Update.add(chan);
				}else{
					lstDetail_Delete.addAll(detailsDubt);
				}
			}
			if(!lstDetail_Delete.isEmpty()){
                deleteRecord(lstDetail_Delete, 'Detail');
            }
			if(!lstChannel_Update.isEmpty()){
				update lstChannel_Update;
            }
            if(!lstDetail_Update.isEmpty()){
				update lstDetail_Update;
            }
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug('## FINNISH BI_PL_ProductsDeleteBatch');
	}

	/*
	* Get map with values Restricted Products
	*/
	private Map<Id,Account> getAccountsWithRestrictedProducts(Set<Id> setAccountIds, Map<Id,Account> mapRestrictAccounts){
		for (Account inAcc : [SELECT Id, Restricted_Products_vod__c 
							FROM Account 
							WHERE Id IN :setAccountIds]){
			if (String.isNotBlank(inAcc.Restricted_Products_vod__c) ) 
				mapRestrictAccounts.put(inAcc.Id,inAcc); //Only Accounts with values in Restricted Products field
				System.debug('## Restricted: ' + inAcc.Restricted_Products_vod__c);
		}
		return mapRestrictAccounts;
	} 

	/*
	* Get all Preparations ordered by their relationship target - chanel - detail preparation
	*/
	private Map<Id, Map<Id, List<BI_PL_Detail_preparation__c>>> getOrganizedPreparations(List<BI_PL_Detail_preparation__c> lstDetailPrep){
		Map<Id, Map<Id, List<BI_PL_Detail_preparation__c>>> mapPreparations = new Map<Id, Map<Id, List<BI_PL_Detail_preparation__c>>>();
		for (BI_PL_Detail_preparation__c inDetail : lstDetailPrep){
			mapPreparations = buildOrganizedDetail(mapPreparations, inDetail);
		}
		return mapPreparations;
	}

	/*
	 * Create a list of details per channel
	*/
	private Map<Id, List<BI_PL_Detail_preparation__c>> getDetailsPerChannel(Set<Id> channels, List<BI_PL_Detail_preparation__c> lstDetailPrep){
		Map<Id, List<BI_PL_Detail_preparation__c>> mappingChaDet = new Map<Id, List<BI_PL_Detail_preparation__c>>();
		for(Id chan : channels){
			List<BI_PL_Detail_preparation__c> tails = new List<BI_PL_Detail_preparation__c>();
			for(BI_PL_Detail_preparation__c dets : lstDetailPrep){
				if(dets.BI_PL_Channel_detail__c == chan){
					tails.add(dets);
				}
			}
			mappingChaDet.put(chan, tails);
		}
		return mappingChaDet;
	}

	/*
	* Get Channel and its Primary Products
	*/
	private Map<Id,Set<String>> getProductsbyChannel(List<BI_PL_Detail_preparation__c> lstDetailPrep){
		
		Map<Id,Set<String>> mapChannelProd = new Map<Id,Set<String>>();
		Set<String> setChannel = new Set<String>();
		//Get all Channel Details related to Current Detail Preparation
		for (BI_PL_Detail_preparation__c inDetail : lstDetailPrep){
			if (!mapChannelProd.containskey(inDetail.BI_PL_Channel_detail__c)){
				mapChannelProd.put(inDetail.BI_PL_Channel_detail__c, new Set<String>());
			}
			setChannel = mapChannelProd.get(inDetail.BI_PL_Channel_detail__c);
			setChannel.add(inDetail.BI_PL_Product__r.Name);
			mapChannelProd.put(inDetail.BI_PL_Channel_detail__c, setChannel);
		}
		return mapChannelProd;
	}

	/*
	* Put Detail Preparation in Organized map relationship target - channel - detail
	*/
	private map<Id, map<Id, List<BI_PL_Detail_preparation__c>>> buildOrganizedDetail(map<Id, map<Id, List<BI_PL_Detail_preparation__c>>> mapPreparations, BI_PL_Detail_preparation__c oDetail){

		map<Id, List<BI_PL_Detail_preparation__c>> mapChannelDetail = new map<Id, List<BI_PL_Detail_preparation__c>>();
		List<BI_PL_Detail_preparation__c> lstDetailPrep = new List<BI_PL_Detail_preparation__c>();

		//check Target Preparation is already in map
		if(!mapPreparations.containskey(oDetail.BI_PL_Channel_detail__r.BI_PL_Target__c)){
			mapPreparations.put(oDetail.BI_PL_Channel_detail__r.BI_PL_Target__c, new map<Id, List<BI_PL_Detail_preparation__c>>());
		}

		mapChannelDetail = mapPreparations.get(oDetail.BI_PL_Channel_detail__r.BI_PL_Target__c);
		
		//check Channel Preparation is already in map to get list Detail Preparation
		if(!mapChannelDetail.containskey(oDetail.BI_PL_Channel_detail__c)){
			mapChannelDetail.put(oDetail.BI_PL_Channel_detail__c, new List<BI_PL_Detail_preparation__c>());
		}

		lstDetailPrep = mapChannelDetail.get(oDetail.BI_PL_Channel_detail__c);

		//put detail preparation in its hierarchy
		lstDetailPrep.add(oDetail);
		mapChannelDetail.put(oDetail.BI_PL_Channel_detail__c, lstDetailPrep);
		mapPreparations.put(oDetail.BI_PL_Channel_detail__r.BI_PL_Target__c, mapChannelDetail);

		return mapPreparations;
	}

	/*
	* Method to get map Targets and its Restricted Products Set from Account
	*/
	private map<Id,set<String>> getRestrictedProducts(List<Account> lstAccount){

		Map<Id,Set<String>> mapTargetRestrictedProd = new map<Id,Set<String>>();
		Set<String> setProduct;
		for(Account inAccount : lstAccount){
			if(String.isNotBlank(inAccount.Restricted_Products_vod__c)){
				setProduct = new Set<String>();
				//every Product is with ;;, included the last Product string
				for(String sRestrictedProd : inAccount.Restricted_Products_vod__c.split(';;') ){
					if(String.isNotBlank(sRestrictedProd)){ 
						setProduct.add(sRestrictedProd);
					}
				}
				//get Target Id from Account and related to set of Products
				mapTargetRestrictedProd.put(inAccount.Id, setProduct);
			}
		}
		return mapTargetRestrictedProd;
	}
	/*
	* function to Delete in try catch method
	*/
	private void deleteRecord(list<sObject> lstToDelete, String sName){
		if (!lstToDelete.isEmpty() ){
			try{
				delete lstToDelete;
			}
			catch(DmlException e){
				BI_PL_ErrorHandlerUtility.saveError(e.getMessage(), '', 'BI_PL_ProductsDeleteBatch', 'DELETE ' + sName + ' ERROR', false);
			}
		}
	}
}