@isTest
private class BI_PL_ProductsDeleteBatch_Test {
	
	/*@isTest static void test_method_one() {
		BI_PL_TestDataFactory.createTestPersonAccounts(2,'US');
		BI_PL_TestDataFactory.createCycleStructure('US');

		List<BI_PL_Cycle__c> ext = [SELECT Id FROM BI_PL_Cycle__c];
		List<String> cycId = new List<String>();
		for(BI_PL_Cycle__c ex : ext){
			cycId.add(ex.Id);
		}

		Test.StartTest();
		Database.executeBatch(new BI_PL_ProductsDeleteBatch(cycId));
		Test.StopTest();
	}*/


	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);


        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        //Date startDate = Date.today()-60;
        //Date endDate =  Date.today()-30;
        //Need complete Year for split
        Integer iYear = Integer.valueof( Date.today().year() );
        Date startDate = Date.newInstance(iYear, 1, 1);
        Date endDate = Date.newInstance(iYear, 12, 31);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);

        List<Account> listAcc = [SELECT Id, Restricted_Products_vod__c, External_ID_vod__c FROM Account];
        for(Account acc: listAcc){
        	acc.Restricted_Products_vod__c = 'TestProd1;;TestProd2';
        }
        update listAcc;

        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

    	List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);

        List<BI_PL_Detail_preparation__c> lstDetailPrep = [SELECT Id, BI_PL_Product__c, BI_PL_Secondary_product__c FROM BI_PL_Detail_preparation__c];	
        lstDetailPrep[0].BI_PL_Secondary_product__c = listProd[0].Id;
        lstDetailPrep[0].BI_PL_Product__c = listProd[0].Id;
        lstDetailPrep[1].BI_PL_Secondary_product__c = listProd[1].Id;
        lstDetailPrep[2].BI_PL_Secondary_product__c = null;
        lstDetailPrep[2].BI_PL_Product__c = listProd[2].Id; 
        lstDetailPrep[3].BI_PL_Secondary_product__c = null;
        lstDetailPrep[3].BI_PL_Product__c = listProd[3].Id; 

        update lstDetailPrep;

    }

     @isTest public static void test() {
        
        List<BI_PL_Cycle__c> cycles = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c, Name FROM BI_PL_Cycle__c ORDER BY BI_PL_Start_date__c ASC];
        List<String> cycId = new List<String>();
		for(BI_PL_Cycle__c ex : cycles){
			cycId.add(ex.Id);
		}

        Test.StartTest();
		Database.executeBatch(new BI_PL_ProductsDeleteBatch(cycId));
		Test.StopTest();
        
    }


}