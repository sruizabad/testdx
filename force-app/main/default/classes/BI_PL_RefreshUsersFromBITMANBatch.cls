global class BI_PL_RefreshUsersFromBITMANBatch implements Database.Batchable<sObject>, Database.Stateful {
	
	String countryCode;	
	Date todayDate;
	Set<String> cyclesToUpdateInVeeva;
	Set<String> cyclesToUpdateInPlanit;


	global BI_PL_RefreshUsersFromBITMANBatch(String countryCode) {

			this.todayDate = System.today();			
			this.countryCode = countryCode;
			this.cyclesToUpdateInPlanit = new Set<String>();
			this.cyclesToUpdateInVeeva = new Set<String>();		

	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, BI_PL_Start_Date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c =: countryCode AND (BI_PL_Start_Date__c >= today OR (BI_PL_Start_Date__c <= today AND BI_PL_End_date__c >= today))]);
	}

   	global void execute(Database.BatchableContext BC, List<BI_PL_Cycle__c> scope) {

   		
   		this.cyclesToUpdateInVeeva = new Set<String>();
   		this.cyclesToUpdateInPlanit = new Set<String>();

   		for(BI_PL_cycle__c c : scope){
   			if(this.todayDate >= c.BI_PL_Start_Date__c && this.todayDate <= c.BI_PL_End_date__c ){
   				this.cyclesToUpdateInVeeva.add(c.Id);
   			}else{
   				this.cyclesToUpdateInPlanit.add(c.Id);
   			}
   		}

	}
	
	global void finish(Database.BatchableContext BC) {


		if(this.cyclesToUpdateInVeeva.size() > 0){
			List<String> cyclesToVeeva = new List<String> (this.cyclesToUpdateInVeeva);
			Database.executeBatch(new BI_PL_UpdateUsersInVeevaBatch(this.countryCode, cyclesToVeeva));
		}

		if(this.cyclesToUpdateInPlanit.size() > 0){
			List<String> cyclesToPlanit = new List<String> (this.cyclesToUpdateInPlanit);
			Database.executeBatch(new BI_PL_UpdateUsersInPLANITBatch(this.countryCode, cyclesToPlanit));
		}

			
	}
	
}