public with sharing class BI_PL_RemotingServiceExt {

	@RemoteAction
	public static void addPlanitVueError(String vueComponent, String functionality, String error, String trace) {
		BI_PL_ErrorHandlerUtility.saveError(error, trace, vueComponent, functionality, false);
	}
	
	@RemoteAction
	public static void addPlanitServerError(String apexClass, String functionality, String error, String trace) {
		BI_PL_ErrorHandlerUtility.saveError(error, trace, apexClass, functionality, true);
	}

	@RemoteAction
	public static Map<String, Boolean> getCurrentUserPermissions() {
		return BI_PL_PreparationServiceUtility.getCurrentUserPermissions();
	}

	@RemoteAction
	public static BI_PL_Country_settings__c getCountrySettings(String countryCode) {
		return BI_PL_CountrySettingsUtility.getCountrySettingsForCountry(countryCode);
	}

	@RemoteAction
	public static BI_PL_Country_settings__c getCountrySettingsForCurrentUser() {
		return BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser();
	}

	@RemoteAction
	public static Set<String> getFieldForceList(String countryCode, String cycleId, String hierachyName) {
		return BI_PL_PreparationServiceUtility.findFieldForcesInCycle(countryCode, cycleId, hierachyName);
	}
	@RemoteAction
	public static List<sObject> getFieldForceListBitman(String countryCode) {
		return BI_PL_PreparationServiceUtility.findFieldForcesInBITMAN(countryCode);
	}

	@RemoteAction
	public static Set<String> getSpecialtyList(String countryCode, String cycleId, String hierachyName) {
		return BI_PL_PreparationServiceUtility.findSpecialtiesInCycle(countryCode, cycleId, hierachyName);
	}
	@RemoteAction
	public static List<sObject> getSpecialtyListVeeva(String fieldPath, String countryCode) {
		return BI_PL_PreparationServiceUtility.findSpecialtiesInVeeva(fieldPath, countryCode);
	}

	@RemoteAction
	public static List<BI_PL_AddNewTargetModalCtrl.PicklistOption> getTargetAddedReasonOptions() {
		return BI_PL_AddNewTargetModalCtrl.getAddedReasonOptions();
	}

	@RemoteAction
	public static List<BI_PL_Business_rule__c> getBusinessRules(String countryCode, List<String> types, List<String> positionsId, List<String> targetsSpecialty, List<BI_PL_PreparationExt.PlanitProductPair> productPairs, String fieldForce, String channel) {
		return BI_PL_BusinessRuleUtility.getBusinessRulesForCountry(countryCode, types, positionsId,targetsSpecialty, productPairs, fieldForce, channel);
	}

	@RemoteAction
	public static Set<String> getCurrentCountryCodes(){
		Set<String> countryCodes = new Set<String>();

		for(Group gm : [SELECT Id,DeveloperName from Group WHERE id IN (SELECT groupid FROM groupmember WHERE UserOrGroupId = :UserInfo.getUserId()) AND name LIKE 'PL_%']){
			String cc= gm.DeveloperName.substringAfter('_');
			
			countryCodes.add(cc);
		}


		return countryCodes;
	}

	@RemoteAction
	public static Map<String, Object> getCurrentUser(){

		Set<String> countryCodes = new Set<String>();
		Map<String, Object> mapUser = new Map<String, Object>();

		for(Group gm : [SELECT Id,DeveloperName from Group WHERE id IN (SELECT groupid FROM groupmember WHERE UserOrGroupId = :UserInfo.getUserId()) AND name LIKE 'PL_%']){
			String cc= gm.DeveloperName.substringAfter('_');
			
			countryCodes.add(cc);
		}

		User u = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id =:UserInfo.getUserId()];

		mapUser.put('user',u);
		mapUser.put('countryCodes',countryCodes);



		return mapUser;
	}

}