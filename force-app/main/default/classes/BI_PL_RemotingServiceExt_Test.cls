@isTest
private class BI_PL_RemotingServiceExt_Test {
		

	private static String vueComponent = 'Component 1';
	private static String functionality = 'functionality';
	private static String error = 'eeeeeerror';
	private static String trace = 'trace';
	private static String hierarchy = 'hierarchyTest';

	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);
/*
        Date startDate = Date.today()-5;
        Date endDate =  Date.today()+5;
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);*/

        /*startDate = Date.today()+15;
        endDate =  Date.today()+30;
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);*/

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        //BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);

        

    }

	@isTest static void testAddPlanitVueError(){
		Test.startTest();
		BI_PL_RemotingServiceExt.addPlanitVueError(vueComponent, functionality, error, trace);
		Test.stopTest();
	}

	@isTest static void testAddPlanitServerError(){
		Test.startTest();
		BI_PL_RemotingServiceExt.addPlanitServerError(vueComponent, functionality, error, trace);
		Test.stopTest();
	}

	@isTest static void testGetCurrentUserPermissions(){
		Test.startTest();
		Map<String, Boolean> mapPermissions = BI_PL_RemotingServiceExt.getCurrentUserPermissions();
		Test.stopTest();
	}

	@isTest static void testGetCountrySettingsForCurrentUser(){
		Test.startTest();
		BI_PL_Country_settings__c cSettings = BI_PL_RemotingServiceExt.getCountrySettingsForCurrentUser();
		Test.stopTest();
	}

	@isTest static void testGetFieldForceList(){
		Test.startTest();
		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
		Set<String> fieldForces = BI_PL_RemotingServiceExt.getFieldForceList(userCountryCode, [SELECT Id FROM BI_PL_Cycle__c LIMIT 1].Id,hierarchy);
		Test.stopTest();
	}

	@isTest static void testGetFieldForceListBitman(){
		Test.startTest();
		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 
		List<sObject> lObjects = BI_PL_RemotingServiceExt.getFieldForceListBitman(userCountryCode);
		Test.stopTest();
	}

	/*@isTest static void testGetSpecialtyList(){
		Test.startTest();
		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 
		BI_PL_RemotingServiceExt.getSpecialtyList(userCountryCode, [SELECT Id FROM BI_PL_Cycle__c LIMIT 1].Id, hierarchy);
	}*/

	@isTest static void testGetSpecialtyListVeeva(){
		Test.startTest();
		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 
		List<sObject> objectList = BI_PL_RemotingServiceExt.getSpecialtyListVeeva('',userCountryCode);
		Test.stopTest();
	}

	@isTest static void testGetTargetAddedReasonOptions(){
		Test.startTest();
		List<BI_PL_AddNewTargetModalCtrl.PicklistOption> picklistOptions = BI_PL_RemotingServiceExt.getTargetAddedReasonOptions();
		Test.stopTest();
	}
	@isTest static void testlast(){
		Test.startTest();
		BI_PL_Country_settings__c users = BI_PL_RemotingServiceExt.getCountrySettings('US');
		Set<String> ext = BI_PL_RemotingServiceExt.getCurrentCountryCodes();
		Map<String, Object> cass = BI_PL_RemotingServiceExt.getCurrentUser();
		BI_PL_Cycle__c cycle = [SELECT id FROM BI_PL_Cycle__c LIMIT 1];
		Test.stopTest();
		/*Test.startTest();
		Set<String> ext2 = BI_PL_RemotingServiceExt.getSpecialtyList('US', cycle.Id, 'Test Cycle');
		Test.stopTest();*/
	}
}