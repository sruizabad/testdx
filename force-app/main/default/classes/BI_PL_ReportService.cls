public with sharing class BI_PL_ReportService {
	public BI_PL_ReportService() {
		
	}
	
	/**
	 * Gets the field force overlap page.
	 *
	 * @param      hierarchyPositions  The hierarchy positions
	 * @param      cycleId             The cycle identifier
	 * @param      hierarchyName       The hierarchy name
	 * @param      channel             The channel
	 * @param      singlePosition      The single position
	 * @param      pageSize            The page size
	 * @param      lastIdOffset        The last identifier offset
	 * @param      ordenation          The ordenation
	 *
	 * @return     The field force overlap page.
	 */
	@RemoteAction
	@ReadOnly
	public static BI_COMMON_VueforceController.RemoteResponse getPositionOverlapReport(List<String> hierarchyPositions, String cycleId, String hierarchyName, String channel, String singlePosition, Integer pageSize, String lastIdOffset, Map<String, String> ordenation) {
		
		String aggQuery = 'select bi_pl_header__r.bi_pl_position_cycle__r.bi_pl_position_name__c, bi_pl_header__r.bi_pl_position_cycle__r.bi_pl_position__r.bi_pl_field_force__c, bi_pl_target_customer__c '+ 
		+ ' from bi_pl_target_preparation__c ' +
		+ BI_PL_PreparationServiceUtility.getTargetFilters(cycleId, hierarchyName, channel, singlePosition);
		//+ ' group by bi_pl_header__r.bi_pl_position_cycle__r.bi_pl_position_name__c, bi_pl_header__r.bi_pl_position_cycle__r.bi_pl_position__r.bi_pl_field_force__c ';

		List<String> messages = new List<String>();

		Map<String, Map<String, Integer>> overlap = new Map<String, Map<String, Integer>>();
		Map<String, Integer> overlapByPositon = new Map<String, Integer>();
		Map<String, BI_PL_PositionWrapper> rowsByPosition = new Map<String, BI_PL_PositionWrapper>();


		//for(AggregateResult ag : Database.query(aggQuery)) {
		//	String pos = String.valueOf(ag.get('position'));
		//	if(rowsByPosition.containsKey(pos)) {
		//		rowsByPosition.get(pos).aggregate(ag);
		//	} else{
		//		rowsByPosition.put(pos, new BI_PL_PositionWrapper(ag));
		//	}
		//}
		
		System.debug(loggingLevel.Error, '*** getFieldForceOverlapPage: ' + aggQuery);

		for(BI_PL_Target_preparation__c tgt : Database.query(aggQuery)) {

			String acc = tgt.BI_PL_target_customer__c;
			String pos = tgt.BI_PL_header__r.BI_PL_position_cycle__r.BI_PL_position_name__c;
			String ff = tgt.BI_PL_header__r.BI_PL_position_cycle__r.BI_PL_position__r.BI_PL_field_force__c;


			if(rowsByPosition.containsKey(pos)) {
				rowsByPosition.get(pos).aggregate(tgt);
			} else{
				rowsByPosition.put(pos, new BI_PL_PositionWrapper(tgt));
			}

			//Add to overlap counter
			if(!overlapByPositon.containsKey(pos)) {
				overlapByPositon.put(pos, 0);
			}

			if(overlap.containsKey(acc)) {

				//We found previously this account
				if(!overlap.get(acc).containsKey(pos)) {
					overlap.get(acc).put(pos, 0);
				} else {
					messages.add('Found account twice for a position ' +  acc);
				}
				//Overlap with other positions?
				if(overlap.get(acc).keySet().size() > 1){
					overlapByPositon.put(pos, overlapByPositon.get(pos) + 1);
				} 
			} else{
				overlap.put(acc, new Map<String, Integer>{ pos => 1 });
			}
		}

		for(BI_PL_PositionWrapper row : rowsByPosition.values()) {
			row.addOverlap(overlapByPositon.get(row.name));
		}

		BI_COMMON_VueforceController.RemoteResponse resp = new BI_COMMON_VueforceController.RemoteResponse();
		resp.records = rowsByPosition.values();
		return resp;
	}

	/*
	Para ponerlo en la pag visual:
	-page: BI_PL_US_Deg_Home_Dashboard
	-linea: 447-448 crear un div hermano a call_reason donde estara el componente con la tabla con sus 4 valores
	*/
	@RemoteAction
	public static BI_PL_CountFlag flagCount(List<String> preparations, String idOffset){ //String cycle, String hierarchy, String channel
		//Consulta sobre PLANiT Target Preparation filtrando cycle, hierarchy y channel para obtener losflags
		BI_PL_CountFlag res = new BI_PL_CountFlag();

		List<BI_PL_Channel_detail_preparation__c> test = [SELECT Id, BI_PL_Target__r.BI_PL_Added_manually__c, BI_PL_Target__r.BI_PL_No_see_list__c,
															BI_PL_Target__r.BI_PL_Next_best_account__c, BI_PL_MSL_flag__c, BI_PL_Rejected__c, BI_PL_Removed__c, BI_PL_Reviewed__c
															FROM BI_PL_Channel_detail_preparation__c WHERE /*BI_PL_Channel__c = :channel 
															AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycle
															AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchy*/
															BI_PL_Target__r.BI_PL_Header__c IN :preparations
															AND Id >:idOffset order by Id asc LIMIT 10000];

		for(BI_PL_Channel_detail_preparation__c rath : test){
			//System.debug('RATH :: ' + rath);
			res.Next_best = ((rath.BI_PL_Target__r.BI_PL_Next_best_account__c) ? res.Next_best+1 : res.Next_best+0);
			res.Added_Manually = ((rath.BI_PL_Target__r.BI_PL_Added_manually__c) ? res.Added_Manually+1 : res.Added_Manually+0);
			res.No_See_List = ((rath.BI_PL_Target__r.BI_PL_No_see_list__c) ? res.No_See_List+1 : res.No_See_List+0);
			res.Removed = ((rath.BI_PL_Removed__c) ? res.Removed+1 : res.Removed+0);
			res.Reviewed = ((rath.BI_PL_Reviewed__c) ? res.Reviewed+1 : res.Reviewed+0);
			res.Rejected = ((rath.BI_PL_Rejected__c) ? res.Rejected+1 : res.Rejected+0);
			//res.Dropped = ((rath.BI_PL_Target__r.BI_PL_Next_best_account__c) ? res.Dropped+1 : res.Dropped+0);
			res.MSL_Flag = ((rath.BI_PL_MSL_flag__c) ? res.MSL_Flag+1 : res.MSL_Flag+0);
		}
		res.Total = test.size();
		System.debug('RESULTADOS :: ' + res);

		if(test.size() == 10000){
			res.lastId = test.get(test.size()-1).Id;
		}else{
			res.lastId = null;
		}
		return res;
	}

	public class BI_PL_CountFlag{
		public Integer Next_best {get; set;}
        public Integer Added_Manually {get; set;}
        public Integer No_See_List {get; set;}
        
        public Integer Removed {get; set;}
        public Integer Reviewed {get; set;}
        public Integer Rejected {get; set;}
        //public Integer Dropped {get; set;}
        public Integer MSL_Flag {get; set;}
        public Integer Total {get; set;}

        public String lastId {get; set;}

        public BI_PL_CountFlag(){
        	Next_best = 0;
        	Added_Manually = 0;
        	No_See_List = 0;

        	Removed = 0;
        	Reviewed = 0;
        	Rejected = 0;
        	MSL_Flag = 0;

        	Total = 0;

        	lastId = null;
        }
	}

	@RemoteAction
	public static BI_PL_DynamicFlag dynFlagCount(List<String> preparations, String next_best, String added_Manually, String no_See_List, String removed, String reviewed, String rejected, String MSL_Flag, String idOffset){

		BI_PL_DynamicFlag exp = new BI_PL_DynamicFlag();

		String test = 'SELECT Id, BI_PL_Target__r.BI_PL_Added_manually__c, BI_PL_Target__r.BI_PL_No_see_list__c, BI_PL_Target__r.BI_PL_Next_best_account__c, BI_PL_MSL_flag__c, BI_PL_Rejected__c,'+
						'BI_PL_Removed__c, BI_PL_Reviewed__c FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Target__r.BI_PL_Header__c IN :preparations AND Id >:idOffset';
		/*BI_PL_Channel__c = :channel AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycle AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchy*/
		switch on next_best{
			when 'true'{test += ' AND BI_PL_Target__r.BI_PL_Next_best_account__c = true';}
			when 'false'{test += ' AND BI_PL_Target__r.BI_PL_Next_best_account__c = false';}
			when else{test += '';}
		}
		switch on added_Manually{
			when 'true'{test += ' AND BI_PL_Target__r.BI_PL_Added_manually__c = true';}
			when 'false'{test +=  ' AND BI_PL_Target__r.BI_PL_Added_manually__c = false';}
			when else{test += '';}
		}
		switch on no_See_List{
			when 'true'{test += ' AND BI_PL_Target__r.BI_PL_No_see_list__c = true';}
			when 'false'{test +=  ' AND BI_PL_Target__r.BI_PL_No_see_list__c = false';}
			when else{test += '';}
		}
		switch on removed{
			when 'true'{test += ' AND BI_PL_Removed__c = true';}
			when 'false'{test += ' AND BI_PL_Removed__c = false';}
			when else{test += '';} 
		}
		switch on reviewed{
			when 'true'{test += ' AND BI_PL_Reviewed__c = true';}
			when 'false'{test +=  ' AND BI_PL_Reviewed__c = false';}
			when else{test += '';}
		}
		switch on rejected{
			when 'true'{test += ' AND BI_PL_Rejected__c = true';}
			when 'false'{test +=  ' AND BI_PL_Rejected__c = false';}
			when else{test += '';}
		}
		switch on MSL_Flag{
			when 'true'{test += ' AND BI_PL_MSL_flag__c = true';}
			when 'false'{test +=  ' AND BI_PL_MSL_flag__c = false';}
			when else{test += '';} 
		}

		test += ' ORDER BY Id ASC LIMIT 10000';

		List<BI_PL_Channel_detail_preparation__c> estes = Database.query(test);

		if(estes.size() == 10000){
			exp.lastId = estes.get(estes.size()-1).Id;
		}else{
			exp.lastId = null;
		}
		exp.aux = estes.size();
		return exp;
	}

	public class BI_PL_DynamicFlag{
		public Integer aux {get; set;}
		public String lastId {get; set;}
		public BI_PL_DynamicFlag(){
			aux = 0;
			lastId = null;
		}
	}
}