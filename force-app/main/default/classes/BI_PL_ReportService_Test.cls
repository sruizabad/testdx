@isTest private class BI_PL_ReportService_Test {


	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        Date startDate = Date.today()-5;
        Date endDate =  Date.today()+5;
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);

        startDate = Date.today()+15;
        endDate =  Date.today()+30;
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);

    }
	
	@isTest static void test() {

		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

		Test.startTest();
		BI_COMMON_VueforceController.RemoteResponse resp = BI_PL_ReportService.getPositionOverlapReport(
			null, [SELECT Id FROM BI_PL_Cycle__c LIMIT 1].Id, BI_PL_TestDataFactory.hierarchy, BI_PL_TestDataFactory.SCHANNEL, null, null, null, null
		);
		Test.stopTest();

	}
    @isTest static void test1() {
        List<BI_PL_Channel_detail_preparation__c> test123 = [SELECT Id, BI_PL_Target__r.BI_PL_Header__c FROM BI_PL_Channel_detail_preparation__c];
        List<String> test321 = new List<String>();
        for(BI_PL_Channel_detail_preparation__c ex : test123){ test321.add(ex.BI_PL_Target__r.BI_PL_Header__c);}
        BI_PL_ReportService.flagCount(test321, '');

        BI_PL_ReportService.dynFlagCount(test321, 'true', 'true', 'true', 'true', 'true', 'true', 'true', '');
        BI_PL_ReportService.dynFlagCount(test321, 'false', 'false', 'false', 'false', 'false', 'false', 'false', '');
        BI_PL_ReportService.dynFlagCount(test321, 'no include', 'no include', 'no include', 'no include', 'no include', 'no include', 'no include', '');
    }
}