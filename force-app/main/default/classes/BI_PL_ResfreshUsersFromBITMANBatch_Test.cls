@isTest
private class BI_PL_ResfreshUsersFromBITMANBatch_Test {
	
	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        Date startDate = Date.today()-5;
        Date endDate =  Date.today()+5;
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);

        startDate = Date.today()+15;
        endDate =  Date.today()+30;
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);

    }
	
	@isTest static void test() {

		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

		Test.startTest();
		Database.executeBatch(new BI_PL_RefreshUsersFromBITMANBatch(userCountryCode));
		Test.stopTest();

	}

	/*@isTest static void test2() {

		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

        List<String> cyclesList = new List<String>();
        for(BI_PL_Cycle__c c: [SELECT Id FROM BI_PL_Cycle__c]){
        	cyclesList.add(c.Id);
        }
		Test.startTest();
		Database.executeBatch(new BI_PL_UpdateUsersInVeevaBatch(userCountryCode, cyclesList));
		Test.stopTest();

	}


	@isTest static void test3() {

		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

        List<String> cyclesList = new List<String>();
        for(BI_PL_Cycle__c c: [SELECT Id FROM BI_PL_Cycle__c]){
        	cyclesList.add(c.Id);
        }
		Test.startTest();
		Database.executeBatch(new BI_PL_UpdateUsersInPLANITBatch(userCountryCode, cyclesList));
		Test.stopTest();

	}*/
	
}