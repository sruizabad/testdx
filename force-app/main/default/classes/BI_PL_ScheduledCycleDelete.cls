global class BI_PL_ScheduledCycleDelete implements Schedulable{

	global void execute(SchedulableContext ctx){
	    Database.executeBatch(new BI_PL_DeleteCycleBatch());
		
	}

}