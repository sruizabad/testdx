global class BI_PL_SplitCycleBatch implements Database.Batchable<sObject>, Database.Stateful {


    global Id cycleId;

    global boolean cyclesCreated;
    global List<BI_PL_Cycle__c> cyclesGeneral;
    global Map<Integer, List<Integer>> frequencies;
    global Map<Id, List<BI_PL_Position_cycle__c>> positionsCycles;
    private static Integer MONTHSTOSPLIT = 12;
    global List<Database.Error> error;
    global Set<String> mapErrors;
    global String cycleName;
    global String cycleFF;
    global Set<String> hierarchies;
    global String productId;

    public BI_PL_SplitCycleBatch (String cycle, Map<Integer, List<Integer>> freq, String pId) {

        this.cycleId = cycle;
        this.cyclesCreated = false;
        this.cyclesGeneral = new List<BI_PL_Cycle__c>();
        this.frequencies = freq;
        this.positionsCycles = new Map<Id, List<BI_PL_Position_cycle__c>>();
        this.mapErrors = new Set<String>();
        this.hierarchies = new Set<String>();
        this.productId = pId;

        //this.error = new List<Database.Error>();

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('### - BI_PL_SplitCycleBatch - start');

        string sQuery = 'SELECT Id, ' +
                        'BI_PL_Hierarchy__c, ' +
                        'BI_PL_Parent_position__c, ' +
                        'BI_PL_Position__c, ' +
                        'BI_PL_Position__r.Name, ' +
                        'BI_PL_External_id__c, ' +
                        'BI_PL_Cycle__c, ' +
                        'BI_PL_Confirmed__c, ' +
                        'BI_PL_Rejected_reason__c, ' +
                        'BI_PL_Cycle__r.BI_PL_Country_code__c, ' +
                        'BI_PL_Cycle__r.BI_PL_End_date__c, ' +
                        'BI_PL_Cycle__r.BI_PL_Field_force__c, ' +
                        'BI_PL_Cycle__r.BI_PL_Field_force__r.Name, ' +
                        'BI_PL_Cycle__r.BI_PL_Global_capacity__c, ' +
                        'BI_PL_Cycle__r.BI_PL_Start_date__c, ' +
                        'BI_PL_Cycle__r.BI_PL_External_id__c, ' +
                        'BI_PL_Cycle__r.Name ' +
                        'FROM BI_PL_Position_cycle__c ' +
                        'WHERE BI_PL_Cycle__c =\'' + cycleId + '\'' +
                        (Test.isRunningTest() ? ' LIMIT 100' : '');
        
        return Database.getQueryLocator(sQuery);
        /*
        return Database.getQueryLocator([
                                            SELECT Id,
                                            BI_PL_Hierarchy__c,
                                            BI_PL_Parent_position__c,
                                            BI_PL_Position__c,
                                            BI_PL_Position__r.Name,
                                            BI_PL_External_id__c,
                                            BI_PL_Cycle__c,
                                            BI_PL_Confirmed__c,
                                            BI_PL_Rejected_reason__c,
                                            BI_PL_Cycle__r.BI_PL_Country_code__c,
                                            BI_PL_Cycle__r.BI_PL_End_date__c,
                                            BI_PL_Cycle__r.BI_PL_Field_force__c,
                                            BI_PL_Cycle__r.BI_PL_Field_force__r.Name,
                                            BI_PL_Cycle__r.BI_PL_Global_capacity__c,
                                            BI_PL_Cycle__r.BI_PL_Start_date__c,
                                            BI_PL_Cycle__r.BI_PL_External_id__c,
                                            BI_PL_Cycle__r.Name
                                            FROM BI_PL_Position_cycle__c
                                            WHERE BI_PL_Cycle__c = :cycleId

                                        ]);
        */
    }

    global void execute(Database.BatchableContext BC, List<BI_PL_Position_cycle__c> scope) {
        System.debug('### - BI_PL_SplitCycleBatch - execute');
        try {

            this.cycleName = scope.get(0).BI_PL_Cycle__r.Name;
            this.cycleFF = scope.get(0).BI_PL_Cycle__r.BI_PL_Field_force__r.Name;

            Set<Id> pcIds = new Set<Id>();
            for (BI_PL_Position_cycle__c pc : scope) {
                pcIds.add(pc.id);
                this.hierarchies.add(pc.BI_PL_Hierarchy__c);
            }

            List<BI_PL_Position_cycle_user__c> listUsers = [SELECT id, BI_PL_External_id__c, BI_PL_User__c, BI_PL_User__r.External_ID__c, BI_PL_Position_cycle__c, BI_PL_Position_cycle__r.BI_PL_External_id__c,
                                               BI_PL_preparation_owner__c FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c in :pcIds];

            Map<Id, List<BI_PL_Position_cycle_user__c>> mapUsers = new Map<Id, List<BI_PL_Position_cycle_user__c>>();

            for (BI_PL_Position_cycle_user__c user : listUsers) {
                if (!mapUsers.containsKey(user.BI_PL_Position_cycle__c)) {
                    mapUsers.put(user.BI_PL_Position_cycle__c, new List<BI_PL_Position_cycle_user__c>());
                    mapUsers.get(user.BI_PL_Position_cycle__c).add(user);
                } else {
                    mapUsers.get(user.BI_PL_Position_cycle__c).add(user);
                }
            }
            Map<String, BI_PL_Cycle__c> listCycle = new Map<String, BI_PL_Cycle__c>();

            if (this.cyclesCreated == false) {
                BI_PL_Cycle__c bigCycle = scope.get(0).BI_PL_Cycle__r;
                Date startDate = bigCycle.BI_PL_Start_date__c;
                Date endDate = bigCycle.BI_PL_End_date__c;
                Date currentDate = startDate;
                Integer year = endDate.year();
                Date currentEndDate;

                for (integer i = 1; i < MONTHSTOSPLIT + 1; i++) {

                    BI_PL_Cycle__c cycle = new BI_PL_Cycle__c();
                    cycle.Name = bigCycle.Name + ' ' + i;

                    //Start and End Date of the current month
                    cycle.BI_PL_Start_date__c = currentDate;
                    cycle.BI_PL_End_date__c = currentDate.addDays(Date.daysInMonth(year, i) - 1);
                    currentDate = currentDate.addMonths(1);

                    //System.debug('currentDate: ' + currentDate + ' endDate: ' +currentDate.addDays(Date.daysInMonth(year, i) - 1));
                    cycle.BI_PL_Field_force__c = bigCycle.BI_PL_Field_force__c;
                    //cycle.BI_PL_Field_force__r = bigCycle.BI_PL_Field_force__r;
                    cycle.BI_PL_Global_capacity__c = bigCycle.BI_PL_Global_capacity__c;
                    cycle.BI_PL_Country_code__c = bigCycle.BI_PL_Country_code__c;
                    cycle.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId(bigCycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, this.cycleFF);
                    cycle.BI_PL_Type__c = 'child';
                    cycle.BI_PL_Parent__c = this.cycleId;
                    //System.debug('** External Id: ' + cycle.BI_PL_External_id__c);
                    //upsert cycle BI_PL_External_id__c;

                    listCycle.put(cycle.BI_PL_External_id__c, cycle);

                }

                if (listCycle.size() == MONTHSTOSPLIT) {
                    Schema.SObjectField field = BI_PL_cycle__c.Fields.BI_PL_External_id__c;
                    Database.UpsertResult [] cr = Database.upsert(listCycle.values(), field, false);
                    for (Database.UpsertResult theResult : cr) {
                        if (!theResult.isSuccess()) {
                            for (Database.Error error : theResult.getErrors()) {
                                system.debug('## Error ' + mapErrors);
                                mapErrors.add(error.getMessage());

                            }
                        }
                    }
                }
            }


            Map<String, BI_PL_Position_cycle__c> listnewpc = new Map<String, BI_PL_Position_cycle__c>();
            List<Id> cycleIds = new List<Id>();
            for (BI_PL_Cycle__c c : listCycle.values()) {
                cycleIds.add(c.Id);
            }
            System.debug('Cycles after insert ' + listCycle );

            for (BI_PL_Position_cycle__c pc : scope) {
                for (BI_PL_Cycle__c cycle : listCycle.values()) {

                    BI_PL_Position_cycle__c newpc = clonePositionCycle(pc, cycle);
                    listnewpc.put(newpc.BI_PL_External_id__c, newpc);

                }
            }
            //insert listnewpc;
            Schema.SObjectField field = BI_PL_Position_cycle__c.Fields.BI_PL_External_id__c;
            Database.UpsertResult [] cr = Database.upsert(listnewpc.values(), field, false);
            Integer i = 0;
            for (Database.UpsertResult theResult : cr) {
                if (!theResult.isSuccess()) {
                    for (Database.Error error : theResult.getErrors()) {
                        system.debug('## Error ' + mapErrors);
                        mapErrors.add(error.getMessage());
                        // throw new BI_PL_Exception('>>'+listnewpc.values().get(i)+' '+error.getMessage());

                    }
                }
                i++;
            }

            for (BI_PL_Position_cycle__c poscycle : listnewpc.values()) {
                if (this.positionsCycles.containsKey(poscycle.BI_PL_Cycle__c)) {
                    this.positionsCycles.get(poscycle.BI_PL_Cycle__c).add(poscycle);
                } else {
                    this.positionsCycles.put(poscycle.BI_PL_Cycle__c, new List<BI_PL_Position_cycle__c>());
                    this.positionsCycles.get(poscycle.BI_PL_Cycle__c).add(poscycle);
                }
            }
            Map<String, BI_PL_Position_cycle_user__c> listUsersToUpsert = new Map<String, BI_PL_Position_cycle_user__c>();
            for (BI_PL_Position_cycle__c pc : scope) {
                if (mapUsers.containsKey(pc.id)) {

                    //listUsersToUpsert.addAll(clonePositionCycleUser(listnewpc.values(),mapUsers.get(pc.id), cycles));
                    listUsersToUpsert.putAll(clonePositionCycleUser(listnewpc.values(), mapUsers.get(pc.id), listCycle.values()));

                }
            }

            field = BI_PL_Position_cycle_user__c.Fields.BI_PL_External_id__c;
            cr = Database.upsert(listUsersToUpsert.values(), field, false);
            for (Database.UpsertResult theResult : cr) {
                if (!theResult.isSuccess()) {
                    for (Database.Error error : theResult.getErrors()) {
                        mapErrors.add(error.getMessage());
                        system.debug('## Error ' + mapErrors);
                    }
                }
            }

            this.cyclesGeneral = listCycle.values();
        } catch (Exception error) {
            mapErrors.add(error.getMessage());
            system.debug('## Error ' + mapErrors);
            BI_PL_ErrorHandlerUtility.addPlanitServerError('BI_PL_SplitCycleBatch', BI_PL_ErrorHandlerUtility.FUNCTIONALITY_SPLIT_CYCLE, error.getMessage(), error.getStackTraceString());

        }




    }

    global void finish(Database.BatchableContext BC) {

        System.debug('### - BI_PL_SplitCycleBatch - finish');
        System.debug('### - error: ' + this.error);
        if (mapErrors.size() == 0 && !Test.isRunningTest() ) {
            Database.executeBatch(new BI_PL_FillChildCyclesBatch(this.cycleId, this.frequencies, this.cyclesGeneral, this.positionsCycles, this.cycleName,this.hierarchies, this.productId), 10);
        } else {
            createEmail();
        }


    }





    private BI_PL_Position_cycle__c clonePositionCycle(BI_PL_Position_cycle__c positionCycle, BI_PL_Cycle__c cycle) {
        BI_PL_Position_cycle__c newPositionCycle = new BI_PL_Position_cycle__c();
        newPositionCycle.BI_PL_Confirmed__c = positionCycle.BI_PL_Confirmed__c;

        newPositionCycle.BI_PL_Cycle__c = cycle.Id;
        //newPositionCycle.BI_PL_Cycle__r = cycle;
        newPositionCycle.BI_PL_Hierarchy__c = positionCycle.BI_PL_Hierarchy__c;
        newPositionCycle.BI_PL_Parent_position__c = positionCycle.BI_PL_Parent_position__c;
        newPositionCycle.BI_PL_Position__c = positionCycle.BI_PL_Position__c;
        //newPositionCycle.BI_PL_Position__r = positionCycle.BI_PL_Position__r;
        newPositionCycle.BI_PL_Rejected_reason__c = positionCycle.BI_PL_Rejected_reason__c;
        newPositionCycle.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, this.cycleFF, positionCycle.BI_PL_Hierarchy__c, positionCycle.BI_PL_Position__r.Name);
        return newPositionCycle;
    }

    private Map<String, BI_PL_Position_cycle_user__c> clonePositionCycleUser(List<BI_PL_Position_cycle__c> positionsNew, List<BI_PL_Position_cycle_user__c> users, List<BI_PL_Cycle__c> cycles) {
        //System.debug('****cloning position cycle user');
        Map<String, BI_PL_Position_cycle_user__c> newUsers = new Map<String, BI_PL_Position_cycle_user__c>();
        Map<Id, BI_PL_Cycle__c> cycleMap = new map<Id, BI_PL_Cycle__c>();
        for (BI_PL_Cycle__c c : cycles) {
            cycleMap.put(c.id, c);
        }
        for (BI_PL_Position_cycle__c posNew : positionsNew) {
            BI_PL_Cycle__c cycle = cycleMap.get(posNew.BI_PL_Cycle__c);
            //System.debug('** Strat date: ' + cycle.BI_PL_Start_date__c + ' End Date: ' + cycle.BI_PL_End_date__c);
            String hierarchy = posNew.BI_PL_Hierarchy__c;

            String position = posNew.BI_PL_Position__r.Name;
            for (BI_PL_Position_cycle_user__c u : users) {
                if (u.BI_PL_Position_cycle__r.BI_PL_External_id__c.contains(hierarchy + '_' + position)) {


                    BI_PL_Position_cycle_user__c newUser = new BI_PL_Position_cycle_user__c();
                    newUser.BI_PL_Position_cycle__c = posNew.id;
                    //newUser.BI_PL_User__r = u.BI_PL_User__r;
                    newUser.BI_PL_User__c = u.BI_PL_User__c;
                    newUser.BI_PL_preparation_owner__c = u.BI_PL_preparation_owner__c;
                    newUser.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_start_date__c, cycle.BI_PL_end_date__c,
                                                   cycle.BI_PL_Field_force__r.Name, posNew.BI_PL_Hierarchy__c, posNew.BI_PL_Position__r.Name, u.BI_PL_User__r.External_ID__c);
                    //System.debug('** USers external id: ' + newUser.BI_PL_External_id__c);
                    newUsers.put(newUser.BI_PL_External_id__c, newUser);
                }
            }
        }
        return newUsers;
        //insert newUsers;
        /* Schema.SObjectField field = BI_PL_Position_cycle_user__c.Fields.BI_PL_External_id__c;
         Database.UpsertResult [] cr = Database.upsert(newUsers, field, false);
         for(Database.UpsertResult theResult : cr) {
           if(!theResult.isSuccess()){
              error = theResult.getErrors();
           }
         }*/

    }

    public void createEmail() {
        String subject;
        String body = '';


        subject = Label.BI_PL_split_cycle_subject_failed + ' ' + this.cycleName;
        body += '<p>' + Label.BI_PL_split_cycle_body_failed + ' ' + '</p>';

        for (String message : mapErrors) {
            body += '<p>' + message + '</p>';

        }


        sendEmail(subject, body);
    }

    public void sendEmail(String subject, String body) {
        try {
            sendEmailWithTemplate(subject, body);
        } catch (exception e) {
            System.debug('Unable to send email: ' + e.getMessage());
        }
    }


    public void sendEmailWithTemplate(String subject, String body) {
        Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
        emailToSend.setTargetObjectId( UserInfo.getUserId() );
        emailToSend.setSaveAsActivity( false );
        emailToSend.setHTMLBody(body);
        emailToSend.setSubject(subject);

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {emailToSend});
    }


}