@isTest
public with sharing class BI_PL_SplitCycleBatch_Test {
	
    private static final String SHIERARCHY = BI_PL_TestDataFactory.hierarchy;

    private static List<BI_PL_Position_Cycle__c> posCycles;
	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c; 

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        //Date startDate = Date.today()-60;
        //Date endDate =  Date.today()-30;
        //Need complete Year for split
        Integer iYear = Integer.valueof( Date.today().year() );
        Date startDate = Date.newInstance(iYear, 1, 1);
        Date endDate = Date.newInstance(iYear, 12, 31);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode,startDate,endDate);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];

        posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);

    }

    @isTest public static void test() {

        String[] hierarchy = new String[]{SHIERARCHY};
        
        List<BI_PL_Cycle__c> cycles = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c, Name FROM BI_PL_Cycle__c ORDER BY BI_PL_Start_date__c ASC];
            
        Map<String, List<Integer>> listDetails = new Map<String, List<Integer>>();
        listDetails = BI_PL_SplitCycleCtrl.getDetails(cycles[0].Id);

        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

        Map<Integer,List<Integer>> frequencies = new Map<Integer,List<Integer>>();

        for(Integer freq : listDetails.get('ALL')){

            if(!frequencies.containsKey(freq)){
                frequencies.put(freq, new List<Integer>());
            }

            for(Integer i = 0; i<12;i++){
                if(i < 5){
                    frequencies.get(freq).add(1);
                }else{
                    frequencies.get(freq).add(0);
                }
            }
        }

        List<BI_PL_Position_cycle_user__c> listPCU = new List<BI_PL_Position_cycle_user__c>();
        for (BI_PL_Position_Cycle__c posCycle : [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c]){
            listPCU.add(
                new BI_PL_Position_cycle_user__c(
                    BI_PL_Position_cycle__c = posCycle.Id,
                    BI_PL_User__c = userCountry.Id
                )
            );
        }
        insert listPCU;

        Test.startTest();
        
        Id batchId = Database.executeBatch(new BI_PL_SplitCycleBatch(cycles[0].Id,frequencies,'') );

        Test.stopTest();

        System.assertNotEquals([SELECT Id FROM CronTrigger WHERE Id = :batchId], null);

    }

    @isTest public static void test_Fillchild() {

        set<String> setHierarchy = new set<String>{SHIERARCHY};
        
        List<BI_PL_Cycle__c> cycles = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c, Name FROM BI_PL_Cycle__c ORDER BY BI_PL_Start_date__c ASC];
            
        Map<String, List<Integer>> listDetails = new Map<String, List<Integer>>();
        listDetails = BI_PL_SplitCycleCtrl.getDetails(cycles[0].Id);

        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

        Map<Integer,List<Integer>> frequencies = new Map<Integer,List<Integer>>();

        for(Integer freq : listDetails.get('ALL')){

            if(!frequencies.containsKey(freq)){
                frequencies.put(freq, new List<Integer>());
            }

            for(Integer i = 0; i<12;i++){
                if(i < 5){
                    frequencies.get(freq).add(1);
                }else{
                    frequencies.get(freq).add(0);
                }
            }
        }
        Map<Id, List<BI_PL_Position_cycle__c>> mapPosCycle = new Map<Id, List<BI_PL_Position_cycle__c>>();
        List<BI_PL_Position_cycle_user__c> listPCU = new List<BI_PL_Position_cycle_user__c>();
        posCycles = [SELECT Id, BI_PL_External_Id__c, BI_PL_Cycle__c FROM BI_PL_Position_Cycle__c];

        for (BI_PL_Position_Cycle__c posCycle : posCycles){
            if (!mapPosCycle.containsKey(posCycle.BI_PL_Cycle__c) ) mapPosCycle.put(posCycle.BI_PL_Cycle__c, new List<BI_PL_Position_cycle__c>());
            mapPosCycle.get(posCycle.BI_PL_Cycle__c).add(posCycle);

            listPCU.add(
                new BI_PL_Position_cycle_user__c(
                    BI_PL_Position_cycle__c = posCycle.Id,
                    BI_PL_User__c = userCountry.Id
                )
            );
        }
        insert listPCU;

        Test.startTest();
        
        Id batchId = Database.executeBatch( new BI_PL_FillChildCyclesBatch(cycles.get(0).Id, frequencies, cycles, mapPosCycle, cycles.get(0).Name, setHierarchy, '') );

        Test.stopTest();

        System.assertNotEquals([SELECT Id FROM CronTrigger WHERE Id = :batchId], null);
    }
}