/**
 *	Retrieves the necesary data for the BI_PL_SplitCyle component.
 *
 *	@author	OMEGA CRM
 */
public without sharing class BI_PL_SplitCycleCtrl {


	@ReadOnly
	@RemoteAction
	public static List<Product_vod__c> getProducts(String idCycle){



		return [SELECT Id, Name,External_ID_vod__c,Active_BI__c FROM Product_vod__c WHERE Id IN (SELECT BI_PL_Product__c FROM BI_PL_Detail_preparation__c WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = : idCycle) ];
	}

	@ReadOnly
	@RemoteAction
	public static Map<String, List<Integer>> getDetails(String idCycle){

		Map<String,Set<Integer>> mapDetails = new Map<String,Set<Integer>>();

		for(BI_PL_Detail_preparation__c ag : [SELECT BI_PL_Product__c, BI_PL_Planned_details__c, BI_PL_Adjusted_details__c FROM BI_PL_Detail_preparation__c WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :idCycle order by BI_PL_Planned_details__c]){

			if(mapDetails.containsKey(ag.BI_PL_Product__c)){
				mapDetails.get(ag.BI_PL_Product__c).add((Integer)ag.BI_PL_Planned_details__c);
				mapDetails.get(ag.BI_PL_Product__c).add((Integer)ag.BI_PL_Adjusted_details__c);
			}else{
				mapDetails.put(ag.BI_PL_Product__c,new Set<Integer>());
				mapDetails.get(ag.BI_PL_Product__c).add((Integer)ag.BI_PL_Planned_details__c);
				mapDetails.get(ag.BI_PL_Product__c).add((Integer)ag.BI_PL_Adjusted_details__c);
			}
				
			if(mapDetails.containsKey('ALL')){
				mapDetails.get('ALL').add((Integer)ag.BI_PL_Planned_details__c);
				mapDetails.get('ALL').add((Integer)ag.BI_PL_Adjusted_details__c);
			}else{
				mapDetails.put('ALL',new Set<Integer>());
				mapDetails.get('ALL').add((Integer)ag.BI_PL_Planned_details__c);
				mapDetails.get('ALL').add((Integer)ag.BI_PL_Adjusted_details__c);			
			}			
		}

		Map<String, List<Integer>> mapDetailsToReturn = new Map<String, List<Integer>>();

		for(String product : mapDetails.keySet()){
			List<Integer> listToSort = new List<Integer>(mapDetails.get(product));
			listToSort.sort();

			mapDetailsToReturn.put(product,listToSort);
		}

		return mapDetailsToReturn;

	}


	/**
	 * @author	OMEGA CRM
	 * @return  The setup data.
	 */
	@RemoteAction
	public static PlanitSetupModel getSetupData() {
		PlanitSetupModel setup = new PlanitSetupModel();

		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		setup.userCountryCode = currentUser.Country_Code_BI__c;

		BI_PL_Cycle__c[] cycles = new List<BI_PL_Cycle__c>([SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, Name  FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :setup.userCountryCode AND BI_PL_Type__c = 'active' ORDER BY Name]);

		//Get current cycle
		BI_PL_Cycle__c[] cycle = new List<BI_PL_Cycle__c>([SELECT Id FROM BI_PL_Cycle__c WHERE BI_PL_Start_date__c > TODAY AND BI_PL_Country_code__c = :setup.userCountryCode AND BI_PL_Type__c = 'active' ORDER BY BI_PL_Start_date__c LIMIT 1]);
		String currentCycleId = '';
	
		if (cycle.size() > 0){

			currentCycleId = cycle.get(0).Id;
		}
		/*else{
			if(cycles.size() > 0){
				currentCycleId = cycles.get(0).Id;
			}
		}*/

		
		setup.cycles = cycles;
		setup.currentCycleId = currentCycleId;


		return setup;
	}

	/**
	 * @author	OMEGA CRM
	 * 
	 */
	@RemoteAction
	public static String splitCycleBatch(String cycleId, Map<Integer,List<Integer>> frequencies, String productId){
		System.debug('splitCycleBatch');
		System.debug(frequencies);

		BI_PL_SplitCycleBatch split = new BI_PL_SplitCycleBatch(cycleId, frequencies, productId);
		String batchId = Database.executeBatch(split,100);
		return batchId;

	}


	/**
	 *  Checks the status of the refresh visibiliy batch to display the results in the view.
	 *  @author OMEGA CRM
	 */
	 @RemoteAction
	public static PlanitBatchModel checkBatchStatus(String batchId) {
		Boolean loading;
		Boolean completed=false;
		String errors = '';
		String batchStatus= '';
		if (batchId != null) {
			AsyncApexJob aaj = [SELECT Id, Status, ExtendedStatus From AsyncApexJob where Id = :batchId LIMIT 1];
			batchStatus = aaj.Status;
			System.debug('*--* aaj: ' + aaj);
			System.debug('*--* status: ' + batchStatus);
			loading = true;

			if (batchStatus == 'Completed') {
				loading = false;

				if (String.isNotBlank(aaj.ExtendedStatus)) {
					errors = Label.BI_PL_Error_during_process;
				} else {
					completed = true;
				}
			} else if (batchStatus == 'Failed' || batchStatus == 'Aborted') {
				errors = Label.BI_PL_Error_during_process;
				loading = false;
			}
		}
		PlanitBatchModel wrapper = new PlanitBatchModel();
		wrapper.batchStatus = batchStatus;
		wrapper.errors = errors;
		wrapper.loading = loading;
		return wrapper;
	}

	public class PlanitSetupModel {

	
		public String userCountryCode;
		public List<BI_PL_Cycle__c> cycles {get; set;}
		public String currentCycleId {get; set;}

		public PlanitSetupModel() {
		}
	}

	public class PlanitBatchModel {

		public String batchStatus;
		public String errors;
		public Boolean loading;

		public PlanitBatchModel() {
		}
	}
}