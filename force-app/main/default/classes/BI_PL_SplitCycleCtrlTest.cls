@isTest
public class BI_PL_SplitCycleCtrlTest {

	private static List<User> listusers;
	private static List<Product_vod__c> listproducts;
	private static List<Account> listaccounts;
	private static Map<Id, List<BI_PL_Position_cycle_user__c>> positionCycleUsersByPosition = new Map<Id, List<BI_PL_Position_cycle_user__c>>();
	private static Map<String, User> users;
	private static BI_PL_Cycle__c cycle;

	@testSetup static void setup(){

		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting('BR');
		//listusers = BI_PL_TestDataFactory.createTestUsers(2,'BR');
		listproducts = BI_PL_TestDataFactory.createTestProduct(1,'BR');
		listaccounts = BI_PL_TestDataFactory.createTestAccounts(1,'BR');
		users = BI_PL_SplitCycleCtrlTest.usersCreation('BR');

		//BI_PL_TestDataFactory.createCycleStructure('BR');

	
		

		List<BI_PL_Position_cycle__c> listpc = BI_PL_SplitCycleCtrlTest.generateCycle();

		

		BI_PL_TestDataFactory.createPreparations('BR',listpc,listaccounts,listproducts);
		
		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'testFieldforce', BI_TM_Country_code__c = 'BR');
		insert fieldForce;

		BI_PL_Cycle__c voidCycle = new BI_PL_Cycle__c(Name = 'voidCycle', BI_PL_Start_date__c = Date.newInstance(2018, 1, 11), BI_PL_End_date__c = Date.newInstance(2018, 1, 14), BI_PL_Field_force__c = fieldForce.Id, BI_PL_Country_Code__c = 'BR');
		insert voidCycle;



	}





	@isTest static void test1(){

		User currentUser = [SELECT id, Country_Code_BI__c, Name, Username FROM User WHERE Country_Code_BI__c = 'BR' and LastName = 'PositionChild2User'];
		Map<String, List<Integer>> listDetails = new Map<String, List<Integer>>();
		cycle = [SELECT id FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = 'BR' LIMIT 1];
		BI_PL_Cycle__c voidCycle = [SELECT Id FROM BI_PL_Cycle__c WHERE Name = 'voidCycle' LIMIT 1];
		System.runAs(currentUser){
				
			listDetails = BI_PL_SplitCycleCtrl.getDetails(cycle.id);
			System.assert(listDetails.size() > 0);

			BI_PL_SplitCycleCtrl.PlanitSetupModel psm = BI_PL_SplitCycleCtrl.getSetupData();

			Map<Integer,List<Integer>> frequencies = new Map<Integer,List<Integer>>();

			for(Integer freq : listDetails.get('ALL')){

				if(!frequencies.containsKey(freq)){
					frequencies.put(freq, new List<Integer>());
				}

				for(Integer i = 0; i<12;i++){
					if(i < 5){
						frequencies.get(freq).add(1);
					}else{
						frequencies.get(freq).add(0);
					}
				}

			}

			BI_PL_Country_settings__c cs = new BI_PL_Country_settings__c();
			cs.Name = 'CSName';
			cs.BI_PL_Specialty_field__c = 'SpecialiTest';
			cs.BI_PL_Country_code__c = currentUser.Country_Code_BI__c;
			cs.BI_PL_Default_owner_user_name__c = currentUser.Username;
			insert cs;

			String batchId = BI_PL_SplitCycleCtrl.splitCycleBatch(cycle.id,frequencies,'');
			String batchId2 = BI_PL_SplitCycleCtrl.splitCycleBatch(voidCycle.id,frequencies,'');

			BI_PL_SplitCycleCtrl.PlanitBatchModel model = BI_PL_SplitCycleCtrl.checkBatchStatus(batchId);
			
			

		

			
		}

	}


	private static List<BI_PL_Position_cycle__c> generateCycle(){

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'fielfforcename', BI_TM_Country_code__c = 'BR');

		insert fieldForce;
		Date startDate = Date.newInstance(2017, 1, 1);
		Date endDate = Date.newInstance(2017,  12, 31);
		cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'BR', BI_PL_Start_date__c = startDate, BI_PL_End_date__c = endDate, BI_PL_Field_force__c = fieldForce.Id, BI_PL_Field_force__r = fieldForce, BI_PL_Type__c = 'active');
		insert cycle;

		List<BI_PL_Position__c> positions = new List<BI_PL_Position__c>();
		List<BI_PL_Position_cycle_user__c> positionCycleUsers = new List<BI_PL_Position_cycle_user__c>();
		List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();

		//Insert Positions

		BI_PL_Position__c positionRoot = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = 'BR');
		BI_PL_Position__c positionChild1 = new BI_PL_Position__c(Name = 'Test2', BI_PL_Country_code__c = 'BR');
		BI_PL_Position__c positionChild2 = new BI_PL_Position__c(Name = 'Test3', BI_PL_Country_code__c = 'BR');
		BI_PL_Position__c positionChild21 = new BI_PL_Position__c(Name = 'Test4', BI_PL_Country_code__c = 'BR');
		BI_PL_Position__c positionChild22 = new BI_PL_Position__c(Name = 'Test5', BI_PL_Country_code__c = 'BR');
		
		positions.add(positionRoot);
		positions.add(positionChild1);
		positions.add(positionChild2);
		positions.add(positionChild21);
		positions.add(positionChild22);
		
		insert positions;

		List<BI_PL_Position_cycle__c> positionCycle = new List<BI_PL_Position_cycle__c>();
		//Insert Positions Cycle

		BI_PL_Position_cycle__c root = new BI_PL_Position_cycle__c(/*Name = 'root', */BI_PL_Position__c = positionRoot.Id, BI_PL_Parent_position__c = null, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = 'hierarchySplitTest');

		root.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, root.BI_PL_Hierarchy__c, positionRoot.Name);

		positionCycle.add(root);

		BI_PL_Position_cycle__c child1 = new BI_PL_Position_cycle__c(/*Name = 'child1', */BI_PL_Position__c = positionChild1.Id, BI_PL_Parent_position__c = positionRoot.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = 'hierarchySplitTest');

		child1.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child1.BI_PL_Hierarchy__c, positionChild1.Name);

		positionCycle.add(child1);

		BI_PL_Position_cycle__c child2 = new BI_PL_Position_cycle__c(/*Name = 'child2', */BI_PL_Position__c = positionChild2.Id, BI_PL_Parent_position__c = positionRoot.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = 'hierarchySplitTest');

		child2.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child2.BI_PL_Hierarchy__c, positionChild2.Name);

		positionCycle.add(child2);

		BI_PL_Position_cycle__c child21 = new BI_PL_Position_cycle__c(/*Name = 'child21', */BI_PL_Position__c = positionChild21.Id, BI_PL_Parent_position__c = positionChild2.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = 'hierarchySplitTest');

		child21.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child21.BI_PL_Hierarchy__c, positionChild21.Name);

		positionCycle.add(child21);


		BI_PL_Position_cycle__c child22 = new BI_PL_Position_cycle__c(/*Name = 'child21', */BI_PL_Position__c = positionChild22.Id, BI_PL_Parent_position__c = positionChild2.Id, BI_PL_Cycle__c = cycle.Id, BI_PL_Hierarchy__c = 'hierarchySplitTest');

		child22.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, child22.BI_PL_Hierarchy__c, positionChild22.Name);
		child22.BI_PL_Synchronized__c = true;

		positionCycle.add(child22);

		insert positionCycle;

		

		//ADD Position Cycle Users
		addPositionCycleUser(cycle, root, 'RootUser', positionRoot.Name);
		addPositionCycleUser(cycle, child1, 'PositionChild1User', positionChild1.Name);
		addPositionCycleUser(cycle, child2, 'PositionChild2User', positionChild2.Name);
		addPositionCycleUser(cycle, child21, 'PositionChild21User', positionChild21.Name);
		addPositionCycleUser(cycle, child21, 'PositionChild21InactiveUser', positionChild21.Name);

		insert getPositionCycleUsers();

		//Insert Preparations

		
		BI_PL_Preparation__c preparation3 = new BI_PL_Preparation__c(BI_PL_Country_code__c = 'BR', BI_PL_Position_cycle__c = child21.Id);
		BI_PL_Preparation__c preparation4 = new BI_PL_Preparation__c(BI_PL_Country_code__c = 'BR', BI_PL_Position_cycle__c = child21.Id);
		BI_PL_Preparation__c preparation5 = new BI_PL_Preparation__c(BI_PL_Country_code__c = 'BR', BI_PL_Position_cycle__c = child22.Id);
		

		
		preparation3.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('BR', cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, 'hierarchySplitTest', positionChild21.Name,'fielfforcename');
		preparation4.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('BR', cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, 'hierarchySplitTest', positionChild1.Name,'fielfforcename');
		preparation5.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId('BR', cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, 'hierarchySplitTest', positionChild22.Name,'fielfforcename');
		

		
		preparations.add(preparation3);
		preparations.add(preparation4);
		preparations.add(preparation5);
		

     	insert preparations;

     	List<BI_PL_Target_preparation__c> targets 			= new List<BI_PL_Target_preparation__c>();
		List<BI_PL_Channel_detail_preparation__c> channels 	= new List<BI_PL_Channel_detail_preparation__c>();
		List<BI_PL_Detail_preparation__c> details 			= new List<BI_PL_Detail_preparation__c>();

     	for(BI_PL_Preparation__c prep : preparations){
     		for(Account acc : listaccounts){

     			BI_PL_Target_preparation__c tgtPrep = new BI_PL_Target_preparation__c(
	    			BI_PL_Target_customer__r = new Account(External_ID_vod__c = acc.External_Id_vod__c),
				  	BI_PL_Header__r = new BI_PL_Preparation__c(BI_PL_External_id__c = prep.BI_PL_External_ID__c),
				  	BI_PL_External_Id__c = prep.BI_PL_External_Id__c + '_' + acc.External_Id_vod__c 
				);

				
				BI_PL_Channel_detail_preparation__c chanDetail = new BI_PL_Channel_detail_preparation__c(
					BI_PL_Channel__c = 'rep_detail_only',
		    		BI_PL_Target__r = new BI_PL_Target_preparation__c(BI_PL_External_Id__c = tgtPrep.BI_PL_External_Id__c),
		    		BI_PL_Reviewed__c = (Math.random() > 0.5) ? true : false,
		    		BI_PL_Edited__c = (Math.random() > 0.5) ? true : false,
		    		BI_PL_Removed__c = (Math.random() > 0.5) ? true : false,
		    		BI_PL_Rejected__c = (Math.random() > 0.5) ? true : false,
		    		BI_PL_External_Id__c = tgtPrep.BI_PL_External_Id__c + '_rep_detail_only' 
				);

				for(Product_vod__c prod : listproducts) {

					BI_PL_Detail_preparation__c detail = new BI_PL_Detail_preparation__c(
						BI_PL_Product__r = new Product_vod__c(External_ID_vod__c = prod.External_ID_vod__c),
						BI_PL_Adjusted_details__c = 1,
					    BI_PL_External_id__c = chanDetail.BI_PL_External_Id__c + '_' + prod.External_ID_vod__c,
					    BI_PL_Planned_details__c = 5,
					    BI_PL_Segment__c = 'No Segmentation',
					    BI_PL_Added_Manually__c = (Math.random() > 0.5) ? true : false,
					    BI_PL_Channel_detail__r = new BI_PL_Channel_detail_preparation__c(BI_PL_External_Id__c = chanDetail.BI_PL_External_Id__c)
					    
					);
					details.add(detail);
					

				}
				targets.add(tgtPrep);
				channels.add(chanDetail);
     		}

     	}

     	upsert targets BI_PL_External_id__c;
		upsert channels BI_PL_External_id__c;
		upsert details BI_PL_External_id__c;

		return positionCycle;

	}


	private static BI_PL_Position_cycle_user__c addPositionCycleUser(BI_PL_Cycle__c cycle, BI_PL_Position_cycle__c positionCycle, String userId, String positionName) {
		if (!positionCycleUsersByPosition.containsKey(positionCycle.Id))
			positionCycleUsersByPosition.put(positionCycle.Id, new List<BI_PL_Position_cycle_user__c>());

		BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_User__c = BI_PL_SplitCycleCtrlTest.getUserId(userId, users));

		pcu.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycle, positionCycle.BI_PL_Hierarchy__c, positionName, BI_PL_SplitCycleCtrlTest.getUser(userId, users).External_id__c);

		positionCycleUsersByPosition.get(positionCycle.Id).add(pcu);
		return pcu;
	}

	private static List<BI_PL_Position_cycle_user__c> getPositionCycleUsers() {
		List<BI_PL_Position_cycle_user__c> output = new List<BI_PL_Position_cycle_user__c>();
		for (List<BI_PL_Position_cycle_user__c> l : positionCycleUsersByPosition.values())
			output.addAll(l);
		return output;
	}

	private static Map<String, User> usersCreation(String countryCode) {
		Map<Id, User> usersById = new Map<Id, User>();
		Map<String, User> output = new Map<String, User>();

		String profName = countryCode + '_%';
		Profile p = [SELECT Id FROM Profile WHERE Name LIKE :profName LIMIT 1];

		List<User> usersList = new List<User>();
		usersList.add(createUser('RootUser', p.Id, countryCode));
		usersList.add(createUser('PositionChild1User', p.Id, countryCode));
		usersList.add(createUser('PositionChild2User', p.Id, countryCode));
		usersList.add(createUser('PositionChild21User', p.Id, countryCode));
		usersList.add(createUser('PositionChild3User', p.Id, countryCode));


		PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name = 'BI_PL_SALES'];
		insert usersList;

		for (User u : usersList) {
			output.put(u.LastName, u);
			usersById.put(u.Id, u);

			System.runAs(new User(Id = Userinfo.getUserId(), UserName = 'username_' + Math.random() + '@u.com', Country_Code_BI__c = countryCode)) {
				insert createPermissionSetAssignment(u.Id, permissionSet.Id);
			}
		}

		User inactiveUser = createUser('PositionChild21InactiveUser', p.Id, countryCode);
		output.put(inactiveUser.LastName, inactiveUser);


		insert inactiveUser;

		System.runAs(new User(Id = Userinfo.getUserId(), UserName = 'username_' + Math.random() + '@u.com', Country_Code_BI__c = countryCode)) {
			insert createPermissionSetAssignment(inactiveUser.Id, permissionSet.Id);
			inactiveUser.IsActive = false;
			update inactiveUser;
		}


		usersById.put(inactiveUser.Id, inactiveUser);

		return output;
	}

	private static String getUserId(String lastName, Map<String, User> u) {
		return String.valueOf(getUser(lastName, users).Id).substring(0, 15);
	}

	private static User createUser(String name, Id profileId, String countryCode) {
		return new User(Alias = name.substring(0, 5), Email = name + '@testorg.com', Country_Code_BI__c = countryCode,
		                EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US',
		                LocaleSidKey = 'en_US', ProfileId = profileId, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = name+'_Test_External_ID');
	}

	private static PermissionSetAssignment createPermissionSetAssignment(Id userId, Id permissionSet) {
		return new PermissionSetAssignment(PermissionSetId = permissionSet, AssigneeId = userId);
	}
	private static User getUser(String lastName, Map<String, User> u) {
		return users.get(lastName);
	}

}