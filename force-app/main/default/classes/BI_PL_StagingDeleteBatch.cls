global class BI_PL_StagingDeleteBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        String userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
        return Database.getQueryLocator('SELECT Id FROM BI_PL_Staging__c WHERE BI_PL_Country_code__c = :userCountryCode');
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        System.debug('### - BI_PL_PreparationActivateBatch - execute - scope = ' + scope);

        deleteStaging(scope);
    }
    
    global void finish(Database.BatchableContext BC){
                            
    }
    
    /**
     * Function that deletes all staging records
     *
     * @param      stgRecords  The stg records
     */
    private void deleteStaging(list<BI_PL_Staging__c> stgRecords){
        system.debug('### - BI_PL_StagingDeleteBatch - deleteStaging - INI');

        delete stgRecords;
    }
}