@isTest
private class BI_PL_StagingDeleteBatch_Test {
	
	@isTest static void test_method_one() {
		String hierarchy = 'hierarchyTest';
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting('US');

		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		String userCountryCode = 'BR';

		BI_PL_TestDataFactory.createStaging(userCountryCode, 5);

		System.runAs(currentUser){
			Test.startTest();
			BI_PL_StagingDeleteBatch sdb = new BI_PL_StagingDeleteBatch();
	    	Database.executeBatch(sdb);
	    	Test.stopTest();
		}

	}
	
}