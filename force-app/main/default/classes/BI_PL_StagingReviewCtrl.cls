global without sharing class BI_PL_StagingReviewCtrl{
    public BI_PL_StagingReviewCtrl(ApexPages.StandardController controller) {}

    /**
     * Method that manages the retrievement of Staging records information
     *
     * @return     The list of StagingWrapper to be shown
     */
    @RemoteAction
    public static StagingWrapper getInitDataAndSetup(String lastIdOffset){
        StagingWrapper initDataSetup = new StagingWrapper();
        //Only records for current user country-code
        String userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;

        //Offset of Ids to paginate records
        String idOffset = lastIdOffset != null ? lastIdOffset : '';

        List<BI_PL_Staging__c> stagingPreps = new List<BI_PL_Staging__c>([SELECT Id, BI_PL_Planned_details__c, 
                                                                                    BI_PL_Channel__c, 
                                                                                    BI_PL_Product__c,
                                                                                    BI_PL_Secondary_Product__c,
                                                                                    BI_PL_Target_customer__c,
                                                                                    BI_PL_Segment__c,
                                                                                    BI_PL_Position_cycle__r.BI_PL_Position__c,
                                                                                    BI_PL_Target_customer__r.External_ID_vod__c,
                                                                                    BI_PL_Target_preparation_external_id__c,
                                                                                    CreatedDate,
                                                                                    CreatedBy.Name,

                                                                                    BI_PL_Product__r.Name,
                                                                                    BI_PL_Product__r.External_ID_vod__c,
                                                                                    BI_PL_Secondary_Product__r.Name,
                                                                                    BI_PL_Secondary_Product__r.External_ID_vod__c,

                                                                                    BI_PL_Position_cycle__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Cycle_name__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Cycle_start_date__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Cycle_end_date__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                                                                                    BI_PL_Position_cycle__r.BI_PL_Position_name__c

                                                                            FROM BI_PL_Staging__c
                                                                            WHERE Id > :idOffset
                                                                            AND BI_PL_Country_Code__c = :userCountryCode
                                                                            ORDER BY Id ASC
                                                                            LIMIT 10000]);

        if(stagingPreps.size() >= 1000){
            System.debug(loggingLevel.Error, '*** stagingPreps.size(): ' + stagingPreps.size());
            initDataSetup.lastIdOffset = stagingPreps.get(stagingPreps.size()-1).Id;
        }


        System.debug(loggingLevel.Error, '*** initDataSetup.lastIdOffset: ' + initDataSetup.lastIdOffset);
        System.debug('*** stagingPreps ' + stagingPreps);

        initDataSetup.countryCode = userCountryCode;
        initDataSetup.stagingRecords = stagingPreps;
        initDataSetup.summaryInfoLst    = getSummaryInfo(stagingPreps);
        //initDataSetup.errorsList        = getErrors(stagingPreps);
        initDataSetup.errorSummary      = getErrors(stagingPreps);
        //initDataSetup.warningSummary    = getWarnings(stagingPreps, userCountryCode);

        return initDataSetup;
    }

    /**
     * Sets the summary information.
     *
     * @param      stagingPreps  The staging preps
     *
     * @return     { description_of_the_return_value }
     */
    public static List<SummaryWrapper> getSummaryInfo(List<BI_PL_Staging__c> stagingPreps){
        Map<String, SummaryWrapper> summaryInfoMap = new Map<String, SummaryWrapper>();
        //Map<String, Set<String>> targetsPerPrep = new Map<String, Set<String>>(); 

        Set<String> uniqueTargets = new Set<String>();

        for(BI_PL_Staging__c stgRecord : stagingPreps){
            SummaryWrapper tmpWrapper = new SummaryWrapper();
            if(summaryInfoMap.containsKey(stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_name__c + 
                                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c + 
                                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c + 
                                                stgRecord.BI_PL_Channel__c)){

                tmpWrapper = summaryInfoMap.get(stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_name__c + 
                                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c + 
                                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c + 
                                                stgRecord.BI_PL_Channel__c);

                if(stgRecord.BI_PL_Planned_details__c != null){
                    tmpWrapper.sumP += stgRecord.BI_PL_Planned_details__c;

                    if(stgRecord.BI_PL_Planned_details__c > tmpWrapper.maxP)
                        tmpWrapper.maxP = stgRecord.BI_PL_Planned_details__c;
                    else
                        tmpWrapper.maxP = 0;

                    tmpWrapper.numberOfDetails++;
                }

                tmpWrapper.targets.add(stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_name__c + 
                                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c + 
                                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c +stgRecord.BI_PL_Target_customer__c);

                if(stgRecord.BI_PL_Product__c != null){

                    String productsId = stgRecord.BI_PL_Product__r.External_ID_vod__c+ (stgRecord.BI_PL_Secondary_Product__c != null ? stgRecord.BI_PL_Secondary_Product__r.External_ID_vod__c : '');

                  

                    if(!tmpWrapper.productsId.contains(productsId)){

                        tmpWrapper.products.add(new ProductWrapper(stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_name__c, stgRecord.BI_PL_Product__r.Name, stgRecord.BI_PL_Product__r.External_ID_vod__c, stgRecord.BI_PL_Secondary_Product__r.Name, stgRecord.BI_PL_Secondary_Product__r.External_ID_vod__c, productsId));

                    }
                    tmpWrapper.productsId.add(productsId);

                }



            }else{
                tmpWrapper.cycle = stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_name__c;
                tmpWrapper.hierarchy = stgRecord.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
                tmpWrapper.territory = stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
                //tmpWrapper.target = stgRecord.BI_PL_Target_customer__c;
                tmpWrapper.channel = stgRecord.BI_PL_Channel__c;
                
                if(stgRecord.BI_PL_Planned_details__c != null){
                    tmpWrapper.maxP = stgRecord.BI_PL_Planned_details__c;
                    tmpWrapper.sumP = stgRecord.BI_PL_Planned_details__c;
                    tmpWrapper.numberOfDetails++;
                }

                //Set<String> targetSet = new Set<String>();
                tmpWrapper.targets.add(stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_name__c + 
                                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c + 
                                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c +stgRecord.BI_PL_Target_customer__c);

                
                if(stgRecord.BI_PL_Product__c != null){
                    String productsId = stgRecord.BI_PL_Product__r.External_ID_vod__c+ (stgRecord.BI_PL_Secondary_Product__c != null ? stgRecord.BI_PL_Secondary_Product__r.External_ID_vod__c : '');
                    tmpWrapper.products.add(new ProductWrapper(stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_name__c, stgRecord.BI_PL_Product__r.Name, stgRecord.BI_PL_Product__r.External_ID_vod__c, stgRecord.BI_PL_Secondary_Product__r.Name, stgRecord.BI_PL_Secondary_Product__r.External_ID_vod__c, productsId));
                   
                    tmpWrapper.productsId.add(productsId);
                }
                
            
                //uniqueTargets.add(stgRecord.BI_PL_Target_customer__c);

            }


            summaryInfoMap.put( stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_name__c + 
                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c + 
                                stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c + 
                                stgRecord.BI_PL_Channel__c, tmpWrapper);

        }
        /*for(String prep : targetsPerPrep.keySet()){
            summaryInfoMap.get(prep).numberOfTargets = targetsPerPrep.get(prep).size();
        }*/
        return summaryInfoMap.values();
    }

    /**
     * Gets the errors list.
     *
     * @param      stagingPreps  The staging preps
     *
     * @return     The errors list.
     */
    public static ErrorSummary getErrors(List<BI_PL_Staging__c> stagingPreps){
        ErrorSummary summary = new ErrorSummary();

        List<ErrorWrapper> errorsList = new List<ErrorWrapper>();
        Map<String, ErrorWrapper> errorsMap = new Map<String, ErrorWrapper>();
        Map<String, List<BI_PL_Staging__c>> posCycles = new Map<String, List<BI_PL_Staging__c>>();
        Map<String, List<BI_PL_Staging__c>> accountsSearchATL = new Map<String, List<BI_PL_Staging__c>>();

        //Generate maps for do searches
        for(BI_PL_Staging__c stgRecord : stagingPreps){

            //Account map
            if(accountsSearchATL.containsKey(stgRecord.BI_PL_Target_customer__c)) {
                accountsSearchATL.get(stgRecord.BI_PL_Target_customer__c).add(stgRecord);
            } else {
                accountsSearchATL.put(stgRecord.BI_PL_Target_customer__c, new List<BI_PL_Staging__c>{stgRecord});
            }

            //Territory map
            if(posCycles.containsKey(stgRecord.BI_PL_Position_cycle__c)) {
                posCycles.get(stgRecord.BI_PL_Position_cycle__c).add(stgRecord);
            } else {
                posCycles.put(stgRecord.BI_PL_Position_cycle__c, new List<BI_PL_Staging__c>{stgRecord});
            }

        }
/*
        System.debug(loggingLevel.Error, '*** accountsSearchATL: ' + accountsSearchATL);
        System.debug(loggingLevel.Error, '*** posCycles: ' + posCycles);*/

        //Check if account is in ATL for the territory informed
        List<Account_Territory_Loader_vod__c> atls = new List<Account_Territory_Loader_vod__c>([SELECT Id, Territory_vod__c, Account_vod__r.External_ID_vod__c
                                                                FROM Account_Territory_Loader_vod__c
                                                                WHERE Account_vod__c IN :accountsSearchATL.keySet()]);

        System.debug(loggingLevel.Error, '*** atls: ' + atls);

        //all accounts are wrong
        if(atls.isEmpty()) {

            for(BI_PL_Staging__c stgRecord : stagingPreps){
                ErrorWrapper newError = new ErrorWrapper();
                //newError.hierarchy = stgRecord.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
                newError.errorTypeNumber = 1;
                newError.errorType = Label.BI_PL_Staging_atl_error;
                newError.fieldValue = stgRecord.BI_PL_Target_customer__r.External_ID_vod__c;
                //newError.territory = stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
                //newError.startDate = stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_start_date__c.format();
                //newError.endDate = stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_end_date__c.format();
                //newError.createdBy = stgRecord.CreatedBy.Name;
                //newError.createdDate = stgRecord.CreatedDate.format();
                //errorsList.add(newError);
                errorsMap.put(newError.errorTypeNumber+newError.fieldValue, newError);

                Integer count = 1;
                if(summary.errorCount.containsKey(newError.errorType)) {
                    count = summary.errorCount.get(newError.errorType) + 1;
                }
                summary.errorCount.put(newError.errorType, count);

            }

        }

        for(Account_Territory_Loader_vod__c atl : atls) {

            for(BI_PL_Staging__c stgRecord : accountsSearchATL.get(atl.Account_vod__c)) {
                if(atl.Territory_vod__c != null){

                

                if(!atl.Territory_vod__c.contains(';'+stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c+';')) {
                    ErrorWrapper newError = new ErrorWrapper();
                    //newError.hierarchy = stgRecord.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
                    newError.errorTypeNumber = 1;
                    newError.errorType = Label.BI_PL_Staging_atl_error;
                    newError.fieldValue = stgRecord.BI_PL_Target_customer__r.External_ID_vod__c;
                    //newError.territory = stgRecord.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
                    //newError.startDate = stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_start_date__c.format();
                    //newError.endDate = stgRecord.BI_PL_Position_cycle__r.BI_PL_Cycle_end_date__c.format();
                    //newError.createdBy = stgRecord.CreatedBy.Name;
                    //newError.createdDate = stgRecord.CreatedDate.format();
                    //errorsList.add(newError);
                    errorsMap.put(newError.errorTypeNumber+newError.fieldValue, newError);
                    Integer count = 1;
                    if(summary.errorCount.containsKey(newError.errorType)) {
                        count = summary.errorCount.get(newError.errorType) + 1;
                    }
                    summary.errorCount.put(newError.errorType, count);

                }
            }
            }

        }

        //Check if positions cycles has users
        List<BI_PL_Position_cycle_user__c> posCycleRecords = new list<BI_PL_Position_cycle_user__c>([SELECT Id, BI_PL_Position_cycle__c 
                                                            FROM BI_PL_Position_cycle_user__c 
                                                            WHERE BI_PL_Position_cycle__c IN :posCycles.keySet() 
                                                                AND BI_PL_User__r.isActive = true]);

        //System.debug(loggingLevel.Error, '*** posCycleRecords: ' + posCycleRecords);

        for(BI_PL_Position_cycle_user__c pc : posCycleRecords) {
            posCycles.remove(pc.BI_PL_Position_cycle__c);
        }

        //System.debug(loggingLevel.Error, '*** posCycles: ' + posCycles);

        //the ones that are already in the map, are the ones that have not entries for usersç
        for(String posKey : posCycles.keySet()) {
            for(BI_PL_Staging__c rec : posCycles.get(posKey)) {
                ErrorWrapper newError = new ErrorWrapper();
                //newError.hierarchy = rec.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
                newError.errorTypeNumber = 2;
                newError.errorType = Label.BI_PL_Staging_territory_user_error;
                newError.fieldValue = rec.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
                //newError.territory = rec.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
                //newError.startDate = rec.BI_PL_Position_cycle__r.BI_PL_Cycle_start_date__c.format();
                //newError.endDate = rec.BI_PL_Position_cycle__r.BI_PL_Cycle_end_date__c.format();
                //newError.createdBy = rec.CreatedBy.Name;
                //newError.createdDate = rec.CreatedDate.format();
                //errorsList.add(newError);
                errorsMap.put(newError.errorTypeNumber+newError.fieldValue, newError);
                Integer count = 1;
                if(summary.errorCount.containsKey(newError.errorType)) {
                    count = summary.errorCount.get(newError.errorType) + 1;
                }
                summary.errorCount.put(newError.errorType, count);

            }
        }

        //System.debug(loggingLevel.Error, '*** errorsMap: ' + errorsMap);
        //return errorsList
        summary.errors = errorsMap.values();
        return summary;
    }   


    /**
     * Gets the warnings list.
     *
     * @param      stagingPreps  The staging preps
     *
     * @return     The warnings list.
     */
    @RemoteAction
    public static WarningSummary getWarnings(String lastIdOffset, String userCountryCode){

        String idOffset = lastIdOffset != null ? lastIdOffset : '';

        List<BI_PL_Staging__c> stagingPreps = new List<BI_PL_Staging__c>([SELECT Id, BI_PL_Planned_details__c, 
                                                                                    BI_PL_Channel__c, 
                                                                                    BI_PL_Product__c,
                                                                                    BI_PL_Secondary_Product__c,
                                                                                    BI_PL_Target_customer__c,
                                                                                    BI_PL_Segment__c,
                                                                                    BI_PL_Position_cycle__r.BI_PL_Position__c,
                                                                                    BI_PL_Target_customer__r.External_ID_vod__c,
                                                                                    BI_PL_Target_preparation_external_id__c,

                                                                                    CreatedDate,
                                                                                    CreatedBy.Name,

                                                                                    BI_PL_Product__r.Name,
                                                                                    BI_PL_Product__r.External_ID_vod__c,
                                                                                    BI_PL_Secondary_Product__r.Name,
                                                                                    BI_PL_Secondary_Product__r.External_ID_vod__c,

                                                                                    BI_PL_Position_cycle__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Cycle_name__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Cycle_start_date__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Cycle_end_date__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                                                                                    BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                                                                                    BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c

                                                                            FROM BI_PL_Staging__c
                                                                            WHERE Id > :idOffset
                                                                            AND BI_PL_Country_Code__c = :userCountryCode
                                                                            ORDER BY Id ASC
                                                                            LIMIT 5000]);

        System.debug('getWarnings preps' + stagingPreps + 'cc' + userCountryCode);
        WarningSummary summary = new WarningSummary();

        if(stagingPreps.size() >= 5000){
            System.debug(loggingLevel.Error, '*** stagingPreps.size(): ' + stagingPreps.size());
            summary.lastIdOffset = stagingPreps.get(stagingPreps.size()-1).Id;
        }


     
        //Get all active business rules for current country
        List<WarningWrapper> warningList = new List<WarningWrapper>();

        Map<String,BI_PL_Business_rule__c> businessRules = getActiveBusinessRules(userCountryCode);

        System.debug('*/*businessRules: ' +businessRules);

        for(BI_PL_Staging__c stgRecord : stagingPreps){
            //Get current business rule to be applied to current record
            String key = stgRecord.BI_PL_Segment__c+stgRecord.BI_PL_Channel__c+stgRecord.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c+stgRecord.BI_PL_Product__c;
            if(stgRecord.BI_PL_Secondary_Product__c != null) key = key + stgRecord.BI_PL_Secondary_Product__c;

            //BI_PL_Business_rule__c br = getCurrentBR(stgRecord, businessRules);
            BI_PL_Business_rule__c br = businessRules.get(key);
            System.debug('*** br: ' + br);
            System.debug('*** stg: ' + stgRecord);
            System.debug('*** key: ' + key);
            if(br!= null)
            {
                if(br.BI_PL_Min_value__c != null && stgRecord.BI_PL_Planned_details__c < br.BI_PL_Min_value__c)
                {
                    System.debug('first if');
                    WarningWrapper warning = new WarningWrapper();
                    warning.warningField = stgRecord.BI_PL_Target_preparation_external_id__c;
                    warning.warningMsg = 'Planned value must be equal or bigger than '+ br.BI_PL_Min_value__c;
                    System.debug(loggingLevel.Error, '*** warning: ' + warning);
                    warningList.add(warning);
                }
                else if(br.BI_PL_Max_value__c != null && stgRecord.BI_PL_Planned_details__c > br.BI_PL_Max_value__c)
                {
                    System.debug('second if');
                    WarningWrapper warning = new WarningWrapper();
                    warning.warningField = stgRecord.BI_PL_Target_preparation_external_id__c;
                    warning.warningMsg = 'Planned value must be equal or lower than '+ br.BI_PL_Max_value__c;
                    System.debug(loggingLevel.Error, '*** warning: ' + warning);
                    warningList.add(warning);

                }

            }
        }

        System.debug(loggingLevel.Error, '*** warningsMap: ' + warningList);

        summary.warnings = warningList;

       

        return summary;


    }


    /**
     * Gets the warnings list.
     *
     * @param      stagingPreps  The staging preps
     *
     * @return     The warnings list.
     */
    @RemoteAction
    public static InsertErrorsSummary getInsertErrors(String lastIdOffset, String userCountryCode){

        String idOffset = lastIdOffset != null ? lastIdOffset : '';

        List<BI_PL_Staging__c> stagingPreps = new List<BI_PL_Staging__c>([SELECT Id, BI_PL_Planned_details__c, 
                                                                                    BI_PL_Channel__c, 
                                                                                    BI_PL_Product__c,
                                                                                    BI_PL_Secondary_Product__c,
                                                                                    BI_PL_Target_customer__c,
                                                                                    BI_PL_Segment__c,
                                                                                    BI_PL_Position_cycle__r.BI_PL_Position__c,
                                                                                    BI_PL_Target_customer__r.External_ID_vod__c,
                                                                                    BI_PL_Target_preparation_external_id__c,
                                                                                    BI_PL_Target_customer__r.Name,

                                                                                    CreatedDate,
                                                                                    CreatedBy.Name,

                                                                                    BI_PL_Product__r.Name,
                                                                                    BI_PL_Product__r.External_ID_vod__c,
                                                                                    BI_PL_Secondary_Product__r.Name,
                                                                                    BI_PL_Secondary_Product__r.External_ID_vod__c,

                                                                                    BI_PL_Position_cycle__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Cycle_name__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Cycle_start_date__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Cycle_end_date__c, 
                                                                                    BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
                                                                                    BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                                                                                    BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c,
                                                                                    BI_PL_Error_log__c,
                                                                                    BI_PL_Error__c

                                                                            FROM BI_PL_Staging__c
                                                                            WHERE Id > :idOffset
                                                                            AND BI_PL_Country_Code__c = :userCountryCode
                                                                            AND BI_PL_Error__c = true
                                                                            ORDER BY Id ASC
                                                                            LIMIT 5000]);

        System.debug('getInsertErrors preps' + stagingPreps + 'cc' + userCountryCode + ' size: ' + stagingPreps.size());
        InsertErrorsSummary summary = new InsertErrorsSummary();

        if(stagingPreps.size() >= 5000){
            System.debug(loggingLevel.Error, '*** stagingPreps.size(): ' + stagingPreps.size());
            summary.lastIdOffset = stagingPreps.get(stagingPreps.size()-1).Id;
        }       

        if(stagingPreps.size() > 0){

            summary.stagingErrors = stagingPreps;
        }


        System.debug('***SUMMARY INSERT ERRORS: ' + summary);

        return summary;


    }




  /* private static BI_PL_Business_rule__c getCurrentBR(BI_PL_Staging__c stgRecord, List<BI_PL_Business_rule__c> brList)
   {
        
        for(BI_PL_Business_rule__c br : brList)
        {

            if((stgRecord.BI_PL_Channel__c == br.BI_PL_Channel__c ) && (stgRecord.BI_PL_Product__c == br.BI_PL_Product__c)  && stgRecord.BI_PL_Secondary_Product__c == br.BI_PL_Secondary_product__c && stgRecord.BI_PL_Segment__c == br.BI_PL_Criteria__c)
            {
                if(stgRecord.BI_PL_Secondary_Product__c != null)
                {
                    if(stgRecord.BI_PL_Position_cycle__r.BI_PL_Position__c == br.BI_PL_Position__c)
                    {
                         return br;
                    }
                }
                else
                {
                    return br;
                }
   
                   
            }
        }
        return null;

   }*/


    /**
     * Gets all active business rules for a given country, whose type is segmentation
     *
     * @param      countryCode  The country code
     *
     * @return     The active business rules.
     */
    private static Map<String, BI_PL_Business_rule__c> getActiveBusinessRules(String countryCode) {
        System.debug('getActiveBusinessRules ' + countryCode);

        Map<String, BI_PL_Business_rule__c> mapBR = new Map<String, BI_PL_Business_rule__c>();


        List<BI_PL_Business_rule__c> listBR =  BI_PL_BusinessRuleUtility.getBusinessRulesForCountry(countryCode, new List<String>{ 'Segmentation' }, null, null, null);
        System.debug('LIST BR: ' + listBR);
        for(BI_PL_Business_rule__c br : listBR){
            String key = br.BI_PL_Criteria__c+br.BI_PL_Channel__c+br.BI_PL_Field_Force__c+br.BI_PL_Product__c;
            if(br.BI_PL_Secondary_Product__c != null) key = key +br.BI_PL_Secondary_Product__c;
            mapBR.put(key,br);
        }

        return mapBR;



        
        //return [
        //           SELECT Id, BI_PL_Max_value__c,
        //           BI_PL_Field_Force__c,
        //           BI_PL_Min_value__c,
        //           BI_PL_Type__c,
        //           BI_PL_Product__c,
        //           BI_PL_Product__r.Name,
        //           BI_PL_Secondary_product__c,
        //           BI_PL_Secondary_product__r.Name,
        //           BI_PL_Specialty__c,
        //           BI_PL_Criteria__c,
        //           BI_PL_Channel__c,
        //           BI_PL_Country_code__c,
        //           BI_PL_Visits_per_day__c,
        //           BI_PL_Position__c,
        //           BI_PL_Percentage__c,

        //           BI_PL_Required_capacity__c

        //           FROM BI_PL_Business_rule__c
        //           WHERE BI_PL_Active__c = true
        //                                   AND BI_PL_Country_code__c = :countryCode AND BI_PL_Type__c='Segmentation'];
    }




    /**
     * Method that manages the retrievement of Staging records information
     *
     * @return     The list of StagingWrapper to be shown
     */
    @RemoteAction
    public static String activePreparations(){
        System.debug('### - BI_PL_StagingPreparationReviewCtrl - activePreparations - INI');

        String bid = Database.executeBatch(new BI_PL_PreparationActivateBatch());

        return bid;
    }

    /**
     * Method that manages the retrievement of Staging records information
     *
     * @return     The list of StagingWrapper to be shown
     */
    @RemoteAction
    public static String deletePreparations(){
        System.debug('### - BI_PL_StagingPreparationReviewCtrl - deletePreparations - INI');

        String bid = Database.executeBatch(new BI_PL_StagingDeleteBatch());

        return bid;
    }




    /**
     * Class for initialize data and setup wrapper.
     */
    public class StagingWrapper{
        public List<SummaryWrapper> summaryInfoLst;
        public List<BI_PL_Staging__c> stagingRecords;
        //public List<ValueLabelWrapper> cycleList;
        //public List<ValueLabelWrapper> hierarchyList;
        //public List<ValueLabelWrapper> channelList;
        public List<ErrorWrapper> errorsList;
        public List<BI_PL_Staging__c> records;
        public String lastIdOffset;
        public ErrorSummary errorSummary;
        public WarningSummary warningSummary;
        public String countryCode;


    }

    /**
     * Class for summary wrapper.
     */
    public class SummaryWrapper{
        public String cycle;
        public String hierarchy;
        public String territory;
        public String target; //Really needed??
        public String channel;
        public Set<String> targets;
        public Integer numberOfTargets = 0;
        public Integer numberOfDetails = 0;
        public Decimal maxP = 0;
        public Decimal sumP = 0;
        public List<ProductWrapper> products;
        public Set<String> productsId;
        
        public SummaryWrapper(){
            targets = new Set<String>();
            products = new List<ProductWrapper>();
            productsId = new Set<String>();
        }
    }

    public class ProductWrapper{
        public String productName;
        public String externalId;
        public String cycleName;
        public String secondaryName;
        public String secondaryId;
        public String key;

        public ProductWrapper(String cycle, String name, String exId, String secondaryName, String secId, String prodKey){
            this.productName = name;
            this.externalId = exId;
            this.cycleName = cycle;
            this.secondaryName = secondaryName;
            this.secondaryId = secId;
            this.key = prodKey;
        }
    }

    /**
     * Class for value label wrapper.
     */
    /*public class ValueLabelWrapper{
        public String value;
        public String label;

        public ValueLabelWrapper(String value, String label){
            this.value = value;
            this.label = label;
        }
    }*/

    /**
     * Class for error wrapper.
     */
    public class ErrorWrapper{
        public String errorType;
        public Integer errorTypeNumber;
        public String fieldValue;
        //public String startDate;
        //public String endDate;
        //public String hierarchy;
        //public String territory;
        //public String createdDate;
        //public String createdBy;
    }

    public class ErrorSummary {
        List<ErrorWrapper> errors;
        Map<String, Integer> errorCount;
        public ErrorSummary() {
            errors = new List<ErrorWrapper>();
            errorCount = new Map<String, Integer>();
        }
    }

    /**
     * Class for warning wrapper.
     */
    public class WarningWrapper{
        public String warningField;
        public String warningMsg;
        //public String startDate;
        //public String endDate;
        //public String hierarchy;
        //public String territory;
        //public String createdDate;
        //public String createdBy;
    }

    public class WarningSummary {
        List<WarningWrapper> warnings;
        String lastIdOffset; 
       
        public WarningSummary() {
            warnings = new List<WarningWrapper>();
        }
    }


    public class InsertErrorsSummary {
        List<BI_PL_Staging__c> stagingErrors;
        String lastIdOffset;

        public InsertErrorsSummary(){
            stagingErrors = new List<BI_PL_Staging__c>();
        }
    }
}