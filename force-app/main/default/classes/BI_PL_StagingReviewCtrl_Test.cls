@isTest
private class BI_PL_StagingReviewCtrl_Test {
	
	@isTest static void test_method_one() {

		Date currentDate = Date.today();

		String countryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();

        BI_PL_Country_settings__c countrySettings = BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser();
        countrySettings.BI_PL_AMA_Products__c = 'TestProduct1_ExtIdStaging;TestProduct2_ExtIdStaging';
        update countrySettings;
        
		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(
        	BI_TM_Country_Code__c = countryCode
        );

        insert fieldForce;

        BI_PL_Cycle__c	cycle = new BI_PL_Cycle__c(
        	BI_PL_Country_code__c = countryCode,
        	BI_PL_Start_date__c = currentDate,
        	BI_PL_End_date__c = currentDate.addMonths(1),
        	BI_PL_External_id__c = 'BR_Cycle_Test_Ext_Id',
        	BI_PL_Field_force__c = fieldForce.Id
        );

        insert cycle;

        BI_PL_Position__c position = new BI_PL_Position__c(
        	Name = 'testPosition',
        	BI_PL_Country_code__c = countryCode,
        	BI_PL_Field_force__c = 'TestFieldForce'
        );

        insert position;

		BI_PL_Position_cycle__c positionCycle= new BI_PL_Position_cycle__c(
        	BI_PL_Cycle__c = cycle.Id,
        	BI_PL_External_id__c = cycle.BI_PL_External_id__c+'PosCycle',
        	BI_PL_Hierarchy__c = 'testHierarchy',
        	BI_PL_Position__c = position.Id
        );

        insert positionCycle;

   
        


        Account acc1 = new Account(
        	Name = 'TestAccount1',
       		External_Id_vod__c = 'TestAccount1_Ext_Id'
        );

         Account acc2 = new Account(
            Name = 'TestAccount2',
            External_Id_vod__c = 'TestAccount2_Ext_Id'
        );

        insert acc1;
        insert acc2;

        //Database.Upsert(acc1, Account.fields.External_Id_vod__c);

        Product_vod__c product1 = new Product_vod__c(
        	Name = 'TestProduct1',
        	RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(),
        	Product_Type_vod__c = 'Detail',
        	External_ID_vod__c = 'TestProduct1_ExtIdStaging'
        );

        insert product1;

        Product_vod__c product2 = new Product_vod__c(
            Name = 'TestProduct2',
            RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(),
            Product_Type_vod__c = 'Detail',
            External_ID_vod__c = 'TestProduct2_ExtIdStaging'
        );

        insert product2;


        BI_PL_Business_rule__c br = new BI_PL_Business_rule__c(
            BI_PL_Max_value__c =  5,
            BI_PL_Min_value__c = 1,
            BI_PL_Type__c = 'Segmentation',
            BI_PL_Product__c = product1.Id,
            BI_PL_Channel__c = 'rep_detail_only',
            BI_PL_Criteria__c = 'Gain',
            BI_PL_Country_code__c = countryCode,
            BI_PL_Active__c = true,
            BI_PL_Field_force__c = 'TestFieldForce'
        );
        insert br;

        BI_PL_TestDataFactory.insertBusinessRules(new List<Product_vod__c> {product1, product2},  countryCode, position);




		BI_PL_Staging__c staging = new BI_PL_Staging__c(BI_PL_Country_code__c = countryCode, BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_Channel__c = 'rep_detail_only', BI_PL_Target_Customer__c = acc1.Id, BI_PL_Product__c =product1.Id, BI_PL_Planned_details__c = 5);

		insert staging;

        BI_PL_Staging__c staging2 = new BI_PL_Staging__c(BI_PL_Country_code__c = countryCode, BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_Channel__c = 'rep_detail_only', BI_PL_Target_Customer__c = acc1.Id, BI_PL_Product__c =product2.Id, BI_PL_Planned_details__c = 5);

        insert staging2;

        BI_PL_Staging__c staging3 = new BI_PL_Staging__c(BI_PL_Country_code__c = countryCode, BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_Channel__c = 'rep_detail_only', BI_PL_Target_Customer__c = acc2.Id, BI_PL_Product__c =product2.Id, BI_PL_Planned_details__c = 5);

        insert staging3;

        BI_PL_Staging__c staging4 = new BI_PL_Staging__c(BI_PL_Country_code__c = countryCode, BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_Channel__c = 'rep_detail_only', BI_PL_Target_Customer__c = acc1.Id, BI_PL_Product__c =product1.Id,BI_PL_Secondary_product__c  = product2.Id, BI_PL_Planned_details__c = 8, BI_PL_Segment__c = 'Gain');

        insert staging4;


        BI_PL_Staging__c staging5 = new BI_PL_Staging__c(BI_PL_Country_code__c = countryCode, BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_Channel__c = 'rep_detail_only', BI_PL_Target_Customer__c = acc1.Id, BI_PL_Product__c =product1.Id,BI_PL_Secondary_product__c  = product2.Id, BI_PL_Planned_details__c = -1, BI_PL_Segment__c = 'Gain');

        insert staging5;

        BI_PL_Staging__c staging6 = new BI_PL_Staging__c(BI_PL_Country_code__c = countryCode, BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_Channel__c = 'rep_detail_only', BI_PL_Target_Customer__c = acc2.Id, BI_PL_Product__c =product1.Id,BI_PL_Secondary_product__c  = product2.Id, BI_PL_Planned_details__c = 4, BI_PL_Segment__c = 'Gain', BI_PL_Error__c = true, BI_PL_Error_log__c = 'Error Test');

        insert staging6;

        BI_PL_Staging__c staging7 = new BI_PL_Staging__c(BI_PL_Country_code__c = countryCode, BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_Channel__c = 'rep_detail_only', BI_PL_Target_Customer__c = acc1.Id, BI_PL_Product__c =product1.Id, BI_PL_Planned_details__c = 0, BI_PL_Segment__c = 'Gain');

        insert staging7;

        BI_PL_Staging__c staging8 = new BI_PL_Staging__c(BI_PL_Country_code__c = countryCode, BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_Channel__c = 'rep_detail_only', BI_PL_Target_Customer__c = acc1.Id, BI_PL_Product__c =product1.Id, BI_PL_Planned_details__c = 9, BI_PL_Segment__c = 'Gain');

        insert staging8;




        Account_Territory_Loader_vod__c territoryLoader = new Account_Territory_Loader_vod__c(Account_vod__c = acc1.Id, Territory_vod__c = ';SP-000003;', External_ID_vod__c = 'oTest_Ext_Id');

        insert territoryLoader;

        //BI_PL_Staging__c staging3 = new BI_PL_Staging__c();
        Test.startTest();
		BI_PL_StagingReviewCtrl.getInitDataAndSetup('');
		BI_PL_StagingReviewCtrl.activePreparations();
		BI_PL_StagingReviewCtrl.deletePreparations();

        BI_PL_StagingReviewCtrl.getSummaryInfo(new List<BI_PL_Staging__c>{staging, staging2});
        BI_PL_StagingReviewCtrl.getErrors(new List<BI_PL_Staging__c>{staging, staging2});
        BI_PL_StagingReviewCtrl.getErrors(new List<BI_PL_Staging__c>{staging3});
        BI_PL_StagingReviewCtrl.ErrorWrapper errorWrapper = new BI_PL_StagingReviewCtrl.ErrorWrapper();
        errorWrapper.errorType = '';
        errorWrapper.fieldValue = '';
        BI_PL_StagingReviewCtrl.getWarnings('', countryCode);
        BI_PL_StagingReviewCtrl.WarningWrapper warningWrapper = new BI_PL_StagingReviewCtrl.WarningWrapper();
        warningWrapper.warningField = '';
        warningWrapper.warningMsg = '';

        BI_PL_StagingReviewCtrl.getInsertErrors('', countryCode);

        Test.stopTest();


	}
	
}