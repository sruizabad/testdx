@isTest
private class BI_PL_SummaryProductsDetailsCtrl_Test {

    private static final String SAFFTYPESALES = BI_PL_TestDataFactory.SAFFTYPESALES;
    private static final String SAFFTYPEINFO = BI_PL_TestDataFactory.SAFFTYPEINFO;
    private static final String SCHANELNAME = BI_PL_TestDataFactory.SCHANNEL;
    private static String userCountryCode;
    private static String hierarchy = BI_PL_TestDataFactory.hierarchy;

    private static List<BI_PL_Position_Cycle__c> posCycles;
    private static BI_PL_Cycle__c cycle;
    private static List<Account> listAcc;
    private static List<Product_vod__c> listProd;
    private static List<BI_PL_Position__c> position;
    private static List<String> lstProductsName;

	private static void getData(){

		userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
		cycle = [SELECT Id, Name FROM BI_PL_Cycle__c LIMIT 1];
		posCycles = [SELECT Id, BI_PL_External_Id__c, BI_PL_Position__c FROM BI_PL_Position_Cycle__c];
		listProd = [SELECT Id, Name, External_ID_vod__c FROM Product_vod__c];
	}

	private static void getVeevaData(){
		
		listAcc = [SELECT Id, External_ID_vod__c FROM Account];
		position = [SELECT Id, Name, BI_PL_External_Id__c FROM BI_PL_Position__c];
		lstProductsName = new List<String>();
		
		BI_PL_TestDataFactory.createTestTerritoryField(userCountryCode, listAcc.get(0), position.get(0));

		for (Product_vod__c inProd : listProd){
			lstProductsName.add(inProd.Name);
		}

		system.debug('## lstProductsName: ' + lstProductsName);
	}

	@testSetup static void setup() {
        
        BI_PL_SummarySalesCtrl controller = new BI_PL_SummarySalesCtrl();
        userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);

        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        
        listAcc = [SELECT Id, External_ID_vod__c FROM Account];

        listProd = [SELECT Id, Name, External_ID_vod__c FROM Product_vod__c];

        cycle = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];
        posCycles = [SELECT Id, BI_PL_External_Id__c, BI_PL_Position__c FROM BI_PL_Position_Cycle__c];
        position = [SELECT Id, Name, BI_PL_External_Id__c FROM BI_PL_Position__c WHERE Id = :posCycles.get(0).BI_PL_Position__c];
        
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

		Call2_vod__c call2;
		List<Call2_vod__c> lstCall2 = new List<Call2_vod__c>();

		for(integer i = 0; i < 4; i++){
			call2 = new Call2_vod__c(
				Status_vod__c = 'Submitted_vod', 
				Call_Date_vod__c = Date.today(), 
				Account_vod__c = listAcc.get(i).Id, 
				Territory_vod__c = position.get(0).Name,
				CP1__c = listProd.get(i).Name
			);
			lstCall2.add(call2);
			call2 = new Call2_vod__c(
				Status_vod__c = 'Submitted_vod', 
				Call_Date_vod__c = Date.today(), 
				Account_vod__c = listAcc.get(i).Id, 
				Territory_vod__c = position.get(0).Name, 
				CP2__c = listProd.get(i).Name
			);
			lstCall2.add(call2);

		}
		insert lstCall2;
    }

	@isTest static void test_getSummaryProductDetails() {
		
		getData();

		BI_PL_SummaryProductsDetailsCtrl o = new BI_PL_SummaryProductsDetailsCtrl();

		BI_PL_SummaryProductsDetailsCtrl.getSummaryProductDetails(cycle.Id, hierarchy, SCHANELNAME, null);
		
		Test.startTest();
		
			Map<String, BI_PL_SummaryProductsDetailsCtrl.SummaryProductDetailModel> mapTest = 
											BI_PL_SummaryProductsDetailsCtrl.getSummaryProductDetails(cycle.Id, hierarchy, SCHANELNAME, posCycles.get(0).BI_PL_Position__c);

			system.assertNotEquals(mapTest,null);

		Test.stopTest();
	}
	
	@isTest static void test_getSummaryProductDetailsFromVeeva() {

		getData();
		getVeevaData();
		
		Test.startTest();

			BI_PL_SummaryProductsDetailsCtrl.SummaryProductVeevaDetailWrapper oSummaryWrap = 
							BI_PL_SummaryProductsDetailsCtrl.getSummaryProductDetailsFromVeeva(cycle.Id, hierarchy, SCHANELNAME, posCycles.get(0).BI_PL_Position__c, lstProductsName, null);

			system.assert(oSummaryWrap != null);
		Test.stopTest();

	}	
	
	@isTest static void test_getSummaryP1P2Details(){

		getData();

		//Add Secondary Product
		list<BI_PL_Detail_preparation__c> lstDetPrep = [SELECT Id, BI_PL_Secondary_product__c FROM BI_PL_Detail_preparation__c];
		for (BI_PL_Detail_preparation__c inDetailPrep : lstDetPrep){
			inDetailPrep.BI_PL_Secondary_product__c = listProd.get(listProd.size()-1).Id;
		}
		update lstDetPrep;

		Test.startTest();

			BI_PL_SummaryProductsDetailsCtrl.SummaryP1P2Wrapper oSummaryWrap = 
														BI_PL_SummaryProductsDetailsCtrl.getSummaryP1P2Details(cycle.Id, hierarchy, SCHANELNAME, posCycles.get(0).BI_PL_Position__c, true);

			system.assert(oSummaryWrap != null);

		Test.stopTest();
	}

	@isTest static void test_getFullSummaryProductDetailsFromVeeva(){
		
		getData();	
		getVeevaData();

		Test.startTest();

			BI_PL_SummaryProductsDetailsCtrl.SummaryProductVeevaDetailWrapper oSummaryWrap = 
							BI_PL_SummaryProductsDetailsCtrl.getFullSummaryProductDetailsFromVeeva(cycle.Id, hierarchy, SCHANELNAME, posCycles.get(0).BI_PL_Position__c, lstProductsName, null);

			system.assert(oSummaryWrap != null);

		Test.stopTest();
	}

	@isTest static void test_getFullSummaryP1P2Details(){

		getData();

		//Add Secondary Product
		list<BI_PL_Detail_preparation__c> lstDetPrep = [SELECT Id, BI_PL_Secondary_product__c FROM BI_PL_Detail_preparation__c];
		for (BI_PL_Detail_preparation__c inDetailPrep : lstDetPrep){
			inDetailPrep.BI_PL_Secondary_product__c = listProd.get(listProd.size()-1).Id;
		}
		update lstDetPrep;

		Test.startTest();

			BI_PL_SummaryProductsDetailsCtrl.SummaryP1P2Wrapper oWrap = 
												BI_PL_SummaryProductsDetailsCtrl.getFullSummaryP1P2Details(cycle.Id, hierarchy, SCHANELNAME, posCycles.get(0).BI_PL_Position__c, true, null);
			system.assert(oWrap != null);

		Test.stopTest();
	}

	@isTest static void test_getIterationFromVeeva(){

		getData();
		getVeevaData();

		Test.startTest();

			Map<String,Map<String,Map<String,Integer>>> mapIterations = 
														BI_PL_SummaryProductsDetailsCtrl.getIterationFromVeeva(SCHANELNAME, cycle.Id, hierarchy, lstProductsName.get(0), lstProductsName.get(1));
			system.assert(mapIterations!=null);

		Test.stopTest();

		mapIterations = BI_PL_SummaryProductsDetailsCtrl.getIterationFromVeeva(SCHANELNAME, cycle.Id, hierarchy, null, lstProductsName.get(1));

		system.assert(mapIterations!=null);
	}

	@isTest static void test_getIterationFromVeevaByProduct(){

		getData();
		getVeevaData();		

		//Add Secondary Product
		list<BI_PL_Detail_preparation__c> lstDetPrep = [SELECT Id, BI_PL_Secondary_product__c FROM BI_PL_Detail_preparation__c];
		for (BI_PL_Detail_preparation__c inDetailPrep : lstDetPrep){
			inDetailPrep.BI_PL_Secondary_product__c = listProd.get(listProd.size()-1).Id;
		}
		update lstDetPrep;

		Test.startTest();

			BI_PL_SummaryProductsDetailsCtrl.IterarionsVeevaWrapper oWrapper = 
												BI_PL_SummaryProductsDetailsCtrl.getIterationFromVeevaByProduct(SCHANELNAME, cycle.Id, hierarchy, lstProductsName, null );
			system.assert(oWrapper!=null);

		Test.stopTest();

		oWrapper = BI_PL_SummaryProductsDetailsCtrl.getIterationFromVeevaByProduct(SCHANELNAME, cycle.Id, hierarchy, null, lstProductsName );

		system.assert(oWrapper!=null);
	}
	
}