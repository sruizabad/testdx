/**
 *  21/08/2017
 *	- Performance fix getHierarchyNodes();
 *	11/08/2017
 *	- Creation.
 *	@author	OMEGA CRM
 */
public without sharing class BI_PL_SummaryReviewCtrl {

	private static final String PREPARATION_STATUS_APPROVED = 'Approved';
	private static final String PREPARATION_STATUS_UNDER_REVIEW = 'Under Review';


	public BI_PL_SummaryReviewCtrl() {


	}


	public static User getDefaultOwnerId() {

		User user = [SELECT Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId()];

		if (user != null) {

			String userCountryCode = user.Country_Code_BI__c;
			for (BI_PL_Country_Settings__c s : BI_PL_Country_Settings__c.getall().values()) {
				if (s.BI_PL_Country_code__c == userCountryCode) {
					if (String.isBlank(BI_PL_Country_Settings__c.getInstance(s.Name).BI_PL_Default_owner_user_name__c))
						throw new BI_PL_Exception(Label.BI_PL_Default_owner_null + ': ' + userCountryCode);

					String userName = BI_PL_Country_Settings__c.getInstance(s.Name).BI_PL_Default_owner_user_name__c;
					User defaultUser = [SELECT Id, Name FROM User WHERE Username = :userName];
					return defaultUser;

				}
			}
		}
		return null;
	}
	/**
	 *	Returns:
	 *	- The future closest cycle.
	 *	- The preparations visible for the current user for the closest future cycle.
	 *	- The hierarchy options available for the cycle and the user's visibility.
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static PLANiTPreparationsWrapper getPreparations(String cycleId, String countryCode) {

		Set<SelectOption> hierarchyOptions = new Set<SelectOption>();
		String linkedHierarchy = '';

		List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();
		for (BI_PL_Preparation__c p : [SELECT Id, Name, BI_PL_Position_cycle__r.BI_PL_Position__c,
		                               BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, BI_PL_Status__c, BI_PL_Field_force__c 
		                               FROM BI_PL_Preparation__c
		                               WHERE BI_PL_Country_code__c = :countryCode
		                                       AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId AND Id IN (SELECT ParentId FROM BI_PL_Preparation__Share WHERE UserOrGroupId = :Userinfo.getUserId())]) {
			
			preparations.add(p);
			



		}

		List<BI_PL_Position_cycle_user__c> hierarchies = [SELECT BI_PL_User__c, BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c FROM BI_PL_Position_cycle_user__c WHERE BI_PL_User__c = :UserInfo.getUserId() AND  BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycleId ];

		for (BI_PL_Position_cycle_user__c hierarchy : hierarchies) {
			hierarchyOptions.add(new SelectOption(hierarchy.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, hierarchy.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c));
		}

		if (hierarchies.size() > 0) {
			linkedHierarchy =  hierarchies.get(0).BI_PL_Position_cycle__r.BI_PL_Hierarchy__c;
			
		}

		return new PLANiTPreparationsWrapper(preparations, cycleId, hierarchyOptions, linkedHierarchy);

	}


	/**
	 * @author	OMEGA CRM
	 * @return  The setup data.
	 */
	@RemoteAction
	public static PlanitSetupModel getSetupData() {
		PlanitSetupModel setup = new PlanitSetupModel();

		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		setup.userCountryCode = currentUser.Country_Code_BI__c;

		// Get cycles for current user
		BI_PL_Cycle__c[] cycles = new List<BI_PL_Cycle__c>([SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, Name FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :currentUser.Country_Code_BI__c ORDER BY BI_PL_Start_date__c]);

		BI_PL_Cycle__c[] futureCycle = new List<BI_PL_Cycle__c>([SELECT Id FROM BI_PL_Cycle__c WHERE BI_PL_Start_date__c > TODAY
		        AND BI_PL_Country_code__c = :currentUser.Country_Code_BI__c ORDER BY BI_PL_Start_date__c LIMIT 1]);

		List<String> countryColumns = new List<String>();

		//Get number of levels to show in the hierarchy
		BI_PL_Country_settings__c settings = [SELECT BI_PL_Levels__c, BI_PL_Individual_Approval__c,BI_PL_Allow_dropping_targets__c, BI_PL_Add_target_mode__c, BI_PL_Show_Hierarchy_Tree__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = :setup.userCountryCode LIMIT 1];

		//Permissions and permission sets
		Map<String, Boolean> permissionsMap = BI_PL_PreparationServiceUtility.getCurrentUserPermissions();
		setup.isSR = permissionsMap.get('isSR');
		setup.isSM = permissionsMap.get('isSM');
		setup.isDS = permissionsMap.get('isDS');
		setup.cycles = cycles;
		setup.levels = settings.BI_PL_Levels__c;

		setup.allowIndividualApproval = settings.BI_PL_Individual_Approval__c;
		setup.showHierarchyTree = settings.BI_PL_Show_Hierarchy_Tree__c;
		setup.allowDroppingTargets = settings.BI_PL_Allow_dropping_targets__c;
		setup.allowedToAdd = settings.BI_PL_Add_target_mode__c != BI_PL_CountrySettingsUtility.ADD_NEW_TARGET_NONE;
		setup.transferAndShare = BI_PL_CountrySettingsUtility.getCountrySettingsForCurrentUser().BI_PL_Transfer_and_share_fieldforces__c;

		if (futureCycle.size() > 0) {
			setup.futureCycle = futureCycle.get(0);
		} else {
			setup.futureCycle = cycles.get(0);
		}

		return setup;
	}
	/**
	 *	Returns a map where each position is a hierarchy node visible for the current user (grouped by position id) and the
	 *	hierarchy root node.
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static PLANiTHierarchyNodesWrapper getHierarchyNodes(String cycleId, String hierarchyName) {
		//Get default owner
		//User defaultOwner = new User();

		Map<String, PLANiTTreeNode> hierarchy = new Map<String, PLANiTTreeNode>();

		Map<String, BI_PL_PositionCycleWrapper> tree = new BI_PL_VisibilityTree(cycleId, hierarchyName).getTree();

		List<String> rootNodeId = new List<String>();
		List<String> treeList = new List<String>(tree.keySet());

		//System.debug(System.LoggingLevel.ERROR, treeList);

		for (Integer i = 0; i < treeList.size(); i++) {
			String positionId =  treeList.get(i);

			PLANiTTreeNode node = new PLANiTTreeNode(tree.get(positionId));
			hierarchy.put(positionId, node);

			if (node.isCurrentUsersNode)
				rootNodeId.add(node.positionId);
		}

		//return tree;

		return new PLANiTHierarchyNodesWrapper(hierarchy, rootNodeId);
		//return null;
	}
	
	/**
	 *	Returns the aggregated data for the preparation.
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static PLANiTAggregatedDataOutput getAggregatedDataForPreparations(List<String> preparationIds, String offSetId) {
		Map<String, Map<String, Map<String,PLANiTPositionAggregatedData>>> dataByChannel = new Map<String, Map<String, Map<String,PLANiTPositionAggregatedData>>> ();
		Set<SelectOption> channelOptions = new Set<SelectOption>();
		Integer counter = 0;
		String lastId = '';

		for (BI_PL_Channel_detail_preparation__c channelDetail : [SELECT Id, BI_PL_Channel__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__c, BI_PL_Sum_adjusted_interactions__c, BI_PL_Sum_planned_interactions__c,
		        BI_PL_Max_adjusted_interactions__c, BI_PL_Max_planned_interactions__c, BI_PL_Target__r.BI_PL_Added_manually__c, BI_PL_Target__r.BI_PL_Next_best_account__c, BI_PL_Target__r.BI_PL_No_see_list__c, BI_PL_Removed__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, BI_PL_Target__r.BI_PL_Target_customer__r.IsPersonAccount, BI_PL_Rejected__c
		        FROM BI_PL_Channel_detail_preparation__c
		        WHERE BI_PL_Target__r.BI_PL_Header__c IN :preparationIds AND Id > :offSetId
		        ORDER BY Id ASC LIMIT 2000]) {

			String channel = channelDetail.BI_PL_Channel__c;
			String position = channelDetail.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__c;
			String accountType = (channelDetail.BI_PL_Target__r.BI_PL_Target_customer__r.IsPersonAccount)?'HCP':'HCO';
			
			if (!dataByChannel.containsKey(channel))
				dataByChannel.put(channel, new Map<String, Map<String,PLANiTPositionAggregatedData>>());

			if (!dataByChannel.get(channel).containsKey(position))
				dataByChannel.get(channel).put(position, new Map<String,PLANiTPositionAggregatedData>());

			if (!dataByChannel.get(channel).get(position).containsKey(accountType))
				dataByChannel.get(channel).get(position).put(accountType, new PLANiTPositionAggregatedData());

			dataByChannel.get(channel).get(position).get(accountType).setHierarchy(channelDetail.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c);
			dataByChannel.get(channel).get(position).get(accountType).setPositionId(channelDetail.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__c);
			dataByChannel.get(channel).get(position).get(accountType).addA((Integer)channelDetail.BI_PL_Sum_adjusted_interactions__c, channelDetail.BI_PL_Target__r.BI_PL_Added_manually__c, channelDetail.BI_PL_Removed__c,channelDetail.BI_PL_Target__r.BI_PL_Next_best_account__c,channelDetail.BI_PL_Target__r.BI_PL_No_see_list__c);
			dataByChannel.get(channel).get(position).get(accountType).addMaxA((Integer)channelDetail.BI_PL_Max_adjusted_interactions__c,channelDetail.BI_PL_Target__r.BI_PL_Added_manually__c, channelDetail.BI_PL_Removed__c,channelDetail.BI_PL_Target__r.BI_PL_Next_best_account__c,channelDetail.BI_PL_Target__r.BI_PL_No_see_list__c);
			dataByChannel.get(channel).get(position).get(accountType).addP((Integer)channelDetail.BI_PL_Sum_planned_interactions__c,channelDetail.BI_PL_Target__r.BI_PL_Added_manually__c,channelDetail.BI_PL_Removed__c, channelDetail.BI_PL_Target__r.BI_PL_Next_best_account__c,channelDetail.BI_PL_Target__r.BI_PL_No_see_list__c);
			dataByChannel.get(channel).get(position).get(accountType).addMaxP((Integer)channelDetail.BI_PL_Max_planned_interactions__c,channelDetail.BI_PL_Target__r.BI_PL_Added_manually__c,channelDetail.BI_PL_Removed__c, channelDetail.BI_PL_Target__r.BI_PL_Next_best_account__c,channelDetail.BI_PL_Target__r.BI_PL_No_see_list__c);
			
			dataByChannel.get(channel).get(position).get(accountType).addTarget(channelDetail.BI_PL_Target__r.BI_PL_Added_manually__c, channelDetail.BI_PL_Removed__c,channelDetail.BI_PL_Target__r.BI_PL_Next_best_account__c,channelDetail.BI_PL_Target__r.BI_PL_No_see_list__c);
			dataByChannel.get(channel).get(position).get(accountType).addRejectedTargets(channelDetail.BI_PL_Rejected__c);

			channelOptions.add(new SelectOption(channel, channel));

			counter++;
			lastId = channelDetail.Id;


		}
		if (counter < 2000)
			lastId = '';


		return new PLANiTAggregatedDataOutput(dataByChannel, channelOptions, lastId);
	}

	/**
	 *	Returns the transfer and share data for the preparation.
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static PLANiTTransferShareDataOutput getTransferShareData(List<String> preparationIds, String hierarchy) {

		// Key = Source preparation Id.
		Map<String, Map<String, Map<String, PLANiTTransferShareData>>> output = new Map<String, Map<String, Map<String, PLANiTTransferShareData>>> ();



		for (BI_PL_Preparation_action_item__c item : [SELECT Id,
		        BI_PL_Parent__r.CreatedDate,
		        BI_PL_Parent__r.BI_PL_Account__c,
		        BI_PL_Parent__r.BI_PL_Type__c,
		        BI_PL_Parent__r.BI_PL_Channel__c,
		        BI_PL_Parent__r.BI_PL_Source_preparation__c,
		        BI_PL_Parent__r.BI_PL_Source_preparation__r.Name,
		        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
		        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position__c,
		        BI_PL_Parent__r.BI_PL_Account__r.IsPersonAccount, 
		        BI_PL_Status__c

		        FROM BI_PL_Preparation_action_item__c
		        WHERE BI_PL_Parent__r.BI_PL_Source_preparation__c IN :preparationIds AND BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchy
		                ORDER BY BI_PL_Parent__r.CreatedDate DESC]) {

				String accountType = (item.BI_PL_Parent__r.BI_PL_Account__r.IsPersonAccount)?'HCP':'HCO';


				if (!output.containsKey(item.BI_PL_Parent__r.BI_PL_Channel__c))
					output.put(item.BI_PL_Parent__r.BI_PL_Channel__c, new Map<String, Map<String, PLANiTTransferShareData>>());

				if (!output.get(item.BI_PL_Parent__r.BI_PL_Channel__c).containsKey(item.BI_PL_Parent__r.BI_PL_Source_preparation__c))
					output.get(item.BI_PL_Parent__r.BI_PL_Channel__c).put(item.BI_PL_Parent__r.BI_PL_Source_preparation__c, new Map<String, PLANiTTransferShareData>());

				if (!output.get(item.BI_PL_Parent__r.BI_PL_Channel__c).get(item.BI_PL_Parent__r.BI_PL_Source_preparation__c).containsKey(accountType))
					output.get(item.BI_PL_Parent__r.BI_PL_Channel__c).get(item.BI_PL_Parent__r.BI_PL_Source_preparation__c).put(accountType, new PLANiTTransferShareData());


				output.get(item.BI_PL_Parent__r.BI_PL_Channel__c).get(item.BI_PL_Parent__r.BI_PL_Source_preparation__c).get(accountType).setHierarchy(item.BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c);
				output.get(item.BI_PL_Parent__r.BI_PL_Channel__c).get(item.BI_PL_Parent__r.BI_PL_Source_preparation__c).get(accountType).add(item.BI_PL_Parent__r.BI_PL_Type__c, item.BI_PL_Status__c);



		}

		return  new PLANiTTransferShareDataOutput(output);

	}


	/**
	 *	Confirms the specified node (BI_PL_Position_cycle__c)
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static Boolean confirmNode(String positionCycleId, Boolean isHierarchyRoot, List<String> preparationsId,  String channel) {
		Savepoint sp = Database.setSavepoint();

		try {
			// Set the Position Cycle to confirmed
			update new BI_PL_Position_cycle__c(Id = positionCycleId, BI_PL_Confirmed__c = true);

			// If the confirmed Position Cycle is the hierarchy's root set all preparations to Approved:
			if (isHierarchyRoot) {
				List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();
				for (String preparationId : preparationsId)
					preparations.add(new BI_PL_Preparation__c(Id = preparationId, BI_PL_Status__c = PREPARATION_STATUS_APPROVED));

				update preparations;
			}


			//Update channel detail in case that those preparations have been rejected
			List<BI_PL_Channel_detail_preparation__c> details = new List<BI_PL_Channel_detail_preparation__c>();
			Map<Id, BI_PL_Target_preparation__c> targets = new Map<Id, BI_PL_Target_Preparation__c>([SELECT Id FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__c IN :preparationsId]);
			for (BI_PL_Channel_detail_preparation__c detail : [SELECT Id FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Channel__c = :channel AND BI_PL_Target__c IN :targets.keySet() AND BI_PL_Edited__c = true AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__c =: positionCycleId]) {
				details.add(new BI_PL_Channel_detail_preparation__c(Id = detail.Id, BI_PL_Reviewed__c = false, BI_PL_Rejected__c= false));
			}
			System.debug('details'+details);

			update details;

		} catch (exception e) {
			Database.rollback(sp);
			return false;
		}
		return true;
	}

	/**
	 *	Confirms a group of nodes (BI_PL_Position_cycle__c)
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static Boolean confirmGroupNodes(List<String> positionCycleIds, Boolean isHierarchyRoot, List<String> preparationsId,  String channel) {
		Savepoint sp = Database.setSavepoint();

		try {
			// Set the Position Cycle to confirmed
			List<BI_PL_Position_cycle__c> positions = new List<BI_PL_Position_cycle__c>();
			for (String positioncycleId : positionCycleIds)
					positions.add(new BI_PL_Position_cycle__c(Id = positioncycleId, BI_PL_Confirmed__c = true));

			update positions;
			//update new BI_PL_Position_cycle__c(Id IN positionCycleId, BI_PL_Confirmed__c = true);

			// If the confirmed Position Cycle is the hierarchy's root set all preparations to Approved:
			if (isHierarchyRoot) {
				List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();
				for (String preparationId : preparationsId)
					preparations.add(new BI_PL_Preparation__c(Id = preparationId, BI_PL_Status__c = PREPARATION_STATUS_APPROVED));

				update preparations;
			}


			//Update channel detail in case that those preparations have been rejected
			List<BI_PL_Channel_detail_preparation__c> details = new List<BI_PL_Channel_detail_preparation__c>();
			Map<Id, BI_PL_Target_preparation__c> targets = new Map<Id, BI_PL_Target_Preparation__c>([SELECT Id FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__c IN :preparationsId]);
			for (BI_PL_Channel_detail_preparation__c detail : [SELECT Id FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Channel__c = :channel AND BI_PL_Target__c IN :targets.keySet() AND BI_PL_Edited__c = true AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__c IN : positionCycleIds AND BI_PL_Rejected__c = false]) {
				details.add(new BI_PL_Channel_detail_preparation__c(Id = detail.Id, BI_PL_Reviewed__c = true, BI_PL_Rejected__c= false));
			}
			System.debug('details'+details);

			update details;

		} catch (exception e) {
			System.debug('CATCH: ' + e.getMessage());
			Database.rollback(sp);
			return false;
		}
		return true;
	}


	/**
	 *	Unconfirms the specified node (BI_PL_Position_cycle__c)
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static Boolean unconfirmNode(String positionCycleId, String reason, Boolean updatePreparationsStatus, List<String> preparationsId, String channel) {
		Savepoint sp = Database.setSavepoint();

		try {
			// Set the Position Cycle to unconfirmed with a reason:
			update new BI_PL_Position_cycle__c(Id = positionCycleId, BI_PL_Confirmed__c = false, BI_PL_Rejected_reason__c = reason);

			// Update the preparations status to under review:
			if (updatePreparationsStatus) {
				List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();
				for (String preparationId : preparationsId)
					preparations.add(new BI_PL_Preparation__c(Id = preparationId, BI_PL_Status__c = PREPARATION_STATUS_UNDER_REVIEW));

				update preparations;
			}

			List<BI_PL_Channel_detail_preparation__c> details = new List<BI_PL_Channel_detail_preparation__c>();
			Map<Id, BI_PL_Target_preparation__c> targets = new Map<Id, BI_PL_Target_Preparation__c>([SELECT Id FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__c IN :preparationsId]);

			for (BI_PL_Channel_detail_preparation__c detail : [SELECT Id FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Channel__c = :channel AND BI_PL_Target__c IN :targets.keySet() AND BI_PL_Edited__c = true AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__c =: positionCycleId]) {
				details.add(new BI_PL_Channel_detail_preparation__c(Id = detail.Id, BI_PL_Reviewed__c = false, BI_PL_Rejected__c= false));
			}


			update details;

		} catch (exception e) {
			Database.rollback(sp);
			System.debug(System.LoggingLevel.ERROR, e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 *	Unconfirms the specified node (BI_PL_Position_cycle__c)
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static List<String> unconfirmNodeList(List<String> positionCycleId, String reason, Boolean updatePreparationsStatus, List<String> preparationsId, String channel) {
		Savepoint sp = Database.setSavepoint();
		List<String> positionsId = new List<String>();

		try {
			List<BI_PL_Position_cycle__c> positions = new List<BI_PL_Position_cycle__c>();
			for (String positionId : positionCycleId){
				positions.add(new BI_PL_Position_cycle__c(Id = positionId, BI_PL_Confirmed__c = false, BI_PL_Rejected_reason__c = reason));
			}

			update positions;

			for(BI_PL_Position_cycle__c po : [SELECT Id, BI_PL_Position__c FROM BI_PL_Position_cycle__c WHERE Id IN: positioncycleId]){
				positionsId.add(po.BI_PL_Position__c);
			}
			
			if (updatePreparationsStatus) {
				List<BI_PL_Preparation__c> preparations = new List<BI_PL_Preparation__c>();
				for (String preparationId : preparationsId){
					preparations.add(new BI_PL_Preparation__c(Id = preparationId, BI_PL_Status__c = PREPARATION_STATUS_UNDER_REVIEW));
				}
				update preparations;
			}

			List<BI_PL_Channel_detail_preparation__c> details = new List<BI_PL_Channel_detail_preparation__c>();
			Map<Id, BI_PL_Target_preparation__c> targets = new Map<Id, BI_PL_Target_Preparation__c>([SELECT Id FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__c IN :preparationsId]);

			for (BI_PL_Channel_detail_preparation__c detail : [SELECT Id FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Channel__c = :channel AND BI_PL_Target__c IN :targets.keySet() AND BI_PL_Edited__c = true AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__c =: positionCycleId]) {
				details.add(new BI_PL_Channel_detail_preparation__c(Id = detail.Id, BI_PL_Reviewed__c = false, BI_PL_Rejected__c= false));
			}


			update details;

		} catch (exception e) {
			Database.rollback(sp);
			System.debug(System.LoggingLevel.ERROR, e.getMessage());
			//return false;
		}
		return positionsId;
	}

	/**
	 *	Approve prepation
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static String approvePreparation(List<String> preparationsId, String channel, String offset) {
		Savepoint sp = Database.setSavepoint();
		String newOffset = '';
		try {

			List<BI_PL_Channel_detail_preparation__c> details = new List<BI_PL_Channel_detail_preparation__c>();
			//Map<Id, BI_PL_Target_preparation__c> targets = new Map<Id, BI_PL_Target_Preparation__c>([SELECT Id FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__c IN :preparationsId]);
			
			Integer i = 0;
			for (BI_PL_Channel_detail_preparation__c detail : [SELECT Id FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Channel__c = :channel AND BI_PL_Target__r.BI_PL_Header__c IN :preparationsId AND BI_PL_Edited__c = true AND BI_PL_Rejected__c = false AND Id > :offset ORDER BY Id LIMIT 2000]) {
				details.add(new BI_PL_Channel_detail_preparation__c(Id = detail.Id, BI_PL_Reviewed__c = true));
				
				i++;
				if(i==2000){
					newOffset = detail.Id;
				}
			}


			update details;

		} catch (exception e) {
			Database.rollback(sp);
			return '';
		}
		return newOffset;
	}


	/**
	 *	Return unreviewed Details
	 *	@author	OMEGA CRM
	 */
	@RemoteAction
	public static PlanitUnreviewedDetailOutput checkUnReviewedDetails(Map<Id, BI_PL_Preparation__c> preparations, String channel, String offSetId) {

		Map<String, List<BI_PL_Channel_detail_preparation__c>>  output = new Map<String, List<BI_PL_Channel_detail_preparation__c>> ();
		Integer counter = 0;
		String offSet = '';
		System.debug(preparations.keySet());
		for (BI_PL_Channel_detail_preparation__c detail : [SELECT Id, BI_PL_Reviewed__c, BI_PL_Target__r.BI_PL_Header__c, BI_PL_Edited__c, BI_PL_Removed__c, BI_PL_Target__r.BI_PL_Added_manually__c, BI_PL_Target__r.BI_PL_Target_customer__r.IsPersonAccount 
		        FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Channel__c = :channel AND BI_PL_Target__r.BI_PL_Header__c IN :preparations.keySet() AND BI_PL_Reviewed__c = false AND BI_PL_Edited__c = true AND BI_PL_Rejected__c = false AND Id > :offSetId ORDER BY Id ASC LIMIT 500]) {
			if (!output.containsKey(detail.BI_PL_Target__r.BI_PL_Header__c))
				output.put(detail.BI_PL_Target__r.BI_PL_Header__c, new List<BI_PL_Channel_detail_preparation__c>());

			output.get(detail.BI_PL_Target__r.BI_PL_Header__c).add(detail);
			counter++;
			if (counter == 500)
				offSet = detail.Id;

		}

		return new PlanitUnreviewedDetailOutput(output, offSet);

	}



	@RemoteAction
	public static Boolean submitPreparation(List<String> preparationsId, String channel){
		Savepoint sp = Database.setSavepoint();
		
		List<BI_PL_Preparation_action_item__c> pendingActionItems = new List<BI_PL_Preparation_action_item__c>([SELECT ID FROM BI_PL_Preparation_action_item__c 
                WHERE (BI_PL_Target_preparation__c IN :preparationsId OR BI_PL_Parent__r.BI_PL_Source_preparation__c IN :preparationsId)
                AND BI_PL_Status__c = 'Pending']);

        if(!pendingActionItems.isEmpty()) {
            throw new BI_PL_Exception(Label.BI_PL_Pending_transfer_share_requests);
        }


        
        try {

        	List<BI_PL_Position_cycle__c> posCyList = new List<BI_PL_Position_cycle__c>();

	        for(BI_PL_Position_cycle__c posC : [SELECT Id, BI_PL_Confirmed__c FROM BI_PL_Position_cycle__c WHERE Id IN (SELECT BI_PL_Position_cycle__c FROM BI_PL_Preparation__c WHERE Id  IN :preparationsId)]){

	        	posC.BI_PL_Confirmed__c = true;
	        	posCyList.add(posC);	
	        }
            update posCyList;


            //Update channel detail in case that those preparations have been rejected
			List<BI_PL_Channel_detail_preparation__c> details = new List<BI_PL_Channel_detail_preparation__c>();
			Map<Id, BI_PL_Target_preparation__c> targets = new Map<Id, BI_PL_Target_Preparation__c>([SELECT Id FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__c IN :preparationsId]);
			for (BI_PL_Channel_detail_preparation__c detail : [SELECT Id FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Channel__c = :channel AND BI_PL_Target__c IN :targets.keySet() AND BI_PL_Edited__c = true]) {
				details.add(new BI_PL_Channel_detail_preparation__c(Id = detail.Id, BI_PL_Reviewed__c = false, BI_PL_Rejected__c= false));
			}
			System.debug('details' + details);

			update details;
            return true;
        } catch(Exception e) {
        	Database.rollback(sp);
            return false;
        }
	}

	@RemoteAction
	public static Map<String, Integer> checkPendingRequests(List<String> preparationsId)
	{
		Map<String, Integer> pendingActions = new Map<String, Integer>();
		Set<String> setPreparations = new Set<String>(preparationsId);
		System.debug(setPreparations);
		for(BI_PL_Preparation_action_item__c action: [SELECT ID, BI_PL_Target_preparation__c,BI_PL_Parent__r.BI_PL_Source_preparation__c, BI_PL_Status__c  FROM BI_PL_Preparation_action_item__c 
                WHERE (BI_PL_Target_preparation__c IN :preparationsId OR BI_PL_Parent__r.BI_PL_Source_preparation__c IN :preparationsId)
                AND BI_PL_Status__c = 'Pending'])
		{
			System.debug('targert '+action.BI_PL_Target_preparation__c +' source '+action.BI_PL_Parent__r.BI_PL_Source_preparation__c);
			if(setPreparations.contains(action.BI_PL_Target_preparation__c) && action.BI_PL_Status__c == 'Pending')
			{
				if(!pendingActions.containsKey(action.BI_PL_Target_preparation__c))
					pendingActions.put(action.BI_PL_Target_preparation__c, 1);
				else
					pendingActions.put(action.BI_PL_Target_preparation__c, pendingActions.get(action.BI_PL_Target_preparation__c)+1);
			}
			if(setPreparations.contains(action.BI_PL_Parent__r.BI_PL_Source_preparation__c) && action.BI_PL_Status__c == 'Pending')
			{
				if(!pendingActions.containsKey(action.BI_PL_Parent__r.BI_PL_Source_preparation__c))
					pendingActions.put(action.BI_PL_Parent__r.BI_PL_Source_preparation__c, 1);
				else
					pendingActions.put(action.BI_PL_Parent__r.BI_PL_Source_preparation__c, pendingActions.get(action.BI_PL_Parent__r.BI_PL_Source_preparation__c)+1);
			}
			
		}
		return pendingActions;
	}





	public class PLANiTAggregatedDataOutput {
		public Set<SelectOption> channelOptions {get; set;}
		public Map<String, Map<String, Map<String,PLANiTPositionAggregatedData>>> aggregatedDataByChannel {get; set;}
		String idOffset;

		public PLANiTAggregatedDataOutput(Map<String, Map<String, Map<String,PLANiTPositionAggregatedData>>> aggregatedDataByChannel, Set<SelectOption> channelOptions, String idOffset) {
			this.channelOptions = channelOptions;
			this.aggregatedDataByChannel = aggregatedDataByChannel;
			this.idOffset = idOffset;
		}
	}

	public class PLANiTTransferShareDataOutput {
		public Map<String, Map<String, Map<String,PLANiTTransferShareData>>> transferShareByChannel {get; set;}

		public PLANiTTransferShareDataOutput(Map<String, Map<String, Map<String, PLANiTTransferShareData>>> transferShareByChannel) {

			this.transferShareByChannel = transferShareByChannel;
		}
	}

	public class PlanitUnreviewedDetailOutput {
		public Map<String, List<BI_PL_Channel_detail_preparation__c>> data {get; set;}
		public String offSet {get; set;}

		public PlanitUnreviewedDetailOutput(Map<String, List<BI_PL_Channel_detail_preparation__c>> data, String offSet) {
			this.data = data;
			this.offSet = offSet;
		}
	}

	public class PlanitSetupModel {

		public Boolean isDS;
		public Boolean isSM;
		public Boolean isSR;

		public String userCountryCode;
		public List<BI_PL_Cycle__c> cycles;
		public BI_PL_Cycle__c futureCycle;
		public Decimal levels;

		public Boolean allowDroppingTargets;
		public Boolean allowedToAdd;
		public Boolean showHierarchyTree;
		public Boolean allowIndividualApproval;
		public String transferAndShare;

		public PlanitSetupModel() {
		}
	}

	public class PLANiTPositionAggregatedData {
		public Integer sumA {get; set;}
		public Integer sumP {get; set;}
		public Integer sumMaxA {get; set;}
		public Integer sumMaxP {get; set;}

		public Decimal nTargets {get; set;}
		public Decimal nAddedTargets {get; set;}
		public Decimal nRemovedTargets {get; set;}

		public String positionId {get; set;}
		public String hierarchy {get; set;}

		public Integer rejectedTargets {get; set;}

		public PLANiTPositionAggregatedData() {
			this.sumA = 0;
			this.sumP = 0;
			this.sumMaxA = 0;
			this.sumMaxP = 0;

			this.nTargets = 0;
			this.nAddedTargets = 0;
			this.nRemovedTargets = 0;
			this.rejectedTargets = 0;
		}

		public void setHierarchy(String hierarchy) {
			this.hierarchy = hierarchy;
		}
		public void setPositionId(String positionId) {
			this.positionId = positionId;
		}
		//GLOS 667 - Total target need to substract the removed, noseelist and nextbest (that not added)
		public void addTarget(Boolean added, Boolean removed, Boolean nextbest, Boolean noseelist) {
			if ((added && nextbest && !removed) || (!removed && !nextbest && !noseelist))
				nTargets++;
			if (added && !removed)
				nAddedTargets++;
			if (removed && !added)
				nRemovedTargets++;
		}
		//GLOS 667 - Adjusted - noseelit and nextbest have details/calls initial, don't sum those if not added the target
		public void addA(Integer amount, Boolean added, Boolean removed, Boolean nextbest, Boolean noseelist) {
			if (amount != null && ((added && nextbest && !removed) || (added && noseelist && !removed) || (!removed && !nextbest && !noseelist)))
				sumA += amount;
		}
		public void addMaxA(Integer amount, Boolean added, Boolean removed, Boolean nextbest, Boolean noseelist) {
			if (amount!=null &&  amount > 0 && ((added && nextbest && !removed) || (added && noseelist && !removed) || (!removed && !nextbest && !noseelist)))
			{
				
				sumMaxA += amount;
			}
		}
		//GLOS 667 - Planned - Added, noseelit and nextbest have details/calls initial, don't sum those
		public void addP(Integer amount, Boolean added, Boolean removed, Boolean nextbest, Boolean noseelist) {
			if (amount != null && ((added && nextbest && !removed) || (added && noseelist && !removed) || (!removed && !nextbest && !noseelist)))
				sumP += amount;
		}
		public void addMaxP(Integer amount, Boolean added, Boolean removed, Boolean nextbest, Boolean noseelist) {
			if (amount != null && ((added && nextbest && !removed) || (added && noseelist && !removed) || (!removed && !nextbest && !noseelist)))
				sumMaxP += amount;
		}

		public void addRejectedTargets(Boolean rejected){
			if(rejected)
				this.rejectedTargets += 1;
		}
	}

	public class PLANiTTransferShareData {
		public Integer numShare {get; set;}
		public Integer numTransfer {get; set;}
		public String hierarchy {get; set;}
		public Integer numSharePending {get; set;}
		public Integer numTransferPending {get; set;}

		public PLANiTTransferShareData() {
			this.numShare = 0;
			this.numTransfer = 0;
			this.numTransferPending = 0;
			this.numSharePending = 0;
		}

		public void setHierarchy(String hierarchy) {
			this.hierarchy = hierarchy;
		}


		public void add(String type, String status) {

			if (type == 'Transfer')
			{
				numTransfer++;
				if(status == 'Pending')
					numTransferPending++;
			}
			if (type == 'Share')
			{
				numShare++;
				if(status == 'Pending')
					numSharePending++;
			}


		}

	}

	public class PLANiTPreparationsWrapper {

		public List<BI_PL_Preparation__c> preparations {get; set;}
		public String cycle { get; set;}
		public Set<SelectOption> hierarchyOptions {get; set;}
		public String linkedHierarchy {get; set;}

		public PLANiTPreparationsWrapper() {
			preparations = new List<BI_PL_Preparation__c>();
			hierarchyOptions = new Set<SelectOption>();
		}

		public PLANiTPreparationsWrapper (List<BI_PL_Preparation__c> preparations, String cycle, Set<SelectOption> hierarchyOptions, String linkedHierarchy) {
			this.preparations = preparations;
			this.cycle = cycle;
			this.hierarchyOptions = hierarchyOptions;
			this.linkedHierarchy = linkedHierarchy;
		}

	}

	public class PLANiTHierarchyNodesWrapper {

		public Map<String, PLANiTTreeNode> hierarchy {get; set;}
		public List<String> rootNodeId {get; set;}

		public PLANiTHierarchyNodesWrapper(Map<String, PLANiTTreeNode> hierarchy, List<String> rootNodeId) {
			this.rootNodeId = rootNodeId;
			this.hierarchy = hierarchy;
		}
	}

	public class PLANiTTreeNode {
		public Id repId;
		public String repName;

		public String positionName;

		public Id positionId;
		public Id parentPositionId;

		public List<String> childrenIds = new List<String>();

		public Boolean isCurrentUsersNode = false;
		public Boolean isConfirmed;
		public String rejectedReason;

		public Id positionCycleId;
		public Integer level;

		public PLANiTTreeNode (BI_PL_PositionCycleWrapper pcw) {
			
			if (pcw.positionCycleUsersExclusiveNode.size() > 0) {
				System.debug('Has exclusive Node');
				this.repId = pcw.positionCycleUsersExclusiveNode.get(0).BI_PL_User__c;
				this.repName = pcw.positionCycleUsersExclusiveNode.get(0).BI_PL_User__r.Name;
			}


			for (BI_PL_Position_cycle_user__c pcu : pcw.positionCycleUsersExclusiveNode) {
				if (pcu.BI_PL_User__c == UserInfo.getUserId()) {
					isCurrentUsersNode = true;
					break;
				}
			}
			this.positionCycleId = pcw.positionCycleId;
			this.positionName = pcw.position.record.Name;
			this.positionId = pcw.position.recordId;
			this.parentPositionId = pcw.parentPosition.recordId;
			this.isConfirmed = pcw.isConfirmed;
			this.rejectedReason = pcw.rejectedReason;
			this.level = pcw.level;

			for (BI_PL_PositionCycleWrapper child : pcw.getChildren())
				this.childrenIds.add(child.position.recordId);
		}
	}
}