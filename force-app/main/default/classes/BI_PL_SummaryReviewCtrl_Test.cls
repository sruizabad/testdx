@isTest
private class BI_PL_SummaryReviewCtrl_Test {

    private static String userCountryCode;
    private static String hierarchy = 'hierarchyTest';
    private static List<BI_PL_Position_Cycle__c> posCycles;
    public static List<Product_vod__c> listProd;

    @testSetup  static void setup() {
        
    }

    @isTest static void test() {

        User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        userCountryCode = testUser.Country_Code_BI__c;
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);

        BI_PL_SummaryReviewCtrl exte = new BI_PL_SummaryReviewCtrl();
        BI_PL_SummaryReviewCtrl.getDefaultOwnerId();
       
        Date startDate = Date.newInstance(2017, 2, 17);
        Date endDate = startDate.addDays(30);

        List<Account> listAcc = new List<Account>([SELECT id, External_Id_vod__c FROM Account LIMIT 250]);
        listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];

        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);


        BI_PL_SummaryReviewCtrl.PlanitSetupModel setup = BI_PL_SummaryReviewCtrl.getSetupData();
        setup.cycles = [SELECT id FROM BI_PL_CYCLE__c];
        BI_PL_Cycle__c[] futureCycle = new List<BI_PL_Cycle__c>([SELECT Id FROM BI_PL_Cycle__c WHERE BI_PL_Start_date__c > TODAY
                AND BI_PL_Country_code__c = :testUser.Country_Code_BI__c ORDER BY BI_PL_Start_date__c LIMIT 1]);
        if(futureCycle.size() > 0)
            setup.futureCycle = futureCycle.get(0);
        String currentCycleId = [SELECT id FROM BI_PL_CYCLE__c LIMIT 1].Id;
        

        Test.startTest();
      
        BI_PL_SummaryReviewCtrl.PLANiTHierarchyNodesWrapper hiherachynodes = BI_PL_SummaryReviewCtrl.getHierarchyNodes(currentCycleId,hierarchy);
        System.debug('********* hyerarchyNodesWrapper: ' + hiherachynodes);
        BI_PL_SummaryReviewCtrl.PLANiTPreparationsWrapper preparationWrap = BI_PL_SummaryReviewCtrl.getPreparations(currentCycleId, userCountryCode);
        System.debug('********* preparationsWrapper: ' + preparationWrap);
        List<BI_PL_Preparation__c> preparations = preparationWrap.preparations;
        System.debug('********* preparations: ' + preparations);

        List<String> preparationsId = new List<String>();
        for(BI_PL_Preparation__c prep : preparations)
        {
            preparationsId.add(prep.Id);
        }


        BI_PL_SummaryReviewCtrl.PLANiTAggregatedDataOutput aggregationData = BI_PL_SummaryReviewCtrl.getAggregatedDataForPreparations(preparationsId, '');
        System.debug('********* aggregationData: ' + aggregationData);

        Map<Id, BI_PL_Preparation__c> preparationsMap = new Map<Id, BI_PL_Preparation__c>(preparations);
        BI_PL_SummaryReviewCtrl.PlanitUnreviewedDetailOutput  unreviewedDetails = BI_PL_SummaryReviewCtrl.checkUnReviewedDetails(preparationsMap, 'rep_detail_only','');
        System.debug('********* unreviewedDetails: ' + unreviewedDetails);

        Boolean sumbitted = BI_PL_SummaryReviewCtrl.submitPreparation(preparationsId, 'rep_detail_only');
        System.debug('********* approvePreparation: ' + sumbitted);

        String approved = BI_PL_SummaryReviewCtrl.approvePreparation(preparationsId,'rep_detail_only', '0');
        System.debug('********* approvePreparation: ' + approved);


        System.debug('***ID ' + preparations.get(0).Id);
        //String prepId = preparations.get(0).Id;
        //BI_PL_SummaryReviewCtrl.PLANiTAggregatedDataOutput datWrap = BI_PL_SummaryReviewCtrl.getAggregatedDataForPreparation(prepId, '');

        BI_PL_SummaryReviewCtrl.PLANiTTransferShareDataOutput datTransfShare = BI_PL_SummaryReviewCtrl.getTransferShareData(preparationsId, hierarchy);

        Boolean confirmPostionValue = BI_PL_SummaryReviewCtrl.confirmNode(preparations.get(0).BI_PL_Position_cycle__c,true,preparationsId, 'rep_detail_only');
        Boolean unconfirmPostionValue = BI_PL_SummaryReviewCtrl.unconfirmNode(preparations.get(0).BI_PL_Position_cycle__c,'reason',true,preparationsId,'rep_detail_only' );

        List<String> preparationsList = new List<String>();
        for(BI_PL_Preparation__c prep1 : preparations)
        {
            preparationsList.add(prep1.BI_PL_Position_cycle__c);
        }
        List<String> unconfirmPostionValueList = BI_PL_SummaryReviewCtrl.unconfirmNodeList(preparationsList, 'reason', true, preparationsId, 'rep_detail_only');

        //System.debug('********* data wrapper: ' + datWrap);
        

        List<String> positionCycleIds = new List<String>();
        for(BI_PL_Position_Cycle__c pc : posCycles){
            positionCycleIds.add(pc.Id);
        }

        Boolean confirmGroups = BI_PL_SummaryReviewCtrl.confirmGroupNodes(positionCycleIds, true, preparationsId, 'rep_detail_only');

        Map<String, Integer> pendintRequests = BI_PL_SummaryReviewCtrl.checkPendingRequests(preparationsId);

        BI_PL_SummaryReviewCtrl.PLANiTTransferShareData lost = new BI_PL_SummaryReviewCtrl.PLANiTTransferShareData();
        lost.setHierarchy('test');
        lost.add('Transfer','Pending');
        lost.add('Share','Pending');

        BI_PL_SummaryReviewCtrl.PLANiTPreparationsWrapper lost1 = new BI_PL_SummaryReviewCtrl.PLANiTPreparationsWrapper();

        Test.stopTest();
    
    }

}