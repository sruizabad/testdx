@isTest
private class BI_PL_SummarySalesCtrl_test{

    private static final String SAFFTYPESALES = BI_PL_TestDataFactory.SAFFTYPESALES;
    private static final String SAFFTYPEINFO = BI_PL_TestDataFactory.SAFFTYPEINFO;

    private static String userCountryCode;
    private static String hierarchy = BI_PL_TestDataFactory.hierarchy;

    public static List<BI_PL_Position_Cycle__c> posCycles;
    public static BI_PL_Cycle__c cycle;

    @testSetup  static void setup() {
        
        BI_PL_SummarySalesCtrl controller = new BI_PL_SummarySalesCtrl();
        userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);

        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        cycle = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];
        posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

        BI_PL_TestDataFactory.createAffiliationSI(SAFFTYPESALES, listAcc, listProd, userCountryCode);
        BI_PL_TestDataFactory.createAffiliationSI(SAFFTYPEINFO, listAcc, listProd, userCountryCode);
    }

    /*@isTest static void test_withsharing(){

        map<String, List<BI_PL_Affiliation__c> > mapAffilitions = new map<String, List<BI_PL_Affiliation__c> >();
        List<BI_PL_Affiliation__c> lstAffilitions = new List<BI_PL_Affiliation__c>();

        cycle = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];
        posCycles = [SELECT Id,BI_PL_External_Id__c,BI_PL_Position__c FROM BI_PL_Position_Cycle__c];

        system.debug('## posCycles: ' + posCycles);

        List<BI_PL_Detail_preparation__c> lstChDetail = [SELECT Id, BI_PL_Primary_sales_affiliation__c FROM BI_PL_Detail_preparation__c];
        system.assert(lstChDetail.size()>0);
        
        for (BI_PL_Affiliation__c inAff : [SELECT Id,BI_PL_Type__c FROM BI_PL_Affiliation__c]){
            if (!mapAffilitions.containskey(inAff.BI_PL_Type__c)) mapAffilitions.put(inAff.BI_PL_Type__c, new List<BI_PL_Affiliation__c>() );
            
            lstAffilitions = mapAffilitions.get(inAff.BI_PL_Type__c);
            lstAffilitions.add(inAff);
            mapAffilitions.put(inAff.BI_PL_Type__c, lstAffilitions );
        }
        
        system.assert(!mapAffilitions.isEmpty() );
        system.assert( mapAffilitions.containskey(SAFFTYPESALES) );

        for (BI_PL_Detail_preparation__c inChDetail : lstChDetail){
            
            inChDetail.BI_PL_Primary_sales_affiliation__c = mapAffilitions.get(SAFFTYPESALES).get(0).Id;
        }
        update lstChDetail;
        
        Test.startTest();

        // this method was deleted
        Map<String, BI_PL_SummarySalesCtrl.SummarySalesModel> mapRes = BI_PL_SummarySalesCtrl.getSummarySalesByProduct(cycle.Id, hierarchy,  'rep_detail_only',  posCycles.get(0).BI_PL_Position__c);

        Test.stopTest();

        system.assertNotEquals(mapRes, null);

        system.debug('## mapRes: ' + mapRes);
    }*/

    @isTest static void test(){

        BI_PL_SummarySalesCtrl controller = new BI_PL_SummarySalesCtrl();
        
        list<BI_PL_Preparation__c> preps = [SELECT Id FROM BI_PL_Preparation__c];

        List<String> accountIds = new List<String>();
        for(Account i : [SELECT Id, External_ID_vod__c FROM Account]){
                accountIds.add(i.Id);
        }
        system.assert(!accountIds.isEmpty());

        List<String> productIds = new List<String>();
        for(Product_vod__c i : [SELECT Id, External_ID_vod__c FROM Product_vod__c]){
                productIds.add(i.Id);
        }
        system.assert(!productIds.isEmpty());

        BI_PL_SummarySalesCtrl.PlanitDataWrapper oPlanitData = BI_PL_SummarySalesCtrL.getTargets(preps, accountIds, '');
        system.assert(oPlanitData != null);

        BI_PL_SummarySalesCtrl.AffiliationDataWrapper oAffilData = BI_PL_SummarySalesCtrL.getPlanitAffiliations(productIds,accountIds,'');
        system.assert(oAffilData != null);
    }

}