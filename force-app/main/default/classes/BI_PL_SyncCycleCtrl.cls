/**
 *  Retrieves the necesary data for the BI_PL_SyncCyle component.
 *
 *  @author OMEGA CRM
 */
public without sharing class BI_PL_SyncCycleCtrl {

    /*@RemoteAction
    public static Boolean getAutoNameCallPlans() {
        String userCountryCode = [SELECT Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
        return [SELECT BI_PL_Synchronize_autoname__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = : userCountryCode LIMIT 1].BI_PL_Synchronize_autoname__c;
    }*/

    /**
     *  Loads the cycle options for the picklist.
     *  @author OMEGA CRM
     */
    /*@RemoteAction
    public static Map<Id, BI_PL_Cycle__c> getCycles() {
        User currentUser = [SELECT Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

        return new Map<Id, BI_PL_Cycle__c>([SELECT Id, Name, BI_PL_Field_force__r.Name, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = :currentUser.Country_Code_BI__c AND BI_PL_Type__c = 'active' ORDER BY Name]);
    }

    @RemoteAction
    public static Set<String> getHierarchies(String cycleId) {
        Set<String> hierarchies = new Set<String>();
        for (BI_PL_Position_cycle__c c : [SELECT BI_PL_Hierarchy__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = :cycleId ORDER BY BI_PL_Hierarchy__c]) {
            if (!hierarchies.contains(c.BI_PL_Hierarchy__c)) {
                hierarchies.add(c.BI_PL_Hierarchy__c);
            }
        }
        return hierarchies;
    }*/

    /**
     * Retrieves the preparations to be shown according to the selected hierarchy and cycle.
     * @author OmegaCRM
     */
    @RemoteAction
    public static List<BI_PL_Preparation__c> loadPreparations(List<String> selectedHierarchies, String selectedCycle) {
        //String userCountryCode = [SELECT Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
        List<BI_PL_Preparation__c> lstToShowPreparations = [SELECT Id, Name, BI_PL_Country_code__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Type__c, BI_PL_Position_name__c, BI_PL_Status__c, BI_PL_Start_date__c, BI_PL_End_date__c, OwnerId, Owner.Name,
                                   BI_PL_position_cycle__r.BI_PL_hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name,
                                   BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                                   BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c, BI_PL_Position_cycle__r.BI_PL_Synchronized__c, BI_PL_position_cycle__r.BI_PL_Cycle__r.BI_PL_External_Id__c
                                   FROM BI_PL_Preparation__c
                                   WHERE /*BI_PL_Country_code__c = : userCountryCode AND*/ BI_PL_position_cycle__r.BI_PL_hierarchy__c IN :selectedHierarchies
                                                   AND BI_PL_position_cycle__r.BI_PL_Cycle__c = : selectedCycle];
        return lstToShowPreparations;
    }

    @RemoteAction
    public static Map<String, List<BI_PL_Preparation__c>> loadAllPreparations (String cycle) {
        //String userCountryCode = [SELECT Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
        Map<String, List<BI_PL_Preparation__c>> prep = new Map<String, List<BI_PL_Preparation__c>>();

        for (BI_PL_Preparation__c p : [SELECT Id, Name, BI_PL_Country_code__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Type__c, BI_PL_Position_name__c, BI_PL_Status__c, BI_PL_Start_date__c, BI_PL_End_date__c, OwnerId, Owner.Name,
                                       BI_PL_position_cycle__r.BI_PL_hierarchy__c, BI_PL_Position_cycle__r.BI_PL_Cycle__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name,
                                       BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force__r.Name, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
                                       BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c, BI_PL_Position_cycle__r.BI_PL_Synchronized__c, BI_PL_position_cycle__r.BI_PL_Cycle__r.BI_PL_External_Id__c
                                       FROM BI_PL_Preparation__c
                                       WHERE /*BI_PL_Country_code__c = : userCountryCode AND*/ BI_PL_position_cycle__r.BI_PL_Cycle__c = : cycle]) {
            if (prep.containsKey(p.BI_PL_position_cycle__r.BI_PL_hierarchy__c)) {
                prep.get(p.BI_PL_position_cycle__r.BI_PL_hierarchy__c).add(p);
            } else {
                prep.put(p.BI_PL_position_cycle__r.BI_PL_hierarchy__c, new List<BI_PL_Preparation__c>());
                prep.get(p.BI_PL_position_cycle__r.BI_PL_hierarchy__c).add(p);
            }
        }
        return prep;
    }

    @RemoteAction
    public static Boolean syncPreparations (String preparationName, List<BI_PL_Preparation__c> preparations, String countryCode) {
        Boolean canSynchronize = false;

        List<String> preparationsToSyncronize = new List<String>();
        for (BI_PL_Preparation__c prep : preparations) {
            preparationsToSyncronize.add(prep.Id);
        }

        Id currentUserId = UserInfo.getUserId();
        try {
            //String userCountryCode = [SELECT Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;

            BI_PL_Country_settings__c countrySettings = [SELECT BI_PL_Channel__c, BI_PL_Multichannel__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = : countryCode LIMIT 1];
            //If there are preparations to syncronize and all are approved.
            if (preparationsToSyncronize.size() > 0 && countrySettings != null) {
                system.debug('## synchronize process started!!');
                canSynchronize = true;
                if (!countrySettings.BI_PL_Multichannel__c) {
                    BI_PL_SynchronizeSAPDataBatch syncDataBatch = new BI_PL_SynchronizeSAPDataBatch(preparationsToSyncronize, preparationName, countrySettings.BI_PL_Channel__c);
                    Id batchJobId = Database.executeBatch(syncDataBatch, 1);
                } else {
                    BI_PL_SynchronizeMCCPDataBatch syncDataBatch = new BI_PL_SynchronizeMCCPDataBatch(preparationsToSyncronize);
                    Id batchJobId = Database.executeBatch(syncDataBatch, 1);
                }
            }
        } catch (Exception e) {
            System.debug('***Exception = ' + e.getMessage());
        }
        return canSynchronize;
    }
}