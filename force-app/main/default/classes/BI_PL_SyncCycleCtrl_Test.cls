@isTest
private class BI_PL_SyncCycleCtrl_Test {

	private static String channel = 'rep_detail_only';
	
	private static Account account1;
	private static Account account2;
	private static Account account3;
	private static Account account4;
	private static Account account5;
	
	/*@isTest public static void test_method_one() {
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		String countryCode = testUser.Country_Code_BI__c;
		BI_PL_TestDataFactory.createCycleStructure(countryCode);
		String hierarchy = BI_PL_TestDataFactory.hierarchy;
		List<String> listah = new List<String>();
		listah.add(hierarchy);
		Test.startTest();
		//BI_PL_SyncCycleCtrl.getCycles();
		BI_PL_Cycle__c cycle1 = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];
		//BI_PL_SyncCycleCtrl.getHierarchies(cycle1.Id);
		
		BI_PL_SyncCycleCtrl.loadPreparations(listah, cycle1.Id);
		BI_PL_SyncCycleCtrl.loadAllPreparations(cycle1.Id);
		List<BI_PL_Preparation__c> prepList = [SELECT Id FROM BI_PL_Preparation__c LIMIT 2];
		BI_PL_SyncCycleCtrl.syncPreparations(hierarchy, prepList, 'US');
		Test.stopTest();
	}*/

	@isTest public static void test_method_two(){
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = testUser.Country_Code_BI__c;
		String hierarchy = BI_PL_TestDataFactory.hierarchy;
		List<String> listah = new List<String>();
		listah.add(hierarchy);
		
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySettingMultiChannel();
		//BI_PL_TestDataFactory.createCountrySetting();

		String sapName = 'TestSAPName';
		String hierarchy1 = 'testHierarchy';
		listah.add(hierarchy1);

		account1 = new Account(Name = 'Account1', External_ID_vod__c = 'testExtAccount1');
		account2 = new Account(Name = 'Account2', External_ID_vod__c = 'testExtAccount2');
		account3 = new Account(Name = 'Account3', External_ID_vod__c = 'testExtAccount3');
		account4 = new Account(Name = 'Account4', External_ID_vod__c = 'testExtAccount4');
		account5 = new Account(Name = 'Account5', External_ID_vod__c = 'testExtAccount5');

		insert new List<Account> {account1, account2, account3, account4, account5};

		BI_PL_Business_rule__c br = new BI_PL_Business_rule__c(BI_PL_Visits_per_day__c = 20,
		        BI_PL_Country_code__c = userCountryCode,
		        BI_PL_Type__c = BI_PL_PreparationUtility.THRESHOLD_RULE_TYPE,
		        BI_PL_Active__c = true);

		insert br;

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'KAM', BI_TM_Country_Code__c =userCountryCode);
		insert fieldForce;

		BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = userCountryCode,
		        BI_PL_Start_date__c = Date.newInstance(2018, 1, 1),
		        BI_PL_End_date__c = Date.newInstance(2018, 1, 31),
		        BI_PL_Field_force__c = fieldForce.Id);

		insert cycle;

		BI_PL_Position__c position1 = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = userCountryCode);
		BI_PL_Position__c position2 = new BI_PL_Position__c(Name = 'Test2', BI_PL_Country_code__c = userCountryCode);

		insert new List<BI_PL_Position__c> {position1, position2};

		BI_PL_Position_cycle__c positionCycle1 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position1.Id,
		        BI_PL_External_id__c = 'test1', BI_PL_Hierarchy__c = hierarchy);
		BI_PL_Position_cycle__c positionCycle2 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position2.Id,
		        BI_PL_External_id__c = 'test2', BI_PL_Hierarchy__c = hierarchy);

		insert new List<BI_PL_Position_cycle__c> {positionCycle1, positionCycle2};
		
		BI_PL_Preparation__c preparation = new BI_PL_Preparation__c(
		    BI_PL_Country_code__c = userCountryCode,
		    BI_PL_External_id__c = 'TestExt1',
		    BI_PL_Position_cycle__c = positionCycle1.Id);
		insert preparation;

		List<BI_PL_Preparation__c> prepList = new List<BI_PL_Preparation__c>();
		prepList.add(preparation);

		Test.startTest();
		BI_PL_SyncCycleCtrl.loadPreparations(listah, cycle.Id);
		BI_PL_SyncCycleCtrl.loadAllPreparations(cycle.Id);
		BI_PL_SyncCycleCtrl.syncPreparations(hierarchy1, prepList,'US');
		Test.stopTest();
	}
}