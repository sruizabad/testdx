@isTest
public with sharing class BI_PL_SynchronizeMCCPDataBatch_Test {
    private static String userCountryCode;
    private static List<BI_PL_Position_Cycle__c> posCycles;
    private static String hierarchy = 'hierarchyTest';

    @testSetup  static void setup() {
       
        userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
    


    }

    @isTest
    public static void test() {

        List<BI_PL_Preparation__c> preparationsToSync = [SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Preparation__c];
        List<String> prepIds = new List<String>();
        for(BI_PL_Preparation__c prep : preparationsToSync)
        {
            prepIds.add(prep.Id);
        }

       

        MC_Cycle_vod__c mccycle = new MC_Cycle_vod__c(
            External_ID_vod__c = 'Default_Test',
            Start_Date_vod__c = preparationsToSync[0].BI_PL_Start_date__c,
            End_Date_vod__c = preparationsToSync[0].BI_PL_End_date__c,
            Country_Code_BI__c = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c);


        //insert mccycle;

        //insert new BI_PL_Mccp__c(Name='rep_detail_only',BI_PL_Mccp_channel__c='Call2_vod__c');
        //insert new BI_PL_Mccp__c(Name='rep_clm',BI_PL_Mccp_channel__c='Call2_vod');

        Test.startTest();
        Database.executeBatch(new BI_PL_SynchronizeMCCPDataBatch(prepIds));
        Test.stopTest();
    }

}