/**
 *  26/07/2017
 *  - Sync algorythm bug.
 *  04/07/2017
 *  - GLOS-342: Do_Not_Call_vod__c (take into account null values)
 *  @author OMEGA CRM
 */
global class BI_PL_SynchronizeSAPDataBatch implements Database.Batchable<sObject>, Database.Stateful {

    global Set<Id> preparationIds = new Set<Id>();
    global Set<Id> synchronizedPrepsIds = new Set<Id>();
    global List<BI_PL_Preparation__c> lTotalPrepsToSync = new List<BI_PL_Preparation__c>();
    global Set<Id> setPositionCycleId = new Set<Id>();
    global String sapName = '';
    global String channel = '';



    global BI_PL_SynchronizeSAPDataBatch(List<String> preparationsToSyncronize, String sapName, String channel) {
        for (String prep : preparationsToSyncronize) {
            preparationIds.add(prep);
        }
        this.sapName = sapName;
        this.channel = channel;

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name, BI_PL_Country_code__c,
                                         BI_PL_Position_name__c, BI_PL_Start_date__c,
                                         BI_PL_End_date__c, OwnerId, BI_PL_External_id__c, BI_PL_Position_cycle__c
                                         FROM BI_PL_Preparation__c WHERE Id in :preparationIds]);
    }

    global void execute(Database.BatchableContext BC, List<BI_PL_Preparation__c> lPreparationsToSync) {
        System.debug('***EXECUTE');

        synchronizePreparations(lPreparationsToSync);
    }

    global void finish(Database.BatchableContext BC) {
        //system.debug('%%% FINISHED');
        // system.debug('%%% synchronizedPrepsIds.size(): ' + synchronizedPrepsIds.size());
        //system.debug('%%% preparationIds.size(): ' + preparationIds.size());
        if (synchronizedPrepsIds.size() < preparationIds.size()) {
            list<BI_PL_Preparation__c> remainingPrepsToSync = new list<BI_PL_Preparation__c>();
            // system.debug('%%% lTotalPrepsToSync: ' + lTotalPrepsToSync);
            for (BI_PL_Preparation__c SAP : lTotalPrepsToSync) {
                //system.debug('%%% SAP.Id: ' + SAP.Id);
                if (!synchronizedPrepsIds.contains(SAP.Id)) {
                    remainingPrepsToSync.add(SAP);
                }
            }
            //system.debug('%%% remainingPrepsToSync: ' + remainingPrepsToSync);
            //system.debug('%%% remainingPrepsToSync.size(): ' + remainingPrepsToSync.size());

        } else {

            if (synchronizedPrepsIds.size() > 0) {

                List<BI_PL_Position_cycle__c> lstPositionCycle = [Select Id, BI_PL_Synchronized__c FROM BI_PL_Position_cycle__c WHERE Id IN: setPositionCycleId];

                for (BI_PL_Position_cycle__c positionCycle : lstPositionCycle) {
                    positionCycle.BI_PL_Synchronized__c = true;
                }

                Database.update(lstPositionCycle, false);
                try {
                    BI_PL_EmailUtility.sendEmailWithTemplate(new list<Id>(synchronizedPrepsIds)[0], 'SAP Synchronize Notification', new list<String> {UserInfo.getUserEmail()});
                } catch (Exception e) {
                    System.debug(e.getMessage());
                }
            }
        }
    }

    /**
     * Method to Synchronize a list of PLANiT Preparations to SAP
     * @param lPreparationsToSync Preparations to be synchronized
     */

    public void synchronizePreparations(List<BI_PL_Preparation__c> lPreparationsToSync) {
        list<Cycle_Plan_vod__c> saps = new list<Cycle_Plan_vod__c>();
        list<Cycle_Plan_Target_vod__c> targets = new list<Cycle_Plan_Target_vod__c>();
        list<Cycle_Plan_Detail_vod__c> details = new list<Cycle_Plan_Detail_vod__c>();

        map<String, list<Cycle_Plan_Target_vod__c>> sapsToTargetsMap = new map<String, list<Cycle_Plan_Target_vod__c>>();
        map<String, list<Cycle_Plan_Detail_vod__c>> targetsToDetailsMap = new map<String, list<Cycle_Plan_Detail_vod__c>>();



        Map<Id, BI_PL_Target_preparation__c> mapALLTargets = new Map<Id, BI_PL_Target_preparation__c>([SELECT Id,
                BI_PL_Target_customer__c,
                BI_PL_Target_customer__r.External_ID_vod__c,
                BI_PL_Added_manually__c,
                BI_PL_Country_code__c,
                BI_PL_Target_customer__r.Do_Not_Call_vod__c, BI_PL_Header__r.BI_PL_External_id__c,
                BI_PL_External_Id__c,
                BI_PL_Next_best_account__c,
                BI_PL_No_see_list__c
                FROM BI_PL_Target_preparation__c
                WHERE BI_PL_Header__c IN :lPreparationsToSync]);

        Map<String, Map<Id, BI_PL_Target_preparation__c>> prepToTargetsMap = new Map<String, Map<Id, BI_PL_Target_preparation__c>>();
        Map<String, String> targetToPrepExtIds = new Map<String, String>();

        for (Id targetId : mapALLTargets.keySet()) {
            if (!prepToTargetsMap.keySet().contains(mapALLTargets.get(targetId).BI_PL_Header__r.BI_PL_External_id__c)) {
                Map<Id, BI_PL_Target_preparation__c> newMap = new Map<Id, BI_PL_Target_preparation__c>();
                newMap.put(targetId, mapALLTargets.get(targetId));
                prepToTargetsMap.put(mapALLTargets.get(targetId).BI_PL_Header__r.BI_PL_External_id__c, newMap);

            } else {
                prepToTargetsMap.get(mapALLTargets.get(targetId).BI_PL_Header__r.BI_PL_External_id__c).put(targetId, mapALLTargets.get(targetId));
            }
            targetToPrepExtIds.put(mapALLTargets.get(targetId).BI_PL_External_id__c, mapALLTargets.get(targetId).BI_PL_Header__r.BI_PL_External_id__c);
        }

        Map<String, List<BI_PL_Channel_detail_preparation__c>> prepToChannelDetailMap = new Map<String, List<BI_PL_Channel_detail_preparation__c>>();

        List<BI_PL_Channel_detail_preparation__c> lstALLChannelDetails = [SELECT Id, BI_PL_Rejected__c, BI_PL_Max_adjusted_interactions__c, BI_PL_Max_planned_interactions__c, BI_PL_Target__c, BI_PL_Target__r.BI_PL_External_id__c, BI_PL_External_Id__c, BI_PL_Reviewed__c, BI_PL_Edited__c, BI_PL_MSL_flag__c, BI_PL_Removed__c  FROM BI_PL_Channel_detail_preparation__c
                WHERE BI_PL_Target__c IN:mapALLTargets.keySet() AND BI_PL_Channel__c = : channel];

        // System.debug('%%lstALLChannelDetails: ' + lstALLChannelDetails);
        // System.debug('channel : ' + channel);
        Map<String, String> channelDetailToPrepExtIds = new Map<String, String>();

        for (BI_PL_Channel_detail_preparation__c cDetailPrep : lstALLChannelDetails) {
            String targetExtId = cDetailPrep.BI_PL_Target__r.BI_PL_External_id__c;
            String prepExtId = targetToPrepExtIds.get(targetExtId);

            if (!prepToChannelDetailMap.keySet().contains(prepExtId)) {
                List<BI_PL_Channel_detail_preparation__c> newList = new List<BI_PL_Channel_detail_preparation__c>();
                newList.add(cDetailPrep);
                prepToChannelDetailMap.put(prepExtId, newList);
            } else {
                prepToChannelDetailMap.get(prepExtId).add(cDetailPrep);
            }
            channelDetailToPrepExtIds.put(cDetailPrep.BI_PL_External_Id__c, prepExtId);
        }

        List<BI_PL_Detail_preparation__c> lstALLDetails = new List<BI_PL_Detail_preparation__c>();
        Map<String, List<BI_PL_Detail_preparation__c>> prepToDetailPrep = new Map<String, List<BI_PL_Detail_preparation__c>>();

        lstALLDetails = [SELECT Id, BI_PL_Product__c, BI_PL_Planned_details__c, BI_PL_Adjusted_details__c, BI_PL_Channel_detail__r.BI_PL_Target__c,
                         BI_PL_Channel_detail__r.BI_PL_Reviewed__c, BI_PL_Channel_detail__r.BI_PL_Rejected__c, BI_PL_Channel_detail__r.BI_PL_Edited__c,
                         BI_PL_Segment__c, BI_PL_Column__c, BI_PL_Row__c, BI_PL_Added_manually__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,
                         BI_PL_Product__r.External_ID_vod__c, BI_PL_Country_code__c, BI_PL_Strategic_segment__c, BI_PL_Channel_detail__r.BI_PL_Removed__c,
                         BI_PL_Channel_detail__r.BI_PL_External_id__c, BI_PL_Secondary_product__c, BI_PL_Secondary_product__r.External_ID_vod__c,
                         BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c,
                         BI_PL_Channel_detail__r.BI_PL_MSL_flag__c, BI_PL_Secondary_strategic_segment__c, BI_PL_Secondary_segment__c
                         FROM BI_PL_Detail_preparation__c
                         WHERE BI_PL_Channel_detail__r.BI_PL_Target__c in :mapALLTargets.keySet()
                         AND BI_PL_Channel_detail__r.BI_PL_Channel__c = : channel AND BI_PL_Channel_detail__r.BI_PL_MSL_flag__c = true];

        //System.debug('%% lstALLDetails: ' + lstALLDetails);
        for (BI_PL_Detail_preparation__c detailPrep : lstALLDetails) {
            String channelExtId = detailPrep.BI_PL_Channel_detail__r.BI_PL_External_id__c;
            String prepExtId = channelDetailToPrepExtIds.get(channelExtId);

            if (!prepToDetailPrep.keySet().contains(prepExtId)) {
                List<BI_PL_Detail_preparation__c> newList = new List<BI_PL_Detail_preparation__c>();
                newList.add(detailPrep);
                prepToDetailPrep.put(prepExtId, newList);
            } else {
                prepToDetailPrep.get(prepExtId).add(detailPrep);
            }
        }


        for (BI_PL_Preparation__c prep : lPreparationsToSync) {
            // system.debug('%%% SYNC SAP');
            synchronizeSinglePrep(prep, saps, sapsToTargetsMap, targetsToDetailsMap, prepToTargetsMap, prepToChannelDetailMap, prepToDetailPrep);

        }

        // UPSERT SAPS
        System.debug('***Saps = ' + saps);
        Schema.SObjectField fieldCP = Cycle_Plan_vod__c.External_ID2__c;
        Database.UpsertResult[] sapsResults = Database.upsert(saps, fieldCP, false);
        for (Database.UpsertResult result : sapsResults) {
            if (!result.isSuccess()) {
                system.debug('%%% SAP ERROR: ' + result.getErrors());
            }
        }

        // Set 'Cycle_Plan_vod__c' field for each target
        // System.debug('***sapsToTargetsMap = ' + sapsToTargetsMap);
        for (Cycle_Plan_vod__c sap : SAPs) {
            if (sap.Id != null) {
                for (Cycle_Plan_Target_vod__c target : sapsToTargetsMap.get(sap.External_ID2__c)) {
                    target.Cycle_Plan_vod__c = sap.Id;
                    targets.add(target);
                }
            }
        }


        // UPERT TARGETS
        Schema.SObjectField fieldT = Cycle_Plan_Target_vod__c.External_ID2__c;
        Database.UpsertResult[] targetsResults = Database.upsert(targets, fieldT, false);
        for (Database.UpsertResult result : targetsResults) {
            if (!result.isSuccess()) {
                system.debug('%%% target ERROR: ' + result.getErrors());
            }
        }

        // Set 'Cycle_Plan_Target_vod__c' field for each detail
        for (Cycle_Plan_Target_vod__c target : targets) {
            if (target.Id != null) {
                //system.debug('%%% target.External_ID2__c: ' + target.External_ID2__c);
                //system.debug('%%% targetsToDetailsMap.get(): ' + targetsToDetailsMap.get(target.External_ID2__c));
                if (targetsToDetailsMap.get(target.External_ID2__c) != null) {
                    for (Cycle_Plan_Detail_vod__c detail : targetsToDetailsMap.get(target.External_ID2__c)) {
                        detail.Cycle_Plan_Target_vod__c = target.Id;
                        details.add(detail);
                    }
                }
            }
        }



        Set<Cycle_Plan_Detail_vod__c> detailsSet = new Set<Cycle_Plan_Detail_vod__c>(details);
        List<Cycle_Plan_Detail_vod__c> detailsList = new List<Cycle_Plan_Detail_vod__c>(detailsSet);

        /*for(Cycle_Plan_Detail_vod__c cpd : detailsList){
            System.debug('//// ' + cpd.External_ID__c);
        }*/
        //System.debug('///MapCPD: ' + mapCPD);
        // INSERT DETAILS
        Schema.SObjectField fieldD = Cycle_Plan_Detail_vod__c.External_ID__c;
        Database.UpsertResult[] detailsResults = Database.upsert(detailsList, fieldD, false);
        for (Database.UpsertResult result : detailsResults) {
            if (!result.isSuccess()) {
                system.debug('%%% detail ERROR: ' + result.getErrors());
            }
        }
    }
    /**
     * Method to fill all maps for a single preparation
     * @param preparation         Preparation to be synchronized
     * @param saps                Cycle Plans to be inserted
     * @param sapsToTargetsMap    Cycle Targets to be inserted and its Cycle Plan
     * @param targetsToDetailsMap Cycle Detail to be inserted and its Cycle Target
     * @author OmegaCRM
     */
    public void synchronizeSinglePrep(BI_PL_Preparation__c preparation,
                                      list<Cycle_Plan_vod__c> saps,
                                      map<String, list<Cycle_Plan_Target_vod__c>> sapsToTargetsMap,
                                      map<String, list<Cycle_Plan_Detail_vod__c>> targetsToDetailsMap,
                                      Map<String, Map<Id, BI_PL_Target_preparation__c>> prepToTargetsMap,
                                      Map<String, List<BI_PL_Channel_detail_preparation__c>> prepToChannelDetailMap,
                                      Map<String, List<BI_PL_Detail_preparation__c>> prepToDetailPrep) {
        //system.debug('%%% createSAP STARTED for ' + preparation.Id);
        synchronizedPrepsIds.add(preparation.Id);

        try {

            /* SAP Synchronization */

            String auxName = '';
            // Default name: Cycle Name + Position_name + start_date + end_date
            //auxName = preparation.BI_PL_Position_name__c + '_' + preparation.BI_PL_Start_date__c.format();
            auxName = preparation.BI_PL_Position_cycle__r.BI_PL_Cycle__r.Name + '_' + preparation.BI_PL_Position_name__c
                      + '_' + preparation.BI_PL_Start_date__c.format() + '_' + preparation.BI_PL_End_date__c.format();
            // system.debug('%%% auxName: ' + auxName);
            setPositionCycleId.add(preparation.BI_PL_Position_cycle__c);

            Date startDateVod = preparation.BI_PL_Start_date__c;
            startDateVod.format();
            Date endDateVod = preparation.BI_PL_End_date__c;
            endDateVod.format();
            String territory = preparation.BI_PL_Position_name__c;

            //IF 0 is the Adjusted value then don't take the line
            //Create the CP, CP target, CP detail based on the Adjusted fields
            Cycle_Plan_vod__c cp = new Cycle_Plan_vod__c(Name = (String.isNotBlank(SAPName)) ? SAPName : auxName,
                    Start_Date_vod__c = startDateVod,
                    End_Date_vod__c = endDateVod,
                    Territory_vod__c = territory,
                    Status_vod__c = 'Approved_vod',
                    OwnerId = preparation.OwnerId,
                    Country_Code_BI__c = preparation.BI_PL_Country_code__c
                                                        );

            String startDay = (preparation.BI_PL_Start_date__c.day() < 10) ? ('0' + preparation.BI_PL_Start_date__c.day()) : ('' + preparation.BI_PL_Start_date__c.day());
            String startMonth = (preparation.BI_PL_Start_date__c.month() < 10) ? ('0' + preparation.BI_PL_Start_date__c.month()) : ('' + preparation.BI_PL_Start_date__c.month());

            String endDay = (preparation.BI_PL_End_date__c.day() < 10) ? ('0' + preparation.BI_PL_End_date__c.day()) : ('' + preparation.BI_PL_End_date__c.day());
            String endMonth = (preparation.BI_PL_End_date__c.month() < 10) ? ('0' + preparation.BI_PL_End_date__c.month()) : ('' + preparation.BI_PL_End_date__c.month());

            String startDate = '' + preparation.BI_PL_Start_date__c.year() + startMonth + startDay;
            String endDate = '' + preparation.BI_PL_End_date__c.year() + endMonth + endDay;

            cp.External_ID2__c = preparation.BI_PL_Country_code__c + '-' + startDate + '-' + endDate + '-' + preparation.BI_PL_Position_name__c;

            saps.add(cp);

            /* Targets Synchronization */

            //go through target preps
            map<Id, Cycle_Plan_Target_vod__c> cptMap = new map<Id, Cycle_Plan_Target_vod__c>();
            map<Id, BI_PL_Target_preparation__c> targetsMap = new map<Id, BI_PL_Target_preparation__c>();

            Map<Id, BI_PL_Target_preparation__c> mapTargets = new Map<Id, BI_PL_Target_preparation__c>();
            if (prepToTargetsMap.keySet().contains(preparation.BI_PL_External_Id__c)) {
                mapTargets = prepToTargetsMap.get(preparation.BI_PL_External_Id__c);
            }

            List<BI_PL_Channel_detail_preparation__c> lstDetails = new List<BI_PL_Channel_detail_preparation__c>();
            // System.debug('% prepToChannelDetailMap '+ prepToChannelDetailMap);
            if (prepToChannelDetailMap.keySet().contains(preparation.BI_PL_External_Id__c)) {
                lstDetails = prepToChannelDetailMap.get(preparation.BI_PL_External_Id__c);
            }

            Map<Id, BI_PL_Channel_detail_preparation__c> mapChannelDetails = new Map<Id, BI_PL_Channel_detail_preparation__c>();
            //System.debug('%%lstDetails ' + lstDetails);

            for (BI_PL_Channel_detail_preparation__c cdp : lstDetails) {
                mapChannelDetails.put(mapTargets.get(cdp.BI_PL_Target__c).Id, cdp);


            }



            for (BI_PL_Target_preparation__c tp : mapTargets.values() ) {

                //system.debug('%%% TP1: ' + tp);
                BI_PL_Channel_detail_preparation__c channelDetail = new BI_PL_Channel_detail_preparation__c();
                // System.debug('mapChannelDetails ' + mapChannelDetails + 'tpID ' + tp.Id);
                if (mapChannelDetails.keySet().contains(tp.Id)) {
                    channelDetail = mapChannelDetails.get(tp.Id);

                    Decimal maxPlanned = (channelDetail.BI_PL_Max_planned_interactions__c != null) ? channelDetail.BI_PL_Max_planned_interactions__c : 0;
                    Decimal maxAdjusted = (channelDetail.BI_PL_Max_adjusted_interactions__c != null) ? channelDetail.BI_PL_Max_adjusted_interactions__c : 0;

                    Decimal planned = getToVeevaAddValue(channelDetail.BI_PL_Reviewed__c, channelDetail.BI_PL_Removed__c, channelDetail.BI_PL_Rejected__c, channelDetail.BI_PL_Edited__c, tp.BI_PL_Added_manually__c, tp.BI_PL_Next_best_account__c, tp.BI_PL_No_see_list__c, maxPlanned, maxAdjusted);
                    // System.debug('planned ' + planned);
                    Boolean syncIt = (planned != -1 && channelDetail.BI_PL_MSL_flag__c);
                    //  System.debug('%% syncIt ' + syncIt);
                    if (syncIt) {
                        Cycle_Plan_Target_vod__c cpt = new Cycle_Plan_Target_vod__c(
                            //Cycle_Plan_vod__c = CP.id,
                            Cycle_Plan_Account_vod__c = tp.BI_PL_Target_customer__c,
                            Planned_Calls_vod__c = planned,
                            Country_Code_BI__c = tp.BI_PL_Country_code__c
                        );

                        cpt.External_ID2__c = cp.External_ID2__c + '-' + tp.BI_PL_Target_customer__r.External_ID_vod__c;


                        // system.debug('%%% put new target: ' + cpt);
                        cptMap.put(tp.id, cpt);
                        targetsMap.put(tp.id, tp);
                    }
                }
            }

            //system.debug('%%% CPTs values: ' + cptMap.values());
            sapsToTargetsMap.put(cp.External_ID2__c, cptMap.values());
            //system.debug('%%% CPTs keys: ' + cptMap.keySet());
            // system.debug('%%% targetsMap: ' + targetsMap.keySet());


            /* Details Synchronization */

            list<Cycle_Plan_Detail_vod__c> cpds = new list<Cycle_Plan_Detail_vod__c>();
            set<String> externalIds = new set<String>();

            List<BI_PL_Detail_preparation__c> lstDetailPreps = new List<BI_PL_Detail_preparation__c>();

            if (prepToDetailPrep.containsKey(preparation.BI_PL_External_Id__c)) {
                lstDetailPreps = prepToDetailPrep.get(preparation.BI_PL_External_Id__c);
            }


            Map<String, Cycle_Plan_Detail_vod__c> mapCPD = new Map<String, Cycle_Plan_Detail_vod__c>();
            for (BI_PL_Detail_preparation__c dp : lstDetailPreps) {


                BI_PL_Target_preparation__c tp = targetsMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c);
                //system.debug('%%% dp.Target_Customer_BI__c.External_ID2__c: ' + targetsMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c));

                if (tp != null) {
                    Decimal plannedDetails = (dp.BI_PL_Planned_details__c != null) ? dp.BI_PL_Planned_details__c : 0;
                    Decimal adjustedDetails = (dp.BI_PL_Adjusted_details__c != null) ? dp.BI_PL_Adjusted_details__c : 0;

                    Decimal planned = getToVeevaAddValue(dp.BI_PL_Channel_detail__r.BI_PL_Reviewed__c, dp.BI_PL_Channel_detail__r.BI_PL_Removed__c, dp.BI_PL_Channel_detail__r.BI_PL_Rejected__c, dp.BI_PL_Channel_detail__r.BI_PL_Edited__c, tp.BI_PL_Added_manually__c, dp.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c, dp.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c, plannedDetails, adjustedDetails);

                    Boolean syncIt = (planned != -1 && dp.BI_PL_Channel_detail__r.BI_PL_MSL_flag__c);
                    String key = preparation.Id + '_' + dp.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c + '_' + dp.BI_PL_Product__c;
                    // This if is just in case...
                    if (syncIt) {
                        String potential = String.valueOf(dp.BI_PL_Row__c);
                        String intimacy = String.valueOf(dp.BI_PL_Column__c);
                        if (dp.BI_PL_Secondary_product__c == null) {

                            if (mapCPD.containsKey(key)) {
                                // System.debug('if no secondary');
                                mapCPD.get(key).Planned_Details_vod__c = mapCPD.get(key).Planned_Details_vod__c + planned;
                                mapCPD.get(key).zvod_PM_Segmentation_BI__c = dp.BI_PL_Segment__c;
                                mapCPD.get(key).zvod_PM_Potential_BI__c = potential;
                                mapCPD.get(key).zvod_PM_Intimacy_BI__c = intimacy;
                                mapCPD.get(key).Primary_Goal_BI__c = mapCPD.get(key).Primary_Goal_BI__c + planned;
                                mapCPD.get(key).zvod_PM_Strategic_Segment_BI__c = dp.BI_PL_Strategic_segment__c;

                            } else {
                                //System.debug('else no secondary');
                                Cycle_Plan_Detail_vod__c cpd = new Cycle_Plan_Detail_vod__c(
                                    Product_vod__c = dp.BI_PL_Product__c,
                                    Planned_Details_vod__c = planned,
                                    zvod_PM_Segmentation_BI__c = dp.BI_PL_Segment__c,
                                    zvod_PM_Potential_BI__c = potential,
                                    zvod_PM_Intimacy_BI__c = intimacy,
                                    Country_Code_BI__c = dp.BI_PL_Country_code__c,
                                    Primary_Goal_BI__c = planned,
                                    Other_Goal_BI__c = 0,
                                    zvod_PM_Strategic_Segment_BI__c = dp.BI_PL_Strategic_segment__c
                                );
                                cpd.External_ID__c = cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c + '-' + dp.BI_PL_Product__r.External_ID_vod__c;

                                //System.debug('%%% cpd: ' + cpd);
                                cpds.add(cpd);
                                mapCPD.put(key, cpd);

                            }




                            // Process final maps
                            list<Cycle_Plan_Detail_vod__c> detailsList = targetsToDetailsMap.get(cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c);
                            if (detailsList == null) {
                                detailsList = new list<Cycle_Plan_Detail_vod__c>();
                                detailsList.add(mapCPD.get(key));
                                targetsToDetailsMap.put(cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c, detailsList);
                                system.debug('%%% targetsToDetailsMap.put(): ' + cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c + ', ' + detailsList);
                            } else {
                                // System.debug('primary DetailList: ' + detailsList);
                                String exId = mapCPD.get(key).External_ID__c;
                                Boolean alreadyInList = false;
                                Boolean finishList = false;
                                Integer index = 0;
                                //System.debug('detailsList size: ' + detailsList.size());
                                while (!alreadyInList && !finishList) {
                                    Cycle_Plan_Detail_vod__c cpdetail = detailsList.get(index);
                                    if (cpdetail.External_ID__c == exId) {
                                        alreadyInList = true;
                                    } else {
                                        index ++;
                                    }

                                    if (index == detailsList.size()) {
                                        finishList = true;
                                    }
                                }
                                //System.debug('index: ' + index + ' boolean; ' + alreadyInList);
                                if (alreadyInList) {
                                    detailsList.remove(index);
                                    detailsList.add(mapCPD.get(key));

                                } else {
                                    detailsList.add(mapCPD.get(key));
                                }


                                //system.debug('%%% targetsToDetailsMap.put(): ' + cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c + ', ' + detailsList);

                                //system.debug('%%% ADDED ' + detailsList );
                            }
                        } else {
                            String key2 =  preparation.Id + '_' + dp.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c + '_' + dp.BI_PL_Secondary_product__c;

                            if (mapCPD.containsKey(key)) {

                                mapCPD.get(key).Planned_Details_vod__c = mapCPD.get(key).Planned_Details_vod__c + planned;
                                mapCPD.get(key).zvod_PM_Segmentation_BI__c = dp.BI_PL_Segment__c;
                                mapCPD.get(key).zvod_PM_Potential_BI__c = potential;
                                mapCPD.get(key).zvod_PM_Intimacy_BI__c = intimacy;
                                mapCPD.get(key).Primary_Goal_BI__c = mapCPD.get(key).Primary_Goal_BI__c + planned;
                                mapCPD.get(key).zvod_PM_Strategic_Segment_BI__c = dp.BI_PL_Strategic_segment__c;
                            } else {

                                Cycle_Plan_Detail_vod__c cpd1 = new Cycle_Plan_Detail_vod__c(
                                    Product_vod__c = dp.BI_PL_Product__c,
                                    Planned_Details_vod__c = planned,
                                    zvod_PM_Segmentation_BI__c = dp.BI_PL_Segment__c,
                                    zvod_PM_Potential_BI__c = potential,
                                    zvod_PM_Intimacy_BI__c = intimacy,
                                    Country_Code_BI__c = dp.BI_PL_Country_code__c,
                                    Primary_Goal_BI__c = planned,
                                    Other_Goal_BI__c = 0,
                                    zvod_PM_Strategic_Segment_BI__c = dp.BI_PL_Strategic_segment__c
                                );
                                cpd1.External_ID__c = cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c + '-' + dp.BI_PL_Product__r.External_ID_vod__c;
                                mapCPD.put(key, cpd1);
                            }

                            if (mapCPD.containsKey(key2)) {
                                mapCPD.get(key2).Planned_Details_vod__c = mapCPD.get(key2).Planned_Details_vod__c + planned;
                                mapCPD.get(key2).zvod_PM_Segmentation_BI__c = dp.BI_PL_Secondary_segment__c;
                                mapCPD.get(key2).zvod_PM_Potential_BI__c = potential;
                                mapCPD.get(key2).zvod_PM_Intimacy_BI__c = intimacy;
                                mapCPD.get(key2).Other_Goal_BI__c = mapCPD.get(key2).Other_Goal_BI__c + planned;
                                mapCPD.get(key2).zvod_PM_Strategic_Segment_BI__c = dp.BI_PL_Secondary_strategic_segment__c;
                            } else {
                                Cycle_Plan_Detail_vod__c cpd2 = new Cycle_Plan_Detail_vod__c(
                                    Product_vod__c = dp.BI_PL_Secondary_product__c,
                                    Planned_Details_vod__c = planned,
                                    zvod_PM_Segmentation_BI__c = dp.BI_PL_Secondary_segment__c,
                                    zvod_PM_Potential_BI__c = potential,
                                    zvod_PM_Intimacy_BI__c = intimacy,
                                    Country_Code_BI__c = dp.BI_PL_Country_code__c,
                                    Other_Goal_BI__c = planned,
                                    Primary_Goal_BI__c = 0,
                                    zvod_PM_Strategic_Segment_BI__c = dp.BI_PL_Secondary_strategic_segment__c
                                );
                                cpd2.External_ID__c = cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c + '-' + dp.BI_PL_Secondary_product__r.External_ID_vod__c;
                                mapCPD.put(key2, cpd2);
                            }



                            list<Cycle_Plan_Detail_vod__c> detailsList = targetsToDetailsMap.get(cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c);
                            if (detailsList == null) {
                                detailsList = new list<Cycle_Plan_Detail_vod__c>();
                                detailsList.add(mapCPD.get(key));
                                detailsList.add(mapCPD.get(key2));
                                targetsToDetailsMap.put(cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c, detailsList);
                                //system.debug('%%% targetsToDetailsMap.put(): ' + cptMap.get(dp.BI_PL_Channel_detail__r.BI_PL_Target__c).External_ID2__c + ', ' + detailsList);
                            } else {
                                //System.debug('secondary DetailList: ' + detailsList);
                                String exIdPrimary = mapCPD.get(key).External_ID__c;
                                String exIdSecondary = mapCPD.get(key2).External_ID__c;
                                Boolean listPrimary = false;
                                Boolean listSecondary = false;
                                Integer indexPrimary = 0;
                                Integer indexSecondary = 0;
                                Boolean finishList = false;
                                //System.debug('detailList size: ' + detailsList.size());
                                while ((!listPrimary || !listSecondary) && !finishList) {
                                    if (detailsList.get(indexPrimary).External_ID__c == exIdPrimary) {
                                        listPrimary = true;
                                    } else {
                                        indexPrimary ++;
                                    }

                                    if (detailsList.get(indexSecondary).External_ID__c == exIdSecondary) {
                                        listSecondary = true;
                                    } else {
                                        indexSecondary ++;
                                    }

                                    if ((indexPrimary == detailsList.size()) || (indexSecondary == detailsList.size())) {
                                        finishList = true;
                                    }
                                }

                                //System.debug('indexPrimary: ' + indexPrimary + ' indexSecondary; ' + indexSecondary + ' primaryBool: ' + listPrimary + ' secondaryBool: ' + listSecondary);

                                if (listPrimary) {
                                    detailsList.remove(indexPrimary);
                                    detailsList.add(mapCPD.get(key));
                                } else {
                                    detailsList.add(mapCPD.get(key));
                                }

                                if (listSecondary) {
                                    detailsList.remove(indexSecondary);
                                    detailsList.add(mapCPD.get(key2));

                                } else {
                                    detailsList.add(mapCPD.get(key2));
                                }



                                // system.debug('%%% ADDED ' + detailsList);
                            }



                        }

                    }
                }
            }
            //system.debug('%%% cpds created: ' + cpds);

        } catch (Exception e) {
            system.debug('Insert unsuccessful.');
            system.debug('%%% Exception: ' + e.getMessage());
            return;
        }
    }
    public static Decimal getToVeevaAddValue (Boolean reviewed, Boolean removed, Boolean rejected, Boolean edited, Boolean addedManually, Boolean nextBest, Boolean noSee, Decimal planned, Decimal adjusted) {

        if ((nextBest || noSee) && !addedManually)
            return -1;

        return getToVeevaAddValue(reviewed, removed, rejected, edited, addedManually, planned, adjusted);
    }
    public static Decimal getToVeevaAddValue (Boolean reviewed, Boolean removed, Boolean rejected, Boolean edited, Boolean addedManually, Decimal planned, Decimal adjusted) {
        /*if(adjusted == null || planned == null){
            return 0;
        }
        else */if (reviewed) {
            if (!removed) {
                if (!rejected && !edited) {
                    return planned;
                } else {
                    return adjusted;
                }
            } else {
                return -1;
            }
        } else {
            if (addedManually) {
                return -1;
            } else {
                return planned;
            }
        }
    }
}