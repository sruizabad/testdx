/**
 *	27/07/2017
 *	@author	OMEGA CRM
 */
@isTest
public with sharing class BI_PL_SynchronizeSAPDataBatch_Test {

	private static String channel = 'rep_detail_only';
	
	private static Account account1;
	private static Account account2;
	private static Account account3;
	private static Account account4;
	private static Account account5;
	private static Account account6;

	@isTest
	public static void test() {
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = testUser.Country_Code_BI__c;
		BI_PL_TestDataUtility.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		String sapName = 'TestSAPName';
		String hierarchy = 'testHierarchy';

		account1 = new Account(Name = 'Account1', External_ID_vod__c = 'testExtAccount1');
		account2 = new Account(Name = 'Account2', External_ID_vod__c = 'testExtAccount2');
		account3 = new Account(Name = 'Account3', External_ID_vod__c = 'testExtAccount3');
		account4 = new Account(Name = 'Account4', External_ID_vod__c = 'testExtAccount4');
		account5 = new Account(Name = 'Account5', External_ID_vod__c = 'testExtAccount5');
		account6 = new Account(Name = 'Account6', External_ID_vod__c = 'testExtAccount6');

		insert new List<Account> {account1, account2, account3, account4, account5, account6};

		BI_PL_Business_rule__c br = new BI_PL_Business_rule__c(BI_PL_Visits_per_day__c = 20,
		        BI_PL_Country_code__c = userCountryCode,
		        BI_PL_Type__c = BI_PL_PreparationUtility.THRESHOLD_RULE_TYPE,
		        BI_PL_Active__c = true);

		insert br;

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'KAM', BI_TM_Country_Code__c =userCountryCode);
		insert fieldForce;

		BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = userCountryCode,
		        BI_PL_Start_date__c = Date.newInstance(2018, 1, 1),
		        BI_PL_End_date__c = Date.newInstance(2018, 1, 31),
		        BI_PL_Field_force__c = fieldForce.Id);

		insert cycle;

		BI_PL_Position__c position1 = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = userCountryCode);
		BI_PL_Position__c position2 = new BI_PL_Position__c(Name = 'Test2', BI_PL_Country_code__c = userCountryCode);

		insert new List<BI_PL_Position__c> {position1, position2};

		BI_PL_Position_cycle__c positionCycle1 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position1.Id,
		        BI_PL_External_id__c = 'test1', BI_PL_Hierarchy__c = hierarchy);
		BI_PL_Position_cycle__c positionCycle2 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position2.Id,
		        BI_PL_External_id__c = 'test2', BI_PL_Hierarchy__c = hierarchy);

		insert new List<BI_PL_Position_cycle__c> {positionCycle1, positionCycle2};

		BI_PL_Preparation__c preparation1 = createPreparation1(positionCycle1.Id);

		Test.startTest();

		Database.executeBatch(new BI_PL_SynchronizeSAPDataBatch(new List<String> {preparation1.Id}, sapName, channel));

		Test.stopTest();

		// Assertions

		Cycle_Plan_Target_vod__c cyclePlanTarget1 = getCyclePlanTarget(account1.Id).get(0);		// Reviewed (planned)
		Cycle_Plan_Target_vod__c cyclePlanTarget2 = getCyclePlanTarget(account2.Id).get(0);		// Reviewed and edited (adjusted)
		Cycle_Plan_Target_vod__c cyclePlanTarget3 = getCyclePlanTarget(account3.Id).get(0);		// Everything false (planned)
		List<Cycle_Plan_Target_vod__c> cyclePlanTarget4 = getCyclePlanTarget(account4.Id);		// Reviewed and removed (do not sync)
		List<Cycle_Plan_Target_vod__c> cyclePlanTarget5 = getCyclePlanTarget(account5.Id);		// Added manually (do not sync)

		// For more information about the synchronization process visit "https://confluence.bi-scrum.com/display/GLOS/Field+calculation+definition"

		System.assertEquals(8, cyclePlanTarget1.Planned_Calls_vod__c, 'The planned calls are right');
		System.assertEquals(10, cyclePlanTarget2.Planned_Calls_vod__c, 'The planned calls are not right');
		System.assertEquals(13, cyclePlanTarget3.Planned_Calls_vod__c, 'The planned calls are not right');
		System.assertEquals(0, cyclePlanTarget4.size(), 'The target should have not been synchronized');
		System.assertEquals(0, cyclePlanTarget5.size(), 'The target should have not been synchronized');

	}
	private static List<Cycle_Plan_Target_vod__c> getCyclePlanTarget(Id accountId) {
		return new List<Cycle_Plan_Target_vod__c>([SELECT Id, Cycle_Plan_Account_vod__c, Planned_Calls_vod__c, Country_Code_BI__c FROM Cycle_Plan_Target_vod__c WHERE Cycle_Plan_Account_vod__c = : accountId]);
	}

	private static BI_PL_Preparation__c createPreparation1(Id positionCycleId) {
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = testUser.Country_Code_BI__c;
		// Preparation creation
		BI_PL_Preparation__c preparation = new BI_PL_Preparation__c(
		    BI_PL_Country_code__c = userCountryCode,
		    BI_PL_External_id__c = 'TestExt1',
		    BI_PL_Position_cycle__c = positionCycleId);

		insert preparation;

		// Targets creation

		BI_PL_Target_preparation__c target1 = new BI_PL_Target_preparation__c(BI_PL_Header__c = preparation.Id, BI_PL_Target_customer__c = account1.Id, BI_PL_External_id__c = 'targetExternal1',
		        /*BI_PL_Removed__c = false,*/ BI_PL_Added_manually__c = false);
		BI_PL_Target_preparation__c target2 = new BI_PL_Target_preparation__c(BI_PL_Header__c = preparation.Id, BI_PL_Target_customer__c = account2.Id, BI_PL_External_id__c = 'targetExternal2',
		        /*BI_PL_Removed__c = false,*/ BI_PL_Added_manually__c = false);
		BI_PL_Target_preparation__c target3 = new BI_PL_Target_preparation__c(BI_PL_Header__c = preparation.Id, BI_PL_Target_customer__c = account3.Id, BI_PL_External_id__c = 'targetExternal3',
		        /*BI_PL_Removed__c = false,*/  BI_PL_Added_manually__c = false);
		BI_PL_Target_preparation__c target4 = new BI_PL_Target_preparation__c(BI_PL_Header__c = preparation.Id, BI_PL_Target_customer__c = account4.Id, BI_PL_External_id__c = 'targetExternal4',
		        /*BI_PL_Removed__c = true,*/ BI_PL_Added_manually__c = false);
		BI_PL_Target_preparation__c target5 = new BI_PL_Target_preparation__c(BI_PL_Header__c = preparation.Id, BI_PL_Target_customer__c = account5.Id, BI_PL_External_id__c = 'targetExternal5',
		        /*BI_PL_Removed__c = false,*/  BI_PL_Added_manually__c = true);

		BI_PL_Target_preparation__c target6 = new BI_PL_Target_preparation__c(BI_PL_Header__c = preparation.Id, BI_PL_Target_customer__c = account6.Id, BI_PL_External_id__c = 'targetExternal6',
		        /*BI_PL_Removed__c = false,*/ BI_PL_Added_manually__c = false);

		insert new List<BI_PL_Target_preparation__c> {target1, target2, target3, target4, target5, target6};

		// Channel detail preparation creation
		BI_PL_Channel_detail_preparation__c channelDetail1 = new BI_PL_Channel_detail_preparation__c(BI_PL_External_id__c = 'channelExtId1', BI_PL_Channel__c = channel,
		        BI_PL_Target__c = target1.Id, BI_PL_Reviewed__c = true, BI_PL_Rejected__c = false, BI_PL_Edited__c = true, BI_PL_MSL_flag__c = true, BI_PL_Removed__c = false);
		BI_PL_Channel_detail_preparation__c channelDetail2 = new BI_PL_Channel_detail_preparation__c(BI_PL_External_id__c = 'channelExtId2', BI_PL_Channel__c = channel,
		        BI_PL_Target__c = target2.Id, BI_PL_Reviewed__c = true, BI_PL_Rejected__c = false, BI_PL_Edited__c = true, BI_PL_MSL_flag__c = true, BI_PL_Removed__c = false);
		BI_PL_Channel_detail_preparation__c channelDetail3 = new BI_PL_Channel_detail_preparation__c(BI_PL_External_id__c = 'channelExtId3', BI_PL_Channel__c = channel,
		        BI_PL_Target__c = target3.Id, BI_PL_Reviewed__c = false, BI_PL_Rejected__c = false, BI_PL_Edited__c = true, BI_PL_MSL_flag__c = true, BI_PL_Removed__c = false);
		BI_PL_Channel_detail_preparation__c channelDetail4 = new BI_PL_Channel_detail_preparation__c(BI_PL_External_id__c = 'channelExtId4', BI_PL_Channel__c = channel,
		        BI_PL_Target__c = target4.Id, BI_PL_Reviewed__c = true, BI_PL_Rejected__c = false, BI_PL_Edited__c = true, BI_PL_MSL_flag__c = false, BI_PL_Removed__c = true);
		BI_PL_Channel_detail_preparation__c channelDetail5 = new BI_PL_Channel_detail_preparation__c(BI_PL_External_id__c = 'channelExtId5', BI_PL_Channel__c = channel,
		        BI_PL_Target__c = target5.Id, BI_PL_Reviewed__c = false, BI_PL_Rejected__c = false, BI_PL_Edited__c = true, BI_PL_MSL_flag__c = true, BI_PL_Removed__c = false);

		BI_PL_Channel_detail_preparation__c channelDetail6 = new BI_PL_Channel_detail_preparation__c(BI_PL_External_id__c = 'channelExtId6', BI_PL_Channel__c = channel,
		        BI_PL_Target__c = target6.Id, BI_PL_Reviewed__c = true, BI_PL_Rejected__c = false, BI_PL_Edited__c = true, BI_PL_MSL_flag__c = true, BI_PL_Removed__c = false);

		insert new List<BI_PL_Channel_detail_preparation__c> {channelDetail1, channelDetail2 ,channelDetail3, channelDetail4, channelDetail5, channelDetail6};

		// Product catalog creation
		Product_vod__c product1 = new Product_vod__c(Name = 'Product1', Product_Type_vod__c = 'Detail');
		Product_vod__c product2 = new Product_vod__c(Name = 'Product2', Product_Type_vod__c = 'Detail');
		Product_vod__c product3 = new Product_vod__c(Name = 'Product3', Product_Type_vod__c = 'Detail');
		Product_vod__c product4 = new Product_vod__c(Name = 'Product4', Product_Type_vod__c = 'Detail');

		insert new List<Product_vod__c> {product1, product2, product3, product4};

		// Detail preparation creation
		BI_PL_Detail_preparation__c detail_11 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt1', BI_PL_Channel_detail__c = channelDetail1.Id, BI_PL_Product__c = product1.Id, BI_PL_Adjusted_details__c = 6, BI_PL_Planned_details__c = 1);
		BI_PL_Detail_preparation__c detail_12 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt2', BI_PL_Channel_detail__c = channelDetail1.Id, BI_PL_Product__c = product2.Id, BI_PL_Adjusted_details__c = 7, BI_PL_Planned_details__c = 2);
		BI_PL_Detail_preparation__c detail_13 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt3', BI_PL_Channel_detail__c = channelDetail1.Id, BI_PL_Product__c = product3.Id, BI_PL_Adjusted_details__c = 8, BI_PL_Planned_details__c = 3);

		BI_PL_Detail_preparation__c detail_21 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt4', BI_PL_Channel_detail__c = channelDetail2.Id, BI_PL_Product__c = product1.Id, BI_PL_Adjusted_details__c = 9, BI_PL_Planned_details__c = 4);
		BI_PL_Detail_preparation__c detail_22 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt5', BI_PL_Channel_detail__c = channelDetail2.Id, BI_PL_Product__c = product2.Id, BI_PL_Adjusted_details__c = 10, BI_PL_Planned_details__c = 5);

		BI_PL_Detail_preparation__c detail_31 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt6', BI_PL_Channel_detail__c = channelDetail3.Id, BI_PL_Product__c = product2.Id, BI_PL_Adjusted_details__c = 12, BI_PL_Planned_details__c = 13);

		BI_PL_Detail_preparation__c detail_41 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt7', BI_PL_Channel_detail__c = channelDetail4.Id, BI_PL_Product__c = product2.Id, BI_PL_Adjusted_details__c = 20, BI_PL_Planned_details__c = 21);

		BI_PL_Detail_preparation__c detail_51 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt8', BI_PL_Channel_detail__c = channelDetail5.Id, BI_PL_Product__c = product2.Id, BI_PL_Adjusted_details__c = 20, BI_PL_Planned_details__c = 21);

		BI_PL_Detail_preparation__c detail_61 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt61', BI_PL_Channel_detail__c = channelDetail6.Id, BI_PL_Product__c = product1.Id, BI_PL_Adjusted_details__c = 20, BI_PL_Planned_details__c = 21, BI_PL_Secondary_product__c = product2.Id);
		BI_PL_Detail_preparation__c detail_62 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt62', BI_PL_Channel_detail__c = channelDetail6.Id, BI_PL_Product__c = product2.Id, BI_PL_Adjusted_details__c = 20, BI_PL_Planned_details__c = 21, BI_PL_Secondary_product__c = product3.Id);
		BI_PL_Detail_preparation__c detail_63 = new BI_PL_Detail_preparation__c(BI_PL_External_id__c = 'detailExt63', BI_PL_Channel_detail__c = channelDetail6.Id, BI_PL_Product__c = product2.Id, BI_PL_Adjusted_details__c = 20, BI_PL_Planned_details__c = 21, BI_PL_Secondary_product__c = product3.Id);

		insert new List<BI_PL_Detail_preparation__c> {detail_11, detail_12, detail_13, detail_21, detail_22, detail_31, detail_41, detail_51, detail_61, detail_62, detail_63 };

		return preparation;
	}
}