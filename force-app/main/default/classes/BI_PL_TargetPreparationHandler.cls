public without sharing class BI_PL_TargetPreparationHandler
	implements BI_PL_TriggerInterface {

	/*
	 *	Contains the related accounts with the specialty fields.
	*/
	Map<Id, Account> accounts = new Map<Id, Account>();
	/*
	 *	Specialty field set by custom setting for each country.
	*/
	Map<String, String> specialtyFieldByCountryCode = new Map<String, String>();
	Map<Id, BI_PL_Preparation__c> prepWithCountryCode = new Map<Id, BI_PL_Preparation__c>();
	Map<String, String> specilatyCodeMapping = new Map<String, String>();
	Map<String, Boolean> specialtyCodeByCountryCode = new Map<String, Boolean>();

	String specialtyField;


	// Constructor
	public BI_PL_TargetPreparationHandler() {
	}

	public static String getSpecialtyFieldForCountry(String countryCode) {
		try {
			return [SELECT BI_PL_Specialty_field__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c = :countryCode].BI_PL_Specialty_field__c;
		} catch (exception e) {
			throw new BI_PL_Exception(Label.BI_PL_Specialty_field_not_properly_countries + countryCode + '. ' + Label.BI_PL_Contact_system_admin);
		}
	}

	

	/**
	 *	Returns all the country codes found for the trigger targets.
	 */
	private Set<String> getTargetDetailCountryCodes() {
		// Instead of using the Account's Country code use the preparation one (otherwise, data loading can be problematic).
		Set<String> countryCodes = new Set<String>();

		List<Id> prepId = new List<Id>();
		for (SObject targetPreparation : Trigger.new) {
			prepId.add(((BI_PL_Target_preparation__c)targetPreparation).BI_PL_Header__c);

		}

		for (BI_PL_Preparation__c preparation : [SELECT Id, BI_PL_Country_code__c FROM BI_PL_Preparation__c WHERE Id IN :prepId]) {
			countryCodes.add(preparation.BI_PL_Country_code__c);
			prepWithCountryCode.put(preparation.id, preparation);
		}

		return countryCodes;
	}

	/**
	 * bulkBefore
	 *
	 */
	public void bulkBefore() {
		// 1.- Retrieve all settings so as to know which specialty field is used for each account country:
		for (BI_PL_Country_settings__c setting : [SELECT BI_PL_Specialty_field__c, BI_PL_Country_code__c, BI_PL_Specialty_code__c FROM BI_PL_Country_settings__c WHERE BI_PL_Country_code__c IN :getTargetDetailCountryCodes()]) {
			if (String.isBlank(setting.BI_PL_Specialty_field__c))
				throw new BI_PL_Exception(Label.BI_PL_Specialty_field_not_set_for_country + ' \'' + setting.BI_PL_Country_code__c + '\'. ' + Label.BI_PL_Contact_system_admin);

			specialtyFieldByCountryCode.put(setting.BI_PL_Country_code__c, setting.BI_PL_Specialty_field__c);
			specialtyCodeByCountryCode.put(setting.BI_PL_Country_code__c,setting.BI_PL_Specialty_code__c);
		}

		for(BI_PL_Specialty_code_mapping__mdt specialtyCodeMap :[SELECT MasterLabel, BI_PL_Code__c FROM BI_PL_Specialty_code_mapping__mdt] ){
			specilatyCodeMapping.put(specialtyCodeMap.MasterLabel, specialtyCodeMap.BI_PL_Code__c);
		}

		System.debug('Sp code Map: ' + specilatyCodeMapping);
	

		

		
		// 1.- Get accounts related to trigger targets:
		List<Id> accountsIds = new List<Id>();

		for (SObject targetPreparation : Trigger.new)
			accountsIds.add(((BI_PL_Target_preparation__c)targetPreparation).BI_PL_Target_customer__c);


		// 2.- Query for the specialty fields of these accounts:
		Set<String> alreadyAddedFields = new Set<String>();
		String query = 'SELECT Id, Country_Code_BI__c';

		for (String field : specialtyFieldByCountryCode.values()) {
			if (!alreadyAddedFields.contains(field)) {
				query += ', ' + field;
				alreadyAddedFields.add(field);
			}
		}

		query += ' FROM Account WHERE Id IN :accountsIds';

		// 3.- Retrieve the accounts with the specialty fields:
		try {
			for (Account a : Database.query(query)) {
				accounts.put(a.Id, a);
			}
		} catch (System.QueryException e) {
			throw new BI_PL_Exception(Label.BI_PL_Specialty_field_not_properly_countries + specialtyFieldByCountryCode.keySet() + '. ' + Label.BI_PL_Contact_system_admin + ' - ' + e.getMessage());
		}




	}
	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkAfter() {

		


	}

	/**
	 * beforeInsert
	 */
	public void beforeInsert(SObject so) {
		BI_PL_Target_preparation__c target = (BI_PL_Target_preparation__c)so;
		fillSpecialtyField(target, accounts.get(target.BI_PL_Target_customer__c));
		fillTargetIdField(target);
	}
	/**
	 * Fills the target's specialty field with the one in account defined by the country custom setting.
	 *
	 * @author	OMEGA CRM
	 */
	private void fillSpecialtyField(BI_PL_Target_preparation__c target, Account account) {
		//String countryCode = account.Country_Code_BI__c;
		String countryCode = prepWithCountryCode.get(target.BI_PL_Header__c).BI_PL_Country_code__c;
		//String countryCode = target.BI_PL_Header__r.BI_PL_Country_code__c;

		if (!specialtyFieldByCountryCode.containsKey(countryCode))
			throw new BI_PL_Exception(Label.BI_PL_Specialty_field_not_set_for_country + ' \'' + countryCode + '\'. ' + Label.BI_PL_Contact_system_admin);


		String specialtyField = specialtyFieldByCountryCode.get(countryCode);
		String specialtyValue = (String)getFieldValue(accounts.get(target.BI_PL_Target_customer__c), specialtyField);

		Schema.SObjectField picklistField = getPicklistFieldIfIsPicklist(specialtyField);

		if (picklistField != null) {
			// Return the picklist label instead of the value.
			// Done this way because business rules' Specialty are set in the country's language.
			specialtyValue = getPicklistLabel(picklistField, specialtyValue);
		}

		target.BI_PL_Specialty__c = specialtyValue;

		if(specialtyCodeByCountryCode.get(countryCode)){
			if(specilatyCodeMapping.containsKey(specialtyValue)){
				target.BI_PL_Specialty_code__c = specilatyCodeMapping.get(specialtyValue);
			}else
				target.BI_PL_Specialty_code__c = '';
		}

		
	}

	public static Schema.SObjectField getPicklistFieldIfIsPicklist(String fieldPath) {
		Schema.SObjectType sobjectType = Account.getSObjectType();
		Schema.SObjectField currentField;

		if (fieldPath.contains('.')) {
			while (fieldPath.contains('.')) {
				// 1.- Get the sObject's describe:
				Schema.DescribeSObjectResult sObjectDescribe = sobjectType.getDescribe();

				// 2.- If the field is a relationship field then change the sObjectType.
				//	   Otherwise, if the field is not a relationship ignore this step:
				String relation = fieldPath.substringBefore('.');

				if (relation.endsWith('__r'))
					relation = relation.replace('__r', '__c');

				currentField = sObjectDescribe.fields.getMap().get(relation);

				if (currentField.getDescribe().getType() == Schema.DisplayType.Reference) {
					sobjectType = currentField.getDescribe().getReferenceTo().get(0);
				}
				fieldPath = fieldPath.substringAfter('.');
			}
		} else {
			currentField = sobjectType.getDescribe().fields.getMap().get(fieldPath);
		}
		// 3.- Get the field value. If the field is a Picklist, the value will be the label.
		//	   Otherwise, directly dump the field value:
		if (currentField.getDescribe().getType() == Schema.DisplayType.PICKLIST) {
			return currentField;
		} else {
			return null;
		}
	}

	private String getPicklistLabel(Schema.SObjectField picklistField, String value) {
		Schema.DescribeFieldResult fieldResult = picklistField.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		for (Schema.PicklistEntry f : ple) {
			if (f.getValue() == value)
				return f.getLabel();
		}
		return null;
	}

	private void fillTargetIdField(BI_PL_Target_preparation__c target) {
		target.BI_PL_Target_customer_id__c = target.BI_PL_Target_customer__c;
	}

	public static Object getFieldValue(SObject o, String field) {

		if (o == null) {
			return null;
		}
		if (field.contains('.')) {
			String nextField = field.substringAfter('.');
			String relation = field.substringBefore('.');
			return getFieldValue((SObject)o.getSObject(relation), nextField);
		} else {
			return o.get(field);
		}

	}


	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
	 */
	public void beforeUpdate(SObject oldSo, SObject so) {
		BI_PL_Target_preparation__c target = (BI_PL_Target_preparation__c)so;
		fillSpecialtyField(target, accounts.get(target.BI_PL_Target_customer__c));
		fillTargetIdField(target);
	}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
	 */
	public void beforeDelete(SObject so) {
	}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
	 */
	public void afterInsert(SObject so) {

		
	}


	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
	 */
	public void afterUpdate(SObject oldSo, SObject so) {
	}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 */
	public void afterDelete(SObject so) {
	}

	/**
	 * andFinally
	 *
	 * This method is called once all records have been processed by the trigger. Use this
	 * method to accomplish any final operations such as creation or updates of other records.
	 */
	public void andFinally() {
	}

}