@isTest
private class BI_PL_TargetPreparationTrigger_Test {

	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

    }
	
	@isTest static void test_method_one() {
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		String countryCode = testUser.Country_Code_BI__c;

		Account acc = new Account(Name = 'testAccountDF', External_ID_vod__c = 'Test_Ex_Id' + countryCode, Country_Code_BI__c = countryCode, BillingCity = 'CityTest', BillingState  = 'stateFull');
		insert acc;

		Decimal oldTargetSize = 0;

		BI_PL_TargetPreparationHandler.getSpecialtyFieldForCountry(countryCode);
		//BI_PL_TargetPreparationHandler.getSpecialtyFieldForCountry('wrong');
		Test.startTest();
		BI_PL_Preparation__c prep = [SELECT ID, BI_PL_Number_of_targets__c FROM BI_PL_Preparation__c LIMIT 1];
		oldTargetSize = prep.BI_PL_Number_of_targets__c;

		BI_PL_Target_preparation__c tarPrep = new BI_PL_Target_preparation__c();
		tarPrep.BI_PL_Target_Customer__c = acc.Id;
		tarPrep.BI_PL_External_id__c = 'test';
		tarPrep.BI_PL_Header__c = prep.Id;
		upsert tarPrep;

		//BI_PL_Target_preparation__c tarPrep2 = [SELECT Id, BI_PL_External_Id__c FROM BI_PL_Target_preparation__c LIMIT 1];
		//tarPrep2.BI_PL_External_id__c = 'test2';
		//update tarPrep2;

		Test.stopTest();

		BI_PL_Preparation__c prepAssert = [SELECT ID, BI_PL_Number_of_targets__c FROM BI_PL_Preparation__c WHERE Id = :prep.Id LIMIT 1];
		System.assertEquals(oldTargetSize + 1,  prepAssert.BI_PL_Number_of_targets__c);

	}
	
}