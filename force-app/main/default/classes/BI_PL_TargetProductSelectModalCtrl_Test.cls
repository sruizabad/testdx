@isTest
private class BI_PL_TargetProductSelectModalCtrl_Test {
	
	private static String channel = BI_PL_TestDataFactory.SCHANNEL;

	private static String countryCode, cycleId;
	private static List<Account> accList;
	private static List<Product_vod__c> prodList;
	private static List<BI_PL_Preparation__c> prepList;
	private static BI_PL_Position__c oPosition;
	private static BI_PL_PreparationExt.PlanitProductPair productPair;

	private static void getData(){

		countryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
		cycleId = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1].Id;

        accList = [SELECT Id, External_ID_vod__c FROM Account];
		system.assertNotEquals(accList, null);

        prodList = [SELECT Id, Name, External_ID_vod__c FROM Product_vod__c];
        system.assertNotEquals(prodList, null);

        productPair = new BI_PL_PreparationExt.PlanitProductPair(prodList.get(0), prodList.get(1));
        system.debug('##productPair: ' + productPair);
        system.assertNotEquals(productPair, null);

        prepList = [SELECT Id, BI_PL_External_Id__c, BI_PL_Country_code__c, BI_PL_Status__c FROM BI_PL_Preparation__c];
		system.assertNotEquals(prepList, null);

        oPosition = [SELECT Id, Name FROM BI_PL_Position__c LIMIT 1];
		system.assertNotEquals(oPosition, null);

	}

	@testSetup static void setup() {
	
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();

		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		String countryCode = testUser.Country_Code_BI__c;

		BI_PL_TestDataFactory.createTestAccounts(2, countryCode);
		BI_PL_TestDataFactory.createTestProduct(2, countryCode);

		BI_PL_TestDataFactory.createCycleStructure(countryCode);

        accList = [SELECT Id, External_ID_vod__c FROM Account];
        prodList = [SELECT Id, Name, External_ID_vod__c FROM Product_vod__c];
		List<BI_PL_Position_Cycle__c> posCycList = [SELECT Id, BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
		
		BI_PL_TestDataFactory.createPreparations(countryCode, posCycList, accList, prodList);

		List<BI_PL_Detail_preparation__c> lstDetPrep = [SELECT Id, BI_PL_External_id__c FROM BI_PL_Detail_preparation__c];
		system.assert(!lstDetPrep.isEmpty() );

		//Add Secondary Product to each Preparation
		for (BI_PL_Detail_preparation__c inDrep : lstDetPrep){
			inDrep.BI_PL_Secondary_product__c = prodList.get(prodList.size()-1 ).Id;
		}
		update lstDetPrep;

	}
	
	@isTest static void test_method_one() {
		
		getData();		

		map<String, BI_PL_PreparationExt.PlanitProductPair> mapProductPair = new map<String, BI_PL_PreparationExt.PlanitProductPair>();

		String prodPairId = prodList.get(0).Id + '' + prodList.get(1).Id;
		mapProductPair.put(prodPairId, productPair);
		
		Test.startTest();

		BI_PL_TargetProductSelectionModalCtrl.getActionTypes();
		
		BI_PL_TargetProductSelectionModalCtrl.getAddedReasonOptions();

		BI_PL_TargetProductSelectionModalCtrl.getTargetPreparationsWithoutProduct('US', oPosition.Id, cycleId, accList.get(0).Id, null);
		
		system.assertNotEquals( BI_PL_TargetProductSelectionModalCtrl.getDetailsHavingTarget(mapProductPair, accList.get(0).Id, oPosition.Name, cycleId, channel), null );
				
		Test.stopTest();


	}
	
	@isTest static void test_method_two() {	

		getData();

		BI_PL_TestDataFactory.insertBusinessRules(prodList, countryCode, oPosition);
		
		List<BI_PL_PreparationExt.PlanitProductPair> prodPairList = new List<BI_PL_PreparationExt.PlanitProductPair>();
		prodPairList.add(productPair);
		
		Map<String, List<BI_PL_Preparation__c>> mapTargetProductSelect = 
			BI_PL_TargetProductSelectionModalCtrl.getTargetPreparationsByProduct(true, prodPairList, BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY, countryCode, oPosition.Id, cycleId);

		system.assert(mapTargetProductSelect.containsKey(productPair.Id) );


		Test.startTest();

		BI_PL_Preparation_action__c action1 = BI_PL_TestDataFactory.createTestPreparationAction(accList.get(0).Id, prepList.get(0),prepList.get(0), channel, false);
		BI_PL_TransferAndSHareUtility.TransferAndShareRequestWrapper request = new BI_PL_TransferAndSHareUtility.TransferAndShareRequestWrapper(action1);
		List<BI_PL_TransferAndSHareUtility.TransferAndShareRequestWrapper> listReq = new List<BI_PL_TransferAndSHareUtility.TransferAndShareRequestWrapper>();
		listReq.add(request);
		BI_PL_TargetProductSelectionModalCtrl.saveRequests(listReq);
		
		for (BI_PL_TransferAndSHareUtility.TransferAndShareRequestWrapper oReq : listReq) {
			system.assertNotEquals(oReq.record.Id, null);
		}
		
		Map<String, Integer> mapActions = BI_PL_TargetProductSelectionModalCtrl.getNumberOfActionsByType(prepList.get(0).Id);
		for (String oStr : mapActions.keyset() ){
			system.assert(mapActions.get(oStr) > 1);
		}
		
		Test.stopTest();

		mapTargetProductSelect = BI_PL_TargetProductSelectionModalCtrl.getTargetPreparationsByProduct(false, prodPairList, BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY, countryCode, oPosition.Id, cycleId);
		system.assert(mapTargetProductSelect.containsKey(productPair.Id) );
	}

	
}