/**
 *	06/11/2017
 *	- GLOS-473 : Now the user can select individual products to transfer / share.
 *	20/09/2017
 *	- Created date.
 *
 *	TargetProductSelectionModal controller.
 *
 *	@author	OMEGA CRM
 */
public without sharing class BI_PL_TargetProductSelectionModalCtrl {

	/**
	 *	Returns the details for the Target in other positions (OVERLAP).
	 *	There must be a detail for each of the provided product pairs in order to be an overlap.
	 *	key = detail.Id, value = detail wrapper
	 *
	 * 	@author OMEGA CRM
	 */
	@ReadOnly
	@RemoteAction
	public static Map<String, RelatedDetailWrapper> getDetailsHavingTarget(Map<String, BI_PL_PreparationExt.PlanitProductPair> productPairs, Id accountId, String positionName, String cycleId, String channel) {

		Map<String, Map<String, RelatedDetailWrapper>> detailsByPositionName = new Map<String, Map<String, RelatedDetailWrapper>>();


		Map<String, RelatedDetailWrapper> output = new Map<String, RelatedDetailWrapper>();

		// 1.- Get all details:
		for (BI_PL_Detail_preparation__c detail : [SELECT Id, BI_PL_Product__c, BI_PL_Secondary_product__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c, BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c, BI_PL_Adjusted_details__c
		        FROM BI_PL_Detail_preparation__c
		        WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c = :accountId
		                AND BI_PL_Channel_detail__r.BI_PL_Removed__c = false
		                        AND (BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c = false OR (BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c = true AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c = true AND BI_PL_Channel_detail__r.BI_PL_Edited__c = true))
		                        AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c != :positionName
		                        AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
		                                AND BI_PL_Channel_detail__r.BI_PL_Channel__c = : channel]) {

			String territoryName = detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c;

			if (!detailsByPositionName.containsKey(territoryName))
				detailsByPositionName.put(territoryName, new Map<String, RelatedDetailWrapper>());

			detailsByPositionName.get(territoryName).put(detail.Id, new RelatedDetailWrapper(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_Customer__c,
			        detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
			        detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__c,
			        detail.BI_PL_Product__c,
			        detail.BI_PL_Secondary_product__c,
			        detail.BI_PL_Adjusted_details__c));

		}

		System.debug('detailsByPositionName ' + detailsByPositionName);
		// 2.- Return only the details that match all provided product pairs:
		for (String territoryName : detailsByPositionName.keySet()) {
			for (String detailId : detailsByPositionName.get(territoryName).keySet()) {
				RelatedDetailWrapper detail = detailsByPositionName.get(territoryName).get(detailId);
				for (BI_PL_PreparationExt.PlanitProductPair productPair : productPairs.values()) {
					// If one of the product pairs matches the detail's products add it:
					if (detail.primaryProductId == productPair.primary.Id && (!productPair.hasSecondary || (productPair.hasSecondary && detail.secondaryProductId == productPair.secondary.Id))) {
						output.put(detailId, detail);
						break;
					}
				}
			}
		}

		return output;
	}

	@ReadOnly
	@RemoteAction
	public static List<BI_PL_Preparation__c> getTargetPreparationsWithoutProduct(String countryCode, String currentPositionId, String cycleId, String accountId, String planId) {

		List<String> overlapPlansWithoutProduct = new List<String>();



		for(BI_PL_Target_preparation__c tgt : [SELECT BI_PL_Header__c 
			FROM BI_PL_Target_preparation__c 
			WHERE BI_PL_Target_Customer__c = :accountId
                        AND BI_PL_Header__r.BI_PL_Country_Code__c = :countryCode
                        AND BI_PL_Header__c != :planId
                        AND BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__c != :currentPositionId
                        AND BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId ]) {
			overlapPlansWithoutProduct.add(tgt.BI_PL_Header__c);
		}

		return [SELECT Id, Name, BI_PL_Position_cycle__r.BI_PL_Position__c, BI_PL_Position_cycle__r.BI_PL_Position_name__c, Owner.Name, BI_PL_Position_cycle__r.BI_PL_Confirmed__c
			FROM BI_PL_Preparation__c 
			WHERE BI_PL_Country_code__c = :countryCode 
			AND BI_PL_Position_cycle__r.BI_PL_Position__c != :currentPositionId 
			AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
			AND ID NOT IN :overlapPlansWithoutProduct];
	}

	/**
	 *	Returns a map (key = productPairId, value = List of BI_PL_Posision__c) grouping the available positions by product for the selected target.
	 *
	 *	@author OMEGA CRM
	 */
	@ReadOnly
	@RemoteAction
	public static Map<String, List<BI_PL_Preparation__c>> getTargetPreparationsByProduct(Boolean allProductsByDefault, List<BI_PL_PreparationExt.PlanitProductPair> currentTargetProductPairs, String targetSpecialty, String countryCode, String currentPositionId, String cycleId) {

		System.debug('getTargetPreparationsByProduct' + allProductsByDefault+ currentTargetProductPairs+targetSpecialty+ countryCode+currentPositionId+ cycleId);


		Map<String, List<BI_PL_Preparation__c>> preparationsByProduct = new Map<String, List<BI_PL_Preparation__c>>();

		Set<String> currentProductPairs = new Set<String>();

		for (BI_PL_PreparationExt.PlanitProductPair p : currentTargetProductPairs)
			currentProductPairs.add(p.Id);

		Map<String, Set<Id>> forbiddenPositionsByProduct = new Map<String, Set<Id>>();

		if (allProductsByDefault) {
			// ALLOWED PRODUCTS ARE FOUND BY THE BUSINESS RULES "PRIMARY AND SECONDARY" MINUS THE "FORBIDDEN PRODUCT".
			// The products already added to the target could not be available in the target Position (Preparation), because
			// the "Primary and Secondary" business rules are set by Position.
			// >>>>>>> The target positions will be the ones having all the target's products available.

			List<BI_PL_Business_rule__c> primaryAndSecondaryBRs = new List<BI_PL_Business_rule__c>();
			List<BI_PL_Business_rule__c> forbiddenProductBRs = new List<BI_PL_Business_rule__c>();

			Set<Id> positionsId = new Set<Id>();

			// "currentTargetProductPairs" are used to filter the business rules. If one of the currentTargetProductPairs is missing in the primary business rules,
			// then the position won't be available to set as the target one.
			for (BI_PL_Business_rule__c br : BI_PL_BusinessRuleUtility.getBusinessRulesForCountry(countryCode,
			        new List<String> {BI_PL_BusinessRuleUtility.CUSTOMER_SPECIALTY,
			                          BI_PL_BusinessRuleUtility.FORBIDDEN_PRODUCT,
			                          BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY
			                         },
			        null,
			        null,
			        currentTargetProductPairs)) {

				if (br.BI_PL_Type__c == BI_PL_BusinessRuleUtility.PRIMARY_AND_SECONDARY) {
					primaryAndSecondaryBRs.add(br);
					positionsId.add(br.BI_PL_Position__c);
				}

				if (br.BI_PL_Type__c == BI_PL_BusinessRuleUtility.FORBIDDEN_PRODUCT)
					forbiddenProductBRs.add(br);

			}

			Set<String> forbiddenProductPairIds = new Set<String>();

			// 1.- Get all forbidden product pairs for the target:
			for (BI_PL_Business_rule__c br : forbiddenProductBRs) {
				String productPairKey = new BI_PL_PreparationExt.PlanitProductPair(br.BI_PL_Product__r, br.BI_PL_Secondary_product__r).Id;

				if (br.BI_PL_Specialty__c == targetSpecialty)
					forbiddenProductPairIds.add(productPairKey);
			}

			Map<Id, BI_PL_Preparation__c> preparationsByPositionId = new Map<Id, BI_PL_Preparation__c>();

			for (BI_PL_Preparation__c p : [SELECT Id, Name, BI_PL_Position_cycle__r.BI_PL_Position__c, BI_PL_Position_cycle__r.BI_PL_Position_name__c, Owner.Name, BI_PL_Position_cycle__r.BI_PL_Confirmed__c  FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__r.BI_PL_Position__c IN :positionsId AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId]) {
				preparationsByPositionId.put(p.BI_PL_Position_cycle__r.BI_PL_Position__c, p);
			}

			System.debug('primaryAndSecondaryBRs ' + primaryAndSecondaryBRs);

			/*
			// 2.- Add the products that are available
			for (BI_PL_Business_rule__c br : primaryAndSecondaryBRs) {
				String productPairKey = new BI_PL_PreparationExt.PlanitProductPair(br.BI_PL_Product__r, br.BI_PL_Secondary_product__r).Id;

				// If the BR product is currently added to the target and it isn't a forbidden one:
				if (isProductInList(productPairKey, new List<String>(currentProductPairs)) && !forbiddenProductPairIds.contains(productPairKey) && br.BI_PL_Position__c != currentPositionId) {
					if(br.BI_PL_Position__c != currentPositionId){
						if (!preparationsByProduct.containsKey(productPairKey))
							preparationsByProduct.put(productPairKey, new List<BI_PL_Preparation__c>());

						preparationsByProduct.get(productPairKey).add(preparationsByPositionId.get(br.BI_PL_Position__c));
					}
				}
			}*/

			// 2.- For each product pair add the positions having them:
			List<Id> positions = new List<Id>();
			Map<Id, Set<String>> productPairsByPositionId = new Map<Id, Set<String>>();

			for (BI_PL_Business_rule__c br : primaryAndSecondaryBRs) {
				if (currentPositionId != br.BI_PL_Position__c) {
					positions.add(br.BI_PL_Position__c);

					if (!productPairsByPositionId.containsKey(br.BI_PL_Position__c))
						productPairsByPositionId.put(br.BI_PL_Position__c, new Set<String>());

					productPairsByPositionId.get(br.BI_PL_Position__c).add(new BI_PL_PreparationExt.PlanitProductPair(br.BI_PL_Product__r, br.BI_PL_Secondary_product__r).Id);
				}
			}

			//if (hasAllElements(productPairsByPositionId.get(positionId), currentProductPairs)) {
			for (String productPairKey : currentProductPairs) {
				Set<String> alreadyAddedIds = new Set<String>();

				if(!forbiddenProductPairIds.contains(productPairKey)){
					if (!preparationsByProduct.containsKey(productPairKey))
						preparationsByProduct.put(productPairKey, new List<BI_PL_Preparation__c>());

					for (Id positionId : positions) {
						if(!alreadyAddedIds.contains(positionId) && productPairsByPositionId.get(positionId).contains(productPairKey)){
							preparationsByProduct.get(productPairKey).add(preparationsByPositionId.get(positionId));
							alreadyAddedIds.add(positionId);
						}
					}
				}
			}
			//}


		} else {
			// ALLOWED PRODUCTS ARE FOUND BY SPECIALTY "CUSTOMER SPECIALTY"
			// The products already added to the target will be available in the target Position (Preparation), because
			// the restrictions are applied by target Specialty (no Position involved).

			for (BI_PL_Preparation__c preparation : [SELECT Id, Name, BI_PL_Position_cycle__r.BI_PL_Position_name__c FROM BI_PL_Preparation__c WHERE BI_PL_Country_code__c = :countryCode AND BI_PL_Position_cycle__r.BI_PL_Position__c != :currentPositionId AND BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId]) {
				for (String productPairId : currentProductPairs) {
					if (!preparationsByProduct.containsKey(productPairId))
						preparationsByProduct.put(productPairId, new List<BI_PL_Preparation__c>());

					preparationsByProduct.get(productPairId).add(preparation);
				}
			}
		}

		return preparationsByProduct;
	}

	private static Boolean hasAllElements(Set<String> a, Set<String> b) {
		return a.containsAll(b);
	}

	@RemoteAction
	public static void saveRequests(List<BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper> requests) {
		BI_PL_TransferAndShareUtility.saveRequests(requests);
	}

	@RemoteAction
	public static List<PicklistOption> getActionTypes() {
		List<PicklistOption> pickListValuesList = new List<PicklistOption>();
		Schema.DescribeFieldResult fieldResult = BI_PL_Preparation_action__c.BI_PL_Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for ( Schema.PicklistEntry pickListVal : ple) {
			if (pickListVal.getValue() == BI_PL_TransferAndShareUtility.SHARE || pickListVal.getValue() == BI_PL_TransferAndShareUtility.TRANSFER)
				pickListValuesList.add(new PicklistOption(pickListVal.getValue(), pickListVal.getLabel()));
		}

		return pickListValuesList;
	}

	@RemoteAction
	public static Map<String, Integer> getNumberOfActionsByType(String sourceid) {
		Map<String, Integer> retMap = new Map<String, Integer>();

		if (sourceid != null) {
			for (BI_PL_Preparation_action__c action : [SELECT Id, BI_PL_Source_preparation__c, BI_PL_Type__c
			        FROM BI_PL_Preparation_action__c
			        WHERE BI_PL_Source_preparation__c = :sourceid]) {
				//AND  BI_PL_All_approved__c = true

				//if(action.BI_PL_Type__c == 'Transfer') {
				if (retMap.containsKey(action.BI_PL_Type__c)) {
					Integer value = Integer.valueOf(retMap.get(action.BI_PL_Type__c)) + 1;
					retMap.put(action.BI_PL_Type__c, value);
				} else {
					retMap.put(action.BI_PL_Type__c, 1);
				}
				//}
			}
		}

		return retMap;
	}

	@RemoteAction
	public static List<PicklistOption> getAddedReasonOptions(){
		List<PicklistOption> pickListValuesList = new List<PicklistOption>();
		Schema.DescribeFieldResult fieldResult = BI_PL_Preparation_action__c.BI_PL_Added_reason__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for ( Schema.PicklistEntry pickListVal : ple) {
			pickListValuesList.add(new PicklistOption(pickListVal.getValue(), pickListVal.getLabel()));
		}

		return pickListValuesList; 
	}

	class PicklistOption {
		public String label;
		public String value;

		public PicklistOption (String value, String label) {
			this.label = label;
			this.value = value;
		}
	}

	class RelatedDetailWrapper {
		public Id targetId;
		public String positionName;
		public String preparationId;
		public Id primaryProductId;
		public Id secondaryProductId;
		public Decimal adjustedDetails;

		public RelatedDetailWrapper (Id targetId, String positionName, String preparationId, Id primaryProductId, Id secondaryProductId, Decimal adjustedDetails) {
			this.targetId = targetId;
			this.positionName = positionName;
			this.preparationId = preparationId;
			this.primaryProductId = primaryProductId;
			this.secondaryProductId = secondaryProductId;
			this.adjustedDetails = adjustedDetails;
		}
	}

}