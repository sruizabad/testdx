public without sharing class BI_PL_TargetReasonCountCtrl {
	public static final String DROP_KEY ='DROP';
	public static final String ADD_KEY ='ADD';
	public static final String SHARE_KEY ='SHARE';
	public static final String TRANSFER_KEY ='TRANSFER';
    public static final String NO_REASON ='No Reason';
    public BI_PL_TargetReasonCountCtrl() {

    }

    @RemoteAction
    public static Map<String, Map<String, CountTarget>> getAddedRemovedTargetCount(List<Id> preparationIds)
    {
    	
        Map<Id, BI_PL_Target_Preparation__c> targets = new Map<Id, BI_PL_Target_Preparation__c>([SELECT Id, CreatedDate, BI_PL_Top_parent__c, BI_PL_Primary_Parent__c, BI_PL_NTL_Value__c, BI_PL_Specialty__c,
                BI_PL_Target_Customer__c, BI_PL_Target_Customer__r.Name, BI_PL_Target_Customer__r.External_ID_vod__c,
                BI_PL_External_ID__c,
                BI_PL_Target_Customer__r.PersonMobilePhone, BI_PL_Target_Customer__r.Phone, BI_PL_Target_Customer__r.Fax, BI_PL_Added_reason__c,
                BI_PL_Added_Manually__c, BI_PL_Parent_External_Id__c
                FROM BI_PL_Target_Preparation__c
                WHERE BI_PL_Header__c IN :preparationIds
                                        ORDER BY Id ASC]);


        Map<String, Map<String, CountTarget>> addedRemovedTargetCount = new Map<String, Map<String, CountTarget>>();

        for (BI_PL_Channel_detail_preparation__c tgtChannel : [SELECT Id, BI_PL_Target__r.BI_PL_Specialty__c, BI_PL_Channel__c, BI_PL_Max_adjusted_interactions__c, BI_PL_Max_adjusted_interactions_input__c, BI_PL_Max_planned_interactions__c,
                BI_PL_Edited__c, BI_PL_Reviewed__c, BI_PL_Rejected__c, BI_PL_Rejected_Reason__c,
                BI_PL_Target__c, BI_PL_Target_account__c,BI_PL_Target__r.BI_PL_Added_manually__c, BI_PL_Target__r.BI_PL_Added_reason__c, BI_PL_Target__r.BI_PL_Target_customer__r.Name,BI_PL_Target__r.BI_PL_Target_customer__r.FirstName,BI_PL_Target__r.BI_PL_Target_customer__r.LastName,BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c,BI_PL_Target__r.BI_PL_Target_customer__r.External_id_vod__c,BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c,BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__c,
                BI_PL_Sum_adjusted_interactions__c, BI_PL_Sum_planned_interactions__c, BI_PL_Removed__c, BI_PL_Removed_reason__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Field_force__c, BI_PL_Target__r.BI_PL_HCP__c, BI_PL_Target__r.BI_PL_HCO__c 
                FROM BI_PL_Channel_detail_preparation__c
                WHERE BI_PL_Target__c IN :targets.keySet() AND (BI_PL_Target__r.BI_PL_Added_manually__c = true OR BI_PL_Removed__c = true)]) {

        	if(tgtChannel.BI_PL_Target__r.BI_PL_Added_manually__c)
        	{
        		String addedReason = '';
                if(tgtChannel.BI_PL_Target__r.BI_PL_Added_reason__c != null && tgtChannel.BI_PL_Target__r.BI_PL_Added_reason__c != '')
                    addedReason = tgtChannel.BI_PL_Target__r.BI_PL_Added_reason__c;
                else
                    addedReason = NO_REASON;


        		if(!addedRemovedTargetCount.containsKey(ADD_KEY))
        		{
        			addedRemovedTargetCount.put(ADD_KEY, new  Map<String, CountTarget>());
        		}
        		
        		if(!addedRemovedTargetCount.get(ADD_KEY).containsKey(addedReason))
        		{
        			CountTarget data = new CountTarget(addedReason,1);
        			addedRemovedTargetCount.get(ADD_KEY).put(addedReason, data);
        			
        		}
        		else
        		{
        			addedRemovedTargetCount.get(ADD_KEY).put(addedReason, new CountTarget(addedReason, addedRemovedTargetCount.get(ADD_KEY).get(addedReason).getCount()+1));
        		}

        	}

        	if(tgtChannel.BI_PL_Removed__c)
        	{
        		String removedReason = '';
                if(tgtChannel.BI_PL_Removed_reason__c != null && tgtChannel.BI_PL_Removed_reason__c != '')
                    removedReason = tgtChannel.BI_PL_Removed_reason__c;
                else
                    removedReason = NO_REASON;

        		if(!addedRemovedTargetCount.containsKey(DROP_KEY))
        		{
        			addedRemovedTargetCount.put(DROP_KEY, new  Map<String, CountTarget>());
        		}
        		
        		if(!addedRemovedTargetCount.get(DROP_KEY).containsKey(removedReason))
        		{
        			CountTarget data = new CountTarget(removedReason,1);
        			addedRemovedTargetCount.get(DROP_KEY).put(removedReason, data);
        			
        		}
        		else
        		{
        			addedRemovedTargetCount.get(DROP_KEY).put(removedReason, new CountTarget(removedReason, addedRemovedTargetCount.get(DROP_KEY).get(removedReason).getCount()+1));
        		}

        	}




        }
        return addedRemovedTargetCount;
    }

    @RemoteAction
    public static Map<String, Map<String, CountTarget>> getTransferShareTargetCount(List<Id> preparationIds)
    {
        Map<String, Map<String, CountTarget>> shareTransferTargetCount = new Map<String, Map<String, CountTarget>>();
        for (BI_PL_Preparation_action__c item : [SELECT Id,
            CreatedDate,
            BI_PL_Account__c,
            BI_PL_Type__c,
            BI_PL_Added_reason__c,
            BI_PL_Channel__c,
            BI_PL_Source_preparation__c,
            BI_PL_Source_preparation__r.Name,
            BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
            BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position__c

            FROM BI_PL_Preparation_action__c
            WHERE BI_PL_Source_preparation__c IN :preparationIds  AND (BI_PL_Type__c = 'Share' OR BI_PL_Type__c = 'Transfer')
                    ORDER BY CreatedDate DESC]) {

            String reason = '';
            if(item.BI_PL_Added_reason__c != null && item.BI_PL_Added_reason__c != '')
                reason = item.BI_PL_Added_reason__c;
            else
                reason = NO_REASON;

            if(item.BI_PL_Type__c == 'Share')
            {
                

                if(!shareTransferTargetCount.containsKey(SHARE_KEY))
                {
                    shareTransferTargetCount.put(SHARE_KEY, new  Map<String, CountTarget>());
                }
                
                if(!shareTransferTargetCount.get(SHARE_KEY).containsKey(reason))
                {
                    CountTarget data = new CountTarget(reason,1);
                    shareTransferTargetCount.get(SHARE_KEY).put(reason, data);
                    
                }
                else
                {
                    shareTransferTargetCount.get(SHARE_KEY).put(reason, new CountTarget(reason, shareTransferTargetCount.get(SHARE_KEY).get(reason).getCount()+1));
                }

            }

            if(item.BI_PL_Type__c == 'Transfer')
            {
                
                if(!shareTransferTargetCount.containsKey(TRANSFER_KEY))
                {
                    shareTransferTargetCount.put(TRANSFER_KEY, new  Map<String, CountTarget>());
                }
                
                if(!shareTransferTargetCount.get(TRANSFER_KEY).containsKey(reason))
                {
                    CountTarget data = new CountTarget(reason,1);
                    shareTransferTargetCount.get(TRANSFER_KEY).put(reason, data);
                    
                }
                else
                {
                    shareTransferTargetCount.get(TRANSFER_KEY).put(reason, new CountTarget(reason, shareTransferTargetCount.get(TRANSFER_KEY).get(reason).getCount()+1));
                }

            }



        }
        return shareTransferTargetCount;

    }

    public class CountTarget{
    	String reason {get; set;}
    	Integer count {get; set;}

    	public CountTarget(String reason, Integer count)
    	{
    		this.reason = reason;
    		this.count = count;
    	}

    	public Integer getCount()
    	{
    		return count;
    	}



    }


}