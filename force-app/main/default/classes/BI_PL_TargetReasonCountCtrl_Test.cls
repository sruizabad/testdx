@isTest
private class BI_PL_TargetReasonCountCtrl_Test{

    private static String userCountryCode;
    private static String hierarchy = 'hierarchyTest';

    private static Date startDate;
    private static Date endDate;
    private static List<BI_PL_Position_Cycle__c> posCycles;
    public static List<Product_vod__c> listProd;



    @isTest static void test(){
        BI_PL_TargetReasonCountCtrl controller = new BI_PL_TargetReasonCountCtrl();
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        userCountryCode = userCountry.Country_Code_BI__C;
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);


            List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
            listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
            System.debug('prods*+*'+listProd);

            BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
            List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
            
                     
            List<BI_PL_Preparation__c> preps = BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

            List<Id> prepIds = new List<Id>();
            for(BI_PL_Preparation__c prep : preps)
            {
                prepIds.add(prep.Id);
            }
            


            BI_PL_TargetReasonCountCtrl.getAddedRemovedTargetCount(prepIds);
            BI_PL_TargetReasonCountCtrl.getTransferShareTargetCount(prepIds);
        
    
        

    }


}