@IsTest
public class BI_PL_TestDataUtility {
	

	public static void createCustomSettings(){
		//try{
			//List<Knipper_Settings__c> ksett = new List<Knipper_Settings__c>([SELECT Id FROM Knipper_Settings__c WHERE External_ID__c =: UserInfo.getProfileId()]);

       		//if(ksett.isEmpty())
            	upsert new Knipper_Settings__c(
            			Account_Detect_Change_FieldList__c='FirstName,Middle_vod__c',
            			Account_Detect_Changes_Record_Type_List__c='Professional_vod',
            			External_ID__c = UserInfo.getProfileId(),
            			SetupOwnerId = Userinfo.getOrganizationId()
            	) External_ID__c;
            //}
        //} catch(Exception e){
        //	System.debug(loggingLevel.Error, '*** BI_PL_TestDataUtility error creating Knipper_Settings__c: ' + e.getMessage());
        //}
	}

	/**
	 * Creates accounts.
	 *
	 * @return     List of created accounts
	 */
	public static List<Account> createAccounts(String countryCode) {
		List<Account> accs = new List<Account> {
			new Account(
	        	Name = 'TestAccount1',
	       		External_Id_vod__c = 'TestAccount1_Ext_Id',
	       		Country_Code_BI__c = countryCode
    		),
			new Account(
	        	Name = 'TestAccount2',
	        	External_Id_vod__c = 'TestAccount2_Ext_Id',
	        	Country_Code_BI__c = countryCode
    		)
		};

        upsert accs External_Id_vod__c;
        return accs;
	}

	/**
	 * Creates products.
	 *
	 * @return     List of created records
	 */
	public static List<Product_vod__c> createProducts() {
		List<Product_vod__c> products = new List<Product_vod__c> {
			new Product_vod__c(
	        	Name = 'Spiriva',
	        	RecordTypeId = Schema.SObjectType.Product_vod__c.getRecordTypeInfosByName().get('Global').getRecordTypeId(),
	        	Product_Type_vod__c = 'Detail',
	        	External_ID_vod__c = 'Spiriva_ExtId'
	        ),
	        new Product_vod__c(
	        	Name = 'Pradaxa',
	        	Product_Type_vod__c = 'Detail',
	        	External_ID_vod__c = 'Pradaxa_ExtId'
	        )
		};		

        insert products;
        return products;
	}

	public static List<BI_PL_Position_Cycle__c> createHierarchy(String countryCode, Date startdate, Date endDate, BI_TM_FF_type__c fieldForce, String hierarchy, Boolean usesBITMAN, Integer iterations){

		List<BI_PL_Position_Cycle__c> posCycle = new List<BI_PL_Position_Cycle__c>();

		BI_PL_Cycle__c	cycle = new BI_PL_Cycle__c(
        	BI_PL_Country_code__c = countryCode,
        	BI_PL_Start_date__c = startDate,
        	BI_PL_End_date__c = endDate,
        	BI_PL_External_id__c = 'BR_Cycle_Test_Ext_Id',
        	BI_PL_Field_force__c = fieldForce.Id
        );

        insert cycle;

        BI_PL_Position__c position = new BI_PL_Position__c(
        	Name = 'testPosition',
        	BI_PL_Country_code__c = countryCode,
        	BI_PL_Field_force__c = usesBITMAN ? fieldForce.Name : 'Cardiology'
        );

        insert position;

        BI_PL_Position_cycle__c positionCycle = new BI_PL_Position_cycle__c(
        	BI_PL_Cycle__c = cycle.Id,
        	BI_PL_External_id__c = cycle.BI_PL_External_id__c+'_'+hierarchy+'_testPosition',
        	BI_PL_Hierarchy__c = hierarchy,
        	BI_PL_Position__c = position.Id
        );

        insert positionCycle;
        posCycle.add(positionCycle);

        User currentUser = [SELECT Id, Name, Country_Code_BI__c, UserName, External_id__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = positionCycle.Id, BI_PL_User__c = currentUser.Id);

		//pcu.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycle, positionCycle.BI_PL_Hierarchy__c, position.Name,currentUser.External_id__c);

		insert pcu;

		return posCycle;
	}

	public static List<BI_PL_Preparation__c> createPreparations(String countryCode, List<BI_PL_Position_Cycle__c> posCycles, List<Account> accs, List<Product_vod__c> products){
		List<BI_PL_Preparation__c> preps 					= new List<BI_PL_Preparation__c>();
		List<BI_PL_Target_preparation__c> targets 			= new List<BI_PL_Target_preparation__c>();
		List<BI_PL_Channel_detail_preparation__c> channels 	= new List<BI_PL_Channel_detail_preparation__c>();
		List<BI_PL_Detail_preparation__c> details 			= new List<BI_PL_Detail_preparation__c>();

		for(BI_PL_Position_Cycle__c pc : posCycles) {

			BI_PL_Preparation__c prep = new BI_PL_Preparation__c(
				BI_PL_Country_code__c = countryCode,
			    BI_PL_Position_cycle__c = pc.Id,
			    BI_PL_Status__c = 'Under Review',
			    BI_PL_External_Id__c = pc.BI_PL_External_ID__c
			);

			preps.add(prep);

			Integer i = 0;
			for(Account acc : accs) {

				BI_PL_Target_preparation__c tgtPrep = new BI_PL_Target_preparation__c(
	    			BI_PL_Target_customer__r = new Account(External_ID_vod__c = acc.External_Id_vod__c),
				  	BI_PL_Header__r = new BI_PL_Preparation__c(BI_PL_External_id__c = pc.BI_PL_External_ID__c),
				  	BI_PL_External_Id__c = prep.BI_PL_External_Id__c + '_' + acc.External_Id_vod__c
				);

				BI_PL_Channel_detail_preparation__c chanDetail = new BI_PL_Channel_detail_preparation__c(
					BI_PL_Channel__c = 'rep_detail_only',
		    		BI_PL_Target__r = new BI_PL_Target_preparation__c(BI_PL_External_Id__c = tgtPrep.BI_PL_External_Id__c),
		    		BI_PL_Reviewed__c = true,
		    		BI_PL_Edited__c = true,
		    		BI_PL_External_Id__c = tgtPrep.BI_PL_External_Id__c + '_rep_detail_only_'
				);
			



				for(Product_vod__c prod : products) {
					BI_PL_Detail_preparation__c detail = new BI_PL_Detail_preparation__c(
						BI_PL_Adjusted_details__c = 1,
					    BI_PL_External_id__c = chanDetail.BI_PL_External_Id__c + '_' + prod.External_ID_vod__c+'_'+i,
					    BI_PL_Planned_details__c = 5,
					    BI_PL_Segment__c = 'No Segmentation',
					    //BI_PL_Channel_detail__c = tc1.Id,
					    //BI_PL_Product__c = product1.Id
					    BI_PL_Channel_detail__r = new BI_PL_Channel_detail_preparation__c(BI_PL_External_Id__c = chanDetail.BI_PL_External_Id__c),
					    BI_PL_Product__r = new Product_vod__c(External_ID_vod__c = prod.External_ID_vod__c)
					);
					i++;
					details.add(detail);
				}

				targets.add(tgtPrep);
				channels.add(chanDetail);
			}
		}
		upsert preps BI_PL_External_id__c;
		upsert targets BI_PL_External_id__c;
		upsert channels BI_PL_External_id__c;
		upsert details BI_PL_External_id__c;

		return preps;
	}

	/**
	 * Creates field forces.
	 *
	 * @return     The list of created records
	 */
	public static List<BI_TM_FF_type__c> createFieldForces(){
		List<BI_TM_FF_type__c> ffs = new List<BI_TM_FF_type__c> {
			new BI_TM_FF_type__c(
        		BI_TM_Country_Code__c = 'BR',
        		Name = 'DI',
        		BI_TM_FF_type_description__c = 'Cardiology'
    		),
    		new BI_TM_FF_type__c(
        		BI_TM_Country_Code__c = 'US',
        		Name = 'DI',
        		BI_TM_FF_type_description__c = 'Cardiology'
    		)
        };

        insert ffs;
        return ffs;
	}

	public static void createAll(){

	}
}