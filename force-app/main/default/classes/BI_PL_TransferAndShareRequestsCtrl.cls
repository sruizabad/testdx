/**
 *	Retrieves the necesary data for the BI_PL_TransferAndShareRequests component.
 *
 *	@author	OMEGA CRM
 */
public without sharing class BI_PL_TransferAndShareRequestsCtrl {

	public static final String VALIDATION_ERROR_SOURCE_SUBMITTED = 'source_submitted';
	public static final String VALIDATION_ERROR_TARGET_SUBMITTED = 'target_submitted';
	public static final String VALIDATION_ERROR_ACTION_CANCELLED = 'action_cancelled';


	/**
	 *	Returns the requests for the preparations specified.
	 */
	@ReadOnly
	@RemoteAction
	public static Map<String, BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper> getRequests(List<String> sourcePreparationsId, String channel) {

		Set<String> preparationIds = new Set<String>(sourcePreparationsId);

		// Key = Source preparation Id.
		Map<String, BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper> output = new Map<String, BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper>();

		Set<Id> relatedActions = new Set<Id>();

		if (sourcePreparationsId.size() == 1) {
			for (BI_PL_Preparation_action_item__c item : [SELECT BI_PL_Parent__c FROM BI_PL_Preparation_action_item__c
			        WHERE BI_PL_Parent__r.BI_PL_Channel__c = : channel
			                AND (BI_PL_Parent__r.BI_PL_Source_preparation__c = :sourcePreparationsId.get(0)
			                        OR BI_PL_Target_preparation__c = :sourcePreparationsId.get(0))]) {
				relatedActions.add(item.BI_PL_Parent__c);
			}
		} else {

			for (BI_PL_Preparation_action_item__c item : [SELECT BI_PL_Parent__c FROM BI_PL_Preparation_action_item__c
			        WHERE BI_PL_Parent__r.BI_PL_Channel__c = : channel
			                AND (BI_PL_Parent__r.BI_PL_Source_preparation__c IN :preparationIds
			                     OR BI_PL_Target_preparation__c IN :preparationIds)]) {
				relatedActions.add(item.BI_PL_Parent__c);
			}
		}


		for (BI_PL_Preparation_action_item__c item : [SELECT Id,
		        BI_PL_Parent__r.CreatedDate,
		        BI_PL_Parent__r.BI_PL_Source_preparation__c,
		        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
		        BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name,
		        BI_PL_Parent__r.BI_PL_Account__c,
		        BI_PL_Parent__r.BI_PL_Account__r.Name,
		        BI_PL_Parent__r.BI_PL_Type__c,
		        BI_PL_Parent__r.BI_PL_All_approved__c,
		        BI_PL_Parent__r.BI_PL_Someone_rejected__c,
		        BI_PL_Parent__r.BI_PL_Canceled__c,
		        BI_PL_Parent__r.BI_PL_Channel__c,
		        BI_PL_Parent__r.BI_PL_Added_reason__c,

		        BI_PL_Product__r.Name,
		        BI_PL_Secondary_product__r.Name,
		        BI_PL_Target_adjustment__c,
		        BI_PL_Rejected_reason__c,
		        BI_PL_Request_response__c,

		        BI_PL_Target_preparation_owner__c,
		        BI_PL_Target_preparation_owner_name__c,

		        BI_PL_Status__c,
		        BI_PL_Target_preparation__c,
		        BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c

		        FROM BI_PL_Preparation_action_item__c
		        WHERE BI_PL_Parent__c IN :relatedActions
		        ORDER BY BI_PL_Parent__r.CreatedDate DESC, BI_PL_Target_preparation__c]) {

			BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper action;

			if (!output.containsKey(item.BI_PL_Parent__c)) {
				action = new BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper(item.BI_PL_Parent__r);
				output.put(item.BI_PL_Parent__c, action);
			} else {
				action = output.get(item.BI_PL_Parent__c);
			}

			output.get(item.BI_PL_Parent__c).addDetail(new BI_PL_TransferAndShareUtility.TransferAndShareDetailRequestWrapper(item, !preparationIds.contains(item.BI_PL_Target_preparation__c), action, isInbound(item, preparationIds)));
		}

		return output;
	}

	@RemoteAction
	public static SaveRequestOutput saveDetailRequests(BI_PL_TransferAndShareUtility.TransferAndShareDetailRequestWrapper[] details, List<String> sourcePreparationsId) {

		Set<String> preparationIds = new Set<String>(sourcePreparationsId);

		try {
			if (details.size() > 0) {
				List<BI_PL_Preparation_action_item__c> items = new List<BI_PL_Preparation_action_item__c>();

				Id actionId = details.get(0).actionId;
				Id preparationId = details.get(0).toPreparation;
				Id accountId = details.get(0).accountId;

				for (BI_PL_TransferAndShareUtility.TransferAndShareDetailRequestWrapper d : details)
					items.add(d.record);

				update items;

				// If the request adds the targets to our preparation:
				if (isInbound(items.get(0), preparationIds)) {
					// Check if updating the items makes the action to be approved:
					BI_PL_Preparation_action__c action = [SELECT Id, BI_PL_All_approved__c FROM BI_PL_Preparation_action__c WHERE Id = :actionId];

					if (action.BI_PL_All_approved__c) {
						BI_PL_Target_preparation__c[] targets = [SELECT Id FROM BI_PL_Target_preparation__c
						                                        WHERE BI_PL_Header__c = :preparationId AND BI_PL_Target_customer__c = : accountId];
						if (targets.size() == 1) {
							return new SaveRequestOutput(new List<String> {targets.get(0).Id}, null);
						}
					}
				}
			}
			return new SaveRequestOutput(null, null);
		} catch (exception e) {			
			return getValidationErrorMessage(e, details[0].record.BI_PL_Request_response__c);
		}
		return new SaveRequestOutput(null, Label.BI_PL_Unexpected_error);
	}
	@RemoteAction
	public static SaveRequestOutput saveActionRequest(BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper action, String channel) {
		try {
			update action.record;
			return new SaveRequestOutput(null, null);
		} catch (exception e) {
			return getValidationErrorMessage(e,action.record.BI_PL_Type__c);
		}

		return new SaveRequestOutput(null, Label.BI_PL_Unexpected_error);
	}

	public class SaveRequestOutput {
		public List<String> result;
		public String error;

		public SaveRequestOutput(List<String> result, String error) {
			this.result = result;
			this.error = error;
		}
	}

	/**
	 *	Returns true if the detail is an inbound action item.
	 * 	- Inbound = true  (the targets will be added to the source preparation)
	 * 	- Inbound = false (the targets will be added to the target preparation)
	 */
	private static Boolean isInbound(BI_PL_Preparation_action_item__c item, Set<String> sourcePreparationsId) {
		//return item.BI_PL_Parent__r.BI_PL_Type__c == BI_PL_PreparationActionUtility.REQUEST && sourcePreparationsId.contains(item.BI_PL_Parent__r.BI_PL_Source_preparation__c) ||
		//       (item.BI_PL_Parent__r.BI_PL_Type__c == BI_PL_PreparationActionUtility.TRANSFER || item.BI_PL_Parent__r.BI_PL_Type__c == BI_PL_PreparationActionUtility.SHARE) && sourcePreparationsId.contains(item.BI_PL_Target_preparation__c) ;
		return (item.BI_PL_Parent__r.BI_PL_Type__c == BI_PL_PreparationActionUtility.REQUEST && sourcePreparationsId.contains(item.BI_PL_Target_preparation__c)) || ((item.BI_PL_Parent__r.BI_PL_Type__c == BI_PL_PreparationActionUtility.TRANSFER || item.BI_PL_Parent__r.BI_PL_Type__c == BI_PL_PreparationActionUtility.SHARE) && sourcePreparationsId.contains(item.BI_PL_Target_preparation__c));
	}

	public static SaveRequestOutput getValidationErrorMessage(Exception e, String t){
		if (e.getMessage().contains(VALIDATION_ERROR_SOURCE_SUBMITTED))
			return new SaveRequestOutput(null, Label.BI_PL_Source_shouldnt_submitted);
		if (e.getMessage().contains(VALIDATION_ERROR_TARGET_SUBMITTED))
			return new SaveRequestOutput(null, Label.BI_PL_Target_shouldnt_submitted);
		if (e.getMessage().contains(VALIDATION_ERROR_ACTION_CANCELLED))
			return new SaveRequestOutput(null, Label.BI_PL_Action_Cancelled + ' : ' + t);

		BI_PL_ErrorHandlerUtility.addPlanitServerError('BI_PL_TransferAndShareRequests', BI_PL_ErrorHandlerUtility.FUNCTIONALITY_TRANSFER_AND_SHARE, e.getMessage(), e.getStackTraceString());
		return new SaveRequestOutput(null, e.getMessage());
	}

}