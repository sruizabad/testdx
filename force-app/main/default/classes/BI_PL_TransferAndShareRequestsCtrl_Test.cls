@isTest
private class BI_PL_TransferAndShareRequestsCtrl_Test {

	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account ORDER BY Name];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        List<BI_PL_Preparation__c> plans = BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

        BI_PL_TestDataFactory.createPlanitViews(userCountryCode);
        //BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);


        BI_PL_Preparation_Action__c action = BI_PL_TestDataFactory.createTestPreparationAction(listAcc.get(0).Id,plans.get(0), plans.get(1), 'rep_detail_only', true);

    }
	
	@isTest static void testGetRequests() {
		// Implement test code
        List<BI_PL_Preparation__c> plans = [SELECT Id FROM BI_PL_Preparation__c];

        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account ORDER BY Name];

		Map<String, BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper> requests = BI_PL_TransferAndShareRequestsCtrl.getRequests(new List<String> { plans.get(0).Id }, 'rep_detail_only');
	}
	
	@isTest static void testSaveAction() {


        List<BI_PL_Preparation_Action_item__c> items = new List<BI_PL_Preparation_Action_item__c>([
			SELECT Id, Name,
			BI_PL_Parent__c,
			BI_PL_Parent__r.BI_PL_Account__c,
	        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
			BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
			BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name,
			BI_PL_Target_preparation_owner_name__c,
			BI_PL_Parent__r.BI_PL_Type__c
			FROM BI_PL_Preparation_Action_item__c
        ]);

        BI_PL_Preparation_Action__c action = [SELECT Id, BI_PL_Account__c,
				BI_PL_Added_reason__c,
				BI_PL_All_approved__c,
				BI_PL_Canceled__c,
				BI_PL_Channel__c,
				BI_PL_Number_of_items__c,
				BI_PL_Number_of_items_approved__c,
				BI_PL_Number_of_items_rejected__c,
				BI_PL_Someone_rejected__c,
				BI_PL_Source_preparation__c,
				BI_PL_Type__c 
			FROM BI_PL_Preparation_Action__c WHERE Id = : items.get(0).BI_PL_Parent__c LIMIT 1];

		String planId = action.BI_PL_Source_preparation__c;
		System.debug(loggingLevel.Error, '*** ACTION TEST: ' + action);

		BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper actionWrapper = new BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper(action);

        System.debug(loggingLevel.Error, '*** ACTION ITEMS TEST: ' + items);
        //Create a new detail for the action
        BI_PL_TransferAndShareUtility.TransferAndShareDetailRequestWrapper detail = new BI_PL_TransferAndShareUtility.TransferAndShareDetailRequestWrapper(items.get(0), true, actionWrapper, false);

		BI_PL_TransferAndShareRequestsCtrl.saveRequestOutput outputDetails =  BI_PL_TransferAndShareRequestsCtrl.saveDetailRequests( new List<BI_PL_TransferAndShareUtility.TransferAndShareDetailRequestWrapper> { detail }, new List<String>{ planId });


		//Cancell action
		action.BI_PL_Canceled__c = true;
		actionWrapper = new BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper(action);
        BI_PL_TransferAndShareRequestsCtrl.SaveRequestOutput output =  BI_PL_TransferAndShareRequestsCtrl.saveActionRequest(actionWrapper, 'rep_detail_only');



        //List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account ORDER BY Name];
		//Map<String, BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper> requests = BI_PL_TransferAndShareRequestsCtrl.getRequests(new List<String> { plans.get(0).Id }, 'rep_detail_only');



	}
	
}