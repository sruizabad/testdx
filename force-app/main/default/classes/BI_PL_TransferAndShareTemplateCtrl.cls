global without sharing class BI_PL_TransferAndShareTemplateCtrl {

	global String transferOrShareId{get;set;}
	public static final String TRANSFER = 'Transfer';
	public static final String SHARE = 'Share';
	public String fromPosName{get{
		TransferAndShareDetailRequestWrapper transferWrapper = getTransferAndShareWrapper();
		fromPosName = transferWrapper.fromPositionName;
		return fromPosName;
		}set;}
	public String fromOwner{get{
		TransferAndShareDetailRequestWrapper transferWrapper = getTransferAndShareWrapper();
		fromOwner = transferWrapper.fromOwnerName;
		return fromOwner;
		}set;}
	public String toPosName{get{
		TransferAndShareDetailRequestWrapper transferWrapper = getTransferAndShareWrapper();
		toPosName = transferWrapper.toPositionName;
		return toPosName;
		}set;}
	public String toOwner{get{
		TransferAndShareDetailRequestWrapper transferWrapper = getTransferAndShareWrapper();
		toOwner = transferWrapper.toOwnerName;
		return toOwner;
		}set;}
	public String transferOrShareType{get{
		TransferAndShareDetailRequestWrapper transferWrapper = getTransferAndShareWrapper();
		transferOrShareType = transferWrapper.transferType;
		return transferOrShareType;
		}set;}
	public String accountName{get{
		TransferAndShareDetailRequestWrapper transferWrapper = getTransferAndShareWrapper();
		String accountTransferId = transferWrapper.accountId;
		accountName = [SELECT Name FROM Account WHERE Id =: accountTransferId LIMIT 1].Name;
		return accountName;
		}set;}

	global BI_PL_TransferAndShareTemplateCtrl(){
	}

	global TransferAndShareDetailRequestWrapper getTransferAndShareWrapper(){
		String transferId = transferOrShareId;
		System.debug('**transferId ' + transferId);
		BI_PL_Preparation_action_item__c transferOrShareRecord = [SELECT Id, BI_PL_Parent__c, BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name, BI_PL_Target_preparation_owner_name__c, BI_PL_Target_preparation__c, BI_PL_Parent__r.BI_PL_Type__c, BI_PL_Parent__r.BI_PL_Source_preparation__c,BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c, BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c FROM BI_PL_Preparation_action_item__c WHERE Id =: transferId LIMIT 1];
		String actionId = transferOrShareRecord.BI_PL_Parent__c;
		BI_PL_Preparation_action__c transferOrShareAction = [SELECT Id, BI_PL_Account__c, BI_PL_Type__c FROM BI_PL_Preparation_action__c WHERE Id =: actionId LIMIT 1];
		TransferAndShareRequestWrapper requestWrapper = new TransferAndShareRequestWrapper(transferOrShareAction);
		TransferAndShareDetailRequestWrapper toReturn = new TransferAndShareDetailRequestWrapper(transferOrShareRecord, false, requestWrapper, false);

		return toReturn;
	}

	global class TransferAndShareRequestWrapper {
		global Boolean isInbound = false;
		global BI_PL_Preparation_action__c record;
		global List<TransferAndShareDetailRequestWrapper> details = new List<TransferAndShareDetailRequestWrapper>();

		global TransferAndShareRequestWrapper (BI_PL_Preparation_action__c record) {
			this.record = record;
		}
	}

	global class TransferAndShareDetailRequestWrapper {
		global BI_PL_Preparation_action_item__c record;
		global Boolean readOnly;

		global Id fromPreparation;
		global String fromPositionName;
		global String fromOwnerName;

		public Id toPreparation;
		global String toPositionName;
		global String toOwnerName;

		global String transferType;

		global Id actionId;
		global Id accountId;

		global Boolean isInbound;

		global TransferAndShareDetailRequestWrapper (BI_PL_Preparation_action_item__c record, Boolean readOnly, TransferAndShareRequestWrapper action, Boolean isInbound) {
			
			this.isInbound = isInbound;

			this.actionId = action.record.Id;
			this.accountId = action.record.BI_PL_Account__c;

			this.record = record;
			this.readOnly = readOnly;

			this.transferType = action.record.BI_PL_Type__c;

			fromPositionName = getFromPositionName(record);
			toPositionName = getToPositionName(record);
			fromOwnerName = getFromOwnerName(record);
			toOwnerName = getToOwnerName(record);

			if (isTransferOrShare(record)) {
				fromPreparation = record.BI_PL_Parent__r.BI_PL_Source_preparation__c;
				toPreparation = record.BI_PL_Target_preparation__c;
			} else {
				fromPreparation = record.BI_PL_Target_preparation__c;
				toPreparation = record.BI_PL_Parent__r.BI_PL_Source_preparation__c;
			}
		}
	}

	global static String getFromPositionName(BI_PL_Preparation_action_item__c item) {
		if (isTransferOrShare(item)) {
			return item.BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
		} else {
			return item.BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
		}
	}
	global static String getToPositionName(BI_PL_Preparation_action_item__c item) {
		if (isTransferOrShare(item)) {
			return item.BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
		} else {
			return item.BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
		}
	}
	global static String getFromOwnerName(BI_PL_Preparation_action_item__c item) {
		if (isTransferOrShare(item)) {
			return item.BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name;
		} else {
			return item.BI_PL_Target_preparation_owner_name__c;
		}
	}
	global static String getToOwnerName(BI_PL_Preparation_action_item__c item) {
		if (isTransferOrShare(item)) {
			return item.BI_PL_Target_preparation_owner_name__c;
		} else {
			return item.BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name;
		}
	}

	private static Boolean isTransferOrShare(BI_PL_Preparation_action_item__c item) {
		return item.BI_PL_Parent__r.BI_PL_Type__c == TRANSFER || item.BI_PL_Parent__r.BI_PL_Type__c == SHARE;
	}
}