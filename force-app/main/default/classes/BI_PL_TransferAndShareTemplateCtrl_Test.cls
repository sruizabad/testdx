@isTest
private class BI_PL_TransferAndShareTemplateCtrl_Test {
	
	@isTest static void test_method_one() {
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
		String countryCode = testUser.Country_Code_BI__c;
		
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting();
		BI_PL_TestDataFactory.createTestAccounts(3, countryCode);
		BI_PL_TestDataFactory.createTestProduct(4, countryCode);
		BI_PL_TestDataFactory.createCycleStructure(countryCode);
		
		List<Account> accList = [SELECT Id, External_ID_vod__c FROM Account];
		List<Product_vod__c> proList = [SELECT Id,External_ID_vod__c FROM Product_vod__c];
		BI_PL_Cycle__c cycle = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];
		List<BI_PL_Position_Cycle__c> listPosCycle = [SELECT Id,BI_PL_External_id__c FROM BI_PL_Position_Cycle__c];
		
		BI_PL_TestDataFactory.createPreparations(countryCode, listPosCycle, accList, proList);
		
		List<BI_PL_Preparation__c> prepList = [SELECT Id FROM BI_PL_Preparation__c];
		
		BI_PL_Preparation_action__c actPrep = BI_PL_TestDataFactory.createTestPreparationAction(accList.get(0).Id, prepList.get(0), prepList.get(1), 'rep_detail_only', true); 
		BI_PL_Preparation_action_item__c actItem = [SELECT Id, BI_PL_Parent__c, BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name, 
		BI_PL_Target_preparation_owner_name__c, BI_PL_Target_preparation__c, BI_PL_Parent__r.BI_PL_Type__c, BI_PL_Parent__r.BI_PL_Source_preparation__c,
		BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c, 
		BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c 
		FROM BI_PL_Preparation_action_item__c LIMIT 1];
		
		BI_PL_TransferAndShareTemplateCtrl controller = new BI_PL_TransferAndShareTemplateCtrl();
		controller.transferOrShareId = actItem.Id;
		String fromPosName = controller.fromPosName;
		String toPosName = controller.toPosName;
		String fromOwner = controller.fromOwner;
		String toOwner = controller.toOwner;
	}
	
}