public without sharing class BI_PL_TransferAndShareUtility {

	public static final String TRANSFER = 'Transfer';
	public static final String SHARE = 'Share';

	@RemoteAction
	public static void saveRequests(List<TransferAndShareRequestWrapper> requests) {
		System.debug('saveRequests ' + requests);

		List<BI_PL_Preparation_action__c> preparationActions = new List<BI_PL_Preparation_action__c>();
		Map<String, List<BI_PL_Preparation_action_item__c>> preparationActionItems = new Map<String, List<BI_PL_Preparation_action_item__c>>();

		for (TransferAndShareRequestWrapper request : requests) {
			BI_PL_Preparation_action__c action = request.record;
			preparationActions.add(action);
			for (TransferAndShareDetailRequestWrapper detail : request.details) {
				BI_PL_Preparation_action_item__c item = detail.record;
				if (!preparationActionItems.containsKey(action.BI_PL_Source_preparation__c))
					preparationActionItems.put(action.BI_PL_Source_preparation__c, new List<BI_PL_Preparation_action_item__c>());

				preparationActionItems.get(action.BI_PL_Source_preparation__c).add(item);
			}
		}
		// Add the Preparation actions:
		Savepoint sp = Database.setSavepoint();

		try {
			insert preparationActions;
		} catch (exception e) {
			throw getValidationError(e, requests[0].record.BI_PL_Type__c);
		}

		// Add the Preparation action details
		Set<BI_PL_Preparation_action_item__c> actionItems = new Set<BI_PL_Preparation_action_item__c>();

		for (BI_PL_Preparation_action__c action : preparationActions) {
			if (preparationActionItems.containsKey(action.BI_PL_Source_preparation__c)) {

				for (BI_PL_Preparation_action_item__c item : preparationActionItems.get(action.BI_PL_Source_preparation__c)) {
					item.BI_PL_Parent__c = action.Id;
					if (!actionItems.contains(item))
						actionItems.add(item);
					System.debug('ITEM ' + item);
				}
			}
		}

		try {
			insert new List<BI_PL_Preparation_action_item__c>(actionItems);
		} catch (exception e) {
			Database.rollback(sp);
			throw getValidationError(e,(new List<BI_PL_Preparation_action_item__c>(actionItems))[0].BI_PL_Request_response__c);
		}


	}

	public class TransferAndShareRequestWrapper {
		public Boolean isInbound = false;
		public BI_PL_Preparation_action__c record;
		public List<TransferAndShareDetailRequestWrapper> details = new List<TransferAndShareDetailRequestWrapper>();

		public TransferAndShareRequestWrapper (BI_PL_Preparation_action__c record) {
			this.record = record;
		}

		public void addDetail(TransferAndShareDetailRequestWrapper detail) {
			this.details.add(detail);
			if (detail.isInbound)
				this.isInbound = true;
		}
	}

	public class TransferAndShareDetailRequestWrapper {
		public BI_PL_Preparation_action_item__c record;
		public Boolean readOnly;

		public Id fromPreparation;
		public String fromPositionName;
		public String fromOwnerName;

		public Id toPreparation;
		public String toPositionName;
		public String toOwnerName;

		public Id actionId;
		public Id accountId;

		public Boolean isInbound;

		public TransferAndShareDetailRequestWrapper (BI_PL_Preparation_action_item__c record, Boolean readOnly, TransferAndShareRequestWrapper action, Boolean isInbound) {

			this.isInbound = isInbound;

			this.actionId = action.record.Id;
			this.accountId = action.record.BI_PL_Account__c;

			this.record = record;
			this.readOnly = readOnly;

			fromPositionName = getFromPositionName(record);
			toPositionName = getToPositionName(record);
			fromOwnerName = getFromOwnerName(record);
			toOwnerName = getToOwnerName(record);

			if (isTransferOrShare(record)) {
				fromPreparation = record.BI_PL_Parent__r.BI_PL_Source_preparation__c;
				toPreparation = record.BI_PL_Target_preparation__c;
			} else {
				fromPreparation = record.BI_PL_Target_preparation__c;
				toPreparation = record.BI_PL_Parent__r.BI_PL_Source_preparation__c;
			}
		}
	}

	public static String getFromPositionName(BI_PL_Preparation_action_item__c item) {
		if (isTransferOrShare(item)) {
			return item.BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
		} else {
			return item.BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
		}
	}
	public static String getToPositionName(BI_PL_Preparation_action_item__c item) {
		if (isTransferOrShare(item)) {
			return item.BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
		} else {
			return item.BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c;
		}
	}
	public static String getFromOwnerName(BI_PL_Preparation_action_item__c item) {
		if (isTransferOrShare(item)) {
			return item.BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name;
		} else {
			return item.BI_PL_Target_preparation_owner_name__c;
		}
	}
	public static String getToOwnerName(BI_PL_Preparation_action_item__c item) {
		if (isTransferOrShare(item)) {
			return item.BI_PL_Target_preparation_owner_name__c;
		} else {
			return item.BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name;
		}
	}

	private static Boolean isTransferOrShare(BI_PL_Preparation_action_item__c item) {
		return item.BI_PL_Parent__r.BI_PL_Type__c == TRANSFER || item.BI_PL_Parent__r.BI_PL_Type__c == SHARE;
	}

	/**
	 * Clones a detail without the Id, the relationships (to use upsert) and the external ids.
	 * Used because standard sobject.clone() on related objects cannot delete the relationship field with the parent.
	 *
	 * @param      sourceDetail  The source detail
	 *
	 * @return     The detail to the transferred
	 */
	public static BI_PL_Detail_preparation__c cloneDetailWithoutKeys(BI_PL_Detail_preparation__c sourceDetail) {
		return new BI_PL_Detail_preparation__c(
		           BI_PL_Product__c = sourceDetail.BI_PL_Product__c,
		           BI_PL_Secondary_product__c = sourceDetail.BI_PL_Secondary_product__c,
		           BI_PL_Column__c = sourceDetail.BI_PL_Column__c,
		           BI_PL_Row__c = sourceDetail.BI_PL_Row__c,
		           BI_PL_Planned_Details__c = sourceDetail.BI_PL_Planned_Details__c,
		           BI_PL_Adjusted_Details__c = sourceDetail.BI_PL_Adjusted_Details__c,
		           BI_PL_Segment__c = sourceDetail.BI_PL_Segment__c,
		           BI_PL_Added_Manually__c = sourceDetail.BI_PL_Added_Manually__c ,
		           BI_PL_Business_rule_ok__c = sourceDetail.BI_PL_Business_rule_ok__c,
		           BI_PL_Commit_target__c = sourceDetail.BI_PL_Commit_target__c,
		           BI_PL_Cvm__c = sourceDetail.BI_PL_Cvm__c,
		           BI_PL_Detail_priority__c = sourceDetail.BI_PL_Detail_priority__c,
		           BI_PL_Fdc__c = sourceDetail.BI_PL_Fdc__c,
		           BI_PL_Ims_id__c = sourceDetail.BI_PL_Ims_id__c,
		           BI_PL_Market_target__c = sourceDetail.BI_PL_Market_target__c,
		           BI_PL_Other_goal__c = sourceDetail.BI_PL_Other_goal__c,
		           BI_PL_Poa_objective__c = sourceDetail.BI_PL_Poa_objective__c,
		           BI_PL_Prescribe_target__c = sourceDetail.BI_PL_Prescribe_target__c,
		           BI_PL_Primary_goal__c = sourceDetail.BI_PL_Primary_goal__c,
		           BI_PL_Strategic_segment__c = sourceDetail.BI_PL_Strategic_segment__c
		       );
	}


	public static BI_PL_Channel_detail_preparation__c cloneChannelDetailWithoutKeys(BI_PL_Channel_detail_preparation__c sourceChannelDetail) {
		return new BI_PL_Channel_detail_preparation__c(
		           BI_PL_Channel__c = sourceChannelDetail.BI_PL_Channel__c
		       );

	}

	/**
	 * Generates a document with the requested report (by cycle and hierarchy) and returns the id of the document created.
	 *
	 * @param      cycleId        The cycle identifier
	 * @param      hierarchyName  The hierarchy name
	 *
	 * @return     The Id of the geneated document
	 */
	@RemoteAction
	public static String generateActionTextReportDocument(String cycleId, String hierarchyName) {

		Document doc = new Document();
		doc.Body = Blob.valueOf(generateActionTextReportBody(cycleId, hierarchyName));
		doc.Name = 'BI_PL_ActionReport_' + Date.today();
		doc.FolderId = UserInfo.getUserId();
		insert doc;

		return doc.Id;

	}

	public static String generateActionTextReportBody(String cycleId, String hierarchyName) {
		System.debug(loggingLevel.Error, '*** cycleId: ' + cycleId);
		System.debug(loggingLevel.Error, '*** 	hierarchyName: ' + 	hierarchyName);
		//17 columns
		String docHeader = 'action_id|cm_id|customer_type|from_position|dest_position|from_ff|dest_ff|action_type|reason|status|status_reason|request_type|request_timestamp|request_network_id|status_timestamp|status_network_id\r\n';
		String docBody = '';

		for (BI_PL_Preparation_action_item__c item : [SELECT Id,
		        BI_PL_Target_preparation__c,
		        BI_PL_Request_response__c,
		        BI_PL_Target_preparation__r.BI_PL_External_id__c,
		        BI_PL_Target_preparation__r.BI_PL_Position_name__c,
		        BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c,
		        BI_PL_Product__c,
		        BI_PL_Parent__c,
		        BI_PL_Status__c,
		        BI_PL_Rejected_reason__c,
		        BI_PL_Parent__r.BI_PL_Type__c,
		        BI_PL_Parent__r.BI_PL_Added_reason__c,
		        BI_PL_Parent__r.BI_PL_Account__r.External_Id_vod__c,
		        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_External_id__c,
		        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_name__c,
		        BI_PL_Parent__r.BI_PL_Account__r.isPersonAccount,
		        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c,
		        CreatedDate,
		        LastModifiedDate,
		        CreatedBy.Alias,
		        LastModifiedBy.Alias
		        FROM BI_PL_Preparation_action_item__c
		        WHERE BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
		                AND BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchyName ]) {

			String statusReason = (item.BI_PL_Status__c == 	'Rejected' ? item.BI_PL_Rejected_reason__c : '');

			System.debug(loggingLevel.Error, '*** 	item: ' + 	item);
			docBody += item.BI_PL_Parent__c + '|' + //action_id
			           +item.BI_PL_Parent__r.BI_PL_Account__r.External_Id_vod__c + '|' + //cm_id
			           +(item.BI_PL_Parent__r.BI_PL_Account__r.isPersonAccount ? 'HCP' : 'HCO') + '|' + //customer_type
			           +item.BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_name__c + '|' + //from_position
			           +item.BI_PL_Target_preparation__r.BI_PL_Position_name__c + '|' + //dest_position
			           +item.BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c + '|' + //from_ff
			           +item.BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position__r.BI_PL_Field_force__c + '|' + //dest_ff
			           +item.BI_PL_Parent__r.BI_PL_Type__c + '|' + //action_type
			           +item.BI_PL_Parent__r.BI_PL_Added_reason__c + '|' + //reason
			           +item.BI_PL_Status__c + '|' + //status
			           +statusReason + '|' + //status reason
			           +(item.BI_PL_Parent__r.BI_PL_Type__c == 'Request' ? 'PULL' : 'PUSH') + '|' + //request_type
			           +item.CreatedDate + '|' + //request_timestamp
			           +item.CreatedBy.Alias + '|' + //request_network_id
			           +item.LastModifiedDate + '|' + //status_timestamp
			           +item.LastModifiedBy.Alias + '|' + //status_network_id
			           +'\r\n';

		}

		System.debug(loggingLevel.Error, '*** 	docBody: ' + 	docBody);

		return docHeader + docBody;
	}

	public static String generateAddsTextReportBody(String cycleId, String hierarchyName, String channel) {
		System.debug(loggingLevel.Error, '*** cycleId: ' + cycleId);
		System.debug(loggingLevel.Error, '*** 	hierarchyName: ' + 	hierarchyName);
		//17 columns
		String docHeader = 'Customer: External Id|Customer: BI External ID 1|Customer: Specialty|Customer: Name|Field Force|Territory|P|A|Product: Product Name|Secondary Product: Product Name|Segment|Strategic Segment|Must Win|HyperTarget|Added Manually|Added Reason\r\n';
		String docBody = '';
		Map<String, Boolean> winByTarget = new Map<String, Boolean>();
		Map<String, Boolean> hyperTargetByTarget = new Map<String, Boolean>();

		for (BI_PL_Affiliation__c aff : [SELECT Id, BI_PL_Must_win__c, BI_PL_HyperTarget__c, BI_PL_Customer__c
		                                 FROM BI_PL_Affiliation__c
		                                 WHERE BI_PL_Customer__c IN (
		                                     SELECT BI_PL_Target_customer__c
		                                     FROM BI_PL_Target_preparation__c
		                                     WHERE BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
		                                             AND BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchyName)
		                                 AND (BI_PL_HyperTarget__c =  true OR BI_PL_Must_win__c = true)]) {
			//affiliationByTarget.put(aff.BI_PL_Customer__c, aff);
			winByTarget.put(aff.BI_PL_Customer__c, aff.BI_PL_Must_win__c);
			hyperTargetByTarget.put(aff.BI_PL_Customer__c, aff.BI_PL_HyperTarget__c);
		}

		for (BI_PL_Detail_preparation__c item : [
		            SELECT  Id, CreatedDate, CreatedBy.Alias,
		            BI_PL_Product__r.External_Id_vod__c,
		            BI_PL_Product__r.Name,
		            BI_PL_Secondary_Product__r.External_Id_vod__c,
		            BI_PL_Secondary_Product__r.Name,
		            BI_PL_Adjusted_Details__c,
		            BI_PL_Planned_Details__c,
		            BI_PL_Segment__c,
		            BI_PL_Strategic_segment__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty__c,		           
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Field_force__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.Name,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_HCP__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_reason__c,
		            BI_PL_Channel_detail__r.BI_PL_MSL_flag__c,
		            BI_PL_Channel_detail__r.BI_PL_Removed_reason__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c,
		            BI_PL_Channel_detail__r.BI_PL_Removed__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c
		            FROM BI_PL_Detail_preparation__c
		            WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c = true
		                          AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
		                                  AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchyName
		                                  		AND BI_PL_Channel_detail__r.BI_PL_Channel__c =: channel]) {

			String reason = (item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_reason__c != null) ? item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_reason__c : '';			
			
			String tragetExternalId = (item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c != null) ? item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c : '';
			String tragetExternalBiID = (item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c != null) ? item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c : '';
			String must_win_flag = (winByTarget.containsKey(item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c)
				                             ? winByTarget.get(item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c) ? '1' : '0'
				                             : '0');

			String hypertarget_flag = (hyperTargetByTarget.containsKey(item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c)
				                                ? hyperTargetByTarget.get(item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c) ? '1' : '0'
				                                : '0');

			String secondaryProductName = (item.BI_PL_Secondary_Product__r.Name != null ? item.BI_PL_Secondary_Product__r.Name : '');

			String segment = (item.BI_PL_Segment__c != null ? item.BI_PL_Segment__c : '');
			String strategicSegment = (item.BI_PL_Strategic_segment__c != null ? item.BI_PL_Strategic_segment__c : '');
			
			String removed = (item.BI_PL_Channel_detail__r.BI_PL_Removed__c == true ? '1' : '0');
			String added = (item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c == true ? '1' : '0');
			
			System.debug(loggingLevel.Error, '*** 	item: ' + 	item);
			docBody += tragetExternalId + '|' + //target external id
						+tragetExternalBiID + '|' + //target ims id
						+item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty__c + '|' + //target specialty
						+item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.Name + '|' + //target Name
		        	    +item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Field_force__c + '|' + //sales_force
			            +item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c + '|' + //position_id
			            +item.BI_PL_Planned_Details__c + '|' + //P
			            +item.BI_PL_Adjusted_Details__c + '|' + //A
			            +item.BI_PL_Product__r.Name + '|' + //primary product name
			            +secondaryProductName + '|' + //secondary product name
			            +segment + '|' + //Segment
			            +strategicSegment + '|' + //Stretaegic segment
			            +must_win_flag + '|' + //Must Win
			            +hypertarget_flag + '|' + //HyperTarget
			            +added + '|' + //Added
			            +reason +  //Added reason
			            + '\r\n';
			
			

		}

		System.debug(loggingLevel.Error, '*** 	docBody: ' + 	docBody);

		return docHeader + docBody;
	}

	public static String generateDropsTextReportBody(String cycleId, String hierarchyName, String channel) {
		String errMsg = 'Channel is mandatory for Drops report';

		if (channel == null || channel == '') {
			addError(errMsg);
			return errMsg;
		}

		System.debug(loggingLevel.Error, '*** cycleId: ' + cycleId);
		System.debug(loggingLevel.Error, '*** 	hierarchyName: ' + 	hierarchyName);
		//17 columns
		String docHeader = 'Customer: External Id|Customer: BI External ID 1|Customer: Specialty|Customer: Name|Field Force|Territory|P|A|Product: Product Name|Secondary Product: Product Name|Segment|Strategic Segment|Must Win|HyperTarget|Removed|Removed Reason\r\n';
		String docBody = '';

		Map<String, Boolean> winByTarget = new Map<String, Boolean>();
		Map<String, Boolean> hyperTargetByTarget = new Map<String, Boolean>();	


		//TODO : Logic to add to the map??? -> duplicates??
		for (BI_PL_Affiliation__c aff : [SELECT Id, BI_PL_Must_win__c, BI_PL_HyperTarget__c, BI_PL_Customer__c
		                                 FROM BI_PL_Affiliation__c
		                                 WHERE BI_PL_Customer__c IN (
		                                     SELECT BI_PL_Target_customer__c
		                                     FROM BI_PL_Target_preparation__c
		                                     WHERE BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
		                                             AND BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchyName)
		                                 AND (BI_PL_HyperTarget__c =  true OR BI_PL_Must_win__c = true)]) {
			//affiliationByTarget.put(aff.BI_PL_Customer__c, aff);
			winByTarget.put(aff.BI_PL_Customer__c, aff.BI_PL_Must_win__c);
			hyperTargetByTarget.put(aff.BI_PL_Customer__c, aff.BI_PL_HyperTarget__c);
		}

		for (BI_PL_Detail_preparation__c item : [
		            SELECT  Id, CreatedDate, CreatedBy.Alias,
		            BI_PL_Product__r.External_Id_vod__c,
		            BI_PL_Product__r.Name,
		            BI_PL_Secondary_Product__r.External_Id_vod__c,
		            BI_PL_Secondary_Product__r.Name,
		            BI_PL_Adjusted_Details__c,
		            BI_PL_Planned_Details__c,
		            BI_PL_Segment__c,
		            BI_PL_Strategic_segment__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty__c,		           
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Field_force__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.Name,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_HCP__c,
		            BI_PL_Channel_detail__r.BI_PL_MSL_flag__c,
		            BI_PL_Channel_detail__r.BI_PL_Removed_reason__c,

		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c,
		            BI_PL_Channel_detail__r.BI_PL_Removed__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c
		            FROM BI_PL_Detail_preparation__c
		            WHERE BI_PL_Channel_detail__r.BI_PL_Removed__c = true
		                          AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
		                                  AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchyName
		                                  	AND BI_PL_Channel_detail__r.BI_PL_Channel__c =: channel]) {

			String reason = (item.BI_PL_Channel_detail__r.BI_PL_Removed_reason__c != null) ? item.BI_PL_Channel_detail__r.BI_PL_Removed_reason__c : '';
			String tragetExternalId = (item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c != null) ? item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c : '';
			String tragetExternalBiID = (item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c != null) ? item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c : '';
			String must_win_flag = (winByTarget.containsKey(item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c)
				                             ? winByTarget.get(item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c) ? '1' : '0'
				                             : '0');

			String hypertarget_flag = (hyperTargetByTarget.containsKey(item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c)
				                                ? hyperTargetByTarget.get(item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c) ? '1' : '0'
				                                : '0');

			String secondaryProductName = (item.BI_PL_Secondary_Product__r.Name != null ? item.BI_PL_Secondary_Product__r.Name : '');

			String segment = (item.BI_PL_Segment__c != null ? item.BI_PL_Segment__c : '');
			String strategicSegment = (item.BI_PL_Strategic_segment__c != null ? item.BI_PL_Strategic_segment__c : '');
			
			String removed = (item.BI_PL_Channel_detail__r.BI_PL_Removed__c == true ? '1' : '0');
			
			System.debug(loggingLevel.Error, '*** 	item: ' + 	item);
			docBody += tragetExternalId + '|' + //target external id
						+tragetExternalBiID + '|' + //target ims id
						+item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty__c + '|' + //target specialty
						+item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.Name + '|' + //target Name
		        	    +item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Field_force__c + '|' + //sales_force
			            +item.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c + '|' + //position_id
			            +item.BI_PL_Planned_Details__c + '|' + //P
			            +item.BI_PL_Adjusted_Details__c + '|' + //A
			            +item.BI_PL_Product__r.Name + '|' + //primary product name
			            +secondaryProductName + '|' + //secondary product name
			            +segment + '|' + //Segment
			            +strategicSegment + '|' + //Stretaegic segment
			            +must_win_flag + '|' + //Must Win
			            +hypertarget_flag + '|' + //HyperTarget
			            +removed + '|' + //Removed
			            +reason +  //Removed reason
			            + '\r\n';
			

		}

		System.debug(loggingLevel.Error, '*** 	docBody: ' + 	docBody);

		return docHeader + docBody;
	}


	@RemoteAction
	public static Map<String, String> generateDetailsTextReportBody(String cycleId, String hierarchyName, String channel, String poa, String offset) {


		Map<String, String> toRet = new Map<String, String>();

		String idOffset = (offset != null ? offset : '');

		String errMsg = 'Channel is mandatory for details report';

		if (channel == null || channel == '') {
			addError(errMsg);
			//return errMsg;
		}

		System.debug(loggingLevel.Error, '*** cycleId: ' + cycleId);
		System.debug(loggingLevel.Error, '*** 	hierarchyName: ' + 	hierarchyName);
		System.debug(loggingLevel.Error, '*** channel: ' + channel);
		//17 columns
		String docBody = '';

		if (idOffset == '')
			docBody += 'cm_id|ims_id|customer_specialty|Specialty_code|sales_force|position|customer_type|frequency|primary|secondary|segment|strategic_segment|must_win_flag|hypertarget_flag|target_flag\r\n';

		//Map<String, BI_PL_Affiliation__c> affiliationByTarget = new Map<String, BI_PL_Affiliation__c>();
		Map<String, Boolean> winByTarget = new Map<String, Boolean>();
		Map<String, Boolean> hyperTargetByTarget = new Map<String, Boolean>();

		//System.debug(loggingLevel.Error, '*** pKeys.size(): ' + pKeys.size());
		//System.debug(loggingLevel.Error, '*** Limits.getCpuTime() 1: ' + Limits.getCpuTime());
		//System.debug(loggingLevel.Error, '*** Limits.getHeapSize() 1: ' + Limits.getHeapSize());


		//TODO : Logic to add to the map??? -> duplicates??
		for (BI_PL_Affiliation__c aff : [SELECT Id, BI_PL_Must_win__c, BI_PL_HyperTarget__c, BI_PL_Customer__c
		                                 FROM BI_PL_Affiliation__c
		                                 WHERE BI_PL_Customer__c IN (
		                                     SELECT BI_PL_Target_customer__c
		                                     FROM BI_PL_Target_preparation__c
		                                     WHERE BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
		                                             AND BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchyName)
		                                 AND (BI_PL_HyperTarget__c =  true OR BI_PL_Must_win__c = true)]) {
			//affiliationByTarget.put(aff.BI_PL_Customer__c, aff);
			winByTarget.put(aff.BI_PL_Customer__c, aff.BI_PL_Must_win__c);
			hyperTargetByTarget.put(aff.BI_PL_Customer__c, aff.BI_PL_HyperTarget__c);
		}

		//System.debug(loggingLevel.Error, '*** affiliationByTarget.keySet().size(): ' + affiliationByTarget.keySet().size());

		//System.debug(loggingLevel.Error, '*** Limits.getCpuTime() 2: ' + Limits.getCpuTime());
		//System.debug(loggingLevel.Error, '*** Limits.getHeapSize() 2: ' + Limits.getHeapSize());

		Id lastId = null;
		Integer detailCount = 0;

		//Generate string
		for (BI_PL_Detail_preparation__c detail : [
		            SELECT Id, CreatedDate, CreatedBy.Alias,
		            BI_PL_Product__r.External_Id_vod__c,
		            BI_PL_Product__r.Name,
		            BI_PL_Secondary_Product__r.External_Id_vod__c,
		            BI_PL_Secondary_Product__r.Name,
		            BI_PL_Adjusted_Details__c,
		            BI_PL_Planned_Details__c,
		            BI_PL_Segment__c,
		            BI_PL_Strategic_segment__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty_code__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Field_force__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_HCP__c,
		            BI_PL_Channel_detail__r.BI_PL_MSL_flag__c,

		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c,
		            BI_PL_Channel_detail__r.BI_PL_Removed__c,
		            BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c

		            FROM BI_PL_Detail_preparation__c
		            WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycleId
		                    AND BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = :hierarchyName
		                            AND BI_PL_Channel_detail__r.BI_PL_Channel__c = :channel
		                                    AND Id > :idOffset
		                                    ORDER BY Id ASC LIMIT 8000]) {



			//BI_PL_Affiliation__c affiliationForAccount = (affiliationByTarget.containsKey(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c)
			//	? affiliationByTarget.get(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c)
			//	: null);

			//String hsbs_must_win_flag = (affiliationForAccount != null
			//	? affiliationForAccount.BI_PL_Must_win__c ? '1' : '0'
			//	: '');
			//String resp_hypertarget_flag = (affiliationForAccount != null
			//	? affiliationForAccount.BI_PL_HyperTarget__c ? '1' : '0'
			//	: '');
			if ((!detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c && !detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c && !detail.BI_PL_Channel_detail__r.BI_PL_Removed__c) || ((detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c || detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_No_see_list__c) && detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c)) {


				String hsbs_must_win_flag = (winByTarget.containsKey(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c)
				                             ? winByTarget.get(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c) ? '1' : '0'
				                             : '0');

				String resp_hypertarget_flag = (hyperTargetByTarget.containsKey(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c)
				                                ? hyperTargetByTarget.get(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c) ? '1' : '0'
				                                : '0');

				String target_flag = (detail.BI_PL_Channel_detail__r.BI_PL_MSL_flag__c ? '1' : '0');

				String specialtyCode = (detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty_code__c != null ? detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty_code__c : '');	

				String segment = '';
				if(detail.BI_PL_Segment__c != null) segment = detail.BI_PL_Segment__c;

				String strategicsSegment = '';
				if(detail.BI_PL_Strategic_segment__c != null) strategicsSegment = detail.BI_PL_Strategic_segment__c;

				String cmId = (detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c != null) ? detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_Id_vod__c : '';
				String primaryProduct = '';
				if (detail.BI_PL_Product__r.Name != null) primaryProduct = detail.BI_PL_Product__r.Name;

				String secondaryProduct = '';
				if (detail.BI_PL_Secondary_Product__r.Name != null) secondaryProduct = detail.BI_PL_Secondary_Product__r.Name;

				String imdId = (detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c != null) ? detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c : '';

				docBody += cmId + '|' + //cm_id
				           +imdId + '|' + //ims_id
				           +detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty__c+'|'+//specialty
				           +specialtyCode+'|'+//specialty code
				           +detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Field_force__c+'|'+//field force (sales force)
				           +detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c + '|' + //position_id
				           +(detail.BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_HCP__c ? 'HCP' : 'HCO') + '|' + //customer_type				           
				           +(poa == 'planned' ? detail.BI_PL_Planned_Details__c : detail.BI_PL_Adjusted_Details__c) + '|' + //frequency
				           +primaryProduct + '|' + //primary_product
				           +secondaryProduct + '|' + //secondary_product
				           +segment+'|'+ //segment
				           +strategicsSegment+'|'+ //strategic segment
				           +hsbs_must_win_flag + '|' + //hsbs_must_win_flag
				           +resp_hypertarget_flag + '|' + //resp_hypertarget_flag
				           +target_flag + //target_flag
				           +'\r\n';


			}

			//For pagination
			lastId = detail.Id;
			detailCount++;


		}

		//      System.debug(loggingLevel.Error, '*** Limits.getCpuTime() 3: ' + Limits.getCpuTime());
		//System.debug(loggingLevel.Error, '*** Limits.getHeapSize() 3: ' + Limits.getHeapSize());

		//      System.debug(loggingLevel.Error, '*** 	docBody.length(): ' + 	docBody.length());

		if (detailCount >= 8000) {
			toRet.put('offset', lastId);
		}

		toRet.put('body', docBody);

		return toRet;
		//return docHeader + docBody;
	}

	public static void addError(String msg) {
		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, msg));
	}

	public static BI_PL_Exception getValidationError(Exception e, String t){

		if (e.getMessage().contains(BI_PL_TransferAndShareRequestsCtrl.VALIDATION_ERROR_SOURCE_SUBMITTED))
			return new BI_PL_Exception(Label.BI_PL_Source_shouldnt_submitted);
		if (e.getMessage().contains(BI_PL_TransferAndShareRequestsCtrl.VALIDATION_ERROR_TARGET_SUBMITTED))
			return new BI_PL_Exception(Label.BI_PL_Target_shouldnt_submitted);
		if (e.getMessage().contains(BI_PL_TransferAndShareRequestsCtrl.VALIDATION_ERROR_ACTION_CANCELLED))
			return new BI_PL_Exception(Label.BI_PL_Action_Cancelled + ' : ' + t);

		return new BI_PL_Exception(e.getMessage());

	}

}