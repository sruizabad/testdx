@isTest
private class BI_PL_TransferAndShareUtility_Test {
	
    private static final String SCHANNEL = BI_PL_TestDataFactory.SCHANNEL;
    private static final String SHIERARCHY = BI_PL_TestDataFactory.hierarchy;
    private static final String TRANSFER = BI_PL_TransferAndShareUtility.TRANSFER;
    private static final String SHARE = BI_PL_TransferAndShareUtility.SHARE;

	private static String countryCode;
    
    private static BI_PL_Preparation_action_item__c item;
    private static List<BI_PL_Position_Cycle__c> posCycles;
    private static List<BI_PL_Preparation__c> listPrep;
    private static List<BI_PL_Preparation_action__c> listPrepAction;
    private static BI_PL_Cycle__c cycle;

    @testSetup static void setup() {
        countryCode =   [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(countryCode);
        BI_PL_TestDataFactory.createCycleStructure(countryCode);
        List<Account> listAcc = BI_PL_TestDataFactory.createTestAccounts(4,countryCode);
        List<Product_vod__c> listProd = BI_PL_TestDataFactory.createTestProduct(3,countryCode);            
        
        cycle = [SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        
        List<BI_PL_Position_cycle__c> listpc = [SELECT id, BI_PL_External_id__c FROM BI_PL_Position_cycle__c where Bi_pl_position__r.Name in ('Test4','Test5')];
        
        listPrep = BI_PL_TestDataFactory.createPreparations(countryCode, listpc, listAcc, listProd);

        listPrepAction = new List<BI_PL_Preparation_action__c>();
        List<BI_PL_Preparation_action_item__c> listPrepActionItem = new List<BI_PL_Preparation_action_item__c>();
        //Test Share
        BI_PL_Preparation_action__c prepAction = new BI_PL_Preparation_action__c();
        prepAction.BI_PL_Account__c = listAcc.get(0).id;
        prepAction.BI_PL_Added_reason__c = 'Influencer';
        prepAction.BI_PL_Channel__c = SCHANNEL;
        prepAction.BI_PL_Source_preparation__c = listPrep.get(0).id;
        prepAction.BI_PL_Type__c = SHARE;
        listPrepAction.add(prepAction);
        //insert prepAction;

        //Test transfer
        BI_PL_Preparation_action__c prepAction3 = new BI_PL_Preparation_action__c();
        prepAction3.BI_PL_Account__c = listAcc.get(1).id;
        prepAction3.BI_PL_Added_reason__c = 'Influencer';
        prepAction3.BI_PL_Channel__c = SCHANNEL;
        prepAction3.BI_PL_Source_preparation__c = listPrep.get(0).id;
        prepAction3.BI_PL_Type__c = TRANSFER;
        listPrepAction.add(prepAction3);
        //insert prepAction3;

        BI_PL_Preparation_action__c prepAction4 = new BI_PL_Preparation_action__c();
        prepAction4.BI_PL_Account__c = listAcc.get(2).id;
        prepAction4.BI_PL_Added_reason__c = 'Influencer';
        prepAction4.BI_PL_Channel__c = SCHANNEL;
        prepAction4.BI_PL_Source_preparation__c = listPrep.get(0).id;
        prepAction4.BI_PL_Type__c = 'Request';
        listPrepAction.add(prepAction4);
        //insert prepAction4;

        insert listPrepAction;

        BI_PL_Preparation_action_item__c prepActionItem = new BI_PL_Preparation_action_item__c();
        prepActionItem.BI_PL_Parent__c = listPrepAction.get(0).id;
        prepActionItem.BI_PL_Target_preparation__c =listPrep.get(1).id;
        prepActionItem.BI_PL_Product__c = listProd.get(0).id;
        prepActionItem.BI_PL_Status__c = 'Approved';
        listPrepActionItem.add(prepActionItem);
        
        BI_PL_Preparation_action_item__c prepActionItem2 = new BI_PL_Preparation_action_item__c();
        prepActionItem2.BI_PL_Parent__c = listPrepAction.get(0).id;
        prepActionItem2.BI_PL_Target_preparation__c =listPrep.get(1).id;
        prepActionItem2.BI_PL_Product__c = listProd.get(1).id;
        prepActionItem2.BI_PL_Status__c = 'Approved';
        listPrepActionItem.add(prepActionItem2);    

        BI_PL_Preparation_action_item__c prepActionItem3 = new BI_PL_Preparation_action_item__c();
        prepActionItem3.BI_PL_Parent__c = listPrepAction.get(1).id;
        prepActionItem3.BI_PL_Target_preparation__c =listPrep.get(1).id;
        prepActionItem3.BI_PL_Product__c = listProd.get(0).id;
        prepActionItem3.BI_PL_Status__c = 'Approved';
        listPrepActionItem.add(prepActionItem3);

        BI_PL_Preparation_action_item__c prepActionItem4 = new BI_PL_Preparation_action_item__c();
        prepActionItem4.BI_PL_Parent__c = listPrepAction.get(2).id;
        prepActionItem4.BI_PL_Target_preparation__c =listPrep.get(1).id;
        prepActionItem4.BI_PL_Product__c = listProd.get(2).id;
        prepActionItem4.BI_PL_Status__c = 'Approved';
        prepActionItem4.BI_PL_Request_response__c = TRANSFER;
        listPrepActionItem.add(prepActionItem4);

        insert listPrepActionItem;
    }
    

	@isTest static void test_method_two() {

        listPrepAction = [SELECT Id, BI_PL_Source_preparation__c, BI_PL_Type__c FROM BI_PL_Preparation_action__c];
        BI_PL_Preparation_action__c oPrepAction = new BI_PL_Preparation_action__c();
        BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper action = new BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper(oPrepAction);

        BI_PL_TransferAndShareUtility.TransferAndShareDetailRequestWrapper oWrp;
        for (BI_PL_Preparation_action_item__c inPrepActionItem : [SELECT Id, BI_PL_Parent__c, BI_PL_Parent__r.BI_PL_Source_preparation__c, BI_PL_Target_preparation__c, 
                                                                         BI_PL_Parent__r.BI_PL_Type__c 
                                                                    FROM BI_PL_Preparation_action_item__c 
                                                                    WHERE BI_PL_Parent__c = :listPrepAction.get(0).Id ]){
            oWrp = new BI_PL_TransferAndShareUtility.TransferAndShareDetailRequestWrapper(inPrepActionItem, true, action, false);
            action.addDetail(oWrp);
        }

        Test.startTest();
        BI_PL_TransferAndShareUtility.saveRequests( new List<BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper>{action} );
        Test.stopTest();
    }

    @isTest static void test_method_one() {
	

        cycle = [SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        //BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper action = New BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper(prepAction, false);
            
        //BI_PL_TransferAndShareUtility.saveRequests(requests);              	
        //BI_PL_TransferAndShareUtility.getFromPositionName(item);
        //BI_PL_TransferAndShareUtility.getToPositionName(item);
        //BI_PL_TransferAndShareUtility.getFromOwnerName(item);
        //BI_PL_TransferAndShareUtility.getToOwnerName(item);
     	
		BI_PL_Detail_preparation__c sourceDetail = [SELECT BI_PL_Product__c, BI_PL_Secondary_product__c, BI_PL_Column__c, BI_PL_Row__c, BI_PL_Planned_Details__c, BI_PL_Adjusted_Details__c, 
						BI_PL_Segment__c, BI_PL_Added_Manually__c , BI_PL_Business_rule_ok__c, BI_PL_Commit_target__c, BI_PL_Cvm__c, BI_PL_Detail_priority__c, 
						BI_PL_Fdc__c, BI_PL_Ims_id__c, BI_PL_Market_target__c, BI_PL_Other_goal__c, BI_PL_Poa_objective__c, BI_PL_Prescribe_target__c, 
						BI_PL_Primary_goal__c, BI_PL_Strategic_segment__c FROM BI_PL_Detail_preparation__c LIMIT 1];
		BI_PL_TransferAndShareUtility.cloneDetailWithoutKeys(sourceDetail);

		BI_PL_Channel_detail_preparation__c sourceChannelDetail = [SELECT BI_PL_Channel__c FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Channel__c = : SCHANNEL LIMIT 1];
		BI_PL_TransferAndShareUtility.cloneChannelDetailWithoutKeys(sourceChannelDetail);
        
        BI_PL_TransferAndShareUtility.generateActionTextReportDocument(cycle.id, SHIERARCHY);
        BI_PL_TransferAndShareUtility.generateActionTextReportBody(cycle.id, SHIERARCHY);
        BI_PL_TransferAndShareUtility.generateAddsTextReportBody(cycle.id, SHIERARCHY, SCHANNEL);
		BI_PL_TransferAndShareUtility.generateDropsTextReportBody(cycle.id, SHIERARCHY, SCHANNEL);

		//channel empty for AddError function
		BI_PL_TransferAndShareUtility.generateDropsTextReportBody(cycle.id, SHIERARCHY, '');
		BI_PL_TransferAndShareUtility.generateDetailsTextReportBody(cycle.id, SHIERARCHY, SCHANNEL, 'planned','100');
		//channel empty for AddError function
		BI_PL_TransferAndShareUtility.generateDetailsTextReportBody(cycle.id, SHIERARCHY, '', 'planned','');

        //Map<String, BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper> getRequests(List<String> sourcePreparationsId, String channel);

        listPrep = [SELECT Id, BI_PL_External_id__c FROM BI_PL_Preparation__c];

        String prepid;
        List<String> listprepid = New List<String>();
        for(BI_PL_Preparation__c a : listPrep){
            prepid = String.valueOf(a);
            listprepid.add(prepid);
        }

        BI_PL_TransferAndShareRequestsCtrl.getRequests(listprepid,SCHANNEL);

        List<String> listprepid1 = New List<String>();
        listprepid1.add(listprepid[0]);
        BI_PL_TransferAndShareRequestsCtrl.getRequests(listprepid1,SCHANNEL);

        //BI_PL_TransferAndShareRequestsCtrl.saveActionRequest(action,channel);

        //List<Id> saveDetailRequests(BI_PL_TransferAndShareUtility.TransferAndShareDetailRequestWrapper[] details, List<String> sourcePreparationsId);
        //List<String> saveActionRequest(BI_PL_TransferAndShareUtility.TransferAndShareRequestWrapper action, String channel);
        



        BI_PL_Preparation_action_item__c listPrepActionItem_share = [SELECT id,BI_PL_Parent__r.BI_PL_Type__c,
                                                                        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                                                                        BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                                                                        BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name,
                                                                        BI_PL_Target_preparation_owner_name__c 
                                                                        from BI_PL_Preparation_action_item__c where BI_PL_Parent__r.BI_PL_Type__c = :SHARE limit 1];
        BI_PL_Preparation_action_item__c listPrepActionItem_transfer = [SELECT id,BI_PL_Parent__r.BI_PL_Type__c,
                                                                        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                                                                        BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                                                                        BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name,
                                                                        BI_PL_Target_preparation_owner_name__c 
                                                                        from BI_PL_Preparation_action_item__c where BI_PL_Parent__r.BI_PL_Type__c = :TRANSFER limit 1];
        BI_PL_Preparation_action_item__c listPrepActionItem_request = [SELECT id,BI_PL_Parent__r.BI_PL_Type__c,
                                                                        BI_PL_Parent__r.BI_PL_Source_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                                                                        BI_PL_Target_preparation__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
                                                                        BI_PL_Parent__r.BI_PL_Source_preparation__r.Owner.Name,
                                                                        BI_PL_Target_preparation_owner_name__c 
                                                                        from BI_PL_Preparation_action_item__c where BI_PL_Parent__r.BI_PL_Type__c = 'Request' limit 1];

        BI_PL_TransferAndShareUtility.getFromPositionName(listPrepActionItem_share);
        BI_PL_TransferAndShareUtility.getFromPositionName(listPrepActionItem_transfer);
        BI_PL_TransferAndShareUtility.getFromPositionName(listPrepActionItem_request);
        BI_PL_TransferAndShareUtility.getToPositionName(listPrepActionItem_share);
        BI_PL_TransferAndShareUtility.getToPositionName(listPrepActionItem_transfer);
        BI_PL_TransferAndShareUtility.getToPositionName(listPrepActionItem_request);
        BI_PL_TransferAndShareUtility.getFromOwnerName(listPrepActionItem_share);
        BI_PL_TransferAndShareUtility.getFromOwnerName(listPrepActionItem_transfer);
        BI_PL_TransferAndShareUtility.getFromOwnerName(listPrepActionItem_request);
        BI_PL_TransferAndShareUtility.getToOwnerName(listPrepActionItem_share);
        BI_PL_TransferAndShareUtility.getToOwnerName(listPrepActionItem_transfer);
        BI_PL_TransferAndShareUtility.getToOwnerName(listPrepActionItem_request);
     


	}
	
	
}