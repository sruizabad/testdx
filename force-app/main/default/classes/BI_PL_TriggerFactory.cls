/**
 * Class TriggerFactory
 *
 * Used to instantiate and execute Trigger Handlers associated with sObjects.
 */
public without sharing class BI_PL_TriggerFactory {
    /**
     * Public static method to create and execute a trigger handler
     *
     * Arguments:   Schema.sObjectType soType - Object type to process (SObject.sObjectType)
     *
     * Throws a TriggerException if no handler has been coded.
     */
    public static void createHandler(Schema.sObjectType soType) {
        // Get a handler appropriate to the object being processed
        BI_PL_TriggerInterface handler = getHandler(soType);

        // Make sure we have a handler registered, new handlers must be registered in the getHandler method.
        if (handler == null) {
            throw new BI_PL_Exception('No Trigger Handler registered for Object Type: ' + soType);
        }

        // Execute the handler to fulfil the trigger
        execute(handler);
    }

    /**
     * private static method to control the execution of the handler
     *
     * Arguments:   BI_PL_TriggerInterface handler - A Trigger Handler to execute
     */
    private static void execute(BI_PL_TriggerInterface handler) {
        System.debug(loggingLevel.Error, '*** Trigger factory execute: ');
        // Before Trigger
        if (Trigger.isBefore) {
            // Call the bulk before to handle any caching of data and enable bulkification
            handler.bulkBefore();

            // Iterate through the records to be inserted passing them to the handler.
            if (Trigger.isInsert) {
                for (SObject so : Trigger.new) {
                    handler.beforeInsert(so);
                }
            }
            // Iterate through the records to be updated passing them to the handler.
            else if (Trigger.isUpdate) {
                for (SObject so : Trigger.old) {
                    handler.beforeUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        } 

        if (Trigger.isAfter) {
            System.debug(loggingLevel.Error, '*** Trigger factory after: ');
            // Call the bulk After to handle any caching of data and enable bulkification
            handler.bulkAfter();

            // Iterate through the records to be inserted passing them to the handler.
            if (Trigger.isInsert) {
                System.debug(loggingLevel.Error, '*** Trigger factory after insert: ');
                for (SObject so : Trigger.new) {
                    handler.afterInsert(so);
                }
            }
            // Iterate through the records to be updated passing them to the handler.
            else if (Trigger.isUpdate) {
                System.debug(loggingLevel.Error, '*** Trigger factory after update: ');
                for (SObject so : Trigger.old) {
                    handler.afterUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        } 
        
        handler.andFinally();
    }

    /**
     * private static method to get the appropriate handler for the object type.
     * Modify this method to add any additional handlers.
     *
     * Arguments:   Schema.sObjectType soType - Object type tolocate (SObject.sObjectType)
     *
     * Returns:     BI_PL_TriggerInterface - A trigger handler if one exists or null.
     */
    private static BI_PL_TriggerInterface getHandler(Schema.sObjectType soType) {
        if (soType == BI_PL_Position__c.sObjectType) {
            return new BI_PL_PositionTriggerHandler();
        }
        if (soType == BI_PL_Cycle__c.sObjectType) {
            return new BI_PL_CycleTriggerHandler();
        }
        if (soType == BI_PL_Business_rule__c.sObjectType){
            return new BI_PL_BusinessRuleTriggerHandler();
        }
        if (soType == BI_PL_Preparation_action__c.sObjectType) {
            return new BI_PL_PreparationActionHandler();
        }
        if (soType == BI_PL_Preparation_action_item__c.sObjectType){
            return new BI_PL_PreparationActionItemHandler();
        }
        if (soType == BI_PL_Target_preparation__c.sObjectType){
            return new BI_PL_TargetPreparationHandler();
        }
        if (soType == BI_PL_Position_cycle__c.sObjectType){
            return new BI_PL_PositionCycleHandler();
        }
        if (soType == BI_PL_Position_cycle_user__c.sObjectType){
            return new BI_PL_PositionCycleUserHandler();
        }
        //if (soType == BI_PL_Detail_preparation__c.sObjectType){
        //    return new BI_PL_DetailPreparationHandler();
        //}
        if (soType == BI_PL_Channel_detail_preparation__c.sObjectType){
            return new BI_PL_ChannelDetailPreparationHandler();
        }
        return null;
    }
}