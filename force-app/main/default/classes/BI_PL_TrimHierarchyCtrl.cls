public without sharing class BI_PL_TrimHierarchyCtrl {

	private static String MORE_THAN_ONE_ROOT_MESSAGE = Label.BI_PL_More_than_one_root_node;

	@RemoteAction
	public static PlanitTrimHierarchyResult executeTrimHierarchy(Id cycleId, String hierarchyName, Map<String, Boolean> nodesToReplaceMap) {

		Set<BI_PL_PositionCycleWrapper> currentUserNodes = new Set<BI_PL_PositionCycleWrapper>();

		Map<String, BI_PL_PositionCycleWrapper> tree = new BI_PL_VisibilityTree(cycleId, hierarchyName).getTree();

		Map<String, BI_PL_DashboardsCtrl.PLANiTTreeNode> hierarchy = new Map<String, BI_PL_DashboardsCtrl.PLANiTTreeNode>();

		List<String> treeList = new List<String>(tree.keySet());

		List<BI_PL_PositionCycleWrapper> leafs = new List<BI_PL_PositionCycleWrapper>();

		Map<Id, Id> positionIdByPositionCycleId = new Map<Id, Id>();

		BI_PL_PositionCycleWrapper rootPositionCycle = null;

		for (Integer i = 0; i < treeList.size(); i++) {
			System.debug('tree size: ' + treeList.size() + ' positionId: ' + treeList.get(i));
			String positionId =  treeList.get(i);

			BI_PL_PositionCycleWrapper node = tree.get(positionId);

			if (node.parent == null) {
				if (rootPositionCycle != null)
					throw new BI_PL_Exception(MORE_THAN_ONE_ROOT_MESSAGE);

				rootPositionCycle = node;
			}

			positionIdByPositionCycleId.put(node.positionCycleId, node.position.record.Id);

			if (node.getChildren().size() == 0)
				leafs.add(node);
		}

		Set<Id> nodesToRemove = new Set<Id>();
		Set<Id> nodesToReplaceParent = new Set<Id>();

		for (BI_PL_PositionCycleWrapper leaf : leafs) {

			BI_PL_PositionCycleWrapper node = leaf;
			Boolean trimNodeFound = false;

			while (node.parent != null) {
				// The node is already in the nodesToReplaceParent set.
				// It means we already went through this branch, so it's not required to continue checking.
				if (nodesToReplaceParent.contains(node.positionCycleId))
					break;

				if (trimNodeFound) {
					nodesToRemove.add(node.positionCycleId);
				} else if (isTrimNode(node.positionCycleId, nodesToReplaceMap)) {
					trimNodeFound = true;
					nodesToRemove.add(node.positionCycleId);

					for (BI_PL_PositionCycleWrapper child : node.getChildren())
						nodesToReplaceParent.add(child.positionCycleId);
				}

				node = node.parent;
			}
		}
		Set<Id> additionalToReplace = new Set<Id>();
		// Add to the replace list the ones that are in a different branch:
		for (Id nodeToRemove : nodesToRemove) {
			BI_PL_PositionCycleWrapper node = tree.get(positionIdByPositionCycleId.get(nodeToRemove));
			for (BI_PL_PositionCycleWrapper child : node.getChildren()) {
				// If the child is not set to remove AND is not already in the replace sets add it:
				if (!nodesToRemove.contains(child.positionCycleId) &&
				        !nodesToReplaceParent.contains(child.positionCycleId) &&
				        !additionalToReplace.contains(child.positionCycleId)) {

					additionalToReplace.add(child.positionCycleId);
				}
			}
		}

		nodesToReplaceParent.addAll(additionalToReplace);

		// Replace parent:
		List<BI_PL_Position_cycle__c> pcToUpdate = new List<BI_PL_Position_cycle__c>();
		for (Id i : nodesToReplaceParent) {
			pcToUpdate.add(new BI_PL_Position_cycle__c(Id = i, BI_PL_Parent_position__c = rootPositionCycle.position.recordId));
		}

		update pcToUpdate;

		// Delete nodesToRemove
		List<BI_PL_Position_cycle__c> pcToRemove = new List<BI_PL_Position_cycle__c>();
		for (Id i : nodesToRemove)
			pcToRemove.add(new BI_PL_Position_cycle__c(Id = i));

		// 1.- Delete BI_PL_Position_cycle_user__c
		delete [SELECT Id FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c IN:pcToRemove];

		// 2.- Delete BI_PL_Position_cycle__c
		delete pcToRemove;
		// Create a BI_PL_Position_cycle_user__c in the root node for the datastewart:
		BI_PL_Cycle__c cycleRecord = [SELECT Id, BI_PL_Field_force__r.Name, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c WHERE Id =:cycleId];
		String userExternalId = [SELECT External_ID__c FROM User WHERE Id = : UserInfo.getUserId()].External_ID__c;
		String positionCycleExternalId = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycleRecord, hierarchyName, rootPositionCycle.position.record.Name, userExternalId);

		upsert new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = rootPositionCycle.positionCycleId,
		                                        BI_PL_User__c = UserInfo.getUserId(),
		                                        BI_PL_Preparation_owner__c = false,
	                                        BI_PL_External_id__c = positionCycleExternalId) BI_PL_External_id__c;

		return new PlanitTrimHierarchyResult(nodesToRemove, nodesToReplaceParent);
	}

	private static Boolean isTrimNode(String nodeId, Map<String, Boolean> nodesToReplaceMap) {
		return nodesToReplaceMap.containsKey(nodeId) && nodesToReplaceMap.get(nodeId);
	}

	@RemoteAction
	@ReadOnly
	public static PlanitHierarchyNodes getHierarchyNodes(String cycleId, String hierarchy) {
		Map<String, BI_PL_PositionCycleWrapper> tree = new BI_PL_VisibilityTree(cycleId, hierarchy).getTree();
		Map<String, PlanitHierarchyNode> output = new Map<String, PlanitHierarchyNode>();

		String rootNodeId = null;

		for (String k : tree.keySet()) {
			if (tree.get(k).parent == null) {
				if (rootNodeId == null)
					rootNodeId = k;
				else
					throw new BI_PL_Exception(MORE_THAN_ONE_ROOT_MESSAGE);
			}
			output.put(k, new PlanitHierarchyNode(tree.get(k)));
		}


		System.debug('getHierarchyNodes' + new PlanitHierarchyNodes(output, rootNodeId));

		return new PlanitHierarchyNodes(output, rootNodeId);
	}

	private static Set<String> getChildrenIds(Set<BI_PL_PositionCycleWrapper> children) {
		Set<String> output = new Set<String>();

		for (BI_PL_PositionCycleWrapper c : children)
			output.add(c.position.record.Id);

		return output;
	}

	class PlanitHierarchyNode {
		public String positionName;
		public Set<String> childrenIds;
		public String positionCycleId;

		public PlanitHierarchyNode (BI_PL_PositionCycleWrapper pcw) {
			this.positionName = pcw.position.record.Name;
			this.childrenIds = getChildrenIds(pcw.getChildren());
			this.positionCycleId = pcw.positionCycleId;
		}
	}

	public class PlanitHierarchyNodes {
		public Map<String, PlanitHierarchyNode> tree;
		public String rootNodeId;

		public PlanitHierarchyNodes(Map<String, PlanitHierarchyNode> tree, String rootNodeId) {
			this.tree = tree;
			this.rootNodeId = rootNodeId;
		}
	}

	public class PlanitTrimHierarchyResult {
		public Set<Id> nodesToRemove;
		public Set<Id> nodesToReplaceParent;

		public PlanitTrimHierarchyResult (Set<Id> nodesToRemove, Set<Id> nodesToReplaceParent) {
			this.nodesToRemove = nodesToRemove;
			this.nodesToReplaceParent = nodesToReplaceParent;
		}
	}

	private static String getUserCountryCode() {
		return [SELECT Country_Code_BI__c FROM USER WHERE Id = :UserInfo.getUserId()].Country_Code_BI__c;
	}

	@RemoteAction
	@ReadOnly
	public static List<BI_PL_Cycle__c> getCycles() {
		return new List<BI_PL_Cycle__c>([SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Field_force__r.Name, Name FROM BI_PL_Cycle__c WHERE BI_PL_Country_code__c = : getUserCountryCode()]);
	}
	/**
	 *	Loads the hierarchy options for the picklist.
	 *	@author OMEGA CRM
	 */
	@RemoteAction
	@ReadOnly
	public static Set<String> getHierarchies(String cycleId) {
		Set<String> output = new Set<String>();

		Set<String> hierarchies = new Set<String>();
		for (BI_PL_Position_cycle__c c : [SELECT BI_PL_Hierarchy__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = :cycleId])
			output.add(c.BI_PL_Hierarchy__c);

		return output;
	}

}