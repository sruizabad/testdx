@isTest
private class BI_PL_TrimHierarchyCtrl_Test {
	
	@isTest static void test_method_one() {
		String hierarchy = 'hierarchyTest';
		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting('US');

		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		String userCountryCode = 'US';

		BI_PL_TestDataFactory.createTestAccounts(6,userCountryCode);
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

		BI_PL_Cycle__c cycle = BI_PL_TestDataFactory.cycle;
		List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id FROM BI_PL_Position_Cycle__c];
		map<String, Boolean> nodesToReplaceMap = new map<String, Boolean>();

		for(BI_PL_Position_Cycle__c posCyc : posCycles){
			Decimal rand = Math.random();
			if(rand < 0.5) nodesToReplaceMap.put(posCyc.Id, true);
			else nodesToReplaceMap.put(posCyc.Id, false);
		}

		List<BI_PL_Preparation__c> prepToDelete = [SELECT Id FROM BI_PL_Preparation__c];
		delete prepToDelete;

		BI_PL_TrimHierarchyCtrl.executeTrimHierarchy(cycle.Id, hierarchy, nodesToReplaceMap);
		BI_PL_TrimHierarchyCtrl.getHierarchyNodes(cycle.Id, hierarchy);
		BI_PL_TrimHierarchyCtrl.getHierarchies(cycle.Id);
		BI_PL_TrimHierarchyCtrl.getCycles();

	}
	
}