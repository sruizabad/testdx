@isTest
private class BI_PL_USDegHomeDashboardCTRL_test{

	private static String userCountryCode;
	private static String hierarchy = 'hierarchyTest';

	private static Date startDate;
	private static Date endDate;
	private static List<BI_PL_Position_Cycle__c> posCycles;
	public static List<Product_vod__c> listProd;



	@isTest static void test(){
		User testUser = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        userCountryCode = testUser.Country_Code_BI__c;

		BI_PL_TestDataFactory.createCustomSettings();
		BI_PL_TestDataFactory.createCountrySetting(userCountryCode);
		BI_PL_TestDataFactory.usersCreation(userCountryCode);
		BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
		BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

		

		List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

		
		listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
		System.debug('prods*+*'+listProd);

		BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
		List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
		
		BI_PL_TestDataFactory.createVeevaData(cycle);

		List<Id> idList = new List<Id>();
		for(Account i : listAcc){
				idList.add(i.Id);
		}

		Call2_vod__c call = new Call2_vod__c (Status_vod__c = 'Submitted_vod', Call_Date_vod__c = sTartDate, Account_vod__c = idList.get(0), CP1__c = 'SPIRIVA');
		Call2_vod__c call2 = new Call2_vod__c (Status_vod__c = 'Submitted_vod', Call_Date_vod__c = sTartDate, Account_vod__c = idList.get(0), CP1__c = 'SPIRIVA');
		Call2_vod__c call3 = new Call2_vod__c (Status_vod__c = 'Submitted_vod', Call_Date_vod__c = sTartDate, Account_vod__c = idList.get(0), CP2__c = 'PRADAXA');
		Call2_vod__c call4 = new Call2_vod__c (Status_vod__c = 'Submitted_vod', Call_Date_vod__c = sTartDate, Account_vod__c = idList.get(0), CP2__c = 'PRADAXA');
		insert call;
		insert call2;
		insert call3;
		insert call4;

		//List<Account> listAcc = new List<Account>([SELECT id, External_Id_vod__c FROM Account LIMIT 250]);
       
		BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
			

		BI_PL_USDegHomeDashboardCtrl.PlanitSetupModel setup = BI_PL_USDegHomeDashboardCtrl.getSetupData();
		setup.currentCycleId = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1].Id;
		System.debug('cycle '+setup.currentCycleId);

		
		

		List<String> accList = new List<String>();
		
		

		List<String> proList = new List<String>();
		listProd = BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);
		System.debug('listProd*+*'+listProd);
		for(Product_vod__c ids: listProd){
			proList.add(ids.Id);
		}

		
		Test.startTest();

		BI_PL_USDegHomeDashboardCTrl.PLANiTHierarchyNodesWrapper hiherachynodes = BI_PL_USDegHomeDashboardCtrl.getHierarchyNodes(userCountryCode, setup.currentCycleId,hierarchy);
		BI_PL_USDegHomeDashboardCTrl.PLANiTPreparationsWrapper preparationWrap = BI_PL_USDegHomeDashboardCtrl.getPreparations(userCountryCode,setup.currentCycleId);
		

		List<BI_PL_Preparation__c> preparations = preparationWrap.preparations;
		System.debug('prepsWrap: '+ preparationWrap);
		
		System.debug('prepsList: '+ preparations);

		BI_PL_USDegHomeDashboardCTrl.PlanitDataWrapper datWrap = BI_PL_USDegHomeDashboardCtrl.getTargets(preparations,'');

		BI_PL_USDegHomeDashboardCtrl.AffiliationDataWrapper planAff = BI_PL_USDegHomeDashboardCTrl.getPlanitAffiliations(proList, accList, '');

		startDate = Date.today();
		endDate = Date.today()+1;

		Datetime startTimeDT = Datetime.newInstance(startDate.year(), startDate.month(), startDate.day());
		Datetime endDateDT = Datetime.newInstance(endDate.year(), endDate.month(), endDate.day());

		String startDateStr = String.valueOf(startTimeDT.getTime());
		String endDateStr = String.valueOf(endDateDT.getTime());

		BI_PL_USDegHomeDashboardCtrl.VeevaDataWrapper getVeeva = BI_PL_USDegHomeDashboardCTrl.getVeevaDetails(startDateStr, endDateStr, '',userCountryCode, idList);

		BI_PL_USDegHomeDashboardCtrl.VeevaDataInteractionsWrapper  getVeevaIteractions = BI_PL_USDegHomeDashboardCTrl.getVeevaInteractionsDetails(startDateStr, endDateStr, '', idList);
		
		BI_PL_USDegHomeDashboardCtrl.PlanitDataWrapper getWrapper = BI_PL_USDegHomeDashboardCtrl.getChannelDetails(preparations, '');

		BI_PL_USDegHomeDashboardCtrl.PlanitDataWrapper getDetails = BI_PL_USDegHomeDashboardCtrl.getDetails(preparations, '');

		String getDetailFilters = BI_PL_USDegHomeDashboardCtrl.getDetailFilters('cycleId','hName','channel');

		//System.debug();


		Test.stopTest();
		

	}


}