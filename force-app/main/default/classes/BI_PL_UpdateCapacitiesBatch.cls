/**
 *	Sets for each preparation in the cycle + hierarchy the Total capacity (if the total capacity manual check is not checked)
 *	@author OMEGA CRM
 */
global class BI_PL_UpdateCapacitiesBatch extends BI_PL_PlanitProcess implements Database.Batchable<sObject>, Database.Stateful {

	private String cycle;
	private String hierarchy;

	global BI_PL_UpdateCapacitiesBatch() {}

	//global BI_PL_UpdateCapacitiesBatch(String cycle, String hierarchy) {
	//	System.debug('BI_PL_UpdateCapacitiesBatch ' + cycle + ' - ' + hierarchy);
	//	this.cycle = cycle;
	//	this.hierarchy = hierarchy;
	//}
	global Database.QueryLocator start(Database.BatchableContext BC) {

		//this.cycle = this.cycle != null ? this.cycle : this.cycleIds.get(0);
		//this.hierarchy = this.hierarchy != null ? this.hierarchy : this.hierarchyNames.get(0);
		//List<String> hs = this.hierarchy != null ? newthis.hierarchy : this.hierarchyNames.get(0);

		return Database.getQueryLocator([SELECT Id, BI_PL_Capacity_adjustment_approved__c, BI_PL_Global_capacity__c, BI_PL_Total_capacity_manual__c, BI_PL_Capacity_adjustment__c, BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c, BI_PL_Field_force__c FROM BI_PL_Preparation__c WHERE BI_PL_Position_cycle__r.BI_PL_Hierarchy__c IN : hierarchyNames AND BI_PL_Position_cycle__r.BI_PL_Cycle__c IN : cycleIds]);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('BI_PL_UpdateCapacitiesBatch ' + cycle + ' - ' + hierarchy);

		Set<String> fieldforces = new Set<String>();
		List<BI_PL_Preparation__c> preparations = (List<BI_PL_Preparation__c>)scope;

		for(BI_PL_Preparation__c p : preparations) {
			fieldforces.add(p.BI_PL_Field_force__c);
		}

		Map<String, Decimal> visitsPerDayByFF = BI_PL_PreparationUtility.getVisitsPerDayPerThreshold(preparations.get(0).BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c, fieldforces);

		for(BI_PL_Preparation__c preparation : preparations){
			if(!preparation.BI_PL_Total_capacity_manual__c)
				preparation.BI_PL_Total_capacity__c = BI_PL_PreparationUtility.calculateTotalCapacity(preparation, visitsPerDayByFF.get(preparation.BI_PL_Field_force__c));
		}

		update preparations;
	}

	global void finish(Database.BatchableContext BC) {
		System.debug('BI_PL_UpdateCapacitiesBatch finish');
		
	}

}