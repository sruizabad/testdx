@isTest
public with sharing class BI_PL_UpdateCapacitiesBatch_Test {

	private static final Integer MANUAL_TOTAL_CAPACITY = 10;
	private static final Integer CAPACITY_ADJUSTMENT = 4;
	private static final Integer GLOBAL_CAPACITY = 10;
	@isTest
	public static void test() {

		String hierarchy = 'testHierarchy';

		BI_PL_Business_rule__c br = new BI_PL_Business_rule__c(BI_PL_Visits_per_day__c = 20,
		        BI_PL_Country_code__c = 'BR',
		        BI_PL_Type__c = BI_PL_PreparationUtility.THRESHOLD_RULE_TYPE,
		        BI_PL_Active__c = true);

		insert br;

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'KAM', BI_TM_Country_Code__c = 'BR');
		insert fieldForce;

		BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = 'BR',
		        BI_PL_Global_capacity__c = GLOBAL_CAPACITY,
		        BI_PL_Start_date__c = Date.newInstance(2018, 1, 1),
		        BI_PL_End_date__c = Date.newInstance(2018, 1, 31),
		        BI_PL_Field_force__c = fieldForce.Id);

		insert cycle;

		BI_PL_Position__c position1 = new BI_PL_Position__c(Name = 'Test1', BI_PL_Country_code__c = 'BR');
		BI_PL_Position__c position2 = new BI_PL_Position__c(Name = 'Test2', BI_PL_Country_code__c = 'BR');
		BI_PL_Position__c position3 = new BI_PL_Position__c(Name = 'Test3', BI_PL_Country_code__c = 'BR');

		insert new List<BI_PL_Position__c> {position1, position2, position3};

		BI_PL_Position_cycle__c positionCycle1 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position1.Id,
		        BI_PL_External_id__c = 'test1', BI_PL_Hierarchy__c = hierarchy);
		BI_PL_Position_cycle__c positionCycle2 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position2.Id,
		        BI_PL_External_id__c = 'test2', BI_PL_Hierarchy__c = hierarchy);
		BI_PL_Position_cycle__c positionCycle3 = new BI_PL_Position_cycle__c(BI_PL_Cycle__c = cycle.Id, BI_PL_Position__c = position3.Id,
		        BI_PL_External_id__c = 'test3', BI_PL_Hierarchy__c = hierarchy);

		insert new List<BI_PL_Position_cycle__c> {positionCycle1, positionCycle2, positionCycle3};

		BI_PL_Preparation__c preparation1 = new BI_PL_Preparation__c(
		    BI_PL_Country_code__c = 'BR',
		    BI_PL_External_id__c = 'Test',
		    BI_PL_Position_cycle__c = positionCycle1.Id,
		    BI_PL_Capacity_adjustment__c = CAPACITY_ADJUSTMENT,
		    BI_PL_Total_capacity__c = MANUAL_TOTAL_CAPACITY,
		    BI_PL_Total_capacity_manual__c = false,
		    BI_PL_Capacity_adjustment_approved__c = false);

		BI_PL_Preparation__c preparation2 = new BI_PL_Preparation__c(
		    BI_PL_Country_code__c = 'BR',
		    BI_PL_External_id__c = 'Test2',
		    BI_PL_Position_cycle__c = positionCycle2.Id,
		    BI_PL_Capacity_adjustment__c = CAPACITY_ADJUSTMENT,
		    BI_PL_Total_capacity__c = MANUAL_TOTAL_CAPACITY,
		    BI_PL_Total_capacity_manual__c = false,
		    BI_PL_Capacity_adjustment_approved__c = true);

		BI_PL_Preparation__c preparation3 = new BI_PL_Preparation__c(
		    BI_PL_Country_code__c = 'BR',
		    BI_PL_External_id__c = 'Test3',
		    BI_PL_Capacity_adjustment_approved__c = false,
		    BI_PL_Position_cycle__c = positionCycle3.Id,
		    BI_PL_Capacity_adjustment__c = CAPACITY_ADJUSTMENT,
		    BI_PL_Total_capacity__c = MANUAL_TOTAL_CAPACITY,
		    BI_PL_Total_capacity_manual__c = true);

		insert new List<BI_PL_Preparation__c> {preparation1, preparation2, preparation3};

		Test.startTest();

		BI_PL_UpdateCapacitiesBatch upd = new BI_PL_UpdateCapacitiesBatch();

		upd.init('BR',new List<String>{cycle.Id},new List<String>{hierarchy},null,null);

		Database.executeBatch(upd);

		Test.stopTest();

		Integer businessRuleVisitsPerDay = (Integer)br.BI_PL_Visits_per_day__c;

		preparation1 = [SELECT Id, BI_PL_Total_capacity__c, BI_PL_Global_capacity__c, BI_PL_Capacity_adjustment__c FROM BI_PL_Preparation__c WHERE Id =: preparation1.Id];
		preparation2 = [SELECT Id, BI_PL_Total_capacity__c, BI_PL_Global_capacity__c, BI_PL_Capacity_adjustment__c FROM BI_PL_Preparation__c WHERE Id =: preparation2.Id];
		preparation3 = [SELECT Id, BI_PL_Total_capacity__c, BI_PL_Global_capacity__c, BI_PL_Capacity_adjustment__c FROM BI_PL_Preparation__c WHERE Id =: preparation3.Id];

		System.assertEquals(cycle.BI_PL_Global_capacity__c * businessRuleVisitsPerDay, preparation1.BI_PL_Total_capacity__c, 'The total capacity is wrong.');
		System.assertEquals((cycle.BI_PL_Global_capacity__c - preparation3.BI_PL_Capacity_adjustment__c) * businessRuleVisitsPerDay, preparation2.BI_PL_Total_capacity__c, 'The total capacity is wrong.');
		System.assertEquals(MANUAL_TOTAL_CAPACITY, preparation3.BI_PL_Total_capacity__c, 'The total capacity is wrong.');


	}
}