/**
*	Fills all cycle records external ids properly:
*	- Position cycles
*	- Preparations
*	- Targets
*	- Channel detail preparations
*	- Detail preparations
*	The batch process could be more efficient if just upserts needed are done.
*	@author Omega CRM
*/
global class BI_PL_UpdateCycleRecordsExtIdsBatch implements Database.Batchable<sObject>, Database.Stateful {

	private Id cycleId;

	private String docBody = '';

	global BI_PL_UpdateCycleRecordsExtIdsBatch(Id cycleId) {
		if (String.isBlank(cycleId))
			throw new BI_PL_Exception(Label.BI_PL_Cycle_null);

		this.cycleId = cycleId;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('BI_PL_UpdateCycleRecordsExtIdsBatch START ' + cycleId );
		// Batch by cycle preparations:
		return Database.getQueryLocator([SELECT
		                                 Id,
		                                 BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c,
		                                 BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
		                                 BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c,
		                                 BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force_name__c,
		                                 BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
		                                 BI_PL_Position_cycle__r.BI_PL_Position_name__c
		                                 FROM BI_PL_Preparation__c
		                                 WHERE BI_PL_Position_cycle__r.BI_PL_Cycle__c = : cycleId]);

	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('BI_PL_UpdateCycleRecordsExtIdsBatch execute');

		List<BI_PL_Preparation__c> preparations = (List<BI_PL_Preparation__c>)scope;

		Set<Id> preparationIDs = new Set<Id>();
		Set<BI_PL_Position_cycle__c> positionCycles = new Set<BI_PL_Position_cycle__c>();

		for (BI_PL_Preparation__c preparation : preparations) {
			preparationIDs.add(preparation.Id);
			preparation.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePreparationExternalId(preparation);

			// ============ POSTIION CYCLES ============ //
			positionCycles.add(new BI_PL_Position_cycle__c(
			                       Id = preparation.BI_PL_Position_cycle__c,
			                       BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(preparation.BI_PL_Position_cycle__r)));
		}

		// ============ TARGETS ============ //
		List<BI_PL_Target_preparation__c> targets = [SELECT Id,
		                                  BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c,
		                                  BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
		                                  BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c,
		                                  BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
		                                  BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
		                                  BI_PL_Target_customer__r.External_ID_vod__c,
		                                  BI_PL_Target_customer__c,
		                                  BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force_name__c,
		                                  BI_PL_Header__r.BI_PL_Position_cycle__c,
		                                  BI_PL_Header__c
		                                  FROM BI_PL_Target_preparation__c WHERE BI_PL_Header__c IN:preparationIDs];

		System.debug('targets' + targets);

		for (BI_PL_Target_preparation__c target : targets)
			target.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generateTargetPreparationExternalId(target);

		// =========== CHANNELS ============ //
		List<BI_PL_Channel_detail_preparation__c> channelDetails = [SELECT Id,
		                                          BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c,
		                                          BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
		                                          BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c,
		                                          BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
		                                          BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
		                                          BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c,
		                                          BI_PL_Target__r.BI_PL_Target_customer__c,
		                                          BI_PL_Channel__c,
		                                          BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force_name__c,

		                                          BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__c,
		                                          BI_PL_Target__r.BI_PL_Header__c,
		                                          BI_PL_Target__c
		                                          FROM BI_PL_Channel_detail_preparation__c WHERE BI_PL_Target__r.BI_PL_Header__c IN:preparationIDs];

		for (BI_PL_Channel_detail_preparation__c channelDetail : channelDetails)
			channelDetail.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generateChannelDetailPreparationExternalId(channelDetail);

		// ============ DETAILS ============ //
		List<BI_PL_Detail_preparation__c> details = [SELECT Id,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Country_code__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Start_date__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_End_date__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position_name__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Channel__c,
		                                  BI_PL_Product__r.External_ID_vod__c,
		                                  BI_PL_Product__c,
		                                  BI_PL_Secondary_product__r.External_ID_vod__c,
		                                  BI_PL_Secondary_product__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__r.BI_PL_Field_force_name__c,

		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__c,
		                                  BI_PL_Channel_detail__r.BI_PL_Target__c,
		                                  BI_PL_Channel_detail__c FROM BI_PL_Detail_preparation__c WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__c IN:preparationIDs];

		for (BI_PL_Detail_preparation__c detail : details)
			detail.BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generateDetailPreparationExternalId(detail);

		upsert new BI_PL_Cycle__c (Id = preparations.get(0).BI_PL_Position_cycle__r.BI_PL_Cycle__c,
									BI_PL_External_id__c = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId(preparations.get(0).BI_PL_Position_cycle__r.BI_PL_Cycle__r)) Id;

		//upsert details Id;
		Database.SaveResult [] detailSaveResult = Database.update(details, false);
		for (Database.SaveResult  sr : detailSaveResult) {
			if (!sr.isSuccess()) {
				for(Database.Error err : sr.getErrors()) {
					docBody += err.getStatusCode() + ': ' + err.getMessage() + '[ Fields : ' + err.getFields() + '] [Id : '+ sr.getId()+'] \r\n';
				}
			}
		}

		//upsert new List<BI_PL_Channel_detail_preparation__c>(channelDetails) Id;
		Database.SaveResult [] channelDetailSaveResult = Database.update(channelDetails, false);
		for (Database.SaveResult  sr : channelDetailSaveResult) {
			if (!sr.isSuccess()) {
				for(Database.Error err : sr.getErrors()) {
					docBody += err.getStatusCode() + ': ' + err.getMessage() + '[ Fields : ' + err.getFields() + '] [Id : '+ sr.getId()+'] \r\n';
				}
			}
		}
		//upsert new List<BI_PL_Target_preparation__c>(targets) Id;
		Database.SaveResult [] targetSaveResult = Database.update(targets, false);
		for (Database.SaveResult  sr : targetSaveResult) {
			if (!sr.isSuccess()) {
				for(Database.Error err : sr.getErrors()) {
					docBody += err.getStatusCode() + ': ' + err.getMessage() + '[ Fields : ' + err.getFields() + '] [Id : '+ sr.getId()+'] \r\n';
				}
			}
		}
		//upsert new List<BI_PL_Preparation__c>(preparations) Id;
		Database.SaveResult [] preparationSaveResult = Database.update(preparations, false);
		for (Database.SaveResult  sr : preparationSaveResult) {
			if (!sr.isSuccess()) {
				for(Database.Error err : sr.getErrors()) {
					docBody += err.getStatusCode() + ': ' + err.getMessage() + '[ Fields : ' + err.getFields() + '] [Id : '+ sr.getId()+'] \r\n';
				}
			}
		}

		//upsert new List<BI_PL_Position_cycle__c>(positionCycles) Id;
		Database.SaveResult [] pcSaveResult = Database.update(new List<BI_PL_Position_cycle__c>(positionCycles), false);
		for (Database.SaveResult  sr : pcSaveResult) {
			if (!sr.isSuccess()) {
				for(Database.Error err : sr.getErrors()) {
					docBody += err.getStatusCode() + ': ' + err.getMessage() + '[ Fields : ' + err.getFields() + '] [Id : '+ sr.getId()+'] \r\n';
				}
			}
		}

	}
	global void finish(Database.BatchableContext BC) {
		//insert document (in personal folder)
		Document doc = new Document();
		doc.Name = 'BI_PL_UpdateCycleRecordsExtIdsBatch' + Datetime.now();
		doc.FolderId = UserInfo.getUserId();
		doc.Body = Blob.valueof(docBody);
		

		insert doc;

	}
}