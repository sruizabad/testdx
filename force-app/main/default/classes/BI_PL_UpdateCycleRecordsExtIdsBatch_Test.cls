@isTest
private class BI_PL_UpdateCycleRecordsExtIdsBatch_Test {
	
	private static BI_PL_Cycle__c cycle;
	private static List<Account> listAcc;
	private static List<BI_PL_Position_Cycle__c> posCycles = new List<BI_PL_Position_Cycle__c>();
	private static List<Product_vod__c> listProd = new List<Product_vod__c>();

	@isTest static void test_method_one() {
		BI_PL_TestDataFactory.createCustomSettings();
		
		BI_PL_TestDataFactory.createCycleStructure('BR');
		BI_PL_TestDataFactory.createTestAccounts(4, 'BR');
		listAcc = BI_PL_TestDataFactory.listAcc;
		cycle = BI_PL_TestDataFactory.cycle;
		BI_PL_TestDataFactory.createTestUsers(1,'BR');
		BI_PL_TestDataFactory.createCountrySetting('BR');
		User u = [SELECT id,Country_Code_BI__c From User where Country_Code_BI__c = 'BR' AND LastName = 'TestUserDatafactory0' LIMIT 1]; 
		BI_PL_Position_Cycle__c posCyc = [SELECT Id FROM BI_PL_Position_Cycle__c LIMIT 1];
		System.debug('**poscyc' + posCyc);
		posCycles.add(BI_PL_TestDataFactory.root);
		posCycles.add(BI_PL_TestDataFactory.child1);
		posCycles.add(BI_PL_TestDataFactory.child2);
		posCycles.add(BI_PL_TestDataFactory.child21);
		listProd = BI_PL_TestDataFactory.createTestProduct(3, 'BR');
		BI_PL_TestDataFactory.createPreparations('BR', posCycles, listAcc, listProd);
		List<BI_PL_Preparation__c> preps = new List<BI_PL_Preparation__c>([SELECT Id FROM BI_PL_Preparation__c LIMIT 4]);
		DELETE preps;
		BI_PL_UpdateCycleRecordsExtIdsBatch toExecute = new BI_PL_UpdateCycleRecordsExtIdsBatch(cycle.Id);
		
		Test.startTest();

		Id batchId = Database.executeBatch(toExecute, 10);
		/*
		try{
			Id batchId = Database.executeBatch(toExecute, 10);
		}catch (Exception e){
		}*/
		Test.stopTest();
		
		

	}
	
}