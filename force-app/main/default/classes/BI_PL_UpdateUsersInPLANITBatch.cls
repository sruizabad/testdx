global class BI_PL_UpdateUsersInPLANITBatch implements Database.Batchable<sObject>,Database.Stateful {
	
	String countryCode;
	List<String> listCycles;
	Set<String> hierarchiesToUpdateOwner;
	Map<String, Set<String>> mapCycleAndHierarchyToUpdateOwner = new Map<String,Set<String>>();




	global BI_PL_UpdateUsersInPLANITBatch(String cCode, List<String> lCycles) {
		this.countryCode = cCode;
		this.hierarchiesToUpdateOwner = new Set<String>();
		this.listCycles = lCycles;
		

		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, BI_PL_Position__r.Name, BI_PL_Parent_position__r.Name, BI_PL_Parent_position__c, BI_PL_Hierarchy__c, BI_PL_External_Id__c, BI_PL_Cycle__r.BI_PL_Country_code__c, BI_PL_Cycle__r.BI_PL_Start_Date__c, BI_PL_Cycle__r.BI_PL_End_date__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c IN: listCycles]);
	}

   	global void execute(Database.BatchableContext BC, List<BI_PL_Position_cycle__c> scope) {

   		Set<String> positionsName = new Set<String>();

   		List<BI_PL_Position_cycle_user__c> newPositionCycleUsers = new List<BI_PL_Position_cycle_user__c>();
   		Set<Id> positionCyclesId = new Set<Id>();

   		for(BI_PL_Position_cycle__c pc : scope){
   			
   			positionsName.add(pc.BI_PL_Position__r.Name);
   			positionsName.add(pc.BI_PL_Parent_position__r.Name);

   			positionCyclesId.add(pc.Id);
   		}

   		List<BI_PL_Position_cycle_user__c> pcUser = [SELECT Id, BI_PL_Position_cycle__c, BI_PL_Position_cycle__r.BI_PL_Position__r.Name, BI_PL_Preparation_owner__c, BI_PL_User__c, BI_PL_Position_cycle__r.BI_PL_Hierarchy__c, BI_PL_External_Id__c FROM BI_PL_Position_cycle_user__c WHERE BI_PL_Position_cycle__c IN: positionCyclesId];

		System.debug('**position cycle users: ' +pcUser);
		Map<String, List<BI_PL_Position_cycle_user__c>> positionToPositionCycleUser = new Map<String, List<BI_PL_Position_cycle_user__c>>();

		for(BI_PL_Position_cycle_user__c pcu : pcUser){

			if(positionToPositionCycleUser.containsKey(pcu.BI_PL_Position_cycle__r.BI_PL_Position__r.Name)){
				positionToPositionCycleUser.get(pcu.BI_PL_Position_cycle__r.BI_PL_Position__r.Name).add(pcu);
			}else{
				positionToPositionCycleUser.put(pcu.BI_PL_Position_cycle__r.BI_PL_Position__r.Name, new List<BI_PL_Position_cycle_user__c>());
				positionToPositionCycleUser.get(pcu.BI_PL_Position_cycle__r.BI_PL_Position__r.Name).add(pcu);
			}


		}


		List<BI_TM_User_territory__c> userToTerritory = [SELECT Id, BI_TM_Territory1__r.Name, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__r.External_Id__c, BI_TM_Start_date__c,BI_TM_End_date__c,BI_TM_Primary__c FROM BI_TM_User_territory__c WHERE  BI_TM_Territory1__r.Name IN: positionsName AND BI_TM_Country_Code__c =: countryCode];

		Map<String, List<BI_TM_User_territory__c>> mapUsersToTerritory = new Map<String, List<BI_TM_User_territory__c>>();

		for(BI_TM_User_territory__c ut : userToTerritory){
			if(mapUsersToTerritory.containsKey(ut.BI_TM_Territory1__r.Name)){
				mapUsersToTerritory.get(ut.BI_TM_Territory1__r.Name).add(ut);
			}else{
				mapUsersToTerritory.put(ut.BI_TM_Territory1__r.Name,new List<BI_TM_User_territory__c>());
				mapUsersToTerritory.get(ut.BI_TM_Territory1__r.Name).add(ut);
			}
		}


		List<BI_PL_Position_cycle_user__c> positionCycleToUpdate = new List<BI_PL_Position_cycle_user__c>();

		for(BI_PL_Position_cycle__c pc : scope){
			String position = pc.BI_PL_Position__r.Name;
			Date startDate = pc.BI_PL_Cycle__r.BI_PL_Start_Date__c;
			Date endDate = pc.BI_PL_Cycle__r.BI_PL_End_date__c;

			if(mapUsersToTerritory.containsKey(position)){
				for(BI_TM_User_territory__c ut : mapUsersToTerritory.get(position)){
					if(ut.BI_TM_Start_date__c <= startDate){
						if(ut.BI_TM_End_date__c == null || ut.BI_TM_End_date__c > startDate){
							String userId = ut.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c;
							if(positionToPositionCycleUser.containsKey(position)){
								List<BI_PL_Position_cycle_user__c> usersInPlanit = positionToPositionCycleUser.get(position);
					
								Boolean isUserInPlanit = false;
								Integer i = 0;
								while(isUserInPlanit == false && i<usersInPlanit.size()){
									BI_PL_Position_cycle_user__c pcu = usersInPlanit.get(i);
									if(pcu.BI_PL_User__c == userId){
										if(pcu.BI_PL_Preparation_owner__c && ut.BI_TM_Primary__c){
											isUserInPlanit = true;
										}else{
											if(pcu.BI_PL_Preparation_owner__c && !ut.BI_TM_Primary__c){
												pcu.BI_PL_Preparation_owner__c = false;
											}else if(!pcu.BI_PL_Preparation_owner__c && ut.BI_TM_Primary__c){
												List<BI_PL_Position_cycle_user__c> pcusersTou = getPreparationOwner(usersInPlanit); 
												if(!pcusersTou.isEmpty()) positionCycleToUpdate.addAll(pcusersTou);
												pcu.BI_PL_Preparation_owner__c = true;
											}											
											positionCycleToUpdate.add(pcu);
											if(this.mapCycleAndHierarchyToUpdateOwner.containsKey(pc.BI_PL_Cycle__c)){
												this.mapCycleAndHierarchyToUpdateOwner.get(pc.BI_PL_Cycle__c).add(pc.BI_PL_Hierarchy__c);
											}else{
												this.mapCycleAndHierarchyToUpdateOwner.put(pc.BI_PL_Cycle__c, new Set<String>());
												this.mapCycleAndHierarchyToUpdateOwner.get(pc.BI_PL_Cycle__c).add(pc.BI_PL_Hierarchy__c);
											}
											//this.hierarchiesToUpdateOwner.add(pc.BI_PL_Hierarchy__c);
											isUserInPlanit = true;
										}
									}

									i++;
								}

								if(!isUserInPlanit){

									if(ut.BI_TM_Primary__c){
										List<BI_PL_Position_cycle_user__c> pcusersTou = getPreparationOwner(usersInPlanit); 
										if(!pcusersTou.isEmpty()) positionCycleToUpdate.addAll(pcusersTou);
										BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = pc.Id, BI_PL_User__c = userId, BI_PL_Preparation_owner__c = true);										
										newPositionCycleUsers.add(pcu);
									}else{
										BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = pc.Id, BI_PL_User__c = userId, BI_PL_Preparation_owner__c = false);										
										newPositionCycleUsers.add(pcu);
									}
									
									if(this.mapCycleAndHierarchyToUpdateOwner.containsKey(pc.BI_PL_Cycle__c)){
										this.mapCycleAndHierarchyToUpdateOwner.get(pc.BI_PL_Cycle__c).add(pc.BI_PL_Hierarchy__c);
									}else{
										this.mapCycleAndHierarchyToUpdateOwner.put(pc.BI_PL_Cycle__c, new Set<String>());
										this.mapCycleAndHierarchyToUpdateOwner.get(pc.BI_PL_Cycle__c).add(pc.BI_PL_Hierarchy__c);
									}
									//this.hierarchiesToUpdateOwner.add(pc.BI_PL_Hierarchy__c);
								}


							}else{
								BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c();
								if(ut.BI_TM_Primary__c){
									pcu.BI_PL_Position_cycle__c = pc.Id;
									pcu.BI_PL_User__c = userId;
									pcu.BI_PL_Preparation_owner__c = true;
									// pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = pc.Id, BI_PL_User__c = userId, BI_PL_Preparation_owner__c = true);
								}else{
									pcu.BI_PL_Position_cycle__c = pc.Id;
									pcu.BI_PL_User__c = userId;
									pcu.BI_PL_Preparation_owner__c = false;
									//BI_PL_Position_cycle_user__c pcu = new BI_PL_Position_cycle_user__c(BI_PL_Position_cycle__c = pc.Id, BI_PL_User__c = userId, BI_PL_Preparation_owner__c = true);

								}
								
								newPositionCycleUsers.add(pcu);
								if(this.mapCycleAndHierarchyToUpdateOwner.containsKey(pc.BI_PL_Cycle__c)){
									this.mapCycleAndHierarchyToUpdateOwner.get(pc.BI_PL_Cycle__c).add(pc.BI_PL_Hierarchy__c);
								}else{
									this.mapCycleAndHierarchyToUpdateOwner.put(pc.BI_PL_Cycle__c, new Set<String>());
									this.mapCycleAndHierarchyToUpdateOwner.get(pc.BI_PL_Cycle__c).add(pc.BI_PL_Hierarchy__c);
								}
								//this.hierarchiesToUpdateOwner.add(pc.BI_PL_Hierarchy__c);
							}
						}
					}
				}
			}



		}

		System.debug('New position Cycles: ' + newPositionCycleUsers);

		if(!newPositionCycleUsers.isEmpty()){} insert newPositionCycleUsers;

		System.debug('**positionCycleToUpdate; ' + positionCycleToUpdate);
		System.debug('*-*mapCycleAndHierarchyToUpdateOwner: ' + mapCycleAndHierarchyToUpdateOwner);

		if(!positionCycleToUpdate.isEmpty()){

			Set<BI_PL_Position_cycle_user__c> removeDuplicates = new Set<BI_PL_Position_cycle_user__c>(positionCycleToUpdate);
			List<BI_PL_Position_cycle_user__c> finalListToUpsert = new List<BI_PL_Position_cycle_user__c>(removeDuplicates);
			upsert finalListToUpsert BI_PL_External_Id__c;

		} 




	
	}
	
	global void finish(Database.BatchableContext BC) {
		
		if(this.mapCycleAndHierarchyToUpdateOwner.size() > 0){
			for(String cycleId : this.mapCycleAndHierarchyToUpdateOwner.keySet()){
				List<String> hierarchies = new List<String> (this.mapCycleAndHierarchyToUpdateOwner.get(cycleId));
				if(hierarchies.size() > 1){
					Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycleId,hierarchies.get(0),false,null,hierarchies));
				}else{
					Database.executeBatch(new BI_PL_PreparationVisibilityUpdaterBatch(cycleId,hierarchies.get(0),false));
				}
			}
		}
		
		
		
	}

	private List<BI_PL_Position_cycle_user__c> getPreparationOwner(List<BI_PL_Position_cycle_user__c> pcUser){
	
		List<BI_PL_Position_cycle_user__c> listToReturn = new List<BI_PL_Position_cycle_user__c>();

		for(BI_PL_Position_cycle_user__c pcu : pcUser){
			if(pcu.BI_PL_Preparation_owner__c){
				pcu.BI_PL_Preparation_owner__c = false;
				listToReturn.add(pcu);
			}
		}
		return listToReturn;



	}
	
}