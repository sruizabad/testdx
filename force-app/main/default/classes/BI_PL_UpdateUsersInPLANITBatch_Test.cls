@isTest
private class BI_PL_UpdateUsersInPLANITBatch_Test{
    static Profile profile;

    @isTest
    static void test()  {
        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();

        User thisUser = [SELECT Id,Country_Code_BI__c from User where Id = :UserInfo.getUserId() ];
        String countryCode = thisUser.Country_code_BI__c;
        list<Account> accs = BI_PL_TestDataFactory.createTestAccounts(5,countryCode);
        BI_PL_TestDataFactory.createCycleStructure(countryCode);
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_ID__c from BI_PL_Position_Cycle__c];
        String cycleId = [SELECT Id from BI_PL_cycle__c LIMIT 1].Id;
        
        List<Product_vod__c> products = BI_PL_TestDataFactory.createTestProduct(3,countryCode);

        BI_PL_TestDataFactory.createPreparations(countryCode, posCycles, accs, products);
        List<Account> paccs = BI_PL_TestDataFactory.createTestPersonAccounts(3,countryCode);
        
        List<String> lCycles = new List<String>(); 
        for (BI_PL_cycle__c a : [SELECT Id from BI_PL_cycle__c])
            lCycles.add(a.Id);

        List<String> positionname = new List<String> ();
        for(BI_PL_position__c pos: [SELECT Name from BI_PL_position__c])
            positionname.add(pos.Name);

        profile = [SELECT Id FROM Profile WHERE Name LIKE 'US_%' LIMIT 1];


        User user1 = createUser('user1.omega');
        User user2 = createUser('user2.omega');
        User user3 = createUser('user3.omega');
        User user4 = createUser('user4.omega');

        insert new List<User> {user1, user2, user3, user4};
        

        // Data creation
        BI_TM_User_mgmt__c um1 = createUserManagement(user1);
        BI_TM_User_mgmt__c um2 = createUserManagement(user2);
        BI_TM_User_mgmt__c um3 = createUserManagement(user3);
        BI_TM_User_mgmt__c um4 = createUserManagement(user4);

        insert new List<BI_TM_User_mgmt__c> {um1, um2, um3, um4};

        BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'CARD', BI_TM_Country_Code__c = 'US', BI_TM_Business__c = 'PM');

        insert fieldForce;

        BI_TM_Alignment__c alignment = new BI_TM_Alignment__c(BI_TM_Country_Code__c = 'US', BI_TM_FF_type__c = fieldForce.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c = 'PM');

        insert alignment;

        BI_TM_Position_Type__c posType = new BI_TM_Position_Type__c(Name = 'PosType1', BI_TM_Business__c='PM', BI_TM_Country_Code__c = 'BR');
        insert posType;

        BI_PL_Cycle__c selectedCycle = new BI_PL_Cycle__c(BI_PL_Start_date__c = alignment.BI_TM_Start_date__c, BI_PL_End_date__c = alignment.BI_TM_End_date__c, BI_PL_Field_force__c = alignment.BI_TM_FF_type__c, BI_PL_Country_code__c = alignment.BI_TM_Country_Code__c);

        insert selectedCycle;

        BI_TM_Territory__c territory1 = new BI_TM_Territory__c(Name = positionname[0], BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = true, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1,  BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');

        insert territory1;

        BI_TM_Territory__c territory2 = new BI_TM_Territory__c(Name = positionname[2], BI_TM_Parent_Position__c = territory1.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');
        BI_TM_Territory__c territory3 = new BI_TM_Territory__c(Name = positionname[3], BI_TM_Parent_Position__c = territory1.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');

        insert new List<BI_TM_Territory__c> {territory2, territory3};

        BI_TM_Territory__c territory4 = new BI_TM_Territory__c(Name = positionname[4], BI_TM_Parent_Position__c = territory2.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');
        BI_TM_Territory__c territory5 = new BI_TM_Territory__c(Name = 'territory5', BI_TM_Parent_Position__c = territory2.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today() + 20, BI_TM_End_date__c = Date.today() + 21, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');
        BI_TM_Territory__c territory7 = new BI_TM_Territory__c(Name = 'territory7', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = true, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 21,  BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');
        
        
        insert new List<BI_TM_Territory__c> {territory4, territory5,  territory7};
        BI_TM_Territory__c territory6 = new BI_TM_Territory__c(Name = 'territory6', BI_TM_Parent_Position__c = territory7.Id, BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_Is_Root__c = false, BI_TM_Is_Active_Checkbox__c = true, BI_TM_FF_type__c = fieldForce.Id, BI_TM_Global_Position_Type__c = 'HE', BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 21, BI_TM_Business__c = 'PM', BI_TM_Position_Type_Lookup__c = posType.Id, BI_TM_Position_Level__c='Country');
        insert territory6;

        BI_TM_Position_relation__c pr1 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory1.BI_TM_Parent_Position__c, BI_TM_Position__r = territory1, BI_TM_Position__c = territory1.Id, BI_TM_Business__c = 'PM');
        BI_TM_Position_relation__c pr2 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory2.BI_TM_Parent_Position__c, BI_TM_Position__r = territory2, BI_TM_Position__c = territory2.Id, BI_TM_Business__c = 'PM');
        BI_TM_Position_relation__c pr3 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory3.BI_TM_Parent_Position__c, BI_TM_Position__r = territory3, BI_TM_Position__c = territory3.Id, BI_TM_Business__c = 'PM');
        BI_TM_Position_relation__c pr4 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory4.BI_TM_Parent_Position__c, BI_TM_Position__r = territory4, BI_TM_Position__c = territory4.Id, BI_TM_Business__c = 'PM');

        BI_TM_Position_relation__c pr5 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory5.BI_TM_Parent_Position__c, BI_TM_Position__r = territory5, BI_TM_Position__c = territory5.Id, BI_TM_Business__c = 'PM');

        BI_TM_Position_relation__c pr6 = new BI_TM_Position_relation__c(BI_TM_Alignment_Cycle__c = alignment.Id, BI_TM_Parent_Position__c = territory7.id, BI_TM_Parent_Position__r = territory7, BI_TM_Position__r = territory6, BI_TM_Position__c = territory6.Id, BI_TM_Business__c = 'PM');

        

        pr1.BI_TM_Position__r.Name = positionname[0];
        pr2.BI_TM_Position__r.Name = positionname[1];
        pr3.BI_TM_Position__r.Name = positionname[2];
        pr4.BI_TM_Position__r.Name = 'territory4';
        pr5.BI_TM_Position__r.Name = 'territory5';
        pr6.BI_TM_Position__r.Name = 'territory6';

        List<BI_TM_Position_relation__c> lstPositionRelation = new List<BI_TM_Position_relation__c> {pr1, pr2, pr3, pr4, pr5, pr6};

        insert lstPositionRelation;


        BI_TM_User_territory__c pcu1 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory1.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user1.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um1.Id, BI_TM_External_Id__c = 'test1', BI_TM_Assignment_Type__c = 'Administration');
        BI_TM_User_territory__c pcu2 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory2.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user2.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um2.Id, BI_TM_External_Id__c = 'test2', BI_TM_Assignment_Type__c = 'Administration');
        BI_TM_User_territory__c pcu3 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory3.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user3.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um3.Id, BI_TM_External_Id__c = 'test3', BI_TM_Assignment_Type__c = 'Administration');
        BI_TM_User_territory__c pcu4 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory4.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user4.Id, BI_TM_Start_date__c = Date.today(), BI_TM_End_date__c = Date.today() + 1, BI_TM_User_mgmt_tm__c = um4.Id, BI_TM_External_Id__c = 'test4', BI_TM_Assignment_Type__c = 'Administration');

        BI_TM_User_territory__c pcu5 = new BI_TM_User_territory__c(BI_TM_Territory1__c = territory4.Id, BI_TM_Business__c = 'PM', BI_TM_Country_code__c = fieldForce.BI_TM_Country_Code__c, BI_TM_User__c = user3.Id, BI_TM_Start_date__c = Date.today() + 20, BI_TM_End_date__c = Date.today() + 21, BI_TM_User_mgmt_tm__c = um4.Id, BI_TM_External_Id__c = 'test5', BI_TM_Assignment_Type__c = 'Administration');
        
        
        insert new List<BI_TM_User_territory__c> {pcu1, pcu2, pcu3, pcu4, pcu5};

        Test.startTest();

        BI_PL_Cycle__c cycle = [SELECT Id, BI_PL_Start_date__c, BI_PL_End_date__c, BI_PL_Field_force__c, BI_PL_Field_force__r.Name, BI_PL_Country_code__c FROM BI_PL_Cycle__c LIMIT 1];

        BI_TM_Position_relation__c[] positionRelations = [SELECT Id, Name, BI_TM_Position__c, BI_TM_Position__r.Name, BI_TM_Parent_Position__r.Name, BI_TM_Parent_Position__r.BI_TM_FF_type__r.Name, BI_TM_Parent_Position__c, BI_TM_Position__r.BI_TM_FF_type__r.Name FROM BI_TM_Position_relation__c
                WHERE BI_TM_Alignment_Cycle__c = : alignment.Id
                        AND BI_TM_Position__r.BI_TM_Start_date__c <= TODAY
                        AND (BI_TM_Position__r.BI_TM_End_date__c = null OR BI_TM_Position__r.BI_TM_End_date__c >= TODAY)];

        BI_PL_BITMANtoPLANiTImportUtility.importFromBitman(alignment.Id, Date.today(), cycle.Id, positionRelations);
        String externalId = '';
        try {
            externalId = BI_PL_BITMANtoPLANiTImportUtility.generateCycleExternalId(selectedCycle);
        } catch (Exception e) {}

        Test.stopTest();

        // Checking
        // Position
        List<BI_PL_Position__c> positions = [SELECT Id, Name, BI_PL_Field_Force__c, BI_PL_External_id__c, BI_PL_Country_code__c FROM BI_PL_Position__c ORDER BY Id];

        System.debug('>>>>positions ' + positions);

        
        String testExtId = BI_PL_BITMANtoPLANiTImportUtility.generatePositionExternalId(positions.get(0));

        // Position Cycle
        List<BI_PL_Position_cycle__c> positionCycles = [SELECT Id, BI_PL_Position__c, BI_PL_Parent_position__c, BI_PL_Cycle__c, BI_PL_Hierarchy__c, BI_PL_External_id__c FROM BI_PL_Position_cycle__c ORDER BY Id];

        
 


        Database.executeBatch(new BI_PL_UpdateUsersInPLANITBatch(countryCode, lCycles));    

    }




    
    private static void checkPosition(String territoryFieldForce, BI_PL_Position__c position) {
            }
    
    private static void checkPositionCycle(BI_PL_Cycle__c cycle, BI_PL_Position_cycle__c positionCycle, BI_PL_Position__c position, Id parentPosition) {
        String testExtID = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleExternalId(cycle, positionCycle.BI_PL_Hierarchy__c, position.Name);
    }
    
    private static void checkPositionCycleUser(BI_PL_Cycle__c cycle, BI_PL_Position_cycle_user__c pcu, BI_PL_Position_cycle__c positionCycle, Id userId, String positionName, String userName) {
        String testExtID = BI_PL_BITMANtoPLANiTImportUtility.generatePositionCycleUserExternalId(cycle.BI_PL_Country_code__c, cycle.BI_PL_Start_date__c, cycle.BI_PL_End_date__c, cycle.BI_PL_Field_Force__r.Name, positionCycle.BI_PL_Hierarchy__c, positionName, pcu.BI_PL_User__r.External_id__c);
    }
    
    private static User createUser(String name) {
        return new User(Alias = name.substring(0, 5), Email = name + '@testorg.com',
                        EmailEncodingKey = 'UTF-8', LastName = name, LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US', ProfileId = profile.Id, UserName = name + '@testorg.com', TimeZoneSidKey = 'America/Los_Angeles', External_id__c = name + '_External_id_Test');
    }
    
    private static BI_TM_User_mgmt__c createUserManagement(User user) {
        return new BI_TM_User_mgmt__c(BI_TM_Currency__c = 'USD', BI_TM_UserId_Lookup__c = user.Id, BI_TM_LanguageLocaleKey__c = user.LanguageLocaleKey,
                                      BI_TM_LocaleSidKey__c = user.LocaleSidKey, BI_TM_Username__c = user.UserName, BI_TM_TimeZoneSidKey__c = user.TimeZoneSidKey,BI_TM_UserCountryCode__c='US',
                                      BI_TM_Alias__c = 'Alias',
                                      BI_TM_Business__c = 'PM', 
                                      BI_TM_COMMUNITYNICKNAME__c = 'CName'+generaNumeroAleatorio(3), 
                                      BI_TM_Email__c = 'email'+generaNumeroAleatorio(3)+'@mail.com', 
                                      BI_TM_Last_name__c = 'LName', 
                                      BI_TM_Profile__c = 'US_AMA', 
                                      BI_TM_Start_date__c = Date.parse(system.today().format()));

    }
    
    private static String generaNumeroAleatorio(Integer nposiciones) {
       integer lower= 0;
       integer upper= 9;

       if( nposiciones == null || nposiciones <= 0 || nposiciones >= 20)
           nposiciones = 3;

       string alea= '';
       for( integer i=1; i<=nposiciones; i++) {
           alea += (Integer) (Math.floor(Math.random() * (upper - lower)) + lower);
       }

       return alea;
   }
}