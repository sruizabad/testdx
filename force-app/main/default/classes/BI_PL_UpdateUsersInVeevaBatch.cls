global class BI_PL_UpdateUsersInVeevaBatch implements Database.Batchable<sObject> {
	
	String countryCode;
	List<String> listCycles;
	List<String> externalIdVeevaSAP;
	String cycle;
	List<String> externalIdVeevaMCCP;
	

	global BI_PL_UpdateUsersInVeevaBatch(String cCode, List<String> lCycles) {
		this.countryCode = cCode;
		this.listCycles = lCycles;
		this.cycle = this.listCycles.remove(0);
		this.externalIdVeevaSAP = new List<String>();
		this.externalIdVeevaMCCP = new List<String>();



	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, BI_PL_Position__r.Name, BI_PL_Parent_position__r.Name, BI_PL_Parent_position__c, BI_PL_Hierarchy__c, BI_PL_External_Id__c, BI_PL_Cycle__r.BI_PL_Country_code__c, BI_PL_Cycle__r.BI_PL_Start_Date__c, BI_PL_Cycle__r.BI_PL_End_date__c,BI_PL_Cycle__r.Name FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c =: cycle]);
	}

   	global void execute(Database.BatchableContext BC, List<BI_PL_Position_cycle__c> scope) {

   		Date startDate = scope.get(0).BI_PL_Cycle__r.BI_PL_Start_Date__c;
   		Date endDate = scope.get(0).BI_PL_Cycle__r.BI_PL_End_date__c;

   		Map<String,List<BI_PL_Position_cycle__c>> cyclesToPositionsCycle = new Map<String, List<BI_PL_Position_cycle__c>>();
   		Set<String> positionsName = new Set<String>();

   		for(BI_PL_Position_cycle__c pc : scope){
   			
   			positionsName.add(pc.BI_PL_Position__r.Name);   			
   			this.externalIdVeevaSAP.add(getVeevaExternalId(pc));
   			String externalIdMCCP = pc.BI_PL_Cycle__r.Name + '_' + pc.BI_PL_Position__r.Name;
   			this.externalIdVeevaMCCP.add(externalIdMCCP);
   		}

   		List<BI_TM_User_territory__c> userToTerritory = [SELECT Id, BI_TM_Territory1__r.Name, BI_TM_Primary__c,BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__r.External_Id__c FROM BI_TM_User_territory__c WHERE  BI_TM_Territory1__r.Name IN: positionsName AND BI_TM_Active__c = true];



   		Map<String, List<BI_TM_User_territory__c>> mapUserToTerritory = new Map<String, List<BI_TM_User_territory__c>>();

		for(BI_TM_User_territory__c tm : userToTerritory){
			if(!mapUserToTerritory.containsKey(tm.BI_TM_Territory1__r.Name)){
				mapUserToTerritory.put(tm.BI_TM_Territory1__r.Name,new List<BI_TM_User_territory__c>());
				mapUserToTerritory.get(tm.BI_TM_Territory1__r.Name).add(tm);
			}else{
				mapUserToTerritory.get(tm.BI_TM_Territory1__r.Name).add(tm);
			}
		}

		

		
		List<Cycle_Plan_vod__c> veevaSapsToUpdate = new List<Cycle_Plan_vod__c>();
		//OldUser,Territory,NewUser
		Map<Id, Map<String,Id>> territoriesToUserForVco = new Map<Id, Map<String,Id>>();
		List<Id> oldUsersSap = new List<Id>();

		for(Cycle_Plan_vod__c sap : [SELECT Id, OwnerId, External_ID2__c, Territory_vod__c FROM Cycle_Plan_vod__c WHERE External_ID2__c IN: externalIdVeevaSAP]){
			Id owner = sap.OwnerId;
			if(mapUserToTerritory.containsKey(sap.Territory_vod__c)){
				List<BI_TM_User_territory__c> bimtnaUserTerritoryList = mapUserToTerritory.get(sap.Territory_vod__c);

				BI_TM_User_territory__c currentUser = getPrimary(bimtnaUserTerritoryList);
				if(currentUser == null ){
					currentUser = bimtnaUserTerritoryList.get(0);
				}
				
				if(owner != currentUser.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c){
					sap.OwnerId = currentUser.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c;
					veevaSapsToUpdate.add(sap);
					if(!territoriesToUserForVco.containsKey(owner)){

						territoriesToUserForVco.put(owner,new Map<String,Id>());
						territoriesToUserForVco.get(owner).put(sap.Territory_vod__c,currentUser.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);
						oldUsersSap.add(owner);

					}else{
						territoriesToUserForVco.get(owner).put(sap.Territory_vod__c,currentUser.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);
					}
						
				}
			}
		}
		

		
		
		List<MC_Cycle_Plan_vod__c> veevaMccpsToUpdate = new List<MC_Cycle_Plan_vod__c>();
		//OldUser,Territory,NewUser
		
		List<Id> oldUsersMCCP = new List<Id>();
		System.debug('externalIdVeevaMCCP: ' + this.externalIdVeevaMCCP);
		for(MC_Cycle_Plan_vod__c sapMccp : [SELECT Id, OwnerId, External_Id_vod__c, Territory_vod__c FROM MC_Cycle_Plan_vod__c WHERE External_Id_vod__c IN: externalIdVeevaMCCP]){
			Id owner = sapMccp.OwnerId;
			if(mapUserToTerritory.containsKey(sapMccp.Territory_vod__c)){
				List<BI_TM_User_territory__c> bimtnaUserTerritoryList = mapUserToTerritory.get(sapMccp.Territory_vod__c);

				BI_TM_User_territory__c currentUser = getPrimary(bimtnaUserTerritoryList);
				if(currentUser == null ){
					currentUser = bimtnaUserTerritoryList.get(0);
				}
				
				if(owner != currentUser.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c){
					sapMccp.OwnerId = currentUser.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c;
					veevaMccpsToUpdate.add(sapMccp);
					if(!territoriesToUserForVco.containsKey(owner)){

						territoriesToUserForVco.put(owner,new Map<String,Id>());
						territoriesToUserForVco.get(owner).put(sapMccp.Territory_vod__c,currentUser.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);
						oldUsersMCCP.add(owner);

					}else{
						territoriesToUserForVco.get(owner).put(sapMccp.Territory_vod__c,currentUser.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);
					}
						
				}
			}
		}
		



		List<Call_Objective_vod__c> callObjectivesToUpdate = new List<Call_Objective_vod__c>();

		for(Call_Objective_vod__c vco : [SELECT Id, External_ID_BI__c, OwnerId,From_Date_vod__c,To_Date_vod__c FROM Call_Objective_vod__c WHERE From_Date_vod__c =: startDate AND To_Date_vod__c =: endDate AND (OwnerId IN: oldUsersSap OR OwnerId IN: oldUsersMCCP)]){

			if(territoriesToUserForVco.containsKey(vco.OwnerId)){
				Id oldOwnerId = vco.OwnerId;
				for(String position : territoriesToUserForVco.get(oldOwnerId).keySet()){
					if(vco.External_ID_BI__c.contains(position)){
						vco.OwnerId = territoriesToUserForVco.get(oldOwnerId).get(position);
						callObjectivesToUpdate.add(vco);
					}
				}
			}
		}


		if(veevaSapsToUpdate.size() > 0){		
			Database.SaveResult [] updateResult =  Database.update(veevaSapsToUpdate,false);
			for (Database.SaveResult r : updateResult)
			{
				if (!r.isSuccess())
				{			
					for (Database.Error e : r.getErrors())
					{
						System.debug('Error Message:  ' + e.getMessage());
					}
				}
			}
		}

		if(veevaMccpsToUpdate.size() > 0){		
			Database.SaveResult [] updateResult =  Database.update(veevaMccpsToUpdate,false);
			for (Database.SaveResult r : updateResult)
			{
				if (!r.isSuccess())
				{			
					for (Database.Error e : r.getErrors())
					{
						System.debug('Error Message:  ' + e.getMessage());
					}
				}
			}
		}

		if(callObjectivesToUpdate.size() > 0){		
			Database.SaveResult [] updateResult =  Database.update(callObjectivesToUpdate,false);
			for (Database.SaveResult r : updateResult)
			{
				if (!r.isSuccess())
				{			
					for (Database.Error e : r.getErrors())
					{
						System.debug('Error Message:  ' + e.getMessage());
					}
				}
			}
		}




	
	}
	
	global void finish(Database.BatchableContext BC) {
		if(this.listCycles.size() > 0){
			Database.executeBatch(new BI_PL_UpdateUsersInVeevaBatch(this.countryCode, this.listCycles));
		}
	}

	private String getVeevaExternalId(BI_PL_Position_cycle__c pc){

		

		String startDay = (pc.BI_PL_Cycle__r.BI_PL_Start_date__c.day() < 10) ? ('0' + pc.BI_PL_Cycle__r.BI_PL_Start_date__c.day()) : ('' + pc.BI_PL_Cycle__r.BI_PL_Start_date__c.day());
        String startMonth = (pc.BI_PL_Cycle__r.BI_PL_Start_date__c.month() < 10) ? ('0' + pc.BI_PL_Cycle__r.BI_PL_Start_date__c.month()) : ('' + pc.BI_PL_Cycle__r.BI_PL_Start_date__c.month());

        String endDay = (pc.BI_PL_Cycle__r.BI_PL_End_date__c.day() < 10) ? ('0' + pc.BI_PL_Cycle__r.BI_PL_End_date__c.day()) : ('' + pc.BI_PL_Cycle__r.BI_PL_End_date__c.day());
        String endMonth = (pc.BI_PL_Cycle__r.BI_PL_End_date__c.month() < 10) ? ('0' + pc.BI_PL_Cycle__r.BI_PL_End_date__c.month()) : ('' + pc.BI_PL_Cycle__r.BI_PL_End_date__c.month());

        String startDate = '' + pc.BI_PL_Cycle__r.BI_PL_Start_date__c.year() + startMonth + startDay;
        String endDate = '' + pc.BI_PL_Cycle__r.BI_PL_End_date__c.year() + endMonth + endDay;

        String externalId = pc.BI_PL_Cycle__r.BI_PL_Country_code__c + '-' + startDate + '-' + endDate + '-' + pc.BI_PL_Position__r.Name;

		return externalId; 
	}  


	private BI_TM_User_territory__c getPrimary(List<BI_TM_User_territory__c> tms){

		for(BI_TM_User_territory__c tm : tms){
			if(tm.BI_TM_Primary__c == true){
				return tm;
			}
		}

		return null;
	}
   
   
}