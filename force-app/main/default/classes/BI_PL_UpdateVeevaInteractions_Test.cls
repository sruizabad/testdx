@IsTest
public with sharing class BI_PL_UpdateVeevaInteractions_Test {
	
    public static final String STATUSVOD = 'Submitted_vod';

    @testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(2, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(2, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, Name, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id, BI_PL_External_Id__c, BI_PL_Position__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);
        
        List<BI_PL_Position__c> lstPosition = [SELECT Id, Name, BI_PL_External_Id__c FROM BI_PL_Position__c WHERE Id = :posCycles.get(0).BI_PL_Position__c];

        Call2_vod__c call2;
        List<Call2_vod__c> lstCall2 = new List<Call2_vod__c>();

        for(integer i = 0; i < 2; i++){
            call2 = new Call2_vod__c(
                Status_vod__c = STATUSVOD, 
                Call_Date_vod__c = Date.today(), 
                Account_vod__c = listAcc.get(i).Id, 
                Territory_vod__c = lstPosition.get(0).Name,
                CP1__c = listProd.get(i).Name
            );
            lstCall2.add(call2);
            call2 = new Call2_vod__c(
                Status_vod__c = STATUSVOD, 
                Call_Date_vod__c = Date.today(), 
                Account_vod__c = listAcc.get(i).Id, 
                Territory_vod__c = lstPosition.get(0).Name, 
                CP2__c = listProd.get(i).Name
            );
            lstCall2.add(call2);

        }
        insert lstCall2;        
    }

    @isTest
    public static void test() {

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        cycle.BI_PL_Start_date__c = Date.today() - 1;
        cycle.BI_PL_End_date__c = Date.today() + 2;
        update cycle;

        String userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1].Country_Code_BI__c;
        List<String> cycleids = new List<String>{ cycle.Id };
        List<String> hierarchyNames = new List<String>{BI_PL_TestDataFactory.hierarchy};
        List<String> channels = new List<String>{BI_PL_TestDataFactory.SCHANNEL}; 

        BI_PL_UpdateVeevaInteractions batchCountry = new BI_PL_UpdateVeevaInteractions(userCountryCode);
        BI_PL_UpdateVeevaInteractions bat = new BI_PL_UpdateVeevaInteractions();
        
        //bat.init(cycleids,  null, null, null);
        bat.init(cycleids, hierarchyNames, channels, null);

        for (BI_PL_Detail_preparation__c inPrep : [SELECT Id, BI_PL_Primary_interactions__c, BI_PL_Secondary_interactions__c 
                                                    FROM BI_PL_Detail_preparation__c 
                                                    WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycle.Id]){
            system.assertEquals(inPrep.BI_PL_Primary_interactions__c, 0);
        }

        Test.startTest();
        Database.executeBatch(bat, 200);
        Test.stopTest();

        Integer iCont = 0;
        for (BI_PL_Detail_preparation__c inPrep : [SELECT Id, BI_PL_Primary_interactions__c, BI_PL_Secondary_interactions__c 
                                                    FROM BI_PL_Detail_preparation__c 
                                                    WHERE BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Cycle__c = :cycle.Id]){
            if (inPrep.BI_PL_Primary_interactions__c > 0) iCont++;
        }
        System.assertNotEquals(iCont,0);
    }
}