global with sharing class BI_PL_ViewService {
	public BI_PL_ViewService() {
		
	}

	public static Map<String, PlanitColumn> allDetailColumns = new Map<String, PlanitColumn> {
			Label.BI_PL_Column_IMS_ID 					=> new PlanitColumn(Label.BI_PL_IMS_Label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.BI_External_ID_1__c'),
	        Label.BI_PL_Column_CM_ID 					=> new PlanitColumn(Label.BI_PL_CMS_Id_Label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.External_ID_vod__c'),
	        Label.BI_PL_Column_Specialty 				=> new PlanitColumn(Label.BI_PL_Speciality_Label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty__c'),
	        Label.BI_PL_Column_Customer_name 			=> new PlanitColumn(Label.BI_PL_Account_Name_Label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.Name'),
	        Label.BI_PL_Column_Customer_first_name 		=> new PlanitColumn(Schema.SObjectType.Account.fields.FirstName.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.FirstName'),
	        Label.BI_PL_Column_Customer_last_name 		=> new PlanitColumn(Schema.SObjectType.Account.fields.LastName.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.LastName'),
	        Label.BI_PL_Column_target_type				=> new PlanitColumn(Label.BI_PL_Type_Label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Type__c'),
	        Label.BI_PL_Column_position_name			=> new PlanitColumn(Label.BI_PL_Position_Label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_name__c'),
            Label.BI_PL_Column_address                  => new PlanitColumn(Schema.SObjectType.Address_vod__c.fields.Name.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Primary_address__c', Label.BI_PL_Column_address),
            Label.BI_PL_Column_City                     => new PlanitColumn(Schema.SObjectType.Address_vod__c.fields.City_vod__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Primary_address_city__c', Label.BI_PL_Column_City),
            Label.BI_PL_Column_State                    => new PlanitColumn(Schema.SObjectType.Address_vod__c.fields.State_vod__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Primary_address_state__c', Label.BI_PL_Column_State),
            Label.BI_PL_Column_Zip                      => new PlanitColumn(Schema.SObjectType.Address_vod__c.fields.Zip_vod__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Primary_address_zip__c', Label.BI_PL_Column_Zip),
	        Label.BI_PL_Column_district					=> new PlanitColumn('District', 'Id'),
	        Label.BI_PL_Column_region				    => new PlanitColumn('Region', 'Id'),
	        Label.BI_PL_Column_zone                     => new PlanitColumn('Zone', 'Id'),
	        Label.BI_PL_Subcolumn_productA 				=> new PlanitColumn(Label.BI_PL_Calls_Freq_Label, 'BI_PL_Adjusted_details__c'),
	        Label.BI_PL_Subcolumn_productSegment 		=> new PlanitColumn(Label.BI_PL_P1_CVM_Table_Label, 'BI_PL_Segment__c'),
	        Label.BI_PL_Subcolumn_strategic_segment		=> new PlanitColumn(Label.BI_PL_P1_Strategic_Table_Label, 'BI_PL_Strategic_Segment__c'),
			Label.BI_PL_Column_product_name  			=> new PlanitColumn(Label.BI_GLOBAL_PRODUCT_NAME, 'BI_PL_Product__r.Name'),
            Label.BI_PL_Column_secondary_product_name   => new PlanitColumn(Label.BI_PL_P2_Table_Label, 'BI_PL_Secondary_product__r.Name'),
            Label.BI_PL_Column_veeva_interactions       => new PlanitColumn(Label.BI_PL_Actual_Label, 'BI_PL_Primary_Interactions__c'),

            Label.BI_PL_Subcolumn_productP              => new PlanitColumn(Label.BI_PL_Planned_Label, 'BI_PL_Planned_details__c'),
            Label.BI_PL_Column_calculated               => new PlanitColumn(Label.BI_PL_Calculated_Label, 'BI_PL_To_veeva__c'),
            label.BI_PL_Column_Next_best_account            => new PlanitColumn(Label.BI_PL_Next_best_accounts, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Next_best_account__c', Label.BI_PL_Column_Next_best_account, 'utility/favorite'),
            Label.BI_PL_Column_Added_manually               => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Added_manually__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Added_manually__c', Label.BI_PL_Column_Added_manually, 'utility/add'),
            Label.BI_PL_Column_Primary_parent               => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Primary_parent__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Primary_parent__c', Label.BI_PL_Column_Primary_parent),
            Label.BI_PL_Column_Specialty                    => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Specialty__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Specialty__c', Label.BI_PL_Column_Specialty),
        
            Label.BI_PL_Column_MSL_flag                     => new PlanitColumn('TGT', 'BI_PL_Channel_detail__r.BI_PL_MSL_flag__c', Label.BI_PL_Column_MSL_flag),
            Label.BI_PL_Column_Pull_through                 => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Pull_through__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Pull_through__c', Label.BI_PL_Column_Pull_through),
            Label.BI_PL_Column_TgtA                         => new PlanitColumn(Label.BI_PL_TgtA, 'TgtA', Label.BI_PL_Column_TgtA),
            Label.BI_PL_Column_SumP                         => new PlanitColumn(Label.BI_SAP_Sum_P_Leyend_Field, 'BI_PL_Channel_detail__r.BI_PL_Sum_planned_interactions__c', Label.BI_PL_Column_SumP),
            Label.BI_PL_Column_SumA                         => new PlanitColumn(Label.BI_SAP_Total_A_Leyend_Field, 'BI_PL_Channel_detail__r.BI_PL_Sum_adjusted_interactions__c', Label.BI_PL_Column_SumA),
            Label.BI_PL_Column_Removed_date                 => new PlanitColumn(Schema.SObjectType.BI_PL_Channel_detail_preparation__c.fields.BI_PL_Removed_date__c.label, 'BI_PL_Channel_detail__r.BI_PL_Removed_date__c', Label.BI_PL_Column_Removed_date),
            Label.BI_PL_Column_HyperTarget                  => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_HyperTarget__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_HyperTarget__c', Label.BI_PL_Column_HyperTarget),
            Label.BI_PL_Column_Must_win                     => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Must_win__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Must_win__c', Label.BI_PL_Column_Must_win),
            Label.BI_PL_Column_NTL                          => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Ntl_value__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Ntl_value__c', Label.BI_PL_Column_NTL),
            Label.BI_PL_Column_PDRP                         => new PlanitColumn(Schema.SObjectType.Account.fields.PDRP_Opt_Out_vod__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_Target_customer__r.PDRP_Opt_Out_vod__c', Label.BI_PL_Column_PDRP),
            Label.BI_PL_Column_GPO_contract_align           => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_GPO_Contract_alignment__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_GPO_Contract_alignment__c', Label.BI_PL_Column_GPO_contract_align),
            Label.BI_PL_Column_HCP_access                   => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_HCP_Access__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_HCP_Access__c', Label.BI_PL_Column_HCP_access),
            Label.BI_PL_Column_DIAB                         => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_DIAB_percent__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_DIAB_percent__c', Label.BI_PL_Column_DIAB),
            Label.BI_PL_Column_CARD                         => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_CARD_percent__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_CARD_percent__c', Label.BI_PL_Column_CARD),
            Label.BI_PL_Column_RESP                         => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_RESP_percent__c.label, 'BI_PL_Channel_detail__r.BI_PL_Target__r.BI_PL_RESP_percent__c', Label.BI_PL_Column_RESP),

			Label.BI_PL_Column_planit  					=> new PlanitColumn(Label.BI_PL_Planit_Label, null, Label.BI_PL_Planit_Label, true, new List<PlanitColumn> {})
			
	};

     public static Map<String, PlanitColumn> allTargetColumns = new Map<String, PlanitColumn> {
        Label.BI_PL_Column_Customer_name                => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Target_customer__c.label, 'record.BI_PL_Target_customer__r.Name', Label.BI_PL_Column_Customer_name),
        Label.BI_PL_Column_Customer_first_name          => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Customer_first_name__c.label, 'record.BI_PL_Customer_first_name__c', Label.BI_PL_Column_Customer_first_name),
        Label.BI_PL_Column_Customer_last_name           => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Customer_last_name__c.label, 'record.BI_PL_Customer_last_name__c', Label.BI_PL_Column_Customer_last_name),
        label.BI_PL_Column_Next_best_account            => new PlanitColumn(Label.BI_PL_Next_best_accounts, 'record.BI_PL_Next_best_account__c', Label.BI_PL_Column_Next_best_account, 'utility/favorite'),
        Label.BI_PL_Column_Added_manually               => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Added_manually__c.label, 'record.BI_PL_Added_manually__c', Label.BI_PL_Column_Added_manually, 'utility/add'),
        Label.BI_PL_Column_shared                       => new PlanitColumn(Label.BI_PL_Shared, 'Label.BI_PL_Column_shared', 'currentTargetChannel.related.length','utility/socialshare'),
        Label.BI_PL_Column_Group                        => new PlanitColumn(Schema.SObjectType.BI_PL_Affiliation__c.fields.BI_PL_Group__c.label, 'affiliationColumns.BI_PL_Group__c', Label.BI_PL_Column_Group),
        Label.BI_PL_Column_Primary_parent               => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Primary_parent__c.label, 'record.BI_PL_Primary_parent__c', Label.BI_PL_Column_Primary_parent),
        Label.BI_PL_Column_Specialty                    => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Specialty__c.label, 'record.BI_PL_Specialty__c', Label.BI_PL_Column_Specialty),
        Label.BI_PL_Column_address                      => new PlanitColumn(Schema.SObjectType.Address_vod__c.fields.Name.label, 'addresses[0].Name', Label.BI_PL_Column_address),
        Label.BI_PL_Column_City                         => new PlanitColumn(Schema.SObjectType.Address_vod__c.fields.City_vod__c.label, 'addresses[0].City_vod__c', Label.BI_PL_Column_City),
        Label.BI_PL_Column_State                        => new PlanitColumn(Schema.SObjectType.Address_vod__c.fields.State_vod__c.label, 'addresses[0].State_vod__c', Label.BI_PL_Column_State),
        Label.BI_PL_Column_Zip                          => new PlanitColumn(Schema.SObjectType.Address_vod__c.fields.Zip_vod__c.label, 'addresses[0].Zip_vod__c', Label.BI_PL_Column_Zip),
        Label.BI_PL_Column_MSL_flag                     => new PlanitColumn('TGT', 'currentTargetChannel.record.BI_PL_MSL_flag__c', Label.BI_PL_Column_MSL_flag),
        Label.BI_PL_Column_Pull_through                 => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Pull_through__c.label, 'record.BI_PL_Pull_through__c', Label.BI_PL_Column_Pull_through),
        Label.BI_PL_Column_TgtA                         => new PlanitColumn(Label.BI_PL_TgtA, 'currentTargetChannel.TgtA', Label.BI_PL_Column_TgtA),
        Label.BI_PL_Column_SumP                         => new PlanitColumn(Label.BI_SAP_Sum_P_Leyend_Field, 'currentTargetChannel.record.BI_PL_Sum_planned_interactions__c', Label.BI_PL_Column_SumP),
        Label.BI_PL_Column_SumA                         => new PlanitColumn(Label.BI_SAP_Total_A_Leyend_Field, 'currentTargetChannel.record.BI_PL_Sum_adjusted_interactions__c', Label.BI_PL_Column_SumA),
        Label.BI_PL_Column_Removed_date                 => new PlanitColumn(Schema.SObjectType.BI_PL_Channel_detail_preparation__c.fields.BI_PL_Removed_date__c.label, 'currentTargetChannel.record.BI_PL_Removed_date__c', Label.BI_PL_Column_Removed_date),
        Label.BI_PL_Column_HyperTarget                  => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_HyperTarget__c.label, 'record.BI_PL_HyperTarget__c', Label.BI_PL_Column_HyperTarget),
        Label.BI_PL_Column_Must_win                     => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Must_win__c.label, 'record.BI_PL_Must_win__c', Label.BI_PL_Column_Must_win),
        Label.BI_PL_Column_NTL                          => new PlanitColumn(Schema.SObjectType.BI_PL_Target_preparation__c.fields.BI_PL_Ntl_value__c.label, 'record.BI_PL_Ntl_value__c', Label.BI_PL_Column_NTL),
        Label.BI_PL_Column_KAM                          => new PlanitColumn('KAM', 'KAM', Label.BI_PL_Column_KAM),
        Label.BI_PL_Column_RAENAD                       => new PlanitColumn('RAE/NAD', 'RAE/NAD', Label.BI_PL_Column_RAENAD),
        Label.BI_PL_Column_CM_ID                        => new PlanitColumn(Label.BI_PL_CMS_Id_Label, 'record.BI_PL_Target_customer__r.External_ID_vod__c', Label.BI_PL_Column_CM_ID),
        Label.BI_PL_Column_IMS_ID                       => new PlanitColumn(Label.BI_PL_IMS_Label, 'record.BI_PL_Target_customer__r.BI_External_ID_1__c', Label.BI_PL_Column_IMS_ID),
        Label.BI_PL_Column_PDRP                         => new PlanitColumn(Schema.SObjectType.Account.fields.PDRP_Opt_Out_vod__c.label, 'record.BI_PL_Target_customer__r.PDRP_Opt_Out_vod__c', Label.BI_PL_Column_PDRP),
        Label.BI_PL_Column_GPO_contract_align           => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_GPO_Contract_alignment__c.label, 'record.BI_PL_GPO_Contract_alignment__c', Label.BI_PL_Column_GPO_contract_align),
        Label.BI_PL_Column_HCP_access                   => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_HCP_Access__c.label, 'record.BI_PL_HCP_Access__c', Label.BI_PL_Column_HCP_access),
        Label.BI_PL_Column_DIAB                         => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_DIAB_percent__c.label, 'record.BI_PL_DIAB_percent__c', Label.BI_PL_Column_DIAB),
        Label.BI_PL_Column_CARD                         => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_CARD_percent__c.label, 'record.BI_PL_CARD_percent__c', Label.BI_PL_Column_CARD),
        Label.BI_PL_Column_RESP                         => new PlanitColumn(Schema.SObjectType.BI_PL_Target_Preparation__c.fields.BI_PL_RESP_percent__c.label, 'record.BI_PL_RESP_percent__c', Label.BI_PL_Column_RESP)

        //MISSING THOS ONES WITH 'value'
    };

    public static Map<String, PlanitColumn> allTargetSubColumns = new Map<String, PlanitColumn> {};

	public static List<BI_PL_View__c> getTargetViews(){
		return null;
	}

	public static Map<String, BI_PL_View__c> getTargetsViews(String fieldforce, String countryCode) {
        System.debug(loggingLevel.Error, '*** getTargetsView fieldforce: ' + fieldforce);
        System.debug(loggingLevel.Error, '*** getTargetsView countryCode: ' + countryCode);

        Map<String, BI_PL_View__c> viewByType = new Map<String, BI_PL_View__c>();

        BI_PL_View__c defaultHCPView;
        BI_PL_View__c defaultHCOView;


        for (BI_PL_View__c ffView : [SELECT Id, BI_PL_Field_Force__c, BI_PL_Field_Forces__c, BI_PL_Country_code__c, 
        							BI_PL_Columns__c, BI_PL_Type__c,
                                    BI_PL_Default_product_view__c, BI_PL_Hidden_products__c,
                                    BI_PL_Allowed_to_add_products__c, BI_PL_Targets_page_size__c, 
                                    BI_PL_Allow_add_prod_only_added_target__c

                                     FROM BI_PL_View__c
                                     WHERE BI_PL_Country_code__c = :countryCode
                                     AND BI_PL_Object_type__c = 'Target']) {

            if (ffView.BI_PL_Field_Forces__c != null) {
                System.debug(loggingLevel.Error, '*** HAS FF  ffView.BI_PL_Field_Forces__c: ' + ffView.BI_PL_Field_Forces__c);

                if (ffView.BI_PL_Field_Forces__c.contains(';') && ffView.BI_PL_Field_Forces__c.contains(';' + fieldforce + ';') ) {
                    System.debug(loggingLevel.Error, '*** MATCHED FIELD FORCE: ' + fieldforce);
                    viewByType.put(ffView.BI_PL_Type__c, ffView);
                }

            }

            if (ffView.BI_PL_Field_Forces__c == null) {

                System.debug(loggingLevel.Error, '*** Default -> ' + viewByType);

                if (!viewByType.containsKey(ffView.BI_PL_Type__c)) {
                    if (ffView.BI_PL_Type__c == Label.BI_PL_View_HCP) {
                        defaultHCPView = ffView;
                    } else if (ffView.BI_PL_Type__c == Label.BI_PL_View_HCO) {
                        defaultHCOView = ffView;
                    }
                    viewByType.put(ffView.BI_PL_Type__c, ffView);
                }
            }

        }

        // GLOS-772: hide the Next Best tab if there's no view set.
        /*
        if (!viewByType.containsKey(Label.BI_PL_View_NextBest_HCO)) {
            viewByType.put(Label.BI_PL_View_NextBest_HCO, defaultHCOView);
        }
        if (!viewByType.containsKey(Label.BI_PL_View_NextBest_HCP)) {
            viewByType.put(Label.BI_PL_View_NextBest_HCP, defaultHCPView);
        }*/
        if (!viewByType.containsKey(Label.BI_PL_View_NoSee_HCO)) {
            viewByType.put(Label.BI_PL_View_NoSee_HCO, defaultHCOView);
        }
        if (!viewByType.containsKey(Label.BI_PL_View_NoSee_HCP)) {
            viewByType.put(Label.BI_PL_View_NoSee_HCP, defaultHCPView);
        }

        return viewByType;
    }

    /**
     * Return a Detail View that matches given parameters
     *
     * @param      fieldforce   The fieldforce
     * @param      countryCode  The country code
     * @param      typ          The view type
     *
     * @return     The details view.
     */
	public static BI_PL_View__c getDetailsView(String fieldforce, String countryCode,  String typ){
		String query = 'SELECT Id, BI_PL_Field_Force__c, BI_PL_Field_Forces__c, BI_PL_Country_code__c, BI_PL_Columns__c, BI_PL_Type__c, BI_PL_Hidden_products__c, BI_PL_Query_locator__c, BI_PL_Export_file_extension__c, BI_PL_Export_data_encapsulator__c, BI_PL_Export_separator__c FROM BI_PL_View__c WHERE BI_PL_Object_type__c = \'Detail\'';
        
        if(countryCode != null) {
        	query += ' AND BI_PL_Country_code__c = :countryCode';
        }

        if(typ != null) {
            query += ' AND BI_PL_Type__c = :typ';
        } 

        query += ' LIMIT 1 ';

        BI_PL_View__c ret = Database.query(query);
        
        if(fieldforce != null && ret != null  ) {
            if(ret.BI_PL_Field_Forces__c.contains(';') && !ret.BI_PL_Field_Forces__c.contains(';' + fieldforce + ';')){
                ret = null;
            }
        } 

        return  ret;                    
		//return null;
	}

	/**
	 * Gets a list of the column Apex wrapper for a give View record 
	 *
	 * @param      view  The view
	 *
	 * @return     The view columns.
	 */
	public static List<PlanitColumn> getViewColumns(BI_PL_View__c view) {
		List<PlanitColumn> result = new List<PlanitColumn>();
		List<String> columnCodes = view.BI_PL_Columns__c.split(';');

		for(String code : columnCodes) {
			if(allDetailColumns.containsKey(code)) {
				result.add(allDetailColumns.get(code));
			}
		}

		return result;
	}


	//
	//
	//// WRAPPER CLASSES
	//
	//
    //



	public class PlanitColumn {
    	public String label {get; set;}
    	public String field {get; set;}
        public String key {get; set;}
    	public String icon {get; set;}
    	public String groupKey {get; set;}
    	public Boolean isGroup = false;
    	public Boolean hidden = false;
    	public Boolean hidable = false;
    	public list<PlanitColumn> childColumns;


        public PlanitColumn(String label, String field) {
            this.label = label;
            this.field = field;
            this.key = field;
        }

        public PlanitColumn(String label, PlanitColumnValue value) {
            this.label = label;
            this.field = field;
            this.key = field;
        }

        
    	public PlanitColumn(String label, String field, String key) {
    		this.label = label;
    		this.field = field;
    		this.key = key;
    	}

        public PlanitColumn(String label, String field, String key, String icon) {
            this.label = label;
            this.field = field;
            this.key = key;
            this.icon = icon;
        }

    	public PlanitColumn(String label, String field, String key, Boolean isGroup, list<PlanitColumn> children) {

    		this.label = label;
    		this.field = field;
    		this.key = key;
    		this.isGroup = true;
    		this.childColumns = new List<PlanitColumn>();
            this.childColumns = children;

    	}

    	public void setGroupKey(String nkey){
    		this.groupKey = nkey;
    	}

    	public void addChildColumn(PlanitColumn col) {
    		col.setGroupKey(this.key);
    		this.childColumns.add(col);
    	}

    }

    public class PlanitColumnValue {
        String expr;
        public PlanitColumnValue(String expression, String fallbackField) {
            this.expr = expression; 
        }
    }

}