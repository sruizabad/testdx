@isTest
private class BI_PL_ViewService_Test {

	@testSetup  static void setup() {
        
        User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;
        

        BI_PL_TestDataFactory.createCustomSettings();
        BI_PL_TestDataFactory.createCountrySetting();
        BI_PL_TestDataFactory.usersCreation(userCountryCode);
        BI_PL_TestDataFactory.createTestAccounts(4, userCountryCode);
        BI_PL_TestDataFactory.createTestProduct(4, userCountryCode);

        BI_PL_TestDataFactory.createCycleStructure(userCountryCode);

        User testUser = BI_PL_TestDataFactory.adminUserCreation(userCountryCode);
        List<Account> listAcc = [SELECT Id, External_ID_vod__c FROM Account];

            
        List<Product_vod__c> listProd = [SELECT Id, External_ID_vod__c FROM Product_vod__c];
        System.debug('prods*+*'+listProd);

        BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
        List<BI_PL_Position_Cycle__c> posCycles = [SELECT Id,BI_PL_External_Id__c FROM BI_PL_Position_Cycle__c];
        BI_PL_TestDataFactory.createPreparations(userCountryCode, posCycles, listAcc, listProd);

        BI_PL_TestDataFactory.createPlanitViews(userCountryCode);
        //BI_PL_TestDataFactory.createTestProductMetrics(listProd[0], userCountryCode,listAcc[0]);

    }
	
	@isTest static void testGetTargetViews() {

		//Cover constructor
		BI_PL_ViewService sv = new BI_PL_ViewService();


		//unimplemented method
		List<BI_PL_View__c> nullViews =  BI_PL_ViewService.getTargetViews();
		System.assertEquals(null, nullViews);

		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;


		Map<String, BI_PL_View__c> views =  BI_PL_ViewService.getTargetsViews('TestFieldForce', userCountryCode);
		//Shoudl be 4 keys with same view (fallback to defaults)
		System.assertEquals(4, views.size());

		views =  BI_PL_ViewService.getTargetsViews(null, userCountryCode);
	}
	
	@isTest static void testGetDetailViews() {
		User userCountry = [SELECT Id, Country_Code_BI__c, UserName from User where Id =: UserInfo.getUserId()];
        String userCountryCode = userCountry.Country_Code_BI__c;

        //Without Field force
		BI_PL_View__c view =  BI_PL_ViewService.getDetailsView(null, userCountryCode, 'BI_PL_Detail_view_lift');
		System.assertNotEquals(null, view);

		List<BI_PL_ViewService.PlanitColumn> columns = BI_PL_ViewService.getViewColumns(view); 
		System.assertEquals(3, columns.size());

		//With Field force
		view =  BI_PL_ViewService.getDetailsView('NOTEXISTITNFIELDFORCE', userCountryCode, 'BI_PL_Detail_view_lift');
		System.assertEquals(null, view);
	}

	
}