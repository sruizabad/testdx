/**
 *	20/11/2017
 *	- GLOS-539 : Visibility changes
 *	09/10/2017
 *	- GLOS-424 : Set the Position Cycle (Preparation) Owner at Position Cycle User level
 *	14/08/2017
 *	- Summary Review page requirements.
 *	11/08/2017
 *	- Get tree method.
 *	- Retrieve position fields (for GLOS-370: hierarchy page)
 */
public without sharing class BI_PL_VisibilityTree {

	public static String CYCLE_STATUS_ACTIVE = 'active';
	public static String CYCLE_STATUS_CHILD = 'child';
	public static String CYCLE_STATUS_HIDDEN = 'hidden';

	private static String PERMISSION_SET_NAME_ADMIN = 'BI_PL_ADMIN';
	private static String PERMISSION_SET_NAME_SALES = 'BI_PL_SALES';
	private static String PERMISSION_SET_NAME_SALES_MANAGER = 'BI_PL_SALES_MANAGER';

	private static Set<String> PERMISSION_SET_NAMES = new Set<String>{PERMISSION_SET_NAME_ADMIN, PERMISSION_SET_NAME_SALES, PERMISSION_SET_NAME_SALES_MANAGER};

	private static Map<Id, String> permissionSetNameByPermissionSetId = new Map<Id, String>();
	private Map<String, BI_PL_PositionCycleWrapper> tree;

	public Id defaultCountryOwnerId {get; set;}

	public BI_PL_VisibilityTree(Id cycle, String hierarchy) {
		permissionSetNameByPermissionSetId = getTypeOfUserByPermissionSetId();
		tree = generate(cycle, hierarchy);
		setDefaultOwnerId();
	}

	private Map<Id, String> getTypeOfUserByPermissionSetId() {
		Map<Id, String> output = new Map<Id, String>();
		for (PermissionSet p : [SELECT Id, Name FROM PermissionSet WHERE Name IN :PERMISSION_SET_NAMES]) {
			output.put(p.Id, p.Name);
		}

		return output;
	}

	private void setDefaultOwnerId() {
		String userCountryCode = [SELECT Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId()].Country_Code_BI__c;
		for (BI_PL_Country_Settings__c s : BI_PL_Country_Settings__c.getall().values()) {
			if (s.BI_PL_Country_code__c == userCountryCode) {
				if (String.isBlank(BI_PL_Country_Settings__c.getInstance(s.Name).BI_PL_Default_owner_user_name__c))
					throw new BI_PL_Exception(Label.BI_PL_Default_owner_null + ': ' + userCountryCode);

				String userName = BI_PL_Country_Settings__c.getInstance(s.Name).BI_PL_Default_owner_user_name__c;
				try {
					defaultCountryOwnerId = [SELECT Id FROM User WHERE Username = :userName].Id;
				} catch (exception e) {
					throw new BI_PL_Exception('Default user not found. Check the default user for the country \'' + userCountryCode + '\'');
				}
				return;
			}
		}
	}

	public Map<String, BI_PL_PositionCycleWrapper> getTree() {
		return tree;
	}

	/**
	 *	Generates the tree map related to a cycle and hierarchy.
	 *	@author Omega CRM
	 *	@return Map where key = positionId.
	 */
	private static Map<String, BI_PL_PositionCycleWrapper> generate(Id cycle, String hierarchy) {

		BI_PL_Cycle__c cycleRecord = [SELECT Id, Name, BI_PL_Type__c FROM BI_PL_Cycle__c WHERE Id = : cycle];

		Map<String, BI_PL_PositionCycleWrapper> output = new Map<String, BI_PL_PositionCycleWrapper>();

		for (BI_PL_Position_cycle__c pc : [SELECT Id, BI_PL_Position__c, BI_PL_Position__r.Name, BI_PL_Parent_position__c, BI_PL_Parent_position__r.Name, BI_PL_Confirmed__c, BI_PL_Rejected_reason__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = : cycle AND BI_PL_Hierarchy__c = : hierarchy]) {
			Id positionId = pc.BI_PL_Position__c;

			if (String.isBlank(positionId))
				throw new BI_PL_Exception(Label.BI_PL_Position_for_position_cycle_null + pc.Id);

			if (!output.containsKey(pc.BI_PL_Position__c)) {
				BI_PL_PositionWrapper position = new BI_PL_PositionWrapper(pc.BI_PL_Position__r, positionId);
				BI_PL_PositionWrapper parentPosition = new BI_PL_PositionWrapper(pc.BI_PL_Parent_position__r, pc.BI_PL_Parent_position__c);

				//Check if the parent node was added
				if (!output.containsKey(pc.BI_PL_Parent_position__c)) {
					output.put(positionId, new BI_PL_PositionCycleWrapper(pc.Id, position, parentPosition, null, pc.BI_PL_Confirmed__c, pc.BI_PL_Rejected_reason__c, 0));
				} else {
					BI_PL_PositionCycleWrapper parentNode = output.get(pc.BI_PL_Parent_position__c);
					output.put(positionId, new BI_PL_PositionCycleWrapper(pc.Id, position, parentPosition, parentNode, pc.BI_PL_Confirmed__c, pc.BI_PL_Rejected_reason__c, parentNode.level));
				}

			}
		}
		List<BI_PL_PositionCycleWrapper> rootNodes = new List<BI_PL_PositionCycleWrapper>();
		// Parents
		for (String positionId : output.keySet()) {
			BI_PL_PositionCycleWrapper current = output.get(positionId);
			Id parent = current.parentPosition.recordId;
			String parentName = current.parentPosition.record.Name;
			if (parent != null) {

				if (!output.containsKey(parent))
					throw new BI_PL_Exception(Label.BI_PL_Parent_position_for_cycle_null + ' \'' + cycleRecord.Name + '\', \'' + hierarchy + '\' (' + parentName + ')');

				current.parent = output.get(parent);
				
				output.get(parent).addChild(current, output.get(parent).level + 1);
			}
			else
			{
				rootNodes.add(current);
			}
		}

		//Set properly the level property
		//Find root nodes
		for(BI_PL_PositionCycleWrapper rootNode : rootNodes)
		{
			output = setLevelPosition(output, rootNode);
		}

		// Visibility
		// The PCU with BI_PL_Preparation_owner__c = true will be the owner. If there are more than one, the one with the oldest BI_PL_User__c will be the owner.
		// The BI_PL_Position_cycle_user__c records with inactive users (isActive = false) are ignored.

		Set<Id> usersId = new Set<Id>();
		Map<Id, BI_PL_Position_cycle_user__c> positionCycleUsers = new Map<Id, BI_PL_Position_cycle_user__c>([SELECT BI_PL_Position_Cycle__r.BI_PL_Position__c,
		        BI_PL_User__c,
		        BI_PL_User__r.Name,
		        BI_PL_Position_cycle__r.BI_PL_Hierarchy__c,
		        BI_PL_Position_cycle__r.BI_PL_Parent_position__c,
		        BI_PL_Position_cycle__r.BI_PL_Position__r.Name,
		        BI_PL_Preparation_owner__c
		        FROM BI_PL_Position_cycle_user__c
		        WHERE BI_PL_Position_Cycle__r.BI_PL_Cycle__c = : cycle
		                AND BI_PL_Position_cycle__r.BI_PL_Hierarchy__c = : hierarchy
		                        AND BI_PL_User__r.isActive = true
		                                ORDER BY BI_PL_Preparation_owner__c DESC, BI_PL_User__c]);

		for (BI_PL_Position_cycle_user__c u : positionCycleUsers.values()) {
			usersId.add(u.BI_PL_User__c);
		}

		Map<Id, String> permissionSetIdByUserId = new Map<Id, String>();

		for (PermissionSetAssignment psa : [SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId IN :usersId]) {
			if(permissionSetNameByPermissionSetId.keySet().contains(psa.PermissionSetId))
				permissionSetIdByUserId.put(psa.AssigneeId, psa.PermissionSetId);
		}
		System.debug('permissionSetIdByUserId '+permissionSetIdByUserId);
		for (BI_PL_Position_cycle_user__c u : positionCycleUsers.values()) {
			Id positionId = u.BI_PL_Position_Cycle__r.BI_PL_Position__c;
			if (doesUserMatchConditionsToBeAdded(u.BI_PL_User__r, permissionSetIdByUserId.get(u.BI_PL_User__c), cycleRecord))
			{
				System.debug('doesUserMatchConditionsToBeAdded'+ u);
				output.get(positionId).addPositionCycleUser(u, true);
			}
		}

		return output;
	}

	private static Map<String, BI_PL_PositionCycleWrapper> setLevelPosition(Map<String, BI_PL_PositionCycleWrapper> output, BI_PL_PositionCycleWrapper node)
	{
		Set<BI_PL_PositionCycleWrapper> children = node.children;
		//Integer i = 0;

		for(BI_PL_PositionCycleWrapper child: children)
		{
			Id parent = child.parentPosition.recordId;
			//child.setLevel(output.get(parent).level + 1);
			//output.get(parent).getChildren().get(i).level = output.get(parent).level + 1;
			output.get(parent).addChild(child, output.get(parent).level + 1);
			//i++;
			setLevelPosition(output,child);

		}
		return output;

	}

	/**
	 *	Checks if the user's permission set allows the user to see the preparation depending on cycle data.
	 *	@author OMEGA CRM
	 */
	private static Boolean doesUserMatchConditionsToBeAdded(User u, Id permissionSetId, BI_PL_Cycle__c cycleRecord) {
		if (permissionSetNameByPermissionSetId.containsKey(permissionSetId)) {
			Boolean isDataSteward = permissionSetNameByPermissionSetId.get(permissionSetId) == PERMISSION_SET_NAME_ADMIN;
			Boolean isSalesManager = permissionSetNameByPermissionSetId.get(permissionSetId) == PERMISSION_SET_NAME_SALES_MANAGER;
			Boolean isRep = permissionSetNameByPermissionSetId.get(permissionSetId) == PERMISSION_SET_NAME_SALES;

			if (isDataSteward) {
				return true;
			} else if (isRep || isSalesManager) {
				if (String.isBlank(cycleRecord.BI_Pl_Type__c) || cycleRecord.BI_PL_Type__c == CYCLE_STATUS_ACTIVE){
					return true;
				}else{
					return false;
				}
			}
		}
		return false;
	}

	/**
	 *	Returns the tree node that belongs to the specified position id.
	 *	@author Omega CRM
	 *	@return Tree node.
	 */
	public BI_PL_PositionCycleWrapper getNode(string positionId) {
		return tree.get(positionId);
	}

	/**
	 *	Returns the tree node that belongs to the specified position id.
	 *	@author Omega CRM
	 *	@return Tree node.
	 */
	public Set<String> getAllPositionIds() {
		return tree.keySet();
	}

	/**
	 *	Returns the tree node that belongs to the specified position id.
	 *	@author Omega CRM
	 *	@return Tree node.
	 */
	public String  getAllPositionIds(String positionId) {
		BI_PL_PositionCycleWrapper positionNode = getNode(positionId);

		if(positionNode != null){
			return positionNode.getChildrenPositionSetForQuery();
		} else {
			throw new BI_PL_Exception('Visibility tree : Node not found in hierarchy : \'' + positionId + '\'');
		}

	}




	/**
	 *	Returns the tree node that belongs to the specified position id.
	 *	@author Omega CRM
	 *	@return Tree node.
	 */
	public BI_PL_PositionCycleWrapper getRootNode() {
		BI_PL_PositionCycleWrapper root = null;
		for(BI_PL_PositionCycleWrapper pcw : tree.values()) {
			if(pcw.level == 0) {
				root = pcw;
				break;
			}
		}

		return root;
	}

	/**
	 *	Returns the Id of the rep user for the specified "position id" node.
	 *	If there are multiple users asigned to the node set as the owner returns the first found (ordered by id). If there aren't, returns the default owner set for the country through the custom setting.
	 *	@author Omega CRM
	 *	@return String id of the rep user.
	 */
	public String getPositionRep(String positionId) {
		if (tree.containsKey(positionId) && getNode(positionId).hasExclusiveNodes()) {
			return getNode(positionId).getOwnerId();
		} else {
			return defaultCountryOwnerId;
		}
	}
}