@isTest
private class BI_PL_VisibilityTree_Test {

	private static String userCountryCode;
	private static String hierarchy = 'hierarchyTest';

	@isTest static void test_method_one() {

		BI_PL_TestDataFactory.createCycleStructure('US');
		upsert new Knipper_Settings__c(
			Account_Detect_Change_FieldList__c='FirstName,Middle_vod__c',
			Account_Detect_Changes_Record_Type_List__c='Professional_vod',
			External_ID__c = UserInfo.getProfileId(),
			SetupOwnerId = Userinfo.getOrganizationId()
    	) External_ID__c;
		User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		userCountryCode = currentUser.Country_Code_BI__c;

		BI_PL_TestDataFactory.createTestAccounts(6,userCountryCode);
		BI_PL_TestDataFactory.createTestProduct(2, userCountryCode);

		BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'test1',BI_TM_Country_code__c = userCountryCode);

		insert fieldForce;

		BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Start_date__c = Date.today(), BI_PL_End_date__c = Date.today() + 1, BI_PL_Field_force__c = fieldForce.Id);
		insert cycle;

		BI_PL_Cycle__c cycle2 = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];

		String cycleId = cycle2.Id;
		//System.debug('*****///***** Pos ' + [SELECT Id, BI_PL_Position__c, BI_PL_Position__r.Name, BI_PL_Parent_position__c, BI_PL_Parent_position__r.Name, BI_PL_Confirmed__c,BI_PL_Rejected_reason__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = : cycle]);
		//System.debug('cycleId es ' + cycleId);

		Map<String, BI_PL_PositionCycleWrapper> tree = new BI_PL_VisibilityTree(cycleId, hierarchy).getTree();

	}

	@isTest static void test_method_two() {
		userCountryCode = 'BR';
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);
		BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
		upsert new Knipper_Settings__c(
				Account_Detect_Change_FieldList__c='FirstName,Middle_vod__c',
				Account_Detect_Changes_Record_Type_List__c='Professional_vod',
				External_ID__c = UserInfo.getProfileId(),
				SetupOwnerId = Userinfo.getOrganizationId()
	    	) External_ID__c;
		User testUser = BI_PL_TestDataFactory.userCreationWithPerm(userCountryCode, 'BI_PL_ADMIN');
		
		System.runAs(testUser)
		{
			
			BI_PL_TestDataFactory.createCountrySetting(userCountryCode);
			
			
			BI_PL_TestDataFactory.createTestAccounts(6,userCountryCode);
			BI_PL_TestDataFactory.createTestProduct(2, userCountryCode);


			String cycleId = cycle.Id;


			Map<String, BI_PL_PositionCycleWrapper> tree = new BI_PL_VisibilityTree(cycleId, hierarchy).getTree();

		}
		

	}

	/*
	 *	Trigger exception in setDefaultOwnerId
	 */
	@isTest static void test_method_three() {
		userCountryCode = 'BR';
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);
		upsert new Knipper_Settings__c(
				Account_Detect_Change_FieldList__c='FirstName,Middle_vod__c',
				Account_Detect_Changes_Record_Type_List__c='Professional_vod',
				External_ID__c = UserInfo.getProfileId(),
				SetupOwnerId = Userinfo.getOrganizationId()
	    	) External_ID__c;
		User testUser = BI_PL_TestDataFactory.userCreationWithPerm(userCountryCode, 'BI_PL_ADMIN');
		
		System.runAs(testUser)
		{
			
			insert new BI_PL_Country_Settings__c(Name = userCountryCode + testUser.Username, BI_PL_Country_Code__c = userCountryCode, BI_PL_Specialty_field__c = 'Specialty_1_vod__c');

			
			
			BI_PL_TestDataFactory.createTestAccounts(6,userCountryCode);
			BI_PL_TestDataFactory.createTestProduct(2, userCountryCode);

			BI_TM_FF_type__c fieldForce = new BI_TM_FF_type__c(Name = 'test1',BI_TM_Country_code__c = userCountryCode);

			insert fieldForce;

			BI_PL_Cycle__c cycle = new BI_PL_Cycle__c(BI_PL_Country_code__c = userCountryCode, BI_PL_Start_date__c = Date.today(), BI_PL_End_date__c = Date.today() + 1, BI_PL_Field_force__c = fieldForce.Id);
			insert cycle;

			BI_PL_Cycle__c cycle2 = [SELECT Id FROM BI_PL_Cycle__c LIMIT 1];

			String cycleId = cycle2.Id;
			//System.debug('*****///***** Pos ' + [SELECT Id, BI_PL_Position__c, BI_PL_Position__r.Name, BI_PL_Parent_position__c, BI_PL_Parent_position__r.Name, BI_PL_Confirmed__c,BI_PL_Rejected_reason__c FROM BI_PL_Position_cycle__c WHERE BI_PL_Cycle__c = : cycle]);
			//System.debug('cycleId es ' + cycleId);
			try{
				Map<String, BI_PL_PositionCycleWrapper> tree = new BI_PL_VisibilityTree(cycleId, hierarchy).getTree();
			}
			catch(Exception e)
			{
				System.debug(e);
			}
			

		}
		

	}

	/*
	 *	Trigger exception in Default user not found
	 */
	@isTest static void test_method_four() {
		userCountryCode = 'BR';
		BI_PL_TestDataFactory.createCycleStructure(userCountryCode);
		BI_PL_Cycle__c cycle = [SELECT id, BI_PL_Start_date__c, BI_PL_End_date__c FROM BI_PL_Cycle__c LIMIT 1];
		upsert new Knipper_Settings__c(
				Account_Detect_Change_FieldList__c='FirstName,Middle_vod__c',
				Account_Detect_Changes_Record_Type_List__c='Professional_vod',
				External_ID__c = UserInfo.getProfileId(),
				SetupOwnerId = Userinfo.getOrganizationId()
	    	) External_ID__c;
		User testUser = BI_PL_TestDataFactory.userCreationWithPerm(userCountryCode, 'BI_PL_ADMIN');
		
		System.runAs(testUser)
		{
			
			insert new BI_PL_Country_Settings__c(Name = userCountryCode + testUser.Username, BI_PL_Country_Code__c = userCountryCode, BI_PL_Default_owner_user_name__c = 'UndefinedUser123123123', BI_PL_Specialty_field__c = 'Specialty_1_vod__c');
			
			
			BI_PL_TestDataFactory.createTestAccounts(6,userCountryCode);
			BI_PL_TestDataFactory.createTestProduct(2, userCountryCode);

			
			String cycleId = cycle.Id;
	
			try{
				Map<String, BI_PL_PositionCycleWrapper> tree = new BI_PL_VisibilityTree(cycleId, hierarchy).getTree();
			}
			catch(Exception e)
			{
				System.debug(e);
			}
			

		}
		

	}


	
}