public with sharing class BI_PL_WrapObjectCSVPreSync {
	
	public BI_PL_WrapObjectCSVPreSync() {
		
	}
        public String SAP_External_ID2 {get; set;}
        public String Start_Date {get; set;}
        public String End_Date {get; set;}
        public String Territory {get; set;}
        public String Status {get; set;}
        public String Country_Code_BI {get; set;}

        public String Target_External_ID2 {get; set;}
        public String Cycle_Plan_Account {get; set;}
        public String Planned_Calls {get; set;}

        public String Detail_External_ID {get; set;}
        public String Product {get; set;}
        public String Planned_Details {get; set;}
        public String PM_Segmentation_BI {get; set;}
        public String PM_Potential_BI {get; set;}
        public String PM_Intimacy_BI {get; set;}
        public String Primary_Goal_BI {get; set;}
        public String Other_Goal_BI {get; set;}
        public String PM_Strategic_Segment_BI {get; set;}
    

    //public static List<detailsEx> wrapdetails {get; set;}
}