global class BI_SAP_Batch_Reminder_End_Cycle implements Database.Batchable<sObject>, Schedulable{

    Date triggerDate = System.today()+4;
	Date todayDate = System.today();
    String template_name = 'SAP Reminder';
    set<String> countryCodes = getCountryCycles(triggerDate);   

    global Database.Querylocator start(Database.BatchableContext BC )
    {    
		System.debug('@@@ countryCodes->' + countryCodes);    
		//End_Date_BI__c > :todayDate AND
		//List<SAP_Preparation_BI__c> lista = [SELECT Id, Status_BI__c, Territory_BI__c, Country_Code_BI__c, OwnerId FROM SAP_Preparation_BI__c WHERE Status_BI__c = 'Under Review' AND  Country_Code_BI__c IN :countryCodes];
		return Database.getQueryLocator([SELECT Id, Status_BI__c, Territory_BI__c, Country_Code_BI__c, OwnerId FROM SAP_Preparation_BI__c WHERE Status_BI__c = 'Under Review' AND Country_Code_BI__c ='BR']);        
    }
    
    global void execute(Database.BatchableContext BC, List<SAP_Preparation_BI__c> dataList )
    {     
        System.debug('@@@ dataList->' + dataList);      

        if(dataList!=null && dataList.size()>0){
            //generate notifications
            Map<String, List<String>> dsm_emails =  getDSMEmails();
                        
            System.debug('@@@ dsm_emails->' + dsm_emails);

            for (SAP_Preparation_BI__c preparation : dataList){
                BI_SAP_EmailUtils.sendEmailWithTemplate(template_name, dsm_emails.get(preparation.Country_Code_BI__c));             
            }   

        }        
        
    }                
    
    global void finish(Database.BatchableContext BC){}
      
    global void execute(SchedulableContext sc){
        BI_SAP_Batch_Reminder_End_Cycle batch = new BI_SAP_Batch_Reminder_End_Cycle();
        Database.executeBatch(batch, 20);
    }    

    global set<String> getCountryCycles(Date triggerDate){
        System.debug('@@@ triggerDate->' + triggerDate);
		set<String> result = new set<String>();
        for (SAP_Cycle_BI__c cycle : [SELECT Country_Code_BI__c
                                        FROM SAP_Cycle_BI__c 
                                        WHERE Cycle_1_End_Date_BI__c = :triggerDate
                                            OR Cycle_2_End_Date_BI__c = :triggerDate
                                            OR Cycle_3_End_Date_BI__c = :triggerDate
                                            OR Cycle_4_End_Date_BI__c = :triggerDate
                                            OR Cycle_5_End_Date_BI__c = :triggerDate
                                            OR Cycle_6_End_Date_BI__c = :triggerDate
                                            OR Cycle_7_End_Date_BI__c = :triggerDate
                                            OR Cycle_8_End_Date_BI__c = :triggerDate
                                            OR Cycle_9_End_Date_BI__c = :triggerDate
                                            OR Cycle_10_End_Date_BI__c = :triggerDate
                                            OR Cycle_11_End_Date_BI__c = :triggerDate
                                            OR Cycle_12_End_Date_BI__c = :triggerDate]){

            result.add(cycle.Country_Code_BI__c);
        }
        return result;  
    }


    global Map<String, List<String>> getDSMEmails(){
        Map<String, List<String>> result = new Map<String, List<String>>();
        for(User usr : [Select Id, name, Email, profileId, ContactId, Country_Code_BI__c from User 
                        Where  Country_Code_BI__c in :countryCodes
                        And profileId in 
                        (SELECT Id FROM Profile where Name like '%DATA_STEWARD%' )]){   
            
            List<String> emails = result.get(usr.Country_Code_BI__c);
            if(emails == null) emails = new List<String>();

            emails.add(usr.Email);
            result.put(usr.Country_Code_BI__c, emails);
        }
		System.debug('@@@ triggerDate->' + triggerDate);
        return result;
    }

}