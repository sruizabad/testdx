/**
 * Controller for BI_SAP_BusinessRules page.
 *
 * Date: 22/11/2016
 * @author Omega CRM Consulting
 * @version 1.0
*/
public with sharing class BI_SAP_BusinessRulesCtrl {
	
	/*-----------------------------------*/
	/*         PUBLIC VARIABLES          */
	/*-----------------------------------*/
	
	public String user_country_code {get; set;}												//Retrieved country code from current user.
	public list<String> lSegmentNames {get; set;}											//list of segment names values from picklist.
	public map<String,map<String,map<String,BI_SAP_Business_rule__c>>> mSegmentationBR {get; set;}		//map of maps to store all segmentation rules.
	public map<String,list<String>> mSpecialtyBR {get; set;}								//map of list to store all specialty rules.
	public Boolean hasMin {get; set;}														//boolean to indicate if any segmentation rule has defined a min value.
	public Boolean hasMax {get; set;}														//boolean to indicate if any segmentation rule has defined a min value.
	public Boolean hasSegmentationBR{get; set;}
	public Boolean hasSpecialtyBR{get; set;}
	
	/*-----------------------------------*/
	/*            CONSTRUCTOR            */
	/*-----------------------------------*/
	
    public BI_SAP_BusinessRulesCtrl(){
    	setUserCountryCode();
    	setBusinessRules();
    }
    
    
    /*-----------------------------------*/
	/*          PRIVATE METHODS          */
	/*-----------------------------------*/
	
    /**
	 * Method to get the user country code.
	 *
	 * @return void
	*/
    private void setUserCountryCode(){
    	//Retrieve the current user record.
    	User currentUser = [SELECT Id, Name, Country_Code_BI__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
    	//Storing user country code value.
    	user_country_code = currentUser.Country_Code_BI__c;
    	system.debug('###user CC: ' + user_country_code);
    }
    
    /**
	 * Main method to set all business rules.
	 *
	 * @return void
	*/
    
    private void setBusinessRules(){
    	//Calling all setters methods. 
    	setSegmentationBR();
    	setSpecialtyBR();
    }
    
    /**
	 * This method extract all values from Segment picklist.
	 *
	 * @return void
	*/
    
    private void setSegmentNames(){
    	//Initialization variables
    	lSegmentNames = new list<String>();
    	
    	//Retrieving picklist values.
    	list<Schema.PicklistEntry> picklistEntries = SAP_Detail_Preparation_BI__c.Segment_BI__c.getDescribe().getPicklistValues();
    	//Storing values.
    	for(Schema.PicklistEntry pE: picklistEntries){
    		lSegmentNames.add(pE.getValue());
    	}
    	system.debug('#### LIST SEGMENT NAMES: ' + lSegmentNames);
    }
    

	/*private set<String> getUserTerritory() {
		set<String> set_territory = new set<String>();
		try{	        
			
			//Get current user territories
			List<UserTerritory> user_territory_list = [SELECT Id, UserId, TerritoryId FROM UserTerritory WHERE UserId = :Userinfo.getUserId()];
			set<string> territory_ids = new set<String>();

			for (UserTerritory UT : user_territory_list)
				territory_ids.add(UT.TerritoryId);

			List<Territory> territoryList = [SELECT Id, Name FROM Territory WHERE Id in :territory_ids];

			//get terrs of the user
			for (Territory T : territoryList) {
				set_territory.add(T.Name);
			}

		}
		catch (Exception  e){
			system.debug('Exception: ' + e);			
		}
		return set_territory;
	}*/


    
    /**
	 * Method to retrieve, process and set all segmentation rules by country code.
	 *
	 * @return void
	*/
    private void setSegmentationBR(){
    	//Initialization variables.
    	hasMin = false;
    	hasMax = false;
    	mSegmentationBR = new map<String,map<String,map<String,BI_SAP_Business_rule__c>>>();
    	
    	//Retrieving all segment names values.
    	setSegmentNames();
    	//set<String> set_territory = getUserTerritory() ;
    	//Retrieving all segmentation business rules using a general method getBusinessRulesByType.
    	map<String,list<BI_SAP_Business_rule__c>> mSegmentationRulesByProduct = getBusinessRulesByType(user_country_code,
    																									'Segmentation',
    																									'BI_SAP_Product__r.Name');


		map<String,map<String,list<BI_SAP_Business_rule__c>>> mSegmentationRules = getSegmentationBusinessRules(user_country_code);

    	hasSegmentationBR = (mSegmentationRules.size()>0);
		system.debug('#### has Segmentation BR: ' + hasSegmentationBR);
    	//processing rules and making structures.

		for(String ffName: mSegmentationRules.keySet()){
			system.debug('#### ffName: ' + mSegmentationRules.get(ffName));
			map<String,map<String,BI_SAP_Business_rule__c>> mapAux = new map<String,map<String,BI_SAP_Business_rule__c>>();
			for(String prodName: mSegmentationRules.get(ffName).keySet()){
				Boolean contains = mapAux.containsKey(prodName);
				map<String,BI_SAP_Business_rule__c> mBRbyCriteria = (contains) ? mapAux.get(prodName) : new map<String,BI_SAP_Business_rule__c>();
				for(String segmentName: lSegmentNames){
					mBRbyCriteria.put(segmentName,new BI_SAP_Business_rule__c());
				} 
				for(BI_SAP_Business_rule__c BR:  mSegmentationRules.get(ffName).get(prodName)){
					hasMin = (!hasMin)? BR.BI_SAP_Min_value__c != null : hasMin;
					hasMax = (!hasMax)? BR.BI_SAP_Max_value__c != null : hasMax;
					mBRbyCriteria.put(BR.BI_SAP_Criteria__c,BR);
				}
				if(!contains) mapAux.put(prodName,mBRbyCriteria);
			}
			mSegmentationBR.put(ffName,mapAux);
		}
    }
    
    /**
	 * Method to retrieve, process and set all specialty rules by country code.
	 *
	 * @return void
	*/
	private void setSpecialtyBR(){
		mSpecialtyBR = new map<String,list<String>>();
		map<String,list<BI_SAP_Business_rule__c>> mSpecialtyRulesByProduct = getBusinessRulesByType(user_country_code,
																									'Customer Speciality',
																									'BI_SAP_Product__r.Name');	
		hasSpecialtyBR = (mSpecialtyRulesByProduct.size()>0);
		system.debug('#### has Specialty BR: ' + hasSpecialtyBR);
		system.debug('#### BR retrieved: ' + mSpecialtyRulesByProduct);														
		for(String prodName: mSpecialtyRulesByProduct.keySet()){
			set<String> sBRbyProduct = new set<String>();
			for(BI_SAP_Business_rule__c BR: mSpecialtyRulesByProduct.get(prodName)){
				if(BR.BI_SAP_Specialty__c!= null) sBRbyProduct.add(BR.BI_SAP_Specialty__r.Name);
			}
			mSpecialtyBR.put(prodName,new list<String>(sBRbyProduct));
		}
		system.debug('#### MAP SPECIALITIES RULES: ' + mSpecialtyBR);
	}

	/**
	* This method returns all business rules by any specified type and indexed by a field specified with a String.
	*/
	private map<String,list<BI_SAP_Business_rule__c>> getBusinessRulesByType(String countryCode, String ruleType, String indexBy){
		list<BI_SAP_Business_rule__c> lBusinessRules = new list<BI_SAP_Business_rule__c>();
		try{

			lBusinessRules = [SELECT Id, BI_SAP_Max_value__c, 
							BI_SAP_Field_Force__c,
							BI_SAP_Min_value__c, 
							BI_SAP_Type__c, 
							BI_SAP_Product__c, 
							BI_SAP_Product__r.Name,
							BI_SAP_Specialty__c,
							BI_SAP_Specialty__r.Name,
							BI_SAP_Criteria__c
							FROM BI_SAP_Business_rule__c 
							WHERE BI_SAP_Active__c = true 
							AND BI_SAP_Country_code__c =: countryCode
							AND BI_SAP_Type__c = :ruleType];			
			
		}catch(Exception e){
			system.debug('Error while retrieving business rules: ' + e.getMessage());
		}
	   
		map<String,list<BI_SAP_Business_rule__c>> rulesMap = new map<String,list<BI_SAP_Business_rule__c>>();
		
		for (BI_SAP_Business_rule__c BR: lBusinessRules){
			String indexVal = (String) getFieldValue(BR,indexBy);
			
			list<BI_SAP_Business_rule__c> lBRDetail = new list<BI_SAP_Business_rule__c>();
			Boolean contains = rulesMap.containsKey(indexVal);
			
			if(contains) lBRDetail = rulesMap.get(indexVal);
			
			lBRDetail.add(BR);
			if(!contains) rulesMap.put(indexVal,lBRDetail);
			
		}
		System.debug('####detailsRulesMap: ' + rulesMap);
		
		return rulesMap;
	}


	private map<String,map<String,list<BI_SAP_Business_rule__c>>> getSegmentationBusinessRules(String countryCode){
		list<BI_SAP_Business_rule__c> lBusinessRules = new list<BI_SAP_Business_rule__c>();
		try{

			lBusinessRules = [SELECT Id, BI_SAP_Max_value__c, 
							BI_SAP_Field_Force__c,
							BI_SAP_Min_value__c, 
							BI_SAP_Type__c, 
							BI_SAP_Product__c, 
							BI_SAP_Product__r.Name,
							BI_SAP_Specialty__c,
							BI_SAP_Specialty__r.Name,
							BI_SAP_Criteria__c
							FROM BI_SAP_Business_rule__c 
							WHERE BI_SAP_Active__c = true 
							AND BI_SAP_Country_code__c =: countryCode
							AND BI_SAP_Type__c = 'Segmentation'];			
			
		}catch(Exception e){
			system.debug('Error while retrieving business rules: ' + e.getMessage());
		}
	   
		map<String,map<String,list<BI_SAP_Business_rule__c>>> fieldForceMap = new map<String,map<String,list<BI_SAP_Business_rule__c>>>();
		
		
		for (BI_SAP_Business_rule__c BR: lBusinessRules){
			map<String,list<BI_SAP_Business_rule__c>> rulesMap = new map<String,list<BI_SAP_Business_rule__c>>();
			list<BI_SAP_Business_rule__c> lBRDetail = new list<BI_SAP_Business_rule__c>();

			System.debug('####fieldforce: ' + BR.BI_SAP_Field_Force__c);

			if (fieldForceMap.containsKey(BR.BI_SAP_Field_Force__c)) rulesMap = fieldForceMap.get(BR.BI_SAP_Field_Force__c);
			
			System.debug('####product: ' + BR.BI_SAP_Product__r.Name);
			Boolean contains = rulesMap.containsKey(BR.BI_SAP_Product__r.Name);			
			if(contains) lBRDetail = rulesMap.get(BR.BI_SAP_Product__r.Name);			
			lBRDetail.add(BR);
			if(!contains) rulesMap.put(BR.BI_SAP_Product__r.Name,lBRDetail);
			
			fieldForceMap.put(BR.BI_SAP_Field_Force__c, rulesMap);
		}
		System.debug('####detailsRulesMap: ' + fieldForceMap);
		
		return fieldForceMap;
	}

	/**
	* This method returns the value of any object field specified with a String.
	*/
	private static Object getFieldValue(SObject o,String field){
		
		system.debug('%%% field: ' + field);
	
		if(o == null){return null;}
		if(field.contains('.')){
			String nextField = field.substringAfter('.');
			String relation = field.substringBefore('.');
			return getFieldValue((SObject)o.getSObject(relation),nextField);
		}else{
			return o.get(field);
		}	
	}	

}