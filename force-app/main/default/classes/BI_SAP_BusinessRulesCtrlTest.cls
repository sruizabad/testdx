@isTest
private class BI_SAP_BusinessRulesCtrlTest {

    @testSetup
    static void setUpData() {
        Test.startTest();
            BI_SAP_SetupClassTest.setupTestData();
        Test.stopTest();
    }
    
  	//BusinessRules main Test
    @isTest
    static void BI_SAP_BusinessRules_MainTest() {
		System.Debug('Test main for BusinessRules Controller');
		
		BI_SAP_BusinessRulesCtrl BRctrl = new BI_SAP_BusinessRulesCtrl();
		
		list<Product_vod__c> products = [SELECT Id, Name FROM Product_vod__c];
		Integer numSpecialties = [SELECT Id, Name FROM Customer_Attribute_BI__c WHERE RecordTypeId = '012K00000000WiwIAE'].size();
		
		list<BI_SAP_Business_rule__c> lBR = [SELECT Id FROM BI_SAP_Business_rule__c WHERE BI_SAP_Type__c = 'Customer Speciality'];
		list<BI_SAP_Business_rule__c> lBRSeg = [SELECT Id, BI_SAP_Field_Force__c FROM BI_SAP_Business_rule__c WHERE BI_SAP_Type__c = 'Segmentation'];
		
		
		set<String> specialties = new set<String>();
		set<String> productsNames = new set<String>();
		set<String> fieldsForce = new set<String>();
		
		for(BI_SAP_Business_rule__c BR: lBRSeg){
			if(BR.BI_SAP_Field_Force__c != null){
				fieldsForce.add(BR.BI_SAP_Field_Force__c);
			}
		}
		
		for(String fieldForce: BRctrl.mSegmentationBR.keySet()){
			productsNames.addAll(BRctrl.mSegmentationBR.get(fieldForce).keySet());
		}
		
		for(String pName: BRctrl.mSpecialtyBR.keySet()){
			specialties.addAll(BRctrl.mSpecialtyBR.get(pName));
		}
		
		//FIELD FORCE ASSERTION
		system.assertEquals(BRctrl.mSegmentationBR.keySet().size(), fieldsForce.size());
		//PRODUCTS ASSERTION
		system.assertEquals(productsNames.size(), products.size());
		//SPECIALTIES ASSERTION
		system.assertEquals(specialties.size(), numSpecialties);
		
    }
}