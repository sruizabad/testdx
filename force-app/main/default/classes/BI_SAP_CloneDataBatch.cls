global class BI_SAP_CloneDataBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global set<Id> SAPsToCloneIds = new set<Id>();
    // original SAPs that have been cloned
    global set<Id> clonedSAPsIds = new set<Id>();
    global list<SAP_Preparation_BI__c> lTotalSAPsToClone = new list<SAP_Preparation_BI__c>();
    global map<String, Id> clonedSAPsToSAPsIdsMap = new map<String, Id>();
    global BI_SAP_DataManagerCtrl.SAPCycleWrapper SAPCycleWrap = null;
    global String userCountryCode = '';

    global BI_SAP_CloneDataBatch(list<SAP_Preparation_BI__c> SAPsToClone, BI_SAP_DataManagerCtrl.SAPCycleWrapper SAPCycleWrap, String userCountryCode){
        for(SAP_Preparation_BI__c SAP: SAPsToClone){
            SAPsToCloneIds.add(SAP.Id);
        }
        system.debug('%%% SAPsToCloneIds: ' + SAPsToCloneIds);
        this.SAPCycleWrap = SAPCycleWrap;
        system.debug('%%% this.SAPCycleWrap: ' + this.SAPCycleWrap);
        this.userCountryCode = userCountryCode;
        system.debug('%%% this.userCountryCode: ' + this.userCountryCode);
        this.lTotalSAPsToClone = SAPsToClone;
    }
    
    /**
     * Start method
     */
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, Name, Country_Code_BI__c, Territory_BI__c, Status_BI__c, Start_Date_BI__c,
                                                End_Date_BI__c, OwnerId, External_ID_BI__c
                                        FROM SAP_Preparation_BI__c WHERE Id in :SAPsToCloneIds]);
    }
    
    /**
     * Batch Execute method
     */
    global void execute(Database.BatchableContext BC, list<SAP_Preparation_BI__c> lSAPsToClone){
        system.debug('%%% cloneSAPs(lSAPsToClone)');
        system.debug('%%% lSAPsToClone: ' + lSAPsToClone);
        cloneSAPs(lSAPsToClone);
    }
    
    /**
     * Batch Finish method
     */
    global void finish(Database.BatchableContext BC){
        system.debug('%%% FINISHED');
        system.debug('%%% clonedSAPsIds.size(): ' + clonedSAPsIds.size());
        system.debug('%%% SAPsToCloneIds.size(): ' + SAPsToCloneIds.size());
        if(clonedSAPsIds.size()<SAPsToCloneIds.size()){
            list<SAP_Preparation_BI__c> remainingSAPsToClone = new list<SAP_Preparation_BI__c>();
            system.debug('%%% lTotalSAPsToClone: ' + lTotalSAPsToClone);
            for(SAP_Preparation_BI__c SAP: lTotalSAPsToClone){
                system.debug('%%% SAP.Id: ' + SAP.Id);
                if(!clonedSAPsIds.contains(SAP.Id)){
                    remainingSAPsToClone.add(SAP);
                }
            }
            system.debug('%%% remainingSAPsToClone: ' + remainingSAPsToClone);
            system.debug('%%% remainingSAPsToClone.size(): ' + remainingSAPsToClone.size());
            BI_SAP_CloneDataBatch cloneDataBatch = new BI_SAP_CloneDataBatch(remainingSAPsToClone, SAPCycleWrap, userCountryCode);
            Id batchJobId = Database.executeBatch(cloneDataBatch, 10);
            system.debug('%%% batchJobId: ' + batchJobId);
        }else{
            if(clonedSAPsIds.size()>0){
                BI_SAP_EmailUtils.sendEmailWithTemplate(new list<Id>(clonedSAPsIds)[0], 'SAP Clone Notification', new list<String>{UserInfo.getUserEmail()});
            }
        }
        
    }
    
    private void cloneSAPs(list<SAP_Preparation_BI__c> lSAPsToClone){
        system.debug('%%% started: cloneSAPs');
        system.debug('%%% lSAPsToClone: ' + lSAPsToClone);
                    
        // This map will be used in order to retrieve the cloned targets Ids for the
        // detail clonation process
        map<Id, SAP_Target_Preparation_BI__c> targetIdToClonedTarget = new map<Id, SAP_Target_Preparation_BI__c>();
        
        /*
            SAPs CLONATION
        */
        
        map<Id, SAP_Preparation_BI__c> mClonedSAPs = new map<Id, SAP_Preparation_BI__c>();
        
        for(Integer i=0; i<lSAPsToClone.size(); i++){
            System.debug('%%% clone SAP: ' + lSAPsToClone[i]);
            
            SAP_Preparation_BI__c clonedSAP = new SAP_Preparation_BI__c(
                Country_Code_BI__c = lSAPsToClone[i].Country_Code_BI__c, 
                Territory_BI__c = lSAPsToClone[i].Territory_BI__c,
                //Status_BI__c = lSAPsToClone[i].Status_BI__c,
                Status_BI__c = 'Under Review',
                Start_Date_BI__c = SAPCycleWrap.startDate,
                End_Date_BI__c = SAPCycleWrap.endDate,
                OwnerId = lSAPsToClone[i].OwnerId
                
            );
            
            String startDay = (SAPCycleWrap.startDate.day()<10) ? ('0' + SAPCycleWrap.startDate.day()) : ('' + SAPCycleWrap.startDate.day());
            String startMonth = (SAPCycleWrap.startDate.month()<10) ? ('0' + SAPCycleWrap.startDate.month()) : ('' + SAPCycleWrap.startDate.month());
            
            String endDay = (SAPCycleWrap.endDate.day()<10) ? ('0' + SAPCycleWrap.endDate.day()) : ('' + SAPCycleWrap.endDate.day());
            String endMonth = (SAPCycleWrap.endDate.month()<10) ? ('0' + SAPCycleWrap.endDate.month()) : ('' + SAPCycleWrap.endDate.month());
            
            String startDate = '' + SAPCycleWrap.startDate.year() + startMonth + startDay;
            String endDate = '' + SAPCycleWrap.endDate.year() + endMonth + endDay;
            
            system.debug('%%% startDate: ' + startDate);
            system.debug('%%% endDate: ' + endDate);
            
            clonedSAP.External_Id_BI__c = lSAPsToClone[i].Country_Code_BI__c + '_' + lSAPsToClone[i].Territory_BI__c + '_' +  startDate + '_' + endDate + '_1';
            
            
            System.debug('%%% NEW SAP TO CLONE: ' + clonedSAP);
            
            mClonedSAPs.put(lSAPsToClone[i].Id, clonedSAP);
            // Add the original SAP's Id
            clonedSAPsIds.add(lSAPsToClone[i].Id);
        }
        
        System.debug('%%% SIZE: ' + mClonedSAPs.size());
        System.debug('%%% mClonedSAPs.size(): ' + mClonedSAPs.size());
        System.debug('%%% mClonedSAPs: ' + mClonedSAPs);
        
        for(SAP_Preparation_BI__c clonedSAP: mClonedSAPs.values()){
            system.debug('%%% ID1: ' + clonedSAP.Id);
        }
        
        //upsert mClonedSAPs.values();
        
        Database.UpsertResult[] results = Database.upsert(mClonedSAPs.values(), SAP_Preparation_BI__c.Fields.External_ID_BI__c, false);
        for(Database.UpsertResult result: results){
            if(!result.isSuccess()){
                system.debug('%%% SAP Preparations upsert error: ' + result.getErrors());
            }
        }
        
        
        for(SAP_Preparation_BI__c clonedSAP: mClonedSAPs.values()){
            system.debug('%%% ID2: ' + clonedSAP.Id);
        }
        
        
        /*
            TARGETS CLONATION
        */
        
        list<SAP_Target_Preparation_BI__c> lClonedTargets = new list<SAP_Target_Preparation_BI__c>();
        
        // Set contaning all SAP_Preparation_BI__c's to clone Ids
        set<Id> SAPIds = new set<Id>();
        for(SAP_Preparation_BI__c SAP: lSAPsToClone){
            SAPIds.add(SAP.Id);
        }
        system.debug('%%% SAPIds: ' + SAPIds);
        
        list<SAP_Target_Preparation_BI__c> lTargetsQueried = [SELECT Id, Target_Customer_BI__c, Planned_Interactions_BI__c, Adjusted_Interactions_BI__c, Added_Manually_BI__c,
                                                                        NTL_Value_BI__c, SAP_Header_BI__c, Country_Code_BI__c, External_ID_BI__c, Removed_Reason_BI__c,
                                                                        Reviewed_BI__c, Removed_BI__c, Rejected_BI__c, Rejected_Reason_BI__c, Edited_BI__c, Total_Planned_Details_BI__c, Target_Customer_BI__r.BI_External_ID_1__c
                                                                FROM SAP_Target_Preparation_BI__c WHERE SAP_Header_BI__c IN :SAPIds];
        
        system.debug('%%% lTargetsQueried.size(): ' + lTargetsQueried.size());
            
        // This map will contain all targets we need to clone for each SAP_Preparation_BI__c
        map<Id, list<SAP_Target_Preparation_BI__c>> targetsBySAP = new map<Id, list<SAP_Target_Preparation_BI__c>>();
        for(SAP_Target_Preparation_BI__c target: lTargetsQueried){
            list<SAP_Target_Preparation_BI__c> lTargets = targetsBySAP.get(target.SAP_Header_BI__c);
            if(lTargets==null){
                lTargets = new list<SAP_Target_Preparation_BI__c>();
                targetsBySAP.put(target.SAP_Header_BI__c, lTargets);
            }
            
            lTargets.add(target);
        }
        system.debug('%%% targetsBySAP.keySet(): ' + targetsBySAP.keySet());
        system.debug('%%% targetsBySAP: ' + targetsBySAP);
        
        for(Integer i=0; i<lSAPsToClone.size(); i++){
            // Retrieve all SAP_Target_Preparation_BI__c for the current SAP_Preparation_BI__c
            addClonedTargets(lSAPsToClone[i], targetsBySAP, lClonedTargets, mClonedSAPs, targetIdToClonedTarget);
        }
        
        for(SAP_Target_Preparation_BI__c clonedTarget: lClonedTargets){
            system.debug('%%% CLONED TARGET: ' + clonedTarget);
        }
        
        Database.upsert(lClonedTargets, SAP_Target_Preparation_BI__c.Fields.External_ID_BI__c, false);      
        for(Database.UpsertResult result: results){
            if(!result.isSuccess()){
                system.debug('%%% SAP Target Preparations upsert error: ' + result.getErrors());
            }
        }
        
        /*
            DETAILS CLONATION
        */
        
        system.debug('%%% targetIdToClonedTarget: ' + targetIdToClonedTarget);
        
        // Now we retrieve all Details for each target
        list<SAP_Detail_Preparation_BI__c> lDetails = [SELECT Planned_Details_BI__c, Adjusted_Details_BI__c, Column_BI__c, Product_BI__c, Product_BI__r.External_ID_vod__c,
                                                            Segment_BI__c, Row_BI__c, Target_Customer_BI__r.Reviewed_BI__c, Target_Customer_BI__r.Rejected_BI__c, Added_Manually_BI__c,
                                                            Target_Customer_BI__r.Removed_BI__c, Target_Customer_BI__r.Edited_BI__c, Target_Customer_BI__r.Added_Manually_BI__c
                                                        FROM SAP_Detail_Preparation_BI__c WHERE Target_Customer_BI__c IN :targetIdToClonedTarget.keySet()];
                                                        
        system.debug('%%% lDetails: ' + lDetails);
        
        list<SAP_Detail_Preparation_BI__c> lClonedDetails = new list<SAP_Detail_Preparation_BI__c>();
        for(SAP_Detail_Preparation_BI__c detail: lDetails){
            SAP_Target_Preparation_BI__c clonedTarget = targetIdToClonedTarget.get(detail.Target_Customer_BI__c);
            if(clonedTarget!=null){
                
                Boolean cloneIt = true;
                Decimal planned = detail.Planned_Details_BI__c;
                Decimal adjusted = detail.Adjusted_Details_BI__c;
                
                if(detail.Target_Customer_BI__r.Reviewed_BI__c){
                    if(detail.Target_Customer_BI__r.Removed_BI__c){
                        cloneIt = false;
                    }else if(detail.Target_Customer_BI__r.Edited_BI__c){
                        planned = adjusted;
                    }
                }else if(detail.Target_Customer_BI__r.Rejected_BI__c){
                    if(detail.Added_Manually_BI__c){
                        cloneIt = false;
                    }else{
                        adjusted = planned;
                    }
                }else if(detail.Target_Customer_BI__r.Removed_BI__c && !detail.Target_Customer_BI__r.Rejected_BI__c && !detail.Target_Customer_BI__r.Reviewed_BI__c){
                    adjusted = planned;
                }else if(detail.Added_Manually_BI__c && !detail.Target_Customer_BI__r.Rejected_BI__c && !detail.Target_Customer_BI__r.Reviewed_BI__c){
                    planned = 0;
                }
                
                if(cloneIt){
                    SAP_Detail_Preparation_BI__c clonedDetail = new SAP_Detail_Preparation_BI__c(
                        Country_Code_BI__c = userCountryCode,
                        Planned_Details_BI__c = planned,
                        Adjusted_Details_BI__c = adjusted,
                        Column_BI__c = detail.Column_BI__c,
                        Product_BI__c = detail.Product_BI__c,
                        Segment_BI__c = detail.Segment_BI__c,
                        Row_BI__c = detail.Row_BI__c,
                        Target_Customer_BI__c = clonedTarget.Id
                    );
                                        
                    clonedDetail.External_ID_BI__c = clonedTarget.External_ID_BI__c.substringBeforeLast('_') + '_' + detail.Product_BI__r.External_ID_vod__c + '_1';
                    
                    /*[Country_Code]_[Territory]_[Start_Date]_[End_Date]_[Target (External ID)]_[Product (External ID)]_1*/
                    
                    
                    lClonedDetails.add(clonedDetail);
                }
            }
        }
        
        system.debug('%%% lClonedDetails: ' + lClonedDetails);
        
        Database.upsert(lClonedDetails, SAP_Detail_Preparation_BI__c.Fields.External_ID_BI__c, false);
        for(Database.UpsertResult result: results){
            if(!result.isSuccess()){
                system.debug('%%% SAP Detail Preparations upsert error: ' + result.getErrors());
            }
        }
    }
    
    // This method returns all targets we have to clone for a specific SAP_Preparation_BI__c
    private void addClonedTargets(SAP_Preparation_BI__c SAP, map<Id, list<SAP_Target_Preparation_BI__c>> targetsBySAP,
                                                        list<SAP_Target_Preparation_BI__c> lClonedTargets, map<Id, SAP_Preparation_BI__c> mClonedSAPs,
                                                        map<Id, SAP_Target_Preparation_BI__c> targetIdToClonedTarget){
                                        
        // CHECK ERROR: if targetsToClone==null
        list<SAP_Target_Preparation_BI__c> lSAPTargets = targetsBySAP.get(SAP.Id);      
        
        for(SAP_Target_Preparation_BI__c target: lSAPTargets){
            String clonedSAPId = mClonedSAPs.get(SAP.Id).Id;
            if(clonedSAPId!=null){
                
                Boolean cloneIt = true;
                Decimal planned = target.Total_Planned_Details_BI__c;
                Decimal adjusted = target.Adjusted_Interactions_BI__c;
                Boolean reviewed = false;
                Boolean edited = false;
                Boolean rejected = false;
                Boolean removed = false;
                
                // Manage P and A values
                if(target.Reviewed_BI__c){
                    if(target.Removed_BI__c){
                        cloneIt = false;
                    }else if(target.Edited_BI__c){
                        planned = adjusted;
                    }
                }else if(target.Rejected_BI__c){
                    if(target.Added_Manually_BI__c){
                        if(target.Removed_BI__c){
                            planned = 0;
                        }else{
                            cloneIt = false;
                        }
                    }else{
                        adjusted = planned;
                    }
                }else if(target.Removed_BI__c && !target.Rejected_BI__c && !target.Reviewed_BI__c){
                    if(target.Added_Manually_BI__c){
                        cloneIt = false;
                    }else{
                        adjusted = planned;
                    }
                }else if(target.Added_Manually_BI__c && !target.Removed_BI__c && !target.Rejected_BI__c && !target.Reviewed_BI__c){
                    planned = 0;
                }
                
                // Manage statuses
                if(target.Edited_BI__c && !target.Rejected_BI__c && !target.Reviewed_BI__c && !target.Removed_BI__c){
                    edited = true;
                }else if(target.Removed_BI__c && !target.Reviewed_BI__c && target.Added_Manually_BI__c){
                    edited = true;
                    removed = true;
                }
                
                if(cloneIt){
                    SAP_Target_Preparation_BI__c clonedTarget = new SAP_Target_Preparation_BI__c(
                        Target_Customer_BI__c = target.Target_Customer_BI__c,
                        Planned_Interactions_BI__c = planned,
                        Adjusted_Interactions_BI__c = adjusted,
                        NTL_Value_BI__c = target.NTL_Value_BI__c,
                        SAP_Header_BI__c = clonedSAPId,
                        Country_Code_BI__c = target.Country_Code_BI__c,
                        Reviewed_BI__c = reviewed,
                        Edited_BI__c = edited,
                        Rejected_BI__c = rejected,
                        Rejected_Reason_BI__c = target.Rejected_Reason_BI__c,
                        Removed_BI__c = removed,
                        Removed_Reason_BI__c = target.Removed_Reason_BI__c
                    );
                    
                    String startDay = (mClonedSAPs.get(SAP.Id).Start_Date_BI__c.day()<10) ? ('0' + mClonedSAPs.get(SAP.Id).Start_Date_BI__c.day()) : ('' + mClonedSAPs.get(SAP.Id).Start_Date_BI__c.day());
                    String startMonth = (mClonedSAPs.get(SAP.Id).Start_Date_BI__c.month()<10) ? ('0' + mClonedSAPs.get(SAP.Id).Start_Date_BI__c.month()) : ('' + mClonedSAPs.get(SAP.Id).Start_Date_BI__c.month());
                    
                    String endDay = (mClonedSAPs.get(SAP.Id).End_Date_BI__c.day()<10) ? ('0' + mClonedSAPs.get(SAP.Id).End_Date_BI__c.day()) : ('' + mClonedSAPs.get(SAP.Id).End_Date_BI__c.day());
                    String endMonth = (mClonedSAPs.get(SAP.Id).End_Date_BI__c.month()<10) ? ('0' + mClonedSAPs.get(SAP.Id).End_Date_BI__c.month()) : ('' + mClonedSAPs.get(SAP.Id).End_Date_BI__c.month());
                    
                    String startDate = '' + mClonedSAPs.get(SAP.Id).Start_Date_BI__c.year() + startMonth + startDay;
                    String endDate = '' + mClonedSAPs.get(SAP.Id).End_Date_BI__c.year() + endMonth + endDay;
                    
                    //clonedTarget.External_ID_BI__c = target.Country_Code_BI__c + '_' + SAP.Territory_BI__c + '_' + startDate + '_' + endDate + '_' + target.External_ID_BI__c + '_1';
                    clonedTarget.External_ID_BI__c = target.Country_Code_BI__c + '_' + SAP.Territory_BI__c + '_' + startDate + '_' + endDate + '_' + target.Target_Customer_BI__r.BI_External_ID_1__c + '_1';

                    
                    /*
                        - SAP Target Preparation:
                        [Country_Code]_[Territory]_[Start_Date]_[End_Date]_[Target (External ID)]_1
                    */
                    
                    lClonedTargets.add(clonedTarget);
                    targetIdToClonedTarget.put(target.Id, clonedTarget);
                }
                
            }
        }
    }
}