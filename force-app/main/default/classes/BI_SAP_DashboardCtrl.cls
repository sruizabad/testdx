public with sharing class BI_SAP_DashboardCtrl {
    public String salesPositionsBySM_JSON {get; set;}
    public list<String> selectSMOptions {get; set;}
    public Boolean isSM {get; set;}
    public Boolean isDS {get; set;}
    private Integer MAX_ITERATIONS = 99;
    private Integer currentIterations = 0;
    private Id SMProfileId;
    private Id SRProfileId;
    private map<Id,set<String>> mSubRoleNameByRoleId;
    
    public BI_SAP_DashboardCtrl(){
        setProfilesIds();
        setPrivileges();
        setPositions();
    }
    
    private void setProfilesIds(){
        system.debug('### INTO setProfilesIds');
        String UCC = [SELECT Id, Country_code_bi__c FROM User WHERE id = :UserInfo.getUserId() LIMIT 1].Country_code_bi__c;
        String SMProfileName = '' + UCC + '_SALES_MANAGER'; 
        String SRProfileName = '' + UCC + '_SALES';
        system.debug('### SMProfileName: ' + SMProfileName);
        system.debug('### SRProfileName: ' + SRProfileName);
        set<String> profileNames = new set<String>{SMProfileName,SRProfileName};
        list<Profile> lProfiles = [SELECT Id, Name FROM Profile WHERE Name IN :profileNames];
        for(Profile p: lProfiles){
            if(p.Name == SMProfileName) SMProfileId = p.Id;
            if(p.Name == SRProfileName) SRProfileId = p.Id;
        }
        system.debug('SMProfileId: ' + SMProfileId);
        system.debug('SRProfileId: ' + SRProfileId);
    }
    
    private void setPrivileges(){
        String pName = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        system.debug('#### PROFILE NAME: ' + pName);
        isSM = Pattern.matches('.*SALES_MANAGER',pName);
        isDS = Pattern.matches('.*DATA_STEWARD',pName);
    }
    
    private void setPositions (){
        system.debug('#### INSIDE SET POSITIONS');
        //main structures
        map<String,set<String>> salesPositionsBySM = new map<String,set<String>>();
        mSubRoleNameByRoleId = new map<Id,set<String>>();

        //aux structures
        list<Territory> parentTerrs = new list<Territory>();
        //map<Id,set<String>> positionsByParentId = new map<Id,set<String>>();
        //list<Territory> lPositions_Sales = new list<Territory>();
        
        //Current User data
        list<UserRole> currentUserRole = [SELECT Id, Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId() limit 1];
        
        
        //Current User roles
        set<Id> parentRoleIds = new set<Id>();
        if(currentUserRole.size()>0){
            parentRoleIds.add(currentUserRole.get(0).Id);
        }
        
        //calling to recursive method in order to map all subroles.
        mappingSubterritories(parentRoleIds);
        
        if(isSM){
            set<String> subSRRoles = new set<String>();
            for(set<String> subRoleNames: mSubRoleNameByRoleId.values()){
                subSRRoles.addAll(subRoleNames);
            }
            
            if(currentUserRole.size()>0){
                salesPositionsBySM.put(currentUserRole.get(0).Name,subSRRoles);
            }
        
        }else if(isDS){
            list<UserRole> luR = [SELECT Id, Name FROM UserRole WHERE Id IN :mSubRoleNameByRoleId.keySet()];
            
            map<Id,UserRole> mSMUserRoles = new map<Id,UserRole>();
            for(UserRole uR: luR){
                mSMUserRoles.put(uR.Id,uR);
            }
            
            for(Id uRId: mSubRoleNameByRoleId.keySet()){
                String SMUserRoleName = mSMUserRoles.get(uRId).Name;
                Boolean contains = salesPositionsBySM.containsKey(SMUserRoleName);
                set<String> saleRoleNames = (contains)? salesPositionsBySM.get(SMUserRoleName) : new set<String>();
                saleRoleNames.addAll(mSubRoleNameByRoleId.get(uRId));
                if(!contains) salesPositionsBySM.put(SMUserRoleName,saleRoleNames);
            }
        }
        
        system.debug('#### salesPositionsBySM: ' + salesPositionsBySM);
        
        //HARD CODED
        //To test with a SM user.
        /*isSM = true;
        salesPositionsBySM.put('SMPositionTest',new set<String>{'sprep','SFF-1990'});*/
        
        //To test with a DS user.
        /*isDS = true;
        salesPositionsBySM.put('SMPosTest1',new set<String>{'sprep','SFF-1990'});
        salesPositionsBySM.put('SMPosTest2',new set<String>{'SFF-1990'});*/
        
        selectSMOptions = new list<String>(salesPositionsBySM.keySet());
        
        system.debug('#### selectSMOptions: ' + selectSMOptions);
        
        salesPositionsBySM_JSON = JSON.serialize(salesPositionsBySM);
    }
    
    private set<UserRole> getSubRoles(set<Id> rolesIds){
        system.debug('### INTO getSubTerritories');
        
        list<UserRole> lSubRoles = [SELECT Id, Name, ParentRoleId, (SELECT Id, ProfileId FROM Users WHERE ProfileId = :SRProfileId) FROM UserRole WHERE ParentRoleId IN :rolesIds];
        
        system.debug('### lSubRoles: ' + lSubRoles);
        return new set<UserRole>(lSubRoles);
    }
    
    private set<Id> findSRUsersByRole(set<UserRole> lSubRoles){
        system.debug('### INTO filterTerritoriesByProfileName =======');
        set<Id> NoSRRoles = new set<Id>();
        
        for(UserRole uR: lSubRoles){
            if(uR.Users.size()>0){
                Boolean contains = mSubRoleNameByRoleId.containsKey(uR.ParentRoleId);
                set<String> subRolesNames = (contains)? mSubRoleNameByRoleId.get(uR.ParentRoleId) : new set<String>();
                subRolesNames.add(uR.Name);
                if(!contains) mSubRoleNameByRoleId.put(uR.ParentRoleId,subRolesNames);
            }else{
                NoSRRoles.add(uR.Id);
            }
        }
        
        system.debug('### mSubRoleNameByRoleId: ' + mSubRoleNameByRoleId);
        system.debug('### NoSRRoles: ' + NoSRRoles);
        
        return NoSRRoles;
    }
    
    private void mappingSubterritories(set<Id> userRolesIds){
        system.debug('## INTO MAPPING SUBTERRITORIES');
        //if there are roles ...
        if(userRolesIds.size() > 0 || Test.isRunningTest()){
            //get all roles.
            set<UserRole> SubRoles = getSubRoles(userRolesIds);
            set<Id> NoSRRoleIds = findSRUsersByRole(SubRoles);
            //if there are roles ... recursive call
            currentIterations ++;
            if(NoSRRoleIds.size() > 0 && currentIterations < MAX_ITERATIONS) mappingSubterritories(NoSRRoleIds);
        }
    }

    private static set<String> getSegmentationOptions (){
        set<String> options = new set<String>(); //new list for holding all of the picklist options
        list<Schema.PicklistEntry> pick_list_values = SAP_Detail_Preparation_BI__c.getSObjectType()
                                                     .getDescribe().fields.getMap().get('Segment_BI__c')
                                                     .getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list     
            options.add(a.getValue()); //add the value and label to our final list
        }
        return options;
    }
    
    @RemoteAction
    public static DashboardDataStructures generateTargetsAndDetailStructures(String positionsJSON){
        set<String> salesPositions = (set<String>) JSON.deserialize(positionsJSON, set<String>.class);
        Boolean hasSAPs = false;
        
        //Aux Structures initialization
        set<Id> sapPreparationIds = new set<Id>();
        list<SAP_Preparation_BI__c> lSAP_Preparation = new list<SAP_Preparation_BI__c>();
        list<SAP_Target_Preparation_BI__c> targets = new list<SAP_Target_Preparation_BI__c>();
        
        /*map<String,set<Id>> targetIdsByPosition = new map<String,set<Id>>();
        map<String,set<Id>> customerIdsByPosition = new map<String,set<Id>>();*/
        transient map<Id,set<Id>> mTargetIdsByCustomerId = new map<Id,set<Id>>();
        transient set<Id> relatedCustomerIds = new set<Id>();
        
        //Main Structures initilization
        //transient map<String,map<String, list<SAP_Target_Preparation_BI__c>>> mTargetsPositionBySpecialty = new map<String,map<String, list<SAP_Target_Preparation_BI__c>>>();
        //transient map<String,map<String, list<SAP_Detail_Preparation_BI__c>>> mDetailsPositionBySpecialty = new map<String,map<String, list<SAP_Detail_Preparation_BI__c>>>();
        //transient map<String,list<SAP_Target_Preparation_BI__c>> mRelatedTargetsBySpecialty = new map<String,list<SAP_Target_Preparation_BI__c>>();
        //transient map<String,list<SAP_Detail_Preparation_BI__c>> mRelatedDetailsBySpecialty = new map<String,list<SAP_Detail_Preparation_BI__c>>();
        //transient map<String,list<SAP_Detail_Preparation_BI__c>> mRelatedDetailsBySegment = new map<String,list<SAP_Detail_Preparation_BI__c>>();
        
        transient map<String,map<String,Integer>> mTargetsPositionBySpecialtyPlanned = new map<String,map<String,Integer>>();
        transient map<String,map<String,Integer>> mDetailsPositionBySpecialtyPlanned = new map<String,map<String,Integer>>();
        transient map<String,map<String,Integer>> mTargetsPositionBySpecialtyAdjusted = new map<String,map<String,Integer>>();
        transient map<String,map<String,Integer>> mDetailsPositionBySpecialtyAdjusted = new map<String,map<String,Integer>>();
        transient map<String,map<String,Integer>> mDetailsPositionBySegmentPlanned = new map<String,map<String,Integer>>();
        transient map<String,map<String,Integer>> mDetailsPositionBySegmentAdjusted = new map<String,map<String,Integer>>();
        
        transient map<String,Integer> mRelatedTargetsBySpecialtyPlanned = new map<String,Integer>();
        transient map<String,Integer> mRelatedTargetsBySpecialtyAdjusted = new map<String,Integer>();
        transient map<String,Integer> mRelatedDetailsBySpecialtyPlanned = new map<String,Integer>();
        transient map<String,Integer> mRelatedDetailsBySpecialtyAdjusted = new map<String,Integer>();
        
        transient map<String,Integer> mRelatedDetailsBySegmentPlanned = new map<String,Integer>();
        transient map<String,Integer> mRelatedDetailsBySegmentAdjusted = new map<String,Integer>();
        
        Date current = Date.today();
        Date firstDate = current.toStartofMonth();
        Date lastDate = current.addMonths(1).toStartofMonth().addDays(-1);
        
        //system.debug('#### SAPS: ' + [SELECT Id, Territory_BI__c FROM SAP_Preparation_BI__c]);
        //Retrieving all SAP Preparation.
        /*lSAP_Preparation = [SELECT Id, Territory_BI__c 
                            FROM SAP_Preparation_BI__c 
                            WHERE Territory_BI__c IN :salesPositions 
                            AND Start_Date_BI__c >= :firstDate 
                            AND End_Date_BI__c <= :lastDate];*/

        lSAP_Preparation = [SELECT Id, Territory_BI__c 
                            FROM SAP_Preparation_BI__c 
                            WHERE Territory_BI__c IN :salesPositions 
                            AND Start_Date_BI__c >= :current];
                            
        //Filling sets in order to make more easy the main iteration process.
        system.debug('### headers: ' + salesPositions);
        salesPositions = new set<String>();
        for(SAP_Preparation_BI__c SAP_Prep: lSAP_Preparation){
            sapPreparationIds.add(SAP_Prep.Id);
            salesPositions.add(SAP_Prep.Territory_BI__c);
        }
        hasSAPs = (lSAP_Preparation.size() > 0);
        if(hasSAPs){
            system.debug('### HAS SAPS');
            system.debug('### SAP Ids: ' + sapPreparationIds);

            set<String> segmentModes = new set<String>();
            segmentModes = getSegmentationOptions();
            system.debug('### segment modes: ' + segmentModes);
            
            //Retrieving all Targets and Details and mapping.
            /*targets = [SELECT Id,
                       Maximum_Adjusted_Interactions_BI__c,
                       Maximum_Planned_Interactions_BI__c,
                       Primary_Specialty_BI__c,
                       Target_Customer_BI__c,
                       SAP_Header_BI__c,
                       SAP_Header_BI__r.Territory_BI__c,
                            (SELECT Id,
                            Adjusted_Details_BI__c,
                            Planned_Details_BI__c,
                            Product_BI__r.Name,
                            Segment_BI__c
                            FROM SAP_Details_Preparation__r)
                      FROM SAP_Target_Preparation_BI__c
                      WHERE SAP_Header_BI__c IN :sapPreparationIds];*/
                      
            system.debug('### targets size: ' + targets.size());      
            for(SAP_Target_Preparation_BI__c TA: [SELECT Id,
                       Maximum_Adjusted_Interactions_BI__c,
                       Maximum_Planned_Interactions_BI__c,
                       Primary_Specialty_BI__c,
                       Target_Customer_BI__c,
                       SAP_Header_BI__c,
                       SAP_Header_BI__r.Territory_BI__c,
                            (SELECT Id,
                            Adjusted_Details_BI__c,
                            Planned_Details_BI__c,
                            Product_BI__r.Name,
                            Segment_BI__c
                            FROM SAP_Details_Preparation__r)
                      FROM SAP_Target_Preparation_BI__c
                      WHERE SAP_Header_BI__c IN :sapPreparationIds]){
                //Filling the targets map by position by specialty with the target in context.
                //external map key exists into
                if(!String.isBlank(TA.Primary_Specialty_BI__c) && !String.isBlank(TA.SAP_Header_BI__r.Territory_BI__c)){
                    Boolean hasSpecialty = (mTargetsPositionBySpecialtyPlanned.containsKey(TA.Primary_Specialty_BI__c));
                    //get the map
                    /*transient map<String,list<SAP_Target_Preparation_BI__c>> targetsByPos = (hasSpecialty)? 
                        mTargetsPositionBySpecialty.get(TA.Primary_Specialty_BI__c) : new map<String,list<SAP_Target_Preparation_BI__c>>();*/
                    transient map<String,Integer> targetsByPosPlanned = (hasSpecialty)? 
                        mTargetsPositionBySpecialtyPlanned.get(TA.Primary_Specialty_BI__c) : new map<String,Integer>();
                    transient map<String,Integer> targetsByPosAdjusted = (hasSpecialty)? 
                        mTargetsPositionBySpecialtyAdjusted.get(TA.Primary_Specialty_BI__c) : new map<String,Integer>();
                    //inner map key exists into
                    Boolean hasPos = (targetsByPosPlanned.containsKey(TA.SAP_Header_BI__r.Territory_BI__c));
                    //get the list
                    /*transient list<SAP_Target_Preparation_BI__c> ltargets = (hasPos)? 
                        targetsByPos.get(TA.SAP_Header_BI__r.Territory_BI__c) : new list<SAP_Target_Preparation_BI__c>();*/
                    Integer planned = (hasPos)? targetsByPosPlanned.get(TA.SAP_Header_BI__r.Territory_BI__c) : 0;
                    Integer adjusted = (hasPos)? targetsByPosAdjusted.get(TA.SAP_Header_BI__r.Territory_BI__c) : 0;
                    //added a new target    
                    //ltargets.add(TA);
                    planned += (TA.Maximum_Planned_Interactions_BI__c != null)? TA.Maximum_Planned_Interactions_BI__c.intValue() : 0; 
                    adjusted += (TA.Maximum_Adjusted_Interactions_BI__c != null)? TA.Maximum_Adjusted_Interactions_BI__c.intValue() : 0;
                    targetsByPosPlanned.put(TA.SAP_Header_BI__r.Territory_BI__c,planned);
                    targetsByPosAdjusted.put(TA.SAP_Header_BI__r.Territory_BI__c,adjusted);
                    //new entry in inner map
                    /*if(!hasPos){
                        targetsByPos.put(TA.SAP_Header_BI__r.Territory_BI__c,ltargets);
                    }*/
                    //new entry in external map
                    if(!hasSpecialty) {
                        //Initialize new list for all positions
                        for (String p: salesPositions){
                            //if(!targetsByPos.containsKey(p)) targetsByPos.put(p, new list<SAP_Target_Preparation_BI__c>());
                            if(!targetsByPosPlanned.containsKey(p)) targetsByPosPlanned.put(p,0);
                            if(!targetsByPosAdjusted.containsKey(p)) targetsByPosAdjusted.put(p,0);
                        }
                        //mTargetsPositionBySpecialty.put(TA.Primary_Specialty_BI__c,targetsByPos);
                        mTargetsPositionBySpecialtyPlanned.put(TA.Primary_Specialty_BI__c,targetsByPosPlanned);
                        mTargetsPositionBySpecialtyAdjusted.put(TA.Primary_Specialty_BI__c,targetsByPosAdjusted);
                        //this map will contains as keys all targets specialties. (First empty values)
                        //mRelatedTargetsBySpecialty.put(TA.Primary_Specialty_BI__c,new list<SAP_Target_Preparation_BI__c>());
                        mRelatedTargetsBySpecialtyPlanned.put(TA.Primary_Specialty_BI__c,0);
                        mRelatedTargetsBySpecialtyAdjusted.put(TA.Primary_Specialty_BI__c,0);
                    }
                    
                    //mProductsByTarget.put(TA.Id,new set<String>());
                    //Filling the detail map by position by specialty with the details in context
                    for(SAP_Detail_Preparation_BI__c DP: TA.SAP_Details_Preparation__r){
                        //external map key exists into
                        hasSpecialty = (mDetailsPositionBySpecialtyPlanned.containsKey(TA.Primary_Specialty_BI__c));
                        Boolean hasSegment = (mDetailsPositionBySegmentPlanned.containsKey(DP.Segment_BI__c));
                        
                        //system.debug('hasSpecialty: ' + hasSpecialty);
                        //get the inner map
                        /*transient map<String, list<SAP_Detail_Preparation_BI__c>> detailsByPos = (hasSpecialty)?
                            mDetailsPositionBySpecialty.get(TA.Primary_Specialty_BI__c) : new map<String, list<SAP_Detail_Preparation_BI__c>>();*/
                        //system.debug('detailsByPos: ' + detailsByPos);
                        //inner map key exists into
                        transient map<String,Integer> detailsByPosPlanned = (hasSpecialty)? 
                            mDetailsPositionBySpecialtyPlanned.get(TA.Primary_Specialty_BI__c) : new map<String,Integer>();
                        transient map<String,Integer> detailsByPosAdjusted = (hasSpecialty)? 
                            mDetailsPositionBySpecialtyAdjusted.get(TA.Primary_Specialty_BI__c) : new map<String,Integer>();
                        
                        //segmentation inner maps
                        transient map<String,Integer> detailsByPosSegPlanned = (hasSegment)?
                            mDetailsPositionBySegmentPlanned.get(DP.Segment_BI__c) : new map<String,Integer>();
                        transient map<String,Integer> detailsByPosSegAdjusted = (hasSegment)?
                            mDetailsPositionBySegmentAdjusted.get(DP.Segment_BI__c) : new map<String,Integer>();
                        
                        //system.debug('### SET STRUCTURES');
                        hasPos = detailsByPosPlanned.containsKey(TA.SAP_Header_BI__r.Territory_BI__c);
                        //system.debug('hasPos: ' + hasPos);
                        //system.debug('target pos: ' + TA.SAP_Header_BI__r.Territory_BI__c);
                        //get the list
                        /*list<SAP_Detail_Preparation_BI__c> ldetails = (hasPos)?
                            detailsByPos.get(TA.SAP_Header_BI__r.Territory_BI__c) : new list<SAP_Detail_Preparation_BI__c>();*/
                        Integer dPlanned = (hasPos)? detailsByPosPlanned.get(TA.SAP_Header_BI__r.Territory_BI__c) : 0;
                        Integer dAdjusted = (hasPos)? detailsByPosAdjusted.get(TA.SAP_Header_BI__r.Territory_BI__c) : 0;
                        //added a new detail
                        //system.debug('DP: ' + DP);
                        //system.debug('ldetails: ' + ldetails);
                        //ldetails.add(DP);
                        Integer plannedToAdd = (DP.Planned_Details_BI__c != null)? DP.Planned_Details_BI__c.intValue() : 0;
                        Integer adjustedToAdd = (DP.Adjusted_Details_BI__c != null)? DP.Adjusted_Details_BI__c.intValue() : 0;
                        dPlanned += plannedToAdd;
                        dAdjusted += adjustedToAdd;
                        detailsByPosPlanned.put(TA.SAP_Header_BI__r.Territory_BI__c,dPlanned);
                        detailsByPosAdjusted.put(TA.SAP_Header_BI__r.Territory_BI__c,dAdjusted);
                        //updating products by target map.
                        //mProductsByTarget.get(TA.Id).add(DP.Product_BI__r.Name);
                        
                        //updating segment counters by pos.
                        Integer dPlannedSeg = (detailsByPosSegPlanned.containsKey(TA.SAP_Header_BI__r.Territory_BI__c))? 
                            detailsByPosSegPlanned.get(TA.SAP_Header_BI__r.Territory_BI__c) : 0;
                        Integer dAdjustedSeg = (detailsByPosSegAdjusted.containsKey(TA.SAP_Header_BI__r.Territory_BI__c))? 
                            detailsByPosSegAdjusted.get(TA.SAP_Header_BI__r.Territory_BI__c) : 0;
                        //system.debug('### BEFORE');
                        //system.debug('dPlannedSeg: ' + dPlannedSeg);
                        //system.debug('plannedToAdd: ' + plannedToAdd);
                        dPlannedSeg += plannedToAdd;
                        //system.debug('### AFTER');
                        dAdjustedSeg += adjustedToAdd;
                        detailsByPosSegPlanned.put(TA.SAP_Header_BI__r.Territory_BI__c,dPlannedSeg);
                        detailsByPosSegAdjusted.put(TA.SAP_Header_BI__r.Territory_BI__c,dAdjustedSeg);
                        
                        //new entry in inner map
                        /*if(!hasPos){
                            detailsByPos.put(TA.SAP_Header_BI__r.Territory_BI__c,ldetails);
                        }*/
                        if(!hasSpecialty || !hasSegment){
                            //Initialize new list for all positions
                            for (String p: salesPositions){
                                if(!hasSpecialty){
                                    //if(!detailsByPos.containsKey(p)) detailsByPos.put(p, new list<SAP_Detail_Preparation_BI__c>());
                                    if(!detailsByPosPlanned.containsKey(p)) detailsByPosPlanned.put(p,0);
                                    if(!detailsByPosAdjusted.containsKey(p)) detailsByPosAdjusted.put(p,0);
                                }
                                if(!hasSegment){
                                    if(!detailsByPosSegPlanned.containsKey(p)) detailsByPosSegPlanned.put(p,0);
                                    if(!detailsByPosSegAdjusted.containsKey(p)) detailsByPosSegAdjusted.put(p,0);
                                }
                            }
                        }
                        //new entry in external map
                        if(!hasSpecialty) {
                            //mDetailsPositionBySpecialty.put(TA.Primary_Specialty_BI__c,detailsByPos);
                            mDetailsPositionBySpecialtyPlanned.put(TA.Primary_Specialty_BI__c,detailsByPosPlanned);
                            mDetailsPositionBySpecialtyAdjusted.put(TA.Primary_Specialty_BI__c,detailsByPosAdjusted);
                            
                            //this map will contains as keys all details specialties. (First empty values)
                            //mRelatedDetailsBySpecialty.put(TA.Primary_Specialty_BI__c,new list<SAP_Detail_Preparation_BI__c>());
                            mRelatedDetailsBySpecialtyPlanned.put(TA.Primary_Specialty_BI__c,0);
                            mRelatedDetailsBySpecialtyAdjusted.put(TA.Primary_Specialty_BI__c,0);
                            
                            
                        }
                        //Fill details by position by segmentation map and related.
                        if(!hasSegment){
                            mDetailsPositionBySegmentPlanned.put(DP.Segment_BI__c,detailsByPosSegPlanned);
                            mDetailsPositionBySegmentAdjusted.put(DP.Segment_BI__c,detailsByPosSegAdjusted);
                                
                            //mRelatedDetailsBySegment.put(DP.Segment_BI__c,new list<SAP_Detail_Preparation_BI__c>());
                            mRelatedDetailsBySegmentPlanned.put(DP.Segment_BI__c,0);
                            mRelatedDetailsBySegmentAdjusted.put(DP.Segment_BI__c,0);
                        }
                    }
                    //Filling aux structures in order to in the future retrieve related targets correctly
                    //updating map Targets by position
                    /*hasPos = (targetIdsByPosition.containsKey(TA.SAP_Header_BI__r.Territory_BI__c));
                    set<Id> targetIds = (hasPos)? 
                        targetIdsByPosition.get(TA.SAP_Header_BI__r.Territory_BI__c) : new set<Id>();
                    targetIds.add(TA.Id);
                    if(!hasPos) targetIdsByPosition.put(TA.SAP_Header_BI__r.Territory_BI__c,targetIds);
                    //updating map customer Ids by position
                    hasPos = (customerIdsByPosition.containsKey(TA.SAP_Header_BI__r.Territory_BI__c));
                    set<Id> customerIds = (hasPos)? 
                        customerIdsByPosition.get(TA.SAP_Header_BI__r.Territory_BI__c) : new set<Id>();
                    customerIds.add(TA.Target_Customer_BI__c);
                    if(!hasPos) customerIdsByPosition.put(TA.SAP_Header_BI__r.Territory_BI__c,customerIds);*/
                    
                    Boolean hasCustomer = (mTargetIdsByCustomerId.containsKey(TA.Target_Customer_BI__c));
                    transient set<Id> targetIdsByCustomer = (hasCustomer) ?
                        mTargetIdsByCustomerId.get(TA.Target_Customer_BI__c): new set<Id>();
                    targetIdsByCustomer.add(TA.Id);
                    if(!hasCustomer) mTargetIdsByCustomerId.put(TA.Target_Customer_BI__c,targetIdsByCustomer);
                }
            }
                
            
            //system.debug('#### mTargetsPositionBySpecialty: ' + mTargetsPositionBySpecialty);
            //system.debug('#### mDetailsPositionBySpecialty: ' + mDetailsPositionBySpecialty);
            system.debug('#### mTargetsPositionBySpecialtyPlanned: ' + mTargetsPositionBySpecialtyPlanned);
            system.debug('#### mTargetsPositionBySpecialtyAdjusted: ' + mTargetsPositionBySpecialtyAdjusted);
            system.debug('#### mDetailsPositionBySpecialtyPlanned: ' + mDetailsPositionBySpecialtyPlanned);
            system.debug('#### mDetailsPositionBySpecialtyAdjusted: ' + mDetailsPositionBySpecialtyAdjusted);
            system.debug('#### mDetailsPositionBySegmentPlanned: ' + mDetailsPositionBySegmentPlanned);
            system.debug('#### mDetailsPositionBySegmentAdjusted: ' + mDetailsPositionBySegmentPlanned);
            system.debug('#### mTargetIdsByCustomerId: ' + mTargetIdsByCustomerId);
            
            
            //Retrieving and mapping related targets by SAP.
            //For each position will be found the other ones to extract all related targets from these positions.
            //By this way is not necessary queries and we are avoiding to iterate unnecessary data.
            
            for(Id customerId: mTargetIdsByCustomerId.keySet()){
                set<Id> targetIds = mTargetIdsByCustomerId.get(customerId);
                if(targetIds.size()>1){
                    relatedCustomerIds.add(customerId);
                }
            }
            
            system.debug('#### relatedCustomerIds: ' + relatedCustomerIds);
            //Retrieving and mapping related targets by SAP.
            //Creating an unique values collection of related targets into a set
            transient list<SAP_Target_Preparation_BI__c> relatedTargets = new list<SAP_Target_Preparation_BI__c>();
            /*relatedTargets = [SELECT Id,
                              Maximum_Adjusted_Interactions_BI__c,
                              Maximum_Planned_Interactions_BI__c,
                              Primary_Specialty_BI__c,
                                  (SELECT Id,
                                  Adjusted_Details_BI__c,
                                  Planned_Details_BI__c,
                                  Segment_BI__c
                                  FROM SAP_Details_Preparation__r)
                              FROM SAP_Target_Preparation_BI__c
                              //WHERE Target_Customer_BI__c IN :customerIds 
                              //AND Id NOT IN :targetsIds
                              //AND SAP_Header_BI__r.Territory_BI__c != :p];
                              WHERE Target_Customer_BI__c IN :relatedCustomerIds
                              AND SAP_Header_BI__r.Start_Date_BI__c >= :firstDate 
                              AND SAP_Header_BI__r.End_Date_BI__c <= :lastDate];*/
            relatedTargets = [SELECT Id,
                              Maximum_Adjusted_Interactions_BI__c,
                              Maximum_Planned_Interactions_BI__c,
                              Primary_Specialty_BI__c,
                                  (SELECT Id,
                                  Adjusted_Details_BI__c,
                                  Planned_Details_BI__c,
                                  Segment_BI__c
                                  FROM SAP_Details_Preparation__r)
                              FROM SAP_Target_Preparation_BI__c
                              //WHERE Target_Customer_BI__c IN :customerIds 
                              //AND Id NOT IN :targetsIds
                              //AND SAP_Header_BI__r.Territory_BI__c != :p];
                              WHERE Target_Customer_BI__c IN :relatedCustomerIds
                              AND SAP_Header_BI__r.Start_Date_BI__c >= :current];
                              
            /*for(String p: targetIdsByPosition.keySet()){ //by position iteration
                set<Id> targetsIds = targetIdsByPosition.get(p);
                set<Id> customerIds = customerIdsByPosition.get(p);
                list<SAP_Target_Preparation_BI__c> lRelatedByPos = new list<SAP_Target_Preparation_BI__c>();
                lRelatedByPos =  [SELECT Id,
                                  Maximum_Adjusted_Interactions_BI__c,
                                  Maximum_Planned_Interactions_BI__c,
                                  Primary_Specialty_BI__c,
                                      (SELECT Id,
                                      Adjusted_Details_BI__c,
                                      Planned_Details_BI__c,
                                      Segment_BI__c
                                      FROM SAP_Details_Preparation__r)
                                  FROM SAP_Target_Preparation_BI__c
                                  WHERE Target_Customer_BI__c IN :customerIds 
                                  AND Id NOT IN :targetsIds
                                  AND SAP_Header_BI__r.Territory_BI__c != :p];
                relatedTargets.addAll(lRelatedByPos);
            }*/
            
            system.debug('### relatedTargets: ' + relatedTargets);
            
            //Filling related main structures
            
            for(SAP_Target_Preparation_BI__c RT: relatedTargets){
                if(!String.isBlank(RT.Primary_Specialty_BI__c)){
                    Boolean hasSpecialty = (mRelatedTargetsBySpecialtyPlanned.containsKey(RT.Primary_Specialty_BI__c));
                    
                    if (hasSpecialty){ //only if the specialty exist as key is possible to add the new related target to the map.
                        //transient list<SAP_Target_Preparation_BI__c> relTargetsBySpecialty = mRelatedTargetsBySpecialty.get(RT.Primary_Specialty_BI__c);
                        Integer planned = mRelatedTargetsBySpecialtyPlanned.get(RT.Primary_Specialty_BI__c);
                        Integer adjusted = mRelatedTargetsBySpecialtyAdjusted.get(RT.Primary_Specialty_BI__c);
                        //relTargetsBySpecialty.add(RT);
                        planned += (RT.Maximum_Planned_Interactions_BI__c != null)? RT.Maximum_Planned_Interactions_BI__c.intValue() : 0;
                        adjusted += (RT.Maximum_Adjusted_Interactions_BI__c != null)? RT.Maximum_Adjusted_Interactions_BI__c.intValue() : 0;
                        mRelatedTargetsBySpecialtyPlanned.put(RT.Primary_Specialty_BI__c,planned);
                        mRelatedTargetsBySpecialtyAdjusted.put(RT.Primary_Specialty_BI__c,adjusted);
                        
                        for(SAP_Detail_Preparation_BI__c RD: RT.SAP_Details_Preparation__r){
                            hasSpecialty = (mRelatedDetailsBySpecialtyPlanned.containsKey(RT.Primary_Specialty_BI__c));
                            Boolean hasSegment = (mRelatedDetailsBySegmentPlanned.containsKey(RD.Segment_BI__c));
                            if(hasSpecialty || hasSegment){//only if the specialty exist as key is possible to add the new related detail to the map.
                                Integer plannedToAdd = RD.Planned_Details_BI__c.intValue();
                                Integer adjustedToAdd = RD.Adjusted_Details_BI__c.intValue();
                                if(hasSpecialty){
                                    //list<SAP_Detail_Preparation_BI__c> relDetailsBySpecialty = mRelatedDetailsBySpecialty.get(RT.Primary_Specialty_BI__c);
                                    Integer dPlanned = mRelatedDetailsBySpecialtyPlanned.get(RT.Primary_Specialty_BI__c) + plannedToAdd;
                                    Integer dAdjusted = mRelatedDetailsBySpecialtyAdjusted.get(RT.Primary_Specialty_BI__c) + adjustedToAdd;
                                    //relDetailsBySpecialty.add(RD);
                                    
                                    mRelatedDetailsBySpecialtyPlanned.put(RT.Primary_Specialty_BI__c,dPlanned);
                                    mRelatedDetailsBySpecialtyAdjusted.put(RT.Primary_Specialty_BI__c,dAdjusted);
                                }
                                if(hasSegment){
                                    //list<SAP_Detail_Preparation_BI__c> relDetailsBySegment = mRelatedDetailsBySegment.get(RD.Segment_BI__c);
                                    Integer dPlanned = mRelatedDetailsBySegmentPlanned.get(RD.Segment_BI__c) + plannedToAdd;
                                    Integer dAdjusted = mRelatedDetailsBySegmentAdjusted.get(RD.Segment_BI__c) + adjustedToAdd;
                                    //relDetailsBySegment.add(RD);
                                    
                                    mRelatedDetailsBySegmentPlanned.put(RD.Segment_BI__c,dPlanned);
                                    mRelatedDetailsBySegmentAdjusted.put(RD.Segment_BI__c,dAdjusted);
                                }
                            }
                        }
                    }               
                }
            }
            
            //system.debug('#### mRelatedTargetsBySpecialty: ' + mRelatedTargetsBySpecialty);
            //system.debug('#### mRelatedDetailsBySpecialty: ' + mRelatedDetailsBySpecialty);
            system.debug('#### mRelatedTargetsBySpecialtyPlanned: ' + mRelatedTargetsBySpecialtyPlanned);
            system.debug('#### mRelatedTargetsBySpecialtyAdjusted: ' + mRelatedTargetsBySpecialtyAdjusted);
            system.debug('#### mRelatedDetailsBySpecialtyPlanned: ' + mRelatedDetailsBySpecialtyPlanned);
            system.debug('#### mRelatedDetailsBySpecialtyAdjusted: ' + mRelatedDetailsBySpecialtyAdjusted);
            //system.debug('#### mRelatedDetailsBySegment: ' + mRelatedDetailsBySegment);
            system.debug('#### mRelatedDetailsBySegmentPlanned: ' + mRelatedDetailsBySegmentPlanned);
            system.debug('#### mRelatedDetailsBySegmentAdjusted: ' + mRelatedDetailsBySegmentAdjusted);
            
            DashboardDataStructures dataObj = new DashboardDataStructures();
            dataObj.salesPositions = salesPositions;
            dataObj.segmentModes = segmentModes;
            dataObj.hasSAPs = hasSAPs;
            //BY SPECIALTY
            //Related
            dataObj.relatedTargetsBySpecialtyPlanned  = mRelatedTargetsBySpecialtyPlanned;
            dataObj.relatedTargetsBySpecialtyAdjusted = mRelatedTargetsBySpecialtyAdjusted;
            dataObj.relatedDetailsBySpecialtyPlanned  = mRelatedDetailsBySpecialtyPlanned;
            dataObj.relatedDetailsBySpecialtyAdjusted = mRelatedDetailsBySpecialtyAdjusted;
            //No Related
            dataObj.targetsBySpecialtyPlanned  = mTargetsPositionBySpecialtyPlanned;
            dataObj.targetsBySpecialtyAdjusted = mTargetsPositionBySpecialtyAdjusted;
            dataObj.detailsBySpecialtyPlanned  = mDetailsPositionBySpecialtyPlanned;
            dataObj.detailsBySpecialtyAdjusted = mDetailsPositionBySpecialtyAdjusted;
            
            //BY SEGMENTATION
            /*dataObj.relatedDetailsBySegmentPlanned  = mRelatedDetailsBySegmentPlanned;
            dataObj.relatedDetailsBySegmentAdjusted = mRelatedDetailsBySegmentAdjusted;
            dataObj.detailsBySegmentPlanned  = mDetailsPositionBySegmentPlanned;
            dataObj.detailsBySegmentAdjusted = mDetailsPositionBySegmentAdjusted;*/
            
            return dataObj;
        }else{
            return new DashboardDataStructures();
        }
    }
    
    public class DashboardDataStructures{
        public set<String> salesPositions = new set<String>();
        public set<String> segmentModes = new set<String>();
        public Boolean hasSAPs = false;
        //BY SPECIALTY
        //Related targets and details planned and adjusted by specialty structures
        public map<String,Integer> relatedTargetsBySpecialtyPlanned;
        public map<String,Integer> relatedTargetsBySpecialtyAdjusted;
        public map<String,Integer> relatedDetailsBySpecialtyPlanned;
        public map<String,Integer> relatedDetailsBySpecialtyAdjusted;
        
        //targets and details planned and adjusted by specialty structures
        public map<String,map<String,Integer>> targetsBySpecialtyPlanned;
        public map<String,map<String,Integer>> targetsBySpecialtyAdjusted;
        public map<String,map<String,Integer>> detailsBySpecialtyPlanned;
        public map<String,map<String,Integer>> detailsBySpecialtyAdjusted;
        
        //BY SEGMENTATION
        //Related details planned and adjusted by segmentation structures
        public map<String,Integer> relatedDetailsBySegmentPlanned;
        public map<String,Integer> relatedDetailsBySegmentAdjusted;
        
        //Details planned and adjusted by segmentation structures
        public map<String,map<String,Integer>> detailsBySegmentPlanned;
        public map<String,map<String,Integer>> detailsBySegmentAdjusted;
    }
    
}