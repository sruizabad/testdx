@isTest
private class BI_SAP_DashboardCtrlTest {
	@testSetup
    static void setUpData() {
        Test.startTest();
            BI_SAP_SetupClassTest.setupTestData();
        Test.stopTest();
    }
    
  	//DashboardCtrl Main Test
    @isTest
    static void BI_SAP_Dashboard_MainTest() {
       System.Debug('Test main for Dashboard Controller');
       User salesManagerUser = [SELECT Id FROM User WHERE Alias = 'SMUser1'];
       User dataStewardUser = [SELECT Id FROM User WHERE Alias = 'DSUser1'];
       User salesUser = [SELECT Id FROM User WHERE Alias = 'SRUser1'];
       
       //START COVERAGE TEST
       system.runAs(salesManagerUser){
       		BI_SAP_DashboardCtrl dashboardAs_SM = new BI_SAP_DashboardCtrl();
	        
       }
       
       system.runAs(dataStewardUser){
       		BI_SAP_DashboardCtrl dashboardAs_DS = new BI_SAP_DashboardCtrl();
       }
       
       system.runAs(salesUser){
       		BI_SAP_DashboardCtrl dashboardAs_SR = new BI_SAP_DashboardCtrl();
       }
       
   	   BI_SAP_DashboardCtrl dashboard = new BI_SAP_DashboardCtrl();
       set<String> positions = new set<String>{'sprep','SFF-1990'};
       String positionsJSON = JSON.serialize (positions);
       system.debug('#TEST positions: ' + positions);
       system.debug('#TEST positionsJSON: ' + positionsJSON);
       BI_SAP_DashboardCtrl.DashboardDataStructures structures = BI_SAP_DashboardCtrl.generateTargetsAndDetailStructures(positionsJSON);
       
       //FINISH COVERAGE TEST
       
       //START FUNCTIONAL TEST
       set<String> mDashboardSpecialtiesForTargets = structures.targetsBySpecialtyPlanned.keySet();
       set<String> mDashboardSpecialtiesForDetails = structures.detailsBySpecialtyPlanned.keySet();
       
       //Limit SAPs by Cycle as DASHBOARD CODE
       Date current = Date.today();
       Date firstDate = current.toStartofMonth();
       Date lastDate = current.addMonths(1).toStartofMonth().addDays(-1);
       
       list<SAP_Preparation_BI__c> lSap = [SELECT Id, Territory_BI__c 
       									   FROM SAP_Preparation_BI__c 
       									   WHERE Territory_BI__c IN :positions
       									   AND Start_Date_BI__c >= :firstDate
       									   AND End_Date_BI__c <= :lastDate];
       set<String> dashboardPositions = structures.salesPositions;
       set<String> territories = new set<String>();
       set<Id> sapIds = new set<Id>();
       set<String> specialtiesTarget = new set<String> ();
       set<String> specialtiesDetail = new set<String> (); 
       
       for(SAP_Preparation_BI__c sap: lSap) {
		   territories.add(sap.Territory_BI__c);
		   sapIds.add(sap.Id);
	   }
	   
	   list<SAP_Target_Preparation_BI__c> lTargets = [SELECT Id,Primary_Specialty_BI__c,
	   														 (SELECT Id FROM SAP_Details_Preparation__r) 
   													  FROM SAP_Target_Preparation_BI__c 
   													  WHERE SAP_Header_BI__c IN :sapIds];
   													  
       for(SAP_Target_Preparation_BI__c TA: lTargets){
       	   specialtiesTarget.add(TA.Primary_Specialty_BI__c);
       	   for(SAP_Detail_Preparation_BI__c DP: TA.SAP_Details_Preparation__r){
       	       specialtiesDetail.add(TA.Primary_Specialty_BI__c);
       	       break;
       	   }
       }
       
       system.debug('#### specialtiesTarget: ' + specialtiesTarget);
       system.debug('#### specialtiesDetail: ' + specialtiesDetail);
       system.debug('#### mDashboardSpecialtiesForTargets: ' + mDashboardSpecialtiesForTargets);
       system.debug('#### mDashboardSpecialtiesForDetails: ' + mDashboardSpecialtiesForDetails);
       system.debug('#### positionsLenght: ' + territories.size());
       system.debug('#### dashboardPositions: ' + dashboardPositions);
       system.debug('#### specialtiesTarget SIZE: ' + specialtiesTarget.size());
       system.debug('#### specialtiesDetail SIZE: ' + specialtiesDetail.size());
       system.debug('#### mDashboardSpecialtiesForTargets SIZE: ' + mDashboardSpecialtiesForTargets.size());
       system.debug('#### mDashboardSpecialtiesForDetails SIZE: ' + mDashboardSpecialtiesForDetails.size());
       system.debug('#### dashboardPosition SIZEs: ' + dashboardPositions.size());
       system.assertEquals(mDashboardSpecialtiesForTargets.size(), specialtiesTarget.size()); //num specialties created
       system.assertEquals(mDashboardSpecialtiesForDetails.size(), specialtiesDetail.size()); //num specialties created
       system.assertEquals(dashboardPositions.size(), territories.size()); //num positions
       
       //END FUNCTIONAL TEST
   
    }
}