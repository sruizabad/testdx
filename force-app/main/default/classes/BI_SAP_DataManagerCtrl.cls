global with sharing class BI_SAP_DataManagerCtrl {
    
    /*
		SAP Clonation and Synchronization Variables
	*/
	public list<SAP_Preparation_BI__c> lSAPsToClone {get; set;}
	public list<SAP_Preparation_BI__c> lSAPs {get; set;}
	public String lSAPsToCloneJSON {get; set;}
	public map<Id,String> ownerNamesBySAPId {get; set;}
	

	/*
        Synchronization Variables
    */
    public list<SAP_Preparation_BI__c> lSAPsToSync {get; set;}
    public list<SAP_Preparation_BI__c> lSyncSAPs {get; set;}
    public String lSAPsToSyncJSON {get; set;}
    public map<Id,String> ownerNamesBySyncSAPId {get; set;}

    
	// This boolean indicates us if we have an available cycle to which
	// we shall clone
	public boolean canClone {get;set;}
	public boolean canSync {get;set;}
	private String userCountryCode;
	
	/*
		Common Variables
	*/
	
	
	/*****************
	 **	Constructor **
	 *****************/
    
    public BI_SAP_DataManagerCtrl(){
    	// Setting common data
    	system.debug('%%% setUserCountryCode()');
    	setUserCountryCode();
    	
    	system.debug('%%% setlSAPsToClone()');
    	setlSAPsToClone();

		system.debug('%%% setlSAPsToSync()');
        setlSAPsToSync();
    }
	

	/***************************
     ** Getting SAPs to Clone **
     **************************/
	public void setlSAPsToClone(){
		Date currentDate = Date.today();
		
		// We will filter all SAPs wich status is currently approved.
		lSAPs = [SELECT Id, Name, Country_Code_BI__c, Territory_BI__c, Status_BI__c, Start_Date_BI__c, End_Date_BI__c, OwnerId
						FROM SAP_Preparation_BI__c 
						WHERE Country_Code_BI__c =: userCountryCode
						AND Start_Date_BI__c != null
						AND END_Date_BI__c != null
						AND Start_Date_BI__c < :currentDate
						AND END_Date_BI__c >= :currentDate];
						
		system.debug('%%% lSAPs.size(): ' + lSAPs.size());
						
		lSAPsToClone = new list<SAP_Preparation_BI__c>();
		ownerNamesBySAPId = new map<Id,String>();
		set<Id> ownerIds = new set<Id>();
		map<Id,Id> OwnerIdBySAPId = new map<Id,Id>();
		
		for(SAP_Preparation_BI__c SAP: lSAPs){
			if(SAP.Status_BI__c == 'Approved'){
				lSAPsToClone.add(SAP);
			}
			ownerIds.add(SAP.OwnerId);
			system.debug('%%% SAP.OwnerId: ' + SAP.OwnerId);
			system.debug('%%% SAP.Id: ' + SAP.Id);
			OwnerIdBySAPId.put(SAP.Id,SAP.OwnerId);
		}
		
		list<User> lUsers = [SELECT Id, Name FROM User WHERE Id IN :ownerIds];
		map <Id,String> userNamesByUserId = new map <Id,String>();
		
		for(User u: lUsers){
			userNamesByUserId.put(u.Id,u.Name);
		}
		
		for(SAP_Preparation_BI__c SAP: lSAPs){
			ownerNamesBySAPId.put(SAP.Id,userNamesByUserId.get(OwnerIdBySAPId.get(SAP.Id)));
		}
		
		system.debug('%%% ownerNamesBySAPId: ' + ownerNamesBySAPId);
		
		system.debug('%%% lSAPsToClone: ' + lSAPsToClone);
    	system.debug('%%% lSAPsToClone.Size(): ' + lSAPsToClone.Size());
		
		canClone = (lSAPsToClone.size()>0) && (lSAPsToClone.size()==lSAPs.size());
		lSAPsToCloneJSON = JSON.serialize(lSAPsToClone);
		
	}
	
	/***************************
     ** Getting SAPs to Sync **
     **************************/
    
	public void setlSAPsToSync(){
        Date currentDate = Date.today();
        
        // We will filter all SAPs wich status is currently approved.
        lSyncSAPs = [SELECT Id, Name, Country_Code_BI__c, Territory_BI__c, Status_BI__c, Start_Date_BI__c, End_Date_BI__c, OwnerId
                        FROM SAP_Preparation_BI__c 
                        WHERE Country_Code_BI__c =: userCountryCode
                        AND Start_Date_BI__c != null
                        AND END_Date_BI__c != null
                        AND Start_Date_BI__c >= :currentDate];
                        
        lSAPsToSync = new list<SAP_Preparation_BI__c>();
        ownerNamesBySyncSAPId = new map<Id,String>();
        set<Id> ownerIds = new set<Id>();
        map<Id,Id> OwnerIdBySAPId = new map<Id,Id>();
        
        for(SAP_Preparation_BI__c SAP: lSyncSAPs){
            if(SAP.Status_BI__c == 'Approved'){
                lSAPsToSync.add(SAP);
            }
            ownerIds.add(SAP.OwnerId);
            OwnerIdBySAPId.put(SAP.Id,SAP.OwnerId);
        }
	 
        list<User> lUsers = [SELECT Id, Name FROM User WHERE Id IN :ownerIds];
        map <Id,String> userNamesByUserId = new map <Id,String>();
        
        for(User u: lUsers){
            userNamesByUserId.put(u.Id,u.Name);
        }
        
        for(SAP_Preparation_BI__c SAP: lSyncSAPs){
            ownerNamesBySyncSAPId.put(SAP.Id,userNamesByUserId.get(OwnerIdBySAPId.get(SAP.Id)));
        }
        
        system.debug('%%% ownerNamesBySyncSAPId: ' + ownerNamesBySyncSAPId);
        
        system.debug('%%% lSAPsToSync: ' + lSAPsToSync);
        system.debug('%%% lSAPsToSync.Size(): ' + lSAPsToSync.Size());
        
        canSync = (lSAPsToSync.size()>0) && (lSAPsToSync.size()==lSyncSAPs.size());
        lSAPsToSyncJSON = JSON.serialize(lSAPsToSync);
        
    }


     
	/******************************
	 **	SAP Synchronization Code **
	 ******************************/
	
	@RemoteAction
	public static Boolean synchronizeSAPs(String SAPName, String lSAPToSynchronizeJSON){
		list<SAP_Preparation_BI__c> lSAPsToSyncronize = (list<SAP_Preparation_BI__c>) JSON.deserialize(lSAPToSynchronizeJSON,list<SAP_Preparation_BI__c>.class);
		Boolean canSynchronize = true;
		
		for(SAP_Preparation_BI__c SAP: lSAPsToSyncronize){
			canSynchronize = SAP.Status_BI__c != null && SAP.Status_BI__c == 'Approved';
			if(!canSynchronize) break;
		}
		
		//If there are SAPs to syncronize and all are approved.
		if(lSAPsToSyncronize.size()>0 && canSynchronize){
			system.debug('## synchronize process started!!');
            if(!Test.isRunningTest()){
			BI_SAP_SynchronizeDataBatch syncDataBatch = new BI_SAP_SynchronizeDataBatch(lSAPsToSyncronize, SAPName);
			Id batchJobId = Database.executeBatch(syncDataBatch, 10);
		}
        }
		return canSynchronize;
	}
    
    
    /************************
	 **	SAP Clonation Code **
	 ************************/
	 
    @RemoteAction
    public static Boolean cloneSAPs(String lSAPsToCloneJSON, String StartDate, String EndDate){
    	
    	list<SAP_Preparation_BI__c> lSAPsToClone = (list<SAP_Preparation_BI__c>) JSON.deserialize(lSAPsToCloneJSON,list<SAP_Preparation_BI__c>.class);
    	String[] sDArr = StartDate.split('/');
    	String[] eDArr = EndDate.split('/');
    	
    	system.debug('### sDArr: ' + sDArr);
    	system.debug('### eDArr: ' + eDArr);
    	
    	// newInstance(year, month, date)
    	Date startDateD = Date.newInstance(Integer.valueOf(sDArr.get(2)),Integer.valueOf(sDArr.get(1)),Integer.valueOf(sDArr.get(0)));
    	Date endDateD = Date.newInstance(Integer.valueOf(eDArr.get(2)),Integer.valueOf(eDArr.get(1)),Integer.valueOf(eDArr.get(0)));
    	
    	system.debug('### startDateD: ' + startDateD);
    	system.debug('### endDateD: ' + endDateD);
    	
    	BI_SAP_DataManagerCtrl.SAPCycleWrapper SAPCycleWrap = new BI_SAP_DataManagerCtrl.SAPCycleWrapper(startDateD,endDateD);
    	
    	system.debug('### SAPCycleWrap: ' + SAPCycleWrap);
    	
    	String userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
    	
    	Boolean canClone = true;
		
		for(SAP_Preparation_BI__c SAP: lSAPsToClone){
			canClone = SAP.Status_BI__c != null && SAP.Status_BI__c == 'Approved';
			if(!canClone) break;
		}
		
    	//If there are SAPs to clone and all are approved.
    	if(lSAPsToClone.size()>0 && canClone){
    		system.debug('## clone process started!!');
            if(!Test.isRunningTest()){
    		BI_SAP_CloneDataBatch cloneDataBatch = new BI_SAP_CloneDataBatch(lSAPsToClone, SAPCycleWrap, userCountryCode);
			Id batchJobId = Database.executeBatch(cloneDataBatch, 10);
    	}
        }
    	
    	return canClone;
    }
    
    
    /*****************
	 **	Common Code **
	 *****************/
    
    private void setUserCountryCode(){
    	Id currentUserId = UserInfo.getUserId();
    	
    	userCountryCode = [SELECT Id, Country_Code_BI__c FROM User WHERE Id =: currentUserId LIMIT 1].Country_Code_BI__c;
    	
    	System.debug('%%% userCountryCode: ' + userCountryCode);
    }
    
    
    /**************************
	 **	Common Inner Classes **
	 **************************/
    
    global class SAPCycleWrapper{
    	public Date startDate;
    	public Date endDate;
    	
    	public SAPCycleWrapper(Date startDate, Date endDate){
    		this.startDate = startDate;
    		this.endDate = endDate;
    	}
    }
    
}