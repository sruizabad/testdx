/** 
 * SAP Data Manager Test Class.
 * Date: 12/13/2016
 * @author Omega CRM Consulting
 * @version 1.0
*/
@isTest
private class BI_SAP_DataManagerCtrlTest { 
    
    @testSetup
    static void testData(){
        Test.startTest();
             BI_SAP_SetupClassTest.setupTestData();
        Test.stopTest();
    }
    
    /**
    ** Main method to launch the main test class methods
    **/
    public static void testMethodMain(){
        Test.startTest();
            testDataManager();
        Test.stopTest();
    }
    
	@isTest
    public static void testMethodMainTest(){
        testMethodMain();
    }
    
    static void testDataManager() {
    	list<Account> lAcc = [SELECT Id, Name FROM Account];
    	system.debug('%%% lAcc: ' + lAcc);
    	
    	list<SAP_Target_Preparation_BI__c> lTargets = [SELECT Id, Target_Customer_BI__r.Name FROM SAP_Target_Preparation_BI__c];
    	system.debug('%%% lTargets: ' + lTargets);
    	
    	
    	system.debug('%%% new BI_SAP_DataManagerCtrl');
    	BI_SAP_DataManagerCtrl dMC = new BI_SAP_DataManagerCtrl();
    	
    	list<SAP_Preparation_BI__c> lSAPs = [SELECT Id, Name FROM SAP_Preparation_BI__c];
    	Integer prevSize = lSAPs.size();
    	
    	Integer startDay = 1;
    	Integer startMonth = Date.today().month();
    	Integer startYear = Date.today().year();
    	
    	Integer endDay = Date.today().toStartOfMonth().addMonths(1).addDays(-1).day();
    	Integer endMonth = startMonth;
    	Integer endYear = startYear;
    	
    	String startDate = startMonth + '/' + startDay + '/' + startYear;
    	String endDate = endDay + '/' + endMonth + '/' + endYear;
    	
    	BI_SAP_DataManagerCtrl.cloneSAPs(dMC.lSAPsToCloneJSON, startDate, endDate);
    	lSAPs = [SELECT Id, Name FROM SAP_Preparation_BI__c];
    	system.debug('%%% lSAPs: ' + lSAPs);
    	//system.assertEquals((prevSize*2), lSAPs.size());
    	system.debug('%%% finished first assert');
    	
    	// Synchronization
    	
    	list<SAP_Preparation_BI__c> lSAPsToSync = [SELECT Id, Name, Status_BI__c FROM SAP_Preparation_BI__c WHERE Status_BI__c = 'Approved'];
    	list<Cycle_Plan_vod__c> synchronizedSAPs = [SELECT Id, Name FROM Cycle_Plan_vod__c];
    	
    	Integer prevToSyncSize = dMC.lSAPsToSync.size();
    	Integer prevSyncSize = synchronizedSAPs.size();
    	system.debug('%%% prevToSyncSize: ' + prevToSyncSize);
    	system.debug('%%% prevSyncSize: ' + prevSyncSize);
    	
    	BI_SAP_DataManagerCtrl.synchronizeSAPs('SAP Name Test',dMC.lSAPsToSyncJSON);
    	
    	synchronizedSAPs = [SELECT Id, Name FROM Cycle_Plan_vod__c];
    	Integer synchronizedSize = synchronizedSAPs.size();
     	system.debug('%%% synchronizedSize: ' + synchronizedSize);

    	//system.assertEquals(synchronizedSize, (prevSyncSize + prevToSyncSize));
    	
    	/*
Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.LastModifiedDate, r.LastModifiedById, r.IsPersonType, r.IsActive, r.Id, r.DeveloperName, r.Description, r.CreatedDate, r.CreatedById, r.BusinessProcessId From RecordType r where r.SobjectType='Account'*/
		
		//system.assertEquals(2, listOfErr.size());
    }
    
}