public class BI_SAP_EmailUtils {
/** 
 * This class provides functinality in order to process, create and send emails.
 * Date: 18/10/2016
 * @author Nubika Consulting
 * @version 1.0
*/


    
    /**
        When sending emails in apex, we must set the targetObject if we are using an email template. The
        Target object id must be a Contact, Lead or User record id. This becomes a problem when we want
        to use a template but the email is not being send to Contact, Lead or User record, but are sending
        the email to a particular email address. In many instances, developers create dummy contacts to achieve
        this, then delete the contact after email is sent. This approach works, but if the contact object requires
        validations then the programmer has to update dummy the contact insertion code as well. A second approach
        is to use find and replace after querying template body, and set the body in apex. This approach requires
        a lot of code and expertise in writing regex, and is not a good choice from a  maintenance point of view.
        This approach requires developers to update code each time an email template is updated with a new field.
    */
    public static void sendEmailWithTemplate(String templateName, list<String> addresses){
        sendEmailWithTemplate(null, templateName, addresses);
    }

    public static void sendEmailWithTemplate(Id whatID, String templateName, list<String> addresses){
        EmailTemplate eT = [select Id from EmailTemplate where Name =: templateName];
        
        /*
            We begin constructing an email message using a WhatID that refers to a Contact. As long as our
            email template doesn�t use �recipient� merge fields, it doesn�t matter which Contact we pick:
            we�re never going to actually send the email message to that Contact.
        */

        
        Contact cnt = [select id from Contact where email != null limit 1];
        System.debug('contact-->' + cnt);
        List<Messaging.SingleEmailMessage> msgList= new List<Messaging.SingleEmailMessage>();
        
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setTemplateId(eT.Id);
        msg.setSaveAsActivity(false);
        msg.setWhatId(whatID);
        msg.setTargetObjectId(cnt.id);
        msg.setToAddresses(addresses);
        
        msgList.add(msg);
        
        /*
            We set a savepoint, send the email, and then rollback the transaction. The key is to understand
            that Salesforce doesn�t send an email immediately when the sendEmail method is executed. Instead,
            Salesforce waits until the end of the transaction. So if you rollback the transaction, Salesforce
            doesn�t send the email at all.
        */
        
        // Send the emails in a transaction, then roll it back
        
        system.debug('#####msglist: ' + msgList);
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(msgList); // Dummy email send
        Database.rollback(sp); // Email will not send as it is rolled Back
        
        /*
            We don�t need to provide a templateId. Instead, we copied the email body from dummy message. Because
            we are supplying the email body, we don�t have to worry about providing the whatId, emailTemplateID
            and TargetObjectID. You can send your email to whomever you want, leveraging email template contents.
        */
        
        // Send Actual email
        
        List<Messaging.SingleEmailMessage> msgListToBeSend = new List<Messaging.SingleEmailMessage>();
        
        for (Messaging.SingleEmailMessage email : msgList) {
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            emailToSend.setToAddresses(email.getToAddresses());
            emailToSend.setPlainTextBody(email.getPlainTextBody());
            emailToSend.setHTMLBody(email.getHTMLBody());
            emailToSend.setSubject(email.getSubject());
            msgListToBeSend.add(emailToSend);
        }
        system.debug('#######Message to be send: ' + msgListToBeSend);
        Messaging.sendEmail(msgListToBeSend);
    }
    
    /*public static String HtmlToPlainText(String source){
        return source.replaceAll('\\<.*?>','');
    }*/
    
}