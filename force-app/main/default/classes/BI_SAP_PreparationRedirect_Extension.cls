/**
    * @Description Class created to redirect SAP Preparation to Veeva or PLANiT application, base on User Country Code
    *
    * @Author      OmegaCRM
*/
global with sharing class BI_SAP_PreparationRedirect_Extension {
    
    public BI_SAP_PreparationRedirect_Extension(ApexPages.StandardController controller) {}
    
    /**
        * @Description Method called at page load that manages redirection
        *
        * @Return      VisualForce page to prepare the SAP
        *
        * @Author      OmegaCRM
    */
    public PageReference init(){

        String param_id = ApexPages.currentPage().getParameters().get('id');
        String user_country_code = [SELECT Id, Country_code_bi__c FROM User WHERE id = :Userinfo.getUserId() LIMIT 1].Country_code_bi__c;
        List<BI_SAP_Country_Settings__c> country_settings = new List<BI_SAP_Country_Settings__c>([SELECT Country_Code__c, Planit_Pilot__c FROM BI_SAP_Country_Settings__c WHERE Country_Code__c =: user_country_code]);   

        if(country_settings == null || country_settings.isEmpty() || country_settings.get(0).Planit_Pilot__c == false) {
            PageReference p = new PageReference('/apex/VEEVA_BI_SAP_Preparation?id='+param_id);
            return p;
        }else{
            PageReference p = new PageReference('/apex/BI_SAP_Preparation?id='+param_id);
            return p;
        }
    }
}