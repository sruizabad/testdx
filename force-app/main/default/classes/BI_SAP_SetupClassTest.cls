/** 
 * SetUp Test Class.
 * Date: 12/22/2016
 * @author Omega CRM Consulting
 * @version 1.0
*/
@isTest
public with sharing class BI_SAP_SetupClassTest {
    
    private static set<String> customerFirstNames = new set<String>{'a','b'};
    public static set<String> names = new set<String>();
    public static list<Account> lAccounts = new list<Account>();
    public static list<Address_vod__c> lAddresses = new list<Address_vod__c>();
    public static list<Affiliation_Vod__c> lAffiliations = new list<Affiliation_Vod__c>();
    public static list<SAP_Target_Preparation_BI__c> lTargets = new list<SAP_Target_Preparation_BI__c>();
    private static String countryCode;
    private static list<Product_vod__c> lProducts = new list<Product_vod__c>();
    public static list<SAP_Detail_Preparation_BI__c> lDetails = new list<SAP_Detail_Preparation_BI__c>();
    private static Customer_Attribute_BI__c customerAttr;
    private static SAP_Cycle_BI__c SAPCycle;
    private static list<SAP_Preparation_BI__c> lSAPs = new list<SAP_Preparation_BI__c>();
    private static set<String> territories = new set<String>();
    private static set<String> states = new set<String>{'Approved', 'Under Review'};
    private static list<BI_SAP_Business_rule__c> lBR = new list<BI_SAP_Business_rule__c>();
    private static list<User> lSalesUsers = new list<User>();
    private static list<User> listSMUsers = new list<User>();
    private static User adminUser = new User();
    private static User dataStewardUser = new User();
    
    // Method for data initializing
    public static void setupTestData(){
        
        User u = [SELECT Id, Country_Code_BI__c FROM User WHERE Id =: UserInfo.getUserId()];
        
        BI_SAP_SetupClassTest.countryCode = u.Country_Code_BI__c;
                        
        BI_SAP_Country_Settings__c biSapCS = new BI_SAP_Country_Settings__c(
            Name = 'testSAP',
            Country_Code__c = BI_SAP_SetupClassTest.countryCode,
            Planit_Pilot__c = true
        );
        
        insert biSapCS;
        
        system.debug('%%% createSAPCycle');
        createSAPCycle();
        
        territories.add('sprep');
        territories.add('SFF-1990');
        
        list<String> lTerritories = new list<String>(territories);
        list<String> lStates = new list<String>(states);
        
        for(String territory: lTerritories){
            for(String state: lStates){
                system.debug('%%% createSAP for territory: ' + territory);
                system.debug('%%% createSAP for state: ' + state);
                BI_SAP_SetupClassTest.createSAP(territory, state);
            }
        }
        
        insert lSAPs;
        
        system.debug('%%% createAccounts');
        BI_SAP_SetupClassTest.createAccounts(20);
        insert lAccounts;
        
        createAddresses();
        insert lAddresses;
        
        createAffiliations();
        insert lAffiliations;
        
        for(SAP_Preparation_BI__c SAP: lSAPs){
            system.debug('%%% createTargets for SAP: ' + SAP.Name);
            BI_SAP_SetupClassTest.createTargets(SAP);
            createAccTerrLoader(SAP);
        }
        
        insert lTargets;
        
        system.debug('%%% createAllDetails');
        BI_SAP_SetupClassTest.createAllDetails();
        
        insert lDetails;
        
        BI_SAP_SetupClassTest.generateBusinessRules();
        BI_SAP_SetupClassTest.createSysAdminUser();
        BI_SAP_SetupClassTest.createDataStewardUser();
        BI_SAP_SetupClassTest.createSalesManagerUsers();
        BI_SAP_SetupClassTest.createSalesUsers();
        //BI_SAP_SetupClassTest.createTerritoriesHierarchy();
    }
    
    
    /************************
    ** SAP Cycles Creation **
    *************************/
    
    private static void createSAPCycle(){
        Date current = Date.today();
        Date firstDate = current.toStartofMonth();
        Date lastDate = current.addMonths(1).toStartofMonth().addDays(-1);
        
        Date firstDate2 = current.addMonths(1).toStartofMonth();
        Date lastDate2 = current.addMonths(2).toStartofMonth().addDays(-1);
        
        SAPCycle = new SAP_Cycle_BI__c(
                Cycle_1_Start_Date_BI__c = firstDate, Cycle_1_End_Date_BI__c =lastDate,
                Cycle_2_Start_Date_BI__c = firstDate2, Cycle_2_End_Date_BI__c = lastDate2/*,
                Cycle_3_Start_Date_BI__c, Cycle_3_End_Date_BI__c,
                Cycle_4_Start_Date_BI__c, Cycle_4_End_Date_BI__c,
                Cycle_5_Start_Date_BI__c, Cycle_5_End_Date_BI__c,
                Cycle_6_Start_Date_BI__c, Cycle_6_End_Date_BI__c,
                Cycle_7_Start_Date_BI__c, Cycle_7_End_Date_BI__c,
                Cycle_8_Start_Date_BI__c, Cycle_8_End_Date_BI__c,
                Cycle_9_Start_Date_BI__c, Cycle_9_End_Date_BI__c,
                Cycle_10_Start_Date_BI__c, Cycle_10_End_Date_BI__c,
                Cycle_11_Start_Date_BI__c, Cycle_11_End_Date_BI__c,
                Cycle_12_Start_Date_BI__c, Cycle_12_End_Date_BI__c*/
        );
        
        insert SAPCycle;
    }
    
    
    /******************
    ** SAPs Creation **
    *******************/
    
    private static void createSAP(String territory, String status) {
                        
        system.debug('%%% createPrep STARTED');
        SAP_Preparation_BI__c SAP = new SAP_Preparation_BI__c();
        SAP.OwnerId = UserInfo.getUserId();
        SAP.Country_Code_BI__c = BI_SAP_SetupClassTest.countryCode;
        SAP.Territory_BI__c = territory;
        SAP.Status_BI__c = status;
        SAP.Start_Date_BI__c = SAPCycle.Cycle_1_Start_Date_BI__c;
        SAP.End_Date_BI__c = SAPCycle.Cycle_1_End_Date_BI__c;
        
        
        
        /*RecordType rT = [SELECT Id, Name FROM RecordType WHERE Id = '012K00000000WiwIAE'];
        system.debug('%%% rT: ' + rT);*/
        
        lSAPs.add(SAP);
    }
    
    
    /**********************
    ** Accounts Creation **
    ***********************/
    
    private static void createAccounts(Integer length){
        
        Knipper_Settings__c ks = Knipper_Settings__c.getInstance();
        system.debug('%%% ks: ' + ks.Id);
        ks.Account_Detect_Change_FieldList__c = 'FirstName,Middle_vod__c,LastName,ME__c,External_ID_vod__c,Specialty_1_vod__c,Credentials_vod__c,PersonEmail';
        ks.Account_Detect_Changes_Record_Type_List__c = 'Professional_vod,KOL_vod,Staff';
        ks.External_ID__c = '1';
        upsert ks;
        
        customerAttr = new Customer_Attribute_BI__c(
            Name = 'Practice Manager',
            Type_BI__c = 'CHILD_Role',
            Country_Code_BI__c = BI_SAP_SetupClassTest.countryCode,
            RecordTypeId = '012K00000000WiwIAE'
        );
        
        system.debug('%%% insert customerAttr');
        insert customerAttr;
        
        for(Integer i=0; i<length; i++){
            String name = generateRandomString(5, BI_SAP_SetupClassTest.names);
            system.debug('%%% name: ' + name);
            createAccount(name);
        }
    }
    
    // SELECT Id, Credentials_vod__c, Specialty_1_vod__c, Status_BI__c, Specialty_BI__c FROM Account WHERE Id = '001K000000fMrY0'
    //Select Id, Name FROM Customer_Attribute_BI__c WHERE Id = 'a2WK0000000AM2x'
    private static Account createAccount(String accName){
        
        // Fake account
        Account fAcc = new Account();
        
        // Get random values
        list<String> lCredentials = getPickListValues(fAcc, 'Credentials_vod__c');
        String credentials = lCredentials.get(Math.mod(Math.abs(Crypto.getRandomInteger()), lCredentials.size()));
        system.debug('%%% credentials: ' + credentials);
        
        list<String> lSpecialty1 = getPickListValues(fAcc, 'Specialty_1_vod__c');
        String specialty1 = lSpecialty1.get(Math.mod(Math.abs(Crypto.getRandomInteger()), lSpecialty1.size()));
        system.debug('%%% specialty1: ' + specialty1);
        
        Account acc = new Account(
            Name = accName,
            Credentials_vod__c = credentials,
            Specialty_1_vod__c = specialty1,
            Status_BI__c = 'Active',
            Specialty_BI__c = customerAttr.Id,
            Country_code_BI__c = BI_SAP_SetupClassTest.countryCode//,
            /*PersonMobilePhone = '(345) 555-1111',
            Phone = '90901188',
            Fax = '(345) 555-2222',*/
            //External_ID_vod__c = 'test0'
        );
        
        system.debug('%%% insert acc');
        
        BI_SAP_SetupClassTest.lAccounts.add(acc);
        
        return acc;
    }
    
    
    /**********************
    ** Addreses Creation **
    ***********************/
    
    private static void createAddresses(){
    
        for(Account acc: BI_SAP_SetupClassTest.lAccounts){
            
            Integer flag = Math.mod(Math.abs(Crypto.getRandomInteger()), 2);
            system.debug('%%% TRY');
            if(flag==1){
                system.debug('%%% OK');
                Address_vod__c address = new Address_vod__c(
                    Account_vod__c = acc.Id,
                    Name = '11 High Street',
                    Zip_vod__c = 'LS25',
                    City_vod__c = 'Leeds',
                    Primary_vod__c = true
                );
                
                BI_SAP_SetupClassTest.lAddresses.add(address);
            }else{
                system.debug('%%% NOT');
            }
        }
    }
    
    
    /**************************
    ** Affiliations Creation **
    ***************************/
    
    private static void createAffiliations(){
    
        Affiliation_Vod__c aff = new Affiliation_Vod__c(
            From_Account_vod__c = BI_SAP_SetupClassTest.lAccounts[0].Id,
            To_Account_vod__c = BI_SAP_SetupClassTest.lAccounts[1].Id,
            Influence_vod__c = 'Standard',
            Role_vod__c = 'VP Marketing'
        );
        
        BI_SAP_SetupClassTest.lAffiliations.add(aff);
        
    }
    
    
    /**************************************
    ** Account Territory Loader Creation **
    ***************************************/
    
    private static void createAccTerrLoader(SAP_Preparation_BI__c SAP){
        list<Account> lNewAcc = new list<Account>();
        
        for(Integer i=0; i<10; i++){
            String name = generateRandomString(5, names);
            system.debug('%%% name: ' + name);
            lNewAcc.add(createAccount(name));
        }
        
        insert lNewAcc;
        
        list<Account_Territory_Loader_vod__c> lAccTerrLoader = new list<Account_Territory_Loader_vod__c>();
        
        for(Account acc: lNewAcc){
            Account_Territory_Loader_vod__c accTerrLoader = new Account_Territory_Loader_vod__c(
                Account_vod__c = acc.Id,
                Territory_vod__c = SAP.Territory_BI__c,
                External_ID_vod__c = acc.Id + '_' + SAP.Territory_BI__c
            );
            
            lAccTerrLoader.add(accTerrLoader);
        }
        
        insert lAccTerrLoader;
        //Account_Territory_Loader_vod__c
    }
    
    
    /*********************
    ** Targets Creation **
    **********************/
    
    private static void createTargets(SAP_Preparation_BI__c SAP){
        system.debug('%%% lAccounts: ' + lAccounts);
        
        for(Account acc: lAccounts){
            
            Integer prob = Math.mod(Math.abs(Crypto.getRandomInteger()), 100);
            Boolean reviewed = false;
            Boolean rejected = false;
            if(prob<50){
                reviewed = true;
            }else{
                rejected = true;
            }
            
            Integer plannedInt = Math.mod(Math.abs(Crypto.getRandomInteger()), 20);
            Integer adjustedInt = 0;
            if(plannedInt>0){
                adjustedInt = Math.mod(Math.abs(Crypto.getRandomInteger()), plannedInt);
                if(Math.mod(Math.abs(Crypto.getRandomInteger()), 2)==0){
                    adjustedInt = adjustedInt * (-1);
                }
            }
            
            SAP_Target_Preparation_BI__c target = new SAP_Target_Preparation_BI__c(
                    Target_Customer_BI__c = acc.Id,
                    Planned_Interactions_BI__c = plannedInt,
                    Adjusted_Interactions_BI__c = adjustedInt,
                    NTL_Value_BI__c = 15500,
                    SAP_Header_BI__c = SAP.id,
                    Country_Code_BI__c = BI_SAP_SetupClassTest.countryCode,
                    External_ID_BI__c = acc.Id + '_' + SAP.Territory_BI__c + '_' + SAP.Id,
                    Rejected_BI__c = rejected,
                    Reviewed_BI__c = reviewed
            );
            
            lTargets.add(target);
        }
                
    }
    
    
    /*********************
    ** Details Creation **
    **********************/
    
    private static void createAllDetails(){
        createProductCatalog();
        
        for(SAP_Target_Preparation_BI__c target: lTargets){
            createDetailsByTarget(target);
        }
        
        system.debug('%%% lDetails: ' + lDetails);
    }
    
    private static void createDetailsByTarget(SAP_Target_Preparation_BI__c target){
        
        // This is the number of products that we are going to assign to the current target
        Integer numOfProducts = Math.mod(Math.abs(Crypto.getRandomInteger()), BI_SAP_SetupClassTest.lProducts.size()) + 1;
        set<Integer> indexes = new set<Integer>();
        for(Integer i=0; i<numOfProducts; i++){
            while(indexes.size()<=i){
                Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), BI_SAP_SetupClassTest.lProducts.size());
                indexes.add(idx);
            }
        }
        
        system.debug('%%% indexes: ' + indexes);
        
        for(Integer idx: indexes){
            // fake detail
            SAP_Detail_Preparation_BI__c fDetail = new SAP_Detail_Preparation_BI__c();
            
            list<String> lSegments = getPickListValues(fDetail, 'Segment_BI__c');
            String segment = lSegments.get(Math.mod(Math.abs(Crypto.getRandomInteger()), lSegments.size()));
            
            Integer plannedInt = Math.mod(Math.abs(Crypto.getRandomInteger()), 20);
            Integer adjustedInt = 0;
            if(plannedInt>0){
                adjustedInt = Math.mod(Math.abs(Crypto.getRandomInteger()), plannedInt);
                if(Math.mod(Math.abs(Crypto.getRandomInteger()), 2)==0){
                    adjustedInt = adjustedInt * (-1);
                }
            }
                        
            SAP_Detail_Preparation_BI__c detail = new SAP_Detail_Preparation_BI__c(
                    Country_Code_BI__c = BI_SAP_SetupClassTest.countryCode,
                    Planned_Details_BI__c = plannedInt,
                    Adjusted_Details_BI__c = adjustedInt,
                    //Column_BI__c = TD.Column_BI__c,
                    Column_BI__c = 6,
                    Product_BI__c = BI_SAP_SetupClassTest.lProducts[idx].Id,
                    Segment_BI__c = segment,
                    //Row_BI__c = TD.Row_BI__c,
                    Row_BI__c = 6,
                    Target_Customer_BI__c = target.Id,
                    External_ID_BI__c = target.External_ID_BI__c + '_' + BI_SAP_SetupClassTest.lProducts[idx].Id
            );
            
            lDetails.add(detail);
        }
        
    }
    
    
    private static void createProductCatalog(){
        
        system.debug('%%% createProductCatalog STARTED');
        
        Product_vod__c p1 = new Product_vod__c ();
        p1.Name = 'EFFORTIL';
        p1.country_code_bi__c = BI_SAP_SetupClassTest.countryCode;
        p1.External_ID_vod__c = 'DEINTID00152';
        p1.manufacturer_vod__c = 'Boehringer Ingelheim Pharma KG';
        p1.product_type_vod__c = 'Detail';
        p1.company_product_vod__c = true;
        p1.sample_quantity_picklist_vod__c = '1';
        
        BI_SAP_SetupClassTest.lProducts.add(p1);
        
        Product_vod__c p2 = new Product_vod__c ();
        p2.Name = 'MICARDIS';
        p2.country_code_bi__c = BI_SAP_SetupClassTest.countryCode;
        p2.External_ID_vod__c = 'B_39';
        p2.manufacturer_vod__c = 'BI';
        p2.product_type_vod__c = 'Detail';
        p2.company_product_vod__c = true;
        
        BI_SAP_SetupClassTest.lProducts.add(p2);
        
        Product_vod__c p3 = new Product_vod__c ();
        p3.Name = 'EFFORTIL PLUS';
        p3.country_code_bi__c = BI_SAP_SetupClassTest.countryCode;
        p3.External_ID_vod__c = 'DEINTID00151';
        p3.manufacturer_vod__c = 'Boehringer Ingelheim Pharma KG';
        p3.product_type_vod__c = 'Detail';
        p3.company_product_vod__c = true;
        p3.sample_quantity_picklist_vod__c = '1';
        
        BI_SAP_SetupClassTest.lProducts.add(p3);
        
        insert BI_SAP_SetupClassTest.lProducts;
        
    }
    
    
    /*******************
    ** Common Methods **
    ********************/
    
    public static String generateRandomString(Integer len, set<String> lAllRandomStrs){
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        Boolean success = false;
        while(success == false){
            system.debug('###### inside');
            while (randStr.length() < len) {
               Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
               randStr += chars.substring(idx, idx+1);
            }
            success = lAllRandomStrs.add(randStr);
        }
        return randStr;
    }
    
    public static list<String> getPickListValues(SObject object_name, String field_name){
        set<String> plValues = new set<String>();
        
        Schema.sObjectType sobject_type = object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        list<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues();
        
        for(Schema.PicklistEntry a: pick_list_values){
            plValues.add(a.getValue());
        }
        return new list<String>(plValues);
    }
    
    /*private static list<Account> createCustomers(){
        list<Account> lCustomers = new list<Account>();
        return lCustomers;
    }
    
    //Main method to create all data for a created SAP.
    private static void createSAPData(Id sap_Id, Integer size){
        list<Account> lCustomers = createCustormers();
        list<SAP_Target_Preparation_BI__c> lTargets = createSAPTargets(sap_Id,lCustomers);
    }
    
    private static list<SAP_Target_Preparation_BI__c> createSAPTargets(Id sap_Id, list<Accounts> lCustomers){
        list<SAP_Target_Preparation_BI__c> lTargets = new list<SAP_Target_Preparation_BI__c>();
        for(Account customer: lCustomers){
        
        }
        return lTargets;
    }
    
    private static void createSAPDetailsToTarget (String target_Id, Integer numProducts){
        
    }*/
    
    private static void generateBusinessRules(){
                
        //Segmentation
        lBR.add(new BI_SAP_Business_rule__c(
            BI_SAP_Criteria__c = 'Gain',
            BI_SAP_Field_Force__c = 'Cardiology',
            BI_SAP_Active__c = true,
            BI_SAP_Country_code__c = BI_SAP_SetupClassTest.countryCode,
            BI_SAP_Min_value__c = 2,
            BI_SAP_Type__c = 'Segmentation',
            BI_SAP_Product__c = lProducts[0].Id
        ));
        lBR.add( new BI_SAP_Business_rule__c(
            BI_SAP_Criteria__c = 'Maintein',
            BI_SAP_Field_Force__c = 'Cardiology',
            BI_SAP_Active__c = true,
            BI_SAP_Country_code__c = BI_SAP_SetupClassTest.countryCode,
            BI_SAP_Max_value__c = 5,
            BI_SAP_Min_value__c = 2,
            BI_SAP_Type__c = 'Segmentation',
            BI_SAP_Product__c = lProducts[0].Id
        ));
        lBR.add(new BI_SAP_Business_rule__c(
            BI_SAP_Criteria__c = 'Defend',
            BI_SAP_Field_Force__c = 'Cardiology',
            BI_SAP_Active__c = true,
            BI_SAP_Country_code__c = BI_SAP_SetupClassTest.countryCode,
            BI_SAP_Min_value__c = 2,
            BI_SAP_Type__c = 'Segmentation',
            BI_SAP_Product__c = lProducts[1].Id
        ));
        lBR.add(new BI_SAP_Business_rule__c(
            BI_SAP_Criteria__c = 'Build',
            BI_SAP_Field_Force__c = 'Cardiology',
            BI_SAP_Active__c = true,
            BI_SAP_Country_code__c = BI_SAP_SetupClassTest.countryCode,
            BI_SAP_Max_value__c = 5,
            BI_SAP_Min_value__c = 2,
            BI_SAP_Type__c = 'Segmentation',
            BI_SAP_Product__c = lProducts[1].Id
        ));
        lBR.add( new BI_SAP_Business_rule__c(
            BI_SAP_Criteria__c = 'Observe',
            BI_SAP_Field_Force__c = 'Cardiology',
            BI_SAP_Active__c = true,
            BI_SAP_Country_code__c = BI_SAP_SetupClassTest.countryCode,
            BI_SAP_Min_value__c = 2,
            BI_SAP_Type__c = 'Segmentation',
            BI_SAP_Product__c = lProducts[2].Id
        ));
        lBR.add( new BI_SAP_Business_rule__c(
            BI_SAP_Criteria__c = 'Gain',
            BI_SAP_Field_Force__c = 'Cardiology',
            BI_SAP_Active__c = true,
            BI_SAP_Country_code__c = BI_SAP_SetupClassTest.countryCode,
            BI_SAP_Max_value__c = 5,
            BI_SAP_Min_value__c = 2,
            BI_SAP_Type__c = 'Segmentation',
            BI_SAP_Product__c = lProducts[2].Id
        ));
        //Specialty
        lBR.add( new BI_SAP_Business_rule__c(
            BI_SAP_Specialty__c = customerAttr.Id,
            BI_SAP_Active__c = true,
            BI_SAP_Country_code__c = BI_SAP_SetupClassTest.countryCode,
            BI_SAP_Type__c = 'Customer Speciality',
            BI_SAP_Product__c = lProducts[0].Id
        ));
        lBR.add( new BI_SAP_Business_rule__c(
            BI_SAP_Specialty__c = customerAttr.Id,
            BI_SAP_Active__c = true,
            BI_SAP_Country_code__c = BI_SAP_SetupClassTest.countryCode,
            BI_SAP_Type__c = 'Customer Speciality',
            BI_SAP_Product__c = lProducts[1].Id
        ));
        
        insert lBR;
    }
    private static void createSysAdminUser(){
    	Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1];
    	
    	adminUser = new User(Alias = 'SAdUser',
    					   Country='Brazil',
    					   Country_code_bi__c='BR',
    					   Email='uATest1@randomdemodomain.com',
    					   EmailEncodingKey='UTF-8', 
    					   LastName='Testing', 
    					   LanguageLocaleKey='en_US',
    					   LocaleSidKey='en_US',
    					   ProfileId = p.Id,
    					   TimeZoneSidKey='America/Los_Angeles',
    					   UserName='uATest1@andomdemodomain.com');
	   insert adminUser;
    }
    
    private static void createDataStewardUser(){
    	Profile p = [SELECT Id FROM Profile WHERE Name = 'BR_DATA_STEWARD' limit 1];
    	    	
    	dataStewardUser = new User(Alias = 'DSUser1',
    					   Country='Brazil',
    					   Country_code_bi__c='BR',
    					   Email='ds@randomdemodomain.com',
    					   EmailEncodingKey='UTF-8', 
    					   LastName='Testing', 
    					   LanguageLocaleKey='en_US',
    					   LocaleSidKey='en_US',
    					   ProfileId = p.Id,
    					   TimeZoneSidKey='America/Los_Angeles',
    					   UserName='ds@andomdemodomain.com');
    					   
		insert dataStewardUser;
    }
    
    private static void createSalesManagerUsers(){
    	Profile p = [SELECT Id FROM Profile WHERE Name = 'BR_SALES_MANAGER' limit 1];
    	
    	listSMUsers = new list<User>();
    	
    	User userSM1 = new User(Alias = 'SMUser1',
    					   Country='Brazil',
    					   Country_code_bi__c='BR',
    					   Email='d1@randomdemodomain.com',
    					   EmailEncodingKey='UTF-8', 
    					   LastName='Testing', 
    					   LanguageLocaleKey='en_US',
    					   LocaleSidKey='en_US',
    					   ProfileId = p.Id,
    					   TimeZoneSidKey='America/Los_Angeles',
    					   UserName='d1@andomdemodomain.com');
    					   
	   User userSM2 = new User(Alias = 'SMUser2',
    					   Country='Brazil',
    					   Country_code_bi__c='BR',
    					   Email='d2@randomdemodomain.com',
    					   EmailEncodingKey='UTF-8', 
    					   LastName='Testing', 
    					   LanguageLocaleKey='en_US',
    					   LocaleSidKey='en_US',
    					   ProfileId = p.Id,
    					   TimeZoneSidKey='America/Los_Angeles',
    					   UserName='d2@andomdemodomain.com');
    					   
    	listSMUsers.add(userSM1);	
    	listSMUsers.add(userSM2);		
    		   
		insert listSMUsers;
    }
    
   private static void createSalesUsers(){
    	Profile p = [SELECT Id FROM Profile WHERE Name = 'BR_SALES' limit 1];
    	
    	lSalesUsers = new list<User>();
    	
    	User u1 = new User(Alias = 'SRUser1',
    					   Country='Brazil',
    					   Country_code_bi__c='BR',
    					   Email='demo2@randomdemodomain.com',
    					   EmailEncodingKey='UTF-8', 
    					   LastName='Testing', 
    					   LanguageLocaleKey='en_US',
    					   LocaleSidKey='en_US',
    					   ProfileId = p.Id,
    					   TimeZoneSidKey='America/Los_Angeles',
    					   UserName='demo2@andomdemodomain.com');
    					   
  		User u2 = new User(Alias = 'SRUser2',
    					   Country='Brazil',
    					   Country_code_bi__c='BR',
    					   Email='demo3@randomdemodomain.com',
    					   EmailEncodingKey='UTF-8', 
    					   LastName='Testing', 
    					   LanguageLocaleKey='en_US',
    					   LocaleSidKey='en_US',
    					   ProfileId = p.Id,
    					   TimeZoneSidKey='America/Los_Angeles',
    					   UserName='demo3@andomdemodomain.com');
		lSalesUsers.add(u1);
		lSalesUsers.add(u2);
		
		insert lSalesUsers;
		
    }
    
    /*private static void createTerritoriesHierarchy(){
    	system.runAs(adminUser){
	    	//Territory names
	    	set<String> SRTerritoryNames = new set<String>{'SRTerritory1','SRTerritory2'};
	    	set<String> SMTerritoryNames = new set<String>{'SMTerritory1','SMTerritory2','SMTerritory3'};
	    	set<String> DSTerritoryNames = new set<String>{'DSTerritory1'};
	    	set<String> territoriesToInsertNames = new set<String>();
	    	territoriesToInsertNames.addAll(SRTerritoryNames);
	    	territoriesToInsertNames.addAll(SMTerritoryNames);
	    	territoriesToInsertNames.addAll(DSTerritoryNames);
	    	
	    	list<Territory> SRTerritories = new list<Territory>();
	    	for(String tName: SRTerritoryNames){
	    		SRTerritories.add(new Territory(Name=tName));
	    	}
	    	list<Territory> SMTerritories = new list<Territory>();
	    	for(String tName: SMTerritoryNames){
	    		SMTerritories.add(new Territory(Name=tName));
	    	}
	    	list<Territory> DSTerritories = new list<Territory>();
	    	for(String tName: DSTerritoryNames){
	    		DSTerritories.add(new Territory(Name=tName));
	    	}
	    	
	    	insert DSTerritories;
	    	SMTerritories.get(0).ParentTerritoryId = DSTerritories.get(0).Id;
	    	SMTerritories.get(1).ParentTerritoryId = DSTerritories.get(0).Id;
	    	SMTerritories.get(2).ParentTerritoryId = DSTerritories.get(0).Id;
	    	insert SMTerritories;
	    	
	    	SRTerritories.get(0).ParentTerritoryId = SMTerritories.get(0).Id;
	    	SRTerritories.get(1).ParentTerritoryId = SMTerritories.get(1).Id;
	    	insert SRTerritories;
	    	
	    	system.debug('##### SRTerritories: ' + SRTerritories);
	    	system.debug('##### SMTerritories: ' + SMTerritories);
	    	system.debug('##### DSTerritories: ' + DSTerritories);
    	}
    }*/

}