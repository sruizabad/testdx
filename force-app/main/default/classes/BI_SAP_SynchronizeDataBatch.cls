global class BI_SAP_SynchronizeDataBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global set<Id> SAPsToSyncIds = new set<Id>();
    // original SAPs that have been synchronized
    global set<Id> synchronizedSAPsIds = new set<Id>();
    global list<SAP_Preparation_BI__c> lTotalSAPsToSync = new list<SAP_Preparation_BI__c>();
    global String userCountryCode = '';
    global String SAPName = '';
    // This map will contain the account Id which target is deleted and reviewed as key,
    // and the details assigned to that account that already exist on Product_Metrics_Vod__c
    //global map<Id, set<Id>> accountIdToProductIdsMap = new map<Id, set<Id>>();
    
    global BI_SAP_SynchronizeDataBatch(list<SAP_Preparation_BI__c> SAPsToSync, String SAPName){
        for(SAP_Preparation_BI__c SAP: SAPsToSync){
            SAPsToSyncIds.add(SAP.Id);
        }
        system.debug('%%% SAPsToSyncIds: ' + SAPsToSyncIds);
        this.lTotalSAPsToSync = SAPsToSync;
        this.SAPName = SAPName;
    }
    
    /**
     * Start method
     */
    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug('%%% START');
        
        //setAccountIdToProductIdsMap();
        //system.debug('%%% accountIdToProductIdsMap.keySet(): ' + accountIdToProductIdsMap.keySet());
        //system.debug('%%% accountIdToProductIdsMap.values(): ' + accountIdToProductIdsMap.values());
        
        return Database.getQueryLocator([SELECT Id, Name, Country_Code_BI__c, Territory_BI__c, Status_BI__c, Start_Date_BI__c,
                                                End_Date_BI__c, OwnerId, External_ID_BI__c
                                        FROM SAP_Preparation_BI__c WHERE Id in :SAPsToSyncIds]);
    }
    
    /**
     * Batch Execute method
     */
    global void execute(Database.BatchableContext BC, list<SAP_Preparation_BI__c> lSAPsToSync){
        system.debug('%%% EXECUTE');
        synchronizeSAPs(lSAPsToSync);
    }
    
    /**
     * Batch Finish method
     */
    global void finish(Database.BatchableContext BC){
        system.debug('%%% FINISHED');
        system.debug('%%% synchronizedSAPsIds.size(): ' + synchronizedSAPsIds.size());
        system.debug('%%% SAPsToSyncIds.size(): ' + SAPsToSyncIds.size());
        if(synchronizedSAPsIds.size()<SAPsToSyncIds.size()){
            list<SAP_Preparation_BI__c> remainingSAPsToSync = new list<SAP_Preparation_BI__c>();
            system.debug('%%% lTotalSAPsToSync: ' + lTotalSAPsToSync);
            for(SAP_Preparation_BI__c SAP: lTotalSAPsToSync){
                system.debug('%%% SAP.Id: ' + SAP.Id);
                if(!synchronizedSAPsIds.contains(SAP.Id)){
                    remainingSAPsToSync.add(SAP);
                }
            }
            system.debug('%%% remainingSAPsToSync: ' + remainingSAPsToSync);
            system.debug('%%% remainingSAPsToSync.size(): ' + remainingSAPsToSync.size());
            BI_SAP_SynchronizeDataBatch syncDataBatch = new BI_SAP_SynchronizeDataBatch(remainingSAPsToSync, SAPName);
            Id batchJobId = Database.executeBatch(syncDataBatch, 10);
            system.debug('%%% batchJobId: ' + batchJobId);
        }else{
            
            if(synchronizedSAPsIds.size()>0){
                BI_SAP_EmailUtils.sendEmailWithTemplate(new list<Id>(synchronizedSAPsIds)[0], 'SAP Synchronize Notification', new list<String>{UserInfo.getUserEmail()});
            }
            /*system.debug('%%% REMOVE PORD METRICS');
            
            list<Product_Metrics_Vod__c> lProdMet = [SELECT Id, Account_vod__c, Products_Vod__c FROM Product_Metrics_Vod__c 
                                                                WHERE Account_vod__c NOT IN : (new list<Id>(accountIdToProductIdsMap.keySet()))];
            
            list<Product_Metrics_Vod__c> lProdMetToDelete = new list<Product_Metrics_Vod__c>();
            for(Product_Metrics_Vod__c prodMetric: lProdMet){
                set<Id> detailsIds = accountIdToProductIdsMap.get(prodMetric.Account_vod__c);
                if(detailsIds!=null && detailsIds.contains(prodMetric.Products_Vod__c)){
                    lProdMetToDelete.add(prodMetric);
                }
            }
            
            system.debug('%%% lProdMetToDelete: ' + lProdMetToDelete);
            delete lProdMetToDelete;*/
        }
    }
    
    public void synchronizeSAPs(list<SAP_Preparation_BI__c> lSAPsToSync){
        list<Cycle_Plan_vod__c> SAPs = new list<Cycle_Plan_vod__c>();
        list<Cycle_Plan_Target_vod__c> targets = new list<Cycle_Plan_Target_vod__c>();
        list<Cycle_Plan_Detail_vod__c> details = new list<Cycle_Plan_Detail_vod__c>();
        
        map<String, list<Cycle_Plan_Target_vod__c>> SAPsToTargetsMap = new map<String, list<Cycle_Plan_Target_vod__c>>();
        map<String, list<Cycle_Plan_Detail_vod__c>> targetsToDetailsMap = new map<String, list<Cycle_Plan_Detail_vod__c>>();
        
        for(SAP_Preparation_BI__c SAP: lSAPsToSync){
            system.debug('%%% SYNC SAP');
            synchronizeSAP(SAP, SAPs, SAPsToTargetsMap, targetsToDetailsMap);
        }
        
        // UPSERT SAPS
        Database.UpsertResult[] SAPsResults = Database.upsert(SAPs, Cycle_Plan_vod__c.Fields.External_ID2__c, false);
        for(Database.UpsertResult result: SAPsResults){
            if(!result.isSuccess()){
                system.debug('%%% SAP ERROR: ' + result.getErrors());
            }
        }
        
        // Set 'Cycle_Plan_vod__c' field for each target
        for(Cycle_Plan_vod__c SAP: SAPs){
            if(SAP.Id!=null){
                for(Cycle_Plan_Target_vod__c target: SAPsToTargetsMap.get(SAP.External_ID2__c)){
                    target.Cycle_Plan_vod__c = SAP.Id;
                    targets.add(target);
                }
            }
        }
        
        
        // UPSERT TARGETS
        Database.UpsertResult[] targetsResults = Database.upsert(targets, Cycle_Plan_Target_vod__c.Fields.External_ID2__c, false);
        for(Database.UpsertResult result: targetsResults){
            if(!result.isSuccess()){
                system.debug('%%% target ERROR: ' + result.getErrors());
            }
        }
        
        // Set 'Cycle_Plan_Target_vod__c' field for each detail
        for(Cycle_Plan_Target_vod__c target: targets){
            if(target.Id!=null){
                //system.debug('%%% target.External_ID2__c: ' + target.External_ID2__c);
                //system.debug('%%% targetsToDetailsMap.get(): ' + targetsToDetailsMap.get(target.External_ID2__c));
                if(targetsToDetailsMap.get(target.External_ID2__c)!=null){
                    for(Cycle_Plan_Detail_vod__c detail: targetsToDetailsMap.get(target.External_ID2__c)){
                        detail.Cycle_Plan_Target_vod__c = target.Id;
                        details.add(detail);
                    }
                }
            }
        }
        
        
        // UPSERT DETAILS
        Database.UpsertResult[] detailsResults = Database.upsert(details, Cycle_Plan_Detail_vod__c.Fields.External_ID__c, false);
        for(Database.UpsertResult result: detailsResults){
            if(!result.isSuccess()){
                system.debug('%%% detail ERROR: ' + result.getErrors());
            }
        }
    }
     
    private void synchronizeSAP(SAP_Preparation_BI__c SAP,
                                list<Cycle_Plan_vod__c> SAPs,
                                map<String, list<Cycle_Plan_Target_vod__c>> SAPsToTargetsMap,
                                map<String, list<Cycle_Plan_Detail_vod__c>> targetsToDetailsMap){
                                    
        system.debug('%%% createSAP STARTED for ' + SAP.Id);
        synchronizedSAPsIds.add(SAP.Id);
    
        try {
            
            /* SAP Synchronization */
            
            String auxName = '';
            auxName = SAP.Territory_BI__c + '_' + SAP.Start_Date_BI__c.format();
            system.debug('%%% auxName: ' + auxName);
            
            Date startDateVod = SAP.Start_Date_BI__c;
            startDateVod.format();
            Date endDateVod = SAP.End_Date_BI__c;
            endDateVod.format();
            String territory = SAP.Territory_BI__c;
            
            //IF 0 is the Adjusted value then don't take the line
            //Create the CP, CP target, CP detail based on the Adjusted fields
            Cycle_Plan_vod__c CP = new Cycle_Plan_vod__c(Name = (SAPName!='') ? SAPName : auxName,
                                                        Start_Date_vod__c = startDateVod,
                                                        End_Date_vod__c = endDateVod,
                                                        Territory_vod__c = territory,
                                                        Status_vod__c = 'Approved_vod',
                                                        OwnerId = SAP.OwnerId,
                                                        Country_Code_BI__c = SAP.Country_Code_BI__c
            );
            
            String startDay = (SAP.Start_Date_BI__c.day()<10) ? ('0' + SAP.Start_Date_BI__c.day()) : ('' + SAP.Start_Date_BI__c.day());
            String startMonth = (SAP.Start_Date_BI__c.month()<10) ? ('0' + SAP.Start_Date_BI__c.month()) : ('' + SAP.Start_Date_BI__c.month());
            
            String endDay = (SAP.End_Date_BI__c.day()<10) ? ('0' + SAP.End_Date_BI__c.day()) : ('' + SAP.End_Date_BI__c.day());
            String endMonth = (SAP.End_Date_BI__c.month()<10) ? ('0' + SAP.End_Date_BI__c.month()) : ('' + SAP.End_Date_BI__c.month());
            
            String startDate = '' + SAP.Start_Date_BI__c.year() + startMonth + startDay;
            String endDate = '' + SAP.End_Date_BI__c.year() + endMonth + endDay;
            
            CP.External_ID2__c = SAP.Country_Code_BI__c + '-' + startDate + '-' + endDate + '-' + SAP.Territory_BI__c;
            
            SAPs.add(CP);
            
            /* Targets Synchronization */
            
            //go through target preps
            map<Id, Cycle_Plan_Target_vod__c> CPTmap = new map<Id, Cycle_Plan_Target_vod__c>();
            map<Id, SAP_Target_Preparation_BI__c> targetsMap = new map<Id, SAP_Target_Preparation_BI__c>();
            for (SAP_Target_Preparation_BI__c TP : [SELECT Id,
                                                        Target_Customer_BI__c,
                                                        Target_Customer_BI__r.Specialty_BI__c,
                                                        Target_Customer_BI__r.External_ID_vod__c,
                                                        Adjusted_Interactions_BI__c,
                                                        Planned_Interactions_BI__c,
                                                        Total_Planned_Details_BI__c,
                                                        Maximum_Adjusted_Interactions_BI__c,
                                                        Maximum_Planned_Interactions_BI__c,
                                                        Rejected_BI__c,
                                                        Reviewed_BI__c,
                                                        Removed_BI__c,
                                                        Edited_BI__c,
                                                        Added_Manually_BI__c,
                                                        SAP_Header_BI__r.Start_Date_BI__c,
                                                        SAP_Header_BI__r.End_Date_BI__c,
                                                        SAP_Header_BI__r.Territory_BI__c,
                                                        Country_Code_BI__c
                                                    FROM SAP_Target_Preparation_BI__c
                                                    WHERE SAP_Header_BI__c = :SAP.Id
                                                    AND Adjusted_Interactions_BI__c != null] ) {
                
                system.debug('%%% TP1: ' + TP);
                //system.debug('%%% TP.Rejected_BI__c: ' + TP.Rejected_BI__c);
                //system.debug('%%% TP.Reviewed_BI__c: ' + TP.Reviewed_BI__c);
                
                Boolean syncIt = true;
                Decimal planned = 0;
                if(TP.Maximum_Planned_Interactions_BI__c!=null)
                    planned = TP.Maximum_Planned_Interactions_BI__c;
                
                if(TP.Reviewed_BI__c){
                    if(TP.Removed_BI__c){
                        syncIt = false;
                    }else if(TP.Edited_BI__c){
                        planned = TP.Maximum_Adjusted_Interactions_BI__c;
                    }
                }else if(TP.Added_Manually_BI__c){
                    if(TP.Reviewed_BI__c){
                        planned = TP.Maximum_Adjusted_Interactions_BI__c;
                    }else{
                        syncIt = false;
                    }
                }
                
                if(syncIt){
                    Cycle_Plan_Target_vod__c CPT = new Cycle_Plan_Target_vod__c(
                        //Cycle_Plan_vod__c = CP.id,
                        Cycle_Plan_Account_vod__c = TP.Target_Customer_BI__c,
                        Planned_Calls_vod__c = planned,
                        Country_Code_BI__c = TP.Country_Code_BI__c
                    );
                    
                    CPT.External_ID2__c = CP.External_ID2__c + '-' + TP.Target_Customer_BI__r.External_ID_vod__c;
                    
                    
                    system.debug('%%% put new target: ' + CPT);
                    CPTmap.put(TP.id, CPT);
                    targetsMap.put(TP.id, TP);
                }
            }
            
            system.debug('%%% CPTs values: ' + CPTmap.values());
            SAPsToTargetsMap.put(CP.External_ID2__c, CPTmap.values());
            system.debug('%%% CPTs keys: ' + CPTmap.keySet());
            system.debug('%%% targetsMap: ' + targetsMap.keySet());
            
            
            /* Details Synchronization */
            
            list<Cycle_Plan_Detail_vod__c> CPDs = new list<Cycle_Plan_Detail_vod__c>();
            //list<Product_Metrics_Vod__c> lProdMetrics = new list<Product_Metrics_Vod__c>();
            set<String> externalIds = new set<String>();
            
            for (SAP_Detail_Preparation_BI__c DP : [SELECT Id, Product_BI__c, Planned_Details_BI__c, Adjusted_Details_BI__c, Target_Customer_BI__c,
                                                            Segment_BI__c, Column_BI__c, Row_BI__c, Product_BI__r.External_ID_vod__c,
                                                            Target_Customer_BI__r.Target_Customer_BI__r.External_ID_vod__c,
                                                            Target_Customer_BI__r.Target_Customer_BI__c, Added_Manually_BI__c,
                                                            Country_Code_BI__c
                                                    FROM SAP_Detail_Preparation_BI__c
                                                    WHERE Target_Customer_BI__c in :CPTmap.keySet()] ) {
                
                SAP_Target_Preparation_BI__c TP = targetsMap.get(DP.Target_Customer_BI__c);
                system.debug('%%% DP.Target_Customer_BI__c.External_ID2__c: ' + targetsMap.get(DP.Target_Customer_BI__c));
                //system.debug('%%% TP2: ' + TP);
                
                if(TP!=null){
                    Boolean syncIt = true;
                    Decimal planned = 0;
                    if(DP.Planned_Details_BI__c!=null)
                        planned = DP.Planned_Details_BI__c;
                    
                    if(TP.Reviewed_BI__c && !DP.Added_Manually_BI__c){
                        if(TP.Removed_BI__c){
                            syncIt = false;
                        }else if(TP.Edited_BI__c){
                            planned = DP.Adjusted_Details_BI__c;
                        }
                    }else if(DP.Added_Manually_BI__c){
                        if(!TP.Reviewed_BI__c){
                            syncIt = false;
                        }else{
                            if(!TP.Removed_BI__c){
                                planned = DP.Adjusted_Details_BI__c;
                            }else{
                                syncIt = false;
                            }
                        }
                    }
    
                    // This if is just in case...
                    if(syncIt){
                        String potential = String.valueOf(DP.Row_BI__c);
                        String intimacy = String.valueOf(DP.Column_BI__c);
                        
                        Cycle_Plan_Detail_vod__c CPD = new Cycle_Plan_Detail_vod__c(
                            Product_vod__c = DP.Product_BI__c,
                            Planned_Details_vod__c = planned,
                            zvod_PM_Segmentation_BI__c = DP.Segment_BI__c,
                            zvod_PM_Potential_BI__c = potential,
                            zvod_PM_Intimacy_BI__c = intimacy,
                            Country_Code_BI__c = DP.Country_Code_BI__c
                        );
                        
                        CPD.External_ID__c = CPTmap.get(DP.Target_Customer_BI__c).External_ID2__c + '-' + DP.Product_BI__r.External_ID_vod__c;
                        
                        System.debug('%%% CPD: ' + CPD);
                        //System.debug('%%% DP.Country_Code_BI__c: ' + DP.Country_Code_BI__c);
                        CPDs.add(CPD);
                        
                        // Process final maps
                        list<Cycle_Plan_Detail_vod__c> detailsList = targetsToDetailsMap.get(CPTmap.get(DP.Target_Customer_BI__c).External_ID2__c);
                        //system.debug('%%% detailsList: ' + detailsList);
                        if(detailsList==null){
                            detailsList = new list<Cycle_Plan_Detail_vod__c>();
                            detailsList.add(CPD);
                            targetsToDetailsMap.put(CPTmap.get(DP.Target_Customer_BI__c).External_ID2__c, detailsList);
                            system.debug('%%% targetsToDetailsMap.put(): ' + CPTmap.get(DP.Target_Customer_BI__c).External_ID2__c + ', ' + detailsList);
                        }else{
                            detailsList.add(CPD);
                            system.debug('%%% ADDED');
                        }
                    }
                }
                
                
                //Product_Metrics_Vod__c
                String externalId = SAP.Country_Code_BI__c + '_' + SAP.Territory_BI__c + '_Visit_' + DP.Target_Customer_BI__r.Target_Customer_BI__r.External_ID_vod__c + '_' + DP.Product_BI__r.External_ID_vod__c;
                //system.debug('%%% External ID: "' + externalId + '"');
                /*lProdMetrics.add(new Product_Metrics_Vod__c(
                    Account_vod__c = DP.Target_Customer_BI__r.Target_Customer_BI__c,
                    Country_Code_BI__c = SAP.Country_Code_BI__c,
                    External_Id_Vod__c = externalId,
                    Products_Vod__c = DP.Product_BI__c,
                    Segmentation_BI__c = DP.Segment_BI__c,
                    Rep_Intimacy_BI__c = DP.Column_BI__c,
                    REP_Potential_BI__c = DP.Row_BI__c
                ));
                externalIds.add(externalId);
                
                // Check if the prodMetric is on the deleted targets map
                // in that case we'll remove it from there
                set<Id> detailsIds = accountIdToProductIdsMap.get(DP.Target_Customer_BI__r.Target_Customer_BI__c);
                if(detailsIds!=null && detailsIds.contains(DP.Product_BI__c)){
                    system.debug('%%% Removed ' + DP.Product_BI__c + ' from set for account ' + DP.Target_Customer_BI__r.Target_Customer_BI__c);
                    detailsIds.remove(DP.Product_BI__c);
                }*/
            }
                        
            //system.debug('%%% lProdMetrics.size(): ' + lProdMetrics.size());
            //system.debug('%%% lProdMetrics: ' + lProdMetrics);
            //system.debug('%%% externalIds: ' + externalIds);
            //Database.upsert(lProdMetrics, Product_Metrics_Vod__c.Fields.External_Id_Vod__c, false);
            system.debug('%%% CPDs created: ' + CPDs);
            
        } catch (Exception e) {
            system.debug('Insert unsuccessful.');
            system.debug('%%% Exception: ' + e.getMessage());
            return;
        }
    }
    
    
    
    /*private void setAccountIdToProductIdsMap(){
        list<SAP_Target_Preparation_BI__c> deletedTargets = [SELECT Id, (SELECT Id FROM SAP_Details_Preparation__r)
                                                            FROM SAP_Target_Preparation_BI__c WHERE Id in :SAPsToSyncIds AND Reviewed_BI__c = true
                                                            AND Removed_BI__c = true];
                                                            
        for(SAP_Target_Preparation_BI__c target: deletedTargets){
            set<Id> targetDetails = new set<Id>();
            
            for(SAP_Detail_Preparation_BI__c detail:target.SAP_Details_Preparation__r){
                targetDetails.add(detail.Id);
            }
            
            if(targetDetails.size()>0){
                accountIdToProductIdsMap.put(target.Id, targetDetails);
            }
        }
    }*/
    
}