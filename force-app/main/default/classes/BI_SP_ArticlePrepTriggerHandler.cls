/**
 * Handler for BI_SP_Article_preparation__c Trigger.
 */
public without sharing class BI_SP_ArticlePrepTriggerHandler 
	implements BI_SP_TriggerInterface { 

	List<BI_SP_Preparation__c> preparationsToDelete = new List<BI_SP_Preparation__c>();
	Map<Id,Decimal> mapArticles = new Map<Id, Decimal>();

	/**
	 * Constructs the object.
	 */
	public BI_SP_ArticlePrepTriggerHandler() {}

	/**
	 * bulkBefore
	 *
	 * This method is called prior to execution of a BEFORE trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 * 
	 */
	public void bulkBefore() {}

	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 * 
	 */
	public void bulkAfter() {
		if (Trigger.isDelete) {
			/**
            * Update article Stock when deleting article preparation
            */
			Set<Id> preparationIdsFromArtPrepDeteled = new Set<Id>();
			for(BI_SP_Article_preparation__c ap : ((List<BI_SP_Article_preparation__c>)Trigger.old)){
				preparationIdsFromArtPrepDeteled.add(ap.BI_SP_Preparation__c);

				Decimal total = (ap.BI_SP_Amount_strategy__c != null ? ap.BI_SP_Amount_strategy__c : 0) 
								+ (ap.BI_SP_Amount_target__c != null ? ap.BI_SP_Amount_target__c : 0) 
								+ (ap.BI_SP_Amount_territory__c != null ? ap.BI_SP_Amount_territory__c : 0);

				Decimal quantity = 0;
				if(mapArticles.get(ap.BI_SP_Article__c) != null){
					quantity = mapArticles.get(ap.BI_SP_Article__c);
				}
				quantity += total;
				mapArticles.put(ap.BI_SP_Article__c, quantity);
			}
			/**
            * Update article Stock when deleting article preparation
            */

			List<AggregateResult> groupedResults = new List<AggregateResult>([SELECT BI_SP_Preparation__c FROM BI_SP_Article_preparation__c WHERE BI_SP_Preparation__c IN :preparationIdsFromArtPrepDeteled GROUP BY BI_SP_Preparation__c]);
			Set<Id> existingPreparationIds = new Set<Id>();
			for (AggregateResult ar : groupedResults)  {
			    existingPreparationIds.add(((Id)ar.get('BI_SP_Preparation__c')));
			}

			preparationIdsFromArtPrepDeteled.removeAll(existingPreparationIds);
			preparationsToDelete = new List<BI_SP_Preparation__c>([SELECT Id FROM BI_SP_Preparation__c WHERE Id IN :preparationIdsFromArtPrepDeteled]);
		}
	}

	/**
	 * beforeInsert
	 *
	 * This method is called iteratively for each record to be inserted during a BEFORE
	 * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
	 * 
	 * @param      so    The SObject (BI_SP_Article_preparation__c)
	 * 
	 */
	public void beforeInsert(SObject so) {}

	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
	 * 
	 * @param      oldSo  The old SObject (BI_SP_Article_preparation__c)
	 * @param      so     The SObject (BI_SP_Article_preparation__c)
	 * 
	 */
	public void beforeUpdate(SObject oldSo, SObject so) {}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
	 * 
	 * @param      so    The SObject (BI_SP_Article_preparation__c)
	 * 
	 */
	public void beforeDelete(SObject so) {}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
	 * 
	 * @param      so    The SObject (BI_SP_Article_preparation__c)
	 * 
	 */
	public void afterInsert(SObject so) {}


	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
	 * 
	 * @param      oldSo  The old SObject (BI_SP_Article_preparation__c)
	 * @param      so     The SObject (BI_SP_Article_preparation__c)
	 * 
	 */
	public void afterUpdate(SObject oldSo, SObject so) {}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 * 
	 * @param      so    The SObject (BI_SP_Article_preparation__c)
	 * 
	 */
	public void afterDelete(SObject so) {}

	/**
	 * andFinally
	 *
	 * This method is called once all records have been processed by the trigger. Use this
	 * method to accomplish any final operations such as creation or updates of other records.
	 * 
	 */
	public void andFinally() {
		if (Trigger.isDelete) {
			delete preparationsToDelete;

			/**
            * Update article Stock when deleting article preparation
            */
			List<BI_SP_Article__c> articlesToUpdate = new List<BI_SP_Article__c>();
			for(BI_SP_Article__c art : [SELECT Id, BI_SP_Stock__c FROM BI_SP_Article__c where Id IN :mapArticles.keySet()]){
				Decimal actualStock = art.BI_SP_Stock__c;
				Decimal quantity = mapArticles.get(art.Id);
				art.BI_SP_Stock__c = actualStock + quantity;
				articlesToUpdate.add(art);
			}
			if(articlesToUpdate.size() > 0){
				update articlesToUpdate;
			}
			/**
            * Update article Stock when deleting article preparation
            */
		}
	}
}