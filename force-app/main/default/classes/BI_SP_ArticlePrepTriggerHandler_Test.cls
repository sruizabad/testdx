@isTest
private class BI_SP_ArticlePrepTriggerHandler_Test{

    public static String countryCode;

    @Testsetup
    static void setUp() {
        countryCode = 'BR';
        
        BI_SP_TestDataUtility.createCustomSettings();
        List<Account> accounts = BI_SP_TestDataUtility.createAccounts();
        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());


        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);

    }

    @isTest static void test_ArticlePrepTriggerHandler() {

        list<BI_SP_Article_preparation__c> articlePreparationList = new list<BI_SP_Article_preparation__c>([SELECT Id FROM BI_SP_Article_preparation__c]);

        BI_SP_Article_preparation__c articlePreparation1 =articlePreparationList.get(0);
        articlePreparation1.Name = 'test1';
        update articlePreparation1;
        
        delete articlePreparationList;

    }
}