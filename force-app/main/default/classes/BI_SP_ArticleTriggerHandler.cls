/**
 * Handler for BI_SP_Article__c Trigger.
 */
public without sharing class BI_SP_ArticleTriggerHandler
	implements BI_SP_TriggerInterface {


	Map<String, Id> currentVeevaFamilyMap = new Map<String, Id>();//SAM-48 - Article - product family assigment
	Map<String, String> currentSAMPROCountries = new Map<String, String>();//SAM-48 - Article - product family assigment

	/**
	 * Constructs the object.
	 */
	public BI_SP_ArticleTriggerHandler() {}

	/**
	 * bulkBefore
	 *
	 * This method is called prior to execution of a BEFORE trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 * 
	 */
	public void bulkBefore() {
		if (Trigger.isInsert || Trigger.isUpdate) {
			currentVeevaFamilyMap = getOtherVeevaFamilyForSameProductFamily((List<BI_SP_Article__c>)Trigger.new);//SAM-48 - Article - product family assigment
			currentSAMPROCountries = getCountrySettings((List<BI_SP_Article__c>)Trigger.new);
		}
	}

	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 * 
	 */
	public void bulkAfter() {}

	/**
	 * beforeInsert
	 *
	 * This method is called iteratively for each record to be inserted during a BEFORE
	 * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
	 * 
	 * @param      so    The SObject (BI_SP_Article__c)
	 * 
	 */
	public void beforeInsert(SObject so) {
		BI_SP_Article__c article = (BI_SP_Article__c)so;  

		/**
		 * SAM-48 - Article - product family assigment
		 */
		if (currentVeevaFamilyMap.containsKey(article.BI_SP_Product_family__c+'_'+article.BI_SP_Raw_country_code__c)) {
			article.BI_SP_Veeva_product_family__c = currentVeevaFamilyMap.get(article.BI_SP_Product_family__c+'_'+article.BI_SP_Raw_country_code__c);
		}

		if (currentSAMPROCountries.containsKey(article.BI_SP_Raw_country_code__c)) {
			article.BI_SP_Country_code__c = currentSAMPROCountries.get(article.BI_SP_Raw_country_code__c);
		}		
		/**
		 * END SAM-48 - Article - product family assigment
		 */	

		/**
		 * Article - Stock Control
		 */
		article.BI_SP_Stock_updated__c = false;
		article.BI_SP_Available_stock__c = article.BI_SP_External_stock__c;	
		article.BI_SP_External_stock_updated__c = false;		
		/**
		 * END Article - Stock Control
		 */
	}

	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
	 * 
	 * @param      oldSo  The old SObject (BI_SP_Article__c)
	 * @param      so     The SObject (BI_SP_Article__c)
	 * 
	 */
	public void beforeUpdate(SObject oldSo, SObject so) {
		BI_SP_Article__c article = (BI_SP_Article__c)so;
		BI_SP_Article__c oldArticle = (BI_SP_Article__c)oldSo;

		/**
		 * SAM-48 - Article - product family assigment
		 */
		if (oldArticle.BI_SP_Product_family__c != article.BI_SP_Product_family__c || oldArticle.BI_SP_Raw_country_code__c != article.BI_SP_Raw_country_code__c) {
			if (currentVeevaFamilyMap.containsKey(article.BI_SP_Product_family__c+'_'+article.BI_SP_Raw_country_code__c)) {
				article.BI_SP_Veeva_product_family__c = currentVeevaFamilyMap.get(article.BI_SP_Product_family__c+'_'+article.BI_SP_Raw_country_code__c);
			}else{
				article.BI_SP_Veeva_product_family__c =null;
			}
		}
		/**
		 * END SAM-48 - Article - product family assigment
		 */

		/**
		 * Article - Stock Control
		 */
		if (article.BI_SP_External_stock_updated__c == true) {
			article.BI_SP_Stock_updated__c = false;
			article.BI_SP_Available_stock__c = article.BI_SP_External_stock__c;
			article.BI_SP_External_stock_updated__c = false;
		}
		/**
		 * END Article - Stock Control
		 */

		/**
		 * Article - Placebo Control
		 */
		if (oldArticle.BI_SP_Type__c != article.BI_SP_Type__c && oldArticle.BI_SP_Type__c == '4' && article.BI_SP_Type__c == '1' && article.BI_SP_Manually_updated__c != true) {
			article.BI_SP_Type__c = oldArticle.BI_SP_Type__c;
		}
		if(article.BI_SP_Manually_updated__c == true){
			article.BI_SP_Manually_updated__c = false;
		}
		/**
		 * END Article - Placebo Control
		 */
	}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
	 * 
	 * @param      so    The SObject (BI_SP_Article__c)
	 * 
	 */
	public void beforeDelete(SObject so) {}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
	 * 
	 * @param      so    The SObject (BI_SP_Article__c)
	 * 
	 */
	public void afterInsert(SObject so) {}


	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
	 * 
	 * @param      oldSo  The old SObject (BI_SP_Article__c)
	 * @param      so     The SObject (BI_SP_Article__c)
	 * 
	 */
	public void afterUpdate(SObject oldSo, SObject so) {}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 * 
	 * @param      so    The SObject (BI_SP_Article__c)
	 * 
	 */
	public void afterDelete(SObject so) {}

	/**
	 * andFinally
	 *
	 * This method is called once all records have been processed by the trigger. Use this
	 * method to accomplish any final operations such as creation or updates of other records.
	 */
	public void andFinally() {}

	/**
	 * OTHER METHODS
	 */

	/**
	 * SAM-48 - Article - product family assigment
	 * Gets the other veeva family for same product family of the articles received in the list
	 *
	 * @param      newArticleList  The new article list
	 *
	 * @return     Map containing current values in veeva family field that have the same product family as the articles in the list.
	 */
	private Map<String, Id> getOtherVeevaFamilyForSameProductFamily(List<BI_SP_Article__c> newArticleList) {
		Set<String> productFamilySet = new Set<String>();
		for (BI_SP_Article__c article : newArticleList) {
			productFamilySet.add(article.BI_SP_Product_family__c);
		}

		Map<String, Id> veevaFamilyMap = new Map<String, Id>(); // <BI_SP_Product_family__c,BI_SP_Veeva_product_family__c>

		String productFamily, veevaFamily,countryCode;
		for (AggregateResult ar : [Select BI_SP_Product_family__c, BI_SP_Veeva_product_family__c,BI_SP_Raw_country_code__c
		                           from BI_SP_Article__c
		                           where BI_SP_Product_family__c IN :productFamilySet
		                           group by BI_SP_Product_family__c, BI_SP_Veeva_product_family__c,BI_SP_Raw_country_code__c]) {

			productFamily = String.valueOf(ar.get('BI_SP_Product_family__c'));
			veevaFamily = String.valueOf(ar.get('BI_SP_Veeva_product_family__c'));
			countryCode = String.valueOf(ar.get('BI_SP_Raw_country_code__c'));

			if (veevaFamily != null && veevaFamily != '' && !veevaFamilyMap.containsKey(productFamily+'_'+countryCode))
				veevaFamilyMap.put(productFamily+'_'+countryCode, veevaFamily);
		}

		return veevaFamilyMap;
	}

	/**
	 * SAM-48 - Article - product family assigment
	 * Gets the the countries for the raw country code in the articles
	 *
	 * @param      newArticleList  The new article list
	 *
	 * @return     Map containing current countries in values for raw country code 
	 */
	private Map<String, String> getCountrySettings(List<BI_SP_Article__c> newArticleList) {
		Set<String> externalCountrySet = new Set<String>();
		for (BI_SP_Article__c article : newArticleList) {
			externalCountrySet.add(article.BI_SP_Raw_country_code__c);
		}

		Map<String, String> countrySettings = new Map<String, String>(); // <external country,country>

		for (BI_SP_Country_settings__c ar : [Select BI_SP_Country_code__c,BI_SP_External_system_country_code__c
		                           from BI_SP_Country_settings__c
		                           where BI_SP_External_system_country_code__c IN :externalCountrySet]) {
			//NOTE: if there are more than one country with the same external country, just take a random one into the map.
			countrySettings.put(ar.BI_SP_External_system_country_code__c, ar.BI_SP_Country_code__c);
		}
		return countrySettings;
	}
}