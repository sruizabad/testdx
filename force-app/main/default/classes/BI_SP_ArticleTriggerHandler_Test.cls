@isTest
private class BI_SP_ArticleTriggerHandler_Test {
    
    public static String countryCode;

    @Testsetup
    static void setUp() {
        countryCode = 'BR';
        
        BI_SP_TestDataUtility.createCustomSettings();
        List<Account> accounts = BI_SP_TestDataUtility.createAccounts();
        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());


        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
    }

    @isTest static void test_ArticleTriggerHandler() {
        String countryCode = 'BR';

        list<Product_vod__c> prods = new list<Product_vod__c>([Select Id,Name,External_Id_vod__c from Product_vod__c]);

        Product_vod__c prod1=prods.get(0);
        Product_vod__c prod2=prods.get(1);

        BI_SP_Article__c article1= new BI_SP_Article__c(
                             BI_SP_Raw_country_code__c = countryCode,
                             BI_SP_Country_code__c = countryCode,
                             BI_SP_Description__c = 'testDescription 111 ' + prod1.Name,
                             BI_SP_E_shop_available__c = false,
                             BI_SP_External_id_1__c = 'testExtid_111_' + prod1.External_Id_vod__c,
                             BI_SP_External_id__c = 'testExtid_111_' + prod1.External_Id_vod__c,
                             BI_SP_External_stock__c = 17,
                             BI_SP_Is_active__c = true,
                             BI_SP_Stock__c = 2,
                             BI_SP_Type__c = '3',
                             BI_SP_Product_family__c = prod1.id,
                             BI_SP_Veeva_product_family__c = prod1.id);

        insert article1;

        BI_SP_Article__c article2= new BI_SP_Article__c(
                             BI_SP_Raw_country_code__c = countryCode,
                             BI_SP_Country_code__c = countryCode,
                             BI_SP_Description__c = 'testDescription 222 ' + prod1.Name,
                             BI_SP_E_shop_available__c = false,
                             BI_SP_External_id_1__c = 'testExtid_222_' + prod1.External_Id_vod__c,
                             BI_SP_External_id__c = 'testExtid_222_' + prod1.External_Id_vod__c,
                             BI_SP_External_stock__c = 17,
                             BI_SP_Is_active__c = true,
                             BI_SP_Stock__c = 2,
                             BI_SP_Type__c = '3',
                             BI_SP_Product_family__c = prod1.id);

        insert article2;

        BI_SP_Article__c article3= new BI_SP_Article__c(
                             BI_SP_Raw_country_code__c = countryCode,
                             BI_SP_Country_code__c = countryCode,
                             BI_SP_Description__c = 'testDescription 333 ' + prod1.Name,
                             BI_SP_E_shop_available__c = false,
                             BI_SP_External_id_1__c = 'testExtid_333_' + prod1.External_Id_vod__c,
                             BI_SP_External_id__c = 'testExtid_333_' + prod1.External_Id_vod__c,
                             BI_SP_External_stock__c = 17,
                             BI_SP_Is_active__c = true,
                             BI_SP_Csv_data_uploaded__c = true,
                             BI_SP_Stock__c = 2,
                             BI_SP_Type__c = '4',
                             BI_SP_Product_family__c = prod1.id,
                             BI_SP_Veeva_product_family__c = prod1.id);

        insert article3;

        article2.BI_SP_Product_family__c=prod2.id;

        update article2;

        article1.BI_SP_External_stock__c=100;
        article1.BI_SP_External_stock_updated__c = true;

        update article1;

        article3.BI_SP_Type__c='1';

        update article3;

        article3.BI_SP_Type__c='1';
        article3.BI_SP_Manually_updated__c = true;

        update article3;

        delete article1;
    }
}