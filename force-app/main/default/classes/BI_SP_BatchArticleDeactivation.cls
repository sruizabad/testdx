/**
 * Batch for article deactivation.
 */
global class BI_SP_BatchArticleDeactivation 
	implements Database.Batchable<sObject> {
	
	String query;

	/**
	 * Batch constructor that make up the query which returns the items 
	 * to be deactivated for the type indicated by parameter
	 *
	 * @param      artType  The article type
	 *
	 */
	global BI_SP_BatchArticleDeactivation(String artType) {
		query = 'SELECT Id, BI_SP_External_stock__c, BI_SP_Is_Active__c, BI_SP_Csv_data_uploaded__c FROM BI_SP_Article__c' +
			' WHERE BI_SP_Type__c = \''+artType+'\' '+
			' AND (BI_SP_Raw_country_code__c = \'MX10\' OR BI_SP_Raw_country_code__c = \'ZD10\') ';
	}
	
	/**
	 * Batch start
	 *
	 * @param      BC    Database.BatchableContext
	 *
	 * @return     Database.QueryLocator
	 */
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	/**
	 * Batch execute
	 *
	 * @param      BC    Database.BatchableContext
	 * @param      scope  The scope
	 * 
	 */
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		for(sObject sc : scope) {
   			if(sc.get('BI_SP_Csv_data_uploaded__c') == false) {
				sc.put('BI_SP_Is_Active__c', false);
   			} else {
				sc.put('BI_SP_Csv_data_uploaded__c', false);	
   			}
   		}
		update scope;
	}
	
	/**
	 * Batch finish
	 *
	 * @param      BC    Database.BatchableContext
	 *
	 */
	global void finish(Database.BatchableContext BC) {}
}