@isTest
private class BI_SP_BatchArticleDeactivation_Test {
	
	@testsetup static void setup() {
		List<Product_vod__c> prods = BI_SP_TestDataUtility.createProducts('MX');
		List<BI_SP_Article__c> art = BI_SP_TestDataUtility.createArticles(prods, 'MX', 'MX10');
	}	

	@isTest static void tesBatchPM() {
		Test.startTest();
		Database.executeBatch(new BI_SP_BatchArticleDeactivation('1'));
		Test.stopTest();
	}
	
	@isTest static void testBatchMS() {
		Test.startTest();
		Database.executeBatch(new BI_SP_BatchArticleDeactivation('3'));
		Test.stopTest();
	}
}