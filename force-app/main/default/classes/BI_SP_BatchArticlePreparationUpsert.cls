/**
 * Batch for article preparation creation and edition.
 */
global class BI_SP_BatchArticlePreparationUpsert 
    implements Database.Batchable<sObject>, Database.Stateful {
    
    List<BI_SP_Article_preparation__c> articlesPreparations {get; set;}
    Map<String,BI_SP_Preparation__c> preparations {get; set;} //<preparationExternalId,preparation>
    List<BI_SP_Article__c> selectedArticles {get; set;}
    String approachType {get; set;}
    boolean allOrNone {get; set;}    
    /**
     * Store all stock reserved in articles preparations by articleId
     */
    Map<Id,Double> totalAllocationQuantityMap {get; set;} //<articleId,totalAllocationQuantity>
    
    /**
     * Batch Constructor that recive all the elements to upserts
     *
     * @param      articlesPreparations  The articles preparations
     * @param      preparations          The preparations of the articles preparations
     * @param      selectedArticles      The selected articles of the articles preparations
     * @param      approachType          The approach type (TG,TR,ST)
     * @param      allOrNone             All or none (indicates if the upserts flag)
     *
     */
    global BI_SP_BatchArticlePreparationUpsert(List<BI_SP_Article_preparation__c> articlesPreparations, Map<String,BI_SP_Preparation__c> preparations, List<BI_SP_Article__c> selectedArticles, String approachType, boolean allOrNone) {
        this.articlesPreparations = articlesPreparations;
        this.preparations = preparations;
        this.selectedArticles = selectedArticles;
        this.allOrNone = allOrNone;
        this.approachType = approachType;
    }
    
    /**
     * Batch start
     *
     * @param      BC    Database.BatchableContext
     *
     * @return     Database.QueryLocator
     */
    global List<BI_SP_Article_preparation__c> start(Database.BatchableContext BC) {
        totalAllocationQuantityMap = new Map<Id,Double>(); //<articleId,totalAllocationQuantity>
        return articlesPreparations;
    }

    /**
     * Batch execute
     *
     * @param      BC    Database.BatchableContext
     * @param      scope  The scope
     * 
     */
    global void execute(Database.BatchableContext BC, List<BI_SP_Article_preparation__c> scope) { 
        List<String> apExternalIds = new List<String>();
        for(BI_SP_Article_preparation__c ap : scope){
            apExternalIds.add(ap.BI_SP_External_id__c);
        }

        Map<String,BI_SP_Article_preparation__c> existingArticlePreps = new Map<String,BI_SP_Article_preparation__c>(); //<articlePreparationExternalId,articlePreparation>
        for(BI_SP_Article_preparation__c existingAP : [SELECT Id, BI_SP_External_id__c, BI_SP_Amount_target__c, BI_SP_Amount_territory__c, BI_SP_Amount_strategy__c
                                                        FROM BI_SP_Article_preparation__c
                                                        WHERE BI_SP_External_id__c in : apExternalIds]){
            existingArticlePreps.put(existingAP.BI_SP_External_id__c, existingAP);
        }
        
        Set<BI_SP_Preparation__c> preparationsSet = new Set<BI_SP_Preparation__c>();
        //totalAllocationQuantityMap = new Map<Id,Double>();    //<articleId,totalAllocationQuantity>
        Map<String,Double> articlePrepAllocationQuantityMap = new Map<String,Double>(); //<articleId,totalAllocationQuantity>
        List<BI_SP_Article_preparation__c> articlePrepsToUpsert = new List<BI_SP_Article_preparation__c>();
        //List<BI_SP_Article_preparation__c> articlePrepsToDelete = new List<BI_SP_Article_preparation__c>();

        for(BI_SP_Article_preparation__c ap : scope){
            BI_SP_Preparation__c p = null;
            String pExternalId = ap.BI_SP_Preparation__r.BI_SP_External_id__c;
            p=preparations.get(pExternalId);

            //Boolean createNewRecord = true;
            //Boolean deleteExistingRecord = false;
            if(approachType == 'TR'){
                ap.BI_SP_Amount_territory__c = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_territory__c) 
                                                + ap.BI_SP_Amount_territory__c; 
                ap.BI_SP_Amount_target__c = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_target__c); 
                ap.BI_SP_Amount_strategy__c = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_strategy__c);

                //Decimal existingTerritoryAmount = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_territory__c);
                //if(ap.BI_SP_Amount_territory__c <= 0 && existingTerritoryAmount <= 0){
                //    createNewRecord = false;
                //}                
                //Decimal totalAmount = ap.BI_SP_Amount_target__c+ap.BI_SP_Amount_strategy__c+ap.BI_SP_Amount_territory__c;
                //if(existingArticlePreps.get(ap.BI_SP_External_id__c) != null && totalAmount<=0){
                //    deleteExistingRecord=true;
                //    createNewRecord = false;
                //}
            }else if(approachType == 'TG'){                
                ap.BI_SP_Amount_territory__c = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_territory__c); 
                ap.BI_SP_Amount_target__c = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_target__c) 
                                                + ap.BI_SP_Amount_target__c; 
                ap.BI_SP_Amount_strategy__c = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_strategy__c);
                
                //Decimal existingTargetAmount = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_target__c);
                //if(ap.BI_SP_Amount_target__c <= 0 && existingTargetAmount <= 0){
                //    createNewRecord = false;
                //}
                //Decimal totalAmount = ap.BI_SP_Amount_target__c+ap.BI_SP_Amount_strategy__c+ap.BI_SP_Amount_territory__c;
                //if(existingArticlePreps.get(ap.BI_SP_External_id__c) != null && totalAmount<=0){
                //    deleteExistingRecord=true;
                //    createNewRecord = false;
                //}
            }else if(approachType == 'ST'){                
                ap.BI_SP_Amount_territory__c = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_territory__c);
                ap.BI_SP_Amount_target__c = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_target__c); 
                ap.BI_SP_Amount_strategy__c = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_strategy__c) 
                                                + ap.BI_SP_Amount_strategy__c; 
                
                //Decimal existingStrategyAmount = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_strategy__c);
                //if(ap.BI_SP_Amount_strategy__c <= 0 && existingStrategyAmount <= 0){
                //    createNewRecord = false;
                //}
                //Decimal totalAmount = ap.BI_SP_Amount_target__c+ap.BI_SP_Amount_strategy__c+ap.BI_SP_Amount_territory__c;
                //if(existingArticlePreps.get(ap.BI_SP_External_id__c) != null && totalAmount<=0){
                //    deleteExistingRecord=true;
                //    createNewRecord = false;
                //}
            }

            //if(deleteExistingRecord){
            //    articlePrepsToDelete.add(existingArticlePreps.get(ap.BI_SP_External_id__c));
            //}
             
            Decimal amount = ((ap.BI_SP_Amount_target__c == null) ? 0 : ap.BI_SP_Amount_target__c) + 
                             ((ap.BI_SP_Amount_territory__c == null) ? 0 : ap.BI_SP_Amount_territory__c) + 
                             ((ap.BI_SP_Amount_strategy__c == null) ? 0 : ap.BI_SP_Amount_strategy__c);
            Decimal existingAmount = ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_target__c) + 
                             ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_territory__c) + 
                             ((existingArticlePreps.get(ap.BI_SP_External_id__c) == null) ? 0 : existingArticlePreps.get(ap.BI_SP_External_id__c).BI_SP_Amount_strategy__c);
            Decimal finalAmount = amount - existingAmount;

            if(totalAllocationQuantityMap.containsKey(ap.BI_SP_Article__c)){
                totalAllocationQuantityMap.put(ap.BI_SP_Article__c, totalAllocationQuantityMap.get(ap.BI_SP_Article__c) + finalAmount);
            }else{
                totalAllocationQuantityMap.put(ap.BI_SP_Article__c, finalAmount);
            }
            articlePrepAllocationQuantityMap.put(ap.BI_SP_External_id__c, finalAmount);

            ap.BI_SP_Adjusted_amount_1__c = ap.BI_SP_Amount_target__c;

            //if(createNewRecord){
                preparationsSet.add(p);
                articlePrepsToUpsert.add(ap);
            //}
        }

        Database.UpsertResult[] resultsPreparations = Database.upsert(new List<BI_SP_Preparation__c>(preparationsSet), BI_SP_Preparation__c.Fields.BI_SP_External_id__c, allOrNone);
        for(Database.UpsertResult result: resultsPreparations){
            if(!result.isSuccess()){
                for(Database.Error err : result.getErrors()) {
                    System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchArticlePreparationUpsert - execute - Preparations error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                }
            }
        }

        Database.UpsertResult[] resultsArticlePreparations = Database.upsert(articlePrepsToUpsert, BI_SP_Article_preparation__c.Fields.BI_SP_External_id__c, allOrNone);
        for(Database.UpsertResult result: resultsArticlePreparations){
            if(!result.isSuccess()){
                for(Database.Error err : result.getErrors()) {
                    System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchArticlePreparationUpsert - execute - Article Preparations error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                }
            }
        }

        for(BI_SP_Article_preparation__c ap : articlePrepsToUpsert){
            if(ap.Id==null){
                if(totalAllocationQuantityMap.containsKey(ap.BI_SP_Article__c)){
                    Decimal amount = articlePrepAllocationQuantityMap.get(ap.BI_SP_External_id__c);
                    totalAllocationQuantityMap.put(ap.BI_SP_Article__c, totalAllocationQuantityMap.get(ap.BI_SP_Article__c)-amount);
                }
            }
        }

        //Database.DeleteResult[] resultsDeleteArticlePreparations = Database.delete(articlePrepsToDelete, allOrNone);
        //for(Database.DeleteResult result: resultsDeleteArticlePreparations){
        //    if(!result.isSuccess()){
        //        for(Database.Error err : result.getErrors()) {
        //            System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchArticlePreparationUpsert - execute - Article Preparations delete error -> ' + 
        //                                                err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
        //        }
        //    }
        //}
    }
    
    /**
     * Batch finish
     *
     * @param      BC    Database.BatchableContext
     *
     */
    global void finish(Database.BatchableContext BC) {
        for(BI_SP_Article__c article : selectedArticles){
            article.BI_SP_Stock__c -= (totalAllocationQuantityMap.get(article.Id)==null) ? 0 : totalAllocationQuantityMap.get(article.Id);
        }

        List<Database.SaveResult> resultsArticles = Database.update(selectedArticles, false);   
        for (Database.SaveResult result : resultsArticles) {
            if (!result.isSuccess()) {
                for(Database.Error err : result.getErrors()) {
                    System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchArticlePreparationUpsert - finish - Article error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                }
            }
        }
    }
    
    public class BI_SP_BatchArticlePreparationUpsertException extends Exception{}
}