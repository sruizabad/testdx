@isTest
private class BI_SP_BatchArticlePreparationUpsert_Test {
	
    public static String countryCode = 'BR';

    @Testsetup
    static void setUp() {
        BI_SP_TestDataUtility.createCustomSettings();
        List<Customer_Attribute_BI__c> specs = BI_SP_TestDataUtility.createSpecialties();
        List<Account> accounts = BI_SP_TestDataUtility.createAccounts(specs);
        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());


        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);

        List<BI_PL_Position__c> positions = BI_SP_TestDataUtility.createPositions(countryCode, 2);
    }

	@isTest static void test_ApproachType_TR() {
		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        
        List<BI_PL_Position__c> selectedPositions = new List<BI_PL_Position__c>([SELECT Id, Name, BI_PL_Country_code__c FROM BI_PL_Position__c]);
        BI_SP_Article__c selectedArticle = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];

		Set<BI_SP_Article__c> selectedArticles = new Set<BI_SP_Article__c>();
		List<BI_SP_Article_preparation__c> articlesPreparations = new List<BI_SP_Article_preparation__c>();
		Map<String,BI_SP_Preparation__c> preparations = new Map<String,BI_SP_Preparation__c>();

        Decimal i = 0;
        for(BI_PL_Position__c position : selectedPositions) {
			BI_SP_Preparation__c p = new BI_SP_Preparation__c();
			p.BI_SP_Position__c = position.Id;
			p.BI_SP_Preparation_period__c = period.Id;
			p.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generatePreparationExternalId(period.BI_SP_External_id__c, position.Name);
			if(!preparations.containsKey(p.BI_SP_External_id__c)){
				preparations.put(p.BI_SP_External_id__c,p);				
			}

			BI_SP_Article_preparation__c ap = new BI_SP_Article_preparation__c();
			ap.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generateArticlePreparationExternalId(p.BI_SP_External_id__c, selectedArticle.BI_SP_External_id__c);
			ap.BI_SP_Preparation__r = new BI_SP_Preparation__c(BI_SP_External_id__c=p.BI_SP_External_id__c);
			ap.BI_SP_Article__c = selectedArticle.Id;
			ap.BI_SP_Status__c = 'NE';
			ap.BI_SP_Amount_target__c = 0;
			ap.BI_SP_Amount_territory__c = i; 
			ap.BI_SP_Amount_strategy__c = 0;
			BI_SP_Article__c art = new BI_SP_Article__c(Id=selectedArticle.Id, BI_SP_Stock__c = selectedArticle.BI_SP_Stock__c);
			selectedArticles.add(art);			
			articlesPreparations.add(ap);
			i++;
		}
        Database.executeBatch(new BI_SP_BatchArticlePreparationUpsert(articlesPreparations, preparations, new List<BI_SP_Article__c>(selectedArticles), 'TR', false)); 

        for(BI_SP_Article_preparation__c ap : articlesPreparations) {
			ap.BI_SP_Amount_territory__c = 0; 
		}
        Database.executeBatch(new BI_SP_BatchArticlePreparationUpsert(articlesPreparations, preparations, new List<BI_SP_Article__c>(selectedArticles), 'TR', false));
	}

	@isTest static void test_ApproachType_TG() {
		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        
        List<BI_PL_Position__c> selectedPositions = new List<BI_PL_Position__c>([SELECT Id, Name, BI_PL_Country_code__c FROM BI_PL_Position__c]);
        BI_SP_Article__c selectedArticle = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];

		Set<BI_SP_Article__c> selectedArticles = new Set<BI_SP_Article__c>();
		List<BI_SP_Article_preparation__c> articlesPreparations = new List<BI_SP_Article_preparation__c>();
		Map<String,BI_SP_Preparation__c> preparations = new Map<String,BI_SP_Preparation__c>();

        Decimal i = 0;
        for(BI_PL_Position__c position : selectedPositions) {
			BI_SP_Preparation__c p = new BI_SP_Preparation__c();
			p.BI_SP_Position__c = position.Id;
			p.BI_SP_Preparation_period__c = period.Id;
			p.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generatePreparationExternalId(period.BI_SP_External_id__c, position.Name);
			if(!preparations.containsKey(p.BI_SP_External_id__c)){
				preparations.put(p.BI_SP_External_id__c,p);				
			}

			BI_SP_Article_preparation__c ap = new BI_SP_Article_preparation__c();
			ap.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generateArticlePreparationExternalId(p.BI_SP_External_id__c, selectedArticle.BI_SP_External_id__c);
			ap.BI_SP_Preparation__r = new BI_SP_Preparation__c(BI_SP_External_id__c=p.BI_SP_External_id__c);
			ap.BI_SP_Article__c = selectedArticle.Id;
			ap.BI_SP_Status__c = 'NE';
			ap.BI_SP_Amount_target__c = i;
			ap.BI_SP_Amount_territory__c = 0; 
			ap.BI_SP_Amount_strategy__c = 0;
			BI_SP_Article__c art = new BI_SP_Article__c(Id=selectedArticle.Id, BI_SP_Stock__c = selectedArticle.BI_SP_Stock__c);
			selectedArticles.add(art);			
			articlesPreparations.add(ap);
			i++;
		}
        Database.executeBatch(new BI_SP_BatchArticlePreparationUpsert(articlesPreparations, preparations, new List<BI_SP_Article__c>(selectedArticles), 'TG', false)); 

        for(BI_SP_Article_preparation__c ap : articlesPreparations) {
			ap.BI_SP_Amount_target__c = 0; 
		}
        Database.executeBatch(new BI_SP_BatchArticlePreparationUpsert(articlesPreparations, preparations, new List<BI_SP_Article__c>(selectedArticles), 'TG', false));
	}

	@isTest static void test_ApproachType_ST() {
		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        
        List<BI_PL_Position__c> selectedPositions = new List<BI_PL_Position__c>([SELECT Id, Name, BI_PL_Country_code__c FROM BI_PL_Position__c]);
        BI_SP_Article__c selectedArticle = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];

		Set<BI_SP_Article__c> selectedArticles = new Set<BI_SP_Article__c>();
		List<BI_SP_Article_preparation__c> articlesPreparations = new List<BI_SP_Article_preparation__c>();
		Map<String,BI_SP_Preparation__c> preparations = new Map<String,BI_SP_Preparation__c>();

        Decimal i = 0;
        for(BI_PL_Position__c position : selectedPositions) {
			BI_SP_Preparation__c p = new BI_SP_Preparation__c();
			p.BI_SP_Position__c = position.Id;
			p.BI_SP_Preparation_period__c = period.Id;
			p.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generatePreparationExternalId(period.BI_SP_External_id__c, position.Name);
			if(!preparations.containsKey(p.BI_SP_External_id__c)){
				preparations.put(p.BI_SP_External_id__c,p);				
			}

			BI_SP_Article_preparation__c ap = new BI_SP_Article_preparation__c();
			ap.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generateArticlePreparationExternalId(p.BI_SP_External_id__c, selectedArticle.BI_SP_External_id__c);
			ap.BI_SP_Preparation__r = new BI_SP_Preparation__c(BI_SP_External_id__c=p.BI_SP_External_id__c);
			ap.BI_SP_Article__c = selectedArticle.Id;
			ap.BI_SP_Status__c = 'NE';
			ap.BI_SP_Amount_target__c = 0;
			ap.BI_SP_Amount_territory__c = 0; 
			ap.BI_SP_Amount_strategy__c = i;
			BI_SP_Article__c art = new BI_SP_Article__c(Id=selectedArticle.Id, BI_SP_Stock__c = selectedArticle.BI_SP_Stock__c);
			selectedArticles.add(art);			
			articlesPreparations.add(ap);
			i++;
		}
        Database.executeBatch(new BI_SP_BatchArticlePreparationUpsert(articlesPreparations, preparations, new List<BI_SP_Article__c>(selectedArticles), 'ST', false)); 

        for(BI_SP_Article_preparation__c ap : articlesPreparations) {
			ap.BI_SP_Amount_strategy__c = 0; 
		}
        Database.executeBatch(new BI_SP_BatchArticlePreparationUpsert(articlesPreparations, preparations, new List<BI_SP_Article__c>(selectedArticles), 'ST', false));
	}


	@isTest static void test_ApproachBad() {
		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        
        List<BI_PL_Position__c> selectedPositions = new List<BI_PL_Position__c>([SELECT Id, Name, BI_PL_Country_code__c FROM BI_PL_Position__c]);
        BI_SP_Article__c selectedArticle = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];

		Set<BI_SP_Article__c> selectedArticles = new Set<BI_SP_Article__c>();
		List<BI_SP_Article_preparation__c> articlesPreparations = new List<BI_SP_Article_preparation__c>();
		Map<String,BI_SP_Preparation__c> preparations = new Map<String,BI_SP_Preparation__c>();

        Decimal i = 0;
        for(BI_PL_Position__c position : selectedPositions) {
			BI_SP_Preparation__c p = new BI_SP_Preparation__c();
			p.BI_SP_Position__c = position.Id;
			p.BI_SP_Preparation_period__c = period.Id;
			p.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generatePreparationExternalId(period.BI_SP_External_id__c, position.Name);
			if(!preparations.containsKey(p.BI_SP_External_id__c)){
				preparations.put(p.BI_SP_External_id__c,p);				
			}

			BI_SP_Article_preparation__c ap = new BI_SP_Article_preparation__c();
			ap.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generateArticlePreparationExternalId(p.BI_SP_External_id__c, selectedArticle.BI_SP_External_id__c);
			ap.BI_SP_Preparation__r = new BI_SP_Preparation__c(BI_SP_External_id__c=p.BI_SP_External_id__c);
			ap.BI_SP_Article__c = selectedArticle.Id;
			ap.BI_SP_Status__c = 'NE';
			ap.BI_SP_Amount_target__c = 0;
			ap.BI_SP_Amount_territory__c = selectedArticle.BI_SP_Stock__c+1; 
			ap.BI_SP_Amount_strategy__c = 0;
			BI_SP_Article__c art = new BI_SP_Article__c(Id=selectedArticle.Id, BI_SP_Stock__c = selectedArticle.BI_SP_Stock__c);
			selectedArticles.add(art);			
			articlesPreparations.add(ap);
			i++;
		}
        Database.executeBatch(new BI_SP_BatchArticlePreparationUpsert(articlesPreparations, preparations, new List<BI_SP_Article__c>(selectedArticles), 'TR', false));

        for(BI_SP_Article_preparation__c ap : articlesPreparations) {
			BI_SP_Preparation__c p = new BI_SP_Preparation__c();
			p.OwnerId = null;
			
			p.BI_SP_Position__c = ap.BI_SP_Preparation__r.BI_SP_Position__c;
			p.BI_SP_Preparation_period__c = period.Id;
			p.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generatePreparationExternalId(period.BI_SP_External_id__c, ap.BI_SP_Preparation__r.BI_SP_Position__r.Name);
			if(!preparations.containsKey(p.BI_SP_External_id__c)){
				preparations.put(p.BI_SP_External_id__c,p);				
			}
			ap.BI_SP_Preparation__r = new BI_SP_Preparation__c(BI_SP_External_id__c=p.BI_SP_External_id__c);
		}
        Database.executeBatch(new BI_SP_BatchArticlePreparationUpsert(articlesPreparations, preparations, new List<BI_SP_Article__c>(selectedArticles), 'TR', false));
	}
	
}