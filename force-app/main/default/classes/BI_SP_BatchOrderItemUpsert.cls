/**
 * Batch fororder and order items creation and modification.
 */
global class BI_SP_BatchOrderItemUpsert implements Database.Batchable<sObject>, Database.Stateful {
    
    BI_SP_Preparation_period__c period {get; set;}
    boolean allOrNone {get; set;}
    /**
     * Store all necessary Country settings
     */
    Map<String, BI_SP_Country_settings__c> csMap {get; set;} //<countrycode,CountrySetting>
    Map<String, BI_SP_Article__c> articleMap {get; set;}    //<articleId,Article>
    
    /**
     * Batch Constructor that recive the preparation period 
     * for which the orders must be generated
     *
     * @param      period     The preparation period
     * @param      allOrNone  All or none (indicates if the upserts flag)
     *
     */
    global BI_SP_BatchOrderItemUpsert(BI_SP_Preparation_period__c period, boolean allOrNone) {
        this.period = period;
        this.allOrNone = allOrNone;
    }
    
    /**
     * Batch start
     *
     * @param      BC    Database.BatchableContext
     *
     * @return     Database.QueryLocator
     */
    global List<BI_SP_Article_preparation__c> start(Database.BatchableContext BC) {
        List<BI_SP_Article_preparation__c> articlesPreparations = new List<BI_SP_Article_preparation__c>([SELECT Id, Name, BI_SP_Adjusted_amount_1__c, BI_SP_Adjusted_amount_2__c, BI_SP_External_id__c, 
                                                                                                            BI_SP_Amount_target__c, BI_SP_Amount_territory__c, BI_SP_Amount_strategy__c, BI_SP_Amount_e_shop__c,
                                                                                                            BI_SP_Approval_status__c, BI_SP_Status__c,
                                                                                                            BI_SP_Preparation__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Position__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Position__r.Name, 
                                                                                                            BI_SP_Preparation__r.BI_SP_Position__r.BI_PL_Country_code__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_External_id__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_Delivery_date__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_Planit_cycle__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_Planit_cycle__r.BI_PL_Start_date__c,
                                                                                                            BI_SP_Article__c,
                                                                                                            BI_SP_Article__r.BI_SP_External_id__c, 
                                                                                                            BI_SP_Article__r.BI_SP_Type__c,
                                                                                                            BI_SP_Article__r.BI_SP_Country_code__c, 
                                                                                                            BI_SP_Article__r.BI_SP_Raw_country_code__c,
                                                                                                            BI_SP_Article__r.BI_SP_Description__c
                                                                                                            FROM BI_SP_Article_preparation__c
                                                                                                            WHERE BI_SP_Preparation__r.BI_SP_Preparation_period__c = :period.Id]);
        Set<String> countriesSet = new Set<String>();
        for (BI_SP_Article_preparation__c ap : articlesPreparations) {
            countriesSet.add(ap.BI_SP_Preparation__r.BI_SP_Position__r.BI_PL_Country_code__c);
        }

        csMap = new Map<String, BI_SP_Country_settings__c>();   //<countrycode,CountrySetting>
        for(BI_SP_Country_settings__c cs : [SELECT BI_SP_PM_Order_reason__c, BI_SP_MS_Order_reason__c, BI_SP_MS_Order_type__c, BI_SP_PL_Order_reason__c, BI_SP_PL_Order_type__c, 
                                                BI_SP_PM_Order_type__c, BI_SP_Country_code__c, 
                                                BI_SP_External_system_country_code__c, BI_SP_Fixed_country_address__c, BI_SP_Is_sap_country__c
                                                FROM BI_SP_Country_settings__c
                                                WHERE BI_SP_Country_code__c IN :countriesSet]){
            csMap.put(cs.BI_SP_Country_code__c, cs);
        }

        List<String> articleIds = new List<String>();
        for(BI_SP_Article_preparation__c ap : articlesPreparations){
            articleIds.add(ap.BI_SP_Article__c);
        }

        articleMap = new Map<String, BI_SP_Article__c>([SELECT Id, Name, BI_SP_Stock__c, BI_SP_Veeva_product_family__c, BI_SP_E_shop_available__c, BI_SP_External_id__c
                                                                                FROM BI_SP_Article__c
                                                                                WHERE Id IN :articleIds]); //<articleId,Article>

        return articlesPreparations;
    }

    /**
     * Batch execute
     *
     * @param      BC    Database.BatchableContext
     * @param      scope  The scope
     * 
     */
    global void execute(Database.BatchableContext BC, List<BI_SP_Article_preparation__c> scope) {
        Map<String, BI_SP_Article__c> articlesToUpdate = new Map<String, BI_SP_Article__c>(); //articles to update <articleId, Article>
        List<String> ordersExternalIds = new List<String>();
        List<String> positionNames = new List<String>();
        List<BI_SP_Article_preparation__c> articlePrepsToUpdate = new List<BI_SP_Article_preparation__c>();

        for(BI_SP_Article_preparation__c ap : scope){
            ordersExternalIds.add(BI_SP_SamproServiceUtility.generateOrderArticlePreparationExternalId('AP', ap.BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_External_id__c,
                                                                                                        ap.BI_SP_Preparation__r.BI_SP_Position__r.Name, ap.BI_SP_Article__r.BI_SP_Type__c));
            positionNames.add(ap.BI_SP_Preparation__r.BI_SP_Position__r.Name);

            //if the status is different from AP, RE and NA indicates that the treatment so far received by the article preparation was as if its state was AP. 
            //At the time of generating the orders, not being really approved should be considered as NA and therefore have to control the stock as if it were a rejection
            if(ap.BI_SP_Approval_status__c != 'AP' && ap.BI_SP_Approval_status__c != 'RE' && ap.BI_SP_Approval_status__c != 'NA'){     

                Decimal adjustmentOldValue = BI_SP_SamproServiceUtility.getArticlePreparationApprovalVariableAmountValue(ap);
                Decimal adjustmentNewValue = BI_SP_SamproServiceUtility.getArticlePreparationFinalVariableAmountValue(ap);

                BI_SP_Article__c article = articleMap.get(ap.BI_SP_Article__c);
                article.BI_SP_Stock__c = article.BI_SP_Stock__c + (adjustmentOldValue - adjustmentNewValue);
                if(article.BI_SP_Stock__c >= 0){
                    articleMap.put(ap.BI_SP_Article__c,article);
                    articlesToUpdate.put(ap.BI_SP_Article__c,article);
                }else{
                    System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchOrderItemUpsert - execute - ArticlePreparation (ID='+ap.Id+')'+
                                                        ' error: Not enougth Stock from Article (ID='+article.Id+'). Not upsert orderItem for this ArticlePreparation.');
                }
                ap.BI_SP_Approval_status__c = 'NA';
                articlePrepsToUpdate.add(ap);

            }
        }

        //get existing orders
        Map<String,BI_SP_Order__c> orderMap = new Map<String,BI_SP_Order__c>(); //<orderExternalId,Order>
        Map<String,Decimal> orderItemNumberMap = new Map<String,Decimal>(); //<orderExternalId, maxOrderItemNumber>
        for(BI_SP_Order__c order : [SELECT Id, BI_SP_External_id__c, BI_SP_Max_order_item_number__c, BI_SP_Delivery_status__c FROM BI_SP_Order__c WHERE BI_SP_External_id__c IN :ordersExternalIds]){
            orderMap.put(order.BI_SP_External_id__c, order);
            orderItemNumberMap.put(order.BI_SP_External_id__c, (order.BI_SP_Max_order_item_number__c==null ? 0 : order.BI_SP_Max_order_item_number__c));
        }

        //get existing orderItems
        Map<String,BI_SP_Order_item__c> orderItemMap = new Map<String,BI_SP_Order_item__c>();   //<orderItemExternalId, orderItem>
        for(BI_SP_Order_item__c orderItem : [SELECT Id, BI_SP_Article_preparation__c, BI_SP_Order__c, BI_SP_External_id__c FROM BI_SP_Order_item__c WHERE BI_SP_Order__r.BI_SP_External_id__c IN :ordersExternalIds]){
            orderItemMap.put(orderItem.BI_SP_External_id__c, orderItem);
        }

        //get structure to find the correct adress (sales rep, ship to, rep address, ...) to the order
        Map<String,Map<String,List<SObject>>> positionSalesRepMap = BI_SP_SamproServiceUtility.getSalesRepByPositionName(positionNames); //<User OR BI_SP_Staging_user_info__c, <territoryName, List of objects(User or BI_SP_Staging_user_info__c)>>

        Set<BI_SP_Order__c> ordersToUpsert = new Set<BI_SP_Order__c>();
        Set<BI_SP_Order_item__c> orderItemsToUpsert = new Set<BI_SP_Order_item__c>();
        for(BI_SP_Article_preparation__c ap : scope){
            String orderExternalId = BI_SP_SamproServiceUtility.generateOrderArticlePreparationExternalId('AP', ap.BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_External_id__c,
                                                                                                            ap.BI_SP_Preparation__r.BI_SP_Position__r.Name, ap.BI_SP_Article__r.BI_SP_Type__c);
            //the order item is generated if the order does not already exist or if the status is Pending to send or Failed
            if(!orderMap.containsKey(orderExternalId) || (orderMap.get(orderExternalId).BI_SP_Delivery_status__c == '0') || (orderMap.get(orderExternalId).BI_SP_Delivery_status__c == '4')){
                BI_SP_Order_item__c oi = new BI_SP_Order_item__c();
                Decimal itemNumber = 1;

                //get the corresponding item number
                if(!orderItemMap.containsKey(ap.Id)){
                    if(orderItemNumberMap.containsKey(orderExternalId)){
                        itemNumber=orderItemNumberMap.get(orderExternalId)+1;
                    }
                    orderItemNumberMap.put(orderExternalId,itemNumber);
                    oi.BI_SP_Order_item_number__c = itemNumber;
                }
                oi.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generateOrderItemArticlePreparationExternalId(ap.Id);
                oi.BI_SP_Article_preparation__c = ap.Id;
                oi.BI_SP_Article__c = ap.BI_SP_Article__c;
                oi.BI_SP_Article_description__c = ap.BI_SP_Article__r.BI_SP_Description__c;

                Decimal amount = ((ap.BI_SP_Amount_territory__c == null) ? 0 : ap.BI_SP_Amount_territory__c) + 
                                 ((ap.BI_SP_Amount_strategy__c == null) ? 0 : ap.BI_SP_Amount_strategy__c) +
                                 BI_SP_SamproServiceUtility.getArticlePreparationFinalVariableAmountValue(ap);
                oi.BI_SP_Quantity__c = amount;

                //if the order item already existed or the quantity of the order item is greater than 0, 
                //the order item will be generated and the order if necessary
                if(orderItemMap.containsKey(ap.Id) || amount>0){
                    Bi_SP_Order__c order = prepareOrders(ap,orderExternalId,csMap.get(ap.BI_SP_Preparation__r.BI_SP_Position__r.BI_PL_Country_code__c), positionSalesRepMap);
                    //the oprder will be generated if order != null and not already exists
                    //if(order != null && !orderMap.containsKey(orderExternalId)) {
                    if(order != null) {
                        orderMap.put(orderExternalId, order);
                    }
                    ordersToUpsert.add(orderMap.get(orderExternalId));
                    oi.BI_SP_Order__r = new BI_SP_Order__c(BI_SP_External_id__c=orderExternalId);
                    oi.BI_SP_Sales_rep_address__c = order.BI_SP_Sales_rep_address__c;
                    orderItemsToUpsert.add(oi);     
                }
            }   
        }

        List<Database.UpsertResult> resultsArticles = Database.upsert(articlesToUpdate.values(), BI_SP_Article__c.BI_SP_External_id__c,true);
        for(Database.UpsertResult sr: resultsArticles){
            if(!sr.isSuccess()){
                for(Database.Error err : sr.getErrors()) {
                    System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchOrderItemUpsert - finish - Article error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                }
            }
        }

        List<Database.UpsertResult> resultsArticlePreps = Database.upsert(articlePrepsToUpdate, BI_SP_Article_preparation__c.BI_SP_External_id__c,true);
        for(Database.UpsertResult sr: resultsArticlePreps){
            if(!sr.isSuccess()){
                for(Database.Error err : sr.getErrors()) {
                    System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchOrderItemUpsert - finish - ArticlePreparations error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                }
            }
        }

        List<Database.UpsertResult> resultsOrders = Database.upsert(new List<BI_SP_Order__c>(ordersToUpsert), BI_SP_Order__c.BI_SP_External_id__c, allOrNone);  
        for (Database.UpsertResult sr : resultsOrders) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchOrderItemUpsert - finish - Order error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                }
            }
        }

        List<Database.UpsertResult> resultsOrderItems = Database.upsert(new List<BI_SP_Order_item__c>(orderItemsToUpsert), BI_SP_Order_item__c.BI_SP_External_id__c, allOrNone);    
        for (Database.UpsertResult sr : resultsOrderItems) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchOrderItemUpsert - finish - Order Item error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                }
            }
        }
    }
    
    /**
     * Batch finish
     *
     * @param      BC    Database.BatchableContext
     *
     */
    global void finish(Database.BatchableContext BC) {
        List<BI_SP_Order__c> ordersToDelete = new List<BI_SP_Order__c>();
        for(BI_SP_Order__c order : [SELECT Id, BI_SP_Max_order_item_number__c  
            FROM BI_SP_Order__c 
            WHERE BI_SP_Preparation_period__c = :period.Id]){
                
                if(!(order.BI_SP_Max_order_item_number__c>0)){
                    ordersToDelete.add(order);
                }
        }
        Database.DeleteResult[] deleteResults = Database.delete(ordersToDelete, false);
        for (Database.DeleteResult sr : deleteResults) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    System.debug(LoggingLevel.ERROR, '### - BI_SP_BatchOrderItemUpsert - finish - Order delete error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                }
            }
        }
    }

    /**
     * The orders are prepared from the indicated parameters
     *
     * @param      articlesPreparation  The article preparation for the order
     * @param      orderExternalId      The order external identifier
     * @param      cs                   The country custom setting for use
     * @param      positionSalesRepMap  The position sales rep map (structure to find the correct adress (sales rep, ship to, rep address, ...) to the order)
     *                                  < User OR BI_SP_Staging_user_info__c, < territoryName, List of objects(User or BI_SP_Staging_user_info__c) > >
     *
     * @return     the order generate
     */
    private BI_SP_Order__c prepareOrders(BI_SP_Article_preparation__c articlesPreparation, String orderExternalId, BI_SP_Country_settings__c cs, Map<String,Map<String,List<SObject>>> positionSalesRepMap){
        if(cs == null) return null;

        BI_SP_Order__c order = new BI_SP_Order__c();
        order.BI_SP_Article_type__c = articlesPreparation.BI_SP_Article__r.BI_SP_Type__c;
        order.BI_SP_Country_code__c = cs.BI_SP_External_system_country_code__c;
        order.BI_SP_Cycle_year__c = (articlesPreparation.BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_Planit_cycle__r.BI_PL_Start_date__c).year();
        order.BI_SP_Delivery_date__c = articlesPreparation.BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_Delivery_date__c;
        order.BI_SP_Delivery_status__c = '0';
        order.BI_SP_External_id__c = orderExternalId;
        if(order.BI_SP_Article_type__c == '1'){
            order.BI_SP_Order_reason__c = cs.BI_SP_MS_Order_reason__c;
            order.BI_SP_Order_type__c = cs.BI_SP_MS_Order_type__c;
        }else if(order.BI_SP_Article_type__c == '3'){
            order.BI_SP_Order_reason__c = cs.BI_SP_PM_Order_reason__c;
            order.BI_SP_Order_type__c = cs.BI_SP_PM_Order_type__c;
        }else if(order.BI_SP_Article_type__c == '4'){
            order.BI_SP_Order_reason__c = cs.BI_SP_PL_Order_reason__c;
            order.BI_SP_Order_type__c = cs.BI_SP_PL_Order_type__c;
        }
        order.BI_SP_Order_type_pl__c = 'AP';
        order.BI_SP_Cycle__c = articlesPreparation.BI_SP_Preparation__r.BI_SP_Preparation_period__c;
        order.BI_SP_Raw_country_code__c = cs.BI_SP_External_system_country_code__c;
        order.BI_SP_Preparation_period__c = articlesPreparation.BI_SP_Preparation__r.BI_SP_Preparation_period__c;
        order.BI_SP_Territory_name__c = articlesPreparation.BI_SP_Preparation__r.BI_SP_Position__r.Name;

        //if the country is a SAP country, the order address is necessary use the BI_SP_Staging_user_info__c record from the positionSalesRepMap structure
        if(cs.BI_SP_Is_sap_country__c){
            Map<String,List<SObject>> stagingUserInfoPositionSalesRepMap = positionSalesRepMap.get('BI_SP_Staging_user_info__c');    //< territoryName, List of BI_SP_Staging_user_info__c >         
            List<BI_SP_Staging_user_info__c> stagingUserInfoPositionList = (List<BI_SP_Staging_user_info__c>) stagingUserInfoPositionSalesRepMap.get(articlesPreparation.BI_SP_Preparation__r.BI_SP_Position__r.Name);
            
            if(stagingUserInfoPositionList != null && stagingUserInfoPositionList.size()>0){
                order.BI_SP_Sales_rep_name__c = stagingUserInfoPositionList.get(0).BI_SP_User__r.Name;
                order.BI_SP_Ship_to__c = stagingUserInfoPositionList.get(0).BI_SP_Ship_to__c;   
                order.BI_SP_Sales_rep_address__c = getAddressString(stagingUserInfoPositionList.get(0).BI_SP_User__r.Street, stagingUserInfoPositionList.get(0).BI_SP_User__r.PostalCode, stagingUserInfoPositionList.get(0).BI_SP_User__r.City, stagingUserInfoPositionList.get(0).BI_SP_User__r.State, stagingUserInfoPositionList.get(0).BI_SP_User__r.Country); 
                order.BI_SP_CPF_number__c = stagingUserInfoPositionList.get(0).BI_SP_CPF_number__c;   
                order.BI_SP_Customer_address__c = stagingUserInfoPositionList.get(0).BI_SP_Customer_address__c;    
            }   
            //if the country has a fixed address   
            if(cs.BI_SP_Fixed_country_address__c != null && cs.BI_SP_Fixed_country_address__c != ''){
                order.BI_SP_Ship_to__c = cs.BI_SP_Fixed_country_address__c;
            }
        }else{//else if the country is not a SAP country, the order address is necessary use the User record from the positionSalesRepMap structure
            Map<String,List<SObject>> userPositionSalesRepMap = positionSalesRepMap.get('User');    //< territoryName, List of User >  
            List<User> userPositionList = (List<User>) userPositionSalesRepMap.get(articlesPreparation.BI_SP_Preparation__r.BI_SP_Position__r.Name);            
            
            if(userPositionList != null && userPositionList.size()>0){
                order.BI_SP_Sales_rep_name__c = userPositionList.get(0).Name;
                order.BI_SP_Ship_to__c = userPositionList.get(0).EmployeeNumber;
                order.BI_SP_Sales_rep_address__c = getAddressString(userPositionList.get(0).Street, userPositionList.get(0).PostalCode, userPositionList.get(0).City, userPositionList.get(0).State, userPositionList.get(0).Country);
            }
        }
        
        return order;
    }

    /**
     * Transform an address field to String
     *
     * @param      addressField  The address field
     *
     * @return     The address string.
     */
    private String getAddressString(String street, String postalCode, String city, String state, String country){
        String result = '';
        result += ((street==null) ? '' : street+' ');
        result += ((postalCode==null) ? '' : postalCode+' ');
        result += ((city==null) ? '' : city+' ');
        result += ((state==null) ? '' : state+' ');
        result += ((country==null) ? '' : country);
        return result;
    }
    
    public class BI_SP_BatchOrderItemUpsertException extends Exception{}
}