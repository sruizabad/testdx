@isTest
private class BI_SP_BatchOrderItemUpsert_Test {
	
    public static String countryCode;

    @Testsetup
    static void setUp() {
        countryCode = 'BR';
		User pm = BI_SP_TestDataUtility.getProductManagerUser(countryCode, 0);
		User admin = BI_SP_TestDataUtility.getAdminUser(countryCode, 0);
        
        System.runAs(admin){
	        BI_SP_TestDataUtility.createCustomSettings();
	        List<Account> accounts = BI_SP_TestDataUtility.createAccounts();
	        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
	        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());

	        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
	        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

	        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
	        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
			
			List<User> userList = new List<User>();
			userList.add(pm);
			List<BI_SP_Staging_user_info__c> stagingUserList = BI_SP_TestDataUtility.createUserStagging(userList, countryCode);

	        List<BI_PL_Position__c> positions = new List<BI_PL_Position__c>([SELECT Id, Name, BI_PL_Country_code__c FROM BI_PL_Position__c]);

			List<Territory> territories = BI_SP_TestDataUtility.createTerritories(positions);
		}
    }

	@isTest static void test_orderItemUpsert_SAPCountry() {
		BI_SP_Preparation_period__c period = [SELECT Id FROM BI_SP_Preparation_period__c LIMIT 1];
		
		List<String> countryCodes = new List<String>();
		countryCodes.add(countryCode);
        Id batchId = Database.executeBatch(new BI_SP_BatchOrderItemUpsert(period, true)); 

        batchId = Database.executeBatch(new BI_SP_BatchOrderItemUpsert(period, true)); 
	}

	@isTest static void test_orderItemUpsert_NOTSAPCountry() {
		List<BI_SP_Country_settings__c> csList = new List<BI_SP_Country_settings__c>([SELECT Id, BI_SP_Is_sap_country__c, BI_SP_Country_code__c 
																						FROM BI_SP_Country_settings__c]);
		for(BI_SP_Country_settings__c cs : csList){																					
			cs.BI_SP_Is_sap_country__c = false;
		}
		update csList;

		BI_SP_Preparation_period__c period = [SELECT Id FROM BI_SP_Preparation_period__c LIMIT 1];
		List<String> countryCodes = new List<String>();
		countryCodes.add(countryCode);
        Id batchId = Database.executeBatch(new BI_SP_BatchOrderItemUpsert(period, true)); 
	}

	@isTest static void test_orderItemUpsertBad() {
		BI_SP_Preparation_period__c period = [SELECT Id FROM BI_SP_Preparation_period__c LIMIT 1];
		List<String> countryCodes = new List<String>();
		countryCodes.add(countryCode);

		List<BI_SP_Article_Preparation__c> artPrepList = new List<BI_SP_Article_Preparation__c>([SELECT Id, Name, BI_SP_Adjusted_amount_1__c, BI_SP_Adjusted_amount_2__c, BI_SP_External_id__c, 
                                                                                                            BI_SP_Amount_target__c, BI_SP_Amount_territory__c, BI_SP_Amount_strategy__c, BI_SP_Amount_e_shop__c,
                                                                                                            BI_SP_Approval_status__c, BI_SP_Status__c,
                                                                                                            BI_SP_Preparation__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Position__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Position__r.Name, 
                                                                                                            BI_SP_Preparation__r.BI_SP_Position__r.BI_PL_Country_code__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_External_id__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_Delivery_date__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_Planit_cycle__c,
                                                                                                            BI_SP_Preparation__r.BI_SP_Preparation_period__r.BI_SP_Planit_cycle__r.BI_PL_Start_date__c,
                                                                                                            BI_SP_Article__c,
                                                                                                            BI_SP_Article__r.BI_SP_External_id__c, 
                                                                                                            BI_SP_Article__r.BI_SP_Type__c,
                                                                                                            BI_SP_Article__r.BI_SP_Country_code__c, 
                                                                                                            BI_SP_Article__r.BI_SP_Raw_country_code__c,
                                                                                                            BI_SP_Article__r.BI_SP_Description__c
                                                                                                            FROM BI_SP_Article_preparation__c
                                                                                                            WHERE BI_SP_Preparation__r.BI_SP_Preparation_period__c = :period.Id]);
		artPrepList.get(0).BI_SP_Approval_status__c = 'NA';
		artPrepList.get(1).BI_SP_Amount_territory__c = 1000;		
		artPrepList.get(2).BI_SP_Approval_status__c = null;
		artPrepList.get(2).BI_SP_Adjusted_amount_2__c = 1;
		artPrepList.get(2).BI_SP_Amount_target__c = 1000;

		update artPrepList;
		
        Id batchId = Database.executeBatch(new BI_SP_BatchOrderItemUpsert(period, true)); 
	}
}