/**
 * Batch for Order Sending (Orders consumed by Skyvva)
 */
global class BI_SP_BatchOrdersSkyvva implements Database.Batchable<sObject>, Schedulable  {
	
	String query;

	/**
	 * Batch constructor that make up the query which returns the items 
	 * to be send by skyvva
	 * 
	 */
	global BI_SP_BatchOrdersSkyvva() {
		query = 'SELECT skyvvasolutions__Related_To__c, skyvvasolutions__Integration__r.Name, Name, skyvvasolutions__SKYVVA_EXTERNAL_ID__c, skyvvasolutions__Type__c, skyvvasolutions__Status__c, '+ 
			'skyvvasolutions__Target__c, skyvvasolutions__RecordLastModifiedDate__c,skyvvasolutions__Modification_Date__c, skyvvasolutions__Comment__c '+
			'FROM skyvvasolutions__IMessage__c '+
			'WHERE skyvvasolutions__Integration__r.Name = \'Boehringer Interfaces\' '+
			'AND skyvvasolutions__Target__c = \'BI_SP_Order_OUT\' '+
			'AND (skyvvasolutions__Status__c = \'Completed\' OR skyvvasolutions__Status__c = \'Failed\') '+
			'AND skyvvasolutions__Type__c = \'Outbound\'' +
			'AND skyvvasolutions__RecordLastModifiedDate__c = TODAY ';
	}	
	
	/**
     * Batch start
     *
     * @param      BC    Database.BatchableContext
     *
     * @return     Database.QueryLocator
     */
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	/**
     * Batch execute
     *
     * @param      BC    Database.BatchableContext
     * @param      scope  The scope 
     * 
     */
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {		
		List<String> orderIdsCompleted = new List<String>();
		List<String> orderIdsFailed = new List<String>();
		List<BI_SP_order__c> ordersCompleted = new List<BI_SP_order__c>();
		List<BI_SP_order__c> ordersFailed = new List<BI_SP_order__c>();

		for(skyvvasolutions__IMessage__c message : (List<skyvvasolutions__IMessage__c>) scope) {
			if((message.skyvvasolutions__Status__c).equals('Completed')){
				orderIdsCompleted.add(message.skyvvasolutions__Related_To__c);
			}else if((message.skyvvasolutions__Status__c).equals('Failed')){
				orderIdsFailed.add(message.skyvvasolutions__Related_To__c);
			}
		}

		//Actualizar primero los fallos en caso de que haya fallos y exito para la misma orden
		ordersFailed = new List<BI_SP_Order__c>([SELECT Id, BI_SP_Delivery_status__c FROM BI_SP_Order__c WHERE Id IN :orderIdsFailed]);

		for(BI_SP_Order__c order : ordersFailed) {
			order.BI_SP_Delivery_status__c = '4';
		}

		update ordersFailed;

		ordersCompleted = new List<BI_SP_Order__c>([SELECT Id, BI_SP_Delivery_status__c FROM BI_SP_Order__c WHERE Id IN :orderIdsCompleted]);

		for(BI_SP_Order__c order : ordersCompleted) {
			order.BI_SP_Delivery_status__c = '2';
		}

		update ordersCompleted;
	}
	
	/**
     * Batch finish
     *
     * @param      BC    Database.BatchableContext
     *
     */
	global void finish(Database.BatchableContext BC) {}
	
	/**
	 * Batch schedule execute
	 *
	 * @param      sc    SchedulableContext
	 *
	 */
	global void execute(SchedulableContext sc){
		Database.executeBatch(new BI_SP_BatchOrdersSkyvva());
	}
}