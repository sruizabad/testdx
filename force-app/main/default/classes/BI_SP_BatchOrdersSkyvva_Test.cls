@isTest
private class BI_SP_BatchOrdersSkyvva_Test {
    public static String countryCode;
	
	@testsetup static void setup() {
		//List<Product_vod__c> prods = BI_SP_TestDataUtility.createProducts('MX');
		//List<BI_SP_Article__c> art = BI_SP_TestDataUtility.createArticles(prods, 'MX', 'MX10');
        countryCode = 'BR';

        BI_SP_TestDataUtility.createCustomSettings();
        List<Account> accounts = BI_SP_TestDataUtility.createAccounts();
        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());

        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
        List<BI_SP_Order__c> orders = BI_SP_TestDataUtility.createOrders(countryCode, artPreps);

		skyvvasolutions__Integration__c inte = new skyvvasolutions__Integration__c(Name = 'Boehringer Interfaces');
		insert inte;

		skyvvasolutions__Interfaces__c iface = new skyvvasolutions__Interfaces__c(
			skyvvasolutions__Name__c = 'BI_SP_Order_OUT', 
			skyvvasolutions__Integration__c = inte.Id, 
			skyvvasolutions__Source_Name__c = 'BI_SP_Order__c');
		insert iface;

		skyvvasolutions__IMessage__c msg = new skyvvasolutions__IMessage__c(
			skyvvasolutions__RecordLastModifiedDate__c = Date.today(),
			skyvvasolutions__Type__c = 'Outbound',
			skyvvasolutions__Status__c = 'Completed',
			skyvvasolutions__Interface__c = iface.Id,
			skyvvasolutions__Integration__c = inte.Id,
			skyvvasolutions__Related_To__c = orders[0].Id
		);

		insert msg;

		skyvvasolutions__IMessage__c msg2 = new skyvvasolutions__IMessage__c(
			skyvvasolutions__RecordLastModifiedDate__c = Date.today(),
			skyvvasolutions__Type__c = 'Outbound',
			skyvvasolutions__Status__c = 'Failed',
			skyvvasolutions__Interface__c = iface.Id,
			skyvvasolutions__Integration__c = inte.Id,
			skyvvasolutions__Related_To__c = orders[1].Id
		);

		insert msg2;
	}	

	@isTest static void testBatch() {
		Test.startTest();
		Database.executeBatch(new BI_SP_BatchOrdersSkyvva());
		Test.stopTest();
	}
	
	@isTest static void testSchedulable() {
		Test.startTest();
		System.schedule('Test schedule', '0 0 23 * * ?', new BI_SP_BatchOrdersSkyvva());
		Test.stopTest();
	}
}