global without sharing class BI_SP_BatchStagingUserInfoUpdate implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

	Map<Id,BI_SP_Staging_user_info__c> stagingListToDeleteUser = new Map<Id,BI_SP_Staging_user_info__c>();

	public BI_SP_BatchStagingUserInfoUpdate() {		
	}

	/**
     * Batch start
     *
     * @param      BC    Database.BatchableContext
     *
     * @return     Database.QueryLocator
     */
    global List<User> start(Database.BatchableContext BC) {
    	DateTime nowHour =  DateTime.now();
		DateTime d3hAgo = nowHour.addHours(-3);
        List<User> lastUpdatedUsers = new List<User>([SELECT Id, EmployeeNumber, Country_Code_BI__c
                                                        FROM User
                                                        WHERE LastModifiedDate < :nowHour AND LastModifiedDate > :d3hAgo AND IsActive = true]);

        return lastUpdatedUsers;
    }

    /**
     * Batch execute
     *
     * @param      BC    Database.BatchableContext
     * @param      scope  The scope
     * 
     */
    global void execute(Database.BatchableContext BC, List<User> scope) {
    	List<BI_SP_Staging_user_info__c> clearStaging = new List<BI_SP_Staging_user_info__c>();
    	for(BI_SP_Staging_user_info__c sui : [SELECT Id, BI_SP_User__c FROM BI_SP_Staging_user_info__c WHERE BI_SP_User__c IN :scope]){
    		sui.BI_SP_User__c = null;
    		clearStaging.add(sui);
    	}
    	update clearStaging;

    	//List<String> allEmployeeNumbers = new List<String>();
    	//Map<String, User> employeeNot0Country_User = new Map<String, User>();
    	List<BI_SP_Staging_user_info__c> updateStaging = new List<BI_SP_Staging_user_info__c>();
        for(User u : scope){
            if(u.EmployeeNumber != null){
                String userEmployeeNumberString = '%'+getCorrectEmployeeNumber(u.EmployeeNumber);
                for(BI_SP_Staging_user_info__c sui : [SELECT Id, BI_SP_Ship_to__c, BI_SP_Raw_country_code__c, BI_SP_User__c FROM BI_SP_Staging_user_info__c WHERE BI_SP_Ship_to__c LIKE :userEmployeeNumberString]){
                    if(getCorrectEmployeeNumber(u.EmployeeNumber) == getCorrectEmployeeNumber(sui.BI_SP_Ship_to__c) && u.Country_Code_BI__c == sui.BI_SP_Raw_country_code__c){
                        sui.BI_SP_User__c = u.Id;
                        updateStaging.add(sui);
                        break;
                    }
                }
            }
        }

    	update updateStaging;
    }

    /**
     * Batch finish
     *
     * @param      BC    Database.BatchableContext
     *
     */
    global void finish(Database.BatchableContext BC) {}

    /**
	 * Gets the correct employee number( (removes all starts zeros).
	 *
	 * @param      shipTo  The ship to
	 *
	 * @return     The correct employee number.
	 */
	private String getCorrectEmployeeNumber(String employeeNumber){
		while(employeeNumber.startsWith('0')){
			employeeNumber = employeeNumber.removeStart('0');
		}
		return employeeNumber;
	}

    global void execute(SchedulableContext sc){
    	BI_SP_BatchStagingUserInfoUpdate b = new BI_SP_BatchStagingUserInfoUpdate();
      	Database.executebatch(b, 50);
    }
}