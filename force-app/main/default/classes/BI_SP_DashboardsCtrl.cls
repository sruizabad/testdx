/**
 * Dashboards Control
 */
public without sharing class BI_SP_DashboardsCtrl {

	public String userPermissionsJSON {get;set;}

	/**
	 * Constructs the object.
	 */
	public BI_SP_DashboardsCtrl() {
		userPermissionsJSON = JSON.serialize(BI_SP_SamproServiceUtility.getCurrentUserPermissions());
	}

	/**
	 * Gets the filters.
	 *
	 * @return     The filters. < String indicate the filter, values >
	 */
	@RemoteAction
	public static Map<String, Object> getFilters() {
		Map<String, Object> toReturn = new Map<String, Object>();

		Map<String, Boolean> permissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();

		if(permissions.get('isRB')){
			toReturn.put('regionsForPeriod', JSON.serialize(new List<String>(BI_SP_SamproServiceUtility.getSetReportBuilderRegionCodes())));
			toReturn.put('countriesForPeriod', JSON.serialize(new List<String>()));
			toReturn.put('countriesForProduct', JSON.serialize(new List<String>()));			
			toReturn.put('articleTypes', JSON.serialize(BI_SP_SamproServiceUtility.getListReportBuilderArticleTypes(BI_SP_Article__c.BI_SP_Type__c)));
			toReturn.put('orderTypes', JSON.serialize(BI_SP_Order__c.BI_SP_Order_type_pl__c.getDescribe().getPicklistValues()));
		}else{
			toReturn.put('regionsForPeriod', JSON.serialize(new List<String>()));
			toReturn.put('countriesForPeriod', JSON.serialize(new List<String>(BI_SP_SamproServiceUtility.getSetCurrentUserRegionCountryCodes())));
			toReturn.put('countriesForProduct', JSON.serialize(new List<String>(BI_SP_SamproServiceUtility.getListCountriesForUserManagesProducts(Userinfo.getUserId()))));
			toReturn.put('articleTypes', JSON.serialize(BI_SP_SamproServiceUtility.getListCurrentUserArticleTypesByCountry(BI_SP_Article__c.BI_SP_Type__c)));
			toReturn.put('orderTypes', JSON.serialize(BI_SP_Order__c.BI_SP_Order_type_pl__c.getDescribe().getPicklistValues()));
		}
		return toReturn;
	}

    /**
     * Gets all country Codes for all region (user permission set ReportBuilder)
     *
     * @return     Set of strings containing all the country codes from all region
     */
	@RemoteAction
    public static String getCountryCodesForRegion(String region) {
        return JSON.serialize(new List<String>(BI_SP_SamproServiceUtility.getCountryCodesForRegion(region)));
    }

	/**
	 * Gets the preparations data.
	 *
	 * @param      countryCode      The country code
	 * @param      period           The period
	 * @param      selectedProduct  The selected product
	 *
	 * @return     List of preparations 
	 */
	@RemoteAction
	@ReadOnly
	public static List<BI_SP_Preparation__c> getPreparationsData(String countryCode, BI_SP_Preparation_period__c period, Product_vod__c selectedProduct) {

		Map<String, Boolean> permissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();

		String query = 'SELECT id, BI_SP_External_id__c, BI_SP_Position__c,BI_SP_Position__r.Name, BI_SP_Preparation_period__c, '+ 
					'(SELECT Id, BI_SP_Adjusted_amount_1__c,'+
						'BI_SP_Adjusted_amount_2__c,'+
						'BI_SP_Amount_e_shop__c,'+
						'BI_SP_Amount_strategy__c,'+
						'BI_SP_Amount_target__c,'+
						'BI_SP_Amount_territory__c,'+
						'BI_SP_Approval_status__c,'+
						'BI_SP_Status__c,'+
						'BI_SP_Article__c,'+
						'BI_SP_Article__r.BI_SP_External_id_1__c,'+
						'BI_SP_Article__r.BI_SP_Description__c,'+
						'BI_SP_Article__r.BI_SP_Veeva_product_family__r.Id,'+
						'BI_SP_Article__r.BI_SP_Veeva_product_family__r.Name,'+
						'toLabel(BI_SP_Article__r.BI_SP_Type__c) '+
					' FROM SAMPro_Article_Preparation__r ';

		
		if(selectedProduct != null) {
			query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c = \''+ selectedProduct.Id + '\')'; 
		} else {
			if(permissions.get('isDS') || permissions.get('isSysAdmin')) {
				query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c != null)'; 
			}
			else if(permissions.get('isPM')) {
				String productQuery = '';
				for(Product_vod__c p : BI_SP_SamproServiceUtility.getListProductsAllowedToDistribute(Userinfo.getUserid())) {
					productQuery += '\'' + p.Id + '\',';
				}

				productQuery = productQuery.removeEnd(',');

				if(productQuery != '') {
					query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c IN ('+ productQuery + '))'; 
				} else {
					query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c != null)'; 
				}
			} else {
				query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c != null)'; 
			}
		}		

		query += ' FROM BI_SP_Preparation__c ';

		query += ' WHERE BI_SP_Preparation_period__c = \''+ period.Id + '\''; 
		
		return (List<BI_SP_Preparation__c> ) Database.query(query);			
	}

	/**
	 * Gets the orders data.
	 *
	 * @param      countryCode         The country code
	 * @param      period              The period
	 * @param      selectedProduct     The selected product
	 * @param      forSpecialShipment  For special shipment
	 * @param      beginDeliveryDate   The begin delivery date
	 * @param      endDeliveryDate     The end delivery date
	 * @param      ssOwnerId           The ss owner identifier
	 * @param      ssType              The ss type
	 *
	 * @return     List of orders
	 */
	@RemoteAction
	@ReadOnly
	public static List<BI_SP_Order__c> getOrdersData(String countryCode, BI_SP_Preparation_period__c period, Product_vod__c selectedProduct, Boolean forSpecialShipment, String beginDeliveryDate, String endDeliveryDate, String ssOwnerId, String ssType) {

		Boolean ss = forSpecialShipment != null ? forSpecialShipment : false;

		Map<String, Boolean> permissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();

		String query = 'SELECT id, BI_SP_External_id__c, BI_SP_Territory_name__c, toLabel(BI_SP_Order_type_pl__c), BI_SP_Country_code__c, BI_SP_Article_type__c, BI_SP_Sales_rep_name__c, BI_SP_Special_shipment__r.Owner.Name, '+ 
					' (SELECT Id, BI_SP_Article_external_id_1__c,'+
						'BI_SP_Family_name__c,'+
						'BI_SP_Order_item_number__c,'+
						'BI_SP_Quantity__c,'+
						'BI_SP_Article_preparation__c,'+
						'BI_SP_Article__r.BI_SP_Description__c,'+
						'BI_SP_Article__r.BI_SP_External_id_1__c,'+
						'BI_SP_Article__r.BI_SP_Veeva_product_family__r.Name,'+
						'BI_SP_Special_shipment__c,'+
						'BI_SP_Article_preparation__r.BI_SP_Amount_strategy__c,'+
						'BI_SP_Article_preparation__r.BI_SP_Amount_territory__c,'+
						'BI_SP_Article_preparation__r.BI_SP_Amount_target__c,'+
						'toLabel(BI_SP_Article_preparation__r.BI_SP_Article__r.BI_SP_Type__c), '+
						'BI_SP_Article_preparation__r.BI_SP_Article__r.BI_SP_Veeva_product_family__r.Name '+
					' FROM SAMPro_Order_Items__r ';

		if(selectedProduct != null) {
			query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c = \''+ selectedProduct.Id + '\')'; 
		} else {
			if(permissions.get('isDS') || permissions.get('isSysAdmin') || permissions.get('isRB')) {
				query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c != null)'; 
			}
			else if(permissions.get('isPM')) {
				String productQuery = '';
				for(Product_vod__c p : BI_SP_SamproServiceUtility.getListProductsAllowedToDistribute(Userinfo.getUserid())) {
					productQuery += '\'' + p.Id + '\',';
				}

				productQuery = productQuery.removeEnd(',');
				if(productQuery != '') {
					query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c IN ('+ productQuery + '))'; 
				} else {
					query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c != null)'; 
				}
			} else {
				query += ' WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c != null)'; 
			}
		}

		query += ' FROM BI_SP_Order__c ';

		if(ss) {
			query += ' WHERE BI_SP_Special_shipment__r.BI_SP_Sales_Rep_Country__c = \''+countryCode+'\'';

			if(ssType != null) {
				query += ' AND BI_SP_Order_type_pl__c = \''+ssType+'\''; 
			} else {
				query += ' AND BI_SP_Order_type_pl__c != \'AP\''; 
			}
			if(beginDeliveryDate != null) {
            	query += ' AND BI_SP_Delivery_date__c >= ' + beginDeliveryDate.split('T')[0];
			}
			if(endDeliveryDate != null) {
            	query += ' AND BI_SP_Delivery_date__c <= ' + endDeliveryDate.split('T')[0];
			}
			if(ssOwnerId != null) {
            	query += ' AND BI_SP_Special_shipment__r.OwnerId = \'' + ssOwnerId + '\'';
			}

		} else {
			query += ' WHERE BI_SP_Order_type_pl__c = \'AP\' AND BI_SP_Preparation_period__c = \''+ period.Id + '\''; 
		}
		
		return (List<BI_SP_Order__c> ) Database.query(query);			
	}

	/**
	 * Gets the inventory data.
	 *
	 * @param      selectedProduct  The selected product
	 *
	 * @return     List of articles.
	 */
	@RemoteAction
	@ReadOnly
	public static List<BI_SP_Article__c> getInventoryData(Product_vod__c selectedProduct) {
		return new List<BI_SP_Article__c>([SELECT Id, BI_SP_Description__c, BI_SP_Stock__c, BI_SP_External_stock__c, BI_SP_External_id_1__c, toLabel(BI_SP_Type__c) FROM BI_SP_Article__c WHERE BI_SP_Veeva_product_family__c = :selectedProduct.Id AND BI_SP_IS_Active__c = true]);
	}

	/**
	 * Gets the preparation periods.
	 *
	 * @param      countryCode  The country code
	 *
	 * @return     List of preparation periods.
	 */
	@RemoteAction
	public static List<BI_SP_Preparation_period__c> getPreparationPeriods(String countryCode) {
		return BI_SP_SamproServiceUtility.getListPeriodsByCountry(countryCode, Userinfo.getUserId());
	}  

	/**
	 * Gets the product families filter.
	 *
	 * @param      regioncode   The regioncode
	 * @param      countrycode  The countrycode
	 * @param      period       The period
	 *
	 * @return     List of products.
	 */
	@RemoteAction
	@ReadOnly
	public static List<Product_vod__c> getProductFamiliesFilter(String regioncode, String countrycode, BI_SP_Preparation_period__c period) {
		Map<String, Boolean> permissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();

		if(permissions.get('isRB')){
			if(period != null) { //los productos del periodo seleccionado
        		return BI_SP_SamproServiceUtility.getListProductsInPeriod(period);
			} else if(countrycode != null) { //los productos del pais
        		return BI_SP_SamproServiceUtility.getListProductsInCountry(countrycode);
			} else if(regioncode != null) { //los productos de la region
        		return BI_SP_SamproServiceUtility.getListProductsInRegion(regioncode);
			} else { //los productos de la region
        		return BI_SP_SamproServiceUtility.getListAllProducts();
			}
		} else if(permissions.get('isPM') && !permissions.get('isDS') && !permissions.get('isCS')){
			if(period != null) {
        		return BI_SP_SamproServiceUtility.getListProductsAllowedToDistributeInCountry(countryCode, Userinfo.getUserId());
			} else {
        		return BI_SP_SamproServiceUtility.getListProductsAllowedToDistribute(Userinfo.getUserId());
			}
		} else {
			if(period != null) {
				return BI_SP_SamproServiceUtility.getListProductsInPeriod(period);
			} else {
				Set<String> userRegionCountryCodes = BI_SP_SamproServiceUtility.getRegionCountryCodes(BI_SP_SamproServiceUtility.getCurrentUserCountryCode());

				return new List<Product_vod__c>([
	                SELECT Id, Name, External_id_vod__c, Country_code_bi__c, Active_BI__c
	                FROM Product_vod__c
	                WHERE Country_code_bi__c IN :userRegionCountryCodes
	                AND Product_Type_vod__c = 'Detail'
	                AND Id IN (SELECT BI_SP_Veeva_product_family__c FROM BI_SP_Article__c)]);
				/*return new List<Product_vod__c>([
	                SELECT Id, Name, External_id_vod__c, Country_code_bi__c
	                FROM Product_vod__c
	                WHERE Country_code_bi__c IN :userRegionCountryCodes
	                AND Product_Type_vod__c = 'Detail'
	                AND Active_BI__c = true
	                AND Id IN (SELECT BI_SP_Veeva_product_family__c FROM BI_SP_Article__c)]);*/
			}
		}
	} 

	/**
	 * Gets the users lookup result.
	 *
	 * @param      regioncode   The regioncode
	 * @param      countryCode  The country code
	 *
	 * @return     List of users
	 */
	@RemoteAction
	@ReadOnly
	public static List<User> getUsersLookupResult(String regioncode, String countryCode) {
		List<User> users = new List<User>();	
		
		Map<String, Boolean> permissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
		if(permissions.get('isRB')){
			if(countrycode != null) { //los product manager del pais
				users = BI_SP_SamproServiceUtility.getProductManagersOfCountry(countryCode).values();
			} else if(regioncode != null) { //los product manager de la region
        		Set<String> countryCodes = BI_SP_SamproServiceUtility.getCountryCodesForRegion(regioncode);
        		users = BI_SP_SamproServiceUtility.getProductManagersOfRegion(countryCodes).values();
			} else { //los product manager de cualquier region
				Set<String> countryCodes = BI_SP_SamproServiceUtility.getAllRegionCountryCodes();
        		users = BI_SP_SamproServiceUtility.getProductManagersOfRegion(countryCodes).values();
			}
		}else{
			users = BI_SP_SamproServiceUtility.getProductManagersOfRegion(BI_SP_SamproServiceUtility.getSetCurrentUserRegionCountryCodes()).values();	
		}
		return users;
	}

	/**
	 * Delete all article preparations for the article and period selected
	 *
	 * @param      article  The article
	 * @param      period   The period
	 *
	 * @return     result
	 */
	@RemoteAction
	public static Boolean deleteByArticle(BI_SP_Article__c article, BI_SP_Preparation_period__c period){
		try {
			delete new List<BI_SP_Article_Preparation__c>([
				SELECT Id FROM BI_SP_Article_Preparation__c 
				WHERE BI_SP_Article__c =: article.Id
				AND BI_SP_Preparation__r.BI_SP_Preparation_period__c =: period.Id
			]);
			return true;
		} catch(Exception e) {
			System.debug(LoggingLevel.ERROR, '### - BI_SP_DashboardsCtrl - deleteByArticle - BI_SP_Article_Preparation__c error -> ' + e);
			return false;
		}
	}

	/**
	 * Delete all article preparations for the position and period selected
	 *
	 * @param      position  The position
	 * @param      period   The period
	 *
	 * @return     result
	 */
	@RemoteAction
	public static Boolean deleteByTerritory(BI_PL_Position__c position, BI_SP_Preparation_period__c period) {	
		try {
			delete new List<BI_SP_Article_Preparation__c>([
				SELECT Id FROM BI_SP_Article_Preparation__c 
				WHERE BI_SP_Preparation__r.BI_SP_Position__c =: position.Id
				AND BI_SP_Preparation__r.BI_SP_Preparation_period__c =: period.Id
			]);
			return true;
		} catch(Exception e) {
			System.debug(LoggingLevel.ERROR, '### - BI_SP_DashboardsCtrl - deleteByTerritory - BI_SP_Article_Preparation__c error -> ' + e);
			return false;
		}
	}
}