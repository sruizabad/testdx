@isTest
private class BI_SP_DashboardsCtrl_Test {

	static String regionCode = 'BR';
	static String countryCode = 'BR';

	@Testsetup
    static void setUp() {
    	User salesRep = BI_SP_TestDataUtility.getSalesRepUser(countryCode, 0);
		User pm = BI_SP_TestDataUtility.getProductManagerUser(countryCode, 0);
		User pm2 = BI_SP_TestDataUtility.getProductManagerUser(countryCode, 1);
		User cs = BI_SP_TestDataUtility.getClientServiceUser(countryCode, 0);
		User admin = BI_SP_TestDataUtility.getAdminUser(countryCode, 0);
		User reportb = BI_SP_TestDataUtility.getReportBuilderUser(countryCode, 0);
        
        System.runAs(admin){
	        BI_SP_TestDataUtility.createCustomSettings();
	        List<Customer_Attribute_BI__c> specs = BI_SP_TestDataUtility.createSpecialties();
	        List<Account> accounts = BI_SP_TestDataUtility.createAccounts(specs);
	        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
	        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());
	        List<BI_SP_Product_family_assignment__c> productsAssigmentsPM = BI_SP_TestDataUtility.createProductsAssigments(products, pm.Id);


	        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
	        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

	        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
	        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
        }
    }

	@isTest static void testDashboardsPreparations() {
		Test.startTest();

		BI_SP_DashboardsCtrl ctrl = new BI_SP_DashboardsCtrl();
		Map<String, Object> filters = BI_SP_DashboardsCtrl.getFilters();
		String countriesForRegion = BI_SP_DashboardsCtrl.getCountryCodesForRegion(regionCode);

		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        Product_vod__c selectedProduct = [Select Id from Product_vod__c limit 1];

		List<BI_SP_Preparation__c> preps = BI_SP_DashboardsCtrl.getPreparationsData(countryCode, period, selectedProduct);
		List<BI_SP_Preparation__c> preps2 = BI_SP_DashboardsCtrl.getPreparationsData(countryCode, period, null);

		//As PM
		User pmUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM0%' LIMIT 1];
		System.runAs(pmUser) {
			Map<String, Object> filters2 = BI_SP_DashboardsCtrl.getFilters();
			List<Product_vod__c> prods; 
			preps = BI_SP_DashboardsCtrl.getPreparationsData(countryCode, period, null);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, period);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, null);
			List<User> lookupUsers = BI_SP_DashboardsCtrl.getUsersLookupResult(regionCode, countryCode);
		}

		//As Admin
		User admUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Admin') AND Username Like 'userAD%' LIMIT 1];
		System.runAs(admUser) {
			List<Product_vod__c> prods; 
			preps = BI_SP_DashboardsCtrl.getPreparationsData(countryCode, period, null);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, period);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, null);
		}
		
		Test.stopTest();
	}

	@isTest static void testDashboardsPreparations2() {
		Test.startTest();

		BI_SP_DashboardsCtrl ctrl = new BI_SP_DashboardsCtrl();

		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];

		List<BI_SP_Preparation__c> preps;
		List<Product_vod__c> prods; 

		//As PM2
		User pmUser2 = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM1%' LIMIT 1];
		System.runAs(pmUser2) {
			Map<String, Object> filters2 = BI_SP_DashboardsCtrl.getFilters();
			preps = BI_SP_DashboardsCtrl.getPreparationsData(countryCode, period, null);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, period);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, null);
			List<User> lookupUsers = BI_SP_DashboardsCtrl.getUsersLookupResult(regionCode, countryCode);
		}

		//As SalesRep
		User salesRepUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_SALES') AND Username Like 'userSR%' LIMIT 1];
		System.runAs(salesRepUser) {
			preps = BI_SP_DashboardsCtrl.getPreparationsData(countryCode, period, null);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, period);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, null);
		}

		//As RB
		User reportbUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_REPORT_BUILDER') AND Username Like 'userRB%' LIMIT 1];
		System.runAs(reportbUser) {
			Map<String, Object> filters = BI_SP_DashboardsCtrl.getFilters();

			preps = BI_SP_DashboardsCtrl.getPreparationsData(countryCode, period, null);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, period);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, countrycode, null);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(regionCode, null, null);
			prods = BI_SP_DashboardsCtrl.getProductFamiliesFilter(null, null, null);
		}
		
		Test.stopTest();
	}
	
	@isTest static void testDashboardsOrders() {
		List<BI_SP_Preparation_period__c> periods = BI_SP_DashboardsCtrl.getPreparationPeriods(countryCode);
		
		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        Product_vod__c selectedProduct = [Select Id from Product_vod__c limit 1];
		List<BI_SP_Order__c> orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, false, null, null, null, null);

		//As PM
		User pmUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM0%' LIMIT 1];
		System.runAs(pmUser) {
			List<Product_vod__c> prods; 
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, false, null, null, null, null);
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, false, null, null, null, null);
		}

		//As PM
		User pmUser2 = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM1%' LIMIT 1];
		System.runAs(pmUser2) {
			List<Product_vod__c> prods; 
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, false, null, null, null, null);
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, false, null, null, null, null);
		}

		//As Admin
		User admUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Admin') AND Username Like 'userAD%' LIMIT 1];
		System.runAs(admUser) {
			List<Product_vod__c> prods; 
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, false, null, null, null, null);
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, false, null, null, null, null);
		}

		//As RB
		User reportbUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_REPORT_BUILDER') AND Username Like 'userRB%' LIMIT 1];
		System.runAs(reportbUser) {
			List<Product_vod__c> prods; 
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, false, null, null, null, null);
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, false, null, null, null, null);
		}
	}

	@isTest static void testDashboardsUsersLookup() {
		List<User> users1 = BI_SP_DashboardsCtrl.getUsersLookupResult(regionCode, countryCode);

		//As PM
		User pmUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM0%' LIMIT 1];
		System.runAs(pmUser) {
			List<User> users; 
			users = BI_SP_DashboardsCtrl.getUsersLookupResult(regionCode, countryCode);
		}

		//As Admin
		User admUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Admin') AND Username Like 'userAD%' LIMIT 1];
		System.runAs(admUser) {
			List<User> users; 
			users = BI_SP_DashboardsCtrl.getUsersLookupResult(regionCode, countryCode);
		}

		//As RB
		User reportbUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_REPORT_BUILDER') AND Username Like 'userRB%' LIMIT 1];
		System.runAs(reportbUser) {
			List<User> users; 
			users = BI_SP_DashboardsCtrl.getUsersLookupResult(regionCode, countryCode);
			users = BI_SP_DashboardsCtrl.getUsersLookupResult(regionCode, null);
			users = BI_SP_DashboardsCtrl.getUsersLookupResult(null, null);
		}
	}

	@isTest static void testDashboardsSpecialShipment() {
		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        Product_vod__c selectedProduct = [Select Id from Product_vod__c limit 1];
		List<BI_SP_Order__c> orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, true, null, null, null, null);
		List<BI_SP_Order__c> orders2 = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, true, null, null, null, null);

		//As PM
		User pmUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM0%' LIMIT 1];
		System.runAs(pmUser) {
			List<Product_vod__c> prods; 
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, true, null, null, null, null);
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, 
				true, '2017-01-01T00:00:00Z', '2017-01-01T00:00:00Z', pmUser.Id, 'SS');
		}

		//As PM2
		User pmUser2 = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM1%' LIMIT 1];
		System.runAs(pmUser) {
			List<Product_vod__c> prods; 
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, true, null, null, null, null);
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, 
				true, '2017-01-01T00:00:00Z', '2017-01-01T00:00:00Z', pmUser2.Id, 'SS');
		}
	}

	@isTest static void testDashboardsSpecialShipment2() {
		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        Product_vod__c selectedProduct = [Select Id from Product_vod__c limit 1];
		List<BI_SP_Order__c> orders;
		List<BI_SP_Order__c> orders2;

		//As Admin
		User admUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Admin') AND Username Like 'userAD%' LIMIT 1];
		System.runAs(admUser) {
			List<Product_vod__c> prods; 
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, false, null, null, null, null);
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, 
				true, '2017-01-01T00:00:00Z', '2017-01-01T00:00:00Z', admUser.Id, 'SS');
		}

		//As RB
		User reportbUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_REPORT_BUILDER') AND Username Like 'userRB%' LIMIT 1];
		System.runAs(reportbUser) {
			List<Product_vod__c> prods; 
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, false, null, null, null, null);
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, 
				true, '2017-01-01T00:00:00Z', '2017-01-01T00:00:00Z', reportbUser.Id, 'SS');
		}

		//As SR
		User salesRepUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_SALES') AND Username Like 'userSR%' LIMIT 1];
		System.runAs(salesRepUser) {
			List<Product_vod__c> prods; 
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  selectedProduct, false, null, null, null, null);
			orders = BI_SP_DashboardsCtrl.getOrdersData(countryCode, period,  null, 
				true, '2017-01-01T00:00:00Z', '2017-01-01T00:00:00Z', salesRepUser.Id, 'SS');
		}
	}

	@isTest static void testDashboardsInventory() {
		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        Product_vod__c selectedProduct = [Select Id from Product_vod__c limit 1];
		List<BI_SP_Article__c> inventory = BI_SP_DashboardsCtrl.getInventoryData(selectedProduct);
	}

	@isTest static void testDashboardsDelete() {
		BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        BI_SP_Article__c selectedArticle = [Select Id from BI_SP_Article__c limit 1];
		Boolean aux = BI_SP_DashboardsCtrl.deleteByArticle(selectedArticle, period);
		aux = BI_SP_DashboardsCtrl.deleteByArticle(null, period);

		BI_PL_Position__c selectedPosition = [Select Id from BI_PL_Position__c limit 1];
		Boolean aux2 = BI_SP_DashboardsCtrl.deleteByTerritory(selectedPosition, period);
		aux2 = BI_SP_DashboardsCtrl.deleteByTerritory(null, period);
	}
}