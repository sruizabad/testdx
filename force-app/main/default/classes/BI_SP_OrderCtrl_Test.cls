@isTest
private class BI_SP_OrderCtrl_Test {
	
	static String regionCode = 'BR';
	static String countryCode = 'BR';

	@Testsetup
    static void setUp() {
		User pm = BI_SP_TestDataUtility.getProductManagerUser(countryCode, 0);
		User admin = BI_SP_TestDataUtility.getAdminUser(countryCode, 0);

        System.runAs(admin){
	        BI_SP_TestDataUtility.createCustomSettings();
	        List<Customer_Attribute_BI__c> specs = BI_SP_TestDataUtility.createSpecialties();
	        List<Account> accounts = BI_SP_TestDataUtility.createAccounts(specs);
	        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
	        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());
	        List<BI_SP_Product_family_assignment__c> productsAssigmentsPM = BI_SP_TestDataUtility.createProductsAssigments(products, pm.Id);

	        List<BI_SP_Staging_user_info__c> stag =  BI_SP_TestDataUtility.createUserStagging(
	        	new List<User> {pm, admin}, 
	        	countryCode);

	        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
	        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

	        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
	        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);

	        List<BI_PL_Position__c> positions = new List<BI_PL_Position__c>([SELECT Id, Name, BI_PL_Country_code__c FROM BI_PL_Position__c]);

			List<Territory> territories = BI_SP_TestDataUtility.createTerritories(positions);
        }
    }

	@isTest static void testInitAndFilters() {
		Test.startTest();

		BI_SP_OrderCtrl ctrl = new BI_SP_OrderCtrl();
		List<BI_SP_Preparation_period__c>ps = BI_SP_OrderCtrl.getPeriods(countryCode);
		List<BI_SP_Preparation_period__c>allps = BI_SP_OrderCtrl.getAllPeriods(countryCode);
		BI_SP_OrderCtrl.FiltersModel filters = BI_SP_OrderCtrl.getFilters(null);
		filters = BI_SP_OrderCtrl.getFilters(countryCode);
		String ots =  BI_SP_OrderCtrl.getOrdersTypes();

		String getOrderArticleTypes = BI_SP_OrderCtrl.getOrderArticleTypes(countryCode);
		String getDeliveryStatus = BI_SP_OrderCtrl.getDeliveryStatus(countryCode);
		getDeliveryStatus = BI_SP_OrderCtrl.getDeliveryStatus('test');
		String getUserCountryCodes = BI_SP_OrderCtrl.getUserCountryCodes(true, true, regionCode);
		String getUserRegionCodes = BI_SP_OrderCtrl.getUserRegionCodes(true);

		Test.stopTest();
	}
	
	@isTest static void testCreateOrders() {
		Test.startTest();
		BI_SP_Preparation_period__c period = [SELECT Id FROM BI_SP_Preparation_period__c LIMIT 1];
		String res = BI_SP_OrderCtrl.createOrders(period);
		Test.stopTest();

		User admUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Admin') AND Username Like 'userAD%' LIMIT 1];
        System.runAs(admUser){
			String d1 = (DateTime.now() - 20).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
			String d2 = (DateTime.now() + 30).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');

			BI_SP_OrderCtrl.OrderResponse resp =  BI_SP_OrderCtrl.getOrders('AP', null, null, null, null, null, countryCode, period, null);

			BI_SP_OrderCtrl.OrderResponse resp2 =  BI_SP_OrderCtrl.getOrders('AP', '1', '1', d1, d2, null, countryCode, period, null);
			BI_SP_OrderCtrl.OrderResponse resp3 =  BI_SP_OrderCtrl.getOrders('SS', '1', '1', d1, d2, null, countryCode, period, null);

			BI_SP_OrderCtrl.OrderResponse resp4 =  BI_SP_OrderCtrl.getOrders('MSEX', '1', '1', d1, d2, null, null, null, null);
			BI_SP_OrderCtrl.OrderResponse resp5 =  BI_SP_OrderCtrl.getOrders('MSEX', '1', '1', d1, d2, regionCode, null, null, null);
			BI_SP_OrderCtrl.OrderResponse resp6 =  BI_SP_OrderCtrl.getOrders('AP', null, null, null, null, null, null, null, null);

		}
	}

	
	@isTest static void testValidateOrders() {
		BI_SP_Country_settings__c cs = [SELECT BI_SP_PM_Order_reason__c, BI_SP_MS_Order_reason__c, BI_SP_MS_Order_type__c, BI_SP_PL_Order_reason__c, BI_SP_PL_Order_type__c, 
                                                BI_SP_PM_Order_type__c, BI_SP_Country_code__c, BI_SP_External_system_country_code__c, BI_SP_Fixed_country_address__c, 
                                                BI_SP_Is_sap_country__c, BI_SP_Is_zenatur_country__c
                                                FROM BI_SP_Country_settings__c
                                                WHERE BI_SP_Country_code__c = :countryCode
                                                LIMIT 1];
		
		Test.startTest();
		BI_SP_Preparation_period__c period = [SELECT Id FROM BI_SP_Preparation_period__c LIMIT 1];
		String res = BI_SP_OrderCtrl.createOrders(period);
		Test.stopTest();

		User admUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Admin') AND Username Like 'userAD%' LIMIT 1];
        System.runAs(admUser){

			cs.BI_SP_Is_sap_country__c = false;
			cs.BI_SP_Is_zenatur_country__c = false;
			update cs;
			BI_SP_OrderCtrl.OrderResponse resp =  BI_SP_OrderCtrl.getOrders('AP', null, null, null, null, null, countryCode, period, null);

			cs.BI_SP_Is_sap_country__c = true;
			update cs;
			resp =  BI_SP_OrderCtrl.getOrders('AP', null, null, null, null, null, countryCode, period, null);

			cs.BI_SP_Is_zenatur_country__c = true;
			update cs;
			resp =  BI_SP_OrderCtrl.getOrders('AP', null, null, null, null, null, countryCode, period, null);

			cs.BI_SP_Is_sap_country__c = false;
			update cs;
			resp =  BI_SP_OrderCtrl.getOrders('AP', null, null, null, null, null, countryCode, period, null);

		}
	}

	@isTest static void testSendOrders() {
		Test.startTest();
		BI_SP_Preparation_period__c period = [SELECT Id FROM BI_SP_Preparation_period__c LIMIT 1];
		String resCreate = BI_SP_OrderCtrl.createOrders(period);
		Test.stopTest();

		User admUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Admin') AND Username Like 'userAD%' LIMIT 1];
        System.runAs(admUser){
			String d1 = (DateTime.now() - 20).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
			String d2 = (DateTime.now() + 30).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');

			BI_SP_OrderCtrl.OrderResponse resp =  BI_SP_OrderCtrl.getOrders('AP', null, null, null, null, null, countryCode, period, null);
			String res = BI_SP_OrderCtrl.sendOrders(resp.orderModels);


			String cancel = BI_SP_OrderCtrl.cancelOrders(resp.orderModels);
		}
	}
}