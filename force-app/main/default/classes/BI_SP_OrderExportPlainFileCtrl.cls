public without sharing class BI_SP_OrderExportPlainFileCtrl {
	
	public string header{get;set;}
    public List<Schema.PicklistEntry> artTypes {get; set;}
    public List<wrapper> lstwrapper {get; set;}
    public class wrapper{
        public string name {get; set;}
        public string artType{get; set;}
        public string territory{get; set;}
        public string artExtId1{get; set;}
        public string famName{get; set;} 
        public string artDesc{get; set;}
        public string quantity{get; set;}
        public string srName{get; set;}
        public string shipTo{get; set;}
        public string srAddress {get; set;}
        public string orderReason{get; set;}
        public string orderType{get; set;}  
        public string delivDate{get; set;}  
        public string ssDesc{get; set;}         
    }

	public String orderType{get;set;}
    public String shipmentType{get;set;}
	public String articleType{get;set;}
	public String deliveryStatus{get;set;}
	public String deliveryDate{get;set;}
	public String orderDate{get;set;}
	public String countryCode{get;set;}
	public String periodId{get;set;}  

    public String selectedItems{get;set;}

	public BI_SP_OrderExportPlainFileCtrl() {
            lstwrapper = new List<wrapper>();
            artTypes = new List<Schema.PicklistEntry>();
            artTypes = BI_SP_Order__c.BI_SP_Article_type__c.getDescribe().getPicklistValues();
            orderType = ApexPages.currentPage().getParameters().get('orderType');
            shipmentType = ApexPages.currentPage().getParameters().get('shipmentType');
            articleType = ApexPages.currentPage().getParameters().get('articleType');
            deliveryStatus = ApexPages.currentPage().getParameters().get('deliveryStatus');
            deliveryDate = ApexPages.currentPage().getParameters().get('deliveryDate');
            orderDate = ApexPages.currentPage().getParameters().get('orderDate');
            countryCode = ApexPages.currentPage().getParameters().get('countryCode');
            periodId = ApexPages.currentPage().getParameters().get('periodId');		

            selectedItems = ApexPages.currentPage().getParameters().get('selectedItems');
	}
	
   
    public void exportToExcel(){
        string queryString = 'SELECT BI_SP_Order__r.Name, BI_SP_Article_type__c, BI_SP_Territory__c,BI_SP_Article_external_id_1__c, BI_SP_Family_name__c, BI_SP_Article_description__c, '+
          						'BI_SP_Quantity__c, BI_SP_Sales_rep_name__c, BI_SP_Ship_to__c, BI_SP_Sales_rep_address__c, BI_SP_Order_reason__c, BI_SP_Order_type__c, BI_SP_Delivery_date__c, '+
          						'BI_SP_Special_shipment_description__c '+
          						'FROM BI_SP_Order_item__c '+
          						'WHERE ';

        if(orderType != null && orderType != ''){
            String oType = orderType == 'AP' ? orderType : (shipmentType == 'SR' ? 'SS' : shipmentType);
            // Special case: special shipment > All shipments --> find all but AP
            if (oType != '')
                queryString+='BI_SP_Order__r.BI_SP_Order_type_pl__c = \''+oType+'\' AND ';
            else
                queryString+='BI_SP_Order__r.BI_SP_Order_type_pl__c != \'AP\' AND ';
        }
        if(articleType != null && articleType != ''){
            queryString+='BI_SP_Order__r.BI_SP_Article_type__c = \''+articleType+'\' AND ';
        }
        if(deliveryStatus != null && deliveryStatus != ''){
            queryString+='BI_SP_Order__r.BI_SP_Delivery_status__c = \''+deliveryStatus+'\' AND '; 
        }
        if(deliveryDate != null && deliveryDate != ''){
            String[] sp = deliveryDate.split('T');
            queryString+='BI_SP_Order__r.BI_SP_Delivery_date__c = '+sp[0]+' AND ';
        }
        if(orderDate != null && orderDate != ''){
            String[] sp = orderDate.split('T');
            queryString+='BI_SP_Order__r.BI_SP_Order_date__c = '+sp[0]+' AND ';
        }
        if(countryCode != null && countryCode != ''){
            if(orderType == 'AP'){
                queryString+='BI_SP_Order__r.BI_SP_Preparation_period__r.BI_SP_Planit_cycle__r.BI_PL_Country_code__c = \''+countryCode+'\' AND ';
            }else{
                queryString+='BI_SP_Order__r.BI_SP_Special_shipment__r.BI_SP_Sales_Rep_Country__c = \''+countryCode+'\' AND ';
            }
        }
        //else{
        //    if(orderType == 'MSEX'){
        //        String countryCodeList = '';
        //        if(regionCode != null && regionCode != ''){
        //            countryCodeList = BI_SP_SamproServiceUtility.stringOutOfSet(BI_SP_SamproServiceUtility.getCountryCodesForRegion(regionCode));
        //        }else{
        //            countryCodeList = BI_SP_SamproServiceUtility.getStringCurrentUserRegionCountryCodes();
        //        }
        //        queryString+='BI_SP_Special_shipment__r.BI_SP_Sales_Rep_Country__c IN ('+countryCodeList+') AND ';
        //    }
        //}
        if(periodId != null && periodId != ''){
            queryString+='BI_SP_Order__r.BI_SP_Preparation_period__c = \''+periodId+'\' AND '; 
        }

        if (selectedItems != null && selectedItems != ''){
            String [] ids = selectedItems.split(',');
            queryString+='BI_SP_Order__c in (\''+String.join(ids, '\',\'')+'\')';
        }

        if(queryString.endsWith('WHERE ')){
            queryString = queryString.removeEnd('WHERE ');
        }else if(queryString.endsWith('AND ')){
            queryString = queryString.removeEnd('AND ');
        }
        queryString+= 'ORDER By BI_SP_Order__c ASC, Id ASC';
        System.debug(queryString);

          List<BI_SP_Order_item__c> oItems = DataBase.Query(queryString);
          system.debug('oItems :'+oItems.size());

          if(oItems.size()>0){
              for(BI_SP_Order_item__c oi :oItems){
                  wrapper w = new wrapper();
                  w.name = oi.BI_SP_Order__r.Name ;

                  for(Schema.PicklistEntry atype : artTypes){
                     if(atype.value == oi.BI_SP_Article_type__c){ 
                         w.artType = atype.label;
                     }
                  }
                  //w.artType = oi.BI_SP_Article_type__c;
                  w.territory = oi.BI_SP_Territory__c;
                  w.artExtId1 = oi.BI_SP_Article_external_id_1__c ;
                  w.famName = oi.BI_SP_Family_name__c;
                  w.artDesc = oi.BI_SP_Article_description__c;
                  w.quantity = string.valueOf(oi.BI_SP_Quantity__c);
                  w.srName = oi.BI_SP_Sales_rep_name__c;
                  w.shipTo = oi.BI_SP_Ship_to__c;
                  w.srAddress = oi.BI_SP_Sales_rep_address__c;
                  w.orderReason = oi.BI_SP_Order_reason__c;
                  w.orderType = oi.BI_SP_Order_type__c;
                  w.orderType = oi.BI_SP_Order_type__c;
                  w.delivDate = string.valueOf(oi.BI_SP_Delivery_date__c);
                  w.ssDesc = oi.BI_SP_Special_shipment_description__c;
                  lstwrapper.add(w);               
              }             
          }
    }
}