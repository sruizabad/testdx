@isTest
private class BI_SP_PrepPeriodApproachCtrl_Test {

    public static String countryCode = 'BR';
    public static String regionCode = 'BR';

    @Testsetup
    static void setUp() {
        BI_SP_TestDataUtility.createCustomSettings();
        List<Customer_Attribute_BI__c> specs = BI_SP_TestDataUtility.createSpecialties();
        List<Account> accounts = BI_SP_TestDataUtility.createAccounts(specs);
        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());

        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
        List<BI_SP_Approach_filter_article__c> artFilters = BI_SP_TestDataUtility.createApproachFilters(articles);
    }

    @isTest static void testInitAndFilters() {
        BI_SP_PrepPeriodApproachCtrl cont = new  BI_SP_PrepPeriodApproachCtrl(); 
        
        PageReference pageRef = Page.BI_SP_PrepPeriodApproach;
        Test.setCurrentPage(pageRef);
        
        BI_SP_PrepPeriodApproachCtrl.FiltersModel filt =  BI_SP_PrepPeriodApproachCtrl.getFilters();
        filt =BI_SP_PrepPeriodApproachCtrl.getCountryCodes(true, true, regionCode);
        filt =BI_SP_PrepPeriodApproachCtrl.getCountryCodes(false, false, regionCode);
        String reg =BI_SP_PrepPeriodApproachCtrl.getUserRegionCodes(true);
        reg =BI_SP_PrepPeriodApproachCtrl.getUserRegionCodes(false);

        List<BI_SP_Preparation_period__c> periods =  BI_SP_PrepPeriodApproachCtrl.getPreparationPeriods(countryCode);
        List<Product_vod__c> families =  BI_SP_PrepPeriodApproachCtrl.getProductFamilies(true, true, countryCode);
        List<Product_vod__c> families2 =  BI_SP_PrepPeriodApproachCtrl.getProductFamilies(false, false, countryCode);
        BI_SP_Preparation_period__c selectedperiod = periods.get(0);
        Product_vod__c selectedproduct = families.get(0);
        
        BI_SP_PrepPeriodApproachCtrl.TargetFiltersModel tgtFilters = BI_SP_PrepPeriodApproachCtrl.getTargetFilters(selectedperiod);
        
        String articleTypes = BI_SP_PrepPeriodApproachCtrl.getArticleTypes(true, true, countryCode);
        String articleTypes2 = BI_SP_PrepPeriodApproachCtrl.getArticleTypes(false, false, countryCode);
        String segmentTypes = BI_SP_PrepPeriodApproachCtrl.getSegments();
        String approachTypes = BI_SP_PrepPeriodApproachCtrl.getApproachTypes();

        List<Customer_Attribute_BI__c> specs =  BI_SP_PrepPeriodApproachCtrl.getSpecialtiesInPeriod(selectedperiod);
        List<Product_vod__c> products = BI_SP_PrepPeriodApproachCtrl.getProductFamilies(true, true, countryCode);
        List<Product_vod__c> products2 = BI_SP_PrepPeriodApproachCtrl.getProductFamilies(false, false, countryCode);
        List<Product_vod__c> products3 = BI_SP_PrepPeriodApproachCtrl.getAllFamiliesManaged();
        List<BI_SP_Article__c> arts2 =  BI_SP_PrepPeriodApproachCtrl.getArticles(products, null);
        List<BI_SP_Article__c> arts3 =  BI_SP_PrepPeriodApproachCtrl.getArticles(products, '1');

        //Approach filters
        List<BI_SP_Approach_filter_article__c> appfitlers = BI_SP_PrepPeriodApproachCtrl.getApproachFilters();
        BI_SP_Approach_filters__c toDeleteFilter = new BI_SP_Approach_filters__c(
                BI_SP_Approach_type__c = 'TR'
        );
        BI_SP_Approach_filter_article__c toDeleteFilterArticle = new BI_SP_Approach_filter_article__c(
                BI_SP_Allocate_territory__c = 1
        );
        List<BI_SP_Approach_filter_article__c> toDeleteFilterArticleList = new List<BI_SP_Approach_filter_article__c>();
        toDeleteFilterArticleList.add(toDeleteFilterArticle);

        Boolean saveResult = BI_SP_PrepPeriodApproachCtrl.saveApproachFilters( toDeleteFilter, toDeleteFilterArticleList );
        Boolean saveResultBad = BI_SP_PrepPeriodApproachCtrl.saveApproachFilters( null, null );

        appfitlers = BI_SP_PrepPeriodApproachCtrl.getApproachFilters();
        BI_SP_Approach_filters__c fil = [SELECT Id from BI_SP_Approach_filters__c LIMIT 1];
        appfitlers = BI_SP_PrepPeriodApproachCtrl.getApproachFilterToLoad(fil.Id);

        List<String> toDeleteFilterIds = new List<String>();
        for(BI_SP_Approach_filters__c fl : [Select Id From BI_SP_Approach_filters__c]){
            toDeleteFilterIds.add(fl.Id);
        }

        Boolean delResult = BI_SP_PrepPeriodApproachCtrl.deleteApproachFilters( toDeleteFilterIds );
        Boolean delResultBad = BI_SP_PrepPeriodApproachCtrl.deleteApproachFilters(new List<String>());  
    }
    
    @isTest static void testEShop() {
        BI_SP_PrepPeriodApproachCtrl cont = new  BI_SP_PrepPeriodApproachCtrl();
        
        PageReference pageRef = Page.BI_SP_PrepPeriodApproach;
        Test.setCurrentPage(pageRef);

        List<BI_SP_Article__c> articlesInitial = new List<BI_SP_Article__c>([SELECT Id, Name, BI_SP_Raw_country_code__c, BI_SP_Country_code__c, BI_SP_Description__c, BI_SP_E_shop_available__c, 
                                                        BI_SP_External_id_1__c, BI_SP_External_stock__c, BI_SP_Is_active__c, BI_SP_Stock__c, BI_SP_Type__c,
                                                        BI_SP_Product_family__c, BI_SP_Veeva_product_family__c FROM BI_SP_Article__c ORDER BY CreatedDate]);
        Integer count = 0;
        for(BI_SP_Article__c article : articlesInitial){
            if(math.mod(count, 2)==0){
                article.BI_SP_E_shop_available__c = !article.BI_SP_E_shop_available__c;
            }
            count++;
        }
         
        BI_SP_PrepPeriodApproachCtrl.updateArticleEShop(articlesInitial);
    }

    @isTest static void testStock() {
        BI_SP_PrepPeriodApproachCtrl cont = new  BI_SP_PrepPeriodApproachCtrl();
        
        PageReference pageRef = Page.BI_SP_PrepPeriodApproach;
        Test.setCurrentPage(pageRef);

        List<BI_SP_Article__c> articles = new List<BI_SP_Article__c>([SELECT Id, Name, BI_SP_Raw_country_code__c, BI_SP_Country_code__c, BI_SP_Description__c, BI_SP_E_shop_available__c, 
                                                        BI_SP_External_id_1__c, BI_SP_External_stock__c, BI_SP_Is_active__c, BI_SP_Stock__c, BI_SP_Type__c,
                                                        BI_SP_Product_family__c, BI_SP_Veeva_product_family__c FROM BI_SP_Article__c]);
        Set<Product_vod__c> selectedFamilies = new Set<Product_vod__c>();
        for(BI_SP_Article__c art : articles){
            Product_vod__c p = new Product_vod__c(Id = art.BI_SP_Veeva_product_family__c);
            selectedFamilies.add(p);
        }
                 
        BI_SP_PrepPeriodApproachCtrl.updateArticleStockMultiple(articles);
        BI_SP_PrepPeriodApproachCtrl.updateFamilyStockMultiple(articles,new List<Product_vod__c>(selectedFamilies));
    }

    @isTest static void testApproachTerritory() {
        BI_SP_PrepPeriodApproachCtrl cont = new  BI_SP_PrepPeriodApproachCtrl();
        
        PageReference pageRef = Page.BI_SP_PrepPeriodApproach;
        Test.setCurrentPage(pageRef);

        BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        Product_vod__c product = [Select Id from Product_vod__c limit 1];
        String lastIdOffset = null;

        BI_SP_PrepPeriodApproachCtrl.AssigmentResponse asignRes = BI_SP_PrepPeriodApproachCtrl.getDataForAssignmentByTerritory(period, lastIdOffset);

        List<BI_PL_Position__c> selectedPositions = new List<BI_PL_Position__c>([SELECT Id, Name, BI_PL_Country_code__c FROM BI_PL_Position__c]);
        BI_SP_Article__c selectedArticle = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];

        BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTerritoryModel art = new BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTerritoryModel();
        art.article = selectedArticle;
        art.allocationQuantity = 5;
        List<BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTerritoryModel> asslist = new List<BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTerritoryModel>();
        asslist.add(art);
        List<BI_SP_PrepPeriodApproachCtrl.AssignmentByTerritoryModel> aaTerrMod = new List<BI_SP_PrepPeriodApproachCtrl.AssignmentByTerritoryModel>();
        for(BI_PL_Position__c pos : selectedPositions){
            BI_SP_PrepPeriodApproachCtrl.AssignmentByTerritoryModel assTerr = new BI_SP_PrepPeriodApproachCtrl.AssignmentByTerritoryModel();
            assTerr.position = pos;
            assTerr.salesRepName = 'Test_'+pos.Name;
            aaTerrMod.add(assTerr);
        }        
        String result = BI_SP_PrepPeriodApproachCtrl.createApproachMultipleByTerritory(period, asslist, aaTerrMod);
    }

   @isTest static void testApproachByTargetSegment() {
        BI_SP_PrepPeriodApproachCtrl cont = new  BI_SP_PrepPeriodApproachCtrl();
        
        PageReference pageRef = Page.BI_SP_PrepPeriodApproach;
        Test.setCurrentPage(pageRef);

        BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        Product_vod__c product = [Select Id from Product_vod__c limit 1];
        String lastIdOffset = null;
        String segment = 'No Segmentation';
        BI_SP_Article__c selectedArticle = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];

        BI_SP_PrepPeriodApproachCtrl.AssigmentResponse asignRes = BI_SP_PrepPeriodApproachCtrl.getDataForAssignmentByTargetSegment(period, product.Id, segment, lastIdOffset);

        BI_SP_PrepPeriodApproachCtrl.AssigmentResponse asignResBad = BI_SP_PrepPeriodApproachCtrl.getDataForAssignmentByTargetSegment(period, product.Id, null, lastIdOffset);

        BI_SP_PrepPeriodApproachCtrl.AssignmentByTargetModel model = new BI_SP_PrepPeriodApproachCtrl.AssignmentByTargetModel();

        model.position = [SELECT ID, Name, BI_PL_External_id__c, BI_PL_Country_code__c FROM BI_PL_Position__c LIMIT 1];
        model.numberOfTargets = 2;
        model.noSegmentationTargets = 2;

        BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTargetModel art = new BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTargetModel();
        art.article = selectedArticle;
        art.allocationQuantity = 5;
        List<BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTargetModel> asslist = new List<BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTargetModel>();
        asslist.add(art);
        String res = BI_SP_PrepPeriodApproachCtrl.createApproachMultipleByTargetSegment(period, asslist, new List<BI_SP_PrepPeriodApproachCtrl.AssignmentByTargetModel>{model});
    }

    @isTest static void testApproachByTargetSpecialty() {
        BI_SP_PrepPeriodApproachCtrl cont = new  BI_SP_PrepPeriodApproachCtrl();
        
        PageReference pageRef = Page.BI_SP_PrepPeriodApproach;
        Test.setCurrentPage(pageRef);

        BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        Product_vod__c product = [Select Id from Product_vod__c limit 1];
        String lastIdOffset = null;
        String segment = 'No Segmentation';
        BI_SP_Article__c selectedArticle = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];
        Customer_Attribute_BI__c spec = [SELECT Id, Name FROM Customer_Attribute_BI__c LIMIT 1];

        BI_SP_PrepPeriodApproachCtrl.AssigmentResponse asignRes = BI_SP_PrepPeriodApproachCtrl.getDataForAssignmentByTargetSpecialty(period, product.Id, spec.Id, '1', null);

        BI_SP_PrepPeriodApproachCtrl.AssignmentByTargetModel targetModel = new BI_SP_PrepPeriodApproachCtrl.AssignmentByTargetModel();
        targetModel.position = [SELECT ID, Name, BI_PL_External_id__c, BI_PL_Country_code__c FROM BI_PL_Position__c LIMIT 1];
        targetModel.numberOfTargets = 2;
        targetModel.noSegmentationTargets = 2;

        BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTargetModel art = new BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTargetModel();
        art.article = selectedArticle;
        art.allocationBuildQuantity = 5;
        art.allocationDefendQuantity = 0;
        art.allocationGainQuantity = 0;
        art.allocationMaintainQuantity = 0;
        art.allocationObserveQuantity = 0;
        art.allocationNoSegmentationQuantity = 1;
        List<BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTargetModel> asslist = new List<BI_SP_PrepPeriodApproachCtrl.ArticleAssignmentTargetModel>();
        asslist.add(art);
        String res = BI_SP_PrepPeriodApproachCtrl.createApproachMultipleByTargetSpecialty(period, asslist, new List<BI_SP_PrepPeriodApproachCtrl.AssignmentByTargetModel> {targetModel});
    }


    @isTest static void testApproachByStrategy() {
        BI_SP_PrepPeriodApproachCtrl cont = new  BI_SP_PrepPeriodApproachCtrl();
        
        PageReference pageRef = Page.BI_SP_PrepPeriodApproach;
        Test.setCurrentPage(pageRef);

        BI_SP_Preparation_period__c period = [SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_hierarchy__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_External_id__c from BI_SP_Preparation_period__c limit 1];
        String lastIdOffset = null;
        Customer_Attribute_BI__c spec = [SELECT Id, Name FROM Customer_Attribute_BI__c LIMIT 1];

        BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel model = new BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel();
        model.position = [SELECT ID, Name, BI_PL_External_id__c, BI_PL_Country_code__c FROM BI_PL_Position__c LIMIT 1];
        model.article = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];
        model.product = [Select Id, External_ID_vod__c from Product_vod__c  limit 1];
        model.allocationQuantity = 3;

        BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel modelErrors = new BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel();
        modelErrors.position = new BI_PL_Position__c(Name = 'NOEXISTING', BI_PL_External_id__c='NOEXISTING', BI_PL_Country_code__c = 'NE');
        modelErrors.article = new BI_SP_Article__c(BI_SP_External_id_1__c='NOEXISTING', BI_SP_External_id__c='NOEXISTING', BI_SP_Stock__c=1);
        modelErrors.product = new Product_vod__c(Name='NOEXISINTG', External_ID_vod__c='NOEXISTING');
        modelErrors.allocationQuantity = 3;

        BI_PL_Position__c posDiffCountry = new BI_PL_Position__c(Name = 'NOEXISTING2', BI_PL_External_id__c='NOEXISTING2', BI_PL_Country_code__c = 'NE');
        insert posDiffCountry;

        Product_vod__c productNotAssigned = new Product_vod__c(
            Name = 'mptAssigned',
            External_ID_vod__c = 'notAssigned'
        );
        insert productNotAssigned;

        BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel modelErrors2 = new BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel();
        modelErrors2.position = posDiffCountry;
        modelErrors2.article = (new List<BI_SP_Article__c>([SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 3])).get(1);
        modelErrors2.product = productNotAssigned;
        modelErrors2.allocationQuantity = 300;

        BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel modelErrors3 = new BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel();
        modelErrors3.position = [SELECT ID, Name, BI_PL_External_id__c, BI_PL_Country_code__c FROM BI_PL_Position__c LIMIT 1];
        modelErrors3.article = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];
        modelErrors3.product = [Select Id, External_ID_vod__c from Product_vod__c  limit 1];
        modelErrors3.allocationQuantity = null;

        BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel modelErrors4 = new BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel();
        modelErrors4.position = [SELECT ID, Name, BI_PL_External_id__c, BI_PL_Country_code__c FROM BI_PL_Position__c LIMIT 1];
        modelErrors4.article = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1];
        modelErrors4.product = [Select Id, External_ID_vod__c from Product_vod__c  limit 1];
        modelErrors4.allocationQuantity = -2;

        BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel modelErrors5 = new BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel();
        modelErrors5.position = [SELECT ID, Name, BI_PL_External_id__c, BI_PL_Country_code__c FROM BI_PL_Position__c LIMIT 1];
        modelErrors5.article = [SELECT Id , BI_SP_External_id_1__c,BI_SP_External_id__c, BI_SP_Stock__c FROM BI_SP_Article__c LIMIT 1 OFFSET 1];
        modelErrors5.product = [Select Id, External_ID_vod__c from Product_vod__c  limit 1];
        modelErrors5.allocationQuantity = 5000000;

        List<BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel> asignRes = BI_SP_PrepPeriodApproachCtrl.getErrorsForSelectedStrategy(countrycode, new List<BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel> {model, modelErrors, modelErrors2, modelErrors3, modelErrors4, modelErrors5});

        String res =  BI_SP_PrepPeriodApproachCtrl.createApproachByStrategy(period, new List<BI_SP_PrepPeriodApproachCtrl.AssigmentByStrategyModel> {asignRes.get(0)});
    }
}