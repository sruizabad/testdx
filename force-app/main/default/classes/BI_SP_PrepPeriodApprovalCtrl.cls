/**
 * Preparation Period Approval Controller
 */
public without sharing class BI_SP_PrepPeriodApprovalCtrl {

  public String userPermissionsJSON {get;set;}

  /**
   * Constructs the object.
   */
  public BI_SP_PrepPeriodApprovalCtrl() {
    userPermissionsJSON = JSON.serialize(BI_SP_SamproServiceUtility.getCurrentUserPermissions());
  }

  /**
   * Constructs the object.
   *
   * @param      controller  The standard controller
   * 
   */
  public BI_SP_PrepPeriodApprovalCtrl(ApexPages.StandardController controller) {
    userPermissionsJSON = JSON.serialize(BI_SP_SamproServiceUtility.getCurrentUserPermissions());
  }

  /**
   * Get the preparation periods for the country passed by parameter that are in dates for approvals
   *
   * @param      countryCode  The country code
   * 
   * @return     The preparation periods.
   */
  @RemoteAction
  public static List<BI_SP_Preparation_period__c> getPeriods(String countryCode) {
    return BI_SP_SamproServiceUtility.getListPeriodsForApprovals(countryCode, Userinfo.getUserId());
  }  

  /**
   * Gets the filters.
   *
   * @return     The filters.
   */
  @RemoteAction
  public static FiltersModel getFilters() {
    Map<String, Boolean> userPermissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
    FiltersModel fm = new FiltersModel();
    fm.periodCountries = getPeriodCountryCodes(userPermissions.get('isRB'), false, null);
    fm.region = getUserRegionCodes(userPermissions.get('isRB'));
    return fm;
  }

  /**
   * Gets the sub filters.
   *
   * @param      prepPeriodId  The preparation period identifier
   *
   * @return     The sub filters.
   */
  @RemoteAction
  public static SubFiltersModel getSubFilters(String prepPeriodId) {
    SubFiltersModel fm = new SubFiltersModel();
    fm.articles = getArticleTypes();
    fm.statuses = getArticlePreparationStatuses();
    fm.approvalStatuses = getArticlePreparationApprovalStatuses();
    fm.products = getProductsForPreparationPeriod(prepPeriodId);
    return fm;
  }

  /**
   * Gets the order article types allowed for the country of the current user.
   *
   * @return     The article types.
   */
  private static String getArticleTypes() {
        return JSON.serialize(BI_SP_SamproServiceUtility.getListCurrentUserArticleTypesByCountry(BI_SP_Article__c.BI_SP_Type__c));
  }

  /**
   * Gets the article preparation statuses.
   *
   * @return     The article preparation statuses.
   */
  private static String getArticlePreparationStatuses() {
    return JSON.serialize(BI_SP_Article_preparation__c.BI_SP_Status__c.getDescribe().getPicklistValues());
  }

  /**
   * Gets the article preparation approval statuses.
   *
   * @return     The article preparation approval statuses.
   */
  private static String getArticlePreparationApprovalStatuses() {
    return JSON.serialize(BI_SP_Article_preparation__c.BI_SP_Approval_status__c.getDescribe().getPicklistValues());
  }

  /**
   * Gets the products with active articles for preparation period.
   *
   * @param      prepPeriodId  The preparation period identifier
   *
   * @return     The products for preparation period.
   */
  private static List<Product_vod__c> getProductsForPreparationPeriod(String prepPeriodId) {
    List<BI_SP_Article__c> articleList = new List<BI_SP_Article__c>([SELECT Id, BI_SP_Veeva_product_family__c, BI_SP_Veeva_product_family__r.Name 
                                      FROM BI_SP_Article__c 
                                      WHERE BI_SP_Is_active__c = true
                                      AND Id IN (SELECT BI_SP_Article__c 
                                              FROM BI_SP_Article_preparation__c
                                              WHERE BI_SP_Preparation__r.BI_SP_Preparation_period__c = :prepPeriodId)]);
    Set<Product_vod__c> productsForPrepPeriods = new Set<Product_vod__c>();
    for(BI_SP_Article__c article : articleList){
      Product_vod__c p = new Product_vod__c();
      p.Id = article.BI_SP_Veeva_product_family__c;
      p.Name = article.BI_SP_Veeva_product_family__r.Name;
      productsForPrepPeriods.add(p);
    }
    return new List<Product_vod__c>(productsForPrepPeriods);
  }
  
  /**
   * Gets the preparation period country codes for the region of the current user.
   *
   * @param      isRb                   Indicates if is ReportBuilder
   * @param      selectedReportBuilder  Indicates if is access like report builder
   * @param      region                 The region
   *
   * @return     The preparation period country codes.
   */
  @RemoteAction
  public static String getPeriodCountryCodes(Boolean isRb, Boolean selectedReportBuilder, String region) {
      List<String> countryCodes = new List<String>();
      if(!isRB || !selectedReportBuilder){
          countryCodes = new List<String>(BI_SP_SamproServiceUtility.getSetCurrentUserRegionCountryCodes());
      }else{
        countryCodes = new List<String>(BI_SP_SamproServiceUtility.getCountryCodesForRegion(region));
      }
        return JSON.serialize(countryCodes);
  }

  /**
   * Gets the region codes for the current user.
   *
   * @param      isRb  Indicates if is ReportBuilder
   *
   * @return     The user region codes.
   */
  public static String getUserRegionCodes(Boolean isRb) {
    List<String> regionCodes = new List<String>();
    if(isRB){
        regionCodes = new List<String>(BI_SP_SamproServiceUtility.getSetReportBuilderRegionCodes());
      }
      return JSON.serialize(regionCodes);
  }

  /**
   * Gets the user permissions.
   *
   * @return     The user permissions. < permissionName, boolean >
   */
  @RemoteAction
  public static Map<String, Boolean> getUserPermissions() {
    return BI_SP_SamproServiceUtility.getCurrentUserPermissions();
  }
  
  /**
   * Gets the products country codes for products manages by the current user
   *
   * @return     The article country codes.
   */
  public static String getProductCountryCodes() {
    List<String> countryCodes = new List<String>(BI_SP_SamproServiceUtility.getListCountriesForUserManagesProducts(Userinfo.getUserId()));
    return JSON.serialize(countryCodes);
  }
  
  /**
   * Gets the positions for the preparation period.
   *
   * @param      period        The preparation period
   * @param      lastIdOffset  The last identifier offset
   *
   * @return     The position Model.
   */
  @RemoteAction
  public static PositionsModel getPositions(BI_SP_Preparation_period__c period, String lastIdOffset) {    
    String idOffset = lastIdOffset != null ? lastIdOffset : '';

    List<BI_PL_Position__c> positions = new List<BI_PL_Position__c>([SELECT Id, Name, BI_PL_Country_code__c, BI_PL_External_id__c, BI_PL_Field_force__c 
                                      FROM BI_PL_Position__c
                                      WHERE BI_PL_Country_code__c = :period.BI_SP_Planit_cycle__r.BI_PL_Country_code__c
                                      AND Id > :idOffset
                                      ORDER By Id ASC
                                      LIMIT 400
                                    ]);
    PositionsModel reponse = new PositionsModel();
    reponse.positions = positions;
    reponse.lastIdOffset = !positions.isEmpty() ? positions.get(positions.size() - 1).Id : null;
    return reponse;
  }

  /**
   * Gets the approval models.
   *
   * @param      prepId      The preparation period identifier
   * @param      lastOffset  The last offset
   *
   * @return     The approval models.
   */
  @RemoteAction
  public static ApprovalModel getApprovalModels(String prepId, String lastOffset){
    String offset = lastOffset != null ? lastOffset : null;
    Decimal limitQuery = 400;

    String query = 'SELECT Id, Name, BI_SP_External_id__c, BI_SP_Amount_strategy__c, BI_SP_Amount_target__c, BI_SP_Amount_territory__c, BI_SP_Amount_e_shop__c, '+
                    'BI_SP_Adjusted_amount_1__c, BI_SP_Adjusted_amount_2__c, BI_SP_Status__c, BI_SP_Approval_status__c, BI_SP_Article__c, BI_SP_Article__r.Name, BI_SP_Article__r.BI_SP_External_id_1__c, '+
                    'BI_SP_Article__r.BI_SP_Stock__c, BI_SP_Article__r.BI_SP_Veeva_product_family__c, BI_SP_Article__r.BI_SP_Veeva_product_family__r.Name, '+
                    'BI_SP_Article__r.BI_SP_Type__c, BI_SP_Preparation__c, BI_SP_Preparation__r.BI_SP_Position__c, BI_SP_Preparation__r.BI_SP_Position__r.Id, BI_SP_Preparation__r.BI_SP_Position__r.Name '+
                  'FROM BI_SP_Article_Preparation__c '+
            'WHERE BI_SP_Article__r.BI_SP_Veeva_product_family__c != null AND BI_SP_Preparation__r.BI_SP_Preparation_period__c = \''+prepId+'\' AND ';
    
    if(offset != null){
      query+=' Id > \''+offset+'\' ';
    }

    if(query.endsWith('AND ')){
      query = query.removeEnd('AND ');
    }

    query+= 'ORDER By Id ASC '+
        'LIMIT '+limitQuery+' ';

    List<BI_SP_Article_Preparation__c> articlePrepList = (List<BI_SP_Article_Preparation__c>) Database.query(query);
    
    ApprovalModel response = new ApprovalModel();
    response.articlePreparationList = articlePrepList;
    response.lastOffset = !articlePrepList.isEmpty() ? articlePrepList.get(articlePrepList.size() - 1).Id : null;
    return response;
  }

  /**
   * Saves approvals.
   *
   * @param      approvalsModels  The approvals models
   *
   * @return     errors
   */
  @RemoteAction
  public static String saveApprovals(List<ApprovalModel> approvalsModels){
    List<BI_SP_Article_preparation__c> selectedArticlesPreps = new List<BI_SP_Article_preparation__c>();
    for(ApprovalModel am : approvalsModels){
      selectedArticlesPreps.addAll(am.articlePreparationList);
    }

    List<String> articlesExternalIds = new List<String>();
    List<String> articlesPrepsExternalIds = new List<String>();
    for(BI_SP_Article_preparation__c ap : selectedArticlesPreps){
      articlesExternalIds.add(ap.BI_SP_Article__c);
      articlesPrepsExternalIds.add(ap.BI_SP_External_id__c);
    }

    Map<String, BI_SP_Article__c> articleMap = new Map<String, BI_SP_Article__c>([SELECT Id, Name, BI_SP_Stock__c, BI_SP_Veeva_product_family__c, BI_SP_E_shop_available__c, BI_SP_External_id_1__c, BI_SP_External_id__c
                                                                FROM BI_SP_Article__c
                                                                WHERE Id IN :articlesExternalIds]); // <articleId, Article>
    
    Map<String, BI_SP_Article_preparation__c> existingArticlePreps = new Map<String, BI_SP_Article_preparation__c>(); // <articlePreparationExternalId, ArticlePreparation>
    for(BI_SP_Article_preparation__c ap : [SELECT Id, Name, BI_SP_External_id__c, BI_SP_Amount_e_shop__c, BI_SP_Adjusted_amount_1__c, BI_SP_Adjusted_amount_2__c, BI_SP_Article__c, BI_SP_Approval_status__c, BI_SP_Amount_target__c, BI_SP_Status__c
                                                  FROM BI_SP_Article_preparation__c
                                                  WHERE BI_SP_External_id__c IN :articlesPrepsExternalIds]){
      existingArticlePreps.put(ap.BI_SP_External_id__c, ap);
    }

    Boolean anyError = false;
    Savepoint sp = Database.setSavepoint();
    String error = '';
    for(BI_SP_Article_preparation__c ap : selectedArticlesPreps){
      Decimal adjustmentOldValue = BI_SP_SamproServiceUtility.getArticlePreparationApprovalVariableAmountValue(existingArticlePreps.get(ap.BI_SP_External_id__c));
      Decimal adjustmentNewValue = BI_SP_SamproServiceUtility.getArticlePreparationApprovalVariableAmountValue(ap);
      BI_SP_Article__c article = articleMap.get(ap.BI_SP_Article__c);
      article.BI_SP_Stock__c = article.BI_SP_Stock__c + (adjustmentOldValue - adjustmentNewValue);
      articleMap.put(ap.BI_SP_Article__c,article);
    }

    List<Database.UpsertResult> resultsArticlePreparations = Database.upsert(selectedArticlesPreps, BI_SP_Article_preparation__c.BI_SP_External_id__c,true);
    for(Database.UpsertResult result: resultsArticlePreparations){
      if(!result.isSuccess()){
        anyError = true;
        for(Database.Error err : result.getErrors()) {
          System.debug(LoggingLevel.ERROR, '### - BI_SP_PrepPeriodApprovalCtrl - saveApprovals - Article Preparations error: -> ' + 
                                              err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
          error += '### Article Preparations error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
        }
      }
    }

    List<Database.UpsertResult> resultsArticles = Database.upsert(articleMap.values(), BI_SP_Article__c.BI_SP_External_id__c,true);
    for(Database.UpsertResult result: resultsArticles){
      if(!result.isSuccess()){
        anyError = true;
        for(Database.Error err : result.getErrors()) {
          System.debug(LoggingLevel.ERROR, '### - BI_SP_PrepPeriodApprovalCtrl - saveApprovals - Article error: -> ' + 
                                              err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
          error += '### Article error -> ' + err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
        }
      }
    }
    
    if(anyError){
      Database.rollback(sp);
      return error;
    }

    return '';
  }

  /**
   * Class for approval model.
   */
  public class ApprovalModel{
    public List<BI_SP_Article_preparation__c> articlePreparationList;
    public List<BI_SP_Article_preparation__c> filteredApprovalsList;
    public BI_PL_Position__c position; 
    public Product_vod__c product; 
    public Boolean approved;
    public Boolean rejected;
    public String keyName;

    public String lastOffset;   

    /**
     * Constructs the object.
     */
     public ApprovalModel() {
      this.articlePreparationList = new List<BI_SP_Article_preparation__c>();
      this.filteredApprovalsList = new List<BI_SP_Article_preparation__c>();
    }
  }

  /**
   * Class for filters model.
   */
  public class FiltersModel{
    public String periodCountries; // JSON SERIALIZADO
    public String region; // JSON SERIALIZADO
  }

  /**
   * Class for sub filters model.
   */
  public class SubFiltersModel{
    public String articles;
    public String statuses;
    public String approvalStatuses;
    public List<Product_vod__c> products;

  }

  /**
   * Class for positions model.
   */
  public class PositionsModel{
    public List<BI_PL_Position__c> positions; 
    public String lastIdOffset; 
  }
}