@isTest
private class BI_SP_PrepPeriodApprovalCtrl_Test {
	
    public static String countryCode;
    public static String regionCode;

    @Testsetup
    static void setUp() {
        countryCode = 'BR';
        regionCode = 'BR';
        
        BI_SP_TestDataUtility.createCustomSettings();
        List<Account> accounts = BI_SP_TestDataUtility.createAccounts();
        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());


        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
    }
	
	@isTest static void BI_SP_PrepPeriodApprovalCtrl_Test_initialiceStandarController() {
	    System.debug('BI_SP_PrepPeriodApprovalCtrl_Test_initialiceStandarController');
        User us = BI_SP_TestDataUtility.getAllPermissionSetUser(countryCode, 0);

        System.runAs(us) {
	        List<BI_SP_Preparation__c> preparationsIds = new List<BI_SP_Preparation__c>([SELECT Id FROM BI_SP_Preparation__c]);
	        System.debug('preparationsIds: '+preparationsIds);
	        String preparationId = preparationsIds.get(0).Id;
	        System.debug('preparationId: '+preparationId);

	        ApexPages.StandardController sc = new ApexPages.StandardController(preparationsIds.get(0));
	        BI_SP_PrepPeriodApprovalCtrl cont = new  BI_SP_PrepPeriodApprovalCtrl(sc); 
	        System.debug('cont: '+cont);
	        
	        PageReference pageRef = Page.BI_SP_PrepPeriodApproval;
	        Test.setCurrentPage(pageRef);
	        System.debug('pageRef: '+pageRef);
	        
	        BI_SP_PrepPeriodApprovalCtrl.FiltersModel filt =  BI_SP_PrepPeriodApprovalCtrl.getFilters();
	        System.debug('filt: '+filt);

	        Map<String, Boolean> permissionMap =  BI_SP_PrepPeriodApprovalCtrl.getUserPermissions();
	        System.debug('permissionMap: '+permissionMap);

	        List<BI_SP_Preparation_period__c> prepPeriodsList = BI_SP_PrepPeriodApprovalCtrl.getPeriods(countryCode);
	        System.debug('prepPeriodsList: '+prepPeriodsList);

	        List<BI_SP_Preparation_period__c> prepPeriodsIds = new List<BI_SP_Preparation_period__c>([SELECT Id, BI_SP_Planit_cycle__r.BI_PL_Country_code__c FROM BI_SP_Preparation_period__c]);
        	BI_SP_PrepPeriodApprovalCtrl.SubFiltersModel subFilt = BI_SP_PrepPeriodApprovalCtrl.getSubFilters(prepPeriodsIds.get(0).Id);
	        System.debug('subFilt: '+subFilt);

	        BI_SP_PrepPeriodApprovalCtrl.PositionsModel positions = BI_SP_PrepPeriodApprovalCtrl.getPositions(prepPeriodsIds.get(0), null);
	        System.debug('positions: '+positions);

	        String productCountryCodeJSON = BI_SP_PrepPeriodApprovalCtrl.getProductCountryCodes();
	        String periodCountryCodeJSON = BI_SP_PrepPeriodApprovalCtrl.getPeriodCountryCodes(true, true, regionCode);
	        String periodCountryCodeJSON2 = BI_SP_PrepPeriodApprovalCtrl.getPeriodCountryCodes(false, false, regionCode);
	    }
    }
	
	@isTest static void BI_SP_PrepPeriodApprovalCtrl_Test_saveApprovals() {
	    System.debug('BI_SP_PrepPeriodApprovalCtrl_Test_initialiceStandarController');
        User us = BI_SP_TestDataUtility.getAllPermissionSetUser(countryCode, 0);

        System.runAs(us) {
	        BI_SP_PrepPeriodApprovalCtrl cont = new  BI_SP_PrepPeriodApprovalCtrl(); 
	        System.debug('cont: '+cont);
	        
	        PageReference pageRef = Page.BI_SP_PrepPeriodApproval;
	        Test.setCurrentPage(pageRef);
	        System.debug('pageRef: '+pageRef);
	        
	        List<BI_SP_Preparation_period__c> prepPeriodsIds = new List<BI_SP_Preparation_period__c>([SELECT Id FROM BI_SP_Preparation_period__c]);
	        System.debug('prepPeriodsIds: '+prepPeriodsIds);

	        BI_SP_PrepPeriodApprovalCtrl.ApprovalModel approvals = BI_SP_PrepPeriodApprovalCtrl.getApprovalModels(prepPeriodsIds.get(0).Id, null);
	        System.debug('approvals: '+approvals);

	        List<BI_SP_PrepPeriodApprovalCtrl.ApprovalModel> approvalList = new List<BI_SP_PrepPeriodApprovalCtrl.ApprovalModel>();
	        approvalList.add(approvals);
	        String result = BI_SP_PrepPeriodApprovalCtrl.saveApprovals(approvalList);
	        System.debug('result: '+result);
	    }
    }
}