/**
 * Preparation adjustments Controller
 */
public without sharing class BI_SP_PreparationAdjustmentCtrl {

	public String userPermissionsJSON {get;set;}
	public static final String SALESREP_PERMISSION_SET_APINAME = 'BI_SP_SALES';
	
	/**
	 * Constructs the object.
	 */
	public BI_SP_PreparationAdjustmentCtrl() {
		String idParameter = ApexPages.currentPage().getParameters().get('id');

		Map<String, Boolean> userPermissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
		userPermissions.put('isReportBuilder',userPermissions.get('isRB'));
		userPermissions.put('isSalesRepForTerritory',isSalesRepForTerritory(idParameter));
		userPermissionsJSON = JSON.serialize(userPermissions);
	}

	/**
	 * Constructs the object.
	 *
	 * @param      controller  The standard controller
	 * 
	 */
	public BI_SP_PreparationAdjustmentCtrl(ApexPages.StandardController controller) {
		
		String idParameter = ApexPages.currentPage().getParameters().get('id');

		Map<String, Boolean> userPermissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
		userPermissions.put('isReportBuilder',userPermissions.get('isRB'));
		userPermissions.put('isSalesRepForTerritory',isSalesRepForTerritory(idParameter));
		userPermissionsJSON = JSON.serialize(userPermissions);
	}
	
	/**
	 * Gets the preparation.
	 *
	 * @param      prepId  The preparation identifier
	 *
	 * @return     The preparation.
	 */
	@RemoteAction
	public static BI_SP_Preparation__c getPreparation(String prepId) {
		List<BI_SP_Preparation__c> prepList = BI_SP_SamproServiceUtility.getListPeriodsForAdjustments(prepId);
		if(prepList.size()>0){
			return prepList.get(0);
		}
		return null;
	}

    /**
     * Gets the filters.
     *
     * @return     The filters. 
     */
	@RemoteAction
	public static FiltersModel getFilters(String prepId) {
		FiltersModel fm = new FiltersModel();
		fm.articles = getArticleTypes();
		fm.statuses = getArticlePreparationStatuses();
		fm.products = getProductsForPeriod(prepId);
		fm.userPermissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
		List<Boolean> canAdjList = canUserMakeAdjustments(prepId, fm.userPermissions);
		fm.canUserMakeAnyAdjustments = canAdjList.get(0);
		fm.canUserMakeAdjustments = canAdjList.get(1);
		fm.canUserMakeEShops = canAdjList.get(2);
		return fm;
	}

	/**
	 * Gets the order article types allowed for the country of the current user.
	 *
	 * @return     The article types.
	 */
	private static String getArticleTypes() {
        return JSON.serialize(BI_SP_SamproServiceUtility.getListCurrentUserArticleTypesByCountry(BI_SP_Article__c.BI_SP_Type__c));
	}

	/**
	 * Gets the article preparation statuses.
	 *
	 * @return     The article preparation statuses.
	 */
	private static String getArticlePreparationStatuses() {
		return JSON.serialize(BI_SP_Article_preparation__c.BI_SP_Status__c.getDescribe().getPicklistValues());
	}

	/**
	 * Gets the products with active articles for period.
	 *
	 * @param      prepId  The preparation identifier
	 *
	 * @return     The products for period.
	 */
	private static List<Product_vod__c> getProductsForPeriod(String prepId) {
		List<BI_SP_Article__c> articleList = new List<BI_SP_Article__c>([SELECT Id, BI_SP_Veeva_product_family__c, BI_SP_Veeva_product_family__r.Name 
																			FROM BI_SP_Article__c 
																			WHERE BI_SP_Is_active__c = true
																			AND Id IN (SELECT BI_SP_Article__c 
																							FROM BI_SP_Article_preparation__c
																							WHERE BI_SP_Preparation__c = :prepId)]);
		Set<Product_vod__c> productsForPeriods = new Set<Product_vod__c>();
		for(BI_SP_Article__c article : articleList){
			Product_vod__c p = new Product_vod__c();
			p.Id = article.BI_SP_Veeva_product_family__c;
			p.Name = article.BI_SP_Veeva_product_family__r.Name;
			productsForPeriods.add(p);
		}
		return new List<Product_vod__c>(productsForPeriods);
	}

	/**
	 * Determines list ability to user make adjustments.
	 *
	 * @param      prepId           The preparation identifier
	 * @param      userPermissions  The user permissions < user permission name, boolean>
	 *
	 * @return     List ability to user make adjustments {canMakeAnyAdjustment, canMakeAdjustments, canMakeEshops}.
	 */
	@RemoteAction
	public static List<Boolean> canUserMakeAdjustments(String prepId, Map<String, Boolean> userPermissions){
		List<Boolean> canAdjList = new List<Boolean>();
		Boolean canAnyAdj = false;
		Boolean canAdj = false;
		Boolean canEShop = false;
		if(!userPermissions.get('isSR')){
			canAdjList.add(canAnyAdj);
			canAdjList.add(canAdj);
			canAdjList.add(canEShop);
			return canAdjList;
		}

		List<String> countries = new List<String>();
		for(BI_SP_Preparation__c p : [SELECT BI_SP_Position__r.BI_PL_Country_code__c FROM BI_SP_Preparation__c WHERE ID = :prepId]){
			countries.add(p.BI_SP_Position__r.BI_PL_Country_code__c);
		}
        for(BI_SP_Country_settings__c cs : [SELECT BI_SP_Can_make_adjustments__c, BI_SP_Can_make_e_shop__c FROM BI_SP_Country_settings__c WHERE BI_SP_Country_code__c IN :countries]){
            if(cs.BI_SP_Can_make_adjustments__c){
            	canAnyAdj = true;
            	canAdj = true;
            }
            if(cs.BI_SP_Can_make_e_shop__c){
            	canAnyAdj = true;
            	canEShop = true;
            }
        }

		canAdjList.add(canAnyAdj);
		canAdjList.add(canAdj);
		canAdjList.add(canEShop);
		return canAdjList;
	}

	/**
	 * Gets the products with active articles for period.
	 *
	 * @param      prepId  The preparation identifier
	 *
	 * @return     The products for period.
	 */
	private static Boolean isSalesRepForTerritory(String prepId) {
		try{
			Map<String, Boolean> userPermissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
			if(!userPermissions.get('isMDMActive')){
				return false;
			}

			BI_SP_Preparation__c preparation = [SELECT Id, BI_SP_Position__c, BI_SP_Position__r.Name, OwnerId FROM BI_SP_Preparation__c WHERE Id = :prepId LIMIT 1];
			
			//si es sales rep del territorio
			Map<Id,Territory> territories = new Map<Id,Territory>([SELECT Id, Name 
	                                                                FROM Territory 
	                                                                WHERE Name = :preparation.BI_SP_Position__r.Name]);

			List<PermissionSetAssignment> salesRepUsersPermissionSetList = new List<PermissionSetAssignment>([SELECT Id, Assignee.Id, Assignee.Name, PermissionSet.Name 
								FROM PermissionSetAssignment 
								WHERE Assignee.IsActive = true 
								AND Assignee.Id = :UserInfo.getUserId()
								AND  PermissionSet.Name = :SALESREP_PERMISSION_SET_APINAME]);

			if(salesRepUsersPermissionSetList.size()==0){
				return false;
			}

	    	for(UserTerritory userTerritory : [SELECT Id, IsActive, TerritoryId, UserId FROM UserTerritory WHERE TerritoryId IN :territories.keySet() AND UserId = :UserInfo.getUserId() AND IsActive = true]){
	    		return true;
	    	}
		}catch(Exception e){}
		return false;
	}

	/**
	 * Gets the adjustments.
	 *
	 * @param      prepId           The preparation identifier
	 * @param      articleType      The article type
	 * @param      productFamilyId  The product family identifier
	 * @param      status           The status
	 *
	 * @return     List of adjustment models.
	 */
	@RemoteAction
	public static List<AdjustmentModel> getAdjustments(String prepId, String articleType, String productFamilyId, String status){
		String query = 'SELECT Id, Name, BI_SP_External_id__c, BI_SP_Amount_strategy__c, BI_SP_Amount_target__c, BI_SP_Amount_territory__c, BI_SP_Amount_e_shop__c, '+
                		'BI_SP_Adjusted_amount_1__c, BI_SP_Adjusted_amount_2__c, BI_SP_Status__c, BI_SP_Approval_status__c, BI_SP_Article__c, BI_SP_Article__r.Name, '+
                		'BI_SP_Article__r.BI_SP_Stock__c, BI_SP_Article__r.BI_SP_External_id_1__c,  BI_SP_Article__r.BI_SP_External_id__c, BI_SP_Article__r.BI_SP_Available_stock__c, BI_SP_Article__r.BI_SP_Description__c, BI_SP_Article__r.BI_SP_Veeva_product_family__c, BI_SP_Article__r.BI_SP_Veeva_product_family__r.Name, '+
                		'BI_SP_Article__r.BI_SP_Type__c, BI_SP_Preparation__c '+
             			'FROM BI_SP_Article_Preparation__c '+
						'WHERE BI_SP_Preparation__c = \''+prepId+'\' AND ';

		if(articleType != null && articleType != ''){
			query+='BI_SP_Article__r.BI_SP_Type__c = \''+articleType+'\' AND ';
		}

		if(productFamilyId != null && productFamilyId != ''){
			query+='BI_SP_Article__r.BI_SP_Veeva_product_family__c = \''+productFamilyId+'\' AND '; 
		} else {
			query+='BI_SP_Article__r.BI_SP_Veeva_product_family__c != null AND '; 
		}

		if(status != null && status != ''){
			query+='BI_SP_Status__c = \''+status+'\' ';
		}

		if(query.endsWith('AND ')){
			query = query.removeEnd('AND ');
		}

		List<BI_SP_Article_Preparation__c> articlePrepList = (List<BI_SP_Article_Preparation__c>)Database.query(query);

		Map<String,AdjustmentModel> modelMap = new Map<String,AdjustmentModel>();	//<productId, AdjustmentModel>
		for(BI_SP_Article_Preparation__c articlePrep : articlePrepList){
			AdjustmentModel model = new AdjustmentModel();
			if(!modelMap.containsKey(articlePrep.BI_SP_Article__r.BI_SP_Veeva_product_family__c)){
				Product_vod__c product = new Product_vod__c();
				product.Name = articlePrep.BI_SP_Article__r.BI_SP_Veeva_product_family__r.Name;
				product.Id = articlePrep.BI_SP_Article__r.BI_SP_Veeva_product_family__c;
				model.product = product;
			}else{
				model = modelMap.get(articlePrep.BI_SP_Article__r.BI_SP_Veeva_product_family__c);
			}
			model.articlePreparationList.add(articlePrep);
			model.sumFixed += articlePrep.BI_SP_Amount_strategy__c + articlePrep.BI_SP_Amount_territory__c;
			model.sumVariable += articlePrep.BI_SP_Amount_target__c;
			modelMap.put(articlePrep.BI_SP_Article__r.BI_SP_Veeva_product_family__c,model);
		}
        return modelMap.values();
	}	

	/**
	 * Gets the eshop articles.
	 *
	 * @param      prepId        The preparation identifier
	 * @param      productId     The product identifier
	 * @param      lastIdOffset  The last identifier offset
	 *
	 * @return     The eshop articles.
	 */
	@RemoteAction
	public static ArticleResponse getEshopArticles(String prepId, String productId, String lastIdOffset){
        String idOffset = lastIdOffset != null ? lastIdOffset : '';

        List<BI_SP_Article__c> articleList = new List<BI_SP_Article__c>([SELECT Id, Name, BI_SP_Stock__c, BI_SP_Veeva_product_family__c, BI_SP_E_shop_available__c, BI_SP_Description__c, BI_SP_External_id_1__c, BI_SP_External_id__c, BI_SP_Available_stock__c
							                                                FROM BI_SP_Article__c
							                                                WHERE BI_SP_Veeva_product_family__c = :productId
							                                                AND BI_SP_E_shop_available__c = true
							                                                AND BI_SP_Is_active__c = true
							                                                AND Id NOT IN (SELECT BI_SP_Article__c FROM BI_SP_Article_Preparation__c WHERE BI_SP_Preparation__c = :prepId)
							                                                AND Id > :idOffset
							                                                ORDER By Id ASC
							                                                LIMIT 200]);
        
		ArticleResponse reponse = new ArticleResponse();
		reponse.articles = articleList;
		reponse.lastIdOffset = !articleList.isEmpty() ? articleList.get(articleList.size() - 1).Id : null;
		return reponse;
	}

	/**
	 * Saves an e shop.
	 *
	 * @param      selectedArticlesPreps  The selected articles preparations
	 *
	 * @return     errors
	 */
	@RemoteAction
	public static String saveEShop(List<BI_SP_Article_preparation__c> selectedArticlesPreps){
		for(BI_SP_Article_preparation__c ap : selectedArticlesPreps){
			ap.BI_SP_Status__c = 'ES';//E-shop
		}
		return saveAdjustments(selectedArticlesPreps);
	}

	/**
	 * Saves adjustments.
	 *
	 * @param      selectedArticlesPreps  The selected articles preparations
	 *
	 * @return     errors
	 */
	@RemoteAction
	public static String saveAdjustments(List<BI_SP_Article_preparation__c> selectedArticlesPreps){
		List<String> articlesExternalIds = new List<String>();
		List<String> articlesPrepsExternalIds = new List<String>();
		for(BI_SP_Article_preparation__c ap : selectedArticlesPreps){
			articlesExternalIds.add(ap.BI_SP_Article__c);
			articlesPrepsExternalIds.add(ap.BI_SP_External_id__c);
			if(ap.BI_SP_Status__c == 'NE'){//Not Edited
				ap.BI_SP_Status__c = 'ED';//Edited
			}
		}

		Map<String, BI_SP_Article__c> articleMap = new Map<String, BI_SP_Article__c>([SELECT Id, Name, BI_SP_Stock__c, BI_SP_Veeva_product_family__c, BI_SP_E_shop_available__c, BI_SP_External_id_1__c, BI_SP_External_id__c
								                                                FROM BI_SP_Article__c
								                                                WHERE Id IN :articlesExternalIds]);	// <articleId, Article>
		
		Map<String, BI_SP_Article_preparation__c> existingArticlePreps = new Map<String, BI_SP_Article_preparation__c>();	//<articlePreparationExternalId, ArticlePreparation>
		for(BI_SP_Article_preparation__c ap : [SELECT Id, Name, BI_SP_External_id__c, BI_SP_Amount_e_shop__c, BI_SP_Adjusted_amount_1__c, BI_SP_Amount_target__c, BI_SP_Article__c, BI_SP_Status__c
	                                                FROM BI_SP_Article_preparation__c
	                                                WHERE BI_SP_External_id__c IN :articlesPrepsExternalIds]){
			existingArticlePreps.put(ap.BI_SP_External_id__c, ap);
		}

		Boolean anyError = false;
		Savepoint sp = Database.setSavepoint();
		String error = '';
		//Stock control
		for(BI_SP_Article_preparation__c ap : selectedArticlesPreps){
			Decimal adjustmentOldValue = BI_SP_SamproServiceUtility.getArticlePreparationAdjustmentsVariableAmountValue(existingArticlePreps.get(ap.BI_SP_External_id__c));
			Decimal adjustmentNewValue = BI_SP_SamproServiceUtility.getArticlePreparationAdjustmentsVariableAmountValue(ap); 
			BI_SP_Article__c article = articleMap.get(ap.BI_SP_Article__c);
			article.BI_SP_Stock__c = article.BI_SP_Stock__c + (adjustmentOldValue - adjustmentNewValue);
			articleMap.put(ap.BI_SP_Article__c,article);
		}

		List<Database.UpsertResult> resultsArticlePreparations = Database.upsert(selectedArticlesPreps, BI_SP_Article_preparation__c.BI_SP_External_id__c,true);
		for(Database.UpsertResult result: resultsArticlePreparations){
            if(!result.isSuccess()){
                anyError = true;
                for(Database.Error err : result.getErrors()) {
                	System.debug(LoggingLevel.ERROR, '### - BI_SP_PreparationAdjustmentCtrl - saveAdjustments - Article Preparation error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                    error += '### - BI_SP_PreparationAdjustmentCtrl - saveAdjustments - Article Preparation error -> ' + 
                    			err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                }
            }
        }

		List<Database.UpsertResult> resultsArticles = Database.upsert(articleMap.values(), BI_SP_Article__c.BI_SP_External_id__c,true);
		for(Database.UpsertResult result: resultsArticles){
            if(!result.isSuccess()){
                anyError = true;
                for(Database.Error err : result.getErrors()) {
                	System.debug(LoggingLevel.ERROR, '### - BI_SP_PreparationAdjustmentCtrl - saveAdjustments - Article error -> ' + 
                                                        err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields());
                    error += '### - BI_SP_PreparationAdjustmentCtrl - saveAdjustments - Article error -> -> ' + 
                    			err.getStatusCode() +' --> ' + err.getMessage() + ' Fields --> ' + err.getFields() + '\r\n';
                }
            }
        }
	    
    	if(anyError){
    		Database.rollback(sp);
    		return error;
    	}

		return '';
	}

	/**
	 * Class for article response.
	 */
	public class ArticleResponse{
		public List<BI_SP_Article__c> articles;
		public Id lastIdOffset;
	}

	/**
	 * Class for adjustment model.
	 */
	public class AdjustmentModel {
		public Product_vod__c product;
		public List<BI_SP_Article_preparation__c> articlePreparationList;
		public Double sumFixed;
		public Double sumVariable;

		/**
		 * Constructs the object.
		 */
		public AdjustmentModel() {
			this.articlePreparationList = new List<BI_SP_Article_preparation__c>();
			this.sumFixed = 0;
			this.sumVariable = 0;
		}
	}

	/**
	 * Class for filters model.
	 */
	public class FiltersModel{
		public String articles;
		public String statuses;
		public List<Product_vod__c> products;
		public boolean canUserMakeAnyAdjustments;
		public boolean canUserMakeAdjustments;
		public boolean canUserMakeEShops;
		public Map<String, Boolean> userPermissions;
	}
}