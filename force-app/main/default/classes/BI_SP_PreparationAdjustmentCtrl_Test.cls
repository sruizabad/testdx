@isTest
private class BI_SP_PreparationAdjustmentCtrl_Test {

    public static String countryCode;

    @Testsetup
    static void setUp() {
        countryCode = 'BR';
        
        BI_SP_TestDataUtility.createCustomSettings();
        List<Account> accounts = BI_SP_TestDataUtility.createAccounts();
        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());

        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
    }
	
	@isTest static void BI_SP_PreparationAdjustmentCtrl_Test_initialiceStandarController() {
	    System.debug('BI_SP_PreparationAdjustmentCtrl_Test_initialiceStandarController');
        User us = BI_SP_TestDataUtility.getReportBuilderUser(countryCode, 0);

        System.runAs(us) {
	        List<BI_SP_Preparation__c> preparationsIds = new List<BI_SP_Preparation__c>([SELECT Id FROM BI_SP_Preparation__c]);
	        String preparationId = preparationsIds.get(0).Id;

	        ApexPages.StandardController sc = new ApexPages.StandardController(preparationsIds.get(0));
	        BI_SP_PreparationAdjustmentCtrl cont = new  BI_SP_PreparationAdjustmentCtrl(sc); 
	        
	        PageReference pageRef = Page.BI_SP_PreparationAdjustment;
	        Test.setCurrentPage(pageRef);

	        BI_SP_Preparation__c prep = BI_SP_PreparationAdjustmentCtrl.getPreparation(null);
	        
	        BI_SP_PreparationAdjustmentCtrl.FiltersModel filt =  BI_SP_PreparationAdjustmentCtrl.getFilters(null);

        	List<BI_SP_PreparationAdjustmentCtrl.AdjustmentModel> adjList = BI_SP_PreparationAdjustmentCtrl.getAdjustments(preparationId, '1', null, null);
	    }
    }
	
    @isTest static void BI_SP_PreparationAdjustmentCtrl_Test_saveEshops() {
	    System.debug('BI_SP_PreparationAdjustmentCtrl_Test_saveEshops');
        User us = BI_SP_TestDataUtility.getAllPermissionSetUser(countryCode, 0);

        System.runAs(us) {
	        List<BI_SP_Preparation__c> preparationsIds = new List<BI_SP_Preparation__c>([SELECT Id FROM BI_SP_Preparation__c]);
	        String preparationId = preparationsIds.get(0).Id;

	        BI_SP_PreparationAdjustmentCtrl cont = new  BI_SP_PreparationAdjustmentCtrl(); 
	        
	        PageReference pageRef = Page.BI_SP_PreparationAdjustment;
	        Test.setCurrentPage(pageRef);

	        BI_SP_Preparation__c prep = BI_SP_PreparationAdjustmentCtrl.getPreparation(preparationId);
	        
	        BI_SP_PreparationAdjustmentCtrl.FiltersModel filt =  BI_SP_PreparationAdjustmentCtrl.getFilters(preparationId);

	        List<BI_SP_Article__c> articles = new List<BI_SP_Article__c>();
            articles.add(new BI_SP_Article__c(
                             BI_SP_Raw_country_code__c = countryCode+'oo',
                             BI_SP_Country_code__c = countryCode,
                             BI_SP_Description__c = 'testDescription 1a ' + filt.products.get(0).Name,
                             BI_SP_E_shop_available__c = true,
                             BI_SP_External_id_1__c = 'testExtid_1a_' + filt.products.get(0).External_Id_vod__c,
                             BI_SP_External_id__c = countryCode+'_testExtid_1a_' + filt.products.get(0).External_Id_vod__c,
                             BI_SP_External_stock__c = 17,
                             BI_SP_Is_active__c = true,
                             BI_SP_Stock__c = 2,
                             BI_SP_Type__c = '3',
                             BI_SP_Product_family__c = filt.products.get(0).id,
                             BI_SP_Veeva_product_family__c = filt.products.get(0).id
                         ));

            articles.add(new BI_SP_Article__c(
                             BI_SP_Raw_country_code__c = countryCode+'oo',
                             BI_SP_Country_code__c = countryCode,
                             BI_SP_Description__c = 'testDescription 2a ' + filt.products.get(0).Name,
                             BI_SP_E_shop_available__c = true,
                             BI_SP_External_id_1__c = 'testExtid_0a_' + filt.products.get(0).External_Id_vod__c,
                             BI_SP_External_id__c = countryCode+'_testExtid_2a_' + filt.products.get(2).External_Id_vod__c,
                             BI_SP_External_stock__c = 18,
                             BI_SP_Is_active__c = true,
                             BI_SP_Stock__c = 2,
                             BI_SP_Type__c = '1',
                             BI_SP_Product_family__c = filt.products.get(0).id,
                             BI_SP_Veeva_product_family__c = filt.products.get(0).id
                         ));

            insert articles;

	        BI_SP_PreparationAdjustmentCtrl.ArticleResponse ar =  BI_SP_PreparationAdjustmentCtrl.getEshopArticles(preparationId, filt.products.get(0).Id, null);

	        List<BI_SP_Article_Preparation__c> apList = new List<BI_SP_Article_Preparation__c>();
	        apList.add(new BI_SP_Article_Preparation__c(
	        	BI_SP_Status__c = 'ES',
				BI_SP_Article__c = ar.articles.get(0).Id,
				BI_SP_Amount_e_shop__c = 1,
				BI_SP_Amount_territory__c = 0,
				BI_SP_Amount_strategy__c = 0,
				BI_SP_Amount_target__c = 0,
				BI_SP_Preparation__c = preparationId,
				BI_SP_External_id__c = prep.BI_SP_External_id__c + '_' + ar.articles.get(0).BI_SP_External_id__c
            ));
            apList.add(new BI_SP_Article_Preparation__c(
	        	BI_SP_Status__c = 'ES',
				BI_SP_Article__c = ar.articles.get(1).Id,
				BI_SP_Amount_e_shop__c = 1,
				BI_SP_Amount_territory__c = 0,
				BI_SP_Amount_strategy__c = 0,
				BI_SP_Amount_target__c = 0,
				BI_SP_Preparation__c = preparationId,
				BI_SP_External_id__c = prep.BI_SP_External_id__c + '_' + ar.articles.get(1).BI_SP_External_id__c
            ));

	    	String result =  BI_SP_PreparationAdjustmentCtrl.saveEShop(apList);
	    }
    }
	

    @isTest static void BI_SP_PreparationAdjustmentCtrl_Test_saveAdjustments() {
        User us = BI_SP_TestDataUtility.getAllPermissionSetUser(countryCode, 0);

        System.runAs(us) {
	        List<BI_SP_Preparation__c> preparationsIds = new List<BI_SP_Preparation__c>([SELECT Id FROM BI_SP_Preparation__c]);
	        String preparationId = preparationsIds.get(0).Id;

	        BI_SP_PreparationAdjustmentCtrl cont = new  BI_SP_PreparationAdjustmentCtrl(); 
	        
	        PageReference pageRef = Page.BI_SP_PreparationAdjustment;
	        Test.setCurrentPage(pageRef);

	        BI_SP_Preparation__c prep = BI_SP_PreparationAdjustmentCtrl.getPreparation(preparationId);
	        
	        BI_SP_PreparationAdjustmentCtrl.FiltersModel filt =  BI_SP_PreparationAdjustmentCtrl.getFilters(preparationId);

        	List<BI_SP_PreparationAdjustmentCtrl.AdjustmentModel> adjList = BI_SP_PreparationAdjustmentCtrl.getAdjustments(preparationId, '1', filt.products.get(0).Id, 'NE');

	        adjList.get(0).articlePreparationList.get(0).BI_SP_Status__c = 'NE';
	        adjList.get(0).articlePreparationList.get(0).BI_SP_Adjusted_amount_1__c = 1;

	        String result = BI_SP_PreparationAdjustmentCtrl.saveAdjustments(adjList.get(0).articlePreparationList);
	    }
    }
}