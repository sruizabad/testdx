/**
 * Preparation period View Controller
 */
public without sharing class BI_SP_PreparationPeriodExt {

    ApexPages.StandardController stdController;

    public BI_SP_Preparation_period__c record;
    public String cycleValue {get; set;}

    public String currentUserRegion {get;set;}
    public String objPrefix {get;set;}

	public String userPermissionsJSON {get;set;}
	public String pagePermissionJSON {get;set;}

    /**
     * Constructs the object.
     *
     * @param      myStdControllerMy standard controller
     * 
     */
    public BI_SP_PreparationPeriodExt(ApexPages.StandardController myStdController) {
        if (!Test.isRunningTest()) myStdController.addFields(new List<String> {'Id', 'Name', 'BI_SP_Planit_hierarchy__c', 'BI_SP_Planit_cycle__c', 'BI_SP_Planit_cycle__r.Name'});
        
        this.stdController = myStdController;
        this.record = (BI_SP_Preparation_period__c)stdController.getRecord();

		Map<String, Boolean> userPermissionMap = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
		userPermissionsJSON = JSON.serialize(userPermissionMap);

		Map<String, Boolean> pagePermissionMap = new Map<String, Boolean>();
		pagePermissionMap.put('hasPermissionToSee', BI_SP_SamproServiceUtility.hasPermissionToSee() || userPermissionMap.get('isRB'));
		Boolean isUserForTerritory = isUserForTerritory(record.Id);
		UserRecordAccess access = [SELECT RecordId, HasDeleteAccess, HasEditAccess FROM UserRecordAccess WHERE RecordId = :record.Id AND UserId = :UserInfo.getUserId() LIMIT 1];
		pagePermissionMap.put('permissionToEdit', BI_SP_SamproServiceUtility.hasPermissionToSee() && isUserForTerritory && access.HasEditAccess);
		pagePermissionMap.put('permissionToDelete', BI_SP_SamproServiceUtility.hasPermissionToSee() && isUserForTerritory && access.HasDeleteAccess);
		pagePermissionMap.put('permissionToClone', BI_SP_SamproServiceUtility.hasPermissionToSee() && isUserForTerritory && access.HasEditAccess);
		pagePermissionMap.put('accessLikeReportBuilder', (!BI_SP_SamproServiceUtility.hasPermissionToSee() || !isUserForTerritory) && userPermissionMap.get('isRB'));
		pagePermissionJSON = JSON.serialize(pagePermissionMap);

        if (this.record.BI_SP_Planit_cycle__c != null ) {
            this.cycleValue = this.record.BI_SP_Planit_cycle__r.Name;
        }

        this.objPrefix = BI_SP_Preparation_period__c.sObjectType.getDescribe().getKeyPrefix();

        this.currentUserRegion = BI_SP_SamproServiceUtility.getCurrentUserRegion();
    }

    /**
     * Delete the preparation period
     *
     * @param      record  The preparation period
     *
     * @return     errors
     */
	@RemoteAction
	public static Map<String, Object> deletePreparationPeriod(BI_SP_Preparation_period__c record) {
		Map<String, Object> toReturn = new Map<String, Object>();

		Savepoint sp = Database.setSavepoint();
		try {
			delete record;
		} catch (Exception e) {
			Database.rollback(sp);
			toReturn.put('error', e.getMessage());
			System.debug('### - BI_SP_PreparationPeriodExt - deletePreparationPeriod - EXCEPTION -> ' + e);
		}
		return toReturn;
	}

	/**
	 * Determines if the current user is correct user for preparation period territory.
	 *
	 * @param      prepId  The preparation period identifier
	 *
	 * @return     True if user for territory, False otherwise.
	 */
	@TestVisible
	private static Boolean isUserForTerritory(String prepId) {
		try{
			BI_SP_Preparation_period__c preparationPeriod = [SELECT Id, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, OwnerId FROM BI_SP_Preparation_period__c WHERE Id = :prepId LIMIT 1];
			
			//si es usuario de la region
			User userInfoCountry = [SELECT Id, Country_Code_BI__c 
                                        FROM User 
                                        WHERE Id = :UserInfo.getUserId() 
                                        LIMIT 1];

            if(preparationPeriod.BI_SP_Planit_cycle__r.BI_PL_Country_code__c == userInfoCountry.Country_Code_BI__c){
            	return true;
            }

            BI_SP_Country_settings__c csUser = [SELECT BI_SP_Region__c
                                                FROM BI_SP_Country_settings__c 
                                                WHERE BI_SP_Country_code__c = :userInfoCountry.Country_Code_BI__c 
                                                LIMIT 1];

            for(BI_SP_Country_settings__c cs : [SELECT BI_SP_Region__c, BI_SP_Country_code__c
                                                FROM BI_SP_Country_settings__c 
                                                WHERE BI_SP_Region__c = :csUser.BI_SP_Region__c]){
            	if(preparationPeriod.BI_SP_Planit_cycle__r.BI_PL_Country_code__c == cs.BI_SP_Country_code__c){
	            	return true;
	            }
            }
		}catch(Exception e){}

		return false;
	}
}