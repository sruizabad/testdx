/**
 * Preparation period Edit Controller
 */
public without sharing class BI_SP_PreparationPeriodNewExt {

    public String userPermissionsJSON {get;set;}
    public String pagePermissionsJSON {get;set;}
    public String retURLParameter {get;set;}
    public String objPrefix {get;set;}
    public Boolean allowCanMakeAdjustments {get;set;}
    public Boolean allowEditDates {get;set;}

    /**
     * Constructs the object.
     *
     * @param      myStdController  My standard controller
     * 
     */
    public BI_SP_PreparationPeriodNewExt(ApexPages.StandardController myStdController) {
        String idParameter = ApexPages.currentPage().getParameters().get('id');
        this.retURLParameter = ApexPages.currentPage().getParameters().get('retURL');
        this.objPrefix = BI_SP_Special_shipment__c.sObjectType.getDescribe().getKeyPrefix();
        String cloneParameter = ApexPages.currentPage().getParameters().get('clone');

        Map<String, Boolean> userPermissionMap = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
        userPermissionsJSON = JSON.serialize(userPermissionMap);

        Map<String, Boolean> pagePermissionMap = new Map<String, Boolean>();

        if(idParameter == null || idParameter == ''){
            pagePermissionMap.put('hasPermissionToSee', BI_SP_SamproServiceUtility.hasPermissionToSee());
            pagePermissionMap.put('permissionToEdit', true);
            pagePermissionMap.put('accessLikeReportBuilder', !BI_SP_SamproServiceUtility.hasPermissionToSee() && userPermissionMap.get('isRB'));
            pagePermissionMap.put('canRegularizeArticles', false);
        }else{
            try{
                Boolean isUserForTerritory = isUserForTerritory(idParameter);
                UserRecordAccess access = [SELECT RecordId, HasDeleteAccess, HasEditAccess FROM UserRecordAccess WHERE RecordId = :idParameter AND UserId = :UserInfo.getUserId() LIMIT 1];
                pagePermissionMap.put('hasPermissionToSee', BI_SP_SamproServiceUtility.hasPermissionToSee() || userPermissionMap.get('isRB'));
                pagePermissionMap.put('permissionToEdit', BI_SP_SamproServiceUtility.hasPermissionToSee() && isUserForTerritory && access.HasEditAccess);
                pagePermissionMap.put('accessLikeReportBuilder', (!BI_SP_SamproServiceUtility.hasPermissionToSee() || !isUserForTerritory) && userPermissionMap.get('isRB'));
                String currentUserRegion = BI_SP_SamproServiceUtility.getCurrentUserRegion();
                String preparationPeriodRegion = getPrepPeriodRegion(idParameter);
                pagePermissionMap.put('canRegularizeArticles', currentUserRegion.equalsIgnoreCase(preparationPeriodRegion) && currentUserRegion.equalsIgnoreCase('MX'));
            }catch(Exception e){}
        }
        pagePermissionsJSON = JSON.serialize(pagePermissionMap);

        if (idParameter != null && idParameter != '') {
            try{
                BI_SP_Preparation_period__c prepPeriod = [Select Id, 
                                                                BI_SP_Planit_cycle__c,
                                                                BI_SP_Allow_edit_dates__c
                                                                from BI_SP_Preparation_period__c
                                                                where Id = :idParameter
                                                                LIMIT 1];
                this.allowCanMakeAdjustments = canMakeAdjustments(prepPeriod.BI_SP_Planit_cycle__c);
                if (cloneParameter == '1') {
                    this.allowEditDates = true;
                }else{
                    this.allowEditDates = prepPeriod.BI_SP_Allow_edit_dates__c;
                }
            }catch(Exception e){
                this.allowCanMakeAdjustments = true;
                this.allowEditDates = true;
            }
        }else{
            this.allowCanMakeAdjustments = true;
            this.allowEditDates = true;
        }
    }

    /**
     * Determines if the current user is correct user for preparation period territory.
     *
     * @param      prepId  The preparation period identifier
     *
     * @return     True if user for territory, False otherwise.
     */
    @TestVisible
    private static Boolean isUserForTerritory(String prepId) {
        try{
            BI_SP_Preparation_period__c preparationPeriod = [SELECT Id, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, OwnerId FROM BI_SP_Preparation_period__c WHERE Id = :prepId LIMIT 1];
            
            //si es usuario de la region
            User userInfoCountry = [SELECT Id, Country_Code_BI__c 
                                        FROM User 
                                        WHERE Id = :UserInfo.getUserId() 
                                        LIMIT 1];

            if(preparationPeriod.BI_SP_Planit_cycle__r.BI_PL_Country_code__c == userInfoCountry.Country_Code_BI__c){
                return true;
            }

            BI_SP_Country_settings__c csUser = [SELECT BI_SP_Region__c
                                                FROM BI_SP_Country_settings__c 
                                                WHERE BI_SP_Country_code__c = :userInfoCountry.Country_Code_BI__c 
                                                LIMIT 1];

            for(BI_SP_Country_settings__c cs : [SELECT BI_SP_Region__c, BI_SP_Country_code__c
                                                FROM BI_SP_Country_settings__c 
                                                WHERE BI_SP_Region__c = :csUser.BI_SP_Region__c]){
                if(preparationPeriod.BI_SP_Planit_cycle__r.BI_PL_Country_code__c == cs.BI_SP_Country_code__c){
                    return true;
                }
            }
        }catch(Exception e){}

        return false;
    }

    /**
     * Gets the preparation period region.
     *
     * @param      prepId  The preparation period identifier
     *
     * @return     The preparation period region.
     */
    @TestVisible
    private String getPrepPeriodRegion(String prepId) {
        BI_SP_Preparation_period__c preparationPeriod = [SELECT Id, BI_SP_Planit_cycle__r.BI_PL_Country_code__c FROM BI_SP_Preparation_period__c WHERE Id = :prepId LIMIT 1];
        String countryCode = preparationPeriod.BI_SP_Planit_cycle__r.BI_PL_Country_code__c;
        String region = null;

        if (countryCode != '') {
            try {
                List<BI_SP_Country_settings__c> csCountryCode = [Select BI_SP_Region__c
                        from BI_SP_Country_settings__c
                        where BI_SP_Country_code__c = :countryCode];

                if(!csCountryCode.isEmpty()) {
                    region = csCountryCode.get(0).BI_SP_Region__c;
                }
            } catch (Exception e) {}
        }

        return region;
    }

    /**
     * Gets the preparation period.
     *
     * @param      recordId  The preparation period identifier
     *
     * @return     The preparation period.
     */
    @RemoteAction
    public static Map<String, Object> getPreparationPeriod(String recordId) {
        Map<String, Object> toReturn = new Map<String, Object>();

        if (recordId != null && recordId != '') {
            try{
                BI_SP_Preparation_period__c prepPeriod = [Select Id, 
                                                Name,
                                                BI_SP_Delivery_date__c,
                                                BI_SP_Distribution_prep_period_start_dat__c,
                                                BI_SP_Approaches_end_date__c,
                                                BI_SP_Adjustments_end_date__c,
                                                BI_SP_Approval_end_date__c,
                                                BI_SP_Distribution_prep_period_end_date__c,
                                                BI_SP_Planit_cycle__c,
                                                BI_SP_Planit_hierarchy__c,
                                                BI_SP_Allow_edit_dates__c,
                                                BI_SP_External_id__c

                                                from BI_SP_Preparation_period__c
                                                where Id = :recordId
                                                LIMIT 1];

                toReturn.put('Id', prepPeriod.Id);
                toReturn.put('Name', prepPeriod.Name);
                toReturn.put('BI_SP_Delivery_date__c', null);
                if (prepPeriod.BI_SP_Delivery_date__c != null) {
                    toReturn.put('BI_SP_Delivery_date__c', DateTime.newInstance(prepPeriod.BI_SP_Delivery_date__c.year(), prepPeriod.BI_SP_Delivery_date__c.month(), prepPeriod.BI_SP_Delivery_date__c.day()).format('YYYY-MM-dd'));
                }
                toReturn.put('BI_SP_Distribution_prep_period_start_dat__c', null);
                if (prepPeriod.BI_SP_Distribution_prep_period_start_dat__c != null) {
                    toReturn.put('BI_SP_Distribution_prep_period_start_dat__c', DateTime.newInstance(prepPeriod.BI_SP_Distribution_prep_period_start_dat__c.year(), prepPeriod.BI_SP_Distribution_prep_period_start_dat__c.month(), prepPeriod.BI_SP_Distribution_prep_period_start_dat__c.day()).format('YYYY-MM-dd'));
                }
                toReturn.put('BI_SP_Approaches_end_date__c', null);
                if (prepPeriod.BI_SP_Approaches_end_date__c != null) {
                    toReturn.put('BI_SP_Approaches_end_date__c', DateTime.newInstance(prepPeriod.BI_SP_Approaches_end_date__c.year(), prepPeriod.BI_SP_Approaches_end_date__c.month(), prepPeriod.BI_SP_Approaches_end_date__c.day()).format('YYYY-MM-dd'));
                }
                toReturn.put('BI_SP_Adjustments_end_date__c', null);
                if (prepPeriod.BI_SP_Adjustments_end_date__c != null) {
                    toReturn.put('BI_SP_Adjustments_end_date__c', DateTime.newInstance(prepPeriod.BI_SP_Adjustments_end_date__c.year(), prepPeriod.BI_SP_Adjustments_end_date__c.month(), prepPeriod.BI_SP_Adjustments_end_date__c.day()).format('YYYY-MM-dd'));
                }
                toReturn.put('BI_SP_Approval_end_date__c', null);
                if (prepPeriod.BI_SP_Approval_end_date__c != null) {
                    toReturn.put('BI_SP_Approval_end_date__c', DateTime.newInstance(prepPeriod.BI_SP_Approval_end_date__c.year(), prepPeriod.BI_SP_Approval_end_date__c.month(), prepPeriod.BI_SP_Approval_end_date__c.day()).format('YYYY-MM-dd'));
                }
                toReturn.put('BI_SP_Distribution_prep_period_end_date__c', null);
                if (prepPeriod.BI_SP_Distribution_prep_period_end_date__c != null) {
                    toReturn.put('BI_SP_Distribution_prep_period_end_date__c', DateTime.newInstance(prepPeriod.BI_SP_Distribution_prep_period_end_date__c.year(), prepPeriod.BI_SP_Distribution_prep_period_end_date__c.month(), prepPeriod.BI_SP_Distribution_prep_period_end_date__c.day()).format('YYYY-MM-dd'));
                }
                toReturn.put('BI_SP_Planit_hierarchy__c', prepPeriod.BI_SP_Planit_hierarchy__c);
                toReturn.put('hierarchies', JSON.serialize(getHierarchiesForCycle(prepPeriod.BI_SP_Planit_cycle__c)));
                toReturn.put('BI_SP_Allow_edit_dates__c', prepPeriod.BI_SP_Allow_edit_dates__c);
                toReturn.put('BI_SP_External_id__c', prepPeriod.BI_SP_External_id__c);

                BI_PL_Cycle__c cycle = [SELECT Id, Name, BI_PL_Country_code__c, BI_PL_Start_date__c, BI_PL_End_date__c
                                                FROM BI_PL_Cycle__c 
                                                WHERE Id = :prepPeriod.BI_SP_Planit_cycle__c
                                                LIMIT 1];
                toReturn.put('BI_SP_Planit_cycle__c', cycle);
            }catch(Exception e){
                return new Map<String, Object>();
            }
        }

        return toReturn;
    }

    /**
     * Gets the cycles.
     *
     * @return     The cycles.
     */
    @RemoteAction
    public static List<BI_PL_Cycle__c> getCycles(){
        BI_SP_SamproServiceUtility.SAMProCyclesAndPositions cp = BI_SP_SamproServiceUtility.approvedCyclesAndPositions(BI_SP_SamproServiceUtility.getSetCurrentUserRegionCountryCodes());
        String approvedCycles = cp.approvedCycles;

        String query = 'SELECT Id,Name,BI_PL_Country_code__c,BI_PL_Start_date__c,BI_PL_End_date__c FROM BI_PL_Cycle__c ';
        if(approvedCycles != ''){
            query += ' WHERE Id IN (' + cp.approvedCycles + ')';
        } 

        List<BI_PL_Cycle__c> cycles = Database.query(query);

        return cycles;
    }

    /**
     * gets the hierarchies for the cycle
     *
     * @param      cycleId  The cycle identifier
     *
     * @return     hierarchies for the cycle (JSON)
     */
    @RemoteAction
    public static String refreshHierarchies(String cycleId) {
        List<String> hierarchies = getHierarchiesForCycle(cycleId);
        return JSON.serialize(hierarchies);
    }

    /**
     * Determines ability to make adjustments.
     *
     * @param      cycleId  The cycle identifier
     *
     * @return     True if able to make adjustments, False otherwise.
     */
    @RemoteAction
    public static Boolean canMakeAdjustments(String cycleId){ 
        if (cycleId != null) {   
            List<BI_PL_Cycle__c> pcList = new List<BI_PL_Cycle__c>([SELECT BI_PL_Country_code__c FROM BI_PL_Cycle__c WHERE Id = : cycleId]); 
            String countryCode = ''; 
            for(BI_PL_Cycle__c pc : pcList){
                countryCode = pc.BI_PL_Country_code__c;
            }
            for(BI_SP_Country_settings__c cs : [SELECT BI_SP_Can_make_adjustments__c, BI_SP_Can_make_e_shop__c FROM BI_SP_Country_settings__c WHERE BI_SP_Country_code__c = :countryCode]){
                if(!cs.BI_SP_Can_make_adjustments__c && !cs.BI_SP_Can_make_e_shop__c){
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Saves a preparation period.
     *
     * @param      prepPeriod         The prep period
     * @param      canMakeAdjustemts  Indicates if make adjustemts
     *
     * @return     errors
     */
    @RemoteAction
    public static SaveResponse savePreparation_period(BI_SP_Preparation_period__c prepPeriod, Boolean canMakeAdjustemts) {
        SaveResponse sr = new SaveResponse();

        if (prepPeriod.BI_SP_Planit_cycle__c != null) {
            String cycleCountryCode = '';

            try {
                cycleCountryCode = [Select BI_PL_Country_code__c from BI_PL_Cycle__c where Id = :prepPeriod.BI_SP_Planit_cycle__c].BI_PL_Country_code__c;
                System.debug(prepPeriod);
                if(prepPeriod.Id == null) {
                    prepPeriod.BI_SP_External_id__c = BI_SP_SamproServiceUtility.generatePeriodExternalId(cycleCountryCode, prepPeriod.BI_SP_Distribution_prep_period_start_dat__c, prepPeriod.BI_SP_Distribution_prep_period_end_date__c,prepPeriod.Name);
                }
            } catch (Exception e) {
                system.debug('OMG>>BI_SP_Preparation_periodNewExt>>savePreparation_period - EXCEPTION:' + e);
                sr.error = true;
                sr.errorString = Label.BI_SP_CycleNoCountryCode;
                return sr;
            }
        } else{
            sr.error = true;
            sr.errorString = Label.BI_SP_CycleMustHaveValue;
            return sr;
        }

        if(!canMakeAdjustemts){
            prepPeriod.BI_SP_Adjustments_end_date__c = prepPeriod.BI_SP_Approaches_end_date__c;
            prepPeriod.BI_SP_Approval_end_date__c = prepPeriod.BI_SP_Approaches_end_date__c;
        }

        try{
            if(prepPeriod.Id != null){
                update prepPeriod;
            }else{
                insert prepPeriod;
            }
            sr.recordId = prepPeriod.Id;
        }catch(System.DmlException ex){
            if(ex.getDmlType(0) == StatusCode.DUPLICATE_VALUE){
                sr.error = true;
                sr.errorString = Label.BI_SP_Preparation_period_duplicated_values;
                return sr;
            }else{
                sr.error = true;
                sr.errorString = ex.getMessage();
                return sr;
            }
        }catch(Exception e){
            sr.error = true;
            sr.errorString = e.getMessage();
            return sr;
        }
        
        return sr;
    }

    /**
     * Gets the hierarchies for a give cycle.
     *
     * @param      cycleId  The cycle identifier
     *
     * @return     The hierarchies for cycle in a list
     */
    @TestVisible
    private static List<String> getHierarchiesForCycle(String cycleId) {
        BI_SP_SamproServiceUtility.SAMProCyclesAndPositions cp = BI_SP_SamproServiceUtility.approvedCyclesAndPositions(BI_SP_SamproServiceUtility.getSetCurrentUserRegionCountryCodes());
        Set<Id> approvedPositions = new Set<Id>();
        approvedPositions.addAll(cp.approvedPositions);

        List<String> hierarchies = new List<String> ();
        if (approvedPositions != null && approvedPositions.size() > 0 && cycleId != null && cycleId != '') {
            for (AggregateResult ag : [SELECT BI_PL_hierarchy__c FROM BI_PL_position_cycle__c WHERE BI_PL_cycle__c = : cycleId AND ID IN :approvedPositions GROUP BY BI_PL_hierarchy__c]) {
                hierarchies.add((String)ag.get('BI_PL_hierarchy__c'));
            }
        }

        return hierarchies;
    }

    /**
     * Class for save response.
     */
    public class SaveResponse {
        public String errorString;
        public Boolean error;
        public String recordId;

        /**
         * Constructs the object.
         */
        public SaveResponse(){            
            this.error = false;
            this.errorString = '';
            this.recordId = '';
        }
    }
}