@isTest
private class BI_SP_PreparationPeriodNewExt_Test {
    
    public static String countryCode;

	@Testsetup
	static void setUp() {
		BI_SP_TestDataUtility.createCustomSettings();
		List<Account> accounts = BI_SP_TestDataUtility.createAccounts(); 
		List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts();
		
		countryCode = 'BR';
		
		List<Account> accs = BI_PL_TestDataUtility.createAccounts(countryCode);


		//PLANIT TEST DATA
		List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
		List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

		List<BI_PL_Cycle__c> cycleList = new List<BI_PL_Cycle__c>([Select Id from BI_PL_Cycle__c]);
		for(BI_PL_Cycle__c cycle : cycleList){
			cycle.BI_PL_Country_code__c = countryCode;
		}
		update cycleList;

		List<BI_PL_Preparation__c> prepsPL = BI_PL_TestDataUtility.createPreparations(countryCode, posCycles, accs, products);
		for(BI_PL_Preparation__c p : prepsPL){
			p.BI_PL_Status__c =  'Approved';
		}
		update prepsPL;

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
		List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
	}

	@isTest static void BI_SP_Preparation_periodNewExt_Test_notId() {
		BI_SP_Preparation_period__c pp = [Select Id, BI_SP_Planit_cycle__c, BI_SP_Planit_cycle__r.Name,BI_SP_Allow_edit_dates__c, 
												BI_SP_Approaches_end_date__c, BI_SP_Adjustments_end_date__c, BI_SP_Approval_end_date__c
												from BI_SP_Preparation_period__c LIMIT 1];
		pp.Id = null;
		
		PageReference pageRef = Page.BI_SP_PreparationPeriodNew;
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController sc = new ApexPages.StandardController(pp);
		BI_SP_PreparationPeriodNewExt ppClass = new BI_SP_PreparationPeriodNewExt(sc);
	}

	@isTest static void BI_SP_Preparation_periodNewExt_Test_cloneId() {
		BI_SP_Preparation_period__c pp = [Select Id, BI_SP_Planit_cycle__c, BI_SP_Planit_cycle__r.Name,BI_SP_Allow_edit_dates__c, 
												BI_SP_Approaches_end_date__c, BI_SP_Adjustments_end_date__c, BI_SP_Approval_end_date__c
												from BI_SP_Preparation_period__c LIMIT 1];

		PageReference pageRef = Page.BI_SP_PreparationPeriodNew;
		pageRef.getParameters().put('id', String.valueOf(pp.Id));
		pageRef.getParameters().put('clone', '1');
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController sc = new ApexPages.StandardController(pp);
		BI_SP_PreparationPeriodNewExt ppClass = new BI_SP_PreparationPeriodNewExt(sc);

		BI_SP_PreparationPeriodNewExt.SaveResponse sr = BI_SP_PreparationPeriodNewExt.savePreparation_period(pp, true);
		System.debug(LoggingLevel.ERROR, sr.errorString);

		pp.BI_SP_Planit_cycle__c = null;
		BI_SP_PreparationPeriodNewExt.SaveResponse sr2 = BI_SP_PreparationPeriodNewExt.savePreparation_period(pp, false);
		System.debug(LoggingLevel.ERROR, sr2.errorString);
	}

	@isTest static void BI_SP_Preparation_periodNewExt_Test_Id() {
		BI_SP_Preparation_period__c pp = [Select Id, BI_SP_Planit_cycle__c, BI_SP_Planit_cycle__r.Name, BI_SP_Planit_cycle__r.BI_PL_Country_code__c, BI_SP_Allow_edit_dates__c, 
												BI_SP_Approaches_end_date__c, BI_SP_Adjustments_end_date__c, BI_SP_Approval_end_date__c, BI_SP_Distribution_prep_period_start_dat__c,
												BI_SP_Distribution_prep_period_end_date__c, Name
												from BI_SP_Preparation_period__c LIMIT 1];

		System.debug(LoggingLevel.ERROR, pp.BI_SP_Planit_cycle__c);
		BI_PL_Cycle__c cycle = [Select Id from BI_PL_Cycle__c WHERE Id = :pp.BI_SP_Planit_cycle__c LIMIT 1];
		cycle.BI_PL_Country_code__c = 'BR';
		update cycle;

		PageReference pageRef = Page.BI_SP_PreparationPeriodNew;
		pageRef.getParameters().put('id', String.valueOf(pp.Id));
		Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController sc = new ApexPages.StandardController(pp);
		BI_SP_PreparationPeriodNewExt ppClass = new BI_SP_PreparationPeriodNewExt(sc);

		Map<String, Object> auxMap = BI_SP_PreparationPeriodNewExt.getPreparationPeriod(pp.Id);
		Boolean aux = BI_SP_PreparationPeriodNewExt.isUserForTerritory(pp.Id);
		String region = ppClass.getPrepPeriodRegion(pp.Id); 
		List<BI_PL_Cycle__c> cycles = BI_SP_PreparationPeriodNewExt.getCycles();
		String aux2 = BI_SP_PreparationPeriodNewExt.refreshHierarchies(cycles.get(0).Id);
		Boolean aux3 = BI_SP_PreparationPeriodNewExt.canMakeAdjustments(cycles.get(0).Id);

		BI_SP_PreparationPeriodNewExt.SaveResponse sr = BI_SP_PreparationPeriodNewExt.savePreparation_period(pp, true);
		System.debug(LoggingLevel.ERROR, sr.errorString);

		pp.Name = 'test1234';
		BI_SP_PreparationPeriodNewExt.SaveResponse sr2 = BI_SP_PreparationPeriodNewExt.savePreparation_period(pp, false);
		System.debug(LoggingLevel.ERROR, sr2.errorString);

		pp.Id = null;
		BI_SP_PreparationPeriodNewExt.SaveResponse sr3 = BI_SP_PreparationPeriodNewExt.savePreparation_period(pp, false);
		System.debug(LoggingLevel.ERROR, sr3.errorString);
	}

	@isTest static void BI_SP_Preparation_periodNewExt_Test_badId() {
		BI_SP_Preparation_period__c pp = [Select Id, BI_SP_Planit_cycle__c, BI_SP_Planit_cycle__r.Name,BI_SP_Allow_edit_dates__c, 
												BI_SP_Approaches_end_date__c, BI_SP_Adjustments_end_date__c, BI_SP_Approval_end_date__c
												from BI_SP_Preparation_period__c LIMIT 1];
		pp.Id = null;
		
		PageReference pageRef = Page.BI_SP_PreparationPeriodNew;
		pageRef.getParameters().put('id', 'afdsadsf');
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController sc = new ApexPages.StandardController(pp);
		BI_SP_PreparationPeriodNewExt ppClass = new BI_SP_PreparationPeriodNewExt(sc);
	}
}