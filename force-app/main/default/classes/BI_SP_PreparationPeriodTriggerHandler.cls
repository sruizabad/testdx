/**
 * Handler for BI_SP_Preparation_period__c Trigger.
 */
public without sharing class BI_SP_PreparationPeriodTriggerHandler implements BI_SP_TriggerInterface{
    
    public static final String ADMIN_PERMISSION_SET_APINAME = 'BI_SP_Admin'; 
    public static final String CLIENTSERVICE_PERMISSION_SET_APINAME = 'BI_SP_Client_service'; 
    public Map<Id,String> prepPeriodCountryMap; //<preparationPeriodId, countrycode>
    public Map<String,String> countryRegionMap; //<countrycode, region>
    public Map<String,Set<User>> region_UsersInRegionSetMap;    //<region, set of users>
    public Map<String,Set<PermissionSetAssignment>> region_AdminUsersInRegionSetMap;    //<region, Set of PermissionSetAssignment>
    public Map<String,Set<PermissionSetAssignment>> region_ServiceClientUsersInRegionSetMap;    //<region, Set of PermissionSetAssignment>

    public List<BI_SP_Preparation_period__Share> prepPeriodsShareList;
    public Map<String,BI_SP_Preparation__c> preparationMap; //<preparationPeriodId, Preparation>

    /**
     * Constructs the object.
     */
    public BI_SP_PreparationPeriodTriggerHandler() {}

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore() {
        if(Trigger.isDelete){
            //gets all preparation periods that have preparations related
            List<BI_SP_Preparation__c> preparationList = new List<BI_SP_Preparation__c>([SELECT Id, BI_SP_Preparation_period__c FROM BI_SP_Preparation__c WHERE BI_SP_Preparation_period__c IN :Trigger.oldMap.keySet()]);

            preparationMap = new Map<String,BI_SP_Preparation__c>();
            for(BI_SP_Preparation__c prep : preparationList){
                preparationMap.put(prep.BI_SP_Preparation_period__c, prep);
            }
        }
    }

    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkAfter() {
        if (Trigger.isInsert || Trigger.isUpdate) {
            //prepare structures to create dinamic sharing
            List<BI_SP_Preparation_period__c> prepPeriodList = new List<BI_SP_Preparation_period__c>([SELECT Id, BI_SP_Planit_cycle__c, BI_SP_Planit_cycle__r.BI_PL_Country_code__c FROM BI_SP_Preparation_period__c WHERE Id IN: Trigger.newMap.keySet()]);

            prepPeriodCountryMap = new Map<Id,String>();
            for(BI_SP_Preparation_period__c prepPeriod : prepPeriodList){
                prepPeriodCountryMap.put(prepPeriod.Id, prepPeriod.BI_SP_Planit_cycle__r.BI_PL_Country_code__c);
            }
            countryRegionMap = getRegionsForCountries(prepPeriodList);
            getUsersForRegions(prepPeriodList);
            prepPeriodsShareList = new List<BI_SP_Preparation_period__Share>();
        }

        if (Trigger.isUpdate) {
            //delete old dinamic sharing
            List<BI_SP_Preparation_period__Share> prepPeriodShareOldList = new List<BI_SP_Preparation_period__Share>([SELECT Id, ParentId FROM BI_SP_Preparation_period__Share WHERE ParentId IN: Trigger.newMap.keySet() AND RowCause = 'Manual']);
            List<Database.DeleteResult> sr = Database.delete(prepPeriodShareOldList,false);         
        }
    }

    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     * 
     * @param      so     The SObject (BI_SP_Preparation_period__c)
     * 
     */
    public void beforeInsert(SObject so) {}

    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     * 
     * @param      oldSo  The old SObject (BI_SP_Preparation_period__c)
     * @param      so     The SObject (BI_SP_Preparation_period__c)
     * 
     */
    public void beforeUpdate(SObject oldSo, SObject so) {}

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     * 
     * @param      so     The SObject (BI_SP_Preparation_period__c)
     * 
     */
    public void beforeDelete(SObject so) {
        BI_SP_Preparation_period__c prepPeriod = (BI_SP_Preparation_period__c)so;

        if(preparationMap.get(prepPeriod.Id)!=null){
            //if the preparation period has preparations related, cant be deleted
            prepPeriod.addError(System.Label.BI_SP_PrepPeriodCantBeDeleted);
        }
    }

    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     * 
     * @param      so     The SObject (BI_SP_Preparation_period__c)
     * 
     */
    public void afterInsert(SObject so) {
        manageSharing(so);
    }


    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     * 
     * @param      oldSo  The old SObject (BI_SP_Preparation_period__c)
     * @param      so     The SObject (BI_SP_Preparation_period__c)
     * 
     */
    public void afterUpdate(SObject oldSo, SObject so) {
        manageSharing(so);
    }

    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     * 
     * @param      so     The SObject (BI_SP_Preparation_period__c)
     * 
     */
    public void afterDelete(SObject so) {}

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        if(Trigger.isAfter && !Trigger.isDelete){
            List<Database.SaveResult> sr = Database.insert(prepPeriodsShareList,false);
        }
    }

    /**
     * Prepare the sharing records for the preparation period
     *
     * @param      so     The SObject (BI_SP_Preparation_period__c)
     * 
     */
    private void manageSharing(SObject so){     
        BI_SP_Preparation_period__c prepPeriod = (BI_SP_Preparation_period__c)so;
        String country = prepPeriodCountryMap.get(prepPeriod.Id);

        List<PermissionSetAssignment> readWriteUsers = new List<PermissionSetAssignment>();
        readWriteUsers.addAll(new List<PermissionSetAssignment>(region_AdminUsersInRegionSetMap.get(country)));
        readWriteUsers.addAll(new List<PermissionSetAssignment>(region_ServiceClientUsersInRegionSetMap.get(country)));

        //sharing records for Admins and Service Client
        for(PermissionSetAssignment rW : readWriteUsers){
            BI_SP_Preparation_period__Share prepPeriodShare = new BI_SP_Preparation_period__Share();
            prepPeriodShare.ParentId = prepPeriod.Id;
            prepPeriodShare.UserOrGroupId = rw.Assignee.Id;
            prepPeriodShare.AccessLevel = 'Edit'; 
            prepPeriodShare.RowCause = 'Manual';
            prepPeriodsShareList.add(prepPeriodShare);
        }

        //sharing records for users in region
        List<User> readUsers = new List<User>(region_UsersInRegionSetMap.get(country));
        for(User u : readUsers){
            BI_SP_Preparation_period__Share prepPeriodShare = new BI_SP_Preparation_period__Share();
            prepPeriodShare.ParentId = prepPeriod.Id;
            prepPeriodShare.UserOrGroupId = u.Id;
            prepPeriodShare.AccessLevel = 'Read'; 
            prepPeriodShare.RowCause = 'Manual';
            prepPeriodsShareList.add(prepPeriodShare);
        }
    }

    /**
     * Gets the regions for countries of the preparatuion period list
     *
     * @param      prepPeriodList  The preparation period list
     *
     * @return     The regions for countries. < countryCode, region >
     */
    private Map<String, String> getRegionsForCountries(List<BI_SP_Preparation_period__c> prepPeriodList) {
        Set<String> countriesSet = new Set<String>();
        for (BI_SP_Preparation_period__c prepPeriod : prepPeriodList) {
            countriesSet.add(prepPeriod.BI_SP_Planit_cycle__r.BI_PL_Country_code__c);
        }

        Map<String, String> countryRegionMap = new Map<String, String>();
        for (BI_SP_Country_settings__c cs : [SELECT BI_SP_Region__c, BI_SP_Country_code__c
                                                FROM BI_SP_Country_settings__c
                                                WHERE BI_SP_Country_code__c IN :countriesSet]) {
            countryRegionMap.put(cs.BI_SP_Country_code__c, cs.BI_SP_Region__c);
        }

        return countryRegionMap;
    }

    /**
     * Complete the following structures: 
     * region_UsersInRegionSetMap < region, Set of PermissionSetAssignment >
     * region_AdminUsersInRegionSetMap < region, Set of PermissionSetAssignment >
     * region_ServiceClientUsersInRegionSetMap < region, Set of User >
     *
     * @param      prepPeriodList  The preparation period list
     * 
     */
    private void getUsersForRegions(List<BI_SP_Preparation_period__c> prepPeriodList) {
        Set<String> countriesSet = new Set<String>();
        for (BI_SP_Preparation_period__c prepPeriod : prepPeriodList) {
            countriesSet.add(prepPeriod.BI_SP_Planit_cycle__r.BI_PL_Country_code__c);
        }

        Map<Id, Id> usersUsed = new Map<Id,Id>();   //<UserId, userId>
        region_AdminUsersInRegionSetMap = new Map<String, Set<PermissionSetAssignment>>();  //< region, Set of PermissionSetAssignment >
        for (PermissionSetAssignment pa : [SELECT Id, Assignee.Id, Assignee.Name, Assignee.Country_Code_BI__c, PermissionSet.Name 
                            FROM PermissionSetAssignment 
                            WHERE Assignee.IsActive = true 
                            AND Assignee.Country_Code_BI__c IN :countriesSet
                            AND PermissionSet.Name = :ADMIN_PERMISSION_SET_APINAME]){           
            if(!usersUsed.containsKey(pa.Assignee.Id)){
                String country = pa.Assignee.Country_Code_BI__c;
                String region = countryRegionMap.get(country);
                Set<PermissionSetAssignment> paSet = new Set<PermissionSetAssignment>();
                if(region_AdminUsersInRegionSetMap.containsKey(region)){
                    paSet=region_AdminUsersInRegionSetMap.get(region);
                }
                paSet.add(pa);
                region_AdminUsersInRegionSetMap.put(region,paSet);
            }
            usersUsed.put(pa.Assignee.Id,pa.Assignee.Id);
        }

        region_ServiceClientUsersInRegionSetMap = new Map<String, Set<PermissionSetAssignment>>();  //< region, Set of PermissionSetAssignment >
        for (PermissionSetAssignment pa : [SELECT Id, Assignee.Id, Assignee.Name, Assignee.Country_Code_BI__c, PermissionSet.Name 
                            FROM PermissionSetAssignment 
                            WHERE Assignee.IsActive = true 
                            AND Assignee.Country_Code_BI__c IN :countriesSet
                            AND PermissionSet.Name = :CLIENTSERVICE_PERMISSION_SET_APINAME]){       
            if(!usersUsed.containsKey(pa.Assignee.Id)){
                String country = pa.Assignee.Country_Code_BI__c;
                String region = countryRegionMap.get(country);
                Set<PermissionSetAssignment> paSet = new Set<PermissionSetAssignment>();
                if(region_ServiceClientUsersInRegionSetMap.containsKey(region)){
                    paSet=region_ServiceClientUsersInRegionSetMap.get(region);
                }
                paSet.add(pa);
                region_ServiceClientUsersInRegionSetMap.put(region,paSet);
            }
            usersUsed.put(pa.Assignee.Id,pa.Assignee.Id);
        }       

        region_UsersInRegionSetMap = new Map<String, Set<User>>();  //< region, Set of Users >
        for (User user : [SELECT Id, Name, Country_Code_BI__c FROM User WHERE IsActive = true AND Country_Code_BI__c IN :countriesSet]){
            if(!usersUsed.containsKey(user.Id)){
                String country = user.Country_Code_BI__c;
                String region = countryRegionMap.get(country);
                Set<User> userSet = new Set<User>();
                if(region_UsersInRegionSetMap.containsKey(region)){
                    userSet=region_UsersInRegionSetMap.get(region);
                }
                userSet.add(user);
                region_UsersInRegionSetMap.put(region,userSet);
            }
            usersUsed.put(user.Id,user.Id);
        }
    }
}