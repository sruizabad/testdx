@isTest
private class BI_SP_PreparationPeriodTrigger_Test {
	
	static String countryCode = 'BR';

	@Testsetup
    static void setUp() {
    	User salesRep = BI_SP_TestDataUtility.getSalesRepUser(countryCode, 0);
		User pm = BI_SP_TestDataUtility.getProductManagerUser(countryCode, 0);
		User cs = BI_SP_TestDataUtility.getClientServiceUser(countryCode, 0);
		User admin = BI_SP_TestDataUtility.getAdminUser(countryCode, 0);
		User reportb = BI_SP_TestDataUtility.getReportBuilderUser(countryCode, 0);
        
        System.runAs(admin){
	        BI_SP_TestDataUtility.createCustomSettings();
	        List<Customer_Attribute_BI__c> specs = BI_SP_TestDataUtility.createSpecialties();
	        List<Account> accounts = BI_SP_TestDataUtility.createAccounts(specs);
	        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
	        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());
	        List<BI_SP_Product_family_assignment__c> productsAssigmentsPM = BI_SP_TestDataUtility.createProductsAssigments(products, pm.Id);


	        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
	        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

	        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
	        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
        }
    }

	@isTest static void test_update() {
		List<BI_SP_Preparation_period__c> prepPeriodList = new List<BI_SP_Preparation_period__c>([SELECT Id, BI_SP_Allow_edit_dates__c FROM BI_SP_Preparation_period__c]);
		for(BI_SP_Preparation_period__c prepPeriod : prepPeriodList){
			prepPeriod.BI_SP_Allow_edit_dates__c = true;
		}
		update prepPeriodList;
	}
	
	@isTest static void test_delete() {
		List<BI_SP_Preparation_period__c> prepPeriodList = new List<BI_SP_Preparation_period__c>([SELECT Id, BI_SP_Allow_edit_dates__c FROM BI_SP_Preparation_period__c]);
		Database.delete(prepPeriodList, false);
	}
}