/**
 * Handler for BI_SP_Preparation__c Trigger.
 */
public with sharing class BI_SP_PreparationTriggerHandler implements BI_SP_TriggerInterface{
	
	public static final String SALESREP_PERMISSION_SET_APINAME = 'BI_SP_SALES'; 
	public Map<Id,String> preparationPositionMap; //<preparationId, TerritoryName>
	public Map<String,String> countryRegionMap;	//<countrycode, region>
	public Map<String,Set<UserTerritory>> position_SalesRepUserTerritoriesInPositionSetMap;	//<territoryName, Set of UserTerritory>
	public List<BI_SP_Preparation__Share> preparationsShareList;

	/**
	 * Constructs the object.
	 */
	public BI_SP_PreparationTriggerHandler() {}

	/**
	 * bulkBefore
	 *
	 * This method is called prior to execution of a BEFORE trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkBefore() {}

	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkAfter() {		
        if (Trigger.isInsert || Trigger.isUpdate) {
            //prepare structures to create dinamic sharing
        	List<BI_SP_Preparation__c> preparationList = new List<BI_SP_Preparation__c>([SELECT Id, BI_SP_Position__c, BI_SP_Position__r.Name FROM BI_SP_Preparation__c WHERE Id IN: Trigger.newMap.keySet()]);

			preparationPositionMap = new Map<Id,String>();
        	for(BI_SP_Preparation__c preparation : preparationList){
        		preparationPositionMap.put(preparation.Id, preparation.BI_SP_Position__r.Name);
        	}
        	getUsersForPositions(preparationList);
        	preparationsShareList = new List<BI_SP_Preparation__Share>();
        }

        if (Trigger.isUpdate) {
            //delete old dinamic sharing
        	List<BI_SP_Preparation__Share> preparationShareOldList = new List<BI_SP_Preparation__Share>([SELECT Id, ParentId FROM BI_SP_Preparation__Share WHERE ParentId IN: Trigger.newMap.keySet() AND RowCause = 'Manual']);
			List<Database.DeleteResult> sr = Database.delete(preparationShareOldList,false);        	
        }
	}

	/**
	 * beforeInsert
	 *
	 * This method is called iteratively for each record to be inserted during a BEFORE
	 * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
	 * 
	 * @param      so    The SObject (BI_SP_Preparation__c)
	 * 
	 */
	public void beforeInsert(SObject so) {}

	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
	 * 
	 * @param      oldSo  The old SObject (BI_SP_Preparation__c)
	 * @param      so     The SObject (BI_SP_Preparation__c)
	 * 
	 */
	public void beforeUpdate(SObject oldSo, SObject so) {}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
	 * 
	 * @param      so    The SObject (BI_SP_Preparation__c)
	 * 
	 */
	public void beforeDelete(SObject so) {}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
	 * 
	 * @param      so    The SObject (BI_SP_Preparation__c)
	 * 
	 */
	public void afterInsert(SObject so) {
		manageSharing(so);
	}


	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
	 * 
	 * @param      oldSo  The old SObject (BI_SP_Preparation__c)
	 * @param      so     The SObject (BI_SP_Preparation__c)
	 * 
	 */
	public void afterUpdate(SObject oldSo, SObject so) {
		manageSharing(so);
	}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
	 * 
	 * @param      so    The SObject (BI_SP_Preparation__c)
	 * 
	 */
	public void afterDelete(SObject so) {}

	/**
	 * andFinally
	 *
	 * This method is called once all records have been processed by the trigger. Use this
	 * method to accomplish any final operations such as creation or updates of other records.
	 */
	public void andFinally() {
		if(Trigger.isAfter && !Trigger.isDelete){
			List<Database.SaveResult> sr = Database.insert(preparationsShareList,false);
		}
	}

    /**
     * Prepare the sharing records for the preparation
	 * 
	 * @param      so    The SObject (BI_SP_Preparation__c)
	 * 
	 */
	private void manageSharing(SObject so){		
		BI_SP_Preparation__c preparation = (BI_SP_Preparation__c)so;
		String position = preparationPositionMap.get(preparation.Id);

		List<UserTerritory> readWriteUserTerritories = new List<UserTerritory>();
		readWriteUserTerritories.addAll(new List<UserTerritory>(position_SalesRepUserTerritoriesInPositionSetMap.get(position)));

        //sharing records for the sales rep users
		for(UserTerritory userTerritory : readWriteUserTerritories){
			BI_SP_Preparation__Share preparationShare = new BI_SP_Preparation__Share();
			preparationShare.ParentId = preparation.Id;
			preparationShare.UserOrGroupId = userTerritory.UserId;
			preparationShare.AccessLevel = 'Edit'; 
			preparationShare.RowCause = 'Manual';
			preparationsShareList.add(preparationShare);
		}
	}

	/**
	 * Complete the following structures: 
     * position_SalesRepUserTerritoriesInPositionSetMap < territoryName, Set of UserTerritory >
	 *
	 * @param      preparationList  The preparation list
	 * 
	 */
	private void getUsersForPositions(List<BI_SP_Preparation__c> preparationList) {
		List<PermissionSetAssignment> salesRepUsersPermissionSetList = new List<PermissionSetAssignment>([SELECT Id, Assignee.Id, Assignee.Name, PermissionSet.Name 
							FROM PermissionSetAssignment 
							WHERE Assignee.IsActive = true 
							AND  PermissionSet.Name = :SALESREP_PERMISSION_SET_APINAME]);

		Set<Id> salesRepIds = new Set<Id>();
    	for(PermissionSetAssignment salesRepUsersPermissionSet : salesRepUsersPermissionSetList){
    		salesRepIds.add(salesRepUsersPermissionSet.AssigneeId);
    	}
    	
		position_SalesRepUserTerritoriesInPositionSetMap = new Map<String, Set<UserTerritory>>();

		Map<Id,Territory> territories = new Map<Id,Territory>([SELECT Id, Name 
                                                                FROM Territory 
                                                                WHERE Name IN :preparationPositionMap.values()]);

    	for(UserTerritory userTerritory : [SELECT Id, IsActive, TerritoryId, UserId FROM UserTerritory WHERE TerritoryId IN :territories.keySet() AND UserId IN :salesRepIds AND IsActive = true]){
    		String position = territories.get(userTerritory.TerritoryId).Name;
			Set<UserTerritory> usersTerritorySet = new Set<UserTerritory>();
			if(position_SalesRepUserTerritoriesInPositionSetMap.containsKey(position)){ usersTerritorySet=position_SalesRepUserTerritoriesInPositionSetMap.get(position); }
			usersTerritorySet.add(userTerritory);
			position_SalesRepUserTerritoriesInPositionSetMap.put(position,usersTerritorySet);
    	}
	}
}