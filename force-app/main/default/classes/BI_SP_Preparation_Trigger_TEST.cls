@isTest
private class BI_SP_Preparation_Trigger_TEST {
	
	@testSetup
	static void setup(){
		// Role
		UserRole rol = new UserRole();
		rol.Name = 'SPAIN';
		rol.DeveloperName = 'SPAIN_Test';
		insert rol;

		// User
		List<User> users = new List<User>();
		User u = new User();
		u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'ES_FR_SALES_CHC' LIMIT 1].Id;
		u.LastName = 'Last name';
		u.Email = 'testEmail@email.com';
		u.Username = 'testEmailSampro@email.com';
		u.Alias = 'alias';
		u.TimeZoneSidKey = 'America/Los_Angeles';
     	u.EmailEncodingKey = 'UTF-8';
     	u.LanguageLocaleKey = 'en_US';
     	u.LocaleSidKey = 'en_US';
     	u.UserRoleId = rol.Id;
     	users.add(u);

     	User u2 = new User();
		u2.ProfileId = [SELECT Id FROM Profile WHERE Name = 'ES_FR_SALES_CHC' LIMIT 1].Id;
		u2.LastName = 'Last name 2';
		u2.Email = 'testEmail2@email.com';
		u2.Username = 'testEmail2Sampro@email.com';
		u2.Alias = 'alias2';
		u2.TimeZoneSidKey = 'America/Los_Angeles';
     	u2.EmailEncodingKey = 'UTF-8';
     	u2.LanguageLocaleKey = 'en_US';
     	u2.LocaleSidKey = 'en_US';
     	u2.UserRoleId = rol.Id;
     	users.add(u2);
     	insert users;

     	// Permission Set Assignments
     	List<PermissionSetAssignment> psAssList = new List<PermissionSetAssignment>();
     	Id psId = [SELECT Id FROM PermissionSet WHERE Name = 'BI_SP_SALES' LIMIT 1].Id;
     	PermissionSetAssignment psAss = new PermissionSetAssignment();
     	psAss.PermissionSetId = psId;
     	psAss.AssigneeId = u.Id;
     	psAssList.add(psAss);

     	PermissionSetAssignment psAss2 = new PermissionSetAssignment();
     	psAss2.PermissionSetId = psId;
     	psAss2.AssigneeId = u2.Id;
     	psAssList.add(psAss2);
     	insert psAssList;
	}

	@isTest
	static void testTrigger(){
		// Position
		BI_PL_Position__c pos = new BI_PL_Position__c();
		pos.Name = 'SPAIN';
		pos.BI_PL_Country_code__c = 'ES';
		insert pos;
		Test.startTest();
		BI_SP_Preparation__c preparation = new BI_SP_Preparation__c();
		preparation.Name = 'Test Preparation';
		preparation.BI_SP_External_id__c = 'extID';
		preparation.BI_SP_Position__c = pos.Id;
		insert preparation;
		preparation.Name = 'Test Preparation UPD';
		update preparation;
		delete preparation;
		Test.stopTest();
	}
}