/**
 * Prroduct Family Assigmnet Controller.
 */
 public without sharing class BI_SP_ProductFamilyAssignmentNewExt {

	public String userPermissionsJSON {get;set;}	

	/**
	 * Constructs the object.
	 */
	public BI_SP_ProductFamilyAssignmentNewExt() {
		userPermissionsJSON = JSON.serialize(BI_SP_SamproServiceUtility.getCurrentUserPermissions());
	}

	/**
	 * Constructs the object.
	 *
	 * @param      controller  The standard controller
	 * 
	 */
	public BI_SP_ProductFamilyAssignmentNewExt(ApexPages.StandardController controller) {
		userPermissionsJSON = JSON.serialize(BI_SP_SamproServiceUtility.getCurrentUserPermissions());
	}

	/**
	 * Gets the filters.
	 *
	 * @return     The filters.
	 */
	@RemoteAction
	public static FiltersModel getFilters() {
		Map<String, Boolean> userPermissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
		FiltersModel fm = new FiltersModel();
		fm.countries = getUserCountryCodes(userPermissions.get('isRB'), false, null);
		fm.region = getUserRegionCodes(userPermissions.get('isRB'));
		return fm;
	}
	
    /**
     * Gets the country codes for the region of the current user.
     *
     * @param      isRb                   Indicates if is ReportBuilder
     * @param      selectedReportBuilder  Indicates if is access like report builder
     * @param      region                 The region
     *
     * @return     The country codes.
     */
	@RemoteAction
	public static String getUserCountryCodes(Boolean isRb, Boolean selectedReportBuilder, String region) {
    	List<String> countryCodes = new List<String>();
    	if(!isRB || !selectedReportBuilder){
        	countryCodes = new List<String>(BI_SP_SamproServiceUtility.getSetCurrentUserRegionCountryCodes());
    	}else{
    		countryCodes = new List<String>(BI_SP_SamproServiceUtility.getCountryCodesForRegion(region));
    	}
        return JSON.serialize(countryCodes);
	}
    
    /**
     * Gets the region codes for the current user.
     *
     * @param      isRb  Indicates if is ReportBuilder
     *
     * @return     The user region codes.
     */
    public static String getUserRegionCodes(Boolean isRb) {
    	List<String> regionCodes = new List<String>();
    	if(isRB){
        	regionCodes = new List<String>(BI_SP_SamproServiceUtility.getSetReportBuilderRegionCodes());
        }
        return JSON.serialize(regionCodes);
    }

    /**
     * Gets the product family items for the country
     *
     * @param      countryCode  The country code
     *
     * @return     The product family items.
     */
	@RemoteAction
    public static List<ProductFamily> getProductFamilyItems(String countryCode) {
    	Map<String, ProductFamily> articleFamilyMap = new Map<String, ProductFamily>(); //Product Family , articleData		

		//Get all the affected articles, groupped by product family
		for (AggregateResult ar : [SELECT BI_SP_Product_family__c,
								   BI_SP_Product_family_description__c,
		                           BI_SP_Veeva_product_family__c veevaFamilyId,
		                           BI_SP_Veeva_product_family__r.Name veevaFamilyName,
		                           BI_SP_Veeva_product_family__r.Description_vod__c  veevaFamilyDescription,
		                           BI_SP_Veeva_product_family__r.Active_BI__c  veevaFamilyActive
		                           FROM BI_SP_Article__c
		                           where (BI_SP_Country_code__c = :BI_SP_SamproServiceUtility.getRegionRawCountryCodes(countryCode)
                                          OR BI_SP_Country_code__c IN :BI_SP_SamproServiceUtility.getSecondaryRegionRawCountryCodes(countryCode))
		                                   group by BI_SP_Product_family__c,BI_SP_Product_family_description__c, BI_SP_Veeva_product_family__c, BI_SP_Veeva_product_family__r.Name, BI_SP_Veeva_product_family__r.Description_vod__c, BI_SP_Veeva_product_family__r.Active_BI__c]) {

			String productFamily = (ar.get('BI_SP_Product_family__c') != null ? String.valueOf(ar.get('BI_SP_Product_family__c')) : '');
			String veevaProductFamily = (ar.get('veevaFamilyId') != null ? String.valueOf(ar.get('veevaFamilyId')) : '');
			
			//Don't show empty product families
			if (productFamily != '') {				
				if (veevaProductFamily != null && veevaProductFamily != '') {
					articleFamilyMap.put(productFamily, new ProductFamily(ar.get('BI_SP_Product_family__c'), ar.get('BI_SP_Product_family_description__c'), ar.get('veevaFamilyId'), ar.get('veevaFamilyName'), ar.get('veevaFamilyDescription'), ar.get('veevaFamilyActive')));
				} else {
					articleFamilyMap.put(productFamily, new ProductFamily(ar.get('BI_SP_Product_family__c'), ar.get('BI_SP_Product_family_description__c')));
				}
			}
		}

		List<ProductFamily> productFamilies = articleFamilyMap.values();//new List<ProductFamily>();
		
		return productFamilies;
	}

	/**
	 * Saves a list of product family assignments.
	 *
	 * @param      productFamilyList  The product family list
	 * @param      countryCode        The country code
	 *
	 * @return     {errors
	 */
	@RemoteAction
    public static String saveProductFamilyAssignment(List<ProductFamily> productFamilyList, String countryCode){
    	String error = '';

    	Set<String> allFamiliesToUpdate = new Set<String>();
    	Map<String, ProductFamily> productFamilyMap	= new Map<String, ProductFamily>();

		//Get all updated lines
		for (ProductFamily pF : productFamilyList) {
			if (pf.productFamily != null && pf.productFamily != '') {
				allFamiliesToUpdate.add(pf.productFamily);
			}
			productFamilyMap.put(pf.productFamily, pf);
		}

		//Update all articles with the same family product
		Map<Id, BI_SP_Article__c> articlesToSave = new Map<Id, BI_SP_Article__c>([Select Id, BI_SP_Product_family__c 
																				from BI_SP_Article__c 
																				where BI_SP_Product_family__c IN :allFamiliesToUpdate 
																					AND (BI_SP_Country_code__c = :BI_SP_SamproServiceUtility.getRegionRawCountryCodes(countryCode)
                                          												OR BI_SP_Country_code__c IN :BI_SP_SamproServiceUtility.getSecondaryRegionRawCountryCodes(countryCode))]);

		for (BI_SP_Article__c article : articlesToSave.values()) {
			article.BI_SP_Veeva_product_family__c = (productFamilyMap.get(article.BI_SP_Product_family__c).veevaProductFamily == null ? null : productFamilyMap.get(article.BI_SP_Product_family__c).veevaProductFamily.Id);
		}

		try {
			update articlesToSave.values();
		} catch (Exception e) {
			error= Label.BI_SP_Error + ' ' + e;
		}

    	return error;
    }

	/**
	 * Class for filters model.
	 */
	public class FiltersModel{
		public String countries; // JSON SERIALIZADO
		public String region; // JSON SERIALIZADO
	}

	/**
	 * Class for product family.
	 */
	public class ProductFamily {
		public String productFamily;
		public String productFamilyDescription;
		public String veevaProductFamilyId;
		public Product_vod__c veevaProductFamily;
		public boolean isAssignment;

		/**
		 * Constructs the object.
		 *
		 * @param      productFamily                  The product family
		 * @param      productFamilyDescription       The product family description
		 * @param      veevaProductFamilyId           The veeva product family identifier
		 * @param      veevaProductFamilyName         The veeva product family name
		 * @param      veevaProductFamilyDescription  The veeva product family description
		 * @param      veevaProductFamilyActive       The veeva product family active flag
		 */
		public ProductFamily(Object productFamily, Object productFamilyDescription, Object veevaProductFamilyId, Object veevaProductFamilyName, Object veevaProductFamilyDescription, Object veevaProductFamilyActive) {
			if (productFamily != null){
				this.productFamily = String.valueOf(productFamily);
			}else{
				this.productFamily = '';
			}

			if (productFamilyDescription != null){
				this.productFamilyDescription = String.valueOf(productFamilyDescription);
			}else{
				this.productFamilyDescription = '';
			}

			String veevaProductFamilyNameStr;
			String veevaProductFamilyDescriptionStr;
			Boolean veevaProductFamilyActiveBool;
			if (veevaProductFamilyId != null){
				this.veevaProductFamilyId = String.valueOf(veevaProductFamilyId);
			}else{
				this.veevaProductFamilyId = '';
			}

			if (veevaProductFamilyName != null){
				veevaProductFamilyNameStr = String.valueOf(veevaProductFamilyName);
			}else{
				veevaProductFamilyNameStr = '';
			}

			if (veevaProductFamilyDescription != null){
				veevaProductFamilyDescriptionStr = String.valueOf(veevaProductFamilyDescription);
			}else{
				veevaProductFamilyDescriptionStr = '';
			}

			if (veevaProductFamilyActive != null){
				veevaProductFamilyActiveBool = Boolean.valueOf(veevaProductFamilyActive);
			}else{
				veevaProductFamilyActiveBool = false;
			}

			if (this.veevaProductFamilyId != null && this.veevaProductFamilyId != ''){
				this.veevaProductFamily = new Product_vod__c(Id = this.veevaProductFamilyId, Name = veevaProductFamilyNameStr, Description_vod__c = veevaProductFamilyDescriptionStr, Active_BI__c = veevaProductFamilyActiveBool);
				this.isAssignment = true;
			}else{
				this.veevaProductFamily = new Product_vod__c();
				this.isAssignment = false;
			}
		}
		
		/**
		 * Constructs the object.
		 *
		 * @param      productFamily             The product family
		 * @param      productFamilyDescription  The product family description
		 */
		public ProductFamily(Object productFamily, Object productFamilyDescription) {
			if (productFamily != null){
				this.productFamily = String.valueOf(productFamily);
			}else{
				this.productFamily = '';
			}

			if (productFamilyDescription != null){
				this.productFamilyDescription = String.valueOf(productFamilyDescription);
			}else{
				this.productFamilyDescription = '';
			}

			this.veevaProductFamilyId = '';
			this.veevaProductFamily = new Product_vod__c();
			this.isAssignment = false;
		}
	}
}