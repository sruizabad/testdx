@isTest
private class BI_SP_ProductFamilyAssignmentNewExt_Test {

    public static String countryCode;
    public static String regionCode;

	@Testsetup
	static void setUp() {
		BI_SP_TestDataUtility.createCustomSettings();
		List<Account> accounts = BI_SP_TestDataUtility.createAccounts();
		List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts();

		countryCode = 'BR';
        regionCode = 'BR';

		//PLANIT TEST DATA
		List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
		List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
		List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
	}


	@isTest static void BI_SP_ProductFamilyAssignmentNewExt_initialize_Test() {
		String countryCode='BR';
		BI_SP_Article__c pp = [Select Id,Name from BI_SP_Article__c where BI_SP_Country_code__c = :countryCode LIMIT 1];
		Product_vod__c prod = [Select Id from Product_vod__c LIMIT 1];

		BI_SP_ProductFamilyAssignmentNewExt ppClass = new BI_SP_ProductFamilyAssignmentNewExt();

		ApexPages.StandardController sc = new ApexPages.StandardController(pp);
		ppClass = new BI_SP_ProductFamilyAssignmentNewExt(sc);

		BI_SP_ProductFamilyAssignmentNewExt.ProductFamily p = new BI_SP_ProductFamilyAssignmentNewExt.ProductFamily(null, null, null, null, null, null);
		BI_SP_ProductFamilyAssignmentNewExt.ProductFamily p2 = new BI_SP_ProductFamilyAssignmentNewExt.ProductFamily(null, null);
		
		BI_SP_ProductFamilyAssignmentNewExt.FiltersModel fil = BI_SP_ProductFamilyAssignmentNewExt.getFilters();

        String userCountryCodeJSON = BI_SP_ProductFamilyAssignmentNewExt.getUserCountryCodes(true, true, regionCode);
        String userCountryCodeJSON2 = BI_SP_ProductFamilyAssignmentNewExt.getUserCountryCodes(false, false, regionCode);
        String userRegionCodeJSON = BI_SP_ProductFamilyAssignmentNewExt.getUserRegionCodes(true);
        List<BI_SP_ProductFamilyAssignmentNewExt.ProductFamily> prodList = BI_SP_ProductFamilyAssignmentNewExt.getProductFamilyItems(countryCode);

        String error = BI_SP_ProductFamilyAssignmentNewExt.saveProductFamilyAssignment(prodList, countryCode);
	}
}