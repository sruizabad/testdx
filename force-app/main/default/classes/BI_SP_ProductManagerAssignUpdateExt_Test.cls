@isTest
private class BI_SP_ProductManagerAssignUpdateExt_Test {

    public static String countryCode;
    public static String regionCode;

    @Testsetup
    static void setUp() {
        countryCode = 'BR';
        regionCode = 'BR';
        BI_SP_TestDataUtility.createCustomSettings();
        List<Account> accounts = BI_SP_TestDataUtility.createAccounts();
        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);

        //PLANIT TEST DATA
        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
    }

    @isTest static void BI_SP_ProductManagerAssignmentNewExt_initialize_Test() {
        String countryCode='BR';
        BI_SP_Article__c pp = [Select Id,Name from BI_SP_Article__c where BI_SP_Country_code__c = :countryCode LIMIT 1];
        Product_vod__c prod = [Select Id from Product_vod__c LIMIT 1];

        BI_SP_ProductManagerAssignmentUpdateExt ppClass = new BI_SP_ProductManagerAssignmentUpdateExt();

        ApexPages.StandardController sc = new ApexPages.StandardController(pp);
        ppClass = new BI_SP_ProductManagerAssignmentUpdateExt(sc);

        BI_SP_ProductManagerAssignmentUpdateExt.ProductManger p = new BI_SP_ProductManagerAssignmentUpdateExt.ProductManger(null, null, null, null, null, null, null, null, null);

        String error = BI_SP_ProductManagerAssignmentUpdateExt.newProductManagerAssignment(prod.Id, UserInfo.getUserId());
        error = BI_SP_ProductManagerAssignmentUpdateExt.newProductManagerAssignment(prod.Id, UserInfo.getUserId());
        error = BI_SP_ProductManagerAssignmentUpdateExt.newProductManagerAssignment(null, UserInfo.getUserId());
        error = BI_SP_ProductManagerAssignmentUpdateExt.newProductManagerAssignment(prod.Id, null);
        List<Product_vod__c> productVeevaList = BI_SP_ProductManagerAssignmentUpdateExt.getVeevaFamily(countryCode);
        
        BI_SP_ProductManagerAssignmentUpdateExt.FiltersModel fil = BI_SP_ProductManagerAssignmentUpdateExt.getFilters();

        String userCountryCodeJSON = BI_SP_ProductManagerAssignmentUpdateExt.getUserCountryCodes(true, true, regionCode);
        String userCountryCodeJSON2 = BI_SP_ProductManagerAssignmentUpdateExt.getUserCountryCodes(false, false, regionCode);
        String userRegionCodeJSON = BI_SP_ProductManagerAssignmentUpdateExt.getUserRegionCodes(true);
        String countryRegionCodeJSON = BI_SP_ProductManagerAssignmentUpdateExt.getCountryRegionCountryCodes(countryCode);
        List<BI_SP_ProductManagerAssignmentUpdateExt.ProductManger> prodList = BI_SP_ProductManagerAssignmentUpdateExt.getProductManagerItems(countryCode);

        error = BI_SP_ProductManagerAssignmentUpdateExt.saveProductManagerAssignment(prodList);
        
        BI_SP_Product_family_assignment__c pFam = [Select Id from BI_SP_Product_family_assignment__c Limit 1];
        error = BI_SP_ProductManagerAssignmentUpdateExt.deleteProductManagerItem(pFam.Id);
    }
}