/**
 * Prroduct Manager Assigmnet Controller.
 */
public without sharing class BI_SP_ProductManagerAssignmentUpdateExt {

    public String userPermissionsJSON {get;set;}    

    /**
     * Constructs the object.
     */
    public BI_SP_ProductManagerAssignmentUpdateExt() {
        userPermissionsJSON = JSON.serialize(BI_SP_SamproServiceUtility.getCurrentUserPermissions());
    }

    /**
     * Constructs the object.
     *
     * @param      controller  The standard controller
     * 
     */
    public BI_SP_ProductManagerAssignmentUpdateExt(ApexPages.StandardController controller) {
        userPermissionsJSON = JSON.serialize(BI_SP_SamproServiceUtility.getCurrentUserPermissions());
    }

    /**
     * Gets the filters.
     *
     * @return     The filters.
     */
    @RemoteAction
    public static FiltersModel getFilters() {
        Map<String, Boolean> userPermissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
        FiltersModel fm = new FiltersModel();
        fm.countries = getUserCountryCodes(userPermissions.get('isRB'), false, null);
        fm.region = getUserRegionCodes(userPermissions.get('isRB'));
        return fm;
    }

    /**
     * Gets the country codes for the region of the current user.
     *
     * @param      isRb                   Indicates if is ReportBuilder
     * @param      selectedReportBuilder  Indicates if is access like report builder
     * @param      region                 The region
     *
     * @return     The country codes.
     */
    @RemoteAction
    public static String getUserCountryCodes(Boolean isRb, Boolean selectedReportBuilder, String region) {
        List<String> countryCodes = new List<String>();
        if(!isRB || !selectedReportBuilder){
            countryCodes = new List<String>(BI_SP_SamproServiceUtility.getSetCurrentUserRegionCountryCodes());
        }else{
            countryCodes = new List<String>(BI_SP_SamproServiceUtility.getCountryCodesForRegion(region));
        }
        return JSON.serialize(countryCodes);
    }

    /**
     * Gets the region codes for the current user.
     *
     * @param      isRb  Indicates if is ReportBuilder
     *
     * @return     The user region codes.
     */
    public static String getUserRegionCodes(Boolean isRb) {
        List<String> regionCodes = new List<String>();
        if(isRB){
            regionCodes = new List<String>(BI_SP_SamproServiceUtility.getSetReportBuilderRegionCodes());
        }
        return JSON.serialize(regionCodes);
    }

    /**
     * Gets the product manager items for the country
     *
     * @param      countryCode  The country code
     *
     * @return     The product manager items.
     */
    @RemoteAction
    public static List<ProductManger> getProductManagerItems(String countryCode) {
        //Map<Id, BI_SP_Product_family_assignment__c> recordMap = new Map<Id, BI_SP_Product_family_assignment__c>([Select Id, Name, BI_SP_User__c, BI_SP_User__r.Name, BI_SP_User__r.FirstName, BI_SP_User__r.LastName, BI_SP_User__r.Country_Code_BI__c, BI_SP_Veeva_product_catalog__c, BI_SP_Veeva_product_catalog__r.Name, BI_SP_Veeva_product_catalog__r.Country_Code_BI__c, BI_SP_Veeva_product_catalog__r.Description_vod__c
        //        from BI_SP_Product_family_assignment__c
        //        where BI_SP_Veeva_product_catalog__r.Country_Code_BI__c = : BI_SP_SamproServiceUtility.getRegionRawCountryCodes(countryCode) AND BI_SP_Veeva_product_catalog__r.Product_Type_vod__c = 'Detail' AND BI_SP_Veeva_product_catalog__r.Active_BI__c=true]);
        Map<Id, BI_SP_Product_family_assignment__c> recordMap = new Map<Id, BI_SP_Product_family_assignment__c>([Select Id, Name, BI_SP_User__c, BI_SP_User__r.Name, BI_SP_User__r.FirstName, BI_SP_User__r.LastName, BI_SP_User__r.Country_Code_BI__c, BI_SP_Veeva_product_catalog__c, BI_SP_Veeva_product_catalog__r.Name, BI_SP_Veeva_product_catalog__r.Country_Code_BI__c, BI_SP_Veeva_product_catalog__r.Description_vod__c, BI_SP_Veeva_product_catalog__r.Active_BI__c
                from BI_SP_Product_family_assignment__c
                where BI_SP_Veeva_product_catalog__r.Country_Code_BI__c = : BI_SP_SamproServiceUtility.getRegionRawCountryCodes(countryCode) AND BI_SP_Veeva_product_catalog__r.Product_Type_vod__c = 'Detail']);

        List<ProductManger> productManagerList = new List<ProductManger>();

        for (Id recordId : recordMap.keySet()) {
            productManagerList.add(new ProductManger(recordId, recordMap.get(recordId).BI_SP_Veeva_product_catalog__r.Name, recordMap.get(recordId).BI_SP_Veeva_product_catalog__r.Description_vod__c, recordMap.get(recordId).BI_SP_Veeva_product_catalog__r.Country_Code_BI__c, recordMap.get(recordId).BI_SP_Veeva_product_catalog__r.Active_BI__c, recordMap.get(recordId).BI_SP_User__c, recordMap.get(recordId).BI_SP_User__r.FirstName, recordMap.get(recordId).BI_SP_User__r.LastName, recordMap.get(recordId).BI_SP_User__r.Country_Code_BI__c));
        }
        return productManagerList;
    }

    @RemoteAction
    public static List<Product_vod__c> getVeevaFamily(String countryCode){
        List<String> countryCodes = new List<String>(BI_SP_SamproServiceUtility.getRegionCountryCodes(countryCode));

        List<BI_SP_Article__c> articles = new List<BI_SP_Article__c>();
        List<Id> prodIds = new List<Id>();
        for(BI_SP_Article__c a : [SELECT BI_SP_Veeva_product_family__c 
                                    FROM BI_SP_Article__c 
                                    WHERE BI_SP_Veeva_product_family__r.Product_Type_vod__c='Detail' 
                                    AND BI_SP_Veeva_product_family__r.Country_Code_BI__c IN :countryCodes]){
            prodIds.add(a.BI_SP_Veeva_product_family__c);
        }
        List<Product_vod__c> prods = new List<Product_vod__c>([SELECT Id,Name,Description_vod__c,Country_Code_BI__c,Active_BI__c FROM Product_vod__c WHERE ID IN :prodIds]);
        return prods;
    }

    /**
     * Delete a product manager item
     *
     * @param      recordId  The product manager item identifier
     *
     * @return     errors
     */
    @RemoteAction
    public static String deleteProductManagerItem(String recordId) {
        String errorString = '';
        if(recordId != null && recordId != ''){
            try {
                List<BI_SP_Product_family_assignment__c> pfaList = new List<BI_SP_Product_family_assignment__c>([Select Id from BI_SP_Product_family_assignment__c where Id = :recordId]);
                delete pfalist.get(0);
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, '### - BI_SP_ProductManagerAssignmentUpdateExt - deleteProductManagerItem - Product Family Assignment Error: -> ' + e);
                errorString=Label.BI_SP_Error + ' '  + e;
            }
        }
        return errorString;
    }

    /**
     * Gets the all country codes for the region of the countr.
     *
     * @param      countryCode  The country code
     *
     * @return     The region country codes.
     */
    @RemoteAction
    public static String getCountryRegionCountryCodes(String countryCode) {
        return BI_SP_SamproServiceUtility.stringOutOfSet(BI_SP_SamproServiceUtility.getRegionCountryCodes(countryCode));
    }

    /**
     * create a new product manager assignment
     *
     * @param      productId  The product identifier
     * @param      userId     The user identifier
     *
     * @return     errors
     */
    @RemoteAction
    public static String newProductManagerAssignment(String productId, String userId){
        String errorString = '';
        BI_SP_Product_family_assignment__c newRecord = new BI_SP_Product_family_assignment__c();

        if (productId != null && productId != ''){
            newRecord.BI_SP_Veeva_product_catalog__c = productId;
        }else{
            errorString = Label.BI_SP_ProductMustHaveValue;
            return errorString;
        }

        if (userId != null && userId != ''){
            newRecord.BI_SP_User__c = userId;
        }else{
            errorString = Label.BI_SP_UserMustHaveValue;
            return errorString;
        }

        try {
            List<BI_SP_Product_family_assignment__c> pfaList = new List<BI_SP_Product_family_assignment__c>([Select Id from BI_SP_Product_family_assignment__c where BI_SP_User__c = :newRecord.BI_SP_User__c AND BI_SP_Veeva_product_catalog__c = :newRecord.BI_SP_Veeva_product_catalog__c]);
            if (pfaList.isEmpty()) {//NO duplicates
                upsert newRecord;
            }else{
                errorString = Label.BI_SP_Record_already_exist;
            }
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, '### - BI_SP_ProductManagerAssignmentUpdateExt - newProductManagerAssignment - Product Family Assignment Error: -> ' + e);
            errorString = Label.BI_SP_Error + ' '  + e;
        }

        return errorString;
    }

    /**
     * Saves a product manager assignment.
     *
     * @param      productFamilyList  The product family list
     *
     * @return     errors
     */
    @RemoteAction
    public static String saveProductManagerAssignment(List<ProductManger> productFamilyList){
        String errorString = '';
        List<BI_SP_Product_family_assignment__c> recordsToUpdate = new List<BI_SP_Product_family_assignment__c>();

        for (ProductManger pf : productFamilyList) {
            if (pf.manager != null && pf.manager.Id != null){
                BI_SP_Product_family_assignment__c pfToUpdate = new BI_SP_Product_family_assignment__c(Id=pf.recordId, BI_SP_User__c=pf.manager.Id);
                recordsToUpdate.add(pfToUpdate);
            }else{
                errorString = Label.BI_SP_UserMustHaveValue;
                return errorString;
            }
        }

        try {
            update recordsToUpdate;
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, '### - BI_SP_ProductManagerAssignmentUpdateExt - saveProductManagerAssignment - Product Family Assignment Error: -> ' + e);
            errorString = Label.BI_SP_Error + ' '  + e;
        }

        return errorString;
    }

    /**
     * Class for filters model.
     */
    public class FiltersModel{
        public String countries; // JSON SERIALIZADO
        public String region; // JSON SERIALIZADO
    }

    /**
     * Class for product manger.
     */
    public class ProductManger {
        public String recordId;
        public String veevaFamilyName;
        public String veevaFamilyDescription;
        public String veevaFamilyCountryCode;
        public Boolean veevaFamilyActive;
        public String managerId;
        public User manager;

        /**
         * Constructs the object.
         *
         * @param      recordId                The record identifier
         * @param      veevaFamilyName         The veeva family name
         * @param      veevaFamilyDescription  The veeva family description
         * @param      veevaFamilyCountryCode  The veeva family country code
         * @param      veevaFamilyActive       The veeva family active
         * @param      managerId               The manager identifier
         * @param      managerFirstName        The manager first name
         * @param      managerLastName         The manager last name
         * @param      managerCountryCode      The manager country code
         */
        public ProductManger (String recordId, String veevaFamilyName, String veevaFamilyDescription, String veevaFamilyCountryCode, Boolean veevaFamilyActive, String managerId, String managerFirstName, String managerLastName, String managerCountryCode){
            this.recordId = recordId;
            this.veevaFamilyName = veevaFamilyName;
            this.veevaFamilyDescription = veevaFamilyDescription;
            this.veevaFamilyCountryCode = veevaFamilyCountryCode;
            this.veevaFamilyActive = veevaFamilyActive;
            
            this.managerId = managerId;

            if (this.managerId != null && this.managerId != ''){
                this.manager = new User(Id = this.managerId, FirstName = managerFirstName, LastName = managerLastName, Country_Code_BI__c = managerCountryCode);
            }else{
                this.manager = new User();
            }
        }
    }
}