/**
 * Sampro Admin Controller.
 */
public without sharing  class BI_SP_SamproAdminCtrl {

    public String userPermissionsJSON {get;set;}
    public String pagePermissionsJSON {get;set;}

    /**
     * Constructs the object.
     */
	public BI_SP_SamproAdminCtrl() {
        userPermissionsJSON = JSON.serialize(BI_SP_SamproServiceUtility.getCurrentUserPermissions());
		pagePermissionsJSON = getUserPagePermissions();
	}

    /**
     * Gets the user page permissions.
     *
     * @return     The user page permissions.
     */
	@RemoteAction
    public static String getUserPagePermissions() {
		String currentUserRegion = BI_SP_SamproServiceUtility.getCurrentUserRegion();
		String currentUserCountry = BI_SP_SamproServiceUtility.getCurrentUserCountryCode();
		return getPagePermissions(currentUserRegion, currentUserCountry);
    }

    /**
     * Gets the page permissions.
     *
     * @param      region   The region code
     * @param      country  The country code
     *
     * @return     The page permissions.
     */
	@RemoteAction
    public static String getPagePermissions(String region, String country) {
    	Map<String, Boolean> pagePermissionMap = new Map<String, Boolean>();
        if(region != null){
		  pagePermissionMap.put('canRegularizeArticles', region.equalsIgnoreCase('MX'));
        }else{
          pagePermissionMap.put('canRegularizeArticles', false);
        }
		pagePermissionMap.put('canManagePlacebo', canManagePlacebo(country));        
		return JSON.serialize(pagePermissionMap);
    }

    /**
     * Determines ability to manage placebo.
     *
     * @param      country  The country code
     *
     * @return     True if able to manage placebo, False otherwise.
     */
    @TestVisible
	private static Boolean canManagePlacebo(String country){
		List<BI_SP_Country_settings__c> csList = new List<BI_SP_Country_settings__c>([Select BI_SP_Allow_pl__c
                        from BI_SP_Country_settings__c
                        where BI_SP_Country_code__c = :country]);
		for(BI_SP_Country_settings__c cs : csList){
            if(cs.BI_SP_Allow_pl__c){
                return true;
            }
        }
        return false;
	}

    /**
     * Gets the region country codes.
     *
     * @param      region  The region code
     *
     * @return     The region country codes.
     */
    @RemoteAction
    public static String getRegionCountryCodes(String region) {
    	List<String> countryCodes = new List<String>();
    	countryCodes = new List<String>(BI_SP_SamproServiceUtility.getCountryCodesForRegion(region));
        return JSON.serialize(countryCodes);
    }

    /**
     * Gets all article types.
     *
     * @return     All article types.
     */
    public static String getAllArticleTypes() {
        List<Schema.PickListEntry> articleTypesList = BI_SP_Article__c.BI_SP_Type__c.getDescribe().getPicklistValues();
        return JSON.serialize(articleTypesList);
    }

    /**
     * Gets the articles to regularize.
     *
     * @param      region   The region code
     * @param      country  The country code
     *
     * @return     The articles to regularize.
     */
    @RemoteAction
    public static List<BI_SP_Article__c> getArticlesToRegularize(String region, String country) {
        List<BI_SP_Article__c> pmArticles = new List<BI_SP_Article__c>([SELECT ID, Name, BI_SP_Veeva_product_family__r.Name, BI_SP_Type__c, BI_SP_External_id_1__c, BI_SP_Stock__c, BI_SP_Available_stock__c, BI_SP_Description__c, BI_SP_Is_active__c, BI_SP_Csv_data_uploaded__c, BI_SP_Raw_country_code__c FROM BI_SP_Article__c WHERE BI_SP_Csv_data_uploaded__c = true AND (BI_SP_Type__c = '1' OR BI_SP_Type__c = '3') AND (BI_SP_Raw_country_code__c = 'MX10' OR BI_SP_Raw_country_code__c = 'ZD10')]);
        
        return pmArticles;
    } 

    /**
     * regularize articles
     *
     * @return     batch ids
     */
    @RemoteAction
    public static List<String> fireRegularizationBatch() {
        List<String> batchIds = new List<String>();
        List<BI_SP_Article__c> pmArticles = new List<BI_SP_Article__c>([SELECT ID, BI_SP_Csv_data_uploaded__c FROM BI_SP_Article__c WHERE BI_SP_Csv_data_uploaded__c = true AND BI_SP_Type__c = '1' AND (BI_SP_Raw_country_code__c = 'MX10' OR BI_SP_Raw_country_code__c = 'ZD10') LIMIT 1]);
        if(!pmArticles.isEmpty()) {
            String bPmId = Database.executeBatch(new BI_SP_BatchArticleDeactivation('1'));
            batchIds.add(bPmId);
        }

        List<BI_SP_Article__c> msArticles = new List<BI_SP_Article__c>([SELECT ID, BI_SP_Csv_data_uploaded__c 
        																FROM BI_SP_Article__c 
        																WHERE BI_SP_Csv_data_uploaded__c = true 
        																AND BI_SP_Type__c = '3'
        																 AND (BI_SP_Raw_country_code__c = 'MX10' OR BI_SP_Raw_country_code__c = 'ZD10') 
        																 LIMIT 1]);
        if(!msArticles.isEmpty()) {
            String bMsId = Database.executeBatch(new BI_SP_BatchArticleDeactivation('3'));
            batchIds.add(bMsId);
        }

        return batchIds;
    }  

    /**
     * Gets all articles for the country
     *
     * @param      region   The region code
     * @param      country  The country code
     *
     * @return     All articles.
     */
    @RemoteAction
    public static List<BI_SP_Article__c> getAllArticlesPlaceboTab(String region, String country) {
    	if(region == null){
			region = BI_SP_SamproServiceUtility.getCurrentUserRegion();
    	}
    	Set<String> countryCodes = BI_SP_SamproServiceUtility.getCountryCodesForRegion(region);
    	List<BI_SP_Article__c> pmArticles = new List<BI_SP_Article__c>([SELECT ID, Name, BI_SP_Veeva_product_family__r.Name, BI_SP_Type__c, 
    		BI_SP_External_id_1__c, BI_SP_Stock__c, BI_SP_Available_stock__c, BI_SP_Description__c, BI_SP_Is_active__c, BI_SP_Csv_data_uploaded__c, 
    		BI_SP_Raw_country_code__c, BI_SP_Country_code__c FROM BI_SP_Article__c WHERE (BI_SP_Type__c = '1' OR BI_SP_Type__c = '4') AND BI_SP_Country_code__c IN :countryCodes]);

        return pmArticles;
    }  

    /**
     * Change the article type to placebo for the selected articles
     *
     * @param      selectedArticles  The selected articles
     *
     * @return     The selected articles
     */
    @RemoteAction
    public static List<BI_SP_Article__c> changeToPlacebo(List<BI_SP_Article__c> selectedArticles) {
    	Set<String> countryCodes = new Set<String>();
    	for(BI_SP_Article__c article : selectedArticles){
    		article.BI_SP_Type__c = '4';
            article.BI_SP_Manually_updated__c = true;
    		countryCodes.add(article.BI_SP_Country_code__c);
    	}
    	update selectedArticles;

    	List<BI_SP_Article__c> pmArticles = new List<BI_SP_Article__c>([SELECT ID, Name, BI_SP_Veeva_product_family__r.Name, BI_SP_Type__c, BI_SP_External_id_1__c, 
    		BI_SP_Stock__c, BI_SP_Available_stock__c, BI_SP_Description__c, BI_SP_Is_active__c, BI_SP_Csv_data_uploaded__c, BI_SP_Raw_country_code__c, 
    		BI_SP_Country_code__c FROM BI_SP_Article__c WHERE (BI_SP_Type__c = '1' OR BI_SP_Type__c = '4') AND BI_SP_Country_code__c IN :countryCodes]);

        return pmArticles;
    }  

    /**
     * Change the article type to mediacal sample for the selected articles
     *
     * @param      selectedArticles  The selected articles
     *
     * @return     The selected articles
     */
    @RemoteAction
    public static List<BI_SP_Article__c> changeToMedicalSample(List<BI_SP_Article__c> selectedArticles) {
    	Set<String> countryCodes = new Set<String>();
    	for(BI_SP_Article__c article : selectedArticles){
    		article.BI_SP_Type__c = '1';
            article.BI_SP_Manually_updated__c = true;
    		countryCodes.add(article.BI_SP_Country_code__c);
    	}
    	update selectedArticles;
    	    	
    	List<BI_SP_Article__c> pmArticles = new List<BI_SP_Article__c>([SELECT ID, Name, BI_SP_Veeva_product_family__r.Name, BI_SP_Type__c, BI_SP_External_id_1__c, 
    		BI_SP_Stock__c, BI_SP_Available_stock__c, BI_SP_Description__c, BI_SP_Is_active__c, BI_SP_Csv_data_uploaded__c, BI_SP_Raw_country_code__c, 
    		BI_SP_Country_code__c FROM BI_SP_Article__c WHERE (BI_SP_Type__c = '1' OR BI_SP_Type__c = '4') AND BI_SP_Country_code__c IN :countryCodes]);

        return pmArticles;
    } 

    /**
     * Gets the filters.
     *
     * @return     The filters.
     */
    @RemoteAction
    public static FiltersModel getFilters() {
        FiltersModel fm = new FiltersModel();
        Map<String, Boolean> userPermissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
        fm.region = getUserRegionCodes(userPermissions.get('isRB'));
        fm.allArticleTypes = getAllArticleTypes();
        return fm;
    }

    /**
     * Gets the country codes for the region of the current user.
     *
     * @param      isRb                   Indicates if is ReportBuilder
     * @param      selectedReportBuilder  Indicates if is access like report builder
     * @param      region                 The region
     *
     * @return     The country codes.
     */
    @RemoteAction
    public static String getUserCountryCodes(Boolean isRb, Boolean selectedReportBuilder, String region) {
    	List<String> countryCodes = new List<String>();
    	if(!isRB || !selectedReportBuilder){
        	countryCodes = new List<String>(BI_SP_SamproServiceUtility.getSetCurrentUserRegionCountryCodes());
    	}else{
    		countryCodes = new List<String>(BI_SP_SamproServiceUtility.getCountryCodesForRegion(region));
    	}
        return JSON.serialize(countryCodes);
    }

    /**
     * Gets the region codes for the current user.
     *
     * @param      isRb  Indicates if is ReportBuilder
     *
     * @return     The user region codes.
     */
    public static String getUserRegionCodes(Boolean isRb) {
    	List<String> regionCodes = new List<String>();
    	if(isRB){
        	regionCodes = new List<String>(BI_SP_SamproServiceUtility.getSetReportBuilderRegionCodes());
        }
        return JSON.serialize(regionCodes);
    }

    /**
     * Class for filters model.
     */
    public class FiltersModel{
        public String country; // JSON SERIALIZADO		
        public String region; // JSON SERIALIZADO 
        public String allArticleTypes; //JSON Serializado
    }
}