@isTest
private class BI_SP_SamproAdminCtrl_Test{

    public static String countryCode;
    public static String regionCode;

    @Testsetup
    static void setUp() {
        countryCode = 'BR';
        regionCode = 'BR';
        BI_SP_TestDataUtility.createCustomSettings();
        List<Account> accounts = BI_SP_TestDataUtility.createAccounts();
        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);

        //PLANIT TEST DATA
        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
        List<BI_SP_Article__c> articlesReg = BI_SP_TestDataUtility.createArticlesRegularize(products, countryCode, countryCode);
        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
    }

    @isTest static void BI_SP_SamproAdminCtrl_initialize_Test() {
        String regionCode = 'BR';
        BI_SP_SamproAdminCtrl ppClass = new BI_SP_SamproAdminCtrl();

        BI_SP_SamproAdminCtrl.FiltersModel fil = BI_SP_SamproAdminCtrl.getFilters();

        String userCountryCodeJSON = BI_SP_SamproAdminCtrl.getUserCountryCodes(true, true, regionCode);
        String userCountryCodeJSON2 = BI_SP_SamproAdminCtrl.getUserCountryCodes(false, false, regionCode);
        String userRegionCodeJSON = BI_SP_SamproAdminCtrl.getUserRegionCodes(true);

        String pagePermissionJSON = BI_SP_SamproAdminCtrl.getUserPagePermissions();
        pagePermissionJSON = BI_SP_SamproAdminCtrl.getPagePermissions(regionCode, countryCode);
        pagePermissionJSON = BI_SP_SamproAdminCtrl.getPagePermissions(null, countryCode);
        Boolean canPlacebo = BI_SP_SamproAdminCtrl.canManagePlacebo(countryCode); 
        String regionCountryJSON = BI_SP_SamproAdminCtrl.getRegionCountryCodes(regionCode);
        String articleTypesJSON = BI_SP_SamproAdminCtrl.getAllArticleTypes();

        List<Id> batchIds = BI_SP_SamproAdminCtrl.fireRegularizationBatch();

        List<BI_SP_Article__c> articles = BI_SP_SamproAdminCtrl.getAllArticlesPlaceboTab(countryCode, regionCode);
        articles = BI_SP_SamproAdminCtrl.getArticlesToRegularize(countryCode, regionCode);
        List<BI_SP_Article__c> articlesPL = BI_SP_SamproAdminCtrl.changeToPlacebo(articles);
        List<BI_SP_Article__c> articlesMS = BI_SP_SamproAdminCtrl.changeToMedicalSample(articles);

    }
}