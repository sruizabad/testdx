@IsTest
public with sharing class BI_SP_SamproServiceUtility_Test {

	@Testsetup
	static void setUp(){
		String countryCode = 'BR';
		User salesRep = BI_SP_TestDataUtility.getSalesRepUser(countryCode, 0);
		User pm = BI_SP_TestDataUtility.getProductManagerUser(countryCode, 0);
		User cs = BI_SP_TestDataUtility.getClientServiceUser(countryCode, 0);
		User admin = BI_SP_TestDataUtility.getAdminUser(countryCode, 0);
		User reportb = BI_SP_TestDataUtility.getReportBuilderUser(countryCode, 0);
		List<User> users = new List<User>();
		users.add(salesRep);
		users.add(pm);
		users.add(cs);
		users.add(admin);
		users.add(reportb);
        
        System.runAs(admin){
			BI_SP_TestDataUtility.createCustomSettings();
	        List<Customer_Attribute_BI__c> specs = BI_SP_TestDataUtility.createSpecialties();
	        List<Account> accounts = BI_SP_TestDataUtility.createAccounts(specs);
	        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
	        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());


	        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
	        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

	        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
	        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
			List<BI_SP_Special_shipment__c> specialShip = BI_SP_TestDataUtility.createSpecialShipments(countryCode, users, articles);
		}
	}

	static testMethod void testUserPermissions() {
		String countryCode = 'BR';
		Map<String, Boolean> permissions = BI_SP_SamproServiceUtility.getCurrentUserPermissions();
		Boolean a = BI_SP_SamproServiceUtility.isSAPCountry(countryCode);
		Boolean b = BI_SP_SamproServiceUtility.isSAPCountry(null);

		Set<String> countryCodes = new Set<String>();
		countryCodes.add(countryCode);
		Map<Id,User> map1 = BI_SP_SamproServiceUtility.getSalesRepByCountry(countryCodes);
		Map<Id,User> map2 = BI_SP_SamproServiceUtility.getProductManagersOfRegion(countryCodes);
		Map<Id,User> map3 = BI_SP_SamproServiceUtility.getProductManagersOfCountry(countryCode);

		Boolean c = BI_SP_SamproServiceUtility.hasPermissionToSee();
	}

	static testMethod void testProducts() {
		String countryCode = 'BR';
		String regionCode = 'BR';
		List<Product_vod__c> pList1 = BI_SP_SamproServiceUtility.getListProductsAllowedToDistribute(UserInfo.getUserId());
		List<Product_vod__c> pList2 = BI_SP_SamproServiceUtility.getListProductsAllowedToDistributeInCountry(countryCode, UserInfo.getUserId());

		BI_SP_Preparation_period__c per = [SELECT Id from BI_SP_Preparation_period__c Limit 1];
		List<Product_vod__c> pList3 = BI_SP_SamproServiceUtility.getListProductsInPeriod(per);
		List<Product_vod__c> pList4 = BI_SP_SamproServiceUtility.getListProductsInCountry(countryCode);
		List<Product_vod__c> pList5 = BI_SP_SamproServiceUtility.getListProductsInRegion(regionCode);
		List<Product_vod__c> pList6 = BI_SP_SamproServiceUtility.getListAllProducts();
	}

	static testMethod void testGeneratedExternalId() {
		String countryCode = 'BR';

		Date st1 = Date.newInstance(2017, 01, 01);
		Date et1 = Date.newInstance(2017, 01, 31);
		String extid1 = BI_SP_SamproServiceUtility.generatePeriodExternalId(countryCode,st1, et1, 'Name');

		String extid2 = BI_SP_SamproServiceUtility.generatePreparationExternalId(extid1,'PositionName');
		String extid3 = BI_SP_SamproServiceUtility.generateArticlePreparationExternalId(extid2,extid1);
		String extid4 = BI_SP_SamproServiceUtility.generateOrderArticlePreparationExternalId('AP',extid1, 'PositionName', '1');
		String extid5 = BI_SP_SamproServiceUtility.generateOrderItemArticlePreparationExternalId(extid3);
		String extid6 = BI_SP_SamproServiceUtility.generateOrderSpecialShipmentExternalId('SS','ssId', 'userId', '1');
		String extid7 = BI_SP_SamproServiceUtility.generateOrderItemSpecialShipmentExternalId('ssId', 'userId', extid3);
	}

	static testMethod void testSAMProModel() {
		String countryCode = 'BR';

		Date st1 = Date.newInstance(2017, 01, 01);
		Date et1 = Date.newInstance(2017, 01, 31);
		BI_SP_SamproServiceUtility.SAMProModel sM = BI_SP_SamproServiceUtility.getSAMProModel(countryCode, st1, et1);
	}

	static testMethod void testUpdateStock() {
		List<Product_vod__c> products = [SELECT Id FROM Product_vod__c];
		
		List<BI_SP_Article__c> articles = [SELECT Id FROM BI_SP_Article__c];
		BI_SP_SamproServiceUtility.updateArticleStockMultiple(articles);
		BI_SP_SamproServiceUtility.updateFamilyStockMultiple(articles, products);
	}

	static testMethod void testRegionAndCountries() {
		String countryCode = 'BR';
		String regionCode = 'BR';

		String str1 = BI_SP_SamproServiceUtility.getStringCurrentUserRegionCountryCodes();
		String str2 = BI_SP_SamproServiceUtility.getStringRegionRawCountryCodes(countryCode);
		String str3 = BI_SP_SamproServiceUtility.getCurrentUserCountryCode();
		String str4 = BI_SP_SamproServiceUtility.getCurrentUserRegion();
		Set<String> set1 = BI_SP_SamproServiceUtility.getSetCurrentUserRegionRawCountryCodes();
		Set<String> set2 = BI_SP_SamproServiceUtility.getSetCurrentUserRegionCountryCodes();
		Set<String> set3 = BI_SP_SamproServiceUtility.getSetReportBuilderCountryCodes();
		Set<String> set4 = BI_SP_SamproServiceUtility.getSetReportBuilderRegionCodes();
		Set<String> set5 = BI_SP_SamproServiceUtility.getRegionCountryCodes(countryCode);
		Set<String> set6 = BI_SP_SamproServiceUtility.getAllRegionCountryCodes();
		Set<String> set7 = BI_SP_SamproServiceUtility.getAllRegionCodes();
		Set<String> set8 = BI_SP_SamproServiceUtility.getCountryCodesForRegion(regionCode);
		Set<String> set9 = BI_SP_SamproServiceUtility.getRegionRawCountryCodes(countryCode);
		Set<String> set10 = BI_SP_SamproServiceUtility.getListCountriesForUserManagesProducts(UserInfo.getUserId());
		Map<String, Id> map1 = BI_SP_SamproServiceUtility.getCountryCodeDefaultOwnerId(new List<String>{'BR'});
	}

	static testMethod void testArticleTypes() {
		String countryCode = 'BR';

		List<Schema.PickListEntry> pl1 = BI_SP_SamproServiceUtility.getListCurrentUserArticleTypesByCountry(BI_SP_Article__c.BI_SP_Type__c);
		List<Schema.PickListEntry> pl2 = BI_SP_SamproServiceUtility.getListArticleTypesByCountry(BI_SP_Article__c.BI_SP_Type__c, countryCode);
		List<Schema.PickListEntry> pl3 = BI_SP_SamproServiceUtility.getListReportBuilderArticleTypes(BI_SP_Article__c.BI_SP_Type__c);
		List<Schema.PickListEntry> pl4 = BI_SP_SamproServiceUtility.getListCurrentUserArticleTypesByRegion(BI_SP_Article__c.BI_SP_Type__c);
		List<Schema.PickListEntry> pl5 = BI_SP_SamproServiceUtility.getListCountriesArticleTypes(BI_SP_Article__c.BI_SP_Type__c, new List<String>{countryCode});
	}

	static testMethod void testOrderTypes() {
		String countryCode = 'BR';

		List<Schema.PickListEntry> pl1 = BI_SP_SamproServiceUtility.getListCurrentUserOrderTypes(BI_SP_Order__c.BI_SP_Order_type_pl__c);
	}

	static testMethod void testString() {
		String countryCode = 'BR';

		String str1 = BI_SP_SamproServiceUtility.stringOutOfSet(BI_SP_SamproServiceUtility.getSetCurrentUserRegionRawCountryCodes());
		Set<Id> ids = new Set<Id>();
		for(BI_SP_Article__c article : [SELECT Id FROM BI_SP_Article__c LIMIT 3]){
			ids.add(article.Id);
		}
		String str2 = BI_SP_SamproServiceUtility.stringOutOfSet(ids);
	}

	static testMethod void testPeriods() {
		String countryCode = 'BR';

		List<BI_SP_Preparation_period__c> prepList1 = BI_SP_SamproServiceUtility.getListPeriodsForApproaches(countryCode, UserInfo.getUserId());

		BI_SP_Preparation__c p = [SELECT Id FROM BI_SP_Preparation__c LIMIT 1];
		List<BI_SP_Preparation__c> prepList2 = BI_SP_SamproServiceUtility.getListPeriodsForAdjustments(p.Id);
		List<BI_SP_Article_Preparation__c> prepList3 = BI_SP_SamproServiceUtility.getListArticlePreparationForAdjustments(p.Id);

		List<BI_SP_Preparation_period__c> prepList4 = BI_SP_SamproServiceUtility.getListPeriodsForApprovals(countryCode, UserInfo.getUserId());

		List<BI_SP_Preparation_period__c> prepList5 = BI_SP_SamproServiceUtility.getListPeriodsForShipping(countryCode, UserInfo.getUserId());

		List<BI_SP_Preparation_period__c> prepList6 = BI_SP_SamproServiceUtility.getListAllPeriodsForShipping(countryCode, UserInfo.getUserId());

		List<BI_SP_Preparation_period__c> prepList7 = BI_SP_SamproServiceUtility.getListPeriodsByCountry(countryCode, UserInfo.getUserId());

		List<BI_SP_Preparation_period__c> prepList8 = BI_SP_SamproServiceUtility.getListPeriodsByCountry(null, UserInfo.getUserId());
	}

	static testMethod void testSpecialShipmnet() {
		String countryCode = 'BR';

		BI_SP_Special_Shipment__c ss = [SELECT Id FROM BI_SP_Special_Shipment__c LIMIT 1];
		Boolean a = BI_SP_SamproServiceUtility.isManagerOfAllSpecialShipmentFamilies(UserInfo.getUserId(), ss.Id);
	}

	static testMethod void testPreparations() {
		String countryCode = 'BR';

		BI_SP_Article_preparation__c ap = [SELECT Id, BI_SP_Status__c, BI_SP_Approval_status__c, BI_SP_Amount_e_shop__c, BI_SP_Adjusted_amount_1__c, BI_SP_Amount_target__c,
											 BI_SP_Adjusted_amount_2__c
											FROM BI_SP_Article_preparation__c LIMIT 1];
		Decimal dec1 = BI_SP_SamproServiceUtility.getArticlePreparationAdjustmentsVariableAmountValue(ap);
		Decimal dec2 = BI_SP_SamproServiceUtility.getArticlePreparationAdjustmentsVariableAmountValue(null);
		Decimal dec3 = BI_SP_SamproServiceUtility.getArticlePreparationApprovalVariableAmountValue(ap);
		Decimal dec4 = BI_SP_SamproServiceUtility.getArticlePreparationApprovalVariableAmountValue(null);
		Decimal dec5 = BI_SP_SamproServiceUtility.getArticlePreparationFinalVariableAmountValue(ap);
		Decimal dec6 = BI_SP_SamproServiceUtility.getArticlePreparationFinalVariableAmountValue(null);
	}

	static testMethod void testTerritoriesPositions() {
		String countryCode = 'BR';

		Map<String, Id> map1 = BI_SP_SamproServiceUtility.getpositionPreparationOwner(new List<String>{'test1', 'test2'});
		Map<String,Map<String,List<SObject>>> map2 = BI_SP_SamproServiceUtility.getSalesRepByPositionName(new List<String>{'test1', 'test2'});

		Set<String> countryCodes = new Set<String>();
		countryCodes.add(countryCode);
		BI_SP_SamproServiceUtility.SAMProCyclesAndPositions sam = BI_SP_SamproServiceUtility.approvedCyclesAndPositions(countryCodes);
	}
}