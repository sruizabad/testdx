@isTest
private class BI_SP_Special_shipment_Trigger_TEST{
    
	static String countryCode = 'BR';

	@Testsetup
    static void setUp() {
    	User salesRep = BI_SP_TestDataUtility.getSalesRepUser(countryCode, 0);
    	User salesRep2 = BI_SP_TestDataUtility.getSalesRepUser('MX', 1);
    	User salesRep3 = BI_SP_TestDataUtility.getSalesRepUser('AR', 2);
		User pm = BI_SP_TestDataUtility.getProductManagerUser(countryCode, 0);
		User cs = BI_SP_TestDataUtility.getClientServiceUser(countryCode, 0);
		User admin = BI_SP_TestDataUtility.getAdminUser(countryCode, 0);
		User reportb = BI_SP_TestDataUtility.getReportBuilderUser(countryCode, 0);
		List<User> users = new List<User>();
		List<User> users2 = new List<User>();
		List<User> users3 = new List<User>();
		users.add(salesRep);
		users2.add(salesRep2);
		users3.add(salesRep3);
		users.add(pm);
		users.add(cs);
		users.add(admin);
		users.add(reportb);
        
        System.runAs(admin){
	        BI_SP_TestDataUtility.createCustomSettings();
	        List<Customer_Attribute_BI__c> specs = BI_SP_TestDataUtility.createSpecialties();
	        List<Account> accounts = BI_SP_TestDataUtility.createAccounts(specs);
	        List<Product_vod__c> products = BI_SP_TestDataUtility.createProducts(countryCode);
	        List<BI_SP_Product_family_assignment__c> productsAssigments = BI_SP_TestDataUtility.createProductsAssigments(products, Userinfo.getUserId());
	        List<BI_SP_Product_family_assignment__c> productsAssigmentsPM = BI_SP_TestDataUtility.createProductsAssigments(products, pm.Id);


	        List<BI_TM_FF_type__c> ffs = BI_PL_TestDataUtility.createFieldForces();
	        List<BI_PL_Position_Cycle__c> posCycles = BI_PL_TestDataUtility.createHierarchy(countryCode, Date.today(), Date.today() + 30, ffs.get(0), 'testHierarchy', false, 1);

	        List<BI_SP_Article__c> articles = BI_SP_TestDataUtility.createArticles(products, countryCode, countryCode);
	        List<BI_SP_Article_Preparation__c> artPreps = BI_SP_TestDataUtility.createCyclesAndArticlePreparations(countryCode, posCycles, accounts, products, articles);
	        List<BI_SP_Staging_user_info__c> staggingUserInfo = BI_SP_TestDataUtility.createUserStagging(users, countryCode);
	        List<BI_SP_Staging_user_info__c> staggingUserInfo2 = BI_SP_TestDataUtility.createUserStagging(users2, 'MX');
	        List<BI_SP_Staging_user_info__c> staggingUserInfo3 = BI_SP_TestDataUtility.createUserStagging(users3, 'AR');
			List<BI_SP_Product_family_assignment__c> productFamilyAssignments = BI_SP_TestDataUtility.createProductFamilyAssignments(users, products);
			List<BI_SP_Special_shipment__c> specialShip = BI_SP_TestDataUtility.createSpecialShipments(countryCode, users, articles);
        }
    }
	
	@isTest static void BI_SP_Special_shipment_Trigger_NoId() {
		Test.startTest();
			BI_SP_Special_shipment__c ss = [Select Id,Name from BI_SP_Special_shipment__c LIMIT 1];

			PageReference pageRef = Page.BI_SP_SpecialShipment;
			Test.setCurrentPage(pageRef);

			User pmUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM%' LIMIT 1];
			System.runAs(pmUser) {
				ApexPages.StandardController sc = new ApexPages.StandardController(ss);
				BI_SP_SpecialShipmentExt ssClass = new BI_SP_SpecialShipmentExt(sc);
			}

			User reportbUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_REPORT_BUILDER') AND Username Like 'userRB%' LIMIT 1];
			System.runAs(reportbUser) {
				ApexPages.StandardController sc = new ApexPages.StandardController(ss);
				BI_SP_SpecialShipmentExt ssClass = new BI_SP_SpecialShipmentExt(sc);
			}

			ApexPages.StandardController sc = new ApexPages.StandardController(ss);
			BI_SP_SpecialShipmentExt ssClass = new BI_SP_SpecialShipmentExt(sc);
		Test.stopTest();
	}
	
	@isTest static void BI_SP_Special_shipment_Trigger_Id_retURL() {
		Test.startTest();
			BI_SP_Special_shipment__c ss = [Select Id,Name from BI_SP_Special_shipment__c LIMIT 1];

			PageReference pageRef = Page.BI_SP_SpecialShipment;
			pageRef.getParameters().put('id', String.valueOf(ss.Id));
			pageRef.getParameters().put('retURL', String.valueOf(ss.Id));
			Test.setCurrentPage(pageRef);

			User pmUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM%' LIMIT 1];
			System.runAs(pmUser) {
				ApexPages.StandardController sc = new ApexPages.StandardController(ss);
				BI_SP_SpecialShipmentExt ssClass = new BI_SP_SpecialShipmentExt(sc);
			}

			User reportbUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_REPORT_BUILDER') AND Username Like 'userRB%' LIMIT 1];
			System.runAs(reportbUser) {
				ApexPages.StandardController sc = new ApexPages.StandardController(ss);
				BI_SP_SpecialShipmentExt ssClass = new BI_SP_SpecialShipmentExt(sc);
			}
			
			ApexPages.StandardController sc = new ApexPages.StandardController(ss);
			BI_SP_SpecialShipmentExt ssClass = new BI_SP_SpecialShipmentExt(sc);

			List<Product_vod__c> products = new List<Product_vod__c>([SELECT id FROM Product_vod__c LIMIT 2]);
			List<BI_SP_Article__c> articles = BI_SP_SpecialShipmentExt.getArticles(countryCode, products);

			List<BI_SP_Special_Shipment_Article__c> ssArt = BI_SP_SpecialShipmentExt.getSSArticles(ss.Id);

			List<BI_SP_Special_Shipment_Article__c> ssArt2 = new List<BI_SP_Special_Shipment_Article__c>{
				new BI_SP_Special_Shipment_Article__c(BI_SP_Amount__c=1, BI_SP_Article__c=articles.get(0).Id)
			};
			ss.BI_SP_Type__c = '1';
			Map<String, Object> ssSave = BI_SP_SpecialShipmentExt.saveSpecialShipment(ss, ssArt2, null, new List<User>([SELECT id from User Limit 3]));

			BI_SP_SpecialShipmentExt.getUsersLookupResult(countryCode);

			BI_SP_SpecialShipmentExt.getUsersLookupResult('MX');
			BI_SP_SpecialShipmentExt.getUsersLookupResult('AR');

			try{
				List<BI_SP_Special_shipment_user__c> ssusers = new List<BI_SP_Special_shipment_user__c>([SELECT id, BI_SP_Special_Shipment__c, BI_SP_User__c FROM BI_SP_Special_shipment_user__c LIMIT 3]);
				List<BI_SP_Special_shipment_user__c> ssUsr = BI_SP_SpecialShipmentExt.upsertSSUsers(ssusers, new List<User>([SELECT Id FROM User LIMIT 3]), null, ss);
			}catch(Exception e){}
 
			Map<String, Object> ssSave2 = BI_SP_SpecialShipmentExt.rejectSpecialShipment(ss);

			Map<String, Object> deleteMap = BI_SP_SpecialShipmentExt.deleteSpecialShipment(ss);
		Test.stopTest();
	}
	
	@isTest static void BI_SP_Special_shipment_Trigger_Id_NoRetURL() {
		Test.startTest();
			BI_SP_Special_shipment__c ss = [Select Id,Name from BI_SP_Special_shipment__c LIMIT 1];
			ss.BI_SP_Approved__c='2';
			update ss;

			PageReference pageRef = Page.BI_SP_SpecialShipment;
			pageRef.getParameters().put('id', String.valueOf(ss.Id));
			Test.setCurrentPage(pageRef);

			User pmUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_Product_manager') AND Username Like 'userPM%' LIMIT 1];
			System.runAs(pmUser) {
				ApexPages.StandardController sc = new ApexPages.StandardController(ss);
				BI_SP_SpecialShipmentExt ssClass = new BI_SP_SpecialShipmentExt(sc);
				String userTypes = BI_SP_SpecialShipmentExt.getuserTypes(countryCode);
			}

			User reportbUser = [SELECT Id, Name, Street, PostalCode, City, State, Country FROM User WHERE Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'BI_SP_REPORT_BUILDER') AND Username Like 'userRB%' LIMIT 1];
			System.runAs(reportbUser) {
				ApexPages.StandardController sc = new ApexPages.StandardController(ss);
				BI_SP_SpecialShipmentExt ssClass = new BI_SP_SpecialShipmentExt(sc);
			}

			Map<String, Object> fil = BI_SP_SpecialShipmentExt.getFilters();
			String shipTypeSS = BI_SP_SpecialShipmentExt.getSHIPMENTTYPESS();
			String shipTypeMS = BI_SP_SpecialShipmentExt.getSHIPMENTTYPEMARKTING();
			String shipTypeSR = BI_SP_SpecialShipmentExt.getUSERTYPESALESREP();
			String shipTypeEX = BI_SP_SpecialShipmentExt.getUSERTYPEEXTERNAL();
			String userTypes = BI_SP_SpecialShipmentExt.getuserTypes(countryCode);
			
			Integer num = BI_SP_SpecialShipmentExt.getNewNumberOfSalesRep(new List<User>());
			num = BI_SP_SpecialShipmentExt.getNewNumberOfSalesRep(new List<User>([SELECT Id, Name, Street, PostalCode, City, State, Country FROM User LIMIT 2]));
			ss.BI_SP_Type__c = '1';
			Integer oldUserNumber = BI_SP_SpecialShipmentExt.getOldNumberOfSalesRep(ss, new List<BI_SP_Special_shipment_user__c>());
			ss.BI_SP_Type__c = '2';
			Integer oldUserNumber2 = BI_SP_SpecialShipmentExt.getOldNumberOfSalesRep(ss, new List<BI_SP_Special_shipment_user__c>());

			Map<Id, BI_SP_Special_Shipment_Article__c> oldSsaMap = new Map<Id, BI_SP_Special_Shipment_Article__c>([SELECT id, BI_SP_Special_Shipment__c, BI_SP_Amount__c, BI_SP_Article__c FROM BI_SP_Special_Shipment_Article__c LIMIT 3]);

			List<Product_vod__c> products = new List<Product_vod__c>([SELECT id FROM Product_vod__c LIMIT 2]);
			List<BI_SP_Article__c> articles = BI_SP_SpecialShipmentExt.getArticles(countryCode, products);
			List<BI_SP_Special_Shipment_Article__c> ssarticles = new List<BI_SP_Special_Shipment_Article__c>([SELECT id, BI_SP_Special_Shipment__c, BI_SP_Amount__c, BI_SP_Article__c FROM BI_SP_Special_Shipment_Article__c LIMIT 3]);
			List<BI_SP_Special_shipment_user__c> ssusers = new List<BI_SP_Special_shipment_user__c>([SELECT id, BI_SP_Special_Shipment__c, BI_SP_User__c FROM BI_SP_Special_shipment_user__c LIMIT 3]);

			Map<String, Object> ssMap = BI_SP_SpecialShipmentExt.getSpecialShipment(ss.Id);
			List<Product_vod__c> products2 = BI_SP_SpecialShipmentExt.getAllFamiliesManaged();
			Map<String, Object> ssMap2 = BI_SP_SpecialShipmentExt.updateFamilyStockMultiple(products2);
			List<BI_SP_Special_Shipment_Article__c> ssArticles2 = BI_SP_SpecialShipmentExt.getSSArticles(ss.Id);

			try{
				List<BI_SP_Special_Shipment_Article__c> ssArt = BI_SP_SpecialShipmentExt.upsertSSArticles(oldSsaMap, ssarticles, ss);
			}catch(Exception e){}
			try{
				List<BI_SP_Article__c> art = BI_SP_SpecialShipmentExt.updateArticlesStock(ssarticles, oldSsaMap, 1, 2);
			}catch(Exception e){}
			try{
				Boolean a = BI_SP_SpecialShipmentExt.thereIsEnoughStockForArticle(3, 2, 10, 1, 2);
			}catch(Exception e){}	
			try{
				Map<String, Object> saveSS = BI_SP_SpecialShipmentExt.saveOnlySpecialShipment(ss);
			}catch(Exception e){}	
		Test.stopTest();
	}
	
	@isTest static void BI_SP_SpecialShipmentTriggerHandler_Test() {
		Test.startTest();
			BI_SP_Special_shipment__c ss = [Select Id,Name from BI_SP_Special_shipment__c LIMIT 1];
			try{
				ss.BI_SP_Approved__c = '2';
				update ss;
			}catch(Exception e){}

			List<Id> ssList = new List<Id>();
			ssList.add(ss.Id);
			try{
				BI_SP_SpecialShipmentTriggerHandler.generateSSOrders(ssList);
				delete ss;
			}catch(Exception e){}

			BI_SP_Special_shipment__c ss2 = [Select Id,Name from BI_SP_Special_shipment__c LIMIT 1];
			try{
				ss2.BI_SP_Approved__c = '1';
				update ss2;
				delete ss2;
			}catch(Exception e){}
		Test.stopTest();
	}
}