/**
 * Handler for BI_SP_Staging_user_info__c Trigger.
 */
public with sharing class BI_SP_StagingUserInfoTriggerHandler implements BI_SP_TriggerInterface{
	
	Map<String, User> usersMap = new Map<String, User>();
	
	/**
	 * Constructs the object.
	 */
	public BI_SP_StagingUserInfoTriggerHandler() {}

	/**
	 * bulkBefore
	 *
	 * This method is called prior to execution of a BEFORE trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkBefore() {
		if (Trigger.isInsert || Trigger.isUpdate) {
			getUsers((List<BI_SP_Staging_user_info__c>)Trigger.new);
		}
	}
	/**
	 * bulkAfter
	 *
	 * This method is called prior to execution of an AFTER trigger. Use this to cache
	 * any data required into maps prior execution of the trigger.
	 */
	public void bulkAfter() {}

	/**
	 * beforeInsert
	 *
	 * This method is called iteratively for each record to be inserted during a BEFORE
	 * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     * 
     * @param      so    The SObject (BI_SP_Staging_user_info__c)
     * 
	 */
	public void beforeInsert(SObject so) {
		BI_SP_Staging_user_info__c stagingUser = (BI_SP_Staging_user_info__c)so;

		String shipToNoZero = getCorrectEmployeeNumber(stagingUser.BI_SP_Ship_to__c);
		if (usersMap.containsKey(shipToNoZero+stagingUser.BI_SP_Raw_country_code__c)) {
			stagingUser.BI_SP_User__c = (usersMap.get(shipToNoZero+stagingUser.BI_SP_Raw_country_code__c)==null) ? null : usersMap.get(shipToNoZero+stagingUser.BI_SP_Raw_country_code__c).Id;
		}else if (usersMap.containsKey(stagingUser.BI_SP_Ship_to__c+stagingUser.BI_SP_Raw_country_code__c)) {
			stagingUser.BI_SP_User__c = (usersMap.get(stagingUser.BI_SP_Ship_to__c+stagingUser.BI_SP_Raw_country_code__c)==null) ? null : usersMap.get(stagingUser.BI_SP_Ship_to__c+stagingUser.BI_SP_Raw_country_code__c).Id;
		}
		
	}

	/**
	 * beforeUpdate
	 *
	 * This method is called iteratively for each record to be updated during a BEFORE
	 * trigger.
     * 
     * @param      oldSo  The old SObject (BI_SP_Staging_user_info__c)
     * @param      so     The SObject (BI_SP_Staging_user_info__c)
     * 
	 */
	public void beforeUpdate(SObject oldSo, SObject so) {
		BI_SP_Staging_user_info__c stagingUser = (BI_SP_Staging_user_info__c)so;
		BI_SP_Staging_user_info__c oldStagingUser = (BI_SP_Staging_user_info__c)oldSo;

		String shipToNoZero = getCorrectEmployeeNumber(stagingUser.BI_SP_Ship_to__c);
		String oldShipToNoZero = getCorrectEmployeeNumber(oldStagingUser.BI_SP_Ship_to__c);
		String newKey = shipToNoZero+stagingUser.BI_SP_Raw_country_code__c;
		String oldKey = oldShipToNoZero+oldStagingUser.BI_SP_Raw_country_code__c;

		if (oldKey != newKey || stagingUser.BI_SP_User__c == null) {
			if (usersMap.containsKey(shipToNoZero+stagingUser.BI_SP_Raw_country_code__c)) {
				stagingUser.BI_SP_User__c = (usersMap.get(shipToNoZero+stagingUser.BI_SP_Raw_country_code__c)==null) ? null : usersMap.get(shipToNoZero+stagingUser.BI_SP_Raw_country_code__c).Id;
			}else if (usersMap.containsKey(stagingUser.BI_SP_Ship_to__c+stagingUser.BI_SP_Raw_country_code__c)) {
				stagingUser.BI_SP_User__c = (usersMap.get(stagingUser.BI_SP_Ship_to__c+stagingUser.BI_SP_Raw_country_code__c)==null) ? null : usersMap.get(stagingUser.BI_SP_Ship_to__c+stagingUser.BI_SP_Raw_country_code__c).Id;
			}else{
				stagingUser.BI_SP_User__c =null;
			}
		}
	}

	/**
	 * beforeDelete
	 *
	 * This method is called iteratively for each record to be deleted during a BEFORE
	 * trigger.
     * 
     * @param      so    The SObject (BI_SP_Staging_user_info__c)
     * 
	 */
	public void beforeDelete(SObject so) {}

	/**
	 * afterInsert
	 *
	 * This method is called iteratively for each record inserted during an AFTER
	 * trigger. Always put field validation in the 'After' methods in case another trigger
	 * has modified any values. The record is 'read only' by this point.
     * 
     * @param      so    The SObject (BI_SP_Staging_user_info__c)
     * 
	 */
	public void afterInsert(SObject so) {}


	/**
	 * afterUpdate
	 *
	 * This method is called iteratively for each record updated during an AFTER
	 * trigger.
     * 
     * @param      oldSo  The old SObject (BI_SP_Staging_user_info__c)
     * @param      so     The SObject (BI_SP_Staging_user_info__c)
     * 
	 */
	public void afterUpdate(SObject oldSo, SObject so) {
	}

	/**
	 * afterDelete
	 *
	 * This method is called iteratively for each record deleted during an AFTER
	 * trigger.
     * 
     * @param      so    The SObject (BI_SP_Staging_user_info__c)
     * 
	 */
	public void afterDelete(SObject so) {}

	/**
	 * andFinally
	 *
	 * This method is called once all records have been processed by the trigger. Use this
	 * method to accomplish any final operations such as creation or updates of other records.
	 */
	public void andFinally() {}

	/**
	 * Obtain all the users related with the Stagging User Info records 
	 *
	 * @param      stagingUserList  The staging user list
	 */
	private void getUsers(List<BI_SP_Staging_user_info__c> stagingUserList) {

		Set<String> userEmployeeNumberSet = new Set<String>();		
		Set<String> rawCountrySet = new Set<String>();
		for (BI_SP_Staging_user_info__c stagingUser : stagingUserList) {
			userEmployeeNumberSet.add(getCorrectEmployeeNumber(stagingUser.BI_SP_Ship_to__c));
			userEmployeeNumberSet.add(stagingUser.BI_SP_Ship_to__c);
			rawCountrySet.add(stagingUser.BI_SP_Raw_country_code__c);
		}

		Map<string, String> countryRawCountryMap = new Map<string, String>();
		Set<String> countrySet = new Set<String>();
		for(BI_SP_Country_settings__c cs : [SELECT Id, BI_SP_Country_code__c, BI_SP_External_system_country_code__c
													FROM BI_SP_Country_settings__c 
													WHERE BI_SP_Country_code__c IN :rawCountrySet]){
			countryRawCountryMap.put(cs.BI_SP_Country_code__c,cs.BI_SP_Country_code__c);
			countrySet.add(cs.BI_SP_Country_code__c);
		}

		usersMap = new Map<String, User>();

		for (User u : [Select Id, EmployeeNumber, Country_Code_BI__c
		                           from User
		                           where EmployeeNumber IN :userEmployeeNumberSet
		                           and Country_Code_BI__c IN :countrySet
		                           and IsActive = true]) {
			String rawCountry = countryRawCountryMap.get(u.Country_Code_BI__c);
			usersMap.put(u.EmployeeNumber+rawCountry, u);
		}
	}

	/**
	 * Gets the correct employee number( (removes all starts zeros).
	 *
	 * @param      shipTo  The ship to
	 *
	 * @return     The correct employee number.
	 */
	private String getCorrectEmployeeNumber(String shipTo){
		String employeeNumber = shipTo;
		while(employeeNumber.startsWith('0')){
			employeeNumber = employeeNumber.removeStart('0');
		}
		return employeeNumber;
	}
}