@isTest
private class BI_SP_StagingUserInfoTrigger_Test {
	
    public static String countryCode;

    @Testsetup
    static void setUp() {
        countryCode = 'BR';
		User pm = BI_SP_TestDataUtility.getProductManagerUser(countryCode, 0);
		User admin = BI_SP_TestDataUtility.getAdminUser(countryCode, 0);
        
        System.runAs(admin){
			List<User> userList = new List<User>();
			userList.add(pm);
			userList.add(admin);
			List<BI_SP_Staging_user_info__c> stagingUserList = BI_SP_TestDataUtility.createUserStagging(userList, countryCode);
		}
    }
    
	@isTest static void test_update() {
		List<BI_SP_Staging_user_info__c> stagingUserList = new List<BI_SP_Staging_user_info__c>([SELECT id, BI_SP_Ship_to__c, BI_SP_Raw_country_code__c FROM BI_SP_Staging_user_info__c]);
		BI_SP_Staging_user_info__c sUI = stagingUserList.get(0);
		User u = BI_SP_TestDataUtility.obtainProductManagerUser(0);
		sUI.BI_SP_Ship_to__c = '0000'+u.EmployeeNumber;
		update sUI;
	}
	
	@isTest static void test_delete() {
		List<BI_SP_Staging_user_info__c> stagingUserList = new List<BI_SP_Staging_user_info__c>([SELECT id, BI_SP_Ship_to__c, BI_SP_Raw_country_code__c FROM BI_SP_Staging_user_info__c]);
		BI_SP_Staging_user_info__c sUI = stagingUserList.get(0);
		delete sUI;
	}
}