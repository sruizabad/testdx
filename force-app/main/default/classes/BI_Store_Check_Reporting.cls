/*

BI_Store_Check_Reporting

Author: Raphael Krausz <raphael.krausz@veeva.com>
Date: 2014-10-20
Description:

So, the change is related to some reporting requirements.
We're currently using Contracts to define targets for Inventory Monitoring, and we have created corresponding
IM_* fields under Contract Lines in order to store the targets and display them in iRep while taking the
Inventory Monitoring (called Store Check in BI).

However SFDC does not have the reporting power needed to join the expected and actual values in one single
report, so we need to copy the target values from the contract to each Store Check.

Here's how it should be done:
Duplicate the Inventory Monitoring Fields in the Inventory_Monitoring_Line_vod Object.
As a naming convention please add T_ as a prefix.
The fields to be "duplicated" should be only the ones used by BI CHC (as per Global profile).
The format should be exactly the same, size, field type, picklist items, etc.
The description of ALL fields should start with "BI CHC ::", this is a useful marker that allows us to filter
when searching for CHC specific fields.

Create an additional checkbox field on Inventory_Monitoring_vod called Targets_Copied_BI (or Processed_BI) and
default it to False.
This will be used by the batch class to identify which IM should be processed.

Create an Apex Class supposed to run each night and that should do the following:
1. Select all Inventory Monitoring where Status = Submitted and Targets_Copied_BI = False
and Contract_vod ISNOTBLANK.
2. Loop on the recordset and
 2.1. Extract the Contract Lines for the Contract specified in Contract_vod
 2.2. Extract the Inventory_Monitoring_Lines of the IM being examined.
3. Loop on the IM Lines and look for the same product in the Contract Lines recordset
4. If found, copy the target fields (IM_*) from the Contract Line to the IM_Line (T_* fields)
5. Once all IM Lines are processed, mark the Targets_Copied_BI fields to True and move to the next
Inventory Monitoring record.

Schedule the batch class as a job to be run every night at 1 AM for example.


Fields to copy to Inventory Monitoring Line (where there are corresponding T_* fields)
-------------------------------------------
IM_Category_Facing_BI__c
IM_Inventory_vod__c
IM_BI_ProducIM_Facings_BI__c
IM_Consumer_Price_vod__c
IM_FronIM_Stock_BI__c
IM_No_Of_Displays_BI__c
IM_Layer_vod__c
IM_Secondary_Store_Position_BI__c
IM_LasIM_month_sales_BI__c
IM_LasIM_week_sales_BI__c

*/

global class BI_Store_Check_Reporting implements Database.Batchable<SObject> {

    private Map<Id, Map<Id, Set<Inventory_Monitoring_Line_vod__c>>> contractProductInventoryMonitoringLineMapSet;
    // contractId -> productId -> Inventory Monitoring Line Set

    private Map<Id, Map<Id, Contract_Line_vod__c>> contractProductContractLineMap;
    // contractId -> productId -> Contract Line

    private Map<Id, Inventory_Monitoring_vod__c> inventoryMonitoringMap;
    private List<Inventory_Monitoring_Line_vod__c> inventoryMonitoringLineToSaveList;
    private Set<Id> inventoryMonitoringToSaveIdSet;

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String selStmt =
            'SELECT'
            + '  Id,'
            + '  Contract_vod__c,'
            + '  Targets_Copied_BI__c'
            + ' FROM Inventory_Monitoring_vod__c'
            + ' WHERE Targets_Copied_BI__c = false'
            + '   AND Status_vod__c = \'Submitted_vod\''
            + '   AND Contract_vod__c != null'
            ;

        return Database.getQueryLocator(selStmt);

    }


    global void execute(Database.BatchableContext BC, List<Inventory_Monitoring_vod__c> inventoryMonitoringList) {

        Set<Id> contractIds = new Set<Id>();
        Set<Id> inventoryMonitoringIds = new Set<Id>();

        for (Inventory_Monitoring_vod__c inventoryMonitoring : inventoryMonitoringList) {
            Id contractId = inventoryMonitoring.Contract_vod__c;
            Id inventoryMonitoringId = inventoryMonitoring.Id;

            contractIds.add(contractId);
            inventoryMonitoringIds.add(inventoryMonitoringId);
        }

        populateInventoryMonitoringMap(inventoryMonitoringList);
        populateInventoryMonitoringLines(inventoryMonitoringIds);
        populateContractLines(contractIds);

        populateInventoryMonitoringLines(inventoryMonitoringList);

        saveChanges();
    }

    private void populateInventoryMonitoringMap(List<Inventory_Monitoring_vod__c> inventoryMonitoringList) {
        inventoryMonitoringMap = new Map<Id, Inventory_Monitoring_vod__c>();

        for (Inventory_Monitoring_vod__c inventoryMonitoring : inventoryMonitoringList) {
            inventoryMonitoringMap.put(inventoryMonitoring.Id, inventoryMonitoring);
        }
    }



    private void populateContractLines(Set<Id> contractIds) {

        contractProductContractLineMap = new Map<Id, Map<Id, Contract_Line_vod__c>>();

        List<Contract_Line_vod__c> contractLineList =
            [
                SELECT
                Id,
                Contract_vod__c,
                Product_vod__c,
                IM_Category_Facing_BI__c,
                IM_Inventory_vod__c,
                IM_BI_Product_Facings_BI__c,
                IM_Consumer_Price_vod__c,
                IM_Front_Stock_BI__c,
                IM_No_Of_Displays_BI__c,
                IM_Layer_vod__c,
                IM_Secondary_Store_Position_BI__c,
                IM_Last_month_sales_BI__c,
                IM_Last_week_sales_BI__c,
                IM_Position_vod__c
                FROM Contract_Line_vod__c
                WHERE Contract_vod__c IN :contractIds
            ];

        // private Map<Id, Map<Id, Contract_Line_vod__c>> contractProductContractLineMap;

        for (Contract_Line_vod__c contractLine : contractLineList) {
            Id contractId = contractLine.Contract_vod__c;
            if ( ! contractProductContractLineMap.containsKey(contractId) ) {
                contractProductContractLineMap.put( contractId, new Map<Id, Contract_Line_vod__c>() );
            }

            Id productId = contractLine.Product_vod__c;

            Map<Id, Contract_Line_vod__c> productContractLineMap = contractProductContractLineMap.get(contractId);

            if ( ! productContractLineMap.containsKey(productId) ) {
                productContractLineMap.put(productId, contractLine);
            }

        }
    }


    private void populateInventoryMonitoringLines(Set<Id> inventoryMonitoringIds) {

        // populates:
        // private Map<Id, Map<Id, Set<Inventory_Monitoring_Line_vod__c>>>
        // contractProductInventoryMonitoringLineMapSet;

        contractProductInventoryMonitoringLineMapSet =
            new Map<Id, Map<Id, Set<Inventory_Monitoring_Line_vod__c>>>();


        List<Inventory_Monitoring_Line_vod__c> inventoryMonitoringLineList =
            [
                SELECT
                Id,
                Inventory_Monitoring_vod__c,
                Product_vod__c,
                T_Category_Facing_BI__c,
                T_Inventory_BI__c,
                T_BI_Product_Facings_BI__c,
                T_Consumer_Price_BI__c,
                T_Front_Stock_BI__c,
                T_No_Of_Displays_BI__c,
                T_Layer_BI__c,
                T_Secondary_Store_Position_BI__c,
                T_Last_month_sales_BI__c,
                T_Last_week_sales_BI__c,
                T_Position_BI__c
                FROM Inventory_Monitoring_Line_vod__c
                WHERE Inventory_Monitoring_vod__c in :inventoryMonitoringIds
            ];

        for (Inventory_Monitoring_Line_vod__c inventoryMonitoringLine : inventoryMonitoringLineList) {

            Id inventoryMonitoringId = inventoryMonitoringLine.Inventory_Monitoring_vod__c;
            Inventory_Monitoring_vod__c inventoryMonitoring =
                inventoryMonitoringMap.get(inventoryMonitoringId);

            Id contractId = inventoryMonitoring.Contract_vod__c;
            Id productId  = inventoryMonitoringLine.Product_vod__c;

            if ( ! contractProductInventoryMonitoringLineMapSet.containsKey(contractId) ) {
                contractProductInventoryMonitoringLineMapSet.put(
                    contractId,
                    new Map<Id, Set<Inventory_Monitoring_Line_vod__c>>()
                );
            }

            Map<Id, Set<Inventory_Monitoring_Line_vod__c>> productInventoryMonitoringMapSet =
                contractProductInventoryMonitoringLineMapSet.get(contractId);

            if ( ! productInventoryMonitoringMapSet.containsKey(productId) ) {
                productInventoryMonitoringMapSet.put(productId, new Set<Inventory_Monitoring_Line_vod__c>());
            }

            Set<Inventory_Monitoring_Line_vod__c> populateInventoryMonitoringLineSet =
                productInventoryMonitoringMapSet.get(productId);

            populateInventoryMonitoringLineSet.add(inventoryMonitoringLine);

        }


    }

    private void populateInventoryMonitoringLines(List<Inventory_Monitoring_vod__c> inventoryMonitoringList) {

        /*
          private Map<Id, Map<Id, Set<Inventory_Monitoring_Line_vod__c>>>
          contractProductInventoryMonitoringLineMapSet;
          // contractId -> productId -> Inventory Monitoring Line Set

          private Map<Id, Map<Id, Contract_Line_vod__c>> contractProductContractLineMap;
          // contractId -> productId -> Contract Line

        */
        Set<Inventory_Monitoring_Line_vod__c> inventoryMonitoringLineToSaveSet =
            new Set<Inventory_Monitoring_Line_vod__c>();
        inventoryMonitoringToSaveIdSet = new Set<Id>();

        for (Inventory_Monitoring_vod__c inventoryMonitoring : inventoryMonitoringList) {
            Id inventoryMonitoringId = inventoryMonitoring.Id;
            Id contractId = inventoryMonitoring.Contract_vod__c;

            if ( ! contractProductContractLineMap.containsKey(contractId) ) {
                continue;
            }

            if ( ! contractProductInventoryMonitoringLineMapSet.containsKey(contractId) ) {
                continue;
            }

            Map<Id, Contract_Line_vod__c> productContractLineMap =
                contractProductContractLineMap.get(contractId);

            Map<Id, Set<Inventory_Monitoring_Line_vod__c>> productInventoryMonitoringLineMapSet =
                contractProductInventoryMonitoringLineMapSet.get(contractId);

            Set<Id> productIdList = productContractLineMap.keySet();
            for (Id productId : productIdList) {

                if ( ! productInventoryMonitoringLineMapSet.containsKey(productId) ) {
                    continue;
                }

                Contract_Line_vod__c contractLine = productContractLineMap.get(productId);
                Set<Inventory_Monitoring_Line_vod__c> inventoryMonitoringLineSet =
                    productInventoryMonitoringLineMapSet.get(productId);

                for (Inventory_Monitoring_Line_vod__c inventoryMonitoringLine : inventoryMonitoringLineSet) {
                    inventoryMonitoringLine.T_Category_Facing_BI__c = contractLine.IM_Category_Facing_BI__c;
                    inventoryMonitoringLine.T_Inventory_BI__c = contractLine.IM_Inventory_vod__c;
                    inventoryMonitoringLine.T_BI_Product_Facings_BI__c =
                        contractLine.IM_BI_Product_Facings_BI__c;
                    inventoryMonitoringLine.T_Consumer_Price_BI__c = contractLine.IM_Consumer_Price_vod__c;
                    inventoryMonitoringLine.T_Front_Stock_BI__c = contractLine.IM_Front_Stock_BI__c;
                    inventoryMonitoringLine.T_No_Of_Displays_BI__c = contractLine.IM_No_Of_Displays_BI__c;
                    inventoryMonitoringLine.T_Layer_BI__c = contractLine.IM_Layer_vod__c;
                    inventoryMonitoringLine.T_Secondary_Store_Position_BI__c =
                        contractLine.IM_Secondary_Store_Position_BI__c;
                    inventoryMonitoringLine.T_Last_month_sales_BI__c = contractLine.IM_Last_month_sales_BI__c;
                    inventoryMonitoringLine.T_Last_week_sales_BI__c = contractLine.IM_Last_week_sales_BI__c;
                    inventoryMonitoringLine.T_Position_BI__c = contractLine.IM_Position_vod__c;

                    inventoryMonitoringLineToSaveSet.add(inventoryMonitoringLine);
                    inventoryMonitoringToSaveIdSet.add(inventoryMonitoringLine.Inventory_Monitoring_vod__c);
                }
            }
        }

        inventoryMonitoringLineToSaveList = new List<Inventory_Monitoring_Line_vod__c>();
        inventoryMonitoringLineToSaveList.addAll(inventoryMonitoringLineToSaveSet);



    }

    private void saveChanges() {

        update inventoryMonitoringLineToSaveList;

        Set<Inventory_Monitoring_vod__c> inventoryMonitoringToSaveSet = new Set<Inventory_Monitoring_vod__c>();
        for (Id inventortyMonitoringId : inventoryMonitoringToSaveIdSet) {
            Inventory_Monitoring_vod__c inventoryMonitoring = inventoryMonitoringMap.get(inventortyMonitoringId);

            if ( inventoryMonitoring == null ) {
                continue;
            }

            inventoryMonitoring.Targets_Copied_BI__c = true;
            inventoryMonitoringToSaveSet.add(inventoryMonitoring);
        }

        List<Inventory_Monitoring_vod__c> inventoryMonitoringToSaveList = new List<Inventory_Monitoring_vod__c>();
        inventoryMonitoringToSaveList.addAll(inventoryMonitoringToSaveSet);

        update inventoryMonitoringToSaveList;

    }

    global void finish(Database.BatchableContext BC) {
    }

}