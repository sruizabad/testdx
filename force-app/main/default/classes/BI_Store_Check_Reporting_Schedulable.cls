global class BI_Store_Check_Reporting_Schedulable implements Schedulable {
    global void execute(SchedulableContext SC) {
      Database.executeBatch(new BI_Store_Check_Reporting(), 200);
    }
}