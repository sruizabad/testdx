@isTest
private class BI_TM_Account_Addres_TrackChanges_Test
{
	// Set to store the names of the relaevant fields to check
	public final static Set<String> fields2Track = new Set<String>{
		'Name', 'RecordTypeId', 'ParentId', 'Primary_Parent_vod__c', 'Account_Type__c', 'Account_Status_BI__c', 'BI_Target_BI__c', 'Contact_Type__c', 'Key_Account_BI__c', 'Primary_Language__c',
		'Scientific_Expert__c', 'Status_BI__c', 'Type_BI__c', 'Country_Code_BI__c ', 'Specialty_2_BI__c', 'Specialty_BI__c', 'Specialty_3_BI__c', 'OKIndividual_Class_BI__c', 'OKWorkplace_Class_BI__c',
		'OK_Individual_Type_BI__c', 'OK_Status_Code_BI__c', 'OK_Structure_Type_BI__c', 'OK_Workplace_Type_BI__c', 'Language_vod__c', 'BI_Maintained_BI__c', 'BI_Specialty_BI__c',
		'OK_Individual_SubType_BI__c', 'Channel_Type_BI__c', 'Account_Sub_Type_BI__c', 'Production_Type_BI__c', 'Top_Parent_Account_BI__c', 'Customer_Classification_BI__c', 'Channel_BI__c'
	};

	@isTest
	static void testOneAccount()
	{
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		Account acc = new Account(Name = 'Test Account');

		Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Account.fields.getMap();
		System.runAs(thisUser){
			Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
			insert ks;
			insert acc;
		}
		Test.startTest();
		//acc = [SELECT BI_TM_TimeStamp__c FROM Account WHERE Id = :acc.Id];
		//Datetime dt = acc.BI_TM_TimeStamp__c;
		//System.assertNotEquals(null, hash);
		//System.assertNotEquals(null, dt);

		// Execute update in account to check that the hash and timestamp remains the same
		update acc;

		Test.stopTest();

	}

	@isTest
	static void testBulkAccounts()
	{
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		List<Account> accList = new List<Account>();
		System.runAs(thisUser){
			Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
			insert ks;
		}
		for(Integer i = 0; i < 2000; i++){
			accList.add(new Account(Name = 'Test account ' + i));
		}
		insert accList;

		/*for(Account acc : [SELECT BI_TM_TimeStamp__c FROM Account WHERE Id IN :accList]){
			System.assertNotEquals(null, acc.BI_TM_TimeStamp__c);
		}*/
	}

	@isTest
	static void testBulkAddress()
	{
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		Account acc = new Account(Name = 'Test Account');
		List<Address_vod__c> addrList = new List<Address_vod__c>();
		System.runAs(thisUser){
			Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
			insert ks;
			insert acc;
		}
		for(Integer i = 0; i < 2000; i++){
			addrList.add(new Address_vod__c(Account_vod__c = acc.Id, Name = 'Address ' + String.ValueOf(i), Brick_vod__c = String.ValueOf(i)));
		}
		insert addrList;

		/*for(Account acc : [SELECT BI_TM_TimeStamp__c FROM Account WHERE Id IN :accList]){
			System.assertNotEquals(null, acc.BI_TM_TimeStamp__c);
		}*/
	}

	// Get a field trackable
	private static String getTrackableField(Map<String, Schema.SObjectField> schemaFieldMap){
		for(String field : fields2Track){
			if(schemaFieldMap.get(field).getDescribe().getSOAPType()==Schema.SOAPType.STRING){
				return field;
			}
			else if(schemaFieldMap.get(field).getDescribe().getSOAPType()==Schema.SOAPType.BOOLEAN){
				return field;
			}
		}
		return null;
	}

	// Check if the field is boolean
	private static Boolean checkBooleanField(String field, Map<String, Schema.SObjectField> schemaFieldMap){
		return schemaFieldMap.get(field).getDescribe().getSOAPType()==Schema.SOAPType.BOOLEAN;
	}
}