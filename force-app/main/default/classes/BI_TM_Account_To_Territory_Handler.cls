public class BI_TM_Account_To_Territory_Handler implements BI_TM_ITriggerHandler
{
  private static boolean recursionCheck=false;//To avoid recursion

  private static String OverlapDatesSystem_WithEndDate = 'Record already exist for the given account and position values. Existing Record Name: ';
  private static String OverlapDatesSystem_WithoutEndDate = 'There is already record existing without enddate for given account and territory in system. Please end date the existing record before creating new. Existing Record Name: ';
  private static String OverlapDatesInput_WithEndDate = 'Duplicate record existing within input source itself. Please validate data before loading.';
  private static String OverlapDatesInput_WithoutEndDate = 'There is already record existing without enddate within input source itself. Please validate data before loading';


  public void BeforeInsert(List<SObject> newItems) {

  }

  public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

  }

  public void BeforeDelete(Map<Id, SObject> oldItems) {

  }

  public void AfterInsert(Map<Id, SObject> newItems) {

    if(!recursionCheck){
      List<BI_TM_Account_To_Territory__c> manAssignments2Manage = getManAssignments2Manage(newItems.values());
      CheckDataOverlap(manAssignments2Manage);
      recursionCheck=TRUE;
      System.debug('**** manAssignments2Manage: ' + manAssignments2Manage);
    }
  }

  public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    if(!recursionCheck){
      List<BI_TM_Account_To_Territory__c> manAssignments2Manage = getManAssignments2Manage(newItems.values());
      CheckDataOverlap(manAssignments2Manage);
      recursionCheck=TRUE;
    }
  }

  public void AfterDelete(Map<Id, SObject> oldItems) {

  }

  public void AfterUndelete(Map<Id, SObject> oldItems) {

  }

  private List<BI_TM_Account_To_Territory__c> getManAssignments2Manage(List<BI_TM_Account_To_Territory__c> manAssignmentsList){
    List<BI_TM_Account_To_Territory__c> manAssignments2Manage = new List<BI_TM_Account_To_Territory__c>();
    for(BI_TM_Account_To_Territory__c ma : manAssignmentsList){
      if(!ma.BI_TM_GAS__c & !ma.BI_TM_Previous_Interactions__c){
        manAssignments2Manage.add(ma);
      }
    }
    return manAssignments2Manage;
  }

  public void CheckDataOverlap (List<BI_TM_Account_To_Territory__c> newItems){

    Map<Id, BI_TM_Account_To_Territory__c> updateMannualAssignments = new Map<Id, BI_TM_Account_To_Territory__c>();
    for(BI_TM_Account_To_Territory__c acc2TerrVar : newItems)
    if(trigger.IsUpdate)
    updateMannualAssignments.put(acc2TerrVar.Id, acc2TerrVar);

    List<BI_TM_Account_To_Territory__c> acc2TerrList = getSystemManualAssigments(newItems);

    for(BI_TM_Account_To_Territory__c acc2TerrOuterVar:(List<BI_TM_Account_To_Territory__c>)newItems){
      Boolean duplicateExistInSystem=false;
      Boolean duplicateExistInFile=false;
      String errorMsg1;
      String errorMsg2;

      //Check with the records already in database
      for(BI_TM_Account_To_Territory__c acc2TerrInnerVar:acc2TerrList){
        if(acc2TerrOuterVar.BI_TM_Account__c == acc2TerrInnerVar.BI_TM_Account__c &&
        acc2TerrOuterVar.BI_TM_Territory_FF_Hierarchy_Position__c == acc2TerrInnerVar.BI_TM_Territory_FF_Hierarchy_Position__c) {

          String errorWithEndDate = OverlapDatesSystem_WithEndDate + acc2TerrInnerVar.Name;
          String errorWithoutEndDate = OverlapDatesSystem_WithoutEndDate + acc2TerrInnerVar.Name;
          System.debug('**** Items comparison 1: ' + acc2TerrOuterVar + ' --- ' + acc2TerrInnerVar);
          System.debug('**** Items in updateMannualAssignments: ' + updateMannualAssignments);
          System.debug('**** errorWithEndDate: ' + errorWithEndDate + '----- errorWithoutEndDate ' + errorWithoutEndDate);
          //To avoid null pointer exception
          if(updateMannualAssignments.containsKey(acc2TerrOuterVar.Id)) {
            if(acc2TerrOuterVar != acc2TerrInnerVar &&
            acc2TerrOuterVar.Id != acc2TerrInnerVar.Id &&
            acc2TerrOuterVar.Id != acc2TerrInnerVar.BI_TM_old_manualAssignment__c &&
            acc2TerrOuterVar.BI_TM_old_manualAssignment__c != acc2TerrInnerVar.Id) {
              errorMsg1 = checkDatesOverlap (acc2TerrOuterVar, acc2TerrInnerVar, errorWithEndDate, errorWithoutEndDate);
              if (errorMsg1 != null)
              duplicateExistInSystem = true;
            }
          } else if(acc2TerrOuterVar != acc2TerrInnerVar &&
          acc2TerrOuterVar.Id != acc2TerrInnerVar.Id &&
          acc2TerrOuterVar.BI_TM_old_manualAssignment__c != acc2TerrInnerVar.Id) {
            errorMsg1 = checkDatesOverlap (acc2TerrOuterVar, acc2TerrInnerVar, errorWithEndDate, errorWithoutEndDate);
            if (errorMsg1 != null)
            duplicateExistInSystem = true;
          }
        }

        if(duplicateExistInSystem)
        break; //Break the loop if duplicate is found.
      }

      //Check with the records coming in single batch.
      for(BI_TM_Account_To_Territory__c acc2TerrInnerVar:(List<BI_TM_Account_To_Territory__c>)newItems){
        if(acc2TerrOuterVar.BI_TM_Account__c == acc2TerrInnerVar.BI_TM_Account__c &&
        acc2TerrOuterVar.BI_TM_Territory_FF_Hierarchy_Position__c == acc2TerrInnerVar.BI_TM_Territory_FF_Hierarchy_Position__c) {
          System.debug('**** Compare acc2TerrOuterVar: ' + acc2TerrOuterVar + ' --updateMannualAssignments -- ' + updateMannualAssignments);
          if(updateMannualAssignments.containsKey(acc2TerrOuterVar.Id)){
            System.debug('Items comparison: ' + acc2TerrOuterVar + ' --- ' + acc2TerrInnerVar);
            if(acc2TerrInnerVar != acc2TerrOuterVar &&
            acc2TerrOuterVar.Id != acc2TerrInnerVar.BI_TM_old_manualAssignment__c &&
            acc2TerrOuterVar.BI_TM_old_manualAssignment__c != acc2TerrInnerVar.Id &&
            ((acc2TerrOuterVar.id != acc2TerrInnerVar.Id) || acc2TerrOuterVar.id == NULL)) {

              errorMsg2 = checkDatesOverlap (acc2TerrOuterVar, acc2TerrInnerVar, OverlapDatesInput_WithEndDate, OverlapDatesInput_WithoutEndDate);
              if (errorMsg2 != null)
              duplicateExistInFile = true;
            }
          }else if(acc2TerrInnerVar != acc2TerrOuterVar &&
          acc2TerrOuterVar.BI_TM_old_manualAssignment__c != acc2TerrInnerVar.Id &&
          ((acc2TerrOuterVar.id!=acc2TerrInnerVar.Id) || acc2TerrOuterVar.id==NULL)) {
            System.debug('Items comparison: ' + acc2TerrOuterVar + ' --- ' + acc2TerrInnerVar);
            errorMsg2 = checkDatesOverlap (acc2TerrOuterVar, acc2TerrInnerVar, OverlapDatesInput_WithEndDate, OverlapDatesInput_WithoutEndDate);
            if (errorMsg2 != null)
            duplicateExistInFile = true;
          }
        }

        if(duplicateExistInFile)
        break; //Break the loop if duplicate is found.
      }

      //check if duplicate existed. Throw error if exist.
      if(acc2TerrOuterVar.BI_TM_End_date__c < acc2TerrOuterVar.BI_TM_Start_date__c)
      acc2TerrOuterVar.addError('End Date Should not be less than Start Date.');

      if(duplicateExistInFile)
      acc2TerrOuterVar.addError(errorMsg2);//Error message for user on duplicate record within file.
      else if (duplicateExistInSystem)
      acc2TerrOuterVar.addError(errorMsg1);//Error message for user on duplicate record within system.
    }

  }

  private List<BI_TM_Account_To_Territory__c> getSystemManualAssigments (List<BI_TM_Account_To_Territory__c> newItems) {
    Set<Id> accounts = new Set<Id>();
    Set<Id> territories = new Set<id>();

    for(BI_TM_Account_To_Territory__c accTerr : (List<BI_TM_Account_To_Territory__c>) newItems) {
      accounts.add(accTerr.BI_TM_Account__c);
      territories.add(accTerr.BI_TM_Territory_FF_Hierarchy_Position__c);
    }

    return [SELECT ID, BI_TM_Account__c, BI_TM_Territory_FF_Hierarchy_Position__c, BI_TM_Start_date__c, BI_TM_End_date__c, Name, BI_TM_old_manualAssignment__c
    FROM BI_TM_Account_To_Territory__c
    WHERE BI_TM_Account__c IN :accounts AND
    BI_TM_Territory_FF_Hierarchy_Position__c IN :territories AND (BI_TM_GAS__c = false AND BI_TM_Previous_Interactions__c = false)];
  }

  private String checkDatesOverlap (BI_TM_Account_To_Territory__c newMA, BI_TM_Account_To_Territory__c otherMA, String errorWithEndDate, String errorWithoutEndDate) {
    //check to avoid record check with itself
    system.debug('**** BI_TM_Account_To_Territory_Handler checkDatesOverlap Start newMA' + newMA + ' --otherMA ' + otherMA);
    if(newMA != otherMA && newMA.Id != otherMA.Id && newMA.Id != otherMA.BI_TM_old_manualAssignment__c && newMA.BI_TM_old_manualAssignment__c != otherMA.Id) {
      if(((newMA.BI_TM_End_date__c != NULL &&
      ((otherMA.BI_TM_Start_date__c >= newMA.BI_TM_Start_date__c && otherMA.BI_TM_Start_date__c <= newMA.BI_TM_End_date__c) ||
      (otherMA.BI_TM_End_date__c   >= newMA.BI_TM_Start_date__c && otherMA.BI_TM_End_date__c   <= newMA.BI_TM_End_date__c) ||
      (otherMA.BI_TM_Start_date__c <= newMA.BI_TM_Start_date__c && otherMA.BI_TM_End_date__c   >= newMA.BI_TM_End_date__c) ||
      (otherMA.BI_TM_Start_date__c <= newMA.BI_TM_Start_date__c && otherMA.BI_TM_End_date__c   >= newMA.BI_TM_Start_date__c))) ||
      (newMA.BI_TM_End_date__c == NULL && ((otherMA.BI_TM_End_date__c >= newMA.BI_TM_Start_date__c)))) &&  otherMA.BI_TM_End_date__c!=Null) {
        return errorWithEndDate;
        system.debug('**** BI_TM_Account_To_Territory_Handler errorWithEndDate: ' + errorWithEndDate);
      }
      else if(otherMA.BI_TM_End_date__c == Null &&
      (otherMA.BI_TM_Start_date__c <= newMA.BI_TM_End_date__c || newMA.BI_TM_End_date__c == NULL)){
        return errorWithoutEndDate;
        system.debug('**** BI_TM_Account_To_Territory_Handler errorWithoutEndDate: ' + errorWithoutEndDate);
      }
    }
    return null;
  }

}