@isTest
private class BI_TM_Account_To_Territory_Handler_Test
{
    @isTest
    static void method01()
    {
        /*Date startDate = system.Today();
        Date endDate = startDate.addDays(10);
        Date startDatePast = startDate.addDays(-7);
        Date startDateFut = startDate.addDays(7);
        Date endDateFut = startDate.addDays(15);
        Date endDatePast = startDate.addDays(-2);*/
        Date startDate = Date.newInstance(2016, 1, 1);
        Date endDate = Date.newInstance(2099, 12, 31);
        Date startDatema = Date.newInstance(2018, 1, 1);
        Date endDatema = Date.newInstance(2018, 1, 31);
        Date startDateCase1 = Date.newInstance(2018, 1, 2);
        Date endDateCase1 = Date.newInstance(2018, 2, 2);
        Date startDateCase2 = Date.newInstance(2018, 1, 2);
        Date endDateCase2 = Date.newInstance(2018, 2, 2);

        /*Date startDateFut = startDate.addDays(7);
        Date endDateFut = startDate.addDays(15);
        Date endDatePast = startDate.addDays(-2);*/

        //We generate data
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
        Account acc = new Account(Name = 'Test Account', Country_Code_BI__c = 'US');
        Account acc1 = new Account(Name = 'Test Account Two', Country_Code_BI__c = 'US');

        System.runAs(thisUser){
            Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
            insert ks;
        }
        insert acc;
        insert acc1;

        BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
        insert posType;

        BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
        insert pos;
        //system.debug('**** BI_TM_Account_To_Territory_Handler_Test posType, pos: ' + posType + ' position ' +pos);

        for(Integer i = 0; i < 10; i++){
            BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
            posList.add(p);
        }
        insert posList;

        BI_TM_Account_To_Territory__c ma = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc.Id, 'US', 'PM', startDatema, endDatema, false, false, 'Both', false, false);
        insert ma;
        BI_TM_Account_To_Territory__c ma1 = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc1.Id, 'US', 'PM', startDate, null , false, false, 'Both', false, false);
        insert ma1;

        system.debug('**** BI_TM_Account_To_Territory_Handler_Test ma: ' + ma);
        system.debug('**** BI_TM_Account_To_Territory_Handler_Test ma1: ' + ma1);



        //system.debug(':*** BI_TM_Account_To_Territory_Handler_Test An exception has ocurred AssgnList= ' +AssgnList );


				BI_TM_Account_To_Territory__c ma2 = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc.Id, 'US', 'PM', startDatema, endDatema + 1, false, false, 'Both', false, false);
				try {
					insert ma2;

				} catch(Exception e) {
					Boolean expectedExceptionThrown =  e.getMessage().contains('error') ? true : false;
					System.assertEquals(true, expectedExceptionThrown);
				}
				
        /*
        //Cases
        //Case 1 OverlapDatesSystem_WithEndDate = 'Record already exist for the given account and position values. Existing Record Name:
        try{
        BI_TM_Account_To_Territory__c case1 = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc.Id, 'US', 'PM', startDateCase1, endDateCase1, false, false, 'Both', false, false);
        insert case1;
        system.debug('**** BI_TM_Account_To_Territory_Handler_Test case1: ' + case1);

        }
        catch(Exception ex){
            system.debug(':*** BI_TM_Account_To_Territory_Handler_Test An exception has ocurred case 1 ' + ex.getMessage());
        }
        //Case 2 - OverlapDatesSystem_WithoutEndDate = 'There is already record existing without enddate for given account and territory in system. Please end date the existing record before creating new. Existing Record Name: ';
        try{
        BI_TM_Account_To_Territory__c case2 = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc1.Id, 'US', 'PM', startDateCase2, null,false, false, 'Both', false, false);
        insert case2;
        system.debug('**** BI_TM_Account_To_Territory_Handler_Test case2: ' + case2);
        }
        catch(Exception ex){
            system.debug('*** BI_TM_Account_To_Territory_Handler_Test An exception has ocurred case 2: ' + ex.getMessage());
        }
        //insert Identical registry to get a exception
        try{
            BI_TM_Account_To_Territory__c pete = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc.Id, 'US', 'PM', startDatema + 1, endDatema, false, false, 'Both', false, false);
            insert pete;

        }
        catch(Exception ex){
            system.debug('*** BI_TM_Account_To_Territory_Handler_Test An exception has ocurred pete: ' + ex.getMessage());
            System.assertEquals(true, ex.getMessage().contains('Record already exist for the given account and position values'));
        }

        //Case 3 - OverlapDatesInput_WithEndDate = 'Duplicate record existing within input source itself. Please validate data before loading.';

        //Case 4 - OverlapDatesInput_WithoutEndDate = 'There is already record existing without enddate within input source itself. Please validate data before loading';


        //Case 5 - Update a Manual assignment
        BI_TM_Account_To_Territory__c assignmentToUpdate;
        assignmentToUpdate= [SELECT BI_TM_Business__c FROM BI_TM_Account_To_Territory__c WHERE BI_TM_Business__c='PM' LIMIT 1];
         // Update the business.
        assignmentToUpdate.BI_TM_Business__c = 'AH';
        // Make the update Assign.
        update assignmentToUpdate;
        */
    }


}