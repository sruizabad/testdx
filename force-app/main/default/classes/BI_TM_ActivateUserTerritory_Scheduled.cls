/********************************************************************************
Name:  BI_TM_ActivateUserTerritory_Scheduled
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Scedule apex to Activate User Territory when start Date=Today comes
and Inactivate User Territory when End Date=Today
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran               16/02/2016   INITIAL DEVELOPMENT
2.0 -    Mario Chaves        30/05/2017   Deprecated
*********************************************************************************/

global class BI_TM_ActivateUserTerritory_Scheduled Implements Schedulable {

    global void execute(SchedulableContext SC){
        /*Commented by Mario Chaves 19052017
        BI_TM_ActivateUserTerritory();

        /*Commented by Mario Chaves 19052017
        BI_TM_InActivateUserTerritory();*/
        BI_TM_updatePrimaryPosition();
        database.executebatch(new BI_TM_UserToPositionActivate_Bch(),99);
    }



    Public void BI_TM_updatePrimaryPosition(){
        BI_TM_UserToPosition_PrimaryPosition updateprimaryPositionHandler = new BI_TM_UserToPosition_PrimaryPosition();
    }
    /*Commented by Mario Chaves 19052017
    public void BI_TM_ActivateUserTerritory(){
        List<BI_TM_User_territory__c> lstUserTerr = [Select Id, BI_TM_Start_date__c, BI_TM_End_date__c, BI_TM_Active__c,BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c,BI_TM_User_mgmt_tm__r.BI_TM_Active__c from BI_TM_User_territory__c Where BI_TM_Start_date__c <=Today and (BI_TM_End_date__c > Today or BI_TM_End_date__c = null) and BI_TM_Active__c = false AND (BI_TM_Status__c='No Approval Needed' or BI_TM_Status__c='All Approved')];
        system.debug('====+++++===='+lstUserTerr);
        List<BI_TM_User_territory__c> lstUpdateTrue= new List<BI_TM_User_territory__c>();
        for(BI_TM_User_territory__c usr : lstUserTerr){
            //if(usr.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c!=null && usr.BI_TM_User_mgmt_tm__r.BI_TM_Active__c==true){
               usr.BI_TM_Active__c = true;
               lstUpdateTrue.add(usr);
             //}
        }

        database.update(lstUpdateTrue, false);

    }

    public void BI_TM_InActivateUserTerritory(){
        List<BI_TM_User_territory__c> lstUserTerr = [Select Id, BI_TM_Territory1__c,BI_TM_Start_date__c, BI_TM_End_date__c, BI_TM_Active__c,BI_TM_By_Pass_Validation_Rule__c,BI_TM_Territory1__r.BI_TM_Is_Active__c from BI_TM_User_territory__c Where BI_TM_End_date__c < Today and BI_TM_Active__c = true];
        system.debug('====+++++===='+lstUserTerr);
        List<BI_TM_User_territory__c> lstUpdateFalse= new List<BI_TM_User_territory__c>();
        for(BI_TM_User_territory__c usr : lstUserTerr){
            if(usr.BI_TM_Territory1__r.BI_TM_Is_Active__c==false){
                usr.BI_TM_By_Pass_Validation_Rule__c=true;
             }
                usr.BI_TM_Active__c = false;
                lstUpdateFalse.add(usr);
        }

        database.update(lstUpdateFalse, false);

    }*/
}