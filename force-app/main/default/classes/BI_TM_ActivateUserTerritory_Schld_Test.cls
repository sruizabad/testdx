/********************************************************************************
Name:  BI_TM_ActivateUserTerritory_Schld_Test
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Test Class for BI_TM_ActivateUserTerritory_Scheduled
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran               16/02/2016   INITIAL DEVELOPMENT
*********************************************************************************/

@isTest(seeAllData=false)

  private class BI_TM_ActivateUserTerritory_Schld_Test{
  public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    static TestMethod void BI_TM_UserTerrActivate_scheduled(){
     list<My_Setup_Products_vod__c> usrprod = new list<My_Setup_Products_vod__c>();
      list<BI_TM_User_territory__c> usrter = new list<BI_TM_User_territory__c>();
      BI_TM_Territory__c Terr= new BI_TM_Territory__c();
      BI_TM_Territory__c Terr1= new BI_TM_Territory__c();
      BI_TM_FF_type__c FF= new BI_TM_FF_type__c();

      List<BI_TM_FF_type__c> fftypeId= new List<BI_TM_FF_type__c> ();
      BI_TM_User_territory__c ut= new BI_TM_User_territory__c();
      My_Setup_Products_vod__c msp= new My_Setup_Products_vod__c();
      RecordType  rtype= new RecordType();
      RecordType  rtype1= new RecordType();
      BI_TM_Mirror_Product__c mp=new BI_TM_Mirror_Product__c();
      BI_TM_Territory_to_product__c tp= new BI_TM_Territory_to_product__c();
      list<Profile> profid = new list<profile>();
       User currentuser = new User();
      currentuser = [Select Country_Code_BI__c,Business_BI__c From User Where Id = : UserInfo.getUserId()];
      Product_vod__c p=new Product_vod__c();
          p.Name='test33';
          p.Country_Code_BI__c=currentuser.Country_Code_BI__c ;
          p.Product_Type_vod__c='Detail';
          p.CurrencyIsoCode='MXN';
      insert p;

      FF.Name='Test MX FF21';
          FF.BI_TM_Business__c='PM';
          FF.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
      insert FF;



     BI_TM_Position_Type__c pt = new BI_TM_Position_Type__c();
      pt.Name='Test MX FF';
      pt.BI_TM_Business__c='AH';
      pt.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
      insert pt;
      rtype=[select Id from RecordType where Name='Position'Limit 1];
              Terr1.Name='Pos test22';
              Terr1.BI_TM_Position_Level__c='Country';
              Terr1.BI_TM_Position_Type_Lookup__c=pt.Id;
              Terr1.BI_TM_Is_Root__c=True;
              Terr1.BI_TM_FF_type__c=FF.Id;
              Terr1.BI_TM_Business__c='PM';
              Terr1.RecordTypeId=rtype.id;
              Terr1.BI_TM_Visible_in_crm__c=True;
              //Terr1.BI_TM_Is_Active__c=false;
              Terr1.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
              Terr1.BI_TM_Start_date__c=system.Today();

      Insert Terr1;

      //rtype1=[select Id from RecordType where Name='Territory'Limit 1];
          Terr.Name='Test Tam121';
          Terr.BI_TM_FF_type__c=FF.Id;
          Terr.BI_TM_Business__c='PM';
          Terr.BI_TM_Position_Level__c='Country';
          Terr.BI_TM_Position_Type_Lookup__c=pt.Id;
          Terr.BI_TM_Parent_Position__c=Terr1.Id;
          Terr.RecordTypeId=rtype.id;
          Terr.BI_TM_Visible_in_crm__c=True;
          //Terr.BI_TM_Is_Active__c=True;
          Terr.BI_TM_Start_date__c=system.Today();
          Terr.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
          //Terr.BI_TM_End_date__c=system.Today();

      Insert Terr;
       BI_TM_Territory_ND__c newTerr= new BI_TM_Territory_ND__c();
          newTerr.Name='testnewterr';
          newTerr.BI_TM_Field_Force__c=FF.Id;
          newTerr.BI_TM_Start_Date__c=Date.today().addDays(-2);
          newTerr.BI_TM_Business__c='PM';
          newTerr.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
      Insert newTerr;
          mp.Name='test33';
          mp.BI_TM_Product_Catalog_Id__c=p.Id;
          mp.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c;
          //mp.BI_TM_Active__c=True;
          //mp.BI_TM_Product_Type__c='Detail';
      Insert mp;

      profid =[select Id from profile where Name='TM-GLOBAL-USER' limit 1];
      User u = new user();
          u.LastName='TestUser33';
          u.Alias='Test33';
          u.Email='Testuser33@test.com';
          u.Username='Testuser33@test.com';
          u.CommunityNickname='Test33';
          u.ProfileId=profid[0].Id;
          u.Business_BI__c='PM';
          u.TimeZoneSidKey='Europe/Athens';
          u.LocaleSidKey='fr';
          u.LanguageLocaleKey='en_US';
          u.CurrencyIsoCode='EUR';
          u.EmailEncodingKey='ISO-8859-1';
          u.Country_Code_BI__c=currentuser.Country_Code_BI__c;
      Insert u;
       userRole ur= new userRole();
       ur=[select Id from UserRole where Name='Mexico' Limit 1];
       BI_TM_User_mgmt__c um = new BI_TM_User_mgmt__c();
       List<BI_TM_User_mgmt__c> ugmnt = new List<BI_TM_User_mgmt__c>();
       um.BI_TM_Active__c=False;
       um.BI_TM_Alias__c='Spatil';
       um.BI_TM_Business__c='PM';
       um.BI_TM_COMMUNITYNICKNAME__c='Spatil';
       um.BI_TM_Country__c=currentuser.Country_Code_BI__c;
       um.BI_TM_Profile__c='MX_SALES';
       um.BI_TM_LanguageLocaleKey__c='English';
       um.BI_TM_Email__c='test80.e.patil@capgemini.com';
       um.BI_TM_Username__c='test80.e.patil@capgemini.com';
       um.BI_TM_First_name__c='Test80';
       um.BI_TM_Last_name__c='Patil';
       um.BI_TM_TimeZoneSidKey__c='America/Mexico_City';
       um.BI_TM_LocaleSidKey__c='English (United States)';
       um.BI_TM_UserCountryCode__c=currentuser.Country_Code_BI__c;
       um.BI_TM_UserRole__c=ur.Id;
       ugmnt.add(um);
       Insert ugmnt;
          tp.BI_TM_Mirror_Product__c=mp.Id;
          tp.BI_TM_Territory_id__c=Terr.Id;
          tp.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c;

      Insert tp;

          ut.BI_TM_User_mgmt_tm__c=um.Id;
          ut.BI_TM_Territory1__c=Terr.Id;
          ut.BI_TM_Start_date__c=system.today();
          //ut.BI_TM_Active__c=True;
          ut.BI_TM_Assignment_Type__c='Shadow';
          ut.BI_TM_Country_Code__c=Terr1.BI_TM_Country_Code__c;
          ut.BI_TM_Business__c=Terr1.BI_TM_Business__c;
      usrter.add(ut);
      Insert usrter ;

      BI_TM_User_territory__c ut1= new BI_TM_User_territory__c();
          ut1.Id=usrter[0].Id;
          ut1.BI_TM_End_date__c=system.today();
          //ut1.BI_TM_Active__c=false;
      update ut1;


      Test.StartTest();
      //  BI_TM_User_territory__c usrterr= new BI_TM_User_territory__c();
          //usrterr.BI_TM_User_mgmt_tm__c='a6ZK00000004eCq';
         // usrterr.BI_TM_Territory1__c='a6QK00000008zoD';
          //usrterr.BI_TM_Start_date__c=system.today();
          //usrterr.BI_TM_Active__c=false;

            //insert usrterr;
        //usrterr=[select Id,BI_TM_Start_date__c,BI_TM_Active__c from BI_TM_User_territory__c where BI_TM_Start_date__c=: usrterr.BI_TM_Start_date__c];
       // usrterr.BI_TM_Active__c=true;
          // update usrterr;
          BI_TM_ActivateUserTerritory_Scheduled sc= new BI_TM_ActivateUserTerritory_Scheduled();
                  // sc.BI_TM_ActivateUserTerritory();
                  //  sc.BI_TM_InActivateUserTerritory();


      // Schedule the test job
      String jobId = System.schedule('BI_TM_ActivateUserTerritory_Scheduled',
                        CRON_EXP,
                        new BI_TM_ActivateUserTerritory_Scheduled());


       try {
          //insert
            }

         catch(DMLException e) {
            system.assertEquals(e.getMessage(), e.getMessage());
        }

       Test.StopTest();

     }

  }