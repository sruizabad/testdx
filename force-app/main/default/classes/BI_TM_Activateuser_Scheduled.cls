/******************************************************************************** 
Name:  BI_TM_Activateuser_Scheduled
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
apex schedule class to Activate,Inactivate the veeva User and Assign Permission Set to Active User
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Kiran              25/01/2016   INITIAL DEVELOPMENT
*********************************************************************************/
global class BI_TM_Activateuser_Scheduled Implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
        // first we de activate users so that we will have more licenses available for activating users
        InactivateUsers();
        // then we activate users
        ActivateUsers();
        //Assign Permission Set to User
        //AssignPermissionSet();
    }
    
    public void InactivateUsers() {
        List<BI_TM_User_mgmt__c> umList = [SELECT Id,BI_TM_Username__c, BI_TM_UserId_Lookup__c FROM BI_TM_User_mgmt__c WHERE BI_TM_End_date__c != null AND BI_TM_End_date__c < Today];
        Set<String> usernames = new Set<String>();
        for(BI_TM_User_mgmt__c um : umList) {
            usernames.add(um.BI_TM_Username__c);
        }
        
        List<User> veevaUsersToInactivate = [SELECT ID,Isactive,Username FROM User WHERE IsActive = True AND username in :usernames];
        for(User u : veevaUsersToInactivate) {
            u.Isactive = false;
        }
        
        database.Update(veevaUsersToInactivate, false);
   }         
  
    
     public void ActivateUsers() {
        List<BI_TM_User_mgmt__c> umList = [SELECT Id,BI_TM_Username__c, BI_TM_UserId_Lookup__c FROM BI_TM_User_mgmt__c where BI_TM_Start_date__c <= TODAY AND (BI_TM_End_date__c >= TODAY OR BI_TM_End_date__c = null)];
        
        Set<String> usernames = new Set<String>();
        Set<User> activeveevausers =new Set<User>();
        for(BI_TM_User_mgmt__c um : umList) {
            usernames.add(um.BI_TM_Username__c);
        }    
        List<User> veevaUsersToActivate = [SELECT ID,Isactive,Username FROM User WHERE IsActive = false AND username in :usernames];
        for(User u : veevaUsersToActivate) {
            u.Isactive = true;
            activeveevausers.add(u);
        }
        
        database.Update(veevaUsersToActivate,false);
        
        
        for(User u : activeveevausers) {
          if(u.IsActive==True){
          system.debug('Inside IF Condition for Active Users');
          system.debug('=====++++Active User======+++'+u.Id);
            System.resetPassword(u.Id, true);
           }
        } 
        // The below code is to Assign Permission Set to User in Veeva 
       try {
        List<BI_TM_User_mgmt__c> umListPermissionset = [Select Id,BI_TM_Username__c,BI_TM_Permission_Set__c from BI_TM_User_mgmt__c where BI_TM_Permission_Set__c != null and BI_TM_Username__c in :usernames];
        system.debug('===+++umListPermissionset+++===='+umListPermissionset);
        
        
      List<PermissionSetAssignment> assignpermission = new List<PermissionSetAssignment>();
      //Set<PermissionSetAssignment> assignpermission1 = new Set<PermissionSetAssignment>();
            //assignpermission=[Select Id from PermissionSet where Name IN:umListPermissionset];
        Set<String> usernamesset = new Set<String>();
        Set<String> userPermissionset =new Set<String>();
        
        for(BI_TM_User_mgmt__c um : umListPermissionset) {
            usernamesset.add(um.BI_TM_Username__c);
            userPermissionset.add(um.BI_TM_Permission_Set__c);
        }    
        system.debug('===+++usernamesset+++===='+usernamesset);
        system.debug('===+++userPermissionset+++===='+userPermissionset);
        
        List<PermissionSet> permissionsetId= [Select Id,Name from PermissionSet Where Name IN: userPermissionset];
        system.debug('===+++permissionsetId+++===='+permissionsetId);
        List<User> veevaUsersAssignpermission = [SELECT ID,Isactive,Username FROM User WHERE username in :usernamesset];
        system.debug('===+++veevaUsersAssignpermission +++===='+veevaUsersAssignpermission );
        
        String myPermissionSetId;
        for(PermissionSet p:permissionsetId){
          myPermissionSetId=p.Id;
        }
        //Get Permission Set Id already assigned to User
        List<PermissionSetAssignment> permissionsetassigned= [select Id,PermissionSet.Id,PermissionSet.Name from PermissionSetAssignment where AssigneeId IN :veevaUsersAssignpermission];
        System.debug('======PermissionSetAssigned to User====='+permissionsetassigned);
        
        String PermissionSetAssignedId;
                 for(PermissionSetAssignment pr : permissionsetassigned){
                    PermissionSetAssignedId=pr.PermissionSet.Id;
                }
        System.debug('======PermissionSet Id Assigned to User====='+PermissionSetAssignedId);
        //Assign Permission set to User
         if(PermissionSetAssignedId != myPermissionSetId){
                for(User u : veevaUsersAssignpermission){
                        
                        PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = myPermissionSetId, AssigneeId = u.Id);                      
                        assignpermission.add(psa); 
                        }
              insert assignpermission;
               }
            }catch (Exception e) {}
    } 
}