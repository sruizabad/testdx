/******************************************************************************** 
Name:  BI_TM_CreateUserRoleAndTerritoryHandler
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Test Calss for BI_TM_ActivateUser_Scheduled
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Kiran               25/01/2016   INITIAL DEVELOPMENT
*********************************************************************************/


@isTest

   private class BI_TM_Activateuser_Scheduled_Test{
     
    static testMethod void TestBI_TM_Activateuser_Scheduled1()   {
    Test.StartTest();
    List<Profile> profId = new List<Profile>();     
     profId = [SELECT Name,Id FROM Profile WHERE Name='CA_SALES_SSR'];    
      User us =new User();
         us.IsActive=True;
         us.Username='testusr1@bi.mvs';
         us.FirstName='Suc';
         us.lastname='Nag';
         us.Email='sucnag@capgemini.com';
         us.Alias='sucnag';
         us.ProfileId=profId[0].Id;
         us.TimeZoneSidKey='America/Chicago';
         us.LanguageLocaleKey='en_US';
         us.LocaleSidKey='en_US';
         us.EmailEncodingKey='ISO-8859-1';
         
      BI_TM_User_mgmt__c usmgmt =new BI_TM_User_mgmt__c();
         usmgmt.BI_TM_Start_Date__c=System.today();
         usmgmt.BI_TM_End_date__c=System.today();
         usmgmt.BI_TM_Username__c='testusr1@bi.mvs';
         usmgmt.BI_TM_First_name__c='Suc';
         usmgmt.BI_TM_Last_name__c='Nag';
         usmgmt.BI_TM_Email__c='sucnag@capgemini.com';
         usmgmt.BI_TM_Alias__c='sucnag';
         usmgmt.BI_TM_Profile__c='CA_SALES_SSR';
         usmgmt.BI_TM_TimeZoneSidKey__c='America/El_Salvador';
         usmgmt.BI_TM_LocaleSidKey__c='English (United States)';
         usmgmt.BI_TM_LanguageLocaleKey__c='Spanish (Mexico)';
        
                
       //insert us;
           insert us;
           insert usmgmt;
           BI_TM_Activateuser_Scheduled sh1= new BI_TM_Activateuser_Scheduled();
           String sch = '0 0 23 * * ?'; 
           system.schedule('UserActive', sch, sh1); 

                    
       /*try {       
            }
        
         catch(DMLException e) {
            system.assertEquals(e.getMessage(), e.getMessage());
        }
        //us.IsActive=False;
        //us.execute(); */
        
          Test.stopTest();
    }
       
       
     
       
    
       
   
   }