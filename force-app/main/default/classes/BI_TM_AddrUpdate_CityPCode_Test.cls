@isTest
private class BI_TM_AddrUpdate_CityPCode_Test
{
	@isTest
	static void method01()
	{
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		Account acc = new Account(Name = 'Test Account', Country_Code_BI__c = 'US', recordTypeId = getRecTypeId('Account', 'AH_Pharmacy'));
		System.runAs(thisUser){
			Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
			insert ks;
			insert acc;
			Address_vod__c addr = new Address_vod__c(Name = 'Test Address', Account_vod__c = acc.Id, Zip_vod__c = 'ZIP01', City_vod__c = 'CITY01', Country_Code_BI__c = 'US');
			insert addr;
			System.debug('*** address: ' + addr);
			BI_TM_Address_BrickCountrySettings__c abcs = new BI_TM_Address_BrickCountrySettings__c(Name = 'Test ABCS US', BI_TM_CountryCode__c = 'US', BI_TM_Record_Types__c = '\'AH Pharmacy\'', BI_TM_Apply_City_Postal_Code_Combination__c = true);
			BI_TM_Address_BrickCountrySettings__c abcs1 = new BI_TM_Address_BrickCountrySettings__c(Name = 'Test ABCS CA', BI_TM_CountryCode__c = 'CA', BI_TM_Record_Types__c = '\'AH Pharmacy\'', BI_TM_Apply_City_Postal_Code_Combination__c = true);
			insert abcs;
			insert abcs1;
			BI_TM_PostalCode__c pc = new BI_TM_PostalCode__c(Name = 'ZIP01;CITY01', BI_TM_Attr1__c = 'BRICK01', BI_TM_Attr2__c = 'BRNAME01', BI_TM_Country_Code__c = 'US');
			insert pc;
			Customer_Attribute_BI__c ca = new Customer_Attribute_BI__c(Name = 'BRNAME01', Type_BI__c = 'ADDR_Brick Name', recordTypeId = getRecTypeId('Customer_Attribute_BI__c', 'OK_Local_Attribute'), Country_Code_BI__c = 'US');
			insert ca;
		}

		Test.startTest();
		database.executeBatch(new BI_TM_AddrUpdate_CityPCode_batch());
		System.schedule('Test schedule', '0 0 23 * * ?', new BI_TM_AddrUpdate_CityPCode_batch());
		Test.stopTest();

	}

	private static Id getRecTypeId(String obj, String recTypeName){
		return [SELECT Id FROM recordType WHERE DeveloperName = :recTypeName AND SObjectType = :obj].Id;
	}
}