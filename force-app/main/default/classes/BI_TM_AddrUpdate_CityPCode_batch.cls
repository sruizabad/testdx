/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Batch to the brick with the combiantion from zip and city
*  @author:         Antonio Ferrero
*  @created:        16-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         16-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_AddrUpdate_CityPCode_batch implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    String strQuery;

    global BI_TM_AddrUpdate_CityPCode_batch()
    {
        List<BI_TM_Address_BrickCountrySettings__c> lstAddressCountrySett = [SELECT BI_TM_CountryCode__c, BI_TM_Record_Types__c FROM BI_TM_Address_BrickCountrySettings__c WHERE BI_TM_Apply_City_Postal_Code_Combination__c = true];

        strQuery='SELECT Id, City_vod__c, Country_Code_BI__c, Brick_vod__c, OK_Brick_Name_BI__c, OK_Brick_Name_BI__r.Name, Zip_vod__c FROM Address_vod__c where Zip_vod__c != null AND City_vod__c != null AND (';
        Integer intCont = 1;
        Integer intSize = lstAddressCountrySett.size();
        for(BI_TM_Address_BrickCountrySettings__c objAddressSettings : lstAddressCountrySett)
        {
          if(intCont == 1){
						strQuery+='(Country_Code_BI__c = \''+ objAddressSettings.BI_TM_CountryCode__c + '\' AND Account_vod__r.RecordType.Name IN (' + objAddressSettings.BI_TM_Record_Types__c + '))';
					}
          else
          {
            strQuery+=' OR (Country_Code_BI__c = \'' + objAddressSettings.BI_TM_CountryCode__c + '\' AND Account_vod__r.RecordType.Name IN (' + objAddressSettings.BI_TM_Record_Types__c + '))';
          }
          intCont++;
        }
        strQuery+=')';
        System.debug('strQuery :: ' + strQuery);
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(strQuery);
    }

  global void execute(Database.BatchableContext BC, List<Address_vod__c> scope)
    {
      Map<Id, String> addressPcCityMap = getPostalCodeCityMap(scope);
      Map<Id, Address_vod__c> addressInMap = new Map<Id, Address_vod__c>(scope);
      Set<String> countryCodeSet = getCountryCodeValues(scope);
      Map<String, BI_TM_PostalCode__c> bPostalCodeMap = getPostalCodeMap(addressPcCityMap, countryCodeSet);
      List<Address_vod__c> addressToUpdate = new List<Address_vod__c>();
      Map<String, Id> customAttrMap = getCustomAttrMap(bPostalCodeMap.values(), countryCodeSet);

      // Run through the address and check if there is a mapping for the postal code + city combination
      for(Id key : addressPcCityMap.keySet()){
        // Get the current brick of the address
        String currentBrick = addressInMap.get(Key).Brick_vod__c != null ? addressInMap.get(Key).Brick_vod__c : '';
        String currentBrickName = addressInMap.get(Key).OK_Brick_Name_BI__r.Name != null ? addressInMap.get(Key).OK_Brick_Name_BI__r.Name : '';

        // Get the combination of postal code and city for this address
        String comb = addressPcCityMap.get(key);
        BI_TM_PostalCode__c pCode = bPostalCodeMap.get(comb);

        // Check if there is any mapping for that combination
        if(pCode != null){
          Address_vod__c addr2update = addressInMap.get(key);
          Boolean addAddress = false;
          if(!String.isBlank(pCode.BI_TM_Attr1__c)){
            String brick = pCode.BI_TM_Attr1__c;
            if(currentBrick != brick){
              addr2update.Brick_vod__c = brick;
              addAddress = true;
            }
          }

          if(!String.isBlank(pCode.BI_TM_Attr2__c)){
            String brickName = pCode.BI_TM_Attr2__c;
            if(currentBrickName != brickName && customAttrMap.get(addr2update.Country_Code_BI__c + brickName) != null){
              addr2update.OK_Brick_Name_BI__c = customAttrMap.get(addr2update.Country_Code_BI__c + brickName);
              addAddress = true;
            }
          }

          // Update brick if needed
          if(addAddress){
            addressToUpdate.add(addr2update);
          }
        }
      }

      // Update addresses
      Database.SaveResult[] srList = Database.update(addressToUpdate, false);
      BI_TM_Utils.manageErrorLogUpdate(srList);
    }

    // Definition of th method to schedule the batch
    global void execute(SchedulableContext sc) {
  		database.executebatch(new BI_TM_AddrUpdate_CityPCode_batch());
    }

    global void finish(Database.BatchableContext BC){

    }

		// Get the set of postal codes from the addresses
		private Map<Id, String> getPostalCodeCityMap(List<Address_vod__c> addresses){
			Map<Id, String> postalCodeCityMap = new Map<Id, String>();
			for(Address_vod__c addr : addresses){
        String comb = addr.Zip_vod__c + ';' + addr.City_vod__c;
        postalCodeCityMap.put(addr.Id, comb);
      }
			return postalCodeCityMap;
		}

    // Get the postal code mapping from the BITMAN table
    private Map<String, BI_TM_PostalCode__c> getPostalCodeMap(Map<Id, String> addressPcCityMap, Set<String> countryCodeSet){
      Map<String, BI_TM_PostalCode__c> bPostalCodeMap = new Map<String, BI_TM_PostalCode__c>();
      for(BI_TM_PostalCode__c pc : [SELECT Name, BI_TM_Country_Code__c, BI_TM_Attr1__c, BI_TM_Attr2__c FROM BI_TM_PostalCode__c WHERE Name IN :addressPcCityMap.values() AND BI_TM_Country_Code__c IN :countryCodeSet])
        bPostalCodeMap.put(pc.Name, pc);
      return bPostalCodeMap;
    }

		// Get the set of country codes from the addresses
		private Set<String> getCountryCodeValues(List<Address_vod__c> addresses){
			Set<String> countryCodeSet = new Set<String>();
			for(Address_vod__c addr : addresses)
				countryCodeSet.add(addr.Country_Code_BI__c);
			return countryCodeSet;
		}

    // Get the map of customer attributes for brick names
    private static Map<String, Id> getCustomAttrMap(List<BI_TM_PostalCode__c> postalCodeList, Set<String> countryCodeSet){
      Set<String> brickNameSets = new Set<String>();
      for(BI_TM_PostalCode__c pc : postalCodeList){
        brickNameSets.add(pc.BI_TM_Attr2__c);
      }
      Map<String, Id> customAttrMap = setCustomAttribMap(brickNameSets, countryCodeSet);
      return customAttrMap;
    }

    // Get the map of customer attributes for brick names
    private static Map<String, Id> setCustomAttribMap(Set<String> brickNameSets, Set<String> countryCodeSet){
      Map<String, Id> customAttrMap = new Map<String, Id>();
      for(Customer_Attribute_BI__c ca : [SELECT Id, Name, Country_Code_BI__c FROM Customer_Attribute_BI__c WHERE Name IN :brickNameSets
                                          AND Country_Code_BI__c IN :countryCodeSet AND Type_BI__c = 'ADDR_Brick Name'
                                          AND RecordType.DeveloperName= 'OK_Local_Attribute']){
        customAttrMap.put(ca.Country_Code_BI__c + ca.Name, ca.Id);
      }
      return customAttrMap;
    }
}