/********************************************************************************
Name:  BI_TM_AddressUpdateDynamic_Batch

Batch to update the address field with the dynamic configuration. The target field is updated with the values of the key field map from
the source object and the source field

VERSION  AUTHOR              DATE         DETAIL
1.0 -    Antonio Ferrero     08/11/2018   INITIAL DEVELOPMENT
*********************************************************************************/
global class BI_TM_AddressUpdateDynamic_Batch implements Database.Batchable<sObject>, Schedulable, Database.stateful {

	String query;
	DateTime timeStamp;
	Integer addrBrListSize;
	List<BI_TM_Address_BrickCountrySettings__c> addrBrList;

	global BI_TM_AddressUpdateDynamic_Batch(Datetime runTime) {
		this.timeStamp = runTime;
		this.query = 'SELECT Id FROM Address_vod__c LIMIT 0';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		addrBrList =  [SELECT Id, BI_TM_Key_Field__c, BI_TM_Source_Key_Field__c, BI_TM_Source_Field__c, BI_TM_Source_Object__c, BI_TM_Target_Field__c, BI_TM_TimeStamp__c, BI_TM_CountryCode__c FROM BI_TM_Address_BrickCountrySettings__c WHERE BI_TM_Apply_Dynamic_Address_Update__c = TRUE AND BI_TM_TimeStamp__c != :timeStamp LIMIT 1];
		addrBrListSize = addrBrList.size();
		if(addrBrListSize > 0){
			addrBrList[0].BI_TM_TimeStamp__c = timeStamp;
			update addrBrList;
			query = 'SELECT Id, ' + addrBrList[0].BI_TM_Key_Field__c + ', ' + addrBrList[0].BI_TM_Target_Field__c + ' FROM Address_vod__c WHERE Account_vod__r.Country_Code_BI__c = \'' + addrBrList[0].BI_TM_CountryCode__c + '\' AND ' + addrBrList[0].BI_TM_Key_Field__c + ' != null ORDER BY ' + addrBrList[0].BI_TM_Key_Field__c;
		}
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<Address_vod__c> scope) {
		// Field to retrieve the keys to match agains the source
		Set<String> keyFieldValues = new Set<String>();
		for(Address_vod__c addr : scope){
			String value = (String)addr.get(addrBrList[0].BI_TM_Key_Field__c);
			keyFieldValues.add(value);
		}

		// Retrieve data from the source table
		String querySource = 'SELECT ' + addrBrList[0].BI_TM_Source_Field__c + ', ' + addrBrList[0].BI_TM_Source_Key_Field__c + '  FROM ' + addrBrList[0].BI_TM_Source_Object__c + ' WHERE ' + addrBrList[0].BI_TM_Source_Key_Field__c + ' IN :keyFieldValues';

		// Build a map with the keys
		Map<String, String> keySourceMap = new Map<String, String>();
		for(SObject sob : Database.query(querySource)){
			keySourceMap.put((String)sob.get(addrBrList[0].BI_TM_Source_Key_Field__c), (String)sob.get(addrBrList[0].BI_TM_Source_Field__c));
		}

		// Loop over the addresses to update the target field with the key map
		List<Address_vod__c> addr2Update = new List<Address_vod__c>();
		for(Address_vod__c addr : scope){
			String key = (String)addr.get(addrBrList[0].BI_TM_Key_Field__c);
			String targetValue = keySourceMap.get(key);
			if(addr.get(addrBrList[0].BI_TM_Target_Field__c) != targetValue && targetValue != null){
				addr.put(addrBrList[0].BI_TM_Target_Field__c, targetValue);
				addr2Update.add(addr);
			}
		}

		// Update the records
		if(addr2Update.size() > 0){
			Database.SaveResult[] srList = Database.update(addr2Update, false);
      BI_TM_Utils.manageErrorLogUpdate(srList);
		}
	}

	global void execute(SchedulableContext sc) {
		Database.executebatch(new BI_TM_AddressUpdateDynamic_Batch(system.now()));
	}

	global void finish(Database.BatchableContext BC) {
		if(addrBrListSize > 0){
			Database.executebatch(new BI_TM_AddressUpdateDynamic_Batch(timeStamp), 2000);
		}
	}
}