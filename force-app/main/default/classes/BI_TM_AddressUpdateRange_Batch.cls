/********************************************************************************
Name:  BI_TM_AddressUpdateRange_Batch

Batch to update the address brick with a range defined in the postal codes

VERSION  AUTHOR              DATE         DETAIL
1.0 -    Antonio Ferrero     08/11/2018   INITIAL DEVELOPMENT
*********************************************************************************/

global class BI_TM_AddressUpdateRange_Batch implements Database.Batchable<sObject>, Schedulable, Database.stateful {
	String query;
	DateTime timeStamp;
	Integer ccListSize;
	List<BI_TM_CountryCodes__c> ccList;

	global BI_TM_AddressUpdateRange_Batch(Datetime runTime) {
		this.timeStamp = runTime;
		this.query = 'SELECT Id FROM Address_vod__c LIMIT 0';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		ccList =  [SELECT Id, CountryCode__c, BI_TM_Address_Range_TimeStamp__c FROM BI_TM_CountryCodes__c WHERE BI_TM_Apply_Address_Update_Range__c = TRUE AND BI_TM_Address_Range_TimeStamp__c != :timeStamp LIMIT 1];
		ccListSize = ccList.size();
		if(ccListSize > 0){
			ccList[0].BI_TM_Address_Range_TimeStamp__c = timeStamp;
			update ccList;
			query = 'SELECT zip_vod__c, Brick_vod__c FROM Address_vod__c WHERE Account_vod__r.Country_Code_BI__c = \'' + ccList[0].CountryCode__c + '\' AND zip_vod__c != null ORDER BY zip_vod__c';
			System.debug('*** query: ' + query);
		}
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<Address_vod__c> scope) {
		Integer minPostalCode = null;
		Integer maxPostalCode = null;

		// Calculate min and max values of the postal codes
		List<Address_vod__c> addr2Check = new List<Address_vod__c>();
		for(Address_vod__c addr : scope){
			if((addr.zip_vod__c).isNumeric() && (addr.zip_vod__c).length() == 8){
				minPostalCode = (minPostalCode == null || Integer.valueOf(addr.zip_vod__c) < minPostalCode) ? Integer.valueOf(addr.zip_vod__c) : minPostalCode;
				maxPostalCode = (maxPostalCode == null || Integer.valueOf(addr.zip_vod__c) > minPostalCode) ? Integer.valueOf(addr.zip_vod__c) : maxPostalCode;
				addr2Check.add(addr);
			}
		}

		// Loop over the addresses that fulfill the conditions and check if postal code is in the range defined
		if(minPostalCode != null && maxPostalCode != null){
			List<Address_vod__c> addr2Update = new List<Address_vod__c>();
			for(BI_TM_PostalCode__c pc : [SELECT Name, BI_TM_NumericPostalCode__c, BI_TM_MaxPostalCode__c, BI_TM_Geography__r.Name FROM BI_TM_PostalCode__c WHERE BI_TM_Country_Code__c = :ccList[0].CountryCode__c AND BI_TM_IsValidForRange__c = TRUE AND (BI_TM_MaxPostalCode__c >= :maxPostalCode OR BI_TM_NumericPostalCode__c <= :minPostalCode)]){
				for(Address_vod__c addr : addr2Check){
					if(Integer.valueOf(addr.zip_vod__c) >= pc.BI_TM_NumericPostalCode__c && Integer.valueOf(addr.zip_vod__c) <= pc.BI_TM_MaxPostalCode__c && addr.Brick_vod__c != pc.BI_TM_Geography__r.Name){
						addr.Brick_vod__c = pc.BI_TM_Geography__r.Name;
						addr2Update.add(addr);
					}
				}
			}
			// Update the records
			if(addr2Update.size() > 0){
				Database.SaveResult[] srList = Database.update(addr2Update, false);
	      BI_TM_Utils.manageErrorLogUpdate(srList);
			}
		}
	}

	global void finish(Database.BatchableContext BC) {
		if(ccListSize > 0){
			Database.executebatch(new BI_TM_AddressUpdateRange_Batch(timeStamp));
		}
	}

	global void execute(SchedulableContext sc) {
		Database.executebatch(new BI_TM_AddressUpdateRange_Batch(system.now()));
	}

}