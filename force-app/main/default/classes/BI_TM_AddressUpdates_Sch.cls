/********************************************************************************
Name:  BI_TM_AddressUpdates_sch

Batch to update the address brick fields this is related with th CR 975, the
Setting must be set up into the custom setting BI_TM_Address_BrickCountrySettings__c

VERSION  AUTHOR              DATE         DETAIL
1.0 -    Mario Chaves       21/04/2017   INITIAL DEVELOPMENT
*********************************************************************************/
global class BI_TM_AddressUpdates_Sch implements Schedulable {
	global void execute(SchedulableContext sc) {
		database.executebatch(new BI_TM_AddressUpdates_batch());

	}
}