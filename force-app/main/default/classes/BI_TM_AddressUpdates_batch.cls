/********************************************************************************
Name:  BI_TM_AddressUpdates_batch

Batch to update the address brick fields this is related with th CR 975, the
Setting must be set up into the custom setting BI_TM_Address_BrickCountrySettings__c

VERSION  AUTHOR              DATE         DETAIL
1.0 -    Mario Chaves       21/04/2017   INITIAL DEVELOPMENT
*********************************************************************************/
global class BI_TM_AddressUpdates_batch implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    String strQuery;
    public Map <String,BI_TM_Address_BrickCountrySettings__c> mapCountryByAddressCountrySett;
    public Map <String,BI_TM_PostalCode__c> mapCodebyPostalCode;
    global Map <String, List<String>> MapCountryByAddresswithoutBrick=new Map<String, List<String>>();
    global BI_TM_AddressUpdates_batch()
    {
        List<BI_TM_Address_BrickCountrySettings__c> lstAddressCountrySett = new List<BI_TM_Address_BrickCountrySettings__c>();
        // Filter added for the DM-6918
        List<BI_TM_Address_BrickCountrySettings__c> lstAllAddressCountrySett = BI_TM_Address_BrickCountrySettings__c.getAll().values();
        for(BI_TM_Address_BrickCountrySettings__c abs : lstAllAddressCountrySett){
          if(!abs.BI_TM_Apply_City_Postal_Code_Combination__c){
            lstAddressCountrySett.add(abs);
          }
        }
        mapCountryByAddressCountrySett=new Map<String,BI_TM_Address_BrickCountrySettings__c>();
        mapCodebyPostalCode=new Map<String,BI_TM_PostalCode__c>();
        strQuery='select id,OK_BRICK_NUMBER_2_BI__c, Country_Code_BI__c, OK_BRICK_NUMBER_3_BI__c,OK_BRICK_NUMBER_4_BI__c,OK_BRICK_NUMBER_7_BI__c,Zip_vod__c from Address_vod__c where (BI_TM_Apply_Dynamic_Address_Update__c = FALSE AND BI_TM_Apply_City_Postal_Code_Combination__c = FALSE) AND zip_vod__c!=null and Country_Code_BI__c IN (';
        Integer intCont=1;
        Integer intSize=lstAddressCountrySett.size();
        for(BI_TM_Address_BrickCountrySettings__c objAddressSettings : lstAddressCountrySett)
        {
            if(intCont==intSize)
            {
                strQuery+='\''+objAddressSettings.BI_TM_CountryCode__c+'\'';
            }
            else
            {
                strQuery+='\''+objAddressSettings.BI_TM_CountryCode__c+'\',';
            }
            intCont++;
            mapCountryByAddressCountrySett.put(objAddressSettings.BI_TM_CountryCode__c,objAddressSettings);
        }

        strQuery+=')';

        System.debug('... strQuery '+strQuery);
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(strQuery);
    }

  global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Address_vod__c> lstAddressToUpdate=new List<Address_vod__c>();
        Map<String,BI_TM_PostalCode__c>mapPostalCodeByPostalCodeobj=new Map<String,BI_TM_PostalCode__c>();
        Set<String> SetCodes=new Set<String>();
        for(sobject objScope:scope)
        {
            Address_vod__c objAddress=(Address_vod__c) objScope;
            BI_TM_Address_BrickCountrySettings__c objSett=mapCountryByAddressCountrySett.get(objAddress.Country_Code_BI__c);
            String strDelimeterChar=' ';
            String strPostalCode;
            if(objSett.BI_TM_Characters__c!=null)
            {
                strDelimeterChar=objSett.BI_TM_Characters__c;
            }
            if(objAddress.Zip_vod__c!=null)
            {
                List<String> lstSplitZip=objAddress.Zip_vod__c.split(strDelimeterChar);

                if(objSett.BI_TM_ApplyOnlyBrick__c)
                {
                    strPostalCode=lstSplitZip.get(0);
                }
                else
                {
                    strPostalCode=objAddress.Zip_vod__c;
                }
                 if(strPostalCode!=null&&!SetCodes.contains(strPostalCode))
                {
                    SetCodes.add(strPostalCode);
                }
            }
        }
        List<BI_TM_PostalCode__c> lstPostalCode=[Select id,BI_TM_Brick__c,name,BI_TM_Attr1__c,BI_TM_Attr2__c,BI_TM_Attr3__c,
                                                                                     BI_TM_Attr4__c,BI_TM_Country_Code__c
                                                                                     from BI_TM_PostalCode__c
                                                                                     where BI_TM_Country_Code__c =:mapCountryByAddressCountrySett.keySet() and name=:setCodes ];
        System.debug('... lstPostalCode '+lstPostalCode);
        for(BI_TM_PostalCode__c objPostalCode:lstPostalCode)
        {

                if(!mapCodebyPostalCode.containsKey(objPostalCode.BI_TM_Country_Code__c+objPostalCode.name))
                {
                    mapCodebyPostalCode.put(objPostalCode.BI_TM_Country_Code__c+objPostalCode.name,objPostalCode);
                }

        }
        for(sobject objScope:scope)
        {
            Address_vod__c objAddress=(Address_vod__c) objScope;
            BI_TM_Address_BrickCountrySettings__c objSett=mapCountryByAddressCountrySett.get(objAddress.Country_Code_BI__c);
            String strDelimeterChar=' ';
            if(objSett.BI_TM_Characters__c!=null)
            {
                strDelimeterChar=objSett.BI_TM_Characters__c;
            }
            if(objAddress.Zip_vod__c!=null)
            {
                List<String> lstSplitZip=objAddress.Zip_vod__c.split(strDelimeterChar);
                String strPostalCode;
                if(objSett.BI_TM_ApplyOnlyBrick__c)
                {
                    strPostalCode=objAddress.Country_Code_BI__c+lstSplitZip.get(0);
                }
                else
                {
                    strPostalCode=objAddress.Country_Code_BI__c+objAddress.Zip_vod__c;
                }
                if(mapCodebyPostalCode.containsKey(strPostalCode))
                {
                    populateAddressField(objAddress,mapCodebyPostalCode.get(strPostalCode),objSett);
                    lstAddressToUpdate.add(objAddress);
                }
                else
                {
                    if(!mapPostalCodeByPostalCodeobj.containsKey(strPostalCode)&&objSett.BI_TM_ApplyPostalCodeCreate__c )
                    {
                         BI_TM_PostalCode__c objPostalCode=new BI_TM_PostalCode__c();
                         objPostalCode.name=objAddress.zip_vod__c;
                         objPostalCode.BI_TM_Country_Code__c=objAddress.Country_Code_BI__c;
                         mapPostalCodeByPostalCodeobj.put(strPostalCode,objPostalCode);
                    }
                }
            }
        }
        database.insert(mapPostalCodeByPostalCodeobj.values());
        for(BI_TM_PostalCode__c objPostalCode:mapPostalCodeByPostalCodeobj.values())
        {
            if(mapCountryByAddresswithoutBrick.containsKey(objPostalCode.BI_TM_Country_Code__c))
            {
                mapCountryByAddresswithoutBrick.get(objPostalCode.BI_TM_Country_Code__c).add(objPostalCode.id);
            }
            else
            {
                List<String> lstAddresswithoutBrick=new List<String>();
                lstAddresswithoutBrick.add(objPostalCode.id);
                mapCountryByAddresswithoutBrick.put(objPostalCode.BI_TM_Country_Code__c,lstAddresswithoutBrick);
            }
        }
        database.update(lstAddressToUpdate);
    }
    public void populateAddressField(Address_vod__c objAddress,BI_TM_PostalCode__c objGeo,BI_TM_Address_BrickCountrySettings__c objSett)
    {
        if(objSett.BI_TM_GeoField1__c!=null&&objSett.BI_TM_AddressField1__c!=null)
        {
            String strGeoValue=(String)objGeo.get(objSett.BI_TM_GeoField1__c);
            if(strGeoValue!=null)
            {
                objAddress.put(objSett.BI_TM_AddressField1__c,strGeoValue);
            }
        }
        if(objSett.BI_TM_GeoField2__c!=null&&objSett.BI_TM_AddressField2__c!=null)
        {
            String strGeoValue=(String)objGeo.get(objSett.BI_TM_GeoField2__c);
            if(strGeoValue!=null)
            {
                objAddress.put(objSett.BI_TM_AddressField2__c,strGeoValue);
            }
        }
        if(objSett.BI_TM_GeoField3__c!=null&&objSett.BI_TM_AddressField3__c!=null)
        {
            String strGeoValue=(String)objGeo.get(objSett.BI_TM_GeoField3__c);
            if(strGeoValue!=null)
            {
                objAddress.put(objSett.BI_TM_AddressField3__c,strGeoValue);
            }
        }
        if(objSett.BI_TM_GeoField4__c!=null&&objSett.BI_TM_AddressField4__c!=null)
        {
            String strGeoValue=(String)objGeo.get(objSett.BI_TM_GeoField4__c);
            if(strGeoValue!=null)
            {
                objAddress.put(objSett.BI_TM_AddressField4__c,strGeoValue);
            }
        }
    }

    global void execute(SchedulableContext sc) {
  		database.executebatch(new BI_TM_AddressUpdates_batch());

  	}

    global void finish(Database.BatchableContext BC)
    {
        System.debug(mapCountryByAddresswithoutBrick);
        List<BI_TM_Address_BrickCountrySettings__c> lstAddressCountrySett=BI_TM_Address_BrickCountrySettings__c.getAll().values();
        Map<String,List<String>> mapCountryCodeByEmail=new Map<String,List<String>>();
        for(BI_TM_Address_BrickCountrySettings__c objSett: lstAddressCountrySett)
        {
            if(objSett.BI_TM_Email__c!=null)
            {
                List<String> lstEmails=objSett.BI_TM_Email__c.split(';');
                mapCountryCodeByEmail.put(objSett.BI_TM_CountryCode__c,lstEmails);
            }
        }
        List<Messaging.SingleEmailMessage> lstMails =new List<Messaging.SingleEmailMessage>();
        String strBody;
        for(String strCountryCode:mapCountryCodeByEmail.keySet())
        {
            List<String> lstPostalCodesIds=mapCountryByAddresswithoutBrick.get(strCountryCode);
            if(mapCountryCodeByEmail.get(strCountryCode)!=null)
            {
                Messaging.SingleEmailMessage objMail = new Messaging.SingleEmailMessage();
                objMail.setToAddresses(mapCountryCodeByEmail.get(strCountryCode));
                objMail.setSubject('BITMAN Address Update');
                strBody='<html><p><span style="color: #000000;">Dear,</span></p> ';
                strBody+='<p><span style="color: #000000;">The next postal codes were created please review and update with their respective Brick.</span></p>';
                strBody+='<p>&nbsp;</p>';
                if(lstPostalCodesIds!=null)
                {
                    for(String strPostalCodeId:lstPostalCodesIds)
                    {
                        strBody+='<a title="url1" href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+strPostalCodeId+'">'+strPostalCodeId+'</a></span></p>';
                        strBody+='<p></p>';
                    }
                }

                strBody+='<p><span style="color: #000000;">Best Regards,</span></p>';
                strBody+='<p><span style="color: #000000;">BITMAN TEAM</span></p>';
                system.debug(strBody);
                objMail.setHtmlBody(strBody);
                lstMails.add(objMail);
            }
        }
        Messaging.sendEmail(lstMails);
    }

}