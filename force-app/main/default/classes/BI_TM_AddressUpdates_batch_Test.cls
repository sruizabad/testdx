@isTest
private class BI_TM_AddressUpdates_batch_Test {

    @isTest
	static void method01()
	{
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		Account acc = new Account(Name = 'Test Account', Country_Code_BI__c = 'CA', recordTypeId = getRecTypeId('Account', 'AH_Pharmacy'));
		System.runAs(thisUser){
			Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
			insert ks;
			insert acc;
			Address_vod__c addr = new Address_vod__c(Name = 'Test Address', Account_vod__c = acc.Id, Zip_vod__c = 'ZIP01', City_vod__c = 'CITY01', Country_Code_BI__c = 'CA');
			insert addr;
            Address_vod__c addr1 = new Address_vod__c(Name = 'Test Address', Account_vod__c = acc.Id, Zip_vod__c = 'ZIP02', City_vod__c = 'CITY01', Country_Code_BI__c = 'CA');
			insert addr1;
			System.debug('*** BI_TM_AddressUpdates_batch_Test address: ' + addr);
			BI_TM_Address_BrickCountrySettings__c abcs = new BI_TM_Address_BrickCountrySettings__c(Name = 'Test ABCS CA', BI_TM_CountryCode__c = 'US', BI_TM_Record_Types__c = '\'AH Pharmacy\'', BI_TM_Apply_City_Postal_Code_Combination__c = false, BI_TM_GeoField1__c='12345',BI_TM_Email__c='miguel.artiach.ext@boehringer-ingelheim.com');
			BI_TM_Address_BrickCountrySettings__c abcs1 = new BI_TM_Address_BrickCountrySettings__c(Name = 'Test ABCS CA 2', BI_TM_CountryCode__c = 'CA',BI_TM_GeoField1__c='BI_TM_Attr1__c',BI_TM_Email__c='miguel.artiach.ext@boehringer-ingelheim.com',BI_TM_ApplyPostalCodeCreate__c=true,BI_TM_AddressField1__c='Brick_vod__c',BI_TM_AddressField2__c='OK_BRICK_NUMBER_3_BI__c',BI_TM_GeoField2__c='BI_TM_Attr2__c',BI_TM_GeoField3__c='BI_TM_AddressField3__c',BI_TM_GeoField4__c='BI_TM_Attr4__c');
			insert abcs;
			insert abcs1;
            System.debug('*** BI_TM_AddressUpdates_batch_Test abcs: ' + abcs);
            System.debug('*** BI_TM_AddressUpdates_batch_Test abcs1: ' + abcs1);
			BI_TM_PostalCode__c pc = new BI_TM_PostalCode__c(Name = 'ZIP01', BI_TM_Attr1__c = 'BRICK01', BI_TM_Attr2__c = 'BRNAME01',BI_TM_Attr4__c = 'BRNAME04', BI_TM_Country_Code__c = 'CA');
			insert pc;
			Customer_Attribute_BI__c ca = new Customer_Attribute_BI__c(Name = 'BRNAME01', Type_BI__c = 'ADDR_Brick Name', recordTypeId = getRecTypeId('Customer_Attribute_BI__c', 'OK_Local_Attribute'), Country_Code_BI__c = 'CA');
			insert ca;
            System.debug('*** BI_TM_AddressUpdates_batch_Test pc: ' + pc);
		}

        Test.startTest();
		database.executeBatch(new BI_TM_AddressUpdates_batch());
		System.schedule('Test schedule', '0 0 23 * * ?', new BI_TM_AddressUpdates_batch());
		Test.stopTest();

    }
    	private static Id getRecTypeId(String obj, String recTypeName){
		return [SELECT Id FROM recordType WHERE DeveloperName = :recTypeName AND SObjectType = :obj].Id;
        }    
}