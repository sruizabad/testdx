/******************************************************************************** 
Name:  BI_TM_AddressVodHandler 
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
apex Handler to update bricks based on postal codes from TM Postal codes object
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Rao G               19/11/2015   INITIAL DEVELOPMENT
*********************************************************************************/
public with sharing class BI_TM_AddressVodHandler {

    public static Boolean isUpdateBricksExecuted = false;   
    public static void updateBricksByPostalCode(List<Address_vod__c> recsToProcess, Set<String> countries, Set<String> postalCodes){
        List<String> postalCodesList = new List<String>(postalCodes);
        List<String> countriesList = new List<String>(countries);
        system.debug('postalCodesList $$$$$:' + postalCodesList);
        system.debug('countriesList $$$$$:' + countriesList);
        
        Map<String,String> postCodeBrickMap = new Map<String,String>();
        try{            
            List<BI_TM_PostalCode__c> pCodes = [select BI_TM_Brick__c, Id, Name, BI_TM_Country_Code__c from BI_TM_PostalCode__c where BI_TM_Country_Code__c=:countriesList and Name =:postalCodesList];
            system.debug('pCodes $$$$$$$$$$:' + pCodes);
            for(BI_TM_PostalCode__c postCode : pCodes){
                postCodeBrickMap.put(postCode.Name, postCode.BI_TM_Brick__c);
            }
            for(Address_vod__c rec : recsToProcess){
                rec.Brick_vod__c = postCodeBrickMap.get(rec.Zip_vod__c);
            }   
            update recsToProcess;       
            isUpdateBricksExecuted = true;
        }catch(Exception ex){
            system.debug('ex $$$$$$$$$$$$:'+ ex);
        }
    }
    

}