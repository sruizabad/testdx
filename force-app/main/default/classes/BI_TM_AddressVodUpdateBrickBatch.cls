/******************************************************************************** 
Name:  BI_TM_AddressVodUpdateBrickBatch 
Copyright � 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Batch apex to update bricks based on postal codes from TM Postal codes object
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Rao G               19/11/2015   INITIAL DEVELOPMENT
*********************************************************************************/
global class BI_TM_AddressVodUpdateBrickBatch implements Database.batchable<sObject> {
	public String Query;
	List<Address_vod__c> finalRecsToBeProcessed = null;
	global Database.QueryLocator Start(Database.BatchableContext info)
    {
    	String countriesToProcess = Label.BI_TM_AddressVod_Trigger_Countries;
		List < String > countries = new List < String > (countriesToProcess.split(','));
		String countryStr = convertListToString(countries);
		system.debug(' countryStr $$$$$$$$$$$:' + countryStr);
		
		// fetch Address_vod__c records ready for updating brick value if empty.
        Query = 'Select id,Country_Code_BI__c,Brick_vod__c,Zip_vod__c from Address_vod__c where Country_Code_BI__c in '+countryStr+' and Zip_vod__c != \'\' and Brick_vod__c=\'\'';
        return Database.getQueryLocator(query);        
    }
	global void Execute(Database.BatchableContext info, List<Address_vod__c> scope)
    {
    	system.debug('scope ###########:' + scope);
    	
    	finalRecsToBeProcessed = new List<Address_vod__c>();
		String countriesToProcess = Label.BI_TM_AddressVod_Trigger_Countries;
		Set < String > countries = new Set < String > (countriesToProcess.split(','));
		Set<String> finalCountries = new Set<String>();
		Set<String> postCodes = new Set<String>();
		
		for(Address_vod__c addVodRec : scope){
			if( countries.contains(addVodRec.Country_Code_BI__c) && String.isBlank(addVodRec.Brick_vod__c) ){
				finalRecsToBeProcessed.add(addVodRec);
				finalCountries.add(addVodRec.Country_Code_BI__c);
				postCodes.add(addVodRec.Zip_vod__c);
			}
		}
		system.debug('finalRecsToBeProcessed $$$$$: ' + finalRecsToBeProcessed);
		if( finalRecsToBeProcessed != null && finalRecsToBeProcessed.size() > 0 && !(BI_TM_AddressVodHandler.isUpdateBricksExecuted) ){
			BI_TM_AddressVodHandler.updateBricksByPostalCode(finalRecsToBeProcessed, finalCountries, postCodes);
		}
		
    }
    global void Finish(Database.BatchableContext info){ 
                 
    }
    
    public static String convertListToString(List<String> cCodes) {
        String str = '(';
        for (String s: cCodes) {
            str += '\'' + s + '\',';
        }

        if(cCodes != null && cCodes.size() > 0){
            str = str.substring(0, str.length() - 1);
            return str + ')';
         }else{
            return '(\'\')'; 
         }
    }
}