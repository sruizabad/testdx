/******************************************************************************** 
Name:  BI_TM_AddressVodUpdateBrickSched 
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Scheduled apex to update bricks based on postal codes from TM Postal codes object
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Rao G               23/11/2015   INITIAL DEVELOPMENT
*********************************************************************************/
global class BI_TM_AddressVodUpdateBrickSched implements Schedulable {
	global void execute(SchedulableContext sc)
    {
        BI_TM_AddressVodUpdateBrickBatch batchObj = new BI_TM_AddressVodUpdateBrickBatch();
		database.executebatch(batchObj, 2000); 
    }

}