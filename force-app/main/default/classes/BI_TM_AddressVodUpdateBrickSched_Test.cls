@isTest(seeAllData = false)
public class BI_TM_AddressVodUpdateBrickSched_Test {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void testBrickUpdate() {
      createTestData();

      Test.startTest();

      // Schedule the test job
      String jobId = System.schedule('BI_TM_AddressVodUpdateBrickSched_Test',
                        CRON_EXP, 
                        new BI_TM_AddressVodUpdateBrickSched());
         
      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

      // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
         ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

      // Verify the next time the job will run
      System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));

      Test.stopTest();

   }
   
   static void createTestData(){
        
        BI_TM_Geography_type__c gtype = new BI_TM_Geography_type__c(Name='Brick',BI_TM_Country_Code__c='CA');
        insert gtype;
        BI_TM_Geography__c geoG = new BI_TM_Geography__c(Name='GX9',BI_TM_Is_Active__c=true,BI_TM_Geography_type__c=gtype.Id,BI_TM_Country_Code__c='CA');
        insert geoG;
        BI_TM_PostalCode__c pc = new BI_TM_PostalCode__c(Name='GX9 134',BI_TM_Geography__c=geoG.Id,BI_TM_Country_Code__c='CA');
        insert pc;
        Knipper_Settings__c ks = new Knipper_Settings__c(Account_Detect_Change_FieldList__c='FirstName,Middle_vod__c',Account_Detect_Changes_Record_Type_List__c='Professional_vod',External_ID__c='1',setupOwnerId = System.Userinfo.getOrganizationId());
        insert ks;
        Account acc = new Account(Name='Test 11');
        insert acc;
        Address_vod__c avod1 = new Address_vod__c(Name='test add',Zip_vod__c='GX9 134',Account_vod__c=acc.Id,Country_Code_BI__c='CA');
        insert avod1;
        Address_vod__c avod2 = new Address_vod__c(Name='test add1',Zip_vod__c='GX9 124',Account_vod__c=acc.Id,Country_Code_BI__c='MX');
        insert avod2;

   }
}