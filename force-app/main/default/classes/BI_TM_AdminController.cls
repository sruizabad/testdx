public class BI_TM_AdminController {


    @RemoteAction
    public static void processTerritoriesVeeva() {
        BI_TM_ProcessTerritoryHierarchyBatch batchable = new BI_TM_ProcessTerritoryHierarchyBatch();
        database.executeBatch(batchable);
    }

/*
    @RemoteAction
    public static void processRolesVeeva() {
        BI_TM_ProcessRoleHierarchyBatch batchable = new BI_TM_ProcessRoleHierarchyBatch();
        database.executeBatch(batchable);
    }

    @RemoteAction
    public static void processUsersInBITMAN() {
        BI_TM_BatchToUpdtelstloginndenddateSched s = new BI_TM_BatchToUpdtelstloginndenddateSched();
        s.execute(null);
    }

    @RemoteAction
    public static String processUsersInactiveVeeva() {
        // activate and inactivate the user in Veeva
        try{
        //BI_TM_Activateuser_Scheduled s = new BI_TM_Activateuser_Scheduled();
        BI_TM_Process_Inactive_Users_Veeva s= new BI_TM_Process_Inactive_Users_Veeva();
        //s.execute(null);
        database.executeBatch(s);
        //s.InactivateUsers();

        system.debug('entro a users in veeva');
        //s.activateUsersRefactor();
        }catch(Exception ex){
          return ex.getMessage();

        }
        return 'OK';
    }
    @RemoteAction
    public static String processUsersActiveVeeva() {
        // activate and inactivate the user in Veeva
        try{
        //BI_TM_Activateuser_Scheduled s = new BI_TM_Activateuser_Scheduled();
        BI_TM_Process_Active_Users_Veeva s= new BI_TM_Process_Active_Users_Veeva();
        database.executeBatch(s);
        //s.execute(null);
        //s.InactivateUsers();
        system.debug('entro a users in veeva');
        //s.activateUsersRefactor();
        }catch(Exception ex){
          return ex.getMessage();

        }
        return 'OK';
    }

    @RemoteAction
    public static void processUserToTerritory() {
        // assign and remove the Territory to User in Veeva
        BI_TM_ActivateUserTerritory_Scheduled s = new BI_TM_ActivateUserTerritory_Scheduled();
        s.execute(null);
    }
    /*
    @RemoteAction
    public static void processUserToProducts() {
        // process user to product assignments
        BI_TM_ProcessProducts batchable = new BI_TM_ProcessProducts();
        database.executeBatch(batchable, 1);
    }

     @RemoteAction
    public static void processInactivePositions() {
        // inactivate user to Position assignments when a Position becomes inactive
        //BI_TM_InactivateUserTerritory_Scheduled s = new BI_TM_InactivateUserTerritory_Scheduled();
        BI_TM_UpdteEndDateforTerrAsgnBatchSched s = new BI_TM_UpdteEndDateforTerrAsgnBatchSched();
        s.execute(null);
    }

    @RemoteAction
    public static void processInactiveTerritories() {
        // End Date Geo to Terr and Terr to Prod when Territory is Inactivated

            BI_TM_UpdtEndDateGeotoTerrAsgnBatchSchd s = new BI_TM_UpdtEndDateGeotoTerrAsgnBatchSchd();
        s.execute(null);
    }

    @RemoteAction
    public static void processUserAssignments() {
        // End date user assignments when user is deactivated
        //BI_TM_UpdteEndDateforUsrAsgnmsbatchSched s = new BI_TM_UpdteEndDateforUsrAsgnmsbatchSched();
        //s.execute(null);

    }


    @RemoteAction
    public static void processProductAssignments() {
        // End date User to Product and Territory to Product when Product is deactivated in BITMAN
        BI_TM_UpdteEndDateforProdAsgnBatchSched s = new BI_TM_UpdteEndDateforProdAsgnBatchSched();
        s.execute(null);
    }

     @RemoteAction
    public static void processChangeRequest() {
        // End date User to Product and Territory to Product when Product is deactivated in BITMAN
        BI_TM_User_to_Pos_ChangeReq_Sch s = new BI_TM_User_to_Pos_ChangeReq_Sch();
        s.execute(null);
    }
 */

}