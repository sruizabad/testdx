/********************************************************************************
Name:  BI_TM_AlignmentCopyBatch
Copyright ? 2018  Omega CRM
=================================================================
=================================================================
Batch to copy the child records for a recently new copied alignment
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    S. ruiz             16/05/2018   INITIAL DEVELOPMENT
*********************************************************************************/
global class BI_TM_AlignmentCopyBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    public final static String GEO2TER_COPY_TYPE = 'Geo2Terr';
    public final static String POSREL_COPY_TYPE = 'PosRel';

    private final static String GEO2TER_OBJECT_NAME = 'Geography to territories';
    private final static String POSREL_OBJECT_NAME = 'Position relations';

    private final static Integer errorsShown = 20;

    String query;
    
    private Id currentRecordId = null;
    private Id newRecordId = null;
    private String copyType = null; 

    private String fieldName = null;
    private String objectName = null;

    global Integer recordCount = 0;
    global Integer successfulRecordCount = 0;

    global List<String> errorRecords = new List<String>();

    /**
     * BI_TM_AlignmentCopyBatch
     *
     * Constructor.
     * Creates a new BI_TM_AlignmentCopyBatch object.
     *
     * @param currentRecordId Id of the copied alignment cycle
     * @param newRecordId Id of the new created alignment cycle
     * @param copyType Type of the copy. It can be 'Geo2Terr' to copy geography to territory or 'PosRel' to copy Position Relation
     */
    global BI_TM_AlignmentCopyBatch(Id currentRecordId, Id newRecordId, String copyType) {
        this.currentRecordId = currentRecordId;
        this.newRecordId = newRecordId;
        this.copyType = copyType;

        this.fieldName = copyType.equals(GEO2TER_COPY_TYPE) ? 'BI_TM_Parent_alignment__c' : copyType.equals(POSREL_COPY_TYPE) ? 'BI_TM_Alignment_Cycle__c' : '';
        this.objectName = copyType.equals(GEO2TER_COPY_TYPE) ? GEO2TER_OBJECT_NAME : copyType.equals(POSREL_COPY_TYPE) ? POSREL_OBJECT_NAME : '';
    }
    
    /**
     * start
     *
     * Creates the query required to retrieved the records, based on copy type.
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if (copyType.equals(GEO2TER_COPY_TYPE))
            query = 'SELECT p.Id, name, p.BI_TM_End_date__c, p.BI_TM_Geography__c, p.BI_TM_Parent_alignment__c, p.BI_TM_Country_Code__c, p.BI_TM_Primary_territory__c, p.BI_TM_Start_date__c, p.BI_TM_Territory_ND__c, BI_TM_Is_Active__c, BI_TM_Business__c FROM BI_TM_Geography_to_territory__c p WHERE BI_TM_Parent_alignment__c = \'' + currentRecordId + '\'';

        if (copyType.equals(POSREL_COPY_TYPE))
            query = 'SELECT p.Id, name,p.BI_TM_Active__c,p.BI_TM_Start_date__c,p.BI_TM_End_date__c,p.BI_TM_Parent_Position__c, p.BI_TM_Position__c,p.BI_TM_Territory__c,p.BI_TM_Business__c,p.BI_TM_Country_Code__c FROM BI_TM_Position_Relation__c p WHERE BI_TM_Alignment_Cycle__c = \'' + currentRecordId + '\'';

        return Database.getQueryLocator(query);
    }

    /**
     * execute
     *
     * Clones each item in scope arg and, for each item, the parent alignment id is set to the new created alignment cycle id
     *
     * @param scope List of records to be cloned
     */
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        List<SObject> items = new List<SObject>();
        for (SObject so : scope) {
            SObject newItem = so.clone(false);
            newItem.put(fieldName, newRecordId);
            items.add(newItem);
        }

        recordCount += scope.size();
        Database.SaveResult[] results = Database.insert(items, false);
        for (Database.SaveResult dsr : results) {
            if (dsr.isSuccess())
                successfulRecordCount++;
            else 
                errorRecords.add('Record has errors: ' + getErrorMessages(dsr.getErrors()));
        }
    }

    private String getErrorMessages (Database.Error [] errors) {
        String msg = '';
        if (errors.size() > 0)
            for (Database.Error error : errors) {
                if (!String.isBlank(msg))
                    msg += '; ';
                msg += error.getMessage();
            }
        return msg;
    }
    
    /**
     * finish
     *
     * Sends email notification to user on successful finish
     */
    global void finish(Database.BatchableContext BC) {
        EmailTemplate et = [SELECT Subject, Body FROM EmailTemplate WHERE DeveloperName = 'BI_TM_AlignmentCopy'];
        String subject = et.Subject.replace('{Object_name}', objectName);
        String body = et.Body.replace('{Object_name}', objectName).
                              replace('{Alignment_name}', [SELECT Name FROM BI_TM_Alignment__c WHERE id = :newRecordId].Name).
                              replace('{Success_records}', String.valueOf(successfulRecordCount)).
                              replace('{Total_records}', String.valueOf(recordCount)).
                              replace('{User_name}', UserInfo.getName()).
                              replace('{Errors}', buildErrorStringForEmail());

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String [] { UserInfo.getUserEmail() });
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        try {
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch (Exception ex) {}
    }   

    private String buildErrorStringForEmail() {
        String errorString = '\n';
        if (errorRecords.size() > 0) {
            errorString += 'There have been the following errors during the copy' + (errorRecords.size() > errorsShown ? ' (only first ' + errorsShown + ' errors shown)' : '') + ':\n';
            for (Integer i=0; i<errorsShown && i<errorRecords.size(); i++)
                errorString += ' - ' + errorRecords.get(i) + '\n';
        }
        return errorString;
    }
}