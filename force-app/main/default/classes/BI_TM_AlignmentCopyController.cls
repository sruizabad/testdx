/********************************************************************************
Name:  BI_TM_AlignmentCopyController
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Alignment to copy the record values along with the Child Records
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran               10/12/2015  INITIAL DEVELOPMENT
2.0      S. Ruiz (Omega CRM) 16/05/2018   Checkboxes in UI for define copy of geo 2 terr and position relation.
                                          Use of batches instead of direct controller logic to avoid query limits
                                          ShowInfoMessage action method
*********************************************************************************/

public class BI_TM_AlignmentCopyController {

    public BI_TM_Alignment__c alignmentLocal {get; set;}
    public BI_TM_Alignment__c cloneAlignment {get; set;}

    public Boolean copyGeo2Ter { get; set; }
    public Boolean copyPosRel { get; set; }

    public ID newRecordId {get; set;}
    public Id recId {get; set;}
    public String currentUserCountryCode {get; set;}

    public BI_TM_AlignmentCopyController(ApexPages.StandardController controller) {
        recId = apexpages.currentpage().getparameters().get('id');

        alignmentLocal = [SELECT Name, BI_TM_End_date__c, BI_TM_Start_date__c, BI_TM_Status__c, BI_TM_FF_type__c, BI_TM_Alignment_Description__c, BI_TM_Country_code__c, BI_TM_Business__c, BI_TM_Include_Previous_Interactions__c, BI_TM_Previous_Months__c, BI_TM_Include_GAS_Assignments__c, BI_TM_Sales1_Status__c, BI_TM_Sales2_Status__c FROM BI_TM_Alignment__c WHERE id = :recId];

        cloneAlignment = new BI_TM_Alignment__c (
            Name = alignmentLocal.name,
            BI_TM_FF_type__c = alignmentLocal.BI_TM_FF_type__c,
            BI_TM_Alignment_Description__c = alignmentLocal.BI_TM_Alignment_Description__c,
            BI_TM_Country_code__c = alignmentLocal.BI_TM_Country_code__c,
            BI_TM_Business__c = alignmentLocal.BI_TM_Business__c,
            BI_TM_Include_Previous_Interactions__c = alignmentLocal.BI_TM_Include_Previous_Interactions__c,
            BI_TM_Previous_Months__c = alignmentLocal.BI_TM_Previous_Months__c,
            BI_TM_Include_GAS_Assignments__c = alignmentLocal.BI_TM_Include_GAS_Assignments__c,
            BI_TM_Sales1_Status__c = alignmentLocal.BI_TM_Sales1_Status__c,
            BI_TM_Sales2_Status__c = alignmentLocal.BI_TM_Sales2_Status__c,
            BI_TM_Status__c='Future'
        );
    }

    /**
     * cloneWithItems
     * 
     * Action method. Clone the alignment cycle and, if checked, copy the childs (Geo2Terr and/or Position Relations). The child copy is realized in a batch.
     * 
     * @return the detail page for the new alignment cycle cloned
     */
    public PageReference cloneWithItems() {
        Savepoint sp = Database.setSavepoint();

        try {
            insert cloneAlignment;

            if (copyGeo2Ter)
                database.executebatch(new BI_TM_AlignmentCopyBatch(recId, cloneAlignment.Id, BI_TM_AlignmentCopyBatch.GEO2TER_COPY_TYPE));

            if (copyPosRel)
                database.executebatch(new BI_TM_AlignmentCopyBatch(recId, cloneAlignment.Id, BI_TM_AlignmentCopyBatch.POSREL_COPY_TYPE));
        } catch (Exception e) {
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
        }

        return new PageReference('/' + cloneAlignment.Id);
    }

    /**
     * showInfoMessage
     *
     * Action method. Shows copy info messages in the page if checkboxs are checked.
     * 
     * @return a null page
     */
    public PageReference showInfoMessage () {
        if (copyGeo2Ter) 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, System.Label.BI_TM_AlignmentCopy_Geo2Ter_Info_Message));

        if (copyPosRel)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, System.Label.BI_TM_AlignmentCopy_PosRel_Info_Message));

        return null;
    }
}