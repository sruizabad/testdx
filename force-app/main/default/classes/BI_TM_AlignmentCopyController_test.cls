@isTest
private class BI_TM_AlignmentCopyController_test {

  BI_TM_Alignment__c newalign= new BI_TM_Alignment__c();
  static testMethod void BI_TM_AlignmentCopyController_Test1()
  {
    User currentuser = new User();
    currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
    BI_TM_FF_type__c FF= new BI_TM_FF_type__c();
    FF.Name='TestFF21';
    FF.BI_TM_Business__c='PM';
    FF.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    System.debug('BI_TM_AlignmentCopyController_test currentuser------------->>>>>'+currentuser);
    insert FF;
    BI_TM_Alignment__c alignment = new BI_TM_Alignment__c();
    alignment.name='MXPRT';
    alignment.BI_TM_FF_type__c=FF.Id;
    alignment.BI_TM_Alignment_Description__c='test';
    alignment.BI_TM_Status__c='Active';
    alignment.BI_TM_End_date__c=system.today();
    alignment.BI_TM_Start_date__c=system.today();
    alignment.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    alignment.BI_TM_Business__c='PM';
    insert alignment;
    BI_TM_Alignment__c alignment1 = new BI_TM_Alignment__c();
    alignment1.name='MXPRT1';
    alignment1.BI_TM_FF_type__c=FF.Id;
    alignment1.BI_TM_Alignment_Description__c='test';
    alignment1.BI_TM_Status__c='Future';
    alignment1.BI_TM_End_date__c=system.today();
    alignment1.BI_TM_Start_date__c=system.today();
    alignment1.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    alignment1.BI_TM_Business__c='PM';
    insert alignment1;

    BI_TM_Geography_type__c geotype =new BI_TM_Geography_type__c();
    geotype.Name='Test';
    geotype.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c;
    geotype.BI_TM_CRM_column__c = 'Brick_vod__c';
    insert geotype ;

    BI_TM_Geography__c geo= new BI_TM_Geography__c();
    geo.Name='Test';
    geo.BI_TM_Geography_type__c=geotype.Id;
    geo.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c;
    insert geo;

    BI_TM_Territory__c Terr= new BI_TM_Territory__c();
    BI_TM_Territory__c Terr1= new BI_TM_Territory__c();

    BI_TM_Position_Type__c pt = new BI_TM_Position_Type__c();
    pt.Name='Test MX FF';
    pt.BI_TM_Business__c='AH';
    pt.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    insert pt;
    

    Terr1.Name='FFHierarchy test21';
    Terr1.BI_TM_Position_Level__c='Country';
    Terr1.BI_TM_Position_Type_Lookup__c=pt.Id;
    Terr1.BI_TM_Is_Root__c=True;
    Terr1.BI_TM_FF_type__c=FF.Id;
    Terr1.BI_TM_Business__c='PM';
    Terr1.BI_TM_Global_Position_Type__c = 'SALES';
    Terr1.BI_TM_Visible_in_crm__c=True;
    //Terr1.BI_TM_Is_Active__c=false;
    Terr1.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
    Terr1.BI_TM_Start_date__c=system.Today();

    Insert Terr1;

    Terr.Name='Test Tam21';
    Terr.BI_TM_FF_type__c=FF.Id;
    Terr.BI_TM_Business__c='PM';
    Terr.BI_TM_Position_Level__c='Country';
    Terr.BI_TM_Position_Type_Lookup__c=pt.Id;
    Terr.BI_TM_Parent_Position__c=Terr1.Id;
    Terr.BI_TM_Global_Position_Type__c = 'SALES';
    Terr.BI_TM_Visible_in_crm__c=True;
    //Terr.BI_TM_Is_Active__c=True;
    Terr.BI_TM_Start_date__c=system.Today();
    Terr.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
    //Terr.BI_TM_End_date__c=system.Today();

    Insert Terr;
    BI_TM_Territory_ND__c newTerr= new BI_TM_Territory_ND__c();
    newTerr.Name='testnewterr';
    newTerr.BI_TM_Field_Force__c=FF.Id;
    newTerr.BI_TM_Start_Date__c=Date.today().addDays(-2);
    newTerr.BI_TM_Business__c='PM';
    newTerr.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    Insert newTerr;

    BI_TM_Geography_to_territory__c geographytoter= new BI_TM_Geography_to_territory__c();
    geographytoter.BI_TM_Parent_alignment__c=alignment1.Id;
    geographytoter.BI_TM_Geography__c= geo.Id;
    geographytoter.BI_TM_Territory_ND__c=newTerr.id;
    geographytoter.BI_TM_Country_Code__c=Terr.BI_TM_Country_Code__c;

    insert geographytoter;

    //Below lines:Line:110-118 are added by Kiran on 26-07-2016 for new changes added in Phase-2 Development
    BI_TM_Position_Relation__c posrel= new BI_TM_Position_Relation__c();
    posrel.BI_TM_Alignment_Cycle__c=alignment1.Id;
    posrel.BI_TM_Territory__c=newTerr.id;
    posrel.BI_TM_Country_Code__c=Terr.BI_TM_Country_Code__c;
    posrel.BI_TM_Business__c=Terr.BI_TM_Business__c;
    posrel.BI_TM_Position__c=Terr1.Id;

    insert posrel;


    BI_TM_Alignment__c newalign= [Select Name, BI_TM_End_date__c, BI_TM_Start_date__c,BI_TM_Status__c,BI_TM_FF_type__c,BI_TM_Alignment_Description__c from BI_TM_Alignment__c where Id=:alignment.Id];
    ApexPages.currentPage().getParameters().put('Id',alignment.Id);

    ApexPages.StandardController con = new ApexPages.StandardController(newalign);
    BI_TM_AlignmentCopyController a = new BI_TM_AlignmentCopyController(con);
    BI_TM_Alignment__c newalign1= [Select Name, BI_TM_End_date__c, BI_TM_Start_date__c,BI_TM_Status__c,BI_TM_FF_type__c,BI_TM_Alignment_Description__c from BI_TM_Alignment__c where Id=:alignment1.Id];
    ApexPages.currentPage().getParameters().put('Id',alignment1.Id);

    ApexPages.StandardController con1 = new ApexPages.StandardController(newalign1);
    BI_TM_AlignmentCopyController a1 = new BI_TM_AlignmentCopyController(con1);
    BI_TM_Alignment__c alignment2 = alignment1.clone();
    alignment2.name ='Change Test';
    a1.cloneAlignment = alignment2;
    PageReference ref2 = a1.cloneWithItems();




    Test.startTest();
    PageReference ref = a1.cloneWithItems();
    PageReference redir = new PageReference('/'+newalign.Id+'/e?retURL=%2F'+newalign.Id);
    //System.assertEquals(ref.getUrl(),redir.getUrl());

    BI_TM_Alignment__c newPO = [select id from BI_TM_Alignment__c where id = :newalign.Id];
    System.assertNotEquals(newPO, null);
    Test.stopTest();
  }


}