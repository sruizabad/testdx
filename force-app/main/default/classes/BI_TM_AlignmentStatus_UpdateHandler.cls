/********************************************************************************
Name:  BI_TM_AlignmentStatus_UpdateHandler
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Alignmet trigger handler to update status of alignments
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Rao G               11/12/2015   INITIAL DEVELOPMENT
*********************************************************************************/
public class BI_TM_AlignmentStatus_UpdateHandler {
	public static Boolean isUpdatePreviousAtiveAlignmentExecuted = false;

	public static void updatePreviousAtiveAlignment(List<String> ffTypesToProcess, Set<Id> currentAllignSet){
		String alignmentPastStatus = Label.BI_TM_Alignment_PastPL_Value;
		String alignmentActiveStatus = Label.BI_TM_Alignment_ActivePL_Value;

		List<BI_TM_Alignment__c> alignmentToDeativate = new List<BI_TM_Alignment__c>();
   	for(BI_TM_Alignment__c alignment :
			[SELECT id, BI_TM_Status__c FROM BI_TM_Alignment__c
				WHERE BI_TM_Status__c = :alignmentActiveStatus AND BI_TM_FF_type__c = :ffTypesToProcess AND Id NOT IN :currentAllignSet]
		){
			alignment.BI_TM_Status__c = alignmentPastStatus;
   		alignmentToDeativate.add(alignment);
   	}
   	system.debug('alignmentToDeativate ########:' + alignmentToDeativate);
		update(alignmentToDeativate);

		isUpdatePreviousAtiveAlignmentExecuted = true;

	}
}