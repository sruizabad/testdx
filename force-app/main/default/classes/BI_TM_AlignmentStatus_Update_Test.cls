@IsTest(seeAllData=false)
public class BI_TM_AlignmentStatus_Update_Test {
    static testMethod void testAlignmentStatusUpdateTrg(){
        System.Test.startTest();

        BI_TM_FF_type__c fftype = new BI_TM_FF_type__c(Name='TESTFF', BI_TM_Country_Code__c = 'US', BI_TM_Business__c = 'PM');
        insert fftype;
        List<BI_TM_Alignment__c> als = new List<BI_TM_Alignment__c>();
        BI_TM_Alignment__c al3 = new BI_TM_Alignment__c(name='test name3',BI_TM_FF_type__c=fftype.id,BI_TM_Status__c='Active',BI_TM_Start_date__c=system.today(), BI_TM_Business__c = 'PM');
        insert al3;
        BI_TM_Alignment__c al1 = new BI_TM_Alignment__c(name='test name1',BI_TM_FF_type__c=fftype.id,BI_TM_Status__c='Future',BI_TM_Start_date__c=system.today()+1, BI_TM_Business__c = 'PM');
        BI_TM_Alignment__c al2 = new BI_TM_Alignment__c(name='test name2',BI_TM_FF_type__c=fftype.id,BI_TM_Status__c='Future',BI_TM_Start_date__c=system.today()+1, BI_TM_Business__c = 'PM');
        als.add(al1);
        als.add(al2);
        insert als;
    }
}