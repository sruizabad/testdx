/******************************************************************************** 
Name:  BI_TM_AlignmentWrapper 
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
wrapper of alignments to sort by end date
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Rao G               08/12/2015   INITIAL DEVELOPMENT
*********************************************************************************/
public class BI_TM_AlignmentWrapper implements Comparable {

    public BI_TM_Alignment__c alignment;
    
    // Constructor
    public BI_TM_AlignmentWrapper(BI_TM_Alignment__c alignment) {
        this.alignment = alignment;
    }
    
    // Compare Alignment based on the BI_TM_End_date__c.
    public Integer compareTo(Object compareTo) {
        // Cast argument to BI_TM_AlignmentWrapper
        BI_TM_AlignmentWrapper compareToAlignment = (BI_TM_AlignmentWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (alignment.BI_TM_End_date__c < compareToAlignment.alignment.BI_TM_End_date__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (alignment.BI_TM_End_date__c > compareToAlignment.alignment.BI_TM_End_date__c) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}