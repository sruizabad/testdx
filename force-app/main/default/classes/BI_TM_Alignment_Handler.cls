public class BI_TM_Alignment_Handler implements BI_TM_ITriggerHandler
{

    public void BeforeInsert(List<SObject> newItems) {
        updateStatus((List<BI_TM_Alignment__c>) newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        updateStatus((List<BI_TM_Alignment__c>) newItems.values());
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {}

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}


    private void updateStatus(List<BI_TM_Alignment__c> newItems) {
        List<String> ffTypesToProcess = new List<String>();
        Set<Id> currentAllignSet = new Set<Id>();
        String alignmentActiveStatus = Label.BI_TM_Alignment_ActivePL_Value;
        for(BI_TM_Alignment__c alRec : newItems){
            if(alRec.BI_TM_Status__c == alignmentActiveStatus ){
                ffTypesToProcess.add(alRec.BI_TM_FF_type__c);

                if (trigger.isUpdate) {
                  currentAllignSet.add(alRec.Id);
                }
            }

        }
        system.debug('ffTypesToProcess ########:');
        system.debug('currentAllign: ' + currentAllignSet);
        if(ffTypesToProcess != null && ffTypesToProcess.size() > 0 && !BI_TM_AlignmentStatus_UpdateHandler.isUpdatePreviousAtiveAlignmentExecuted){
            BI_TM_AlignmentStatus_UpdateHandler.updatePreviousAtiveAlignment(ffTypesToProcess, currentAllignSet);
        }
    }


}