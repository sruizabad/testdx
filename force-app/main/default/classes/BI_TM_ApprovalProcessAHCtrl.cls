public without sharing class BI_TM_ApprovalProcessAHCtrl {

		public Boolean openPeriodStatus {get;set;}

		public BI_TM_ApprovalProcessAHCtrl(){
			List<BI_TM_Change_Request_Conf__c> crConf = [SELECT BI_TM_Active_Period__c FROM BI_TM_Change_Request_Conf__c];
			openPeriodStatus = crConf[0].BI_TM_Active_Period__c;
		}

		// Obtain the values of the custom setting for the field forces
		@RemoteAction
    public static List<BI_TM_US_FieldForce_Map__c> getChangeRequestConf() {
    	List<BI_TM_US_FieldForce_Map__c> usFFMapList = new List<BI_TM_US_FieldForce_Map__c>([SELECT Id, Name, BI_TM_USFF_Name__c, BI_TM_USFF_L1__c, BI_TM_USFF_L2__c, BI_TM_USFF_L3__c, BI_TM_Automatically_Approved__c FROM BI_TM_US_FieldForce_Map__c ORDER BY Name]);
			system.debug('usFFMapList :: ' + usFFMapList);
      return usFFMapList;
    }

		// Function that updates custom setting
		@RemoteAction
		public static Boolean updateCSOpenPeriod(){
			List<BI_TM_Change_Request_Conf__c> crConf = [SELECT Id, BI_TM_Active_Period__c FROM BI_TM_Change_Request_Conf__c];
			Boolean check = crConf[0].BI_TM_Active_Period__c;
			crConf[0].BI_TM_Active_Period__c = !check;
			update crConf[0];

			if (crConf[0].BI_TM_Active_Period__c) {
				BI_TM_Change_Request_Init_Batch initBatch = new BI_TM_Change_Request_Init_Batch();
				database.executebatch(initBatch);
			}
			return crConf[0].BI_TM_Active_Period__c;
		}

		// Funcition that returns the status of the open period for the approval process
		public boolean getOpenPeriodStatus(){
			List<BI_TM_Change_Request_Conf__c> crConf = [SELECT BI_TM_Active_Period__c FROM BI_TM_Change_Request_Conf__c];
			openPeriodStatus = crConf[0].BI_TM_Active_Period__c;
			return openPeriodStatus;
		}


}