@isTest
private class BI_TM_ApprovalProcessAHCtrl_Test
{
	@isTest
	static void itShould()
	{
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs(thisUser){
			List<BI_TM_Change_Request_Conf__c> confList = [SELECT Id FROM BI_TM_Change_Request_Conf__c WHERE Name = 'Default'];
			if(confList.isEmpty()){
				BI_TM_Change_Request_Conf__c conf = new BI_TM_Change_Request_Conf__c(Name = 'Default', BI_TM_Active_Period__c = true);
				insert conf;
			}
			BI_TM_US_FieldForce_Map__c ffUSconf = new BI_TM_US_FieldForce_Map__c(Name = 'Species', BI_TM_USFF_Name__c = 'US-AH-Species', BI_TM_USFF_L1__c = 'Region', BI_TM_USFF_L2__c = 'Zone', BI_TM_USFF_L3__c = 'National');
			insert ffUSconf;
			List<BI_TM_US_FieldForce_Map__c> ffMapList = BI_TM_ApprovalProcessAHCtrl.getChangeRequestConf();
			BI_TM_ApprovalProcessAHCtrl controller = new BI_TM_ApprovalProcessAHCtrl();
			Boolean openPeriod = controller.getOpenPeriodStatus();
			Boolean updateOpenPeriod = BI_TM_ApprovalProcessAHCtrl.updateCSOpenPeriod();
			updateOpenPeriod = BI_TM_ApprovalProcessAHCtrl.updateCSOpenPeriod();
		}

	}
}