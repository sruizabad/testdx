/********************************************************************************
Name:  BI_TM_AssignmentRuleItemCreateExt
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
controller Extension for BI_TM_AssigmentRuleItemCreate page

=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -   Shilpa P             12/18/2015    INITIAL DEVELOPMENT
*********************************************************************************/
public without sharing class BI_TM_AssignmentRuleItemCreateExt {
  Transient List<BI_TM_Assignment_rule__c> ruletype = new List<BI_TM_Assignment_rule__c>();
  // Map<String, RuleTypes__c> countries = RuleTypes__c.getAll();
  Transient List<BI_TM_Rule_Type__c> ruletypeCustom=new List<BI_TM_Rule_Type__c>();
  Transient List<string> lstPickvals=new List<string>();
  Transient List<SelectOption> options;
  Transient public BI_TM_Assignment_Rule_Item__c assRuleItem{get;set;}
  //  List<BI_TM_Assignment_Rule_Item__c> assRuleItem=new List<BI_TM_Assignment_Rule_Item__c>();
  Transient public String assruleItemId {get;set;}
  String queryString ='';
  Transient public String pickvalues{get;set;}
  // List<string> pickvalues=new List<string>();
  Transient List<string> selPickvals=new List<string>();
  Public List<String> leftselected{get;set;}
  Public List<String> rightselected{get;set;}
  //public set<string> leftvalues{get;set;}
  set<string> leftvalues = new set<string>();
  Set<string> rightvalues = new Set<string>();

  Transient Public List<Customer_Attribute_BI__c> lookupvalues{get;set;}
  Transient Public List<Product_Metrics_vod__c> speciesvalues{get;set;}
  Transient Public List<BI_TM_Geography__c> geoAttributes3{get;set;}
  Transient Public List<BI_TM_Assignment_Rule_Item__c> existingValues{get;set;}
  Transient Set<string> existingValuesSet = new Set<string>();
  Transient List<RecordType> recordType=new List<RecordType>();
  // List<SelectOption> options = new List<SelectOption>();
  public BI_TM_AssignmentRuleItemCreateExt(ApexPages.StandardController controller) {
    leftselected = new list<string>();
    rightselected = new list<string>();
    existingValues = new List<BI_TM_Assignment_Rule_Item__c>();
    lookupvalues= new list<Customer_Attribute_BI__c>();
    try{
      assruleItemId = ApexPages.currentPage().getParameters().get('id');
      queryString = ApexPages.currentPage().getparameters().get('assgid');
      ruletype= [select BI_TM_Rule_Type_New__r.Name,Name,BI_TM_Rule_name__r.BI_TM_Country_Code__c,BI_TM_Country_Code__c  from BI_TM_Assignment_rule__c where id=:queryString limit 1];

      if(ruletype.size()>0){
        if(ruletype[0].BI_TM_Rule_Type_New__r.Name!=NULL && ruletype[0].BI_TM_Rule_Type_New__r.Name!='' && ruletype.size()>0)
          ruletypeCustom=[select BI_TM_CRM_Column__c,BI_TM_CRM_Table__c,Name,BI_TM_Country_Code__c from BI_TM_Rule_Type__c where Name=:ruletype[0].BI_TM_Rule_Type_New__r.Name and BI_TM_Country_Code__c=:ruletype[0].BI_TM_Rule_name__r.BI_TM_Country_Code__c  limit 1 ];
      }
      existingValues=[select BI_TM_Value__c from BI_TM_Assignment_Rule_Item__c where BI_TM_Assignment_rule__c=:queryString];
      for(BI_TM_Assignment_Rule_Item__c str:existingValues){
        existingValuesSet.add(str.BI_TM_Value__c);
      }
      if((ruletypeCustom.size()>0) && ((ruletypeCustom[0].BI_TM_CRM_Column__c=='RecordTypeId')||(ruletypeCustom[0].BI_TM_CRM_Column__c=='RecordType'))){
        recordType=[select Name,Id from RecordType where SobjectType=:ruletypeCustom[0].BI_TM_CRM_Table__c];
        for(RecordType rt:recordType)
          leftvalues.add(rt.Name);
        removeExistingValues(existingValues, leftvalues);
      }
      Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
      if(ruletypeCustom.size()>0 && ruletypeCustom[0].BI_TM_CRM_Table__c!=NULL){
        Schema.SObjectType leadSchema = schemaMap.get(ruletypeCustom[0].BI_TM_CRM_Table__c);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        if(ruletypeCustom.size()>0 && ruletypeCustom[0].BI_TM_CRM_Column__c!=NULL && ruletypeCustom[0].BI_TM_CRM_Column__c!='RecordType' && ruletypeCustom[0].BI_TM_CRM_Column__c!='RecordTypeId' ){
          Schema.DisplayType fielddataType = fieldMap.get(ruletypeCustom[0].BI_TM_CRM_Column__c).getDescribe().getType();
          boolean formulatype=fieldMap.get(ruletypeCustom[0].BI_TM_CRM_Column__c).getDescribe().isCalculated();
          boolean lukuptype=fieldMap.get(ruletypeCustom[0].BI_TM_CRM_Column__c).getDescribe().isIdLookup();
          if(fielddataType ==Schema.DisplayType.Picklist)
          {
            List<Schema.PicklistEntry> pick_list_values = fieldMap.get(ruletypeCustom[0].BI_TM_CRM_Column__c).getDescribe().getPickListValues();
            for (Schema.PicklistEntry a : pick_list_values)
              leftvalues.add(a.getValue());
            removeExistingValues(existingValues, leftvalues);
          }
          // Chgeck if the field is a multipicklist
          else if(fielddataType ==Schema.DisplayType.MultiPicklist){
            List<Schema.PicklistEntry> pick_list_values = fieldMap.get(ruletypeCustom[0].BI_TM_CRM_Column__c).getDescribe().getPickListValues();
            List<String> results = new List<String>();
            for (Integer i = 0; i < pick_list_values.size(); i++){
              Integer resultsLength = results.size();
              for (Integer j = 0; j < resultsLength; j++) {
                results.add(results.get(j) + ';' + pick_list_values[i].getValue() );
              }
              results.add(pick_list_values[i].getValue());
            }
            Set<String> availableValues = new Set<String> (results);
            leftvalues = availableValues;
            removeExistingValues(existingValues, leftvalues);
          }

          if(fielddataType ==Schema.DisplayType.Boolean)
          {
            leftvalues.add('0');
            leftvalues.add('1');
            removeExistingValues(existingValues, leftvalues);
          }
          String country=ruletype[0].BI_TM_Country_Code__c;
          String ruleTypeCountry=ruletypeCustom[0].BI_TM_Country_Code__c;
          String columnName=ruletypeCustom[0].BI_TM_CRM_Column__c;
          string objectName=ruletypeCustom[0].BI_TM_CRM_Table__c;
          if((formulatype == True || columnName=='Production_Type_BI__c') && ruletypeCustom[0].BI_TM_CRM_Table__c=='Account')
          {
            List<Account> acclst =Database.query('select '+columnName +' from Account where Country_Code_BI__c =:country and ' + columnName + ' != \'\' LIMIT 50000');
            for(account acc:acclst)
              leftvalues.add((String)acc.get(columnName));

            removeExistingValues(existingValues, leftvalues);
          }
          //start Added for BR requirement
          if(formulatype == True && columnName=='SAP_Territory_BI__c' )
          {
            List<Cycle_Plan_Target_vod__c> terrBI =Database.query('select '+columnName +' from Cycle_Plan_Target_vod__c where  Country_Code_BI__c =:country and ' + columnName + ' != \'\'');
            for(Cycle_Plan_Target_vod__c terr:terrBI )
              leftvalues.add((String)terr.get(columnName));

            removeExistingValues(existingValues, leftvalues);
          }
          //End
          if(fielddataType!=Schema.DisplayType.Picklist && fielddataType !=Schema.DisplayType.Boolean && formulatype!=true && fielddataType==Schema.DisplayType.REFERENCE && objectName=='Account' && ruletypeCustom[0].BI_TM_CRM_Column__c!='RecordTypeId'){
            lookupvalues=Database.query('SELECT NAME FROM Customer_Attribute_BI__c where ID IN (select '+ruletypeCustom[0].BI_TM_CRM_Column__c +' from '+ objectName +' where Country_Code_BI__c =:country AND '+ruletypeCustom[0].BI_TM_CRM_Column__c +' != NULL) order by Name LIMIT 50000');
            for(Customer_Attribute_BI__c cuAtt:lookupvalues){
              leftvalues.add(cuAtt.NAME);
            }
            removeExistingValues(existingValues, leftvalues);
          }
          if(fielddataType!=Schema.DisplayType.Picklist && fielddataType !=Schema.DisplayType.Boolean && formulatype!=true && fielddataType==Schema.DisplayType.REFERENCE && objectName=='Child_Account_vod__c' && ruletypeCustom[0].BI_TM_CRM_Column__c!='RecordTypeId'){
            lookupvalues=Database.query('SELECT NAME FROM Customer_Attribute_BI__c where ID IN (select '+ruletypeCustom[0].BI_TM_CRM_Column__c +' from '+ objectName +' where Child_Account_vod__r.Country_Code_BI__c =:country AND '+ruletypeCustom[0].BI_TM_CRM_Column__c +' != NULL) order by Name LIMIT 50000');
            for(Customer_Attribute_BI__c cuAtt:lookupvalues){
              leftvalues.add(cuAtt.NAME);
            }
            removeExistingValues(existingValues, leftvalues);
          }
          if((ruletypeCustom[0].BI_TM_CRM_Column__c).toUpperCase() == 'CUSTOMER_CLASSIFICATION_BI__C'){

            for(AggregateResult ar : [SELECT Customer_classification_BI__c FROM Account
            WHERE Country_Code_BI__c = :ruleTypeCountry GROUP BY Customer_Classification_BI__c LIMIT 50000]){
              if(ar.get('Customer_classification_BI__c') != null) leftvalues.add((String)ar.get('Customer_classification_BI__c'));
            }
            removeExistingValues(existingValues, leftvalues);
          }

          if((ruletypeCustom[0].BI_TM_CRM_Column__c).toUpperCase() == 'CHANNEL_BI__C'){

            for(Account acc : [SELECT Channel_BI__c FROM Account
            WHERE Country_Code_BI__c = :ruleTypeCountry AND Channel_BI__c != null LIMIT 50000]){
              leftvalues.add(acc.Channel_BI__c);
            }
            for(BI_TM_Assignment_Rule_Item__c str : existingValues)
            {
              leftvalues.remove(str.BI_TM_Value__c);
              //rightvalues.add(str.BI_TM_Value__c);
            }
          }

          //AH change
          string type='Detail Group';
          if(fielddataType!=Schema.DisplayType.Picklist && fielddataType !=Schema.DisplayType.Boolean && formulatype!=true && fielddataType==Schema.DisplayType.REFERENCE && objectName=='Product_Metrics_vod__c' && ruletypeCustom[0].BI_TM_CRM_Column__c=='products_vod__c'){
            speciesvalues=Database.query('SELECT Products_vod__r.name,Account_vod__r.Country_Code_BI__c FROM Product_Metrics_vod__c where Products_vod__r.Product_Type_vod__c=:type and Account_vod__r.Country_Code_BI__c =:ruleTypeCountry LIMIT 50000');
            system.debug('speciesvalues ---->>'+speciesvalues);
            for(Product_Metrics_vod__c cuAtt:speciesvalues){
              leftvalues.add(cuAtt.Products_vod__r.name);
            }
            removeExistingValues(existingValues, leftvalues);
          }
          if(objectName=='BI_TM_Geography__c' && ruletypeCustom[0].BI_TM_CRM_Column__c=='BI_TM_Attribute_3__c'){
            String query = 'SELECT BI_TM_Attribute_3__c FROM BI_TM_Geography__c WHERE BI_TM_Country_Code__c = \'';
            query += ruleTypeCountry;
            query += '\'';
            query += ' LIMIT 50000';
            geoAttributes3 = Database.query(query);

            for(BI_TM_Geography__c geo : geoAttributes3){
              leftvalues.add(geo.BI_TM_Attribute_3__c);
            }
            removeExistingValues(existingValues, leftvalues);
          }
        }

      }
    }
    catch (Exception e) {
      ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An Error has occurred');
      ApexPages.addMessage(errormsg);

    }
  }

  public List<SelectOption> getvalues()
  {

    List<SelectOption> options = new List<SelectOption>();
    Transient List<string> tempList = new List<String>();
    tempList.addAll(leftvalues);
    tempList.sort();
    for(string s : tempList)
      options.add(new SelectOption(s,s));

    return options;
  }

  public PageReference  Save()
  {
    list<BI_TM_Assignment_Rule_Item__c > newlist= new list<BI_TM_Assignment_Rule_Item__c >();

    if(rightvalues.size()!=0 || !rightvalues.isEmpty()){
      for(string s:rightvalues)
      {
        assRuleItem = new BI_TM_Assignment_Rule_Item__c ();
        assRuleItem.BI_TM_Assignment_rule__c=queryString;
        assRuleItem.BI_TM_Value__c=s;
        assRuleItem.BI_TM_Record_status__c=True;
        newlist.add(assRuleItem);
      }
    }
    try{
      if(newlist.size()!=0 || !newlist.isEmpty())
      Insert newlist;
    }
    catch (Exception e) {
      ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An Error has occurred');
      ApexPages.addMessage(errormsg);

    }
    //upsert assRuleItem;
    return new PageReference('/'+queryString) ;
  }

  public PageReference  Cancel(){
    return new PageReference('/'+queryString);
  }

  public PageReference selectclick()
  {
    rightselected.clear();
    if(leftselected.size()!=0 || !leftselected.isEmpty()){
      for(String s : leftselected){
        leftvalues.remove(s);
        rightvalues.add(s);
      }
    }
    return null;
  }

  public PageReference unselectclick()
  {
    leftselected.clear();
    for(String s : rightselected){
      rightvalues.remove(s);
      leftvalues.add(s);
    }
    return null;
  }

  public List<SelectOption> getSelectedValues()
  {
    List<SelectOption> options1 = new List<SelectOption>();
    Transient List<string> tempList = new List<String>();
    tempList.addAll(rightvalues);
    tempList.sort();
    for(String s : tempList)
      options1.add(new SelectOption(s,s));
    return options1;
  }

  private void removeExistingValues(List<BI_TM_Assignment_Rule_Item__c> existingValues, Set<string> leftvalues){
    for(BI_TM_Assignment_Rule_Item__c str : existingValues)
    {
      leftvalues.remove(str.BI_TM_Value__c);
    }
  }

}