@isTest
private class BI_TM_AssignmentRuleItemCreateExt_Test {
  set<string> leftvalues = new set<string>();
  
  static testMethod void TestBI_TM_AssignmentRuleItemCreateExt() {
        // Create some test data
        User currentuser = new User();
      currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
       BI_TM_FF_type__c ff= new BI_TM_FF_type__c();
        ff.Name='TestFF';
        ff.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        ff.BI_TM_Business__c='PM';
       insert ff; 
       BI_TM_Assignment_Criteria_Type__c act= new BI_TM_Assignment_Criteria_Type__c();
        act.Name='TestFF';
        act.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        act.BI_TM_Business__c='PM';
       insert act; 
       BI_TM_Assignment__c as1 = new BI_TM_Assignment__c();
           as1.Name ='Testassignment';
           as1.BI_TM_Assignment_object__c = 'HCP';
           as1.BI_TM_Assignment_Criteria_Type__c=act.Id;
           as1.BI_TM_Business__c = 'AH';
           //as1.BI_TM_FF_type_id__c = ff.Id;
           as1.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
           as1.BI_TM_Business__c='PM';
       insert as1;
      
       BI_TM_Rule_Type__c rule = new BI_TM_Rule_Type__c();
           rule.BI_TM_CRM_Column__c='Status_BI__c';
           rule.BI_TM_CRM_Table__c='Account';
           rule.Name='HCP Status';
           rule.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
       insert rule;
            
       BI_TM_Assignment_rule__c asr= new BI_TM_Assignment_rule__c();
           asr.BI_TM_Rule_Type_New__c=rule.Id;
           asr.BI_TM_Rule_name__c= as1.Id;
           
       insert asr;
      
        
        BI_TM_Assignment_Rule_Item__c ba = new BI_TM_Assignment_Rule_Item__c();
        ba.BI_TM_Value__c ='Inactive';
        ba.BI_TM_Assignment_rule__c = asr.Id;
        insert ba;
        String queryString ='';
       /*   ApexPages.StandardController sc = new ApexPages.standardController(a);
         BI_TM_AssignmentRuleItemCreateExt e = new BI_TM_AssignmentRuleItemCreateExt(sc);*/
       PageReference pageRef = Page.BI_TM_AssigmentRuleItemCreate;
         Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(asr);
        BI_TM_AssignmentRuleItemCreateExt e = new BI_TM_AssignmentRuleItemCreateExt(sc);
        set<string> values = new set<string>();
        List<BI_TM_Rule_Type__c> ruletypeCustom= new List<BI_TM_Rule_Type__c>();
        List<BI_TM_Assignment_rule__c> ruletype = new List<BI_TM_Assignment_rule__c>();
         apexpages.currentpage().getparameters().put('assgid' , asr.Id);

        ruletype= [select BI_TM_Rule_Type_New__r.Name,Name from BI_TM_Assignment_rule__c where id=:queryString limit 1];

        ruletypeCustom=[select BI_TM_CRM_Column__c,BI_TM_CRM_Table__c,Name from BI_TM_Rule_Type__c where Name='HCP Status' limit 1 ];
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get('Account');
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Schema.DisplayType fielddataType = fieldMap.get('Status_BI__c').getDescribe().getType();
        if(fielddataType ==Schema.DisplayType.Picklist){ 
          List<Schema.PicklistEntry> pick_list_values = fieldMap.get('Status_BI__c').getDescribe().getPickListValues();
             
                //leftvalues.add(a.getValue());
                values.add('Temporarily inactive');
                values.add('Valid');
                values.add('Archived');
               
               }
      
            e.leftselected.add('invalid');
            e.selectclick();
            e.save();
            e.getvalues();
            e.unselectclick();
            e.Cancel();
            e.getSelectedValues();
  
           }
    static testMethod void TestBI_TM_AssignmentRuleItemCreateExt2() {
        // Create some test data
         User currentuser = new User();
      currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
       BI_TM_FF_type__c ff1= new BI_TM_FF_type__c();
        ff1.Name='TestFF1';
        ff1.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        ff1.BI_TM_Business__c='PM';
       insert ff1;
        BI_TM_Assignment_Criteria_Type__c act= new BI_TM_Assignment_Criteria_Type__c();
        act.Name='TestFF';
        act.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        act.BI_TM_Business__c='PM';
       insert act; 
       BI_TM_Assignment__c as2 = new BI_TM_Assignment__c();
           as2.Name ='Testassignment1';
           as2.BI_TM_Assignment_object__c = 'HCP';
           as2.BI_TM_Business__c = 'AH';
           as2.BI_TM_Assignment_Criteria_Type__c=act.Id;
           as2.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
           as2.BI_TM_Business__c='PM';
       insert as2;
      
       BI_TM_Rule_Type__c rule1 = new BI_TM_Rule_Type__c();
           rule1.BI_TM_CRM_Column__c='BI_TARGET_BI__C';
           rule1.BI_TM_CRM_Table__c='Account';
           rule1.Name='Target';
           rule1.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
       insert rule1;
            
       BI_TM_Assignment_rule__c asr1= new BI_TM_Assignment_rule__c();
           asr1.BI_TM_Rule_Type_New__c=rule1.Id;
           asr1.BI_TM_Rule_name__c= as2.Id;
       insert asr1;
      
        
        BI_TM_Assignment_Rule_Item__c ba1 = new BI_TM_Assignment_Rule_Item__c();
        ba1.BI_TM_Value__c ='0';
        ba1.BI_TM_Assignment_rule__c = asr1.Id;
        insert ba1;
       /*   ApexPages.StandardController sc = new ApexPages.standardController(a);
         BI_TM_AssignmentRuleItemCreateExt e = new BI_TM_AssignmentRuleItemCreateExt(sc);*/
        PageReference pageRef = Page.BI_TM_AssigmentRuleItemCreate;
         Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(asr1);
        apexpages.currentpage().getparameters().put('assgid' , asr1.Id);
        BI_TM_AssignmentRuleItemCreateExt e1 = new BI_TM_AssignmentRuleItemCreateExt(sc1);
        set<string> values = new set<string>();
        List<BI_TM_Rule_Type__c> ruletypeCustom= new List<BI_TM_Rule_Type__c>();
       
        ruletypeCustom=[select BI_TM_CRM_Column__c,BI_TM_CRM_Table__c,Name from BI_TM_Rule_Type__c where Name='HCP Status' limit 1 ];
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get('Account');
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Schema.DisplayType fielddataType = fieldMap.get('BI_TARGET_BI__C').getDescribe().getType();
        if(fielddataType ==Schema.DisplayType.Picklist){ 
          List<Schema.PicklistEntry> pick_list_values = fieldMap.get('BI_TARGET_BI__C').getDescribe().getPickListValues();
             
                //leftvalues.add(a.getValue());
               // values.add('0');
                
               
               }
            
           // e1.leftselected.add('0');
            e1.selectclick();
            e1.save();
            e1.getvalues();
            e1.unselectclick();
            e1.Cancel();
            e1.getSelectedValues();
  
           }
    static testMethod void TestBI_TM_AssignmentRuleItemCreateExt3() {
        // Create some test data
         User currentuser = new User();
      currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
       BI_TM_FF_type__c ff2= new BI_TM_FF_type__c();
        ff2.Name='TestFF';
        ff2.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        ff2.BI_TM_Business__c='PM';
       insert ff2;
        BI_TM_Assignment_Criteria_Type__c act= new BI_TM_Assignment_Criteria_Type__c();
        act.Name='TestFF';
        act.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        act.BI_TM_Business__c='PM';
       insert act; 
       BI_TM_Assignment__c as3 = new BI_TM_Assignment__c();
           as3.Name ='Testassignment';
           as3.BI_TM_Assignment_object__c = 'HCP';
           as3.BI_TM_Business__c = 'AH';
           as3.BI_TM_Assignment_Criteria_Type__c=act.Id;
           as3.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
           as3.BI_TM_Business__c='PM';
       insert as3;
      
       BI_TM_Rule_Type__c rule2 = new BI_TM_Rule_Type__c();
           rule2.BI_TM_CRM_Column__c='SPECIALTY_VIEW_2_BI__C';
           rule2.BI_TM_CRM_Table__c='Account';
           rule2.Name='HCP Status';
           rule2.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
       insert rule2;
            
       BI_TM_Assignment_rule__c asr2= new BI_TM_Assignment_rule__c();
           asr2.BI_TM_Rule_Type_New__c=rule2.Id;
           asr2.BI_TM_Rule_name__c= as3.Id;
       insert asr2;
      
        
        BI_TM_Assignment_Rule_Item__c ba2 = new BI_TM_Assignment_Rule_Item__c();
        ba2.BI_TM_Value__c ='Inactive';
        ba2.BI_TM_Assignment_rule__c = asr2.Id;
        insert ba2;
       /*   ApexPages.StandardController sc = new ApexPages.standardController(a);
         BI_TM_AssignmentRuleItemCreateExt e = new BI_TM_AssignmentRuleItemCreateExt(sc);*/
        PageReference pageRef = Page.BI_TM_AssigmentRuleItemCreate;
         Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc2 = new ApexPages.StandardController(asr2);
        apexpages.currentpage().getparameters().put('assgid' , asr2.Id);
        BI_TM_AssignmentRuleItemCreateExt e2 = new BI_TM_AssignmentRuleItemCreateExt(sc2);
        set<string> values = new set<string>();
        List<BI_TM_Rule_Type__c> ruletypeCustom= new List<BI_TM_Rule_Type__c>();
       
        ruletypeCustom=[select BI_TM_CRM_Column__c,BI_TM_CRM_Table__c,Name from BI_TM_Rule_Type__c where Name='Speciality View' limit 1 ];
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get('Account');
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Schema.DisplayType fielddataType = fieldMap.get('SPECIALTY_VIEW_2_BI__C').getDescribe().getType();
        if(fielddataType ==Schema.DisplayType.Picklist){ 
          List<Schema.PicklistEntry> pick_list_values = fieldMap.get('SPECIALTY_VIEW_2_BI__C').getDescribe().getPickListValues();
             
                //leftvalues.add(a.getValue());
                values.add('Temporarily inactive');
                values.add('Valid');
                values.add('Archived');
               
               }
      
            e2.leftselected.add('invalid');
            e2.selectclick();
            e2.save();
            e2.getvalues();
            e2.unselectclick();
            e2.Cancel();
            e2.getSelectedValues();
  
           }
   static testMethod void TestBI_TM_AssignmentRuleItemCreateExt4() {
        // Create some test data
         User currentuser = new User();
      currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
       BI_TM_FF_type__c ff3= new BI_TM_FF_type__c();
        ff3.Name='TestFF';
        ff3.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        ff3.BI_TM_Business__c='PM';
       insert ff3;
        BI_TM_Assignment_Criteria_Type__c act= new BI_TM_Assignment_Criteria_Type__c();
        act.Name='TestFF';
        act.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        act.BI_TM_Business__c='PM';
       insert act; 
       BI_TM_Assignment__c as4 = new BI_TM_Assignment__c();
           as4.Name ='Testassignment';
           as4.BI_TM_Assignment_object__c = 'HCP';
           as4.BI_TM_Business__c = 'AH';
           as4.BI_TM_Assignment_Criteria_Type__c=act.Id;
           as4.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
           as4.BI_TM_Business__c='PM';
       insert as4;
      
       BI_TM_Rule_Type__c rule3 = new BI_TM_Rule_Type__c();
           rule3.BI_TM_CRM_Column__c='RecordType';
           rule3.BI_TM_CRM_Table__c='Account';
           rule3.Name='RecordType';
           rule3.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
       insert rule3;
            
       BI_TM_Assignment_rule__c asr3= new BI_TM_Assignment_rule__c();
           asr3.BI_TM_Rule_Type_New__c=rule3.Id;
           asr3.BI_TM_Rule_name__c= as4.Id;
       insert asr3;
      
        
       /* BI_TM_Assignment_Rule_Item__c ba3 = new BI_TM_Assignment_Rule_Item__c();
        ba3.BI_TM_Value__c ='Inactive';
        ba3.BI_TM_Assignment_rule__c = asr3.Id;
        insert ba3;*/
       /*   ApexPages.StandardController sc = new ApexPages.standardController(a);
         BI_TM_AssignmentRuleItemCreateExt e = new BI_TM_AssignmentRuleItemCreateExt(sc);*/
        PageReference pageRef = Page.BI_TM_AssigmentRuleItemCreate;
         Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc3 = new ApexPages.StandardController(asr3);
        apexpages.currentpage().getparameters().put('assgid' , asr3.Id);
        BI_TM_AssignmentRuleItemCreateExt e3 = new BI_TM_AssignmentRuleItemCreateExt(sc3);
        set<string> values = new set<string>();
        List<BI_TM_Rule_Type__c> ruletypeCustom= new List<BI_TM_Rule_Type__c>();
       
        
      
            e3.leftselected.add('invalid');
            e3.selectclick();
            e3.save();
            e3.getvalues();
            e3.unselectclick();
            e3.Cancel();
            e3.getSelectedValues();
  
           }
         
     }