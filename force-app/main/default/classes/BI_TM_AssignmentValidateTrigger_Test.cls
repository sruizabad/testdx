//Test class for trigger BI_TM_AssignmentValidateTrigger
@isTest
private class BI_TM_AssignmentValidateTrigger_Test {
    
    static testMethod void BI_TM_AssignmentValidateTrigger_Test() {
    User currentuser = new User();
    currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
       BI_TM_FF_type__c ff= new BI_TM_FF_type__c();
        ff.Name='TestFF';
        ff.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        ff.BI_TM_Business__c='PM';
       insert ff; 
        BI_TM_Assignment__c assign = new BI_TM_Assignment__c (        
            Name = 'TestTrigger1',
            BI_TM_Assignment_description__c = 'TestTrigger1',
            BI_TM_FF_type_id__c = ff.id,
            BI_TM_Country_Code__c=currentuser.Country_Code_BI__c,
            BI_TM_Assignment_object__c = 'HCO',
            BI_TM_Assignment_status__c = 'Active',
            BI_TM_Business__c = 'PM',
            BI_TM_IsActive__c = true
            );
        insert assign;
        
        BI_TM_Assignment__c assignUpdate = new BI_TM_Assignment__c(
            Id = assign.id,
            BI_TM_Assignment_object__c = 'HCP'
        );
        update assignUpdate;  
        List<SObject> newItems= new List<SObject>();
        Map<Id, SObject> newItems1=new Map<Id, SObject>();
        Map<Id, SObject> oldItems= new Map<Id, SObject> ();
        
            
        BI_TM_Assignment_Handler handler=new BI_TM_Assignment_Handler ();
        handler.BeforeInsert(newItems);
        handler.BeforeUpdate(newItems1,oldItems);
        handler.BeforeDelete(oldItems);
        handler.AfterInsert(newItems1);
        handler.AfterUpdate(newItems1,oldItems);
        handler.AfterDelete(oldItems);
        handler.AfterUndelete(oldItems);
        
    }
}