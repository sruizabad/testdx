/********************************************************************************
Name:  BI_TM_BatchToUpdateMirrorProduct
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Batch to get all the product into newly created custom object i.e BI_TM_Mirror_Product__c
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran               1/11/2016   INITIAL DEVELOPMENT
*********************************************************************************/

global class BI_TM_BatchToUpdateMirrorProduct implements Database.Batchable<sObject>, database.stateful {

    private static final String TO_ADDRESS = 'zzITM_SBITMANSFCoE@boehringer-ingelheim.com';
    private static final String EMAIL_SUBJECT = 'BITMAN - Errors Batch Mirror Products';

    //global string prodtype ='Detail';
    String ProductstoProcessforcountries = Label.BI_TM_AlignmentDataBkp_Countries;
    Set < String > countries;

    String ProductTypestoProcess = Label.BI_TM_ProductTypes;
    Set < String > prodtypes = new Set < String > (ProductTypestoProcess.split(','));

    public BI_TM_BatchToUpdateMirrorProduct()
    {
      List<BI_TM_CountryCodes__c> lstCountry=BI_TM_CountryCodes__c.getAll().values();
      countries=new Set<String>();
      for(BI_TM_CountryCodes__c objCountrySett:lstCountry)
      {
        countries.add(objCountrySett.CountryCode__c);
      }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        system.debug('======countries are=====' + countries);
        system.debug('======ProductTypes=====' + prodtypes);

        //List<BI_TM_CountryCodes__c> countrycodes =[SELECT Name,CountryCode__c From BI_TM_CountryCodes__c];
        String strQuery = 'SELECT Id, Name,Product_Type_vod__c,Country_Code_BI__c,External_ID_vod__c FROM Product_vod__c Where Product_Type_vod__c =: prodTypes and Country_Code_BI__c=:countries';
        system.debug('======ProductList=====' + strQuery);

        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Product_vod__c> lstproductcatalog)
    {
      Set<Id> setProductCatalogIds=new Set<Id>();
      for(Product_vod__c objProduct:lstproductcatalog)
        setProductCatalogIds.add(objProduct.id);

      Map<Id, BI_TM_Mirror_Product__c> mirrorProductMap = new Map<ID, BI_TM_Mirror_Product__c> ();
      for(BI_TM_Mirror_Product__c objMirror: [select id,BI_TM_Product_Catalog_Id__c, Name, BI_TM_External_ID__c
                                                     from BI_TM_Mirror_Product__c
                                                     where BI_TM_Product_Catalog_Id__c=:setProductCatalogIds])
        mirrorProductMap.put(objMirror.BI_TM_Product_Catalog_Id__c, objMirror);

      List<BI_TM_Mirror_Product__c> lstMirrorProductToInsert=new List<BI_TM_Mirror_Product__c>();
      List<BI_TM_Mirror_Product__c> toUpdate = new List<BI_TM_Mirror_Product__c>();
      System.debug('Init comparison process');
      for(Product_vod__c objProduct:lstproductcatalog)
      {
        //System.debug('Product Catalog: ' + objProduct);
        if(!mirrorProductMap.containsKey(objProduct.id))
        {
          BI_TM_Mirror_Product__c objMirrorToProd=new BI_TM_Mirror_Product__c();
          objMirrorToProd.name=objProduct.name;
          objMirrorToProd.BI_TM_Product_Catalog_Id__c=objProduct.id;
          objMirrorToProd.BI_TM_Country_Code__c=objProduct.Country_Code_BI__c;
          lstMirrorProductToInsert.add(objMirrorToProd);
          System.debug('To insert: ' + objMirrorToProd);
        } else {
          BI_TM_Mirror_Product__c mp = mirrorProductMap.get(objProduct.id);
          //System.debug('Mirror product: ' + mp);
          if (objProduct.Name != mp.Name) {
            mp.Name = objProduct.Name;
            toUpdate.add(mp);
            System.debug('To update: ' + mp);
          }
          if (objProduct.External_ID_vod__c != mp.BI_TM_External_ID__c) {
            if(!toUpdate.contains(mp)){
                toUpdate.add(mp);
                System.debug('To update 2: ' + mp);
            }
          }
        }
      }
      System.debug('End comparison process');

      Database.SaveResult [] results = new Database.SaveResult[] {};
      if(lstMirrorProductToInsert.size()>0)
        results.addAll(Database.insert(lstMirrorProductToInsert, false));
      if (!toUpdate.isEmpty())
        results.addAll(Database.update(toUpdate, false));
      processResults(results);
    }

    private void processResults(Database.SaveResult[] results) {
      String errorMessage = '';
      for (Database.SaveResult sr : results) {
        if (!sr.isSuccess())
          for (Database.Error err : sr.getErrors()) {
            System.debug('Error: ' + err);
            errorMessage += ' - ' + err.getStatusCode() + ': ' + err.getMessage() + '\n';
          }
      }

      if (!String.isBlank(errorMessage)) {
        errorMessage = 'The following errors has occurred during mirror product batch database insert/update operations: \n' + errorMessage;
        System.debug(errorMessage);

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String [] { TO_ADDRESS });
        mail.setSubject(EMAIL_SUBJECT);
        mail.setPlainTextBody(errorMessage);
        try {
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        } catch (Exception ex) {}
      }
    }

    global void finish(Database.BatchableContext BC) {

      BI_TM_ConsolidateUserRoleUpdate b = new BI_TM_ConsolidateUserRoleUpdate();
      database.executebatch(b);

    }
}