/*
Name:  BI_TM_BatchToUpdateMirrorProductTest
Requirement ID: ProductUpload
Description: Test class for  BI_TM_BatchToUpdateMirrorProduct
Version | Author-Email | Date | Comment
1.0 | Suchitra | 01.02.2016 | initial version
*/
@isTest
private class  BI_TM_BatchToUpdateMirrorProductTest {

    static List<Product_vod__c >  lstProd  = new List<Product_vod__c >();

    public  BI_TM_BatchToUpdateMirrorProductTest ()
    {
        //lstTerritory = [SELECT Id, Name FROM Territory LIMIT 10];
    }

    @isTest(SeeAllData=true)
    static void testWithAllDataAccess() {
          String prodType = 'Detail';
          String countrycode1='MX';
        lstProd  = [SELECT Id, Name,Product_Type_vod__c,Country_Code_BI__c FROM Product_vod__c Where Product_Type_vod__c = :prodType and Country_Code_BI__c=:countrycode1  LIMIT 10];
        // Can query all data in the organization.
    }

 private static testMethod void testBI_TM_BatchToUpdateMirrorProductTest () {

//   List<BI_TM_Mirror_Product__c> lstMirrorProductToInsert =new List<BI_TM_Mirror_Product__c>();
//   List<Product_vod__c > lstProdToInsert = new List<Product_vod__c >();
//        User currentuser = new User();
//       currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
//       Product_vod__c p= new Product_vod__c();
//        p.Name='Test';
//        p.Product_Type_vod__c='Detail';
//        Insert p;
//        BI_TM_Mirror_Product__c mp= new BI_TM_Mirror_Product__c();
//        mp.BI_TM_Active__c=True;
//        mp.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
//        mp.Name='Test';
//        mp.BI_TM_Product_Type__c='Detail';
//        mp.BI_TM_Product_Catalog_Id__c=p.Id;
//        Insert mp;
//
//
//         for(Product_vod__c  objProd : lstProd )
//         {
//             lstMirrorProductToInsert.add(new BI_TM_Mirror_Product__c(Name = objProd.Name));
//         }
//
//  Test.startTest();
//
//         insert lstMirrorProductToInsert ;
//
//         System.runAs(new User(Id = UserInfo.getUserId())) {
//
//
//
//   // Insert Product records
//             for(Integer intCount = 1 ; intCount <= 10; intCount++) {
//
//                 Product_vod__c  objProd = new Product_vod__c(Name = 'TestProd'+intCount, Product_Type_vod__c='Detail',
//                  Country_Code_BI__c ='MX');
//                 lstProdToInsert .add(objProd );
//             }
//
//             insert lstProdToInsert ;
//         }
//
//         Id batchID;
//
//  BI_TM_BatchToUpdateMirrorProduct c = new BI_TM_BatchToUpdateMirrorProduct ();
//             batchID = Database.executeBatch(c,2000);
//         Test.stopTest();
//
// System.abortJob(batchID);
    }


}