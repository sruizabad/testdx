global class BI_TM_BatchToUpdateMirrorProduct_Sch implements Schedulable {
   global void execute(SchedulableContext SC) {
      BI_TM_BatchToUpdateMirrorProduct obj = new BI_TM_BatchToUpdateMirrorProduct();
      database.executebatch(obj);   
   }
}