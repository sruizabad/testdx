/*
Name:  BI_TM_BatchToUpdateMirrorProductTest
Requirement ID: ProductUpload
Description: Test class for  BI_TM_BatchToUpdateMirrorProduct
Version | Author-Email | Date | Comment
1.0 | Suchitra | 01.02.2016 | initial version
2.0 | Migart | 26.10.2018
*/
@isTest
private class  BI_TM_BatchToUpdateMirrorProduct_Test {

     static List<Product_vod__c >  lstProd  = new List<Product_vod__c >();

 private static testMethod void testBI_TM_BatchToUpdateMirrorProductTest () {

  String CountryCode='MX';

  List<BI_TM_CountryCodes__c> ccList=[select name from BI_TM_CountryCodes__c where CountryCode__c=:CountryCode];
  if (ccList.size()==0) {
      BI_TM_CountryCodes__c cc= new BI_TM_CountryCodes__c (Name='Mexico',CountryCode__c=CountryCode);
      insert cc;
  }
  // We insert three products
      Product_vod__c p= new Product_vod__c();
       p.Name='Test';
       p.Product_Type_vod__c='Detail';
       p.Country_Code_BI__c=CountryCode;
       Insert p;
       Product_vod__c p1= new Product_vod__c();
       p1.Name='Test1';
       p1.Product_Type_vod__c='Detail';
       p1.Country_Code_BI__c=CountryCode;
       Insert p1;
       Product_vod__c p2= new Product_vod__c();
       p2.Name='Test2';
       p2.Product_Type_vod__c='Detail';
       p2.Country_Code_BI__c=CountryCode;
       p2.External_ID_vod__c='TestMirror';
       Insert p2;

//We get the Id of the inserted products test1 and test2
       String prodIdp = [SELECT Id FROM Product_vod__c Where Name='Test1' limit 1].Id;
       String prodIdp2 = [SELECT Id FROM Product_vod__c Where Name='Test2' limit 1].Id;

// Insert a record in Mirror Product equal to Test1 but with different name      
       BI_TM_Mirror_Product__c m= new BI_TM_Mirror_Product__c();
       m.Name='TestMirror';
       m.BI_TM_Country_Code__c=CountryCode;
       m.BI_TM_Product_Catalog_Id__c=prodIdp;
       Insert m;
// Insert a record in Mirror Product equal to Test2 with different External_Id
       BI_TM_Mirror_Product__c m2= new BI_TM_Mirror_Product__c();
       m2.BI_TM_Product_Catalog_Id__c=prodIdp2;
       m2.Name='Test2';
       m2.BI_TM_Country_Code__c=CountryCode;
       m2.BI_TM_External_ID__c='Different';
       Insert m2;      
// we have to update external id again to make them different as trigger makes them equals
       p2.External_ID_vod__c='Different';
       update p2;

       system.debug('======Product p1=====' + p1);  
       system.debug('======Product m =====' + m); 
       system.debug('======Product p2 =====' + p2); 
       system.debug('======Product m2 =====' + m2);   
        
        Test.startTest();
        Database.executeBatch(new BI_TM_BatchToUpdateMirrorProduct());
        Test.stopTest();

        List<BI_TM_Mirror_Product__c> lstMirrorProduct=[select id from BI_TM_Mirror_Product__c where Name Like 'Test' and bi_tm_country_code__c=:CountryCode];
        System.assertEquals(1, lstMirrorProduct.size());
        List<BI_TM_Mirror_Product__c> lstMirrorProduct1=[select id from BI_TM_Mirror_Product__c where Name Like 'Test1' and bi_tm_country_code__c=:CountryCode];
        System.assertEquals(1, lstMirrorProduct1.size());
        List<BI_TM_Mirror_Product__c> lstMirrorProduct2=[select id from BI_TM_Mirror_Product__c where Name Like 'Test2' and bi_tm_country_code__c=:CountryCode and BI_TM_External_ID__c='Different'];
        System.assertEquals(1, lstMirrorProduct2.size());

    }


}