global class BI_TM_BatchToUpdatelastloginandenddate implements Database.Batchable<sObject>,database.stateful {


    global Database.QueryLocator start(Database.BatchableContext BC) {
        String strQuery = 'select Id,BI_TM_Username__c,BI_TM_End_date__c,BI_TM_Start_date__c, BI_TM_Active__c,BI_TM_Rep_Sample_Eligible__c, BI_TM_Visible_in_CRM__c from BI_TM_User_mgmt__c WHERE BI_TM_Visible_in_CRM__c = true';
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<BI_TM_User_mgmt__c> bitmanUsers) {

        Set<String> usernames = new Set<String>();
        for(BI_TM_User_mgmt__c u: bitmanUsers){
            usernames.add(u.BI_TM_Username__c);
        }

        List<User> veevaUsers = [SELECT ID, LastLoginDate, Username, IsActive,Rep_Sample_Eligible_BI__c FROM User WHERE Username IN :usernames];
        Map<String, User> veevaUsersMap = new Map<String, User>();
        for (User vu : veevaUsers) {
            veevaUsersMap.put(vu.username, vu);
        }


        for(BI_TM_User_mgmt__c bitmanUser : bitmanUsers) {
            User veevaUser = veevaUsersMap.get(bitmanUser.BI_TM_Username__c);
            if (veevaUser != null && bitmanUser.BI_TM_Visible_in_CRM__c) {
                bitmanUser.BI_TM_Last_Login__c = veevaUser.LastLoginDate;
                bitmanUser.BI_TM_UserId_Lookup__c = veevaUser.Id;
                bitmanUser.BI_TM_Rep_Sample_Eligible__c=veevaUser.Rep_Sample_Eligible_BI__c;
            }

            if(bitmanUser.BI_TM_End_date__c != null && bitmanUser.BI_TM_End_date__c < system.Today() &&  bitmanUser.BI_TM_Active__c) {
                bitmanUser.BI_TM_Active__c = false;
            } else if(!bitmanUser.BI_TM_Active__c && bitmanUser.BI_TM_Start_date__c <= System.Today() && (bitmanUser.BI_TM_End_date__c == null || bitmanUser.BI_TM_End_date__c >= system.Today())) {
                bitmanUser.BI_TM_Active__c = true;
            }

            BI_TM_boolean.run = true;
        }
        Database.update(bitmanUsers, false);

    }

    global void finish(Database.BatchableContext BC) {

      BI_TM_Process_Inactive_Users_Veeva s= new BI_TM_Process_Inactive_Users_Veeva();
      //s.execute(null);
      database.executeBatch(s);

    }
}