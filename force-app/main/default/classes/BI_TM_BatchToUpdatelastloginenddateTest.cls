/******************************************************************************** 
Name:  BI_TM_BatchToUpdatelastloginandenddate_Test
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
apex test class For BI_TM_BatchToUpdatelastloginandenddate
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Kiran              14/04/2016   INITIAL DEVELOPMENT
*********************************************************************************/
@isTest
private class BI_TM_BatchToUpdatelastloginenddateTest{

   // CRON expression: midnight on March 15.
   // Because this is a test, job executes
   // immediately after Test.stopTest().
   public static String CRON_EXP = '0 0 0 15 3 ? 2022';

   static testmethod void TestBI_TM_updatelastloginuser_Scheduled () {
      Test.startTest();
          list<Profile> profid = new list<profile>();
          BI_TM_User_mgmt__c um= new BI_TM_User_mgmt__c();
          
      User currentuser = new User();
          currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
      
      profid =[select Id from profile where Name='TM-GLOBAL-USER' limit 1];
    
       User u = new user();
              u.LastName='TestUser1';
              u.Alias='Test';
              u.Email='Testuser10@test.com';
              u.Username='Testuser10@sample.com';
              u.CommunityNickname='Test';
              u.ProfileId=profid[0].Id;
              u.Business_BI__c='PM';
              u.TimeZoneSidKey='Europe/Athens';
              u.LocaleSidKey='fr';
              u.LanguageLocaleKey='en_US';
              u.CurrencyIsoCode='EUR';
              u.EmailEncodingKey='ISO-8859-1';
              u.Country_Code_BI__c='MX';
              u.Isactive=False;
              u.Rep_Sample_Eligible_BI__c =True;
       Insert u;
      
          um.BI_TM_Business__c='AH';
          um.BI_TM_COMMUNITYNICKNAME__c='Nick';
          um.BI_TM_Alias__c='Nick';
          um.BI_TM_BIGLOBALID__c='gi000123';
          um.BI_TM_Email__c='nick@test.com';
          um.BI_TM_End_date__c=system.today();
          um.BI_TM_First_name__c='Nick';
          um.BI_TM_LanguageLocaleKey__c='English';
          um.BI_TM_TimeZoneSidKey__c='America/Mexico_City';
          um.BI_TM_Username__c='Testuser10@sample.com';
          um.BI_TM_Profile__c=profid[0].id;
          um.BI_TM_LocaleSidKey__c='English (Canada)';
          um.BI_TM_UserCountryCode__c='MX';
          um.BI_TM_Last_Login__c=system.today();
          um.BI_TM_UserId_Lookup__c=u.Id;
          um.BI_TM_Active__c=true;
          um.BI_TM_Rep_Sample_Eligible__c = True;
          um.BI_TM_Email_Encoding__c='Unicode (UTF-8)';
      Insert um;
      
       Id batchID;
      BI_TM_BatchToUpdatelastloginandenddate c = new BI_TM_BatchToUpdatelastloginandenddate();
        batchID = Database.executeBatch(c,200);
    }
  }