@isTest
private class BI_TM_Batch_ManAssgn_From_SAPL_Test {

	@isTest static void test_method_one() {

		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Account acc = new Account(Name = 'Test Account');
		System.runAs(thisUser){
			Date startDate = Date.newInstance(2018,4,16);
			Date endDate = Date.newInstance(2018,6,30);
			Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
			insert ks;
			insert acc;

			BI_TM_Field_Force_Rules__c ffr = new BI_TM_Field_Force_Rules__c(BI_TM_Country_Code__c = 'BR', Name = 'MAPOR', BI_TM_Direct_Child_Assignment__c = true, BI_TM_Direct_Parent_Assignment__c = true, BI_TM_Fonte_Pagadora_Check__c = true);
			insert ffr;

			Cycle_Plan_vod__c cp = new Cycle_Plan_vod__c(Territory_vod__c = 'BR-PM-Pos0-Test', Start_Date_vod__c = startDate, End_Date_vod__c = endDate);
			insert cp;
			Cycle_Plan_Target_vod__c cpt = new Cycle_Plan_Target_vod__c(Cycle_Plan_Account_vod__c = acc.Id, Cycle_Plan_vod__c = cp.Id, Country_Code_BI__c = 'BR');
			insert cpt;
			Cycle_Plan_vod__c cp1 = new Cycle_Plan_vod__c(Territory_vod__c = 'BR-PM-Pos0-TestNO', Start_Date_vod__c = startDate, End_Date_vod__c = endDate);
			insert cp1;
			Cycle_Plan_Target_vod__c cpt1 = new Cycle_Plan_Target_vod__c(Cycle_Plan_Account_vod__c = acc.Id, Cycle_Plan_vod__c = cp1.Id, Country_Code_BI__c = 'BR');
			insert cpt1;

			BI_TM_FF_type__c ff = BI_TM_UtilClassTestDataFactory.createFieldForce('MAPOR', 'BR', 'PM');
			insert ff;
			BI_TM_Position_Type__c posType = BI_TM_UtilClassTestDataFactory.createPosType('BR-PM-PosTypeTest','BR','PM');
			insert posType;
			BI_TM_Territory__c pos = BI_TM_UtilClassTestDataFactory.createPosition('BR', 'BR', 'PM', ff.Id, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
			insert pos;

			for(Integer i = 0; i < 10; i++){
				BI_TM_Territory__c p = BI_TM_UtilClassTestDataFactory.createPosition('BR-PM-Pos' + Integer.ValueOf(i) + '-Test', 'BR', 'PM', ff.Id, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
				posList.add(p);
			}
			insert posList;

			BI_TM_Account_to_territory__c ma = BI_TM_UtilClassTestDataFactory.createManualAssignment(posList[0].Id, acc.Id, 'BR', 'PM', startDate - 365, startDate - 1, false, false, 'Both', false, true);
		}
		Test.startTest();
		BI_TM_Batch_ManualAssignments_From_SAPL batchable = new BI_TM_Batch_ManualAssignments_From_SAPL();
		database.executeBatch(batchable);
		Test.stopTest();
		List<BI_TM_Account_to_territory__c> manAssigList = [SELECT Id FROM BI_TM_Account_to_territory__c WHERE BI_TM_Territory_FF_Hierarchy_Position__r.Name = 'BR-PM-Pos0-Test' AND BI_TM_Account__c = :acc.Id];
		System.assertEquals(manAssigList.size() > 0, true);
		List<BI_TM_Account_to_territory__c> manAssigList1 = [SELECT Id FROM BI_TM_Account_to_territory__c WHERE BI_TM_Territory_FF_Hierarchy_Position__r.Name = 'BR-PM-Pos0-TestNO' AND BI_TM_Account__c = :acc.Id];
		System.assertEquals(manAssigList1.size() > 0, false);
	}

}