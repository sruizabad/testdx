/********************************************************************************
Name:  BI_TM_Batch_ManualAssignments_From_SAPL
Copyright ? 2016  Capgemini India Pvt Ltd
=================================================================
=================================================================
Batch to pull Account to Territories from Cycle_Plan_Target_vod__c(SAP Target) and push it to BI_TM_Account_To_Territory__c object
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE(MM/DD/YYYY)         DETAIL
1.0 -    Kiran                    12/09/2016        INITIAL DEVELOPMENT
*********************************************************************************/

global class BI_TM_Batch_ManualAssignments_From_SAPL implements Database.Batchable<sObject>, database.stateful{

  global Database.QueryLocator start(Database.BatchableContext BC) {
    //Account to Territories from SAPL for the Countries defined in Custom Label
    //String AccounttoTerritories = Label.BI_TM_Account_to_Territory_From_Cycle_Plan_Target;
    Map<Id, BI_TM_Country__mdt> countryMap = new Map<Id, BI_TM_Country__mdt>([SELECT Id, BI_TM_Country_Code__c, BI_TM_Filter_Start_Date__c, BI_TM_Source_Start_Date_Targets__c FROM BI_TM_Country__mdt WHERE BI_TM_Run_Manual_Assignment_From_SAPL__c = true]);

    /*Set <String> countries = new Set <String> (AccounttoTerritories.split(','));*/
    List <String> countries = new List <String> ();
    for(Id cmdt : countryMap.keySet()){
      countries.add(countryMap.get(cmdt).Id);
    }
    //Query Account to Territories from Cycle_Plan_Target_vod__c
    if(countries.size() > 0){
      String strQuery = 'SELECT Id,Account_ID_BI__c,SAP_Territory_BI__c,Country_Code_BI__c,Cycle_Plan_vod__c,Cycle_Plan_vod__r.Territory_vod__c,Cycle_Plan_vod__r.Active_vod__c,Cycle_Plan_vod__r.Start_Date_vod__c,Cycle_Plan_vod__r.End_Date_vod__c FROM Cycle_Plan_Target_vod__c';
      // Add the first country of the list
      strQuery += ' WHERE (Country_Code_BI__c = \'' + countryMap.get(countries[0]).BI_TM_Country_Code__c + '\'';
      if(countryMap.get(countries[0]).BI_TM_Filter_Start_Date__c != null || countryMap.get(countries[0]).BI_TM_Source_Start_Date_Targets__c != null){
        strQuery += ' AND Cycle_Plan_vod__r.Start_Date_vod__c = ';
        String startDate = formatDate(countryMap.get(countries[0]).BI_TM_Source_Start_Date_Targets__c);
        strQuery += countryMap.get(countries[0]).BI_TM_Filter_Start_Date__c != null ? countryMap.get(countries[0]).BI_TM_Filter_Start_Date__c : startDate;
      }
      strQuery += ')';

      // Add other countries to the query
      for(Integer i = 1; i< countries.size();i++){
        strQuery += ' OR (Country_Code_BI__c = \'' + countryMap.get(countries[i]).BI_TM_Country_Code__c + '\'';
        if(countryMap.get(countries[i]).BI_TM_Filter_Start_Date__c != null || countryMap.get(countries[i]).BI_TM_Source_Start_Date_Targets__c != null){
          strQuery += ' AND Cycle_Plan_vod__r.Start_Date_vod__c = ';
          String startDate = formatDate(countryMap.get(countries[0]).BI_TM_Source_Start_Date_Targets__c);
          strQuery += countryMap.get(countries[i]).BI_TM_Filter_Start_Date__c != null ? countryMap.get(countries[i]).BI_TM_Filter_Start_Date__c : startDate;
        }
        strQuery += ')';
      }
      system.debug('Query :: ' + strQuery);
      return Database.getQueryLocator(strQuery);
    }
    else return null;


  }

  global void execute(Database.BatchableContext BC, List<Cycle_Plan_Target_vod__c> lstSAPL) {
    List<BI_TM_Account_To_Territory__c> lstAcctToTerrinsert= new List<BI_TM_Account_To_Territory__c>();
    //get Manual Assignment records
    List<BI_TM_Account_To_Territory__c> lstAcctoTerr = [SELECT Id,BI_TM_SAP_Target_Id__c FROM BI_TM_Account_To_Territory__c WHERE BI_TM_SAP_Target_Id__c IN:lstSAPL];
    // Retrieve the start date to be used in the manual assignments for the country if any
    List<BI_TM_Country__mdt> countrySettingsList = [SELECT Id, BI_TM_Country_Code__c, BI_TM_Manual_Assignments_Start_Date__c, BI_TM_Manual_Assignments_End_Date__c FROM BI_TM_Country__mdt WHERE BI_TM_Run_Manual_Assignment_From_SAPL__c= true];
    Map<String, countryWrapper> countryWrapperMap = new Map<String, countryWrapper>();
    for(BI_TM_Country__mdt c : countrySettingsList){
      countryWrapper cw = new countryWrapper();
      cw.startDate = c.BI_TM_Manual_Assignments_Start_Date__c;
      cw.endDate = c.BI_TM_Manual_Assignments_End_Date__c;
      countryWrapperMap.put(c.BI_TM_Country_Code__c, cw);
    }

    Set<String> terrnames = new Set<String>();
    for(Cycle_Plan_Target_vod__c terr: lstSAPL){
      terrnames.add(terr.SAP_Territory_BI__c);
    }
    system.debug('=====++Territory Name++====='+terrnames);

    //get Position Id,Country Code,BUsiness for the Positions From SAPL
    List<BI_TM_Territory__c> posIDS= [Select Id,Name,BI_TM_Business__c,BI_TM_Country_Code__c,BI_TM_FF_type__c,BI_TM_FF_type__r.Name from BI_TM_Territory__c where Name IN: terrnames];
    Set<String> Pos_Vs_SAPLTerrs= new Set<String>();
    for(BI_TM_Territory__c varPos:posIDS){
      Pos_Vs_SAPLTerrs.add(varPos.Name);
    }
    system.debug('===Positons Name in BITMAN'+Pos_Vs_SAPLTerrs);
    List<String> lstpositionsnotpresent= new List<String>();
    for(String s:terrnames){
      if(!Pos_Vs_SAPLTerrs.contains(s))
      lstpositionsnotpresent.add(s);
    }
    system.debug('===Positons not present in BITMAN'+lstpositionsnotpresent);
    //Error out records(BI_TM_ExceptionHandler__c) when Positions are not present in BITMAN
    List<BI_TM_ExceptionHandler__c> errorlog= new List<BI_TM_ExceptionHandler__c>();
    try{
      if(lstpositionsnotpresent.size()>0){
        for(String lstpos: lstpositionsnotpresent){
          BI_TM_ExceptionHandler__c objerrorlog= new BI_TM_ExceptionHandler__c();
          objerrorlog.BI_TM_Exception_Message__c= lstpos +'_'+ 'This Position is Not present in BITMAN Position Object';
          errorlog.add(objerrorlog);
        }
      }
      insert errorlog;
    }catch(Exception e){
      system.debug('Error :: ' + e);
    }

    Map<String, String> Position_Vs_FF= new Map<String, String>();

    for(BI_TM_Territory__c FFNames : posIDS){
      if(!Position_Vs_FF.containsKey(FFNames.Name))
      Position_Vs_FF.put(FFNames.Name, FFNames.BI_TM_FF_type__r.Name);
    }


    //Get SAPL vs FieldForce info into map
    Map<ID,String> saplID_vs_FFName=new map<Id, String>();
    for(Cycle_Plan_Target_vod__c saplVar:lstSAPL){
      if(!saplID_vs_FFName.containsKey(saplVar.id))
      saplID_vs_FFName.put(saplVar.id, Position_Vs_FF.get(saplVar.SAP_Territory_BI__c));

    }
    system.debug('=====++Position Ids++====='+posIDS);
    Map<String,String> posBussiness = new Map<String,String>();
    Map<String,String> posCountry = new Map<String,String>();
    Map<String,String> mapterrIds= new Map<String,String>();
    for (BI_TM_Territory__c varTerrId : posIDS) {
      mapterrIds.put(varTerrId.Name, varTerrId.Id);
      posBussiness.put(varTerrId.Name,varTerrId.BI_TM_Business__c);
      posCountry.put(varTerrId.Name,varTerrId.BI_TM_Country_Code__c);
      system.debug('====++mapterrIds++====='+mapterrIds);
      system.debug('====++Position++====='+mapterrIds.get(varTerrId.Name));
    }
    Map<String,String> mapIds= new Map<String,String>();
    for(BI_TM_Account_To_Territory__c ids:lstAcctoTerr){
      mapIds.put(ids.BI_TM_SAP_Target_Id__c,ids.Id);
      system.debug('====++mapIds++====='+mapIds);
    }

    for(Cycle_Plan_Target_vod__c SAPL : lstSAPL){
      if(mapterrIds.get(SAPL.SAP_Territory_BI__c) != null){
        BI_TM_Account_To_Territory__c acctoterr = new BI_TM_Account_To_Territory__c();
        acctoterr.BI_TM_Account__c = SAPL.Account_ID_BI__c;
        acctoterr.BI_TM_Territory_FF_Hierarchy_Position__c= mapterrIds.get(SAPL.SAP_Territory_BI__c);
        system.debug('====++Position++====='+acctoterr.BI_TM_Territory_FF_Hierarchy_Position__c);
        countryWrapper cw = countryWrapperMap.get(posCountry.get(SAPL.SAP_Territory_BI__c));

        if(cw.startDate != null){
          acctoterr.BI_TM_Start_Date__c = cw.startDate;
        }
        else {
          acctoterr.BI_TM_Start_Date__c = SAPL.Cycle_Plan_vod__r.Start_Date_vod__c;
        }
        if(cw.endDate != null){
          acctoterr.BI_TM_End_Date__c = cw.endDate;
        }
        else {
          acctoterr.BI_TM_End_Date__c = SAPL.Cycle_Plan_vod__r.End_Date_vod__c;
        }

        acctoterr.BI_TM_Country_Code__c = posCountry.get(SAPL.SAP_Territory_BI__c);
        acctoterr.BI_TM_SAP_Target_Id__c= SAPL.Id;
        acctoterr.BI_TM_SAP__c=true;
        //acctoterr.BI_TM_Assign_Recursively__c=true;
        acctoterr.Id=mapIds.get(SAPL.Id);
        acctoterr.BI_TM_Business__c=posBussiness.get(SAPL.SAP_Territory_BI__c);
        if(saplID_vs_FFName.get(SAPL.Id)!=NULL){
          BI_TM_Field_Force_Rules__c ffCustomSetting = BI_TM_Field_Force_Rules__c.getValues(saplID_vs_FFName.get(SAPL.Id));
          if(ffCustomSetting!=NULL && acctoterr.BI_TM_Country_Code__c==ffCustomSetting.BI_TM_Country_Code__c){
            acctoterr.BI_TM_Direct_Parent_Assignment__c=ffCustomSetting.BI_TM_Direct_Parent_Assignment__c;
            acctoterr.BI_TM_Direct_Child_Assignment__c=ffCustomSetting.BI_TM_Direct_Child_Assignment__c;
            acctoterr.BI_TM_Fonte_Pagadora_Check__c=ffCustomSetting.BI_TM_Fonte_Pagadora_Check__c;
          }
        }
        if(acctoterr.BI_TM_Territory_FF_Hierarchy_Position__c != null && acctoterr.BI_TM_Account__c != null)
        lstAcctToTerrinsert.add(acctoterr);
      }


    }
    try{
      system.debug('Manual assignments :: ' + lstAcctToTerrinsert);
      database.insert(lstAcctToTerrinsert,false);
    }catch(Exception ex){

    }
  }
  global void finish(Database.BatchableContext BC) {

    /* Do Nothing*/
  }

  private String formatDate(Date d){
    Datetime dt = Datetime.newInstance(d.year(), d.month(), d.day());
    return (String.valueOfGMT(dt)).split(' ')[0];
  }

  private class countryWrapper{
    Date startDate;
    Date endDate;
  }

}