/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Batch to delete the blacklist accounts related to the losing account in the merge process
*  @author:         Antonio Ferrero
*  @created:        07-Sep-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         20-Sep-2018                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_BlacklistAccount_MergeAcc_Batch implements Database.Batchable<sObject> {

	String query;

	global BI_TM_BlacklistAccount_MergeAcc_Batch() {

	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id FROM BI_TM_Blacklist_account__c WHERE BI_TM_Merged_Account_Check__c = true';
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<BI_TM_Blacklist_account__c> scope) {
		Map<Id, BI_TM_Blacklist_account__c> blackAccountMap = new Map<Id, BI_TM_Blacklist_account__c>(scope);
		Database.SaveResult[] updateSrList = Database.update(scope, false);
		List<BI_TM_Blacklist_account__c> blackAccount2Delete = new List<BI_TM_Blacklist_account__c>();
		Set<Id> blacklistWithoutErrors = new Set<Id>();
		for(Database.SaveResult sr : updateSrList){
			if (sr.isSuccess()){
				blacklistWithoutErrors.add(sr.getId());
			}
		}

		// Compare records that didn´t fail with the original list
		for(Id ba : blackAccountMap.keySet()){
			if(!blacklistWithoutErrors.contains(ba)){
				blackAccount2Delete.add(blackAccountMap.get(ba));
			}
		}

		if(blackAccount2Delete.size() > 0){
			delete blackAccount2Delete;
		}
	}

	global void finish(Database.BatchableContext BC) {
		//Database.executebatch(new BI_TM_Manage_PreviousInteractions_Batch(System.now()));
	}

}