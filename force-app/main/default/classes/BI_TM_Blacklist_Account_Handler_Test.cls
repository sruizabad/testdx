@isTest
private class BI_TM_Blacklist_Account_Handler_Test
{
	@isTest
	static void method01()
	{
		Date startDate = Date.newInstance(2016, 1, 1);
		Date endDate = Date.newInstance(2099, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Account acc = new Account(Name = 'Test Account', Country_Code_BI__c = 'US');

		System.runAs(thisUser){
			Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
			insert ks;
		}
		insert acc;

		BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
		insert posType;

		BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
		insert pos;

		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
			posList.add(p);
		}
		insert posList;

		BI_TM_Blacklist_account__c ba = BI_TM_UtilClassDataFactory_Test.createBlacklistAccount(acc.Id, posList[0].Id, 'US', 'PM', startDate, null);
		insert ba;

		Test.startTest();
		List<BI_TM_Blacklist_account__c> baList = new List<BI_TM_Blacklist_account__c>();
		BI_TM_Blacklist_account__c ba1 = BI_TM_UtilClassDataFactory_Test.createBlacklistAccount(acc.Id, posList[0].Id, 'US', 'PM', startDate+100, endDate);
		BI_TM_Blacklist_account__c ba2 = BI_TM_UtilClassDataFactory_Test.createBlacklistAccount(acc.Id, posList[0].Id, 'US', 'PM', startDate+200, endDate);
		baList.add(ba1);
		baList.add(ba2);
		try{
			insert baList;
		}catch(Exception ex){
			System.debug('An exception has ocurred :: ' + ex.getMessage());
		}

		BI_TM_Blacklist_account__c ba3 = BI_TM_UtilClassDataFactory_Test.createBlacklistAccount(acc.Id, posList[0].Id, 'US', 'PM', startDate+100, endDate);
		try{
			insert ba3;
		}catch(Exception ex){
			System.debug('An exception has ocurred :: ' + ex.getMessage());
		}
		Test.stopTest();
		List<BI_TM_Blacklist_account__c> baOverlapList = [SELECT Id FROM BI_TM_Blacklist_account__c WHERE Id = :ba1.Id OR Id = :ba2.Id OR Id = :ba3.Id];
		System.assertEquals(0, baOverlapList.size());
	}
}