/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Trigger to prevent dates overlapping in the blacklist accounts
*  @author:         Antonio Ferrero
*  @created:        07-Sep-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         20-Sep-2018                 Antonio Ferrero             Construction of the class.
*/

public with sharing class BI_TM_Blacklist_account_handler {
  // This should be used in conjunction with the ApexTriggerComprehensive.trigger template
  // The origin of this pattern is http://www.embracingthecloud.com/2010/07/08/ASimpleTriggerTemplateForSalesforce.aspx
  private boolean m_isExecuting = false;
  private integer BatchSize = 0;

  public BI_TM_Blacklist_account_handler(boolean isExecuting, integer size){
    m_isExecuting = isExecuting;
    BatchSize = size;
  }

  /*public void OnBeforeInsert(BI_TM_Blacklist_account__c[] newRecords){
  }*/

  public void OnAfterInsert(BI_TM_Blacklist_account__c[] newRecords){
    checkOverlapDates(new Map<Id, BI_TM_Blacklist_account__c>(newRecords));
  }

  /*public void OnBeforeUpdate(BI_TM_Blacklist_account__c[] oldRecords, BI_TM_Blacklist_account__c[] updatedRecords, Map<ID, BI_TM_Blacklist_account__c> recordMap){
  }*/

  public void OnAfterUpdate(BI_TM_Blacklist_account__c[] oldRecords, BI_TM_Blacklist_account__c[] updatedRecords, Map<ID, BI_TM_Blacklist_account__c> recordMap){
    checkOverlapDates(recordMap);
  }

  /*public void OnBeforeDelete(BI_TM_Blacklist_account__c[] recordsToDelete, Map<ID, BI_TM_Blacklist_account__c> recordMap){

  }

  public void OnAfterDelete(BI_TM_Blacklist_account__c[] deletedRecords, Map<ID, BI_TM_Blacklist_account__c> recordMap){

  }

  public void OnUndelete(BI_TM_Blacklist_account__c[] restoredRecords){

  }*/

  /*public boolean IsTriggerContext{
    get{ return m_isExecuting;}
  }

  public boolean IsVisualforcePageContext{
    get{ return !IsTriggerContext;}
  }

  public boolean IsWebServiceContext{
    get{ return !IsTriggerContext;}
  }

  public boolean IsExecuteAnonymousContext{
    get{ return !IsTriggerContext;}
  }*/

  // Function to check the overlap dates
	public void checkOverlapDates(Map<Id, BI_TM_Blacklist_account__c> newItems){
		// First we have to check overlaps in the incoming records
		Map<String, Set<Id>> keyMapNewItems = new Map<String, Set<Id>>();

		for(Id ba : newItems.keySet()){
			String key = (newItems.get(ba)).BI_TM_Key__c;
			if(keyMapNewItems.get(key) != null){
				Set<Id> idSet = keyMapNewItems.get(key);
				idSet.add(ba);
				keyMapNewItems.put(key, idSet);
			}
			else{
				Set<Id> idSet = new Set<Id>();
				idSet.add(ba);
				keyMapNewItems.put(key, idSet);
			}
		}

		// Clone the map with the keys to remove the ids that are overlapped
		Map<String, Set<Id>> keyMapNewItemsNoOverlap = new Map<String, Set<Id>>(keyMapNewItems);
		Set<Id> keySetOverlap = new Set<Id>();

		// Check the overlap in the cncoming records
		if(keyMapNewItems != null){
			for(String key : keyMapNewItems.keySet()){
				if(keyMapNewItems.get(key) != null && keyMapNewItems.get(key).size() > 1){ // There is more that one incoming record with same key
					Set<Id> idSet = keyMapNewItems.get(key);
					Set<Id> idSetCopy = new Set<Id>(idSet);

					// Check overlap dates with records with same key
					for(Id id1 : idSet){
						BI_TM_Blacklist_account__c ba1 = (BI_TM_Blacklist_account__c)newItems.get(id1);
						for(Id id2 : idSetCopy){
							if(id1 != id2){
								BI_TM_Blacklist_account__c ba2 = ((BI_TM_Blacklist_account__c)newItems.get(id2));

								if(((ba1.BI_TM_Start_Date__c >= ba2.BI_TM_Start_Date__c) && (ba2.BI_TM_End_Date__c != null ? ba1.BI_TM_Start_Date__c <= ba2.BI_TM_End_Date__c : true)) ||
											((ba1.BI_TM_Start_Date__c <= ba2.BI_TM_Start_Date__c) && (ba1.BI_TM_End_Date__c != null ? ba1.BI_TM_End_Date__c >= ba2.BI_TM_Start_Date__c : true)) ||
												((ba1.BI_TM_End_Date__c != null && ba2.BI_TM_End_Date__c != null) ? ((ba1.BI_TM_End_Date__c <= ba2.BI_TM_End_Date__c) ? (ba1.BI_TM_End_Date__c >= ba2.BI_TM_Start_Date__c) : (ba1.BI_TM_Start_Date__c <= ba2.BI_TM_End_Date__c)) : false)){
									ba1.addError('You are trying to insert records with overlapped dates - Record 1: ' + ba1.Id + ' : SD - ' + ba1.BI_TM_Start_Date__c + ' : ED - ' + ba1.BI_TM_End_Date__c + ', Record 2: ' + ba2.Id + ' : SD - ' + ba2.BI_TM_Start_Date__c + ' : ED - ' + ba2.BI_TM_End_Date__c);

									Set<Id> keySetOverlap2remove = keyMapNewItemsNoOverlap.get(key);
									if(keySetOverlap2remove.contains(ba1.Id)){
										keySetOverlap2remove.remove(ba1.Id);
										keyMapNewItemsNoOverlap.put(key, keySetOverlap2remove);
									}
								}
							}
						}
					}
				}
			}
		}

		// Check overlap dates with the records in the system
		if(keyMapNewItemsNoOverlap != null){
			Map<Id, BI_TM_Blacklist_account__c> pos2ProdMapSystem = new Map<Id, BI_TM_Blacklist_account__c>([SELECT Id, BI_TM_Key__c, BI_TM_Start_Date__c, BI_TM_End_Date__c FROM BI_TM_Blacklist_account__c WHERE Id NOT IN :newItems.keySet() AND BI_TM_Key__c IN :keyMapNewItemsNoOverlap.keySet()]);
			Map<String, Set<Id>> keyMapSystemItems = new Map<String, Set<Id>>();

			for(Id p2p : pos2ProdMapSystem.keySet()){
				//system.debug('Key :: ' + (terr2ProdMapSystem.get(t2p)).BI_TM_Key_Terr2Prod__c);
				String key = (pos2ProdMapSystem.get(p2p)).BI_TM_Key__c;
				if(keyMapSystemItems.get(key) != null){
					Set<Id> idSet = keyMapSystemItems.get(key);
					idSet.add(p2p);
					keyMapSystemItems.put(key, idSet);
				}
				else{
					Set<Id> idSet = new Set<Id>();
					idSet.add(p2p);
					keyMapSystemItems.put(key, idSet);
				}
			}

			for(String key : keyMapNewItemsNoOverlap.keySet()){
				if(keyMapNewItemsNoOverlap.get(key) != null && keyMapSystemItems.get(key) != null){
					Set<Id> idSet = keyMapNewItemsNoOverlap.get(key);
					Set<Id> idSetSystem = keyMapSystemItems.get(key);

					// Check overlap dates with records with same key in the system
					for(Id id1 : idSet){
						BI_TM_Blacklist_account__c ba1 = (BI_TM_Blacklist_account__c)newItems.get(id1);
						for(Id id2 : idSetSystem){
							if(id1 != id2){
								BI_TM_Blacklist_account__c ba2 = pos2ProdMapSystem.get(id2);
								if((((ba1.BI_TM_Start_Date__c >= ba2.BI_TM_Start_Date__c) && (ba2.BI_TM_End_Date__c != null ? ba1.BI_TM_Start_Date__c <= ba2.BI_TM_End_Date__c : true)) ||
											((ba1.BI_TM_Start_Date__c <= ba2.BI_TM_Start_Date__c) && (ba1.BI_TM_End_Date__c != null ? ba1.BI_TM_End_Date__c >= ba2.BI_TM_Start_Date__c : true) ||
												((ba1.BI_TM_End_Date__c != null && ba2.BI_TM_End_Date__c != null) ?
													((ba1.BI_TM_End_Date__c <= ba2.BI_TM_End_Date__c) ? ba1.BI_TM_End_Date__c >= ba2.BI_TM_Start_Date__c : ba1.BI_TM_Start_Date__c <= ba2.BI_TM_End_Date__c) : false)))){
									ba1.addError('There are already records in the system that overlapped the dates - Record New: ' + ba1.Id + ' : SD - ' + ba1.BI_TM_Start_Date__c + ' : ED - ' + ba1.BI_TM_End_Date__c + ', Existing record: ' + ba2.Id + ' : SD - ' + ba2.BI_TM_Start_Date__c + ' : ED - ' + ba2.BI_TM_End_Date__c);
								}
							}
						}
					}
				}
			}
		}
	}
}