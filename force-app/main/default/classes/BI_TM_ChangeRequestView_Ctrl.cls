/**
 * Get all approval process related to
 *  Geo to terr
 *  User to Pos
 *  Manual assignments
 */
public with sharing class BI_TM_ChangeRequestView_Ctrl {

  public static final String GEO_TO_TERR_TYPE = 'BI_TM_Geography_to_territory__c';
  public static final String USER_TO_POS_TYPE = 'BI_TM_User_territory__c';
  public static final String MANUAL_ASSIG_TYPE = 'BI_TM_Account_To_Territory__c';

  public static final Integer COLLECTION_MAXROWS = 50;

  static final String DATASTEWART_PERMISSIONSET = 'BI_TM_DataSteward';

  public static Map<String,String> PROCESS_STATUS_VALUES_MAP = new Map<String,String> {
    ' ' => 'All',
    'Pending' => 'Pending',
    'Approved' => 'Approved',
    'Rejected' => 'Rejected'
  };

  public static final Map<String,String> DATE_FILTER_VALUES_MAP = new Map<String,String> {
    ' ' => 'All',
    '= THIS_WEEK' => 'This week',
    '>= LAST_WEEK' => 'From the last week',
    '= THIS_MONTH' => 'This month',
    '>= LAST_MONTH' => 'From the last month',
    '= THIS_QUARTER' => 'This quarter',
    '= THIS_YEAR' => 'This year'
  };

  public User currentUsr {
    get {
      if (this.currentUsr == null) {
          this.currentUsr = [SELECT Id, Country_Code_BI__c, Business_BI__c FROM User WHERE Id = :Userinfo.getUserId()];
      }
      return this.currentUsr;
    }
    set;
  }

  public Boolean showGeoToTer {
    get{
      return (!'PM'.equals(this.currentUsr.Business_BI__c));
    }
    set;
  }

  public Boolean showUserToPos {
    get{
      return true;
    }
    set;
  }

  public Boolean showManualAss {
    get{
      return (!'PM'.equals(this.currentUsr.Business_BI__c));
    }
    set;
  }

  private Boolean isDataStewart {
    get {
      if (this.isDataStewart == null) {
        this.isDataStewart = false;
        PermissionSet dataStewartPS = [SELECT Id, Name FROM PermissionSet WHERE Name = :DATASTEWART_PERMISSIONSET];
        List<PermissionSetAssignment> psAss = [SELECT Id FROM PermissionSetAssignment WHERE PermissionSetId = :dataStewartPS.Id AND AssigneeId = :UserInfo.getUserId()];
        if (psAss != null && !psAss.isEmpty()) {
          this.isDataStewart = true;
        }
      }
      return this.isDataStewart;
    }
    set;
  }

  // Filters
  public String statusFltr {
    get {
      if (this.statusFltr == null) {
        this.statusFltr = 'Pending';
      }
      return this.statusFltr;
    }
    set;
  }
  public String createdDateFltr {
    get {
      if (this.createdDateFltr == null) {
        this.createdDateFltr = '= THIS_WEEK';
      }
      return this.createdDateFltr;
    }
    set;
  }
  public String completedDateFltr {
    get {
      if ('Pending'.equals(this.statusFltr)) {
        this.completedDateFltr = '';
      }
      return this.completedDateFltr;
    }
    set;
  }

  public List<SelectOption> statusValues {
    get {
      List<SelectOption> options = new List<SelectOption>();
      for (String statusKey : PROCESS_STATUS_VALUES_MAP.keyset()) {
        options.add(new SelectOption(statusKey,PROCESS_STATUS_VALUES_MAP.get(statusKey)));
      }
      return options;
    }
    set;
  }

  public List<SelectOption> createdDateValues {
    get {
      List<SelectOption> options = new List<SelectOption>();
      for (String dateKey : DATE_FILTER_VALUES_MAP.keyset()) {
        options.add(new SelectOption(dateKey,DATE_FILTER_VALUES_MAP.get(dateKey)));
      }
      return options;
    }
    set;
  }

  public List<SelectOption> completedDateValues {
    get {
      List<SelectOption> options = new List<SelectOption>();
      for (String dateKey : DATE_FILTER_VALUES_MAP.keyset()) {
        options.add(new SelectOption(dateKey,DATE_FILTER_VALUES_MAP.get(dateKey)));
      }
      return options;
    }
    set;
  }

  public Boolean disableCompDate {
    get {
      return ('Pending'.equals(this.statusFltr));
    }
    set;
  }

  public List<TableRow> geoToTerrRelProcess {
    get {
      System.debug('geoToTerrRelProcess');
      return (this.tableData.containsKey(GEO_TO_TERR_TYPE)) ? this.tableData.get(GEO_TO_TERR_TYPE) : null;
    }
    set;
  }

  public List<TableRow> userToPosRelProcess {
    get {
      System.debug('userToPosRelProcess');
      return (this.tableData.containsKey(USER_TO_POS_TYPE)) ? this.tableData.get(USER_TO_POS_TYPE) : null;
    }
    set;
  }

  public List<TableRow> manualAssRelProcess {
    get {
      System.debug('manualAssRelProcess');
      return (this.tableData.containsKey(MANUAL_ASSIG_TYPE)) ? this.tableData.get(MANUAL_ASSIG_TYPE) : null;
    }
    set;
  }

  public void manualAssGoPrevious() {
    System.debug('manualAssGoPrevious');
    Integer currPage = this.currentPageMap.get(MANUAL_ASSIG_TYPE);
    if (currPage > 0) {
      this.currentPageMap.put(MANUAL_ASSIG_TYPE, currPage - 1);
    }
    this.tableData = null;
  }

  public void manualAssGoNext() {
    System.debug('manualAssGoNext');

    Integer currPage = this.currentPageMap.get(MANUAL_ASSIG_TYPE);
    Integer rowCounter = this.rowCounterMap.get(MANUAL_ASSIG_TYPE);
    Integer actualRows = (currPage + 1) * COLLECTION_MAXROWS;
    if (actualRows < rowCounter) {
      this.currentPageMap.put(MANUAL_ASSIG_TYPE, currPage + 1);
    }

    this.tableData = null;
  }

  public void geoToTerrGoPrevious() {
    System.debug('geoToTerrGoPrevious');
    Integer currPage = this.currentPageMap.get(GEO_TO_TERR_TYPE);
    if (currPage > 0) {
      this.currentPageMap.put(GEO_TO_TERR_TYPE, currPage - 1);
    }
    this.tableData = null;
  }

  public void geoToTerrGoNext() {
    System.debug('geoToTerrGoNext');

    Integer currPage = this.currentPageMap.get(GEO_TO_TERR_TYPE);
    Integer rowCounter = this.rowCounterMap.get(GEO_TO_TERR_TYPE);
    Integer actualRows = (currPage + 1) * COLLECTION_MAXROWS;
    if (actualRows < rowCounter) {
      this.currentPageMap.put(GEO_TO_TERR_TYPE, currPage + 1);
    }

    this.tableData = null;
  }

  public void usrToPosGoPrevious() {
    System.debug('usrToPosGoPrevious');
    Integer currPage = this.currentPageMap.get(USER_TO_POS_TYPE);
    if (currPage > 0) {
      System.debug('Navigate Down');
      this.currentPageMap.put(USER_TO_POS_TYPE, currPage - 1);
    }
    System.debug(this.currentPageMap);
    this.tableData = null;
  }

  public void usrToPosGoNext() {
    System.debug('usrToPosGoNext');
    System.debug(this.rowCounterMap);
    Integer currPage = this.currentPageMap.get(USER_TO_POS_TYPE);
    Integer rowCounter = this.rowCounterMap.get(USER_TO_POS_TYPE);
    System.debug('currPage=' + currPage);
    System.debug('rowCounter=' + rowCounter);
    Integer actualRows = (currPage + 1) * COLLECTION_MAXROWS;
    if (actualRows < rowCounter) {
      System.debug('Navigate Up');
      this.currentPageMap.put(USER_TO_POS_TYPE, currPage + 1);
    }

    System.debug(this.currentPageMap);
    this.tableData = null;
  }

  public Map<String, Boolean> goNextMap {
    get {
      System.debug('goNextMap');
      System.debug(this.goNextMap);
      if (this.goNextMap == null) {
        this.goNextMap = new Map<String, Boolean> {MANUAL_ASSIG_TYPE => true, USER_TO_POS_TYPE => true, GEO_TO_TERR_TYPE => true};
      }

      return this.goNextMap;
    }
    set;
  }

  public Map<String, Integer> rowCounterMap {
    get {
      System.debug(this.rowCounterMap);
      if (this.rowCounterMap == null) {
        this.rowCounterMap = new Map<String, Integer> {MANUAL_ASSIG_TYPE => 0, USER_TO_POS_TYPE => 0, GEO_TO_TERR_TYPE => 0};
      }

      return this.rowCounterMap;
    }
    set;
  }
  public Map<String, Integer> currentPageMap {
    get {
      if (this.currentPageMap == null) {
        this.currentPageMap = new Map<String, Integer> {MANUAL_ASSIG_TYPE => 0, USER_TO_POS_TYPE => 0, GEO_TO_TERR_TYPE => 0};
      }
      return this.currentPageMap;
    }
    set;
  }
  public Map<String, Integer> totalPageMap {
    get {
      totalPageMap = new Map<String, Integer> {MANUAL_ASSIG_TYPE => 0, USER_TO_POS_TYPE => 0, GEO_TO_TERR_TYPE => 0};
      if (rowCounterMap != null) {
        for (String forObjType : totalPageMap.keyset()) {
          Integer totRows = rowCounterMap.get(forObjType);
          Integer totalPage = totRows / COLLECTION_MAXROWS;
          totalPage += (Math.mod(totRows,COLLECTION_MAXROWS) != 0) ? 1 : 0;
          totalPageMap.put(forObjType, totalPage);
        }

      }
      return totalPageMap;
    }
    set;
  }

  public Map<String, List<TableRow>> tableData {
    get {
      If (this.tableData == null || this.tableData.isEmpty()) {
        System.debug('Set tableData');
        this.tableData = new Map<String, List<TableRow>>();
        Map<String, List<TableRow>> tmpDataMap = setRelatedProcess (this.statusFltr, this.createdDateFltr, this.completedDateFltr);

        Set<String> objectTypes = new Set<String>{MANUAL_ASSIG_TYPE,USER_TO_POS_TYPE,GEO_TO_TERR_TYPE};
        // Check for pagination
        for (String forObjType : objectTypes) {
          System.debug('Object type ' + forObjType);
          if (tmpDataMap.containsKey(forObjType)) {
            List<TableRow> tmpRows = tmpDataMap.get(forObjType);

            Integer currentPage = this.currentPageMap.get(forObjType);
            Integer rowCounter = tmpRows.size();

            this.rowCounterMap.put(forObjType, rowCounter);
            this.tableData.put(forObjType, new List<TableRow>());

            Integer startIdx = currentPage * COLLECTION_MAXROWS;
            Integer endIdx = ((currentPage + 1) * COLLECTION_MAXROWS < rowCounter) ? (currentPage + 1) * COLLECTION_MAXROWS : rowCounter;

            System.debug('diseable go next: ' + (endIdx == rowCounter));
            this.goNextMap.put(forObjType, !(endIdx == rowCounter));

            System.debug('rows: ' + tmpRows);
            System.debug('rowCounter: ' + rowCounter);
            System.debug('startIdx=' + startIdx);
            System.debug('endIdx=' + endIdx);
            for (Integer idx = startIdx; idx < endIdx; idx++) {
              this.tableData.get(forObjType).add(tmpRows.get(idx));
              System.debug(this.tableData.get(forObjType));
            }

          }
          System.debug(this.tableData.get(forObjType));
        }

      }

      return this.tableData;
    }
    set;
  }

  public BI_TM_ChangeRequestView_Ctrl() {
    //this.tableData = setRelatedProcess(this.statusFltr, this.createdDateFltr, this.completedDateFltr);
  }


  /**
   *  Select GeoToTerr created (? and the updated ones)
   */
    public List<BI_TM_Geography_to_territory__c> approvedGeoTerr{
        get{
            return [Select Id, Name, CreatedBy.Name, BI_TM_Parent_alignment__r.Name, BI_TM_Country_Code__c, BI_TM_Geography__r.Name, BI_TM_Territory_ND__r.Name From BI_TM_Geography_to_territory__c Where CreatedById =: UserInfo.getUserId()];
        }
        set;
    }

    public class TableRow {
      public String targetobjectId {get;set;}
      public String targetobjectType {get;set;}

      public List<ProcessInstance> processList {get;set;}

      public BI_TM_Geography_to_territory__c geoToTerr {get;set;}
      public BI_TM_User_territory__c userToTerr {get;set;}
      public BI_TM_User_territory__c oldUserToTerr {get;set;} // Info of the user to pos going to deactivate
      public BI_TM_Account_To_Territory__c manualAss {get;set;}
      public BI_TM_Account_To_Territory__c oldManualAss {get;set;}

      public tableRow(String targetobjectId, String targetobjectType, List<ProcessInstance> processList) {
        this.targetobjectId =targetobjectId;
        this.processList = processList;
        this.targetobjectType = targetobjectType;
      }

    }

    public void refreshTables() {
      System.debug('refreshTables');
      this.tableData = null;

      this.currentPageMap = null;
      this.rowCounterMap = null;
      this.goNextMap = null;
      this.totalPageMap = null;

    }

    private Map<String, List<TableRow>> setRelatedProcess (String statusFltr, String createdDateFltr, String completedDateFltr) {
      System.debug('setRelatedProcess');
      String userId = UserInfo.getUserId();
      String userCountryCode = this.currentUsr.Country_Code_BI__c;

      String sqlStr = 'SELECT Id, CreatedDate, CompletedDate, LastActorId, Status, TargetObjectId, Targetobject.Type, ' +
                      ' (SELECT Id, ProcessInstanceId, OriginalActorId, OriginalActor.name FROM Steps ORDER By ElapsedTimeInMinutes ASC) ' +
                      ' FROM ProcessInstance ';
      sqlStr += 'WHERE Targetobject.Type IN (' +
        '\'' + GEO_TO_TERR_TYPE + '\',\'' + USER_TO_POS_TYPE + '\',\'' + MANUAL_ASSIG_TYPE + '\'' +
      ') ';

      sqlStr += (!isDataStewart) ? ' AND SubmittedById = :userId ' : '';

      // status = '' for Select all
      sqlStr += (String.isNotBlank(statusFltr)) ?  ' AND Status = :statusFltr' : '';
      sqlStr += (String.isNotBlank(createdDateFltr)) ?  ' AND CreatedDate ' + createdDateFltr : '';
      sqlStr += (String.isNotBlank(completedDateFltr)) ?  ' AND CompletedDate ' + completedDateFltr : '';

      sqlStr += ' ORDER BY CreatedDate DESC ';

      Set<Id> geoToTerIds = new Set<Id>();
      Set<Id> userToPosIds = new Set<Id>();
      Set<Id> manualAssIds = new Set<Id>();
      Map<String, List<TableRow>> retMap = new Map<String, List<TableRow>> {
        GEO_TO_TERR_TYPE => new List<TableRow>(),
        USER_TO_POS_TYPE => new List<TableRow>(),
        MANUAL_ASSIG_TYPE => new List<TableRow>()
      };
      Map<Id, List<ProcessInstance>> procListByTargetId = new Map<Id, List<ProcessInstance>>();

      System.debug('statusFltr=' + statusFltr);
      System.debug('createdDateFltr=' + createdDateFltr);
      System.debug('completedDateFltr=' + completedDateFltr);
      System.debug('userId=' + userId);
      System.debug(sqlStr);
      for (ProcessInstance forProc : Database.query(sqlStr)) {
        if (!procListByTargetId.containsKey(forProc.TargetobjectId)) {
          procListByTargetId.put(forProc.TargetobjectId, new List<ProcessInstance>());
        }
        procListByTargetId.get(forProc.TargetobjectId).add(forProc);

        if (GEO_TO_TERR_TYPE.equals(forProc.Targetobject.Type)) {
          geoToTerIds.add(forProc.TargetobjectId);
          System.debug(forProc.TargetobjectId);
        } else if (USER_TO_POS_TYPE.equals(forProc.Targetobject.Type)) {
          userToPosIds.add(forProc.TargetobjectId);
        } else if (MANUAL_ASSIG_TYPE.equals(forProc.Targetobject.Type)) {
          manualAssIds.add(forProc.TargetobjectId);
        }
      }

      if (!geoToTerIds.isEmpty()) {
        Map<Id, BI_TM_Geography_to_territory__c> geoToTerrInfo = getGeoToTerr(geoToTerIds);
        for (Id forObjId : geoToTerIds) {
          TableRow row = new TableRow(forObjId, GEO_TO_TERR_TYPE, procListByTargetId.get(forObjId));
          if (geoToTerrInfo.containsKey(forObjId)) {
              row.geoToTerr = geoToTerrInfo.get(forObjId);
          }
          retMap.get(GEO_TO_TERR_TYPE).add(row);
        }

      }
      if (!userToPosIds.isEmpty()) {
        Map<Id, BI_TM_User_territory__c> userPosInfo = getUserToPos(userToPosIds);
        Integer counter = 0;
        for (Id forObjId : userToPosIds) {

          TableRow row = new TableRow(forObjId, USER_TO_POS_TYPE, procListByTargetId.get(forObjId));
          if (userPosInfo.containsKey(forObjId)) {
            row.userToTerr = userPosInfo.get(forObjId);
            //System.debug('row.userToTerr ' + row.userToTerr);
            if (userPosInfo.get(forObjId).BI_TM_OldPosition__c != null
              && userPosInfo.containsKey(userPosInfo.get(forObjId).BI_TM_OldPosition__c)
            ) {
              row.oldUserToTerr = userPosInfo.get(userPosInfo.get(forObjId).BI_TM_OldPosition__c);
              //System.debug('row.oldUserToTerr ' + row.oldUserToTerr);
            }
          }
          retMap.get(USER_TO_POS_TYPE).add(row);
        }

      }
      if (!manualAssIds.isEmpty()) {
        Map<Id, BI_TM_Account_To_Territory__c> manualAssInfo = getManualAss(manualAssIds);
        for (Id forObjId : manualAssIds) {
          TableRow row = new TableRow(forObjId, MANUAL_ASSIG_TYPE, procListByTargetId.get(forObjId));
          if (manualAssInfo.containsKey(forObjId)) {
            row.manualAss = manualAssInfo.get(forObjId);
            if (manualAssInfo.get(forObjId).BI_TM_old_manualAssignment__c != null
              && manualAssInfo.containsKey(manualAssInfo.get(forObjId).BI_TM_old_manualAssignment__c)
            ) {
              row.oldManualAss = manualAssInfo.get(manualAssInfo.get(forObjId).BI_TM_old_manualAssignment__c);
            }
          }
          retMap.get(MANUAL_ASSIG_TYPE).add(row);
        }

      }

      System.debug(retMap);
      return retMap;
    }

    public Map<Id, BI_TM_Geography_to_territory__c> getGeoToTerr(Set<Id> ids){
      return new Map<Id, BI_TM_Geography_to_territory__c> (
        [SELECT Id, Name, BI_TM_Status__c, BI_TM_Parent_alignment__r.Name, BI_TM_Geography__c, BI_TM_Geography__r.Name, BI_TM_Territory_ND__c, BI_TM_Territory_ND__r.Name
          FROM BI_TM_Geography_to_territory__c WHERE Id IN :ids]);
    }

    /**
     *  Return BI_TM_User_territory__c object information for the passed ids.
     *  If old position field is filled, related info is also added.
     */
    public Map<Id, BI_TM_User_territory__c> getUserToPos(Set<Id> ids){
      System.debug(ids);
      Map<Id, BI_TM_User_territory__c> retMap = new Map<Id, BI_TM_User_territory__c> (
        [SELECT Id, Name, BI_TM_Territory1__c, BI_TM_Territory1__r.Name, BI_TM_OldPosition__c,
          BI_TM_User_mgmt_tm__c, BI_TM_User_mgmt_tm__r.Name,
          BI_TM_Assignment_Type__c, BI_TM_Start_date__c, BI_TM_End_date__c
            FROM BI_TM_User_territory__c WHERE Id IN :ids]
      );

      Set<Id> oldUserPosId = new Set<Id>();
      for (Id forUserPosId : retMap.keySet()) {
        if (retMap.get(forUserPosId).BI_TM_OldPosition__c != null) {
          oldUserPosId.add(retMap.get(forUserPosId).BI_TM_OldPosition__c);
        }
      }

      for (BI_TM_User_territory__c forUserPos :
        [SELECT Id, Name, BI_TM_Territory1__c, BI_TM_Territory1__r.Name, BI_TM_OldPosition__c,
          BI_TM_User_mgmt_tm__c, BI_TM_User_mgmt_tm__r.Name,
          BI_TM_Assignment_Type__c, BI_TM_Start_date__c, BI_TM_End_date__c
            FROM BI_TM_User_territory__c WHERE Id IN :oldUserPosId]
      ) {
        retMap.put(forUserPos.Id, forUserPos);
      }

      return retMap;
    }

    public Map<Id, BI_TM_Account_To_Territory__c> getManualAss(Set<Id> ids){
      Map<Id, BI_TM_Account_To_Territory__c> retMap = new Map<Id, BI_TM_Account_To_Territory__c> (
        [SELECT Id, BI_TM_ActiveForumla__c, BI_TM_Account__c, BI_TM_Account__r.Name, Name, BI_TM_HCP_HCO__c,
          BI_TM_Territory_FF_Hierarchy_Position__c, BI_TM_Territory_FF_Hierarchy_Position__r.Name, BI_TM_old_manualAssignment__c, BI_TM_Start_Date__c, BI_TM_End_Date__c
          FROM BI_TM_Account_To_Territory__c WHERE Id IN :ids]
      );

      Set<Id> oldAccTerrId = new Set<Id>();
      for (Id forAccToTerrId : retMap.keySet()) {
        if (retMap.get(forAccToTerrId).BI_TM_old_manualAssignment__c != null) {
          oldAccTerrId.add(retMap.get(forAccToTerrId).BI_TM_old_manualAssignment__c);
        }
      }

      for (BI_TM_Account_To_Territory__c forManualAss :
        [SELECT Id, BI_TM_ActiveForumla__c, BI_TM_Account__c, BI_TM_Account__r.Name, Name, BI_TM_HCP_HCO__c,
          BI_TM_Territory_FF_Hierarchy_Position__c, BI_TM_Territory_FF_Hierarchy_Position__r.Name, BI_TM_old_manualAssignment__c, BI_TM_Start_Date__c, BI_TM_End_Date__c
          FROM BI_TM_Account_To_Territory__c WHERE Id IN :oldAccTerrId]
      ) {
        retMap.put(forManualAss.Id, forManualAss);
      }

      return retMap;
    }

}