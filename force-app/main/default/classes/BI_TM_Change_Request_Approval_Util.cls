/**
 *  Bitman Approval Workflow core class with all required logic and utilities to initiate, approve and escalate the approval processes for Bitman Change Request object.
 *
 * @author    Sergio Ruiz
 * @created   2018-07-12
 * @version   1.0
 * @since     43.0 (Force.com ApiVersion)
 *
 * @changelog
 * 2018-07-12 V1.0 Sergio Ruiz - Created
 */
 public without sharing class BI_TM_Change_Request_Approval_Util {

    public static final String SOURCE_APPROVAL_PROCESS_NAME = 'BI_TM_Source_Approval';
    public static final String TARGET_APPROVAL_PROCESS_NAME = 'BI_TM_Target_Approval';

    private static final Boolean TARGET = true;
    private static final Boolean SOURCE = false;

    /**
     * initApprovalWorkflow
     *
     * ...
     *
     * @param      items  The items
     *
     * @return     nothing
     */
     public static void initApprovalWorkflow (List<BI_TM_Change_Request__c> items) {
        if (items == null || items.isEmpty()) {
            System.debug('No change request to be processed');
            return;
        }

        Map<Id, BI_TM_Change_Request__c> crMap = reloadRequiredChangeRequests(items);

        prepareData(crMap.values());

        List<BI_TM_Change_Request__c> toUpdate = new List<BI_TM_Change_Request__c>();

        Map<String, List<Id>> ffMap = getFieldForceTypeMap(crMap.keySet(), BI_TM_US_FieldForce_Map__c.getAll().values());
        System.debug('BI_TM_UserToPosition_Approval_Handler - processRecordsNew -> ffMap: ' + ffMap);

        Map<Id, Map<String, List<Id>>> positionsMap = new Map<Id, Map<String, List<Id>>>();
        for (String ff : ffMap.keySet()) {
            if (approveAutomatically(ff)) {
                for (Id crId : ffMap.get(ff)) {
                    BI_TM_Change_Request__c cr = crMap.get(crId);
                    cr.BI_TM_Approval_Status__c = 'Approved';
                    toUpdate.add(cr);
                }
            } else {
                Integer levels = getRequiredLevels(ff);
                if (levels > 0) {
                    setPositionsToMap(positionsMap, getFieldNamesForTarget(levels), ffMap.get(ff), 'Target');
                    setPositionsToMap(positionsMap, getFieldNamesForSource(levels), ffMap.get(ff), 'Source');
                }
            }
        }
        System.debug('BI_TM_UserToPosition_Approval_Handler - processRecordsNew -> positionsMap: ' + positionsMap);

        List<Id> allPositions = new List<Id> ();
        for (Map<String, List<Id>> m : positionsMap.values())
        for (List<Id> l : m.values())
        allPositions.addAll(l);
        Map<Id, Id> position2user = getPosition2UserMap(allPositions);
        System.debug('BI_TM_UserToPosition_Approval_Handler - processRecordsNew -> position2user: ' + position2user);

        for (Id crId : crMap.keySet()) {
            Map<String, List<Id>> allCRPositions = positionsMap.get(crId);
            BI_TM_Change_Request__c cr = crMap.get(crId);
            if (allCRPositions != null && !allCRPositions.isEmpty()) {
                System.debug('BI_TM_UserToPosition_Approval_Handler - processRecordsNew -> allCRPositions: ' + allCRPositions);
                List<Id> targetPositions = allCRPositions.get('Target');
                if (targetPositions != null) {
                   System.debug('BI_TM_UserToPosition_Approval_Handler - processRecordsNew -> targetPositions: ' + targetPositions);
                   if (targetPositions.size() > 0) cr.BI_TM_Target_Approver_1__c = position2user.get(targetPositions.get(0));
                   if (targetPositions.size() > 1) cr.BI_TM_Target_Approver_2__c = position2user.get(targetPositions.get(1));
                   if (targetPositions.size() > 2) cr.BI_TM_Target_Approver_3__c = position2user.get(targetPositions.get(2));
               }

                List<Id> sourcePositions = allCRPositions.get('Source');
                if (sourcePositions != null) {
                    if (sourcePositions.size() > 0) cr.BI_TM_Source_Approver_1__c = position2user.get(sourcePositions.get(0));
                    if (sourcePositions.size() > 1) cr.BI_TM_Source_Approver_2__c = position2user.get(sourcePositions.get(1));
                    if (sourcePositions.size() > 2) cr.BI_TM_Source_Approver_3__c = position2user.get(sourcePositions.get(2));
                }

                if (toUpdate.indexOf(cr)==-1) {
                    cr.BI_TM_Approval_Status__c = 'Approval Pending';
                    toUpdate.add(cr);
                }
            }
        }

        if (!toUpdate.isEmpty())
        update toUpdate;

        System.debug('ProcessRecordsNew - toUpdate: ' + toUpdate);

        Approval.ProcessSubmitRequest [] requests = new List<Approval.ProcessSubmitRequest>();
        for (Id crId : crMap.keySet()) {
            BI_TM_Change_Request__c cr = crMap.get(crId);

            if (String.isNotBlank(cr.BI_TM_Source_Approver_1__c))
            requests.add(createApproval(cr, SOURCE_APPROVAL_PROCESS_NAME));

            if (String.isNotBlank(cr.BI_TM_Target_Approver_1__c))
            requests.add(createApproval(cr, TARGET_APPROVAL_PROCESS_NAME));
        }

        if (!requests.isEmpty())
        Approval.ProcessResult [] results = Approval.process(requests);
    }


    /**
     * { function_description }
     *
     * @param      items  The items
     */
     private static void prepareData (List<BI_TM_Change_Request__c> items) {
        // Territories

        List<Id> territories = new List<Id> ();
        for (BI_TM_Change_Request__c cr : items)
            if (String.isNotBlank(cr.BI_TM_Territory__c))
                territories.add(cr.BI_TM_Territory__c);

        Map<Id, Id> terrToPosition = new Map<Id, Id> ();
        BI_TM_Territory_ND__c [] terrs = [SELECT Id, (SELECT Id FROM positions__r WHERE BI_TM_Is_Active__c = true LIMIT 1) FROM BI_TM_Territory_ND__c WHERE Id IN :territories];
        for (BI_TM_Territory_ND__c t: terrs)
            for (BI_TM_Territory__c p : t.positions__r)
                terrToPosition.put(t.Id, p.Id);

        for (BI_TM_Change_Request__c cr : items)
            if (String.isNotBlank(cr.BI_TM_Territory__c))
                if (terrToPosition.containsKey(cr.BI_TM_Territory__c))
                    cr.BI_TM_Position__c = terrToPosition.get(cr.BI_TM_Territory__c);

        // User 2 Position

        List<Id> u2ps = new List<Id> ();
        for (BI_TM_Change_Request__c cr : items)
            if (String.isNotBlank(cr.BI_TM_User_to_Position__c))
                u2ps.add(cr.BI_TM_User_to_Position__c);
        System.debug('U2Ps: ' + u2ps);

        Map<Id, Id> u2pToPosition = new Map<Id, Id> ();
        for (BI_TM_User_territory__c u2p: [SELECT Id, BI_TM_Territory1__c FROM BI_TM_User_territory__c WHERE Id IN :u2ps])
            u2pToPosition.put(u2p.Id, u2p.BI_TM_Territory1__c);
                System.debug('u2pToPosition: ' + u2pToPosition);

        for (BI_TM_Change_Request__c cr : items)
            if (String.isNotBlank(cr.BI_TM_User_to_Position__c))
                if (u2pToPosition.containsKey(cr.BI_TM_User_to_Position__c))
                    cr.BI_TM_Source_Position__c = u2pToPosition.get(cr.BI_TM_User_to_Position__c);

        // Manual Assigment

        List<Id> manualAssigments = new List<Id> ();
        for (BI_TM_Change_Request__c cr : items)
            if (String.isNotBlank(cr.BI_TM_Manual_Assignment__c))
                manualAssigments.add(cr.BI_TM_Manual_Assignment__c);

        Map<Id, Id> maToPosition = new Map<Id, Id> ();
        for (BI_TM_Account_To_Territory__c ma: [SELECT Id, BI_TM_Territory_FF_Hierarchy_Position__c FROM BI_TM_Account_To_Territory__c WHERE Id IN :manualAssigments])
            maToPosition.put(ma.Id, ma.BI_TM_Territory_FF_Hierarchy_Position__c);

        for (BI_TM_Change_Request__c cr : items)
            if (String.isNotBlank(cr.BI_TM_Manual_Assignment__c))
                if (maToPosition.containsKey(cr.BI_TM_Manual_Assignment__c))
                    cr.BI_TM_Source_Position__c = maToPosition.get(cr.BI_TM_Manual_Assignment__c);

        // Geography 2 Territory

        List<Id> geo2terrs = new List<Id> ();
        for (BI_TM_Change_Request__c cr : items)
            if (String.isNotBlank(cr.BI_TM_Geography_to_Territory__c))
                geo2terrs.add(cr.BI_TM_Geography_to_Territory__c);

        Map<Id, Id> geo2terrToTerr = new Map<Id, Id> ();
        for (BI_TM_Geography_to_Territory__c g2t : [SELECT Id, BI_TM_Territory_ND__c FROM BI_TM_Geography_to_territory__c WHERE Id IN :geo2terrs])
            geo2terrToTerr.put(g2t.Id,g2t.BI_TM_Territory_ND__c);

        terrToPosition = new Map<Id, Id> ();
        for (BI_TM_Territory_ND__c t: [SELECT Id, (SELECT Id FROM positions__r WHERE BI_TM_Is_Active__c = true LIMIT 1) FROM BI_TM_Territory_ND__c WHERE Id IN :geo2terrToTerr.values()])
            for (BI_TM_Territory__c p : t.positions__r)
                terrToPosition.put(t.Id, p.Id);

        for (BI_TM_Change_Request__c cr : items)
            if (String.isNotBlank(cr.BI_TM_Geography_to_Territory__c))
                if (geo2terrToTerr.containsKey(cr.BI_TM_Geography_to_Territory__c)) {
                    Id terrId = geo2terrToTerr.get(cr.BI_TM_Geography_to_Territory__c);
                    if (terrId != null && terrToPosition.containsKey(terrId))
                    cr.BI_TM_Source_Position__c = terrToPosition.get(terrId);
                }

        // The first level for target and source positions should be 'Region', so it is required to calculate the first region position in the hierarchy
        getRegionPositions(items);

        update items;
    }

    /**
     * Gets the region positions.
     *
     * @param      items  The items
     */
    private static void getRegionPositions (BI_TM_Change_Request__c [] items) {
        List<Id> currentPositions = new List<Id> ();
        for (BI_TM_Change_Request__c cr : items) {
            if (cr.BI_TM_Position__c != null) {
                cr.BI_TM_Target_Position__c = cr.BI_TM_Position__c;
                currentPositions.add(cr.BI_TM_Position__c);
            }
            if (cr.BI_TM_Source_Position__c != null) currentPositions.add(cr.BI_TM_Source_Position__c);
        }
        while (!currentPositions.isEmpty()) {
            Map<Id, Id> searchMap = searchRegionPositions(currentPositions);
            currentPositions = new List<Id> ();
            for (BI_TM_Change_Request__c cr : items) {
                if (cr.BI_TM_Target_Position__c != null && searchMap.containsKey(cr.BI_TM_Target_Position__c)) {
                    Id parentPosition = searchMap.get(cr.BI_TM_Target_Position__c);
                    cr.BI_TM_Target_Position__c = parentPosition;
                    currentPositions.add(parentPosition);
                }
                if (cr.BI_TM_Source_Position__c != null && searchMap.containsKey(cr.BI_TM_Source_Position__c)) {
                    Id parentPosition = searchMap.get(cr.BI_TM_Source_Position__c);
                    cr.BI_TM_Source_Position__c = parentPosition;
                    currentPositions.add(parentPosition);
                }
            }
        }
    }

    /**
     * { function_description }
     *
     * @param      currentPositions  The current positions
     *
     * @return     { description_of_the_return_value }
     */
    private static Map<Id, Id> searchRegionPositions (List<Id> currentPositions) {
        Map<Id, Id> m = new Map<Id, Id> ();
        System.debug('searchRegionPositions - currentPositions: ' + currentPositions);
        BI_TM_Territory__c [] results = [SELECT Id, BI_TM_Position_Level__c, BI_TM_Parent_Position__c FROM BI_TM_Territory__c WHERE Id IN :currentPositions];
        System.debug('searchRegionPositions - Results: ' + results);
        for (BI_TM_Territory__c p : results) {
          if (p.BI_TM_Position_Level__c != 'Region' && p.BI_TM_Parent_Position__c != null)
          m.put(p.Id, p.BI_TM_Parent_Position__c);
      }
      return m;
    }

    /**
     * getFieldForceTypeMap
     *
     * ...
     *
     * @param crIds
     *
     * @return a map with field force - list of change request Ids for the field force
     */
     private static Map<String, List<Id>> getFieldForceTypeMap (Set<Id> crIds, BI_TM_US_FieldForce_Map__c [] usFFs) {
        Map<String, List<Id>> retMap = new Map<String, List<Id>> ();
        for (BI_TM_Change_Request__c cr : [SELECT Id, BI_TM_Field_Force__c FROM BI_TM_Change_Request__c WHERE Id IN :crIds]) {
            System.debug('***cr: ' + cr);
            String ff = getFieldForceType(cr.BI_TM_Field_Force__c, usFFs);
            System.debug('***ff: ' + ff);
            if (ff != null) {
                List<Id> ids = retMap.get(ff);
                if (ids == null)
                ids = new List<Id>();
                ids.add(cr.Id);
                retMap.put(ff, ids);
            }
        }
        return retMap;
    }

    /**
     * getFieldForceType
     *
     * ...
     *
     * @param fieldForceValue
     *
     * @return the field force
     */
     private static String getFieldForceType (String fieldForceValue, BI_TM_US_FieldForce_Map__c [] usFFs) {
        for (BI_TM_US_FieldForce_Map__c usFF : usFFs)
            if (usFF.BI_TM_USFF_Name__c.equals(fieldForceValue))
                return usFF.Name;
        return null;
    }

    /**
     * setPositionsToMap
     *
     * ...
     *
     * @param positionsMap
     * @param fieldNames
     * @param itemsIds
     * @param branch
     *
     * @return nothing
     */
     private static void setPositionsToMap(Map<Id, Map<String, List<Id>>> positionsMap, String [] fieldNames, List<Id> itemsIds, String branch) {
        String query = createPositionsQuery(fieldNames, itemsIds);
        System.debug('Query: ' + query);
        for (BI_TM_Change_Request__c cr : Database.query(query)) {
            System.debug('CR: ' + cr);
            List<Id> positions = new List<Id> ();
            for (String fieldName : fieldNames) {
                String value = getValue(cr, fieldName);
                if (value != null)
                positions.add(value);
            }
            if (!positions.isEmpty()) {
                Map<String, List<Id>> innerMap = positionsMap.get(cr.Id);
                if (innerMap == null)
                innerMap = new Map<String, List<Id>> { branch => positions};
                else
                innerMap.put(branch, positions);
                positionsMap.put(cr.Id, innerMap);
            }
        }
    }

    /**
     * getValue
     *
     * ...
     *
     * @param so
     * @param fieldName
     *
     * @return nothing
     */
     private static String getValue(SObject so, String fieldName) {
        System.debug('SO: ' + so);
        System.debug('FieldName: ' + fieldName);
        List<String> tokens = fieldName.split('\\.');
        System.debug('Tokens: ' + tokens);
        Integer lastTokenIndex = tokens.size() - 1;
        System.debug('lastTokenIndex: ' + lastTokenIndex);
        SObject tempSO = lastTokenIndex > 0 ? so.getSObject(tokens[0]) : so;
        if (tempSO == null)
        return null;
        System.debug('tempSO: ' + tempSO);
        for (Integer i = 1; i<lastTokenIndex;i++) {
            tempSO = tempSO.getSObject(tokens[i]);
            if (tempSO == null)
            return null;
            System.debug('tempSO: ' + tempSO);
        }
        return (String)tempSO.get(tokens[lastTokenIndex]);
    }

    /**
     * getRequiredLevels
     *
     * ...
     *
     * @param so
     * @param fieldName
     *
     * @return the levels of approval
     */
     private static Integer getRequiredLevels (String fieldForceType) {
        BI_TM_US_FieldForce_Map__c usFF = BI_TM_US_FieldForce_Map__c.getInstance(fieldForceType);
        System.debug('getRequiredLevels - usFF: ' + usFF);
        if (String.isNotBlank(usFF.BI_TM_USFF_L5__c)) return 5;
        if (String.isNotBlank(usFF.BI_TM_USFF_L4__c)) return 4;
        if (String.isNotBlank(usFF.BI_TM_USFF_L3__c)) return 3;
        if (String.isNotBlank(usFF.BI_TM_USFF_L2__c)) return 2;
        if (String.isNotBlank(usFF.BI_TM_USFF_L1__c)) return 1;
        return 0;
    }

    /**
     * { function_description }
     *
     * @param      fieldForceType  The field force type
     *
     * @return     { description_of_the_return_value }
     */
    private static Boolean approveAutomatically (String fieldForceType) {
        BI_TM_US_FieldForce_Map__c usFF = BI_TM_US_FieldForce_Map__c.getInstance(fieldForceType);
        System.debug('getRequiredLevels - usFF: ' + usFF);
        if (usFF == null)
            return false;
        return usFF.BI_TM_Automatically_Approved__c;
    }

    /**
     * Gets the field names for target.
     *
     * @param      levels  The levels
     *
     * @return     The field names for target.
     */
     private static String [] getFieldNamesForTarget (Integer levels) {
        String [] fieldNames = new List<String>();
        fieldNames.add('BI_TM_Target_Position__c');
        if (levels>1) fieldNames.add('BI_TM_Target_Position__r.BI_TM_Parent_Position__c');
        if (levels>2) fieldNames.add('BI_TM_Target_Position__r.BI_TM_Parent_Position__r.BI_TM_Parent_Position__c');
        return fieldNames;
    }

    /**
     * Gets the field names for source.
     *
     * @param      levels  The levels
     *
     * @return     The field names for source.
     */
     private static String [] getFieldNamesForSource (Integer levels) {
        String [] fieldNames = new List<String>();
        fieldNames.add('BI_TM_Source_Position__c');
        if (levels>1) fieldNames.add('BI_TM_Source_Position__r.BI_TM_Parent_Position__c');
        if (levels>2) fieldNames.add('BI_TM_Source_Position__r.BI_TM_Parent_Position__r.BI_TM_Parent_Position__c');
        return fieldNames;
    }

    /**
     * Creates a positions query.
     *
     * @param      fieldNames  The field names
     * @param      recordsIds  The records identifiers
     *
     * @return     { description_of_the_return_value }
     */
     private static String createPositionsQuery (String [] fieldNames, List<Id> recordsIds) {
        String query = 'SELECT Id';
        for (String fieldName : fieldNames)
        query += ', ' + fieldName;
        query += ' FROM BI_TM_Change_Request__c WHERE Id IN (\'' + String.join(recordsIds, '\',\'') + '\')';
        return query;
    }

    /**
     * { function_description }
     *
     * @param      items  The items
     *
     * @return     { description_of_the_return_value }
     */
     private static Map<Id, BI_TM_Change_Request__c> reloadRequiredChangeRequests (List<BI_TM_Change_Request__c> items) {
        List<Id> tempList = new List<Id> (new Map<Id, BI_TM_Change_Request__c> (items).keySet());
        //String whereClause = 'Id IN (\'' + String.join(items, '\',\'') + '\')';
        String query = getCreatableFieldsSOQL('BI_TM_Change_Request__c', 'Id IN (\'' + String.join(tempList, '\',\'') + '\')');
        List<BI_TM_Change_Request__c> l = Database.query(query);
        return new Map<Id, BI_TM_Change_Request__c> (l);
    }



    /**
     * Gets the position 2 user map.
     *
     * @param      positionIds  The position identifiers
     *
     * @return     The position 2 user map.
     */
     private static Map<Id, Id> getPosition2UserMap (List<Id> positionIds) {
        Map<Id, Id> mymap = new Map<Id, Id>();

        // Position to User map (only for those users whose assignment type is primary)
        for (BI_TM_User_territory__c u2p : [SELECT BI_TM_Territory1__c, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c FROM BI_TM_User_territory__c WHERE BI_TM_Assignment_Type__c = 'Primary' AND  BI_TM_Territory1__c IN :positionIds]) {
            Id userId = u2p.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c;
            if (userId != null)
            mymap.put(u2p.BI_TM_Territory1__c, userId);
        }

        return mymap;
    }


    /**
     * Creates an approval.
     *
     * @param      cr                   The carriage return
     * @param      approvalProcessName  The approval process name
     *
     * @return     { description_of_the_return_value }
     */
     private static Approval.ProcessSubmitRequest createApproval(BI_TM_Change_Request__c cr, String approvalProcessName) {
        Approval.ProcessSubmitRequest ar =new Approval.ProcessSubmitRequest();
        ar.setComments('Submitting request for approval.');
        ar.setObjectId(cr.Id);
        //ar.setNextApproverIds(new List<Id> { approver });
        ar.setSubmitterId(cr.CreatedById);
        ar.setSkipEntryCriteria(true);
        ar.setProcessDefinitionNameOrId(approvalProcessName);
        return ar;
    }

    /**
     * Gets the field value.
     *
     * @param      o      { parameter_description }
     * @param      field  The field
     *
     * @return     The field value.
     */
     private static Object getFieldValue(SObject o, String field){
        if(o == null){
            return null;
        }
        if(field.contains('.')) {
            String nextField = field.substringAfter('.');
            String relation = field.substringBefore('.');
            return getFieldValue((SObject)o.getSObject(relation),nextField);
            } else {
                return o.get(field);
            }
        }




    /**
     * Gets the creatable fields soql.
     *
     * @param      objectName   The object name
     * @param      whereClause  The where clause
     *
     * @return     The creatable fields soql.
     */
     public static string getCreatableFieldsSOQL(String objectName, String whereClause){
        String selects = '';
        if (whereClause == null || whereClause == ''){ return null; }

        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();

        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }

        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}

        }
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
    }

    /*public static void escalateApprovals (ProcessInstanceWorkitem [] piwis) {
        System.debug('escalateApproval - piwis: ' + piwis);
        Approval.ProcessWorkitemRequest [] requests = new List<Approval.ProcessWorkitemRequest>();
        for (ProcessInstanceWorkitem piwi : piwis) {
            System.debug('escalateApproval - piwi: ' + piwi);
            Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
            request.setComments('Escalation');
            request.setAction('Approve');
            request.setWorkitemId(piwi.Id);
            requests.add(request);
            System.debug('escalateApproval - request: ' + request);
        }
        Approval.ProcessResult [] results = Approval.process(requests);
        System.debug('escalateApproval - results: ' + results);
        }*/

    /**
     * { function_description }
     *
     * @param      piwi  The piwi
     */
     public static void escalateApproval (ProcessInstanceWorkitem piwi) {
        System.debug('escalateApproval - piwi: ' + piwi);
        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
        request.setComments('Escalation');
        request.setAction('Approve');
        request.setWorkitemId(piwi.Id);
        System.debug('escalateApproval - request: ' + request);

        // If something goes wrong after the approval ... let the rest of approval continue
        try {
            Approval.ProcessResult result = Approval.process(request);
            System.debug('escalateApproval - result: ' + result);
            } catch (Exception e) {
                System.debug ('escalateApproval - error while escalating: ' + e.getMessage());
            }
        }

    /**
     * Determines if fully approved.
     *
     * @param      cr    The carriage return
     *
     * @return     True if fully approved, False otherwise.
     */
     public static boolean isFullyApproved (BI_TM_Change_Request__c cr) {
        return ((requiresSource(cr) && cr.BI_TM_Last_Source_Approval_Recevied__c) || !requiresSource(cr)) &&
        ((requiresTarget(cr) && cr.BI_TM_Last_Target_Approval_Recevied__c) || !requiresTarget(cr));
    }

    /**
     * { function_description }
     *
     * @param      cr    The carriage return
     *
     * @return     { description_of_the_return_value }
     */
     private static boolean requiresSource (BI_TM_Change_Request__c cr) {
        return cr.BI_TM_Source_Approver_1__c != null;
    }

    /**
     * { function_description }
     *
     * @param      cr    The carriage return
     *
     * @return     { description_of_the_return_value }
     */
     private static boolean requiresTarget (BI_TM_Change_Request__c cr) {
        return cr.BI_TM_Target_Approver_1__c != null;
    }

    /**
     * Calculates the approval status.
     *
     * @param      approvedCRs  The approved c rs
     */
     public static void calculateApprovalStatus (List<BI_TM_Change_Request__c> approvedCRs) {
        // Get the escalation user
        BI_TM_Change_Request_Conf__c config = BI_TM_Change_Request_Conf__c.getValues('Default');
        Id escalationUser = config.BI_TM_Escalation_User_Id__c;

        Map<Id, BI_TM_Change_Request__c> crMap = new Map<Id, BI_TM_Change_Request__c> (approvedCRs);
        Map<Id, List<ProcessInstanceStep>> stepMap = createCRStepsMap(crMap.keySet());
        for (Id crId : stepMap.keySet()) {
            BI_TM_Change_Request__c cr = crMap.get(crId);
            Boolean sourceApproved = false;
            Boolean targetApproved = false;
            for (ProcessInstanceStep s : stepMap.get(crId)) {
                if (isSource(s) && !sourceApproved && !isEscalation(s, escalationUser))
                sourceApproved = true;
                if (isTarget(s) && !targetApproved && !isEscalation(s, escalationUser))
                targetApproved = true;
            }

            System.debug('Requires Source: ' + requiresSource(cr));
            System.debug('Requires Target: ' + requiresTarget(cr));
            System.debug('Source approved: ' + sourceApproved);
            System.debug('Target approved: ' + targetApproved);
            if (((requiresSource(cr) && sourceApproved) || !requiresSource(cr)) &&
             ((requiresTarget(cr) && targetApproved) || !requiresTarget(cr)))
            cr.BI_TM_Approval_Status__c = 'Approved';
            else if ((requiresSource(cr) && !sourceApproved) || (requiresTarget(cr) && !targetApproved))
            cr.BI_TM_Approval_Status__c = ' No Response';
        }
    }

    /**
     * Creates a cr steps map.
     *
     * @param      crIds  The carriage return identifiers
     *
     * @return     { description_of_the_return_value }
     */
     private static Map<Id, List<ProcessInstanceStep>> createCRStepsMap (Set<Id> crIds) {
        List<ProcessInstanceStep> steps = [
        SELECT Id, Comments, ActorId, ProcessInstance.TargetObjectId, ProcessInstance.ProcessDefinition.DeveloperName
        FROM ProcessInstanceStep
        WHERE StepStatus = 'Approved' AND
        ProcessInstance.TargetObjectId = :crIds];
        Map<Id, List<ProcessInstanceStep>> m = new Map<Id, List<ProcessInstanceStep>> ();
        for (ProcessInstanceStep s : steps) {
            Id crId = s.ProcessInstance.TargetObjectId;
            List<ProcessInstanceStep> l = m.get(crId);
            if (l != null)
            l.add(s);
            else
            l = new List<ProcessInstanceStep> {s};
            m.put(crId, l);
        }
        return m;
    }

    /**
     * Determines if source.
     *
     * @param      step  The step
     *
     * @return     True if source, False otherwise.
     */
     private static Boolean isSource (ProcessInstanceStep step) {
        return SOURCE_APPROVAL_PROCESS_NAME.equals(step.ProcessInstance.ProcessDefinition.DeveloperName);
    }
    /**
     * Determines if target.
     *
     * @param      step  The step
     *
     * @return     True if target, False otherwise.
     */
     private static Boolean isTarget (ProcessInstanceStep step) {
        return TARGET_APPROVAL_PROCESS_NAME.equals(step.ProcessInstance.ProcessDefinition.DeveloperName);
    }

    /**
     * Determines if escalation.
     *
     * @param      step            The step
     * @param      escalationUser  The escalation user
     *
     * @return     True if escalation, False otherwise.
     */
     private static Boolean isEscalation (ProcessInstanceStep step, Id escalationUser) {
        //return 'Escalation'.equals(step.Comments);
        return step.ActorId == escalationUser;
    }

    /**
     * getFFPosTerrMap
     *
     * ...
     *
     * @param
     *
     * @return
     */
     public static Set<Id> getFFPosTerrMap (Set<Id> crIds, BI_TM_US_FieldForce_Map__c [] usFFs) {
        Set<Id> retList = new Set<Id> ();
        Set<String> ffSetConfig = new Set<String>();
        for(BI_TM_US_FieldForce_Map__c ffc : usFFs){
          ffSetConfig.add(ffc.BI_TM_USFF_Name__c);
        }
        system.debug('ffSetConfig :: ' + ffSetConfig);
        for (BI_TM_Change_Request__c cr : [SELECT Id, BI_TM_Territory__r.BI_TM_Field_Force__r.Name, BI_TM_Position__r.BI_TM_FF_type__r.Name FROM BI_TM_Change_Request__c WHERE Id IN :crIds]) {
            System.debug('cr :: ' + cr);
            String ff = cr.BI_TM_Territory__r.BI_TM_Field_Force__r.Name != null ? cr.BI_TM_Territory__r.BI_TM_Field_Force__r.Name : cr.BI_TM_Position__r.BI_TM_FF_type__r.Name;
            System.debug('ff :: ' + ff);
            if (ff != null && !ffSetConfig.contains(ff))
              retList.add(cr.Id);
        }
        return retList;
    }
}