global class BI_TM_Change_Request_Escalation_Batch implements Database.Batchable<sObject>, Schedulable {
    
    private static final String DateGMTFormat = 'yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX';
    
    private String query = null;

    global BI_TM_Change_Request_Escalation_Batch () {

        BI_TM_Change_Request_Conf__c config = BI_TM_Change_Request_Conf__c.getValues('Default');
        Integer escalationThreshold = (config != null && config.BI_TM_Escalation_Threshold__c != null) ? config.BI_TM_Escalation_Threshold__c.intValue() : 48;

        System.debug('Change Request thresholds: escalation = ' + escalationThreshold + ' hours');

        Datetime dt = Datetime.now().addHours(-escalationThreshold);
        Map<Id, BI_TM_Change_Request__c> m = new Map<Id, BI_TM_Change_Request__c> ([SELECT Id FROM BI_TM_Change_Request__c WHERE BI_TM_Approval_Status__c = 'Approval Pending']);
        query = 'SELECT Id FROM ProcessInstanceWorkitem WHERE CreatedDate < ' + dt.formatGmt(DateGMTFormat) + ' AND ProcessInstance.TargetObjectId IN (\'' + String.join(new List<Id> (m.keySet()), '\',\'') + '\')';

        System.debug('Query: ' + query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ProcessInstanceWorkitem> scope) {
        for (ProcessInstanceWorkitem piwi : scope)
            BI_TM_Change_Request_Approval_Util.escalateApproval(piwi);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }

    global void execute(SchedulableContext sc) {
        BI_TM_Change_Request_Escalation_Batch b = new BI_TM_Change_Request_Escalation_Batch (); 
        database.executebatch(b);
    }
    
}