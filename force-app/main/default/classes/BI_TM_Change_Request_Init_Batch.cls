global class BI_TM_Change_Request_Init_Batch implements Database.Batchable<sObject>, database.stateful, Schedulable {

	String query;

	global BI_TM_Change_Request_Init_Batch() {
		query = BI_TM_Change_Request_Approval_Util.getCreatableFieldsSOQL('BI_TM_Change_Request__c', 'BI_TM_Approval_Status__c = \'New\'');
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<BI_TM_Change_Request__c> scope) {
		BI_TM_Change_Request_Approval_Util.initApprovalWorkflow(scope);
	}

	global void execute(SchedulableContext sc) {
    BI_TM_Change_Request_Init_Batch b = new BI_TM_Change_Request_Init_Batch();
    database.executebatch(b);
  }

	global void finish(Database.BatchableContext BC) {

	}

}