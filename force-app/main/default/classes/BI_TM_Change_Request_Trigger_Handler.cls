/**
 *  Class used in the BITMAN change requests approvals logic
 *
 * @author    Antonio Ferrero
 * @created   2018-02-26
 * @version   1.0
 * @since     27.0 (Force.com ApiVersion)
 *
 * @changelog
 * 2018-02-26 Antonio Ferrero - Created
 */

public with sharing class BI_TM_Change_Request_Trigger_Handler {
  private boolean m_isExecuting = false;
  private integer BatchSize = 0;

  public BI_TM_Change_Request_Trigger_Handler(boolean isExecuting, integer size){
    m_isExecuting = isExecuting;
    BatchSize = size;
  }

  public void OnAfterInsert(BI_TM_Change_Request__c[] newRecords) {
    // Check the field force of the CR if it is attached to an approval process
    Map<Id, BI_TM_Change_Request__c> crMap = new Map<Id, BI_TM_Change_Request__c>(newRecords);

    Set<Id> crIdSet = BI_TM_Change_Request_Approval_Util.getFFPosTerrMap(crMap.keySet(), BI_TM_US_FieldForce_Map__c.getAll().values());

    for(Id crId : crIdSet){
      BI_TM_Change_Request__c cr = crMap.get(crId);
      cr.addError(Label.BI_TM_ErrorFieldForceApproval);
    }

    // Update list of records to process, removing the ones with the field force not managed
    List<BI_TM_Change_Request__c> crToProccessList = new List<BI_TM_Change_Request__c>();
    for(BI_TM_Change_Request__c cr : newRecords){
      if(!crIdSet.contains(cr.Id))
        crToProccessList.add(cr);
    }

    // In case the approval period is active, the initialization of the approval flow is started right now.
    // If the period is closed, the change request is not initialized and the approval status remain as 'New'
    // In that case, the change request will be initialized whenever the period is open again
    BI_TM_Change_Request_Conf__c config = BI_TM_Change_Request_Conf__c.getValues('Default');
    if (config != null && config.BI_TM_Active_Period__c)
      BI_TM_Change_Request_Approval_Util.initApprovalWorkflow(crToProccessList);
  }

  public void OnBeforeUpdate(BI_TM_Change_Request__c[] oldRecords, BI_TM_Change_Request__c[] updatedRecords, Map<ID, BI_TM_Change_Request__c> recordMap){
    // Check for those change request that are fully approved and the status is Approval Pending (in other words, the change request has been fully approved right now)
    List<BI_TM_Change_Request__c> toProcess = new List<BI_TM_Change_Request__c> ();
    for (BI_TM_Change_Request__c cr : updatedRecords)
      if (BI_TM_Change_Request_Approval_Util.isFullyApproved(cr) && cr.BI_TM_Approval_Status__c == 'Approval Pending')
        toProcess.add(cr);

    // All approved change request are processed massively
    BI_TM_Change_Request_Approval_Util.calculateApprovalStatus(toProcess);
  }

  /*** Unrequired methods ***/
  public void OnBeforeInsert(BI_TM_Change_Request__c[] newRecords) {}
  public void OnAfterUpdate(BI_TM_Change_Request__c[] oldRecords, BI_TM_Change_Request__c[] updatedRecords, Map<ID, BI_TM_Change_Request__c> recordMap){}
  // public void OnBeforeDelete(BI_TM_Change_Request__c[] recordsToDelete, Map<ID, BI_TM_Change_Request__c> recordMap){}
  // public void OnAfterDelete(BI_TM_Change_Request__c[] deletedRecords, Map<ID, BI_TM_Change_Request__c> recordMap){}
  // public void OnUndelete(BI_TM_Change_Request__c[] restoredRecords){}
  // public boolean IsTriggerContext{get{ return m_isExecuting;}}
  // public boolean IsVisualforcePageContext{get{ return !IsTriggerContext;}}
  // public boolean IsWebServiceContext{get{ return !IsTriggerContext;}}
  // public boolean IsExecuteAnonymousContext{get{ return !IsTriggerContext;}}
}