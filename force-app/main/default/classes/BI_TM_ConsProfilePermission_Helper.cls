/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Helper for the batch to set the profile and permission sets from the user management to the user
*  @author:         Antonio Ferrero
*  @created:        16-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         16-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
public class BI_TM_ConsProfilePermission_Helper{

	// Check and compare the user profile to be updated
	public static List<User> getUserList2UpdateProfile(List<BI_TM_User_mgmt__c> umList){
		List<User> userList2UpdateProfile = new List<User>();
		Map<String, Id> profileMap = getProfileMap(umList);
		for(BI_TM_User_mgmt__c um : umList){
			if(um.BI_TM_Profile__c != um.BI_TM_UserId_Lookup__r.Profile.Name && profileMap.get(um.BI_TM_Profile__c) != null){
				User u = new User(Id = um.BI_TM_UserId_Lookup__c, ProfileId = profileMap.get(um.BI_TM_Profile__c));
				userList2UpdateProfile.add(u);
			}
		}
		return userList2UpdateProfile;
	}

	// Manage permission sets for user. Compare existing permission sets assignments with the user management permission set value
	public static void managePermissionSetList(List<BI_TM_User_mgmt__c> umList){
		Map<Id, BI_TM_User_mgmt__c> umMap = new Map<Id, BI_TM_User_mgmt__c>();
		for(BI_TM_User_mgmt__c um : umList){
			if(um.BI_TM_UserId_Lookup__c != null){
				umMap.put(um.BI_TM_UserId_Lookup__c, um);
			}
		}
		Map<String, Id> permissionSetIds = getPermissionSetIdsBITMAN();
		Map<Id, List<String>> userManagementPermissionSetsMap = getuserManagementPermissionSetsMap(umList);
		Map<Id, List<String>> userPermissionSetsMap = getUserPermissionSetsMap(userManagementPermissionSetsMap.keySet(), permissionSetIds);
		List<PermissionSetAssignment> psa2Add = new List<PermissionSetAssignment>();
		List<PermissionSetAssignment> psa2Remove = new List<PermissionSetAssignment>();
		Map<String, Id> existingPermissionSetsUser = getExistingPermissionSetsUSer(userPermissionSetsMap.keySet(), permissionSetIds.values());
		for(Id uId : userManagementPermissionSetsMap.keySet()){
			if(userManagementPermissionSetsMap.get(uId) != null){
				List<String> permissionSetUMan = userManagementPermissionSetsMap.get(uId);
				List<String> permissionSetUser = userPermissionSetsMap.get(uId) != null ? userPermissionSetsMap.get(uId) : new List<String>{''};
				psa2Add.addAll(addPermissionSet2User(uId, permissionSetUMan, permissionSetUser, permissionSetIds));
				if(umMap.get(uId).BI_TM_Override_Veeva_Permission_Sets__c){
					psa2Remove.addAll(removePermissionSet2User(uId, permissionSetUMan, permissionSetUser, permissionSetIds, existingPermissionSetsUser));
				}
			}
		}

		if(psa2Add.size() > 0){
			Database.SaveResult[] insertSrList = Database.insert(psa2Add, false);
		 	manageErrorLogSave(insertSrList);
		}

		if(psa2Remove.size() > 0){
			Database.DeleteResult[] deleteSrList = Database.delete(psa2Remove, false);
		 	manageErrorLogDelete(deleteSrList);
		}
	}

	// Get the profile id map from the user management
	private static Map<String, Id> getProfileMap(List<BI_TM_User_mgmt__c> umList){
		Map<String, Id> profileMap = new Map<String, Id>();
		Set<String> profileNameSet = new Set<String>();
		for(BI_TM_User_mgmt__c um :umList){
			profileNameSet.add(um.BI_TM_UserId_Lookup__r.Profile.Name);
		}
		for(Profile pr : [SELECT Id, Name FROM Profile WHERE Name IN :profileNameSet]){
			profileMap.put(pr.Name, pr.Id);
		}
		return profileMap;
	}

	// Get the permission set list from the user management
	public static Map<Id, List<String>> getuserManagementPermissionSetsMap(List<BI_TM_User_mgmt__c> umList){
		Map<Id, List<String>> userManagementPermissionSetsMap = new Map<Id, List<String>>();
		for(BI_TM_User_mgmt__c um : umList){
			//if(um.BI_TM_Override_Veeva_Permission_Sets__c){
				List<String> permissionSetList = um.BI_TM_Permission_Set__c != null ? (um.BI_TM_Permission_Set__c).split(';') : new List<String>{''};
				userManagementPermissionSetsMap.put(um.BI_TM_UserId_Lookup__c, permissionSetList);
			//}
		}
		return userManagementPermissionSetsMap;
	}

	// Get the permission set list from the user
	public static Map<Id, List<String>> getUserPermissionSetsMap(Set<Id> uIdSet, Map<String, Id> permissionSetIds){
		Map<Id, List<String>> userPermissionSetsMap = new Map<Id, List<String>>();
		for(PermissionSetAssignment psa : [SELECT Id, AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId IN :uIdSet AND PermissionSetId IN :permissionSetIds.values()]){
			if(userPermissionSetsMap.get(psa.AssigneeId) != null){
				List<String> psList = userPermissionSetsMap.get(psa.AssigneeId);
				psList.add(psa.PermissionSet.Name);
				userPermissionSetsMap.put(psa.AssigneeId, psList);
			}
			else{
				List<String> psList = new List<String>();
				psList.add(psa.PermissionSet.Name);
				userPermissionSetsMap.put(psa.AssigneeId, psList);
			}
		}
		return userPermissionSetsMap;
	}

	// Build list of permission sets to add to the user
	private static List<PermissionSetAssignment> addPermissionSet2User(Id uId, List<String> psUMan, List<String> psU, Map<String, Id> permissionSetIds){
		List<PermissionSetAssignment> psa2Add = new List<PermissionSetAssignment>();
		Set<String> psUManSet = new Set<String>(psUman);
		Set<String> psUSet = new Set<String>(psU);
		for(String ps : psUManSet){
			if(!psUSet.contains(ps) && permissionSetIds.get(ps) != null){
				PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = uId, PermissionSetId = permissionSetIds.get(ps));
				psa2Add.add(psa);
			}
		}
		return psa2Add;
	}

	// Build list of permission sets to remove to the user
	private static List<PermissionSetAssignment> removePermissionSet2User(Id uId, List<String> psUMan, List<String> psU, Map<String, Id> permissionSetIds, Map<String, Id> existingPermissionSetsUser){
		List<PermissionSetAssignment> psa2Remove = new List<PermissionSetAssignment>();
		Set<String> psUManSet = new Set<String>(psUman);
		Set<String> psUSet = new Set<String>(psU);
		for(String ps : psUSet){
			if(!psUManSet.contains(ps) && permissionSetIds.get(ps) != null){
				String key = (String)uId + (String)permissionSetIds.get(ps);
				if(existingPermissionSetsUser.get(key) != null){
					PermissionSetAssignment psa = new PermissionSetAssignment(Id = existingPermissionSetsUser.get(key));
					psa2Remove.add(psa);
				}
			}
		}
		return psa2Remove;
	}

	//Build a map with the existing permission sets assignments
	private static Map<String, Id> getExistingPermissionSetsUSer(Set<Id> userIds, List<Id> psIds){
		Map<String, Id> existingPermissionSetsUSer = new Map<String, Id>();
		for(PermissionSetAssignment psa : [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE AssigneeId IN :userIds AND PermissionSetId IN :psIds]){
			String key = (String)psa.AssigneeId + (String)psa.PermissionSetId;
			existingPermissionSetsUSer.put(key, psa.Id);
		}
		return existingPermissionSetsUSer;
	}

	// Build set with the user ids
	public static Set<Id> getUserIdsSet(List<BI_TM_User_mgmt__c> umList){
		Set<Id> userIdsSet = new Set<Id>();
		for(BI_TM_User_mgmt__c um : umList){
			userIdsSet.add(um.BI_TM_UserId_Lookup__c);
		}
		return userIdsSet;
	}

	// Build map with permission set names and ids
	public static Map<String, Id> getPermissionSetIdsBITMAN(){
		List<Schema.PicklistEntry> dpv = BI_TM_User_mgmt__c.BI_TM_Permission_Set__c.getDescribe().getPicklistValues();
		Map<String, Id> permissionSetMap = new Map<String, Id>();
		Set<String> psDevNames = new Set<String>();
		for(Schema.PicklistEntry pe : dpv){
			psDevNames.add(pe.getValue());
		}
		for(PermissionSet ps : [SELECT Id, Name FROM PermissionSet WHERE Name IN :psDevNames]){
			permissionSetMap.put(ps.Name, ps.Id);
		}
		return permissionSetMap;
	}

	// Build map with permission set labels and the API names
	public static Map<String, String> getPermissionSetLabelsBITMAN(){
		List<Schema.PicklistEntry> dpv = BI_TM_User_mgmt__c.BI_TM_Permission_Set__c.getDescribe().getPicklistValues();
		Map<String, String> permissionSetMap = new Map<String, String>();
		for(Schema.PicklistEntry pe : dpv){
			permissionSetMap.put(pe.getLabel(), pe.getValue());
		}
		return permissionSetMap;
	}

	public static void manageErrorLogSave(Database.SaveResult[] srList){
		// Iterate through each returned result
		for (Database.SaveResult sr : srList) {
			if (!sr.isSuccess()){
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Record fields that affected this error: ' + err.getFields());
				}
			}
		}
	}

	private static void manageErrorLogDelete(Database.DeleteResult[] srList){
		// Iterate through each returned result
		for (Database.DeleteResult sr : srList) {
			if (!sr.isSuccess()){
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Record fields that affected this error: ' + err.getFields());
				}
			}
		}
	}
}