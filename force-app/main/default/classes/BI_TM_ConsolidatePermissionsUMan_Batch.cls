/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Batch to consolidate the permission sets from the user to the user management
*  @author:         Antonio Ferrero
*  @created:        16-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         16-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_ConsolidatePermissionsUMan_Batch implements Database.Batchable<sObject> {

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT Id, BI_TM_Permission_Set__c, BI_TM_UserId_Lookup__c FROM BI_TM_User_mgmt__c WHERE BI_TM_Visible_in_CRM__c = true AND BI_TM_UserId_Lookup__c != null';
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<BI_TM_User_mgmt__c> scope) {
		Map<String, Id> permissionSetIds = BI_TM_ConsProfilePermission_Helper.getPermissionSetIdsBITMAN();
		Map<Id, BI_TM_User_mgmt__c> uManIdMap = getUManIdMap(scope);
		Set<Id> uIds = BI_TM_ConsProfilePermission_Helper.getUserIdsSet(scope);
		Map<Id, List<String>> userPermissionMap = BI_TM_ConsProfilePermission_Helper.getUserPermissionSetsMap(uIds, permissionSetIds);
		List<BI_TM_User_mgmt__c> umList2Update = getUmList2Update(userPermissionMap, uManIdMap);
		system.debug('umList2Update :: ' + umList2Update);
		if(umList2Update.size() > 0){
			Database.SaveResult[] updateSrList = Database.update(umList2Update, false);
		 	BI_TM_ConsProfilePermission_Helper.manageErrorLogSave(updateSrList);
		}
	}

	global void finish(Database.BatchableContext BC) {

	}

	// Build a map between user id and the user management id
	private Map<Id, BI_TM_User_mgmt__c> getUManIdMap(List<BI_TM_User_mgmt__c> umList){
		Map<Id, BI_TM_User_mgmt__c> uManIdMap = new Map<Id, BI_TM_User_mgmt__c>();
		for(BI_TM_User_mgmt__c um : umList){
			uManIdMap.put(um.BI_TM_UserId_Lookup__c, um);
		}
		return uManIdMap;
	}

	// Get the user management list with the whole list of permission sets
	private static List<BI_TM_User_mgmt__c> getUmList2Update(Map<Id, List<String>> userPermissionMap, Map<Id, BI_TM_User_mgmt__c> uManIdMap){
		List<BI_TM_User_mgmt__c> umList2Update = new List<BI_TM_User_mgmt__c>();
		for(Id uId : userPermissionMap.keySet()){
			BI_TM_User_mgmt__c um = new BI_TM_User_mgmt__c(Id = uManIdMap.get(uId).Id);
			if(uManIdMap.get(uId).BI_TM_Permission_Set__c != null && userPermissionMap.get(uId).size() > 0){
				um.BI_TM_Permission_Set__c = setPermissionSetNotNull(userPermissionMap, uManIdMap, uId);
				umList2Update.add(um);
			}
			else if(userPermissionMap.get(uId).size() > 0){
				um.BI_TM_Permission_Set__c = setPermissionSet(userPermissionMap.get(uId));
				umList2Update.add(um);
			}
		}
		return umList2Update;
	}

	// Build the permission set of the user management when it was empty
	private static String setPermissionSet(List<String> psList){
		return String.join(psList, ';');
	}

	// Build the permission set of the user management when it was not empty
	private static String setPermissionSetNotNull(Map<Id, List<String>> userPermissionMap, Map<Id, BI_TM_User_mgmt__c> uManIdMap, Id uId){
		List<String> psList = userPermissionMap.get(uId);
		List<String> psUman = uManIdMap.get(uId).BI_TM_Permission_Set__c != null ? (uManIdMap.get(uId).BI_TM_Permission_Set__c).split(';') : null;
		String pSetUman = uManIdMap.get(uId).BI_TM_Permission_Set__c;
		if(psUman != null){
			Set<String> psUmanSet = new Set<String>(psUman);
			for(String ps : psList){
				if(!psUmanSet.contains(ps)){
					pSetUman += ';' + ps;
				}
			}
		}
		return pSetUman;
	}
}