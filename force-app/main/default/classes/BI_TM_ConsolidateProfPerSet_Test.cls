@isTest
private class BI_TM_ConsolidateProfPerSet_Test
{
	@isTest
	static void test_method_one() {
		Test.startTest();
		// Implement test code
		Date startDate = Date.newInstance(2016, 1, 1);
		Date endDate = Date.newInstance(2099, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Set<String> nameUserRoleSet = new Set<String>();

		BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
		insert posType;
		BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
		insert pos;

		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
			posList.add(p);
		}
		insert posList;

		for(BI_TM_Territory__c p : posList){
			nameUserRoleSet.add(p.Name);
		}

		DescribeFieldResult describeCountryCode = BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c.getDescribe();
		List<PicklistEntry> availableValuesCountryCode = describeCountryCode.getPicklistValues();
		Map<String, List<String>> dependentProfile = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Profile__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentCurrency = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Currency__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentLocale = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LocaleSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentLanguage = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LanguageLocaleKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentTimezone = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_TimeZoneSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);

		String countryCodeUM = '';
		for(Integer cc = 0; cc < availableValuesCountryCode.size() ; cc++){
			String countryCode = availableValuesCountryCode[cc].getValue();
			if(dependentProfile.get(countryCode) != null && (dependentProfile.get(countryCode)).size() > 0
			&& dependentCurrency.get(countryCode) != null && (dependentCurrency.get(countryCode)).size() > 0
			&& dependentLocale.get(countryCode) != null && (dependentLocale.get(countryCode)).size() > 0
			&& dependentLanguage.get(countryCode) != null && (dependentLanguage.get(countryCode)).size() > 0
			&& dependentTimezone.get(countryCode) != null && (dependentTimezone.get(countryCode)).size() > 0){
				countryCodeUM = countryCode;
				break;
			}
		}

		List<BI_TM_User_mgmt__c> userManList = new List<BI_TM_User_mgmt__c>();
		List<BI_TM_User_territory__c> u2pList = new List<BI_TM_User_territory__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_User_mgmt__c um = BI_TM_UtilClassDataFactory_Test.createUserManagementVisibleCRM('Smith' + Integer.ValueOf(i), 'John' + Integer.ValueOf(i), countryCodeUM, 'PM', 'JSMith' + Integer.ValueOf(i),
			'jsmith' + Integer.ValueOf(i) + '@test.bitman', 'John.Smith' + Integer.ValueOf(i) + startDate.format(), 'jsmith' + Integer.ValueOf(i) + '@test.bitman', (dependentProfile.get(countryCodeUM))[0], (dependentLocale.get(countryCodeUM))[0],
		(dependentTimezone.get(countryCodeUM))[0], (dependentLanguage.get(countryCodeUM))[0], (dependentCurrency.get(countryCodeUM))[0], 'General US & Europe(ISO-8859-1)', startDate, endDate, true, true, true);
			userManList.add(um);
		}
		insert userManList;

		for(Integer i = 0; i < 10; i++){
			BI_TM_User_territory__c u2p = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[i].Id, userManList[i].Id, countryCodeUM, 'PM', true, 'Primary', startDate, endDate, 'No Approval Needed');
			u2pList.add(u2p);
		}
		insert u2pList;

		Test.stopTest();
		System.runAs(thisUser){
			List<Profile> profileIdList = [SELECT Id, Name FROM Profile WHERE UserLicense.name = 'Salesforce Platform'];
			List<User> uList = [SELECT Id, Profile.Name FROM User WHERE Name Like '%John%' AND IsActive = true];
			uList[0].ProfileId = profileIdList[2].Id;
			update uList[0];

			List<PermissionSetAssignment> psAssignmentList = new List<PermissionSetAssignment>();
			Map<String, Id> permissionSetMap = BI_TM_User_mgmtinsertingstdUserHandler.getPermissionSetsMap();
			for(String key : permissionSetMap.keySet()){
				PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = uList[0].Id, PermissionSetId = permissionSetMap.get(key));
				psAssignmentList.add(psa);
				continue;
			}
			insert psAssignmentList;
		}
		DataBase.executeBatch(new BI_TM_ConsolidateProfilePermission_Batch());
	}

	@isTest
	static void test_method_two() {
		Test.startTest();
		// Implement test code
		Date startDate = Date.newInstance(2016, 1, 1);
		Date endDate = Date.newInstance(2099, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		DescribeFieldResult describeCountryCode = BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c.getDescribe();
		List<PicklistEntry> availableValuesCountryCode = describeCountryCode.getPicklistValues();
		Map<String, List<String>> dependentProfile = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Profile__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentCurrency = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Currency__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentLocale = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LocaleSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentLanguage = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LanguageLocaleKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentTimezone = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_TimeZoneSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);

		String countryCodeUM = '';
		for(Integer cc = 0; cc < availableValuesCountryCode.size() ; cc++){
			String countryCode = availableValuesCountryCode[cc].getValue();
			if(dependentProfile.get(countryCode) != null && (dependentProfile.get(countryCode)).size() > 0
			&& dependentCurrency.get(countryCode) != null && (dependentCurrency.get(countryCode)).size() > 0
			&& dependentLocale.get(countryCode) != null && (dependentLocale.get(countryCode)).size() > 0
			&& dependentLanguage.get(countryCode) != null && (dependentLanguage.get(countryCode)).size() > 0
			&& dependentTimezone.get(countryCode) != null && (dependentTimezone.get(countryCode)).size() > 0){
				countryCodeUM = countryCode;
				break;
			}
		}

		List<BI_TM_User_mgmt__c> userManList = new List<BI_TM_User_mgmt__c>();
		List<BI_TM_User_territory__c> u2pList = new List<BI_TM_User_territory__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_User_mgmt__c um = BI_TM_UtilClassDataFactory_Test.createUserManagementVisibleCRM('Smith' + Integer.ValueOf(i), 'John' + Integer.ValueOf(i), countryCodeUM, 'PM', 'JSMith' + Integer.ValueOf(i),
			'jsmith' + Integer.ValueOf(i) + '@test.bitman', 'John.Smith' + Integer.ValueOf(i) + startDate.format(), 'jsmith' + Integer.ValueOf(i) + '@test.bitman', (dependentProfile.get(countryCodeUM))[0], (dependentLocale.get(countryCodeUM))[0],
		(dependentTimezone.get(countryCodeUM))[0], (dependentLanguage.get(countryCodeUM))[0], (dependentCurrency.get(countryCodeUM))[0], 'General US & Europe(ISO-8859-1)', startDate, endDate, true, true, true);
			userManList.add(um);
			if(i == 3){
				Map<String, Id> psMap = BI_TM_ConsProfilePermission_Helper.getPermissionSetIdsBITMAN();
				List<String> psList = new List<String>(psMap.keySet());
				userManList[i].BI_TM_Permission_Set__c = psList[0];
			}
		}
		system.debug('userManList[3].BI_TM_Permission_Set__c :: ' + userManList[3].BI_TM_Permission_Set__c);
		insert userManList;

		Test.stopTest();
		System.runAs(thisUser){
			List<Profile> profileIdList = [SELECT Id, Name FROM Profile WHERE UserLicense.name = 'Salesforce Platform'];
			List<User> uList = [SELECT Id, Profile.Name FROM User WHERE Name Like '%John%' AND IsActive = true];
			uList[0].ProfileId = profileIdList[2].Id;
			update uList[0];

			List<PermissionSetAssignment> psAssignmentList = new List<PermissionSetAssignment>();
			Map<String, Id> permissionSetMap = BI_TM_User_mgmtinsertingstdUserHandler.getPermissionSetsMap();
			Integer k = 0;
			for(String key : permissionSetMap.keySet()){
				PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = uList[k].Id, PermissionSetId = permissionSetMap.get(key));
				psAssignmentList.add(psa);
				k++;
				if(k == 9) break;
			}
			insert psAssignmentList;
		}
		DataBase.executeBatch(new BI_TM_ConsolidatePermissionsUMan_Batch());
	}

}