/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Batch to set the profile and permission sets from the user management to the user
*  @author:         Antonio Ferrero
*  @created:        16-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         16-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_ConsolidateProfilePermission_Batch implements Database.Batchable<sObject> {

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT Id, BI_TM_Profile__c, BI_TM_Permission_Set__c, BI_TM_Override_Veeva_Permission_Sets__c, BI_TM_UserId_Lookup__c, BI_TM_UserId_Lookup__r.Profile.Name FROM BI_TM_User_mgmt__c WHERE BI_TM_Visible_in_CRM__c = true AND BI_TM_UserId_Lookup__c != null';
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<BI_TM_User_mgmt__c> scope) {
		List<User> userList2UpdateProfile = BI_TM_ConsProfilePermission_Helper.getUserList2UpdateProfile(scope);
		if(userList2UpdateProfile.size() > 0){
			Database.SaveResult[] updateSrList = Database.update(userList2UpdateProfile, false);
		 	BI_TM_ConsProfilePermission_Helper.manageErrorLogSave(updateSrList);
		}

		BI_TM_ConsProfilePermission_Helper.managePermissionSetList(scope);

	}

	global void finish(Database.BatchableContext BC) {
		Database.executebatch(new BI_TM_Manage_GAS_Batch(System.now()), 2000);
	}

}