/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Consolidate roles with the primary position and update primary position in user management and update role in Salesforce´s user
*  @author:         Antonio Ferrero
*  @created:        10-Apr-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         10-Apr-2018                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_ConsolidateRoleUpdate implements Database.Batchable<sObject>, database.stateful, Schedulable{

  global Database.QueryLocator start(Database.BatchableContext BC) {

    String strQuery = 'SELECT Id, BI_TM_User_Role__r.Name, BI_TM_UserId_Lookup__r.UserRoleId, BI_TM_Primary_Position__c FROM BI_TM_User_mgmt__c Where BI_TM_UserId_Lookup__c != null AND BI_TM_User_Role__c != null and BI_TM_Visible_in_CRM__c = true';


    return Database.getQueryLocator(strQuery);
  }

  global void execute(Database.BatchableContext BC, List<BI_TM_User_mgmt__c> scope) {
    List<User> user2update = new List<User>();
    List<User> userList = new List<User>();
    Set<String> roleSet = new Set<String>();
    Set<Id> roleIdSet = new Set<Id>();
    Set<String> roleExtIdSet = new Set<String>();

    // Consolidate primary position and user role in user management Records
    Map<Id, String> mapUmUser2PrimPos = new Map<Id, String>();  // Map with user management and primary position
    Map<Id, String> mapUmPrimPos = new Map<Id, String>(); // Map with user management and the position from user to position set as primary
    Map<Id, String> mapUmUserRole = new Map<Id, String>(); // Map with user management and the user role
    Map<Id, String> mapUmUser2PrimPosId = new Map<Id, String>(); // Map with user management and the position from user to position set as primary


    // Consolidate role between user management and standard user records
    for(BI_TM_User_mgmt__c u : scope){
      if(u.BI_TM_UserId_Lookup__r.UserRoleId != null){
        roleIdSet.add(u.BI_TM_UserId_Lookup__r.UserRoleId);
      }
      if(u.BI_TM_User_Role__r.Name != null){
        roleSet.add(u.BI_TM_User_Role__r.Name);
      }
    }

    Map<Id, UserRole> userRoleMap = new Map<Id, UserRole>([Select Id, Name FROM UserRole WHERE Name IN :roleSet OR Id IN :roleIdSet]);
    Map<String, UserRole> userRoleNameMap = new Map<String, UserRole>();
    for(Id r : userRoleMap.keySet()){
      userRoleNameMap.put(userRoleMap.get(r).Name, userRoleMap.get(r));
    }

    for(BI_TM_User_mgmt__c u : scope){
      if(u.BI_TM_User_Role__r.Name != null){
        if(u.BI_TM_UserId_Lookup__r.UserRoleId != null){
          system.debug('u.BI_TM_User_Role__r.Name :: ' + u.BI_TM_User_Role__r.Name);
          system.debug('userRoleMap.get(u.BI_TM_UserId_Lookup__r.UserRoleId)).Name :: ' + ((userRoleMap.get(u.BI_TM_UserId_Lookup__r.UserRoleId)).Name));
          if(u.BI_TM_User_Role__r.Name != (userRoleMap.get(u.BI_TM_UserId_Lookup__r.UserRoleId)).Name){
            User us = new User(Id = u.BI_TM_UserId_Lookup__c);
            us.UserRoleId = userRoleNameMap.get(u.BI_TM_User_Role__r.Name).Id;
            userList.add(us);
          }
        }
        else if (u.BI_TM_User_Role__r.Name != null){
          User us = new User(Id = u.BI_TM_UserId_Lookup__c);
          us.UserRoleId = userRoleNameMap.get(u.BI_TM_User_Role__r.Name).Id;
          userList.add(us);
        }
      }
    }

    if(userList.size() > 0){

      Database.SaveResult[] srList = Database.update(userList, false);

      // Iterate through each returned result
      for (Database.SaveResult sr : srList) {
        if (sr.isSuccess()) {
          // Operation was successful, so get the ID of the record that was processed
          System.debug('Successfully updated user. User ID: ' + sr.getId());
        }
        else {
          // Operation failed, so get all errors
          for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('User fields that affected this error: ' + err.getFields());
          }
        }
      }
    }
  }

  global void execute(SchedulableContext sc) {
    BI_TM_ConsolidateRoleUpdate b = new BI_TM_ConsolidateRoleUpdate();
    database.executebatch(b);
  }

  global void finish(Database.BatchableContext BC) {
    database.executeBatch(new BI_TM_UpdateProfilePermission_Batch());
  }

}