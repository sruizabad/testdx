global class BI_TM_ConsolidateUserRoleUpdate implements Database.Batchable<sObject>, database.stateful, Schedulable {

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String strQuery = 'SELECT Id, BI_TM_User_Role__r.Name, BI_TM_User_Role__r.BI_TM_External_ID__c, BI_TM_UserId_Lookup__r.UserRoleId, BI_TM_Primary_Position__c, (Select Id, BI_TM_Territory1__c, BI_TM_Territory1__r.Name FROM User_territory_tm__r WHERE BI_TM_Primary__c = true AND BI_TM_Active__c = true) FROM BI_TM_User_mgmt__c Where BI_TM_UserId_Lookup__c != null';

		return Database.getQueryLocator(strQuery);
	}

	global void execute(Database.BatchableContext BC, List<BI_TM_User_mgmt__c> scope) {

		List<User> user2update = new List<User>();
		List<User> userList = new List<User>();
		Set<String> roleSet = new Set<String>();
		Set<Id> roleIdSet = new Set<Id>();
		Set<String> roleExtIdSet = new Set<String>();

		// Consolidate primary position and user role in user management Records
		Map<Id, String> mapUmUser2PrimPos = new Map<Id, String>();  // Map with user management and primary position
		Map<Id, String> mapUmPrimPos = new Map<Id, String>(); // Map with user management and the position from user to position set as primary
		Map<Id, String> mapUmUserRole = new Map<Id, String>(); // Map with user management and the user role
		Map<Id, String> mapUmUser2PrimPosId = new Map<Id, String>(); // Map with user management and the position from user to position set as primary

		for(BI_TM_User_mgmt__c um : scope){
			mapUmPrimPos.put(um.Id, um.BI_TM_Primary_Position__c);
			mapUmUserRole.put(um.Id, um.BI_TM_User_Role__r.BI_TM_External_ID__c);

			for(BI_TM_User_territory__c u2p :  um.User_territory_tm__r){
				mapUmUser2PrimPos.put(um.Id, u2p.BI_TM_Territory1__r.Name);
				mapUmUser2PrimPosId.put(um.Id, u2p.BI_TM_Territory1__c);
				roleExtIdSet.add(u2p.BI_TM_Territory1__c);
			}
		}

		List<BI_TM_User_Role__c> userRoleList = [SELECT Id, BI_TM_External_ID__c FROM BI_TM_User_Role__c WHERE BI_TM_External_ID__c in :roleExtIdSet];
		//system.debug('userRoleList :: ' + userRoleList);
		Map<String, Id> bUserRoleMap = new Map<String, Id>(); // Map that will hold the BITMAN user roles with the external id (pointing to the position) and the id as value
		Map<Id, BI_TM_User_mgmt__c> userManagementMap2Update = new Map<Id, BI_TM_User_mgmt__c>(); // Map that will hold the map of the user management records to update
		for(BI_TM_User_Role__c ur : userRoleList){
			bUserRoleMap.put(ur.BI_TM_External_ID__c, ur.Id);
		}

		List<BI_TM_User_mgmt__c> userManagementList2Update = new List<BI_TM_User_mgmt__c>();
		Set<String> userRoleSetExtIdSet = new Set<String>();
		// Check that the user role in the user management is the same as the position in the user to position that is active and set as primary
		for(Id um : mapUmUser2PrimPosId.keySet()){
			if(mapUmUserRole.get(um) != null){
				if(mapUmUserRole.get(um) != mapUmUser2PrimPosId.get(um) && bUserRoleMap.get(mapUmUser2PrimPosId.get(um)) != null){
					BI_TM_User_mgmt__c um2update = new BI_TM_User_mgmt__c(Id = um, BI_TM_User_Role__c = bUserRoleMap.get(mapUmUser2PrimPosId.get(um)));
					userManagementMap2Update.put(um2update.Id, um2update);
				}
			}
			else if(bUserRoleMap.get(mapUmUser2PrimPosId.get(um)) != null){
				BI_TM_User_mgmt__c um2update = new BI_TM_User_mgmt__c(Id = um, BI_TM_User_Role__c = bUserRoleMap.get(mapUmUser2PrimPosId.get(um)));
				userManagementMap2Update.put(um2update.Id, um2update);
			}
		}

		// Check that primary position in user management is the same as the position in the user to position that is active and set as primary
		for(Id um : mapUmUser2PrimPos.keySet()){
			if(mapUmPrimPos.get(um) != null){
				if(mapUmPrimPos.get(um) != mapUmUser2PrimPos.get(um)){
					if(!(userManagementMap2Update.keySet()).contains(um)){
						BI_TM_User_mgmt__c um2update = new BI_TM_User_mgmt__c(Id = um, BI_TM_Primary_Position__c = mapUmUser2PrimPos.get(um));
						userManagementMap2Update.put(um, um2update);
					}
					else{
						BI_TM_User_mgmt__c um2update = userManagementMap2Update.get(um);
						um2update.BI_TM_Primary_Position__c = mapUmUser2PrimPos.get(um);
						userManagementMap2Update.put(um, um2update);
					}
				}
			}
			else {
				if(!(userManagementMap2Update.keySet()).contains(um)){
					BI_TM_User_mgmt__c um2update = new BI_TM_User_mgmt__c(Id = um, BI_TM_Primary_Position__c = mapUmUser2PrimPos.get(um));
					userManagementMap2Update.put(um, um2update);
				}
				else{
					BI_TM_User_mgmt__c um2update = userManagementMap2Update.get(um);
					um2update.BI_TM_Primary_Position__c = mapUmUser2PrimPos.get(um);
					userManagementMap2Update.put(um, um2update);
				}
			}
		}

		for(Id um : userManagementMap2Update.keySet()){
			userManagementList2Update.add(userManagementMap2Update.get(um));
		}

		if(userManagementList2Update.size() > 0){

      Database.SaveResult[] srList = Database.update(userManagementList2Update, false);

      // Iterate through each returned result
      for (Database.SaveResult sr : srList) {
        if (sr.isSuccess()) {
          // Operation was successful, so get the ID of the record that was processed
          System.debug('Successfully updated user. User ID: ' + sr.getId());
        }
        else {
          // Operation failed, so get all errors
          for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('User fields that affected this error: ' + err.getFields());
          }
        }
      }
    }

	}

	global void execute(SchedulableContext sc) {
		BI_TM_ConsolidateUserRoleUpdate b = new BI_TM_ConsolidateUserRoleUpdate();
		database.executebatch(b);
	}

	global void finish(Database.BatchableContext BC) {
		BI_TM_ConsolidateRoleUpdate b = new BI_TM_ConsolidateRoleUpdate();
		database.executebatch(b);
	}

}