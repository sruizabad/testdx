public class BI_TM_CreateUserTerritoryQueueable implements Queueable,Database.AllowsCallouts {
    Map<id,BI_TM_User_territory__c> newMap;
    Map<id,BI_TM_User_territory__c> oldMap;


    public BI_TM_CreateUserTerritoryQueueable(Map<id,BI_TM_User_territory__c> newMap,Map<id,BI_TM_User_territory__c> oldMap){
        this.newMap = newMap;
        this.oldMap = oldMap;
    }

    public void execute(QueueableContext qu){
      /*Commented by Mario Chaves 23052017
        list<BI_TM_User_territory__c> userTerrList = [select BI_TM_Active__c,BI_TM_User__c,BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c,BI_TM_User_mgmt_tm__r.BI_TM_Username__c,BI_TM_Territory1__c,BI_TM_Territory1__r.BI_TM_TerritoryID__c  from BI_TM_User_territory__c where id in: newMap.keySet()];

        list<String> userNameList = new list<String>();
        for(BI_TM_User_territory__c ut : userTerrList) {
            userNameList.add(ut.BI_TM_User_mgmt_tm__r.BI_TM_Username__c);
        }

        list<User> userlist= [Select Id,userName from User where userName in: userNameList];
        system.debug('user territory  list'+userTerrList);
        system.debug('user list'+userList);


        Map<String,User> standardUserNameMap = new Map<String,User>();

        for(User userterrVar : userlist) {
            standardUserNameMap.Put(userTerrVar.userName ,userTerrVar);
        }

        list<id> userIdList = new list<id>();
        list<id> userTerritoryIdList = new List<id>();
        for(BI_TM_User_territory__c userTerrObj  :userTerrList) {
                if((oldMap.get(userTerrObj.id).BI_TM_Active__c == false) && newMap.get(userTerrObj.id).BI_TM_Active__c == true) {
                    HttpRequest req = new HttpRequest();
                    Http http = new Http();
                    String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
                    req.setEndpoint('callout:BITMAN');
                    req.setMethod('POST');
                    req.setHeader('Content-type', 'application/json');
                    req.setHeader('Authorization', 'OAuth {!$Credential.OAuthToken}');
                    req.setBody('{"UserId" :' +'"'+userTerrObj.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c+'"' + ',"TerritoryId" :' +'"'+ userTerrObj.BI_TM_Territory1__r.BI_TM_TerritoryID__c+'"' + '}');
                    system.debug('request body'+ req.getBody());
                    try {
                        HttpResponse response = http.send(req);
                        if ( response.getStatusCode() != 200 ) {
                            System.debug('###User assigned to the territory succesfully: ' + response.getBody());
                        } else {
                            System.debug('###Error assigning the user to the territory: ' + response.getBody());
                        }

                    }
                    catch( exception ex){
                        system.debug('###ex : ' + ex.getMessage());
                    }
                } else if((oldMap.get(userTerrObj.id).BI_TM_Active__c == true) && newMap.get(userTerrObj.id).BI_TM_Active__c == false) {
                    userIdList.add(userTerrObj.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);
                    userTerritoryIdList.add(userTerrObj.BI_TM_Territory1__r.BI_TM_TerritoryID__c);
                }
       }


        for(userTerritory utx : [select id,userId,territoryId from userTerritory where userId in: userIdList and territoryId in:userTerritoryIdList]) {
            for(BI_TM_User_territory__c userTerrx : userTerrList ){
                if((userTerrx.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c == utx.userId && userTerrx.BI_TM_Territory1__r.BI_TM_TerritoryID__c == utx.territoryId)){

                    try{
                        HttpRequest req = new HttpRequest();
                        Http http = new Http();
                        req.setEndpoint('callout:BITMAN/'+ utx.id);
                        req.setMethod('DELETE');
                        req.setHeader('Authorization', 'OAuth {!$Credential.OAuthToken}');
                        HttpResponse response = http.send(req);

                        if ( response.getStatusCode() != 200 ) {
                            System.debug('###User removed from the territory succesfully: ' + response.getBody());
                        } else {
                            System.debug('###Error removing the user from the territory: ' + response.getBody());
                        }
                    }

                    catch( exception ex){
                        system.debug('###ex : ' + ex.getMessage());
                    }
                }

            }
        }*/
    }
}