/********************************************************************************
Name:  BI_TM_CreateUserToProductHandler_Test
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Test Class for BI_TM_ProcessProducts

=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -   Shilpa P              21/01/2016   INITIAL DEVELOPMENT
2.0 -   Migart                29/10/2018
*********************************************************************************/
@isTest
private class BI_TM_CreateUserToProductHandler_Test
{
	@isTest
	static void test_method_one() {
		system.debug('+++++++++++++++++ BI_TM_CreateUserToProductHandler_Test ++++++++++++++++++++++++');
		Test.startTest();
		// Implement test code
		Date startDate = Date.newInstance(2016, 1, 1);
		Date startDate2 = Date.newInstance(2017, 1, 1);
		Date startDate3 = Date.newInstance(2018, 1, 1);
		Date endDate = Date.newInstance(2016, 12, 31);
		Date endDate2 = Date.newInstance(2017, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Set<String> nameUserRoleSet = new Set<String>();

		DescribeFieldResult describeCountryCode = BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c.getDescribe();
		List<PicklistEntry> availableValuesCountryCode = describeCountryCode.getPicklistValues();
		Map<String, List<String>> dependentProfile = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Profile__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentCurrency = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Currency__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentLocale = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LocaleSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentLanguage = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LanguageLocaleKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
		Map<String, List<String>> dependentTimezone = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_TimeZoneSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);

		String countryCodeUM = '';
		for(Integer cc = 0; cc < availableValuesCountryCode.size() ; cc++){
			String countryCode = availableValuesCountryCode[cc].getValue();
			if(dependentProfile.get(countryCode) != null && (dependentProfile.get(countryCode)).size() > 0
			&& dependentCurrency.get(countryCode) != null && (dependentCurrency.get(countryCode)).size() > 0
			&& dependentLocale.get(countryCode) != null && (dependentLocale.get(countryCode)).size() > 0
			&& dependentLanguage.get(countryCode) != null && (dependentLanguage.get(countryCode)).size() > 0
			&& dependentTimezone.get(countryCode) != null && (dependentTimezone.get(countryCode)).size() > 0){
				countryCodeUM = countryCode;
				break;
			}
		}

		BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
		insert posType;
		BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
		insert pos;
        system.debug('********** Pos ******** '+pos);
		for(Integer i = 0; i < 3; i++){
			BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
			p.BI_TM_Profile__c = (dependentProfile.get(countryCodeUM))[1];
			Map<String, String> psMap = BI_TM_ConsProfilePermission_Helper.getPermissionSetLabelsBITMAN();
			List<String> psList = new List<String>(psMap.keySet());
			System.debug('*** psList[0]: ' + psList[0]);
			p.BI_TM_Permission_Set__c = psList[0];
			posList.add(p);
		}
		insert posList;

		for(BI_TM_Territory__c p : posList){
			nameUserRoleSet.add(p.Name);
		}

		List<BI_TM_User_mgmt__c> userManList = new List<BI_TM_User_mgmt__c>();
		List<BI_TM_User_territory__c> u2pList = new List<BI_TM_User_territory__c>();
		for(Integer i = 0; i < 3; i++){
			BI_TM_User_mgmt__c um = BI_TM_UtilClassDataFactory_Test.createUserManagementVisibleCRM('Smith' + Integer.ValueOf(i), 'John' + Integer.ValueOf(i), countryCodeUM, 'PM', 'JSMith' + Integer.ValueOf(i),
			'jsmith' + Integer.ValueOf(i) + '@test.bitman', 'John.Smith' + Integer.ValueOf(i) + startDate.format(), 'jsmith' + Integer.ValueOf(i) + '@test.bitman', (dependentProfile.get(countryCodeUM))[0], (dependentLocale.get(countryCodeUM))[0],
		(dependentTimezone.get(countryCodeUM))[0], (dependentLanguage.get(countryCodeUM))[0], (dependentCurrency.get(countryCodeUM))[0], 'General US & Europe(ISO-8859-1)', startDate, endDate, true, true, true);
			userManList.add(um);
		}
		insert userManList;
		system.debug('********** userManList ******** '+userManList);

		for(Integer i = 0; i < 3; i++){
			BI_TM_User_territory__c u2p = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[i].Id, userManList[i].Id, countryCodeUM, 'PM', true, 'Primary', startDate, endDate, 'No Approval Needed');
			u2pList.add(u2p);
		}
		insert u2pList;
		system.debug('**************** u2pList '+u2pList);
		// CASE 1: A position can be primary for only one user at given point of time (Dates overlapping) - Must throw exception:
		//Start date and End date for this particular request is overlapping with already existing user to position record. 
		//A position can be primary for only one user at given point of time.Existing Record Name:UP-2882
		try {
		List<BI_TM_User_territory__c> u3pList = new List<BI_TM_User_territory__c>();
		for(Integer i = 0; i < 1; i++){
			BI_TM_User_territory__c u3p = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[i].Id, userManList[i].Id, countryCodeUM, 'PM', true, 'Primary', startDate, endDate, 'No Approval Needed');
			u3pList.add(u3p);
		}
		insert u3pList;
		system.debug('**************** u3pList '+u3pList);
		}
		catch(Exception ex){
            system.debug(':*** BI_TM_CreateUserToProductHandler_Test An exception has ocurred case 1 ' + ex.getMessage());
        }
		Test.stopTest();
		// CASE 2: Add temporary position and primary=false - Must allow
		try {
		List<BI_TM_User_territory__c> u4pList = new List<BI_TM_User_territory__c>();
		for(Integer i = 0; i < 1; i++){
			BI_TM_User_territory__c u4p = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[i].Id, userManList[i].Id, countryCodeUM, 'PM', false, 'Temporary', startDate, endDate, 'No Approval Needed');
			u4pList.add(u4p);
		}
		insert u4pList;	
		system.debug('**************** u4pList '+u4pList);	
		}
		catch(Exception ex2){
            system.debug(':*** BI_TM_CreateUserToProductHandler_Test An exception has ocurred case 2 ' + ex2.getMessage());
        }
		// CASE 3: Add Primary position - No date overlapping (end date null)- Must allow
		try {
		List<BI_TM_User_territory__c> u5pList = new List<BI_TM_User_territory__c>();
		for(Integer i = 0; i < 1; i++){
			BI_TM_User_territory__c u5p = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[i].Id, userManList[i].Id, countryCodeUM, 'PM', true, 'Primary', startDate2, null, 'No Approval Needed');
			u5pList.add(u5p);
		}
		insert u5pList;	
		system.debug('**************** u5pList '+u5pList);
				}
		catch(Exception ex3){
            system.debug(':*** BI_TM_CreateUserToProductHandler_Test An exception has ocurred case 3 ' + ex3.getMessage());
        }

		// CASE 4: Add Primary position - No end date in previous record - Fail: There is already record existing without enddate
		try {
		List<BI_TM_User_territory__c> u6pList = new List<BI_TM_User_territory__c>();
		for(Integer i = 0; i < 1; i++){
			BI_TM_User_territory__c u6p = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[i].Id, userManList[i].Id, countryCodeUM, 'PM', true, 'Primary', startDate3, null, 'No Approval Needed');
			u6pList.add(u6p);
		}
		insert u6pList;	
		system.debug('**************** u6pList '+u6pList);	
				}
		catch(Exception ex4){
            system.debug(':*** BI_TM_CreateUserToProductHandler_Test An exception has ocurred case 4 ' + ex4.getMessage());
        }
		// Case 5 startdate2 <= stardate1 y enddate2 >= stardate1 - line 289
		try {
		BI_TM_User_territory__c ut = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[2].Id, userManList[2].Id, countryCodeUM, 'PM', true, 'Primary', startDate2, null, 'No Approval Needed');
		BI_TM_User_territory__c ut2 = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[2].Id, userManList[2].Id, countryCodeUM, 'PM', true, 'Primary', startDate, endDate2, 'No Approval Needed');
		List<BI_TM_User_territory__c> u7pList = new List<BI_TM_User_territory__c>();
		u7pList.add(ut);
		u7pList.add(ut2);
		insert u7pList;
		system.debug('**************** u7pList '+u7pList);
		}
	
		catch(Exception ex5){
            system.debug(':*** BI_TM_CreateUserToProductHandler_Test An exception has ocurred case 5 ' + ex5.getMessage());
        }
		// Case 6 Batch: There is already record existing without enddate
		try {
		BI_TM_User_territory__c ut3 = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[3].Id, userManList[3].Id, countryCodeUM, 'PM', true, 'Primary', startDate, null, 'No Approval Needed');
		BI_TM_User_territory__c ut4 = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[3].Id, userManList[3].Id, countryCodeUM, 'PM', true, 'Primary', startDate2, null, 'No Approval Needed');
		List<BI_TM_User_territory__c> u8pList = new List<BI_TM_User_territory__c>();
		u8pList.add(ut3);
		u8pList.add(ut4);
		insert u8pList;
		system.debug('**************** u8pList '+u8pList);
		}
	
		catch(Exception ex6){
            system.debug(':*** BI_TM_CreateUserToProductHandler_Test An exception has ocurred case 6 ' + ex6.getMessage());
        }
	 

	}
}