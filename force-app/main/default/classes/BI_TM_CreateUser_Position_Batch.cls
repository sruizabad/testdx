global class BI_TM_CreateUser_Position_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.stateful {

      //public String qryString;
     Map<Id, BI_TM_User_territory__c> NewUserPosMap = new Map<Id, BI_TM_User_territory__c>();
     Map<Id, BI_TM_User_territory__c> OldUserPosMap = new Map<Id, BI_TM_User_territory__c>();

     public BI_TM_CreateUser_Position_Batch(Map<id,BI_TM_User_territory__c> NewUserPosMap,Map<id,BI_TM_User_territory__c> OldUserPosMap){

        this.NewUserPosMap = NewUserPosMap;
        this.OldUserPosMap= OldUserPosMap;
        system.debug('NewUserPosMap'+NewUserPosMap);
        system.debug('OldUserPosMap'+OldUserPosMap);

    }

      global Database.QueryLocator start(Database.BatchableContext BC) {
          system.debug('Inside Start Method');
        //Commented by Mario Chaves 23052017return Database.getQueryLocator([select Id,BI_TM_Active__c,BI_TM_User__c,BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c,BI_TM_User_mgmt_tm__r.BI_TM_Username__c,BI_TM_Territory1__c,BI_TM_Territory1__r.BI_TM_TerritoryID__c  from BI_TM_User_territory__c where id in: NewUserPosMap.keySet()]);
        return Database.getQueryLocator('');
    }

     global void execute(Database.BatchableContext BC, List<BI_TM_User_territory__c> lstUserPos) {
        /* Commented by Mario Chaves 23052017 system.debug('User to Positions List'+lstUserPos);
          list<String> userNameList = new list<String>();
        for(BI_TM_User_territory__c ut : lstUserPos) {
            userNameList.add(ut.BI_TM_User_mgmt_tm__r.BI_TM_Username__c);
        }

        list<User> userlist= [Select Id,userName from User where userName in: userNameList];
        system.debug('user territory  list'+lstUserPos);
        system.debug('user list'+userList);


        Map<String,User> standardUserNameMap = new Map<String,User>();

        for(User userterrVar : userlist) {
            standardUserNameMap.Put(userTerrVar.userName ,userTerrVar);
        }

        list<id> userIdList = new list<id>();
        list<id> userTerritoryIdList = new List<id>();
        for(BI_TM_User_territory__c userTerrObj  :lstUserPos) {
            system.debug('OldUserPosMap.get(userTerrObj.id).BI_TM_Active__c'+OldUserPosMap.get(userTerrObj.id).BI_TM_Active__c);
            system.debug('NewUserPosMap.get(userTerrObj.id).BI_TM_Active__c'+NewUserPosMap.get(userTerrObj.id).BI_TM_Active__c);
                 if((OldUserPosMap.get(userTerrObj.id).BI_TM_Active__c == false) && NewUserPosMap.get(userTerrObj.id).BI_TM_Active__c == true) {
                    HttpRequest req = new HttpRequest();
                    Http http = new Http();
                    String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
                    req.setEndpoint('callout:BITMAN');
                    req.setMethod('POST');
                    req.setHeader('Content-type', 'application/json');
                    req.setHeader('Authorization', 'OAuth {!$Credential.OAuthToken}');
                    req.setBody('{"UserId" :' +'"'+userTerrObj.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c+'"' + ',"TerritoryId" :' +'"'+ userTerrObj.BI_TM_Territory1__r.BI_TM_TerritoryID__c+'"' + '}');
                    system.debug('request body'+ req.getBody());
                    req.setTimeout(60000);
                    try {
                        HttpResponse response = http.send(req);
                        if ( response.getStatusCode() != 200 ) {
                            System.debug('###User assigned to the territory succesfully: ' + response.getBody());
                        } else {
                            System.debug('###Error assigning the user to the territory: ' + response.getBody());
                        }

                    }
                    catch( exception ex){
                        system.debug('###ex : ' + ex.getMessage());
                    }
                    system.debug('OldUserPosMap---->'+OldUserPosMap);
                    system.debug('NewUserPosMap---->'+NewUserPosMap);
                } else if((OldUserPosMap.get(userTerrObj.id).BI_TM_Active__c == true) && NewUserPosMap.get(userTerrObj.id).BI_TM_Active__c == false) {
                    userIdList.add(userTerrObj.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);
                    userTerritoryIdList.add(userTerrObj.BI_TM_Territory1__r.BI_TM_TerritoryID__c);
                }
       }


        for(userTerritory utx : [select id,userId,territoryId from userTerritory where userId in: userIdList and territoryId in:userTerritoryIdList]) {
            for(BI_TM_User_territory__c userTerrx : lstUserPos){
                if((userTerrx.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c == utx.userId && userTerrx.BI_TM_Territory1__r.BI_TM_TerritoryID__c == utx.territoryId)){
                    try{
                        HttpRequest req = new HttpRequest();
                        Http http = new Http();
                        req.setEndpoint('callout:BITMAN/'+ utx.id);
                        req.setMethod('DELETE');
                        req.setHeader('Authorization', 'OAuth {!$Credential.OAuthToken}');
                        req.setTimeout(60000);
                        HttpResponse response = http.send(req);

                        if ( response.getStatusCode() != 200 ) {
                            System.debug('###User removed from the territory succesfully: ' + response.getBody());
                        } else {
                            System.debug('###Error removing the user from the territory: ' + response.getBody());
                        }
                    }

                    catch( exception ex){
                        system.debug('###ex : ' + ex.getMessage());
                    }
                }

            }
        }
         */
     }

      global void finish(Database.BatchableContext BC) {

        /*Do Nothing*/
    }
}