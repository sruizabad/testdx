/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Batch to delete the manual assignments from GAS and previous interactions that are no longer valid
*  @author:         Antonio Ferrero
*  @created:        21-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         21-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_Delete_ManAssignments_Batch implements Database.Batchable<sObject> {

	String query;
	global BI_TM_Delete_ManAssignments_Batch(DateTime timeStamp) {
		query = 'SELECT Id FROM BI_TM_Account_To_Territory__c WHERE (BI_TM_GAS__C = true OR BI_TM_Previous_Interactions__c = true) AND BI_TM_TimeStamp__c != ' + timeStamp.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ' AND BI_TM_Override_GAS__c = false';
		system.debug('Query to delete :: ' + query);
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<BI_TM_Account_To_Territory__c> scope) {
		Database.DeleteResult[] deleteSrList = Database.delete(scope, false);
		BI_TM_Utils.manageErrorLogDelete(deleteSrList);
	}

	global void finish(Database.BatchableContext BC) {
		database.executeBatch(new BI_TM_ManAssig_MergeAcc_Batch());
	}

}