@isTest
private class BI_TM_Delete_ManAssignments_Test
{
	@isTest
	static void method01()
	{
		Date startDate = Date.newInstance(2016, 1, 1);
		Date endDate = Date.newInstance(2099, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Account acc = new Account(Name = 'Test Account', Country_Code_BI__c = 'US');

		Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
		insert ks;
		insert acc;

		BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
		insert posType;

		BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
		insert pos;

		BI_TM_FF_type__c ff = BI_TM_UtilClassDataFactory_Test.createFieldForce('FF Test', 'US', 'PM');
		insert ff;

		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
			posList.add(p);
		}
		insert posList;

		BI_TM_Account_To_Territory__c ma = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc.Id, 'US', 'PM', startDate, endDate, false, false, 'Both', false, false);
		ma.BI_TM_GAS__c = true;
		ma.BI_TM_TimeStamp__c = System.now();
		insert ma;

		Test.startTest();
		database.executeBatch(new BI_TM_Delete_ManAssignments_Batch(System.now().addMinutes(10)));
		Test.stopTest();
	}
}