/********************************************************************************
Name:  BI_TM_Emailnotification_Handler
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Apex Class used to send Email to Data Stewards when BIDS User Status is New or To Be Update with Field Changes
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran               29/01/2016   INITIAL DEVELOPMENT
1.1 -    S.Ruiz              21/02/2018   FLAG TO AVOID REPETITIVE EXECUTIONS
*********************************************************************************/

public with sharing class BI_TM_Emailnotification_Handler{

  // Flag to avoid repetitive executions of the trigger during the same transaction
  public static Boolean alreadyExecuted = false;

  // Set to store the names of the relaevant fields to check
  public static final Set<String> relevantFields = new Set<String>{
    'BI_TM_Given_Name__c', 'BI_TM_Surname__c', 'BI_TM_Global_Id__c', 'BI_TM_Functional_Area_Text__c', 'BI_TM_Email_Address__c', 'BI_TM_Facsmilenumber__c', 'BI_TM_BIPeople_Number__c',
    'BI_TM_CPF_ID__c', 'BI_TM_GBS_ID__c', 'BI_TM_Mobile_Number__c', 'BI_TM_Manager_Id__c', 'BI_TM_Is_Manager__c', 'BI_TM_Company_Name__c', 'BI_TM_Company_Code__c', 'BI_TM_Current_Hire_Date__c',
    'BI_TM_Network_Id__c', 'BI_TM_Service_Date__c', 'BI_TM_Termination_Date__c', 'BI_TM_Cost_Center__c', 'BI_TM_Legacy_ID__c', 'BI_TM_Employee_Type__c', 'BI_TM_Gender__c', 'BI_TM_Original_Hire_Date__c',
    'BI_TM_Home_Phone__c', 'BI_TM_Middle_Name__c', 'BI_TM_New_Hire__c', 'BI_TM_SAP_Sold_to_Id__c', 'BI_TM_Voice_Mail__c', 'BI_TM_Warehouse__c', 'BI_TM_Work_Phone__c', 'BI_TM_Job_Title__c', 'BI_TM_Employee_Status__c',
    'BI_TM_External_Employee__c', 'BI_TM_Workforce_Id__c', 'BI_TM_CN__c'
  };

  //Below Custom Setting(BI_TM_BIDSUser_EmailNotfication_Restrict__c) is Used as part of UOR to Stop the Email Notifications for specific country Data Stewards.
  public List<BI_TM_BIDS__c> sendEmail(List<BI_TM_BIDS__c> idset) {

  system.debug('alreadyExecuted :: ' + BI_TM_Emailnotification_Handler.alreadyExecuted);
    if (BI_TM_Emailnotification_Handler.alreadyExecuted) {
      return null;
    }
    Set<String> countryBusinessRestrictSet = new Set<String>();
    List<BI_TM_BIDSUser_EmailNotfication_Restrict__c> mapCodes = BI_TM_BIDSUser_EmailNotfication_Restrict__c.getAll().values();
    for(BI_TM_BIDSUser_EmailNotfication_Restrict__c  countrybusiness : mapCodes){
      countryBusinessRestrictSet.add(countrybusiness.BI_TM_Country_Business__c);
    }

    Map<string,list<GroupMember>> groupId= new Map<string,list<GroupMember>>();
    Map<String,BI_TM_BIDS__c> countries= new Map<String,BI_TM_BIDS__c>();
    List<Id> targetId= new List<Id>();
    for(BI_TM_BIDS__c usersList : idset){

      countries.put('TM'+'_'+usersList.BI_TM_Country_code__c+'_'+usersList.BI_TM_Functional_Area_Text__c,usersList);

    }
    List<Group> groupList=[Select Id,name,(Select userOrGroupId from GroupMembers) from Group where Name IN:countries.keySet()];
    //Getting Users from Public Group based on Country Code and Business
    for(Group groupListId : groupList){

      groupId.put(groupListId.Name,groupListId.groupMembers);

    }

    string userType = Schema.SObjectType.User.getKeyPrefix();

    for(BI_TM_BIDS__c BIDSUsers : idset){

      if(BIDSUsers.BI_TM_Status__c == 'New' || BIDSUsers.BI_TM_Status__c =='To be Update'){
        system.debug('====== Control Status=====' + BIDSUsers.BI_TM_Status__c);
        if(groupId != null && groupId.containskey('TM'+'_'+BIDSUsers.BI_TM_Country_code__c+'_'+BIDSUsers.BI_TM_Functional_Area_Text__c)) {
          for(GroupMember g : groupId.get('TM'+'_'+BIDSUsers.BI_TM_Country_code__c+'_'+BIDSUsers.BI_TM_Functional_Area_Text__c)){

            String idString=(String)(g.userOrGroupId);
            if(idString.startsWith(userType))
            targetId.add(g.userOrGroupId);

          }
        }

      }
    }

    List<User> userListForEmail = [select id,email,username from user where id in: targetId];
    Map<id,String> mapUserIdToUser = new Map<id,String>();
    if(userListForEmail!=null){
      for(User u : userListForEmail ) {

        mapUserIdToUser.put(u.id,u.email);

      }
    }


    List<Messaging.singleEmailMessage> emails = new List<Messaging.singleEmailMessage>();
    for(BI_TM_BIDS__c BIDSUsers : idset){
      String strcountrybusiness= BIDSUsers.BI_TM_Country_code__c + BIDSUsers.BI_TM_Functional_Area_Text__c;
      list<String> addressTo = new list<String>();

      //Below Code is for when New User is added to BIDS User Table with Status as New
      if(BIDSUsers.BI_TM_Status__c == 'New' && !countryBusinessRestrictSet.contains(strcountrybusiness)){
        Messaging.singleEmailMessage mail = new Messaging.singleEmailMessage();
        mail.emailPriority = 'Highest';
        mail.setSubject('Notification to Data Steward -Status is NEW');
        mail.setPlainTextBody('Dear User,\r\n \r\n This is to inform you that a New User is added in BIDS User Table.'
        +'Please Click the Link to go the user page and take action '+BIDSUsers.BI_TM_SFDC_Instance_Id__c+'/'+BIDSUsers.id  + '\r\n  \r\n' +  'Status is:'+ BIDSUsers.BI_TM_Status__c +'\r\n \r\nThanks,\r\n'+'BITMAN');
        if(groupId != null && groupId.containskey('TM'+'_'+BIDSUsers.BI_TM_Country_code__c+'_'+BIDSUsers.BI_TM_Functional_Area_Text__c)) {
          for(GroupMember g : groupId.get('TM'+'_'+BIDSUsers.BI_TM_Country_code__c+'_'+BIDSUsers.BI_TM_Functional_Area_Text__c)){

            String idString=(String)(g.userOrGroupId);
            if(idString.startsWith(userType))
            addressTo.add(mapUserIdToUser.get(g.userOrGroupId));
          }
        }
        mail.setToAddresses(addressTo);
        emails.add(mail);
      }
      // Below is the code for any Changes in the field Values(BI_TM_Status__c='To be Update') as part of CR
      system.debug('======Status=====' + BIDSUsers.BI_TM_Status__c +' Restrict '+countryBusinessRestrictSet.contains(strcountrybusiness));

      if(BIDSUsers.BI_TM_Status__c =='To be Update' &&  !countryBusinessRestrictSet.contains(strcountrybusiness)){

        //Fetch Field Changes Dynamically
        Map<String,Schema.SObjectType> SchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType ObjSchema = SchemaMap.get('BI_TM_BIDS__c');
        Map<String, Schema.SObjectField> FieldMap = ObjSchema.getDescribe().fields.getMap();

        String changedFields  = '';
        System.debug('*** relevantFields: ' + relevantFields);
        Set<String> fields2Check = checkFieldChanges(relevantFields, BIDSUsers.Id);
        system.debug('fields2Check :: ' + fields2Check);
        if(fields2Check.size() > 0){
          Integer i = 0;
          for(string field: fields2Check){
            String label= fieldMap.get(field).getDescribe().getLabel();
            changedFields += '<b>'+ label +'</b> - is Changed from <b><i>' + trigger.oldMap.get(BIDSUsers.Id).get(field) + '</i></b> to <b><i>' + BIDSUsers.get(field) + '</i></b>';
            i++;
            if(i < fields2Check.size()){
              changedFields += ',<br>';
            }
          }
          messaging.singleEmailMessage mail = new messaging.singleEmailMessage();

          mail.emailPriority = 'High';
          mail.setSubject('Notification to Data Steward-Status is To Be Update');
          /* string htmlBody = 'Dear User,\r\n \r\n This is to inform you the User details are Updated in BITMAN BIDS User Table.'
          +'Please take action by clicking on the following link to update these changes '+BIDSUsers.BI_TM_SFDC_Instance_Id__c+'/'+BIDSUsers.id  + '\r\n  \r\n' +  'Status is:'+ BIDSUsers.BI_TM_Status__c +'\r\n \r\nBelow are the changes:\r\n';*/

          string htmlBody = '<html><body> Dear User,'+ '<br></br>This is to inform you the User details are Updated in BITMAN BIDS User Table.'
          + '<br/>'+ 'Please take action by clicking on the following link to update these changes' + '&nbsp;&nbsp;'
          + '<a href="'+ BIDSUsers.BI_TM_SFDC_Instance_Id__c +'/'+BIDSUsers.id+'">' + BIDSUsers.BI_TM_SFDC_Instance_Id__c +'/'+BIDSUsers.Id+'</a>' + '<br></br>' +'Below are the changes:'+ '<br><br> </body></html>' ;
          htmlBody += changedFields;
          htmlBody += '<br></br>'+'Thanks,'+'<br/>'+'BITMAN';

          mail.setHtmlBody('<p style="font-family:Calibri">'+htmlBody+'</p>' );
          //mail.setHTMLBody(htmlBody);


          if(groupId != null && groupId.containskey('TM'+'_'+BIDSUsers.BI_TM_Country_code__c+'_'+BIDSUsers.BI_TM_Functional_Area_Text__c)) {
            for(GroupMember g : groupId.get('TM'+'_'+BIDSUsers.BI_TM_Country_code__c+'_'+BIDSUsers.BI_TM_Functional_Area_Text__c)){

              String idString=(String)(g.userOrGroupId);
              if(idString.startsWith(userType))
              addressTo.add(mapUserIdToUser.get(g.userOrGroupId));
            }
          }

          mail.setToAddresses(addressTo);
          emails.add(mail);
        }

      }

    }
    if(emails.size() > 0){
      try{
        // Set flag to true in order to avoid mailing on follow on executions
        BI_TM_Emailnotification_Handler.alreadyExecuted = true;
        Messaging.sendEmail(emails);
      }catch(Exception Ex){

      }
    }

    return null;
  }

  // Method to ehck the changes in the relevant fields
  public static Set<String> checkFieldChanges(Set<String> relevantFields, Id bids){
    Set<String> fields2Check = new Set<String>();
    for(String field : relevantFields){
      if(trigger.oldMap.get(bids).get(field) != trigger.newMap.get(bids).get(field)){
        fields2Check.add(field);
      }
    }
    return fields2Check;

  }
}