/******************************************************************************** 
Name:  BI_TM_Emailnotification_HandlerTest
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
apex Test Class for BI_TM_Emailnotification_Handler
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Kiran             14/04/2016   INITIAL DEVELOPMENT
*********************************************************************************/

@IsTest
private class BI_TM_Emailnotification_HandlerTest{
     static testmethod void BI_TM_Emailnotification_HandlerTest(){    
           User usercountrycode = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
             BI_TM_BIDS__c u = new BI_TM_BIDS__c();
                    u.BI_TM_Given_Name__c= 'Test';
                    u.BI_TM_Surname__c = 'kiran';
                    u.BI_TM_Email_Address__c = 'test@gmail.com.sandbox';
                    u.BI_TM_Country_code__c = usercountrycode.Country_Code_BI__c;
                    u.BI_TM_Functional_Area_Text__c = 'PM';
                    u.BI_TM_Status__c = 'New';
                    u.BI_TM_CN__c = 'kiru';
                //u.BI_TM_SFDC_Instance_Id__c = 'https://cs9.salesforce.com';       
                insert u;

        } 
          }