/******************************************************************************** 
Name:  BI_TM_Execute_Batch_Acc_To_Position_SAPL
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Apex Class to Execute the BI_TM_Batch_ManualAssignments_From_SAPL batch Apex Manually
   
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Kiran              26/12/2016   INITIAL DEVELOPMENT
*********************************************************************************/


public class BI_TM_Execute_Batch_Acc_To_Position_SAPL {

     @RemoteAction
    public static void processAccounttoPositions() {
        BI_TM_Batch_ManualAssignments_From_SAPL batchable = new BI_TM_Batch_ManualAssignments_From_SAPL();
         Database.executeBatch(batchable);
    }

}