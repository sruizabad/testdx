global class BI_TM_FF2Prod_ChildHandler_Batch implements Database.Batchable<sObject> {

	String strQuery;

	global BI_TM_FF2Prod_ChildHandler_Batch()
	{
		strQuery='select id, BI_TM_Country_Code__c, BI_TM_Mirror_Product__c, BI_TM_End_Date__c, BI_TM_Start_Date__c, BI_TM_FF_Type__c ';
		strQuery+='from BI_TM_FF_Type_To_Product__c ';
		strQuery+='where BI_TM_Active__c=true OR (BI_TM_Active__c=false AND BI_TM_Start_Date__c>=TODAY )';
	}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(strQuery);
	}

  global void execute(Database.BatchableContext BC, List<sObject> lstScope)
	{
		set<String> setFFIds=new set<String>();
		Map<String,set<String>> mapFFIdXSetProds=new Map<String,set<String>>();
		Map<String,BI_TM_FF_Type_To_Product__c> mapFFProdIdXFFToProd=new Map<String,BI_TM_FF_Type_To_Product__c>();
		for(SObject objScope:lstScope)
		{
			BI_TM_FF_Type_To_Product__c objFFToProd=(BI_TM_FF_Type_To_Product__c)objScope;
			//objFFToProd.BI_TM_Batch_executed__c=true;
			String strFFIdPrdId=(String)objFFToProd.BI_TM_FF_Type__c+(String)objFFToProd.BI_TM_Mirror_Product__c;
			if(!mapFFProdIdXFFToProd.containskey(strFFIdPrdId))
			{
				mapFFProdIdXFFToProd.put(strFFIdPrdId, objFFToProd);
			}
			if(!setFFIds.contains(objFFToProd.BI_TM_FF_type__c))
			{
				setFFIds.add(objFFToProd.BI_TM_FF_type__c);
			}
			if(mapFFIdXSetProds.containskey(objFFToProd.BI_TM_FF_type__c))
			{
				set<String> setPrdIds=mapFFIdXSetProds.get(objFFToProd.BI_TM_FF_type__c);
				if(!setPrdIds.contains(objFFToProd.BI_TM_Mirror_Product__c))
				{
					setPrdIds.add(objFFToProd.BI_TM_Mirror_Product__c);
				}

			}
			else
			{
				set<String> setPrdIds=new Set<String>();
				setPrdIds.add(objFFToProd.BI_TM_Mirror_Product__c);
				mapFFIdXSetProds.put(objFFToProd.BI_TM_FF_type__c,setPrdIds);
			}
		}
		List<BI_TM_Territory_ND__C> lstTerr=[select id,BI_TM_Field_Force__c,BI_TM_Business__c,
																												 BI_TM_Country_Code__c, BI_TM_End_Date__c,BI_TM_Start_Date__c
																												 from BI_TM_Territory_ND__c
																												 where BI_TM_Field_Force__c =:setFFIds and (BI_TM_Active__c=true or (BI_TM_Active__c=false and BI_TM_Start_Date__c>=TODAY))];
		List<BI_TM_Territory__c> lstPos=[select id, BI_TM_FF_type__c,BI_TM_Business__c,
																										BI_TM_Country_Code__c, BI_TM_End_Date__c,BI_TM_Start_Date__c
																										from BI_TM_Territory__c
																										where BI_TM_FF_type__c=:setFFIds and(BI_TM_Is_Active__c=true OR (BI_TM_Is_Active__c=false and BI_TM_Start_Date__c>=TODAY))];

		List<BI_TM_Territory_to_Product_ND__c> lstTerrToProd =getLstTerrToProd(mapFFIdXSetProds,mapFFProdIdXFFToProd,lstTerr);
		List<BI_TM_Territory_to_product__c> lstPosToProd=getLstPosToProd(mapFFIdXSetProds,mapFFProdIdXFFToProd,lstPos);
		database.insert(lstTerrToProd,false);
		database.insert(lstPosToProd,false);
		//database.update(lstScope);
	}

	public List<BI_TM_Territory_to_Product_ND__c> getLstTerrToProd(Map<String,set<String>> mapFFIdXSetProds,Map<String,BI_TM_FF_Type_To_Product__c> mapFFProdIdXFFToProd,List<BI_TM_Territory_ND__c> lstTerr)
	{
		List<BI_TM_Territory_to_Product_ND__c> lstTerrToProd=new List<BI_TM_Territory_to_Product_ND__c>();
		for(BI_TM_Territory_ND__c objTerr:lstTerr)
		{
			Set<String> setProdIds=mapFFIdXSetProds.get(objTerr.BI_TM_Field_Force__c);
			for(String strProdId:setProdIds)
			{
				String strFFIdPrdId=(String)objTerr.BI_TM_Field_Force__c+strProdId;
				BI_TM_Territory_to_Product_ND__c objTerrToProd= new BI_TM_Territory_to_Product_ND__c();
				objTerrToProd.BI_TM_Business__c=objTerr.BI_TM_Business__c;
				objTerrToProd.BI_TM_Country_Code__c=objTerr.BI_TM_Country_Code__c;
				objTerrToProd.BI_TM_Territory_ND__c=objTerr.id;
				objTerrToProd.BI_TM_Mirror_Product__c=strProdId;
				objTerrToProd.BI_TM_Batch_executed__c=true;
				if(mapFFProdIdXFFToProd.containsKey(strFFIdPrdId))
				{
					BI_TM_FF_Type_To_Product__c objFFToProd=mapFFProdIdXFFToProd.get(strFFIdPrdId);
					objTerrToProd.BI_TM_Parent_Field_Force_To_Product__c=objFFToProd.id;
					objTerrToProd.BI_TM_End_Date__c=objFFToProd.BI_TM_End_Date__c;
					objTerrToProd.BI_TM_Start_date__c=objFFToProd.BI_TM_Start_Date__c;
				}
				lstTerrToProd.add(objTerrToProd);
			}
		}
		return lstTerrToProd;
	}
	public List<BI_TM_Territory_to_product__c> getLstPosToProd(Map<String,set<String>> mapFFIdXSetProds,Map<String,BI_TM_FF_Type_To_Product__c> mapFFProdIdXFFToProd,List<BI_TM_Territory__c> lstPos)
	{
		List<BI_TM_Territory_to_product__c> lstPosToProd=new List<BI_TM_Territory_to_product__c>();
		for(BI_TM_Territory__c objPos:lstPos)
		{
			Set<String> setProdIds=mapFFIdXSetProds.get(objPos.BI_TM_FF_type__c);
			for(String strProdId:setProdIds)
			{
				String strFFIdPrdId=(String)objPos.BI_TM_FF_type__c+strProdId;
				BI_TM_Territory_to_product__c objPosToProd= new BI_TM_Territory_to_product__c();
				objPosToProd.BI_TM_Business__c=objPos.BI_TM_Business__c;
				objPosToProd.BI_TM_Country_Code__c=objPos.BI_TM_Country_Code__c;
				objPosToProd.BI_TM_End_Date__c=objPos.BI_TM_End_Date__c;
				objPosToProd.BI_TM_Start_Date__c=objPos.BI_TM_Start_Date__c;
				objPosToProd.BI_TM_Territory_id__c=objPos.id;
				objPosToProd.BI_TM_Mirror_Product__c=strProdId;
				if(mapFFProdIdXFFToProd.containsKey(strFFIdPrdId))
				{
					BI_TM_FF_Type_To_Product__c objFFToProd=mapFFProdIdXFFToProd.get(strFFIdPrdId);
					objPosToProd.BI_TM_Parent_Field_Force_To_Product__c=objFFToProd.id;
					objPosToProd.BI_TM_End_Date__c=objFFToProd.BI_TM_End_Date__c;
					objPosToProd.BI_TM_Start_date__c=objFFToProd.BI_TM_Start_Date__c;
				}
				lstPosToProd.add(objPosToProd);
			}
		}
		return lstPosToProd;
	}
	global void finish(Database.BatchableContext BC)
	{
		database.executebatch(new BI_TM_Terr2Prod_ChildHandler_Batch());
	}

}