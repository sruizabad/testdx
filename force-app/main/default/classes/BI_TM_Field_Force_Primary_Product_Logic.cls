public class BI_TM_Field_Force_Primary_Product_Logic{

	//// Check the primary products in the field force before an insert
	public void beforeInsertPrimary(List<SObject> newItems){
		Set<Id> fieldForces = new Set<Id>();

		for(BI_TM_FF_Type_To_Product__c ffProduct : (List<BI_TM_FF_Type_To_Product__c>) newItems) {
			fieldForces.add(ffProduct.BI_TM_FF_Type__c);
		}

		System.Debug('Field forces *****' + fieldForces);
		List<AggregateResult> queryResults = Database.query('SELECT COUNT(Id), BI_TM_FF_Type__c FROM BI_TM_FF_Type_To_Product__c WHERE BI_TM_Primary_Product__c = true AND BI_TM_FF_Type__c IN :fieldForces  GROUP BY BI_TM_FF_Type__c');

		System.Debug('queryResults ***' + queryResults);

		System.Debug('Insert trigger***');
		//only one can be flagged as primary product
		for(BI_TM_FF_Type_To_Product__c ffProduct : (List<BI_TM_FF_Type_To_Product__c>) newItems) {
			for(AggregateResult ar: queryResults) {
				Integer noOfPrimaryProducts = 0;
				if (ffProduct.BI_TM_FF_Type__c == String.valueOf(ar.get('BI_TM_FF_Type__c'))
						&& Integer.valueOf(ar.get('expr0')) > 0
						&& ffProduct.BI_TM_Primary_Product__c == true) {

					if (ffProduct.BI_TM_Primary_Product__c == true) {
						noOfPrimaryProducts = Integer.valueOf(ar.get('expr0')) + 1;
					}
					else {
						noOfPrimaryProducts = Integer.valueOf(ar.get('expr0')) - 1;
					}
					if ( noOfPrimaryProducts > 0) {
						ffProduct.addError('Already one product flagged as primary product for the field force. More than one product cannot be flagged as primary product.');
						break;
					}
				}
			}
		}
	}

	// Check the primary products in the field force before an update
	public void beforeUpdateprimary(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
		Set<Id> fieldForces = new Set<Id>();

		for(BI_TM_FF_Type_To_Product__c ffProduct : (List<BI_TM_FF_Type_To_Product__c>) newItems.values()) {
			fieldForces.add(ffProduct.BI_TM_FF_Type__c);
		}

		System.Debug('Field forces *****' + fieldForces);
		List<AggregateResult> queryResults = Database.query('SELECT COUNT(Id), BI_TM_FF_Type__c FROM BI_TM_FF_Type_To_Product__c WHERE BI_TM_Primary_Product__c = true AND BI_TM_FF_Type__c IN :fieldForces  GROUP BY BI_TM_FF_Type__c');

		System.Debug('queryResults ***' + queryResults);
		//only one can be flagged as primary product
		for(BI_TM_FF_Type_To_Product__c ffProduct : (List<BI_TM_FF_Type_To_Product__c>)newItems.values()) {
			// Access the "old" record by its ID in Trigger.oldMap
			BI_TM_FF_Type_To_Product__c oldFfProduct= (BI_TM_FF_Type_To_Product__c)oldItems.get(ffProduct.Id);
			System.Debug('Update trigger***old FFProduct***' + oldFfProduct);

			for(AggregateResult ar: queryResults) {
				Integer noOfPrimaryProducts = 0;
				if (ffProduct.BI_TM_FF_Type__c == String.valueOf(ar.get('BI_TM_FF_Type__c'))
						&& Integer.valueOf(ar.get('expr0')) > 0
						&& ffProduct.BI_TM_Primary_Product__c != oldFfProduct.BI_TM_Primary_Product__c) {

					if (ffProduct.BI_TM_Primary_Product__c == true) {
						noOfPrimaryProducts = Integer.valueOf(ar.get('expr0')) + 1;
					}
					else {
						noOfPrimaryProducts = Integer.valueOf(ar.get('expr0')) - 1;
					}
					if ( noOfPrimaryProducts > 0) {
						ffProduct.addError('Already one product flagged as primary product for the field force. More than one product cannot be flagged as primary product.');
						break;
					}
				}
			}
		}
	}

	// Function to check that there is only one primary product per field force in the same period of time
	public void checkPrimaryProduct(Map<Id, SObject> newItems){

		// Build a map with the field force and the ids of the records with primary flag as true
		Map<Id, Set<Id>> inFf2ProdMap = new Map<Id, Set<Id>>();
		for(Id ff2p : newItems.keySet()){
			Id ff = ((BI_TM_FF_Type_To_Product__c)newItems.get(ff2p)).BI_TM_FF_Type__c;
			if(((BI_TM_FF_Type_To_Product__c)newItems.get(ff2p)).BI_TM_Primary_Product__c == TRUE){
				if(inFf2ProdMap.get(ff) != null){
					Set<Id> ff2prodSet = inFf2ProdMap.get(ff);
					ff2prodSet.add(ff2p);
					inFf2ProdMap.put(ff, ff2prodSet);
				}
				else{
					Set<Id> ff2prodSet = new Set<Id>();
					ff2prodSet.add(ff2p);
					inFf2ProdMap.put(ff, ff2prodSet);
				}
			}
		}

    Map<Id, Set<Id>> keyMapNewItemsNoOverlap = new Map<Id, Set<Id>>(inFf2ProdMap);
		Set<Id> keySetOverlap = new Set<Id>();

    // Check if there is more than one product set as primary per field force
		for(Id ff : inFf2ProdMap.keySet()){
			if(inFf2ProdMap.get(ff) != null && inFf2ProdMap.get(ff).size() > 1){
				Set<Id> ff2prodSet = inFf2ProdMap.get(ff);

        // For each record, check if there is more records for the same dates
				for(Id id1 : ff2prodSet){

					BI_TM_FF_Type_To_Product__c ff2p1 = (BI_TM_FF_Type_To_Product__c)newItems.get(id1);
					for(Id id2 : ff2prodSet){
						if(id1 != id2){
							BI_TM_FF_Type_To_Product__c ff2p2 = ((BI_TM_FF_Type_To_Product__c)newItems.get(id2));
							if((((ff2p1.BI_TM_Start_Date__c >= ff2p2.BI_TM_Start_Date__c) && (ff2p2.BI_TM_End_Date__c != null ? ff2p1.BI_TM_Start_Date__c <= ff2p2.BI_TM_End_Date__c : true)) ||
										((ff2p1.BI_TM_Start_Date__c <= ff2p2.BI_TM_Start_Date__c) && (ff2p1.BI_TM_End_Date__c != null ? ff2p1.BI_TM_End_Date__c >= ff2p2.BI_TM_Start_Date__c : true) ||
											((ff2p1.BI_TM_End_Date__c != null && ff2p2.BI_TM_End_Date__c != null) ?
												((ff2p1.BI_TM_End_Date__c <= ff2p2.BI_TM_End_Date__c) ? ff2p1.BI_TM_End_Date__c >= ff2p2.BI_TM_Start_Date__c : ff2p1.BI_TM_Start_Date__c <= ff2p2.BI_TM_End_Date__c) : false)))){
								ff2p1.addError('You are trying to insert records with primary record and overlapped dates - Record 1: ' + ff2p1.Id + ' : SD - ' + ff2p1.BI_TM_Start_Date__c + ' : ED - ' + ff2p1.BI_TM_End_Date__c + ', Record 2: ' + ff2p2.Id + ' : SD - ' + ff2p2.BI_TM_Start_Date__c + ' : ED - ' + ff2p2.BI_TM_End_Date__c);
                // Remove ids that are already overlapped
                Set<Id> keySetOverlap2remove = keyMapNewItemsNoOverlap.get(ff);
                if(keySetOverlap2remove.contains(ff2p1.Id)){
                  keySetOverlap2remove.remove(ff2p1.Id);
                  keyMapNewItemsNoOverlap.put(ff, keySetOverlap2remove);
                }
							}
						}
					}
				}
			}
		}

    system.debug('Map no overlap records :: ' + keyMapNewItemsNoOverlap);

    // Check overlap dates with the records in the system
    if(keyMapNewItemsNoOverlap != null){
      Map<Id, BI_TM_FF_Type_To_Product__c> ff2ProdMapSystem = new Map<Id, BI_TM_FF_Type_To_Product__c>([SELECT Id, BI_TM_FF_Type__c, BI_TM_Start_Date__c, BI_TM_End_Date__c FROM BI_TM_FF_Type_To_Product__c WHERE Id NOT IN :newItems.keySet() AND BI_TM_Primary_Product__c = TRUE AND BI_TM_FF_Type__c IN :keyMapNewItemsNoOverlap.keySet()]);
      Map<Id, Set<Id>> keyMapSystemItems = new Map<Id, Set<Id>>();

      for(Id ff2p : ff2ProdMapSystem.keySet()){
        system.debug('Key :: ' + (ff2ProdMapSystem.get(ff2p)).BI_TM_FF_Type__c);
        String key = (ff2ProdMapSystem.get(ff2p)).BI_TM_FF_Type__c;
        if(keyMapSystemItems.get(key) != null){
          Set<Id> idSet = keyMapSystemItems.get(key);
          idSet.add(ff2p);
          keyMapSystemItems.put(key, idSet);
        }
        else{
          Set<Id> idSet = new Set<Id>();
          idSet.add(ff2p);
          keyMapSystemItems.put(key, idSet);
        }
      }

      for(Id ff : keyMapNewItemsNoOverlap.keySet()){
        if(keyMapNewItemsNoOverlap.get(ff) != null && keyMapSystemItems.get(ff) != null){
          Set<Id> idSet = keyMapNewItemsNoOverlap.get(ff);
          Set<Id> idSetSystem = keyMapSystemItems.get(ff);

          // Check overlap dates with records with same key in the system
          for(Id id1 : idSet){
            BI_TM_FF_Type_To_Product__c ff2p1 = (BI_TM_FF_Type_To_Product__c)newItems.get(id1);
            system.debug('ff2p1 new items:: ' + id1);
            for(Id id2 : idSetSystem){
              if(id1 != id2){
                BI_TM_FF_Type_To_Product__c ff2p2 = ff2ProdMapSystem.get(id2);
                system.debug('ff2p2 system items:: ' + id1);
                if((((ff2p1.BI_TM_Start_Date__c >= ff2p2.BI_TM_Start_Date__c) && (ff2p2.BI_TM_End_Date__c != null ? ff2p1.BI_TM_Start_Date__c <= ff2p2.BI_TM_End_Date__c : true)) ||
                      ((ff2p1.BI_TM_Start_Date__c <= ff2p2.BI_TM_Start_Date__c) && (ff2p1.BI_TM_End_Date__c != null ? ff2p1.BI_TM_End_Date__c >= ff2p2.BI_TM_Start_Date__c : true) ||
                        ((ff2p1.BI_TM_End_Date__c != null && ff2p2.BI_TM_End_Date__c != null) ?
                          ((ff2p1.BI_TM_End_Date__c <= ff2p2.BI_TM_End_Date__c) ? ff2p1.BI_TM_End_Date__c >= ff2p2.BI_TM_Start_Date__c : ff2p1.BI_TM_Start_Date__c <= ff2p2.BI_TM_End_Date__c) : false)))){
                  ff2p1.addError('There are already records in the system with primary product and overlapped the dates - Record New: ' + ff2p1.Id + ' : SD - ' + ff2p1.BI_TM_Start_Date__c + ' : ED - ' + ff2p1.BI_TM_End_Date__c + ', Existing record: ' + ff2p2.Id + ' : SD - ' + ff2p2.BI_TM_Start_Date__c + ' : ED - ' + ff2p2.BI_TM_End_Date__c);

                }
              }
            }
          }
        }
      }
    }

	}
}