public class BI_TM_Field_Force_To_Product_Handler implements BI_TM_ITriggerHandler
{
    private static boolean recursionCheck=false;//To avoid recursion

    public void BeforeInsert(List<SObject> newItems) {

        if(!recursionCheck){
            CheckDataOverlap(newItems);
            recursionCheck=true;
        }

    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

         if(!recursionCheck){
            CheckDataOverlap(newItems.values());
            recursionCheck=TRUE;
        }
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
      BI_TM_Field_Force_Primary_Product_Logic logicFFProduct = new BI_TM_Field_Force_Primary_Product_Logic();
      //logicFFProduct.beforeInsertPrimary(newItems.values());
      logicFFProduct.checkPrimaryProduct(newItems);
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        BI_TM_Field_Force_Primary_Product_Logic logicFFProduct = new BI_TM_Field_Force_Primary_Product_Logic();
        //logicFFProduct.beforeUpdateprimary(newItems, oldItems);
        logicFFProduct.checkPrimaryProduct(newItems);
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}

    /*Method to check for date overlap for fieldForce and product combination
    Author:Deepak N
    */
    public void CheckDataOverlap (List<BI_TM_FF_Type_To_Product__c> newItems){

        Set<Id> fieldForces = new Set<Id>();
        Set<Id> products=new Set<id>();

        for(BI_TM_FF_Type_To_Product__c ffProduct : (List<BI_TM_FF_Type_To_Product__c>) newItems) {
            fieldForces.add(ffProduct.BI_TM_FF_Type__c);
            products.add(ffProduct.BI_TM_Mirror_Product__c);
        }

        //Query records from database
        List<BI_TM_FF_Type_To_Product__c> ff2ProdList=new List<BI_TM_FF_Type_To_Product__c>([SELECT ID, BI_TM_FF_Type__c, BI_TM_Mirror_Product__c, BI_TM_Start_date__c, BI_TM_End_date__c, Name FROM BI_TM_FF_Type_To_Product__c where
        BI_TM_FF_Type__c IN :fieldForces AND BI_TM_Mirror_Product__c IN :products]);

        for(BI_TM_FF_Type_To_Product__c ff2ProdOuterVar:(List<BI_TM_FF_Type_To_Product__c>)newItems){
            Boolean duplicateExistInSystem=false;
            Boolean duplicateExistInFile=false;
            String errorMsg1;
            String errorMsg2;
            //Check with the records already in database
            for(BI_TM_FF_Type_To_Product__c ff2ProdInnerVar:ff2ProdList){
                if(ff2ProdOuterVar.BI_TM_FF_Type__c==ff2ProdInnerVar.BI_TM_FF_Type__c && ff2ProdOuterVar.BI_TM_Mirror_Product__c==ff2ProdInnerVar.BI_TM_Mirror_Product__c){
                    if(ff2ProdOuterVar!=ff2ProdInnerVar && ff2ProdOuterVar.Id!=ff2ProdInnerVar.Id){//check to avoid record check with itself
                        if(((ff2ProdOuterVar.BI_TM_End_date__c != NULL && ((ff2ProdInnerVar.BI_TM_Start_date__c >= ff2ProdOuterVar.BI_TM_Start_date__c && ff2ProdInnerVar.BI_TM_Start_date__c <= ff2ProdOuterVar.BI_TM_End_date__c) ||
                                (ff2ProdInnerVar.BI_TM_End_date__c  >= ff2ProdOuterVar.BI_TM_Start_date__c && ff2ProdInnerVar.BI_TM_End_date__c  <= ff2ProdOuterVar.BI_TM_End_date__c) ||
                                (ff2ProdInnerVar.BI_TM_Start_date__c <= ff2ProdOuterVar.BI_TM_Start_date__c &&ff2ProdInnerVar.BI_TM_End_date__c  >= ff2ProdOuterVar.BI_TM_End_date__c) ||
                                (ff2ProdInnerVar.BI_TM_Start_date__c<=ff2ProdOuterVar.BI_TM_Start_date__c && ff2ProdInnerVar.BI_TM_End_date__c >= ff2ProdOuterVar.BI_TM_Start_date__c))) ||
                                (ff2ProdOuterVar.BI_TM_End_date__c == NULL && ((ff2ProdInnerVar.BI_TM_End_date__c >= ff2ProdOuterVar.BI_TM_Start_date__c)))) &&
                                ff2ProdInnerVar.BI_TM_End_date__c!=Null){
                                duplicateExistInSystem=true;
                                errorMsg1='Record already exist for the given product vs fieldforce values. Existing Record Name:'+ff2ProdInnerVar.Name;

                            }
                            else if(ff2ProdInnerVar.BI_TM_End_date__c==Null && (ff2ProdInnerVar.BI_TM_Start_date__c <=ff2ProdOuterVar.BI_TM_End_date__c || ff2ProdOuterVar.BI_TM_End_date__c == NULL)){
                                duplicateExistInSystem=true;
                                errorMsg1='There is already record existing without enddate for given product and fieldforce in system. Please end date the existing record before creating new. Existing Record Name:'+ff2ProdInnerVar.Name;
                            }
                    }
                    if(duplicateExistInSystem)break;//Break the loop if duplicate is found.
                }
            }

            //Check with the records coming in single batch.
            for(BI_TM_FF_Type_To_Product__c ff2ProdInnerVar:(List<BI_TM_FF_Type_To_Product__c>)newItems){
                if(ff2ProdOuterVar.BI_TM_FF_Type__c==ff2ProdInnerVar.BI_TM_FF_Type__c && ff2ProdOuterVar.BI_TM_Mirror_Product__c==ff2ProdInnerVar.BI_TM_Mirror_Product__c){
                    if(ff2ProdInnerVar!=ff2ProdOuterVar){
                        if( (ff2ProdOuterVar.Id!=ff2ProdInnerVar.Id) || ff2ProdOuterVar.Id==NULL){//check to avoid record check with itself
                            if(((ff2ProdOuterVar.BI_TM_End_date__c != NULL && ((ff2ProdInnerVar.BI_TM_Start_date__c >= ff2ProdOuterVar.BI_TM_Start_date__c && ff2ProdInnerVar.BI_TM_Start_date__c <= ff2ProdOuterVar.BI_TM_End_date__c) ||
                                (ff2ProdInnerVar.BI_TM_End_date__c  >= ff2ProdOuterVar.BI_TM_Start_date__c && ff2ProdInnerVar.BI_TM_End_date__c  <= ff2ProdOuterVar.BI_TM_End_date__c) ||
                                (ff2ProdInnerVar.BI_TM_Start_date__c <= ff2ProdOuterVar.BI_TM_Start_date__c && ff2ProdInnerVar.BI_TM_End_date__c  >= ff2ProdOuterVar.BI_TM_End_date__c) ||
                                (ff2ProdInnerVar.BI_TM_Start_date__c<=ff2ProdOuterVar.BI_TM_Start_date__c && ff2ProdInnerVar.BI_TM_End_date__c >= ff2ProdOuterVar.BI_TM_Start_date__c))) ||
                                (ff2ProdOuterVar.BI_TM_End_date__c == NULL && ((ff2ProdInnerVar.BI_TM_End_date__c >= ff2ProdOuterVar.BI_TM_Start_date__c)))) &&
                                ff2ProdInnerVar.BI_TM_End_date__c!=Null){
                                duplicateExistInFile=true;
                                errorMsg2='Duplicate record existing within input source itself. Please validate data before loading.';

                            }
                            else if(ff2ProdInnerVar.BI_TM_End_date__c==Null && (ff2ProdInnerVar.BI_TM_Start_date__c <=ff2ProdOuterVar.BI_TM_End_date__c || ff2ProdOuterVar.BI_TM_End_date__c == NULL)){
                                duplicateExistInFile=true;
                                errorMsg2='There is already record existing without enddate within input source itself. Please validate data before loading';
                            }
                        }
                    }
                }
                    if(duplicateExistInFile)break;//Break the loop if duplicate is found.

            }
            // Commented by Antonio Ferrero 5-Jun-2017
            //check if duplicate existed. Throw error if exist.
            /*if(ff2ProdOuterVar.BI_TM_End_date__c<ff2ProdOuterVar.BI_TM_Start_date__c)
                ff2ProdOuterVar.addError('End Date Should not be less than Start Date.');
            else*/
            if(duplicateExistInFile)
                ff2ProdOuterVar.addError(errorMsg2);//Error message for user on duplicate record within file.
            else if (duplicateExistInSystem)
                ff2ProdOuterVar.addError(errorMsg1);//Error message for user on duplicate record within system.

        }
    }



}