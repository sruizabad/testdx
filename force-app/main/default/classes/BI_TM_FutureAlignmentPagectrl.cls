/******************************************************************************** 
Name:  BI_TM_FutureAlignmentPagectrl
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
class to preview the assignment records for FFType of Alignment
   
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -   Shilpa P              21/01/2016   INITIAL DEVELOPMENT
*********************************************************************************/
public class BI_TM_FutureAlignmentPagectrl{

  List<BI_TM_Assignment__c> ass=new List<BI_TM_Assignment__c>();
  list<BI_TM_Alignment__c> alig= new List<BI_TM_Alignment__c>();
  public string aid;
   private integer totalRecs = 0;
   private integer OffsetSize = 0;
   private integer LimitSize= 200;
  public BI_TM_FutureAlignmentPagectrl(){
    aid=ApexPages.currentPage().getParameters().get('id');
     //alig=[select BI_TM_FF_type__c from BI_TM_Alignment__c where ID=:aid];
  
  system.debug('***************'+alig);
  }
  public List<Account> getAccountList(){
      return [select id,name from account limit :LimitSize offset :OffsetSize];
  }
  public List<BI_TM_Future_Alignment__c> getassilist()
    {
     
       // String fftype=alig[0].BI_TM_FF_type__c;
        totalRecs = [select count() from BI_TM_Future_Alignment__c];
         List<BI_TM_Future_Alignment__c> assi =[SELECT Id,BI_TM_Account__c,BI_TM_Account__r.Name,BI_TM_Territory__c,BI_TM_Alignment_Cycle__c,BI_TM_Position__c,BI_TM_Processed_Date__c FROM BI_TM_Future_Alignment__c WHERE BI_TM_Alignment_Cycle__c=:aid LIMIT:LimitSize OFFSET:OffsetSize];
     // if(assi.size()!=0)
        return assi;
    }
    public PageReference cancel(){
        id aid=ApexPages.currentPage().getParameters().get('id');
        return new PageReference('/'+aid);          
    } 

       public integer getTotalRecs() {
        return totalRecs ;
       
       }

    public void FirstPage()
    {
    OffsetSize = 0;
    }
    public void previous()
    {
        OffsetSize = OffsetSize - LimitSize;
    }
   public void next()
    {
    OffsetSize = OffsetSize + LimitSize;
    }
    public void LastPage()
    {    
    OffsetSize = totalrecs - math.mod(totalRecs,LimitSize);
    }
    public boolean getprev()
    {
    if(OffsetSize == 0)
    return true;
    else
    return false;
    }
    public boolean getnxt()
    {
    if((OffsetSize+LimitSize) > totalRecs)
    return true;
    else
    return false;
    }
}