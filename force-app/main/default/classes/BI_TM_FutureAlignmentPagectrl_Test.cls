@isTest(SeeAllData=true)
public class BI_TM_FutureAlignmentPagectrl_Test {
    
static testMethod void TestBI_TM_FutureAlignmentPagectrl() {
         User currentuser = new User();
      currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
       
        //RecordType accRtId = new RecordType();
        //accRtId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Hospital_vod' AND SobjectType = 'Account' limit 1];
       
    List<Account> acc= new List<Account>();
        acc=[SELECT Id FROM Account LIMIT 1];


  
     BI_TM_FF_type__c FF= new BI_TM_FF_type__c();
          FF.Name='TestFF21';
          FF.BI_TM_Business__c='PM';
          FF.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
      insert FF;
      
     BI_TM_Alignment__c alignment = new BI_TM_Alignment__c();
         alignment.name='MXPRT';
         alignment.BI_TM_FF_type__c=FF.Id;
         alignment.BI_TM_Alignment_Description__c='test';
         alignment.BI_TM_Status__c='Active';
         alignment.BI_TM_End_date__c=system.today();
         alignment.BI_TM_Start_date__c=system.today();
         alignment.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
         alignment.BI_TM_Business__c='PM';
     insert alignment;
      
      BI_TM_Position_Type__c pt = new BI_TM_Position_Type__c();
      pt.Name='Test MX FF';
      pt.BI_TM_Business__c='AH';
      pt.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
      insert pt;
    
     BI_TM_Territory__c Terr1 =new BI_TM_Territory__c();
          Terr1.Name='FFHierarchy test21';
              Terr1.BI_TM_Position_Level__c='Country';
              Terr1.BI_TM_Position_Type_Lookup__c=pt.Id;
              Terr1.BI_TM_Is_Root__c=True;
              Terr1.BI_TM_FF_type__c=FF.Id;
              Terr1.BI_TM_Business__c='PM';
              //Terr1.RecordTypeId=rtype.id;
              Terr1.BI_TM_Visible_in_crm__c=True;
              Terr1.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
              Terr1.BI_TM_Start_date__c=system.Today();
              
      Insert Terr1;
   
        
       BI_TM_Future_Alignment__c FuAll = new BI_TM_Future_Alignment__c();
           // FuAll.Name ='Testallignment0508';
            if(acc.size()>0)
            FuAll.BI_TM_Account__c = acc[0].id;
            FuAll.BI_TM_Alignment_Cycle__c = alignment.Id;          
            FuAll.BI_TM_Position__c = Terr1.id;
       insert FuAll;
      
        BI_TM_FutureAlignmentPagectrl e = new BI_TM_FutureAlignmentPagectrl();
         e.getassilist();
         e.cancel();
         e.getAccountList();
         e.getTotalRecs();
         e.FirstPage();
         e.previous();
         e.next();
         e.LastPage();
         e.getprev();
         e.getnxt();
    }
}