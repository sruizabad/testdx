public with sharing class BI_TM_FutureAssignments_ctrl {

    public String selectedAccounts { get; set; }
    List<BI_TM_Assignment__c> assilist {get;set;}
    public integer totalRecs;
    public String promoteval; 
    public Boolean selected {get; set;}

   
    public BI_TM_FutureAssignments_ctrl()
    {   
         totalRecs = [select count() from BI_TM_Assignment__c WHERE BI_TM_Is_Future__c=True];
    }
    public String getTotalRecs() 
    {

        if(totalRecs >0)
        {
            promoteval=String.valueOf(totalRecs);
            return promoteval;
        }
        else
        {
            promoteval='No Future ';
            return promoteval;
        }
    }
    public List<BI_TM_Assignment__c> getassilist()
    {
        
         List<BI_TM_Assignment__c> assi = Database.Query('SELECT Name,Id,BI_TM_Assignment_Id__c, BI_TM_Assignment_description__c,BI_TM_Assignment_object__c,BI_TM_FF_type_id__c,BI_TM_Assignment_status__c,LastModifiedBy.name,CreatedBy.name,CreatedDate,LastModifiedDate,BI_TM_Is_Future__c FROM BI_TM_Assignment__c WHERE BI_TM_Is_Future__c=True');

        return assi;
    }
    
    public pageReference Promote()
    { 
                   List<BI_TM_Assignment__c> asupdate= new List<BI_TM_Assignment__c>();       
                    
                for (BI_TM_Assignment__c updateassign: [SELECT Id,BI_TM_Is_Future__c,BI_TM_Assignment_status__c FROM BI_TM_Assignment__c WHERE BI_TM_Is_Future__c=True]) 
                {
                   BI_TM_Assignment__c a= new BI_TM_Assignment__c();
                   // a.BI_TM_IsActive__c=True;
                    a.BI_TM_Is_Future__c=False;
                    a.BI_TM_Assignment_status__c='Active';
                    a.Id=updateassign.Id;  
                    asupdate.add(a);         
                }
                update asupdate;
                pageReference pr = new pageReference('/' + BI_TM_Assignment__c.sobjecttype.getDescribe().getKeyPrefix() + '/o');
                pr.setRedirect(true);
                return pr;  
        }
 
       public PageReference cancel(){
        
        return new PageReference('/' + BI_TM_Assignment__c.sobjecttype.getDescribe().getKeyPrefix() + '/o');          
    } 
     
}