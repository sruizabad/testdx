@isTest
private class BI_TM_FutureAssignments_ctrl_Test {
 
  
  static testMethod void TestBI_TM_FutureAssignments_ctrl() {
          User currentuser = new User();
      currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
      BI_TM_Assignment_Criteria_Type__c act= new BI_TM_Assignment_Criteria_Type__c();
         act.Name='actype33'; 
         act.BI_TM_Business__c='PM';
         act.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c;
        Insert act; 
       BI_TM_FF_type__c ff= new BI_TM_FF_type__c();
        ff.Name='TestFF33';
        ff.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        ff.BI_TM_Business__c='PM';
       insert ff; 
       BI_TM_Assignment__c as1 = new BI_TM_Assignment__c();
           as1.Name ='Testassignment33';
           as1.BI_TM_Assignment_object__c = 'HCP';
           as1.BI_TM_Business__c = 'AH';
           as1.BI_TM_FF_type_id__c = ff.Id;
           as1.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
           as1.BI_TM_Business__c='PM';
           as1.BI_TM_Is_Future__c=True;
           as1.BI_TM_IsActive__c=True;
           as1.BI_TM_Assignment_Criteria_Type__c=act.Id;
       insert as1;
      
       BI_TM_Rule_Type__c rule = new BI_TM_Rule_Type__c();
           rule.BI_TM_CRM_Column__c='Status_BI__c';
           rule.BI_TM_CRM_Table__c='Account';
           rule.Name='HCP Status';
           rule.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
       insert rule;
            
       BI_TM_Assignment_rule__c asr= new BI_TM_Assignment_rule__c();
           asr.BI_TM_Rule_Type_New__c=rule.Id;
           asr.BI_TM_Rule_name__c= as1.Id;
           
       insert asr;
      
        
        BI_TM_Assignment_Rule_Item__c ba = new BI_TM_Assignment_Rule_Item__c();
        ba.BI_TM_Value__c ='Inactive';
        ba.BI_TM_Assignment_rule__c = asr.Id;
        insert ba;
        BI_TM_FutureAssignments_ctrl e = new BI_TM_FutureAssignments_ctrl();
        //e.wrapAsstList[0].selected=True;
       // e.processSelected();
        e.Promote();
  }
 }