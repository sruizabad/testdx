/**
 *  Class used in the geogrpahy to territory approval logic
 *
 @author    Antonio Ferrero
 @created   2016-09-25
 @version   1.0
 @since     27.0 (Force.com ApiVersion)
 *
 @changelog
 * 2016-09-25 Antonio Ferrero - Created
 * 2016-10-15 Corrado Invernizzi - Updated
 */
public class BI_TM_GeoToTerr_Approval_Handler{

  public static final String PENDING_APPROVAL_STATUS = 'Pending';
  public static final String NO_RESPONSE_GEOTOTERR_STATUS = 'No response';
  public static final String APPROVED_GEOTOTERR_STATUS = 'Approved';
  public static final String REJECTED_GEOTOTERR_STATUS = 'Rejected';

  static final String ACTIVATE_APPROVAL_PROCESS_NAME = 'BI_TM_Approval_Geo_To_Terr';
  static final String DEACTIVATE_APPROVAL_PROCESS_NAME = 'BI_TM_Approval_Geo_To_Terr_Deactivate';

  static final String DATASTEWART_PERMISSIONSET = 'BI_TM_DataSteward';
  static final String SALESMANAGER_PERMISSIONSET = 'BI_TM_Sales_Manager';
  static final String SYSADMIN_PROFILE = 'System Administrator';

  static final String FUTURE_ALIGN_STATUSVALUE = 'Future';

  static Set<String> NO_ACCEPTED_START_STATUS_SET = new Set<String> {NO_RESPONSE_GEOTOTERR_STATUS,APPROVED_GEOTOTERR_STATUS,REJECTED_GEOTOTERR_STATUS};

  public class positionHerarchy {
    Id positionId;
    Id parentPosition1;
    Id parentPosition2;
    Id parentPosition3;

    public positionHerarchy(Id positionId) {
      this.positionId = positionId;
    }

    public positionHerarchy(Id positionId, Id parentPosition1, Id parentPosition2, Id parentPosition3) {
      this.positionId = positionId;
      this.parentPosition1 = parentPosition1;
      this.parentPosition2 = parentPosition2;
      this.parentPosition3 = parentPosition3;
    }

    public Set<Id> getAllParent() {
      return new Set<Id> {this.parentPosition1, this.parentPosition2, this.parentPosition3};
    }
  }

  /**
   *  Called after BI_TM_Geography_to_territory__c creation or after time triggered workflow
   */
  @InvocableMethod(label='Set Next Approver Geo to Terr' description='Dynamic set of the next approver of the record')
  public static List<BI_TM_Geography_to_territory__c> setNextApprover(List<BI_TM_Geography_to_territory__c> itemsToapprove){

    System.debug('setNextApprover itemsToapprove:');
    System.debug(itemsToapprove);

    // Check for executing user permission set and profile: Admin profiles and DataStewart permission sets do not create approval processes
    if (approvalProcessNeeded()) {

      // Check for the alignement: Active or Future one.
      // For future ones all territory child positions should be taken from the related BI_TM_Position_Relation__c
      // For active ones Position (BI_TM_Territory__c) should be used.
      System.debug('Check for future alignements:');
      Map<Id, BI_TM_Geography_to_territory__c> alignmentByGeoToTerrId = getRelatedAlignementInfo(itemsToapprove);
      System.debug(alignmentByGeoToTerrId);

      // Retrieve previously created approvals: for these geo to terr objects, we have to escalate the approval to the next manager
      System.debug('Check for previous approvals');
      Map<Id, ProcessInstanceWorkitem> approvalProcessByGeoToTerrId = getRelatedApprovalProcesses(itemsToapprove);
      System.debug('Previous approvals ' + approvalProcessByGeoToTerrId);

      Set<Id> geoToTerrToEscalate = new Set<Id>();
      for (Id geoToTerrId : approvalProcessByGeoToTerrId.keySet()) {
        if (PENDING_APPROVAL_STATUS.equals(approvalProcessByGeoToTerrId.get(geoToTerrId).ProcessInstance.Status)) {
          geoToTerrToEscalate.add(geoToTerrId);
        }
      }

      Map<Id, BI_TM_Geography_to_territory__c> geoToTerrToEscalateById = new Map<Id, BI_TM_Geography_to_territory__c>();
      Map<Id, BI_TM_Geography_to_territory__c> mapGeoToTerr = new Map<Id, BI_TM_Geography_to_territory__c>();// Map with the geogrpahy to territories
      Map<Id, BI_TM_Geography_to_territory__c> mapGeoToTerrFuture = new Map<Id, BI_TM_Geography_to_territory__c>();
      // Map activation / deactivation
      Map<Id, Boolean> isActivateProcessByGeoTerId = new Map<Id, Boolean>();

      // Build the map with the geography to territories to approve
      for(BI_TM_Geography_to_territory__c gt : itemsToapprove){
        // Extra check for avoid call for approval when status is not correct

        if (NO_ACCEPTED_START_STATUS_SET.contains(gt.BI_TM_Status__c)) {
          System.debug('GeoToTerr to SKIP: ' + gt);
          continue;
        }

        System.debug('Current geo to terr:' + gt);
        if (geoToTerrToEscalate.contains(gt.Id)) {
          geoToTerrToEscalateById.put(gt.Id, gt);
          // Already in approvals: BI_TM_Is_Active__c == true --> approval to deactivate
          isActivateProcessByGeoTerId.put(gt.Id, !gt.BI_TM_Is_Active__c);
          System.debug('GeoToTerr to escalated: ' + gt);
        } else {

          if (FUTURE_ALIGN_STATUSVALUE.equals(alignmentByGeoToTerrId.get(gt.Id).BI_TM_Parent_alignment__r.BI_TM_Status__c)) {
            mapGeoToTerrFuture.put(gt.Id, gt);
            System.debug('GeoToTerr future: ' + gt);
          } else {
            mapGeoToTerr.put(gt.Id, gt);
            System.debug('GeoToTerr active: ' + gt);
          }

          // New approvals: BI_TM_Is_Active__c == true --> approval to activate
          isActivateProcessByGeoTerId.put(gt.Id, gt.BI_TM_Is_Active__c);
          //System.debug('GeoToTerr to approve: ' + gt);
        }

      }

      Set<Id> geoToTerrIdNoResponse;
      List<BI_TM_Geography_to_territory__c> geoToTerrToUpd;
      Map<Id, Id> nextApproverByGeoToTerrId;



      /***************************************************************************
       *  Iterate for all GeoToTerr to approve for the first time
       ***************************************************************************/
      if (!mapGeoToTerr.isEmpty() || !mapGeoToTerrFuture.isEmpty()) {
        // Retrieve the parent position of the child positions of the territories and all their hierarchy:
        //  first level parent: the manager for the approval
        //  second level parent: the Selected manager if no one is set in the first level and the next approver if waiting time is passed
        //  third level parent: if the first approver intent, already in the second level parent, has no response in the waiting time, this is the next approver in the process
        System.debug('Get related positions');
        Map<Id, positionHerarchy> positionByGeoToTerrIdFuture = new Map<Id, positionHerarchy>();
        Map<Id, positionHerarchy> positionByGeoToTerryId = new Map<Id, positionHerarchy>();

        if (!mapGeoToTerrFuture.isEmpty()) {
          positionByGeoToTerrIdFuture = getPositionHierByGeoToTerrId(true, mapGeoToTerrFuture);
        }
        if (!mapGeoToTerr.isEmpty()) {
            positionByGeoToTerryId = getPositionHierByGeoToTerrId(false, mapGeoToTerr);
        }
        System.debug('Future:' + positionByGeoToTerrIdFuture);
        System.debug('Active: ' + positionByGeoToTerryId);

        // Retrieve the users assigned to all the passed positions
        System.debug('Get user related to the positions:');
        Map<Id, Id> userIdByPosIdFuture;
        Map<Id, Id> userIdByPosId;

        if (!positionByGeoToTerrIdFuture.isEmpty()) {
          userIdByPosIdFuture = getUserIdByPosition (true, positionByGeoToTerrIdFuture, alignmentByGeoToTerrId);
        }
        if (!positionByGeoToTerryId.isEmpty()) {
          userIdByPosId = getUserIdByPosition (false, positionByGeoToTerryId, null);
        }
        System.debug('Future: ' + userIdByPosIdFuture);
        System.debug('Active: ' + userIdByPosId);

        // Merging into only one
        for(Id geoToTerrId : positionByGeoToTerrIdFuture.keyset()) {
          positionByGeoToTerryId.put(geoToTerrId, positionByGeoToTerrIdFuture.get(geoToTerrId));
        }

        for(Id geoToTerrId : mapGeoToTerrFuture.keyset()) {
          mapGeoToTerr.put(geoToTerrId, mapGeoToTerrFuture.get(geoToTerrId));
        }

        Map<Id, Id> usersByGeoToTerrId = new Map<Id, Id>();
        nextApproverByGeoToTerrId = new Map<Id, Id>();
        geoToTerrIdNoResponse = new Set<Id>();

        for (Id geoToTerrId : mapGeoToTerr.keySet()) {

          Id userId;
          Id nextApproverId;
          Map<Id, Id> currUserIdByPosId = (FUTURE_ALIGN_STATUSVALUE.equals(alignmentByGeoToTerrId.get(geoToTerrId).BI_TM_Parent_alignment__r.BI_TM_Status__c))
            ? userIdByPosIdFuture : userIdByPosId;
          // check for the user
          if (!positionByGeoToTerryId.containsKey(geoToTerrId)) {
            // No position nor Manager found
            geoToTerrIdNoResponse.add(geoToTerrId);
            System.debug('geoToTerrIdNoResponse ' + geoToTerrIdNoResponse);

          } else {

            Id positionId = positionByGeoToTerryId.get(geoToTerrId).parentPosition1;
            if (currUserIdByPosId.containsKey(positionId)) {
              // The first level parent position Manager is found
              usersByGeoToTerrId.put(geoToTerrId, currUserIdByPosId.get(positionId));
              System.debug('usersByGeoToTerrId ' + usersByGeoToTerrId);
              // Set the next approver if waiting time is passed without any response
              if (currUserIdByPosId.containsKey(positionByGeoToTerryId.get(geoToTerrId).parentPosition2)) {
                nextApproverByGeoToTerrId.put(geoToTerrId, currUserIdByPosId.get(positionByGeoToTerryId.get(geoToTerrId).parentPosition2));
                System.debug('nextApproverByGeoToTerrId ' + nextApproverByGeoToTerrId);
              }
              /* else if (currUserIdByPosId.containsKey(positionByGeoToTerryId.get(geoToTerrId).parentPosition3)) {
                // No manager in the parent second level; Look for the last one
                nextApproverByGeoToTerrId.put(geoToTerrId, currUserIdByPosId.get(positionByGeoToTerryId.get(geoToTerrId).parentPosition3));
                System.debug('nextApproverByGeoToTerrId ' + nextApproverByGeoToTerrId);
              } */

            } else if (currUserIdByPosId.containsKey(positionByGeoToTerryId.get(geoToTerrId).parentPosition2)) {
              // The parent of the parent position Manager is found
              usersByGeoToTerrId.put(geoToTerrId, currUserIdByPosId.get(positionByGeoToTerryId.get(geoToTerrId).parentPosition2));
              System.debug('usersByGeoToTerrId ' + usersByGeoToTerrId);
              // Set the next approver if waiting time is passed without any response: Only the third level
              if (currUserIdByPosId.containsKey(positionByGeoToTerryId.get(geoToTerrId).parentPosition3)) {
                // No manager in the parent second level; Look for the last one
                nextApproverByGeoToTerrId.put(geoToTerrId, currUserIdByPosId.get(positionByGeoToTerryId.get(geoToTerrId).parentPosition3));
                System.debug('nextApproverByGeoToTerrId ' + nextApproverByGeoToTerrId);
              }

            } else {
              // No Manager found
              geoToTerrIdNoResponse.add(geoToTerrId);
              System.debug('geoToTerrIdNoResponse ' + geoToTerrIdNoResponse);
            }
          }

        }
        System.debug('Create approval process');
        createApprovals(usersByGeoToTerrId, mapGeoToTerr, isActivateProcessByGeoTerId);

        // Update GeoToTerr Status and next approver
        geoToTerrToUpd = new List<BI_TM_Geography_to_territory__c>();
        for (Id geoToTerrId : mapGeoToTerr.keySet()) {

          if (nextApproverByGeoToTerrId.containsKey(geoToTerrId)) {
            geoToTerrToUpd.add(new BI_TM_Geography_to_territory__c(Id = geoToTerrId, BI_TM_Next_Approver__c = nextApproverByGeoToTerrId.get(geoToTerrId)));

          } else if (geoToTerrIdNoResponse.contains(geoToTerrId)) {
            Boolean isActive = true;
            // All 'No response' mean no update available for the status.
            if (isActivateProcessByGeoTerId.get(geoToTerrId)) {
              isActive = false;
            }
            geoToTerrToUpd.add(new BI_TM_Geography_to_territory__c(Id = geoToTerrId, BI_TM_Status__c = NO_RESPONSE_GEOTOTERR_STATUS, BI_TM_Is_Active__c = isActive, BI_TM_Next_Approver__c = null));

          }

        }

        System.debug('geoToTerrToUpd');
        System.debug(geoToTerrToUpd);
        if (!geoToTerrToUpd.isEmpty()) {
          update geoToTerrToUpd;
        }
      }

      /***************************************************************************
       *  Iterate for all GeoToTerr to approve for the second time:
       *    waiting period has passed
       ***************************************************************************/
       if (!geoToTerrToEscalateById.isEmpty()) {
         nextApproverByGeoToTerrId = new Map<Id, Id>();
         geoToTerrIdNoResponse = new Set<Id>();

         Map<Id, ProcessInstanceWorkitem> approvalToUpdProcessByGeoToTerrId = new Map<Id, ProcessInstanceWorkitem>();
         Map<Id, ProcessInstanceWorkitem> approvalToRejProcessByGeoToTerrId = new Map<Id, ProcessInstanceWorkitem>();

         for (Id geoToTerrId : geoToTerrToEscalateById.keySet()) {
           ProcessInstanceWorkitem actProcItm = approvalProcessByGeoToTerrId.get(geoToTerrId);
           if (geoToTerrToEscalateById.get(geoToTerrId).BI_TM_Next_Approver__c != null) {
            // Update the approver
            Id approverId = geoToTerrToEscalateById.get(geoToTerrId).BI_TM_Next_Approver__c;
            nextApproverByGeoToTerrId.put(geoToTerrId, approverId);
            approvalToUpdProcessByGeoToTerrId.put(geoToTerrId, actProcItm);

          } else {
            geoToTerrIdNoResponse.add(geoToTerrId);
            approvalToRejProcessByGeoToTerrId.put(geoToTerrId, actProcItm);
          }
        }

        System.debug('update Approvals');
        updateApprovals(approvalToUpdProcessByGeoToTerrId, nextApproverByGeoToTerrId);
        System.debug('reject Approvals');
        rejectApprovals(approvalToRejProcessByGeoToTerrId);

        // Update GeoToTerr Status and next approver
        geoToTerrToUpd = new List<BI_TM_Geography_to_territory__c>();
        for (Id geoToTerrId : geoToTerrToEscalateById.keySet()) {

          if (nextApproverByGeoToTerrId.containsKey(geoToTerrId)) {
            // No more approver, set to null
            geoToTerrToUpd.add(new BI_TM_Geography_to_territory__c(Id = geoToTerrId, BI_TM_Next_Approver__c = null));

          } else if (geoToTerrIdNoResponse.contains(geoToTerrId)) {
            geoToTerrToUpd.add(new BI_TM_Geography_to_territory__c(Id = geoToTerrId, BI_TM_Status__c = NO_RESPONSE_GEOTOTERR_STATUS, BI_TM_Next_Approver__c = null));

          }
        }

        System.debug('geoToTerrToUpd escalated to be updated');
        System.debug(geoToTerrToUpd);
        if (!geoToTerrToUpd.isEmpty()) {
          update geoToTerrToUpd;
        }

      }

    }

    return null;
  }

  private static Map<Id, BI_TM_Geography_to_territory__c> getGeoToTerrInfo (Set<Id> geoToTerIds) {
    return new Map<Id, BI_TM_Geography_to_territory__c> ([SELECT Id, BI_TM_Status__c FROM BI_TM_Geography_to_territory__c WHERE Id in :geoToTerIds]);

  }


  /**
   *  Get existing approvals for the passed geoToTerr. It will return the pending one (with its workitems) and the closed ones (approved or rejected)
   *  @return Map <geoToTerrId, ProcessInstance>
   */
  @TestVisible
  private static Map<Id, ProcessInstanceWorkitem> getRelatedApprovalProcesses(List<BI_TM_Geography_to_territory__c> currentGeoToTerrList) {
    Map<Id, ProcessInstanceWorkitem> retMap = new Map<Id, ProcessInstanceWorkitem>();
    if (!currentGeoToTerrList.isEmpty()) {
      Set<Id> targetObjts = new Set<Id>();
      for (BI_TM_Geography_to_territory__c forGeoToTerr : currentGeoToTerrList) {
        targetObjts.add(forGeoToTerr.Id);
      }

      // Look for pending / open Approvals
      for (ProcessInstanceWorkitem forProcInstanceWItm
        : [SELECT Id, ActorId, ProcessInstanceId, ProcessInstance.Status, ProcessInstance.targetObjectId FROM ProcessInstanceWorkitem  WHERE ProcessInstance.targetObjectId IN :targetObjts ORDER BY ElapsedTimeInMinutes DESC]
      ) {
        System.debug('Add ProcessInstance ' + forProcInstanceWItm);
        retMap.put(forProcInstanceWItm.ProcessInstance.targetObjectId, forProcInstanceWItm);
      }

    }

    return retMap;
  }

  /**
   *  Retreieve all active and primary User to Position for the passed Positions
   *  TODO: remove
   */
  private static Map<Id, BI_TM_User_territory__c> getUserPositionByPosition (Set<Id> positions) {
    Map<Id, BI_TM_User_territory__c> userPositionByPosition = new Map<Id, BI_TM_User_territory__c>();
    for (BI_TM_User_territory__c forUserPos :
      [SELECT Id, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_Territory1__c
        FROM BI_TM_User_territory__c
        WHERE BI_TM_Territory1__c IN :positions
        AND BI_TM_Active__c = true and BI_TM_Assignment_Type__c = 'Primary']
    ) {
      userPositionByPosition.put(forUserPos.BI_TM_Territory1__c, forUserPos);
    }
    
    return userPositionByPosition;
  }

  private static Map<Id, Id> getUserIdByPosition(
    Boolean isFuture, Map<Id, positionHerarchy> positionHierByGeoToTerrId, Map<Id, BI_TM_Geography_to_territory__c> alignmentByGeoToTerrId
  ) {
    System.debug('getUserByPosition isFuture: ' + isFuture);
    System.debug('positionHierByGeoToTerrId: ' + positionHierByGeoToTerrId);
    System.debug('alignmentByGeoToTerrId: ' + alignmentByGeoToTerrId);

    Map<Id, Id> userIdByPositionId = new Map<Id, Id>();

    Set<Id> positionIds = new Set<Id>();
    for (Id forGeoToTerrId : positionHierByGeoToTerrId.keyset()) {
      positionHerarchy currPosHie = positionHierByGeoToTerrId.get(forGeoToTerrId);

      positionIds.addAll(currPosHie.getAllParent());
    }
    System.debug('Position to look for:');
    System.debug(positionIds);

    if (!positionIds.isEmpty()) {

      if (isFuture && alignmentByGeoToTerrId != null) {

        Map<Id, BI_TM_User_territory__c> userTerrByPosId = new Map<Id, BI_TM_User_territory__c>();
        for (BI_TM_User_territory__c forUserPos :
          [SELECT Id, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_Territory1__c, BI_TM_Start_date__c, BI_TM_End_date__c
            FROM BI_TM_User_territory__c
            WHERE BI_TM_Territory1__c IN :positionIds AND BI_TM_Assignment_Type__c = 'Primary']
        ) {
          Id currUserId;
          if (forUserPos.BI_TM_User_mgmt_tm__r != null && forUserPos.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c != null) {
            currUserId = forUserPos.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c;
          }
          userIdByPositionId.put(forUserPos.BI_TM_Territory1__c, currUserId);
          userTerrByPosId.put(forUserPos.BI_TM_Territory1__c, forUserPos);
        }

        for (Id forGeoToTerrId : positionHierByGeoToTerrId.keyset()) {
          Date alignStart = alignmentByGeoToTerrId.get(forGeoToTerrId).BI_TM_Parent_alignment__r.BI_TM_Start_date__c;
          Date alignEnd = alignmentByGeoToTerrId.get(forGeoToTerrId).BI_TM_Parent_alignment__r.BI_TM_End_date__c;

          positionHerarchy currPosHie = positionHierByGeoToTerrId.get(forGeoToTerrId);
          for (String posId : currPosHie.getAllParent()) {
            if (userTerrByPosId.containsKey(posId) && userIdByPositionId.get(posId) != null) {
              // Check for start & end date
              Date usrStart = userTerrByPosId.get(posId).BI_TM_Start_date__c;
              Date usrEnd = userTerrByPosId.get(posId).BI_TM_End_date__c;

              if (!(usrStart <= alignStart && (usrEnd == null || usrEnd > alignStart))) {
                // This user is not active in the alignement: remove from the returned list
                userIdByPositionId.remove(posId);
                System.debug('User no active in the alignement');
              }
            }
          }
        }

      } else {
        /*for (BI_TM_User_territory__c forUserPos :
          [SELECT Id, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_Territory1__c
            FROM BI_TM_User_territory__c
            WHERE BI_TM_Territory1__c IN :positionIds
            AND BI_TM_Active__c = true  AND BI_TM_Assignment_Type__c = 'Primary']
        ) {

          if (forUserPos.BI_TM_User_mgmt_tm__r != null && forUserPos.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c != null) {
            Id currUserId = forUserPos.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c;
            userIdByPositionId.put(forUserPos.BI_TM_Territory1__c, currUserId);
          }

        }*/
      }

    }

    return userIdByPositionId;
  }

  /**
   *  Create an approval
   */
  private static void createApprovals(
    Map<Id, Id> usersByGeoToTerrId, Map<Id, BI_TM_Geography_to_territory__c> geoToTerrById, Map<Id, Boolean> isActivateProcessByGeoTerId
  ) {
    List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();

    for(Id gtoTerrId : usersByGeoToTerrId.keySet()){
      // Create an approval request for the record
      Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
      req1.setComments('Submitting request for approval.');
      req1.setObjectId(gtoTerrId);

      // Set the approver to the process
      req1.setNextApproverIds(new List<Id> {usersByGeoToTerrId.get(gtoTerrId)});
      System.debug('Next approver :: ' + req1.getNextApproverIds());

      // The submitter will be the creator of the record
      Id userSubmitter = geoToTerrById.get(gtoTerrId).CreatedById;

      // Submit on behalf of the record creator
      req1.setSubmitterId(userSubmitter);

      // Submit the record to specific process and skip the criteria evaluation
      req1.setSkipEntryCriteria(true);
      String approvalName = (isActivateProcessByGeoTerId.get(gtoTerrId)) ? ACTIVATE_APPROVAL_PROCESS_NAME : DEACTIVATE_APPROVAL_PROCESS_NAME;
      req1.setProcessDefinitionNameOrId(approvalName);

      requests.add(req1);
    }

    // Submit all the requests
    Approval.ProcessResult[] processResults = null;
    if(requests.size() > 0){
      processResults = Approval.process(requests, true);
      System.debug('approval request :' + requests);

    }

  }

  /**
   *  Update already created approval
   */
  private static void updateApprovals(
    Map<Id, ProcessInstanceWorkitem> approvalProcessByGeoToTerrId, Map<Id, Id> nextApproverByGeoToTerrId
  ) {
    List<ProcessInstanceWorkitem> workItmsToUpd = new List<ProcessInstanceWorkitem> ();

    for (Id forGeoToTerrId : approvalProcessByGeoToTerrId.keySet()) {

      ProcessInstanceWorkitem currProcWItem = approvalProcessByGeoToTerrId.get(forGeoToTerrId);
      Id nextApproverId = nextApproverByGeoToTerrId.get(forGeoToTerrId);
      currProcWItem.ActorId = nextApproverId;
      workItmsToUpd.add(currProcWItem);

    }

    // Update the approver and send email to him
    update workItmsToUpd;

  }

  private static void rejectApprovals(Map<Id, ProcessInstanceWorkitem> approvalProcessByGeoToTerrId) {

    Approval.ProcessWorkitemRequest reqToreject = new Approval.ProcessWorkitemRequest();
    for (Id forGeoToTerrId : approvalProcessByGeoToTerrId.keySet()) {

      reqToreject.setComments('Reject for no manager set.');
      reqToreject.setAction('Reject');
      reqToreject.setNextApproverIds(new Id[] {UserInfo.getUserId()});
      reqToreject.setWorkitemId(approvalProcessByGeoToTerrId.get(forGeoToTerrId).Id);

      Approval.ProcessResult results =  Approval.process(reqToreject);
      System.debug(results);
    }

  }

  private static Boolean approvalProcessNeeded() {
    Boolean needApproval = false;
    if (Test.isRunningTest()) {
      return true; // Always to test the overall process
    }

    /*
    Profile admin = [SELECT Id FROM Profile WHERE Name = :SYSADMIN_PROFILE];
    if (admin.Id == UserInfo.getProfileId()) {
      needApproval = false;

    } else {
      PermissionSet dataStewartPS = [SELECT Id, Name FROM PermissionSet WHERE Name = :DATASTEWART_PERMISSIONSET];

      List<PermissionSetAssignment> psAss = [SELECT Id FROM PermissionSetAssignment WHERE PermissionSetId = :dataStewartPS.Id AND AssigneeId = :UserInfo.getUserId()];
      if (psAss != null && !psAss.isEmpty()) {
        needApproval = false;
      }
    }
    */

    PermissionSet salesManagerPS = [SELECT Id, Name FROM PermissionSet WHERE Name = :SALESMANAGER_PERMISSIONSET];

    List<PermissionSetAssignment> psAss = [SELECT Id FROM PermissionSetAssignment WHERE PermissionSetId = :salesManagerPS.Id AND AssigneeId = :UserInfo.getUserId()];
    if (psAss != null && !psAss.isEmpty()) {
      needApproval = true;
    }

    return needApproval;
  }

  private static Map<Id, BI_TM_Geography_to_territory__c> getRelatedAlignementInfo(List<BI_TM_Geography_to_territory__c> itemsToapprove) {
    Map<Id, BI_TM_Geography_to_territory__c> retMap = new Map<Id, BI_TM_Geography_to_territory__c>();
    // TODO Check this...Error for Bad rightOperand type: got SObjectScriptRow: operator e dataType 1
    Set<Id> geoToTerrIds = new Set<Id>();
    for (BI_TM_Geography_to_territory__c forGeoToTer : itemsToapprove) {
      geoToTerrIds.add(forGeoToTer.Id);
    }

    for (BI_TM_Geography_to_territory__c forGeoToTerr :
      [SELECT Id, BI_TM_Parent_alignment__r.BI_TM_Status__c, BI_TM_Parent_alignment__r.BI_TM_End_date__c, BI_TM_Parent_alignment__r.BI_TM_Start_date__c
        FROM BI_TM_Geography_to_territory__c WHERE Id IN :geoToTerrIds]
    ) {
      retMap.put(forGeoToTerr.Id, forGeoToTerr);
    }

    return retMap;
  }


  private static Map<Id, positionHerarchy> getPositionHierByGeoToTerrId(Boolean isFuture, Map<Id, BI_TM_Geography_to_territory__c> geoToTerrMap) {

    Map<Id, positionHerarchy> posHierarchyByGeoTerrId = new Map<Id, positionHerarchy>();

    if (!geoToTerrMap.isEmpty()) {

      Map<Id, Id> alignementByGeoTerrId = new Map<Id, Id>();
      Map<Id, BI_TM_Geography_to_territory__c> geoToTerrByTerrId = new Map<Id,BI_TM_Geography_to_territory__c>();
      for (BI_TM_Geography_to_territory__c forGeoToTerr : geoToTerrMap.values()) {
        alignementByGeoTerrId.put(forGeoToTerr.Id, forGeoToTerr.BI_TM_Parent_alignment__c);
        geoToTerrByTerrId.put(forGeoToTerr.BI_TM_Territory_ND__c, forGeoToTerr);
      }

      if (isFuture) {

        Map<Id,Set<Id>> positionByParent1Level = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> positionByParent2Level = new Map<Id,Set<Id>>();
        Map<Id, Id> geoToTerrByPositionId = new Map<Id,Id>();

        Map<Id, positionHerarchy> posHierarchyByPositionId = new Map<Id, positionHerarchy>();

        Set<Id> positionToRetrieve = new Set<Id>();

        // Get first level
        for (BI_TM_Position_Relation__c forPosRel
          : [SELECT Id, Name, BI_TM_Alignment_Cycle__c, BI_TM_Parent_Position__c, BI_TM_Position__r.BI_TM_Territory_ND__c FROM BI_TM_Position_Relation__c
              WHERE BI_TM_Alignment_Cycle__c IN :alignementByGeoTerrId.values()
                AND BI_TM_Position__r.BI_TM_Territory_ND__c IN :geoToTerrByTerrId.keyset()]
        ) {
          System.debug('First level: ' +  forPosRel.Id + ' ' + forPosRel.Name);
          Id geoToterrId = geoToTerrByTerrId.get(forPosRel.BI_TM_Position__r.BI_TM_Territory_ND__c).Id;
          geoToTerrByPositionId.put(forPosRel.BI_TM_Position__c, geoToterrId);

          if (!posHierarchyByPositionId.containsKey(forPosRel.BI_TM_Position__c)) {
            posHierarchyByPositionId.put(forPosRel.BI_TM_Position__c, new positionHerarchy(forPosRel.BI_TM_Position__c));
          }
          posHierarchyByPositionId.get(forPosRel.BI_TM_Position__c).parentPosition1 = forPosRel.BI_TM_Parent_Position__c;

          if (!positionByParent1Level.containsKey(forPosRel.BI_TM_Parent_Position__c)) {
            positionByParent1Level.put(forPosRel.BI_TM_Parent_Position__c, new Set<Id> {forPosRel.BI_TM_Position__c});
          } else {
            positionByParent1Level.get(forPosRel.BI_TM_Parent_Position__c).add(forPosRel.BI_TM_Position__c);
          }

        }

        // Get second level
        for (BI_TM_Position_Relation__c forPosRel
          : [SELECT Id, Name, BI_TM_Position__c, BI_TM_Parent_Position__c FROM BI_TM_Position_Relation__c
              WHERE BI_TM_Alignment_Cycle__c IN :alignementByGeoTerrId.values()
                AND BI_TM_Position__c IN :positionByParent1Level.keyset()]
        ) {
          System.debug('Second level: ' +  forPosRel.Id + ' ' + forPosRel.Name);
          //Id originPosition = positionByParent1Level.get(forPosRel.BI_TM_Position__c);
          for (Id originPosition : positionByParent1Level.get(forPosRel.BI_TM_Position__c)) {
            positionHerarchy currHierarchy = posHierarchyByPositionId.get(originPosition);
            currHierarchy.parentPosition2 = forPosRel.BI_TM_Parent_Position__c;
            posHierarchyByPositionId.put(originPosition, currHierarchy);

            if (!positionByParent2Level.containsKey(forPosRel.BI_TM_Parent_Position__c)) {
              positionByParent2Level.put(forPosRel.BI_TM_Parent_Position__c, new Set<Id> {originPosition});
            } else {
              positionByParent2Level.get(forPosRel.BI_TM_Parent_Position__c).add(originPosition);
            }

          }
        }

        // Get third level
        for (BI_TM_Position_Relation__c forPosRel
          : [SELECT Id, Name, BI_TM_Position__c, BI_TM_Parent_Position__c FROM BI_TM_Position_Relation__c
              WHERE BI_TM_Alignment_Cycle__c IN :alignementByGeoTerrId.values()
                AND BI_TM_Position__c IN :positionByParent2Level.keyset()]
        ) {
          System.debug('Third level: ' +  forPosRel.Id + ' ' + forPosRel.Name);
          //Id originPosition = positionByParent2Level.get(forPosRel.BI_TM_Position__c);
          for (Id originPosition : positionByParent2Level.get(forPosRel.BI_TM_Position__c)) {
            positionHerarchy currHierarchy = posHierarchyByPositionId.get(originPosition);
            currHierarchy.parentPosition3 = forPosRel.BI_TM_Parent_Position__c;
            posHierarchyByPositionId.put(originPosition, currHierarchy);
          }
        }

        for (Id forPosId : posHierarchyByPositionId.keyset()) {
          posHierarchyByGeoTerrId.put(geoToTerrByPositionId.get(forPosId), posHierarchyByPositionId.get(forPosId));
        }

      } else {
        // All hierarchy from position (BI_TM_Territory__c)
        // NOTE: limit 1 means that all child positions of a territory should have always the same parent position, so we can consult for parent only one child
        for(BI_TM_Territory_ND__c t :
          [SELECT Id, (SELECT Id, BI_TM_Parent_Position__c, BI_TM_Parent_Position__r.BI_TM_Is_Root__c,
                        BI_TM_Parent_Position__r.BI_TM_Parent_Position__c, BI_TM_Parent_Position__r.BI_TM_Parent_Position__r.BI_TM_Is_Root__c,
                        BI_TM_Parent_Position__r.BI_TM_Parent_Position__r.BI_TM_Parent_Position__c
                        FROM Positions__r limit 1)
            FROM BI_TM_Territory_ND__c
            WHERE Id in :geoToTerrByTerrId.keySet()]
        ){
          if (t.Positions__r != null && !t.Positions__r.isEmpty()) {
            BI_TM_Territory__c basePos = t.Positions__r[0];
            Id parent1 = basePos.BI_TM_Parent_Position__c;
            Id parent2 = (parent1 != null) ? basePos.BI_TM_Parent_Position__r.BI_TM_Parent_Position__c : null;
            Id parent3 = (parent2 != null) ? basePos.BI_TM_Parent_Position__r.BI_TM_Parent_Position__r.BI_TM_Parent_Position__c : null;
            positionHerarchy currPosHierarchy = new positionHerarchy(
              basePos.Id,
              basePos.BI_TM_Parent_Position__c,
              basePos.BI_TM_Parent_Position__r.BI_TM_Parent_Position__c,
              basePos.BI_TM_Parent_Position__r.BI_TM_Parent_Position__r.BI_TM_Parent_Position__c
            );

            Id geoToTerrId = geoToTerrByTerrId.get(t.Id).Id;
            posHierarchyByGeoTerrId.put(geoToTerrId, currPosHierarchy);
          }
        }

      }
    }

    System.debug(posHierarchyByGeoTerrId);
    return posHierarchyByGeoTerrId;

  }


}