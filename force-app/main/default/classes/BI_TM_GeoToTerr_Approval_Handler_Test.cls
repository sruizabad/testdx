@isTest
private class BI_TM_GeoToTerr_Approval_Handler_Test {
  static final String DATASTEWART_PERMISSIONSET = 'BI_TM_DataSteward';
  static final String SALESMANAGER_PERMISSIONSET = 'BI_TM_Sales_Manager';
  static final String BITMAN_GLOBAL_USER_PROFILE = 'MX_SALES';
  static final String SYSADMIN_PROFILE = 'System Administrator';


  static String businessCode;
  static String countryCode = 'US';
  static Group grp;
  static User admin;
  static User ds;
  static User noadmin;
  static BI_TM_FF_type__c ffTest;

  //https://help.salesforce.com/articleView?id=security_sharing_cbs_about.htm&language=en_US&type=0
  // Crieria rules can be checked in Apex Test


  private static void createData() {

    admin = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    // Insert as current user (https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_dml_non_mix_sobjects_test_methods.htm)
    System.runAs (admin) {
      //admin = [SELECT Id, Name, ProfileId FROM User WHERE Profile.Name = :SYSADMIN_PROFILE AND IsActive = true LIMIT 1];
      // Get Country Business
      Group grpUS = [SELECT DeveloperName,Id,Name,Type FROM Group WHERE DeveloperName = 'TM_US' LIMIT 1];
      grp = [SELECT DeveloperName,Id,Name,Type FROM Group WHERE DeveloperName IN ('TM_US_PM','TM_US_AH') LIMIT 1];
      System.debug('BITMAN Group for US ' + grp);
      businessCode = grp.DeveloperName.split('_').get(2);
      System.debug('Test for US & ' + businessCode);


      PermissionSet dataStewartPS = [SELECT Id, Name FROM PermissionSet WHERE Name = :DATASTEWART_PERMISSIONSET];
      PermissionSet salesManagerPS = [SELECT Id, Name FROM PermissionSet WHERE Name = :SALESMANAGER_PERMISSIONSET];
      System.debug('Data Stewart & salesManagerPS permission set found');

      Profile globalTMProfile = [SELECT Id FROM Profile WHERE Name = :BITMAN_GLOBAL_USER_PROFILE LIMIT 1];
      ds = new User(
        IsActive=true, Username='dataStewartTest@bi.mvs', FirstName='Test', Lastname='DS', Email='testDS@example.com',
        Alias='DSTest', ProfileId=globalTMProfile.Id, TimeZoneSidKey='America/Chicago', LanguageLocaleKey='en_US', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1'
      );

      noadmin = new User(
        IsActive=true, Username='salesTest@bi.mvs', FirstName='Test', Lastname='Sales', Email='testSales@example.com',
        Alias='SalTest', ProfileId=globalTMProfile.Id, TimeZoneSidKey='America/Chicago', LanguageLocaleKey='en_US', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1'
      );
      insert new List<User> {ds, noadmin};

      // Add to US group
      insert new GroupMember (UserOrGroupId = ds.Id, GroupId = grpUS.Id);
      insert new GroupMember (UserOrGroupId = noadmin.Id, GroupId = grpUS.Id);
      // Permission sets
      insert new PermissionSetAssignment(AssigneeId = ds.Id, PermissionSetId = dataStewartPS.Id);
      insert new PermissionSetAssignment(AssigneeId = noadmin.Id, PermissionSetId = salesManagerPS.Id);

      System.debug('Data Stewart Test: ' + ds);
      System.debug('Sales Manager Test: ' + noadmin);

      ffTest = new BI_TM_FF_type__c(Name='FF_Test1', BI_TM_Business__c=businessCode,BI_TM_Country_Code__c=countryCode ); // BI_TM_External_Id__c = US_AH_FF_Test1
      insert ffTest;

      DateTime startDate = Datetime.now();
      DateTime endDate = startDate.addMonths(6);

      BI_TM_Alignment__c aligTest1 = new BI_TM_Alignment__c(
        name='Alig_Test1',BI_TM_FF_type__c=ffTest.id,
        BI_TM_Status__c='Active',
        BI_TM_Country_Code__c=countryCode,
        BI_TM_Business__c=businessCode,
        BI_TM_AlignmentExternalId__c = 'US_Alig_Test1',
        BI_TM_Start_date__c=startDate.date(),
        BI_TM_End_date__c=endDate.date()
      );

      BI_TM_Alignment__c aligTest2 = new BI_TM_Alignment__c(
        name='Alig_Test2',BI_TM_FF_type__c=ffTest.id,
        BI_TM_Status__c='Future',
        BI_TM_Country_Code__c=countryCode,
        BI_TM_Business__c=businessCode,
        BI_TM_AlignmentExternalId__c = 'US_Alig_Test2',
        BI_TM_Start_date__c=startDate.addMonths(7).date(),
        BI_TM_End_date__c=endDate.addMonths(6).date()
      );

      insert new List<BI_TM_Alignment__c> {aligTest1,aligTest2};

      BI_TM_Territory_ND__c terr1 = new BI_TM_Territory_ND__c(
        Name = 'US-' + businessCode +'-TerrTest-1',
        BI_TM_Field_Force__c = ffTest.id,
        BI_TM_Business__c = businessCode,
      	BI_TM_Country_Code__c = countryCode,
        BI_TM_Start_Date__c = startDate.date(),
        BI_TM_End_Date__c = endDate.addMonths(6).date()
      );

      BI_TM_Territory_ND__c terr2 = new BI_TM_Territory_ND__c(
        Name = 'US-' + businessCode + '-TerrTest-2',
        BI_TM_Field_Force__c = ffTest.id,
        BI_TM_Business__c = businessCode,
      	BI_TM_Country_Code__c = countryCode,
        BI_TM_Start_Date__c = startDate.date(),
        BI_TM_End_Date__c = endDate.addMonths(6).date()
      );
      insert new List<BI_TM_Territory_ND__c> {terr1, terr2};

      BI_TM_Geography_type__c geoType = new BI_TM_Geography_type__c(
        Name = 'Postal Code Test', BI_TM_CRM_column__c = 'zip_vod__c', BI_TM_Country_Code__c = countryCode
      );
      insert geoType;

      BI_TM_Geography__c geo1 = new BI_TM_Geography__c(
        Name = '1000',
        BI_TM_Is_Active__c = true,
        BI_TM_Country_Code__c = countryCode,
        BI_TM_External_id_geography__c = 'US_1000',
        BI_TM_Geography_type__c = geoType.Id
      );
      BI_TM_Geography__c geo2 = new BI_TM_Geography__c(
        Name = '2000',
        BI_TM_Is_Active__c = true,
        BI_TM_Country_Code__c = countryCode,
        BI_TM_External_id_geography__c = 'US_2000',
        BI_TM_Geography_type__c = geoType.Id
      );

      insert new List<BI_TM_Geography__c> {geo1,geo2};

    }

  }

  private static List<BI_TM_Territory__c> createPosition(Id territoryId, Id territory2Id) {
    BI_TM_Position_Type__c regionType = new BI_TM_Position_Type__c(
      Name = 'RegionTEST', BI_TM_Business__c = businessCode, BI_TM_Country_Code__c = countryCode
    );
    BI_TM_Position_Type__c districtType = new BI_TM_Position_Type__c(
      Name = 'DistrictTEST', BI_TM_Business__c = businessCode, BI_TM_Country_Code__c = countryCode
    );
    BI_TM_Position_Type__c posType = new BI_TM_Position_Type__c(
      Name = 'posTEST', BI_TM_Business__c = businessCode, BI_TM_Country_Code__c = countryCode
    );

    insert new List<BI_TM_Position_Type__c> {regionType, districtType, posType};

    BI_TM_Territory__c region = new BI_TM_Territory__c(
      BI_TM_Business__c = businessCode, BI_TM_Country_Code__c = countryCode,
      BI_TM_Parent_Position__c = null, 	BI_TM_Is_Root__c = true, BI_TM_Position_Level__c = 'Region',
      BI_TM_Position_Type_Lookup__c = regionType.Id,
      BI_TM_Start_date__c = Datetime.now().date()
    );
    insert region;
    BI_TM_Territory__c district = new BI_TM_Territory__c(
      BI_TM_Business__c = businessCode, BI_TM_Country_Code__c = countryCode,
      BI_TM_Parent_Position__c = region.Id, BI_TM_Position_Level__c = 'District',
      BI_TM_Position_Type_Lookup__c = districtType.Id,
      BI_TM_Start_date__c = Datetime.now().date()
    );
    insert district;
    BI_TM_Territory__c rep = new BI_TM_Territory__c(
      BI_TM_Business__c = businessCode, BI_TM_Country_Code__c = countryCode,
      BI_TM_Parent_Position__c = district.Id, BI_TM_Position_Level__c = 'Representative',
      BI_TM_Start_date__c = Datetime.now().date(),
      BI_TM_Position_Type_Lookup__c = posType.Id,
      BI_TM_Territory_ND__c = territoryId
    );
    insert rep;
    BI_TM_Territory__c rep2 = new BI_TM_Territory__c(
      BI_TM_Business__c = businessCode, BI_TM_Country_Code__c = countryCode,
      BI_TM_Parent_Position__c = district.Id, BI_TM_Position_Level__c = 'Representative',
      BI_TM_Start_date__c = Datetime.now().date(),
      BI_TM_Position_Type_Lookup__c = posType.Id,
      BI_TM_Territory_ND__c = territory2Id
    );
    insert rep2;

    BI_TM_Territory_to_Position__c terrToPos = new BI_TM_Territory_to_Position__c (
      BI_TM_Start_Date__c = Datetime.now().date(), BI_TM_Territory__c = territoryId, BI_TM_Position__c = rep.Id
    );
    insert terrToPos;

    return new List<BI_TM_Territory__c> {region, district, rep, rep2};
  }

  private static void createUserToPos (BI_TM_Territory__c pos1, BI_TM_Territory__c pos2, User userPos1, User userPos2) {
    List<BI_TM_User_mgmt__c> userMngToCreate = new List<BI_TM_User_mgmt__c>();

    for (User forUser :
      [SELECT Id, Name, FirstName, LastName, Username, Alias, Email, CommunityNickname, TimeZoneSidKey, LanguageLocaleKey, DefaultCurrencyIsoCode, LocaleSidKey FROM User WHERE Id IN (:userPos1.Id, :userPos2.Id)]
    ) {

      userMngToCreate.add(new BI_TM_User_mgmt__c(
        Name = forUser.Name, BI_TM_UserId_Lookup__c = forUser.Id, BI_TM_First_name__c = forUser.FirstName, BI_TM_Last_name__c = forUser.LastName,
        BI_TM_Username__c = forUser.Username, BI_TM_Alias__c = forUser.Alias, BI_TM_Active__c = true,
        BI_TM_Start_date__c = Datetime.now().date(), BI_TM_Business__c = businessCode,
        BI_TM_COMMUNITYNICKNAME__c = forUser.CommunityNickname, BI_TM_Country__c = countryCode,
        BI_TM_UserCountryCode__c = countryCode, BI_TM_Email__c = forUser.Email,
        BI_TM_Currency__c = forUser.DefaultCurrencyIsoCode, BI_TM_LanguageLocaleKey__c = forUser.LanguageLocaleKey,
        BI_TM_LocaleSidKey__c = forUser.LocaleSidKey, BI_TM_TimeZoneSidKey__c = forUser.TimeZoneSidKey
      ));

    }
    insert userMngToCreate;

    List<BI_TM_User_territory__c> userToPosToCreate = new List<BI_TM_User_territory__c>();
    for (BI_TM_User_mgmt__c forUSM : userMngToCreate) {
      userToPosToCreate.add(new BI_TM_User_territory__c(
       BI_TM_Assignment_Type__c = 'Primary', BI_TM_Business__c = businessCode, BI_TM_Country_Code__c = countryCode,
        BI_TM_Territory1__c = (forUSM.BI_TM_UserId_Lookup__c == userPos1.Id) ? pos1.Id : pos2.Id, BI_TM_Primary__c = true, BI_TM_Start_date__c = Datetime.now().date(),
        BI_TM_User_mgmt_tm__c = forUSM.Id
      ));
    }

    insert userToPosToCreate;

  }

  private static BI_TM_Position_Relation__c createPositionRelation(BI_TM_Territory__c child, BI_TM_Territory__c parent, Id alignId) {
    return new BI_TM_Position_Relation__c(
      BI_TM_Active__c = true, BI_TM_Alignment_Cycle__c = alignId, BI_TM_Business__c = businessCode, BI_TM_Country_Code__c = countryCode,
      BI_TM_Parent_Position__c = parent.Id, BI_TM_Position__c = child.Id
    );

  }


  @isTest
  static void createNewGeoToTerr() {
    createData();

    System.debug(countryCode + ' / ' + businessCode);
    String terExtId = 'US_US-' + businessCode + '-TerrTest-1';
    String terExt2Id = 'US_US-' + businessCode + '-TerrTest-2';
    System.debug(terExtId);
    BI_TM_Territory_ND__c terr1 = [SELECT Id FROM BI_TM_Territory_ND__c WHERE BI_TM_External_Id__c = :terExtId LIMIT 1];
    BI_TM_Territory_ND__c terr2 = [SELECT Id FROM BI_TM_Territory_ND__c WHERE BI_TM_External_Id__c = :terExt2Id LIMIT 1];
    BI_TM_Geography__c geo1  = [SELECT Id, BI_TM_Country_Code__c FROM BI_TM_Geography__c WHERE BI_TM_External_id_geography__c = 'US_1000' LIMIT 1];
    BI_TM_Geography__c geo2  = [SELECT Id, BI_TM_Country_Code__c FROM BI_TM_Geography__c WHERE BI_TM_External_id_geography__c = 'US_2000' LIMIT 1];
    BI_TM_Alignment__c align1 = [SELECT Id FROM BI_TM_Alignment__c WHERE 	BI_TM_AlignmentExternalId__c = 'US_Alig_Test1' LIMIT 1];

    List<BI_TM_Territory__c> regDistPosList = createPosition(terr1.Id, terr2.Id);
    BI_TM_Territory__c reg = regDistPosList.get(0);
    BI_TM_Territory__c dist = regDistPosList.get(1);

    BI_TM_Geography_to_territory__c geoToTerrTest;
    BI_TM_Geography_to_territory__c geoToTerrTest2;

    geoToTerrTest = new BI_TM_Geography_to_territory__c(
      BI_TM_Parent_alignment__c = align1.Id,
      BI_TM_Geography__c = geo1.Id,
      BI_TM_Territory_ND__c = terr1.Id,
      BI_TM_Business__c = businessCode,
      BI_TM_Country_Code__c = countryCode,
      BI_TM_Is_Active__c = true,
      BI_TM_Start_date__c = Datetime.now().date()
    );

    geoToTerrTest2 = new BI_TM_Geography_to_territory__c(
      BI_TM_Parent_alignment__c = align1.Id,
      BI_TM_Geography__c = geo2.Id,
      BI_TM_Territory_ND__c = terr2.Id,
      BI_TM_Business__c = businessCode,
      BI_TM_Country_Code__c = countryCode,
      BI_TM_Is_Active__c = true,
      BI_TM_Start_date__c = Datetime.now().date()
    );
    insert new List<BI_TM_Geography_to_territory__c> {geoToTerrTest, geoToTerrTest2};

    List<BI_TM_Geography_to_territory__c> geoToTerrList =
      [SELECT Id, BI_TM_Is_Active__c, BI_TM_Business__c, BI_TM_Country_Code__c, BI_TM_Status__c
        FROM BI_TM_Geography_to_territory__c WHERE BI_TM_Parent_alignment__c = :align1.Id];
    System.assertEquals(2, geoToTerrList.size());
    // Approvation needed and as no maneger as set-> Update status to No response and no activation nor approval process created

    for (BI_TM_Geography_to_territory__c forGeoToTerr : geoToTerrList) {
      System.assertEquals(false, forGeoToTerr.BI_TM_Is_Active__c);
      System.assertEquals(BI_TM_GeoToTerr_Approval_Handler.NO_RESPONSE_GEOTOTERR_STATUS, forGeoToTerr.BI_TM_Status__c);
      System.assertEquals(businessCode, forGeoToTerr.BI_TM_Business__c);
      System.assertEquals(countryCode, forGeoToTerr.BI_TM_Country_Code__c);
    }

    // No approval created
    Map<Id, ProcessInstanceWorkitem> apprMap =  BI_TM_GeoToTerr_Approval_Handler.getRelatedApprovalProcesses(geoToTerrList);
    System.assert(apprMap.isEmpty());

  }

  @isTest
  static void createNewGeoToTerr2() {
    createData();

    System.debug(countryCode + ' / ' + businessCode);
    String terExtId = 'US_US-' + businessCode + '-TerrTest-1';
    String terExt2Id = 'US_US-' + businessCode + '-TerrTest-2';
    System.debug(terExtId);
    BI_TM_Territory_ND__c terr1 = [SELECT Id FROM BI_TM_Territory_ND__c WHERE BI_TM_External_Id__c = :terExtId LIMIT 1];
    BI_TM_Territory_ND__c terr2 = [SELECT Id FROM BI_TM_Territory_ND__c WHERE BI_TM_External_Id__c = :terExt2Id LIMIT 1];
    BI_TM_Geography__c geo1  = [SELECT Id, BI_TM_Country_Code__c FROM BI_TM_Geography__c WHERE BI_TM_External_id_geography__c = 'US_1000' LIMIT 1];
    BI_TM_Geography__c geo2  = [SELECT Id, BI_TM_Country_Code__c FROM BI_TM_Geography__c WHERE BI_TM_External_id_geography__c = 'US_2000' LIMIT 1];
    BI_TM_Alignment__c align1 = [SELECT Id FROM BI_TM_Alignment__c WHERE 	BI_TM_AlignmentExternalId__c = 'US_Alig_Test1' LIMIT 1];

    List<BI_TM_Territory__c> regDistPosList = createPosition(terr1.Id, terr2.Id);
    BI_TM_Territory__c reg = regDistPosList.get(0);
    BI_TM_Territory__c dist = regDistPosList.get(1);

    BI_TM_Geography_to_territory__c geoToTerrTest;
    BI_TM_Geography_to_territory__c geoToTerrTest2;

    createUserToPos (reg, dist, admin, ds);

    Test.startTest();

    geoToTerrTest = new BI_TM_Geography_to_territory__c(
      BI_TM_Parent_alignment__c = align1.Id,
      BI_TM_Geography__c = geo1.Id,
      BI_TM_Territory_ND__c = terr1.Id,
      BI_TM_Business__c = businessCode,
      BI_TM_Country_Code__c = countryCode,
      BI_TM_Is_Active__c = true,
      BI_TM_Start_date__c = Datetime.now().date()
    );

    geoToTerrTest2 = new BI_TM_Geography_to_territory__c(
      BI_TM_Parent_alignment__c = align1.Id,
      BI_TM_Geography__c = geo2.Id,
      BI_TM_Territory_ND__c = terr2.Id,
      BI_TM_Business__c = businessCode,
      BI_TM_Country_Code__c = countryCode,
      BI_TM_Is_Active__c = true,
      BI_TM_Start_date__c = Datetime.now().date()
    );

    List<BI_TM_Geography_to_territory__c> geoToTerToCreate = new List<BI_TM_Geography_to_territory__c> {geoToTerrTest, geoToTerrTest2};
    insert geoToTerToCreate;

    Test.stopTest();

    // Approver are set and status is changed
    List<BI_TM_Geography_to_territory__c> geoToTerrToApp = [SELECT Id, BI_TM_Is_Active__c, BI_TM_Business__c, BI_TM_Country_Code__c, BI_TM_Status__c, BI_TM_Next_Approver__c FROM BI_TM_Geography_to_territory__c WHERE Id IN :geoToTerToCreate];
    Integer count = 0;
    for (BI_TM_Geography_to_territory__c forGeoToTerr : geoToTerrToApp) {
      System.assertEquals(false, forGeoToTerr.BI_TM_Is_Active__c); // Pending to approve
      System.assertEquals('Pending approval - To activate', forGeoToTerr.BI_TM_Status__c);
      System.assertEquals(businessCode, forGeoToTerr.BI_TM_Business__c);
      System.assertEquals(countryCode, forGeoToTerr.BI_TM_Country_Code__c);
      System.assertEquals(admin.Id, forGeoToTerr.BI_TM_Next_Approver__c);
      count++;
    }
    System.assertEquals(2,count);

    Map<Id, ProcessInstanceWorkitem> processWIMap = BI_TM_GeoToTerr_Approval_Handler.getRelatedApprovalProcesses(geoToTerrToApp);
    System.assertEquals(2, processWIMap.keyset().size());

    for (Id forGeoToTerId : processWIMap.keyset()) {
      ProcessInstanceWorkitem procWI = processWIMap.get(forGeoToTerId);

      System.assertEquals(BI_TM_GeoToTerr_Approval_Handler.PENDING_APPROVAL_STATUS, procWI.ProcessInstance.Status);
      System.assertEquals(ds.Id, procWI.ActorId);

    }

    // Simulating waiting period passed
    BI_TM_GeoToTerr_Approval_Handler.setNextApprover(geoToTerrToApp);

    geoToTerrToApp = [SELECT Id, BI_TM_Is_Active__c, BI_TM_Business__c, BI_TM_Country_Code__c, BI_TM_Status__c, BI_TM_Next_Approver__c FROM BI_TM_Geography_to_territory__c WHERE Id IN :geoToTerToCreate];
    count = 0;
    for (BI_TM_Geography_to_territory__c forGeoToTerr : geoToTerrToApp) {
      System.assertEquals(false, forGeoToTerr.BI_TM_Is_Active__c); // Pending to approve
      System.assertEquals('Pending approval - To activate', forGeoToTerr.BI_TM_Status__c);
      System.assertEquals(businessCode, forGeoToTerr.BI_TM_Business__c);
      System.assertEquals(countryCode, forGeoToTerr.BI_TM_Country_Code__c);
      System.assertEquals(null, forGeoToTerr.BI_TM_Next_Approver__c);
      count++;
    }
    System.assertEquals(2,count);

    processWIMap = BI_TM_GeoToTerr_Approval_Handler.getRelatedApprovalProcesses(geoToTerrToApp);
    System.assertEquals(2, processWIMap.keyset().size());

    for (Id forGeoToTerId : processWIMap.keyset()) {
      ProcessInstanceWorkitem procWI = processWIMap.get(forGeoToTerId);

      System.assertEquals(BI_TM_GeoToTerr_Approval_Handler.PENDING_APPROVAL_STATUS, procWI.ProcessInstance.Status);
      System.assertEquals(admin.Id, procWI.ActorId);

    }

    // Simulating waiting period passed: Last step: No response for this approval
    BI_TM_GeoToTerr_Approval_Handler.setNextApprover(geoToTerrToApp);

    geoToTerrToApp = [SELECT Id, BI_TM_Is_Active__c, BI_TM_Business__c, BI_TM_Country_Code__c, BI_TM_Status__c, BI_TM_Next_Approver__c FROM BI_TM_Geography_to_territory__c WHERE Id IN :geoToTerToCreate];
    count = 0;
    for (BI_TM_Geography_to_territory__c forGeoToTerr : geoToTerrToApp) {
      System.assertEquals(false, forGeoToTerr.BI_TM_Is_Active__c); // Not approved
      System.assertEquals(BI_TM_GeoToTerr_Approval_Handler.NO_RESPONSE_GEOTOTERR_STATUS, forGeoToTerr.BI_TM_Status__c);
      System.assertEquals(null, forGeoToTerr.BI_TM_Next_Approver__c);
      count++;
    }
    System.assertEquals(2,count);

    processWIMap = BI_TM_GeoToTerr_Approval_Handler.getRelatedApprovalProcesses(geoToTerrToApp);
    System.assertEquals(true, processWIMap.isEmpty());

  }

  @isTest
  static void createNewGeoToTerrFuture() {
    createData();

    System.debug(countryCode + ' / ' + businessCode);
    String terExtId = 'US_US-' + businessCode + '-TerrTest-1';
    String terExt2Id = 'US_US-' + businessCode + '-TerrTest-2';
    System.debug(terExtId);
    BI_TM_Territory_ND__c terr1 = [SELECT Id FROM BI_TM_Territory_ND__c WHERE BI_TM_External_Id__c = :terExtId LIMIT 1];
    BI_TM_Territory_ND__c terr2 = [SELECT Id FROM BI_TM_Territory_ND__c WHERE BI_TM_External_Id__c = :terExt2Id LIMIT 1];
    BI_TM_Geography__c geo1  = [SELECT Id, BI_TM_Country_Code__c FROM BI_TM_Geography__c WHERE BI_TM_External_id_geography__c = 'US_1000' LIMIT 1];
    BI_TM_Geography__c geo2  = [SELECT Id, BI_TM_Country_Code__c FROM BI_TM_Geography__c WHERE BI_TM_External_id_geography__c = 'US_2000' LIMIT 1];
    BI_TM_Alignment__c align2 = [SELECT Id FROM BI_TM_Alignment__c WHERE 	BI_TM_AlignmentExternalId__c = 'US_Alig_Test2' LIMIT 1];

    List<BI_TM_Territory__c> regDistPosList = createPosition(terr1.Id, terr2.Id);
    BI_TM_Territory__c reg = regDistPosList.get(0);
    BI_TM_Territory__c dist = regDistPosList.get(1);
    BI_TM_Territory__c rep1 = regDistPosList.get(2);
    BI_TM_Territory__c rep2 = regDistPosList.get(3);

    BI_TM_Position_Relation__c posRelationParents = createPositionRelation(reg, dist, align2.Id);
    BI_TM_Position_Relation__c posRelationRep1 = createPositionRelation(rep1, reg, align2.Id);
    BI_TM_Position_Relation__c posRelationRep2 = createPositionRelation(rep2, reg, align2.Id);
    insert new List<BI_TM_Position_Relation__c> {posRelationParents, posRelationRep1, posRelationRep2};

    createUserToPos (reg, dist, admin, ds);

    BI_TM_Geography_to_territory__c geoToTerrTest;
    BI_TM_Geography_to_territory__c geoToTerrTest2;
    Test.startTest();

    geoToTerrTest = new BI_TM_Geography_to_territory__c(
      BI_TM_Parent_alignment__c = align2.Id,
      BI_TM_Geography__c = geo1.Id,
      BI_TM_Territory_ND__c = terr1.Id,
      BI_TM_Business__c = businessCode,
      BI_TM_Country_Code__c = countryCode,
      BI_TM_Is_Active__c = true,
      BI_TM_Start_date__c = Datetime.now().date()
    );

    geoToTerrTest2 = new BI_TM_Geography_to_territory__c(
      BI_TM_Parent_alignment__c = align2.Id,
      BI_TM_Geography__c = geo2.Id,
      BI_TM_Territory_ND__c = terr2.Id,
      BI_TM_Business__c = businessCode,
      BI_TM_Country_Code__c = countryCode,
      BI_TM_Is_Active__c = true,
      BI_TM_Start_date__c = Datetime.now().date()
    );

    List<BI_TM_Geography_to_territory__c> geoToTerToCreate = new List<BI_TM_Geography_to_territory__c> {geoToTerrTest, geoToTerrTest2};
    insert geoToTerToCreate;

    Test.stopTest();

    // Approver are set and status is changed
    List<BI_TM_Geography_to_territory__c> geoToTerrToApp = [SELECT Id, BI_TM_Is_Active__c, BI_TM_Business__c, BI_TM_Country_Code__c, BI_TM_Status__c, BI_TM_Next_Approver__c FROM BI_TM_Geography_to_territory__c WHERE Id IN :geoToTerToCreate];
    Integer count = 0;
    for (BI_TM_Geography_to_territory__c forGeoToTerr : geoToTerrToApp) {
      System.assertEquals(false, forGeoToTerr.BI_TM_Is_Active__c); // Pending to approve
      System.assertEquals('Pending approval - To activate', forGeoToTerr.BI_TM_Status__c);
      System.assertEquals(businessCode, forGeoToTerr.BI_TM_Business__c);
      System.assertEquals(countryCode, forGeoToTerr.BI_TM_Country_Code__c);
      System.assertEquals(ds.Id, forGeoToTerr.BI_TM_Next_Approver__c);
      count++;
    }
    System.assertEquals(2,count);

    Map<Id, ProcessInstanceWorkitem> processWIMap = BI_TM_GeoToTerr_Approval_Handler.getRelatedApprovalProcesses(geoToTerrToApp);
    System.assertEquals(2, processWIMap.keyset().size());

    for (Id forGeoToTerId : processWIMap.keyset()) {
      ProcessInstanceWorkitem procWI = processWIMap.get(forGeoToTerId);

      System.assertEquals(BI_TM_GeoToTerr_Approval_Handler.PENDING_APPROVAL_STATUS, procWI.ProcessInstance.Status);
      System.assertEquals(admin.Id, procWI.ActorId);

    }


  }

}