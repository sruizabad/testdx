public class BI_TM_Geo_To_Territory_Handler implements BI_TM_ITriggerHandler
{
  private static boolean run = true;

  public void BeforeInsert(List<SObject> newItems) {
  }

  public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
  }


  public void BeforeDelete(Map<Id,SObject> oldItems) {
    Geo_to_Terr_Delete(oldItems);
  }

  public void AfterInsert(Map<Id, SObject> newItems) {}
  public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
/*    if(run){
      run = false;
      List<BI_TM_Geography_to_territory__c> itemsToApprove = new List<BI_TM_Geography_to_territory__c>();
      for(Id ni : newItems.keySet()){
        /*if(((BI_TM_Geography_to_territory__c)newItems.get(ni)).BI_TM_Status__c.startsWith('Pending approval')){*/
  /*        BI_TM_Geography_to_territory__c gt = (BI_TM_Geography_to_territory__c) newItems.get(ni);
          itemsToApprove.add(gt);
        }
      //}
      if(itemsToApprove.size() > 0){
        BI_TM_GeoToTerr_Approval_Handler handler = new BI_TM_GeoToTerr_Approval_Handler();
        handler.setNextApprover(itemsToApprove);
      }
    }*/
  }
  public void AfterDelete(Map<Id, SObject> oldItems) {}

  public void AfterUndelete(Map<Id, SObject> oldItems) {}


    private void Geo_to_Terr_Delete(Map<Id,SObject> oldItems) {

      list<BI_TM_Geography_to_territory__c> geoToTer = [select Id,BI_TM_Parent_alignment__r.BI_TM_Status__c from BI_TM_Geography_to_territory__c where Id IN : oldItems.keyset()];

      for(BI_TM_Geography_to_territory__c obj: geoToTer)
      {
        if(obj.BI_TM_Parent_alignment__r.BI_TM_Status__c=='Past' || obj.BI_TM_Parent_alignment__r.BI_TM_Status__c=='Active')
        {
          olditems.get(obj.id).addError('Geography to Territory assignments cannot be deleted from an active or past Alignment');

        }
      }

    }
  }