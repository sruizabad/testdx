/********************************************************************************
Name:  BI_TM_GeototerrCreateEditExtn
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Visualforce controller extension to save/edit geo & terr data
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Rao G               10/12/2015   INITIAL DEVELOPMENT
*********************************************************************************/
public with sharing class BI_TM_GeototerrCreateEditExtn {


    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
    // the actual Geography_to_territory
    public BI_TM_Geography_to_territory__c geototerr {get; set;}
    public String selectedTerritory {get; set;}
    private String parentAlignmentFFtype;
    private String parentAlignmentCountryCode;
    private String parentAlignmentBusiness;
    private String parentAlignment;
    public boolean renderTerr {
        get;
        set;
    }

    public BI_TM_GeototerrCreateEditExtn(ApexPages.StandardController controller) {
        //initialize the stanrdard controller
        controller = controller;
        String recId = ApexPages.currentPage().getParameters().get('id');

        geototerr = (BI_TM_Geography_to_territory__c)controller.getRecord();
        if ( recId != null ) {
            String soql = getCreatableFieldsSOQL('BI_TM_Geography_to_territory__c', geototerr.getsObjectType(), 'id=\'' + recId + '\'');
            system.debug('geo to terr soql ####:' + soql);
            geototerr = (BI_TM_Geography_to_territory__c) Database.query(soql);
            system.debug('geo to terr ####:' + geototerr);
            renderTerr = true;
        }
        try {
            if ( this.geototerr != null && geototerr.BI_TM_Parent_alignment__c != null) {
                parentAlignment = geototerr.BI_TM_Parent_alignment__c;
                system.debug('geototerr - Alignment ####:' + parentAlignment);

                BI_TM_Alignment__c parentAlignmentLoc = [select id, BI_TM_FF_type__c, BI_TM_Country_Code__c,BI_TM_Business__c from BI_TM_Alignment__c where id = :parentAlignment];
                parentAlignmentFFtype = parentAlignmentLoc != null ? parentAlignmentLoc.BI_TM_FF_type__c : null;
                parentAlignmentCountryCode = parentAlignmentLoc != null ? parentAlignmentLoc.BI_TM_Country_Code__c : null;
                parentAlignmentBusiness = parentAlignmentLoc != null ? parentAlignmentLoc.BI_TM_Business__c: null;
                system.debug('parentAlignmentCountryCode' + parentAlignmentCountryCode);
                parentAlignment = parentAlignmentLoc != null ? parentAlignmentLoc.Id : null;
            }

            geototerr.BI_TM_Country_Code__c = parentAlignmentCountryCode;
            geototerr.BI_TM_Business__C= parentAlignmentBusiness;
            system.debug('Geo to Terr Country Code--->' + geototerr.BI_TM_Country_Code__c);
            //renderTerr=true;
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error loading page'));
        }
    }

    public List < SelectOption > getTerritories() {
        List < SelectOption > options = new List < SelectOption > ();
        options.add(new SelectOption('None', '--None--'));
        String terrActivePLVal = Label.BI_TM_Update_Utility_TerrActivePLValue;
        system.debug('terrActivePLVal :' + terrActivePLVal );
        try {
            for (BI_TM_Territory_ND__c terr : [select Name from BI_TM_Territory_ND__c where BI_TM_Field_Force__c = : parentAlignmentFFtype and BI_TM_Country_Code__c = :parentAlignmentCountryCode and (BI_TM_End_Date__c = null OR BI_TM_End_Date__c >= TODAY)]) {
                options.add(new SelectOption(terr.Id, terr.Name));
            }
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BI_TM_GeotoTerrExtn_TerrLoadFailMsg));
        }
        system.debug('Territories--->' + options);
        return options;
    }
    public PageReference saveGeototerr() {
                          String x=BI_TM_Alignment__c.sobjecttype.getDescribe().getKeyPrefix();
                          system.debug('Prefix Value'+x);


        PageReference returnURL = new PageReference(parentAlignment != null ? '/' + parentAlignment : x+'/o');
        system.debug('Alignment is--->' + parentAlignment);
        List<BI_TM_Alignment__c> alignmentvalues = [Select Id, BI_TM_Status__c From BI_TM_Alignment__c Where id = :parentAlignment];
        BI_TM_Geography_to_territory__c newvalues = new BI_TM_Geography_to_territory__c();
        system.debug('Status is =====>' + alignmentvalues[0].BI_TM_Status__c);
        //if(alignmentvalues.size()>0){
        if (alignmentvalues[0].BI_TM_Status__c == 'Past') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BI_TM_UpdateNotAllowOnPastAlignment));
            return new PageReference('/apex/BI_TM_GeototerrCreateEditPage');
        }
        //}
        try {
            system.debug('============');
            String recordId = ApexPages.currentPage().getParameters().get('id');
            if (recordId != null) {

                newvalues.BI_TM_Is_Active__c = geototerr.BI_TM_Is_Active__c;
                newvalues.BI_TM_Primary_territory__c = geototerr.BI_TM_Primary_territory__c;
                newvalues.Id = recordId;
                try{
                update newvalues;
                }catch(Exception ex){
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BI_TM_GeotoTerrExtn_SaveFaileMsg + ex.getMessage()));
                  return null;
                }

            } else {
                geototerr.BI_TM_Territory_ND__c = selectedTerritory;
                insert geototerr;
            }
            system.debug('=======+++++=====' + geototerr);
            return returnURL;
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BI_TM_GeotoTerrExtn_SaveFaileMsg + ex.getMessage()));
            return null;
        }

    }

    private string getCreatableFieldsSOQL(String objectName, Schema.SObjectType objectType, String whereClause) {
        List < String > sObjectFields = new List < String > ();
        if (objectType != null) {
            sObjectFields.addAll(
                objectType.getDescribe().fields.getMap().keySet());
        }
        String allSObjectFieldsQuery = 'SELECT ' + sObjectFields.get(0);
        for (Integer i = 1; i < sObjectFields.size(); i++) {
            allSObjectFieldsQuery += ', ' + sObjectFields.get(i);
        }
        return allSObjectFieldsQuery + ' FROM ' + objectName + ' WHERE ' + whereClause;

    }

    public PageReference cancel() {
        String x=BI_TM_Alignment__c.sobjecttype.getDescribe().getKeyPrefix();
                          system.debug('Prefix Value'+x);
        PageReference returnURL = new PageReference(parentAlignment != null ? '/' + parentAlignment : x+'/o');
        return returnURL;
    }

}