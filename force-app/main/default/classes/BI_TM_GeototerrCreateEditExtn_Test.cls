@IsTest
public class BI_TM_GeototerrCreateEditExtn_Test {
  static testMethod void testGeototerr(){
    BI_TM_Territory__c Terr= new BI_TM_Territory__c();
    BI_TM_Territory__c Terr1= new BI_TM_Territory__c();
    BI_TM_FF_type__c FF= new BI_TM_FF_type__c();
    User currentuser = new User();
    currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
    PageReference pageRef = Page.BI_TM_GeototerrCreateEditPage;
    Test.setCurrentPage(pageRef);
    System.Test.startTest();
    FF.Name='Test MX FF21';
    FF.BI_TM_Business__c='PM';
    FF.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    insert FF;
    BI_TM_Territory_ND__c newTerr= new BI_TM_Territory_ND__c();
    newTerr.Name='testnewterr';
    newTerr.BI_TM_Field_Force__c=FF.Id;
    newTerr.BI_TM_Start_Date__c=Date.today().addDays(-2);
    newTerr.BI_TM_Business__c='PM';
    newTerr.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    Insert newTerr;
    BI_TM_Position_Type__c pt = new BI_TM_Position_Type__c();
    pt.Name='Test MX FF';
    pt.BI_TM_Business__c='AH';
    pt.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    insert pt;
    Terr1.Name='FFHierarchy test21';
    Terr1.BI_TM_Position_Level__c='Country';
    Terr1.BI_TM_Position_Type_Lookup__c=pt.Id;
    Terr1.BI_TM_Is_Root__c=True;
    Terr1.BI_TM_FF_type__c=FF.Id;
    Terr1.BI_TM_Business__c='PM';
    Terr1.BI_TM_Visible_in_crm__c=True;
    // Terr1.BI_TM_Is_Active_Checkbox__c=True;
    Terr1.BI_TM_Start_date__c=Date.today().addDays(-1);
    Terr1.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
    //Terr1.BI_TM_End_date__c=system.Today();
    // Terr1.BI_TM_Is_Active_Checkbox__c=false;

    Insert Terr1;
    //rtype1=[select Id from RecordType where Name='Territory'Limit 1];
    Terr.Name='Test Tam21';
    Terr.BI_TM_FF_type__c=FF.Id;
    Terr.BI_TM_Business__c='PM';
    Terr.BI_TM_Position_Level__c='Country';
    Terr.BI_TM_Position_Type_Lookup__c=pt.Id;
    Terr.BI_TM_Parent_Position__c=Terr1.Id;
    Terr.BI_TM_Visible_in_crm__c=True;
    //Terr.BI_TM_Is_Active_Checkbox__c=True;
    Terr.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
    Terr.BI_TM_Start_date__c=Date.today().addDays(-1);
    //Terr.BI_TM_End_date__c=system.Today();
    //Terr.BI_TM_Is_Active_Checkbox__c=false;

    Insert Terr;
    BI_TM_Geography_type__c gtype = new BI_TM_Geography_type__c(Name='Brick',BI_TM_Country_Code__c=currentuser.Country_Code_BI__c);
    insert gtype;

    BI_TM_Geography__c geoG1 = new BI_TM_Geography__c(Name='GX9',BI_TM_Is_Active__c=true,BI_TM_Geography_type__c=gtype.Id,BI_TM_Country_Code__c=currentuser.Country_Code_BI__c);
    insert geoG1;

    BI_TM_FF_type__c fftype = new BI_TM_FF_type__c(Name='TESTFF',BI_TM_Country_Code__c=currentuser.Country_Code_BI__c);
    insert fftype;
    BI_TM_Alignment__c al = new BI_TM_Alignment__c(name='test name',BI_TM_FF_type__c=FF.Id,BI_TM_Country_Code__c=currentuser.Country_Code_BI__c,BI_TM_Business__c='PM',BI_TM_Start_date__c=system.today());
    insert al;

    BI_TM_Geography_to_territory__c gt = new BI_TM_Geography_to_territory__c(BI_TM_Parent_alignment__c=al.id,BI_TM_Geography__c=geoG1.Id,BI_TM_Is_Active__c=True,BI_TM_Territory_ND__c=newTerr.id,BI_TM_Country_Code__c=currentuser.Country_Code_BI__c);
    Insert gt;
    ApexPages.currentPage().getParameters().put('Id',gt.Id);
    ApexPages.StandardController controller = new ApexPages.StandardController(gt);
    BI_TM_GeototerrCreateEditExtn getoExtn = new BI_TM_GeototerrCreateEditExtn(controller);
    getoExtn.getTerritories();
    getoExtn.saveGeototerr();
    getoExtn.cancel();
    System.Test.stopTest();
  }
  static void createTestData(){

  }
}