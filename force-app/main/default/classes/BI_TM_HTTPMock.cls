global with sharing class BI_TM_HTTPMock  implements HTTPCalloutMock{
    global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('Territory Created succesfully');
        res.setStatusCode(201);
        return res;
    }
}