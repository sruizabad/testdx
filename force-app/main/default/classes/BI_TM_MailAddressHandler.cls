public class BI_TM_MailAddressHandler{
     public BI_TM_MailAddressHandler(Map<Id, SObject> addM){
        List<User> u = new List<User>();
        
        List<BI_TM_User_Address__c> usrAdd = new List<BI_TM_User_Address__c>();
        usrAdd=[select BI_TM_Address_Line_1__c,BI_TM_Address_Line_2__c,BI_TM_City__c,BI_TM_State__c,BI_TM_Address_Type__c,BI_TM_Zip__c,
                 BI_TM_Country_Code__c,BI_TM_User__r.BI_TM_UserId_Lookup__c from BI_TM_User_Address__c where Id IN :addM.keyset() and BI_TM_Address_Type__c='MAILING ADDRESS'];
        for(BI_TM_User_Address__c adddet:usrAdd){
          User u1= new User();
                if(adddet.BI_TM_Address_Line_2__c!=NULL)
              u1.Street=adddet.BI_TM_Address_Line_1__c+','+adddet.BI_TM_Address_Line_2__c;
           else
              u1.Street=adddet.BI_TM_Address_Line_1__c;
              u1.City=adddet.BI_TM_City__c;
              u1.PostalCode=adddet.BI_TM_Zip__c;
              u1.State=adddet.BI_TM_State__c;
              u1.Country=adddet.BI_TM_Country_Code__c;
              u1.Id=adddet.BI_TM_User__r.BI_TM_UserId_Lookup__c;
          if(adddet.BI_TM_User__r.BI_TM_UserId_Lookup__c!=NULL && adddet.BI_TM_Address_Type__c=='MAILING ADDRESS')    
           u.add(u1); 
        }     
        if(u.size()>0)
        {
            update u;
        }
        
     }
 }