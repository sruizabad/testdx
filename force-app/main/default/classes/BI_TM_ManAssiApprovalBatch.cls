global class BI_TM_ManAssiApprovalBatch implements Database.Batchable<sObject>{
  String strApproved = 'All Approved';
    String strRejected = 'Rejected';
    string specialChar = '/';
    global Database.QueryLocator start(Database.BatchableContext BC) {

        String strQuery = 'SELECT Id, Name, BI_TM_Approval_Status__c, BI_TM_old_manualAssignment__c FROM BI_TM_Account_To_Territory__c WHERE BI_TM_old_manualAssignment__c!= null';// AND BI_TM_Approval_Status!= \''+strApproved+'\' AND BI_TM_Approval_Status!= \''+strRejected +'\''BI_TM_old_manualAssignment__c!= null';
        return Database.getQueryLocator(strQuery);

    }

     global void execute(Database.BatchableContext BC, List<BI_TM_Account_To_Territory__c> lstmanAss){
        List<BI_TM_Account_To_Territory__c> lstFilteredManAss = new List<BI_TM_Account_To_Territory__c>();
        Map<Id, List<ProcessInstance>> mapTargetObjIdToListProInstance = new Map<Id, List<ProcessInstance>>();
        List<BI_TM_Account_To_Territory__c> lstManAssForUpdate = new List<BI_TM_Account_To_Territory__c>();

        for(BI_TM_Account_To_Territory__c objMA :  lstmanAss) {

            if(objMA.BI_TM_Approval_Status__c != 'All Approved' && objMA.BI_TM_Approval_Status__c!= 'Rejected')
                lstFilteredManAss.add(objMA);
        }

        Map<Id, BI_TM_Account_To_Territory__c> mapIdToManAss = new Map<Id, BI_TM_Account_To_Territory__c>(lstFilteredManAss);

        // Get all the process Instance related to each instance
        for(ProcessInstance objProIns : [SELECT TargetObjectId, Status, Id FROM ProcessInstance WHERE TargetObjectId IN: mapIdToManAss.keyset()]) {

            if(mapTargetObjIdToListProInstance.containskey(objProIns.TargetObjectId))
                mapTargetObjIdToListProInstance.get(objProIns.TargetObjectId).add(objProIns);
            else
                mapTargetObjIdToListProInstance.put(objProIns.TargetObjectId, new List<ProcessInstance>{objProIns});
        }

        System.debug('====== mapIdToManAss.keyset()======='+ mapIdToManAss.keyset());

        // Iterate over the all the User TO Position records which has been changed Position
        for(Id objTarObjId : mapIdToManAss.keyset()) {

            if(mapTargetObjIdToListProInstance.containskey(objTarObjId)) {

                List<Id> lstApprovedPI = new List<Id>();
                Boolean isRejected = false;

                for(ProcessInstance objPI : mapTargetObjIdToListProInstance.get(objTarObjId)) {

                    if(objPI.Status == 'Approved')
                        lstApprovedPI.add(objPI.Id);

                    if(objPI.Status == 'Rejected')
                       isRejected = true;
                }

                // Update the fields as Approved by All the Manager and make empty
                if(lstApprovedPI.size() == mapTargetObjIdToListProInstance.get(objTarObjId).size()) {

                    lstManAssForUpdate.add(new BI_TM_Account_To_Territory__c(Id = objTarObjId, BI_TM_Approval_Status__c= 'All Approved'));

                    // Make inactive Old User to Postion if all the mangers has approved
                    lstManAssForUpdate.add(new BI_TM_Account_To_Territory__c(Id = mapIdToManAss.get(objTarObjId).BI_TM_old_manualAssignment__c , BI_TM_End_date__c = System.today()));
                }

                if(isRejected)
                    lstManAssForUpdate.add(new BI_TM_Account_To_Territory__c(Id = objTarObjId, BI_TM_Approval_Status__c= 'Rejected'));
            }
        }

        System.debug('======lstManAssForUpdate======='+lstManAssForUpdate);

        if(!lstManAssForUpdate.isEmpty())
            update lstManAssForUpdate;



     }

     global void finish(Database.BatchableContext BC){


       Map<Id,BI_TM_Account_To_Territory__c> lst_AccTerr_Pending = new Map<Id,BI_TM_Account_To_Territory__c>([SELECT Id, Name, BI_TM_Approval_Status__c,BI_TM_Territory_FF_Hierarchy_Position__c,BI_TM_Territory_FF_Hierarchy_Position__r.BI_TM_Parent_Position__c, BI_TM_old_manualAssignment__c  FROM BI_TM_Account_To_Territory__c
                                                WHERE BI_TM_old_manualAssignment__c != null AND BI_TM_Approval_Status__c ='Pending for Approval']);



       Set<Id> recordId_DelayManagerApproval=new Set<Id>();

        Map<Id, ProcessInstance> All_Pending_ProcessInst= new Map<Id, ProcessInstance>([SELECT TargetObjectId, CreatedDate, Status, Id,(SELECT ActorId,CreatedDate,OriginalActorId,ProcessInstanceId,Id FROM Workitems)FROM ProcessInstance WHERE TargetObjectId IN: lst_AccTerr_Pending.keyset() and Status='Pending']);

        Map<Id, ProcessInstance> pending_ProcessInst=new Map<Id, ProcessInstance>();
        for(ProcessInstance pIVar: All_Pending_ProcessInst.values()){

                Long CreatedTme = pIVar.CreatedDate.getTime();
                Long CurrentTme = system.now().getTime();
                Long DifferenceTime = CurrentTme - CreatedTme;
                Long DifferenceHours = DifferenceTime / 3600000;

                if(DifferenceHours>48){
                    pending_ProcessInst.put(pIVar.Id, pIVar);
                }

                if(DifferenceHours>96){
                    recordId_DelayManagerApproval.add(pIVar.TargetObjectId);
                }


        }

        //Create list of approvers
        List<Id> approverList=new List<Id>();

        for(ProcessInstance pIVar: pending_ProcessInst.values()){
            for(ProcessInstanceWorkitem wiVar: pIVar.Workitems){
                approverList.add(wiVar.ActorId);
            }
        }

        //Fetch user territory for approvers.
        List<BI_TM_User_mgmt__c> userManagmentList=new List<BI_TM_User_mgmt__c>([SELECT Id FROM BI_TM_User_mgmt__c WHERE BI_TM_UserId_Lookup__c IN :approverList]);

        List<BI_TM_User_territory__c> userToPositionChildRecords=new List<BI_TM_User_territory__c>([SELECT Id, BI_TM_User_mgmt_tm__c, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_Territory1__c, BI_TM_Territory1__r.BI_TM_Parent_Position__c FROM BI_TM_User_territory__c WHERE BI_TM_User_mgmt_tm__c IN :userManagmentList AND BI_TM_Active__c=TRUE and BI_TM_Assignment_Type__c='Primary']);

        //Map of child usermangent with PositionId
        Map<Id, Id> child_actor_vs_Parent_Position=new Map<Id,Id>();
        for(BI_TM_User_territory__c uPVar:userToPositionChildRecords){
            if(uPVar.BI_TM_User_mgmt_tm__c!= NULL && uPVar.BI_TM_Territory1__c!=NULL)
                child_actor_vs_Parent_Position.put(uPVar.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, uPVar.BI_TM_Territory1__r.BI_TM_Parent_Position__c);
        }


        List<Id> ParentPositionId=new List<Id>();
        for(BI_TM_User_territory__c UTVar: userToPositionChildRecords){
            if(UTVar.BI_TM_Territory1__c!=NULL){
                ParentPositionId.add(UTVar.BI_TM_Territory1__r.BI_TM_Parent_Position__c);
            }
        }

        //Fetch parent position users
        List<BI_TM_User_territory__c> userToPositionParentRecords=new List<BI_TM_User_territory__c>([SELECT Id, BI_TM_User_mgmt_tm__c, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_Territory1__c, BI_TM_Territory1__r.BI_TM_Parent_Position__c FROM BI_TM_User_territory__c WHERE BI_TM_Territory1__c IN :ParentPositionId AND BI_TM_Active__c=TRUE and BI_TM_Assignment_Type__c='Primary']);

        //Map parent position vs parent position's user managment
        Map<Id, Id> parent_Position_vs_User= new Map<Id, Id>();
        for(BI_TM_User_territory__c UTVar: userToPositionParentRecords) {
            if(UTVar.BI_TM_User_mgmt_tm__c!=NULL)
            parent_Position_vs_User.put(UTVar.BI_TM_Territory1__c,UTVar.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);
        }
        //Creating map to hold account to territory verses process Instance List
        //Map<Id,List<ProcessInstance>> accTerr_ProcessInst=new Map<Id,List<ProcessInstance>>();
        List<ProcessInstanceWorkitem> workItemUpdate=new List<ProcessInstanceWorkitem>();
        for(ProcessInstance pIVar: pending_ProcessInst.Values()){

            for(ProcessInstanceWorkitem wiVar: pIVar.Workitems){
            ProcessInstanceWorkitem wI=new ProcessInstanceWorkitem();
                WI.Id=WiVar.Id;
                system.debug(parent_Position_vs_User.get(child_actor_vs_Parent_Position.get(wiVar.ActorId)));
                if(parent_Position_vs_User.get(child_actor_vs_Parent_Position.get(wiVar.ActorId))!=Null)
                    WI.ActorId=parent_Position_vs_User.get(child_actor_vs_Parent_Position.get(wiVar.ActorId));
                    workItemUpdate.add(wI);
            }
        }

        Try{
            Update workItemUpdate;
        }
        catch(Exception e){
            System.debug('Error: '+e);
        }

        //Update status of account to territory records which are not approved in 48 Hours
        List<BI_TM_Account_To_Territory__c> UpdateList=new List<BI_TM_Account_To_Territory__c>();

        for(Id loopVarId: recordId_DelayManagerApproval){
            BI_TM_Account_To_Territory__c accToTerr=new BI_TM_Account_To_Territory__c();
            accToTerr=lst_AccTerr_Pending.get(loopVarId);
            if(accToTerr.BI_TM_Approval_Status__c != 'No Respond'){
                accToTerr.BI_TM_Approval_Status__c='No Respond';
                UpdateList.add(accToTerr);
            }
        }

        try{
            system.debug(updatelist);
            Update UpdateList;
        }

        catch(Exception e){
            System.debug('Error: '+e);
        }
        

    }
}