@istest(SeeAllData=true)
private class BI_TM_ManAssiApprovalBatchTest{
    static List<BI_TM_Account_To_Territory__c> accToTerrList= new List<BI_TM_Account_To_Territory__c>();
@isTest
    static void testWithAllDataAccess() {
        
        accToTerrList=[SELECT Id, Name, BI_TM_Approval_Status__c, BI_TM_Account__c, BI_TM_Territory_FF_Hierarchy_Position__c, BI_TM_Direct_Parent_Assignment__c,
        BI_TM_Direct_Child_Assignment__c, BI_TM_HCP_HCO__c, BI_TM_Start_Date__c, BI_TM_End_Date__c, BI_TM_Country_Code__c, BI_TM_old_manualAssignment__c
        FROM BI_TM_Account_To_Territory__c WHERE BI_TM_old_manualAssignment__c!= null];
        system.debug('First Debug:'+accToTerrList);
    }
    
    private static testmethod void testBI_TM_ManAssiApprovalBatchTest(){
        
        List<BI_TM_Account_To_Territory__c> listAccTerrToInsert= new List<BI_TM_Account_To_Territory__c>();
       
        for(BI_TM_Account_To_Territory__c loopVar: accToTerrList){
            
            listAccTerrToInsert.add(new BI_TM_Account_To_Territory__c(BI_TM_Account__c=loopVar.BI_TM_Account__c, BI_TM_Territory_FF_Hierarchy_Position__c=loopVar.BI_TM_Territory_FF_Hierarchy_Position__c, 
            BI_TM_Direct_Parent_Assignment__c=loopVar.BI_TM_Direct_Parent_Assignment__c, BI_TM_Direct_Child_Assignment__c=loopVar.BI_TM_Direct_Child_Assignment__c, BI_TM_HCP_HCO__c=loopVar.BI_TM_HCP_HCO__c, 
            BI_TM_Start_Date__c=loopVar.BI_TM_Start_Date__c, BI_TM_End_Date__c=loopVar.BI_TM_End_Date__c, BI_TM_Country_Code__c=loopVar.BI_TM_Country_Code__c, BI_TM_Business__c=loopVar.BI_TM_Business__c,
            BI_TM_old_manualAssignment__c=loopVar.BI_TM_old_manualAssignment__c));
        }
        
        Test.startTest();
        system.runAs(new User(Id = UserInfo.getUserId())){
            
            //insert listAccTerrToInsert;
            system.debug('Created Records:' +listAccTerrToInsert);
         }
         
         BI_TM_ManAssiApprovalBatch batch=new BI_TM_ManAssiApprovalBatch();
         Id batchId=DataBase.executeBatch(batch, 2000);
         Test.stopTest();
         
    }
}