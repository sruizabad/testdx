/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Batch to delete the manual assignments related to the losing account in the merge process
*  @author:         Antonio Ferrero
*  @created:        07-Sep-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         07-Sep-2018                 Antonio Ferrero             Construction of the class.
*				1.1					20-Sep-2018									Antonio Ferrero							First update the manual assignment if we get an error, we delete it.
*/
global class BI_TM_ManAssig_MergeAcc_Batch implements Database.Batchable<sObject> {

	String query;

	global BI_TM_ManAssig_MergeAcc_Batch() {
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id FROM BI_TM_Account_To_Territory__c WHERE BI_TM_Merged_Account_Check__c = true';
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<BI_TM_Account_To_Territory__c> scope) {
		Map<Id, BI_TM_Account_To_Territory__c> manAssigMap = new Map<Id, BI_TM_Account_To_Territory__c>(scope);
		Database.SaveResult[] updateSrList = Database.update(scope, false);
		List<BI_TM_Account_To_Territory__c> manAssignments2Delete = new List<BI_TM_Account_To_Territory__c>();
		Set<Id> manualAssignmentsWithoutErrors = new Set<Id>();
		for(Database.SaveResult sr : updateSrList){
			if (sr.isSuccess()){
				manualAssignmentsWithoutErrors.add(sr.getId());
			}
		}

		// Compare records that didn´t fail with the original list
		for(Id ma : manAssigMap.keySet()){
			if(!manualAssignmentsWithoutErrors.contains(ma)){
				manAssignments2Delete.add(manAssigMap.get(ma));
			}
		}

		if(manAssignments2Delete.size() > 0){
			delete manAssignments2Delete;
		}
	}

	global void finish(Database.BatchableContext BC) {
		Database.executebatch(new BI_TM_BlacklistAccount_MergeAcc_Batch());
	}

}