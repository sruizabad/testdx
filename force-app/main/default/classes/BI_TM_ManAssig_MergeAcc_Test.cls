@isTest
private class BI_TM_ManAssig_MergeAcc_Test
{
	@isTest
	static void method01()
	{
		Date startDate = Date.newInstance(2016, 1, 1);
		Date endDate = Date.newInstance(2099, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Account acc = new Account(Name = 'Test Account', Country_Code_BI__c = 'US');
		Account acc1 = new Account(Name = 'Test Account Merged', Country_Code_BI__c = 'US');

		System.runAs(thisUser){
			Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
			insert ks;
		}
		insert acc;
		insert acc1;

		BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
		insert posType;

		BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
		insert pos;

		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
			posList.add(p);
		}
		insert posList;

		BI_TM_Account_To_Territory__c ma = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc.Id, 'US', 'PM', startDate, endDate, false, false, 'Both', false, false);
		insert ma;
		BI_TM_Account_To_Territory__c ma1 = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc1.Id, 'US', 'PM', startDate, endDate, false, false, 'Both', false, false);
		insert ma1;
		BI_TM_Account_To_Territory__c ma2 = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc1.Id, 'US', 'PM', startDate-100, startDate-1, false, false, 'Both', false, false);
		insert ma2;

		try{
			merge acc acc1;
		}
		catch(DmlException ex){
			system.debug('An exception has ocurred :: ' + ex.getMessage());
		}

		Test.startTest();
		BI_TM_Account_To_Territory__c [] maList = [SELECT Id, BI_TM_Merged_Account_Check__c FROM BI_TM_Account_To_Territory__c WHERE Id = :ma1.Id];
		System.assertEquals(true, maList[0].BI_TM_Merged_Account_Check__c);
		database.executeBatch(new BI_TM_ManAssig_MergeAcc_Batch()); // this batch is linked to the BI_TM_Manage_GAS_Batch
		Test.stopTest();
		BI_TM_Account_To_Territory__c [] maMergedList = [SELECT Id, BI_TM_Merged_Account_Check__c FROM BI_TM_Account_To_Territory__c WHERE Id = :ma1.Id];
		System.assertEquals(0, maMergedList.size());
	}
}