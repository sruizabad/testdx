public class BI_TM_Manage_FF_Terr_To_Position{

  // When inserting a new position, creates a new field force to position and a new territory to position records
  public void createRelations(Map<Id, SObject> posMap){
    List<BI_TM_Field_Force_Position__c> ffPosList = new List<BI_TM_Field_Force_Position__c>();
    List<BI_TM_Territory_to_Position__c> terrPosList = new List<BI_TM_Territory_to_Position__c>();

    for(Id pos : posMap.keySet()){

      if (((BI_TM_Territory__c)posMap.get(pos)).BI_TM_FF_type__c != null) {
        // Creates the FF to position
        BI_TM_Field_Force_Position__c ffp = new BI_TM_Field_Force_Position__c();
        ffp.BI_TM_Field_Force__c = ((BI_TM_Territory__c)posMap.get(pos)).BI_TM_FF_type__c;
        ffp.BI_TM_Position__c = ((BI_TM_Territory__c)posMap.get(pos)).Id;
        ffp.BI_TM_Start_date__c = System.now(); // The start date of the record is when is asscociated to the FF
        ffPosList.add(ffp);
      }

      if (((BI_TM_Territory__c)posMap.get(pos)).BI_TM_Territory_ND__c != null) {
        // Creates the territory to position
        BI_TM_Territory_to_Position__c terrpos = new BI_TM_Territory_to_Position__c();
        terrpos.BI_TM_Territory__c = ((BI_TM_Territory__c)posMap.get(pos)).BI_TM_Territory_ND__c;
        terrpos.BI_TM_Position__c = ((BI_TM_Territory__c)posMap.get(pos)).Id;
        terrpos.BI_TM_Start_date__c = System.now(); // The start date of the record is when is asscociated to the territory
        terrPosList.add(terrpos);
      }

    }

    if(ffPosList.size() > 0){

      Database.insert(ffPosList, false);
      /* 31-08-2016 REMOVING as no message is shown in Position detail page
      try{
        insert ffPosList;
      }
      catch(Exception e){
        for(BI_TM_Field_Force_Position__c ffp : ffPosList){
          ffp.addError('There was and error creating the Field Force to Position record: ' + e.getMessage());
        }
      }*/
    }

    if(terrPosList.size() > 0){

      Database.insert(terrPosList, false);
      /* 31-08-2016 REMOVING as no message is shown in Position detail page
      try{
        insert terrPosList;
      }
      catch(Exception e){
        for(BI_TM_Territory_to_Position__c terrpos : terrPosList){
          terrpos.addError('There was and error creating the Territory to Position record: ' + e.getMessage());
        }
      }*/
    }

  }

  public void updateRelations(Map<Id, SObject> posMapOld, Map<Id, SObject> posMapNew){
    List<BI_TM_Field_Force_Position__c> ffPosList = new List<BI_TM_Field_Force_Position__c>();
    List<BI_TM_Territory_to_Position__c> terrPosList = new List<BI_TM_Territory_to_Position__c>();

    List<Id> posIdList = new List<Id>();
    List<Id> pos2IdList = new List<Id>();

    for(Id posOld : posMapOld.keySet()){
      BI_TM_Territory__c pNew = (BI_TM_Territory__c) posMapNew.get(posOld);

      // check if the FF has changed
      if(pNew.BI_TM_FF_type__c != ((BI_TM_Territory__c)posMapOld.get(posOld)).BI_TM_FF_type__c){

        if (pNew.BI_TM_FF_type__c != null) {
          // Creates a new FF to position
          BI_TM_Field_Force_Position__c ffp = new BI_TM_Field_Force_Position__c();
          ffp.BI_TM_Field_Force__c = pNew.BI_TM_FF_type__c;
          ffp.BI_TM_Position__c = pNew.Id;
          ffp.BI_TM_Start_date__c = System.now();
          ffPosList.add(ffp);
        }

        posIdList.add(posOld);
      }

      // Check if the parent territory has changed
      if(pNew.BI_TM_Territory_ND__c != ((BI_TM_Territory__c)posMapOld.get(posOld)).BI_TM_Territory_ND__c){

        if (pNew.BI_TM_Territory_ND__c != null) {
          // Creates a new territory to position
          BI_TM_Territory_to_Position__c terrpos = new BI_TM_Territory_to_Position__c();
          terrpos.BI_TM_Territory__c = pNew.BI_TM_Territory_ND__c;
          terrpos.BI_TM_Position__c = pNew.Id;
          terrpos.BI_TM_Start_date__c = System.now();
          terrPosList.add(terrpos);
        }

        pos2IdList.add(posOld);
      }
    }

    // Update the end date of the previous related records
    for(BI_TM_Territory_to_Position__c terrPos :
      [Select Id, BI_TM_Position__c, BI_TM_Active__c, BI_TM_End_date__c
        From BI_TM_Territory_to_Position__c
          Where BI_TM_Position__c in :pos2IdList and BI_TM_Active__c = true]
    ){
      terrPos.BI_TM_End_date__c = System.now();
      terrPosList.add(terrPos);
    }

    for(BI_TM_Field_Force_Position__c ffPos :
      [Select Id, BI_TM_End_date__c
        From BI_TM_Field_Force_Position__c
          Where BI_TM_Position__c in :posIdList and BI_TM_Active__c = true]
    ){
      ffPos.BI_TM_End_Date__c = System.now();
      ffPosList.add(ffPos);
    }

    if(ffPosList.size() > 0){

      Database.upsert(ffPosList, false);
      /* 31-08-2016 REMOVING as no message is shown in Position detail page
      try{
        Database.upsert ffPosList;
      }
      catch(Exception e){
        for(BI_TM_Field_Force_Position__c ffp : ffPosList){
          ffp.addError('There was and error updating the Field Force to Position record: ' + e.getMessage());
        }
      } */

    }

    if(terrPosList.size() > 0){

      Database.upsert(terrPosList, false);
      /* 31-08-2016 REMOVING as no message is shown in Position detail page
      try{
        upsert terrPosList;
      }
      catch(Exception e){
        for(BI_TM_Territory_to_Position__c terrpos : terrPosList){
          terrpos.addError('There was and error creating the Territory to Position record: ' + e.getMessage());
        }
      }*/
    }

  }
}