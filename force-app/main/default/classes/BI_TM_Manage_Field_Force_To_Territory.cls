public class BI_TM_Manage_Field_Force_To_Territory{

  // When inserting a new position, creates a new field force to position
  public void createFieldForceToTerritory (Map<Id, SObject> terrMap){
    List<BI_TM_Field_Force_to_Territory__c> ffTerrList = new List<BI_TM_Field_Force_to_Territory__c>();

    for(Id terr : terrMap.keySet()){

      if (((BI_TM_Territory_ND__c)terrMap.get(terr)).BI_TM_Field_Force__c != null) {
        BI_TM_Field_Force_to_Territory__c fft = new BI_TM_Field_Force_to_Territory__c();
        fft.BI_TM_Field_Force__c = ((BI_TM_Territory_ND__c)terrMap.get(terr)).BI_TM_Field_Force__c;
        fft.BI_TM_Territory__c = ((BI_TM_Territory_ND__c)terrMap.get(terr)).Id;
        fft.BI_TM_Start_date__c = System.now(); // The start date of the record is when is asscociated to the FF
        ffTerrList.add(fft);
      }

    }

    if(ffTerrList.size() > 0){
      Database.insert(ffTerrList, false);
      /* 31-08-2016 REMOVING as no message is shown in Territory detail page
      try{
        insert ffTerrList;
      }
      catch(Exception e){
        for(BI_TM_Field_Force_to_Territory__c fft : ffTerrList){
          fft.addError('There was and error creating the Field Force to Territory record: ' + e.getMessage());
        }
      }*/
    }

  }

  public void updateFieldForceToTerritory (Map<Id, SObject> terrMapNew, Map<Id, SObject> terrMapOld){
    List<BI_TM_Field_Force_to_Territory__c> ffTerrList = new List<BI_TM_Field_Force_to_Territory__c>();
    List<Id> terrIdList = new List<Id>();

    // check if the FF has changed
    for(Id terrNew : terrMapNew.keySet()){
      BI_TM_Territory_ND__c tNew = (BI_TM_Territory_ND__c) terrMapNew.get(terrNew);
      BI_TM_Territory_ND__c terrOld = (BI_TM_Territory_ND__c)terrMapOld.get(terrNew);
      if(tNew.BI_TM_Field_Force__c != ((BI_TM_Territory_ND__c)terrMapOld.get(terrNew)).BI_TM_Field_Force__c){

        if (tNew.BI_TM_Field_Force__c != null) {
          // Creates a new FF to territory
          BI_TM_Field_Force_to_Territory__c fft = new BI_TM_Field_Force_to_Territory__c();
          fft.BI_TM_Field_Force__c = tNew.BI_TM_Field_Force__c;
          fft.BI_TM_Territory__c = tNew.Id;
          fft.BI_TM_Start_date__c = System.now();
          ffTerrList.add(fft);
        }

        terrIdList.add(terrOld.Id);
      }
    }

    // Update the end date of the previous FF to territory record
    for(BI_TM_Field_Force_to_Territory__c fft :
      [Select Id, BI_TM_Territory__c, BI_TM_Active__c, BI_TM_End_date__c
        From BI_TM_Field_Force_to_Territory__c Where BI_TM_Territory__c in :terrIdList and BI_TM_Active__c = true]
    ){
      fft.BI_TM_End_date__c = System.now();
      ffTerrList.add(fft);
    }

    if(ffTerrList.size() > 0){

      Database.upsert(ffTerrList, false);

      /* 31-08-2016 REMOVING as no message is shown in Territory detail page
      try{
        upsert ffTerrList;
      }
      catch(Exception e){
        for(BI_TM_Field_Force_to_Territory__c fft : ffTerrList){
          fft.addError('There was and error updating the Field Force to Territory record: ' + e.getMessage());
        }
      }*/
    }
  }
}