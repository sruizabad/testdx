/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Batch to generate the manual assignments from GAS
*  @author:         Antonio Ferrero
*  @created:        20-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         20-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_Manage_GAS_Batch implements Database.Batchable<sObject>, Schedulable, database.stateful {

	String query;
	DateTime timeStamp;
	Integer ccListSize;
	List<BI_TM_CountryCodes__c> ccList;

	global BI_TM_Manage_GAS_Batch(DateTime runDate) {
		this.timeStamp = runDate;
		this.query = 'SELECT Id FROM GAS_History_BI__c LIMIT 0';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		ccList =  [SELECT Id, BI_TM_GAS_Date__c, CountryCode__c, BI_TM_GAS_Include_Child_Accounts__c, BI_TM_GAS_Include_Parent_Accounts__c FROM BI_TM_CountryCodes__c WHERE BI_TM_GAS_Date__c != null AND BI_TM_Run_GAS__c != :timeStamp LIMIT 1];
		ccListSize = ccList.size();
		if(ccListSize > 0){
			ccList[0].BI_TM_Run_GAS__c = timeStamp;
			update ccList;
			String dateGAS = BI_TM_Utils.formatDate(ccList[0].BI_TM_GAS_Date__c);
			query = 'SELECT Id, Account_BI__c, GAS_Alignment_Date_BI__c, User_Territory_Name_BI__c FROM GAS_History_BI__c WHERE Account_BI__r.Country_Code_BI__c = \'' + ccList[0].CountryCode__c + '\' AND GAS_Alignment_Date_BI__c >= ' + dateGAS;
			system.debug('Query manage GAS :: ' + query);
		}
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<GAS_History_BI__c> scope) {
		Set<String> terrNames = getTerritoryNames(scope);
		Map<String, BI_TM_Territory__c> positionMap = getPositionMap(terrNames);
		List<BI_TM_Account_To_Territory__c> manualAssignments2Upsert = getManualAssignments(scope, positionMap);
		Database.UpsertResult[] upsertSrList = Database.upsert(manualAssignments2Upsert, BI_TM_Account_To_Territory__c.Fields.BI_TM_External_Id__c, false);
		BI_TM_Utils.manageErrorLogSave(upsertSrList);
	}

	global void finish(Database.BatchableContext BC) {
		if(ccListSize > 0){
			Database.executebatch(new BI_TM_Manage_GAS_Batch(timeStamp), 2000);
		}
		else{
			Database.executebatch(new BI_TM_Delete_ManAssignments_Batch(timeStamp));
		}
	}

	// Schedulable method
	global void execute(SchedulableContext sc) {
		Database.executebatch(new BI_TM_Manage_GAS_Batch(system.now()));
	}

	private static Set<String> getTerritoryNames(List<GAS_History_BI__c> gasList){
		Set<String> terrNames = new Set<String>();
		for(GAS_History_BI__c gas : gasList){
			terrNames.add(gas.User_Territory_Name_BI__c);
		}
		return terrNames;
	}

	// Get the position map with the name as key and the Id as value
	private static Map<String, BI_TM_Territory__c> getPositionMap(Set<String> terrNames){
		Map<String, BI_TM_Territory__c> positionMap = new Map<String, BI_TM_Territory__c>();
		for(BI_TM_Territory__c pos : [SELECT Id, Name, BI_TM_Business__c, BI_TM_Country_Code__c FROM BI_TM_Territory__c WHERE Name IN :terrNames]){
			positionMap.put(pos.Name, pos);
		}
		return positionMap;
	}

	// Build the manual assignments
	private List<BI_TM_Account_To_Territory__c> getManualAssignments(List<GAS_History_BI__c> scope, Map<String, BI_TM_Territory__c> positionMap){
		Map<String, BI_TM_Account_To_Territory__c> manualAssignmentMap = new Map<String, BI_TM_Account_To_Territory__c>();

		Boolean directChild = ccList[0].BI_TM_GAS_Include_Child_Accounts__c;
		Boolean directParent = ccList[0].BI_TM_GAS_Include_Parent_Accounts__c;
		for(GAS_History_BI__c gas : scope){
			if(positionMap.get(gas.User_Territory_Name_BI__c) != null){
				String key = (String)gas.Account_BI__c + (String)positionMap.get(gas.User_Territory_Name_BI__c).Id;
				BI_TM_Account_To_Territory__c ma = new BI_TM_Account_To_Territory__c(BI_TM_Account__c = gas.Account_BI__c, BI_TM_Business__c = positionMap.get(gas.User_Territory_Name_BI__c).BI_TM_Business__c,
																						BI_TM_Country_Code__c = positionMap.get(gas.User_Territory_Name_BI__c).BI_TM_Country_Code__c, BI_TM_GAS__c = true, BI_TM_Territory_FF_Hierarchy_Position__c =
																						positionMap.get(gas.User_Territory_Name_BI__c).Id, BI_TM_Start_Date__c = gas.GAS_Alignment_Date_BI__c, BI_TM_End_Date__C = null,
																						BI_TM_TimeStamp__c = timeStamp, BI_TM_Direct_Child_Assignment__c = directChild, BI_TM_Direct_Parent_Assignment__c = directParent);
				ma.BI_TM_External_Id__c = ma.BI_TM_Country_Code__c + '_' + ma.BI_TM_Account__c + '_' + (String)positionMap.get(gas.User_Territory_Name_BI__c).Name + '_GAS';
				manualAssignmentMap.put(ma.BI_TM_Account__c + '_' + (String)positionMap.get(gas.User_Territory_Name_BI__c).Name, ma);
			}
		}

		Set<String> manAssignments2NotUpdate = getKeys2Override(getExternaldIDS(manualAssignmentMap));
		for(String key : manAssignments2NotUpdate){
			manualAssignmentMap.remove(key);
		}
		return manualAssignmentMap.values();
	}

	// Get the manual assignments set to override the GAS assignment
	private static Set<String> getKeys2Override(Set<String> extIdSet){
		Set<String> keys2Remove = new Set<String>();
		for(BI_TM_Account_To_Territory__c ma : [SELECT BI_TM_External_Id__c, BI_TM_Account__c, BI_TM_Territory_FF_Hierarchy_Position__r.Name FROM BI_TM_Account_To_Territory__c WHERE BI_TM_External_Id__c IN :extIdSet AND BI_TM_Override_GAS__c = true]){
			String key = ma.BI_TM_Account__c + '_' + ma.BI_TM_Territory_FF_Hierarchy_Position__r.Name;
			keys2Remove.add(key);
		}
		return keys2Remove;
	}

	// Get the manual assignments external ids
	private static Set<String> getExternaldIDS(Map<String, BI_TM_Account_To_Territory__c> manualAssignmentMap){
		Set<String> externalIdSet = new Set<String>();
		for(String key : manualAssignmentMap.keySet()){
			externalIdSet.add(manualAssignmentMap.get(key).BI_TM_External_Id__c);
		}
		return externalIdSet;
	}
}