@isTest
private class BI_TM_Manage_GAS_Test
{
	@isTest
	static void method01()
	{
		Date startDate = Date.newInstance(2016, 1, 1);
		Date endDate = Date.newInstance(2099, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Account acc = new Account(Name = 'Test Account', Country_Code_BI__c = 'US');

		System.runAs(thisUser){
			Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
			insert ks;
		}
		insert acc;

		BI_TM_CountryCodes__c cc = new BI_TM_CountryCodes__c(Name = 'Test country code', CountryCode__c = 'US', BI_TM_GAS_Date__c = startDate);
		insert cc;

		BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
		insert posType;

		BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
		insert pos;

		BI_TM_FF_type__c ff = BI_TM_UtilClassDataFactory_Test.createFieldForce('FF Test', 'US', 'PM');
		insert ff;

		List<BI_TM_Territory_ND__c> terrList = new List<BI_TM_Territory_ND__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory_ND__c terr = BI_TM_UtilClassDataFactory_Test.createTerritory('US-PM-TERR-' + String.ValueOf(i+1), 'US', 'PM', null, ff.Id, startDate, endDate, true);
			terrList.add(terr);
		}
		insert terrList;

		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, terrList[i].Id, 'SALES', posType.Id, 'Representative');
			posList.add(p);
		}
		insert posList;

		BI_TM_Account_To_Territory__c ma = BI_TM_UtilClassDataFactory_Test.createManualAssignment(posList[1].Id, acc.Id, 'US', 'PM', startDate, endDate, false, false, 'Both', false, false);
		ma.BI_TM_GAS__c = true;
		ma.BI_TM_Override_GAS__c = true;
		insert ma;

		GAS_History_BI__c gas = new GAS_History_BI__c(Account_BI__c = acc.Id, User_Territory_Name_BI__c = posList[0].Name, GAS_Alignment_Date_BI__c = System.Today());
		insert gas;

		GAS_History_BI__c gas1 = new GAS_History_BI__c(Account_BI__c = acc.Id, User_Territory_Name_BI__c = posList[1].Name, GAS_Alignment_Date_BI__c = System.Today());
		insert gas1;

		Test.startTest();
		database.executeBatch(new BI_TM_Manage_PreviousInteractions_Batch(System.now())); // this batch is linked to the BI_TM_Manage_GAS_Batch
		System.schedule('Test schedule', '0 0 23 * * ?', new BI_TM_Manage_GAS_Batch(System.now()));
		Test.stopTest();
	}
}