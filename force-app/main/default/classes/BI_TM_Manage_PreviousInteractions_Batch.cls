/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Batch to generate the manual assignments from the previous interactions
*  @author:         Antonio Ferrero
*  @created:        23-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         23-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_Manage_PreviousInteractions_Batch implements Database.Batchable<sObject>, Schedulable, database.stateful {

	String query;
	DateTime timeStamp;
	Integer alignmentListSize;
	String fieldForce;
	Map<String, BI_TM_Territory__c> posMap;

	global BI_TM_Manage_PreviousInteractions_Batch(DateTime timeStamp) {
		this.timeStamp = timeStamp;
		this.query = 'SELECT Id FROM Call2_vod__c limit 0';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		List<BI_TM_Alignment__c> alignmentList =  [SELECT Id, BI_TM_FF_type__c, BI_TM_Previous_Months__c, BI_TM_Run_Previous_Interactions__c, BI_TM_Country_Code__c FROM BI_TM_Alignment__c WHERE BI_TM_Include_Previous_Interactions__c = true
																								AND BI_TM_Previous_Months__c != null AND BI_TM_Status__c = 'Active' AND BI_TM_Run_Previous_Interactions__c != :timeStamp];
		this.alignmentListSize = alignmentList.size();
		if(this.alignmentListSize > 0){
			alignmentList[0].BI_TM_Run_Previous_Interactions__c = timeStamp;
			update alignmentList[0];
			this.fieldForce = alignmentList[0].BI_TM_FF_type__c;
			Integer numMonths = (Integer)alignmentList[0].BI_TM_Previous_Months__c;
			posMap = getPosMap(fieldForce);
			List<String> posNames = new List<String>(posMap.keySet());
			query = 'SELECT Id, Territory_vod__c, Account_vod__c, Call_Date_vod__c FROM Call2_vod__c WHERE Account_vod__r.Country_Code_BI__c = \'' + alignmentList[0].BI_TM_Country_Code__c + '\'';
			query += ' AND status_vod__c = \'Submitted_vod\' AND Territory_vod__c IN (\'' + String.join(posNames, '\',\'') + '\') AND Call_Date_vod__c >= Last_n_months:' + numMonths;
			system.debug('Query manage Previsous interactions :: ' + query);
		}
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<Call2_vod__c> scope) {
		List<BI_TM_Account_To_Territory__c> manualAssignments2Upsert = getManualAssignments(scope, posMap);
		Database.UpsertResult[] upsertSrList = Database.upsert(manualAssignments2Upsert, BI_TM_Account_To_Territory__c.Fields.BI_TM_External_Id__c, false);
		BI_TM_Utils.manageErrorLogSave(upsertSrList);
	}

	// Check if the next alignment needs to be managed
	global void finish(Database.BatchableContext BC) {
		if(this.alignmentListSize > 1){
			Database.executebatch(new BI_TM_Manage_PreviousInteractions_Batch(timeStamp));
		}
		else {
			Database.executebatch(new BI_TM_Manage_GAS_Batch(timeStamp));
		}
	}

	// Schedulable method
	global void execute(SchedulableContext sc) {
		Database.executebatch(new BI_TM_Manage_PreviousInteractions_Batch(system.now()));
	}

	// Get the map with the position names and the ids
	private Map<String, BI_TM_Territory__c> getPosMap(String fieldForce){
		Map<String, BI_TM_Territory__c> posMap = new Map<String, BI_TM_Territory__c>();
		for(BI_TM_Territory__c pos : [SELECT Id, Name, BI_TM_Country_Code__c, BI_TM_Business__c FROM BI_TM_Territory__c WHERE BI_TM_Is_Active__c = true AND BI_TM_Territory_ND__r.BI_TM_Field_Force__c = :fieldForce]){
			posMap.put(pos.Name, pos);
		}
		return posMap;
	}

	// Build the list with the manual assignments to upsert
	private List<BI_TM_Account_To_Territory__c> getManualAssignments(List<Call2_vod__c> scope, Map<String, BI_TM_Territory__c> posMap){
		Map<String, BI_TM_Account_To_Territory__c> manualAssignmentMap = new Map<String, BI_TM_Account_To_Territory__c>();
		for(Call2_vod__c inter : scope){
			if(posMap.get(inter.Territory_vod__c) != null){
				String key = (String)inter.Account_vod__c + (String)posMap.get(inter.Territory_vod__c).Id;
				BI_TM_Account_To_Territory__c ma = new BI_TM_Account_To_Territory__c(BI_TM_Account__c = inter.Account_vod__c, BI_TM_Business__c = posMap.get(inter.Territory_vod__c).BI_TM_Business__c,
																						BI_TM_Country_Code__c = posMap.get(inter.Territory_vod__c).BI_TM_Country_Code__c, BI_TM_Previous_Interactions__c = true, BI_TM_Territory_FF_Hierarchy_Position__c =
																						posMap.get(inter.Territory_vod__c).Id, BI_TM_Start_Date__c = inter.Call_Date_vod__c, BI_TM_End_Date__C = null,
																						BI_TM_TimeStamp__c = timeStamp);
				ma.BI_TM_External_Id__c = ma.BI_TM_Country_Code__c + '_' + ma.BI_TM_Account__c + '_' + inter.Territory_vod__c + '_PREVINTER';
				manualAssignmentMap.put(ma.BI_TM_Account__c + '_' + inter.Territory_vod__c, ma);
			}
		}
		return manualAssignmentMap.values();
	}
}