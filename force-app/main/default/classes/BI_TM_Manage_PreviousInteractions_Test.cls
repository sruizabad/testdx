@isTest
private class BI_TM_Manage_PreviousInteractions_Test
{
	@isTest
	static void method01()
	{
		Date startDate = Date.newInstance(2016, 1, 1);
		Date endDate = Date.newInstance(2099, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Account acc = new Account(Name = 'Test Account', Country_Code_BI__c = 'US');

		Knipper_Settings__c ks = new Knipper_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_Detect_Changes_Record_Type_List__c = 'Professional_vod');
		insert ks;
		insert acc;

		BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
		insert posType;

		BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
		insert pos;

		BI_TM_FF_type__c ff = BI_TM_UtilClassDataFactory_Test.createFieldForce('FF Test', 'US', 'PM');
		insert ff;

		BI_TM_FF_type__c ff1 = BI_TM_UtilClassDataFactory_Test.createFieldForce('FF Test1', 'US', 'PM');
		insert ff1;

		List<BI_TM_Territory_ND__c> terrList = new List<BI_TM_Territory_ND__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory_ND__c terr = BI_TM_UtilClassDataFactory_Test.createTerritory('US-PM-TERR-' + String.ValueOf(i+1), 'US', 'PM', null, ff.Id, startDate, endDate, true);
			terrList.add(terr);
		}
		insert terrList;

		BI_TM_Alignment__c ali = BI_TM_UtilClassDataFactory_Test.createAlignment('Alignment test', 'US', 'PM', ff.Id, startDate, null, 'Active', true, 10);
		insert ali;

		BI_TM_Alignment__c ali1 = BI_TM_UtilClassDataFactory_Test.createAlignment('Alignment test 1', 'US', 'PM', ff1.Id, startDate, null, 'Active', true, 10);
		insert ali1;

		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, terrList[i].Id, 'SALES', posType.Id, 'Representative');
			posList.add(p);
		}
		insert posList;

		Call2_vod__c inter1 = BI_TM_UtilClassDataFactory_Test.createInteraction(acc.Id, poslist[0].Name, 'Submitted_vod');
		//inter1.recordTypeId = '012U0000000K93oIAC';
		inter1.Call_Date_vod__c = System.today();
		insert inter1;

		Test.startTest();
		database.executeBatch(new BI_TM_Manage_PreviousInteractions_Batch(System.now()));
		System.schedule('Test schedule', '0 0 23 * * ?', new BI_TM_Manage_PreviousInteractions_Batch(System.now()));
		Test.stopTest();

	}
}