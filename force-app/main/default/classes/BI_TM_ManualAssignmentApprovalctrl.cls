public class BI_TM_ManualAssignmentApprovalctrl {
  
    public BI_TM_Account_To_Territory__c clonemanAssi       {   get; set;   }
    public BI_TM_Account_To_Territory__c objmanAssiOldValues       {   get; set;   }
    public ID newRecordId                                   {   get; set;   }
    public Id recId                                         {   get; set;   }
    
    
    public BI_TM_ManualAssignmentApprovalctrl(ApexPages.StandardController controller) {
        
        // Old Manual Assignment record Id
        recId = apexpages.currentpage().getparameters().get('id');
        objmanAssiOldValues = [ SELECT Name, BI_TM_Country_Code__c, BI_TM_End_date__c, BI_TM_Account__c, BI_TM_Territory_FF_Hierarchy_Position__c,BI_TM_Territory_FF_Hierarchy_Position__r.BI_TM_Parent_Position__c, BI_TM_Start_date__c, 
                                      BI_TM_Alignment_Cycle__c,BI_TM_Apply_Territory_Rules__c,BI_TM_Direct_Child_Assignment__c,BI_TM_Direct_Parent_Assignment__c,
                                      BI_TM_HCP_HCO__c,BI_TM_Indirect_Account_Assigned__c,BI_TM_Indirect_Account_Assignment__c,
                                      BI_TM_Business__c 
                                 FROM BI_TM_Account_To_Territory__c WHERE Id =: recId];
                                 
        clonemanAssi= new BI_TM_Account_To_Territory__c();
        clonemanAssi.BI_TM_old_manualAssignment__c = recId;
        clonemanAssi.BI_TM_Account__c= objmanAssiOldValues.BI_TM_Account__c;
        clonemanAssi.BI_TM_Country_Code__c = objmanAssiOldValues.BI_TM_Country_Code__c;
        clonemanAssi.BI_TM_Approval_Status__c = 'Pending for Approval';
        clonemanAssi.BI_TM_Business__c = objmanAssiOldValues.BI_TM_Business__c;
        
      }    
    
    // Method to submit for approval on click of save button 
    public PageReference cloneWithItems() {
     
        Set<Id> setUserMgmtIds = new Set<Id>();
        Set<Id> setparentUserMgmtIds = new Set<Id>();
      
        System.debug('=========objmanAssiOldValues===='+objmanAssiOldValues);
        System.debug('=========cloneuserpos.BI_TM_Territory1__c===='+objmanAssiOldValues.BI_TM_Territory_FF_Hierarchy_Position__r.BI_TM_Parent_Position__c);
         
        Set<Id> setParentPostionIds = new Set<Id>();
        Set<Id> setParentParentPostionIds = new Set<Id>();
         
        // Old Parent Position
        setParentPostionIds.add(objmanAssiOldValues.BI_TM_Territory_FF_Hierarchy_Position__r.BI_TM_Parent_Position__c);
        system.debug('++++++'+setParentPostionIds);
         
        Savepoint sp = Database.setSavepoint();
        
        try {
                insert clonemanAssi;
                newRecordId = clonemanAssi.Id;
        } catch(Exception e) {
          
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
        } 
     
        System.debug('=========setParentPostionIds===='+setParentPostionIds);
        System.debug('=========cloneuserpos===='+clonemanAssi);
         
        clonemanAssi = new BI_TM_Account_To_Territory__c();
         
        for(BI_TM_Account_To_Territory__c obj : [SELECT Id, BI_TM_Territory_FF_Hierarchy_Position__c,BI_TM_Territory_FF_Hierarchy_Position__r.BI_TM_Parent_Position__c FROM BI_TM_Account_To_Territory__c WHERE Id =: newRecordId])
            clonemanAssi =  obj;   
     
        System.debug('=========cloneuserpos.BI_TM_Territory_FF_Hierarchy_Position====' + clonemanAssi.BI_TM_Territory_FF_Hierarchy_Position__c);
      
        for(BI_TM_Account_To_Territory__c obj : [ SELECT BI_TM_Territory_FF_Hierarchy_Position__c,BI_TM_Territory_FF_Hierarchy_Position__r.BI_TM_Parent_Position__c FROM BI_TM_Account_To_Territory__c WHERE Id =: clonemanAssi.Id]){
            
            // Adding newly created user to position's parent positions
            if(obj.BI_TM_Territory_FF_Hierarchy_Position__c != null)
                setParentPostionIds.add(obj.BI_TM_Territory_FF_Hierarchy_Position__r.BI_TM_Parent_Position__c);
        }  
        system.debug('++++++'+setParentPostionIds);
        //code for getting parent position of parent
       /*   for(BI_TM_Territory__c obj : [ SELECT BI_TM_Parent_Position__c FROM BI_TM_Territory__c WHERE Id =: setParentPostionIds]){
            
            // Adding newly created user to position's parent positions
            if(obj.BI_TM_Parent_Position__c != null)
                setParentParentPostionIds.add(obj.BI_TM_Parent_Position__c);
        }  
        for(BI_TM_User_territory__c objUsrToPos : [ SELECT Id, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c  FROM BI_TM_User_territory__c WHERE BI_TM_Territory1__c IN : setParentParentPostionIds]) {
                
                if(objUsrToPos.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c!= null)
                    setparentUserMgmtIds.add(objUsrToPos.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);
        } */
         
        System.debug('=========setParentPostionIds===='+setParentPostionIds);
         
        // Get all the users for that Positions
        for(BI_TM_User_territory__c objUsrToPos : [ SELECT Id, BI_TM_User_mgmt_tm__c FROM BI_TM_User_territory__c WHERE BI_TM_Territory1__c IN : setParentPostionIds]) {
                
                if(objUsrToPos.BI_TM_User_mgmt_tm__c != null)
                    setUserMgmtIds.add(objUsrToPos.BI_TM_User_mgmt_tm__c);
        }
        System.debug('======setUserIds====='+setUserMgmtIds);
        
        Set<Id> setUserIds = new Set<Id>();
     
        for(BI_TM_User_mgmt__c objUserMagt : [SELECT Id, BI_TM_UserId_Lookup__c FROM BI_TM_User_mgmt__c WHERE Id IN : setUserMgmtIds])
            setUserIds.add(objUserMagt.BI_TM_UserId_Lookup__c);        
        
        System.debug('======setUserIds====='+setUserIds);
    
        List<Id> lstApproverIds = new List<Id>();
        lstApproverIds.addAll(setUserIds);
        
        // Iterate the over the newly created record and submit it for approver
        for(BI_TM_Account_To_Territory__c  objaccToPos : [SELECT Id FROM BI_TM_Account_To_Territory__c WHERE Id =: clonemanAssi.Id]) {
            
            List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
            
            for(Id approverId : lstApproverIds) {
                
                System.debug('=======inside for approver==='+approverId);
                
                Approval.ProcessSubmitRequest objProcessSubmitRequest = new Approval.ProcessSubmitRequest();
                objProcessSubmitRequest.setComments('Submitted for approval');
                objProcessSubmitRequest.setObjectId(objaccToPos.Id);
                objProcessSubmitRequest.setNextApproverIds(new List<Id>{approverId});
                requests.add(objProcessSubmitRequest); 
            }
            
            System.debug('======requests====='+requests);
            // Submit the approval request for the User_to_Position
            List<Approval.ProcessResult> results = Approval.process(requests); 
            System.debug('======results====='+results);
        }
        return new PageReference('/' + newRecordId);
    }
   
}