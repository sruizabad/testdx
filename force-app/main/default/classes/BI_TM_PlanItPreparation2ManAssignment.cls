global class BI_TM_PlanItPreparation2ManAssignment implements Database.batchable<sObject> {
	public String Query;
	global Database.QueryLocator Start(Database.BatchableContext info)
	{
		// Retrieve list of countries that have enabled the process to create manual assignments from PLANiT
		Map<Id, BI_TM_Country__mdt> countryMap = new Map<Id, BI_TM_Country__mdt>([SELECT Id, BI_TM_Country_Code__c, BI_TM_Filter_Start_Date__c, BI_TM_Source_Start_Date_Targets__c FROM BI_TM_Country__mdt WHERE BI_TM_Run_Manual_Assignment_From_PLANiT__c = true]);

		List <String> countries = new List <String> ();
		for(Id cmdt : countryMap.keySet()){
			countries.add(countryMap.get(cmdt).Id);
		}

		// Build the query to fetch target prepations
    if(countries.size() > 0){
      String strQuery = 'SELECT BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name, BI_PL_Target__r.BI_PL_Target_customer__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_Start_date__c, BI_PL_Target__r.BI_PL_Header__r.BI_PL_End_date__c FROM BI_PL_Channel_detail_preparation__c WHERE (( BI_PL_Target__r.BI_PL_No_see_list__c = false AND BI_PL_Target__r.BI_PL_Next_best_account__c = false AND BI_PL_Removed__c = false) OR (( BI_PL_Target__r.BI_PL_Next_best_account__c  = true OR BI_PL_Target__r.BI_PL_No_see_list__c = true) AND BI_PL_Target__r.BI_PL_Added_manually__c = true ))';
      // Add the first country of the list
      strQuery += ' AND (BI_PL_Target__r.BI_PL_Country_code__c = \'' + countryMap.get(countries[0]).BI_TM_Country_Code__c + '\'';
      if(countryMap.get(countries[0]).BI_TM_Filter_Start_Date__c != null || countryMap.get(countries[0]).BI_TM_Source_Start_Date_Targets__c != null){
        strQuery += ' AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Start_date__c = ';
				String startDate = formatDate(countryMap.get(countries[0]).BI_TM_Source_Start_Date_Targets__c);
        strQuery += countryMap.get(countries[0]).BI_TM_Filter_Start_Date__c != null ? countryMap.get(countries[0]).BI_TM_Filter_Start_Date__c : startDate;
      }
      strQuery += ')';

      // Add other countries to the query
      for(Integer i = 1; i< countries.size();i++){
        strQuery += ' OR (BI_PL_Target__r.BI_PL_Country_code__c = \'' + countryMap.get(countries[i]).BI_TM_Country_Code__c + '\'';
        if(countryMap.get(countries[i]).BI_TM_Filter_Start_Date__c != null || countryMap.get(countries[i]).BI_TM_Source_Start_Date_Targets__c != null){
          strQuery += ' AND BI_PL_Target__r.BI_PL_Header__r.BI_PL_Start_date__c = ';
					String startDate = formatDate(countryMap.get(countries[0]).BI_TM_Source_Start_Date_Targets__c);
	        strQuery += countryMap.get(countries[0]).BI_TM_Filter_Start_Date__c != null ? countryMap.get(countries[0]).BI_TM_Filter_Start_Date__c : startDate;
        }
        strQuery += ')';
      }
      //system.debug('Query :: ' + strQuery);
			strQuery += ' order by BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name';
			System.debug('*** strQuery: ' + strQuery);
      return Database.getQueryLocator(strQuery);
    }
    else return null;
	}
	global void Execute(Database.BatchableContext info, List<BI_PL_Channel_detail_preparation__c> scope)
	{
		Set<String> posNames = new Set<String>();
		Map<Id, BI_PL_Channel_detail_preparation__c> cdpMap = new Map<Id, BI_PL_Channel_detail_preparation__c>();
		Map<String, cdpWrapper> keyCDPMap = new Map<String, cdpWrapper>();

		// Retrieve the custom metadata information that enables countries to run this process. Use to set start and dates if need to be different than the plan
		List<BI_TM_Country__mdt> countrySettingsList = [SELECT Id, BI_TM_Country_Code__c, BI_TM_Manual_Assignments_Start_Date__c, BI_TM_Manual_Assignments_End_Date__c FROM BI_TM_Country__mdt WHERE BI_TM_Run_Manual_Assignment_From_PLANiT__c = true];
    Map<String, countryWrapper> countryWrapperMap = new Map<String, countryWrapper>();
    for(BI_TM_Country__mdt c : countrySettingsList){
      countryWrapper cw = new countryWrapper();
      cw.startDate = c.BI_TM_Manual_Assignments_Start_Date__c;
      cw.endDate = c.BI_TM_Manual_Assignments_End_Date__c;
      countryWrapperMap.put(c.BI_TM_Country_Code__c, cw);
    }

		// Use thw wrapper class to build the information needed to create the manual assignments
		for(BI_PL_Channel_detail_preparation__c cdp : scope){
			posNames.add(cdp.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name);
			cdpMap.put(cdp.Id, cdp);
			String key = cdp.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name + cdp.BI_PL_Target__r.BI_PL_Target_customer__c;
			system.debug('Key :: ' + key);
			if(!keyCDPMap.keySet().contains(key)){
				cdpWrapper cdpw = new cdpWrapper();
				cdpw.cdpID = cdp.Id;
				cdpw.account = cdp.BI_PL_Target__r.BI_PL_Target_customer__c;
				cdpw.cdpID = cdp.Id;
				cdpw.posName = cdp.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Position_cycle__r.BI_PL_Position__r.Name;
				cdpw.startDate = cdp.BI_PL_Target__r.BI_PL_Header__r.BI_PL_Start_date__c;
				cdpw.endDate = cdp.BI_PL_Target__r.BI_PL_Header__r.BI_PL_End_date__c;
				keyCDPMap.put(key, cdpw);
			}

		}

		// Retrieve information about the BITMAN positions
		Map<String, BI_TM_territory__c> posMap = new Map<String, BI_TM_territory__c>();
		for(BI_TM_territory__c pos : [Select Id, Name, BI_TM_Country_Code__c, BI_TM_Business__c FROM BI_TM_territory__c where Name IN :posNames]){
			posMap.put(pos.Name, pos);
		}
		List<BI_TM_Account_To_Territory__c> manAssignments2Upsert = new List<BI_TM_Account_To_Territory__c>();

		// Create the manual assignments with the information from the previous steps
		for(String key : keyCDPMap.keySet()){
			cdpWrapper cdpw = keyCDPMap.get(key);
			BI_TM_Account_To_Territory__c ma = new BI_TM_Account_To_Territory__c();
			ma.BI_TM_Account__c = cdpw.account;
			ma.BI_TM_Business__c = posMap.get(cdpw.posName).BI_TM_Business__c;
			ma.BI_TM_Country_Code__c = posMap.get(cdpw.posName).BI_TM_Country_Code__c;
			ma.BI_TM_Direct_Child_Assignment__c = false;
			ma.BI_TM_Direct_Parent_Assignment__c = false;
			ma.BI_TM_HCP_HCO__c = 'Both';

			// The start and end dates depend on the settings of the country
			countryWrapper cw = countryWrapperMap.get(posMap.get(cdpw.posName).BI_TM_Country_Code__c);
			ma.BI_TM_Start_Date__c = cw.startDate != null ? cw.startDate : cdpw.startDate;
			ma.BI_TM_End_Date__c = cw.endDate != null ? cw.endDate : cdpw.endDate;
			ma.BI_TM_SAP__c = true;
			ma.BI_TM_Territory_FF_Hierarchy_Position__c = posMap.get(cdpw.posName).Id;
			manAssignments2Upsert.add(ma);
		}
		//system.debug('List of manual assignments :: ' + manAssignments2Upsert);
		Database.SaveResult[] srList = Database.insert(manAssignments2Upsert, false);

		// Iterate through each returned result
		for (Database.SaveResult sr : srList) {
			if (sr.isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				System.debug('Successfully inserted manual assignment: ' + sr.getId());
			}
			else {
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Manual assignments fields that affected this error: ' + err.getFields());
				}
			}
		}

	}
	global void Finish(Database.BatchableContext info){

	}

	private Class cdpWrapper{
		public Id cdpID {get; set;}
		public Id account {get; set;}
		public String posName {get; set;}
		public Date startDate {get; set;}
		public Date endDate {get; set;}
	}

	private String formatDate(Date d){
		Datetime dt = Datetime.newInstance(d.year(), d.month(), d.day());
		return (String.valueOfGMT(dt)).split(' ')[0];
	}

	private class countryWrapper{
		Date startDate;
		Date endDate;
	}
}