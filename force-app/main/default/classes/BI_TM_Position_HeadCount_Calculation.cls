public class BI_TM_Position_HeadCount_Calculation{

  public void insertposition(Map<Id,Sobject> newTrigger){
  Map<Id,BI_TM_Territory__c > newpositions = (Map<Id,BI_TM_Territory__c >) newTrigger;
  try{
   Set<Id> parentIdSet1 = new Set<Id>();
    for(BI_TM_Territory__c record : newpositions.values())
    {
        parentIdSet1.add(record.BI_TM_Parent_Position__c);
    }
    Map<Id,BI_TM_Territory__c> allParentRecords1 = new Map<Id,BI_TM_Territory__c>([Select BI_TM_Head_Count__c from BI_TM_Territory__c where id in :parentIdSet1]);
    system.debug('1st set'+allParentRecords1);
    for(BI_TM_Territory__c record : newpositions.values())
   {
       if(allParentRecords1.size() > 0 && allParentRecords1.containsKey(record.BI_TM_Parent_Position__c) && allParentRecords1.get(record.BI_TM_Parent_Position__c).BI_TM_Head_Count__c!=null)
       {
               allParentRecords1.get(record.BI_TM_Parent_Position__c).BI_TM_Head_Count__c = allParentRecords1.get(record.BI_TM_Parent_Position__c).BI_TM_Head_Count__c + 1;
       }
  }
      update allParentRecords1.values();
              }catch(Exception ex){
                System.debug('Error in Catch block:'+ex);
      }
 }

  public void BeforeUpdateposition(Map<Id, SObject> newTrigger, Map<Id, SObject> posMap){
  Map<Id,BI_TM_Territory__c > newpositions = (Map<Id,BI_TM_Territory__c >) newTrigger;
    try{
        Map<Id, BI_TM_Territory__c> enhancedPositions =
            new Map<Id,BI_TM_Territory__c>([select Id, (SELECT Id FROM Positions1__r) from BI_TM_Territory__c where Id in :newpositions.keySet()]);
        for (BI_TM_Territory__c position : newpositions.values()) {
            position.BI_TM_Head_Count__c= enhancedPositions.get(position.Id).Positions1__r.size();
            if(enhancedPositions.get(position.Id).Positions1__r.size()==0)
            position.BI_TM_Head_Count__c=1;       
                  }
       }
       catch(exception e){
           System.debug('Error :'+e);
                         }
    }
    public void AfterUpdateposition(Map<Id, SObject> newTrigger, Map<Id, SObject> posMap){
    Map<Id,BI_TM_Territory__c > newpositions = (Map<Id,BI_TM_Territory__c >) newTrigger;
   // List<BI_TM_Territory__c > oldpositions = (List<BI_TM_Territory__c >) oldTrigger;
    try{
        BI_TM_Territory__c[] positionsToUpdate = new List<BI_TM_Territory__c>();
        for (BI_TM_Territory__c position : newpositions.values()) {
            BI_TM_Territory__c oldPosition = (BI_TM_Territory__c) posMap.get(position.Id);
            if (oldPosition.BI_TM_Parent_Position__c!= position.BI_TM_Parent_Position__c) {
                positionsToUpdate.add(new BI_TM_Territory__c(Id = position.BI_TM_Parent_Position__c));
                positionsToUpdate.add(new BI_TM_Territory__c(Id = oldPosition.BI_TM_Parent_Position__c));
            }

        }
        update positionsToUpdate;
        }

        catch(exception e){
        System.debug('Error :'+e);
        }
    }
}