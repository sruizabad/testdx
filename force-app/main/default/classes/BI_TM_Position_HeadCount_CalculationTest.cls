@isTest
public class BI_TM_Position_HeadCount_CalculationTest {

     public static testmethod void Position_HeadCount_CalculationTest(){
        //BI_TM_Territory__c P2=[select id from BI_TM_Territory__c limit 1];
        BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
    		insert posType;
        BI_TM_Territory__c P1=new BI_TM_Territory__c();
        P1.Name='TestPosition1';
        P1.BI_TM_Position_Type_Lookup__c = posType.Id;
        //P1.BI_TM_Parent_Position__c=P2.Id;
        P1.BI_TM_Business__c='PM';
        P1.BI_TM_Country_Code__c='MX';
        P1.BI_TM_Global_Position_Type__c='SALES';
        P1.BI_TM_Start_date__c=system.today();
        P1.bi_tm_position_level__c = 'Representative';
        //P1.BI_TM_Head_Count__c='4';
        P1.BI_TM_Is_Root__c=true;
        insert P1;
        BI_TM_Territory__c P2=new BI_TM_Territory__c();
        P2.Name='TestPosition2';
        P2.BI_TM_Position_Type_Lookup__c = posType.Id;
        P2.BI_TM_Global_Position_Type__c='Territory';
        P2.BI_TM_Parent_Position__c=P1.Id;
        P2.BI_TM_Business__c='PM';
        P2.BI_TM_Country_Code__c='MX';
        P2.BI_TM_Global_Position_Type__c='SALES';
        P2.bi_tm_position_level__c = 'Representative';
        P2.BI_TM_Start_date__c=system.today();
        //P2.BI_TM_Head_Count__c='1';
        //P2.BI_TM_Is_Root__c=true;
        insert P2;
        BI_TM_Territory__c P3=new BI_TM_Territory__c();
        P3.Name='TestPosition3';
        P3.BI_TM_Position_Type_Lookup__c = posType.Id;
        P3.BI_TM_Global_Position_Type__c='Territory';
        P3.BI_TM_Parent_Position__c=P1.Id;
        P3.BI_TM_Business__c='PM';
        P3.BI_TM_Country_Code__c='MX';
        P3.BI_TM_Global_Position_Type__c='SALES';
        P3.BI_TM_Start_date__c=system.today();
        P3.bi_tm_position_level__c = 'Representative';
        //P3.BI_TM_Head_Count__c='1';
        //P2.BI_TM_Is_Root__c=true;
        insert P3;
        P3.BI_TM_Parent_Position__c=P2.Id;
        update P3;
    }
}