/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Check the profile and the permission set values in the positions
*  @author:         Antonio Ferrero
*  @created:        16-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         16-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
public without sharing class BI_TM_Position_Helper {

	// Method to check profile and permission sets in the position
	public static void checkPermissionProfileSettings(List<BI_TM_Territory__c> positionList){
		Set<String> profileSet = getProfileValues();
		checkProfile(positionList, profileSet);
		Set<String> permissionSetSet = getPermissionSetValues();
		checkPermissionSets(positionList, permissionSetSet);
	}

	// Get the profile values from the user management
	private static Set<String> getProfileValues(){
		Set<String> profileSet = new Set<String>();
		List<Schema.PicklistEntry> peList = BI_TM_User_mgmt__c.BI_TM_Profile__c.getDescribe().getPicklistValues();
		for(Schema.PicklistEntry pe : peList){
			profileSet.add(pe.getValue());
		}
		return profileSet;
	}

	// Compare the value of the profile with the settings in the user management
	private static void checkProfile(List<BI_TM_Territory__c> positionList, Set<String> profileSet){
		for(BI_TM_Territory__c pos : positionList){
			if(pos.BI_TM_Profile__c != null && !profileSet.contains(pos.BI_TM_Profile__c)){
				pos.addError(Label.BI_TM_Error_ProfileNoValid);
			}
		}
	}

	// Get the permission set from the user management
	@TestVisible
	private static Set<String> getPermissionSetValues(){
		Set<String> permissionSetSet = new Set<String>();
		List<Schema.PicklistEntry> peList = BI_TM_User_mgmt__c.BI_TM_Permission_Set__c.getDescribe().getPicklistValues();
		for(Schema.PicklistEntry pe : peList){
			permissionSetSet.add(pe.getLabel());
		}
		return permissionSetSet;
	}

	// Compare the set of values of the permission set in the position with the settings in the user management
	private static void checkPermissionSets(List<BI_TM_Territory__c> positionList, Set<String> permissionSetSet){
		for(BI_TM_Territory__c pos : positionList){
			if(pos.BI_TM_Permission_Set__c != null){
				List<String> psPosition = getPositionPermissionSet(pos.BI_TM_Permission_Set__c);
				for(String ps : psPosition){
					if(!permissionSetSet.contains(ps)){
						pos.addError(Label.BI_TM_Error_PermissionSetNoValid);
					}
				}
			}
		}
	}

	private static List<String> getPositionPermissionSet(String pSet){
		return pSet.split(';');
	}
}