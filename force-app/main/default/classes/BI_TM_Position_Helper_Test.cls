@isTest
private class BI_TM_Position_Helper_Test
{
	@isTest
	static void method01()
	{
		Date startDate = Date.newInstance(2016, 1, 1);
		Date endDate = Date.newInstance(2099, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Set<String> nameUserRoleSet = new Set<String>();

		System.runAs(thisUser){
			BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
			insert posType;
			BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
			insert pos;

			for(Integer i = 0; i < 10; i++){
				BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
				posList.add(p);
			}
			insert posList;

			for(BI_TM_Territory__c p : posList){
				nameUserRoleSet.add(p.Name);
			}

		}

		posList[0].BI_TM_Profile__c = 'Test non existing profile';
		try{
			update posList[0];
		}catch(Exception ex) {
			System.debug('Exception message :: ' + ex.getMessage());
			System.assertEquals(true, ex.getMessage().contains(Label.BI_TM_Error_ProfileNoValid));
		}

		posList[2].BI_TM_Permission_Set__c = 'Test non existing permission set';
		try{
			update posList[2];
		}catch(Exception ex) {
			System.debug('Exception message :: ' + ex.getMessage());
			System.assertEquals(true, ex.getMessage().contains(Label.BI_TM_Error_PermissionSetNoValid));
		}
	}

	@isTest
	static void method02()
	{
		Date startDate = Date.newInstance(2016, 1, 1);
		Date endDate = Date.newInstance(2099, 12, 31);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		Set<String> nameUserRoleSet = new Set<String>();

		System.runAs(thisUser){
			BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
			insert posType;
			BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
			insert pos;

			for(Integer i = 0; i < 10; i++){
				BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
				posList.add(p);
			}
			insert posList;

			for(BI_TM_Territory__c p : posList){
				nameUserRoleSet.add(p.Name);
			}

		}
		Test.startTest();
		posList[0].BI_TM_Parent_Position__c = posList[1].Id;
		update posList[0];
		Test.stopTest();
	}
}