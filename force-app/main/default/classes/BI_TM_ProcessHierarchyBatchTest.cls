/********************************************************************************
Name:  BI_TM_ProcessHierarchyBatchTest
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Test Class for BI_TM_ProcessHierarchyBatch and BI_TM_ProcessRoleHierarchyBatch
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran               04/13/2016   INITIAL DEVELOPMENT
*********************************************************************************/

@isTest

public Class BI_TM_ProcessHierarchyBatchTest{

  private static testMethod void  BI_TM_UpdateFutureTerritoryBatchTest_Method() {

    BI_TM_Territory__c Terr= new BI_TM_Territory__c();
    BI_TM_Territory__c Terr1= new BI_TM_Territory__c();
    BI_TM_FF_type__c FF= new BI_TM_FF_type__c();
    BI_TM_Position_Type__c postype= new BI_TM_Position_Type__c();

    FF.Name='Test MX FF21';
    FF.BI_TM_Business__c='PM';
    FF.BI_TM_Country_Code__c= 'MX' ;
    insert FF;
    
    postype.Name='Test pos';
    postype.BI_TM_Business__c='PM';
    postype.BI_TM_Country_Code__c='MX' ;
    insert postype;
    Terr1.Name='FFHierarchy test21';
    Terr1.BI_TM_Is_Root__c=True;
    Terr1.BI_TM_Business__c='PM';
    Terr1.BI_TM_Visible_in_crm__c=True;
    Terr1.BI_TM_Position_Type_Lookup__c=postype.Id;
    Terr1.BI_TM_FF_type__c=FF.Id;
    Terr1.BI_TM_Position_Level__c='District';
    Terr1.BI_TM_Global_Position_Type__c='SPEC';
    //Terr1.BI_TM_Is_Active_Checkbox__c=true;
    Terr1.BI_TM_Country_Code__c = 'MX';
    Terr1.BI_TM_Start_date__c=Date.today().addDays(-1);
    //Terr.BI_TM_End_date__c=;

    Insert Terr1;

    Terr.Name='Test Tam21';
    Terr.BI_TM_FF_type__c=FF.Id;
    Terr.BI_TM_Business__c='PM';
    Terr.BI_TM_Parent_Position__c=Terr1.Id;
    Terr.BI_TM_Position_Type_Lookup__c=postype.Id;
    Terr.BI_TM_Visible_in_crm__c=True;
    Terr.BI_TM_Position_Level__c='District';
    Terr.BI_TM_Global_Position_Type__c='SPEC';
    //Terr.BI_TM_Is_Active_Checkbox__c=True;
    Terr.BI_TM_Country_Code__c = 'MX';
    Terr.BI_TM_Start_date__c=Date.today().addDays(-1);
    //Terr.BI_TM_End_date__c=system.Today();

    Insert Terr;

    test.starttest();

    test.stoptest();
    BI_TM_User_role__c su = [select Id from BI_TM_User_role__c where Name='Test Tam21' ];
    // BI_TM_UserRolecreateHandler  uc= new BI_TM_UserRolecreateHandler (u[0].id,isinsert);
    // uc.insertuserRole(u[0].id);
    System.assertEquals(true, su.Id != null);



    Id batchID;
    BI_TM_ProcessTerritoryHierarchyBatch c = new BI_TM_ProcessTerritoryHierarchyBatch ();
    batchID = Database.executeBatch(c,200);
    BI_TM_ProcessRoleHierarchyBatch r = new BI_TM_ProcessRoleHierarchyBatch();
    batchID = Database.executeBatch(r,200);


  }


}