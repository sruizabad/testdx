/********************************************************************************
Name:  BI_TM_ProcessProducts
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Batch class to assign Products to Users in BITMAN User to Product
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran              29/07/2016   INITIAL DEVELOPMENT
*********************************************************************************/

global class BI_TM_ProcessProducts implements Database.Batchable<sObject>,database.stateful {


    global Database.QueryLocator start(Database.BatchableContext BC) {
        String strQuery = 'SELECT Id FROM BI_TM_Territory__c WHERE recordtype.name = \'Position\'';
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<BI_TM_Territory__c> positions) {

        // System.debug('positions:' + positions);
        //
        // try {
        //     Set<Id> idList = new Set<Id>();//All positions ID List
        //     Set<Id> setposIds = new Set<Id>();//All position ID which has users active in it.
        //
        //     //Get list of all position's ID
        //     for(BI_TM_Territory__c pos : positions) {
        //         idList.add(pos.Id);
        //     }
        //     System.debug('idList:' + idList);
        //
        //     //Fetching user to territory information
        //     list<BI_TM_User_territory__c> userTerrList = [select BI_TM_User__c,BI_TM_User_mgmt_tm__r.name,BI_TM_User_mgmt_tm__r.BI_TM_Username__c,BI_TM_Start_date__c,BI_TM_End_date__c ,BI_TM_Territory1__c,BI_TM_Active__c, BI_TM_Country_Code__c, BI_TM_Business__c from BI_TM_User_territory__c where BI_TM_Territory1__c in: idList AND BI_TM_Active__c=true];
        //     System.debug('userTerrList:' + userTerrList);
        //
        //     //Adding set with position ids which has active user associated.
        //     for(BI_TM_User_territory__c posIds : userTerrList) {
        //         setposIds.add(posIds.BI_TM_Territory1__c);
        //     }
        //
        //     //Fetch all the position to product information for the active positions.
        //     list<BI_TM_Territory_to_product__c> terrProdList = [select BI_TM_Mirror_Product__r.name,BI_TM_Mirror_Product__c,BI_TM_Territory_id__c, BI_TM_Start_date__c, BI_TM_End_date__c from BI_TM_Territory_to_product__c where BI_TM_Territory_id__c in: setposIds AND BI_TM_Active__c=true];
        //     System.debug('terrProdList:' + terrProdList);
        //     list<BI_TM_User_to_product__c> userProdtoCreate = new list<BI_TM_User_to_product__c>();
        //
        //     //Map to store username versus user to territory info.
        //     Map<String,BI_TM_User_territory__c> userNameMap = new Map<String,BI_TM_User_territory__c>();
        //     for(BI_TM_User_territory__c userterrVar : userTerrList) {
        //     if(null!=userTerrVar.BI_TM_User_mgmt_tm__r.BI_TM_Username__c)
        //         userNameMap.Put((userTerrVar.BI_TM_User_mgmt_tm__r.BI_TM_Username__c).toLowerCase(),userTerrVar);
        //     }
        //     System.debug('userNameMap:' + userNameMap);
        //
        //     //Fetch userinformation by providing username as input
        //     list<User> userlist= [Select Id,userName from User where userName in: userNameMap.Keyset()];
        //     System.debug('userlist:' + userlist);
        //
        //     //Fetch the user to product information to check already existing mapping.
        //     list<BI_TM_User_to_product__c> validateProdList = [select id,BI_TM_User__r.name,BI_TM_Product__r.name from BI_TM_User_to_product__c where BI_TM_User__r.BI_TM_Username__c in: userNameMap.keyset() ];
        //     Map<String,BI_TM_User_to_product__c> validateProdMap = new Map<String,BI_TM_User_to_product__c>();
        //
        //     //Set ProductName_UserName versus Product record map
        //     for(BI_TM_User_to_product__c prod : validateProdList ){
        //         validateProdMap.put((prod.BI_TM_Product__r.name+'_'+ prod.BI_TM_User__r.name).toLowerCase(),prod);
        //     }
        //     System.debug('validateProdMap:' + validateProdMap);
        //
        //     Map<String,String> standardUserNameMap = new Map<String,String>();
        //
        //     for(User userterrVar : userlist) {
        //         standardUserNameMap.Put((userTerrVar.userName).toLowerCase() ,userTerrVar.Id);
        //     }
        //     System.debug('standardUserNameMap:' + standardUserNameMap);
        //     for(BI_TM_User_territory__c userTerrVar : userTerrList) {
        //
        //        for(BI_TM_Territory_to_product__c terrProdVar : terrProdList ) {
        //
        //            if(terrprodVar.BI_TM_Territory_id__c ==  userTerrVar.BI_TM_Territory1__c) {
        //
        //                 BI_TM_User_to_product__c userToProd = new BI_TM_User_to_product__c();
        //                 userToProd.BI_TM_Product__c = terrProdVar.BI_TM_Mirror_Product__c;
        //                 if(standardUserNameMap.ContainsKey((userTerrVar.BI_TM_User_mgmt_tm__r.BI_TM_Username__c).toLowerCase()))
        //                 userToProd.BI_TM_UserLookup__c= standardUserNameMap.get((userTerrVar.BI_TM_User_mgmt_tm__r.BI_TM_Username__c).toLowerCase());
        //                 userToProd.BI_TM_User__c=userTerrVar.BI_TM_User_mgmt_tm__r.Id;
        //                 userToProd.BI_TM_Start_date__c =terrProdVar.BI_TM_Start_date__c ;
        //                 userToProd.BI_TM_End_date__c=terrProdVar.BI_TM_End_date__c;
        //                 //Below Lines 73-74 added by Kiran to update Country Code and Business on User to Product
        //                 userToProd.BI_TM_Country_Code__c=userTerrVar.BI_TM_Country_Code__c;
        //                 userToProd.BI_TM_Business__c = userTerrVar.BI_TM_Business__c;
        //                 if(userTerrVar.BI_TM_Active__c==True && !validateProdMap.containsKey((terrProdVar.BI_TM_Mirror_Product__r.name+'_'+userTerrVar.BI_TM_User_mgmt_tm__r.name).toLowerCase() ))
        //                 userProdtoCreate.add(userToProd);
        //            }
        //        }
        //     }
        //
        //     database.insert(userProdToCreate, false);
        //     /*Map<Id,BI_TM_User_to_product__c> mapIdXUsrToProd=new Map<Id,BI_TM_User_to_product__c>();
        //     for(BI_TM_User_to_product__c objUserToProd:userProdToCreate)
        //     {
        //       mapIdXUsrToProd.put(objUserToProd.id,objUserToProd);
        //     }
        //     BI_TM_User_to_productHandler objUserToProdHandler=new BI_TM_User_to_productHandler();
        //     objUserToProdHandler.createMySetupProducts(mapIdXUsrToProd,false);*/
        // } Catch(Exception ex) {
        //     system.debug('@@@@@@ Exception occured @@@@@');
        //     system.debug('@@@@@@ Exception Line Number @@@@@'+ ex.getLineNumber());
        //     system.debug('@@@@@@ Exception Message @@@@@'+ ex.getMessage());
        // }

    }

    global void finish(Database.BatchableContext BC) {
      BI_TM_UserToProdMySetUpProdConf_Batch objBatch = new BI_TM_UserToProdMySetUpProdConf_Batch();
        database.executebatch(objBatch);

    }
}