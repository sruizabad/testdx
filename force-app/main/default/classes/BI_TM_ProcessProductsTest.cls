@isTest
public class BI_TM_ProcessProductsTest {
    
    public static testmethod void processProductTest(){
        
    BI_TM_Territory__c terr = new BI_TM_Territory__c();
    
    BI_TM_Position_Type__c postype = new BI_TM_Position_Type__c();
    postype.BI_TM_Business__c = 'PM';
    postype.BI_TM_Country_Code__c= 'MX';
    postype.Name = 'Unit Test Position Type';
    
    insert posType;

    recordType rt = [select id from recordtype where name = 'Position' and  sObjectType = 'BI_TM_Territory__c' limit 1];
    
    terr.name = 'Unit Test Territory';
    terr.recordtypeId = rt.id;
    terr.BI_TM_Position_Type_Lookup__c = posType.id;
    terr.BI_TM_Start_date__c = system.today();
    terr.BI_TM_Business__c = 'PM';
    terr.BI_TM_Country_Code__c = 'MX';
    terr.BI_TM_Is_Root__c = true;
    insert terr;
    Product_vod__c p=new Product_vod__c();
    p.Name='test';
    p.Country_Code_BI__c='MX' ;
    p.Product_Type_vod__c='Detail';
    p.CurrencyIsoCode='MXN';
    insert p;
    BI_TM_Mirror_Product__c mp=new BI_TM_Mirror_Product__c();
    mp.Name='test';
    mp.BI_TM_Product_Catalog_Id__c=p.Id;
    mp.BI_TM_Country_Code__c='MX';
    //mp.BI_TM_Active__c=True;   
    //mp.BI_TM_Product_Type__c='Detail';   
    Insert mp;
    BI_TM_Territory_to_product__c tp= new BI_TM_Territory_to_product__c();
    tp.BI_TM_Mirror_Product__c=mp.Id;
    tp.BI_TM_Territory_id__c=Terr.Id;
    tp.BI_TM_Country_Code__c='MX';
    Insert tp;
    BI_TM_User_mgmt__c um= new BI_TM_User_mgmt__c();
    um.BI_TM_Last_name__c='TestUser121';
    um.BI_TM_First_name__c='BI';
    um.Name='Testuser121 BI';
    um.BI_TM_Alias__c='Test121';
    um.BI_TM_Email__c='Testuser1@test.com';
    um.BI_TM_Username__c='Testuser101@test.com';
    um.BI_TM_COMMUNITYNICKNAME__c='Test';
    um.BI_TM_Profile__c='MX_DATA_STEWARD';
    um.BI_TM_Business__c='PM';
    um.BI_TM_TimeZoneSidKey__c='Europe/Athens';
    um.BI_TM_LocaleSidKey__c='English (Canada)';
    um.BI_TM_LanguageLocaleKey__c='English';
    um.BI_TM_Currency__c='EUR';
    um.BI_TM_UserCountryCode__c='MX';
    Insert um;
    BI_TM_User_territory__c ut= new BI_TM_User_territory__c();
    ut.BI_TM_User_mgmt_tm__c=um.Id;
    ut.BI_TM_Territory1__c=Terr.Id;
    ut.BI_TM_Country_Code__c='MX';
    ut.BI_TM_Start_date__c=system.today();
    ut.BI_TM_Business__c='PM';
    Insert ut;
    BI_TM_ProcessProducts products = new BI_TM_ProcessProducts();
    database.executeBatch(products,200);
    }
}