global class BI_TM_ProcessProducts_Sch implements Schedulable {
   global void execute(SchedulableContext SC) {
      BI_TM_ProcessProducts obj = new BI_TM_ProcessProducts();
      Database.executeBatch(obj,1); 
   }
}