/********************************************************************************
Name:  BI_TM_ProcessRoleHierarchyBatch
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Batch class to send Roles to Veeva
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran              16/04/2016   INITIAL DEVELOPMENT
*********************************************************************************/


global class BI_TM_ProcessRoleHierarchyBatch implements Database.batchable<sObject> {

    private Boolean execNextBatch = true;

    public void disableNextBacthExecution() {
        this.execNextBatch = false;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Name, BI_TM_Parent_User_Role__c, BI_TM_Parent_User_Role__r.BI_TM_veevaroleId__c, BI_TM_veevaroleId__c FROM BI_TM_User_role__c WHERE BI_TM_Active__c = true AND BI_TM_Visible_in_crm__c = true AND BI_TM_Is_Root__c = false';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<BI_TM_User_role__c> bitmanRoles) {

        //Fetch the role names and store in set. List of unique role name is set in setRoleName.
        set<String> setroleName = new Set<String>();
        for(BI_TM_User_role__c roles:bitmanRoles){
             setroleName.add(roles.Name);

        }
        system.debug('===++++===Role Name ===+++'+setroleName);
        // Send BITMAN Roles to Veeva
        List<UserRole> allRoles = [SELECT Id, Name, ParentRoleId FROM UserRole Where Name IN:setroleName];
        system.debug('===++++===VeevaRoles Size===+++'+allRoles.size());

        //Role name versus role record map.
        Map<String, UserRole> veevaUserRoleMap = new Map<String, UserRole>();
        for (UserRole ur : allRoles) {
            veevaUserRoleMap.put(ur.Name, ur);
        }

        List<UserRole> rolesToInsert = new List<UserRole>();
        List<UserRole> rolesToUpdate = new List<UserRole>();

        for (BI_TM_User_role__c br :  bitmanRoles) {
            if (!veevaUserRoleMap.containsKey(br.Name)) {
                UserRole ur = new UserRole();
                ur.Name = br.Name;
                if(br.BI_TM_Parent_User_Role__r.BI_TM_veevaroleId__c != null){
                  ur.ParentRoleId = br.BI_TM_Parent_User_Role__r.BI_TM_veevaroleId__c;
                }
                rolesToInsert.add(ur);
            } else {
                UserRole veevaRole = veevaUserRoleMap.get(br.Name);
                if (veevaRole.ParentRoleId != br.BI_TM_Parent_User_Role__r.BI_TM_veevaroleId__c) {
                    veevaRole.ParentRoleId = br.BI_TM_Parent_User_Role__r.BI_TM_veevaroleId__c;
                    rolesToUpdate.add(veevaRole);
                }
            }
        }

        if(rolesToInsert.size() > 0) insert rolesToInsert;
        if(rolesToUpdate.size() > 0) update rolesToUpdate;


    }

    global void finish(Database.BatchableContext BC) {
        // we don't want to execute the logic in the triggers, we only want to update the Veeva IDs
        BI_TM_TriggerDispatcher.SKIP_EXECUTION = true;

        //Fetch the details of all the bitman user role records with veeva role ID as null
        List<BI_TM_User_role__c> bitman_UserRoleList=new List<BI_TM_User_role__c>([SELECT Id, name from BI_TM_User_role__c where BI_TM_veevaroleId__c = '']);
        Set<String> bi_roleName=new Set<String>();
        for(BI_TM_User_role__c biUserRoleVar: bitman_userRoleList)
            bi_roleName.add(biUserRoleVar.Name);
        // Update Veeva Role IDs
        Map<String, Id> veevaRoles = new Map<String, Id>();
        for (UserRole ur : [SELECT Id, Name From UserRole WHERE Name IN :bi_roleName]) {
            veevaRoles.put(ur.name.toUpperCase(), ur.id);
        }

        List<BI_TM_User_role__c> rolesToUpdate = new List<BI_TM_User_role__c>();
        for (BI_TM_User_role__c r : bitman_UserRoleList) {
            if (veevaRoles.get(r.name.toUpperCase()) != null) {
                r.BI_TM_veevaroleId__c = veevaRoles.get(r.name.toUpperCase());
                rolesToUpdate.add(r);
            }
        }

        System.debug('###rolesToUpdate: ' + rolesToUpdate);
        if(rolesToUpdate.size() > 0) update rolesToUpdate;

        if (this.execNextBatch) {
          BI_TM_BatchToUpdatelastloginandenddate batchObj = new BI_TM_BatchToUpdatelastloginandenddate();
          database.executebatch(batchObj);
        }
    }

}