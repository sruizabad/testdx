/********************************************************************************
Name:  BI_TM_ProcessTerritoryHierarchyBatch
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Batch class to send Positions to Veeva
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran              16/04/2016   INITIAL DEVELOPMENT
*********************************************************************************/

global class BI_TM_ProcessTerritoryHierarchyBatch implements Database.batchable<sObject> {

    private Boolean execNextBatch = true;

    public void disableNextBacthExecution() {
        this.execNextBatch = false;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        // get country codes managed by BITMAN
        List<BI_TM_CountryCodes__c> codes = BI_TM_CountryCodes__c.getall().values();
        Set<String> codesSet = new Set<String>();
        for(BI_TM_CountryCodes__c c : codes) {
            codesSet.add(c.CountryCode__c);
        }

        String query = 'SELECT id, Name, BI_TM_Country_Code__c, BI_TM_Business__c,BI_TM_External_Id__c, BI_TM_Is_Active__c,BI_TM_Visible_in_crm__c, BI_TM_Start_date__c,BI_TM_End_date__c,BI_TM_Parent_Position__r.BI_TM_TerritoryID__c,BI_TM_Description__c FROM BI_TM_Territory__c WHERE BI_TM_Is_Active__c=true AND BI_TM_Visible_in_crm__c = true and BI_TM_Is_Root__c = false and BI_TM_Country_Code__c IN :codesSet';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<BI_TM_Territory__c> bitmanTerritories) {
        set<String> setterritoryName = new Set<String>();
        for(BI_TM_Territory__c territories:bitmanTerritories){
            setterritoryName.add(territories.Name);
        }

        // get country codes managed by BITMAN
        List<BI_TM_CountryCodes__c> codes = BI_TM_CountryCodes__c.getall().values();
        Set<String> codesSet = new Set<String>();
        for(BI_TM_CountryCodes__c c : codes) {
            codesSet.add(c.CountryCode__c);
        }


        // Send BITMAN Territories to Veeva
        Map<String, Territory> veevaTerritoryMap = new Map<String, Territory>();
        for (Territory vt : [SELECT Id, Name, ParentTerritoryId, Description, AccountAccessLevel FROM Territory where country_code_bi__c in :codesSet AND Name IN:setterritoryName]) {
            veevaTerritoryMap.put(vt.Name.toUpperCase(), vt);
        }

        List<Territory> territoriesToInsert = new List<Territory>();
        List<Territory> territoriesToUpdate = new List<Territory>();

        for (BI_TM_Territory__c bt :  bitmanTerritories) {
            Id parentId = bt.BI_TM_Parent_Position__r.BI_TM_TerritoryID__c ;
            if (!veevaTerritoryMap.containsKey(bt.Name.toUpperCase())) {
                Territory vt = new Territory();
                vt.Name = bt.Name;
                vt.ParentTerritoryId = parentId;
                vt.Country_code_bi__c = bt.BI_TM_Country_Code__c;
                vt.Business_bi__c = bt.BI_TM_Business__c;
                //Below line is added by Kiran on 23-June-2016 to update the AccountAccessLevel to View and Edit in Veeva Territory
                vt.AccountAccessLevel='Edit';
                //Below line is added by Kiran on 05-July-2016 to add the Description as Territory Name in Veeva
                //Below Line updated on 12-04-2017 to update the Description same as BITMAN Description
                vt.Description=bt.BI_TM_Description__c;
                territoriesToInsert.add(vt);
            } else {
                Territory veevaTerr = veevaTerritoryMap.get(bt.Name.toUpperCase());
                System.debug('bt: ' + bt);
                System.debug('t:' + veevaTerr);
                system.debug('VeevaTerritory Description'+veevaTerr.Description);
                if (veevaTerr.ParentTerritoryId != parentId || veevaTerr.Description != bt.BI_TM_Description__c) {
                    veevaTerr.ParentTerritoryId = parentId;
                    veevaTerr.AccountAccessLevel='Edit';
                    veevaTerr.Description = bt.BI_TM_Description__c;
                    territoriesToUpdate.add(veevaTerr);
                }
            }
        }

        if(territoriesToInsert.size() > 0) insert territoriesToInsert;
        if(territoriesToUpdate.size() > 0)  update territoriesToUpdate;

    }

    global void finish(Database.BatchableContext BC) {
        // we don't want to execute the logic in the triggers, we only want to update the Veeva IDs
        BI_TM_TriggerDispatcher.SKIP_EXECUTION = true;

        //Fetch the BITMAN territory object data that doesn't have veeva ID field.
        List<BI_TM_Territory__c> bi_TerritoryList=new List<BI_TM_Territory__c>([SELECT Id, name from BI_TM_Territory__c where BI_TM_TerritoryID__c = '']);
        Set<String> territoryNames=new Set<String>();
        for(BI_TM_Territory__c biTerrVar:bi_TerritoryList){
            territoryNames.add(biTerrVar.Name);
        }
        // Update Veeva Territory IDs
        Map<String, Id> veevaTerrs = new Map<String, Id>();
        for (Territory t : [SELECT Id, Name From Territory where Name IN :territoryNames]) {
            veevaTerrs.put(t.name.toUpperCase(), t.id);
        }

        List<BI_TM_Territory__c> terrsToUpdate = new List<BI_TM_Territory__c>();
        for (BI_TM_Territory__c t : bi_TerritoryList) {
            if (veevaTerrs.get(t.name.toUpperCase()) != null) {
                t.BI_TM_TerritoryID__c = veevaTerrs.get(t.name.toUpperCase());
                terrsToUpdate.add(t);
            }
        }

        System.debug('###terrsToUpdate: ' + terrsToUpdate);
        if(terrsToUpdate.size() > 0) update terrsToUpdate;

        if (this.execNextBatch) {
            BI_TM_ProcessRoleHierarchyBatch batchable = new BI_TM_ProcessRoleHierarchyBatch();
            database.executeBatch(batchable, 1);
        }
    }

}