/********************************************************************************
Name:  BI_TM_ProcessHierarchyBatchTest
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Schedule Class for BI_TM_ProcessTerritoryHierarchyBatch
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran               04/13/2016   INITIAL DEVELOPMENT
*********************************************************************************/

global class BI_TM_ProcessTerritoryHierarchyBatch_Sch implements Schedulable {
   global void execute(SchedulableContext SC) {
      BI_TM_ProcessTerritoryHierarchyBatch obj = new BI_TM_ProcessTerritoryHierarchyBatch();
      Database.executeBatch(obj, 1);
   }
}