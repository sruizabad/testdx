/********************************************************************************
Name:  BI_TM_Process_Active_Users_Veeva
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Batch apex class to Activate the veeva User and Assign Permission Set to Active User
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran              24/01/2017   INITIAL DEVELOPMENT
2.0-     Kiran              06/04/2017   As Part of UOR we added Visible in CRM field in Start Query.
Based on this field Activation will be defined
*********************************************************************************/

global class BI_TM_Process_Active_Users_Veeva implements Database.Batchable<sObject>,database.stateful{

  global Database.QueryLocator start(Database.BatchableContext BC) {
    String strQuery = 'SELECT Id,BI_TM_Username__c, BI_TM_UserId_Lookup__c, BI_TM_UserId_Lookup__r.LastLoginDate, BI_TM_Visible_in_CRM__c FROM BI_TM_User_mgmt__c where BI_TM_Start_date__c <= TODAY AND (BI_TM_End_date__c >= TODAY OR BI_TM_End_date__c = null) and BI_TM_UserId_Lookup__c!=null and BI_TM_UserId_Lookup__r.isActive= false AND BI_TM_Visible_in_CRM__c = true';
    return Database.getQueryLocator(strQuery);
  }

  global void execute(Database.BatchableContext BC, List<BI_TM_User_mgmt__c> lstBIUserMgmnt) {
    List<User> lstUser=new List<User>();
    Map<String,Set<String>> mapPermissionSetNameXUserIds=new Map<String,Set<String>>();
    Set<String> setUserIdsToUpdate=new set<String>();
    for(BI_TM_User_mgmt__c objUserMgmt:lstBIUserMgmnt)
    {
      if(objUserMgmt.BI_TM_UserId_Lookup__c!=null )
      {
        if(!setUserIdsToUpdate.contains(objUserMgmt.BI_TM_UserId_Lookup__c))
        {
          User objUser=new User(ID=objUserMgmt.BI_TM_UserId_Lookup__c,isActive=true);
          lstUser.add(objUser);

        }
      }
    }

    try{
      Database.SaveResult[] srList = database.Update(lstUser,false);
      BI_TM_Utils.manageErrorLogUpdate(srList);
      Set<Id> userIds = new Set<Id>();
      for(Database.SaveResult sr : srList){
        if(sr.isSuccess()){
          userIds.add(sr.getId());
        }
      }
      // Get map with users that have the SSO enabled
      Map<Id, boolean> userIsSSOMap = BI_TM_Utils.getUserIsSSOMap(userIds);

      for(User u : [SELECT Id, ProfileId FROM User WHERE isActive = TRUE AND Id IN :userIds AND LastLoginDate = null]){
        try {
          if(!userIsSSOMap.keySet().contains(u.Id)){
            System.resetPassword(u.Id, true);
          }
        } catch(Exception ex) {
          System.debug('*** exception: ' + ex.getMessage());
        }
      }
    }catch(system.Dmlexception ex){
      Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
      message.toAddresses = new String[] { UserInfo.getUserEmail() };
      message.optOutPolicy = 'FILTER';
      message.subject = 'User Activation failure:';
      message.plainTextBody = 'Dear User,\r\n The Number of licenses are exceeded. Please contact your admin for help.\r\n \r\n Error: \r\n'+ex.getDmlMessage(0) +'\r\n \r\n Thanks,\r\n'+'BITMAN';
      Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
      Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
      system.debug (ex);
    }
  }

  global void finish(Database.BatchableContext BC) {
    database.executebatch(new BI_TM_UserToPositionActivate_Bch(),99);
  }

}