/********************************************************************************
Name:  BI_TM_Process_Inactive_Users_Veeva
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Batch apex class to Inactivate the veeva User
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran              24/01/2017   INITIAL DEVELOPMENT
*********************************************************************************/

global class BI_TM_Process_Inactive_Users_Veeva implements Database.Batchable<sObject>,database.stateful{

  global Database.QueryLocator start(Database.BatchableContext BC) {
    String strQuery = 'SELECT Id,BI_TM_Username__c, BI_TM_UserId_Lookup__c, BI_TM_Visible_in_CRM__c FROM BI_TM_User_mgmt__c WHERE BI_TM_UserId_Lookup__c !=null AND ((BI_TM_End_date__c != null AND BI_TM_End_date__c < Today AND BI_TM_UserId_Lookup__r.IsActive=true AND BI_TM_Visible_in_CRM__c = TRUE) OR BI_TM_Visible_in_CRM__c = FALSE)';
    return Database.getQueryLocator(strQuery);
  }

  global void execute(Database.BatchableContext BC, List<BI_TM_User_mgmt__c> lstBIUserMgmnt) {

    List<User> lstUser=new List<User>();
    for(BI_TM_User_mgmt__c um: lstBIUserMgmnt) {
      User u = new User(Id = um.BI_TM_UserId_Lookup__c, IsActive = false);
    }
    try
    {
      Database.SaveResult[] srList = database.Update(lstUser,false);
      BI_TM_Utils.manageErrorLogUpdate(srList);
    }
    catch(System.exception e)
    {
      Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
      message.toAddresses = new String[] { UserInfo.getUserEmail() };
      message.optOutPolicy = 'FILTER';
      message.subject = 'User Inactivation failure:';
      message.plainTextBody = 'Dear User,\r\n The next issue has been happened . Please contact your admin for help.\r\n \r\n Error: \r\n'+e.getMessage() +'\r\n \r\n Thanks,\r\n'+'BITMAN';
      Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
      Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
      system.debug (e);
    }

  }

  global void finish(Database.BatchableContext BC) {

    BI_TM_Process_Active_Users_Veeva s= new BI_TM_Process_Active_Users_Veeva();
    database.executeBatch(s);

  }

}