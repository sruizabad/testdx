/********************************************************************************
Name:  BI_TM_Process_Inactive_Users_Veeva_Test
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Test class for BI_TM_Process_Inactive_Users_Veeva

=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Kiran              24/01/2017   INITIAL DEVELOPMENT
*********************************************************************************/
@isTest
public class BI_TM_Process_Inactive_Users_Veeva_Test {

  public static testMethod void Test_BI_TM_Process_Active_Users_Veeva () {

    Date startDate = Date.newInstance(2016, 1, 1);
    Date endDate = Date.newInstance(2099, 12, 31);
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    System.runAs (thisUser) {
      DescribeFieldResult describeCountryCode = BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c.getDescribe();
      List<PicklistEntry> availableValuesCountryCode = describeCountryCode.getPicklistValues();
      Map<String, List<String>> dependentProfile = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Profile__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
      Map<String, List<String>> dependentCurrency = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Currency__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
      Map<String, List<String>> dependentLocale = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LocaleSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
      Map<String, List<String>> dependentLanguage = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LanguageLocaleKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
      Map<String, List<String>> dependentTimezone = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_TimeZoneSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);

      String countryCodeUM = '';
      for(Integer cc = 0; cc < availableValuesCountryCode.size() ; cc++){
        String countryCode = availableValuesCountryCode[cc].getValue();
        if(dependentProfile.get(countryCode) != null && (dependentProfile.get(countryCode)).size() > 0
        && dependentCurrency.get(countryCode) != null && (dependentCurrency.get(countryCode)).size() > 0
        && dependentLocale.get(countryCode) != null && (dependentLocale.get(countryCode)).size() > 0
        && dependentLanguage.get(countryCode) != null && (dependentLanguage.get(countryCode)).size() > 0
        && dependentTimezone.get(countryCode) != null && (dependentTimezone.get(countryCode)).size() > 0){
          countryCodeUM = countryCode;
          break;
        }
      }

      List<BI_TM_User_mgmt__c> userManList = new List<BI_TM_User_mgmt__c>();

      for(Integer i = 0; i < 10; i++){
        BI_TM_User_mgmt__c um = BI_TM_UtilClassDataFactory_Test.createUserManagement('Smith' + Integer.ValueOf(i), 'John' + Integer.ValueOf(i), countryCodeUM, 'PM', 'JSMith' + Integer.ValueOf(i),
        'jsmith' + Integer.ValueOf(i) + '@test.bitman', 'John.Smith' + Integer.ValueOf(i) + startDate.format(), 'jsmith' + Integer.ValueOf(i) + '@test.bitman', (dependentProfile.get(countryCodeUM))[0], (dependentLocale.get(countryCodeUM))[0],
        (dependentTimezone.get(countryCodeUM))[0], (dependentLanguage.get(countryCodeUM))[0], (dependentCurrency.get(countryCodeUM))[0], 'General US & Europe(ISO-8859-1)', startDate, endDate, true);
        userManList.add(um);
      }

      Test.startTest();
      BI_TM_Process_Inactive_Users_Veeva bc = new BI_TM_Process_Inactive_Users_Veeva();
      Database.executeBatch(bc, 200);
      Test.stopTest();

    }

  }
}