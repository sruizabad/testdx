public class BI_TM_RecordVerification {
    
    private String countryFieldName = 'BI_TM_Country_Code__c';
    private String businessFieldName = 'BI_TM_Business__c';
    private User u;
    
    private Map<String,Set<String>> countryBusinessMap;
    
    public BI_TM_RecordVerification () {
        countryBusinessMap = BI_TM_UserInfo.getInstance().countryBusinessMap;
        system.debug('====countryBusinessMap ====='+countryBusinessMap);
        u = BI_TM_UserInfo.getInstance().userSettings;
        system.debug('====++++====='+u);
    }
    
    public void checkCountryAndBusiness(List<SObject> records) {
        if(Test.isRunningTest()) return;
        if(BI_TM_Utils.isAdmin()) return;
        
        for (SObject o : records) {
            Map<String,Schema.SObjectField> mfields = o.getSObjectType().getDescribe().fields.getMap();
            if (!mfields.containsKey(countryFieldName)) continue;
            
            if (String.isBlank((String)o.get(countryFieldName)))  {
                if (countryBusinessMap.keySet().size() > 1) {
                    o.addError('country code field required');
                } else {
                    o.put(countryFieldName, u.Country_Code_BI__c);
                }
                system.debug('====*****====='+countryFieldName);
            } 
            else {
                if (!countryBusinessMap.containsKey((String)o.get(countryFieldName))) {
                    o.addError('country code not allowed');
                    system.debug('====**++***====='+countryBusinessMap);
                } 
            }


            if (mfields.containsKey(businessFieldName)) {
                if (String.isBlank((String)o.get(businessFieldName)))  {
                    if (!String.isBlank(countryFieldName) && countryBusinessMap.get((String)o.get(countryFieldName)) != null && countryBusinessMap.get((String)o.get(countryFieldName)).size() > 1) {
                        o.addError('business field required');
                    } else {
                        o.put(businessFieldName, u.Business_BI__c);
                    }
                } 
                else {
                    if (countryBusinessMap.get((String)o.get(countryFieldName)) != null && 
                        !countryBusinessMap.get((String)o.get(countryFieldName)).contains((String)o.get(businessFieldName))) {
                        o.addError('business not allowed');
                    }
                }
            } 
        }
    }
    


}