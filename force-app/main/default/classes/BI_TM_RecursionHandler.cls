/* 
Name: BI_TM_RecursionHandler 

Description: This class for handling recursion.
Version | Author-Email | Date | Comment 
1.0 | Shilpa Patil | 08.03.2016 | initial version 
*/
public without sharing Class BI_TM_RecursionHandler {

    public static Boolean isBool = true;
    public static Boolean isInsertTrigger = true;
    public static Boolean isDeleteTrigger = true; 

}