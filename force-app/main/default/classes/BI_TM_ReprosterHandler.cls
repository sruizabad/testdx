public class BI_TM_ReprosterHandler{
     public BI_TM_ReprosterHandler(set<id> uId){
         List<BI_TM_User_Address__c> uAList= new List<BI_TM_User_Address__c>();
         List<Rep_Roster_vod__c> rList= new List<Rep_Roster_vod__c>();
         List<Id> rdelList= new List<Id>();
         map<string,string>umap=new  map<string,string>();
         List<Rep_Roster_vod__c> rListexist= new List<Rep_Roster_vod__c>();
         String Concate1;String Concate2;
         
         uAList=[Select BI_TM_Address_Line_1__c,BI_TM_Address_Line_2__c,BI_TM_City__c,BI_TM_Address_Type__c,BI_TM_Computer_Asset_Tag__c,BI_TM_Country_Code__c,BI_TM_User__r.BI_TM_Phone__c,BI_TM_User__r.BI_TM_Employee_Number__c,
                        BI_TM_Fax_Number__c,BI_TM_State__c,BI_TM_User__r.BI_TM_Username__c,BI_TM_User__r.BI_TM_Home_Phone__c,BI_TM_User__r.BI_TM_Fax__c,
                        BI_TM_Territory__c,BI_TM_VIN__c,BI_TM_Zip__c,BI_TM_User__r.BI_TM_First_name__c,BI_TM_User__r.BI_TM_Last_name__c from BI_TM_User_Address__c where Id in:uId and BI_TM_Address_Type__c='SHIPPING ADDRESS'];
        for(BI_TM_User_Address__c ul:uAList){
        umap.put(ul.BI_TM_User__r.BI_TM_Username__c,ul.BI_TM_Country_Code__c);
        }
        rListexist=[select Id,name,Username_External_Id_vod__c from Rep_Roster_vod__c where Username_External_Id_vod__c in:umap.keyset()];
        try{
         for(BI_TM_User_Address__c u:uAList){
             
                Rep_Roster_vod__c r= new Rep_Roster_vod__c();
                   
                    //Concate2 = u.BI_TM_Shipping_Territory__c+u.BI_TM_UserCountryCode__c+';';
                    r.Address_Line_1_vod__c=u.BI_TM_Address_Line_1__c;
                    r.Address_Line_2_vod__c=u.BI_TM_Address_Line_2__c;
                    r.City_vod__c=u.BI_TM_City__c;
                    r.Computer_Asset_Tag__c=u.BI_TM_Computer_Asset_Tag__c;
                    r.Country_Code_BI__c=u.BI_TM_Country_Code__c;
                    r.Fax_Number__c=u.BI_TM_User__r.BI_TM_Fax__c;
                    r.Home_Phone__c=u.BI_TM_User__r.BI_TM_Home_Phone__c;
                    r.Mobile_Number__c=u.BI_TM_User__r.BI_TM_Phone__c;
                    r.Organization_Id__c=u.BI_TM_User__r.BI_TM_Employee_Number__c;
                    r.State_vod__c=u.BI_TM_State__c;
                    r.Territory_vod__c=u.BI_TM_Territory__c;
                    r.VIN__c=u.BI_TM_VIN__c;
                    r.Zip_vod__c=u.BI_TM_Zip__c.left(6);
                    r.Name=u.BI_TM_User__r.BI_TM_Last_name__c+','+u.BI_TM_User__r.BI_TM_First_name__c;
                    r.Username_External_Id_vod__c=u.BI_TM_User__r.BI_TM_Username__c;
                   for(Rep_Roster_vod__c rE:rListexist){
                        if(rE.Username_External_Id_vod__c==u.BI_TM_User__r.BI_TM_Username__c){
                            r.Id=rE.Id;
                        }
                   }
                  if(u.BI_TM_Address_Type__c=='SHIPPING ADDRESS')
                    rList.add(r);
                  
                  system.debug('*******rList*************'+rList);
                  system.debug('*******rdelList*************'+rdelList);
                 
           }
        if(rList.size()>0){ 
             Database.Upsert(rList,false);
         }
        }catch(Exception Ex){
        
        } 
         
     }

}