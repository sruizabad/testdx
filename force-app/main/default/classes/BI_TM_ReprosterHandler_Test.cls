@isTest
public class BI_TM_ReprosterHandler_Test {
  public static testMethod void TestBI_TM_ReprosterHandler() {
    Date startDate = Date.newInstance(2016, 1, 1);
    Date endDate = Date.newInstance(2099, 12, 31);

    List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
    BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PosTypeTest','US','PM');
    insert posType;
    BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
    insert pos;

    for(Integer i = 0; i < 10; i++){
      BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
      posList.add(p);
    }
    insert posList;

    DescribeFieldResult describeCountryCode = BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c.getDescribe();
    List<PicklistEntry> availableValuesCountryCode = describeCountryCode.getPicklistValues();
    Map<String, List<String>> dependentProfile = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Profile__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
    Map<String, List<String>> dependentCurrency = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_Currency__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
    Map<String, List<String>> dependentLocale = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LocaleSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
    Map<String, List<String>> dependentLanguage = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_LanguageLocaleKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);
    Map<String, List<String>> dependentTimezone = BI_TM_FieldDescribeUtil.getDependentOptionsImpl(BI_TM_User_mgmt__c.BI_TM_TimeZoneSidKey__c, BI_TM_User_mgmt__c.BI_TM_UserCountryCode__c);

    String countryCodeUM = '';
    for(Integer cc = 0; cc < availableValuesCountryCode.size() ; cc++){
      String countryCode = availableValuesCountryCode[cc].getValue();
      if(dependentProfile.get(countryCode) != null && (dependentProfile.get(countryCode)).size() > 0
      && dependentCurrency.get(countryCode) != null && (dependentCurrency.get(countryCode)).size() > 0
      && dependentLocale.get(countryCode) != null && (dependentLocale.get(countryCode)).size() > 0
      && dependentLanguage.get(countryCode) != null && (dependentLanguage.get(countryCode)).size() > 0
      && dependentTimezone.get(countryCode) != null && (dependentTimezone.get(countryCode)).size() > 0){
        countryCodeUM = countryCode;
        break;
      }
    }

    List<BI_TM_User_mgmt__c> userManList = new List<BI_TM_User_mgmt__c>();
    List<BI_TM_User_territory__c> u2pList = new List<BI_TM_User_territory__c>();
    for(Integer i = 0; i < 10; i++){
      BI_TM_User_mgmt__c um = BI_TM_UtilClassDataFactory_Test.createUserManagementVisibleCRM('Smith' + Integer.ValueOf(i), 'John' + Integer.ValueOf(i), countryCodeUM, 'PM', 'JSMith' + Integer.ValueOf(i),
      'jsmith' + Integer.ValueOf(i) + '@test.bitman', 'John.Smith' + Integer.ValueOf(i) + startDate.format(), 'jsmith' + Integer.ValueOf(i) + '@test.bitman', (dependentProfile.get(countryCodeUM))[0], (dependentLocale.get(countryCodeUM))[0],
      (dependentTimezone.get(countryCodeUM))[0], (dependentLanguage.get(countryCodeUM))[0], (dependentCurrency.get(countryCodeUM))[0], 'General US & Europe(ISO-8859-1)', startDate, endDate, true, true, true);
      userManList.add(um);
    }
    insert userManList;

    for(Integer i = 0; i < 10; i++){
			BI_TM_User_territory__c u2p = BI_TM_UtilClassDataFactory_Test.createUser2Position(posList[i].Id, userManList[i].Id, countryCodeUM, 'PM', true, 'Primary', startDate, endDate, 'No Approval Needed');
			u2pList.add(u2p);
		}
		insert u2pList;

    BI_TM_User_Address__c  UA= new BI_TM_User_Address__c();
    UA.BI_TM_Address_Line_1__c='DTP';
    UA.BI_TM_Address_Line_2__c='3F-120';
    UA.BI_TM_City__c='Bangalore';
    UA.BI_TM_Computer_Asset_Tag__c='';
    UA.BI_TM_Country_Code__c='US';
    UA.BI_TM_Fax_Number__c='12345';
    UA.BI_TM_Home_Phone__c='12345';
    UA.BI_TM_Mobile_Number__c='12345678';
    UA.BI_TM_Organization_Id__c='';
    UA.BI_TM_State__c='Karnataka';
    UA.BI_TM_Territory__c=';MX01;MX02;MX03;';
    UA.BI_TM_VIN__c='';
    UA.BI_TM_Zip__c='1258';
    UA.BI_TM_User__C=userManList[0].Id;

    Insert UA;
  }
}