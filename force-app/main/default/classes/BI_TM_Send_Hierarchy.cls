/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  @Description:    Update position hierarchy with the position relation records from the alignment cycle
*  @author:         Antonio Ferrero
*  @created:        12-Apr-2017
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         12-Apr-2017                 Antonio Ferrero             Construction of the class.
*/

global class BI_TM_Send_Hierarchy{
  WebService static void updatePositionHierarchy(String aliId){

    // Retrieve all active position relations from the active alignment
    List<BI_TM_Position_Relation__c> posRelList = [SELECT Id, BI_TM_Position__c, BI_TM_Parent_Position__c, BI_TM_Territory__c, BI_TM_Start_Date__c, BI_TM_End_Date__c FROM BI_TM_Position_Relation__c WHERE BI_TM_Alignment_Cycle__c =:aliID AND BI_TM_Active__c = TRUE AND BI_TM_Alignment_Cycle__r.BI_TM_Status__c = 'Active'];
    Set<Id> positionSet = new Set<Id>();
    for(BI_TM_Position_Relation__c pr : posRelList){
      positionSet.add(pr.BI_TM_Position__c);
    }

    // Retrieve all positions that are related to the position relation records
    Map<Id, BI_TM_Territory__c> posMap = new Map<Id, BI_TM_Territory__c>([SELECT Id, BI_TM_Parent_Position__c, BI_TM_Territory_ND__c, BI_TM_Start_date__c, BI_TM_End_date__c FROM BI_TM_Territory__c WHERE Id IN :positionSet]);

    List<BI_TM_Territory__c> position2updateList = new List<BI_TM_Territory__c>();
    for(BI_TM_Position_Relation__c pr : posRelList){
      Boolean isChanged = false; // only update if any changes
      BI_TM_Territory__c pos = posMap.get(pr.BI_TM_Position__c);

      // Update of the parent position
      if(pr.BI_TM_Parent_Position__c != null && pos.BI_TM_Parent_Position__c != pr.BI_TM_Parent_Position__c){
        pos.BI_TM_Parent_Position__c = pr.BI_TM_Parent_Position__c;
        isChanged = true;
      }

      // Update of the parent territory
      if(pr.BI_TM_Territory__c != null && pos.BI_TM_Territory_ND__c != pr.BI_TM_Territory__c){
        pos.BI_TM_Territory_ND__c = pr.BI_TM_Territory__c;
        isChanged = true;
      }

      // If start date is less restrictive update that date
      if(pos.BI_TM_Start_date__c > pr.BI_TM_Start_date__c){
        pos.BI_TM_Start_date__c = pr.BI_TM_Start_date__c;
        isChanged = true;
      }

      // Update end date to less restrictive
      if(((pos.BI_TM_End_date__c != null) && (pr.BI_TM_End_date__c != null) && (pos.BI_TM_End_date__c < pr.BI_TM_End_date__c)) || pr.BI_TM_End_date__c == null){
        pos.BI_TM_End_date__c = pr.BI_TM_End_date__c;
        isChanged = true;
      }

      if(isChanged){
        position2updateList.add(pos);
      }
    }

    try{
      if(position2updateList.size() > 0){
        update position2updateList;
      }
    }
    catch(Exception ex){
      system.debug('An exception has ocurred :: ' + ex.getMessage());
    }

  }
}