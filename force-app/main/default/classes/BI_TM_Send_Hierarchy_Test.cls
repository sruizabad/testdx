/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  @Decription:      Update position hierarchy with the position relation records from the alignment cycle - Test class
*  @author:         Antonio Ferrero
*  @created:        12-Apr-2017
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         05-Jan-2016                 Antonio Ferrero             Construction of the class.
*/
@isTest
public class BI_TM_Send_Hierarchy_Test{
  static testMethod void validateNewPositionHierarchy(){

    Date startDate = Date.newInstance(2016, 1, 1);
    Date startDate2 = Date.newInstance(2015, 1, 1);
    Date endDate = Date.newInstance(2017, 12, 31);

    BI_TM_Position_Type__c pType = new BI_TM_Position_Type__c(Name = 'PType', BI_TM_Country_Code__c = 'US', BI_TM_Business__c = 'PM');
    insert pType;

    BI_TM_FF_type__c ff = new BI_TM_FF_type__c(Name = 'FF-TST', BI_TM_Country_Code__c = 'US', BI_TM_Business__c = 'PM');
    insert ff;

    BI_TM_Alignment__c ali = new BI_TM_Alignment__c(Name = 'Alignment TEST', BI_TM_Country_Code__c = 'US', BI_TM_Business__c = 'PM', BI_TM_FF_type__c = ff.Id, BI_TM_Start_date__c = startDate2, BI_TM_Status__c = 'Active');
    insert ali;

    BI_TM_Territory_ND__c terr1 = new BI_TM_Territory_ND__c(Name = 'TERR-TEST-1', BI_TM_Country_Code__c = 'US', BI_TM_Business__c = 'PM', BI_TM_Start_date__c = startDate, BI_TM_Field_Force__c = ff.Id);
    BI_TM_Territory_ND__c terr2 = new BI_TM_Territory_ND__c(Name = 'TERR-TEST-2', BI_TM_Country_Code__c = 'US', BI_TM_Business__c = 'PM', BI_TM_Start_date__c = startDate, BI_TM_Field_Force__c = ff.Id);
    insert terr1;
    insert terr2;

    BI_TM_Territory__c pos1 = new BI_TM_Territory__c();
    pos1.Name = 'Region 1';
    pos1.BI_TM_Position_Level__c = 'Region';
    pos1.BI_TM_Position_Type_Lookup__c = pType.Id;
    pos1.BI_TM_Start_date__c = startDate;
    pos1.BI_TM_Country_Code__c = 'US';
    pos1.BI_TM_Business__c = 'PM';
    pos1.BI_TM_Global_Position_Type__c = 'GR';
    pos1.BI_TM_Is_Root__c = true;
    insert pos1;

    BI_TM_Territory__c pos2 = new BI_TM_Territory__c();
    pos2.Name = 'District 1';
    pos2.BI_TM_Position_Level__c = 'District';
    pos2.BI_TM_Position_Type_Lookup__c = pType.Id;
    pos2.BI_TM_Start_date__c = startDate;
    pos2.BI_TM_Country_Code__c = 'US';
    pos2.BI_TM_Business__c = 'PM';
    pos2.BI_TM_Global_Position_Type__c = 'GR';
    pos2.BI_TM_Parent_Position__c = pos1.Id;
    insert pos2;

    BI_TM_Territory__c pos = new BI_TM_Territory__c();
    pos.Name = 'TST-Primary Position';
    pos.BI_TM_Position_Level__c = 'Representative';
    pos.BI_TM_Position_Type_Lookup__c = pType.Id;
    pos.BI_TM_Start_date__c = startDate;
    pos.BI_TM_End_date__c = endDate;
    pos.BI_TM_Country_Code__c = 'US';
    pos.BI_TM_Business__c = 'PM';
    pos.BI_TM_Global_Position_Type__c = 'GR';
    pos.BI_TM_Parent_Position__c = pos2.Id;
    pos.BI_TM_Territory_ND__c = terr1.Id;
    insert pos;

    BI_TM_Position_Relation__c pr = new BI_TM_Position_Relation__c();
    pr.BI_TM_Active__c = true;
    pr.BI_TM_Alignment_Cycle__c = ali.Id;
    pr.BI_TM_Business__c = 'PM';
    pr.BI_TM_Country_Code__c = 'US';
    pr.BI_TM_Position__c = pos.Id;
    pr.BI_TM_Parent_Position__c = pos1.Id;
    pr.BI_TM_Territory__c = terr2.Id;
    insert pr;

    Test.startTest();
    BI_TM_Send_Hierarchy.updatePositionHierarchy(ali.Id);
    Test.stopTest();

    BI_TM_Territory__c posTest = [Select Id, BI_TM_Parent_Position__c, BI_TM_Territory_ND__c, BI_TM_Start_date__c, BI_TM_End_date__c FROM BI_TM_Territory__c WHERE Id = :pos.Id];
    System.assertEquals(posTest.BI_TM_Parent_Position__c, pos1.Id);
    System.assertEquals(posTest.BI_TM_Territory_ND__c, terr2.Id);
    System.assertEquals(posTest.BI_TM_Start_date__c, startDate2);
    System.assertEquals(posTest.BI_TM_End_date__c, null);
  }
}