global class BI_TM_Terr2Prod_ChildHandler_Batch  implements Database.Batchable<sObject> {

    String strQuery;

    global BI_TM_Terr2Prod_ChildHandler_Batch()
    {
        strQuery='select id, BI_TM_Country_Code__c, BI_TM_Mirror_Product__c,  BI_TM_End_Date__c, BI_TM_Start_Date__c, BI_TM_Territory_ND__c ';
        strQuery+='from BI_TM_Territory_to_Product_ND__c ';
        strQuery+='where BI_TM_Active__c=true OR (BI_TM_Active__c=false AND BI_TM_Start_Date__c>=TODAY )';
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(strQuery);
    }

  global void execute(Database.BatchableContext BC, List<sObject> lstScope)
    {
        set<String> setTerrIds=new set<String>();
        Map<String,set<String>> mapTerrIdXSetProds=new Map<String,set<String>>();
        Map<String,BI_TM_Territory_to_Product_ND__c> mapTerrProdIdXTerrToProd=new Map<String,BI_TM_Territory_to_Product_ND__c>();
        for(SObject objScope:lstScope)
        {
            BI_TM_Territory_to_Product_ND__c objTerrToProd=(BI_TM_Territory_to_Product_ND__c)objScope;
            //objTerrToProd.BI_TM_Batch_executed__c=true;
            String strTerrIdPrdId=(String)objTerrToProd.BI_TM_Territory_ND__c+(String)objTerrToProd.BI_TM_Mirror_Product__c;
            if(!mapTerrProdIdXTerrToProd.containskey(strTerrIdPrdId))
            {
                mapTerrProdIdXTerrToProd.put(strTerrIdPrdId, objTerrToProd);
            }
            if(!setTerrIds.contains(objTerrToProd.BI_TM_Territory_ND__c))
            {
                setTerrIds.add(objTerrToProd.BI_TM_Territory_ND__c);
            }
            if(mapTerrIdXSetProds.containskey(objTerrToProd.BI_TM_Territory_ND__c))
            {
                set<String> setPrdIds=mapTerrIdXSetProds.get(objTerrToProd.BI_TM_Territory_ND__c);
                if(!setPrdIds.contains(objTerrToProd.BI_TM_Mirror_Product__c))
                {
                    setPrdIds.add(objTerrToProd.BI_TM_Mirror_Product__c);
                }

            }
            else
            {
                set<String> setPrdIds=new Set<String>();
                setPrdIds.add(objTerrToProd.BI_TM_Mirror_Product__c);
                mapTerrIdXSetProds.put(objTerrToProd.BI_TM_Territory_ND__c,setPrdIds);
            }
        }
        List<BI_TM_Territory__c> lstPos=[select id, BI_TM_Territory_ND__c,BI_TM_Business__c,
                                                                                        BI_TM_Country_Code__c, BI_TM_End_Date__c,BI_TM_Start_Date__c
                                                                         from BI_TM_Territory__c
                                                                         where BI_TM_Territory_ND__c=:setTerrIds and(BI_TM_Is_Active__c=true OR (BI_TM_Is_Active__c=false and BI_TM_Start_Date__c>=TODAY))];

        List<BI_TM_Territory_to_product__c> lstPosToProd=getLstPosToProd(mapTerrIdXSetProds,mapTerrProdIdXTerrToProd,lstPos);
        database.insert(lstPosToProd,false);
        //database.update(lstScope);
    }

    public List<BI_TM_Territory_to_product__c> getLstPosToProd(Map<String,set<String>> mapTerrIdXSetProds,Map<String,BI_TM_Territory_to_Product_ND__c> mapTerrProdIdXTerrToProd,List<BI_TM_Territory__c> lstPos)
    {
        List<BI_TM_Territory_to_product__c> lstPosToProd=new List<BI_TM_Territory_to_product__c>();
        for(BI_TM_Territory__c objPos:lstPos)
        {
            Set<String> setProdIds=mapTerrIdXSetProds.get(objPos.BI_TM_Territory_ND__c);
            for(String strProdId:setProdIds)
            {
                String strTerrIdPrdId=(String)objPos.BI_TM_Territory_ND__c+strProdId;
                BI_TM_Territory_to_product__c objPosToProd= new BI_TM_Territory_to_product__c();
                objPosToProd.BI_TM_Business__c=objPos.BI_TM_Business__c;
                objPosToProd.BI_TM_Country_Code__c=objPos.BI_TM_Country_Code__c;
                objPosToProd.BI_TM_Territory_id__c=objPos.BI_TM_Territory_ND__c;
                objPosToProd.BI_TM_Mirror_Product__c=strProdId;
        if(mapTerrProdIdXTerrToProd.containsKey(strTerrIdPrdId))
                {
                    BI_TM_Territory_to_Product_ND__c objTerrToProd=mapTerrProdIdXTerrToProd.get(strTerrIdPrdId);
                    objPosToProd.BI_TM_Parent_Territory_to_Product__c=objTerrToProd.id;
                    objPosToProd.BI_TM_End_Date__c=objTerrToProd.BI_TM_End_Date__c;
                    objPosToProd.BI_TM_Start_date__c=objTerrToProd.BI_TM_Start_Date__c;
                }
                lstPosToProd.add(objPosToProd);
            }
        }
        return lstPosToProd;
    }
    global void finish(Database.BatchableContext BC)
    {
        database.executebatch(new BI_TM_User2Product_Handler_Batch(),20);
    }
}