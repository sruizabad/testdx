@isTest
private class BI_TM_Territory2Product_Handler_Test {
	@isTest
	private static void checkParentAttributes() {
		Date startDate = Date.newInstance(2016, 1, 1);
    Date endDate = Date.newInstance(2017, 12, 31);

		BI_TM_FF_type__c fieldForce = BI_TM_UtilClassDataFactory_Test.createFieldForce('FF-Test', 'US', 'PM');
		insert fieldForce;
		BI_TM_Position_Type__c posType = BI_TM_UtilClassDataFactory_Test.createPosType('US-PM-PT-1', 'US', 'PM');
		insert posType;
		BI_TM_Territory__c pos = BI_TM_UtilClassDataFactory_Test.createPosition('US', 'US', 'PM', null, startDate, endDate, true, true, null, null, 'SALES', posType.Id, 'Country');
		insert pos;

		List<BI_TM_Territory_ND__c> terrList = new List<BI_TM_Territory_ND__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory_ND__c terr = BI_TM_UtilClassDataFactory_Test.createTerritory('US-PM-Terr' + Integer.ValueOf(i), 'US', 'PM', null, fieldForce.Id, startDate, endDate, true);
			terrList.add(terr);
		}
		insert terrList;

		List<BI_TM_Territory__c> posList = new List<BI_TM_Territory__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory__c p = BI_TM_UtilClassDataFactory_Test.createPosition('US-PM-Pos' + Integer.ValueOf(i), 'US', 'PM', null, startDate, endDate, true, true, pos.Id, null, 'SALES', posType.Id, 'Representative');
			posList.add(p);
		}
		insert posList;

		List<Product_vod__c> prodList = new List<Product_vod__c>();
		for(Integer i = 0; i < 10; i++){
			Product_vod__c pr = BI_TM_UtilClassDataFactory_Test.createProduct('US Prod ' + Integer.ValueOf(i) , 'US', 'Detail', 'US P0' + Integer.ValueOf(i));
			prodList.add(pr);
		}
		insert prodList;

		List<BI_TM_Mirror_Product__c> mProdList = new List<BI_TM_Mirror_Product__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_Mirror_Product__c mpr = BI_TM_UtilClassDataFactory_Test.createMirrorProduct('US Prod ' + Integer.ValueOf(i) , 'US', prodList[i].Id);
			mProdList.add(mpr);
		}
		insert mProdList;

		List<BI_TM_FF_Type_To_Product__c> ff2prodList = new List<BI_TM_FF_Type_To_Product__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_FF_Type_To_Product__c ff2p = BI_TM_UtilClassDataFactory_Test.createFieldForce2Product(fieldForce.Id, mProdList[i].Id, 'US', 'PM', false, startDate, endDate);
			ff2prodList.add(ff2p);
		}
		insert ff2prodList;

		List<BI_TM_Territory_to_Product_ND__c> terr2prodList = new List<BI_TM_Territory_to_Product_ND__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory_to_Product_ND__c t2p = BI_TM_UtilClassDataFactory_Test.createTerritory2Product(terrList[i].Id, mProdList[i].Id, 'US', 'PM', false, startDate, endDate, false, null);
			terr2prodList.add(t2p);
		}
		insert terr2prodList;
/*
		List<BI_TM_Territory_to_product__c> pos2prodList = new List<BI_TM_Territory_to_product__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_Territory_to_product__c p2p = BI_TM_UtilClassDataFactory_Test.createPosition2Product(posList[i].Id, mProdList[i].Id, 'US', 'PM', false, startDate, endDate, false, null, null);
			pos2prodList.add(p2p);
		}
		insert pos2prodList;

		List<BI_TM_User_mgmt__c> userManList = new List<BI_TM_User_mgmt__c>();
		for(Integer i = 0; i < 10; i++){
			BI_TM_User_mgmt__c um = BI_TM_UtilClassDataFactory_Test.createUserManagement('Smith' + Integer.ValueOf(i), 'John' + Integer.ValueOf(i), 'US', 'PM', 'JSMith' + Integer.ValueOf(i),
			'jsmith' + Integer.ValueOf(i) + '@test.bitman', 'John.Smith' + Integer.ValueOf(i) + startDate.format(), 'jsmith' + Integer.ValueOf(i) + '@test.bitman', 'Standard User', 'English',
			'America/New_York', 'English', 'U.S Dollar', 'General US & Europe(ISO-8859-1)', startDate, endDate);
			userManList.add(um);
		}
		insert userManList;
*/
		Test.startTest();
		ff2prodList[0].BI_TM_Start_Date__c = startDate + 10;
		ff2prodList[0].BI_TM_End_Date__c = endDate + 10;
		update ff2prodList[0];

		terr2prodList[0].BI_TM_Parent_Field_Force_To_Product__c = ff2prodList[0].Id;
		BI_TM_Territory2Product_Trigger_Handler.recursionCheck = false;
		update terr2prodList[0];

		BI_TM_Territory_to_Product_ND__c t2p1 = BI_TM_UtilClassDataFactory_Test.createTerritory2Product(terrList[1].Id, mProdList[1].Id, 'US', 'PM', true, endDate + 1, endDate + 365, false, null);
		BI_TM_Territory2Product_Trigger_Handler.recursionCheck = false;
		insert t2p1;

		BI_TM_Territory_to_Product_ND__c t2p2 = BI_TM_UtilClassDataFactory_Test.createTerritory2Product(terrList[1].Id, mProdList[2].Id, 'US', 'PM', true, endDate + 1, endDate + 365, false, null);
		BI_TM_Territory2Product_Trigger_Handler.recursionCheck = false;
		try {
			insert t2p2;
		} catch(Exception ex) {
			Boolean checkInsertRecord = ex.getMessage().contains('There are already records in the system with primary product and overlapped the dates') ? true : false;
			system.assertEquals(true, checkInsertRecord);
		}
/*
		List<BI_TM_Territory_to_Product_ND__c> terr2prodList2Test = new List<BI_TM_Territory_to_Product_ND__c>();
		BI_TM_Territory_to_Product_ND__c t2p3 = BI_TM_UtilClassDataFactory_Test.createTerritory2Product(terrList[3].Id, mProdList[3].Id, 'US', 'PM', false, endDate + 1, endDate + 365, false, null);
		BI_TM_Territory_to_Product_ND__c t2p4 = BI_TM_UtilClassDataFactory_Test.createTerritory2Product(terrList[3].Id, mProdList[3].Id, 'US', 'PM', false, endDate + 2, endDate + 366, false, null);
		terr2prodList2Test.add(t2p3);
		terr2prodList2Test.add(t2p4);
		BI_TM_Territory2Product_Trigger_Handler.recursionCheck = false;
		//insert terr2prodList2Test;
		Database.SaveResult[] t2pResult = Database.insert(terr2prodList2Test, false);
		for(Database.SaveResult sr : t2pResult){
			if(!sr.isSuccess()){
				for(Database.Error err : sr.getErrors()){
					Boolean checkInsertRecord = err.getMessage().contains('insert records with overlapped dates') ? true : false;
					system.assertEquals(true, checkInsertRecord);
				}
			}
		}
*/
		List<BI_TM_Territory_to_Product_ND__c> terr2prodList2Test2 = new List<BI_TM_Territory_to_Product_ND__c>();
		BI_TM_Territory_to_Product_ND__c t2p5 = BI_TM_UtilClassDataFactory_Test.createTerritory2Product(terrList[1].Id, mProdList[3].Id, 'US', 'PM', true, endDate + 1, endDate + 365, false, null);
		BI_TM_Territory_to_Product_ND__c t2p6 = BI_TM_UtilClassDataFactory_Test.createTerritory2Product(terrList[1].Id, mProdList[4].Id, 'US', 'PM', true, endDate + 1, endDate + 365, false, null);
		terr2prodList2Test2.add(t2p5);
		terr2prodList2Test2.add(t2p6);
		BI_TM_Territory2Product_Trigger_Handler.recursionCheck = false;
		Database.SaveResult[] t2pResult2 = Database.insert(terr2prodList2Test2, false);
		for(Database.SaveResult sr : t2pResult2){
			system.debug('SR :: ' + sr);
			if(!sr.isSuccess()){
				for(Database.Error err : sr.getErrors()){
					system.debug(LoggingLevel.ERROR, 'Error :: ' + err.getMessage());
					Boolean checkInsertRecord = err.getMessage().contains('overlapped') ? true : false;
					system.assertEquals(true, checkInsertRecord);
				}
			}
		}

		Test.stopTest();

		system.assertEquals(ff2prodList[0].BI_TM_Start_Date__c, [Select BI_TM_Start_Date__c FROM BI_TM_Territory_to_Product_ND__c WHERE Id = :terr2prodList[0].Id].BI_TM_Start_Date__c);
		system.assertEquals(ff2prodList[0].BI_TM_End_Date__c, [Select BI_TM_End_Date__c FROM BI_TM_Territory_to_Product_ND__c WHERE Id = :terr2prodList[0].Id].BI_TM_End_Date__c);
		system.assertNotEquals(null, [Select Id FROM BI_TM_Territory_to_Product_ND__c WHERE Id = :t2p1.Id].Id);
	}

}