/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Territory to product trigger handler.
*  @author:         Antonio Ferrero
*  @created:        01-Jun-2017
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         01-Jun-2017                 Antonio Ferrero             Construction of the class.
*/

public with sharing class BI_TM_Territory2Product_Trigger_Handler implements BI_TM_ITriggerHandler {

	public static boolean recursionCheck=false;

	public void BeforeInsert(List<SObject> newItems) {}

	public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
		if(!recursionCheck){
			consolidateParentAttributes(newItems, oldItems);
		}
	}

	public void BeforeDelete(Map<Id, SObject> oldItems) {}

	public void AfterInsert(Map<Id, SObject> newItems) {
		if(!recursionCheck){
			checkOverlapDates(newItems);
			checkPrimaryProduct(newItems);
			recursionCheck = true;
		}
	}

	public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
		if(!recursionCheck){
			checkOverlapDates(newItems);
			checkPrimaryProduct(newItems);
			recursionCheck = true;
		}
	}

	public void AfterDelete(Map<Id, SObject> oldItems) {}

	public void AfterUndelete(Map<Id, SObject> oldItems) {}

	// Function to consolidate the attributes in the child position records when the override flag is unchecked
	public void consolidateParentAttributes(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
		// Sets with the parent field force to product and territory to products
		Set<Id> parentFF2prodSet = new Set<Id>();
		Set<Id> terr2prodSet = new Set<Id>();

		// The override flag has been changed to false, so the attributes from the parent need to be consolidated to the territory to product record
		for(Id t2p : newItems.keySet()){
			if(((BI_TM_Territory_to_Product_ND__c)newItems.get(t2p)).BI_TM_Override_Parent_FF_to_Product__c == FALSE
			&& ((((BI_TM_Territory_to_Product_ND__c)newItems.get(t2p)).BI_TM_Parent_Field_Force_To_Product__c != null) || (((BI_TM_Territory_to_Product_ND__c)newItems.get(t2p)).BI_TM_Parent_Field_Force_To_Product__c != ''))){

				terr2prodSet.add(t2p);

				// Build the sets with the parents
				Id parentFF2Prod = ((BI_TM_Territory_to_Product_ND__c)newItems.get(t2p)).BI_TM_Parent_Field_Force_To_Product__c;
				if(parentFF2Prod != null || parentFF2Prod != ''){
					parentFF2prodSet.add(parentFF2Prod);
				}
			}
		}

		Map<Id, BI_TM_FF_Type_To_Product__c> ff2prodMap = new Map<Id, BI_TM_FF_Type_To_Product__c>([SELECT Id, BI_TM_End_Date__c, BI_TM_Primary_Product__c, BI_TM_Start_Date__c
		FROM BI_TM_FF_Type_To_Product__c
		WHERE Id IN :parentFF2prodSet]);

		// For the territory to products, check if attributes are different from the parents´ attributes
		for(Id t2p : terr2prodSet){
			BI_TM_Territory_to_Product_ND__c terr2prod = (BI_TM_Territory_to_Product_ND__c) newItems.get(t2p);
			if(terr2prod.BI_TM_Parent_Field_Force_To_Product__c != null || terr2prod.BI_TM_Parent_Field_Force_To_Product__c != ''){
				Id parentFF2prod = terr2prod.BI_TM_Parent_Field_Force_To_Product__c;

				if(ff2prodMap.get(parentFF2prod) != null){
					if(terr2prod.BI_TM_Start_Date__c != ff2prodMap.get(parentFF2prod).BI_TM_Start_Date__c){
						terr2prod.BI_TM_Start_Date__c = ff2prodMap.get(parentFF2prod).BI_TM_Start_Date__c;
					}
					if(terr2prod.BI_TM_End_Date__c != ff2prodMap.get(parentFF2prod).BI_TM_End_Date__c){
						terr2prod.BI_TM_End_Date__c = ff2prodMap.get(parentFF2prod).BI_TM_End_Date__c;
					}
				}
			}
		}
	}

	// Function to check the overlap dates
	public void checkOverlapDates(Map<Id, SObject> newItems){
		// First we have to check overlaps in the incoming records
		Map<String, Set<Id>> keyMapNewItems = new Map<String, Set<Id>>();

		for(Id t2p : newItems.keySet()){
			//system.debug('Key :: ' + ((BI_TM_Territory_to_Product_ND__c)newItems.get(t2p)).BI_TM_Key_Terr2Prod__c);
			String key = ((BI_TM_Territory_to_Product_ND__c)newItems.get(t2p)).BI_TM_Key_Terr2Prod__c;
			if(keyMapNewItems.get(key) != null){
				Set<Id> idSet = keyMapNewItems.get(key);
				idSet.add(t2p);
				keyMapNewItems.put(key, idSet);
			}
			else{
				Set<Id> idSet = new Set<Id>();
				idSet.add(t2p);
				keyMapNewItems.put(key, idSet);
			}
		}

		// Clone the map with the keys to remove the ids that are overlapped
		Map<String, Set<Id>> keyMapNewItemsNoOverlap = new Map<String, Set<Id>>(keyMapNewItems);
		Set<Id> keySetOverlap = new Set<Id>();

		// Check the overlap in the cncoming records
		if(keyMapNewItems != null){
			for(String key : keyMapNewItems.keySet()){
				if(keyMapNewItems.get(key) != null && keyMapNewItems.get(key).size() > 1){ // There is more that one incoming record with same key
					Set<Id> idSet = keyMapNewItems.get(key);
					Set<Id> idSetCopy = new Set<Id>(idSet);

					// Check overlap dates with records with same key
					for(Id id1 : idSet){
						//system.debug('t2p1 new items:: ' + id1);
						BI_TM_Territory_to_Product_ND__c t2p1 = (BI_TM_Territory_to_Product_ND__c)newItems.get(id1);
						for(Id id2 : idSetCopy){
							if(id1 != id2){
								BI_TM_Territory_to_Product_ND__c t2p2 = ((BI_TM_Territory_to_Product_ND__c)newItems.get(id2));
								if((((t2p1.BI_TM_Start_Date__c >= t2p2.BI_TM_Start_Date__c) && (t2p2.BI_TM_End_Date__c != null ? t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_End_Date__c : true)) ||
											((t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_Start_Date__c) && (t2p1.BI_TM_End_Date__c != null ? t2p1.BI_TM_End_Date__c >= t2p2.BI_TM_Start_Date__c : true) ||
												((t2p1.BI_TM_End_Date__c != null && t2p2.BI_TM_End_Date__c != null) ?
													((t2p1.BI_TM_End_Date__c <= t2p2.BI_TM_End_Date__c) ? t2p1.BI_TM_End_Date__c >= t2p2.BI_TM_Start_Date__c : t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_End_Date__c) : false)))){
									t2p1.addError('You are trying to insert records with overlapped dates - Record 1: ' + t2p1.Id + ' : SD - ' + t2p1.BI_TM_Start_Date__c + ' : ED - ' + t2p1.BI_TM_End_Date__c + ', Record 2: ' + t2p2.Id + ' : SD - ' + t2p2.BI_TM_Start_Date__c + ' : ED - ' + t2p2.BI_TM_End_Date__c);
									Set<Id> keySetOverlap2remove = keyMapNewItemsNoOverlap.get(key);
									if(keySetOverlap2remove.contains(t2p1.Id)){
										keySetOverlap2remove.remove(t2p1.Id);
										keyMapNewItemsNoOverlap.put(key, keySetOverlap2remove);
									}
								}
							}
						}
					}
				}
			}
		}

		//system.debug('Map no overlap records :: ' + keyMapNewItemsNoOverlap);

		// Check overlap dates with the records in the system
		if(keyMapNewItemsNoOverlap != null){
			Map<Id, BI_TM_Territory_to_Product_ND__c> terr2ProdMapSystem = new Map<Id, BI_TM_Territory_to_Product_ND__c>([SELECT Id, BI_TM_Key_Terr2Prod__c, BI_TM_Start_Date__c, BI_TM_End_Date__c, BI_TM_Territory_ND__c, BI_TM_Mirror_Product__c FROM BI_TM_Territory_to_Product_ND__c WHERE Id NOT IN :newItems.keySet() AND BI_TM_Key_Terr2Prod__c IN :keyMapNewItemsNoOverlap.keySet()]);
			Map<String, Set<Id>> keyMapSystemItems = new Map<String, Set<Id>>();

			for(Id t2p : terr2ProdMapSystem.keySet()){
				//system.debug('Key :: ' + (terr2ProdMapSystem.get(t2p)).BI_TM_Key_Terr2Prod__c);
				String key = (terr2ProdMapSystem.get(t2p)).BI_TM_Key_Terr2Prod__c;
				if(keyMapSystemItems.get(key) != null){
					Set<Id> idSet = keyMapSystemItems.get(key);
					idSet.add(t2p);
					keyMapSystemItems.put(key, idSet);
				}
				else{
					Set<Id> idSet = new Set<Id>();
					idSet.add(t2p);
					keyMapSystemItems.put(key, idSet);
				}
			}

			for(String key : keyMapNewItemsNoOverlap.keySet()){
				if(keyMapNewItemsNoOverlap.get(key) != null && keyMapSystemItems.get(key) != null){
					Set<Id> idSet = keyMapNewItemsNoOverlap.get(key);
					Set<Id> idSetSystem = keyMapSystemItems.get(key);

					// Check overlap dates with records with same key in the system
					for(Id id1 : idSet){
						BI_TM_Territory_to_Product_ND__c t2p1 = (BI_TM_Territory_to_Product_ND__c)newItems.get(id1);
						system.debug('t2p1 new items:: ' + id1);
						for(Id id2 : idSetSystem){
							if(id1 != id2){
								BI_TM_Territory_to_Product_ND__c t2p2 = terr2ProdMapSystem.get(id2);
								system.debug('t2p2 system items:: ' + id2);
								if((((t2p1.BI_TM_Start_Date__c >= t2p2.BI_TM_Start_Date__c) && (t2p2.BI_TM_End_Date__c != null ? t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_End_Date__c : true)) ||
											((t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_Start_Date__c) && (t2p1.BI_TM_End_Date__c != null ? t2p1.BI_TM_End_Date__c >= t2p2.BI_TM_Start_Date__c : true) ||
												((t2p1.BI_TM_End_Date__c != null && t2p2.BI_TM_End_Date__c != null) ?
													((t2p1.BI_TM_End_Date__c <= t2p2.BI_TM_End_Date__c) ? t2p1.BI_TM_End_Date__c >= t2p2.BI_TM_Start_Date__c : t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_End_Date__c) : false)))){
									t2p1.addError('There are already records in the system that overlapped the dates - Record New: ' + t2p1.Id + ' : SD - ' + t2p1.BI_TM_Start_Date__c + ' : ED - ' + t2p1.BI_TM_End_Date__c + ', Existing record: ' + t2p2.Id + ' : SD - ' + t2p2.BI_TM_Start_Date__c + ' : ED - ' + t2p2.BI_TM_End_Date__c);

								}
							}
						}
					}
				}
			}
		}
	}

	// Function to check that there is only one primary product per territory in the same period of time
	public void checkPrimaryProduct(Map<Id, SObject> newItems){

		// Build a map with the territory and the ids of the records with primary flag as true
		Map<Id, Set<Id>> inTerr2ProdMap = new Map<Id, Set<Id>>();
		for(Id t2p : newItems.keySet()){
			Id terr = ((BI_TM_Territory_to_Product_ND__c)newItems.get(t2p)).BI_TM_Territory_ND__c;
			if(((BI_TM_Territory_to_Product_ND__c)newItems.get(t2p)).BI_TM_Primary_Product__c == TRUE){
				if(inTerr2prodMap.get(terr) != null){
					Set<Id> t2prodSet = inTerr2prodMap.get(terr);
					t2prodSet.add(t2p);
					inTerr2prodMap.put(terr, t2prodSet);
				}
				else{
					Set<Id> t2prodSet = new Set<Id>();
					t2prodSet.add(t2p);
					inTerr2prodMap.put(terr, t2prodSet);
				}
			}
		}

		Map<Id, Set<Id>> keyMapNewItemsNoOverlap = new Map<Id, Set<Id>>(inTerr2prodMap);
		Set<Id> keySetOverlap = new Set<Id>();

		// Check if there is more than one product set as primary per territory
		for(Id terr : inTerr2prodMap.keySet()){
			if(inTerr2prodMap.get(terr) != null && inTerr2prodMap.get(terr).size() > 1){
				Set<Id> t2prodSet = inTerr2prodMap.get(terr);

				// For each record, check if there is more records for the same dates
				for(Id id1 : t2prodSet){

					BI_TM_Territory_to_Product_ND__c t2p1 = (BI_TM_Territory_to_Product_ND__c)newItems.get(id1);
					for(Id id2 : t2prodSet){
						if(id1 != id2){
							BI_TM_Territory_to_Product_ND__c t2p2 = ((BI_TM_Territory_to_Product_ND__c)newItems.get(id2));
							if((((t2p1.BI_TM_Start_Date__c >= t2p2.BI_TM_Start_Date__c) && (t2p2.BI_TM_End_Date__c != null ? t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_End_Date__c : true)) ||
										((t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_Start_Date__c) && (t2p1.BI_TM_End_Date__c != null ? t2p1.BI_TM_End_Date__c >= t2p2.BI_TM_Start_Date__c : true) ||
											((t2p1.BI_TM_End_Date__c != null && t2p2.BI_TM_End_Date__c != null) ?
												((t2p1.BI_TM_End_Date__c <= t2p2.BI_TM_End_Date__c) ? t2p1.BI_TM_End_Date__c >= t2p2.BI_TM_Start_Date__c : t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_End_Date__c) : false)))){
								t2p1.addError('You are trying to insert records with primary record and overlapped dates - Record 1: ' + t2p1.Id + ' : SD - ' + t2p1.BI_TM_Start_Date__c + ' : ED - ' + t2p1.BI_TM_End_Date__c + ', Record 2: ' + t2p2.Id + ' : SD - ' + t2p2.BI_TM_Start_Date__c + ' : ED - ' + t2p2.BI_TM_End_Date__c);
								// Remove ids that are already overlapped
								Set<Id> keySetOverlap2remove = keyMapNewItemsNoOverlap.get(terr);
								if(keySetOverlap2remove.contains(t2p1.Id)){
									keySetOverlap2remove.remove(t2p1.Id);
									keyMapNewItemsNoOverlap.put(terr, keySetOverlap2remove);
								}
							}
						}
					}
				}
			}
		}

		system.debug('Map no overlap records :: ' + keyMapNewItemsNoOverlap);

		// Check overlap dates with the records in the system
		if(keyMapNewItemsNoOverlap != null){
			Map<Id, BI_TM_Territory_to_Product_ND__c> t2prodMapSystem = new Map<Id, BI_TM_Territory_to_Product_ND__c>([SELECT Id, BI_TM_Territory_ND__c, BI_TM_Start_Date__c, BI_TM_End_Date__c FROM BI_TM_Territory_to_Product_ND__c WHERE Id NOT IN :newItems.keySet() AND BI_TM_Primary_Product__c = TRUE AND BI_TM_Territory_ND__c IN :keyMapNewItemsNoOverlap.keySet()]);
			Map<Id, Set<Id>> keyMapSystemItems = new Map<Id, Set<Id>>();

			for(Id t2p : t2prodMapSystem.keySet()){
				//system.debug('Key :: ' + (t2prodMapSystem.get(t2p)).BI_TM_Territory_ND__c);
				String key = (t2prodMapSystem.get(t2p)).BI_TM_Territory_ND__c;
				if(keyMapSystemItems.get(key) != null){
					Set<Id> idSet = keyMapSystemItems.get(key);
					idSet.add(t2p);
					keyMapSystemItems.put(key, idSet);
				}
				else{
					Set<Id> idSet = new Set<Id>();
					idSet.add(t2p);
					keyMapSystemItems.put(key, idSet);
				}
			}

			for(Id terr : keyMapNewItemsNoOverlap.keySet()){
				if(keyMapNewItemsNoOverlap.get(terr) != null && keyMapSystemItems.get(terr) != null){
					Set<Id> idSet = keyMapNewItemsNoOverlap.get(terr);
					Set<Id> idSetSystem = keyMapSystemItems.get(terr);

					// Check overlap dates with records with same key in the system
					for(Id id1 : idSet){
						BI_TM_Territory_to_Product_ND__c t2p1 = (BI_TM_Territory_to_Product_ND__c)newItems.get(id1);
						//SaveResultsystem.debug('t2p1 new items:: ' + id1);
						for(Id id2 : idSetSystem){
							if(id1 != id2){
								BI_TM_Territory_to_Product_ND__c t2p2 = t2prodMapSystem.get(id2);
								//system.debug('t2p2 system items:: ' + id1);
								if((((t2p1.BI_TM_Start_Date__c >= t2p2.BI_TM_Start_Date__c) && (t2p2.BI_TM_End_Date__c != null ? t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_End_Date__c : true)) ||
											((t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_Start_Date__c) && (t2p1.BI_TM_End_Date__c != null ? t2p1.BI_TM_End_Date__c >= t2p2.BI_TM_Start_Date__c : true) ||
												((t2p1.BI_TM_End_Date__c != null && t2p2.BI_TM_End_Date__c != null) ?
													((t2p1.BI_TM_End_Date__c <= t2p2.BI_TM_End_Date__c) ? t2p1.BI_TM_End_Date__c >= t2p2.BI_TM_Start_Date__c : t2p1.BI_TM_Start_Date__c <= t2p2.BI_TM_End_Date__c) : false)))){
									t2p1.addError('There are already records in the system with primary product and overlapped the dates - Record New: ' + t2p1.Id + ' : SD - ' + t2p1.BI_TM_Start_Date__c + ' : ED - ' + t2p1.BI_TM_End_Date__c + ', Existing record: ' + t2p2.Id + ' : SD - ' + t2p2.BI_TM_Start_Date__c + ' : ED - ' + t2p2.BI_TM_End_Date__c);

								}
							}
						}
					}
				}
			}
		}
	}
}