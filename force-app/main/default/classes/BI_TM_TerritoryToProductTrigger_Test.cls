@isTest
public class BI_TM_TerritoryToProductTrigger_Test {

    static testMethod void testTerritoryProdcutInsert(){

        System.Test.startTest();
        BI_TM_Mirror_Product__c mp=new BI_TM_Mirror_Product__c();
         BI_TM_Mirror_Product__c mp1=new BI_TM_Mirror_Product__c();
         BI_TM_Territory__c Terr= new BI_TM_Territory__c();
         BI_TM_Territory__c Terr1= new BI_TM_Territory__c();
          BI_TM_FF_type__c FF= new BI_TM_FF_type__c();
          BI_TM_Position_Type__c pt = new BI_TM_Position_Type__c();
          RecordType  rtype= new RecordType();
          RecordType  rtype1= new RecordType();
          User currentuser = new User();
          currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
          FF.Name='Test MX FF';
          FF.BI_TM_Business__c='AH';
          FF.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
          insert FF;
          pt.Name='Test MX FF';
          pt.BI_TM_Business__c='AH';
          pt.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
          insert pt;
          Terr1.Name='FFHierarchy test213';
          Terr1.BI_TM_FF_type__c=FF.Id;
          Terr1.BI_TM_Is_Root__c=True;
          Terr1.BI_TM_Position_Level__c='Country';
          Terr1.BI_TM_Position_Type_Lookup__c=pt.Id;

          Terr1.BI_TM_Visible_in_crm__c=True;
          Terr1.BI_TM_Global_Position_Type__c = 'SALES';

          Terr1.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
          Terr1.BI_TM_Start_date__c=Date.today().addDays(-1);
     // Terr1.BI_TM_End_date__c=system.Today();
      //Terr1.BI_TM_Is_Active_Checkbox__c=false;

          Terr1.BI_TM_Business__c='AH';
      Insert Terr1;
      //rtype1=[select Id from RecordType where Name='Territory'Limit 1];
      Terr.Name='Test Tam213';
      Terr.BI_TM_FF_type__c=FF.Id;
      Terr.BI_TM_Business__c='AH';
      Terr.BI_TM_Position_Level__c='Country';
      Terr.BI_TM_Position_Type_Lookup__c=pt.Id;
      Terr.BI_TM_Parent_Position__c=Terr1.Id;
      Terr.BI_TM_Visible_in_crm__c=True;
      //Terr.BI_TM_Is_Active_Checkbox__c=True;
      Terr.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
      Terr.BI_TM_Start_date__c=Date.today().addDays(-1);
      Terr.BI_TM_End_date__c=system.Today();
      Terr1.BI_TM_Position_Level__c='Representative';
      Terr.BI_TM_Global_Position_Type__c = 'SALES';
      //Terr.BI_TM_Is_Active_Checkbox__c=false;

          Insert Terr;
        BI_TM_Territory_to_product__c terrProduct = new BI_TM_Territory_to_product__c();
        Product_vod__c prod = new Product_vod__c();
        prod.name = 'Metacam';
        prod.Country_Code_BI__c = 'MX';
        prod.Product_Type_vod__c = 'Detail';
        insert prod;
         mp.Name='Metacam';
          mp.BI_TM_Product_Catalog_Id__c=prod.Id;
          mp.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c;
         // mp.BI_TM_Active__c=True;
          //mp.BI_TM_Product_Type__c='Detail';
          Insert mp;
         mp1.Name='Metacam2';
          mp1.BI_TM_Product_Catalog_Id__c=prod.Id;
          mp1.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c;
          //mp1.BI_TM_Active__c=True;
          //mp1.BI_TM_Product_Type__c='Detail';
          Insert mp1;
           terrProduct.BI_TM_Mirror_Product__c =mp.Id;
        //terrProduct.BI_TM_Product_id__c = prod.id;
        terrProduct.BI_TM_Territory_id__c= Terr.id;
        terrProduct.BI_TM_Is_Primary_Product__c = true;
        terrProduct.BI_TM_Business__c = 'AH';
        terrProduct.BI_TM_Start_date__c = system.today();
        terrProduct.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        insert terrProduct;
        BI_TM_Territory_to_product__c terrProduct1 = new BI_TM_Territory_to_product__c();
        terrProduct1.BI_TM_Mirror_Product__c =mp1.Id;
        //terrProduct.BI_TM_Product_id__c = prod.id;
        terrProduct1.BI_TM_Territory_id__c= Terr1.id;
        terrProduct1.BI_TM_Is_Primary_Product__c = false;
        terrProduct1.BI_TM_Business__c = 'AH';
        terrProduct1.BI_TM_Start_date__c = system.today();
        terrProduct1.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
        insert terrProduct1;
        terrProduct.BI_TM_Is_Primary_Product__c = false;
        update terrProduct;
        Map<Id, SObject> oldItems= new Map<Id, SObject>();
        /*
        BI_TM_Territory_to_product__c terrProduct1 = new BI_TM_Territory_to_product__c();
        Product_vod__c prod1 = new Product_vod__c();
        prod1.name = 'OsteoCox';
        prod1.Country_Code_BI__c = 'MX';
        prod1.Product_Type_vod__c = 'Detail';
        insert prod1;

        terrProduct1.BI_TM_Product_id__c = prod1.id;
        terrProduct1.BI_TM_Mirror_Territory__c = mmTerritory.id;
        terrProduct1.BI_TM_Is_Primary_Product__c = true;
        insert terrProduct1;
        */
        BI_TM_Territory_To_Product_Handler handler = new BI_TM_Territory_To_Product_Handler ();

        handler.BeforeDelete(oldItems);
        handler.AfterDelete(oldItems);
        handler.AfterUndelete(oldItems);

        System.Test.stopTest();
    }

}