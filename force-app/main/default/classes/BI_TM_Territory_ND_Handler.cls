public class BI_TM_Territory_ND_Handler implements BI_TM_ITriggerHandler{
  public void BeforeInsert(List<SObject> newItems) {
    //BI_TM_Territory_Position_Logic.territoryCheckUniqueNames(newItems);
  }

  public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

  public void BeforeDelete(Map<Id, SObject> oldItems) {}

  public void AfterInsert(Map<Id, SObject> newItems) {
    BI_TM_Manage_Field_Force_To_Territory manageFFTerritory = new BI_TM_Manage_Field_Force_To_Territory();
    manageFFTerritory.createFieldForceToTerritory(newItems);
  }

  public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    BI_TM_Manage_Field_Force_To_Territory manageFFTerritory = new BI_TM_Manage_Field_Force_To_Territory();
    manageFFTerritory.updateFieldForceToTerritory(newItems, oldItems);
  }

  public void AfterDelete(Map<Id, SObject> oldItems) {}

  public void AfterUndelete(Map<Id, SObject> oldItems) {}

}