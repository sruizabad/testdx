public class BI_TM_Territory_NewHandler implements BI_TM_ITriggerHandler
{
    public static boolean IsInsert = false;

    public void BeforeInsert(List<SObject> newItems) {
      // Check the permission sets and profile
      BI_TM_Position_Helper.checkPermissionProfileSettings(newItems);
    }

    public void BeforeUpdate(Map<Id,SObject> newItems, Map<Id, SObject> oldItems) {
      BI_TM_Position_HeadCount_Calculation PosHeadCount= new BI_TM_Position_HeadCount_Calculation();
      PosHeadCount.BeforeUpdateposition(newItems,olditems);

      // Check the permission sets and profile
      BI_TM_Position_Helper.checkPermissionProfileSettings(newItems.values());

    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
        IsInsert = true;
        insertRoles((Set<Id>) newItems.keySet());

        // Call function to create the Field Force to Position record
        BI_TM_Manage_FF_Terr_To_Position manageFFTerrPosition = new BI_TM_Manage_FF_Terr_To_Position();
        manageFFTerrPosition.createRelations(newItems);

        //HeadCount
        BI_TM_Position_HeadCount_Calculation PosHeadCount= new BI_TM_Position_HeadCount_Calculation();
        PosHeadCount.insertposition(newItems);

    }


    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        updateRoles((Set<Id>) newItems.keySet());

        // Call fucntion to update the field force to position and create the new one
        BI_TM_Manage_FF_Terr_To_Position manageFFTerrPosition = new BI_TM_Manage_FF_Terr_To_Position();
        manageFFTerrPosition.updateRelations(oldItems, newItems);

        BI_TM_Position_HeadCount_Calculation PosHeadCount= new BI_TM_Position_HeadCount_Calculation();
        PosHeadCount.AfterUpdateposition(newItems,olditems);
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}


    private void insertRoles(Set<Id> territoryIds) {
        List<BI_TM_Territory__c> territories = [SELECT Id, Name, BI_TM_Is_Root__c, BI_TM_Visible_in_crm__c, BI_TM_Country_Code__c, BI_TM_Business__c, BI_TM_Start_date__c, BI_TM_End_date__c, BI_TM_Parent_Position__r.BI_TM_ExternalIdTerrMgmt__c,BI_TM_Parent_Position__r.Id FROM BI_TM_Territory__c WHERE Id in :territoryIds];
        List<BI_TM_User_role__c> roles = new List<BI_TM_User_role__c>();

        for(BI_TM_Territory__c t : territories) {
            //System.debug('###t: ' + t);
            BI_TM_User_role__c r = new BI_TM_User_role__c();
            r.BI_TM_External_ID__c = t.Id;//t.BI_TM_Country_Code__c + '_' + t.Name;
            r.Name = t.Name;
            r.BI_TM_Country_Code__c = t.BI_TM_Country_Code__c;
            r.BI_TM_Business__c = t.BI_TM_Business__c;
            r.BI_TM_Territory__c = t.Id;
            r.BI_TM_Start_date__c = t.BI_TM_Start_date__c;
            r.BI_TM_End_date__c = t.BI_TM_End_date__c;
            r.BI_TM_Visible_In_CRM__c = t.BI_TM_Visible_In_CRM__c;
            String extId = t.BI_TM_Parent_Position__r.Id;
           if (!String.isEmpty(extId)) {
                r.BI_TM_Parent_User_Role__r = new BI_TM_User_role__c(BI_TM_External_ID__c= extId);
            }
            r.BI_TM_Is_Root__c = t.BI_TM_Is_Root__c;
            //System.debug('###r' + r);
            roles.add(r);
        }
        //System.debug('###roles: ' + roles);
        try{
           insert roles;
           }catch (Exception ex) {

           }
    }


    private void updateRoles(Set<Id> territoryIds) {
        List<BI_TM_Territory__c> territories = [SELECT Id, Name, BI_TM_Is_Root__c, BI_TM_Visible_in_crm__c, BI_TM_Country_Code__c, BI_TM_Business__c, BI_TM_Start_date__c, BI_TM_End_date__c,  BI_TM_Parent_Position__r.BI_TM_ExternalIdTerrMgmt__c,BI_TM_Parent_Position__c,BI_TM_Parent_Position__r.Name,BI_TM_Parent_Position__r.Id FROM BI_TM_Territory__c WHERE Id in :territoryIds];
        List<BI_TM_User_role__c> existingRolesList = [Select Id, BI_TM_Override_Position_Hierarchy__c, BI_TM_External_ID__c, BI_TM_Parent_User_Role__c, BI_TM_Parent_User_Role__r.BI_TM_External_ID__c FROM BI_TM_User_role__c WHERE BI_TM_External_ID__c IN :territoryIds];
        Map<String, BI_TM_User_role__c> userRoleMap = new Map<String, BI_TM_User_role__c>();
        for(BI_TM_User_role__c ur : existingRolesList) {
          userRoleMap.put(ur.BI_TM_External_ID__c, ur);
        }

        List<BI_TM_User_role__c> roles = new List<BI_TM_User_role__c>();

        Map<Id, BI_TM_Territory__c> positionsMap = new Map<Id, BI_TM_Territory__c>();
        for(BI_TM_Territory__c p : territories){
          positionsMap.put(p.Id, p);
        }

        for(BI_TM_Territory__c t : territories) {
            //System.debug('###t: ' + t);
            BI_TM_User_role__c r = new BI_TM_User_role__c();
            r.BI_TM_External_ID__c = t.Id;//t.BI_TM_Country_Code__c + '_' + t.Name;
            r.BI_TM_Country_Code__c = t.BI_TM_Country_Code__c;
            r.BI_TM_Business__c = t.BI_TM_Business__c;
            r.BI_TM_Start_date__c = t.BI_TM_Start_date__c;
            r.BI_TM_End_date__c = t.BI_TM_End_date__c;
            r.BI_TM_Visible_In_CRM__c = t.BI_TM_Visible_In_CRM__c;
            String extId = t.BI_TM_Parent_Position__r.Id;
            // Check if the role has to override the position hierarchy, so the parent will not be updated
            if((userRoleMap.get(r.BI_TM_External_ID__c)) != null){
                if (!String.isEmpty(extId) && !((userRoleMap.get(r.BI_TM_External_ID__c)).BI_TM_Override_Position_Hierarchy__c)) {
                r.BI_TM_Parent_User_Role__r = new BI_TM_User_role__c(BI_TM_External_ID__c= extId);
                }
            }
            else{
                r.Name = t.Name;
                if (!String.isEmpty(extId)) {
                r.BI_TM_Parent_User_Role__r = new BI_TM_User_role__c(BI_TM_External_ID__c= extId);
                }
            }

            r.BI_TM_Is_Root__c = t.BI_TM_Is_Root__c;
            //System.debug('###r' + r);
            roles.add(r);
        }

        System.debug('###roles: ' + roles);
         try{
                upsert roles BI_TM_External_ID__c;
              }catch (Exception ex) {
                system.debug('An exception has ocrred :: ' + ex.getMessage());
                system.debug('An exception has ocrred in the line :: ' + ex.getLineNumber());
           }

    }


}