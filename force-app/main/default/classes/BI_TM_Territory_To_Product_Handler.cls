public class BI_TM_Territory_To_Product_Handler implements BI_TM_ITriggerHandler
{
	private static boolean recursionCheck=false;

	public void BeforeInsert(List<SObject> newItems) {
		Set<Id> territories = new Set<Id>();
		Set<Id> products=new Set<id>();

		for(BI_TM_Territory_to_product__c terrProduct : (List<BI_TM_Territory_to_product__c>) newItems) {
			territories.add(terrProduct.BI_TM_Territory_id__c);
			products.add(terrProduct.BI_TM_Mirror_Product__c);
		}

		System.Debug('territories *****' + territories);
		List<AggregateResult> queryResults = Database.query('SELECT COUNT(Id), BI_TM_Territory_id__c FROM BI_TM_Territory_to_product__c WHERE BI_TM_Is_Primary_Product__c = true AND BI_TM_Territory_id__c IN :territories  GROUP BY BI_TM_Territory_id__c');

		System.Debug('queryResults ***' + queryResults);

		System.Debug('Insert trigger***');
		//only one can be flagged as primary product
		for(BI_TM_Territory_to_product__c terrProduct : (List<BI_TM_Territory_to_product__c>) newItems) {
			for(AggregateResult ar: queryResults) {
				if (terrProduct.BI_TM_Territory_id__c == String.valueOf(ar.get('BI_TM_Territory_id__c'))
						&& Integer.valueOf(ar.get('expr0')) > 0
						&& terrProduct.BI_TM_Is_Primary_Product__c == true) {

					terrProduct.addError('Already one product flagged as primary product for Position. More than one product cannot be flagged as primary product.');
					break;
				}
			}
		}
	}

	public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

		// Consolidate parent attributes. Update start date, end date, primary product if the override rules flag is not checked off and they are different from the parent record
		consolidateParentAttributes(newItems, oldItems);
		Set<Id> territories = new Set<Id>();

		for(BI_TM_Territory_to_product__c terrProduct : (List<BI_TM_Territory_to_product__c>) newItems.values()) {
			territories.add(terrProduct.BI_TM_Territory_id__c);
		}

		System.Debug('territories *****' + territories);
		List<AggregateResult> queryResults = Database.query('SELECT COUNT(Id), BI_TM_Territory_id__c FROM BI_TM_Territory_to_product__c WHERE BI_TM_Is_Primary_Product__c = true AND BI_TM_Territory_id__c IN :territories  GROUP BY BI_TM_Territory_id__c');

		System.Debug('queryResults ***' + queryResults);
		//only one can be flagged as primary product
		for(BI_TM_Territory_to_product__c terrProduct : (List<BI_TM_Territory_to_product__c>)newItems.values()) {
			// Access the "old" record by its ID in Trigger.oldMap
			BI_TM_Territory_to_product__c oldTerrProduct= (BI_TM_Territory_to_product__c)oldItems.get(terrProduct.Id);
			System.Debug('Update trigger***oldTerrProduct***' + oldTerrProduct);

			for(AggregateResult ar: queryResults) {
				Integer noOfPrimaryProducts = 0;
				if (terrProduct.BI_TM_Territory_id__c == String.valueOf(ar.get('BI_TM_Territory_id__c'))
						&& Integer.valueOf(ar.get('expr0')) > 0
						&& terrProduct.BI_TM_Is_Primary_Product__c != oldTerrProduct.BI_TM_Is_Primary_Product__c) {

					if (terrProduct.BI_TM_Is_Primary_Product__c == true) {
						noOfPrimaryProducts = Integer.valueOf(ar.get('expr0')) + 1;
					}
					else {
						noOfPrimaryProducts = Integer.valueOf(ar.get('expr0')) - 1;
					}
					if ( noOfPrimaryProducts > 0) {
						terrProduct.addError('Already one product flagged as primary product for Position. More than one product cannot be flagged as primary product.');
						break;
					}
				}
			}
		}

	}

	public void BeforeDelete(Map<Id, SObject> oldItems) {}

	public void AfterInsert(Map<Id, SObject> newItems) {
		// BI_TM_Create_Child_Products_Handler handler = new BI_TM_Create_Child_Products_Handler();
		if(!recursionCheck){
			checkOverlapDates(newItems);
			//checkPrimaryProduct(newItems);
			recursionCheck = true;
		}
		if(BI_TM_RecursionHandler.isInsertTrigger) {
			BI_TM_RecursionHandler.isInsertTrigger = false;
			//handler.CreateChildTerritoryProducts((Set<Id>)newItems.keySet());

		}
		// Below lines are for Apex Sharing for List Views
		//BI_TM_ApexSharingHandler terrtoprodshare = new BI_TM_ApexSharingHandler();
		//terrtoprodshare.TerritorytoProductSharing((List<BI_TM_Territory_to_product__c>)newItems.values());
	}

	public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
		if(!recursionCheck){
			checkOverlapDates(newItems);
			//checkPrimaryProduct(newItems);
			recursionCheck = true;
		}

	}

	public void AfterDelete(Map<Id, SObject> oldItems) {}

	public void AfterUndelete(Map<Id, SObject> oldItems) {}


	/*Method to check for date overlap
		Validation to check to make sure there are no duplicate records for the same dates.
		*/
	public void CheckDataOverlap (List<BI_TM_Territory_to_product__c> newItems){

		Set<Id> territories = new Set<Id>();
		Set<Id> products=new Set<id>();

		for(BI_TM_Territory_to_product__c terrProduct : (List<BI_TM_Territory_to_product__c>) newItems) {
			territories.add(terrProduct.BI_TM_Territory_id__c);
			products.add(terrProduct.BI_TM_Mirror_Product__c);
		}

		//Query records from database
		List<BI_TM_Territory_to_product__c> terr2ProdList=new List<BI_TM_Territory_to_product__c>([SELECT ID, BI_TM_Territory_id__c, BI_TM_Mirror_Product__c, BI_TM_Start_date__c, BI_TM_End_date__c, Name FROM BI_TM_Territory_to_product__c where
		BI_TM_Territory_id__c IN :territories AND BI_TM_Mirror_Product__c IN :products]);
		system.debug(terr2ProdList);

		for(BI_TM_Territory_to_product__c terr2ProdOuterVar:(List<BI_TM_Territory_to_product__c>)newItems){
			Boolean duplicateExistInSystem=false;
			Boolean duplicateExistInFile=false;
			String errorMsg1;//Error message if duplicate already exist in salesforce.
			String errorMsg2;//Error message if duplicate exist within system.
			if(terr2ProdOuterVar.BI_TM_End_date__c<terr2ProdOuterVar.BI_TM_Start_date__c)
			terr2ProdOuterVar.addError('End Date Should not be less than Start Date.');
			//Check with the records already in database
			for(BI_TM_Territory_to_product__c ter2ProdInnerVar:terr2ProdList){
				if(terr2ProdOuterVar.BI_TM_Territory_id__c== ter2ProdInnerVar.BI_TM_Territory_id__c && terr2ProdOuterVar.BI_TM_Mirror_Product__c==ter2ProdInnerVar.BI_TM_Mirror_Product__c){
					if(terr2ProdOuterVar!=ter2ProdInnerVar && terr2ProdOuterVar.Id!=ter2ProdInnerVar.Id){//check to avoid record check with itself
						if(((terr2ProdOuterVar.BI_TM_End_date__c != NULL && ((ter2ProdInnerVar.BI_TM_Start_date__c >= terr2ProdOuterVar.BI_TM_Start_date__c && ter2ProdInnerVar.BI_TM_Start_date__c <= terr2ProdOuterVar.BI_TM_End_date__c) ||
											(ter2ProdInnerVar.BI_TM_End_date__c  >= terr2ProdOuterVar.BI_TM_Start_date__c && ter2ProdInnerVar.BI_TM_End_date__c  <= terr2ProdOuterVar.BI_TM_End_date__c) ||
											(ter2ProdInnerVar.BI_TM_Start_date__c <= terr2ProdOuterVar.BI_TM_Start_date__c && ter2ProdInnerVar.BI_TM_End_date__c  >= terr2ProdOuterVar.BI_TM_End_date__c) ||
											(ter2ProdInnerVar.BI_TM_Start_date__c<=terr2ProdOuterVar.BI_TM_Start_date__c && ter2ProdInnerVar.BI_TM_End_date__c >= terr2ProdOuterVar.BI_TM_Start_date__c))) ||
									(terr2ProdOuterVar.BI_TM_End_date__c == NULL && ((ter2ProdInnerVar.BI_TM_End_date__c >= terr2ProdOuterVar.BI_TM_Start_date__c)))) &&
								ter2ProdInnerVar.BI_TM_End_date__c!=Null){
							duplicateExistInSystem=true;
							errorMsg1='Record already exist for the given product vs position values. Existing Record Name:'+ter2ProdInnerVar.Name;

						}
						else if(ter2ProdInnerVar.BI_TM_End_date__c==Null && (ter2ProdInnerVar.BI_TM_Start_date__c <=terr2ProdOuterVar.BI_TM_End_date__c || terr2ProdOuterVar.BI_TM_End_date__c == NULL)){
							duplicateExistInSystem=true;
							errorMsg1='There is already record existing without enddate for this position to product in system. Please end date the existing record before creating new. Existing Record Name:'+ter2ProdInnerVar.Name;
						}
					}
				}
				if(duplicateExistInSystem)break;
			}

			//Check with the records coming in single batch.
			for(BI_TM_Territory_to_product__c ter2ProdInnerVar:(List<BI_TM_Territory_to_product__c>)newItems){
				if(terr2ProdOuterVar.BI_TM_Territory_id__c== ter2ProdInnerVar.BI_TM_Territory_id__c && terr2ProdOuterVar.BI_TM_Mirror_Product__c==ter2ProdInnerVar.BI_TM_Mirror_Product__c){
					if(ter2ProdInnerVar!=terr2ProdOuterVar ){//check to avoid record check with itself
						if((terr2ProdOuterVar.Id!=ter2ProdInnerVar.Id) || terr2ProdOuterVar.Id==NULL){//Check to avoid record check with itself in update scenario.
							if(((terr2ProdOuterVar.BI_TM_End_date__c != NULL && ((ter2ProdInnerVar.BI_TM_Start_date__c >= terr2ProdOuterVar.BI_TM_Start_date__c && ter2ProdInnerVar.BI_TM_Start_date__c <= terr2ProdOuterVar.BI_TM_End_date__c) ||
												(ter2ProdInnerVar.BI_TM_End_date__c  >= terr2ProdOuterVar.BI_TM_Start_date__c && ter2ProdInnerVar.BI_TM_End_date__c  <= terr2ProdOuterVar.BI_TM_End_date__c) ||
												(ter2ProdInnerVar.BI_TM_Start_date__c <= terr2ProdOuterVar.BI_TM_Start_date__c && ter2ProdInnerVar.BI_TM_End_date__c  >= terr2ProdOuterVar.BI_TM_End_date__c) ||
												(ter2ProdInnerVar.BI_TM_Start_date__c<=terr2ProdOuterVar.BI_TM_Start_date__c && ter2ProdInnerVar.BI_TM_End_date__c >= terr2ProdOuterVar.BI_TM_Start_date__c))) ||
										(terr2ProdOuterVar.BI_TM_End_date__c == NULL && ((ter2ProdInnerVar.BI_TM_End_date__c >= terr2ProdOuterVar.BI_TM_Start_date__c)))) &&
									ter2ProdInnerVar.BI_TM_End_date__c!=Null){
								duplicateExistInFile=true;
								errorMsg2='Duplicate record existing within input source itself. Please validate data before loading.';

							}
							else if(ter2ProdInnerVar.BI_TM_End_date__c==Null && (ter2ProdInnerVar.BI_TM_Start_date__c <=terr2ProdOuterVar.BI_TM_End_date__c || terr2ProdOuterVar.BI_TM_End_date__c == NULL)){
								duplicateExistInFile=true;
								errorMsg2='There is already record existing without enddate within input source itself. Please validate data before loading';
							}
						}
					}
				}
				if(duplicateExistInFile)break;//Break the loop if duplicate is found.
			}

			//check if duplicate existed. Throw error if exist.
			if(terr2ProdOuterVar.BI_TM_End_date__c<terr2ProdOuterVar.BI_TM_Start_date__c)
			terr2ProdOuterVar.addError('End Date Should not be less than Start Date.');
			else if(duplicateExistInFile)
			terr2ProdOuterVar.addError(errorMsg2);//Error message for user on duplicate record within file.
			else if(duplicateExistInSystem)
			terr2ProdOuterVar.addError(errorMsg1);//Error message for user on duplicate record within system.

		}
	}

	// Function to consolidate the attributes in the child position records when the override flag is unchecked
	public void consolidateParentAttributes(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
		// Sets with the parent field force to product and territory to products
		Set<Id> ff2prodSet = new Set<Id>();
		Set<Id> terr2prodSet = new Set<Id>();
		Set<Id> pos2prodSet = new Set<Id>();

		// The override flag has been changed to false, so the attributes from the parent need to be consolidated to the position to product record
		for(Id p2p : newItems.keySet()){
			if(((BI_TM_Territory_to_product__c)newItems.get(p2p)).BI_TM_Override_Parent__c == FALSE && ((BI_TM_Territory_to_product__c)oldItems.get(p2p)).BI_TM_Override_Parent__c == TRUE
					&& (((((BI_TM_Territory_to_product__c)newItems.get(p2p)).BI_TM_Parent_Field_Force_To_Product__c != null) || (((BI_TM_Territory_to_product__c)newItems.get(p2p)).BI_TM_Parent_Field_Force_To_Product__c != ''))
						|| ((((BI_TM_Territory_to_product__c)newItems.get(p2p)).BI_TM_Parent_Territory_to_Product__c != null) || (((BI_TM_Territory_to_product__c)newItems.get(p2p)).BI_TM_Parent_Territory_to_Product__c != '')))){

				pos2prodSet.add(p2p);

				// Build the sets with the parents
				Id parentFf2Prod = ((BI_TM_Territory_to_product__c)newItems.get(p2p)).BI_TM_Parent_Field_Force_To_Product__c;
				Id parentTerr2Prod = ((BI_TM_Territory_to_product__c)newItems.get(p2p)).BI_TM_Parent_Territory_to_Product__c;

				if(parentFf2Prod != null || parentFf2Prod != ''){
					ff2prodSet.add(parentFf2Prod);
				}

				if(parentTerr2Prod != null || parentTerr2Prod != ''){
					terr2prodSet.add(parentTerr2Prod);
				}
			}
		}

		Map<Id, BI_TM_FF_Type_To_Product__c> ff2prodMap = new Map<Id, BI_TM_FF_Type_To_Product__c>([SELECT Id, BI_TM_End_Date__c, BI_TM_Primary_Product__c, BI_TM_Start_Date__c
                                                                                            		FROM BI_TM_FF_Type_To_Product__c
                                                                                            		WHERE Id IN :ff2prodSet]);
		Map<Id, BI_TM_Territory_to_Product_ND__c> terr2prodMap = new Map<Id, BI_TM_Territory_to_Product_ND__c>([SELECT Id, BI_TM_End_Date__c, BI_TM_Primary_Product__c, BI_TM_Start_Date__c
                                                                                                        		FROM BI_TM_Territory_to_Product_ND__c
                                                                                                        		WHERE Id IN :terr2prodSet]);

		// For the position to products, check if attributes are different from the parents´ attributes
		for(Id p2p : pos2prodSet){
			BI_TM_Territory_to_product__c pos2prod = (BI_TM_Territory_to_product__c) newItems.get(p2p);
			if(pos2prod.BI_TM_Parent_Field_Force_To_Product__c != null || pos2prod.BI_TM_Parent_Field_Force_To_Product__c != ''){
				Id parentFf2prod = pos2prod.BI_TM_Parent_Field_Force_To_Product__c;
				if(ff2prodMap.get(parentFf2prod) != null){
					if(pos2prod.BI_TM_Start_Date__c != ff2prodMap.get(parentFf2prod).BI_TM_Start_Date__c){
						pos2prod.BI_TM_Start_Date__c = ff2prodMap.get(parentFf2prod).BI_TM_Start_Date__c;
					}
					if(pos2prod.BI_TM_End_Date__c != ff2prodMap.get(parentFf2prod).BI_TM_End_Date__c){
						pos2prod.BI_TM_End_Date__c = ff2prodMap.get(parentFf2prod).BI_TM_End_Date__c;
					}
				}
			}
			if(pos2prod.BI_TM_Parent_Territory_to_Product__c != null || pos2prod.BI_TM_Parent_Territory_to_Product__c != ''){
				Id parentTerr2prod = pos2prod.BI_TM_Parent_Territory_to_Product__c;
				if(terr2prodMap.get(parentTerr2prod) != null){
					if(pos2prod.BI_TM_Start_Date__c != terr2prodMap.get(parentTerr2prod).BI_TM_Start_Date__c){
						pos2prod.BI_TM_Start_Date__c = terr2prodMap.get(parentTerr2prod).BI_TM_Start_Date__c;
					}
					if(pos2prod.BI_TM_End_Date__c != terr2prodMap.get(parentTerr2prod).BI_TM_End_Date__c){
						pos2prod.BI_TM_End_Date__c = terr2prodMap.get(parentTerr2prod).BI_TM_End_Date__c;
					}
				}
			}
		}

	}

	// Function to check the overlap dates
	public void checkOverlapDates(Map<Id, SObject> newItems){
		// First we have to check overlaps in the incoming records
		Map<String, Set<Id>> keyMapNewItems = new Map<String, Set<Id>>();

		for(Id p2p : newItems.keySet()){
			//system.debug('Key :: ' + ((BI_TM_Territory_to_Product_ND__c)newItems.get(t2p)).BI_TM_Key_Terr2Prod__c);
			String key = ((BI_TM_Territory_to_Product__c)newItems.get(p2p)).BI_TM_Key_Pos2Prod__c;
			if(keyMapNewItems.get(key) != null){
				Set<Id> idSet = keyMapNewItems.get(key);
				idSet.add(p2p);
				keyMapNewItems.put(key, idSet);
			}
			else{
				Set<Id> idSet = new Set<Id>();
				idSet.add(p2p);
				keyMapNewItems.put(key, idSet);
			}
		}

		// Clone the map with the keys to remove the ids that are overlapped
		Map<String, Set<Id>> keyMapNewItemsNoOverlap = new Map<String, Set<Id>>(keyMapNewItems);
		Set<Id> keySetOverlap = new Set<Id>();

		// Check the overlap in the cncoming records
		if(keyMapNewItems != null){
			for(String key : keyMapNewItems.keySet()){
				if(keyMapNewItems.get(key) != null && keyMapNewItems.get(key).size() > 1){ // There is more that one incoming record with same key
					Set<Id> idSet = keyMapNewItems.get(key);
					Set<Id> idSetCopy = new Set<Id>(idSet);

					// Check overlap dates with records with same key
					for(Id id1 : idSet){
						system.debug('p2p1 new items:: ' + id1);
						BI_TM_Territory_to_Product__c p2p1 = (BI_TM_Territory_to_Product__c)newItems.get(id1);
						for(Id id2 : idSetCopy){
							if(id1 != id2){
								BI_TM_Territory_to_Product__c p2p2 = ((BI_TM_Territory_to_Product__c)newItems.get(id2));

								if(((p2p1.BI_TM_Start_Date__c >= p2p2.BI_TM_Start_Date__c) && (p2p2.BI_TM_End_Date__c != null ? p2p1.BI_TM_Start_Date__c <= p2p2.BI_TM_End_Date__c : true)) ||
											((p2p1.BI_TM_Start_Date__c <= p2p2.BI_TM_Start_Date__c) && (p2p1.BI_TM_End_Date__c != null ? p2p1.BI_TM_End_Date__c >= p2p2.BI_TM_Start_Date__c : true)) ||
												((p2p1.BI_TM_End_Date__c != null && p2p2.BI_TM_End_Date__c != null) ? ((p2p1.BI_TM_End_Date__c <= p2p2.BI_TM_End_Date__c) ? (p2p1.BI_TM_End_Date__c >= p2p2.BI_TM_Start_Date__c) : (p2p1.BI_TM_Start_Date__c <= p2p2.BI_TM_End_Date__c)) : false)){
									p2p1.addError('You are trying to insert records with overlapped dates - Record 1: ' + p2p1.Id + ' : SD - ' + p2p1.BI_TM_Start_Date__c + ' : ED - ' + p2p1.BI_TM_End_Date__c + ', Record 2: ' + p2p2.Id + ' : SD - ' + p2p2.BI_TM_Start_Date__c + ' : ED - ' + p2p2.BI_TM_End_Date__c);

									Set<Id> keySetOverlap2remove = keyMapNewItemsNoOverlap.get(key);
									if(keySetOverlap2remove.contains(p2p1.Id)){
										keySetOverlap2remove.remove(p2p1.Id);
										keyMapNewItemsNoOverlap.put(key, keySetOverlap2remove);
									}
								}
							}
						}
					}
				}
			}
		}

		//system.debug('Map no overlap records :: ' + keyMapNewItemsNoOverlap);

		// Check overlap dates with the records in the system
		if(keyMapNewItemsNoOverlap != null){
			Map<Id, BI_TM_Territory_to_Product__c> pos2ProdMapSystem = new Map<Id, BI_TM_Territory_to_Product__c>([SELECT Id, BI_TM_Key_Pos2Prod__c, BI_TM_Start_Date__c, BI_TM_End_Date__c, BI_TM_Territory_id__c, BI_TM_Mirror_Product__c FROM BI_TM_Territory_to_Product__c WHERE Id NOT IN :newItems.keySet() AND BI_TM_Key_Pos2Prod__c IN :keyMapNewItemsNoOverlap.keySet()]);
			Map<String, Set<Id>> keyMapSystemItems = new Map<String, Set<Id>>();

			for(Id p2p : pos2ProdMapSystem.keySet()){
				//system.debug('Key :: ' + (terr2ProdMapSystem.get(t2p)).BI_TM_Key_Terr2Prod__c);
				String key = (pos2ProdMapSystem.get(p2p)).BI_TM_Key_Pos2Prod__c;
				if(keyMapSystemItems.get(key) != null){
					Set<Id> idSet = keyMapSystemItems.get(key);
					idSet.add(p2p);
					keyMapSystemItems.put(key, idSet);
				}
				else{
					Set<Id> idSet = new Set<Id>();
					idSet.add(p2p);
					keyMapSystemItems.put(key, idSet);
				}
			}

			for(String key : keyMapNewItemsNoOverlap.keySet()){
				if(keyMapNewItemsNoOverlap.get(key) != null && keyMapSystemItems.get(key) != null){
					Set<Id> idSet = keyMapNewItemsNoOverlap.get(key);
					Set<Id> idSetSystem = keyMapSystemItems.get(key);

					// Check overlap dates with records with same key in the system
					for(Id id1 : idSet){
						BI_TM_Territory_to_Product__c p2p1 = (BI_TM_Territory_to_Product__c)newItems.get(id1);
						system.debug('p2p1 new items:: ' + id1);
						for(Id id2 : idSetSystem){
							if(id1 != id2){
								BI_TM_Territory_to_Product__c p2p2 = pos2ProdMapSystem.get(id2);
								system.debug('p2p2 system items:: ' + id2);
								if((((p2p1.BI_TM_Start_Date__c >= p2p2.BI_TM_Start_Date__c) && (p2p2.BI_TM_End_Date__c != null ? p2p1.BI_TM_Start_Date__c <= p2p2.BI_TM_End_Date__c : true)) ||
											((p2p1.BI_TM_Start_Date__c <= p2p2.BI_TM_Start_Date__c) && (p2p1.BI_TM_End_Date__c != null ? p2p1.BI_TM_End_Date__c >= p2p2.BI_TM_Start_Date__c : true) ||
												((p2p1.BI_TM_End_Date__c != null && p2p2.BI_TM_End_Date__c != null) ?
													((p2p1.BI_TM_End_Date__c <= p2p2.BI_TM_End_Date__c) ? p2p1.BI_TM_End_Date__c >= p2p2.BI_TM_Start_Date__c : p2p1.BI_TM_Start_Date__c <= p2p2.BI_TM_End_Date__c) : false)))){
									p2p1.addError('There are already records in the system that overlapped the dates - Record New: ' + p2p1.Id + ' : SD - ' + p2p1.BI_TM_Start_Date__c + ' : ED - ' + p2p1.BI_TM_End_Date__c + ', Existing record: ' + p2p2.Id + ' : SD - ' + p2p2.BI_TM_Start_Date__c + ' : ED - ' + p2p2.BI_TM_End_Date__c);

								}
							}
						}
					}
				}
			}
		}
	}

}