/******************************************************************************** 
Name:  BI_TM_TerrtoGeoHistoryBatch 
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Batch apex to push geo to terr data to history object
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Rao G               09/12/2015   INITIAL DEVELOPMENT
*********************************************************************************/
global class BI_TM_TerrtoGeoHistoryBatch implements Database.batchable<sObject> {
    public String Query;
    
    global Database.QueryLocator Start(Database.BatchableContext info)
    {
        List<String> alignmentsToProcess = BI_TM_TerrtoGeoHistoryBatchUtil.getValidParentAlignments();
        system.debug('alignmentsToProcess ########:' + alignmentsToProcess);        
        // fetch Address_vod__c records ready for updating brick value if empty.
        Query = 'SELECT BI_TM_Geography__c,BI_TM_Parent_alignment__c,BI_TM_Territory_ND__c,Id,Name,BI_TM_Primary_territory__c,BI_TM_End_date__c,BI_TM_Start_date__c FROM BI_TM_Geography_to_territory__c where BI_TM_Parent_alignment__c=:alignmentsToProcess';
        return Database.getQueryLocator(query);
        
    }
    global void Execute(Database.BatchableContext info, List<BI_TM_Geography_to_territory__c> scope)
    {
        system.debug('scope $$$$$$$$$$:'+scope);        
        BI_TM_TerrtoGeoHistoryBatchUtil.moveGeoToTerrToHistory(scope);
        //After moving to History delete from actual object
        delete scope;
    }
    
    global void Finish(Database.BatchableContext info){ 
             
    }
}