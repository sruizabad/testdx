/******************************************************************************** 
Name:  BI_TM_TerrtoGeoHistoryBatchUtil 
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Batch apex util to process valid alignments to push to history object
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Rao G               09/12/2015   INITIAL DEVELOPMENT
*********************************************************************************/

public without sharing class BI_TM_TerrtoGeoHistoryBatchUtil {
    public static List < String > getValidParentAlignments() {
        String countriesToProcess = Label.BI_TM_AlignmentDataBkp_Countries;
        Set < String > countries = new Set < String > (countriesToProcess.split(','));
        date d = system.today().addDays(-1095);
        system.debug('====='+d);
        List < BI_TM_Alignment__c > pasAls = [select id, name, createddate, BI_TM_FF_type__c, BI_TM_End_date__c, BI_TM_FF_type__r.Name from BI_TM_Alignment__c where bi_tm_status__c = 'Past' AND BI_TM_Country_code__c=:countries 
        AND BI_TM_End_date__c <= :d];
        system.debug(pasAls);
        
        List < String > finalAlignments = new List < String > ();
        Map < String, List < BI_TM_AlignmentWrapper >> pastalmap = new Map < String, List < BI_TM_AlignmentWrapper >> ();
        for (BI_TM_Alignment__c al: pasAls) {
            if (pastalmap != null && !pastalmap.containsKey(al.BI_TM_FF_type__c)) {
                List < BI_TM_AlignmentWrapper > ls = new List < BI_TM_AlignmentWrapper > ();
                BI_TM_AlignmentWrapper alw = new BI_TM_AlignmentWrapper(al);
                ls.add(alw);
                pastalmap.put(al.BI_TM_FF_type__c, ls);
            } else if (pastalmap != null && pastalmap.containsKey(al.BI_TM_FF_type__c)) {
                BI_TM_AlignmentWrapper alw = new BI_TM_AlignmentWrapper(al);
                pastalmap.get(al.BI_TM_FF_type__c).add(alw);
            }
        }

        system.debug('pastalmap $$$$:' + pastalmap);

        for (String fType: pastalmap.keySet()) {
            List < BI_TM_AlignmentWrapper > wls = pastalmap.get(fType);
            wls.sort();
            for (Integer i = 0; i < wls.size(); i++) {
                system.debug('Name #####: ' + wls[i].alignment.BI_TM_FF_type__r.Name + ':' + wls[i].alignment.Name);
                finalAlignments.add(wls[i].alignment.Id);
            }
        }

        system.debug('finalAlignments @@@@@@@@@:' + finalAlignments);

        return finalAlignments;
    }
    
    public static void moveGeoToTerrToHistory(List<BI_TM_Geography_to_territory__c> recs){
        system.debug('records being processed ....');
        List<BI_TM_Geography_to_Territory_History__c> historyRecs = new List<BI_TM_Geography_to_Territory_History__c>();
        for(BI_TM_Geography_to_territory__c geottoterr : recs){
            BI_TM_Geography_to_Territory_History__c geotoTerrHis = new BI_TM_Geography_to_Territory_History__c();
            geotoTerrHis.BI_TM_Start_date__c = geottoterr.BI_TM_Start_date__c;
            geotoTerrHis.BI_TM_End_date__c = geottoterr.BI_TM_End_date__c;
            geotoTerrHis.BI_TM_Geography__c = geottoterr.BI_TM_Geography__c;
            geotoTerrHis.BI_TM_Territory_ND__c = geottoterr.BI_TM_Territory_ND__c;
            geotoTerrHis.BI_TM_Parent_alignment__c = geottoterr.BI_TM_Parent_alignment__c;
            geotoTerrHis.BI_TM_Primary_territory__c = geottoterr.BI_TM_Primary_territory__c;
            geotoTerrHis.Name = geottoterr.Name;
            geotoTerrHis.BI_TM_Country_Code__c=geottoterr.BI_TM_Country_Code__c;
            geotoTerrHis.BI_TM_Business__c=geottoterr.BI_TM_Business__c;
            historyRecs.add(geotoTerrHis);
        }
        insert historyRecs;
    }   
}