/******************************************************************************** 
Name:  BI_TM_TerrtoGeoHistorySched 
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Scheduled apex which invokes BI_TM_TerrtoGeoHistoryBatch
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -    Rao G               09/12/2015   INITIAL DEVELOPMENT
*********************************************************************************/
global class BI_TM_TerrtoGeoHistorySched implements Schedulable {
	global void execute(SchedulableContext sc)
    {
        BI_TM_TerrtoGeoHistoryBatch batchObj = new BI_TM_TerrtoGeoHistoryBatch();
		database.executebatch(batchObj); 
    }

}