@isTest
public class BI_TM_TerrtoGeoHistorySched_Test {
  public static String CRON_EXP = '0 0 0 15 3 ? 2022';

  static testmethod void testGeoTerrBkpUpdate() {

    createTestData();

    Test.startTest();

    // Schedule the test job
    String jobId = System.schedule('BI_TM_TerrtoGeoHistorySched_Test',
    CRON_EXP,
    new BI_TM_TerrtoGeoHistorySched());

    // Get the information from the CronTrigger API object
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,
    NextFireTime
    FROM CronTrigger WHERE id = :jobId];

    // Verify the expressions are the same
    System.assertEquals(CRON_EXP,
    ct.CronExpression);

    // Verify the job has not run
    System.assertEquals(0, ct.TimesTriggered);

    // Verify the next time the job will run
    System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));

    Test.stopTest();

  }
  static void createTestData(){

    User currentuser = new User();
    currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];

    BI_TM_Geography_type__c gtype = new BI_TM_Geography_type__c(Name='Brick',BI_TM_Country_Code__c=currentuser.Country_Code_BI__c, BI_TM_CRM_Column__c = 'brick_vod__c');
    insert gtype;

    BI_TM_Geography__c geoG1 = new BI_TM_Geography__c(Name='GX9',BI_TM_Is_Active__c=true,BI_TM_Geography_type__c=gtype.Id,BI_TM_Country_Code__c=currentuser.Country_Code_BI__c);
    insert geoG1;

    BI_TM_Geography__c geoG2 = new BI_TM_Geography__c(Name='BX9',BI_TM_Is_Active__c=true,BI_TM_Geography_type__c=gtype.Id,BI_TM_Country_Code__c=currentuser.Country_Code_BI__c);
    insert geoG2;

    //BI_TM_Territory__c terr1 = new BI_TM_Territory__c(Name='Test FF1', BI_TM_Status__c='Active', BI_TM_Is_position__c=false,BI_TM_Is_FF_hierarchy__c=true);
    //insert terr1;

    //Below lines added by Kiran on 14-04-2-2016 from Line No:48 to Line No:92
    BI_TM_Territory__c Terr= new BI_TM_Territory__c();
    BI_TM_Territory__c Terr1= new BI_TM_Territory__c();
    BI_TM_FF_type__c FF= new BI_TM_FF_type__c();
    BI_TM_Position_Type__c pt = new BI_TM_Position_Type__c();
    BI_TM_Territory_ND__c newTerr= new BI_TM_Territory_ND__c();

    FF.Name='Test MX FF21';
    FF.BI_TM_Business__c='PM';
    FF.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    insert FF;

    pt.Name='Test MX FF';
    pt.BI_TM_Business__c='AH';
    pt.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    insert pt;
    newTerr.Name='testnewterr';
    newTerr.BI_TM_Field_Force__c=FF.Id;
    newTerr.BI_TM_Start_Date__c=Date.today().addDays(-2);
    newTerr.BI_TM_Business__c='PM';
    newTerr.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c ;
    Insert newTerr;

    Terr1.BI_TM_Is_Root__c=True;
    Terr1.BI_TM_FF_type__c=FF.Id;
    Terr1.BI_TM_Business__c='PM';
    Terr1.BI_TM_Position_Level__c='Country';
    Terr1.BI_TM_Position_Type_Lookup__c=pt.Id;
    Terr1.BI_TM_Visible_in_crm__c=True;
    //Terr1.BI_TM_Is_Active_Checkbox__c=true;
    Terr1.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
    Terr1.BI_TM_Start_date__c=Date.today().addDays(-1);
    //Terr1.BI_TM_Start_date__c=system.Today();

    //Insert Terr1;

    // rtype1=[select Id from RecordType where Name='Territory'Limit 1];
    Terr.Name='Test Tam21';
    Terr.BI_TM_FF_type__c=FF.Id;
    Terr.BI_TM_Business__c='PM';
    Terr.BI_TM_Position_Level__c='Country';
    Terr.BI_TM_Position_Type_Lookup__c=pt.Id;
    Terr.BI_TM_Parent_Position__c=Terr1.Id;
    Terr.BI_TM_Visible_in_crm__c=True;
    //Terr.BI_TM_Is_Active_Checkbox__c=True;
    Terr.BI_TM_Country_Code__c = currentuser.Country_Code_BI__c;
    Terr.BI_TM_Start_date__c=Date.today().addDays(-1);
    //Terr.BI_TM_End_date__c=system.Today();

    BI_TM_Alignment__c al1 = new BI_TM_Alignment__c(Name='test 1', BI_TM_Status__c='Future', BI_TM_Country_code__c=currentuser.Country_Code_BI__c, BI_TM_FF_type__c=FF.id,BI_TM_Business__c='PM',BI_TM_Start_date__c=system.today().addDays(-1200),BI_TM_End_date__c=system.today().addDays(-1195));
    insert al1;
    BI_TM_Alignment__c al2 = new BI_TM_Alignment__c(Name='test 2', BI_TM_Status__c='Future', BI_TM_Country_code__c=currentuser.Country_Code_BI__c, BI_TM_FF_type__c=FF.id,BI_TM_Business__c='PM',BI_TM_Start_date__c=system.today().addDays(-1200),BI_TM_End_date__c=system.today().addDays(-1195));
    insert al2;
    BI_TM_Alignment__c al3 = new BI_TM_Alignment__c(Name='test 3', BI_TM_Status__c='Future', BI_TM_Country_code__c=currentuser.Country_Code_BI__c, BI_TM_FF_type__c=FF.id,BI_TM_Business__c='PM',BI_TM_Start_date__c=system.today().addDays(-1200),BI_TM_End_date__c=system.today().addDays(-1195));
    insert al3;

    BI_TM_Geography_to_territory__c geototerr = new BI_TM_Geography_to_territory__c(BI_TM_Parent_alignment__c=al3.id,BI_TM_Geography__c=geoG2.id,BI_TM_Territory_ND__c=newTerr.id,BI_TM_Country_Code__c=currentuser.Country_Code_BI__c);
    insert geototerr;

  }
}