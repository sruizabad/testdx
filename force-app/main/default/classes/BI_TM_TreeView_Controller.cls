global class BI_TM_TreeView_Controller {

    public enum Platform {ALL, VEEVA, BITMAN}

    public String selectedCountry {get;set;}
    public String selectedBusiness {get;set;}
    public String selectedStatus {get;set;}
    public String selectedAlignment {get;set;}
    public String selectedPlatformStatus {get;set;}
    public List<SelectOption> countries {get; set;}


    public static final String HIERARCHY_TYPE_GLOBAL = 'global';
    public static final String HIERARCHY_TYPE_ALIGNAMENT = 'alignment';

    public static final String ALIGNMENT_STATUS_PAST = 'Past';
    public static final String ALIGNMENT_STATUS_ACTIVE = 'Active';
    public static final String ALIGNMENT_STATUS_FUTURE = 'Future';

    public static final String REPORT_NUM_ACCOUNTS = 'BI_TM_Accounts_by_Territory';

    public static Map<Id, BI_TM_Position_Relation__c> posRelByPostionId = new Map<Id, BI_TM_Position_Relation__c>();

    public String hierarchyType {
      get {
        System.debug('GET hierarchyType ' + hierarchyType);
        if (hierarchyType == null) {
          hierarchyType = HIERARCHY_TYPE_GLOBAL;
        }
        return hierarchyType;
      }
      set;
    }

    public String getHIERARCHY_TYPE_GLOBAL() {
      return HIERARCHY_TYPE_GLOBAL;
    }

    public String getHIERARCHY_TYPE_ALIGNAMENT() {
      return HIERARCHY_TYPE_ALIGNAMENT;
    }

    /**
     *
     */
    public List<SelectOption> alignmentOptions {
      get{
        System.debug('GET alignmentOptions');
        if(HIERARCHY_TYPE_ALIGNAMENT.equals(hierarchyType) && String.isNotBlank(selectedCountry) && String.isNotBlank(selectedBusiness)){
          alignmentOptions = new List<SelectOption>();
          System.debug('statusOptions ' + statusOptions);
          System.debug('selectedCountry ' + selectedCountry);
          System.debug('selectedBusiness ' + selectedBusiness);
          for(BI_TM_Alignment__c al :
            [SELECT Id, Name FROM BI_TM_Alignment__c
              WHERE BI_TM_Status__c = :selectedStatus AND BI_TM_Country_Code__c = :selectedCountry AND BI_TM_Business__c = :selectedBusiness]
          ){
            alignmentOptions.add(new SelectOption(al.Id, al.Name));
          }

          // Check for empty list
          if (alignmentOptions.isEmpty()) {
              alignmentOptions.add(new SelectOption('', 'No values available'));
          }
        }
        return alignmentOptions;
      }
      set;
    }
    //public String status {get; set;}
    public BI_TM_Position_Relation__c futurePosition {get;set;}
    public String selectPositionId {get;set;}
    public BI_TM_Territory__c relatedPositionInfo {
      get {
        this.relatedPositionInfo = null;
        this.futurePosition = null;
        if (!String.isBlank(this.selectPositionId)) {
          if ('Future'.equals(this.selectedStatus) && !String.isBlank(this.selectedAlignment)) {
            try {
              this.futurePosition =
                [SELECT Id, BI_TM_End_Date__c, BI_TM_Start_Date__c, BI_TM_Territory__c, BI_TM_Territory__r.Name
                  FROM BI_TM_Position_Relation__c
                    WHERE   BI_TM_Position__c = :this.selectPositionId];
            } catch (System.QueryException e) {
              // No position relation...will be used position info
            }

          }

          this.relatedPositionInfo =
            [SELECT Id, Name, BI_TM_Territory_ND__c, BI_TM_Territory_ND__r.Name, BI_TM_FF_type__c,
              BI_TM_FF_type__r.Name, BI_TM_Position_Level__c, BI_TM_Is_Active__c, BI_TM_Start_date__c, BI_TM_End_date__c,
              (SELECT Id, Name, BI_TM_User_mgmt_tm__c, BI_TM_User_mgmt_tm__r.Name FROM User_territory1__r WHERE BI_TM_Active__c = true ORDER BY BI_TM_User_mgmt_tm__r.Name),
              (SELECT Id, BI_TM_Mirror_Product__c, BI_TM_Mirror_Product__r.Name FROM Territory_to_Products__r WHERE BI_TM_Active__c= true ORDER BY BI_TM_Mirror_Product__r.Name ASC)
              FROM BI_TM_Territory__c
                WHERE Id = :this.selectPositionId];

        }
        return this.relatedPositionInfo;
      }
      set;
    }

    public Id reportIdNumAccounts{
      get{
        this.reportIdNumAccounts = [SELECT Id FROM Report WHERE DeveloperName = :REPORT_NUM_ACCOUNTS].Id;
        return this.reportIdNumAccounts;
      }
      set;
    }

    private Map<String,Set<String>> countryBusinessMap = null;


    public BI_TM_TreeView_Controller() {
        countryBusinessMap  = BI_TM_UserInfo.getInstance().countryBusinessMap;
        countries = getCountries();
        //status = 'false';
        if (countries.size() > 0) {
            selectedCountry = countries[0].getValue();
        }

    }

    public List<SelectOption> getCountriesOptions() {
        return countries;
    }

    private List<SelectOption> getCountries() {

        List<SelectOption> countryOptions = new List<SelectOption>();

        for(String s : countryBusinessMap.keySet()) {
            countryOptions.add(new SelectOption(s,s));
        }

        return countryOptions;

    }

    /**
     *  For every available country we have different business set depending by
     *  current user given groups. Changing country selector will be look for
     *  the related business.
     */
    public List<SelectOption> getBusinessOptions() {
        List<SelectOption> businessOptions = new List<SelectOption>();
        if(selectedCountry != null) {
            for(String s : countryBusinessMap.get(selectedCountry)) {
                businessOptions .add(new SelectOption(s,s));
            }
        }

        return businessOptions;
    }

    public List<SelectOption> statusOptions {
      get {
        if (statusOptions == null) {
          statusOptions = new List<SelectOption>();
          statusOptions.add(new SelectOption(ALIGNMENT_STATUS_PAST, ALIGNMENT_STATUS_PAST));
          statusOptions.add(new SelectOption(ALIGNMENT_STATUS_ACTIVE, ALIGNMENT_STATUS_ACTIVE));
          statusOptions.add(new SelectOption(ALIGNMENT_STATUS_FUTURE,ALIGNMENT_STATUS_FUTURE));
        }
        return statusOptions;
      }
      set;
    }
/*
    public List<SelectOption> getAlignmentOptions(){
      //Map<Id, BI_TM_Alignment__c> mapFutureAlignments = new Map<Id,BI_TM_Alignment__c>();
      List<SelectOption> alignmentOptions = new List<SelectOption>();
      system.debug('Alignment :: ' + status);
      if(selectedStatus == 'Future'){
        List<SelectOption> alignmentOptions = new List<SelectOption>();
        for(BI_TM_Alignment__c al : [Select Id, Name From BI_TM_Alignment__c Where BI_TM_Status__c = 'Future']){
          alignmentOptions.add(new SelectOption(al.Id, al.Name));
        }
        return alignmentOptions;
      }
      else return null;
    }*/

    public List<SelectOption> getPlatformStatusOptions() {
        List<SelectOption> platformStatus = new List<SelectOption>();
        platformStatus.add(new SelectOption(Platform.ALL.name(), 'All positions'));
        platformStatus.add(new SelectOption(Platform.VEEVA.name(), 'Positions sent to Veeva'));
        platformStatus.add(new SelectOption(Platform.BITMAN.name(), 'Positions not sent to Veeva'));
        return platformStatus;
    }

    /*
    public void statusChange(){

      if(selectedStatus == 'Future'){
        status = 'true';
      }
      else{
        status = 'false';
      }
    }
    */

    public void resetForm() {
        system.debug('resetForm');
        selectedBusiness = null;
        selectedStatus = null;
        selectedAlignment = null;
        selectedPlatformStatus = null;
        hierarchyType = null;
    }

    @RemoteAction
    global static List<sObject> getTree(
        String hierarchyType, String country, String business, String status, String alignamentCycle, String platformStatus, String nodeId
    ) {
        System.debug('###hierarchyType: ' + hierarchyType);
        System.debug('###country: ' + country + '; business: ' + business + '; status: ' + status + '; alignamentCycle: ' + alignamentCycle + '; platformStatus: ' + platformStatus + '; nodeId: ' + nodeId);
        List<sObject> retElements = new List<sObject>();

        if (HIERARCHY_TYPE_GLOBAL.equals(hierarchyType)) {
            // If nodeId is null we are looking for root element. Query is the same for Active and Future
            retElements = BI_TM_TreeView_Controller.getPositionFromParent(country, business, status, alignamentCycle, platformStatus, nodeId);

        } else {
            retElements = BI_TM_TreeView_Controller.getPositionRelationsFromParent(country, business, status, alignamentCycle, platformStatus, nodeId);

        }

        return retElements;
    }

    public void setPositionId() {
      System.debug('selectPositionId: ' + selectPositionId);
    }

    public void setHierarchyType() {
      System.debug('hierarchyType: ' + hierarchyType);
    }

    @TestVisible
    private static List<sObject> getPositionFromParent(
        String country, String business, String status, String alignamentCycle, String platformStatus, String nodeId
    ) {
        // Root element cannot be set with the BI_TM_Business__c field, so avoid filtering for them.
        String queryStr = 'SELECT Id, Name FROM BI_TM_Territory__c '
            + 'WHERE BI_TM_Country_code__c = :country AND BI_TM_ParentId__c = :nodeId '
            + 'AND (BI_TM_Business__c = :business OR BI_TM_Is_Root__c = true) ';

        if (platformStatus == Platform.BITMAN.name()) {
            queryStr += ' AND BI_TM_Visible_in_crm__c = false';
        } else if (platformStatus == Platform.VEEVA.name()) {
            queryStr += ' AND BI_TM_Visible_in_crm__c = true';
        }
        System.debug('###getTree query: ' + queryStr);
        return Database.Query(queryStr);
    }

    @TestVisible
    private static List<sObject> getPositionRelationsFromParent(
        String country, String business, String status, String alignamentCycle, String platformStatus, String nodeId
    ) {
        String queryStr;
        if (String.isBlank(nodeId)) {
            queryStr = 'SELECT BI_TM_Position__r.BI_TM_Parent_Position__r.Id, BI_TM_Position__r.BI_TM_Parent_Position__r.Name FROM BI_TM_Position_Relation__c '
                + 'WHERE BI_TM_Country_code__c = :country AND BI_TM_Business__c = :business AND BI_TM_Alignment_Cycle__c = :alignamentCycle '
                + 'AND BI_TM_Position__r.BI_TM_Parent_Position__r.BI_TM_Is_Root__c = true ';
        } else {
            queryStr = 'SELECT BI_TM_Position__r.Id, BI_TM_Position__r.Name FROM BI_TM_Position_Relation__c '
                + 'WHERE BI_TM_Country_code__c = :country AND BI_TM_Business__c = :business AND BI_TM_Alignment_Cycle__c = :alignamentCycle ';
            queryStr += 'AND BI_TM_Parent_Position__c = :nodeId ';

        }

        if (platformStatus == Platform.BITMAN.name()) {
            queryStr += ' AND BI_TM_Position__r.BI_TM_Visible_in_crm__c = false';
        } else if (platformStatus == Platform.VEEVA.name()) {
            queryStr += ' AND BI_TM_Position__r.BI_TM_Visible_in_crm__c = true';
        }
        queryStr += ' ORDER BY Name ASC';
        if (String.isBlank(nodeId)) {
                queryStr += ' LIMIT 1';
        }

        System.debug('###getTree query: ' + queryStr);

        List<sObject> posRelations = Database.Query(queryStr);
        // This is for all cases in which position hierarchy is set but incomplete and without any root
        if (posRelations.isEmpty() && String.isBlank(nodeId)) {
          Set<Id> positions = new Set<Id>();
          List<BI_TM_Position_Relation__c> posRelationsExt = new List<BI_TM_Position_Relation__c> ();
          posRelationsExt = [SELECT Id, BI_TM_Position__c, BI_TM_Parent_Position__c FROM BI_TM_Position_Relation__c
                            WHERE BI_TM_Country_code__c = :country AND BI_TM_Business__c = :business AND BI_TM_Alignment_Cycle__c = :alignamentCycle];

          for (BI_TM_Position_Relation__c forPosRel : posRelationsExt) {
            positions.add(forPosRel.BI_TM_Position__c);
          }

          Set<Id> parentIds = new Set<Id>();
          for (BI_TM_Position_Relation__c forPosRel : posRelationsExt) {
            if (!positions.contains(forPosRel.BI_TM_Parent_Position__c)) {
              parentIds.add(forPosRel.BI_TM_Parent_Position__c);
            }
          }

          if (!parentIds.isEmpty()) {
            queryStr = 'SELECT Id, Name FROM BI_TM_Territory__c '
                + 'WHERE Id IN :parentIds ';

            posRelations = Database.Query(queryStr);
          }

        }

        return posRelations;
    }

}