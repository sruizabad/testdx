/******************************************************************************** 
Name:  BI_TM_TreeView_ControllerTest 
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Test Class for BI_TM_TreeView_Controller
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -     Kiran               15/04/2016   INITIAL DEVELOPMENT
*********************************************************************************/

@isTest(SeeAllData=true)
private class BI_TM_TreeView_ControllerTest
{
    
  private static testmethod void testgetData()
  {
    test.startTest();
    
    User currentuser = new User();
      currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
    
    BI_TM_FF_type__c fftype = new BI_TM_FF_type__c(Name='TESTFIELDFORCE',BI_TM_Country_Code__c=currentuser.Country_Code_BI__c,BI_TM_Business__c='PM');   
        insert fftype;
   
    BI_TM_Alignment__c al = new BI_TM_Alignment__c(name='AlignmentTestclass',BI_TM_FF_type__c=fftype.Id,BI_TM_Status__c='Future',BI_TM_Country_Code__c=currentuser.Country_Code_BI__c,BI_TM_Business__c='PM',BI_TM_Start_date__c=system.today());
        insert al;
   
   BI_TM_TreeView_Controller ctrl=new BI_TM_TreeView_Controller();

     List<SelectOption> selOpts=ctrl.alignmentOptions;
     BI_TM_Alignment__c al1 = new BI_TM_Alignment__c(name='AlignmentTestclass1',BI_TM_FF_type__c=fftype.Id,BI_TM_Country_Code__c=currentuser.Country_Code_BI__c,BI_TM_Business__c='PM',BI_TM_Start_date__c=system.today());
        insert al1;
        
       //selOpts.add(new SelectOption(al1.Id, al1.Name));
      
        String country='MX';
        String business='PM';
        String status='Active';
        String platformStatus='True';
        String alignamentCycle=al.Id;
        String nodeId='Mexico';
        Id Id=al.Id;
        String Name =al.Name;
    
    BI_TM_TreeView_Controller con= new BI_TM_TreeView_Controller();
      con.getCountriesOptions();
      con.getBusinessOptions();
      //con.getStatusOptions();
      con.getPlatformStatusOptions();
      //con.getalignmentOptions();
      con.resetForm();
      System.assertEquals(business, 'PM');
      
       //BI_TM_TreeView_Controller.getTree(country,business,status,alignamentCycle,platformStatus,nodeId);
        
       BI_TM_TreeView_Controller.getPositionFromParent(country,business, status, alignamentCycle, platformStatus,nodeId);
       BI_TM_TreeView_Controller.getPositionRelationsFromParent(country,business, status, alignamentCycle, platformStatus,nodeId);
      
    //System.assertNotEquals(null,BI_TM_TreeView_Controller.getTree(country,business,status,alignamentCycle,platformStatus,nodeId));
    
    test.stopTest();
  }  
}