public class BI_TM_TriggerDispatcher 
{
    public static Boolean SKIP_EXECUTION = false;
    
    /*
        Call this method from your trigger, passing in an instance of a trigger handler which implements ITriggerHandler.
        This method will fire the appropriate methods on the handler depending on the trigger context.
    */
    public static void Run(BI_TM_ITriggerHandler handler)
    {
        // Detect the current trigger context and fire the relevant methods on the trigger handler:
        if (!BI_TM_TriggerDispatcher.SKIP_EXECUTION) {
            // Before trigger logic
            if (Trigger.IsBefore )
            {
                if (Trigger.IsInsert) 
                {
                    BI_TM_RecordVerification verification = new BI_TM_RecordVerification();
                    verification.checkCountryAndBusiness(trigger.new);
                    handler.BeforeInsert(trigger.new);
                }
                if (Trigger.IsUpdate) 
                {
                    BI_TM_RecordVerification verification = new BI_TM_RecordVerification();
                    verification.checkCountryAndBusiness(trigger.new);
                    handler.BeforeUpdate(trigger.newMap, trigger.oldMap);
                }
                if (Trigger.IsDelete)
                {
                    handler.BeforeDelete(trigger.oldMap);
                }
            }
            
            // After trigger logic
            if (Trigger.IsAfter)
            {
                if (Trigger.IsInsert)
                    handler.AfterInsert(Trigger.newMap);
    
                if (Trigger.IsUpdate)
                    handler.AfterUpdate(trigger.newMap, trigger.oldMap);
    
                if (trigger.IsDelete)
                    handler.AfterDelete(trigger.oldMap);
    
                if (trigger.isUndelete)
                    handler.AfterUndelete(trigger.oldMap);
            }
        }
    }
}