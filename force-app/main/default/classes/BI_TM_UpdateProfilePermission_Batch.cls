/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Batch to set the profile and permission sets from the position to the user management
*  @author:         Antonio Ferrero
*  @created:        16-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         16-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_UpdateProfilePermission_Batch implements Database.Batchable<sObject>, database.stateful, Schedulable {

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT Id, BI_TM_Territory1__r.BI_TM_Profile__c, BI_TM_Territory1__r.BI_TM_Permission_Set__c, BI_TM_User_mgmt_tm__c FROM BI_TM_User_territory__c WHERE BI_TM_Active__c = true AND BI_TM_User_mgmt_tm__r.BI_TM_Visible_in_CRM__c = true AND (BI_TM_Assignment_Type__c = \'Primary\' OR BI_TM_Primary__c = true) AND BI_TM_User_mgmt_tm__r.BI_TM_Override_Position_Settings__c = false';
		system.debug('Query :: ' + query);
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<BI_TM_User_territory__c> scope) {
		List<BI_TM_User_mgmt__c> umList2Update = new List<BI_TM_User_mgmt__c>();
		for(BI_TM_User_territory__c u2p : scope){
			BI_TM_User_mgmt__c um = new BI_TM_User_mgmt__c(Id = u2p.BI_TM_User_mgmt_tm__c);
			Boolean addRecord = false;
			if(BI_TM_UpdateProfilePermission_Helper.checkProfile(u2p)){
				um.BI_TM_Profile__c = u2p.BI_TM_Territory1__r.BI_TM_Profile__c;
				addRecord = true;
			}
			if(BI_TM_UpdateProfilePermission_Helper.checkPermissionSet(u2p)){
				um.BI_TM_Permission_Set__c = BI_TM_UpdateProfilePermission_Helper.setPermissionSet(u2p.BI_TM_Territory1__r.BI_TM_Permission_Set__c);
				addRecord = true;
			}
			if(addRecord){
				umList2Update.add(um);
			}
		}
		update umList2Update;
	}

	global void execute(SchedulableContext sc) {
    BI_TM_UpdateProfilePermission_Batch b = new BI_TM_UpdateProfilePermission_Batch();
    database.executebatch(b);
  }

	global void finish(Database.BatchableContext BC) {
		database.executeBatch(new BI_TM_ConsolidateProfilePermission_Batch());
	}

}