/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Helper of the batch to set the profile and permission sets from the position to the user management
*  @author:         Antonio Ferrero
*  @created:        16-Aug-2018
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         16-Aug-2018                 Antonio Ferrero             Construction of the class.
*/
public without sharing class BI_TM_UpdateProfilePermission_Helper {

	//Check if the profile is filled in
	public static Boolean checkProfile(BI_TM_User_territory__c u2p){
		return u2p.BI_TM_Territory1__r.BI_TM_Profile__c != null;
	}

	// Check if the permission set is filled in
	public static Boolean checkPermissionSet(BI_TM_User_territory__c u2p){
		return u2p.BI_TM_Territory1__r.BI_TM_Permission_Set__c != null;
	}

	// Get the permission sets API values and set them to the user management record
	public static String setPermissionSet(String ps){
		String pSet = '';
		List<String> psList = ps.split(';');
		Map<String, String> permissionSetMap = getPermissionSetsMap();
		for(String psv : psList){
			if(permissionSetMap.get(psv) != null){
				pSet += permissionSetMap.get(psv) + ';';
			}
		}
		return pSet;
	}

	// build the map between the permission set lables and the API names from the user management picklist
	private static Map<String, String> getPermissionSetsMap(){
		List<Schema.PicklistEntry> dpv = BI_TM_User_mgmt__c.BI_TM_Permission_Set__c.getDescribe().getPicklistValues();
		Map<String, String> permissionSetMap = new Map<String, String>();
		for(Schema.PicklistEntry pe : dpv){
			permissionSetMap.put(pe.getLabel(), pe.getValue());
		}
		return permissionSetMap;
	}
}