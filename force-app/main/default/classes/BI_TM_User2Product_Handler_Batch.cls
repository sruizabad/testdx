/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Create user to product from the user to position and the position to product records
*  @author:         Antonio Ferrero
*  @created:        24-May-2017
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         24-May-2017                 Antonio Ferrero             Construction of the class.
*/
global class BI_TM_User2Product_Handler_Batch implements Database.Batchable<sObject> {

	private static final String TO_ADDRESS = 'zzITM_SBITMANSFCoE@boehringer-ingelheim.com';
    private static final String EMAIL_SUBJECT = 'BITMAN - Errors Batch User To Product';

	String query = 'SELECT Id, BI_TM_UserCountryCode__c, BI_TM_Business__c, BI_TM_External_ID__c FROM BI_TM_User_mgmt__c';

	global BI_TM_User2Product_Handler_Batch() {
		this.query = query;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<BI_TM_User_mgmt__c> userList) {
		Set<Id> userIds = new Set<Id>();
		Set<Id> posIds = new Set<Id>();
		Set<Id> prodIds = new Set<Id>();

		Map<Id, BI_TM_User_mgmt__c> userMap = new Map<Id, BI_TM_User_mgmt__c>();
		Map<Id, Set<Id>> user2posMap = new Map<Id, Set<Id>>();
		Map<Id, Set<Id>> pos2prodMap = new Map<Id, Set<Id>>();
		Map<Id, Set<Id>> user2prodMap = new Map<Id, Set<Id>>();
		Map<Id, List<BI_TM_User_to_product__c>> user2prodMapUser = new Map<Id, List<BI_TM_User_to_product__c>>();

		List<BI_TM_User_to_product__c> user2prodList2UpsertAll = new List<BI_TM_User_to_product__c>();
		List<BI_TM_User_to_product__c> user2prodList2Upsert = new List<BI_TM_User_to_product__c>();

		for(BI_TM_User_mgmt__c u : userList){
			userIds.add(u.Id);
			userMap.put(u.Id, u);
		}

		/* Query active user to positions records */
		List<BI_TM_User_territory__c> user2posList = [SELECT Id, BI_TM_Territory1__c, BI_TM_User_mgmt_tm__c
																									FROM BI_TM_User_territory__c
																									WHERE BI_TM_User_mgmt_tm__c IN :userIds AND BI_TM_Active__c = TRUE];

		for(BI_TM_User_territory__c up : user2posList){
			posIds.add(up.BI_TM_Territory1__c);
			if(user2posMap.get(up.BI_TM_User_mgmt_tm__c) != null){
				Set<Id> posSet = user2posMap.get(up.BI_TM_User_mgmt_tm__c);
				posSet.add(up.BI_TM_Territory1__c);
				user2posMap.put(up.BI_TM_User_mgmt_tm__c, posSet);
			}
			else{
				Set<Id> posSet = new Set<Id>();
				posSet.add(up.BI_TM_Territory1__c);
				user2posMap.put(up.BI_TM_User_mgmt_tm__c, posSet);
			}
		}

		system.debug('position ids :: ' + posIds);

		/* Query active position to products records */
		List<BI_TM_Territory_to_product__c> pos2prodList = [SELECT Id, BI_TM_Territory_id__c, BI_TM_Mirror_Product__c
																												FROM BI_TM_Territory_to_product__c
																												WHERE BI_TM_Territory_id__c IN :posIds AND BI_TM_Active__c = TRUE];

		for(BI_TM_Territory_to_product__c pp : pos2prodList){
			prodIds.add(pp.BI_TM_Mirror_Product__c);
			if(pos2prodMap.get(pp.BI_TM_Territory_id__c) != null){
				Set<Id> prodSet = pos2prodMap.get(pp.BI_TM_Territory_id__c);
				prodSet.add(pp.BI_TM_Mirror_Product__c);
				pos2prodMap.put(pp.BI_TM_Territory_id__c, prodSet);
			}
			else{
				Set<Id> prodSet = new Set<Id>();
				prodSet.add(pp.BI_TM_Mirror_Product__c);
				pos2prodMap.put(pp.BI_TM_Territory_id__c, prodSet);
			}
		}

		// Build map with mirror product information
		Map<Id, BI_TM_Mirror_Product__c> prodMap = new Map<Id, BI_TM_Mirror_Product__c>([SELECT Id, BI_TM_External_ID__c
																																											FROM BI_TM_Mirror_Product__c
																																											WHERE Id IN :prodIds]);

		/* Query user to products records */
		List<BI_TM_User_to_product__c> user2prodList = [SELECT Id, BI_TM_User__c, BI_TM_User__r.BI_TM_UserCountryCode__c, BI_TM_Product__c, BI_TM_End_date__c, BI_TM_External_ID__c
																										FROM BI_TM_User_to_product__c
																										WHERE BI_TM_User__c IN :userIds AND BI_TM_No_Apply_Rules__c = FALSE];

		for(BI_TM_User_to_product__c upr : user2prodList){
			if(user2prodMap.get(upr.BI_TM_User__c) != null){
				Set<Id> prodSet = user2prodMap.get(upr.BI_TM_User__c);
				prodSet.add(upr.BI_TM_Product__c);
				user2prodMap.put(upr.BI_TM_User__c, prodSet);
			}
			else{
				Set<Id> prodSet = new Set<Id>();
				prodSet.add(upr.BI_TM_Product__c);
				user2prodMap.put(upr.BI_TM_User__c, prodSet);
			}
			if(user2prodMapUser.get(upr.BI_TM_User__c) != null){
				List<BI_TM_User_to_product__c> uprodList = user2prodMapUser.get(upr.BI_TM_User__c);
				uprodList.add(upr);
				user2prodMapUser.put(upr.BI_TM_User__c, uprodList);
			}
			else{
				List<BI_TM_User_to_product__c> uprodList = new List<BI_TM_User_to_product__c>();
				uprodList.add(upr);
				user2prodMapUser.put(upr.BI_TM_User__c, uprodList);
			}
		}

		/* For each user, check active positions and products assigned to those positions */
		for(Id u : userIds){
			system.debug('User :: ' + u);
			// Build product set per user
			Set<Id> user2productSet = new Set<Id>();
			if(user2posMap.get(u) != null){
				Set<Id> uPosSet = user2posMap.get(u);
				system.debug('Positions set for user :: ' + u + ', positions :: ' + uPosSet);
				for(Id posId : uPosSet){
					if(pos2prodMap.get(posId) != null){
						Set<Id> prodSet = pos2prodMap.get(posId);
						for(Id prod : prodSet){
							user2productSet.add(prod);
						}
					}
				}
			}

			system.debug('Product list :: ' + user2productSet);

			// User has products assigned in user to product
			if(user2prodMap.get(u) != null){
				Set<Id> currentProducts = user2prodMap.get(u);
				for(Id prod : user2productSet){
					// Check if the user already has a user to product related to the products that should be active
					if(currentProducts.contains(prod)){
						List<BI_TM_User_to_product__c> uprodList = user2prodMapUser.get(u);
						system.debug('Current User to products :: ' + uprodList);
						for(BI_TM_User_to_product__c up : uprodList){
							if(up.BI_TM_Product__c == prod && up.BI_TM_End_date__c != null){
								up.BI_TM_End_date__c = null;
								user2prodList2UpsertAll.add(up);
								system.debug('Update end date existing record :: ' + up);
							}
						}
					}
					// User has user to product records but not with the product that should have
					else{
						BI_TM_User_to_product__c up = new BI_TM_User_to_product__c();
						up.BI_TM_User__c = u;
						up.BI_TM_Product__c = prod;
						up.BI_TM_Country_Code__c = userMap.get(u).BI_TM_UserCountryCode__c;
						up.BI_TM_Business__c = userMap.get(u).BI_TM_Business__c;
						up.BI_TM_Start_Date__c = Date.Today();
						up.BI_TM_External_ID__c = userMap.get(u).BI_TM_UserCountryCode__c + '_' + userMap.get(u).BI_TM_External_ID__c + '_' + prodMap.get(prod).BI_TM_External_ID__c;
						user2prodList2UpsertAll.add(up);
						system.debug('Add user to product :: ' + up);
					}
				}
				// Inactivate user to products that user shouldn´t have active
				system.debug('Current products :: ' + currentProducts);
				for(Id prod : currentProducts){
					if(!user2productSet.contains(prod)){
						List<BI_TM_User_to_product__c> uprodList = user2prodMapUser.get(u);
						for(BI_TM_User_to_product__c up : uprodList){
							if(up.BI_TM_Product__c == prod && up.BI_TM_End_date__c == null){
								up.BI_TM_End_date__c = Date.Today();
								user2prodList2UpsertAll.add(up);
								system.debug('End date user to product :: ' + up);
							}
						}
					}
				}
			}

			// User has no products in user to product so we need to create them
			else{
				for(Id prod : user2productSet){
					BI_TM_User_to_product__c up = new BI_TM_User_to_product__c();
					up.BI_TM_User__c = u;
					up.BI_TM_Product__c = prod;
					up.BI_TM_Country_Code__c = userMap.get(u).BI_TM_UserCountryCode__c;
					up.BI_TM_Business__c = userMap.get(u).BI_TM_Business__c;
					up.BI_TM_Start_Date__c = Date.Today();
					up.BI_TM_External_ID__c = userMap.get(u).BI_TM_UserCountryCode__c + '_' + userMap.get(u).BI_TM_External_ID__c + '_' + prodMap.get(prod).BI_TM_External_ID__c;
					user2prodList2UpsertAll.add(up);
					system.debug('New user to product :: ' + up);
				}
			}
		}

		system.debug('User to product list to upsert :: ' + user2prodList2UpsertAll);

		// Remove user to products that have the "No Apply rules" flag checked, so we don´t override manual rules
		Set<String> user2prodExtIdSet = new Set<String>();
		for(BI_TM_User_to_product__c u2p : user2prodList2UpsertAll){
			user2prodExtIdSet.add(u2p.BI_TM_External_ID__c);
		}

		List<BI_TM_User_to_product__c> user2prodNoApplyRulesList = [SELECT Id, BI_TM_External_ID__c FROM BI_TM_User_to_product__c WHERE BI_TM_External_ID__c IN :user2prodExtIdSet AND BI_TM_No_Apply_Rules__c = TRUE];
		Map<String, BI_TM_User_to_product__c> user2prodNoApplyRulesMap = new Map<String, BI_TM_User_to_product__c>();
		for(BI_TM_User_to_product__c u2p : user2prodNoApplyRulesList){
			user2prodNoApplyRulesMap.put(u2p.BI_TM_External_ID__c, u2p);
		}

		// Build a map with the user to products without restrictions
		Map<String, BI_TM_User_to_product__c> user2prodAllMap = new Map<String, BI_TM_User_to_product__c>();
		for(BI_TM_User_to_product__c u2p : user2prodList2UpsertAll){
			user2prodAllMap.put(u2p.BI_TM_External_ID__c, u2p);
		}

		// Remove the Ids from the Set to upser
		Set<String> extIdsAllSet = new Set<String>(user2prodAllMap.keySet());
		Set<String> extIdsNoRulesSet = new Set<String>(user2prodNoApplyRulesMap.keySet());

		extIdsAllSet.removeAll(extIdsNoRulesSet);

		// Build the final list of user to product to load in the System
		for(BI_TM_User_to_product__c u2p : user2prodList2UpsertAll){
			if(extIdsAllSet.contains(u2p.BI_TM_External_ID__c)){
				user2prodList2Upsert.add(u2p);
			}
		}

		try{
			System.debug('Records to upsert: ' + user2prodList2Upsert);
			Database.upsert(user2prodList2Upsert, BI_TM_User_to_product__c.fields.BI_TM_External_ID__c, false);
		}
		catch(Exception ex){
			system.debug('An exception ocurred :: ' + ex.getMessage());
		}

	}

	private void processResults(Database.UpsertResult[] results) {
      String errorMessage = '';
      for (Database.UpsertResult ur : results) {
        if (!ur.isSuccess())
          for (Database.Error err : ur.getErrors()) {
            System.debug('Error: ' + err);
            errorMessage += ' - ' + err.getStatusCode() + ': ' + err.getMessage() + '\n';
          }
      }

      if (!String.isBlank(errorMessage)) {
        errorMessage = 'The following errors has occurred during mirror product batch database upsert operation: \n' + errorMessage;
        System.debug(errorMessage);

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String [] { TO_ADDRESS });
        mail.setSubject(EMAIL_SUBJECT);
        mail.setPlainTextBody(errorMessage);
        try {
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        } catch (Exception ex) {}       
      }
    }

	global void finish(Database.BatchableContext BC) {
		BI_TM_UserToProdMySetUpProdConf_Batch objBatch = new BI_TM_UserToProdMySetUpProdConf_Batch();
		database.executebatch(objBatch);
	}

}