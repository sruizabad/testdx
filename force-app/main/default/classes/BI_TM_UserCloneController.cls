/********************************************************************************
Name:  BI_TM_UserCloneController
Copyright ? 2016  Capgemini India Pvt Ltd
=================================================================
=================================================================
Clone user with new user name and BIDS ID.
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Vivek              23/12/2016  PHASE-4 DEVELOPMENT
*********************************************************************************/

public class BI_TM_UserCloneController{

    public BI_TM_User_mgmt__c userLocal {get; set;}
    public BI_TM_User_mgmt__c cloneUser {get; set;}

    public ID newRecordId {get; set;}
    public Id recId {get; set;}
    public String currentUserCountryCode {get; set;}

    public BI_TM_UserCloneController(ApexPages.StandardController controller) {
        recId = apexpages.currentpage().getparameters().get('id');
        system.debug('----->' + recId);

        userLocal = [SELECT BI_TM_Active__c,BI_TM_Address__c,BI_TM_Alias__c,BI_TM_BIDS_ID__c,BI_TM_BIGLOBALID__c,BI_TM_Business__c,BI_TM_COMMUNITYNICKNAME__c,BI_TM_Company_Code__c,BI_TM_Company_Name__c,BI_TM_Concur_User_ID__c,BI_TM_Cost_Center__c,BI_TM_Country__c,BI_TM_CPF_ID__c,BI_TM_Currency__c,BI_TM_Current_Hire_Date__c,BI_TM_Email__c,BI_TM_Email_Encoding__c,BI_TM_Employee_Number__c,BI_TM_Employee_Status__c,BI_TM_Employee_Type__c,BI_TM_End_date__c,BI_TM_External_Employee__c,BI_TM_External_ID__c,BI_TM_Fax__c,BI_TM_First_name__c,BI_TM_GBS_ID__c,BI_TM_Gender__c,BI_TM_Home_Phone__c,BI_TM_Is_Manager__c,BI_TM_LanguageLocaleKey__c,BI_TM_Last_Login__c,BI_TM_Last_name__c,BI_TM_LocaleSidKey__c,BI_TM_Manager_Id__c,BI_TM_Manager__c,BI_TM_Middle_Name__c,BI_TM_Network_Id__c,BI_TM_New_Hire__c,BI_TM_Operational_User__c,BI_TM_Original_Hire_Date__c,BI_TM_Permission_Set__c,BI_TM_Phone__c,BI_TM_Primary_Position__c,BI_TM_Profile__c,BI_TM_Rep_Sample_Eligible__c,BI_TM_Sample_Status__c,BI_TM_SAP_Sold_to_Id__c,BI_TM_Service_Date__c,BI_TM_SFDC_Instance_Id__c,BI_TM_Start_date__c,BI_TM_Suffix__c,BI_TM_Termination_Date__c,BI_TM_TimeZoneSidKey__c,BI_TM_Title_Description__c,BI_TM_UserCountryCode__c,BI_TM_UserCreate__c,BI_TM_UserId1__c,BI_TM_UserId_Lookup__c,BI_TM_UserId__c,BI_TM_Username__c,BI_TM_UserRole__c,BI_TM_User_Role__c,BI_TM_Visible_in_CRM__c,BI_TM_Voice_Mail__c,BI_TM_Warehouse__c,BI_TM_Work_Phone__c,Name FROM BI_TM_User_mgmt__c where id = :recId];

        system.debug('******' + cloneUser);
        cloneUser = new BI_TM_User_mgmt__c();
        //cloneUser.BI_TM_Active__c= userLocal.BI_TM_Active__c;
        cloneUser.BI_TM_Address__c= userLocal.BI_TM_Address__c;
        cloneUser.BI_TM_Alias__c= userLocal.BI_TM_Alias__c;
        cloneUser.BI_TM_BIDS_ID__c= userLocal.BI_TM_BIDS_ID__c;
        cloneUser.BI_TM_BIGLOBALID__c = userLocal.BI_TM_BIGLOBALID__c;
        cloneUser.BI_TM_Business__c= userLocal.BI_TM_Business__c;
        cloneUser.BI_TM_COMMUNITYNICKNAME__c= userLocal.BI_TM_COMMUNITYNICKNAME__c;
        cloneUser.BI_TM_Company_Code__c= userLocal.BI_TM_Company_Code__c;
        cloneUser.BI_TM_Company_Name__c= userLocal.BI_TM_Company_Name__c;
        cloneUser.BI_TM_Cost_Center__c= userLocal.BI_TM_Cost_Center__c;
        cloneUser.BI_TM_Country__c= userLocal.BI_TM_Country__c;
        cloneUser.BI_TM_CPF_ID__c = userLocal.BI_TM_CPF_ID__c;                                    
        cloneUser.BI_TM_Currency__c = userLocal.BI_TM_Currency__c; 
        cloneUser.BI_TM_Current_Hire_Date__c =userLocal.BI_TM_Current_Hire_Date__c;
        cloneUser.BI_TM_Email__c =userLocal.BI_TM_Email__c; 
        cloneUser.BI_TM_Email_Encoding__c =userLocal.BI_TM_Email_Encoding__c; 
        cloneUser.BI_TM_Employee_Number__c =userLocal.BI_TM_Employee_Number__c; 
        cloneUser.BI_TM_Employee_Status__c =userLocal.BI_TM_Employee_Status__c; 
        cloneUser.BI_TM_Employee_Type__c =userLocal.BI_TM_Employee_Type__c; 
        cloneUser.BI_TM_End_date__c =userLocal.BI_TM_End_date__c; 
        cloneUser.BI_TM_External_Employee__c =userLocal.BI_TM_External_Employee__c; 
        cloneUser.BI_TM_External_ID__c =userLocal.BI_TM_External_ID__c;
        cloneUser.BI_TM_Fax__c =userLocal.BI_TM_Fax__c;
        cloneUser.BI_TM_First_name__c =userLocal.BI_TM_First_name__c ;
        cloneUser.BI_TM_GBS_ID__c =userLocal.BI_TM_GBS_ID__c; 
        cloneUser.BI_TM_Gender__c =userLocal.BI_TM_Gender__c ;
        cloneUser.BI_TM_Home_Phone__c =userLocal.BI_TM_Home_Phone__c ;
        cloneUser.BI_TM_Is_Manager__c =userLocal.BI_TM_Is_Manager__c ;
        cloneUser.BI_TM_LanguageLocaleKey__c =userLocal.BI_TM_LanguageLocaleKey__c; 
        //cloneUser.BI_TM_Last_Login__c =userLocal.BI_TM_Last_Login__c ;
        cloneUser.BI_TM_Last_name__c =userLocal.BI_TM_Last_name__c ;
        cloneUser.BI_TM_LocaleSidKey__c =userLocal.BI_TM_LocaleSidKey__c ;
        cloneUser.BI_TM_Manager_Id__c =userLocal.BI_TM_Manager_Id__c ;
        cloneUser.BI_TM_Manager__c =userLocal.BI_TM_Manager__c ;
        cloneUser.BI_TM_Middle_Name__c =userLocal.BI_TM_Middle_Name__c ;
        cloneUser.BI_TM_Network_Id__c =userLocal.BI_TM_Network_Id__c ;
        cloneUser.BI_TM_New_Hire__c =userLocal.BI_TM_New_Hire__c ;
        cloneUser.BI_TM_Operational_User__c =userLocal.BI_TM_Operational_User__c;
        cloneUser.BI_TM_Original_Hire_Date__c =userLocal.BI_TM_Original_Hire_Date__c ;
        cloneUser.BI_TM_Permission_Set__c ='' ;
        cloneUser.BI_TM_Phone__c =userLocal.BI_TM_Phone__c ;
        cloneUser.BI_TM_Primary_Position__c =userLocal.BI_TM_Primary_Position__c ;
        cloneUser.BI_TM_Profile__c =userLocal.BI_TM_Profile__c ;
        cloneUser.BI_TM_Rep_Sample_Eligible__c =userLocal.BI_TM_Rep_Sample_Eligible__c ;
        cloneUser.BI_TM_Sample_Status__c =userLocal.BI_TM_Sample_Status__c ;
        cloneUser.BI_TM_SAP_Sold_to_Id__c =userLocal.BI_TM_SAP_Sold_to_Id__c ;
        cloneUser.BI_TM_Service_Date__c =userLocal.BI_TM_Service_Date__c ;
        cloneUser.BI_TM_Start_date__c =userLocal.BI_TM_Start_date__c ;
        cloneUser.BI_TM_Suffix__c =userLocal.BI_TM_Suffix__c;
        cloneUser.BI_TM_Termination_Date__c =userLocal.BI_TM_Termination_Date__c ;
        cloneUser.BI_TM_TimeZoneSidKey__c =userLocal.BI_TM_TimeZoneSidKey__c ;
        cloneUser.BI_TM_Title_Description__c =userLocal.BI_TM_Title_Description__c ;
        cloneUser.BI_TM_UserCountryCode__c =userLocal.BI_TM_UserCountryCode__c ;
        cloneUser.BI_TM_UserCreate__c =userLocal.BI_TM_UserCreate__c ;
        cloneUser.BI_TM_UserId1__c =userLocal.BI_TM_UserId1__c ;
        //cloneUser.BI_TM_UserId_Lookup__c =userLocal.BI_TM_UserId_Lookup__c ;
        cloneUser.BI_TM_UserId__c =userLocal.BI_TM_UserId__c ;
        cloneUser.BI_TM_Username__c =userLocal.BI_TM_Username__c ;
        cloneUser.BI_TM_UserRole__c =userLocal.BI_TM_UserRole__c;
        cloneUser.BI_TM_User_Role__c =userLocal.BI_TM_User_Role__c ;
        cloneUser.BI_TM_Visible_in_CRM__c=false;
        cloneUser.BI_TM_Voice_Mail__c =userLocal.BI_TM_Voice_Mail__c ;
        cloneUser.BI_TM_Warehouse__c =userLocal.BI_TM_Warehouse__c ;
        cloneUser.BI_TM_Work_Phone__c =userLocal.BI_TM_Work_Phone__c ;
        cloneUser.Name = userLocal.Name;
        
    }
    
    public PageReference cloneUser(){
     Savepoint sp = Database.setSavepoint();
     try {
            system.debug('new Record cloneUser before save =====' + cloneUser);
            cloneUser.BI_TM_Visible_in_CRM__c=false;
            insert cloneUser;
            newRecordId = cloneUser.id;
      }catch (Exception e) {
            Database.rollback(sp);
            ApexPages.addMessages(e);
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Value already exists for Below field.Please enter unique value');
            //ApexPages.addMessage(myMsg);
            return null;
      }
    return new PageReference('/' + newRecordId);           
    } 
}