public class BI_TM_UserInfo {

    private final String appId = 'TM';
    private static BI_TM_UserInfo instance = null;
    
    public Map<String,Set<String>> countryBusinessMap {get;private set;}
    public User userSettings { 
        get { 
        if (userSettings == null) {
            userSettings = [SELECT Country_Code_BI__c, Business_BI__c FROM User WHERE Id =: UserInfo.getUserId()];
        }
            return userSettings;
        } private set;
    }
    
    
    private BI_TM_UserInfo() {
        countryBusinessMap = getMap();
    }
    
    
    private Map<String,Set<String>> getMap() {
        Map<String,Set<String>> countryBusinessMap = new Map<String,Set<String>>();
        List<Group> groups = null;
        if (BI_TM_Utils.isAdmin()) {
            groups = getAllGroups();
        } else {
            groups = getGroupsForUser(UserInfo.getUserID());
        }
        System.debug(groups);
        for(Group g : groups) {
            if(g.Name.startsWith(appId) && g.Name.countMatches('_') == 2) {
                Integer startIndexCountry = g.Name.indexOf('_') + 1;
                Integer lastIndexCountry = g.Name.lastindexOf('_');
                String country = g.Name.Substring(startIndexCountry, lastIndexCountry );
                
                Integer startIndexBusiness = g.Name.lastindexOf('_') + 1;
                Integer lastIndexBusiness = g.Name.Length();
                String business = g.Name.Substring(startIndexBusiness, lastIndexBusiness );
                
                if (!countryBusinessMap.containsKey(country)) {
                    countryBusinessMap.put(country, new Set<String>());
                }
                Set<String> businesses = countryBusinessMap.get(country);
                businesses.add(business);
                countryBusinessMap.put(country,businesses);               
                
            }
        }
        return countryBusinessMap;
    }
    
    private List<Group> getAllGroups() {
        return [select id, name from group where name like 'TM_%' and name != 'TM_ADMIN'];
    }
        
    // return list of all groups the user belongs to via direct or indirect membership
    private List<Group> getGroupsForUser(Id userId){
        Set<Id> groupIds = getGroupsForIds(new Set<Id>{userId});
        return [select Id, Name from Group where Id IN: groupIds];
    }
    
    // return all ids the user belongs to via direct or indirect membership
    private Set<Id> getGroupsForIds(Set<Id> userOrGroupIds){
    
        Set<Id> output = new Set<Id>();
        Set<Id> nestedGroupIds = new Set<Id>();
    
        // only query actual groups and not roles and queues
        list<GroupMember> records = [select id, GroupId, UserOrGroupId from GroupMember where UserOrGroupId =: userOrGroupIds and UserOrGroupId != null and Group.Type = 'Regular'];
    
        for (GroupMember record:records)
        {
            // found a group, remember for traversal
            if (!(record.UserOrGroupId + '').startsWith('005'))
            {
                nestedGroupIds.add(record.UserOrGroupId);   
            }
            else
            {
                output.add(record.GroupId);
            }
        }
    
        // call self to get nested groups we found
        if (nestedGroupIds.size() > 0)
        {
            output.addAll(getGroupsForIds(nestedGroupIds));
        }
    
        return output;
    }


    public static BI_TM_UserInfo getInstance(){
        // lazy load the record type - only initialize if it doesn't already exist
        if(instance == null) instance = new BI_TM_UserInfo ();
        return instance;
    }

}