/********************************************************************************
Name:  BI_TM_UserManagementCreateEdit
Copyright ? 2015  Capgemini India Pvt Ltd

=================================================================
=================================================================
Visualforce controller to Create User in User Management
=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -    Shilpa              13/01/2016   INITIAL DEVELOPMENT
*********************************************************************************/

Public class BI_TM_UserManagementCreateEdit{
    public String BIDSId {get;set;}
    //public String BIDSId1 {get;set;}
    public BI_TM_BIDS__c BIDSListforupdate;
    public BI_TM_BIDS__c BIDSList {get;set;}
    public BI_TM_User_mgmt__c usrMgmntList {get;set;}
    transient List<Schema.PicklistEntry> ple = new List<Schema.PicklistEntry>();

    List<BI_TM_User_mgmt__c> umgnt= new List<BI_TM_User_mgmt__c>();
    List<BI_TM_User_mgmt__c> umgnt1= new List<BI_TM_User_mgmt__c>();
    public user userec {get;set;}
    //public userrole usrrolIdold {get;set;}
    private Map<String,Set<String>> countryBusinessMap = null;
    //public BI_TM_User_mgmt__c BI_TM_UserManagementCreateEdit{get;set;}
    //  public user user {get;set;}
    //_TM_User_mgmt__c usrMgmntList = new BI_TM_User_mgmt__c();
    public BI_TM_UserManagementCreateEdit(ApexPages.StandardController controller) {
     usrMgmntList = new BI_TM_User_mgmt__c();
     //this.BI_TM_User_mgmt__c=true;




      userec = new user();
       BIDSId = controller.getRecord() != null ? controller.getRecord().Id : null;
      // if(BIDSId.length()!=0){
        BIDSList=[select Name, BI_TM_BIPeople_Number__c, BI_TM_CPF_ID__c, BI_TM_GBS_ID__c, BI_TM_Login_Disabled__c,Id,BI_TM_Country_code__c,BI_TM_CN__c,BI_TM_Functional_Area_Text__c,BI_TM_Plant_Number__c,BI_TM_Email_Address__c,BI_TM_Facsmilenumber__c,BI_TM_Given_Name__c,BI_TM_Surname__c,BI_TM_Mobile_Number__c,BI_TM_Manager_Id__c,BI_TM_Is_Manager__c,BI_TM_Workforce_Id__c,BI_TM_Preffered_Given_Name__c,
                  BI_TM_Company_Code__c,BI_TM_Company_Name__c,BI_TM_Cost_Center__c,BI_TM_Employee_Type__c,BI_TM_Gender__c,BI_TM_Global_Id__c,BI_TM_Original_Hire_Date__c,BI_TM_Current_Hire_Date__c,BI_TM_New_Hire__c,BI_TM_SAP_Sold_to_Id__c,BI_TM_Service_Date__c,BI_TM_Warehouse__c,BI_TM_Work_Phone__c,BI_TM_Termination_Date__c,
                  BI_TM_Personal_Title__c,BI_TM_Job_Title__c,BI_TM_Voice_Mail__c,BI_TM_Middle_Name__c,BI_TM_Home_Phone__c,BI_TM_Employee_Status__c,BI_TM_Network_Id__c,BI_TM_External_Employee__c, BI_TM_Legacy_ID__c from BI_TM_BIDS__c where Id=:BIDSId];
        if(BIDSList.BI_TM_Preffered_Given_Name__c==NULL)
         BIDSList.BI_TM_Preffered_Given_Name__c=BIDSList.BI_TM_CN__c;
         system.debug('BIDS User Global Id====='+BIDSList.BI_TM_Global_Id__c);
       //}
       //String usrname=BIDSList[0].BI_TM_Email_Address__c;
       if(BIDSList.BI_TM_Email_Address__c ==null){
         apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Email Address Should not be null')) ;
       }
       if(BIDSList.BI_TM_Email_Address__c !=null){
         //umgnt=[select Id from BI_TM_User_mgmt__c where BI_TM_Username__c =:BIDSList.BI_TM_Email_Address__c.toLowerCase() limit 1 ];
          umgnt=[select Id,BI_TM_BIGLOBALID__c from BI_TM_User_mgmt__c where BI_TM_BIDS_User__c =:BIDSList.Id OR BI_TM_BIGLOBALID__c=:BIDSList.BI_TM_Global_Id__c limit 1 ];
          }
       if(umgnt.size()>0)
      usrMgmntList=[select BI_TM_Active__c,BI_TM_GBS_ID__c,BI_TM_CPF_ID__c, BI_TM_LanguageLocaleKey__c,BI_TM_Permission_Set__c,BI_TM_UserId__c,BI_TM_Start_date__c,BI_TM_TimeZoneSidKey__c,BI_TM_UserRole__c,BI_TM_User_Role__c,BI_TM_Email_Encoding__c,BI_TM_LocaleSidKey__c,BI_TM_End_date__c,BI_TM_Profile__c,BI_TM_UserCountryCode__c,BI_TM_Currency__c,BI_TM_Visible_in_CRM__c,BI_TM_External_Employee__c,BI_TM_BIDS_User__c,  BI_TM_BIGLOBALID__c,BI_TM_Username__c from BI_TM_User_mgmt__c where BI_TM_BIDS_User__c =:BIDSList.Id OR BI_TM_BIGLOBALID__c=:BIDSList.BI_TM_Global_Id__c limit 1];
     system.debug('Usermanagement List====='+usrMgmntList);
     system.debug('Usermanagement List====='+usrMgmntList.BI_TM_BIGLOBALID__c);
     //  countryBusinessMap  = BI_TM_UserInfo.getInstance().countryBusinessMap;

    }

    public PageReference edit1(){

    /*  if (!countryBusinessMap.containsKey(usrMgmntList.BI_TM_UserCountryCode__c)) {
                   apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Country Code not allowed')) ;
                   return null;

       } */
    //BIDSId1 = controller.getRecord() != null ? controller.getRecord().Id : null;


    BIDSListforupdate = new BI_TM_BIDS__c ();
    List<BI_TM_User_mgmt__c> usrmgntId= new List<BI_TM_User_mgmt__c>();
    list<User> stduser2= new List<User>();
    list<UserRole> usrrol = new list<UserRole>();

    //Below Line added on 06-03-2017 previously we are updating based on Username now we changed the Logic based on BIDS User Id in Usermanagement
    if(BIDSId != NULL){
       usrMgmntList.BI_TM_BIDS_User__c=BIDSId;

    }
    if(userec.UserRoleId!=NULL)
        usrrol=[select Id,Name from UserRole where Id=:userec.UserRoleId];
    if(BIDSList.BI_TM_Login_Disabled__c==TRUE ||BIDSList.BI_TM_Login_Disabled__c==false)
        //usrMgmntList.BI_TM_Active__c=BIDSList.BI_TM_Login_Disabled__c;
    if(BIDSList.BI_TM_Manager_Id__c!=NULL)
         stduser2=[SELECT Id FROM User where BIDS_ID_BI__c=:BIDSList.BI_TM_Manager_Id__c];
    if(BIDSList.BI_TM_Email_Address__c!=NULL)
        usrmgntId=[select Id from BI_TM_User_mgmt__c where BI_TM_BIDS_User__c =:BIDSList.Id OR BI_TM_BIGLOBALID__c=:BIDSList.BI_TM_Global_Id__c];
        usrMgmntList.BI_TM_Alias__c=generateAlias(BIDSList.BI_TM_Given_Name__c, BIDSList.BI_TM_Surname__c);
        usrMgmntList.BI_TM_Business__c=BIDSList.BI_TM_Functional_Area_Text__c;
        usrMgmntList.BI_TM_Email__c=BIDSList.BI_TM_Email_Address__c.toLowerCase();
        usrMgmntList.BI_TM_Fax__c=BIDSList.BI_TM_Facsmilenumber__c;
        usrMgmntList.BI_TM_First_name__c=BIDSList.BI_TM_Given_Name__c;
        usrMgmntList.BI_TM_Last_name__c=BIDSList.BI_TM_Surname__c;
        usrMgmntList.BI_TM_UserCountryCode__c=BIDSList.BI_TM_Country_Code__c;
        usrMgmntList.BI_TM_BIPeople_Number__c=BIDSList.BI_TM_BIPeople_Number__c;
        system.debug('BI people number :: ' + BIDSList.BI_TM_BIPeople_Number__c);
        usrMgmntList.BI_TM_CPF_ID__c= BIDSList.BI_TM_CPF_ID__c;
        usrMgmntList.BI_TM_GBS_ID__c= BIDSList.BI_TM_GBS_ID__c;

     //if(BIDSList!=NULL && BIDSList.BI_TM_Mobile_Number__c!=NULL)

  // UOR 7459005 start

        if(BIDSList!=NULL){
            usrMgmntList.BI_TM_Phone__c=BIDSList.BI_TM_Mobile_Number__c;
        }
// UOR 7459005 end



    if(BIDSList!=NULL  && BIDSList.BI_TM_Email_Address__c!=NULL){
        usrMgmntList.Name=BIDSList.BI_TM_Given_Name__c + ' ' + BIDSList.BI_TM_Surname__c;
        if(usrMgmntList.BI_TM_Username__c == null)
          usrMgmntList.BI_TM_Username__c=BIDSList.BI_TM_Email_Address__c.toLowerCase();
    }
    if(BIDSList!=NULL  && BIDSList.BI_TM_Manager_Id__c!=NULL)
        usrMgmntList.BI_TM_Manager_Id__c=BIDSList.BI_TM_Manager_Id__c;

    if(stduser2.size()>0)
        usrMgmntList.BI_TM_Manager__c=stduser2[0].Id;

    if(BIDSList.BI_TM_Is_Manager__c==true || BIDSList.BI_TM_Is_Manager__c==false)
        usrMgmntList.BI_TM_Is_Manager__c=BIDSList.BI_TM_Is_Manager__c;

  // if(BIDSList!=NULL  && BIDSList.BI_TM_Global_Id__c!=NULL)
    //    usrMgmntList.BI_TM_Employee_Number__c=BIDSList.BI_TM_Global_Id__c;


    if(BIDSList!=NULL  && BIDSList.BI_TM_Global_Id__c!=NULL)
        usrMgmntList.BI_TM_BIDS_ID__c=BIDSList.BI_TM_Global_Id__c;
        usrMgmntList.BI_TM_BIGLOBALID__C=BIDSList.BI_TM_Global_Id__c;
        usrMgmntList.BI_TM_Business__c=BIDSList.BI_TM_Functional_Area_Text__c;
        usrMgmntList.BI_TM_COMMUNITYNICKNAME__c=generateNickName(BIDSList.BI_TM_Given_Name__c, BIDSList.BI_TM_Surname__c);

    if(usrrol.size()>0) {
        usrMgmntList.BI_TM_UserRole__c=usrrol[0].Name;
    }
    usrMgmntList.BI_TM_Company_Name__c=BIDSList.BI_TM_Company_Name__c;
     if(BIDSList.BI_TM_Country_code__c=='US')
     {
         usrMgmntList.BI_TM_Company_Code__c=BIDSList.BI_TM_Company_Code__c;
         usrMgmntList.BI_TM_Cost_Center__c=BIDSList.BI_TM_Cost_Center__c;
         usrMgmntList.BI_TM_Legacy_ID__c=BIDSList.BI_TM_Legacy_ID__c;
         usrMgmntList.BI_TM_Employee_Type__c=BIDSList.BI_TM_Employee_Type__c;
         usrMgmntList.BI_TM_Gender__c=BIDSList.BI_TM_Gender__c;
         usrMgmntList.BI_TM_Original_Hire_Date__c=BIDSList.BI_TM_Original_Hire_Date__c;
         usrMgmntList.BI_TM_Current_Hire_Date__c=BIDSList.BI_TM_Current_Hire_Date__c;
         usrMgmntList.BI_TM_Network_Id__c =BIDSList.BI_TM_Network_Id__c;
         usrMgmntList.BI_TM_Home_Phone__c=BIDSList.BI_TM_Home_Phone__c;
         usrMgmntList.BI_TM_Middle_Name__c=BIDSList.BI_TM_Middle_Name__c;
         usrMgmntList.BI_TM_New_Hire__c=BIDSList.BI_TM_New_Hire__c;
         usrMgmntList.BI_TM_SAP_Sold_to_Id__c=BIDSList.BI_TM_SAP_Sold_to_Id__c;
         usrMgmntList.BI_TM_Service_Date__c=BIDSList.BI_TM_Service_Date__c;
         usrMgmntList.BI_TM_Termination_Date__c=BIDSList.BI_TM_Termination_Date__c;
         usrMgmntList.BI_TM_Voice_Mail__c=BIDSList.BI_TM_Voice_Mail__c;
         if(BIDSList.BI_TM_Warehouse__c != null){
          usrMgmntList.BI_TM_Warehouse__c=BIDSList.BI_TM_Warehouse__c;
         }
         usrMgmntList.BI_TM_Work_Phone__c=BIDSList.BI_TM_Work_Phone__c;
         usrMgmntList.BI_TM_Title_Description__c=BIDSList.BI_TM_Job_Title__c;
         usrMgmntList.BI_TM_Employee_Status__c =BIDSList.BI_TM_Employee_Status__c;
         usrMgmntList.BI_TM_External_Employee__c=BIDSList.BI_TM_External_Employee__c;
         usrMgmntList.BI_TM_Employee_Number__c=BIDSList.BI_TM_Workforce_Id__c;
     }
    try{
      if(usrmgntId.size()>0){
           usrMgmntList.Id=usrmgntId[0].Id;
           database.update(usrMgmntList);

      system.debug('BIDSList.Id------------->>>>>>>'+BIDSList.Id);
          BIDSListforupdate.Id=BIDSList.Id;
          BIDSListforupdate.BI_TM_Status__c='Updated';
      update BIDSListforupdate;
      }
      else{
      insert usrMgmntList;
          BIDSListforupdate.Id=BIDSList.Id;
          BIDSListforupdate.BI_TM_Status__c='Created';
      database.update(BIDSListforupdate);
      }
      return new PageReference('/' + BI_TM_User_mgmt__c.sobjecttype.getDescribe().getKeyPrefix() + '/o');
      }
      catch (system.Dmlexception e) {
                integer recordIndex=e.getDmlIndex(0);
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] { UserInfo.getUserEmail() };
                message.optOutPolicy = 'FILTER';
                message.subject = 'User setup failure for the User:';
                message.plainTextBody = 'Dear User,\r\n The user creation/Updation is not setup properly. Please contact your admin for help.\r\n \r\n Error: \r\n'+e.getDmlMessage(0) +'\r\n \r\n Thanks,\r\n'+'BITMAN';
                Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                system.debug (e);
        //system.debug (e);
       // apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error has Occured:'+e)) ;
        return new PageReference('/' + BI_TM_User_mgmt__c.sobjecttype.getDescribe().getKeyPrefix() + '/o');
      }
    }
    public PageReference cancel(){
        return new PageReference('/' + BI_TM_BIDS__c.sobjecttype.getDescribe().getKeyPrefix() + '/o');
    }

    private String generateNickName(String firstname, String lastname) {
        String nickname = firstname + '.' + lastname + '.' + DateTime.now().format('yyyymmddhhmm');
        return nickname.left(40);
    }

    private String generateAlias(String firstname, String lastname) {
        String alias = firstname.left(1) + lastname.left(4) ;
        return alias;
    }

}