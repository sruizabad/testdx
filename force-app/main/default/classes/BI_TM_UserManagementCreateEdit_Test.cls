/******************************************************************************** 
Name:  BI_TM_UserManagementCreateEdit_Test 
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Apex test Class for BI_TM_UserManagementCreateEdit Class
   
=================================================================
================================================================= 
History  -------
VERSION  AUTHOR              DATE         DETAIL                
1.0 -   Shilpa P              21/01/2016   INITIAL DEVELOPMENT
*********************************************************************************/
@isTest(SeeAllData=true)
private class BI_TM_UserManagementCreateEdit_Test {
  
   static testMethod void TestBI_TM_UserManagementCreateEdit() {
     User currentuser = new User();
      currentuser = [SELECT Country_Code_BI__c, Business_BI__c FROM User WHERE Id =: UserInfo.getUserId()];
      //currentuser = [Select Country_Code_BI__c From User Where Id = : UserInfo.getUserId()];
       //System.assertEquals(1, accts.size());
       BI_TM_BIDS__c bi = new BI_TM_BIDS__c();
      // bi.BI_TM_Country_code__c=currentuser.Country_Code_BI__c;
       bi.BI_TM_Given_Name__c='Shilpa';
       bi.BI_TM_Email_Address__c='shilpa.e.patil1@capgemini.com';
       bi.BI_TM_Is_Manager__c=false;
       bi.BI_TM_Mobile_Number__c='12345678';
       bi.BI_TM_Surname__c='Patil';
       bi.BI_TM_CN__c='gi000001';
       bi.BI_TM_Global_Id__c='gi000001';
       bi.BI_TM_Status__c='created';
       bi.BI_TM_Workforce_Id__c='gi000001';
       bi.BI_TM_Preffered_Given_Name__c='shilpa';
       bi.BI_TM_Manager_Id__c='gi000001';
       bi.BI_TM_Functional_Area_Text__c=currentuser.Business_BI__c;
       bi.BI_TM_Country_code__c=currentuser.Country_Code_BI__c;
       bi.BI_TM_Status__c='New';
       Test.StartTest();
       insert bi;
       Test.StopTest();
       BI_TM_User_role__c ur=new BI_TM_User_role__c ();
       ur.Name='Mexico';
       ur.BI_TM_Is_Root__c=True;
       ur.BI_TM_Visible_in_CRM__c=True;
       ur.BI_TM_Country_Code__c=currentuser.Country_Code_BI__c;
       ur.BI_TM_Business__c=currentuser.Business_BI__c;
       Insert ur;
       
       BI_TM_User_mgmt__c um = new BI_TM_User_mgmt__c();
       List<BI_TM_User_mgmt__c> ugmnt = new List<BI_TM_User_mgmt__c>();
       um.BI_TM_Active__c=False;
       um.BI_TM_Alias__c='Spatil';
       um.BI_TM_Business__c=currentuser.Business_BI__c;
       um.BI_TM_COMMUNITYNICKNAME__c='Spatil';
       um.BI_TM_Country__c=currentuser.Country_Code_BI__c;
       um.BI_TM_Profile__c='MX_SALES';
       um.BI_TM_LanguageLocaleKey__c='English';
       um.BI_TM_Email__c='shilpa.e.patil@capegemini.com';
       um.BI_TM_Username__c='shilpa.e.patil@capegemini.com';
       um.BI_TM_First_name__c='shilpa';
       um.BI_TM_Last_name__c='Patil';
       um.BI_TM_TimeZoneSidKey__c='America/Mexico_City';
       um.BI_TM_LocaleSidKey__c='English (United States)';
       um.BI_TM_Currency__c='Mexican Peso';
       um.BI_TM_UserCountryCode__c=currentuser.Country_Code_BI__c;
       um.BI_TM_User_Role__c=ur.Id;
      // ugmnt.add(um);
       //Test.startTest();
      // Insert ugmnt;
     BI_TM_User_mgmt__c um1= new BI_TM_User_mgmt__c();
       //um1.Id=ugmnt[0].Id;
       um1.BI_TM_COMMUNITYNICKNAME__c='Spatil';
       um1.BI_TM_Country__c=currentuser.Country_Code_BI__c;
       um1.BI_TM_Profile__c='MX_DATA_STEWARD';
       um1.BI_TM_LanguageLocaleKey__c='English';
       um1.BI_TM_Email__c='shilpa.e.patil1@capgemini.com';
       um1.BI_TM_Username__c='shilpa.e.patil1@capgemini.com';
       um1.BI_TM_First_name__c='shilpa1';
       um1.BI_TM_Last_name__c='Patil1';
       um1.BI_TM_LanguageLocaleKey__c='English';
       um1.BI_TM_TimeZoneSidKey__c='America/Mexico_City';
       um1.BI_TM_LocaleSidKey__c='English (United States)';
       um1.BI_TM_Currency__c='Mexican Peso';
       um1.BI_TM_UserCountryCode__c=currentuser.Country_Code_BI__c;
       Insert um1;
       BI_TM_BIDS__c b= new BI_TM_BIDS__c ();
       b.Id=bi.Id;
       b.BI_TM_Status__c='Updated';
       update b;
      // Test.stopTest();
      //  BI_TM_UserManagementCreateEdit usmgnt= new BI_TM_UserManagementCreateEdit();
        BI_TM_BIDS__c bId= new BI_TM_BIDS__c();
        bId=[select Id from BI_TM_BIDS__c Limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(bi);
        BI_TM_UserManagementCreateEdit usmgnt= new BI_TM_UserManagementCreateEdit(sc);
        
        usmgnt.usrMgmntList =um;
        usmgnt.BIDSListforupdate=bi;
        usmgnt.edit1();
        usmgnt.cancel();
        ApexPages.StandardController sc1 = new ApexPages.StandardController(bi);
        BI_TM_UserManagementCreateEdit usmgnt1= new BI_TM_UserManagementCreateEdit(sc1);
         usmgnt1.usrMgmntList =um1;
        //usmgnt.BIDSListforupdate=bi;
    /*    ApexPages.StandardController sc1 = new ApexPages.StandardController(b);
        BI_TM_UserManagementCreateEdit usmgnt1= new BI_TM_UserManagementCreateEdit(sc1);
        usmgnt1.usrMgmntList =um1;
        usmgnt1.BIDSListforupdate=b;
        usmgnt1.edit1();
        usmgnt1.cancel();*/
        
    }

}