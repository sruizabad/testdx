global class BI_TM_UserToPositionActivate_Bch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.stateful {

	String strQuery;
    global List<String> lstUsrPosIds;
    private Map<String,UserTerritory> mapIdXUserTerr;
	global BI_TM_UserToPositionActivate_Bch()
	{
		lstUsrPosIds=new List<String>();
		strQuery='select id,  BI_TM_Active__c , BI_TM_User_Territory_Created__c, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_Territory1__r.BI_TM_TerritoryID__c ';
		strQuery+='from BI_TM_User_territory__c order by BI_TM_User_mgmt_tm__c ';
		getMapUserTerritories();
		
	}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		System.debug(strQuery);
		return Database.getQueryLocator(strQuery);
	}

    public void getMapUserTerritories()
    {
        List<UserTerritory> lstUserterr=[select id, UserId, TerritoryId 
                                         from UserTerritory];
        mapIdXUserTerr=new Map<String,UserTerritory>();
        for(UserTerritory objUserTerr:lstUserTerr)
        {
            String strKey=(String)objUserTerr.UserId+(String)objUserTerr.TerritoryId;
            mapIdXUserTerr.put(strKey,objUserTerr);
        }
        
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
	    Set<String> setUserManIds=new set<String>();
	    for(SObject objScope:scope)
		{
			BI_TM_User_territory__c objUserTerr=(BI_TM_User_territory__c)objScope;
			if(!setUserManIds.contains(objUserTerr.BI_TM_User_mgmt_tm__c))
			{
			    setUserManIds.add(objUserTerr.BI_TM_User_mgmt_tm__c);
			}
		}
		
		List<BI_TM_User_territory__c> lstUserTerrTemp=[select id,  BI_TM_Active__c , BI_TM_User_Territory_Created__c, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_Territory1__r.BI_TM_TerritoryID__c
		                                               from BI_TM_User_territory__c 
		                                               where  BI_TM_User_mgmt_tm__c=:setUserManIds ]; 
		Map<String, List<BI_TM_User_territory__c>> mapUserManXlstUserTerr=new Map<String,List<BI_TM_User_territory__c>>();
		for(BI_TM_User_territory__c objUserTerrTmp: lstUserTerrTemp)
		{
		    if(mapUserManXlstUserTerr.containsKey(objUserTerrTmp.BI_TM_User_mgmt_tm__c))
		    {
		        mapUserManXlstUserTerr.get(objUserTerrTmp.BI_TM_User_mgmt_tm__c).add(objUserTerrTmp);
		    }
		    else
		    {
		        List<BI_TM_User_territory__c> lstUserTerrByUsr=new List<BI_TM_User_territory__c>();
		        lstUserTerrByUsr.add(objUserTerrTmp);
		        mapUserManXlstUserTerr.put(objUserTerrTmp.BI_TM_User_mgmt_tm__c,lstUserTerrByUsr);
		    }
		}
		for(SObject objScope:scope)
		{
			BI_TM_User_territory__c objUserTerr=(BI_TM_User_territory__c)objScope;
			String strKey=(String)objUserTerr.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c+(String)objUserTerr.BI_TM_Territory1__r.BI_TM_TerritoryID__c;
			if(objUserTerr.BI_TM_Active__c)
			{
			    if(!mapIdXUserTerr.containsKey(strKey))
			    {
			        activateUsrTerr(objUserTerr);
			    }
			    
			}
			else
			{
			   List<BI_TM_User_territory__c> lstUsrTerr=mapUserManXlstUserTerr.get(objUserTerr.BI_TM_User_mgmt_tm__c);
			   boolean blnInactive=true;
			   for(BI_TM_User_territory__c objUserTerrTmp:lstUsrTerr)
			   {
			       if(objUserTerrTmp.BI_TM_Active__c&&objUserTerr.BI_TM_Territory1__c==objUserTerrTmp.BI_TM_Territory1__c)
			       {
			          blnInactive=false;
			       }
			   }
			   if(blnInactive&&mapIdXUserTerr.containsKey(strKey))
			   {
			      deactivateUsrTerr(objUserTerr);
			   }
			}
		
		}
	


	}
	global void activateUsrTerr(BI_TM_User_territory__c objUserTerr)
	{
	    	if(objUserTerr.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c!=null&&objUserTerr.BI_TM_Territory1__r.BI_TM_TerritoryID__c!=null)
			{
				HttpRequest req = new HttpRequest();
    		    Http http = new Http();
    		    String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
                req.setEndpoint('callout:BITMAN');
                req.setMethod('POST');
                req.setHeader('Content-type', 'application/json');
                req.setHeader('Authorization', 'OAuth {!$Credential.OAuthToken}');
                req.setBody('{"UserId" :"'+objUserTerr.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c+'","TerritoryId" :"'+objUserTerr.BI_TM_Territory1__r.BI_TM_TerritoryID__c+'"}');
                system.debug('request body'+ req.getBody());
				HttpResponse response = http.send(req);
				if(response.getStatusCode()==201)
				{
					objUserTerr.BI_TM_User_Territory_Created__c=true;
				}
				else
				{
				    lstUsrPosIds.add(objUserTerr.id);
				}

			}
	}
		global void deactivateUsrTerr(BI_TM_User_territory__c objUserTerrBI)
	{
				String strKey=(String)objUserTerrBI.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c+(String)objUserTerrBI.BI_TM_Territory1__r.BI_TM_TerritoryID__c;
				if(mapIdXUserTerr.containsKey(strKey))
				{
    				UserTerritory objUserTerr=mapIdXUserTerr.get(strKey);
    				HttpRequest req = new HttpRequest();
    				Http http = new Http();
    				String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    				req.setEndpoint('callout:BITMAN/'+ objUserTerr.id);
    				req.setMethod('DELETE');
    				req.setHeader('Content-type', 'application/json');
    				req.setHeader('Authorization', 'OAuth {!$Credential.OAuthToken}');
    				HttpResponse response = http.send(req);
    				if(response.getStatusCode()==204||response.getStatusCode()==200)
    				{
    					
    				}
    				else
    				{
    				    lstUsrPosIds.add(objUserTerr.id);
    				}
				}	
	}
	
	

	global void finish(Database.BatchableContext BC)
	{
		//database.executeBatch(new BI_TM_UserToPositionDeactivate_Bch(),99);
	    database.executeBatch(new BI_TM_FF2Prod_ChildHandler_Batch(), 1);
		if(lstUsrPosIds!=null&&lstUsrPosIds.size()>0)
		{
		    List<Messaging.SingleEmailMessage> lstMails =new List<Messaging.SingleEmailMessage>();
		    String strBody;
			Messaging.SingleEmailMessage objMail = new Messaging.SingleEmailMessage();
			Organization myOrg = [Select instanceName From Organization o];
			objMail.setToAddresses(new String[]{'zzITM_SBITMANSFCoE@boehringer-ingelheim.com '});
			objMail.setSubject('BITMAN User to position Activation '+ myOrg.InstanceName);
			strBody='<html><p><span style="color: #000000;">Dear,</span></p> ';
			strBody+='<p><span style="color: #000000;">The next User To positions have had problems please review.</span></p>';
			strBody+='<p>&nbsp;</p>';

			for(String strUserToPosId:lstUsrPosIds)
			{
				strBody+='<a title="url1" href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+strUserToPosId+'">'+strUserToPosId+'</a></span></p>';
				strBody+='<p></p>';
			}

			strBody+='<p><span style="color: #000000;">Best Regards,</span></p>';
			strBody+='<p><span style="color: #000000;">BITMAN</span></p>';
			system.debug(strBody);
			objMail.setHtmlBody(strBody);
			lstMails.add(objMail);
			Messaging.sendEmail(lstMails);
		}
		
	
	
	}

}