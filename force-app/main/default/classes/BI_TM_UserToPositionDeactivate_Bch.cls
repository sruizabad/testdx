global class BI_TM_UserToPositionDeactivate_Bch implements Database.Batchable<sObject>, Database.AllowsCallouts {

	String strQuery;
    global List<String> lstUsrPosIds;
	global BI_TM_UserToPositionDeactivate_Bch()
	{
		lstUsrPosIds=new List<String>();
		strQuery='select id, BI_TM_User_Territory_Created__c, BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c, BI_TM_Territory1__r.BI_TM_TerritoryID__c ';
		strQuery+='from BI_TM_User_territory__c ';
		strQuery+='where BI_TM_User_Territory_Created__c=true and BI_TM_Active__c=false';
	}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		System.debug(strQuery);
		return Database.getQueryLocator(strQuery);
	}

  global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		List<BI_TM_User_territory__c > lstUserTerrToUpdate=new List<BI_TM_User_territory__c>();
		Map<String,BI_TM_User_territory__c> mapUserTerrIDByUserTerritory=new Map<String,BI_TM_User_territory__c>();
		List<String> lstUserIds=new List<String>();
		List<String> lstTerritoryIds=new List<String>();
		for(SObject objScope:scope)
		{
			BI_TM_User_territory__c objUserTerr=(BI_TM_User_territory__c)objScope;
			if(objUserTerr.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c!=null&&objUserTerr.BI_TM_Territory1__r.BI_TM_TerritoryID__c!=null)
			{
				String strKeyId=objUserTerr.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c+objUserTerr.BI_TM_Territory1__r.BI_TM_TerritoryID__c;
				if(!mapUserTerrIDByUserTerritory.containsKey(strKeyId))
				{
					mapUserTerrIDByUserTerritory.put(strKeyId,objUserTerr);
				}
				lstUserIds.add(objUserTerr.BI_TM_User_mgmt_tm__r.BI_TM_UserId_Lookup__c);
				lstTerritoryIds.add(objUserTerr.BI_TM_Territory1__r.BI_TM_TerritoryID__c);
			}

		}
		List<UserTerritory> lstUserTerritory=[select id, userId,territoryId
																					from userTerritory
																					where userId=:lstUserIds and territoryId =:lstTerritoryIds ];

		for(UserTerritory objUserTerr:lstUserTerritory)
		{
			String strKeyId=(String)objUserTerr.userId+(String)objUserTerr.territoryId;
			if(mapUserTerrIDByUserTerritory.containskey(strKeyId))
			{
				BI_TM_User_territory__c objBIUserTerr=mapUserTerrIDByUserTerritory.get(strKeyId);
				HttpRequest req = new HttpRequest();
				Http http = new Http();
				String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
				req.setEndpoint('callout:BITMAN/'+ objUserTerr.id);
				req.setMethod('DELETE');
				req.setHeader('Content-type', 'application/json');
				req.setHeader('Authorization', 'OAuth {!$Credential.OAuthToken}');
				HttpResponse response = http.send(req);
				if(response.getStatusCode()==204||response.getStatusCode()==200)
				{
					objBIUserTerr.BI_TM_User_Territory_Created__c=false;
					lstUserTerrToUpdate.add(objBIUserTerr);
				}
				else
				{
				    lstUsrPosIds.add(objUserTerr.id);
				}
			}
		}
		List<database.saveResult> lstSaveResult=Database.update(lstUserTerrToUpdate);
		for(Database.saveResult objSaveResult:lstSaveResult)
		{
		    if(!objSaveResult.isSuccess())
		    {
		        lstUsrPosIds.add(objSaveResult.getId());
		    }
		    
		}


	}

	global void finish(Database.BatchableContext BC)
	{
		database.executeBatch(new BI_TM_FF2Prod_ChildHandler_Batch(), 1);
		if(lstUsrPosIds!=null&&lstUsrPosIds.size()>0)
		{
		    List<Messaging.SingleEmailMessage> lstMails =new List<Messaging.SingleEmailMessage>();
		    String strBody;
			Messaging.SingleEmailMessage objMail = new Messaging.SingleEmailMessage();
			objMail.setToAddresses(new String[]{'zzITBITMANAdmins@boehringer-ingelheim.com'});
			objMail.setSubject('BITMAN User to position Deactivation');
			strBody='<html><p><span style="color: #000000;">Dear,</span></p> ';
			strBody+='<p><span style="color: #000000;">The next User To positions have had problems please review.</span></p>';
			strBody+='<p>&nbsp;</p>';

			for(String strUserToPosId:lstUsrPosIds)
			{
				strBody+='<a title="url1" href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+strUserToPosId+'">'+strUserToPosId+'</a></span></p>';
				strBody+='<p></p>';
			}

			strBody+='<p><span style="color: #000000;">Best Regards,</span></p>';
			strBody+='<p><span style="color: #000000;">BITMAN</span></p>';
			system.debug(strBody);
			objMail.setHtmlBody(strBody);
			lstMails.add(objMail);
			Messaging.sendEmail(lstMails);
		}
		
	}

}