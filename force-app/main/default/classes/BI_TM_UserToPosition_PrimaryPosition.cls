/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Update primary position in user management
*  @author:         Antonio Ferrero
*  @created:        29-Dec-2016
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         29-Dec-2016                 Antonio Ferrero             Construction of the class.
*/
public class BI_TM_UserToPosition_PrimaryPosition{

  public static Boolean primaryPositionCheck = false;

  public void updatePrimaryPosition(Map<Id, SObject> newMap){

    Map<Id, BI_TM_User_territory__c> userPosMap = new Map<Id, BI_TM_User_territory__c>();
    //Map<Id, String> userPosTypeMap = new Map<Id, String>();
    //Map<Id, String> usermngtPosTypeMap = new Map<Id, String>();
    Set<Id> positionsSet = new Set<Id>();
    Set<Id> usersSet = new Set<Id>();
    //Set<String> usersSetPosType = new Set<String>();
    Map<Id, String> userPosPrimaryMap = new Map<Id, String>();

    for(Id idSobject : newMap.keySet()){
      userPosMap.put(idSObject, (BI_TM_User_territory__c)newMap.get(idSobject));
    }


    // Retrieve user to position as primary
    for(Id idUserPos : userPosMap.keySet()){
      userPosPrimaryMap.put(userPosMap.get(idUserPos).BI_TM_User_mgmt_tm__c, '');
      system.debug('++++===userPosPrimaryMap===+++'+userPosPrimaryMap);
      usersSet.add(userPosMap.get(idUserPos).BI_TM_User_mgmt_tm__c);
      //usersSetPosType.add(userPosMap.get(idUserPos).BI_TM_User_mgmt_tm__r.BI_TM_Position_Type__c);
      if(userPosMap.get(idUserPos).BI_TM_Primary__c == true){
        positionsSet.add(userPosMap.get(idUserPos).BI_TM_Territory1__c);
      }
    }

    // Build a map to populate primary position with the primary and active user to position records
    for(Id idUserPos : userPosMap.keySet()){
      userPosPrimaryMap.put(userPosMap.get(idUserPos).BI_TM_User_mgmt_tm__c, '');
      usersSet.add(userPosMap.get(idUserPos).BI_TM_User_mgmt_tm__c);
      if(userPosMap.get(idUserPos).BI_TM_Primary__c == true && userPosMap.get(idUserPos).BI_TM_Active__c == true){
        positionsSet.add(userPosMap.get(idUserPos).BI_TM_Territory1__c);
      }
    }

    List<BI_TM_User_territory__c> userPosList = [Select Id, BI_TM_Primary__c, BI_TM_Territory1__c, BI_TM_Territory1__r.Name, BI_TM_User_mgmt_tm__c From BI_TM_User_territory__c Where BI_TM_User_mgmt_tm__c IN :usersSet AND BI_TM_Primary__c = true AND BI_TM_Active__c = true];
    //List<BI_TM_User_territory__c> userPosListforPosType = [Select Id,  BI_TM_Territory1__c, BI_TM_Territory1__r.BI_TM_Position_Type_Lookup__r.Name, BI_TM_User_mgmt_tm__c,BI_TM_Position_Type__c From BI_TM_User_territory__c Where BI_TM_User_mgmt_tm__c IN :usersSet];
    //List<BI_TM_User_mgmt__c> userPosTypeLst= [Select Id,BI_TM_Position_Type__c From BI_TM_User_mgmt__c Where Id IN :usersSet];
    Map<Id, BI_TM_Territory__c> positionMap = new Map<Id, BI_TM_Territory__c>([Select Id, Name From BI_TM_Territory__c Where Id IN :positionsSet]);
    Map<Id, BI_TM_User_mgmt__c> userMap = new Map<Id, BI_TM_User_mgmt__c>([Select Id, BI_TM_Primary_Position__c From BI_TM_User_mgmt__c Where Id IN :usersSet]);
    // With the input user to position and the existing ones, finish to build the map with the primary position per user
    for(Id idUserPos : userPosMap.keySet()){
      if((userPosMap.get(idUserPos).BI_TM_Primary__c == true) && (userPosMap.get(idUserPos).BI_TM_Active__c == true) && (userPosPrimaryMap.get(idUserPos) != null)){
        Id priPosition = userPosMap.get(idUserPos).BI_TM_Territory1__c;
        String namePosition = positionMap.get(priPosition).Name;
        userPosPrimaryMap.put(userPosMap.get(idUserPos).BI_TM_User_mgmt_tm__c, namePosition);
      }
    }

    if(userPosList.size() > 0){
      for(BI_TM_User_territory__c up : userPosList){
        userPosPrimaryMap.put(up.BI_TM_User_mgmt_tm__c, up.BI_TM_Territory1__r.Name);
      }
    }
    //Added from Lines 66 to 70 and Line 80 on 10-04-2017 as part of CR to update the Position Type on Usermanagement
    /* if(userPosListforPosType.size() > 0){
      for(BI_TM_User_territory__c up : userPosListforPosType){
        userPosTypeMap.put(up.BI_TM_User_mgmt_tm__c, up.BI_TM_Territory1__r.BI_TM_Position_Type_Lookup__r.Name);
      }
    } */

    /* if(userPosTypeLst.size() > 0){
      for(BI_TM_User_mgmt__c postype : userPosTypeLst){
        usermngtPosTypeMap.put(postype.Id, postype.BI_TM_Position_Type__c);
      }
    }*/

    // Prepare the list of users to be updates with the value of the primary position

    List<BI_TM_User_mgmt__c> user2update = new List<BI_TM_User_mgmt__c>();

    for(Id u : userMap.keySet()){
      if(userPosPrimaryMap.containsKey(u)){
        if((String.isBlank(userMap.get(u).BI_TM_Primary_Position__c) && String.isBlank(userPosPrimaryMap.get(u))) || (userMap.get(u).BI_TM_Primary_Position__c != userPosPrimaryMap.get(u))){
          BI_TM_User_mgmt__c user = userMap.get(u);
          user.BI_TM_Primary_Position__c = userPosPrimaryMap.get(u);
          //user.BI_TM_Position_Type__c = userPosTypeMap.get(u);
            //if(user.BI_TM_Position_Type__c =='Territory' && (usermngtPosTypeMap!= null || ! usermngtPosTypeMap.containskey('Territory'))){
                //user.BI_TM_Created_Today__c =system.today();
            //}
          user2update.add(user);
        }
      }
    }

    if(user2update.size() > 0){
      try{
        primaryPositionCheck = true;
        update user2update;
      } catch(Exception ex){
        system.debug('Exception updating primary position :: ' + ex.getMessage());
      }
    }

  }
}