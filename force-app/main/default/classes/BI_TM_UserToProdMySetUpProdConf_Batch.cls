global class BI_TM_UserToProdMySetUpProdConf_Batch implements Database.Batchable<sObject> {

	String strQuery;

	global BI_TM_UserToProdMySetUpProdConf_Batch()
	{
		strQuery='select BI_TM_My_setup_product_id__c,BI_TM_Product__r.BI_TM_Product_Catalog_Id__c,BI_TM_User__r.BI_TM_UserId_Lookup__c, BI_TM_Country_Code__c,BI_TM_End_date__c,BI_TM_Start_date__c from BI_TM_User_to_product__c';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(strQuery);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
			BI_TM_User_to_productHandler userToProdctr=new BI_TM_User_to_productHandler();
			userToProdctr.createMySetupProducts(scope,false);

	}

	global void finish(Database.BatchableContext BC)
	{
		BI_TM_BatchToUpdateMirrorProduct obj = new BI_TM_BatchToUpdateMirrorProduct();
		database.executebatch(obj);  
	}

}