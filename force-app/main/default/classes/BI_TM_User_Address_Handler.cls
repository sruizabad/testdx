public class BI_TM_User_Address_Handler implements BI_TM_ITriggerHandler
{

    
    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void BeforeDelete(Map<Id, SObject> oldItems) {
    /*if(trigger.isAfter && trigger.isDelete ){
         set<string> rrName = new set<string>();
          system.debug('trigger.oldmap.keyset()+++++++'+trigger.oldmap.keyset());
            List<BI_TM_User_Address__c> UAList= new List<BI_TM_User_Address__c>();
            UAList=[select BI_TM_User__r.BI_TM_Username__c,Id from BI_TM_User_Address__c where Id IN:trigger.oldmap.keyset()];
            //Map<String,string> UAMap= new Map<String,string>();
            for(BI_TM_User_Address__c ua:UAList){
            system.debug('ua.BI_TM_User__r.BI_TM_Username__c++++++++++++++++++++'+ua.BI_TM_User__r.BI_TM_Username__c);
             //UAMap.put(ua.BI_TM_User__r.BI_TM_Username__c,ua.Id);
             rrName.add(ua.BI_TM_User__r.BI_TM_Username__c);
            }
            List<Rep_Roster_vod__c> rrList= new List<Rep_Roster_vod__c>();
            rrList=[select Id from Rep_Roster_vod__c where Username_External_Id_vod__c in:rrName];
            if(rrList.size()>0)
            {
                Database.delete(rrList);
            }
       
       }*/
        
    }

    public void AfterInsert(Map<Id, SObject> newItems) {
       
        BI_TM_ReprosterHandler reproster= new BI_TM_ReprosterHandler(trigger.newmap.keyset()); 
        BI_TM_MailAddressHandler mailAddress = new BI_TM_MailAddressHandler(newItems);  
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        BI_TM_ReprosterHandler reproster= new BI_TM_ReprosterHandler(trigger.newmap.keyset());
        BI_TM_MailAddressHandler mailAddress = new BI_TM_MailAddressHandler(newItems); 
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {
        if(trigger.isAfter && trigger.isDelete ){
         set<string> rrName = new set<string>();
         set<string> usrName = new set<string>();
         List<BI_TM_User_Address__c> UAdelList= new List<BI_TM_User_Address__c>();
         
         List<BI_TM_User_mgmt__c> usrdelList= new List<BI_TM_User_mgmt__c>();
          for(BI_TM_User_Address__c u:(List<BI_TM_User_Address__c>)trigger.old)
          {
          system.debug('u.BI_TM_User__r.BI_TM_Username__c+++++++'+u.BI_TM_User__r.BI_TM_Username__c);
            usrName.add(u.BI_TM_User__c);
          }
           usrdelList=[select BI_TM_Username__c,Id from BI_TM_User_mgmt__c where Id IN:usrName];
           for(BI_TM_User_mgmt__c ua:usrdelList){
            system.debug('ua.BI_TM_User__r.BI_TM_Username__c++++++++++++++++++++'+ua.BI_TM_Username__c);
             rrName.add(ua.BI_TM_Username__c);
             }
            system.debug('rrNamec++++++++++++++++++++'+rrName);
            List<Rep_Roster_vod__c> rrList= new List<Rep_Roster_vod__c>();
            rrList=[select Id from Rep_Roster_vod__c where Username_External_Id_vod__c in:rrName];
            if(rrList.size()>0)
            {
                Database.delete(rrList);
            }
       
       }
        
    }

    public void AfterUndelete(Map<Id, SObject> oldItems) {
        

    
    }
    
    
}