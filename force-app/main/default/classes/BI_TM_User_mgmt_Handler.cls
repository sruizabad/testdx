public class BI_TM_User_mgmt_Handler implements BI_TM_ITriggerHandler
{

  public static boolean hasRun = false;

  public void BeforeInsert(List<SObject> newItems) {
  }

  public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
  }

  public void BeforeDelete(Map<Id, SObject> oldItems) {
  }

  public void AfterInsert(Map<Id, SObject> newItems) {
    BI_TM_User_mgmtinsertingstdUserHandler userinsert =new BI_TM_User_mgmtinsertingstdUserHandler();
    if(!hasRun){
      BI_TM_User_mgmtinsertingstdUserHandler.CreateUsermgmnt((List<BI_TM_User_mgmt__c>)newItems.values());
      hasRun = true;
    }
  }

  public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    BI_TM_User_mgmtinsertingstdUserHandler userinsert = new BI_TM_User_mgmtinsertingstdUserHandler();
    if(!hasRun){
      if(!System.isBatch() && !System.isFuture()){
        BI_TM_User_mgmtinsertingstdUserHandler.managePermissionSet(newItems, oldItems);
        BI_TM_User_mgmtinsertingstdUserHandler.CreateUsermgmnt((List<BI_TM_User_mgmt__c>)newItems.values());
        BI_TM_User_mgmtinsertingstdUserHandler.updateUser((List<BI_TM_User_mgmt__c>)newItems.values());
        hasRun = true;
      }
    }
  }

  public void AfterDelete(Map<Id, SObject> oldItems) {
  }

  public void AfterUndelete(Map<Id, SObject> oldItems) {
  }
  
}