/********************************************************************************
Name:  BI_TM_User_mgmtinsertingstdUserHandler
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Trigger handler to insert user in veeva user object

=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -   Shilpa P              21/01/2016   INITIAL DEVELOPMENT
*********************************************************************************/
public class BI_TM_User_mgmtinsertingstdUserHandler{

    public static void CreateUsermgmnt (List<BI_TM_User_mgmt__c> userList){
      List<Id> userIdList = new List<Id>();
      for(BI_TM_User_mgmt__c u : userList){
        userIdList.add(u.Id);
      }
      if(!System.isBatch() && !System.isFuture()){
          manageUsers(userIdList);//Method to insert or update the user object in Veeva
      }
    }

    @future
    private static void manageUsers(List<Id> userIdList){
        List<User> userList2Insert = new List<User>();
        List<User> userList2Update = new List<User>();

        List<BI_TM_User_mgmt__c> umList = [SELECT Name, BI_TM_Alias__c, BI_TM_BIGLOBALID__c, BI_TM_Business__c, BI_TM_Credentials__c, BI_TM_COMMUNITYNICKNAME__c, BI_TM_Currency__c, BI_TM_Email__c,
                                                                            BI_TM_Email_Encoding__c, BI_TM_Employee_Number__c, BI_TM_Company_Name__c, BI_TM_Title_Description__c, BI_TM_End_date__c, BI_TM_Fax__c, BI_TM_First_name__c,
                                                                            BI_TM_LanguageLocaleKey__c, BI_TM_Last_name__c, BI_TM_LocaleSidKey__c, BI_TM_UserId__c, BI_TM_Manager__c, BI_TM_User_Role__r.Name, BI_TM_Phone__c,
                                                                            BI_TM_Profile__c, BI_TM_Start_date__c, BI_TM_TimeZoneSidKey__c, BI_TM_UserCountryCode__c, BI_TM_Username__c, BI_TM_User_Role__c, BI_TM_Permission_Set__c,
                                                                            BI_TM_UserId_Lookup__c, BI_TM_Primary_Position__c, BI_TM_Oracle_Location_ID__c, BI_TM_Warehouse__c  FROM BI_TM_User_mgmt__c WHERE Id IN :userIdList AND BI_TM_Visible_in_CRM__c = true];

        Set<String> userRoleSet = new Set<String>();
        Set<String> userProfileSet = new Set<String>();
        for(BI_TM_User_mgmt__c um : umList){
            userRoleSet.add(um.BI_TM_User_Role__r.Name);
            userProfileSet.add(um.BI_TM_Profile__c);
        }

        Map<String, Id> userRoleMap = getUserRoleMap(userRoleSet);
        Map<String, Id> userProfileMap = getUserProfileMap(userProfileSet);

        for(BI_TM_User_mgmt__c um : umList){
            User u = new User();
            u.FirstName = um.BI_TM_First_name__c;
            u.LastName = um.BI_TM_Last_name__c;
            u.Username = um.BI_TM_Username__c ;
            u.Email = um.BI_TM_Email__c;
            u.CompanyName = um.BI_TM_Company_Name__c;
            u.Title = um.BI_TM_Title_Description__c;
            if(um.BI_TM_User_Role__r.Name != null && userRoleMap.get(um.BI_TM_User_Role__r.Name) != null){
                u.UserRoleId = userRoleMap.get(um.BI_TM_User_Role__r.Name);
            }
            if(um.BI_TM_Profile__c != null){
                u.ProfileId = userProfileMap.get(um.BI_TM_Profile__c);
            }
            u.IsActive = getActiveStatus(um);
            u.Alias = um.BI_TM_Alias__c;
            u.CommunityNickname = um.BI_TM_COMMUNITYNICKNAME__c;
            u.TimeZoneSidKey = um.BI_TM_TimeZoneSidKey__c;
            u.LanguageLocaleKey = um.BI_TM_LanguageLocaleKey__c;
            u.LocaleSidKey = um.BI_TM_LocaleSidKey__c;
            u.EmailEncodingKey = um.BI_TM_Email_Encoding__c;
            u.Business_BI__c = um.BI_TM_Business__c;
            u.Country_Code_BI__c = um.BI_TM_UserCountryCode__c;
            u.DefaultcurrencyIsoCode = um.BI_TM_Currency__c;
            u.BIDS_ID_BI__c = um.BI_TM_BIGLOBALID__c;
            u.ManagerId = um.BI_TM_Manager__c;
            u.Fax = um.BI_TM_Fax__c;
            u.MobilePhone = um.BI_TM_Phone__c;
            u.National_Sales_Manager_BI__c = um.BI_TM_UserId__c;
            u.Concur_User_Id_vod__c = um.BI_TM_Email__c;
            u.EmployeeNumber = um.BI_TM_Employee_Number__c;
            u.Country = um.BI_TM_UserCountryCode__c;
            u.Credentials_BI__c = um.BI_TM_Credentials__c;
            u.Primary_Territory_vod__c = um.BI_TM_Primary_Position__c;
            if(um.BI_TM_BIGLOBALID__c != null){
                u.External_ID__c = um.BI_TM_UserCountryCode__c + um.BI_TM_BIGLOBALID__c;
            }
            if(um.BI_TM_Oracle_Location_ID__c != null){
                u.Oracle_Location_ID_BI__c = um.BI_TM_Oracle_Location_ID__c;
            }
            if(um.BI_TM_Warehouse__c != null){
                u.Warehouse_ID_BI__c = um.BI_TM_Warehouse__c;
            }

            if(um.BI_TM_UserId_Lookup__c != null){
                u.Id = um.BI_TM_UserId_Lookup__c;
                userList2Update.add(u);
            }
            else {
                userList2Insert.add(u);
            }
        }

        Set<Id> userIds = new Set<Id>();
        if(userList2Update.size() > 0){
            Database.SaveResult[] updateSrList = Database.update(userList2Update, false);
            manageErrorLogSave(updateSrList, true);
            for(Database.SaveResult sr : updateSrList){
              if(sr.isSuccess()){
                userIds.add(sr.getId());
              }
            }
        }
        if(userList2Insert.size() > 0){
            Database.SaveResult[] insertSrList = Database.insert(userList2Insert, false);
            manageErrorLogSave(insertSrList, true);
            updateLookUpUser(userList2Insert);
            for(Database.SaveResult sr : insertSrList){
              if(sr.isSuccess()){
                userIds.add(sr.getId());
              }
            }
        }

        // Get map with users that have the SSO enabled
        Map<Id, boolean> userIsSSOMap = BI_TM_Utils.getUserIsSSOMap(userIds);

        for(User u : [SELECT Id, ProfileId FROM User WHERE isActive = TRUE AND Id IN :userIds AND LastLoginDate = null]){
          try {
            if(!userIsSSOMap.keySet().contains(u.Id)){
              System.resetPassword(u.Id, true);
            }
          } catch(Exception ex) {
            System.debug('*** exception: ' + ex.getMessage());
          }
        }
    }

    // Get user role map with the name and the Id
    private static Map<String, Id> getUserRoleMap(Set<String> stringSet){
        Map<String, Id> userRoleMap = new Map<String, Id>();
        for(UserRole ur : [SELECT Id, Name FROM UserRole WHERE Name IN :stringSet]){
            userRoleMap.put(ur.Name, ur.Id);
        }
        return userRoleMap;
    }

    // Get user profile map with the name and the Id
    private static Map<String, Id> getUserProfileMap(Set<String> stringSet){
        Map<String, Id> userProfileMap = new Map<String, Id>();
        for(Profile pr : [SELECT Id, Name FROM Profile WHERE Name IN :stringSet]){
            userProfileMap.put(pr.Name, pr.Id);
        }
        return userProfileMap;
    }

    // Update lookup in user management to user
    private static void updateLookUpUser(List<User> userList){
        Map<String, Id> userLookupMap = new Map<String, Id>();
        List<BI_TM_User_mgmt__c> umList2Update = new List<BI_TM_User_mgmt__c>();
        for(User u : userList){
            userLookupMap.put(u.username, u.Id);
        }
        for(BI_TM_User_mgmt__c um : [SELECT Id, BI_TM_UserId_Lookup__c, BI_TM_Username__c, BI_TM_Active__c, BI_TM_Start_date__c, BI_TM_End_date__c FROM BI_TM_User_mgmt__c WHERE BI_TM_Username__c IN :userLookupMap.keySet()]){
            um.BI_TM_UserId_Lookup__c = userLookupMap.get(um.BI_TM_Username__c);
            um.BI_TM_Active__c = getActiveStatus(um);
            umList2Update.add(um);
            system.debug('um :: ' + um);
        }
        Database.SaveResult[] updateSrList = Database.update(umList2Update, false);
        manageErrorLogSave(updateSrList, false);
    }

    /* Update attributes in the User related to the user management*/
    public static void updateUser(List<BI_TM_User_mgmt__c> usermList){
        Set<Id> userIds = new Set<Id>();
        for(BI_TM_User_mgmt__c um : usermList){
            userIds.add(um.BI_TM_UserId_Lookup__c);
        }

        List<User> userList = [SELECT Id, Primary_Territory_vod__c FROM User WHERE Id IN :userIds];
        List<User> users2updateList = new List<User>();
        for(User u : userList){
            for(BI_TM_User_mgmt__c um : usermList){
                if(u.Id == um.BI_TM_UserId_Lookup__c && u.Primary_Territory_vod__c != um.BI_TM_Primary_Position__c){
                    u.Primary_Territory_vod__c = um.BI_TM_Primary_Position__c;
                    users2updateList.add(u);
                    break;
                }
            }
        }

        if(users2updateList.size() > 0){
            Database.SaveResult[] updateSrList = Database.update(users2updateList, false);
            manageErrorLogSave(updateSrList, true);
        }
    }

    // Check if user management is active with the start/end dates
    private static Boolean getActiveStatus(BI_TM_User_mgmt__c um){
        return um.BI_TM_End_date__c != null ? (um.BI_TM_Start_date__c <= System.Today() &&  um.BI_TM_End_date__c > System.Today()) : um.BI_TM_Start_date__c <= System.Today();
    }

    // Build map with permission set names and ids
    @TestVisible
    private static Map<String, Id> getPermissionSetsMap(){
        List<Schema.PicklistEntry> dpv = BI_TM_User_mgmt__c.BI_TM_Permission_Set__c.getDescribe().getPicklistValues();
        Map<String, Id> permissionSetMap = new Map<String, Id>();
        Set<String> psDevNames = new Set<String>();
        for(Schema.PicklistEntry pe : dpv){
            psDevNames.add(pe.getValue());
        }
        for(PermissionSet ps : [SELECT Id, Name FROM PermissionSet WHERE Name IN :psDevNames]){
            permissionSetMap.put(ps.Name, ps.Id);
        }
        return permissionSetMap;
    }

    // Check permission set lists of users, prepare list of permissions to add and remove
    public static void managePermissionSet(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        List<String> permissionSetUm2AddList = new List<String>(); // List to hold permission set value to add in the user management if different in the update
        List<String> permissionSetUm2RemoveList = new List<String>(); // List to hold permission set value to remove in the user management if different in the update

        for(Id ni : newItems.keySet()){
            if(((BI_TM_User_mgmt__c)newItems.get(ni)).BI_TM_UserId_Lookup__c != null){
                String niPs = ((BI_TM_User_mgmt__c)newItems.get(ni)).BI_TM_Permission_Set__c != null ? ((BI_TM_User_mgmt__c)newItems.get(ni)).BI_TM_Permission_Set__c : '';
                String oiPs = ((BI_TM_User_mgmt__c)oldItems.get(ni)).BI_TM_Permission_Set__c != null ? ((BI_TM_User_mgmt__c)oldItems.get(ni)).BI_TM_Permission_Set__c : '';
                if(niPs != oiPs){
                    // Build lists of permission sets with the user id
                    String newPermissionSet = comparePermissionSet(niPs, oiPs);
                    String removePermissionSet = comparePermissionSet(oiPs, niPs);
                    String newPermissionSetString = (String)((BI_TM_User_mgmt__c)newItems.get(ni)).BI_TM_UserId_Lookup__c + ';' + newPermissionSet;
                    String removePermissionSetString = (String)((BI_TM_User_mgmt__c)newItems.get(ni)).BI_TM_UserId_Lookup__c + ';' + removePermissionSet;
                    permissionSetUm2AddList.add(newPermissionSetString);
                    permissionSetUm2RemoveList.add(removePermissionSetString);
                }
            }
        }
        addPermissionSet(permissionSetUm2AddList);
        removePermissionSet(permissionSetUm2RemoveList);
    }

    // Add the permission set to the users
    @future
    public static void addPermissionSet(List<String> permissionSet2Add){
        List<PermissionSetAssignment> psAssignmentList = new List<PermissionSetAssignment>();
        Map<String, Id> permissionSetMap = getPermissionSetsMap();
        List<PermissionSetAssignment> psaList2Insert = new List<PermissionSetAssignment>();

        for(String key : permissionSet2Add){
            String userId = getUserFromString(key);
            List<String> permissionListUser = getPermissionSetsUser(key);
            for(String ps : permissionListUser){
                if(permissionSetMap.get(ps) != null){
                    PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = (Id)userId, PermissionSetId = permissionSetMap.get(ps));
                    psAssignmentList.add(psa);
                }
            }
        }

        // Insert permission set assignments
        Database.SaveResult[] srList = Database.insert(psAssignmentList, false);
        manageErrorLogSave(srList, false);
    }

    // Remove permission set from user
    @future
    public static void removePermissionSet(List<String> permissionSet2Remove){
        Map<String, Id> permissionSetMap = getPermissionSetsMap();
        List<PermissionSetAssignment> psaList2Delete = new List<PermissionSetAssignment>();
        List<Id> userIdList = new List<Id>();
        Map<String, Set<String>> userPermissionMap = getUserPermissionMap(permissionSet2Remove);
        for(String key : permissionSet2Remove){
            userIdList.add((Id)getUserFromString(key));
        }
        for(PermissionSetAssignment psa : [SELECT Id, AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId IN :userIdList]){
            if(userPermissionMap.get((String)psa.AssigneeId) != null && (userPermissionMap.get((String)psa.AssigneeId)).size() > 0){
                Set<String> psSet = userPermissionMap.get((String)psa.AssigneeId);
                if(psSet.contains(psa.PermissionSet.Name) && permissionSetMap.get(psa.PermissionSet.Name) != null){
                    psaList2Delete.add(psa);
                }
            }
        }
        // Delete permission set assignments
        Database.DeleteResult[] srList = Database.delete(psaList2Delete, false);
        manageErrorLogDelete(srList, false);
    }

    // Compare Permission Set
    private static String comparePermissionSet(String ps1, String ps2){
        String comp = '';
        List<String> psSource = splitPermissionSet(ps1);
        List<String> pstarget = splitPermissionSet(ps2);
        Set<String> pstargetSet = new Set<String>(pstarget);
        for(String str : psSource){
            if(!pstargetSet.contains(str)){
                comp += str + ';';
            }
        }
        return comp;
    }

    private static List<String> splitPermissionSet(String ps){
        return ps.split(';');
    }

    private static String getUserFromString(String str){
        return str.split(';').get(0);
    }

    private static List<String> getPermissionSetsUser(String str){
        String ps = str.split(';', 2).get(1);
        return ps.split(';');
    }

    // Build map with the user id and a set of the permission set names
    private static Map<String, Set<String>> getUserPermissionMap(List<String> strList){
        Map<String, Set<String>> userPermissionMap = new Map<String, Set<String>>();
        for(String s : strList){
            String userId = getUserFromString(s);
            List<String> psList = getPermissionSetsUser(s);
            for(String ps : psList){
                buildMapStrings(userPermissionMap, userId, ps);
            }
        }
        return userPermissionMap;
    }

    private static Map<String, Set<String>> buildMapStrings(Map<String, Set<String>> stringMap, String s1, String s2){
        if(stringMap.get(s1) != null){
            Set<String> sSet = stringMap.get(s1);
            sSet.add(s2);
            stringMap.put(s1, sSet);
        }
        else{
            Set<String> sSet = new Set<String>();
            sSet.add(s2);
            stringMap.put(s1, sSet);
        }
        return stringMap;
    }

    private static void manageErrorLogSave(Database.SaveResult[] srList, Boolean email){
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()){
							// Operation failed, so get all errors
							if (!email){
									showErrors(sr.getErrors());
							}
							else {
									showErrorsWithEmail(sr.getErrors());
							}
						}
        }
    }

    private static void manageErrorLogDelete(Database.DeleteResult[] srList, Boolean email){
        // Iterate through each returned result
        for (Database.DeleteResult sr : srList) {
          if (!sr.isSuccess()){
						// Operation failed, so get all errors
						if (!email){
								showErrors(sr.getErrors());
						}
						else {
								showErrorsWithEmail(sr.getErrors());
						}
					}
        }
    }

    private static void showErrors(List<Database.Error> errList){
        for(Database.Error err : errList) {
            System.debug('The following error has occurred.');
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('Permission set assignments fields that affected this error: ' + err.getFields());
        }
    }


    private static void showErrorsWithEmail(List<Database.Error> errList){
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
        for(Database.Error err : errList) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { UserInfo.getUserEmail() };
            message.optOutPolicy = 'FILTER';
            message.subject = 'User setup failure for user';
            message.plainTextBody = 'Dear User,\r\n The user creation/Update is not setup properly. Please contact your admin for help.\r\n \r\n Error: \r\n' + err.getMessage() + ' ' + err.getFields() +'\r\n \r\n Thanks,\r\n'+'BITMAN Support';
            messages.add(message);

            System.debug('The following error has occurred.');
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('User fields that affected this error: ' + err.getFields());

        }
        try {
        	Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        } catch(Exception ex) {
        	System.debug('*** An exception ocurred sending the email: ' + ex.getMessage());
        }
    }
}