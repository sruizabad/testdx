/********************************************************************************
Name:  BI_TM_User_to_productHandler
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Trigger handler to insert the user to product assignments in veeva mysetupproducts

=================================================================
=================================================================
History  -------
VERSION  AUTHOR              DATE         DETAIL
1.0 -   Shilpa P              21/01/2016   INITIAL DEVELOPMENT
*********************************************************************************/
public class BI_TM_User_to_productHandler implements BI_TM_ITriggerHandler{
    private static boolean recursionCheck=false;
    public static boolean blnMySetProdExecuted=false;

  public void BeforeInsert(List<SObject> newItems) {

        if(!recursionCheck){
            //CheckDataOverlap(newItems);//Call method within class to check for any date overlaps
            recursionCheck=true;
        }
   }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

       if(!recursionCheck){
            //CheckDataOverlap(newItems.values());//Call method within class to check for any date overlaps
            recursionCheck=true;
        }
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
      if(!blnMySetProdExecuted)
      {
        list<BI_TM_User_to_product__c> usertoprod=[select BI_TM_My_setup_product_id__c,BI_TM_Product__r.BI_TM_Product_Catalog_Id__c,BI_TM_User__r.BI_TM_UserId_Lookup__c, BI_TM_Country_Code__c,BI_TM_End_date__c,BI_TM_Start_date__c from BI_TM_User_to_product__c where Id IN: newItems.KeySet()];
        createMySetupProducts(usertoprod,true);
        blnMySetProdExecuted=true;
      }
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

      if(!blnMySetProdExecuted)
      {
        list<BI_TM_User_to_product__c> usertoprod=[select BI_TM_My_setup_product_id__c,BI_TM_Product__r.BI_TM_Product_Catalog_Id__c,BI_TM_User__r.BI_TM_UserId_Lookup__c, BI_TM_Country_Code__c,BI_TM_End_date__c,BI_TM_Start_date__c from BI_TM_User_to_product__c where Id IN: newItems.KeySet()];
        createMySetupProducts(usertoprod,true);
        blnMySetProdExecuted=true;
      }


    }
    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}

    public Map<String, List<My_Setup_Products_vod__c>> getMapExtIdXMySetupProd(List<BI_TM_User_to_product__c>lstUserToProd)
    {
      Set<String> setOwnerIds=new Set<String>();
      for(BI_TM_User_to_product__c objUserToProd:lstUserToProd)
      {
        if(!setOwnerIds.contains(objUserToProd.BI_TM_User__r.BI_TM_UserId_Lookup__c))
        {
           setOwnerIds.add(objUserToProd.BI_TM_User__r.BI_TM_UserId_Lookup__c);
        }
      }
      List<My_Setup_Products_vod__c> lstMySetupProducts=[select id,OwnerId,Product_vod__c,createdDate
                                                         from My_Setup_Products_vod__c
                                                         where OwnerId=:setOwnerIds];
      Map<String,List<My_Setup_Products_vod__c>> mapExtIdXMySetUpProd=new Map<String,List<My_Setup_Products_vod__c>>();
      for(My_Setup_Products_vod__c objMySetupProd:lstMySetupProducts)
      {
        if(objMySetupProd.Product_vod__c!=null)
        {
          String strExtId=(String)objMySetupProd.ownerId+(String)objMySetupProd.Product_vod__c;
          if(!mapExtIdXMySetUpProd.containsKey(strExtId))
          {
            List<My_Setup_Products_vod__c> lstMySetupProductsTemp=new List<My_Setup_Products_vod__c>();
            lstMySetupProductsTemp.add(objMySetupProd);
            mapExtIdXMySetUpProd.put(strExtId,lstMySetupProductsTemp);
          }
          else
          {
            mapExtIdXMySetUpProd.get(strExtId).add(objMySetupProd);
          }
        }
      }
      return mapExtIdXMySetUpProd;
    }
    public void createMySetupProducts( list<BI_TM_User_to_product__c> usertoprod, boolean isTriggered)
    {
      list<My_Setup_Products_vod__c> myprod = new list<My_Setup_Products_vod__c>();
      list<My_Setup_Products_vod__c> lstMySetupPrdToDelete = new list<My_Setup_Products_vod__c>();
      list<String> lstMySetupPrdToDeleteIds=new List<String>();
      /*list<BI_TM_User_to_product__c> usertoprod= new List<BI_TM_User_to_product__c>();
      usertoprod=[select BI_TM_My_setup_product_id__c,BI_TM_Product__r.BI_TM_Product_Catalog_Id__c,BI_TM_User__r.BI_TM_UserId_Lookup__c, BI_TM_Country_Code__c,BI_TM_End_date__c,BI_TM_Start_date__c from BI_TM_User_to_product__c where Id IN: newItems.KeySet()];*/
      Map<String,String> mapUsrPrdIdXmySetUppr=new Map<String,String>();
      Map<String,List<My_Setup_Products_vod__c>> mapExtIdXMySetUpProd=getMapExtIdXMySetupProd(usertoprod);
      list<UsrToprdMysetUpToPrdrdWrp> lstusrtoWRP=new List<UsrToprdMysetUpToPrdrdWrp>();
      for(BI_TM_User_to_product__c up:usertoprod)
      {
        if(up.BI_TM_Start_date__c<=System.today()&&(up.BI_TM_End_date__c==null||(up.BI_TM_End_date__c>=System.today())))
        {
         if(up.BI_TM_My_setup_product_id__c==null||up.BI_TM_My_setup_product_id__c=='')
         {
            String strKeyId=(String)up.BI_TM_User__r.BI_TM_UserId_Lookup__c+(String)up.BI_TM_Product__r.BI_TM_Product_Catalog_Id__c;
            UsrToprdMysetUpToPrdrdWrp objUserToProdWRP=new UsrToprdMysetUpToPrdrdWrp();
            objUserToProdWRP.objUserToProd=up;
            if(mapExtIdXMySetUpProd.containsKey(strKeyId))
            {
               objUserToProdWRP.objMySetupProd=mapExtIdXMySetUpProd.get(strKeyId).get(0);
            }
            else
            {
             My_Setup_Products_vod__c mp= new My_Setup_Products_vod__c();
             mp.Product_vod__c = up.BI_TM_Product__r.BI_TM_Product_Catalog_Id__c;
             mp.OwnerId=up.BI_TM_User__r.BI_TM_UserId_Lookup__c;
             mp.Favorite_vod__c = true;
             mp.Country_Code_BI__c = up.BI_TM_Country_Code__c;
             objUserToProdWRP.objUserToProd=up;
             objUserToProdWRP.objMySetupProd=mp;
             myprod.add(mp);
           }
           lstusrtoWRP.add(objUserToProdWRP);
         }
       }
       else
       {
         if(up.BI_TM_My_setup_product_id__c!=null)
         {
           UsrToprdMysetUpToPrdrdWrp objUserToProdWRP=new UsrToprdMysetUpToPrdrdWrp();
           objUserToProdWRP.objUserToProd=up;
           objUserToProdWRP.objMySetupProd=null;
           //My_Setup_Products_vod__c objMySetupProdToDel=new My_Setup_Products_vod__c(id=up.BI_TM_My_setup_product_id__c);
           //lstMySetupPrdToDelete.add(objMySetupProdToDel);
           lstMySetupPrdToDeleteIds.add(up.BI_TM_My_setup_product_id__c);
           lstusrtoWRP.add(objUserToProdWRP);
         }
       }
     }

      try {
        system.debug('-----------List To Insert--------------'+myprod);
        Insert myProd;//Inserting the MySetUp Product Record
        lstMySetupPrdToDelete=[select id from My_Setup_Products_vod__c where id=:lstMySetupPrdToDeleteIds];
        delete lstMySetupPrdToDelete;
        for(UsrToprdMysetUpToPrdrdWrp objUserToProdWRP:lstusrtoWRP)
        {
          if(objUserToProdWRP.objMySetupProd!=null)
          {
            mapUsrPrdIdXmySetUppr.put(objUserToProdWRP.objUserToProd.id,objUserToProdWRP.objMySetupProd.id);
          }
          else
          {
            mapUsrPrdIdXmySetUppr.put(objUserToProdWRP.objUserToProd.id,'');
          }
        }
        if(mapUsrPrdIdXmySetUppr.size()>0)
        {
          if(!System.isBatch()&&!System.isFuture())
          {
            updateMysetupProductId(mapUsrPrdIdXmySetUppr);
          }
          else
          {
            if(!isTriggered)
            {
              updateMysetupProductId(lstusrtoWRP);
            }
          }

        }
      } catch (system.Dmlexception e) {
          system.debug (e);
      }
  }

    @future
    public static void updateMysetupProductId(Map<String,String> mapUsrPrdIdXmySetUppr)
    {
      List<BI_TM_User_to_product__c> lstUserToProd=[select id ,BI_TM_My_setup_product_id__c
                                                    from BI_TM_User_to_product__c
                                                    where id=:mapUsrPrdIdXmySetUppr.keyset()];
      for(BI_TM_User_to_product__c objUserToProd:lstUserToProd)
      {
        objUserToProd.BI_TM_My_setup_product_id__c=mapUsrPrdIdXmySetUppr.get(objUserToProd.id);
      }
      database.update(lstUserToProd);
    }
    public void updateMysetupProductId(list<UsrToprdMysetUpToPrdrdWrp> lstusrtoWRP)
    {
      list<BI_TM_User_to_product__c> lstUserToProd=new List<BI_TM_User_to_product__c>();
      for(UsrToprdMysetUpToPrdrdWrp objUserToProdWRP:lstusrtoWRP)
      {
        if(objUserToProdWRP.objMySetupProd!=null)
        {
          objUserToProdWRP.objUserToProd.BI_TM_My_setup_product_id__c=objUserToProdWRP.objMySetupProd.id;
        }
        else
        {
          objUserToProdWRP.objUserToProd.BI_TM_My_setup_product_id__c=null;
        }
        lstUserToProd.add(objUserToProdWRP.objUserToProd);
      }
      database.update(lstUserToProd);
    }
    public class UsrToprdMysetUpToPrdrdWrp
    {
      public BI_TM_User_to_product__c objUserToProd;
      public My_Setup_Products_vod__c objMySetupProd;
    }
    /*Method to check for date overlap for user and product combination
    Author: Deepak N
    Commented by Antonio Ferrero 22nd May 2017
    */
    // public void CheckDataOverlap (List<BI_TM_User_to_product__c> newItems){
    //
    //     Set<Id> users = new Set<Id>();
    //     Set<Id> products=new Set<id>();
    //
    //     for(BI_TM_User_to_product__c userProduct : (List<BI_TM_User_to_product__c>) newItems) {
    //         users.add(userProduct.BI_TM_User__c);
    //         products.add(userProduct.BI_TM_Product__c);
    //     }
    //
    //     //Query records from database
    //     List<BI_TM_User_to_product__c> user2ProdList=new List<BI_TM_User_to_product__c>([SELECT ID, BI_TM_User__c, BI_TM_Product__c, BI_TM_Start_date__c, BI_TM_End_date__c, Name FROM BI_TM_User_to_product__c where
    //     BI_TM_User__c IN :users AND BI_TM_Product__c IN :products]);
    //
    //     for(BI_TM_User_to_product__c user2ProdOuterVar:(List<BI_TM_User_to_product__c>)newItems){
    //          Boolean duplicateExistInSystem=false;
    //         Boolean duplicateExistInFile=false;
    //         String errorMsg1;//Error message if duplicate already exist in salesforce.
    //         String errorMsg2;//Error message if duplicate exist within system.
    //         //Check with the records already in database
    //         for(BI_TM_User_to_product__c user2ProdInnerVar:user2ProdList){
    //             if(user2ProdOuterVar.BI_TM_User__c==user2ProdInnerVar.BI_TM_User__c && user2ProdOuterVar.BI_TM_Product__c==user2ProdInnerVar.BI_TM_Product__c){
    //                 if(user2ProdOuterVar!=user2ProdInnerVar && user2ProdOuterVar.Id!=user2ProdInnerVar.Id){//check to avoid record check with itself
    //                     if(((user2ProdOuterVar.BI_TM_End_date__c != NULL && ((user2ProdInnerVar.BI_TM_Start_date__c >= user2ProdOuterVar.BI_TM_Start_date__c && user2ProdInnerVar.BI_TM_Start_date__c <= user2ProdOuterVar.BI_TM_End_date__c) ||
    //                             (user2ProdInnerVar.BI_TM_End_date__c  >= user2ProdOuterVar.BI_TM_Start_date__c && user2ProdInnerVar.BI_TM_End_date__c  <= user2ProdOuterVar.BI_TM_End_date__c) ||
    //                             (user2ProdInnerVar.BI_TM_Start_date__c <= user2ProdOuterVar.BI_TM_Start_date__c &&user2ProdInnerVar.BI_TM_End_date__c  >= user2ProdOuterVar.BI_TM_End_date__c) ||
    //                             (user2ProdInnerVar.BI_TM_Start_date__c<=user2ProdOuterVar.BI_TM_Start_date__c && user2ProdInnerVar.BI_TM_End_date__c >= user2ProdOuterVar.BI_TM_Start_date__c))) ||
    //                             (user2ProdOuterVar.BI_TM_End_date__c == NULL && ((user2ProdInnerVar.BI_TM_End_date__c >= user2ProdOuterVar.BI_TM_Start_date__c)))) &&
    //                             user2ProdInnerVar.BI_TM_End_date__c!=Null){
    //                             duplicateExistInSystem=true;
    //                             errorMsg1='Record already exist for the given product vs user values. Existing Record Name:'+user2ProdInnerVar.Name;
    //
    //                         }
    //                         else if(user2ProdInnerVar.BI_TM_End_date__c==Null && (user2ProdInnerVar.BI_TM_Start_date__c <=user2ProdOuterVar.BI_TM_End_date__c || user2ProdOuterVar.BI_TM_End_date__c == NULL)){
    //                             duplicateExistInSystem=true;
    //                             errorMsg1='There is already record existing without enddate for this user to product in system. Please end date the existing record before creating new. Existing Record Name:'+user2ProdInnerVar.Name;
    //                         }
    //                 }
    //                 if(duplicateExistInSystem)break;//Break the loop if duplicate is found.
    //             }
    //         }
    //
    //         //Check with the records coming in single batch.
    //         for(BI_TM_User_to_product__c user2ProdInnerVar:(List<BI_TM_User_to_product__c>)newItems){
    //             if(user2ProdOuterVar.BI_TM_User__c==user2ProdInnerVar.BI_TM_User__c && user2ProdOuterVar.BI_TM_Product__c==user2ProdInnerVar.BI_TM_Product__c){
    //                     if(user2ProdInnerVar!=user2ProdOuterVar){
    //                         if((user2ProdOuterVar.Id!=user2ProdInnerVar.Id) ||user2ProdOuterVar.Id==NULL){//check to avoid record check with itself
    //                         system.debug('Third Stage');
    //                             if(((user2ProdOuterVar.BI_TM_End_date__c != NULL && ((user2ProdInnerVar.BI_TM_Start_date__c >= user2ProdOuterVar.BI_TM_Start_date__c && user2ProdInnerVar.BI_TM_Start_date__c <= user2ProdOuterVar.BI_TM_End_date__c) ||
    //                                 (user2ProdInnerVar.BI_TM_End_date__c  >= user2ProdOuterVar.BI_TM_Start_date__c && user2ProdInnerVar.BI_TM_End_date__c  <= user2ProdOuterVar.BI_TM_End_date__c) ||
    //                                 (user2ProdInnerVar.BI_TM_Start_date__c <= user2ProdOuterVar.BI_TM_Start_date__c && user2ProdInnerVar.BI_TM_End_date__c  >= user2ProdOuterVar.BI_TM_End_date__c) ||
    //                                 (user2ProdInnerVar.BI_TM_Start_date__c<=user2ProdOuterVar.BI_TM_Start_date__c && user2ProdInnerVar.BI_TM_End_date__c >= user2ProdOuterVar.BI_TM_Start_date__c))) ||
    //                                 (user2ProdOuterVar.BI_TM_End_date__c == NULL && ((user2ProdInnerVar.BI_TM_End_date__c >= user2ProdOuterVar.BI_TM_Start_date__c)))) &&
    //                                 user2ProdInnerVar.BI_TM_End_date__c!=Null){
    //                                 duplicateExistInFile=true;
    //                                 errorMsg2='Duplicate record existing within input source itself. Please validate data before loading.';
    //
    //                             }
    //                             else if(user2ProdInnerVar.BI_TM_End_date__c==Null && (user2ProdInnerVar.BI_TM_Start_date__c <=user2ProdOuterVar.BI_TM_End_date__c || user2ProdOuterVar.BI_TM_End_date__c == NULL)){
    //                                 duplicateExistInFile=true;
    //                                 errorMsg2='There is already record existing without enddate within input source itself. Please validate data before loading';
    //                             }
    //                         }
    //                 }
    //             }
    //             if(duplicateExistInFile)break;//Break the loop if duplicate is found.
    //
    //         }
    //
    //         //check if duplicate existed. Throw error if exist.
    //         if(user2ProdOuterVar.BI_TM_End_date__c<user2ProdOuterVar.BI_TM_Start_date__c)
    //             //user2ProdOuterVar.addError('End Date Should not be less than Start Date.');
    //         if(duplicateExistInFile)
    //             user2ProdOuterVar.addError(errorMsg2);//Error message for user on duplicate record within file.
    //         if(duplicateExistInSystem)
    //             user2ProdOuterVar.addError(errorMsg1);//Error message for user on duplicate record within system.
    //     }
    // }


}