global class BI_TM_UsersView_Batch implements Database.Batchable<sObject>, Schedulable {

	String query;

	global BI_TM_UsersView_Batch() {
		query = 'SELECT Id, Name, BI_TM_Supervisor_Email__c FROM BI_TM_Users_View__c WHERE BI_TM_File_Type__c = \'personnel_ashfield\' AND BI_TM_Employee_Status__c = \'ACTIVE\' AND Name != \'\' AND   BI_TM_Supervisor_Email__c != \'\'';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<BI_TM_Users_View__c> scope) {
		if (!scope.isEmpty()) {
			// UsersViewMap
			Map<String, String> usersViewMap = new Map<String, String>();
			for (BI_TM_Users_View__c uv : scope) {
				if (!String.isBlank(uv.Name) && !String.isBlank(uv.BI_TM_Supervisor_Email__c))
				usersViewMap.put(uv.BI_TM_Supervisor_Email__c, uv.Name);
			}

			// UserMap
			Map<String, String> userMap = new Map<String, String> ();
			for (User u : [SELECT Id, Username, BIDS_ID_BI__c FROM User WHERE UserName = :usersViewMap.keySet()]) {
				if (!String.isBlank(u.UserName) && !String.isBlank(u.BIDS_ID_BI__c)) {
					String bid = usersViewMap.get(u.UserName);
					userMap.put(bid, u.BIDS_ID_BI__c);
				}
			}

			// BIDS USer
			BI_TM_BIDS__c[] toUpdate = new List<BI_TM_BIDS__c>();
			for (BI_TM_BIDS__c bids : [SELECT Id, BI_TM_Global_Id__c, BI_TM_Manager_Id__c FROM BI_TM_BIDS__c WHERE BI_TM_Global_Id__c = :userMap.keySet()]) {
				String userManagerId = userMap.get(bids.BI_TM_Global_Id__c);
				if (bids.BI_TM_Manager_Id__c != userManagerId) {
					bids.BI_TM_Manager_Id__c = userManagerId;
					toUpdate.add(bids);
				}
			}

			if (!toUpdate.isEmpty()) {
				System.debug('To Update: ' + toUpdate);
				Database.SaveResult[] updateSrList = Database.update(toUpdate, false);
				for (Database.SaveResult sr : updateSrList ) {
					if (!sr.isSuccess()){
						// Operation failed, so get all errors
						for(Database.Error err : sr.getErrors()) {
							System.debug('The following error has occurred.');
							System.debug(err.getStatusCode() + ': ' + err.getMessage());
							System.debug('Record fields that affected this error: ' + err.getFields());
						}
					}
				}
			}
		}
	}

	global void finish(Database.BatchableContext BC) {

	}

	global void execute(SchedulableContext SC)
	{
		database.executebatch(new BI_TM_UsersView_Batch());
	}

}