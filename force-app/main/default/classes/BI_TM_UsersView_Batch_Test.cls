@isTest
private class BI_TM_UsersView_Batch_Test {
	
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';

	@testSetup 
    static void setup() {
        BI_TM_BIDS__c bids = new BI_TM_BIDS__c();
        bids.Name = 'Test'; // Salesforce Name
        bids.BI_TM_CN__c = 'Test'; // Alias
        bids.BI_TM_Preffered_Given_Name__c = 'Test'; // Preffered Given Name
		bids.BI_TM_Global_Id__c = 'TEST_U_01';
		bids.BI_TM_Manager_Id__c = 'TEST_M_01';
		insert bids;

		User user = new User();
		user.LastName = 'Test'; // LastName
		user.Email = 'user@email.test'; // Email
		user.Alias = 'UserTest'; // Alias
		user.TimeZoneSidKey = 'GMT'; // TimeZoneSidKey
		user.LocaleSidKey = 'en_GB'; // LocaleSidKey
		user.EmailEncodingKey = 'ISO-8859-1'; // EmailEncodingKey
		user.ProfileId = '00eU0000000e4IGIAY'; // ProfileId
		user.LanguageLocaleKey = 'en_US'; // LanguageLocaleKey
		user.username = 'manager@email.test';
		user.BIDS_ID_BI__c = 'TEST_M_02';
		insert user;

		BI_TM_Users_View__c uv = new BI_TM_Users_View__c();
		uv.BI_TM_File_Type__c = 'personnel_ashfield';
		uv.BI_TM_Employee_Status__c = 'ACTIVE';
		uv.Name = 'TEST_U_01';
		uv.BI_TM_Supervisor_Email__c = 'manager@email.test';
		insert uv;
    }	

	static testmethod void test() {        
        Test.startTest();
        BI_TM_UsersView_Batch b = new BI_TM_UsersView_Batch();
		System.schedule('ScheduledApexTest', CRON_EXP, b);
		database.executebatch(b);
		Test.stopTest();

        BI_TM_BIDS__c[] res = [select BI_TM_Manager_Id__c from BI_TM_BIDS__c where BI_TM_Global_Id__c = 'TEST_U_01'];
        System.assertEquals(res[0].BI_TM_Manager_Id__c, 'TEST_M_02');
    }
}