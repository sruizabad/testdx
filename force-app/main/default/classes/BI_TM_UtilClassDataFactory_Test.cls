/**
* ===================================================================================================================================
*                                   BITMAN BI
* ===================================================================================================================================
*  Decription:      Util class to generate test data
*  @author:         Antonio Ferrero
*  @created:        06-Jun-2017
*  @version:        1.0
*  @see:            Salesforce BITMAN
*  @since:          34.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         06-Jun-2017                 Antonio Ferrero             Construction of the class.
*/
@isTest
public with sharing class BI_TM_UtilClassDataFactory_Test {

  public static BI_TM_FF_type__c createFieldForce(String name, String country, String business){
    return new BI_TM_FF_type__c(Name = name, BI_TM_Country_Code__c = country, BI_TM_Business__c = business);
  }

  public static BI_TM_Assignment_Criteria_Type__c createAssignmentCriteriaType(String name, String country, String business){
    return new BI_TM_Assignment_Criteria_Type__c(Name = name, BI_TM_Country_Code__c = country, BI_TM_Business__c = business);
  }

  public static BI_TM_Geography_type__c createGeographyType(String name, String country){
    return new BI_TM_Geography_type__c(Name = name, BI_TM_Country_Code__c = country);
  }

  public static BI_TM_Geography__c createGeography(String name, String country, Id geoType){
    return new BI_TM_Geography__c(Name = name, BI_TM_Country_Code__c = country, BI_TM_Geography_type__c = geoType);
  }

  public static BI_TM_Territory_ND__c createTerritory(String name, String country, String business, Id assignmentCriteriaType, Id fieldForce, Date startDate, Date endDate, Boolean visibleReport){
    return new BI_TM_Territory_ND__c(Name = name, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_Assignment_Criteria_Type__c = assignmentCriteriaType,
                                      BI_TM_Field_Force__c = fieldForce, BI_TM_Start_Date__c = startDate, BI_TM_End_Date__c = endDate, BI_TM_Visible_in_report__c = visibleReport);
  }

  public static BI_TM_Position_Type__c createPosType(String name, String country, String business){
    return new BI_TM_Position_Type__c(Name = name, BI_TM_Country_Code__c = country, BI_TM_Business__c = business);
  }

  public static BI_TM_Alignment__c createAlignment(String name, String country, String business, Id ffId, Date startDate, Date endDate, String status, Boolean prevInter, Decimal timePeriod){
    return new BI_TM_Alignment__c(Name = name, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_FF_type__c = ffId, BI_TM_Start_date__c = startDate, BI_TM_End_date__c = endDate, BI_TM_Status__c = status, BI_TM_Include_Previous_Interactions__c = prevInter, BI_TM_Previous_Months__c = timePeriod);
  }

  public static BI_TM_Territory__c createPosition(String name, String country, String business, Id fieldForce, Date startDate, Date endDate, Boolean visibleReport, Boolean isRoot, Id parentPosition, Id parentTerritory, String globalPosType, Id posType, String posLevel){
    return new BI_TM_Territory__c (Name = name, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_FF_type__c = fieldForce, BI_TM_Start_date__c = startDate, BI_TM_End_date__c = endDate, BI_TM_Visible_in_crm__c = visibleReport, BI_TM_Is_Root__c = isRoot, BI_TM_Parent_Position__c = parentPosition, BI_TM_Territory_ND__c = parentTerritory, BI_TM_Global_Position_Type__c = globalPosType, BI_TM_Position_Type_Lookup__c = posType, BI_TM_Position_Level__c = posLevel);
  }

  public static User createUser(String lastName, String firstName, String business, String country, String alias, String email, String commName, String username, String profile, String locale, String timeZone, String language, String currencyISO, String emailEncoding){
    return new User (LastName = lastName, FirstName = firstName, Business_BI__c = business, Country_Code_BI__c = country, Alias = alias, Email = email, CommunityNickname = commName, Username = username, ProfileId = [SELECT Id FROM Profile WHERE Name = :profile].Id, LocaleSidKey = locale, TimeZoneSidKey = timeZone, LanguageLocaleKey = language, DefaultcurrencyIsoCode = currencyISO, EmailEncodingKey = emailEncoding);
  }

  public static BI_TM_User_mgmt__c createUserManagement(String lastName, String firstName, String country, String business, String alias, String email, String commName, String username, String profile, String locale, String timeZone, String language, String currencyISO, String emailEncoding, Date startDate, Date endDate, Boolean active){
    return new BI_TM_User_mgmt__c (Name = firstName + ' ' + lastName, BI_TM_Last_name__c = lastName, BI_TM_First_name__c = firstName, BI_TM_Business__c = business, BI_TM_Alias__c = alias, BI_TM_Email__c = email, BI_TM_UserCountryCode__c = country, BI_TM_COMMUNITYNICKNAME__c = commName, BI_TM_Username__c = username, BI_TM_Profile__c = profile, BI_TM_LocaleSidKey__c = getLocaleValue(locale), BI_TM_TimeZoneSidKey__c = timeZone, BI_TM_LanguageLocaleKey__c = getLanguageValue(language), BI_TM_Currency__c = getCurrencyValue(currencyISO), BI_TM_Email_Encoding__c = getEmailEncodingValue(emailEncoding), BI_TM_Start_date__c = startDate, BI_TM_End_date__c = endDate, BI_TM_Active__c = active);
  }

  public static BI_TM_User_mgmt__c createUserManagementVisibleCRM(String lastName, String firstName, String country, String business, String alias, String email, String commName, String username, String profile, String locale, String timeZone, String language, String currencyISO, String emailEncoding, Date startDate, Date endDate, Boolean active, Boolean visibleCRM, Boolean overridePS){
    return new BI_TM_User_mgmt__c (Name = firstName + ' ' + lastName, BI_TM_Last_name__c = lastName, BI_TM_First_name__c = firstName, BI_TM_Business__c = business, BI_TM_Alias__c = alias, BI_TM_Email__c = email, BI_TM_UserCountryCode__c = country, BI_TM_COMMUNITYNICKNAME__c = commName, BI_TM_Username__c = username, BI_TM_Profile__c = profile, BI_TM_LocaleSidKey__c = getLocaleValue(locale), BI_TM_TimeZoneSidKey__c = timeZone, BI_TM_LanguageLocaleKey__c = getLanguageValue(language), BI_TM_Currency__c = getCurrencyValue(currencyISO), BI_TM_Email_Encoding__c = getEmailEncodingValue(emailEncoding), BI_TM_Start_date__c = startDate, BI_TM_End_date__c = endDate, BI_TM_Active__c = active, BI_TM_Visible_in_CRM__c = visibleCRM, BI_TM_Override_Veeva_Permission_Sets__c = overridePS);
  }

  public static BI_TM_User_Role__c createUserRole(String name, Id parentRole, String country, String business, Boolean root, Date startDate, Date endDate, Boolean visibleInCRM){
    return new BI_TM_User_Role__c (Name = name, BI_TM_Parent_User_Role__c = parentRole, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_Is_Root__c = root,  BI_TM_Start_date__c = startDate, BI_TM_End_date__c = endDate, BI_TM_Visible_in_CRM__c = visibleInCRM);
  }

  public static BI_TM_Mirror_Product__c createMirrorProduct(String name, String country, Id prodCatalog){
    return new BI_TM_Mirror_Product__c(Name = name, BI_TM_Country_Code__c = country, BI_TM_Product_Catalog_Id__c = prodCatalog);
  }

  public static Product_vod__c createProduct(String name, String country, String prodType, String externalId){
    return new Product_vod__c(Name = name, Country_Code_BI__c = country, Product_Type_vod__c = prodType, External_ID_vod__c = externalId);
  }

  public static BI_TM_Territory_to_product__c createPosition2Product(Id position, Id product, String country, String business, Boolean primary, Date startDate, Date endDate, Boolean overrideParent, Id parentFF2Prod, Id parentTerr2Prod){
    return new BI_TM_Territory_to_product__c(BI_TM_Territory_id__c = position, BI_TM_Mirror_Product__c = product, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_Is_Primary_Product__c = primary, BI_TM_Start_Date__c = startDate, BI_TM_End_Date__c = endDate, BI_TM_Override_Parent__c = overrideParent, BI_TM_Parent_Field_Force_To_Product__c = parentFF2Prod, BI_TM_Parent_Territory_to_Product__c = parentTerr2Prod);
  }

  public static BI_TM_FF_Type_To_Product__c createFieldForce2Product(Id fieldForce, Id product, String country, String business, Boolean primary, Date startDate, Date endDate){
    return new BI_TM_FF_Type_To_Product__c(BI_TM_FF_Type__c = fieldForce, BI_TM_Mirror_Product__c = product, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_Primary_Product__c = primary, BI_TM_Start_Date__c = startDate, BI_TM_End_Date__c = endDate);
  }

  public static BI_TM_Territory_to_Product_ND__c createTerritory2Product(Id territory, Id product, String country, String business, Boolean primary, Date startDate, Date endDate, Boolean overrideParent, Id parentFF2Prod){
    return new BI_TM_Territory_to_Product_ND__c(BI_TM_Territory_ND__c = territory, BI_TM_Mirror_Product__c = product, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_Primary_Product__c = primary, BI_TM_Start_Date__c = startDate, BI_TM_End_Date__c = endDate, BI_TM_Override_Parent_FF_to_Product__c = overrideParent, BI_TM_Parent_Field_Force_To_Product__c = parentFF2Prod);
  }

  public static BI_TM_User_territory__c createUser2Position(Id position, Id user, String country, String business, Boolean primary, String assignmentType, Date startDate, Date endDate, String status){
    return new BI_TM_User_territory__c(BI_TM_Territory1__c = position, BI_TM_User_mgmt_tm__c = user, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_Primary__c = primary, BI_TM_Assignment_Type__c = assignmentType, BI_TM_Start_Date__c = startDate, BI_TM_End_Date__c = endDate, BI_TM_Status__c = status);
  }

  public static BI_TM_Account_To_Territory__c createManualAssignment(Id position, Id account, String country, String business, Date startDate, Date endDate, Boolean childAssignment, Boolean parentAssignment, String hcphco, Boolean excludeDynamic, Boolean sap){
    return new BI_TM_Account_To_Territory__c(BI_TM_Territory_FF_Hierarchy_Position__c = position, BI_TM_Account__c = account, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_Start_Date__c = startDate, BI_TM_End_Date__c = endDate, BI_TM_Direct_Child_Assignment__c = childAssignment, BI_TM_Direct_Parent_Assignment__c = parentAssignment, BI_TM_HCP_HCO__c = hcphco, BI_TM_Exclude_Dynamic_Rules__c = excludeDynamic, BI_TM_SAP__c = sap);
  }

  public static BI_TM_Blacklist_account__c createBlacklistAccount(Id account, Id position, String country, String business, Date startDate, Date endDate){
    return new BI_TM_Blacklist_account__c(BI_TM_Account__c = account, BI_TM_Position__c = position, BI_TM_Country_Code__c = country, BI_TM_Business__c = business, BI_TM_Start_Date__c = startDate, BI_TM_End_Date__c = endDate);
  }

  public static UserRole createRole(String name, Id parentRoleId){
    return new UserRole(Name = name, ParentRoleId = parentRoleId);
  }

  public static Call2_vod__c createInteraction(Id account, String territory, String status){
    return new Call2_vod__c(Account_vod__c = account, Territory_vod__c = territory, Status_vod__c = status);
  }

  private static String getCurrencyValue(String label){
		String value = '';
		List<Schema.PicklistEntry> peList = BI_TM_User_mgmt__c.BI_TM_Currency__c.getDescribe().getPicklistValues();
		for(Schema.PicklistEntry pe : peList){
			if(pe.getLabel() == label){
        value = pe.getValue();
      }
		}
		return value;
	}

  private static String getEmailEncodingValue(String label){
		String value = '';
		List<Schema.PicklistEntry> peList = BI_TM_User_mgmt__c.BI_TM_Email_Encoding__c.getDescribe().getPicklistValues();
		for(Schema.PicklistEntry pe : peList){
			if(pe.getLabel() == label){
        value = pe.getValue();
      }
		}
		return value;
	}

  private static String getLocaleValue(String label){
    String value = '';
    List<Schema.PicklistEntry> peList = BI_TM_User_mgmt__c.BI_TM_LocaleSidKey__c.getDescribe().getPicklistValues();
    for(Schema.PicklistEntry pe : peList){
      if(pe.getLabel() == label){
        value = pe.getValue();
      }
    }
    return value;
  }

  private static String getLanguageValue(String label){
    String value = '';
    List<Schema.PicklistEntry> peList = BI_TM_User_mgmt__c.BI_TM_LanguageLocaleKey__c.getDescribe().getPicklistValues();
    for(Schema.PicklistEntry pe : peList){
      if(pe.getLabel() == label){
        value = pe.getValue();
      }
    }
    return value;
  }
}