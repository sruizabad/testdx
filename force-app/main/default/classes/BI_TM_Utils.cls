public without sharing class BI_TM_Utils {

  public static Boolean isAdmin() {
      List<PermissionSetAssignment> adminPermissionSet =    [SELECT Id, PermissionSet.Name,AssigneeId
                                                              FROM PermissionSetAssignment
                                                              WHERE AssigneeId = :Userinfo.getUserId() and PermissionSet.Name = 'BI_TM_Admin' ];
      return (adminPermissionSet.size() > 0);
  }

  // Format the date in YYYY-MM-DD
	public static String formatDate(Date d) {
    DateTime dt = d;
    return dt.format('yyyy-MM-dd');
	}

  // Manage error logs from a delete operation
  public static void manageErrorLogDelete(Database.DeleteResult[] srList){
		// Iterate through each returned result
		for (Database.DeleteResult sr : srList) {
			if (!sr.isSuccess()){
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Record fields that affected this error: ' + err.getFields());
				}
			}
		}
	}

  // Manage error records from upsert operation
	public static void manageErrorLogSave(Database.UpsertResult[] srList){
		// Iterate through each returned result
		for (Database.UpsertResult sr : srList) {
			if (!sr.isSuccess()){
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Record fields that affected this error: ' + err.getFields());
				}
			}
		}
	}

  // Manage error records from update operation
	public static void manageErrorLogUpdate(Database.SaveResult[] srList){
		// Iterate through each returned result
    for (Database.SaveResult sr : srList) {
      if (!sr.isSuccess()) {
        // Operation failed, so get all errors
        for(Database.Error err : sr.getErrors()) {
          System.debug('The following error has occurred.');
          System.debug(err.getStatusCode() + ': ' + err.getMessage());
          System.debug('Record fields that affected this error: ' + err.getFields());
        }
      }
    }
	}

  // Get recordtypeid from SObject
  public static Id getRecTypeId(String obj, String recTypeName){
		return [SELECT Id FROM recordType WHERE DeveloperName = :recTypeName AND SObjectType = :obj].Id;
	}

  // Get the list of account fields
	private static Set<String> getAccountFields(Map<String, Schema.SObjectField> schemaFieldMap){
		Set<String> accountFields = new Set<String>();
		for (String fieldName: schemaFieldMap.keySet()) {
    	accountFields.add(fieldName.toUpperCase());
    }
		return accountFields;
	}

  // Get map with the user ids that have enabled the SSO via profile or permission sets
  public static Map<Id, boolean> getUserIsSSOMap(Set<Id> userIds){
    Map<Id, Boolean> userIsSSOMap = new Map<Id, Boolean>();
    Map<Id, Id> profileUserMap = BI_TM_Utils.getProfileUserMap(userIds); // Build map with the relationship between profileId and user id
    Set<Id> permissionSetUserSSO = BI_TM_Utils.getPermissionSetUserSSO(userIds);
    Map<Id, Boolean> profileEnabledSSO = BI_TM_Utils.isProfileEnabledSSO(profileUserMap.values());
    for(Id uId : userIds){
      Boolean checkProfileIsSSO = profileEnabledSSO.get(profileUserMap.get(uId));
      if(checkProfileIsSSO || permissionSetUserSSO.contains(uId)){
        userIsSSOMap.put(uId, true);
      }
    }
    return userIsSSOMap;
  }

  // Build map with the user id and the profile id
  public static Map<Id, Id> getProfileUserMap(Set<Id> userIds){
    Map<Id, Id> profileUserMap = new Map<Id, Id>();
    for(User u : [SELECT Id, ProfileId FROM User WHERE Id IN :userIds]){
      profileUserMap.put(u.Id, u.ProfileId);
    }
    return profileUserMap;
  }

  // Check profile is enabled for SSO
  public static Map<Id, boolean> isProfileEnabledSSO(List<Id> profileIds){
    Map<Id, Boolean> profileEnabledSSO = new Map<Id, Boolean>();
    for(Profile pr : [SELECT Id, IsSsoEnabled FROM Profile WHERE Id IN :profileIds]){
      profileEnabledSSO.put(pr.ID, pr.IsSsoEnabled);
    }
    return profileEnabledSSO;
  }

  // Build map with the user ids and the list of the permission sets ids with SSO enabled
  public static Set<Id> getPermissionSetUserSSO(Set<Id> userIds){
    Set<Id> permissionSetUserSSO = new Set<Id>();
    for(PermissionSetAssignment psa : [SELECT AssigneeId, PermissionSet.PermissionsIsSsoEnabled FROM PermissionSetAssignment WHERE AssigneeId IN :userIds]){
      if(psa.PermissionSet.PermissionsIsSsoEnabled){
        permissionSetUserSSO.add(psa.AssigneeId);
      }
    }
    return permissionSetUserSSO;
  }
}