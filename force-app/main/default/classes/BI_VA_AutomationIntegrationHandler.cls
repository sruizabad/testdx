/*****************************************************************************************
Name: BI_VA_AutomationIntegrationHandler
Copyright © BI
==========================================================================================
==========================================================================================
Purpose:--------
This is the Class which contain method that we are Exposing for Myshop to consume
==========================================================================================
==========================================================================================
History
-------
VERSION        AUTHOR                  DATE               DETAIL

 1.0        VisweswaraRao                             Initial development

******************************************************************************************/
global Class BI_VA_AutomationIntegrationHandler {

 webservice static BI_VA_AutomationWrapper.BI_VA_AutomationResponse automateTask(BI_VA_AutomationWrapper.BI_VA_AutomationRequest request) {
    
      BI_VA_AutomationWrapper.BI_VA_AutomationResponse response = new BI_VA_AutomationWrapper.BI_VA_AutomationResponse ();
    
      response = BI_VA_AutomationProcessHandler.processAutomationTask(request);
    
      return response;
} 

}