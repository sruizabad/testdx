/*
  DESCRIPTION: This is a Test class for BI_VA_AutomationIntegrationHandler_Test Class for Webservice
*/

@isTest
public class BI_VA_AutomationIntegrationHandler_Test {
  
    public static testmethod void autoMationIntegrationTest1(){
         test.startTest();
        
        BI_VA_CountryCodes__c ccs = new BI_VA_CountryCodes__c();
        ccs.Name = 'US';
        ccs.AH_Prod__c = 'Child 11';
        ccs.AH_QA__c = 'Full 12';
        ccs.PM_Prod__c = 'Child 1';
        ccs.PM_QA__c = 'Full 2';
        insert ccs;
        
        BI_VA_AutomationWrapper.BI_VA_AutomationRequest req= new BI_VA_AutomationWrapper.BI_VA_AutomationRequest();   
        req.userName='Test';
        req.emailId='Test@test11.com';
        req.buinessUnit='PM';
        req.orgName='ORD';
        req.country='US';
        req.environment='Production';
        req.myShopNumber='123456789';
        req.BidsId='123';
        req.Type='ResetPassword';
        System.assertEquals('US', req.country);
        BI_VA_AutomationIntegrationHandler.automateTask(req);
        test.stopTest();
    }
}