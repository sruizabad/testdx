/*****************************************************************************************
Name: BI_VA_AutomationProcessHandler
Copyright © BI
==========================================================================================
==========================================================================================
Purpose:
--------
This is the Class which will process or Automate task based on the Resetpassword and user unlock Requests from Myshop 
==========================================================================================
==========================================================================================
History
-------
VERSION        AUTHOR                  DATE               DETAIL

 1.0        Visweswara Rao                             Initial development

******************************************************************************************/
public Class BI_VA_AutomationProcessHandler {
    public static BI_VA_AutomationWrapper.BI_VA_AutomationResponse  processAutomationTask(BI_VA_AutomationWrapper.BI_VA_AutomationRequest request){       
        BI_VA_AutomationWrapper.BI_VA_AutomationResponse response =new  BI_VA_AutomationWrapper.BI_VA_AutomationResponse();
        List<BI_VA_AutomationWrapper.BI_VA_Error> errorList = new  List<BI_VA_AutomationWrapper.BI_VA_Error>();        

        
        //List<BI_VA_CountryCodes__c> lcc= BI_VA_CountryCodes__c.getall().values();

        try{
            if(request.businessUnit == null || request.businessUnit== '' || request.environment == null || request.environment == '')
            {            
                BI_VA_AutomationWrapper.BI_VA_Error error = new  BI_VA_AutomationWrapper.BI_VA_Error() ;
                error.message = 'Business unit or Environment is Invalid/Blank';
                errorList.add(error);
                response.errorMessages = errorList;
                response.responseStatus = 'Failed';
                response.myShopNumber =  request.myShopNumber;
                return response;  
            }
            /*perfoming Password Reset operation when type is Password Reset  */
            
            if(request.type=='Password Reset')
             {
                response = redirect(request);
                return response;
             }
                       
            /*perfoming UnlockUser operation when type is UnlockUser */
            else if(request.type=='Account Unlock')
             {                                                
                response = redirect(request);
                return response;
             }
            else 
             {
                BI_VA_AutomationWrapper.BI_VA_Error error = new  BI_VA_AutomationWrapper.BI_VA_Error() ;
                error.message = 'Invalid Request Type';
                errorList.add(error);
                response.errorMessages = errorList;                                                             
             }          
            
               /*else{
                response.errorMessages=' Provided  User Name is not active ';
               } */
            
         
         } catch(exception e) /*Handling  Exceptions */
           {
            BI_VA_AutomationWrapper.BI_VA_Error error = new  BI_VA_AutomationWrapper.BI_VA_Error() ;
            error.message = 'Provided  User Name is Invalid/Blank';
            errorList.add(error);
            response.errorMessages = errorList;             
            return response; 
           }
            return response;
      } 
    public static BI_VA_AutomationWrapper.BI_VA_AutomationResponse redirect(BI_VA_AutomationWrapper.BI_VA_AutomationRequest request)
      {
        BI_VA_AutomationWrapper.BI_VA_AutomationResponse response =new  BI_VA_AutomationWrapper.BI_VA_AutomationResponse() ;
        
        /* Finding the Sub Org based on Business unit, Environment, Country Code using Custom Settings  */
        
        List<BI_VA_CountryCodes__c> lcc= BI_VA_CountryCodes__c.getall().values();
  
          if(request.businessUnit == 'PM')
          {
              if(request.environment == 'Production')
              {
                  for(BI_VA_CountryCodes__c cc : lcc)
                  {
                      if(request.country == cc.Name)
                      {
                          /* Sending the request to the Target org */
                          
                          BI_VA_CountryCodes__c countryCode = BI_VA_CountryCodes__c.getValues(cc.Name);
                          BI_VA_Routing b = new BI_VA_Routing();
                          response = b.sendRequest(countryCode.PM_Prod__c,request.userName, request.businessUnit, request.emailId, request.Type, request.myShopNumber);
                          break;                                              
                      }
                  }
              }
             
              else if(request.environment == 'QA')
              {
                  for(BI_VA_CountryCodes__c cc : lcc)
                  {
                      if(request.country == cc.Name)
                      {
                          BI_VA_CountryCodes__c countryCode = BI_VA_CountryCodes__c.getValues(cc.Name);
                          BI_VA_Routing b = new BI_VA_Routing();
                          System.debug('orgName'+countryCode.PM_QA__c);
                          response = b.sendRequest(countryCode.PM_QA__c,request.userName, request.businessUnit, request.emailId, request.Type, request.myShopNumber);// calling send Request matched country name based on environment QA and Bussiness unit Related to PM  
                          break;                                              
                      }
                  }
              }
          }
         
          else if(request.businessUnit == 'AH')
          {
              if(request.environment == 'Production')
              {
                  for(BI_VA_CountryCodes__c cc : lcc)
                  {
                      if(request.country == cc.Name)
                      {
                          BI_VA_CountryCodes__c countryCode = BI_VA_CountryCodes__c.getValues(cc.Name);
                          BI_VA_Routing b = new BI_VA_Routing();
                          response = b.sendRequest(countryCode.AH_Prod__c,request.userName, request.businessUnit, request.emailId, request.Type, request.myShopNumber);//calling send Request matched country name based on environment production and Bussiness unit Related to AH 
                          break;                                              
                      }
                  }
              }
               
              else if(request.environment == 'QA')
              {
                  for(BI_VA_CountryCodes__c cc : lcc)
                  {
                      if(request.country == cc.Name)
                      {
                          BI_VA_CountryCodes__c countryCode = BI_VA_CountryCodes__c.getValues(cc.Name);
                          BI_VA_Routing b = new BI_VA_Routing();
                          response = b.sendRequest(countryCode.AH_QA__c,request.userName, request.businessUnit, request.emailId, request.Type, request.myShopNumber);//calling send Request matched country name based on environment QA and Bussiness unit Related to AH 
                          break;                                              
                      }
                  }
              }
          }
          return response; 
       }
         
                
}