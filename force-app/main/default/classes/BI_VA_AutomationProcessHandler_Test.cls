/*
  CreatedBy SAI KIRAN
  DESCRIPTION: This is a Test class for BI_VA_AutomationProcessHandler_Test Class for Webservice
*/
@isTest
public class BI_VA_AutomationProcessHandler_Test {
    
    public static testmethod void testAcutomationProcessHander(){
        test.startTest();
  
      /* Testing the variables with sample data for Expected values  */
        
        BI_VA_CountryCodes__c ccs = new BI_VA_CountryCodes__c();
           ccs.Name = 'US';
           ccs.AH_Prod__c = 'Child 11';
           ccs.AH_QA__c = 'Full 12';
           ccs.PM_Prod__c = 'Child 1';
           ccs.PM_QA__c = 'Full 2';
           insert ccs;
        
        BI_VA_AutomationWrapper.BI_VA_AutomationRequest req= new BI_VA_AutomationWrapper.BI_VA_AutomationRequest();   
           req.userName='Test';
           req.emailId='Test@test11.com';
           req.buinessUnit='AH';
           req.orgName='ORD';
           req.country='US';
           req.environment='QA';
           req.myShopNumber='123456789';
           req.BidsId='123';
           req.Type='ResetPassword';
        System.assertEquals('US', req.country);
        
     
            /* BI_VA_AutomationWrapper.BI_VA_AutomationResponse res= new BI_VA_AutomationWrapper.BI_VA_AutomationResponse();
            res.responseStatus = 'Success';
            res.myShopNumber = '987654321';
            //res.ErroerrorMessages = 'Test error';
            BI_VA_AutomationWrapper.BI_VA_Error error = new BI_VA_AutomationWrapper.BI_VA_Error();
            error.type = 'Test';
            error.message = 'Test Message';*/

        BI_VA_AutomationProcessHandler.processAutomationTask(req);
           test.stopTest();
    }
    
    public static testmethod void testAcutomationProcessHander1(){
        test.startTest();
        List<BI_VA_CountryCodes__c> lcc= BI_VA_CountryCodes__c.getall().values();
        BI_VA_AutomationWrapper.BI_VA_AutomationRequest req= new BI_VA_AutomationWrapper.BI_VA_AutomationRequest(); 
        
        BI_VA_CountryCodes__c ccs = new BI_VA_CountryCodes__c();
           ccs.Name = 'US';
           ccs.AH_Prod__c = 'Child 11';
           ccs.AH_QA__c = 'Full 12';
           ccs.PM_Prod__c = 'Child 1';
           ccs.PM_QA__c = 'Full 2';
           insert ccs;
        
           req.userName='Test222';
           req.emailId='Test222@test11.com';
           req.buinessUnit='PM';
           req.orgName='ORD';
           req.country=ccs.Name;
           req.environment='Production';
           req.myShopNumber='123456789';
           req.BidsId='123';
           req.Type='Password Reset';
           System.assertEquals('US', req.country); 
       BI_VA_AutomationProcessHandler.processAutomationTask(req);
          test.stopTest();
    }
    
     public static testmethod void testAcutomationProcessHander2(){
        test.startTest();
        List<BI_VA_CountryCodes__c> lcc= BI_VA_CountryCodes__c.getall().values();
        BI_VA_AutomationWrapper.BI_VA_AutomationRequest req= new BI_VA_AutomationWrapper.BI_VA_AutomationRequest(); 
        
        BI_VA_CountryCodes__c ccs = new BI_VA_CountryCodes__c();
          ccs.Name = 'US';
          ccs.AH_Prod__c = 'Child 11';
          ccs.AH_QA__c = 'Full 12';
          ccs.PM_Prod__c = 'Child 1';
          ccs.PM_QA__c = 'Full 2';
          insert ccs;
        
          req.userName='Test222';
          req.emailId='Test222@test11.com';
          req.buinessUnit='PM';
          req.orgName='ORD';
          req.country=ccs.Name;
          req.environment='QA';
          req.myShopNumber='123456789';
          req.BidsId='123';
          req.Type='Account Unlock';
          System.assertEquals('US', req.country); 
        BI_VA_AutomationProcessHandler.processAutomationTask(req);
         test.stopTest();
    }
    
     public static testmethod void testAcutomationProcessHander3(){
          test.startTest();
        List<BI_VA_CountryCodes__c> lcc= BI_VA_CountryCodes__c.getall().values();
        BI_VA_AutomationWrapper.BI_VA_AutomationRequest req= new BI_VA_AutomationWrapper.BI_VA_AutomationRequest(); 
        
        BI_VA_CountryCodes__c ccs = new BI_VA_CountryCodes__c();
           ccs.Name = 'US';
           ccs.AH_Prod__c = 'Child 11';
           ccs.AH_QA__c = 'Full 12';
           ccs.PM_Prod__c = 'Child 1';
           ccs.PM_QA__c = 'Full 2';
           insert ccs;
        
           req.userName='Test222';
           req.emailId='Test222@test11.com';
           req.buinessUnit='AH';
           req.orgName='ORD';
           req.country=ccs.Name;
           req.environment='Production';
           req.myShopNumber='123456789';
           req.BidsId='123';
           req.Type='Account Unlock';
        System.assertEquals('US', req.country); 
        BI_VA_AutomationProcessHandler.processAutomationTask(req);
        test.stopTest();
    }
				
        public static testmethod void testAcutomationProcessHander4(){
           test.startTest();
        List<BI_VA_CountryCodes__c> lcc= BI_VA_CountryCodes__c.getall().values();
        BI_VA_AutomationWrapper.BI_VA_AutomationRequest req= new BI_VA_AutomationWrapper.BI_VA_AutomationRequest(); 
        
           BI_VA_CountryCodes__c ccs = new BI_VA_CountryCodes__c();
             ccs.Name = 'US';
             ccs.AH_Prod__c = 'Child 11';
             ccs.AH_QA__c = 'Full 12';
             ccs.PM_Prod__c = 'Child 1';
             ccs.PM_QA__c = 'Full 2';
             insert ccs;
        
             req.userName='Test222';
             req.emailId='Test222@test11.com';
             req.buinessUnit='AH';
             req.orgName='ORD';
             req.country=ccs.Name;
             req.environment='QA';
             req.myShopNumber='123456789';
             req.BidsId='123';
             req.Type='Account Unlock';
           System.assertEquals('US', req.country); 
         BI_VA_AutomationProcessHandler.processAutomationTask(req);
          test.stopTest();
        }       
   
    
       public static testmethod void testAcutomationProcessHander5(){
             test.startTest();
             Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
             User u = new User(Alias = 'standt', Email='standarduser@testorgmyInfo.com', 
             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
             LocaleSidKey='en_US', ProfileId = p.Id, 
             TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorgmyInfo.com');
             INSERT U;
                // System.resetPassword(u.id,true);
                /*UserLogin objUserLogin = [Select UserId,IsPasswordLocked from UserLogin where UserId=:u.id ];
                objUserLogin.IsPasswordLocked = true;
                update objUserLogin; */
       
         System.runAs(u) {
            BI_VA_AutomationWrapper.BI_VA_AutomationRequest req= new BI_VA_AutomationWrapper.BI_VA_AutomationRequest();   
            req.userName='standarduser@testorgmyInfo.com';
            req.emailId='standarduser@testorgmyInfo.com'; 
            req.Type='UnlockUser';
            BI_VA_AutomationProcessHandler.processAutomationTask(req);
           }
          test.stopTest();
    }
      
}