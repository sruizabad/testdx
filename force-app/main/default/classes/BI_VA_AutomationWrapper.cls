/*****************************************************************************************
Name: BI_VA_AutomationWrapper
Copyright © BI
==========================================================================================
==========================================================================================
Purpose:
--------
This is the Wrapper Class that we are Exposing for Myshop to consume
===========================================================================================
===========================================================================================
History
-------
VERSION        AUTHOR                  DATE               DETAIL

 1.0         Visweswara Rao                            Initial development

******************************************************************************************/
global class BI_VA_AutomationWrapper {
    
    global class BI_VA_AutomationRequest {
    
        webservice String userName;
        webservice String emailId;
        webservice String businessUnit;
        //webservice String orgName;
        webservice String country;
        webservice String environment;
        webservice String myShopNumber;
        webservice String BidsId;//for testing
        webservice String Type; //for testing
    
     }
     
       /*Created variables for Response Status,Myshop Number And Error Message*/
     global class BI_VA_AutomationResponse {
             webservice String responseStatus;
             webservice String myShopNumber;
             webservice list<BI_VA_Error> errorMessages;
             //webservice String  errorMessages;//for testing 
      }
    
     /*Created variables for Error Type And Message*/
    global class BI_VA_Error {
       
           webservice String type;
           webservice String message;
    }
}