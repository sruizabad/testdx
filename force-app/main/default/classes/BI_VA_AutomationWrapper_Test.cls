/*
  DESCRIPTION: This is a Test class for BI_VA_AutomationWrapperTest Class for Webservice
*/

@isTest
Public class BI_VA_AutomationWrapper_Test {
    
        Static testMethod void automationTest(){
         //List<String> Error= New List<String>();
         test.startTest();
        
         /* Testing the variables with sample data for Expected values  */
        
           BI_VA_AutomationWrapper.BI_VA_AutomationRequest req= new BI_VA_AutomationWrapper.BI_VA_AutomationRequest();   
           req.userName='Test@test.com';
           req.emailId='Test@test11.com';
           req.buinessUnit='AH';
           req.orgName='ORD';
           req.country='US';
           req.environment='QA';
           req.myShopNumber='123456789';
           req.BidsId='123';
           req.Type='ResetPassword';
           System.assertEquals('US', req.country);
           
        
        BI_VA_AutomationWrapper.BI_VA_AutomationResponse res= new BI_VA_AutomationWrapper.BI_VA_AutomationResponse();
          res.responseStatus = 'Success';
          res.myShopNumber = '987654321';
          // error.add(res);
          // res.errorMessages = 'Test error';
    
        
        BI_VA_AutomationWrapper.BI_VA_Error error = new BI_VA_AutomationWrapper.BI_VA_Error();
          error.type = 'Test';
          error.message = 'Test Message';
          test.stopTest();
       }
}