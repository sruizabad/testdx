public class BI_VA_Routing
{
    
    String endPoint;
    
    public BI_VA_AutomationWrapper.BI_VA_AutomationResponse sendRequest(String OrgName, String UserId, String BU, String email, String type, String myShopNum)
    {
        BI_VA_AutomationWrapper.BI_VA_AutomationResponse res =new  BI_VA_AutomationWrapper.BI_VA_AutomationResponse() ;  
        List<BI_VA_AutomationWrapper.BI_VA_Error> errorList = new  List<BI_VA_AutomationWrapper.BI_VA_Error>() ;       
       
        try
        {
             BI_VA_Routing_Details__c routingDetails = BI_VA_Routing_Details__c.getValues(OrgName);                    
             endPoint = routingDetails.BI_VA_EndPoint__c;    
        }
        catch(NullPointerException npe)
        {
             BI_VA_AutomationWrapper.BI_VA_Error error = new  BI_VA_AutomationWrapper.BI_VA_Error() ;
             error.message = 'Please provide proper business unit or environment';
             errorList.add(error);
             res.errorMessages = errorList; 
             res.responseStatus = 'Failed';
             res.myShopNumber = myShopNum;             
             return res;
        }        
        
        try
        {   
                 
             Http h1 = new Http();
             HttpRequest req1 = new HttpRequest();             
             req1.setHeader('Content-Type','application/json');
             req1.setHeader('accept','application/json');
             req1.setBody('{"userId" : "'+UserId+'","BU" : "'+BU+'", "email" : "'+email+'", "type" : "'+type+'", "myShopNum" : "'+myShopNum+'"}');//Send JSON body
             req1.setMethod('PUT');
             req1.setEndpoint(endPoint+'/services/apexrest/BiAutomation/');
             HttpResponse res1 = h1.send(req1);
             system.debug('RESPONSE_BODY'+res1.getbody());
             BI_VA_AutomationResponseFromRedirectedOrg response = (BI_VA_AutomationResponseFromRedirectedOrg)JSON.deserialize(res1.getbody(), BI_VA_AutomationResponseFromRedirectedOrg.class);
             res.responseStatus = response.responseStatus;
             res.myShopNumber = response.myShopNumber;
             BI_VA_AutomationWrapper.BI_VA_Error error = new  BI_VA_AutomationWrapper.BI_VA_Error() ;
             error.message = response.errorMessages;
             errorList.add(error);
             res.errorMessages = errorList;              
             return res;
            
        }
        catch(Exception e)
        {
            System.debug('Exception occured: '+e.getMessage());
            BI_VA_AutomationWrapper.BI_VA_Error error = new  BI_VA_AutomationWrapper.BI_VA_Error() ;
            error.message = e.getMessage();
            errorList.add(error);
            res.errorMessages = errorList;            
            return res;
        }
    }
    
    public void sendLoadRequest(String OrgName, Blob file)
    {
        BI_VA_Routing_Details__c routingDetails = BI_VA_Routing_Details__c.getValues(OrgName);        
        endPoint = routingDetails.BI_VA_EndPoint__c;
        String blobToString = EncodingUtil.base64Encode(file);        
        
        try
        {      
             Http h1 = new Http();
             HttpRequest req1 = new HttpRequest();            
             req1.setHeader('Content-Type','application/json');
             req1.setHeader('accept','application/json');
             req1.setBody('{"file" : "'+blobToString+'"}');//Send JSON body
             req1.setMethod('POST');
             req1.setEndpoint(endPoint+'/services/apexrest/BiAutomation/');
             HttpResponse res1 = h1.send(req1);
             system.debug('RESPONSE_BODY Data Load '+res1.getbody());            
        }
        catch(Exception e)
        {
            System.debug('Exception occured: '+e.getMessage());
        }
    }
    public class BI_VA_AutomationResponseFromRedirectedOrg 
    {
    
        public String responseStatus;
        public String myShopNumber;
        //public list<Error> errorMessages;
        public String  errorMessages;//for testing 
    
    }
    
}