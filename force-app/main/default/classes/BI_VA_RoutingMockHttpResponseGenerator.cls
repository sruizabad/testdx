@isTest
global class BI_VA_RoutingMockHttpResponseGenerator implements HttpCalloutMock {
   
    global HTTPResponse respond(HTTPRequest req) {
        
        System.assertEquals('callout:BI_VA_Full3Cred/services/apexrest/BiAutomation/', req.getEndpoint());        
        
        // Creating a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"responseStatus":"test","myShopNumber":"test","errorMessages":"test"}');
        res.setStatusCode(200);
        return res;
    }
}