@RestResource(urlMapping = '/BiAutomation/*')
global class BI_VA_Routing_Target
{
    
    @HttpPut
    global static BI_VA_AutomationResponse doUnlockOrPwdRst(String BIDSGlobalID, String BU, String email, String type, String myShopNum)
    {
        BI_VA_AutomationResponse response = new BI_VA_AutomationResponse();        
          
        List<User> UserDetails;
        response.myShopNumber = myShopNum; 
         //Test  
        try
        {
            UserDetails= [select id,Profile.IsSsoEnabled,BIDS_ID_BI__c,Business_BI__c,email,IsActive from user where  BIDS_ID_BI__c=:BIDSGlobalID and isActive=True];
            if(UserDetails.size() == 0)
            {
                UserDetails = [select id,Profile.IsSsoEnabled,BIDS_ID_BI__c,Business_BI__c,email,IsActive from user where  email =:email and isActive=True];
                if(UserDetails.size() == 0)
                {
                    response.errorMessages= 'Invalid email (or) BIDS Global ID'; 
                    response.responseStatus = 'Failed';
                    return response;
                }
                if(UserDetails.size() > 1)
                {
                    response.errorMessages= 'Multiple Users found with provided email /BIDS Global ID';
                    response.responseStatus = 'Failed';
                    return response;
                }
            }
        }
        catch(QueryException qe)
        {
           response.errorMessages= 'Invalid global id or Wrong user deatils'; 
           response.responseStatus = 'Failed';
           return response;
        } 
           
             //Checking condition whether  user is active and should not be null based on id we are getting from soql query
            if(UserDetails[0].IsActive==true)
            {               
                    if(UserDetails[0].Business_BI__c !=BU && BU!=Null && BU !='PM' && BU != 'AH')
                    {
                        response.errorMessages='Provided Business Unit is Invalid '; 
                        response.responseStatus = 'Failed';
                        return response;                          
                    }
              
              // Checking condition whether user is request user mail id should be matched with existing mail id
                    if(UserDetails[0].email!=email)
                    {
                        response.errorMessages='Provided Email is Invalid';
                        response.responseStatus = 'Failed';
                        return response;
                    }
                //perfoming reset operation when type is ResetPassword if its not SSO enabled
                    if(type=='Password Reset')
                    {
                        if(UserDetails[0].Profile.IsSsoEnabled!=True)
                        {                                            
                              system.resetPassword(UserDetails[0].id, true);
                              response.responseStatus='Success';
                              return response;
                        }                       
                          //Sending message for ResetPassword if its SSO enabled
                        else 
                        {
                              response.errorMessages='User is SSO enabled, please forward to GSD'; 
                              response.responseStatus='Failed';  
                              return response;
                        }                  
                    }
                 /*perfoming UnlockUser operation when type is UnlockUser based on IsPasswordLocked values from userlogin and we showing message if user is not locked */
                    if(type=='Account Unlock')
                    {
                        userlogin UL;
                        try
                        {
                            UL=[select id,IsPasswordLocked from userlogin where userId = :UserDetails[0].id]  ;
                        }
                        catch(QueryException qe)
                        {
                            response.errorMessages= 'User is having no login history'; 
                            response.responseStatus = 'Failed';
                            return response;
                        }
                        if(UL.IsPasswordLocked==True)
                        {
                            UL.IsPasswordLocked=False;
                            try
                            {
                                update UL;
                            }
                            catch(exception e)
                            {
                                response.errorMessages='Error while processing User unlock request, please contact administrator.';
                                response.responseStatus='Failed';
                                return response;
                            }
                            response.responseStatus='Provided user unlock has been done.';             
                            return response;
                        }
                        else 
                        {
                            response.errorMessages='Provided user is not locked. In case of password reset, please raise a new request. ';
                            return response;
                        }                        
                        
                    }
             } 
            else 
            {
                response.errorMessages='Provided user is not active or not available. ';
                response.responseStatus= 'Failed';
                return response;
            }   
        return response;
    }
    
    global class BI_VA_AutomationResponse 
    {    
        String responseStatus;
        String myShopNumber;        
        String  errorMessages;    
    }

    global class BI_VA_Error 
    {   
        String type;
        String message;    
    }
}