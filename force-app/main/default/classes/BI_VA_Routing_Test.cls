@isTest
private class BI_VA_Routing_Test
{
    private static testMethod void routingTest()
    {
        BI_VA_Routing_Details__c routingDetails = new BI_VA_Routing_Details__c(Name = 'Test');
        routingDetails.BI_VA_EndPoint__c = 'callout:BI_VA_Full3Cred';
        insert routingDetails;
        
        Test.setMock(HttpCalloutMock.class, new BI_VA_RoutingMockHttpResponseGenerator());
        Test.startTest();
        BI_VA_Routing route = new BI_VA_Routing();
        BI_VA_AutomationWrapper.BI_VA_AutomationResponse res = route.sendRequest('Test','Test','Test','Test','password reset','Test');
        Test.stopTest();
        
    }
    private static testMethod void routingExceptionTest()
    {
        BI_VA_Routing_Details__c routingDetails = new BI_VA_Routing_Details__c(Name = 'Test');
        routingDetails.BI_VA_EndPoint__c = 'callout:BI_VA_Full3Cred';
        insert routingDetails;
        
        Test.setMock(HttpCalloutMock.class, new BI_VA_RoutingMockHttpResponseGenerator());
        Test.startTest();
        BI_VA_Routing route = new BI_VA_Routing();
        BI_VA_AutomationWrapper.BI_VA_AutomationResponse res = route.sendRequest('Test1','Test','Test','Test','password reset','Test');
        Test.stopTest();
        String test = 'Test';
        Blob b= Blob.valueOf(test);
        route.sendLoadRequest('Test',b);
    }
    private static testMethod void routeExceptionTest2()
    {
        BI_VA_Routing_Details__c routingDetails = new BI_VA_Routing_Details__c(Name = 'Test');
        routingDetails.BI_VA_EndPoint__c = '';
        insert routingDetails;
        
        Test.setMock(HttpCalloutMock.class, new BI_VA_RoutingMockHttpResponseGenerator());        
        BI_VA_Routing route = new BI_VA_Routing();
        BI_VA_AutomationWrapper.BI_VA_AutomationResponse res = route.sendRequest('Test','Test','Test','Test','password reset','Test');
        
    }
    private static testMethod void routingDataLoadTest()
    {
        BI_VA_Routing_Details__c routingDetails = new BI_VA_Routing_Details__c(Name = 'Test');
        routingDetails.BI_VA_EndPoint__c = 'callout:BI_VA_Full3Cred';
        insert routingDetails;
        
        Test.setMock(HttpCalloutMock.class, new BI_VA_RoutingMockHttpResponseGenerator());
        String body = 'Test';
        Blob b= Blob.valueOf(body);
        Test.startTest();
        BI_VA_Routing route = new BI_VA_Routing();
        route.sendLoadRequest('Test',b);
        Test.stopTest();       
        
    }
}