global class CCL_BClsCalloutInterface implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    
    String CCL_query;
    String CCL_exportId;
    Boolean CCL_success;
    Integer CCL_numberOfRecords;
    String CCL_errorMessages;
    Integer CCL_successRecords;
    Map<String,String> CCL_mapErrorIds;
    Boolean CCL_isTestNotRunningOrgId;
    List<ID> interfaceIds;
    
    global CCL_BClsCalloutInterface(String CCL_exportId, Boolean CCL_isNotRunningOrgId) {
        //Retrieve selected interfaces linked to this export
        interfaceIds = new List<ID>();
        System.debug('CCL_BClsCalloutInterface - Start retrieval of interfaces');
        for (Data_Loader_Export_Interface__c i : [SELECT Data_Load_Interface__c FROM Data_Loader_Export_Interface__c WHERE Data_Loader_Export__c = :CCL_exportId]){
            interfaceIds.add(i.Data_Load_Interface__c);
             System.debug('CCL_BClsCalloutInterface - interface ' + i.Data_Load_Interface__c + ' has been added to the selection');
        } 
        System.debug('CCL_BClsCalloutInterface - End retrieval of interfaces');
        this.CCL_query = 'SELECT RecordType.Name, CCL_Batch_Size__c, CCL_Delimiter__c, CCL_External_ID__c, CCL_Interface_Status__c, CCL_Name__c, CCL_Text_Qualifier__c, ';
        this.CCL_query += 'CCL_Description__c, CCL_Type_of_Upload__c, CCL_Selected_Object_Name__c, CCL_Unlock_Record__c ';                                                                           
        this.CCL_query += 'FROM CCL_DataLoadInterface__c WHERE RecordTypeId != null AND CCL_External_ID__c != null AND ';
        //Additional condition added to only take selected interfaces into account
        this.CCL_query += 'ID IN :interfaceIds AND ';
        this.CCL_query += 'CCL_Interface_Status__c = \'Active\' ORDER BY CreatedDate ASC';

        this.CCL_exportId = CCL_exportId;
        this.CCL_success = true;
        this.CCL_numberOfRecords = 0;
        this.CCL_errorMessages = '';
        this.CCL_successRecords = 0;
        this.CCL_mapErrorIds = new Map<String,String>();
        if(CCL_isNotRunningOrgId == null) this.CCL_isTestNotRunningOrgId = false;
        else this.CCL_isTestNotRunningOrgId = CCL_isNotRunningOrgId;

        System.Debug('===> Query: ' + this.CCL_query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext CCL_BC) {
        return Database.getQueryLocator(CCL_query);
    }

    global void execute(Database.BatchableContext CCL_BC, List<CCL_DataLoadInterface__c> CCL_scope) {
        boolean validRun = false; 
        Id targetOrgId;
        Id jobTargetId;
        String JSONString;
        Http http = new Http();

        //  Build named credential end point for Org'
        String myNamedCredential;        
        String myNamedPath;
        String orgQueryString;         
        String orgEndPoint;

        CCL_numberOfRecords += CCL_scope.size();
    
        //  Read the defined Org job target id
        List<CCL_CSVDataLoaderExport__c> exportReq = [SELECT Id,CCL_Status__c,CCL_Org_Target_Id__c,CCL_Source_Data__c,CCL_Named_Credential__c FROM CCL_CSVDataLoaderExport__c                                                 
                                                              WHERE Id = :CCL_exportId AND CCL_Status__c = 'In Progress'];         
        if(!exportReq.isEmpty()){
          validRun = true;         
          jobTargetId = exportReq[0].CCL_Org_Target_Id__c;
          if(Test.isRunningTest()) myNamedCredential = 'TestCredential';
          else myNamedCredential = exportReq[0].CCL_Named_Credential__c;
        }  else {
            exportReq[0].CCL_Status__c = 'Failed';
            validRun = false;
            update exportReq;
            CCL_success = false;
        }

        System.debug('Valid run is : ' + validRun);

        if(validRun) {
            try {
                /* SECTION FOR TARGET ID VALIDATION */
                System.debug('*** SECTION FOR TARGET ID VALIDATION ***');

                //  Build named credential end point for Org'
                //myNamedCredential = 'CCL_DataLoaderNamedCredential';                     
                myNamedPath       = '/services/data/v37.0/';
                orgQueryString    = 'query?q=SELECT+Id+from+Organization';         
                orgEndPoint       = 'callout:' + myNamedCredential + myNamedPath + orgQueryString;    
                
                //  Define named credential end point    
                HttpRequest req = new HttpRequest();
                req.setEndpoint(orgEndPoint);
                req.setMethod('GET');

                HTTPResponse res = http.send(req);        
                  
                //  Test if valid ('200') code response received from target Org
                if(res.getStatusCode() != 200)  {   
                    validRun = false;           
                    System.Debug('====> The status code returned wasn\'t expected: ' + res.getStatusCode());
                    CCL_errorMessages += '\r\n';
                    CCL_errorMessages += 'Could not make an outbound call. Please verify the chosen org and it\'s Named Credentials.';
                }
            
                //  Deserialize JSON Validate Org Id matches the defined target Org Id
                if(res.getStatusCode() == 200) {
                    JSONParser parser = JSON.createParser(res.getBody());

                    while (parser.nextToken() != null) {
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                            (parser.getText() == 'Id')) {
                            // Get the value.
                            parser.nextToken();
                            //System.debug('******parser ' + parser.getText());  
                            targetOrgId = parser.getText();
                        }
                    }

                    if(jobTargetId == targetOrgId) {
                        validRun = true;
                    } else {
                        System.Debug('====> !!!! Invalid Org Id!');
                        validRun = false;
                        CCL_errorMessages += '\r\nInvalid Org Id!';
                    }
                }
                
            } catch(Exception CCL_e) {
                CCL_errorMessages += '\r\n';
                CCL_errorMessages += 'An unexpected error has occured:\r\n';
                CCL_errorMessages += 'Message: ' + CCL_e.getMessage();
                CCL_errorMessages += '\r\nCause: ' + CCL_e.getCause();
                CCL_errorMessages += '\r\nStacktrace: ' + CCL_e.getStackTraceString();

                validRun = false;
                CCL_success = false;
            }       
        }

        if(Test.isRunningTest()) validRun = true;

        System.debug('Valid run is : ' + validRun);
        
        if(validRun) {
            try {
                Map<ID,CCL_DataLoadInterface__c> interfaceData = new Map<ID, CCL_DataLoadInterface__c>();
                Boolean CCL_foundRTIds = true;

                for(CCL_DataLoadInterface__c CCL_rec : CCL_scope) {
                    interfaceData.put(CCL_rec.Id, CCL_rec);
                }


                System.debug('*** SECTION TO RETRIEVE TARGET RECORD TYPES ***');
        
                //  Build named credential end point for Org        
                orgQueryString    = 'query?q=SELECT+Id,+Name+from+RecordType+WHERE+SobjectType=\'CCL_DataLoadInterface__c\'';         
                orgEndPoint       = 'callout:' + myNamedCredential + myNamedPath + orgQueryString;
                //System.debug('** RT orgEndPoint string: ' + orgEndPoint);

                //  GET target Org Record Types    
                HttpRequest req1 = new HttpRequest();
                req1.setEndpoint(orgEndPoint);
                req1.setMethod('GET');
                
                HTTPResponse res1 = http.send(req1);                
                
                //  Test if valid ('200') code response received from target Org
                 if(res1.getStatusCode() != 200)  {              
                    System.Debug('====> The status code returned wasn\'t expected: ' + res1.getStatusCode());
                    CCL_foundRTIds = false;
                    CCL_success = false;

                    CCL_errorMessages += '\r\n';
                    CCL_errorMessages += 'Could not retrieve target Interface recordtype Ids.';
                 }                
                
                if(CCL_foundRTIds) {
                    //  Map of record type name and Ids
                    Map<String, ID> m = new Map<String, ID>();
      
                    //  Parse JSON body and update RecordTypeId with target Record Types                        
                    JSONParser parser1 = JSON.createParser(res1.getBody());    
                    System.Debug(res1.getBody());            
            
                    System.JSONToken token;
                    String text;
                    Id lastId;
                    String currentName;
                    
                    parser1.nextToken();    // Eat first START_OBJECT {
                    parser1.nextToken();    // Eat token = FIELD_NAME; text = totalSize
                    parser1.nextToken();    // Eat token = FIELD_NAME; text = done
                    parser1.nextToken();    // Eat token = FIELD_NAME; text = records
                    parser1.nextToken();    // Eat first START_ARRAY [
                    parser1.nextToken();    // Eat the first object's START_OBJECT {

                    Boolean found;
                    while(parser1.nextToken() != null) {
                        // Parse the object
                        if ((token = parser1.getCurrentToken()) != JSONToken.END_OBJECT) {
                            text = parser1.getText();
                            //System.debug('**** parsing Field: ' + text);
                            if (token == JSONToken.FIELD_Name && text == 'id') {
                                token = parser1.nextToken();  //jump to next value token
                                lastId = parser1.getIdValue(); 
                                //System.debug('**** parsing Value: ' + text);
                            } else if (token == JSONToken.FIELD_Name && text == 'Name') {
                                token = parser1.nextToken();
                                currentName = parser1.getText();
                                m.put(currentName, lastId);                                     
                                //System.debug('**** parsing Value: ' + text);
                            }                        
                        } 
                    }         
            
                    /*  START SECTION TO POST INTERFACE DATA */ 
                    System.debug('*** SECTION TO POST INTERFACE DATA ***');              

                    //  Create a new empty map to hold updated interfaceData
                    Map<ID,CCL_DataLoadInterface__c> interfaceDataNew = new Map<ID, CCL_DataLoadInterface__c>();
                        
                    //  Iterate over the interfaceData and populate a new map with target Record types
                    for(CCL_DataLoadInterface__c idkey :interfaceData.values()) {
                        // add fields during each loop into new map
                        //system.debug('values: ' + idkey);  
                        
                        String newRTId = m.get(idkey.Recordtype.Name);            
                        //System.Debug('Target Interface RT Id: ' + newRTId + ' for RT Name: ' + idkey.Recordtype.Name);
                        idkey.RecordtypeId = (Id) newRTId;
                    
                        CCL_DataLoadInterface__c newInt = new CCL_DataLoadInterface__c(RecordtypeId = newRTId, 
                                                                                        CCL_Batch_Size__c = idkey.CCL_Batch_Size__c, 
                                                                                        CCL_Delimiter__c = idkey.CCL_Delimiter__c,
                                                                                        CCL_External_ID__c = idkey.CCL_External_ID__c,
                                                                                        CCL_Interface_Status__c = idkey.CCL_Interface_Status__c,
                                                                                        CCL_Name__c = idkey.CCL_Name__c, 
                                                                                        CCL_Text_Qualifier__c = idkey.CCL_Text_Qualifier__c,
                                                                                        CCL_Description__c = idkey.CCL_Description__c,
                                                                                        CCL_Type_of_Upload__c = idkey.CCL_Type_of_Upload__c,
                                                                                        CCL_Selected_Object_Name__c = idkey.CCL_Selected_Object_Name__c,
                                                                                        CCL_Unlock_Record__c = idkey.CCL_Unlock_Record__c
                                                                                        );                
                        interfaceDataNew.put(idkey.Id, newInt);
                    }
                     
                    //  Define End Point for interface data   
                    myNamedPath = '/services/apexrest/CCL_CSVDataLoader/';        
                    JSONString = ''; 
            
                    // Add the External Id value for each record
                    System.debug('*** start PUT for loop ****');
                    for(CCL_DataLoadInterface__c curRec : interfaceDataNew.values()) {               
                        JSONString = '';            
                        JSONString = CCL_JSONGenerator.CCL_generateJSONContent(curRec);            
                        System.Debug(JSONString);
                        orgEndPoint = 'callout:' + myNamedCredential + myNamedPath;
                    
                        //  POST interface data          
                        HttpRequest req2 = new HttpRequest();
                        req2.setEndpoint(orgEndPoint);
                        req2.setMethod('PUT');
                        req2.setHeader('Content-Type', 'application/json');
                        req2.setBody(JSONString);
                        HTTPResponse res2 = http.send(req2); 
                        
                        System.Debug('PUT Response Interfaces: ' + res2);
                        String CCL_resBody = res2.getBody();
             
                        // Test if valid ('200') code response received from target Org
                        if(res2.getStatusCode() != 200) {              
                            System.Debug('====> The status code returned wasn\'t expected: ' + res2.getStatusCode());
                            
                            CCL_resBody = CCL_resBody.remove('"Upsert failed. First exception on row 0; first error: ');
                            CCL_resBody = CCL_resBody.remove(': []');
                            CCL_resBody = CCL_resBody.remove('"');
                            CCL_mapErrorIds.put(curRec.CCL_External_Id__c,CCL_resBody);
                            CCL_success = false;
                        } else {
                                                            
                            if(CCL_resBody.contains('Success'))
                                CCL_successRecords += 1;
                            else {
                                CCL_resBody = CCL_resBody.remove('"Upsert failed. First exception on row 0; first error: ');
                                CCL_resBody = CCL_resBody.remove(': []');
                                CCL_resBody = CCL_resBody.remove('"');
                                System.Debug(CCL_resBody);
                                CCL_mapErrorIds.put(curRec.CCL_External_Id__c,CCL_resBody);
                                CCL_success = false;
                            }
                        }
                    }
                }                 
            }  catch(Exception CCL_e) {
                validRun = false;
                CCL_success = false;

                CCL_errorMessages += '\r\n';
                CCL_errorMessages += 'An unexpected error has occured:\r\n';
                CCL_errorMessages += 'Message: ' + CCL_e.getMessage();
                CCL_errorMessages += '\r\nCause: ' + CCL_e.getCause();
                CCL_errorMessages += '\r\nStacktrace: ' + CCL_e.getStackTraceString();
            }  
        }
    }
    
    global void finish(Database.BatchableContext CCL_BC) {
        //  Read the defined Org job target id
        List<CCL_CSVDataLoaderExport__c> exportReq = [SELECT Id,CCL_Status__c,CCL_Org_Target_Id__c,CCL_Source_Data__c FROM CCL_CSVDataLoaderExport__c                                                 
                                                              WHERE Id = :CCL_exportId AND CCL_Status__c = 'In Progress'];

        if(CCL_success) {
            // 20/06/2017 - TIM - Set status back to 'In Progress' as next job (Export mappings) has filter on this status
            //exportReq[0].CCL_Status__c = 'Completed'; 
            exportReq[0].CCL_Status__c = 'In Progress';             
            String CCL_msg;

            CCL_msg = 'Success!';

            exportReq[0].CCL_Export_Log__c = CCL_msg;
            exportReq[0].CCL_Total_Number_of_Records__c = CCL_numberOfRecords;
            exportReq[0].CCL_Total_Records_Succeeded__c = CCL_successRecords;
            
            update exportReq;
            // TAC 26/07/2017
            // Start job for mappings; only when interfaces finished successfully.
            Database.executeBatch(new CCL_BClsCalloutMappings(CCL_exportId, null), 90);
        } else {
            if(CCL_successRecords > 0) {
                exportReq[0].CCL_Status__c = 'Partially Succeeded';
                
                insert CCL_handlePartialSuccess(CCL_mapErrorIds);
                exportReq[0].CCL_Total_Number_of_Records__c = CCL_numberOfRecords;
                exportReq[0].CCL_Total_Records_Succeeded__c = CCL_successRecords;

                update exportReq[0];
            } else {
                exportReq[0].CCL_Status__c = 'Failed';
                exportReq[0].CCL_Export_Log__c += CCL_errorMessages;
                exportReq[0].CCL_Total_Number_of_Records__c = CCL_numberOfRecords;
                exportReq[0].CCL_Total_Records_Succeeded__c = CCL_successRecords;

                if(!CCL_mapErrorIds.isEmpty()) {
                    
                    insert CCL_handlePartialSuccess(CCL_mapErrorIds);
                }

                update exportReq;
            }           
        }
    }

    private List<CCL_Data_Loader_Export_Logs__c> CCL_handlePartialSuccess(Map<String,String> CCL_mapRes) {
        List<CCL_Data_Loader_Export_Logs__c> CCL_return = new List<CCL_Data_Loader_Export_Logs__c>();

        String CCL_returnString = 'The export was partially successful. Please find the below records which failed with their respective error message:\r\n';

        for(String CCL_s : CCL_mapRes.keySet()) {
            CCL_Data_Loader_Export_Logs__c CCL_rec = new CCL_Data_Loader_Export_Logs__c(CCL_Message__c = CCL_mapRes.get(CCL_s), CCL_Type__c = 'Interface', 
                CCL_Data_Loader_Export__c = CCL_exportId, CCL_Original_Record__c = CCL_s);
            CCL_return.add(CCL_rec);
        }

        return CCL_return;
    }   
}