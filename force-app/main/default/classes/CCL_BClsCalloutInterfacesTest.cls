/***************************************************************************************************************************
Apex Class Name :	CCL_BClsCalloutInterfacesTest
Version : 			1.0.0
Created Date : 		22/11/2016
Function : 			Test class for CCL_BClsCalloutInterfaces
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Steve van Noort					  	22/11/2016      		        		Initial creation
***************************************************************************************************************************/
@isTest
private class CCL_BClsCalloutInterfacesTest {
	@testSetup static void CCL_createTestData() {
		Id CCL_RTInterfaceInsert = [SELECT Id FROM Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records'].Id;

        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'userterritory',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert',
																	CCL_External_Id__c = 'EXTTEST1', RecordtypeId = CCL_RTInterfaceInsert);
        insert CCL_interfaceRecord;
		System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_User = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'userid',
																	CCL_Column_Name__c = 'User ID', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'userid', CCL_SObject_Name__c = 'userterritory', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id,
																	CCL_External_Id__c = 'EXTMAP1');
		insert CCL_mappingRecord_User;
		System.Debug('===CCL_mappingRecord_User: ' + CCL_mappingRecord_User.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Terr = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'territoryid',
																	CCL_Column_Name__c = 'Territory ID', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'territoryid', CCL_SObject_Name__c = 'userterritory', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = true);
		insert CCL_mappingRecord_Terr;
		System.Debug('===CCL_mappingRecord_Terr: ' + CCL_mappingRecord_Terr.Id);

		CCL_Org_Connection__c CCL_orgCon = new CCL_Org_Connection__c(Name = 'Test', CCL_Named_Credential__c = 'TestCredential', CCL_Org_Active__c = true, 
																	CCL_Org_Id__c = UserInfo.getOrganizationId(), CCL_Org_Type__c = 'Developer Edition');

		insert CCL_orgCon;

		CCL_CSVDataLoaderExport__c CCL_export = new CCL_CSVDataLoaderExport__c(CCL_Org_Connection__c = CCL_orgCon.Id, CCL_Source_Data__c = 'Mappings',
																				CCL_Status__c = 'In Progress');
		insert CCL_export;
	}
	
	@isTest static void CCL_testOrgId() {
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		CCL_DataLoadInterface__c CCL_int = [SELECT Id, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c LIMIT 1];
		CCL_int.CCL_Interface_Status__c = 'Active';
		update CCL_int;

		String CCL_orgIdResponse = '{"totalSize":1,"done":true,"records":[{"attributes":{"type":"Organization","url":"/services/data/v37.0/sobjects/Organization/'+ UserInfo.getOrganizationId() + '"},"Id":"' + UserInfo.getOrganizationId() + '"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_orgIdResponse, CCL_responseHeaders));

		Test.startTest();

		CCL_BClsCalloutInterface CCL_batchInterface = new CCL_BClsCalloutInterface(CCL_export.Id, null);
		Database.executeBatch(CCL_batchInterface, 10);

		Test.stopTest();

		CCL_export = [SELECT Id, CCL_Status__c, CCL_Export_Log__c FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		List<CCL_Data_Loader_Export_Logs__c> CCL_logs = [SELECT Id FROM CCL_Data_Loader_Export_Logs__c];

		System.assertEquals('Failed', CCL_export.CCL_Status__c);
		System.assert(CCL_export.CCL_Export_Log__c != null);
		System.assertEquals(0, CCL_logs.size());
	}

	@isTest static void CCL_testInvalidOrgId() {
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		CCL_DataLoadInterface__c CCL_int = [SELECT Id, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c LIMIT 1];
		CCL_int.CCL_Interface_Status__c = 'Active';
		update CCL_int;

		String CCL_orgIdResponse = '{"totalSize":1,"done":true,"records":[{"attributes":{"type":"Organization","url":"/services/data/v37.0/sobjects/Organization/000000000000000000"},"Id":"000000000000000000"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_orgIdResponse, CCL_responseHeaders));

		Test.startTest();

		CCL_BClsCalloutInterface CCL_batchInterface = new CCL_BClsCalloutInterface(CCL_export.Id, null);
		Database.executeBatch(CCL_batchInterface, 10);

		Test.stopTest();

		CCL_export = [SELECT Id, CCL_Status__c, CCL_Export_Log__c FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		List<CCL_Data_Loader_Export_Logs__c> CCL_logs = [SELECT Id FROM CCL_Data_Loader_Export_Logs__c];

		System.assertEquals('Failed', CCL_export.CCL_Status__c);
		System.assert(CCL_export.CCL_Export_Log__c != null);
		System.assertEquals(0, CCL_logs.size());
	}

	@isTest static void CCL_testInvalidResponseOrgId() {
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		CCL_DataLoadInterface__c CCL_int = [SELECT Id, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c LIMIT 1];
		CCL_int.CCL_Interface_Status__c = 'Active';
		update CCL_int;

		String CCL_orgIdResponse = '';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(400, 'Bad Request', CCL_orgIdResponse, CCL_responseHeaders));

		Test.startTest();

		CCL_BClsCalloutInterface CCL_batchInterface = new CCL_BClsCalloutInterface(CCL_export.Id, null);
		Database.executeBatch(CCL_batchInterface, 10);

		Test.stopTest();

		CCL_export = [SELECT Id, CCL_Status__c, CCL_Export_Log__c FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		List<CCL_Data_Loader_Export_Logs__c> CCL_logs = [SELECT Id FROM CCL_Data_Loader_Export_Logs__c];

		System.assertEquals('Failed', CCL_export.CCL_Status__c);
		System.assert(CCL_export.CCL_Export_Log__c.contains('Could not make an outbound call'));
	}

	@isTest static void CCL_testValidInterface() {
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		CCL_DataLoadInterface__c CCL_int = [SELECT Id, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c LIMIT 1];
		CCL_int.CCL_Interface_Status__c = 'Active';
		update CCL_int;

		String CCL_mappingResponse = 'Success';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_mappingResponse, CCL_responseHeaders));

		Test.startTest();

		CCL_BClsCalloutInterface CCL_batchInterface = new CCL_BClsCalloutInterface(CCL_export.Id, true);
		Database.executeBatch(CCL_batchInterface, 10);

		Test.stopTest();

		CCL_export = [SELECT Id, CCL_Status__c, CCL_Export_Log__c FROM CCL_CSVDataLoaderExport__c LIMIT 1];

		System.assertEquals('Failed', CCL_export.CCL_Status__c);
		System.assert(CCL_export.CCL_Export_Log__c != null);
	}

	@isTest static void CCL_testInvalidInterface() {
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		CCL_DataLoadInterface__c CCL_int = [SELECT Id, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c LIMIT 1];
		CCL_int.CCL_Interface_Status__c = 'Active';
		update CCL_int;

		String CCL_mappingResponse = 'Error';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(400, 'Bad Request', CCL_mappingResponse, CCL_responseHeaders));

		Test.startTest();

		CCL_BClsCalloutInterface CCL_batchInterface = new CCL_BClsCalloutInterface(CCL_export.Id, true);
		Database.executeBatch(CCL_batchInterface, 10);

		Test.stopTest();

		CCL_export = [SELECT Id, CCL_Status__c, CCL_Export_Log__c FROM CCL_CSVDataLoaderExport__c LIMIT 1];

		System.assertEquals('Failed', CCL_export.CCL_Status__c);
		System.assert(CCL_export.CCL_Export_Log__c != null);
	}

	@isTest static void CCL_testValidRTs() {
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		CCL_DataLoadInterface__c CCL_int = [SELECT Id, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c LIMIT 1];
		CCL_int.CCL_Interface_Status__c = 'Active';
		update CCL_int;

		String CCL_mappingResponse = '{"totalSize":5,"done":true,"records":[{"attributes":{"type":"RecordType","url":"/services/data/v37.0/sobjects/RecordType/0120Y000000D1AlQAK"},"Id":"0120Y000000D1AlQAK","Name":"Delete Records"},{"attributes":{"type":"RecordType","url":"/services/data/v37.0/sobjects/RecordType/0120Y000000D1ApQAK"},"Id":"0120Y000000D1ApQAK","Name":"Deprecated Records"},{"attributes":{"type":"RecordType","url":"/services/data/v37.0/sobjects/RecordType/0120Y000000D1AmQAK"},"Id":"0120Y000000D1AmQAK","Name":"Insert Records"},{"attributes":{"type":"RecordType","url":"/services/data/v37.0/sobjects/RecordType/0120Y000000D1AnQAK"},"Id":"0120Y000000D1AnQAK","Name":"Update Records"},{"attributes":{"type":"RecordType","url":"/services/data/v37.0/sobjects/RecordType/0120Y000000D1AoQAK"},"Id":"0120Y000000D1AoQAK","Name":"Upsert Records"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_mappingResponse, CCL_responseHeaders));

		Test.startTest();

		CCL_BClsCalloutInterface CCL_batchInterface = new CCL_BClsCalloutInterface(CCL_export.Id, true);
		Database.executeBatch(CCL_batchInterface, 10);

		Test.stopTest();

		CCL_export = [SELECT Id, CCL_Status__c, CCL_Export_Log__c FROM CCL_CSVDataLoaderExport__c LIMIT 1];

		System.assertEquals('Failed', CCL_export.CCL_Status__c);
		System.assert(CCL_export.CCL_Export_Log__c != null);
	}

	@isTest static void CCL_testPendingJobs() {
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		CCL_DataLoadInterface__c CCL_int = [SELECT Id, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c LIMIT 1];
		CCL_int.CCL_Interface_Status__c = 'Active';
		update CCL_int;

		CCL_DataLoadInterface_DataLoadJob__c CCL_job = new CCL_DataLoadInterface_DataLoadJob__c(CCL_Data_Load_Interface__c = CCL_int.Id,
																								CCL_Name__c = 'Test', CCL_Job_Status__c = 'Active');
		insert CCL_job;

		CCL_DataLoadJob_DataLoadTask__c CCL_task = new CCL_DataLoadJob_DataLoadTask__c(CCL_Data_Load_Job__c = CCL_job.Id);
		insert CCL_task;

		String CCL_mappingResponse = 'The Interface has pending Jobs.';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_mappingResponse, CCL_responseHeaders));

		Test.startTest();

		CCL_BClsCalloutInterface CCL_batchInterface = new CCL_BClsCalloutInterface(CCL_export.Id, true);
		Database.executeBatch(CCL_batchInterface, 10);

		Test.stopTest();

		CCL_export = [SELECT Id, CCL_Status__c, CCL_Export_Log__c FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		List<CCL_Data_Loader_Export_Logs__c> CCL_logs = [SELECT Id FROM CCL_Data_Loader_Export_Logs__c];

		System.assertEquals('Failed', CCL_export.CCL_Status__c);
		System.assert(CCL_export.CCL_Export_Log__c != null);
		System.assertEquals(0, CCL_logs.size());
	}
}