global class CCL_BClsCalloutMappings implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    
    String CCL_query;
    String CCL_exportId;
    Boolean CCL_success;
    Integer CCL_numberOfRecords;
    String CCL_errorMessages;
    Integer CCL_successRecords;
    Map<String,String> CCL_mapErrorIds;
    Boolean CCL_isTestNotRunningOrgId;
    List<ID> interfaceIds;

    global CCL_BClsCalloutMappings(String CCL_exportRecordId, Boolean CCL_isNotRunningOrgId) {
        //Retrieve selected interfaces linked to this export              
        interfaceIds = new List<ID>();
        for (Data_Loader_Export_Interface__c i : [SELECT Data_Load_Interface__c FROM Data_Loader_Export_Interface__c WHERE Data_Loader_Export__c = :CCL_exportRecordId]){
            interfaceIds.add(i.Data_Load_Interface__c);
            System.debug('CCL_BClsCalloutMappings - ' + i.Data_Load_Interface__c  + ' added to selection');
        }         
        this.CCL_query = 'SELECT CCL_Column_Name__c, CCL_Data_Load_Interface__r.CCL_External_Id__c, CCL_Default_Value__c, CCL_Deploy_Overide__c, CCL_Default__c, ';
        this.CCL_query += 'CCL_Field_Type__c, CCL_isReferenceToExternalId__c, CCL_isUpsertId__c, CCL_ReferenceType__c, CCL_Required__c, CCL_SObject_Field__c, ';
        this.CCL_query += 'CCL_SObject_Mapped_Field__c, CCL_SObject_Name__c, CCL_SObjectExternalId__c, CCL_External_ID__c ';
        this.CCL_query += 'FROM CCL_DataLoadInterface_DataLoadMapping__c WHERE '; //CCL_Data_Load_Interface__r.CCL_External_Id__c != null AND ';
        //Filer on selected interfaces only
        this.CCL_query += 'CCL_Data_Load_Interface__c IN :interfaceIds';
        //this.CCL_query += 'CCL_Data_Load_Interface__r.RecordTypeId != null AND CCL_External_ID__c != null AND CCL_Data_Load_Interface__r.CCL_Interface_Status__c = \'Active\'';

        this.CCL_exportId = CCL_exportRecordId;
        this.CCL_success = true;
        this.CCL_numberOfRecords = 0;
        this.CCL_errorMessages = '';
        this.CCL_successRecords = 0;
        this.CCL_mapErrorIds = new Map<String,String>();
        if(CCL_isNotRunningOrgId == null) this.CCL_isTestNotRunningOrgId = false;
        else this.CCL_isTestNotRunningOrgId = CCL_isNotRunningOrgId;

        System.Debug('===> Query: ' + this.CCL_query);
        System.Debug('===> Export Id: ' + CCL_exportId);
    }
    
    global Database.QueryLocator start(Database.BatchableContext CCL_BC) {
        return Database.getQueryLocator(CCL_query);
    }

    global void execute(Database.BatchableContext CCL_BC, List<CCL_DataLoadInterface_DataLoadMapping__c> CCL_scope) {
        System.Debug('Matching Mapping Records: ' + CCL_scope.size());
        boolean validRun = false;
        Id targetOrgId;
        Id jobTargetId;
        String JSONString;
        Http http = new Http();

        //  Build named credential end point for Org'
        String myNamedCredential;        
        String myNamedPath;       
        String orgEndPoint;
        String orgQueryString;

        CCL_numberOfRecords += CCL_scope.size();

        //  Read the defined Org job target id
        System.debug('CCL_BClsCalloutMappings  during execute; ID is ' + CCL_exportId );
        List<CCL_CSVDataLoaderExport__c> exportReq = [SELECT Id,CCL_Status__c,CCL_Org_Target_Id__c,CCL_Source_Data__c,CCL_Named_Credential__c 
                                                      FROM CCL_CSVDataLoaderExport__c                                                 
                                                      WHERE Id = :CCL_exportId];         
        if(!exportReq.isEmpty()){
          validRun = true;         
          jobTargetId = exportReq[0].CCL_Org_Target_Id__c;
          if(Test.isRunningTest()) myNamedCredential = 'TestCredential';
          else myNamedCredential = exportReq[0].CCL_Named_Credential__c;
        } 

        System.debug('Valid run is : ' + validRun);

        if(validRun && !CCL_isTestNotRunningOrgId) {
            try {
                /* SECTION FOR TARGET ID VALIDATION */
                System.debug('*** SECTION FOR TARGET ID VALIDATION ***');

                //  Build named credential end point for Org'
                //myNamedCredential = 'CCL_DataLoaderNamedCredential';        
                myNamedPath       = '/services/data/v37.0/';
                orgQueryString    = 'query?q=SELECT+Id+from+Organization';         
                orgEndPoint       = 'callout:' + myNamedCredential + myNamedPath + orgQueryString;    
                
                //  Define named credential end point    
                HttpRequest req = new HttpRequest();
                req.setEndpoint(orgEndPoint);
                req.setMethod('GET');

                HTTPResponse res = http.send(req);        
                  
                //  Test if valid ('200') code response received from target Org
                if(res.getStatusCode() != 200)  {   
                    validRun = false;           
                    System.Debug('====> The status code returned wasn\'t expected: ' + res.getStatusCode());
                    CCL_errorMessages += '\r\n';
                    CCL_errorMessages += 'Could not make an outbound call. Please verify the chosen org and it\'s Named Credentials.';
                }
            
                //  Deserialize JSON Validate Org Id matches the defined target Org Id
                if(res.getStatusCode() == 200) {
                    System.debug('RES: ' + res.getBody());
                    JSONParser parser = JSON.createParser(res.getBody());

                    while (parser.nextToken() != null) {
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                            (parser.getText() == 'Id')) {
                            // Get the value.
                            parser.nextToken();
                            //System.debug('******parser ' + parser.getText());  
                            targetOrgId = parser.getText();
                        }
                    }

                    if(jobTargetId == targetOrgId) {
                        validRun = true;
                    } else {
                        System.Debug('====> !!!! Invalid Org Id!');
                        validRun = false;
                        CCL_errorMessages += '\r\nInvalid Org Id!';
                    }
                }
                
            } catch(Exception CCL_e) {
                CCL_errorMessages += '\r\n';
                CCL_errorMessages += 'An unexpected error has occured:\r\n';
                CCL_errorMessages += 'Message: ' + CCL_e.getMessage();
                CCL_errorMessages += '\r\nCause: ' + CCL_e.getCause();
                CCL_errorMessages += '\r\nStacktrace: ' + CCL_e.getStackTraceString();

                validRun = false;
                CCL_success = false;
            }       
        }

        if(Test.isRunningTest()) validRun = true;

        System.debug('Valid run is : ' + validRun);

        if(validRun) {
            try {
                Map<ID,CCL_DataLoadInterface_DataLoadMapping__c> mappingData = new Map<ID, CCL_DataLoadInterface_DataLoadMapping__c>();

                for(CCL_DataLoadInterface_DataLoadMapping__c CCL_m : CCL_scope) {
                    mappingData.put(CCL_m.Id, CCL_m);
                }

                System.debug('*** SECTION FOR MAPPING DATA ***');

                //  Build named credential end point for Org 
                //myNamedCredential = 'CCL_DataLoaderNamedCredential';       
                myNamedPath = '/services/apexrest/CCL_CSVDataLoadMapping/';
                orgEndPoint = 'callout:' + myNamedCredential + myNamedPath;    
                System.debug('** Mapping Data orgEndPoint string: ' + orgEndPoint);

                for(CCL_DataLoadInterface_DataLoadMapping__c curRec : mappingData.values()) {
                    system.debug('Dealing with Mapping: ' + curRec);
                    curRec.CCL_Deploy_Overide__c = true;
                    JSONString = '';
                    JSONString = CCL_JSONGenerator.CCL_generateJSONMapContent(curRec);
                    System.Debug('JSONString is: ' +JSONString);
                
                    orgEndPoint = 'callout:' + myNamedCredential + myNamedPath;
                
                    // POST interface data          
                    HttpRequest req3 = new HttpRequest();
                    req3.setEndpoint(orgEndPoint);
                    req3.setMethod('PUT');
                    req3.setHeader('Content-Type', 'application/json');
                    req3.setBody(JSONString);
                    HTTPResponse res3 = http.send(req3); 
                
                    System.debug('Status Code: ' + res3.getStatusCode());
                    System.Debug('PUT Response Mapping Data: ' + res3);
                    String CCL_resBody = res3.getBody();
                
                    // Test if valid ('200') code response received from target Org
                    if(res3.getStatusCode() != 200) {              
                        System.Debug('====> The status code returned wasn\'t expected: ' + res3.getStatusCode());                           
                        CCL_resBody = CCL_resBody.remove('"Upsert failed. First exception on row 0; first error: ');
                        CCL_resBody = CCL_resBody.remove(': []');
                        CCL_resBody = CCL_resBody.remove('"');
                        CCL_mapErrorIds.put(curRec.CCL_External_Id__c,CCL_resBody);
                        CCL_success = false;
                    } else {
                        if(CCL_resBody.contains('Success'))
                                CCL_successRecords += 1;
                        else {
                            CCL_resBody = CCL_resBody.remove('"Upsert failed. First exception on row 0; first error: ');
                            CCL_resBody = CCL_resBody.remove(': []');
                            CCL_resBody = CCL_resBody.remove('"');
                            System.Debug(CCL_resBody);
                            CCL_mapErrorIds.put(curRec.CCL_External_Id__c,CCL_resBody);
                            CCL_success = false;
                        }
                    }                                             
                }
            } catch(Exception CCL_e) {
                validRun = false;
                CCL_success = false;

                CCL_errorMessages += '\r\n';
                CCL_errorMessages += 'An unexpected error has occured:\r\n';
                CCL_errorMessages += 'Message: ' + CCL_e.getMessage();
                CCL_errorMessages += '\r\nCause: ' + CCL_e.getCause();
                CCL_errorMessages += '\r\nStacktrace: ' + CCL_e.getStackTraceString();
            }
        }
    }
    
    global void finish(Database.BatchableContext CCL_BC) {
        //  Read the defined Org job target id
        List<CCL_CSVDataLoaderExport__c> exportReq = [SELECT Id, CCL_Status__c, CCL_Org_Target_Id__c, CCL_Source_Data__c, CCL_Export_Log__c 
                                                      FROM CCL_CSVDataLoaderExport__c                                                 
                                                      WHERE Id = :CCL_exportId];

        if(CCL_success) {
            exportReq[0].CCL_Status__c = 'Completed';
            String CCL_msg;

            CCL_msg = 'Success!';

            exportReq[0].CCL_Mapping_Export_Log__c = CCL_msg;
            exportReq[0].CCL_Total_Number_of_Mappings__c = CCL_numberOfRecords;
            exportReq[0].CCL_Total_Mappings_Succeeded__c = CCL_successRecords;

            update exportReq;
        } else {
            if(CCL_successRecords > 0) {
                exportReq[0].CCL_Status__c = 'Partially Succeeded';
                exportReq[0].CCL_Mapping_Export_Log__c = 'The export was partially successful. Please find the below records which failed with their respective error message:\r\n';
                insert CCL_handlePartialSuccess(CCL_mapErrorIds);

                exportReq[0].CCL_Total_Number_of_Mappings__c = CCL_numberOfRecords;
                exportReq[0].CCL_Total_Mappings_Succeeded__c = CCL_successRecords;

                update exportReq[0];
            } else {
                exportReq[0].CCL_Status__c = 'Failed';
                exportReq[0].CCL_Mapping_Export_Log__c = CCL_errorMessages;

                if(!CCL_mapErrorIds.isEmpty()) {
                    insert CCL_handlePartialSuccess(CCL_mapErrorIds);

                    exportReq[0].CCL_Total_Number_of_Mappings__c = CCL_numberOfRecords;
                    exportReq[0].CCL_Total_Mappings_Succeeded__c = CCL_successRecords;
                }

                update exportReq;   
            }           
        }
    }

    private List<CCL_Data_Loader_Export_Logs__c> CCL_handlePartialSuccess(Map<String,String> CCL_mapRes) {
        List<CCL_Data_Loader_Export_Logs__c> CCL_return = new List<CCL_Data_Loader_Export_Logs__c>();

        String CCL_returnString = 'The export was partially successful. Please find the below records which failed with their respective error message:\r\n';

        for(String CCL_s : CCL_mapRes.keySet()) {
            CCL_Data_Loader_Export_Logs__c CCL_rec = new CCL_Data_Loader_Export_Logs__c(CCL_Message__c = CCL_mapRes.get(CCL_s), CCL_Type__c = 'Mapping', 
                CCL_Data_Loader_Export__c = CCL_exportId, CCL_Original_Record__c = CCL_s);
            CCL_return.add(CCL_rec);
        }

        return CCL_return;
    }
    
}