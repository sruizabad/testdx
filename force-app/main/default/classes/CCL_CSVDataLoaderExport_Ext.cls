public without sharing class CCL_CSVDataLoaderExport_Ext {
    private CCL_CSVDataLoaderExport__c CCL_curRec;
    public Boolean CCL_showDeployButton { 
        get{
            return (CCL_curRec != null && (CCL_curRec.CCL_Status__c == 'Pending')) ? true : false;
        } 
    }
    public Boolean CCL_showEditButton { 
        get{
            return (CCL_curRec != null && (CCL_curRec.CCL_Status__c == 'Pending' || CCL_curRec.CCL_Status__c == 'Draft')) ? true : false;
        } 
    }
    public String warningMsg {get; set;}
       
    // Constructor
    public CCL_CSVDataLoaderExport_Ext(ApexPages.StandardController controller) {
        this.CCL_curRec = (CCL_CSVDataLoaderExport__c) controller.getRecord();
        System.Debug('CCL_LOG: Current Record = ' + CCL_curRec);         
        sanityCheck();              
    }
    
    // Custom Copy button
    public PageReference copy() {        
        try{
            system.debug('CCL_LOG: Start copying Export Details');
            CCL_CSVDataLoaderExport__c copy = new CCL_CSVDataLoaderExport__c();
            copy.CCL_Status__c = 'Draft';
            copy.CCL_Export_Log__c = '' ;
            copy.CCL_Mapping_Export_Log__c = '' ;
            copy.CCL_Org_Connection__c = CCL_curRec.CCL_Org_Connection__c ;
            insert copy;
            System.debug('CCL_LOG: Start Copying related interfaces');
            List<Data_Loader_Export_Interface__c> dleis = new List<Data_Loader_Export_Interface__c>();
            for (Data_Loader_Export_Interface__c dlei : [SELECT Data_Loader_Export__c, Data_Load_Interface__c FROM Data_Loader_Export_Interface__c  Where Data_Loader_Export__c = :CCL_curRec.id]){
                dleis.add(new Data_Loader_Export_Interface__c(Data_Loader_Export__c = copy.id, Data_Load_Interface__c  = dlei.Data_Load_Interface__c  ));
            }
            insert dleis;
            PageReference pageRef = new PageReference('/' + copy.id);
            return pageRef;
        } catch(Exception e) {
            return null;
        }
    }
        
    // Action Deploy button
    public PageReference deploy() {        
        String deploy_result = CCL_InterfaceIntegrationController.ws_export(ApexPages.currentPage().getParameters().get('id'),false);        
        PageReference CCL_ref = new PageReference('/' + ApexPages.currentPage().getParameters().get('id')); 
        CCL_ref.setRedirect(true);
        return CCL_ref;        
    }
    
    
    private void sanityCheck(){
        if (CCL_curRec.Number_of_Selected_Interfaces__c == 0) {
            warningMsg = 'No interfaces selected; Please select at least 1 interface';
        } else {
            List<Id> interfaceIds = new List<Id>();
            List<Id> mappingIds = new List<Id>();  
            Boolean invalidMapping = false;          
            For (Data_Loader_Export_Interface__c exportInterface : [SELECT ID, Data_Load_Interface__c FROM Data_Loader_Export_Interface__c WHERE Data_Loader_Export__c = :ApexPages.currentPage().getParameters().get('id')]){
                interfaceIds.add(exportInterface.Data_Load_Interface__c);
            }
            List<CCL_DataLoadInterface_DataLoadMapping__c> validmappings = database.query('SELECT ID FROM CCL_DataLoadInterface_DataLoadMapping__c WHERE CCL_Data_Load_Interface__c in :interfaceIds AND CCL_External_ID__c != \'\'');
            List<CCL_DataLoadInterface_DataLoadMapping__c> invalidmappings = database.query('SELECT ID FROM CCL_DataLoadInterface_DataLoadMapping__c WHERE CCL_Data_Load_Interface__c in :interfaceIds AND CCL_External_ID__c = \'\'');
            if (validmappings == null || validmappings.size() == 0){
                warningMsg =  'Selected interface(s) do not contain any valid mappings.';    
            } else if (invalidmappings  != null && invalidmappings.size() != 0) {
                warningMsg = invalidmappings.size() + ' mappings found without a valid external id';
            } 
                    
        }
    }
}