/***************************************************************************************************************************
Apex Class Name :	CCL_CSVDataLoaderExport_ExtTest
Version : 			1.0.0
Created Date : 		22/11/2016
Function : 			Test class for CCL_CSVDataLoaderExport_Ext
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Steve van Noort					  	22/11/2016      		        		Initial creation
* Wouter Jacops							05/03/2018								Updated Testclass to cover 'recent' changes
***************************************************************************************************************************/


@isTest
private class CCL_CSVDataLoaderExport_ExtTest {
	@testSetup static void CCL_createTestData() {
		Id CCL_RTInterfaceInsert = [SELECT Id FROM Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records'].Id;

        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'userterritory',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert',
																	CCL_External_Id__c = 'EXTTEST1', RecordtypeId = CCL_RTInterfaceInsert);
        insert CCL_interfaceRecord;
		System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_User = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'userid',
																	CCL_Column_Name__c = 'User ID', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'userid', CCL_SObject_Name__c = 'userterritory', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id,
																	CCL_External_Id__c = 'EXTMAP1');
		insert CCL_mappingRecord_User;
		System.Debug('===CCL_mappingRecord_User: ' + CCL_mappingRecord_User.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Terr = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'territoryid',
																	CCL_Column_Name__c = 'Territory ID', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'territoryid', CCL_SObject_Name__c = 'userterritory', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = true);
		insert CCL_mappingRecord_Terr;
		System.Debug('===CCL_mappingRecord_Terr: ' + CCL_mappingRecord_Terr.Id);
        
        CCL_DataLoadInterface__c CCL_interfaceRecordAccount = new CCL_DataLoadInterface__c(CCL_Name__c = 'Account', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'Account',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert',
																	CCL_External_Id__c = 'EXTACCOUNT', RecordtypeId = CCL_RTInterfaceInsert);
        insert CCL_interfaceRecordAccount;
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Account = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'name',
																	CCL_Column_Name__c = null, CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'name', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecordAccount.Id,
																	CCL_External_Id__c = null);
		insert CCL_mappingRecord_Account;
		

		CCL_Org_Connection__c CCL_orgCon = new CCL_Org_Connection__c(Name = 'Test', CCL_Named_Credential__c = 'TestCredential', CCL_Org_Active__c = true, 
																	CCL_Org_Id__c = UserInfo.getOrganizationId(), CCL_Org_Type__c = 'Developer Edition');

		insert CCL_orgCon;

		CCL_CSVDataLoaderExport__c CCL_export = new CCL_CSVDataLoaderExport__c(CCL_Org_Connection__c = CCL_orgCon.Id, CCL_Source_Data__c = 'Interfaces',
																				CCL_Status__c = 'Pending');
		insert CCL_export;
            
        CCL_interfaceRecord.CCL_Interface_Status__c = 'Active';
        update CCL_interfaceRecord;
        
        Data_Loader_Export_Interface__c CCL_expInterface1 = new Data_Loader_Export_Interface__c(Data_Load_Interface__c = CCL_interfaceRecord.Id, Data_Loader_Export__c = CCL_export.Id);
        Data_Loader_Export_Interface__c CCL_expInterface2 = new Data_Loader_Export_Interface__c(Data_Load_Interface__c = CCL_interfaceRecord.Id, Data_Loader_Export__c = CCL_export.Id);
        
        insert CCL_expInterface1;
        insert CCL_expInterface2;
	}
	
	@isTest static void CCL_testExt() {
		Test.startTest();
		
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id, OwnerId, IsDeleted, Name, Number_of_Selected_Interfaces__c, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, 
												CCL_Source_Data__c, CCL_Status__c, CCL_Org_Connection__c, CCL_Org_Target_Id__c, CCL_Export_Log__c, 
												CCL_Total_Number_of_Records__c, CCL_Total_Records_Succeeded__c, CCL_Failed_records__c, CCL_Image_Status__c, 
												CCL_Named_Credential__c, SystemModstamp FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		
		CCL_export.CCL_Status__c = 'Completed';
		update CCL_export;
        
		PageReference CCL_pageRef = Page.CCL_CSVDataLoaderExportPage;
		ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_export);
		CCL_CSVDataLoaderExport_Ext CCL_ext = new CCL_CSVDataLoaderExport_Ext(CCL_stdCon);
		
		Test.setCurrentPage(CCL_pageRef);
		System.currentPageReference().getParameters().put('id', CCL_export.Id);

		String CCL_orgIdResponse = '{"totalSize":1,"done":true,"records":[{"attributes":{"type":"Organization","url":"/services/data/v37.0/sobjects/Organization/'+ UserInfo.getOrganizationId() + '"},"Id":"' + UserInfo.getOrganizationId() + '"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_orgIdResponse, CCL_responseHeaders));

		CCL_ext.deploy();
        Boolean b = CCL_ext.CCL_showDeployButton;
        Boolean c = CCL_ext.CCL_showEditButton;
		
		Test.stopTest();

		CCL_CSVDataLoaderExport__c CCL_assertExport = [SELECT Id, CCL_Status__c FROm CCL_CSVDataLoaderExport__c LIMIT 1];
		System.assertEquals('Completed', CCL_assertExport.CCL_Status__c);
	}
    
    @isTest static void CCL_testExt2() {
		Test.startTest();
		
		CCL_CSVDataLoaderExport__c CCL_export = new CCL_CSVDataLoaderExport__c(CCL_Source_Data__c = 'Interfaces', CCL_Status__c = 'Pending');
		
        Data_Loader_Export_Interface__c CCL_expInterface1 = [SELECT Id, Data_Load_Interface__c, Data_Loader_Export__c FROM Data_Loader_Export_Interface__c LIMIT 1];
        delete CCL_expInterface1;
        
		PageReference CCL_pageRef = Page.CCL_CSVDataLoaderExportPage;
		ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_export);
		CCL_CSVDataLoaderExport_Ext CCL_ext = new CCL_CSVDataLoaderExport_Ext(CCL_stdCon);
		
		Test.setCurrentPage(CCL_pageRef);
		System.currentPageReference().getParameters().put('id', CCL_export.Id);

        CCL_ext.copy();
		CCL_ext.deploy();
		Test.stopTest();

		CCL_CSVDataLoaderExport__c CCL_assertExport = [SELECT Id, CCL_Status__c FROm CCL_CSVDataLoaderExport__c LIMIT 1];
		System.assertEquals('Pending', CCL_assertExport.CCL_Status__c);
	}
    
    @isTest static void CCL_testExt3() {
		Test.startTest();
		
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id, OwnerId, IsDeleted, Name, Number_of_Selected_Interfaces__c, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, 
												CCL_Source_Data__c, CCL_Status__c, CCL_Org_Connection__c, CCL_Org_Target_Id__c, CCL_Export_Log__c, 
												CCL_Total_Number_of_Records__c, CCL_Total_Records_Succeeded__c, CCL_Failed_records__c, CCL_Image_Status__c, 
												CCL_Named_Credential__c, SystemModstamp FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		
		CCL_export.CCL_Status__c = 'Completed';
		update CCL_export;
        
		PageReference CCL_pageRef = Page.CCL_CSVDataLoaderExportPage;
		ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_export);
		CCL_CSVDataLoaderExport_Ext CCL_ext = new CCL_CSVDataLoaderExport_Ext(CCL_stdCon);
		
		Test.setCurrentPage(CCL_pageRef);
		System.currentPageReference().getParameters().put('id', CCL_export.Id);

		String CCL_orgIdResponse = '{"totalSize":1,"done":true,"records":[{"attributes":{"type":"Organization","url":"/services/data/v37.0/sobjects/Organization/'+ UserInfo.getOrganizationId() + '"},"Id":"' + UserInfo.getOrganizationId() + '"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_orgIdResponse, CCL_responseHeaders));

		CCL_ext.copy();
		
		Test.stopTest();

		CCL_CSVDataLoaderExport__c CCL_assertExport = [SELECT Id, CCL_Status__c FROm CCL_CSVDataLoaderExport__c LIMIT 1];
		System.assertEquals('Completed', CCL_assertExport.CCL_Status__c);
	}
    
    @isTest static void CCL_testExt4() {
        CCL_Org_Connection__c CCL_orgCon = [SELECT Id FROM CCL_Org_Connection__c LIMIT 1];
        CCL_DataLoadInterface__c CCL_intAcc = [SELECT Id FROM CCL_DataLoadInterface__c WHERE CCL_External_Id__c = 'EXTACCOUNT' LIMIT 1];
        CCL_CSVDataLoaderExport__c CCL_export = new CCL_CSVDataLoaderExport__c(CCL_Org_Connection__c = CCL_orgCon.Id, CCL_Source_Data__c = 'Interfaces',
																				CCL_Status__c = 'Pending');
		insert CCL_export;
            
        CCL_intAcc.CCL_Interface_Status__c = 'Active';
        update CCL_intAcc;
        
        Data_Loader_Export_Interface__c CCL_expInterface1 = new Data_Loader_Export_Interface__c(Data_Load_Interface__c = CCL_intAcc.Id, Data_Loader_Export__c = CCL_export.Id);
        
        insert CCL_expInterface1;
        
        
		Test.startTest();
		
		CCL_export.CCL_Status__c = 'Completed';
		update CCL_export;
        
		PageReference CCL_pageRef = Page.CCL_CSVDataLoaderExportPage;
		ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_export);
		CCL_CSVDataLoaderExport_Ext CCL_ext = new CCL_CSVDataLoaderExport_Ext(CCL_stdCon);
		
		Test.setCurrentPage(CCL_pageRef);
		System.currentPageReference().getParameters().put('id', CCL_export.Id);

		String CCL_orgIdResponse = '{"totalSize":1,"done":true,"records":[{"attributes":{"type":"Organization","url":"/services/data/v37.0/sobjects/Organization/'+ UserInfo.getOrganizationId() + '"},"Id":"' + UserInfo.getOrganizationId() + '"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_orgIdResponse, CCL_responseHeaders));

		CCL_ext.deploy();
        Boolean b = CCL_ext.CCL_showDeployButton;
        Boolean c = CCL_ext.CCL_showEditButton;
		
		Test.stopTest();

		CCL_CSVDataLoaderExport__c CCL_assertExport = [SELECT Id, CCL_Status__c FROm CCL_CSVDataLoaderExport__c LIMIT 1];
		System.assertEquals('Pending', CCL_assertExport.CCL_Status__c);
	}
}