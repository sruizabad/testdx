/***************************************************************************************************************************
Apex Class Name :	CCL_CSVIterator
Version : 			1.3
Created Date : 		04/02/2015
Function : 			Custom Iterator class for the CSV values. Which will be used in CCL_batchCSVReader class. A large string
					of data will be coming in and needs to be chopped and interated. 
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Robin Wijnen								04/02/2015								Initial Creation Iterator class
***************************************************************************************************************************/

global without sharing class CCL_CSVIterator implements Iterator<string>, Iterable<string> {
	private String CCL_CSVData;
	private String CCL_introValue;
    
	public CCL_CSVIterator(String fileData, String introValue) {
		CCL_CSVData = fileData;
		CCL_introValue = introValue;
	}
    
	global Boolean hasNext() {
		return CCL_CSVData.length() > 1 ? true : false;        
	}
    
   global String next() {  
       String row = CCL_CSVData.subString(0, CCL_CSVData.indexOf(CCL_introValue));
	CCL_CSVData = CCL_CSVData.subString(CCL_CSVData.indexOf(CCL_introValue) + CCL_introValue.length(),CCL_CSVData.length());
	
    	return row;
   }
    
   global Iterator<string> Iterator() {
       return this;   
   }
}