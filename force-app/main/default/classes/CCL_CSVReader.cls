/***************************************************************************************************************************
Apex Class Name :	CCL_CSVReader
Version : 			1.0
Created Date : 		1/02/2015
Function : 			This class will parse the CSV file and return a list containing columns and rows of the csv file
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois							  	1/02/2015								Creation of class
* Joris Artois								15/10/2015								Update Bug Fix to allow fields with a comma in the text to be inserted
																					"Error with empty currency or number columns"
* Robin Wijnen								20/04/2016								Update Bug Fix towards Text Qualifier
***************************************************************************************************************************/

public without sharing class CCL_CSVReader {	
	/**
	*	This method will read the CSV file and return a List<List<String>> containing separate rows and columns of the file
	**/
	public static List<List<String>> CCL_readCSVFile(String CCL_csvString, String CCL_delimiter, String CCL_textQualifier, String CCL_lineSeparator) {        
        CCL_csvValues CCL_csvField = 		new CCL_csvValues();
        List<list<String>> CCL_csvValue = 	new List<List<String>>();
        List<String> CCL_csvRows = 			new List<String>();
        Boolean eod = false;  // Whether end of CSV data is reached        
        
        while (!eod) {			
			// Get all the details about each field of the csv file from the method below
            CCL_csvField = CCL_readCSVValue(CCL_csvString, CCL_lineSeparator, CCL_delimiter, CCL_textQualifier);
            CCL_csvRows.add(CCL_csvField.CCL_field);
            
            if (CCL_csvField.CCL_delimiter == CCL_lineSeparator) {
              CCL_csvValue.add(CCL_csvRows);
              
              if (CCL_csvValue.size() > 0) {
                //System.assertEquals(CCL_csvValue.get(0).size(), CCL_csvRows.size());
              }
              CCL_csvRows = new List<String>();
            }
            
            if (CCL_csvField.CCL_biteSize() == CCL_csvString.length()) {
              eod = true;
            }
            else {
              CCL_csvString = CCL_csvString.substring(CCL_csvField.CCL_biteSize());
            }
        }
        
        return CCL_csvValue;
    }
    
    
    /**
    *	This method reads the CSV file and returns a object of CCL_csvValues containing all the informations about
    *	each field in the csv file. It handles value per value.
    **/
	public static CCL_csvValues CCL_readCSVValue(String CCL_csvString, String CCL_lineSeparator, String CCL_delimiter, String CCL_textQualifier) {
        System.assert(CCL_csvString.endsWith(CCL_lineSeparator));
     
        CCL_csvValues CCL_csvField = new CCL_csvValues();
        CCL_csvField.CCL_textQualifier = CCL_textQualifier;

		// First check if the value is enclosed with a text qualifier
        if (CCL_csvString.startsWith(CCL_textQualifier)) {
          	CCL_csvField.CCL_enclosed = true;
          
			Integer searchIndex = 0;      // starting index to search
            Integer dquoteIndex = -1;     // index of DQUOTE
            Integer dquotesIndex = -1;    // index of DQUOTEDQUOTE
                            
            Boolean closerFound = false;
            
            /**
            * Author:		Robin Wijnen <robin.wijnen@c-clearpartners.com>
            * Date:			20/04/2016
            * Description:	Bug fix towards Text Qualifier
            */
            while (!closerFound) {
                dquoteIndex = CCL_csvString.indexOf(CCL_textQualifier, searchIndex);                
                dquotesIndex = CCL_csvString.indexOf(CCL_textQualifier,searchIndex+1);
                
                System.assert(dquoteIndex != -1);
                
                if (dquoteIndex == dquotesIndex) {
                    searchIndex = dquotesIndex + CCL_textQualifier.length();
                }
                else {
                	// Check if the value of the CSV cell isn't the same as the text qualifier
                	if((dquoteIndex + 1) == dquotesIndex) {
                		Integer nextDelimiter = CCL_csvString.indexOf(CCL_delimiter, searchIndex);
	        			dquotesIndex = dquoteIndex + nextDelimiter - CCL_textQualifier.length();
	                	String tmpStr = CCL_csvString.substring(CCL_textQualifier.length(), dquotesIndex);
	        
	                	if(tmpStr == CCL_textQualifier) {
	                		closerFound = true;
	                	}
                	}
                	
                    closerFound = true;
                }
            }
            
            CCL_csvField.CCL_field = CCL_csvString.substring(CCL_textQualifier.length(), dquotesIndex);
            
            /**
            * Author: Joris Artois
            * Date: 16/10/2015
            * Description: Bug fix changing dquoteIndex to dquotesIndex to only start checking after the second text qualifier
            **/
            Integer commaIndex = CCL_csvString.indexOf(CCL_delimiter, dquotesIndex);
            Integer crlfIndex = CCL_csvString.indexOf(CCL_lineSeparator, dquotesIndex);
            
            if (commaIndex != -1 && commaIndex < crlfIndex) {
                CCL_csvField.CCL_delimiter = CCL_delimiter;
            }
            else {
                CCL_csvField.CCL_delimiter = CCL_lineSeparator;
            }
        }
        else {
          CCL_csvField.CCL_enclosed = false;
          
            Integer commaIndex = CCL_csvString.indexOf(CCL_delimiter);
            Integer crlfIndex = CCL_csvString.indexOf(CCL_lineSeparator);

            if (commaIndex != -1 && commaIndex < crlfIndex) {
                CCL_csvField.CCL_field = CCL_csvString.substring(0, commaIndex);
                CCL_csvField.CCL_delimiter = CCL_delimiter;
            }
            else {
                CCL_csvField.CCL_field = CCL_csvString.substring(0, crlfIndex);
                CCL_csvField.CCL_delimiter = CCL_lineSeparator;
            }
        }
        
        System.debug('===Returning CCL_csvField:: ' + CCL_csvField);
        
        return CCL_csvField;
	}
}