/***************************************************************************************************************************
Apex Class Name :	CCL_ClsTestUtility
Version : 			1.0.0
Created Date : 		22/11/2016
Function : 			Test Utility Class
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Steve van Noort					  	22/11/2016      		        		Initial creation
* Robin Wijnen                          13/04/2018                              Added randomizer
***************************************************************************************************************************/

@isTest
public class CCL_ClsTestUtility {
    public static String CCL_generateRandomString(Integer CCL_length) {
        final String CCL_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String CCL_randStr = '';

        while (CCL_randStr.length() < CCL_length) {
            Integer CCL_idx = Math.mod(Math.abs(Crypto.getRandomInteger()), CCL_chars.length());
            CCL_randStr += CCL_chars.substring(CCL_idx, CCL_idx+1);
        }

        return CCL_randStr; 
    }
}