/***************************************************************************************************************************
Apex Class Name :   CCL_DataLoadExport_newEditPage_Ext
Version :       1.0
Created Date :     16/06/2017
Function :      Extension of Standard controller on sObject CCL_DateLoadExport__c. The extension will be used on VisualForce
                page CCL_DataLoadExport_NewPage 

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                                Date                                Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Tim Calje                              16/06/2017                          Initial Creation   
* Wouter Jacops                          28/03/2018                          Added Validated field and sorting
* Wouter Jacops							 09/08/2018							 Added Number(Name) field and sorting
***************************************************************************************************************************/
Public With Sharing class CCL_DataLoadExport_newEditPage_Ext{

 CCL_CSVDataLoaderExport__c CCL_exportRecord;
 public Map<Id,ExportInterfaceWrapper> InterfaceList {get; set;}
 public Boolean toggler {get; set;}
 public String sortOrder {get; set;}

 public CCL_DataLoadExport_newEditPage_Ext(ApexPages.StandardController CCL_stdCon) {
    if (CCL_stdCon.getRecord() != null){
        this.CCL_exportRecord = (CCL_CSVDataLoaderExport__c) CCL_stdCon.getRecord();
    } else {
        this.CCL_exportRecord = New CCL_CSVDataLoaderExport__c();
    }
    init();
  }
  
  private void init(){
      this.sortOrder = 'CCL_Name__c';
      this.InterfaceList = buildInterfaceMap();
      this.toggler = False;
  }
  
  private Map<Id,ExportInterfaceWrapper> buildInterfaceMap(){
    InterfaceList = new map<Id,ExportInterfaceWrapper>(); 
    List<CCL_DataLoadInterface__c> activeInterfaces = New List<CCL_DataLoadInterface__c>();
    List<Data_Loader_Export_Interface__c> selectedInterfaces = New List<Data_Loader_Export_Interface__c>();
    activeInterfaces = Database.query('SELECT ID, CCL_External_ID__c,Name, CCL_Validated__c, CCL_Name__c, CCL_Selected_Object_Name__c, CCL_Type_of_Upload__c, CCL_Description__c' +
                                     ' FROM CCL_DataLoadInterface__c' +
                                     ' WHERE CCL_Interface_Status__c = \'Active\' AND CCL_External_ID__c != \'\'' +                                    
                                     ' ORDER BY CCL_Name__c ASC');
    if (this.CCL_exportRecord != null && this.CCL_exportRecord.id != null)                      
        selectedInterfaces = [SELECT ID, Data_Load_Interface__r.id FROM Data_Loader_Export_Interface__c WHERE Data_Loader_Export__c = :this.CCL_exportRecord.id];                      
    for (CCL_DataLoadInterface__c activeInterface : activeInterfaces ){
        ExportInterfaceWrapper wrappedInterface = new ExportInterfaceWrapper(this,activeInterface );
        InterfaceList.put(activeInterface.id,wrappedInterface );
    }  
    for (Data_Loader_Export_Interface__c selectedInterface : selectedInterfaces){
        if (InterfaceList.get(selectedInterface.Data_Load_Interface__r.id) != null)
            InterfaceList.get(selectedInterface.Data_Load_Interface__r.id).wSelected = True;
    }
    return InterfaceList;
  }
   
  public List<ExportInterfaceWrapper> getInterfaces(){
      List<ExportInterfaceWrapper> myInterfaces;
      myInterfaces = InterfaceList.values();
      myInterfaces.sort();
      return myInterfaces;
  }
   
  Public PageReference toggle(){
      for (ID wrappedInterfaceId : InterfaceList.keyset()){
          InterfaceList.get(wrappedInterfaceId).wSelected = toggler;
      }
      return null;
  } 
  
  public void sortByNumber() {
      this.sortOrder = 'Name';   
  }
  
  public void sortByType() {
      this.sortOrder = 'CCL_Type_of_Upload__c';   
  }
    
  public void sortByObject() {
      this.sortOrder = 'CCL_Selected_Object_Name__c';
  }
  
    public void sortByExternalId() {
      this.sortOrder = 'CCL_External_ID__c';
  }
    
  public void sortByName() {
      this.sortOrder = 'CCL_Name__c';
  }
    
  public void sortByValidated() {
      this.sortOrder = 'CCL_Validated__c';
  }
   
  Public PageReference save(){
   try{
       upsert CCL_exportRecord;
       List<Data_Loader_Export_Interface__c> newInterfaces = new list<Data_Loader_Export_Interface__c>();
       List<Data_Loader_Export_Interface__c> oldInterfaces = new list<Data_Loader_Export_Interface__c>();
       for (Data_Loader_Export_Interface__c  oldInterface : [SELECT ID FROM Data_Loader_Export_Interface__c Where Data_Loader_Export__c = :CCL_exportRecord.id]){
           oldInterfaces.add(oldInterface);
       }       
       for (ID wrappedInterfaceId : InterfaceList.keyset()){
           if (interFaceList.get(wrappedInterfaceId).wSelected) {
               Data_Loader_Export_Interface__c newInterface =  new Data_Loader_Export_Interface__c();
               newInterface.Data_Load_Interface__c = interFaceList.get(wrappedInterfaceId).wInterface.id;
               newInterface.Data_Loader_Export__c = CCL_exportRecord.id;
               newInterfaces.add(newInterface);
            }
       }
       //Replace previous interfaces with new selection
       delete oldInterfaces;
       insert newInterfaces;
       Return new Pagereference('/apex/CCL_CSVDataLoaderExportPage?id=' + CCL_exportRecord.id + '&sfdc.override=1');     
   } catch (Exception e){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,e.getMessage()));
   }
   return null;
  }
  
  public Class ExportInterfaceWrapper implements comparable{  
      Public CCL_DataLoadInterface__c wInterface {get; set;}
      Public Boolean wSelected {get; set; }
      CCL_DataLoadExport_newEditPage_Ext wParentRef;      
      
      Public ExportInterfaceWrapper(CCL_DataLoadExport_newEditPage_Ext pParentRef, CCL_DataLoadInterface__c pInterface){
           wInterface = pInterface;
           wSelected = false;   
           wParentRef = pParentref;
      }  
      
     public Integer compareTo(Object compareTo) {
        try {
            ExportInterfaceWrapper compareToEmp = (ExportInterfaceWrapper)compareTo;
            if (wParentRef.sortOrder == 'CCL_Selected_Object_Name__c'){
                if (wInterface.CCL_Selected_Object_Name__c == compareToEmp.wInterface.CCL_Selected_Object_Name__c) return 0;
                if (wInterface.CCL_Selected_Object_Name__c > compareToEmp.wInterface.CCL_Selected_Object_Name__c) return 1;
            } else if (wParentRef.sortOrder == 'Name'){
                if (wInterface.Name == compareToEmp.wInterface.Name) return 0;
                if (wInterface.Name> compareToEmp.wInterface.Name) return 1;
            } else if (wParentRef.sortOrder == 'CCL_Name__c'){
                if (wInterface.CCL_Name__c == compareToEmp.wInterface.CCL_Name__c) return 0;
                if (wInterface.CCL_Name__c > compareToEmp.wInterface.CCL_Name__c) return 1;
            } else if (wParentRef.sortOrder == 'CCL_Type_of_Upload__c'){
                if (wInterface.CCL_Type_of_Upload__c== compareToEmp.wInterface.CCL_Type_of_Upload__c) return 0;
                if (wInterface.CCL_Type_of_Upload__c> compareToEmp.wInterface.CCL_Type_of_Upload__c) return 1;
            } else if (wParentRef.sortOrder == 'CCL_External_ID__c'){
                if (wInterface.CCL_External_ID__c== compareToEmp.wInterface.CCL_External_ID__c) return 0;
                if (wInterface.CCL_External_ID__c> compareToEmp.wInterface.CCL_External_ID__c) return 1;
            } else if (wParentRef.sortOrder == 'CCL_Validated__c'){
                if (wInterface.CCL_Validated__c== compareToEmp.wInterface.CCL_Validated__c) return 0;
                else return 1;
            } 
            return -1;                 
        } catch (Exception e){
            System.debug('Exception during Sort: ' + e.getmessage());
            return -1;
        }     
     }
      
  }
  

}