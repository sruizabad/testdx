/***************************************************************************************************************************
Apex Class Name :	CCL_DataLoadExport_newEditPage_ExtTest
Version : 			1.0.0
Created Date : 		05/03/2018
Function : 			Test class for CCL_DataLoadExport_newEditPage_Ext
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops							  	05/03/2018      		        		Initial creation
* Wouter Jacops								28/03/2018								Added sort on Validated field
* Wouter Jacops								09/08/2018								Added sort on Interface number (Name)
***************************************************************************************************************************/

@isTest
private class CCL_DataLoadExport_newEditPage_ExtTest {
	@testSetup static void CCL_createTestData() {
		
        CCL_Org_Connection__c CCL_orgCon = new CCL_Org_Connection__c(Name = 'Test', CCL_Named_Credential__c = 'TestCredential', CCL_Org_Active__c = true, 
																	CCL_Org_Id__c = UserInfo.getOrganizationId(), CCL_Org_Type__c = 'Developer Edition');
		insert CCL_orgCon;

		CCL_CSVDataLoaderExport__c CCL_export = new CCL_CSVDataLoaderExport__c(CCL_Org_Connection__c = CCL_orgCon.Id, CCL_Source_Data__c = 'Interfaces',
																				CCL_Status__c = 'Pending');
		insert CCL_export;
        
        Id CCL_RTInterfaceInsert = [SELECT Id FROM Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records'].Id;
        
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'Active', CCL_Selected_Object_Name__c = 'userterritory',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert',
																	CCL_External_Id__c = 'EXTTEST1', RecordtypeId = CCL_RTInterfaceInsert);
        insert CCL_interfaceRecord;
        
        CCL_DataLoadInterface__c CCL_interfaceRecord2 = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test1', CCL_Batch_Size__c = 20, CCL_Delimiter__c = ';',
																	CCL_Interface_Status__c = 'Active', CCL_Selected_Object_Name__c = 'account',
																	CCL_Text_Qualifier__c = ':', CCL_Type_Of_Upload__c = 'Update',
																	CCL_External_Id__c = 'EXTTEST2', RecordtypeId = CCL_RTInterfaceInsert);
        insert CCL_interfaceRecord2;
        
        
        Data_Loader_Export_Interface__c CCL_exportInterfaceRecord = new Data_Loader_Export_Interface__c(Data_Loader_Export__c = CCL_export.Id, Data_Load_Interface__c = CCL_interfaceRecord.Id);
        
        insert CCL_exportInterfaceRecord;
        
        Data_Loader_Export_Interface__c CCL_exportInterfaceRecord2 = new Data_Loader_Export_Interface__c(Data_Loader_Export__c = CCL_export.Id, Data_Load_Interface__c = CCL_interfaceRecord2.Id);

        insert CCL_exportInterfaceRecord2;
	}
	@isTest static void CCL_testExt() {

        Test.startTest();
		
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id, OwnerId, IsDeleted, Name, Number_of_Selected_Interfaces__c, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, 
												CCL_Source_Data__c, CCL_Status__c, CCL_Org_Connection__c, CCL_Org_Target_Id__c, CCL_Export_Log__c, 
												CCL_Total_Number_of_Records__c, CCL_Total_Records_Succeeded__c, CCL_Failed_records__c, CCL_Image_Status__c, 
												CCL_Named_Credential__c, SystemModstamp FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		
		CCL_DataLoadInterface__c CCL_interfaceRecord = [SELECT Id, CCL_Name__c, CCL_Batch_Size__c, CCL_Delimiter__c, CCL_Interface_Status__c, CCL_Selected_Object_Name__c, 
                                                       CCL_Text_Qualifier__c, CCL_Type_Of_Upload__c, CCL_External_Id__c FROM CCL_DataLoadInterface__c WHERE CCL_External_Id__c = 'EXTTEST1']; 
        CCL_DataLoadInterface__c CCL_interfaceRecord2 = [SELECT Id, CCL_Name__c, CCL_Batch_Size__c, CCL_Delimiter__c, CCL_Interface_Status__c, CCL_Selected_Object_Name__c, 
                                                       CCL_Text_Qualifier__c, CCL_Type_Of_Upload__c, CCL_External_Id__c FROM CCL_DataLoadInterface__c WHERE CCL_External_Id__c = 'EXTTEST2']; 
        
		PageReference CCL_pageRef = Page.CCL_DataLoadExport_NewPage;
		ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_export);
		CCL_DataLoadExport_newEditPage_Ext CCL_ext = new CCL_DataLoadExport_newEditPage_Ext(CCL_stdCon);
        		
		Test.setCurrentPage(CCL_pageRef);

        List<CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper> listexpintwrap = new List <CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper>();
        
        CCL_ext.toggler = true;
		CCL_ext.getInterfaces();
        CCL_ext.toggle();
        CCL_ext.save();
        
		CCL_ext.sortByType();        
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord2));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.sort();
        
        CCL_ext.sortByObject();
        listexpintwrap.clear();
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord2));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.sort();
        
        CCL_ext.sortByExternalId();
        listexpintwrap.clear();
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord2));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.sort();
        
        CCL_ext.sortByName();
        listexpintwrap.clear();
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord2));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.sort();
        
        CCL_ext.sortByValidated();
        listexpintwrap.clear();
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord2));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.sort();
        
        CCL_ext.sortByNumber();
        listexpintwrap.clear();
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord2));
        listexpintwrap.add(new CCL_DataLoadExport_newEditPage_Ext.ExportInterfaceWrapper(CCL_ext, CCL_interfaceRecord));
        listexpintwrap.sort();
        
		Test.stopTest();	
	}
    
}