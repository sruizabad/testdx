/***************************************************************************************************************************
Apex Class Name :	CCL_DataLoadInterface_cloneButton_Ext
Version : 			1.0
Created Date : 		24/02/2015
Function :  		Apex class controller behind the clone button. Class is used to clone the 'Interface' sObject and its related Mappings.

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois						  		24/02/2015      		        			Initial Class Creation       
***************************************************************************************************************************/

public class CCL_DataLoadInterface_cloneButton_Ext {
    // Definition of Variables
    ApexPages.Standardcontroller CCL_conInterface {get;set;}
    CCL_DataLoadInterface__c CCL_interface = new CCL_DataLoadInterface__c();
    List<CCL_DataLoadInterface_DataLoadMapping__c> CCL_maps = new List<CCL_DataLoadInterface_DataLoadMapping__c>();

    SavePoint sp;
    public Boolean displayBackButton {get;set;}
    ID CCL_newInterface = null;

    // Get the originial interface record from which a clone must be created
    public CCL_DataLoadInterface_cloneButton_Ext (ApexPages.StandardController conInterface) {
        this.CCL_conInterface = conInterface;
        
        CCL_interface = (CCL_DataLoadInterface__c) CCL_conInterface.getRecord();
    	
        displayBackButton = false;
    }
    
    // Creation URL for the Back Button which gets called when there is missing information
    public PageReference goback() {
        PageReference backButton = new PageReference ('/'+CCL_interface.Id);
		return backButton;
    }
    
    // Cloning process
    public Pagereference cloneInterface() { 
        // Initialize variables
        CCL_DataLoadInterface__c newInterface;
		List<CCL_DataLoadInterface_DataLoadMapping__c> CCL_mapList = new List<CCL_DataLoadInterface_DataLoadMapping__c>();
                
        CCL_interface = [SELECT Id, CCL_Batch_Size__c, CCL_Delimiter__c, CCL_Description__c, CCL_Interface_Status__c, CCL_Name__c, 
                        CCL_Selected_Object_Name__c, CCL_Text_Qualifier__c, CCL_Type_of_Upload__c
                        FROM CCL_DataLoadInterface__c
                        WHERE Id =: CCL_interface.Id];
        
        CCL_mapList = [SELECT Id, CCL_Column_Name__c, CCL_Field_type__c, CCL_isReferenceToExternalId__c, CCL_isUpsertId__c,
                    CCL_ReferenceType__c, CCL_Required__c, CCL_SObjectExternalId__c, CCL_SObject_Field__c, CCL_SObject_Mapped_Field__c, CCL_SObject_Name__c,
                    CCL_Default__c, CCL_Default_Value__c
					FROM CCL_DataLoadInterface_DataLoadMapping__c
					WHERE CCL_Data_Load_Interface__c =: CCL_interface.Id];
        
        newInterface = CCL_interface.clone(false, true);
		
        // Set standard values
        if (newInterface.CCL_Type_of_Upload__c == 'Insert')
            newInterface.RecordTypeId = Schema.SObjectType.CCL_DataLoadInterface__c.RecordTypeInfosByName.get('Insert Records').RecordTypeId;
        else if (newInterface.CCL_Type_of_Upload__c == 'Update')
        	newInterface.RecordTypeId = Schema.SObjectType.CCL_DataLoadInterface__c.RecordTypeInfosByName.get('Update Records').RecordTypeId;
        else if (newInterface.CCL_Type_of_Upload__c == 'Upsert')
        	newInterface.RecordTypeId = Schema.SObjectType.CCL_DataLoadInterface__c.RecordTypeInfosByName.get('Upsert Records').RecordTypeId;
        else if (newInterface.CCL_Type_of_Upload__c == 'Delete')
        	newInterface.RecordTypeId = Schema.SObjectType.CCL_DataLoadInterface__c.RecordTypeInfosByName.get('Delete Records').RecordTypeId;
        newInterface.CCL_Interface_Status__c = 'In Development';
        
        
        if(!CCL_mapList.isEmpty()){
            for(CCL_DataLoadInterface_DataLoadMapping__c mapList: CCL_mapList){
                CCL_DataLoadInterface_DataLoadMapping__c newMap = new CCL_DataLoadInterface_DataLoadMapping__c();
                newMap = mapList.clone(false, true);
                CCL_maps.add(newMap);
            }
        }
        try{
            insert newInterface;
            for(CCL_DataLoadInterface_DataLoadMapping__c mapList: CCL_maps){
                mapList.CCL_Data_Load_Interface__c = newInterface.Id;
                insert mapList;
            }
            update newInterface;
            CCL_newInterface = newInterface.Id;
           
        } catch (Exception e){
        	ApexPages.addMessages(e);
            
            displayBackButton = true;
            return null;
        }
        return new PageReference('/'+newInterface.id);   
    }
}