/***************************************************************************************************************************
Apex Class Name :	CCL_DataLoadInterface_clone_ExtTest
Version : 			1.0
Created Date : 		24/02/2015
Function :  		Test class for standard controller extension on CCL_DataLoadInterface_cloneButton_Ext object.

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois						  		24/02/2015      		        			Initial Class Creation       
***************************************************************************************************************************/

@isTest
private class CCL_DataLoadInterface_clone_ExtTest {
    /**
     * Test method for creating an insert interface and its mappings and then cloning it.
    **/
    static testMethod void CCL_DataLoadInterface_createAndCloneInsert(){
        // Create an interface record and different related mapping records
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
                                                                	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'account',
                                                                    CCL_Text_Qualifier__c = '"', CCL_Type_of_Upload__c = 'Insert');
        insert CCL_interfaceRecord;
        System.Debug('===CCL_interfaceRecord: '+CCL_interfaceRecord.Id);
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Name = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'Name',
																	CCL_Column_Name__c = 'Name', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'Name', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id);
		insert CCL_mappingRecord_Name;
		System.Debug('===CCL_mappingRecord_Name: ' + CCL_mappingRecord_Name.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Site = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'site',
																	CCL_Column_Name__c = 'Site', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'site', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = true);
		insert CCL_mappingRecord_Site;
		System.Debug('===CCL_mappingRecord_Site: ' + CCL_mappingRecord_Site.Id);
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Empl = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'NumberOfEmployees',
																	CCL_Column_Name__c = 'Employees', CCL_Field_Type__c = 'INTEGER', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'NumberOfEmployees', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = false);
		insert CCL_mappingRecord_Empl;
		System.Debug('===CCL_mappingRecord_Site: ' + CCL_mappingRecord_Empl.Id);
        
        CCL_interfaceRecord.CCL_Interface_Status__c = 'Active';
		update CCL_interfaceRecord;
        
        
        // Start test
        Test.startTest();
        
        // Create the standard controller and an instance of the to be tested class
        PageReference CCL_pageRef = Page.CCL_DataLoadInterface_cloneButton;
        ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_interfaceRecord);
		CCL_DataLoadInterface_cloneButton_Ext CCL_ext = new CCL_DataLoadInterface_cloneButton_Ext(CCL_stdCon);
        
        Test.setCurrentPage(CCL_pageRef);
        
        // Run the main method to clone the interface
        CCL_ext.cloneInterface();
        
        Test.stopTest();
        
		
		// Some queries and asserts to check the results
		List<CCL_DataLoadInterface__c> interfaces = [SELECT Id, CCL_Name__c, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c WHERE CCL_Name__c = 'Test'];
        System.assert(interfaces.size() == 2);
		List<CCL_DataLoadInterface__c> newInterface = [SELECT Id, CCL_Name__c, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c WHERE CCL_Interface_Status__c = 'In Development'];
        System.assert(newInterface.size() == 1);
        
        List<CCL_DataLoadInterface_DataLoadMapping__c> newMaps = [SELECT Id, CCL_SObject_Name__c FROM CCL_DataLoadInterface_DataLoadMapping__c WHERE CCL_Data_Load_Interface__c =: newInterface[0].Id];
        System.assert(newMaps.size() == 3);
        System.assert(newMaps[0].CCL_SObject_Name__c == 'account');
    }
    

    /**
     * Test method for creating an update interface and its mappings and then cloning it.
    **/
    static testMethod void CCL_DataLoadInterface_createAndCloneUpdate(){
        // Create an interface record and different related mapping records
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
                                                                	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'account',
                                                                    CCL_Text_Qualifier__c = '"', CCL_Type_of_Upload__c = 'Update');
        insert CCL_interfaceRecord;
        System.Debug('===CCL_interfaceRecord: '+CCL_interfaceRecord.Id);
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Name = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'Name',
																	CCL_Column_Name__c = 'Name', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'Name', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id);
		insert CCL_mappingRecord_Name;
		System.Debug('===CCL_mappingRecord_Name: ' + CCL_mappingRecord_Name.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Site = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'site',
																	CCL_Column_Name__c = 'Site', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'site', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = true);
		insert CCL_mappingRecord_Site;
		System.Debug('===CCL_mappingRecord_Site: ' + CCL_mappingRecord_Site.Id);
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Empl = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'NumberOfEmployees',
																	CCL_Column_Name__c = 'Employees', CCL_Field_Type__c = 'INTEGER', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'NumberOfEmployees', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = false);
		insert CCL_mappingRecord_Empl;
		System.Debug('===CCL_mappingRecord_Site: ' + CCL_mappingRecord_Empl.Id);
        
        CCL_interfaceRecord.CCL_Interface_Status__c = 'Active';
		update CCL_interfaceRecord;
        
        
        // Start test
        Test.startTest();
        
        // Create the standard controller and an instance of the to be tested class
        PageReference CCL_pageRef = Page.CCL_DataLoadInterface_cloneButton;
        ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_interfaceRecord);
		CCL_DataLoadInterface_cloneButton_Ext CCL_ext = new CCL_DataLoadInterface_cloneButton_Ext(CCL_stdCon);
        
        Test.setCurrentPage(CCL_pageRef);
        
        // Run the main method to clone the interface
        CCL_ext.cloneInterface();
        
        Test.stopTest();
        
		
		// Some queries and asserts to check the results
		List<CCL_DataLoadInterface__c> interfaces = [SELECT Id, CCL_Name__c, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c WHERE CCL_Name__c = 'Test'];
        System.assert(interfaces.size() == 2);
		List<CCL_DataLoadInterface__c> newInterface = [SELECT Id, CCL_Name__c, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c WHERE CCL_Interface_Status__c = 'In Development'];
        System.assert(newInterface.size() == 1);
        
        List<CCL_DataLoadInterface_DataLoadMapping__c> newMaps = [SELECT Id, CCL_SObject_Name__c FROM CCL_DataLoadInterface_DataLoadMapping__c WHERE CCL_Data_Load_Interface__c =: newInterface[0].Id];
        System.assert(newMaps.size() == 3);
        System.assert(newMaps[0].CCL_SObject_Name__c == 'account');
    }
    
    
    /**
     * Test method for creating an upsert interface and its mappings and then cloning it.
    **/
    static testMethod void CCL_DataLoadInterface_createAndCloneUpsert(){
        // Create an interface record and different related mapping records
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
                                                                	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'account',
                                                                    CCL_Text_Qualifier__c = '"', CCL_Type_of_Upload__c = 'Upsert');
        insert CCL_interfaceRecord;
        System.Debug('===CCL_interfaceRecord: '+CCL_interfaceRecord.Id);
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Name = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'Name',
																	CCL_Column_Name__c = 'Name', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'Name', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id);
		insert CCL_mappingRecord_Name;
		System.Debug('===CCL_mappingRecord_Name: ' + CCL_mappingRecord_Name.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Site = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'site',
																	CCL_Column_Name__c = 'Site', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'site', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = true);
		insert CCL_mappingRecord_Site;
		System.Debug('===CCL_mappingRecord_Site: ' + CCL_mappingRecord_Site.Id);
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Empl = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'NumberOfEmployees',
																	CCL_Column_Name__c = 'Employees', CCL_Field_Type__c = 'INTEGER', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'NumberOfEmployees', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = false);
		insert CCL_mappingRecord_Empl;
		System.Debug('===CCL_mappingRecord_Site: ' + CCL_mappingRecord_Empl.Id);
        
        CCL_interfaceRecord.CCL_Interface_Status__c = 'Active';
		update CCL_interfaceRecord;
        
        
        // Start test
        Test.startTest();
        
        // Create the standard controller and an instance of the to be tested class
        PageReference CCL_pageRef = Page.CCL_DataLoadInterface_cloneButton;
        ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_interfaceRecord);
		CCL_DataLoadInterface_cloneButton_Ext CCL_ext = new CCL_DataLoadInterface_cloneButton_Ext(CCL_stdCon);
        
        Test.setCurrentPage(CCL_pageRef);
        
        // Run the main method to clone the interface
        CCL_ext.cloneInterface();
        
        Test.stopTest();
        
		
		// Some queries and asserts to check the results
		List<CCL_DataLoadInterface__c> interfaces = [SELECT Id, CCL_Name__c, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c WHERE CCL_Name__c = 'Test'];
        System.assert(interfaces.size() == 2);
		List<CCL_DataLoadInterface__c> newInterface = [SELECT Id, CCL_Name__c, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c WHERE CCL_Interface_Status__c = 'In Development'];
        System.assert(newInterface.size() == 1);
        
        List<CCL_DataLoadInterface_DataLoadMapping__c> newMaps = [SELECT Id, CCL_SObject_Name__c FROM CCL_DataLoadInterface_DataLoadMapping__c WHERE CCL_Data_Load_Interface__c =: newInterface[0].Id];
        System.assert(newMaps.size() == 3);
        System.assert(newMaps[0].CCL_SObject_Name__c == 'account');
    }
    
    
    /**
     * Test method for creating an delete interface and its mappings and then cloning it.
    **/
    static testMethod void CCL_DataLoadInterface_createAndCloneDelete(){
        // Create an interface record and different related mapping records
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
                                                                	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'account',
                                                                    CCL_Text_Qualifier__c = '"', CCL_Type_of_Upload__c = 'Delete');
        insert CCL_interfaceRecord;
        System.Debug('===CCL_interfaceRecord: '+CCL_interfaceRecord.Id);
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Name = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'Name',
																	CCL_Column_Name__c = 'Name', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'Name', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id);
		insert CCL_mappingRecord_Name;
		System.Debug('===CCL_mappingRecord_Name: ' + CCL_mappingRecord_Name.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Site = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'site',
																	CCL_Column_Name__c = 'Site', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'site', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = true);
		insert CCL_mappingRecord_Site;
		System.Debug('===CCL_mappingRecord_Site: ' + CCL_mappingRecord_Site.Id);
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Empl = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'NumberOfEmployees',
																	CCL_Column_Name__c = 'Employees', CCL_Field_Type__c = 'INTEGER', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'NumberOfEmployees', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = false);
		insert CCL_mappingRecord_Empl;
		System.Debug('===CCL_mappingRecord_Site: ' + CCL_mappingRecord_Empl.Id);
        
        CCL_interfaceRecord.CCL_Interface_Status__c = 'Active';
		update CCL_interfaceRecord;
        
        
        // Start test
        Test.startTest();
        
        // Create the standard controller and an instance of the to be tested class
        PageReference CCL_pageRef = Page.CCL_DataLoadInterface_cloneButton;
        ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_interfaceRecord);
		CCL_DataLoadInterface_cloneButton_Ext CCL_ext = new CCL_DataLoadInterface_cloneButton_Ext(CCL_stdCon);
        
        Test.setCurrentPage(CCL_pageRef);
        
        // Run the main method to clone the interface
        CCL_ext.cloneInterface();
        
        Test.stopTest();
        
		
		// Some queries and asserts to check the results
		List<CCL_DataLoadInterface__c> interfaces = [SELECT Id, CCL_Name__c, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c WHERE CCL_Name__c = 'Test'];
        System.assert(interfaces.size() == 2);
		List<CCL_DataLoadInterface__c> newInterface = [SELECT Id, CCL_Name__c, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c WHERE CCL_Interface_Status__c = 'In Development'];
        System.assert(newInterface.size() == 1);
        
        List<CCL_DataLoadInterface_DataLoadMapping__c> newMaps = [SELECT Id, CCL_SObject_Name__c FROM CCL_DataLoadInterface_DataLoadMapping__c WHERE CCL_Data_Load_Interface__c =: newInterface[0].Id];
        System.assert(newMaps.size() == 3);
        System.assert(newMaps[0].CCL_SObject_Name__c == 'account');
    }
}