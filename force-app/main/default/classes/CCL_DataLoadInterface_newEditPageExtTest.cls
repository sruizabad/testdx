/***************************************************************************************************************************
Apex Test Class Name : CCL_DataLoadInterface_newEditPageExtTest
Version : 1.0
Created Date : 01/02/2015
Function :  Test class for standard controller extension on DataLoadInterface object. 

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois						  01/02/2015		      		        Initial Class Creation        
***************************************************************************************************************************/

@isTest
private class CCL_DataLoadInterface_newEditPageExtTest {
	/**
	* Test method creating the extension and comparing the list of sObjects from the page towards the list
	* captured from the global Schema
	**/
    static testMethod void CCL_DataLoadInterface_createRecord() {
        Test.startTest();
        PageReference CCL_testPage = Page.CCL_DataLoadInterface_newEditPage;
        Id CCL_upsertRT = [SELECT Id FROM RecordType WHERE sObjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Upsert_Records'].Id;

        Test.setCurrentPage(CCL_testPage);
        CCL_testPage.getParameters().put('RecordType',String.valueof(CCL_upsertRT));

        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c();
        
        ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_interfaceRecord);
        CCL_DataLoadInterface_newEditPage_Ext CCL_extension = new CCL_DataLoadInterface_newEditPage_Ext(CCL_stdCon);

        // Retrieve the available list of sObjects, put them in a list and assertEqual towards the list of the extension
        List<SelectOption> CCL_listSObjects = new List<SelectOption>();
        Map<String, Schema.SObjectType> CCL_map_stringSObjectName_SchemaSObjectType = Schema.getGlobalDescribe();
        Integer picklistLimit = 0; 
        for (AggregateResult aggr : [SELECT SObjectType From ObjectPermissions Group By SObjectType]){
            try{ 
                DescribeSObjectResult d = CCL_map_stringSObjectName_SchemaSObjectType.get((String)aggr.get('SObjectType')).getDescribe();            
                CCL_listSObjects.add(new SelectOption(d.getName(),d.getLabel()));
                picklistLimit++;
                if (picklistLimit == 999) break; //Safeguard picklist limit
            } catch (Exception e) {
                // Object not found
            }
        }
        CCL_listSObjects.sort();
        
        // Retrieve the list from the Page Layout and compare        
        System.assertEquals(CCL_listSObjects,CCL_extension.getCCL_sObjectNames()); 
        
        // Compare the selected Sobject towards the record saved in the DB
        CCL_interfaceRecord.CCL_Selected_Object_Name__c = 'account';
        CCL_interfaceRecord.CCL_Batch_Size__c = 25;
        CCL_interfaceRecord.CCL_Delimiter__c = ',';
        CCL_interfaceRecord.CCL_Interface_Status__c = 'In Development';
        CCL_interfaceRecord.CCL_Name__c = 'TestName';
        CCL_interfaceRecord.CCL_Text_Qualifier__c = '\"';
        CCL_interfaceRecord.CCL_Type_of_Upload__c = 'Upsert';
        
        CCL_stdCon.save();
        
        CCL_DataLoadInterface__c CCL_queriedResult = [SELECT CCL_Selected_Object_Name__c 
        												FROM CCL_DataLoadInterface__c
        												WHERE CCL_Name__c = 'TestName'];
        
        System.assert(CCL_queriedResult.CCL_Selected_Object_Name__c == 'account');
        System.assert(true, CCL_extension.getCCL_isUpsert());
        
        Test.stopTest();     
    }
    
    /**
    * Test method editing an existing record
    **/
    static testMethod void CCL_DataLoadInterface_editRecord() {
        Id CCL_insertRT = [SELECT Id FROM RecordType WHERE sObjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Upsert_Records'].Id;

    	CCL_DataLoadInterface__c CCL_updateRecord = new CCL_DataLoadInterface__c(
    		CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',', CCL_Description__c = 'Test Description',
    		CCL_Interface_Status__c = 'In Development', CCL_Name__c = 'TestName', 
    		CCL_Selected_Object_Name__c = 'account', CCL_Text_Qualifier__c = '\"',
    		CCL_Type_of_Upload__c = 'Upsert', RecordTypeId = CCL_insertRT);
    	insert CCL_updateRecord;
    	
    	// Interface and edit the record
    	ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_updateRecord);
    	CCL_DataLoadInterface_newEditPage_Ext CCL_extension = new CCL_DataLoadInterface_newEditPage_Ext(CCL_stdCon);
    	
    	Test.startTest();
    	
    	PageReference CCL_testPage = Page.CCL_dataLoadInterface_newEditPage;
    	CCL_testPage.getParameters().put('id',String.valueOf(CCL_updateRecord.Id));
    	Test.setCurrentpage(CCL_testPage);
    	
    	 // Retrieve the available list of sObjects, put them in a list and assertEqual towards the list of the extension
        List<SelectOption> CCL_listSObjects = new List<SelectOption>();
        Map<String, Schema.SObjectType> CCL_map_stringSObjectName_SchemaSObjectType = Schema.getGlobalDescribe();
        Integer picklistLimit = 0; 
        for (AggregateResult aggr : [SELECT SObjectType From ObjectPermissions Group By SObjectType]){
            try{ 
                DescribeSObjectResult d = CCL_map_stringSObjectName_SchemaSObjectType.get((String)aggr.get('SObjectType')).getDescribe();            
                CCL_listSObjects.add(new SelectOption(d.getName(),d.getLabel()));
                picklistLimit++;
                if (picklistLimit == 999) break; //Safeguard picklist limit
            } catch (Exception e) {
                // Object not found
            }
        }
        CCL_listSObjects.sort();

        // Retrieve the list from the Page Layout and compare        
        System.assertEquals(CCL_listSObjects,CCL_extension.getCCL_sObjectNames()); 
        
        // Edit and save the record to another selected Sobject
        CCL_updateRecord.CCL_Selected_Object_Name__c = 'user';
        CCL_stdCon.save();
        
        CCL_DataLoadInterface__c CCL_queriedRecord = [SELECT CCL_Selected_Object_Name__c
        											FROM CCL_DataLoadInterface__c
        											WHERE Id =: CCL_updateRecord.Id];
        											 
        // Compare the selected Sobject to the value we expect
        System.assert(CCL_queriedRecord.CCL_Selected_Object_Name__c == 'user');
        
        Test.stopTest();   
    }
}