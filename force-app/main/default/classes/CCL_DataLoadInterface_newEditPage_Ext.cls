/***************************************************************************************************************************
Apex Class Name :   CCL_DataLoadInterface_newEditPage_Ext
Version :           1.0
Created Date :      27/01/2015
Function :          Extension of Standard controller on sObject CCL_DateLoadInterface__c. The extension will be used on VisualForce
                    page CCL_DataLoadInterface_NewEditPage which has a dynamic picklist of available sObjects within the Salesforce
                    org. The dynamic binding will be done using the below code. 

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                                 Date                                    Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois                              27/01/2015                              Initial Creation        
***************************************************************************************************************************/

public without sharing class CCL_DataLoadInterface_newEditPage_Ext {
    private CCL_DataLoadInterface__c CCL_interfaceRecord;
    private static Map<String, Schema.SObjectType> CCL_map_stringKeyName_schemasObjects = Schema.getGlobalDescribe();
    private String CCL_dmlRecordType = '';
    private boolean CCL_isUpsert = false;
    
    public CCL_DataLoadInterface_newEditPage_Ext(ApexPages.StandardController CCL_stdCon) {
        this.CCL_interfaceRecord = (CCL_DataLoadInterface__c) CCL_stdCon.getRecord();

        if(ApexPages.currentPage().getParameters().get('RecordType') != null) {
            String CCL_recordTypeAPIName = [SELECT DeveloperName 
                                            FROM RecordType 
                                            WHERE Id =: ApexPages.currentPage().getParameters().get('RecordType') 
                                            AND SobjectType =: ('CCL_DataLoadInterface__c') LIMIT 1].DeveloperName;
            
            if(CCL_recordTypeAPIName == 'CCL_DataLoadInterface_Delete_Records') {
                CCL_dmlRecordType = 'Delete';   
                
            } else if(CCL_recordTypeAPIName == 'CCL_DataLoadInterface_Insert_Records') {
                CCL_dmlRecordType = 'Insert';   
                
            } else if(CCL_recordTypeAPIName == 'CCL_DataLoadInterface_Update_Records') {
                CCL_dmlRecordType = 'Update';   
                
            } else if(CCL_recordTypeAPIName == 'CCL_DataLoadInterface_Upsert_Records') {
                CCL_dmlRecordType = 'Upsert';
                CCL_isUpsert = true;    
            }

            this.CCL_interfaceRecord.CCL_Type_of_Upload__c = CCL_dmlRecordType;
            
        } else if(CCL_interfaceRecord.RecordTypeId != null && CCL_interfaceRecord.CCL_Type_of_Upload__c == 'Upsert') {
            CCL_isUpsert = true;
        }
    }
    
    /**
    *   Getters and Setters
    **/
    public boolean getCCL_isUpsert() {
        return CCL_isUpsert;
    }
    
    /**
    *   Method to get all the sObjects available to the SalesForce org.
    **/
     public List<SelectOption> getCCL_sObjectNames() {
        Map<String,SObjectType> objectsMap = Schema.getGlobalDescribe();
        List<SelectOption> CCL_list_selectsObjectOptions = new List<SelectOption>();
        Integer picklistLimit = 0;                
        for (AggregateResult aggr : [SELECT SObjectType From ObjectPermissions Group By SObjectType]){
            try{ 
                DescribeSObjectResult d = objectsMap.get((String)aggr.get('SObjectType')).getDescribe();            
                CCL_list_selectsObjectOptions.add(new SelectOption(d.getName(),d.getLabel()));
                picklistLimit++;
                if (picklistLimit == 999) break; //Safeguard picklist limit
            } catch (Exception e) {
                // Object not found
            }
        }
        CCL_list_selectsObjectOptions.sort();
        return CCL_list_selectsObjectOptions;    
    }

}