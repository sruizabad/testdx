/***************************************************************************************************************************
Apex Class Name : 	CCL_DataLoadMapping_newEditPage_Ext
Version : 			1.0
Created Date : 		01/02/2015
Function : 			Standard Controller Extension for VF page related to Mapping columns.  

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date    				Description
* -----------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois						  		01/02/2015		     	Initial Class Creation      
* Joris Artois								19/10/2015				Update the code to move all logic to the DataLoadUtility class,
																	thereby making the code accessible from a trigger as well for imports  
***************************************************************************************************************************/

public without sharing class CCL_DataLoadMapping_newEditPage_Ext {	
	private ApexPages.StandardController controller;
	private CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord;
	private String selectedObject;
	private Map<String, Schema.SObjectField> fieldMap;
	private String CCL_fieldType = '';
	private List<SelectOption> fieldNames = new List<SelectOption>();
	private List<SelectOption> externalIdFields = new List<SelectOption>();
	private String CCL_interfaceId = '';
	private List <Schema.sObjectType> parentObjects;
	
	public Boolean isReference = false;
	public Boolean isExternal = false;
	public Boolean CCL_isDefault = false;
		
	public CCL_DataLoadMapping_newEditPage_Ext(ApexPages.StandardController CCL_stdCon){
		this.controller = CCL_stdCon;
		this.CCL_mappingRecord = (CCL_DataLoadInterface_DataLoadMapping__c) CCL_stdCon.getRecord();
        
        if(CCL_mappingRecord.CCL_Data_Load_Interface__c != null) {
        	
        	//System.Debug('--> sObject Name: ' + CCL_mappingRecord.CCL_SObject_Name__c);
        	List<CCL_DataLoadInterface_DataLoadMapping__c> mappingRecord_lst = new List<CCL_DataLoadInterface_DataLoadMapping__c>();
        	mappingRecord_lst.add(CCL_mappingRecord);
        	CCL_dataLoadUtilityClass.CCL_SelectedObject(mappingRecord_lst);
        	System.Debug('--> sObject Name: ' + CCL_mappingRecord.CCL_SObject_Name__c);
        	
        	selectedObject = CCL_mappingRecord.CCL_SObject_Name__c;
        	/*
            CCL_interfaceId = ApexPages.currentPage().getParameters().get('retURL');
            CCL_interfaceId = CCL_interfaceId.remove('/');
            
            
            if(CCL_mappingRecord.CCL_SObject_Field__c == '' || CCL_MappingRecord.CCL_Sobject_Field__c == null) {
                try {
                    selectedObject = [SELECT CCL_Selected_Object_Name__c
                                     FROM CCL_DataLoadInterface__c
                                     WHERE Id =: CCL_interfaceId LIMIT 1].CCL_Selected_Object_Name__c;
                    CCL_mappingRecord.CCL_Sobject_Name__c = selectedObject;
                    CCL_mappingRecord.CCL_Data_Load_Interface__c = CCL_interfaceId;
                } catch (Exception CCL_e) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, CCL_e.getMessage()));
                }
            } else {
                selectedObject = [SELECT CCL_SObject_Name__c
                                 FROM CCL_DataLoadInterface_DataLoadMapping__c
                                 WHERE Id =: CCL_mappingRecord.Id LIMIT 1].CCL_SObject_Name__c;
            }*/
        }
		
		if(selectedObject != '' && selectedObject != null) {	
			fieldNames.add(new SelectOption('--None--','--None--'));
			for(String fieldName: CCL_dataLoadUtilityClass.CCL_getMap_fieldName_sObjectField(selectedObject).keySet()){
				fieldNames.add(new SelectOption(fieldName, fieldName));
			}
			fieldNames.sort();
		}
		
		CCL_mappingRecord.CCL_Default_Value__c = null;
		FieldType();
			
	}
	
	public Boolean getisReference(){
		return isReference;
	}
	
	public Boolean getisExternal(){
		
		return isExternal;
	}
	
	public Boolean getisDefault(){
		return CCL_isDefault;
	}
	
	public void isDefaultValue(){
		CCL_isDefault = CCL_mappingRecord.CCL_Default__c;
		if(!CCL_isDefault){
			CCL_mappingRecord.CCL_Default_Value__c = null;
		}
	}
	
	public void isExtern(){
		if (CCL_mappingRecord.CCL_ReferenceType__c == 'External Id' & isReference){
			isExternal = true;
			ObjectExternalId();
           	
		} else {
			isExternal = false;
		}
	}
	
	public String selectedField {get;}
	
	
	public List<SelectOption> getObjectFields() {
		return fieldNames;
	}
	
	public List<SelectOption> getExternalIdFields() {
		return externalIdFields;
	}
	
	public void FieldType(){
		if (CCL_mappingRecord.CCL_SObject_Field__c == '--None--'){
			CCL_fieldType = '';
			CCL_mappingRecord.CCL_Field_Type__c = CCL_fieldType;
		}
		else if(CCL_mappingRecord.CCL_SObject_Field__c != null){
			List<CCL_DataLoadInterface_DataLoadMapping__c> mappingRecord_lst = new List<CCL_DataLoadInterface_DataLoadMapping__c>();
			mappingRecord_lst.add(CCL_mappingRecord);
			CCL_dataLoadUtilityClass.CCL_FieldType(mappingRecord_lst);
			
			CCL_fieldType = CCL_mappingRecord.CCL_Field_Type__c;
			if(CCL_fieldType == 'ID' || CCL_fieldType == 'REFERENCE'){
				isReference = true;
			}
			else {
				isReference = false;
			}
			/*
			Schema.DisplayType myType = CCL_dataLoadUtilityClass.CCL_getMap_fieldName_sObjectField(selectedObject).get(CCL_mappingRecord.CCL_SObject_Field__c).getDescribe().getType();
			if(myType == Schema.DisplayType.Reference ){
				isReference = true;
				
			} else {
				isReference = false;
				CCL_mappingRecord.CCL_ReferenceType__c = null;
				CCL_mappingRecord.CCL_SObjectExternalId__c = null;
			}
			
			CCL_fieldType = String.valueOf(myType);
			
			CCL_mappingRecord.CCL_Field_Type__c = CCL_fieldType;*/
		}
		else {
			System.Debug('===emptySObject==='); 
		}
				
		isExtern();
	}
	
	public void ObjectExternalId(){
		if(isExternal & isReference){
			externalIdFields.clear();
			
			parentObjects = CCL_dataLoadUtilityClass.CCL_getList_referencesObject(selectedObject,CCL_mappingRecord.CCL_Sobject_Field__c);
			Map<String, Schema.SObjectField> this_fieldMap;
			
			for(Schema.sObjectType parentObject: parentObjects){
				this_fieldMap = parentObject.getDescribe().fields.getMap();
					for(String field: this_fieldmap.keySet()){
						if(this_fieldMap.get(field).getDescribe().isExternalId()){
							externalIdFields.add(new SelectOption(field, field));
						}
					}
			}
			externalIdFields.sort();
		} else {
			externalIdFields.clear();
		}
	}
	
	public PageReference save(){
		/*if(isReference){
			List <Schema.sObjectType> CCL_list_referencesObject = CCL_dataLoadUtilityClass.CCL_getList_referencesObject(selectedObject,CCL_mappingRecord.CCL_Sobject_Field__c);
			Schema.DescribeSObjectResult dsr = CCL_list_referencesObject[0].getDescribe();
			
			/**
            * Author: Joris Artois
            * Date: 14/03/2015
            * Description: Bug fix on isCustom. Instead of checking on the object where the reference is to, check on the object itself if it ends with "__c".
            **/
       /*     boolean isCustom = false;
            if(String.ValueOf(CCL_mappingRecord.CCL_Sobject_Field__c).endsWith('__c')){
                isCustom = true;
            }
			CCL_mappingRecord.CCL_SObject_Mapped_Field__c = CCL_dataLoadUtilityClass.CCL_getMappedField_Name(isReference, isExternal, CCL_mappingRecord.CCL_Sobject_Field__c, CCL_mappingRecord.CCL_SObjectExternalId__c, isCustom);
		} else {
			CCL_mappingRecord.CCL_SObject_Mapped_Field__c = CCL_mappingRecord.CCL_Sobject_Field__c;
		}
		
		/**
		* Check if the chosen field is an External Id field
		**/
		/*if(CCL_mappingRecord.CCL_SObject_Mapped_Field__c != '--None--'){
			CCL_mappingRecord.CCL_isUpsertId__c = CCL_DataLoadUtilityClass.CCL_isUpsertField(selectedObject, CCL_mappingRecord.CCL_Sobject_Field__c);
		}*/
		
		try{
			this.controller.save();
        } catch(System.DMLException e) {
            System.Debug('======e: ' + e);
		} catch(exception CCL_e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, CCL_e.getDmlMessage(0)));
		}
        
        if(!ApexPages.hasMessages()) {
            PageReference CCL_returnPage = new PageReference('/'+CCL_mappingRecord.CCL_Data_Load_Interface__c);		
			CCL_returnPage.setRedirect(true);
			return CCL_returnPage;
        }
		return null;
	}
}