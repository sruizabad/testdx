/***************************************************************************************************************************
Apex Test Class Name : CCL_DataLoadMapping_newEditPage_ExtTest
Version : 1.0
Created Date : 01/02/2015
Function :  Test class for extention towards standard controller on DataLoadMapping object. 

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois								01/02/2015	      		        		Initial Creation
* Joris Artois							    13/02/2015								Adjust for new validation rules 
* Joris Artois								20/10/2015								(R2.4/PB-03) Update to accomodate new update DataloadUtility class
																					and the new Mapping Extension with the logic in the trigger
***************************************************************************************************************************/

@isTest
private class CCL_DataLoadMapping_newEditPage_ExtTest {

    static testMethod void CCL_createRecord() {
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'account',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Upsert');
        insert CCL_interfaceRecord;
        
        Test.startTest();
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mapRecord = new CCL_DataLoadInterface_DataLoadMapping__c();
        
        PageReference CCL_pageRef = Page.CCL_DataLoadMapping_NewEditPage;        
        ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_mapRecord);
   		ApexPages.currentPage().getParameters().put('retURL','/'+CCL_interfaceRecord.Id);
        CCL_mapRecord.CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id;   
        CCL_mapRecord.CCL_Field_Type__c = 'test';
        CCL_DataLoadMapping_newEditPage_Ext CCL_ext = new CCL_DataLoadMapping_newEditPage_Ext(CCL_stdCon);
        
        Test.setCurrentPage(CCL_pageRef);
        
        /* 	Author: Joris Artois
        	R2.4/PB-03: comment this because the new trigger would fire and fill in all the hidden fields
        	This would cause errors because none of the fields are populated yet.
        */
        //insert CCL_mapRecord;
        
        // Check if the Sobject Name has been copied from the interface 

        //System.Debug('===mapRecordId: '+CCL_mapRecord.Id);
        
        System.assert(CCL_mapRecord.CCL_SObject_Name__c == 'account');
        
        // Check the list of sObject Field
        Map<String, Schema.SObjectType> CCL_schemaMap = Schema.getGlobalDescribe();
		Schema.SObjectType CCL_objectSchema = CCL_schemaMap.get(CCL_mapRecord.CCL_SObject_Name__c);
		Map<String, Schema.SObjectField> CCL_map_fieldName_sObjectField = CCL_objectSchema.getDescribe().fields.getMap();
        
        // Check the retrieved sobject Fields
        System.assert(CCL_map_fieldName_sObjectField.keySet() == CCL_dataLoadUtilityClass.CCL_getMap_fieldName_sObjectField(CCL_interfaceRecord.CCL_Selected_Object_Name__c).keySet());
        
        // Compare option list
        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('--None--', '--None--'));
        
        for(String fieldName : CCL_map_fieldName_sObjectField.keySet()) {
            fieldNames.add(new SelectOption(fieldName, fieldName));
        }
        fieldNames.sort();
        
        //System.assert(fieldNames == CCL_ext.getObjectFields());
        
        // Set the field 'Name' as selected field
        CCL_mapRecord.CCL_SObject_Field__c = 'Name';
        
        // Compare is the field is an External reference
        System.assert(CCL_ext.getisReference() == false);
        
        // Compare if the field is an External Id
        System.assert(CCL_ext.getisExternal() == false);
        
        // Compare the field Type by first calling the function to determine the type of the Field
        CCL_ext.FieldType();
        System.assert(CCL_mapRecord.CCL_Field_Type__c == 'STRING');
        
        // Populate the other fields manually and save the record
        CCL_mapRecord.CCL_Column_Name__c = 'CSVName';
        CCL_mapRecord.CCL_Required__c = true;
        
        CCL_ext.save();
        
        // Query the record and compare to the expected results
        List<CCL_DataLoadInterface_DataLoadMapping__c> CCL_queriedField = [SELECT CCL_Field_Type__c, CCL_isReferenceToExternalId__c, CCL_isUpsertId__c, CCL_ReferenceType__c,
                                                                    CCL_Required__c, CCL_SObjectExternalId__c, CCL_SObject_Field__c, CCL_Sobject_Mapped_Field__c, CCL_Sobject_Name__c
                                                                    FROM CCL_DataLoadInterface_DataLoadMapping__c
                                                                    WHERE CCL_Data_Load_Interface__c =: CCL_interfaceRecord.Id];
                                                                    
        System.assert(CCL_queriedField[0].CCL_Field_Type__c == 'STRING');
        System.assert(CCL_queriedField[0].CCL_isReferenceToExternalId__c == false);
        System.assert(CCL_queriedField[0].CCL_isUpsertId__c == false);
        System.assert(CCL_queriedField[0].CCL_ReferenceType__c == null);
        System.assert(CCL_queriedField[0].CCL_Required__c == true);
        System.assert(CCL_queriedField[0].CCL_SObjectExternalId__c == null);
        System.assert(CCL_queriedField[0].CCL_SObject_Field__c == 'Name');
        System.assert(CCL_queriedField[0].CCL_Sobject_Mapped_Field__c == 'Name');
        System.assert(CCL_queriedField[0].CCL_SObject_Name__c == 'account');
              
        Test.stopTest();
    }
    
    static testMethod void CCL_createReferenceRecord() {
  
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'opportunity',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Upsert');
        insert CCL_interfaceRecord;
        System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord.Id);
        
        Test.startTest();
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mapRecord = new CCL_DataLoadInterface_DataLoadMapping__c();
        
        PageReference CCL_pageRef = Page.CCL_DataLoadMapping_NewEditPage;        
        ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_mapRecord);
   		ApexPages.currentPage().getParameters().put('retURL','/'+CCL_interfaceRecord.Id);
        CCL_mapRecord.CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id;   
        CCL_mapRecord.CCL_Field_Type__c = 'test';
        CCL_DataLoadMapping_newEditPage_Ext CCL_ext = new CCL_DataLoadMapping_newEditPage_Ext(CCL_stdCon);
        
        Test.setCurrentPage(CCL_pageRef);
        
        /* 	Author: Joris Artois
        	R2.4/PB-03: comment this because the new trigger would fire and fill in all the hidden fields
        	This would cause errors because none of the fields are populated yet.
        */
        //insert CCL_mapRecord;
        
        // Check if the Sobject Name has been copied from the interface 

        //System.Debug('===mapRecordId: '+CCL_mapRecord.Id);
        
        //System.assert(CCL_mapRecord.CCL_SObject_Name__c == 'opportunity');
        
        // Check the list of sObject Field
        Map<String, Schema.SObjectType> CCL_schemaMap = Schema.getGlobalDescribe();
		Schema.SObjectType CCL_objectSchema = CCL_schemaMap.get(CCL_mapRecord.CCL_SObject_Name__c);
		Map<String, Schema.SObjectField> CCL_map_fieldName_sObjectField = CCL_objectSchema.getDescribe().fields.getMap();
        
        // Check the retrieved sobject Fields
        System.assert(CCL_map_fieldName_sObjectField.keySet() == CCL_dataLoadUtilityClass.CCL_getMap_fieldName_sObjectField(CCL_interfaceRecord.CCL_Selected_Object_Name__c).keySet());
        
        // Compare option list
        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('--None--', '--None--'));
        
        for(String fieldName : CCL_map_fieldName_sObjectField.keySet()) {
            fieldNames.add(new SelectOption(fieldName, fieldName));
        }
        fieldNames.sort();
        
        System.assert(fieldNames == CCL_ext.getObjectFields());
        
        // Set the field 'Name' as selected field
        CCL_mapRecord.CCL_SObject_Field__c = 'accountid';
        CCL_mapRecord.CCL_ReferenceType__c = 'External Id';
        
        insert CCL_mapRecord;
        
        // Compare is the field is an External reference
        //System.assert(CCL_ext.getisReference() == true);
        
        // Compare if the field is an External Id
        //System.assert(CCL_ext.getisExternal() == true);
        
        // Compare the field Type by first calling the function to determine the type of the Field
        CCL_ext.FieldType();
        System.assert(CCL_mapRecord.CCL_Field_Type__c == 'REFERENCE');
        
        // Populate the other fields manually and save the record
        CCL_mapRecord.CCL_Column_Name__c = 'CSVName';
        CCL_mapRecord.CCL_Required__c = true;
        
        CCL_ext.save();
        
        // Query the record and compare to the expected results
        List<CCL_DataLoadInterface_DataLoadMapping__c> CCL_queriedField = [SELECT CCL_Field_Type__c, CCL_isReferenceToExternalId__c, CCL_isUpsertId__c, CCL_ReferenceType__c,
                                                                    CCL_Required__c, CCL_SObjectExternalId__c, CCL_SObject_Field__c, CCL_Sobject_Mapped_Field__c, CCL_Sobject_Name__c
                                                                    FROM CCL_DataLoadInterface_DataLoadMapping__c
                                                                    WHERE CCL_Data_Load_Interface__c =: CCL_interfaceRecord.Id];
                                                                    
        System.assert(CCL_queriedField[0].CCL_Field_Type__c == 'REFERENCE');
        System.assert(CCL_queriedField[0].CCL_isReferenceToExternalId__c == false);
        System.assert(CCL_queriedField[0].CCL_isUpsertId__c == false);
        System.assert(CCL_queriedField[0].CCL_ReferenceType__c == 'External Id');
        System.assert(CCL_queriedField[0].CCL_Required__c == true);
        System.assert(CCL_queriedField[0].CCL_SObjectExternalId__c == null);
        System.assert(CCL_queriedField[0].CCL_SObject_Field__c == 'accountid');
        //System.assert(CCL_queriedField[0].CCL_Sobject_Mapped_Field__c == 'Name');
        System.assert(CCL_queriedField[0].CCL_SObject_Name__c == 'opportunity');
        
        Test.stopTest();
    }	
    
    
    static testMethod void CCL_createIdRecord() {
  
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'account',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Upsert');
        insert CCL_interfaceRecord;
        System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord.Id);
        
        Test.startTest();
        
        CCL_DataLoadInterface_DataLoadMapping__c CCL_mapRecord = new CCL_DataLoadInterface_DataLoadMapping__c();
        
        PageReference CCL_pageRef = Page.CCL_DataLoadMapping_NewEditPage;        
        ApexPages.StandardController CCL_stdCon = new ApexPages.StandardController(CCL_mapRecord);
   		ApexPages.currentPage().getParameters().put('retURL','/'+CCL_interfaceRecord.Id);
        CCL_mapRecord.CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id;   
        CCL_mapRecord.CCL_Field_Type__c = 'test';
        CCL_DataLoadMapping_newEditPage_Ext CCL_ext = new CCL_DataLoadMapping_newEditPage_Ext(CCL_stdCon);
        
        Test.setCurrentPage(CCL_pageRef);
        
        /* 	Author: Joris Artois
        	R2.4/PB-03: comment this because the new trigger would fire and fill in all the hidden fields
        	This would cause errors because none of the fields are populated yet.
        */
        //insert CCL_mapRecord;
        
        // Check if the Sobject Name has been copied from the interface 

        //System.Debug('===mapRecordId: '+CCL_mapRecord.Id);
        
        //System.assert(CCL_mapRecord.CCL_SObject_Name__c == 'account');
        
        // Check the list of sObject Field
        Map<String, Schema.SObjectType> CCL_schemaMap = Schema.getGlobalDescribe();
		Schema.SObjectType CCL_objectSchema = CCL_schemaMap.get(CCL_mapRecord.CCL_SObject_Name__c);
		Map<String, Schema.SObjectField> CCL_map_fieldName_sObjectField = CCL_objectSchema.getDescribe().fields.getMap();
        
        // Check the retrieved sobject Fields
        System.assert(CCL_map_fieldName_sObjectField.keySet() == CCL_dataLoadUtilityClass.CCL_getMap_fieldName_sObjectField(CCL_interfaceRecord.CCL_Selected_Object_Name__c).keySet());
        
        // Compare option list
        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('--None--', '--None--'));
        
        for(String fieldName : CCL_map_fieldName_sObjectField.keySet()) {
            fieldNames.add(new SelectOption(fieldName, fieldName));
        }
        fieldNames.sort();
        
        System.assert(fieldNames == CCL_ext.getObjectFields());
        
        // Set the field 'Name' as selected field
        CCL_mapRecord.CCL_SObject_Field__c = 'id';
        
        
        insert CCL_mapRecord;
        
        // Compare is the field is an External reference
        System.assert(CCL_ext.getisReference() == false);
        
        // Compare if the field is an External Id
        System.assert(CCL_ext.getisExternal() == false);
        
        // Compare the field Type by first calling the function to determine the type of the Field
        CCL_ext.FieldType();
        System.assert(CCL_mapRecord.CCL_Field_Type__c == 'ID');
        
        // Populate the other fields manually and save the record
        CCL_mapRecord.CCL_Column_Name__c = 'CSVName';
        CCL_mapRecord.CCL_Required__c = true;
        
        CCL_ext.save();
        
        // Query the record and compare to the expected results
        List<CCL_DataLoadInterface_DataLoadMapping__c> CCL_queriedField = [SELECT CCL_Field_Type__c, CCL_isReferenceToExternalId__c, CCL_isUpsertId__c, CCL_ReferenceType__c,
                                                                    CCL_Required__c, CCL_SObjectExternalId__c, CCL_SObject_Field__c, CCL_Sobject_Mapped_Field__c, CCL_Sobject_Name__c
                                                                    FROM CCL_DataLoadInterface_DataLoadMapping__c
                                                                    WHERE CCL_Data_Load_Interface__c =: CCL_interfaceRecord.Id];
                                                                    
        System.assert(CCL_queriedField[0].CCL_Field_Type__c == 'ID');
        System.assert(CCL_queriedField[0].CCL_isReferenceToExternalId__c == false);
        System.assert(CCL_queriedField[0].CCL_isUpsertId__c == true);
        System.assert(CCL_queriedField[0].CCL_ReferenceType__c == null);
        System.assert(CCL_queriedField[0].CCL_Required__c == true);
        System.assert(CCL_queriedField[0].CCL_SObjectExternalId__c == null);
        System.assert(CCL_queriedField[0].CCL_SObject_Field__c == 'id');
        //System.assert(CCL_queriedField[0].CCL_Sobject_Mapped_Field__c == 'Name');
        System.assert(CCL_queriedField[0].CCL_SObject_Name__c == 'account');
         
        Test.stopTest();
    }
}