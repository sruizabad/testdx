/***************************************************************************************************************************
Apex Class Name :   CCL_DataLoadTask_ReadAttachment_Ext
Version :           1.0
Created Date :      1/02/2015
Function :          Extension class for the CCL_DataLoadTask_CSVParser VisualForce Page on the sObject CCL_DataLoadJob_DataLoadTask__c. 
                    This class starts an APEX Batch to parse, read, and import a csv document.
            
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                                 Date                                    Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois                              4/02/2015                               Updated with schedule function.
* Joris Artois                              9/09/2015                               Updated to get User Session ID for call of REST API in the Batch Class
***************************************************************************************************************************/

public without sharing class CCL_DataLoadTask_ReadAttachment_Ext{
    
    private     CCL_DataLoadJob_DataLoadTask__c CCL_currentRecord;
    private     CCL_batchCSVReader CCL_batch;
    public      Blob CCL_csvFile{get; set;}
    public      String CCL_csvString{get; set;}
    
    public CCL_DataLoadTask_ReadAttachment_Ext(ApexPages.StandardController CCL_stdCon){
        this.CCL_currentRecord = (CCL_DataLoadJob_DataLoadTask__c) CCL_stdCon.getRecord();
    }
    
    
    public PageReference CCL_readAttachment(){
        // Query to get all the variables, related to CSV parsing and scheduling, stored in CCL_DataLoadJob_DataLoadTask__c.                        
        List<CCL_DataLoadJob_DataLoadTask__c> CCL_list_DataLoadTasks = [SELECT CCL_Interface_Delimiter__c, CCL_textQualifier__c, CCL_batchSize__c,
                                        Name, CCL_Schedule__c, CCL_Scheduled_Date_Time__c, CCL_Status__c 
                                        FROM CCL_DataLoadJob_DataLoadTask__c WHERE Id =: CCL_currentRecord.Id LIMIT 1]; 
        String CCL_delimiter = CCL_list_DataLoadTasks[0].CCL_Interface_Delimiter__c;
        String CCL_textQualifier = CCL_list_DataLoadTasks[0].CCL_textQualifier__c;
        Integer CCL_batchSize = CCL_list_DataLoadTasks[0].CCL_batchSize__c.intValue();
        
        
        // Check if the Task is scheduled, otherwise it is performed 'immediately'
        if(CCL_list_DataLoadTasks[0].CCL_Schedule__c){
            Datetime dateSchedule = CCL_list_DataLoadTasks[0].CCL_Scheduled_Date_Time__c;
            String cron = dateSchedule.second() + ' ' + dateSchedule.minute() + ' ' + dateSchedule.hour() + ' ' + dateSchedule.day() + ' ' + dateSchedule.month() + ' ? ' + dateSchedule.year();
             
            /**
            * Author: Joris Artois
            * Date: 9/09/2015
            * Description: Get User session ID to give to the batch class for REST API
            **/
            String CCL_sessionId = userInfo.getSessionId();
            CCL_scheduledBatchable CCL_scheduledBatch = new CCL_scheduledBatchable(CCL_currentRecord.Id, CCL_delimiter, CCL_textQualifier, CCL_batchSize, CCL_sessionId);
            CCL_list_DataLoadTasks[0].CCL_Status__c = 'Queued';
            
            system.schedule('Scheduled Batch Task Nr ' + CCL_list_DataLoadTasks[0].Name, cron , CCL_scheduledBatch);
            
        }else {
            try{
                /**
                * Author: Joris Artois
                * Date: 9/09/2015
                * Description: Get User session ID to give to the batch class for REST API
                **/
                String CCL_sessionId = userInfo.getSessionId();
                CCL_batch = new CCL_batchCSVReader(CCL_currentRecord.Id, CCL_delimiter, CCL_textQualifier, CCL_sessionId);
                database.executeBatch(CCL_batch, CCL_batchSize);
            
                CCL_list_DataLoadTasks[0].CCL_Status__c = 'Pending';
            }
            catch(exception CCL_e){
                CCL_list_DataLoadTasks[0].CCL_Status__C = 'Failed';
            
            }
        }
        
        try {
                update CCL_list_DataLoadTasks[0];
            } catch(exception CCL_e){
                System.Debug('===Update Task Record==='+CCL_e);
            }
        PageReference test = new PageReference('/' + CCL_currentRecord.Id);
        test.setRedirect(true);
        return test;
    }
}