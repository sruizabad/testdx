/***************************************************************************************************************************
Apex Class Name :	CCL_DataLoaderRegistrationHandlerTest
Version : 			1.0
Created Date : 		13/04/2018
Function : 			Test class for CCL_DataLoaderRegistrationHandler
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Robin Wijnen								13/04/2018								Initial version
***************************************************************************************************************************/

@isTest
private with sharing class CCL_DataLoaderRegistrationHandlerTest {
    private static user CCL_u;
    private static String CCL_username;

    @testSetup
    static void CCL_createTestData() {
        String CCL_randomString = CCL_ClsTestUtility.CCL_generateRandomString(5);
        CCL_username = 'testRegistration@ccl.com';
        CCL_u = CCL_TestUserDataFactory.CCL_getUser(CCL_randomString, CCL_username, 'System Administrator', null, true);
    }

    @isTest
    private static void CCL_testCreateUser() {
        CCL_DataLoaderRegistrationHandler CCL_handler = new CCL_DataLoaderRegistrationHandler();
        Auth.UserData CCL_userData = new Auth.UserData('testId', 'testFirst', 'testLast', 'testFirst testLast', 'testuser@c-clearpartners.com', null, 'testRegistration@ccl.com', 'en_us', 'facebook', null, new Map<String, String>{'language' => 'en_US'});

        Test.startTest();

        User CCL_u = CCL_handler.createUser(null, CCL_userData);

        Test.stopTest();

        // Assertion not required
    }

    @isTest
    private static void CCL_testUpdateUser() {
        Test.startTest();

        CCL_DataLoaderRegistrationHandler CCL_handler = new CCL_DataLoaderRegistrationHandler();
        CCL_handler.updateUser(null, null, null);

        Test.stopTest();
    }
}