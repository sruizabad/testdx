/***************************************************************************************************************************
Apex Class Name :	CCL_HttpRequest
Version : 			1.0
Created Date : 		9/09/2015
Function : 			This class will create the HttpRequest to insert / delete objects that do not support DML operations via Apex
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois							  	09/09/2015      		        		Creation of class.
***************************************************************************************************************************/

public class CCL_HttpRequest {
    public String CCL_decrypt(String value, String key){
        if((value != null) && (key != null)){
            Blob blbValue = EncodingUtil.base64Decode(value);
            Blob blbKey = EncodingUtil.base64decode(key);
            Blob decryptedValue = Crypto.decryptWithManagedIV('AES128', blbKey, blbValue);
            return decryptedValue.toString();
        }
        return null;
    }
    
    public String CCL_getSessionID(string CCL_sessionId, boolean CCL_isSandbox){
        
        CCL_DataLoader__c customSetting = CCL_DataLoader__c.getOrgDefaults();
        String uri = 'https://' + (CCL_isSandbox ? 'test' : 'login') + '.salesforce.com/services/oauth2/token';
        
        String userKey = CCL_decrypt(customSetting.CCL_ClientID__c,customSetting.CCL_encrKey__c);
        String userSecret = CCL_decrypt(customSetting.CCL_ClientSecret__c, customSetting.CCL_encrKey__c);
        String userName = CCL_decrypt(customSetting.CCL_username__c, customSetting.CCL_encrKey__c);
        string password = CCL_decrypt(customSetting.CCL_password__c, customSetting.CCL_encrKey__c);
        
        String sessionID = '';
        
        string body = 	'grant_type=password'+
            			'&client_id=' + userKey +
            			'&client_secret=' + userSecret +
            			'&username=' + userName +
            			'&password=' + password;
        
       	Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setEndpoint(uri);
        req.setMethod('POST');
        req.setBody(body);
        
        HttpResponse res = h.send(req);
        
        if (res.getStatusCode() != 200) {
            System.Debug('error status code not 200. Actual: '+ res.getStatusCode() + ', statusMsg=' + res.getStatus());
            sessionID = CCL_sessionId;
        }
        else {
            System.Debug('Body' + res.getBody()); 
            Map<String, String> resMap = (Map<String, String>) JSON.deserialize(res.getBody(), Map<String, String>.class);
            System.Debug('login success. token: ' + resMap.get('access_token'));
            sessionID = resMap.get('access_token');
        }
        
        return sessionID;
    }
    
    public List<String> CCL_deleteUserTerr(List<SObject> CCL_imported, string CCL_sessionId, List<String> CCL_fieldNames){
        /**
		* Author:		Robin Wijnen <robin.wijnen@c-clearpartners.com>
		* Date:			05/02/2015
		* Description:	Returning list of error messages as there can be more then 1 deletion
		**/
		
        String CCL_errorMessage = '';
        List<String> CCL_lstErrorMessages = new List<String>();

        Http CCL_h = new Http();
        HttpRequest CCL_req = new HttpRequest();
        Integer row_number = 0;
        for ( String fieldName : CCL_fieldNames ){
            if ( fieldName == 'id'){
                break;
            } else {
                row_number += 1;
            }
        }
        
        for(SObject import_row : CCL_imported){
            CCL_req.setHeader('Authorization', 'Bearer ' + CCL_sessionId);
            String CCL_URL = URL.getSalesforceBaseUrl().toExternalForm();
            System.Debug('CCL_URL' + CCL_URL);
            CCL_req.setEndpoint(CCL_URL+'/services/data/v20.0/sobjects/UserTerritory/' + import_row.get(CCL_fieldNames[row_number]));
            CCL_req.setMethod('DELETE');
            
            HttpResponse CCL_res = CCL_h.send(CCL_req);
            System.Debug(CCL_res.getBody());
            //return CCL_res.getBody();
        	
            JSONParser parser = System.JSON.createParser(CCL_res.getBody());
            while (parser.nextToken() != null){
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'message')){
                    parser.nextToken();
                    CCL_errorMessage += import_row.get(CCL_fieldNames[row_number]) + ',' + parser.getText() + '\n';
                }
            }
            
            CCL_lstErrorMessages.add(CCL_errorMessage);
            
            //return res.getBody();
        }
        return CCL_lstErrorMessages;
    }
    
    
    public List<String> CCL_insertUserTerrCCL(List<SObject> CCL_imported, string CCL_sessionId, List<String> CCL_fieldNames){
		/**
		* Author:		Robin Wijnen <robin.wijnen@c-clearpartners.com>
		* Date:			05/02/2015
		* Description:	Returning list of error messages as there can be more then 1 inserts
		**/
		
        String CCL_errorMessage = '';
        List<String> CCL_lstErrorMessages = new List<String>();
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setHeader('Authorization', 'Bearer ' + CCL_sessionId);
        String CCL_URL = URL.getSalesforceBaseUrl().toExternalForm();
        System.Debug('CCL_URL' + CCL_URL);
        req.setEndpoint(CCL_URL + '/services/data/v20.0/sobjects/UserTerritory');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        
        System.Debug('===imported: ' + CCL_imported);
        for(SObject import_row : CCL_imported){
            System.Debug('===import_row: ' + import_row);
            String utJSON = JSON.serializePretty(import_row);
            req.setBody(utJSON);
            HttpResponse res = h.send(req);
            System.Debug(res);
            
            System.Debug('Res Body: ' + res.getBody());
          		
            JSONParser parser = System.JSON.createParser(res.getBody());
            
            while (parser.nextToken() != null){            	
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'message')){
                    parser.nextToken();
                    CCL_errorMessage += import_row.get(CCL_fieldNames[0]) + ',' + import_row.get(CCL_fieldNames[1]) + ',' + parser.getText() + '\n';
                }
            }
            
            CCL_lstErrorMessages.add(CCL_errorMessage);
            
            //return res.getBody();
        }
        return CCL_lstErrorMessages;
    }
}