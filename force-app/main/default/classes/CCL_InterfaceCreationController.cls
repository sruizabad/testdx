/***************************************************************************************************************************
Apex Class Name :   CCL_InterfaceCreationController
Version :  	1.0
Created Date :   09/10/2018
Function :   Apex Controller for lightning component: CCL_InterfaceCreation.cmp 

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                                Date                                Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops							   09/10/2018						   Initial Creation
***************************************************************************************************************************/
public class CCL_InterfaceCreationController {
    @AuraEnabled
    public static requestInfo checkRequest(String requestId){
        //Get request record
        CCL_Request__c request = [SELECT Id,CCL_Batch_Size__c,CCL_BI_Division__c,CCL_Column_Delimiter__c,CCL_New_Interface_Name__c,
                                  CCL_Number_of_Fields__c,CCL_Object__c,CCL_Operation__c,CCL_Requested_By__c,CCL_ROPU_Country_Team__c,
                                  CCL_SF_Orgs__c,CCL_Source_Interface_Name__c,CCL_Status__c,CCL_Text_Qualifier__c,CCL_Type__c 
                                  FROM CCL_Request__c WHERE Id =: requestId];
        
        
        
        //Check for interface with the same external ID
        String reqExtId = request.CCL_New_Interface_Name__c.toUpperCase().replace(' ','_');
        List<CCL_DataLoadInterface__c> extIdInt = [SELECT Id FROM CCL_DataLoadInterface__c WHERE CCL_External_ID__c = :reqExtId];
        
        if(extIdInt !=null && !extIdInt.isEmpty()){
            requestInfo reqInfo = new requestInfo();
            reqInfo.goFurther = false;
            reqInfo.message = 'The Interface Name is already in use. Please choose a different Interface Name.';
            reqInfo.check = 'InterfaceNameInUse';
            return reqInfo;
            
        } else {
            
            //Get request field records
            List<CCL_Request_Field__c> requestFields = [SELECT Id,CCL_csvColumnName__c,CCL_DataType__c,CCL_DeveloperName__c,CCL_External_Key__c,
                                                        CCL_Label__c,CCL_Reference_Type__c,CCL_RelatedTo__c,CCL_Request__c,CCL_Required__c 
                                                        FROM CCL_Request_Field__c WHERE CCL_Request__c =: requestId];
            
            
            for(CCL_Request_Field__c reqF : requestFields){
                if(reqF.CCL_RelatedTo__c != null && reqF.CCL_RelatedTo__c != '' && reqF.CCL_Reference_Type__c == 'External Id'){
                    if(reqF.CCL_External_Key__c == null || reqF.CCL_External_Key__c == '') {
                        requestInfo reqInfo = new requestInfo();
                        reqInfo.goFurther = false;
                        reqInfo.message = 'Please populate the external Key for all external Id reference fields.';
                        reqInfo.check = 'ExternalIdMissing';
                        return reqInfo;
                    } else {
                        String parentObject = reqF.CCL_RelatedTo__c;
                        List<String> extApi = new List<String>();
                        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
                        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(parentObject).getDescribe().fields.getMap();
                        for(String field: fieldMap.keySet()){
                            if(fieldMap.get(field).getDescribe().isExternalId()){
                                extapi.add(fieldMap.get(field).getDescribe().getName());
                            }
                        }
                        if(extapi !=null && !extapi.isEmpty()){
                            for(String st : extapi){
                                if(st == reqF.CCL_External_Key__c){
                                    requestInfo reqInfo = new requestInfo();
                                    reqInfo.goFurther = true;
                                    reqInfo.message = '';
                                    return reqInfo;
                                } 
                            }
                            requestInfo reqInfo = new requestInfo();
                            reqInfo.goFurther = false;
                            reqInfo.message = 'Please check if the external Id of the reference field is valid.';
                            reqInfo.check = 'noCorrectExtId';
                            return reqInfo;
                        } else {
                            requestInfo reqInfo = new requestInfo();
                            reqInfo.goFurther = false;
                            reqInfo.message = 'No external Id was detected on the reference object.';
                            reqInfo.check = 'noExtId';
                            return reqInfo;
                        }
                    }
                }
            }
            
            //Get interfaces with same object
            List<CCL_DataLoadInterface__c> interfaces = [SELECT Id,CCL_Name__c,Name,NrOfMaps__c,CCL_Selected_Object_Name__c,CCL_Type_of_Upload__c	 
                                                         FROM CCL_DataLoadInterface__c 
                                                         WHERE CCL_Selected_Object_Name__c = :request.CCL_Object__c 
                                                         AND NrOfMaps__c = :request.CCL_Number_of_Fields__c
                                                         AND CCL_Type_of_Upload__c = :request.CCL_Operation__c];
            
            //If interface exist, check for mappings
            if(interfaces !=null && !interfaces.isEmpty()){
                requestInfo reqInfo = new requestInfo();
                reqInfo.goFurther = false;
                reqInfo.message = 'One or more interfaces exist for the same object with the same number of fields. Are you sure you want to continue?';
                return reqInfo;
                
                //If no interface exist    
            } else{
                
                
                requestInfo reqInfo = new requestInfo();
                reqInfo.goFurther = true;
                reqInfo.message = '';
                return reqInfo;
                
            }
        }
    }
    
    @AuraEnabled
    public static requestInfo createInterface(String requestId){
        
        //Get request record
        CCL_Request__c request = [SELECT Id,CCL_Batch_Size__c,CCL_Description__c,CCL_BI_Division__c,CCL_Column_Delimiter__c,CCL_New_Interface_Name__c,
                                  CCL_Number_of_Fields__c,CCL_Object__c,CCL_Operation__c,CCL_Requested_By__c,CCL_ROPU_Country_Team__c,
                                  CCL_SF_Orgs__c,CCL_Source_Interface_Name__c,CCL_Status__c,CCL_Text_Qualifier__c,CCL_Type__c 
                                  FROM CCL_Request__c WHERE Id =: requestId];
        
        //Get request field records
        List<CCL_Request_Field__c> requestFields = [SELECT Id,CCL_csvColumnName__c,CCL_DataType__c,CCL_DeveloperName__c,CCL_External_Key__c,
                                                    CCL_Label__c,CCL_Reference_Type__c,CCL_RelatedTo__c,CCL_Request__c,CCL_Required__c 
                                                    FROM CCL_Request_Field__c WHERE CCL_Request__c =: requestId];
        
        //Create new Interface
        CCL_DataLoadInterface__c newInterface = new CCL_DataLoadInterface__c();
        
        newInterface.CCL_Batch_Size__c = request.CCL_Batch_Size__c;
        newInterface.CCL_Interface_Status__c = 'In Development';
        newInterface.CCL_Name__c = request.CCL_New_Interface_Name__c.toUpperCase();
        newInterface.CCL_Selected_Object_Name__c =  request.CCL_Object__c; 
        newInterface.CCL_Type_of_Upload__c = request.CCL_Operation__c;
        newInterface.CCL_Text_Qualifier__c = request.CCL_Text_Qualifier__c;
        newInterface.CCL_Delimiter__c = request.CCL_Column_Delimiter__c;
        newInterface.CCL_Description__c = request.CCL_Description__c;
        newInterface.CCL_Unlock_Record__c = false;
        newInterface.CCL_Validated__c = false;     
        newInterface.CCL_Source_Interface_Name__c = request.CCL_Source_Interface_Name__c;
        newInterface.CCL_External_ID__c = request.CCL_New_Interface_Name__c.toUpperCase().replace(' ','_');
        
        insert newInterface;
        
        //Create Mapping records
        List<CCL_DataLoadInterface_DataLoadMapping__c> dlMappings = new List<CCL_DataLoadInterface_DataLoadMapping__c>();
        
        for(CCL_Request_Field__c reqfld : requestFields){
            CCL_DataLoadInterface_DataLoadMapping__c dlMap = new CCL_DataLoadInterface_DataLoadMapping__c();
            dlMap.CCL_Data_Load_Interface__c = newInterface.Id; 
            dlMap.CCL_Column_Name__c = reqfld.CCL_csvColumnName__c; 
            dlMap.CCL_External_ID__c = newInterface.CCL_External_ID__c + '_' + reqfld.CCL_csvColumnName__c.toUpperCase().replace(' ','_'); 
            dlMap.CCL_Field_Type__c = reqfld.CCL_DataType__c; 
            dlMap.CCL_ReferenceType__c = reqfld.CCL_Reference_Type__c; 
            dlMap.CCL_Field_Type__c = reqfld.CCL_DataType__c; 
            dlMap.CCL_SObject_Field__c = reqfld.CCL_DeveloperName__c.toLowerCase(); 
            dlMap.CCL_Required__c = reqfld.CCL_Required__c;
            dlMap.CCL_SObjectExternalId__c = reqfld.CCL_External_Key__c;
            
            dlMappings.add(dlMap);
        }
        
        if(dlMappings !=null && !dlMappings.isEmpty()){
            insert(dlMappings); 
        }   
        
        //Update Request Status field
        request.CCL_Status__c = 'Completed';
        update request;
        
        requestInfo reqInfo = new requestInfo();
        reqInfo.goFurther = true;
        reqInfo.message = '';
        reqInfo.check = newInterface.Id;
        return reqInfo;
    }
    
    
    public class requestInfo{
        @AuraEnabled public Boolean goFurther {get;set;}
        @AuraEnabled public String check {get;set;}
        @AuraEnabled public String message {get;set;}
    }    
}