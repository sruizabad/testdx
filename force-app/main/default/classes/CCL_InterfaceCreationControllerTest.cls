/***************************************************************************************************************************
Apex Class Name :	CCL_InterfaceCreationControllerTest
Version : 			1.0
Created Date : 		22/10/2018
Function : 			Test class for CCL_InterfaceCreationControllerTest
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops						       22/10/2018								Initial version
***************************************************************************************************************************/
@isTest
public class CCL_InterfaceCreationControllerTest {

    @testSetup
    static void CCL_InterfaceCreationControllerTest(){
        
        String CCL_randomString = CCL_ClsTestUtility.CCL_generateRandomString(5); 
        Id CCL_RTInterfaceInsert = [SELECT Id FROM Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records'].Id;


        CCL_Request__c CCL_requestRecord = new CCL_Request__c(CCL_Batch_Size__c = 200, CCL_Column_Delimiter__c = ',',CCL_BI_Division__c = 'AH',CCL_Description__c = 'Test', CCL_New_Interface_Name__c = 'Account', 
																	CCL_Status__c = 'New', CCL_Object__c = 'Account', CCL_Type__c = 'New', CCL_Number_of_Fields__c = 1,
																	CCL_Text_Qualifier__c = '"', CCL_Operation__c = 'Insert');
        insert CCL_requestRecord;
		
		CCL_Request_Field__c CCL_mappingRecord_User = new CCL_Request_Field__c(CCL_DeveloperName__c = 'name',
																	CCL_csvColumnName__c = 'Name', CCL_Label__c = 'Name', CCL_DataType__c = 'STRING', 
																	CCL_Request__c = CCL_requestRecord.Id);
		insert CCL_mappingRecord_User;

        
        
        CCL_Request__c CCL_requestRecord5 = new CCL_Request__c(CCL_Batch_Size__c = 200, CCL_Column_Delimiter__c = ',',CCL_BI_Division__c = 'AH',CCL_Description__c = 'Test123', CCL_New_Interface_Name__c = CCL_randomString, 
																	CCL_Status__c = 'New', CCL_Object__c = 'Account', CCL_Type__c = 'New', CCL_Number_of_Fields__c = 1,
																	CCL_Text_Qualifier__c = '"', CCL_Operation__c = 'Insert');
        insert CCL_requestRecord5;
		
		CCL_Request_Field__c CCL_mappingRecord_User5 = new CCL_Request_Field__c(CCL_DeveloperName__c = 'name',
																	CCL_csvColumnName__c = 'Name', CCL_Label__c = 'Name', CCL_DataType__c = 'STRING', 
																	CCL_Request__c = CCL_requestRecord.Id);
		insert CCL_mappingRecord_User5;
        

        
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = CCL_randomString, CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'Account',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert',
																	CCL_External_Id__c = 'ACCOUNT', RecordtypeId = CCL_RTInterfaceInsert);
        insert CCL_interfaceRecord;
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_intmappingRecord_User = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'name',
																	CCL_Column_Name__c = 'Name', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'name', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id,
																	CCL_External_Id__c = CCL_randomString + 'map1');
		insert CCL_intmappingRecord_User;
        
        
        
        CCL_Request__c CCL_requestRecord2 = new CCL_Request__c(CCL_Batch_Size__c = 200, CCL_Column_Delimiter__c = ',',CCL_BI_Division__c = 'AH',CCL_Description__c = 'Test', CCL_New_Interface_Name__c = CCL_randomString, 
																	CCL_Status__c = 'New', CCL_Object__c = 'Case', CCL_Type__c = 'New',
																	CCL_Text_Qualifier__c = '"', CCL_Operation__c = 'Insert');
        insert CCL_requestRecord2;
        
        CCL_Request_Field__c CCL_mappingRecord_User2 = new CCL_Request_Field__c(CCL_DeveloperName__c = 'Subject',
																	CCL_csvColumnName__c = 'Subject', CCL_Label__c = 'Subject', CCL_DataType__c = 'STRING', 
																	CCL_Request__c = CCL_requestRecord2.Id);
		insert CCL_mappingRecord_User2;

        
        CCL_Request__c CCL_requestRecord8 = new CCL_Request__c(CCL_Batch_Size__c = 200, CCL_Column_Delimiter__c = ',',CCL_BI_Division__c = 'AH',CCL_Description__c = 'Asset', CCL_New_Interface_Name__c = CCL_randomString, 
																	CCL_Status__c = 'New', CCL_Object__c = 'Asset', CCL_Type__c = 'New',
																	CCL_Text_Qualifier__c = '"', CCL_Operation__c = 'Insert');
        insert CCL_requestRecord8;
        
        CCL_Request_Field__c CCL_mappingRecord_User8 = new CCL_Request_Field__c(CCL_DeveloperName__c = 'Account',
																	CCL_csvColumnName__c = 'Account', CCL_Label__c = 'Account', CCL_DataType__c = 'REFERENCE', CCL_RelatedTo__c = 'ACCOUNT', CCL_Reference_Type__c = 'External Id', CCL_External_Key__c = 'ext_id__c',
																	CCL_Request__c = CCL_requestRecord8.Id);
		insert CCL_mappingRecord_User8;
        
        
        
        CCL_DataLoader__c settings = CCL_DataLoader__c.getOrgDefaults();
		settings.CCL_Salesforce_Environments__c = 'Test1;Test2';     
        settings.CCL_Request_Target_Email__c = 'test@test.com';
        settings.CCL_templateURL__c = 'http://123.456';
        upsert settings CCL_DataLoader__c.Id;
    }
    
    @isTest
    static void test1(){
        
        CCL_Request__c req = [SELECT Id FROM CCL_Request__c WHERE CCL_Object__c = 'Account' LIMIT 1];
        
        Test.startTest();
        
        CCL_InterfaceCreationController.requestInfo info = CCL_InterfaceCreationController.checkRequest(req.Id);

        Test.stopTest();
    }
    
    @isTest
    static void test2(){
        
        CCL_Request__c req = [SELECT Id FROM CCL_Request__c WHERE CCL_Object__c = 'Case' LIMIT 1];
        
        Test.startTest();
        
        CCL_InterfaceCreationController.requestInfo info = CCL_InterfaceCreationController.checkRequest(req.Id);
        CCL_InterfaceCreationController.requestInfo create = CCL_InterfaceCreationController.createInterface(req.Id);

        Test.stopTest();
        
    }

    @isTest
    static void test3(){
        
        CCL_Request__c req = [SELECT Id FROM CCL_Request__c WHERE CCL_Description__c = 'Test123' LIMIT 1];
        
        Test.startTest();
        
        CCL_InterfaceCreationController.requestInfo info = CCL_InterfaceCreationController.checkRequest(req.Id);

        Test.stopTest();
    }
    
        @isTest
    static void test4(){
        
        CCL_Request__c req = [SELECT Id FROM CCL_Request__c WHERE CCL_Object__c = 'Asset' LIMIT 1];
        
        Test.startTest();
        
        CCL_InterfaceCreationController.requestInfo info = CCL_InterfaceCreationController.checkRequest(req.Id);

        Test.stopTest();
    }
}