/***************************************************************************************************************************
Apex Class Name :	CCL_InterfaceIntegrationController
Version : 			1.0
Created Date : 		04/10/2017
Function : 			This class acts as a controller class to send and receive Interfaces 
                    and Mappings between multiple SFDC orgs that have this class
                    deployed. Payload of these Interfaces and Mappings are wrapped inside
                    a WS_payload record.
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Tim Caljé							       04/10/2017								Initial version
***************************************************************************************************************************/

@RestResource(urlMapping='/v1/CCL_CSVDataLoader/*')
global with sharing class CCL_InterfaceIntegrationController{
    
     @HttpPut
    global static void ws_import(){
        
        Savepoint sp = Database.setSavepoint(); 
        RestResponse res = RestContext.response;
        
        try {
        
            System.debug(LoggingLevel.FINE,'Initiate placeholders for DML operations');
            List<CCL_DataLoadInterface__c> InterfacesForUpsert = new List<CCL_DataLoadInterface__c>();
            List<CCL_DataLoadInterface__c> InterfacesForUpdate = new List<CCL_DataLoadInterface__c>();
            List<CCL_DataLoadInterface_DataLoadMapping__c> MappingsForUpsert = new List<CCL_DataLoadInterface_DataLoadMapping__c>();
            Map<String,Id> interfacesAfterUpsert = new Map<String,Id>(); // Store Interface ID's used on Mapping records
            Map<String,Id> mappingsAfterUpsert = new Map<String,Id>(); // Store Mapping ID's used on Upsert Interface records            
            Map<String,String> externalIdMappings = new Map<String,String>(); // Store Mapping-Interface realtionships by External id's
            Map<String,String> externalUpsertMappings = new Map<String,String>();
            Map<String,String> responseMap = new Map<String,String>();
                        
            System.debug(LoggingLevel.FINE,'Deserialize inbound JSON Body request');
            System.Debug('CCL_Log ===> RextContext: ' + RestContext.request);
            String JsonString = RestContext.request.requestBody.toString();                
            System.debug('CCL_LOG: Inbound JSON String = ' + JsonString);        
            WS_Payload wsp = deserialize(JsonString,ExternalIdMappings,externalUpsertMappings);                            
            
            System.debug(LoggingLevel.FINE,'Traverse payload');
            for(WS_Payload_Record wspr : wsp.ws_records){                
                InterfacesForUpsert.add(wspr.ws_interface);               
                if (wspr.ws_interface.CCL_Type_of_Upload__c == 'Upsert') InterfacesForUpdate.add(wspr.ws_interface);
                MappingsForUpsert.addAll(wspr.ws_mappings);
            }
            
            // Upsert Interfaces
            if (InterfacesForUpsert.size() > 0) { 
                System.debug('CCL_LOG: Start Upsert operation for ' + InterfacesForUpsert.size() + ' Interfaces');
                Database.upsertResult[] urs = Database.upsert(InterfacesForUpsert,CCL_DataLoadInterface__c.fields.CCL_External_ID__c,!wsp.allowPartialSuccess);
                for (CCL_DataLoadInterface__c di : InterfacesForUpsert)
                    InterfacesAfterUpsert.put(di.CCL_External_ID__c,di.id);
                Integer i_oks = 0;
                Integer i_noks = 0;
                String i_err = '';
                for (Integer i = 0; i < urs.size(); i++){
                    if (urs[i].isSuccess()){
                        i_oks++;
                    } else {
                        i_noks++; 
                        i_err += '\n' + urs[i].getErrors()[0].getMessage() + ' for record: ' + mappingsForUpsert[i].CCL_External_ID__c;
                    }
                }
                if (i_noks > 0){
                    res.addHeader('i_r','Errors found! :' + i_err);
                } else {
                    res.addHeader('i_r','All Interfaces processed successfully!');
                }
                res.addHeader('i_oks', String.valueOf(i_oks));
                res.addHeader('i_noks', String.valueOf(i_noks));
                System.debug('CCL_LOG: End Upsert operation for ' + InterfacesForUpsert.size() + ' Interfaces.');
                System.debug('CCL_LOG: Successfull interfaces: ' + i_oks + '.');
                System.debug('CCL_LOG: Failed interfaces: ' + i_noks + '.');            
            } else {
                System.debug('CCL_LOG: 0 Interfaces to process');
                res.addHeader('i_oks', '0');
                res.addHeader('i_noks', '0');
            }    
                                

            // Upsert Mappings
            if (MappingsForUpsert.size() > 0) {
                System.debug('CCL_LOG: Start Upsert operation for ' + MappingsForUpsert.size() + ' Mappings');                
                for (CCL_DataLoadInterface_DataLoadMapping__c dm : MappingsForUpsert){
                    System.debug(LoggingLevel.FINE,'CCL_LOG: Retrieve Interface Id for Mapping with External id: ' + dm.ccl_external_id__c);
                    System.debug(LoggingLevel.FINE,'CCL_LOG: Interface External Id related to current mapping is: ' + ExternalIdMappings.get(dm.CCL_External_id__c));
                    System.debug(LoggingLevel.FINE,'CCL_LOG: Interface Id retrieved: '+ interfacesAfterUpsert.get(ExternalIdMappings.get(dm.CCL_External_id__c)));
                    dm.CCL_Data_Load_Interface__c  = interfacesAfterUpsert.get(ExternalIdMappings.get(dm.CCL_External_id__c));                    
                    System.debug(LoggingLevel.FINE,'CCL_LOG: Mapping details: ' + dm);
                }
                Database.upsertResult[] urs = Database.upsert(MappingsForUpsert,CCL_DataLoadInterface_DataLoadMapping__c.fields.CCL_External_ID__c,!wsp.allowPartialSuccess);
                for (CCL_DataLoadInterface_DataLoadMapping__c dm : mappingsForUpsert)
                    mappingsAfterUpsert.put(dm.CCL_External_ID__c,dm.id);
                Integer m_oks = 0;
                Integer m_noks = 0;
                String m_err = '';
                for (Integer i = 0; i < urs.size(); i++){
                    if (urs[i].isSuccess()){
                        m_oks++;
                    } else {
                        m_noks++;
                        m_err += '\n' + urs[i].getErrors()[0].getMessage() + ' for record: ' + mappingsForUpsert[i].CCL_External_ID__c;
                    }
                }
                if (m_noks > 0){
                    res.addHeader('m_r','Errors found! :' + m_err);
                } else {
                    res.addHeader('m_r','All Mappings processed successfully!');
                }
                res.addHeader('m_oks', String.valueOf(m_oks));
                res.addHeader('m_noks', String.valueOf(m_noks));
                System.debug('CCL_LOG: End Upsert operation for ' + MappingsForUpsert.size() + ' mappings.');
                System.debug('CCL_LOG: Successfull mappings: ' + m_oks + '.');
                System.debug('CCL_LOG: Failed mappings: ' + m_noks + '.');            
            } else {
                System.debug('CCL_LOG: 0 Mappings to process');
                res.addHeader('m_oks', '0');
                res.addHeader('m_noks', '0');
            }
            
            
            // Update usert interfaces
            if (InterfacesForUpdate.size() > 0) {
                System.debug('CCL_LOG: Start Upsert operation for ' + InterfacesForUpdate.size() + ' Upsert-Interfaces');
                for (CCL_DataLoadInterface__c di : InterfacesForUpdate)
                    di.CCL_Upsert_Field__c = mappingsAfterUpsert.get(externalUpsertMappings.get(di.CCL_External_id__c));                    
                Database.upsertResult[] urs = Database.upsert(InterfacesForUpdate,CCL_DataLoadInterface__c.fields.CCL_External_ID__c,!wsp.allowPartialSuccess);
                Integer iu_oks = 0;
                Integer iu_noks = 0;
                for (Database.upsertResult ur : urs){
                    if (ur.isSuccess()){
                        iu_oks++;
                    } else {
                        iu_noks++;
                    }
                }
                System.debug('CCL_LOG: End Upsert operation for ' + InterfacesForUpdate.size() + ' Upsert-Interfaces.');
                System.debug('CCL_LOG: Successfull Upsert-Interfaces: ' + iu_oks + '.');
                System.debug('CCL_LOG: Failed Upsert-Interfaces: ' + iu_noks + '.');            
            } else { 
                System.debug('CCL_LOG: 0 Upsert-interfaces to update');
            }
        
        
            // verify result - complete versus partial completion.
            //@Todo + create Log-result to send back
        
        } catch (Exception e) {
        
            System.debug('CCL_LOG: Error Caught during import of Interfaces & Mappings: ' + e.getMessage());                        
            Database.rollback(sp);                     
            
        }
        
        // Response requires:
        // - Data Loader Export Id
        // - Interfaces success
        // - Interface Fails
        // - Mapping Success
        // - Mapping Fails
        // list of Data Loader Export Logs:
        // - Data Loader Export id
        // - Message
        // - Record
        // - ?
        // Send response code 200 "OK"
        //return '200';

    }
       
    public static String ws_export(Id ExportId, Boolean allowPartialSuccess){   
        
        try {

            // Retrieve Export Record Details
            CCL_CSVDataLoaderExport__c exp = [SELECT Id,CCL_Status__c,CCL_Org_Target_Id__c,CCL_Source_Data__c,CCL_Named_Credential__c 
                                              FROM CCL_CSVDataLoaderExport__c                                                 
                                              WHERE Id = :ExportId][0];
              
            // Wrap payload data for export
            WS_Payload wsp = createPayload(ExportId, true);//allowPartialSuccess);
            
        
            // Build outbound HttpRequest
            Http http = new Http();
            HttpRequest req = new HttpRequest(); 
            req.setMethod('PUT');
            req.setEndpoint('callout:' + exp.CCL_Named_Credential__c + '/services/apexrest/v1/CCL_CSVDataLoader');
            req.setHeader('content-type', 'application/json');
            req.setBody(wsp.generateJSON());   
            HttpResponse response = http.send(req);                           
            
            // Verify Result
            System.Debug('CCL_LOG: RESPONSE = ' + response);
            String res = response.getBody();
            System.debug('CCL_LOG: RESPONSE BODY = ' + res);
            Integer i_oks = Integer.valueOf(response.getheader('i_oks'));            
            System.debug('CCL_LOG: RESPONSE INTERFACES OK = ' + i_oks);            
            Integer i_noks = Integer.valueOf(response.getheader('i_noks'));
            System.debug('CCL_LOG: RESPONSE INTERFACES NOT OK = ' + i_noks);            
            Integer m_oks = Integer.valueOf(response.getheader('m_oks'));
            System.debug('CCL_LOG: RESPONSE MAPPINGS OK = ' + m_oks);            
            Integer m_noks = Integer.valueOf(response.getheader('m_noks'));
            System.debug('CCL_LOG: RESPONSE MAPPINGS NOT OK = ' + m_noks);  
            String i_r = response.getheader('i_r');
            String m_r = response.getheader('m_r');
            
            // Update Export Log
            CCL_CSVDataLoaderExport__c de = [SELECT ID, 
                                            CCL_Total_Records_Succeeded__c,
                                            CCL_Total_Mappings_Succeeded__c,
                                            CCL_Total_Number_of_Records__c,
                                            CCL_Total_Number_of_Mappings__c,
                                            CCL_Status__c,
                                            CCL_Export_Log__c,
                                            CCL_Mapping_Export_Log__c
                                            FROM CCL_CSVDataLoaderExport__c  
                                            WHERE ID = :ExportId];
            de.CCL_Total_Records_Succeeded__c = i_oks;
            de.CCL_Total_Mappings_Succeeded__c = m_oks;
            de.CCL_Total_Number_of_Records__c = (i_oks + i_noks);
            de.CCL_Total_Number_of_Mappings__c= (m_oks + m_noks);
            de.CCL_Export_Log__c = i_r;
            de.CCL_Mapping_Export_Log__c = m_r;
            if (i_noks == 0 && m_noks == 0){
                de.CCL_Status__c = 'Completed';
            } else if (i_oks == 0 && m_oks == 0){
                de.CCL_Status__c = 'Failed';
            } else {
                de.CCL_Status__c = 'Partially Succeeded';
            }
            update de;
            
            // Verify Response Code from the outbound ws call to check for faillure
            if (response.getStatusCode() != 200 ) {
                
                // write failure to debug logs
                System.debug('Excecution of ws_export failed. Error code: ' + response.getStatusCode());
                
            }
        
        } catch (Exception e) {
            
            // Write Error to debug logs
            System.debug('Exception during execution of ws_export. Exception: ' + e.getMessage());
            
            // Return code 500 "Internal Error"
            return '500';
            
        }
        
        // Return code 200 "OK"
        return '200';        
    }
    
    
    /*
        Apex Method Name: createPayload
        
        Apex method used to generate a WS_Payload record as payload
        for the export functionality based upon the Id of an Export record
    */
    private static WS_Payload createPayload(Id ExportId, Boolean allowPartialSuccess){               
        
        // Retrieve list of selected interfaces
        Map<Id, CCL_DataLoadInterface__c> interfaces = new Map<Id,CCL_DataLoadInterface__c>();
        for(Data_Loader_Export_Interface__c dlei : [SELECT Data_Loader_Export__c, 
                                                           Data_Load_Interface__c 
                                                    FROM Data_Loader_Export_Interface__c 
                                                    WHERE Data_Loader_Export__c = :ExportId]){
            interfaces.put(dlei.Data_Load_Interface__c,null);
            
        }        
        
        // Retrieve interface details
        for(CCL_DataLoadInterface__c dli : [SELECT Id, 
                                                CCL_Batch_Size__c, 
                                                CCL_Delimiter__c, 
                                                CCL_External_ID__c, 
                                                CCL_Interface_Status__c, 
                                                CCL_Name__c, 
                                                CCL_Text_Qualifier__c, 
                                                CCL_Description__c, 
                                                CCL_Type_of_Upload__c, 
                                                CCL_Selected_Object_Name__c, 
                                                CCL_Unlock_Record__c, 
                                                CCL_Upsert_Field__r.CCL_External_ID__c,
                                                recordtypeId,
                                                recordtype.name 
                                            FROM CCL_DataLoadInterface__c 
                                            WHERE ID in :interfaces.keyset()]){
            interfaces.put(dli.id, dli);
        }
        
        // Retrieve mapping details
        Map<Id, List<CCL_DataLoadInterface_DataLoadMapping__c>> mappings = new Map<Id, List<CCL_DataLoadInterface_DataLoadMapping__c>>();
        for(CCL_DataLoadInterface_DataLoadMapping__c dlm : [SELECT CCL_Column_Name__c, 
                                                                   CCL_Data_Load_Interface__c, 
                                                                   CCL_Data_Load_Interface__r.CCL_External_Id__c, 
                                                                   CCL_Default_Value__c, 
                                                                   CCL_Deploy_Overide__c, 
                                                                   CCL_Default__c, 
                                                                   CCL_Field_Type__c, 
                                                                   CCL_isReferenceToExternalId__c, 
                                                                   CCL_isUpsertId__c, 
                                                                   CCL_ReferenceType__c, 
                                                                   CCL_Required__c, 
                                                                   CCL_SObject_Field__c, 
                                                                   CCL_SObject_Mapped_Field__c, 
                                                                   CCL_SObject_Name__c, 
                                                                   CCL_SObjectExternalId__c, 
                                                                   CCL_External_ID__c 
                                                              FROM CCL_DataLoadInterface_DataLoadMapping__c 
                                                              WHERE CCL_Data_Load_Interface__c in :interfaces.keyset()]){
            // Initiate new List as Value if Key doesn't exist yet
            if (mappings.get(dlm.CCL_Data_Load_Interface__c) == null) mappings.put(dlm.CCL_Data_Load_Interface__c, new List<CCL_DataLoadInterface_DataLoadMapping__c>());
            mappings.get(dlm.CCL_Data_Load_Interface__c).add(dlm);
        }
        
        
        // Create Payload
        WS_Payload wsp = new WS_payload();
        wsp.allowPartialSuccess = allowPartialSuccess;
        
        // Fill Payload
        for (Id i : interfaces.keyset()){
            // Create new WS_Payload_record
            WS_Payload_Record wspr = new WS_Payload_Record();
            wspr.ws_interface = interfaces.get(i);
            System.debug('CCL_LOG: Adding Mappings to Interface with Id: ' + i);
            System.debug('CCL_LOG: Mappins is: ' + mappings.get(i));  
            if(mappings.get(i) != null)          
                wspr.ws_mappings.addAll(mappings.get(i));
            wsp.ws_records.add(wspr);
        }
          
        // Return Payload     
        return wsp;
    }
    
    /*
        Inner Class Name: WS_Payload
        
        Inner Apex Class used for serialization and wrapping of outbound/inbound 
        Interface and Mappings data.
    */
    global class WS_Payload{
        
        // List of ws_records; 
        // Wrapper class used to facilitate Interface and Mapping data
        List<WS_Payload_Record> ws_records{get; set;}  
        
        // Boolean to specify if we allow partial success or not
        // Default value is False
        Boolean allowPartialSuccess {get; set;}   
        
        // Constructor
        public WS_Payload(){
            ws_records = new List<WS_Payload_Record>();
            allowPartialSuccess = False;
        } 
        
        // generate JSON String for callout
        public String generateJSON() {
            // Create JSON String
            String JsonString = '';      
            // Open JSON String
            JsonString += '{';           
            // Serialize Partial Success
            JsonString += '\"allowPartialSuccess\" : ' + allowPartialSuccess + ',';
            // Serialize Payload Records
            JsonString += '\"ws_records\" : [';
            // Add Serialization  per WS_Payload_Record
            for(WS_Payload_Record wspr : ws_records){
                            JsonString += wspr.generateJSON();
                JsonString += ',';
            }
            // Remove last comma
            JsonString = jSonString.removeEnd(',');
            // Close Array of Interfaces
            JsonString += ']'; 
            // Close JSON String
            JsonString += '}'; 
            // Write generated JSON to log file
            System.debug('Generated JSON: ' + JSONString);
            // Return JSON
            return JsonString;
        }

    }
    
    
    /*
        Inner Class Name: WS_Payload_Record
        
        Inner Apex Class used for serialization and wraping of a single Interface 
        record and list of related Mapping records.
    */
    global class WS_Payload_Record{                        
    
        CCL_DataLoadInterface__c ws_interface {get; set;}        
        List<CCL_DataLoadInterface_DataLoadMapping__c> ws_mappings {get; set;}
        
        // Constructor
        public WS_Payload_Record(){
            ws_mappings = new List<CCL_DataLoadInterface_DataLoadMapping__c>();
        }   
        
        // Generate JSON String for Callout
        public String generateJSON() {
            String JSonString = '';
            JsonString += '{\"ws_interface\" : ';
            JsonString += CCL_JSONGenerator.CCL_generateJSONContent(ws_interface);
            JsonString += ',';
            JsonString += '\"ws_mappings\" : [';
            for(CCL_DataLoadInterface_DataLoadMapping__c dlm : ws_mappings){
                JsonString += CCL_JSONGenerator.CCL_generateJSONMapContent(dlm);
                JsonString += ',';
            }
            // remove last comma
            JsonString = JsonString.removeEnd(',');
            // Close array of ws_records 
            JsonString += ']';
            // Close Json Body
            JsonString += '}';
            return JsonString;
        }                           
    
    }
    
    
    
    
    
    private static WS_Payload deserialize(String JsonString,Map<String,String> ExternalIdMappings, Map<String,String> externalUpsertMappings){
        WS_Payload wsp = new WS_Payload();
        WS_JsonDeserializator.JSON2WS_Payload j_wsp = WS_JsonDeserializator.parse(JsonString);
        wsp.allowPartialSuccess = j_wsp.allowPartialSuccess;        
        Map<String,Schema.RecordTypeInfo> irm = Schema.SObjectType.CCL_DataLoadInterface__c.getRecordTypeInfosByName();      
        for (WS_JsonDeserializator.Ws_record j_wsr : j_wsp.ws_records){
            WS_Payload_Record wspr = new WS_Payload_Record();
            CCL_DataLoadInterface__c di = new CCL_DataLoadInterface__c();
            di.CCL_Batch_Size__c = Integer.valueOf(j_wsr.ws_interface.batchSize);
            di.CCL_Delimiter__c = j_wsr.ws_interface.delimiter;
            di.CCL_External_ID__c = j_wsr.ws_interface.externalId;
            di.CCL_Interface_Status__c = 'In Development'; //j_wsr.ws_interface.interfaceStatus;
            di.CCL_Name__c = j_wsr.ws_interface.name;
            di.CCL_Text_Qualifier__c = j_wsr.ws_interface.textQualifier;
            di.CCL_Description__c = j_wsr.ws_interface.description;
            di.CCL_Type_of_Upload__c = j_wsr.ws_interface.typeOfUpload;
            di.CCL_Selected_Object_Name__c = j_wsr.ws_interface.selectedObjectName;
            //di.CCL_Upsert_Field__c = j_wsr.ws_interface.upsertField;
            if (di.ccl_type_of_upload__c == 'Upsert'){
                di.recordtypeid = irm.get('Upsert Records').getRecordTypeId();
            } else if (di.ccl_type_of_upload__c == 'Insert'){
                di.recordtypeid = irm.get('Insert Records').getRecordTypeId();            
            } else if (di.ccl_type_of_upload__c == 'Update'){
                di.recordtypeid = irm.get('Update Records').getRecordTypeId();            
            } else if (di.ccl_type_of_upload__c == 'Delete'){
                di.recordtypeid = irm.get('Delete Records').getRecordTypeId();            
            } else {
                System.debug('CCL_LOG: FATAL EXCEPTION - UNKNOWN TYPE OF UPLOAD: ' + di.ccl_type_of_upload__c);
            }
            externalUpsertMappings.put(di.CCL_External_ID__c,j_wsr.ws_interface.upsertField);
            di.CCL_Unlock_Record__c = j_wsr.ws_interface.unlockRecord;        
            wspr.ws_interface = di;
            for (WS_JsonDeserializator.Ws_mapping j_m : j_wsr.ws_mappings){
                CCL_DataLoadInterface_DataLoadMapping__c dm = new CCL_DataLoadInterface_DataLoadMapping__c();
                //dm.CCL_Data_Load_Interface__c = di.CCL_External_ID__c;
                dm.CCL_Column_Name__c = j_m.columnName;
                dm.CCL_Default_Value__c = j_m.defaultValue;
                dm.CCL_Deploy_Overide__c = true; //j_m.deployOveride;
                dm.CCL_Default__c = j_m.mapDefault;
                dm.CCL_Field_Type__c = j_m.fieldType;
                dm.CCL_isReferenceToExternalId__c = j_m.isReferenceToExternalId;
                dm.CCL_isUpsertId__c = j_m.isUpsertId;
                dm.CCL_ReferenceType__c = j_m.referenceType;
                dm.CCL_Required__c = j_m.required;
                dm.CCL_SObject_Field__c = j_m.sObjectField;
                dm.CCL_SObject_Mapped_Field__c = j_m.sObjectMappedField;
                dm.CCL_SObject_Name__c = j_m.sObjectName;
                dm.CCL_SObjectExternalId__c = j_m.sObjectExternalId;
                dm.CCL_External_ID__c = j_m.externalId;
                //dm.CCL_Data_Load_Interface__c = di.CCL_External_ID__c;
                ExternalIdMappings.put(dm.CCL_External_ID__c,di.CCL_External_ID__c); 
                wspr.ws_mappings.add(dm);
            }
            wsp.ws_records.add(wspr);
        }
        return wsp;
    }

}