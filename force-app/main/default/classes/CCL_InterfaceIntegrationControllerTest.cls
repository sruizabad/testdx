/***************************************************************************************************************************
Apex Class Name :	CCL_InterfaceIntegrationControllerTest
Version : 			1.0
Created Date : 		13/04/2018
Function : 			Test class for CCL_InterfaceIntegrationController
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops						       13/04/2018							Initial version
***************************************************************************************************************************/

@isTest
public class CCL_InterfaceIntegrationControllerTest {
    @testSetup static void CCL_createTestData() {
        Id CCL_RTInterfaceInsert = [SELECT Id FROM Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records'].Id;
        String CCL_randomString = CCL_ClsTestUtility.CCL_generateRandomString(5);   

        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = CCL_randomString, CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'Account',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert',
																	CCL_External_Id__c = CCL_randomString, RecordtypeId = CCL_RTInterfaceInsert);
        insert CCL_interfaceRecord;
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_User = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'name',
																	CCL_Column_Name__c = 'Name', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'name', CCL_SObject_Name__c = 'account', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id,
																	CCL_External_Id__c = CCL_randomString + 'map1');
		insert CCL_mappingRecord_User;

		CCL_Org_Connection__c CCL_orgCon = new CCL_Org_Connection__c(Name = 'Test', CCL_Named_Credential__c = 'TestCredential', CCL_Org_Active__c = true, 
																	CCL_Org_Id__c = UserInfo.getOrganizationId(), CCL_Org_Type__c = 'Developer Edition');

		insert CCL_orgCon;

        for(Integer i = 0; i < 2; i++) {
            CCL_CSVDataLoaderExport__c CCL_export = new CCL_CSVDataLoaderExport__c(CCL_Org_Connection__c = CCL_orgCon.Id, CCL_Source_Data__c = 'Interfaces',
																				CCL_Status__c = 'Pending');
            insert CCL_export;
                
            CCL_interfaceRecord.CCL_Interface_Status__c = 'Active';
            update CCL_interfaceRecord;
            
            Data_Loader_Export_Interface__c CCL_expInterface1 = new Data_Loader_Export_Interface__c(Data_Load_Interface__c = CCL_interfaceRecord.Id, Data_Loader_Export__c = CCL_export.Id);
            
            insert CCL_expInterface1;
        }
	}

    static testMethod void CCL_testCalloutImportInsert(){ 
        String CCL_randomString = CCL_ClsTestUtility.CCL_generateRandomString(5);     
        String CCL_body = '{"allowPartialSuccess" : true,"ws_records" : [{"ws_interface" : { "batchSize" : "200", "delimiter" : ",", "externalId" : "' + CCL_randomString + '", "interfaceStatus" : "Active", "name" : "TestClassMappings", "textQualifier" : "", "description" : "TestClassMappings", "typeOfUpload" : "Insert", "selectedObjectName" : "Account", "upsertField" : null, "unlockRecord" : false },"ws_mappings" : [{ "columnName" : "name", "defaultValue" : null, "deployOveride" : false, "mapDefault" : false, "fieldType" : "STRING", "isReferenceToExternalId" : false, "isUpsertId" : false, "referenceType" : null, "required" : false, "sObjectField" : "name", "sObjectMappedField" : "name", "sObjectName" : "Account", "sObjectExternalId" : null, "externalId" : "name", "parentInterfaceExternalId" : "TestClassMappings" }]}]}';  
        Test.setMock(HttpCalloutMock.class, new CCL_MockHttpResponse(200, 'OK', '{"Body message"}', new Map<String, String>{'Content-Type' => 'text/plain'}));

        RestRequest CCL_request = new RestRequest();
        RestResponse CCL_response = new RestResponse();
        CCL_request.requestUri = URL.getSalesforceBaseUrl().toexternalForm() + '/services/apexrest/v1/CCL_CSVDataLoader';
        CCL_request.addHeader('Content-Type', 'text-plain');
        CCL_request.httpMethod = 'PUT';
        CCL_request.requestBody = Blob.valueOf(CCL_body);

        RestContext.request = CCL_request;
        RestContext.response = CCL_response;

        Test.startTest();
        
        CCL_InterfaceIntegrationController.ws_import();    
        
        Test.stopTest();

        System.assert([SELECT Id FROM CCL_DataLoadInterface__c WHERE CCL_External_ID__c =: CCL_randomString] != null);
        
    }

    static testMethod void CCL_testCalloutImportUpsert(){ 
        String CCL_randomString = CCL_ClsTestUtility.CCL_generateRandomString(5);     
        String CCL_body = '{"allowPartialSuccess" : true,"ws_records" : [{"ws_interface" : { "batchSize" : "200", "delimiter" : ",", "externalId" : "' + CCL_randomString + '", "interfaceStatus" : "Active", "name" : "TestClassMappings", "textQualifier" : "", "description" : "TestClassMappings", "typeOfUpload" : "Upsert", "selectedObjectName" : "Account", "upsertField" : null, "unlockRecord" : false },"ws_mappings" : [{ "columnName" : "name", "defaultValue" : null, "deployOveride" : false, "mapDefault" : false, "fieldType" : "STRING", "isReferenceToExternalId" : false, "isUpsertId" : false, "referenceType" : null, "required" : false, "sObjectField" : "name", "sObjectMappedField" : "name", "sObjectName" : "Account", "sObjectExternalId" : null, "externalId" : "name", "parentInterfaceExternalId" : "TestClassMappings" }]}]}';  
        Test.setMock(HttpCalloutMock.class, new CCL_MockHttpResponse(200, 'OK', '{"Body message"}', new Map<String, String>{'Content-Type' => 'text/plain'}));

        RestRequest CCL_request = new RestRequest();
        RestResponse CCL_response = new RestResponse();
        CCL_request.requestUri = URL.getSalesforceBaseUrl().toexternalForm() + '/services/apexrest/v1/CCL_CSVDataLoader';
        CCL_request.addHeader('Content-Type', 'text-plain');
        CCL_request.httpMethod = 'PUT';
        CCL_request.requestBody = Blob.valueOf(CCL_body);

        RestContext.request = CCL_request;
        RestContext.response = CCL_response;

        Test.startTest();
        
        CCL_InterfaceIntegrationController.ws_import();    
        
        Test.stopTest();

        System.assert([SELECT Id FROM CCL_DataLoadInterface__c WHERE CCL_External_ID__c =: CCL_randomString] != null);
        
    }

    static testMethod void CCL_testCalloutExport(){	
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c WHERE CCL_Status__c = 'Pending' LIMIT 1];

        Test.startTest();

		String CCL_orgIdResponse = '{"totalSize":1,"done":true,"records":[{"attributes":{"type":"Organization","url":"/services/data/v37.0/sobjects/Organization/'+ UserInfo.getOrganizationId() + '"},"Id":"' + UserInfo.getOrganizationId() + '"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');
        CCL_responseHeaders.put('i_oks', '1');
        CCL_responseHeaders.put('i_noks', '0');
        CCL_responseHeaders.put('m_oks', '1');
        CCL_responseHeaders.put('m_noks', '0');
        CCL_responseHeaders.put('i_r', 'Ok');
        CCL_responseHeaders.put('m_r', 'Ok');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_orgIdResponse, CCL_responseHeaders));

		String CCL_response = CCL_InterfaceIntegrationController.ws_export(CCL_export.Id, true);
		
		Test.stopTest();

		CCL_CSVDataLoaderExport__c CCL_assertExport = [SELECT Id, CCL_Status__c FROm CCL_CSVDataLoaderExport__c LIMIT 1];
		System.assertEquals('Completed', CCL_assertExport.CCL_Status__c);
    }

    static testMethod void CCL_testCalloutExportFailed(){	
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c WHERE CCL_Status__c = 'Pending' LIMIT 1];

        Test.startTest();

		String CCL_orgIdResponse = '{"totalSize":1,"done":true,"records":[{"attributes":{"type":"Organization","url":"/services/data/v37.0/sobjects/Organization/'+ UserInfo.getOrganizationId() + '"},"Id":"' + UserInfo.getOrganizationId() + '"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');
        CCL_responseHeaders.put('i_oks', '0');
        CCL_responseHeaders.put('i_noks', '1');
        CCL_responseHeaders.put('m_oks', '0');
        CCL_responseHeaders.put('m_noks', '1');
        CCL_responseHeaders.put('i_r', 'Ok');
        CCL_responseHeaders.put('m_r', 'Ok');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_orgIdResponse, CCL_responseHeaders));

		String CCL_response = CCL_InterfaceIntegrationController.ws_export(CCL_export.Id, true);
		
		Test.stopTest();

		CCL_CSVDataLoaderExport__c CCL_assertExport = [SELECT Id, CCL_Status__c FROm CCL_CSVDataLoaderExport__c LIMIT 1];
		System.assertEquals('Failed', CCL_assertExport.CCL_Status__c);
    }
}