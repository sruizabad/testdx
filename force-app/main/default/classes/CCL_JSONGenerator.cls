public class CCL_JSONGenerator {
    public static String CCL_generateJSONContent(CCL_DataLoadInterface__c curRec) {
        // Create a JSONGenerator object.
        // Pass true to the constructor for pretty print formatting.
        JSONGenerator CCL_gen = JSON.createGenerator(true);
        
        CCL_gen.writeStartObject();
        //CCL_gen.writeStringField('RecordType', curRec.RecordType.Name); 
        CCL_gen.writeStringField('batchSize', String.valueOf(curRec.CCL_Batch_Size__c));        
        CCL_gen.writeStringField('delimiter', curRec.CCL_Delimiter__c);
        CCL_gen.writeStringField('externalId', curRec.CCL_External_ID__c);
        CCL_gen.writeStringField('interfaceStatus', curRec.CCL_Interface_Status__c);
        CCL_gen.writeStringField('name', curRec.CCL_Name__c);
        CCL_gen.writeStringField('textQualifier', curRec.CCL_Text_Qualifier__c);
        if(curRec.CCL_Description__c != null) CCL_gen.writeStringField('description', curRec.CCL_Description__c); else 
            CCL_gen.writeNullField('description');
        if(curRec.CCL_Type_of_Upload__c != null) CCL_gen.writeStringField('typeOfUpload', curRec.CCL_Type_of_Upload__c); else 
            CCL_gen.writeNullField('typeOfUpload');
        if(curRec.CCL_Selected_Object_Name__c != null) CCL_gen.writeStringField('selectedObjectName', curRec.CCL_Selected_Object_Name__c); else
            CCL_gen.writeNullField('selectedObjectName');
        if(curRec.CCL_Upsert_Field__c != null) CCL_gen.writeStringField('upsertField', curRec.CCL_Upsert_Field__r.CCL_External_Id__c); else
            CCL_gen.writeNullField('upsertField');    
        CCL_gen.writeBooleanField('unlockRecord', curRec.CCL_Unlock_Record__c);
            
        CCL_gen.writeEndObject();
        
        return CCL_gen.getAsString();
    }
    
    public static String CCL_generateRequestJSONContent(CCL_Request__c curRec) {
        // Create a JSONGenerator object.
        // Pass true to the constructor for pretty print formatting.
        JSONGenerator CCL_gen = JSON.createGenerator(true);
        
        CCL_gen.writeStartObject();
        CCL_gen.writeStringField('batchSize', String.valueOf(curRec.CCL_Batch_Size__c));       
        CCL_gen.writeStringField('division', curRec.CCL_BI_Division__c);
        CCL_gen.writeStringField('delimiter', curRec.CCL_Column_Delimiter__c);
        if(curRec.CCL_Description__c != null) CCL_gen.writeStringField('description', curRec.CCL_Description__c); else 
            CCL_gen.writeNullField('description');
        if(curRec.CCL_New_Interface_Name__c != null) CCL_gen.writeStringField('newInterfaceName', curRec.CCL_New_Interface_Name__c); else 
            CCL_gen.writeNullField('newInterfaceName');
        CCL_gen.writeStringField('numberOfFields', String.valueOf(curRec.CCL_Number_of_Fields__c));      
        if(curRec.CCL_Object__c != null) CCL_gen.writeStringField('selectedObject', curRec.CCL_Object__c); else
            CCL_gen.writeNullField('selectedObject');
        if(curRec.CCL_Operation__c != null) CCL_gen.writeStringField('operation', curRec.CCL_Operation__c); else 
            CCL_gen.writeNullField('operation');
        if(curRec.CCL_Requested_By__c != null) CCL_gen.writeStringField('requestedBy', curRec.CCL_Requested_By__c); else 
            CCL_gen.writeNullField('requestedBy');
		if(curRec.CCL_ROPU_Country_Team__c != null) CCL_gen.writeStringField('countryTeam', curRec.CCL_ROPU_Country_Team__c); else 
            CCL_gen.writeNullField('countryTeam');
        if(curRec.CCL_SF_Orgs__c != null) CCL_gen.writeStringField('sfOrg', curRec.CCL_SF_Orgs__c); else 
            CCL_gen.writeNullField('sfOrg');
        if(curRec.CCL_Source_Interface_Name__c != null) CCL_gen.writeStringField('sourceInterfaceName', curRec.CCL_Source_Interface_Name__c); else 
            CCL_gen.writeNullField('sourceInterfaceName');
        if(curRec.CCL_Status__c != null) CCL_gen.writeStringField('status', curRec.CCL_Status__c); else 
            CCL_gen.writeNullField('status');
        CCL_gen.writeStringField('textQualifier', curRec.CCL_Text_Qualifier__c);
        CCL_gen.writeStringField('requestType', curRec.CCL_Type__c);

        CCL_gen.writeEndObject();
        
        return CCL_gen.getAsString();
    }
    
    public static String CCL_generateJSONMapContent(CCL_DataLoadInterface_DataLoadMapping__c curRec) {
        // Create a JSONGenerator object.
        // Pass true to the constructor for pretty print formatting.
        JSONGenerator CCL_gen = JSON.createGenerator(true);
        System.Debug('===> curRec: ' + curRec);
        
        CCL_gen.writeStartObject();
        if(curRec.CCL_Column_Name__c != null) CCL_gen.writeStringField('columnName', curRec.CCL_Column_Name__c);
        else CCL_gen.writeNullField('columnName');
        
        if(curRec.CCL_Default_Value__c != null) CCL_gen.writeStringField('defaultValue', curRec.CCL_Default_Value__c);
        else CCL_gen.writeNullField('defaultValue');
        CCL_gen.writeBooleanField('deployOveride', curRec.CCL_Deploy_Overide__c);
        
        if(curRec.CCL_Default__c != null) CCL_gen.writeBooleanField('mapDefault', curRec.CCL_Default__c);
        else CCL_gen.writeNullField('mapDefault');
        
        if(curRec.CCL_Field_Type__c != null) CCL_gen.writeStringField('fieldType', curRec.CCL_Field_Type__c);
        else CCL_gen.writeNullField('fieldType');
        
        if(curRec.CCL_isReferenceToExternalId__c != null) CCL_gen.writeBooleanField('isReferenceToExternalId', curRec.CCL_isReferenceToExternalId__c);
        else CCL_gen.writeNullField('isReferenceToExternalId');
        
        if(curRec.CCL_isUpsertId__c != null) CCL_gen.writeBooleanField('isUpsertId', curRec.CCL_isUpsertId__c);
        else CCL_gen.writeNullField('isUpsertId');
        
        if(curRec.CCL_ReferenceType__c != null) CCL_gen.writeStringField('referenceType', curRec.CCL_ReferenceType__c);
        else CCL_gen.writeNullField('referenceType');
        
        if(curRec.CCL_Required__c != null) CCL_gen.writeBooleanField('required', curRec.CCL_Required__c);
        else CCL_gen.writeNullField('required');
        
        if(curRec.CCL_SObject_Field__c != null) CCL_gen.writeStringField('sObjectField', curRec.CCL_SObject_Field__c);
        else CCL_gen.writeNullField('sObjectField');
        
        if(curRec.CCL_SObject_Mapped_Field__c != null) CCL_gen.writeStringField('sObjectMappedField', curRec.CCL_SObject_Mapped_Field__c);
        else CCL_gen.writeNullField('sObjectMappedField');
        
        if(curRec.CCL_SObject_Name__c != null) CCL_gen.writeStringField('sObjectName', curRec.CCL_SObject_Name__c);
        else CCL_gen.writeNullField('sObjectName');
        
        if(curRec.CCL_SObjectExternalId__c != null) CCL_gen.writeStringField('sObjectExternalId', curRec.CCL_SObjectExternalId__c);
        else CCL_gen.writeNullField('sObjectExternalId');
        
        if(curRec.CCL_External_Id__c != null) CCL_gen.writeStringField('externalId', curRec.CCL_External_Id__c);
        else CCL_gen.writeNullField('externalId');        
        
        if(curRec.CCL_Data_Load_Interface__r.CCL_External_Id__c != null) CCL_gen.writeStringField('parentInterfaceExternalId', curRec.CCL_Data_Load_Interface__r.CCL_External_Id__c);
        else CCL_gen.writeNullField('parentInterfaceExternalId');
        
        
        CCL_gen.writeEndObject();
        
        return CCL_gen.getAsString();
    }
    
    public static String CCL_generateRequestJSONMapContent(CCL_Request_Field__c curRec) {
        // Create a JSONGenerator object.
        // Pass true to the constructor for pretty print formatting.
        JSONGenerator CCL_gen = JSON.createGenerator(true);
        System.Debug('===> curRec: ' + curRec);
        
        CCL_gen.writeStartObject();
        if(curRec.CCL_csvColumnName__c != null) CCL_gen.writeStringField('columnName', curRec.CCL_csvColumnName__c);
        else CCL_gen.writeNullField('columnName');
        
        if(curRec.CCL_DataType__c != null) CCL_gen.writeStringField('dataType', curRec.CCL_DataType__c);
        else CCL_gen.writeNullField('dataType');
        
        if(curRec.CCL_DeveloperName__c != null) CCL_gen.writeStringField('developerName', curRec.CCL_DeveloperName__c);
        else CCL_gen.writeNullField('developerName');
        
        if(curRec.CCL_External_Key__c != null) CCL_gen.writeStringField('externalKey', curRec.CCL_External_Key__c);
        else CCL_gen.writeNullField('externalKey');
        
        if(curRec.CCL_Label__c != null) CCL_gen.writeStringField('label', curRec.CCL_Label__c);
        else CCL_gen.writeNullField('label');
        
        if(curRec.CCL_Reference_Type__c != null) CCL_gen.writeStringField('referenceType', curRec.CCL_Reference_Type__c);
        else CCL_gen.writeNullField('referenceType');
        
        if(curRec.CCL_RelatedTo__c != null) CCL_gen.writeStringField('relatedTo', curRec.CCL_RelatedTo__c);
        else CCL_gen.writeNullField('relatedTo');
        
        if(curRec.CCL_Required__c != null) CCL_gen.writeBooleanField('required', curRec.CCL_Required__c);
        else CCL_gen.writeNullField('required');
        
        //if(curRec.CCL_Request__r.CCL_External_Id__c != null) CCL_gen.writeStringField('requestInterfaceExternalId', curRec.CCL_Request__r.CCL_External_Id__c);
        //else CCL_gen.writeNullField('requestInterfaceExternalId');
        
        CCL_gen.writeEndObject();
        
        return CCL_gen.getAsString();
    }
    
}