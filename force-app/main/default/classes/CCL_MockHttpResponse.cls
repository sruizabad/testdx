/***************************************************************************************************************************
Apex Class Name :	CCL_MockHttpResponse
Version : 			1.0
Created Date : 		13/04/2018
Function : 			Test class generating Mock Responses
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops						       13/04/2018							Initial version
***************************************************************************************************************************/

@isTest
global class CCL_MockHttpResponse implements HttpCalloutMock {
    private Integer CCL_code;
    private String CCL_status;
    private String CCL_body;
    private Map<String, String> CCL_responseHeaders;

    global CCL_MockHttpResponse(Integer CCL_code, String CCL_status, String CCL_body, Map<String, String> CCL_responseHeaders) {
        this.CCL_code = CCL_code;
        this.CCL_status = CCL_status;
        this.CCL_body = CCL_body;
        this.CCL_responseHeaders = CCL_responseHeaders;
    }

    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse CCL_res = new HttpResponse();

        for(String CCL_key : this.CCL_responseHeaders.keySet()) {
            CCL_res.setHeader(CCL_key, CCL_responseHeaders.get(CCL_key));
        }

        CCL_res.setBody(this.CCL_body);
        CCL_res.setStatusCode(this.CCL_code);
        CCL_res.setStatus(this.CCL_status);

        return CCL_res;
    }
}