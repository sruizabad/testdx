/***************************************************************************************************************************
Apex Class Name :   CCL_RequestController
Version :  	1.0
Created Date :   09/08/2018
Function :   Apex Controller for lightning component: CCL_Request.cmp 

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                                Date                                Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops							   09/08/2018						   Initial Creation
***************************************************************************************************************************/


public class CCL_RequestController {  
    
    @AuraEnabled
    public static List<String> getOrgs() {
        String orgString = CCL_DataLoader__c.getOrgDefaults().CCL_Salesforce_Environments__c;
        List <String> allOrgs = orgString.split(';');
        allOrgs.sort();
        return allOrgs;
    }
    
    @AuraEnabled
    public static String getOrg() {
        Organization O = [SELECT Name FROM Organization][0];
        return O.Name;
    }
    
    @AuraEnabled
    public static List <String> getselectOptions(sObject objObject, string fld) {
        List <String> allOpts = new list <String> ();
        for (Schema.PicklistEntry a: objObject.getSObjectType().getDescribe().fields.getMap().get(fld).getDescribe().getPickListValues()) {
            allOpts.add(a.getValue());
        }
        allOpts.sort();
        return allOpts;
    }
    
    @AuraEnabled
    public static String getUserDetails() {
        return UserInfo.getName();
    }
    
    @AuraEnabled
    public static List<objectInfo> getObjectNames() {
        Map<String,SObjectType> objectsMap = Schema.getGlobalDescribe();
        List<objectInfo> CCL_list_selectsObjectOptions = new List<objectInfo>();
        Integer picklistLimit = 0;                
        for (AggregateResult aggr : [SELECT SObjectType From ObjectPermissions Group By SObjectType]){
            try{ 
                DescribeSObjectResult d = objectsMap.get((String)aggr.get('SObjectType')).getDescribe();   
                objectInfo returnobjectInfo = new objectInfo();
                returnobjectInfo.apiName = d.getName();
                returnobjectInfo.label = d.getLabel();
                CCL_list_selectsObjectOptions.add(returnobjectInfo);
                picklistLimit++;
                if (picklistLimit == 999) break; //Safeguard picklist limit
            } catch (Exception e) {
                // Object not found
            }
        }
        CCL_list_selectsObjectOptions.sort();
        return CCL_list_selectsObjectOptions;    
    }
    
    @AuraEnabled
    public static List<fieldInfo> getfieldNames(String objectName) { 
        if(objectName != null) { 
            Schema.DescribeSObjectResult obj = Schema.getGlobalDescribe().get(objectName).getDescribe();
            Map<String,SObjectField> fieldsMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
            List<fieldInfo> CCL_list_sObjectFields = new List<fieldInfo>();
            Integer i = 1;
            for (String key : fieldsmap.keyset()){
                try{ 
                    Schema.DescribeFieldResult fld = obj.fields.getMap().get(key).getDescribe();   
                    fieldInfo returnfieldInfo = new fieldInfo();
                    returnfieldInfo.id = String.valueOf(i);
                    returnfieldInfo.apiName = fld.getName();
                    returnfieldInfo.label = fld.getLabel();
                    returnfieldInfo.dataType = string.valueOf(fld.getType());
                    returnfieldInfo.relatedTo = string.valueOf(fld.getReferenceTo()).replace('(','').replace(')','');
                    if (returnfieldInfo.dataType == 'REFERENCE'){
                        returnfieldInfo.refType = 'Salesforce Id';
                        returnfieldInfo.extId = true;
                        returnfieldInfo.actionDisabled = false;
                    }  else {
                        returnfieldInfo.refType = '';
                        returnfieldInfo.extId = false;
                        returnfieldInfo.actionDisabled = true;
                    }
                    
                    
                    CCL_list_sObjectFields.add(returnfieldInfo);
                    i++;
                } catch (Exception e) {
                }
            }
            CCL_list_sObjectFields.sort();
            return CCL_list_sObjectFields;    
        } else {
            return null;
        }
    }   
    
    @AuraEnabled
    public static List<fieldInfo> inlineEdit(String masterFields, String changedFields){
        List<fieldInfo> mFields = (List<fieldInfo>)JSON.deserialize(masterFields,List<fieldInfo>.class);
        List<fieldInfo> cFields = (List<fieldInfo>)JSON.deserialize(changedFields,List<fieldInfo>.class);
        
        for(fieldInfo mfi : mFields){
            for(fieldInfo cfi : cFields){
                if(mfi.id == cfi.id){
                    if(cfi.required != null) mfi.required = cfi.required;
                    if(cfi.csvColumnName != null) mfi.csvColumnName = cfi.csvColumnName;
                    if(cfi.extKey != null) mfi.extKey = cfi.extKey;
                }
            }
        }
        return mFields;
    }
    
    @AuraEnabled
    public static Boolean checkData(String fieldsString){
        List<fieldInfo> fieldsToCheck = (List<fieldInfo>)JSON.deserialize(fieldsString,List<fieldInfo>.class);
        Set<String> csvColumnNames = new Set<String>();
        
        for(fieldInfo fdi : fieldsToCheck){
            if(fdi.csvColumnName == null || fdi.csvColumnName == '' || csvColumnNames.contains(fdi.csvColumnName)){
                return false;
            } else {
                csvColumnNames.add(fdi.csvColumnName);
            }
        }
        return true;
    }
    
    
    @AuraEnabled
    public static void addRequestFields(String requestId, String fieldsString){
        List<fieldInfo> fieldsToAdd = (List<fieldInfo>)JSON.deserialize(fieldsString,List<fieldInfo>.class);
        List<CCL_Request_Field__c> requestFields = new list<CCL_Request_Field__c>();
        
        for(fieldInfo fdi : fieldsToAdd){
            CCL_Request_Field__c requestField = new CCL_Request_Field__c();
            requestField.CCL_DataType__c = fdi.dataType;
            requestField.CCL_DeveloperName__c = fdi.apiName;
            requestField.CCL_Label__c = fdi.label;
            requestField.CCL_RelatedTo__c = fdi.relatedTo;
            if(fdi.required == true) requestField.CCL_Required__c = true;
            if(fdi.required == false) requestField.CCL_Required__c = false;
            requestField.CCL_csvColumnName__c = fdi.csvColumnName;
            requestField.CCL_Request__c = requestId;
            requestField.CCL_Reference_Type__c = fdi.refType;
            requestField.CCL_External_Key__c = fdi.extKey;
            requestFields.add(requestField);
        }
        insert requestFields;        
    }
    
    @AuraEnabled
    public static void sendEmail(String requestId){
        
        //Create email
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        
        //get emailaddress
        String emailaddress = CCL_DataLoader__c.getOrgDefaults().CCL_Request_Target_Email__c;
        
        //Set emailaddress
        List<String> sendTo = new List<String>();
        sendTo.add(emailaddress);
        message.setToAddresses(sendTo);
        
        //Create Body
        CCL_Request__c request = [SELECT Id, CCL_Batch_Size__c, CCL_BI_Division__c, CCL_Column_Delimiter__c, CCL_SF_Orgs__c, CCL_Number_of_Fields__c, CCL_Object__c, CCL_Operation__c, CCL_ROPU_Country_Team__c, CCL_Text_Qualifier__c, Name, OwnerId  FROM CCL_Request__c WHERE Id =: requestId];
        User requestedBy = [SELECT Name, Email FROM User WHERE Id =: request.OwnerId];
        
        //Set Body
        message.setSubject('CSV DataLoader: New Interface Request: ' + request.Name);
        message.setHtmlBody('Request:<b> ' + request.Name +' </b>has been created.<p>'+
                            '<p> User Requested: ' + requestedBy.Name + ' (' + requestedBy.Email + ') </p>' +
                            '<p> ROPU Country Team: ' + request.CCL_ROPU_Country_Team__c + '</p>' +
                            '<p> BI Division: ' + request.CCL_BI_Division__c + '</p>' +
                            '<p> SF Org: ' + request.CCL_SF_Orgs__c + '</p>' +
                            '<p> Object: ' + request.CCL_Object__c + '</p>' +
                            '<p> Operation: ' + request.CCL_Operation__c + '</p>' +
                            '<p> Batch Size: ' + request.CCL_Batch_Size__c + '</p>' +
                            '<p> Column Delimiter: ' + request.CCL_Column_Delimiter__c + '</p>' +
                            '<p> Text Qualifier: ' + request.CCL_Text_Qualifier__c + '</p>' +
                            '<p> Number of fields: ' + request.CCL_Number_of_Fields__c + '</p>');
        
        
        //Create CSV File        
        List<CCL_Request_Field__c> requestFields = [SELECT Id, CCL_Label__c, CCL_DeveloperName__c, CCL_External_Key__c, CCL_Reference_Type__c, CCL_DataType__c, CCL_RelatedTo__c, CCL_Required__c, CCL_csvColumnName__c FROM CCL_Request_Field__c WHERE CCL_Request__c =: requestId];
        String csvContent = 'Field Label,API Name,CSV Column Name,Data Type,Required,Related To,Reference Type,External Key\n';
        
        for(CCL_Request_Field__c rfi : requestFields){    
            if(rfi.CCL_RelatedTo__c == null || rfi.CCL_RelatedTo__c == 'null') rfi.CCL_RelatedTo__c = '';
            if(rfi.CCL_csvColumnName__c == null || rfi.CCL_csvColumnName__c == 'null') rfi.CCL_csvColumnName__c = '';
            if(rfi.CCL_Reference_Type__c == null || rfi.CCL_Reference_Type__c == 'null') rfi.CCL_Reference_Type__c = '';
            if(rfi.CCL_External_Key__c == null || rfi.CCL_External_Key__c == 'null') rfi.CCL_External_Key__c = '';
            csvContent = csvContent + rfi.CCL_Label__c + ',' 
                + rfi.CCL_DeveloperName__c + ',' 
                + rfi.CCL_csvColumnName__c + ',' 
                + rfi.CCL_DataType__c + ',' 
                + rfi.CCL_Required__c + ',' 
                + rfi.CCL_RelatedTo__c +  ',' 
                + rfi.CCL_Reference_Type__c +  ','
                + rfi.CCL_External_Key__c + '\n';
        }
        
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf(csvContent);
        attachment.Name = request.Name +'-requestFields.csv';
        attachment.ParentId = requestId; 
        Messaging.EmailFileAttachment emailAtt = new Messaging.EmailFileAttachment();
        emailAtt.setBody(attachment.Body);
        emailAtt.setContentType('text/csv');
        emailAtt.setFileName(attachment.Name);
        emailAtt.setinline(false); 
        
        List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
        emailAttachments.add(emailAtt);
        
        //Attach CSV File
        message.setFileAttachments(emailAttachments);
        
        //Set who the email is sent from
        //mail.setReplyTo('noreply@gmail.com');
        //mail.setSenderDisplayName('salesforce User'); 
        
        //Send Email
        Messaging.SendEmailResult result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {message})[0];
        
        if (result.isSuccess()) {
            //status =  'SUCCESS';
        } else {
            //status = 'ERROR';
            //result.getErrors()[0].getMessage();
        }        
    } 
    
    @AuraEnabled
    public static void sendRequest(String requestId){  
        List<CCL_Org_Connection__c> orgC = [SELECT Id,CCL_Named_Credential__c,CCL_Org_Active__c,CCL_Org_ID__c,CCL_Org_Type__c,CCL_Request_Dev_Org__c 
                                           FROM CCL_Org_Connection__c WHERE CCL_Org_Active__c = true AND CCL_Request_Dev_Org__c = true];
        
        if(orgC.size() > 0 && orgC.size() < 2 && orgC[0].CCL_Org_ID__c != UserInfo.getOrganizationId()){
            CCL_RequestIntegrationController.ws_export(requestId,false); 
        }
    }
    
    
    //Get interface record
    @AuraEnabled
    public static CCL_DataLoadInterface__c loadInterfaceData(String interfaceId){
       CCL_DataLoadInterface__c interfaceRecord = [SELECT CCL_Batch_Size__c,CCL_Delimiter__c,CCL_Name__c,CCL_Selected_Object_Name__c,CCL_Text_Qualifier__c,CCL_Type_of_Upload__c FROM CCL_DataLoadInterface__c WHERE Id =: interfaceId];
       return interfaceRecord;
    }

    //Get interface mapping records
    @AuraEnabled
    public static List<fieldInfo> loadMappingData(String interfaceId, String masterFields){
		List<fieldInfo> mFields = (List<fieldInfo>)JSON.deserialize(masterFields,List<fieldInfo>.class);
        List<CCL_DataLoadInterface_DataLoadMapping__c> dliMaps = [SELECT CCL_Column_Name__c, CCL_SObject_Field__c, CCL_Field_Type__c, CCL_ReferenceType__c, CCL_Required__c, CCL_SObjectExternalId__c, CCL_External_ID__c FROM CCL_DataLoadInterface_DataLoadMapping__c WHERE CCL_Data_Load_Interface__c =: interfaceId];
        
        for(fieldInfo mfi : mFields){
            for(CCL_DataLoadInterface_DataLoadMapping__c dim : dliMaps){
                if(mfi.apiName.toLowerCase() == dim.CCL_SObject_Field__c){
            		//mfi.apiName = dim.CCL_SObject_Field__c;
            		//mfi.label = dim.
                    mfi.dataType = dim.CCL_Field_Type__c;
                    //mfi.relatedTo = dim.
                    mfi.required = dim.CCL_Required__c;
                    mfi.csvColumnName = dim.CCL_Column_Name__c;
                    //mfi.id = dim.
                    mfi.refType = dim.CCL_ReferenceType__c;
                    //mfi.actionDisabled = dim.
                    //mfi.extId = dim.
                    mfi.extKey = dim.CCL_SObjectExternalId__c;
                    mfi.mapField = true;
                }
            }
        }
        
        return mFields;
    }
    
    public class objectInfo implements Comparable{
        @AuraEnabled public String apiName {get;set;}
        @AuraEnabled public String label {get;set;}
        
        public Integer compareTo(Object o){
            return label.CompareTo(((objectInfo)o).label);
        }
    }  
    
    public class fieldInfo implements Comparable{
        @AuraEnabled public String apiName {get;set;}
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String dataType {get;set;}
        @AuraEnabled public String relatedTo {get;set;}
        @AuraEnabled public Boolean required {get; set;}
        @AuraEnabled public String csvColumnName {get;set;}
        @AuraEnabled public String id {get;set;}
        @AuraEnabled public String refType {get;set;}
        @AuraEnabled public Boolean actionDisabled {get;set;}
        @AuraEnabled public Boolean extId {get;set;}
        @AuraEnabled public String extKey {get;set;}
        @AuraEnabled public Boolean mapField {get;set;}
        
        public Integer compareTo(Object o){
            return label.CompareTo(((fieldInfo)o).label);
        }
    }  
}