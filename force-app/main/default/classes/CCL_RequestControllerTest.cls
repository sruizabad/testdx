@isTest
public class CCL_RequestControllerTest {

    @isTest
    static void requestControllerTest(){

        sObject objObject = Schema.getGlobalDescribe().get('Account').newSObject();
        CCL_Request__c request = new CCL_Request__c();
        insert request;
        CCL_DataLoader__c settings = CCL_DataLoader__c.getOrgDefaults();
		settings.CCL_Salesforce_Environments__c = 'Test1;Test2';     
        settings.CCL_Request_Target_Email__c = 'test@test.com';
        settings.CCL_templateURL__c = 'http://123.456';
        upsert settings;
        
        Test.startTest();
        
        List<String> selectOptions = CCL_RequestController.getselectOptions(objObject, 'Type');
        List<CCL_RequestController.objectInfo> objectNames = CCL_RequestController.getObjectNames();
        List<CCL_RequestController.fieldInfo> fieldNames = CCL_RequestController.getfieldNames('Account');
        List<CCL_RequestController.fieldInfo> fieldNames2 = CCL_RequestController.getfieldNames(null);
        List<CCL_RequestController.fieldInfo> changedFields = CCL_RequestController.inlineEdit('[{"Id":"1","label":"Name","apiName":"Name","dataType":"dt","relatedTo":"Account","required":false,"csvColumnName":"Account"},{"Id":"2","label":"Name","apiName":"Name","dataType":"dt","relatedTo":"Account","required":false,"csvColumnName":"Account"}]','[{"Id":"1","label":"Name","apiName":"Name","dataType":"dt","relatedTo":"Account","required":true,"csvColumnName":"Contact"}]');
		CCL_RequestController.addRequestFields(request.Id, '[{"Id":"1","label":"Name","apiName":"Name","dataType":"dt","relatedTo":"Account","required":false,"csvColumnName":"Account"}]');
        String result = CCL_RequestController.getOrg();
        List<String> result2 = CCL_RequestController.getOrgs();
        CCL_RequestController.sendEmail(request.Id);

        Test.stopTest();
        
        System.assert(selectOptions != null, 'Could\'t find Picklist Values.');
        System.assert(result != null, 'Could\'t find Orgs');
        System.assert(objectNames != null, 'Could\'t find Objects.');
        System.assert(fieldNames != null, 'Could\'t find fields.');
        System.assert(changedFields != null, 'changed fields not correctly detected');
        
    }
}