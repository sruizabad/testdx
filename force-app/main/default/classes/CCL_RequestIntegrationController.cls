/***************************************************************************************************************************
Apex Class Name :	CCL_RequestIntegrationController
Version : 			1.0
Created Date : 		19/10/2018
Function : 			This class acts as a controller class to send and receive Requests 
and Mappings between multiple SFDC orgs that have this class
deployed. Payload of these requests and Mappings are wrapped inside
a WS_Payload record.

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops						       04/10/2017								Initial version
***************************************************************************************************************************/

@RestResource(urlMapping='/v1/CCL_CSVDataLoaderRequest/*')
global with sharing class CCL_RequestIntegrationController{
    
    @HttpPut
    global static void ws_import(){
        
        Savepoint sp = Database.setSavepoint(); 
        RestResponse res = RestContext.response;
        
        try {
            //System.debug(LoggingLevel.FINE,'Initiate placeholders for DML operations');
            CCL_Request__c request = new CCL_Request__c();
            List<CCL_Request_Field__c> requestFields = new List<CCL_Request_Field__c>();
            //System.debug(LoggingLevel.FINE,'Deserialize inbound JSON Body request');
            //System.Debug('CCL_Log ===> RextContext: ' + RestContext.request);
            String JsonString = RestContext.request.requestBody.toString();                
            //System.debug('CCL_LOG: Inbound JSON String = ' + JsonString);        
            WS_Payload wsp = deserialize(JsonString);                            
            
            request = wsp.ws_request;
            insert request;
            System.debug('request: ' + request);
            
            requestFields.addAll(wsp.ws_mappings);
            
            if(requestFields.size() > 0) {
                for(CCL_Request_Field__c reqF : requestFields){
                    reqF.CCL_Request__c = request.Id;
                }
                
                insert requestFields;
            }
            
        } catch (Exception e) {
            System.debug('CCL_LOG: Error Caught during import of Request & Mappings: ' + e.getMessage());                        
            Database.rollback(sp);                     
        }
    }
    
    public static String ws_export(Id RequestId, Boolean allowPartialSuccess){   
        
        try {  
            
            CCL_Org_Connection__c orgC = [SELECT Id,CCL_Named_Credential__c,CCL_Org_Active__c,CCL_Org_ID__c,CCL_Org_Type__c,CCL_Request_Dev_Org__c 
                                          FROM CCL_Org_Connection__c WHERE CCL_Request_Dev_Org__c = :true][0];
            
            // Wrap payload data for export
            WS_Payload wsp = createPayload(RequestId, true);//allowPartialSuccess);
            
            // Build outbound HttpRequest
            Http http = new Http();
            HttpRequest req = new HttpRequest(); 
            req.setMethod('PUT');
            req.setEndpoint('callout:' + orgC.CCL_Named_Credential__c + '/services/apexrest/v1/CCL_CSVDataLoaderRequest');
            req.setHeader('content-type', 'application/json');
            req.setBody(wsp.generateJSON());   
            HttpResponse response = http.send(req);                           
            
            
            // Verify Response Code from the outbound ws call to check for failure
            if (response.getStatusCode() != 200 ) {
                // write failure to debug logs
                System.debug('Excecution of ws_export failed. Error code: ' + response.getStatusCode()); 
            } else if(response.getStatusCode() == 200 ) {
                // Update Request record
                CCL_Request__c request = [SELECT ID,CCL_Status__c FROM CCL_Request__c WHERE ID = :RequestId];
                request.CCL_Status__c = 'Transferred';
                update request;
            }
            
        } catch (Exception e) {
            // Write Error to debug logs
            System.debug('Exception during execution of ws_export. Exception: ' + e.getMessage());
            // Return code 500 "Internal Error"
            return '500';  
        }
        // Return code 200 "OK"
        return '200';        
    }
    
    
    /*
Apex Method Name: createPayload

Apex method used to generate a WS_Payload record as payload
for the export functionality based upon the Id of a request record
*/
    private static WS_Payload createPayload(Id RequestId, Boolean allowPartialSuccess){               
        // Retrieve request
        CCL_Request__c request = [SELECT Id,CCL_Batch_Size__c,CCL_BI_Division__c,CCL_Column_Delimiter__c,CCL_Description__c, 
                                  CCL_New_Interface_Name__c,CCL_Number_of_Fields__c,CCL_Object__c,CCL_Operation__c,CCL_Requested_By__c, 
                                  CCL_ROPU_Country_Team__c,CCL_SF_Orgs__c,CCL_Source_Interface_Name__c,CCL_Status__c,CCL_Text_Qualifier__c,
                                  CCL_Type__c FROM CCL_Request__c WHERE ID = :RequestId];
        
        // Retrieve request Fields
        List<CCL_Request_Field__c> requestFields = [SELECT CCL_csvColumnName__c,CCL_DataType__c,CCL_DeveloperName__c,CCL_External_Key__c, 
                                                    CCL_Label__c,CCL_Reference_Type__c,CCL_RelatedTo__c,CCL_Request__c,CCL_Required__c
                                                    FROM CCL_Request_Field__c WHERE CCL_Request__c = :RequestId];
        
        // Create Payload
        WS_Payload wsp = new WS_payload();
        wsp.allowPartialSuccess = allowPartialSuccess;
        wsp.ws_request = request;
        wsp.ws_mappings = requestFields;
        
        // Return Payload     
        return wsp;
    }
    
    /*
Inner Class Name: WS_Payload_Record

Inner Apex Class used for serialization and wraping of a single request 
record and list of related Mapping records.
*/
    global class WS_Payload{                        
        
        Boolean allowPartialSuccess {get; set;}   
        CCL_Request__c ws_request {get; set;}        
        List<CCL_Request_Field__c> ws_mappings {get; set;}
        
        // Constructor
        public WS_Payload(){
            ws_request = new CCL_Request__c();
            ws_mappings = new List<CCL_Request_Field__c>();
            allowPartialSuccess = False;
        }   
        
        // Generate JSON String for Callout
        public String generateJSON() {
            String JSonString = '';
            JsonString += CCL_JSONGenerator.CCL_generateRequestJSONContent(ws_request);
            JsonString = JsonString.removeEnd('}');
            JsonString += ',';
            JsonString += '\"ws_mappings\" : [';
            for(CCL_Request_Field__c rf : ws_mappings){
                JsonString += CCL_JSONGenerator.CCL_generateRequestJSONMapContent(rf);
                JsonString += ',';
            }
            JsonString = JsonString.removeEnd(',');
            JsonString += ']}';
            return JsonString;
        }                             
        
    }
    
    private static WS_Payload deserialize(String JsonString){
        WS_RequestJsonDeserializator.Ws_request j_wsp = WS_RequestJsonDeserializator.parse(JsonString);
        WS_Payload wsp = new WS_Payload();
        wsp.allowPartialSuccess = j_wsp.allowPartialSuccess;
        wsp.ws_request.CCL_Batch_Size__c = Integer.valueOf(j_wsp.batchSize); 
        wsp.ws_request.CCL_BI_Division__c = j_wsp.division;
        wsp.ws_request.CCL_Column_Delimiter__c = j_wsp.delimiter;
        wsp.ws_request.CCL_Description__c = j_wsp.description;
        wsp.ws_request.CCL_New_Interface_Name__c = j_wsp.newInterfaceName;
        wsp.ws_request.CCL_Number_of_Fields__c = Integer.valueOf(j_wsp.numberOfFields);
        wsp.ws_request.CCL_Object__c = j_wsp.selectedObject;
        wsp.ws_request.CCL_Operation__c = j_wsp.operation;
        wsp.ws_request.CCL_Requested_By__c = j_wsp.requestedBy;
        wsp.ws_request.CCL_ROPU_Country_Team__c = j_wsp.countryTeam;
        wsp.ws_request.CCL_SF_Orgs__c = j_wsp.sfOrg;
        wsp.ws_request.CCL_Source_Interface_Name__c = j_wsp.sourceInterfaceName;
        wsp.ws_request.CCL_Status__c = 'Transferred';
        wsp.ws_request.CCL_Text_Qualifier__c = j_wsp.textQualifier;
        wsp.ws_request.CCL_Type__c = j_wsp.requestType;

        for (WS_RequestJsonDeserializator.Ws_mapping j_f : j_wsp.ws_mappings){
            CCL_Request_Field__c reqF = new CCL_Request_Field__c();
            reqF.CCL_csvColumnName__c = j_f.columnName;
            reqF.CCL_DataType__c = j_f.dataType;
            reqF.CCL_DeveloperName__c = j_f.developerName;
            reqF.CCL_External_Key__c = j_f.externalKey;
            reqF.CCL_Label__c = j_f.label;
            reqF.CCL_Reference_Type__c = j_f.referenceType;
            reqF.CCL_RelatedTo__c = j_f.relatedTo;
            reqF.CCL_Required__c = j_f.required;
            wsp.ws_mappings.add(reqF);
        }

        return wsp;
    }
}