/***************************************************************************************************************************
Apex Class Name :	CCL_RequestIntegrationControllerTest
Version : 			1.0
Created Date : 		13/04/2018
Function : 			Test class for CCL_RequestIntegrationController
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops						       13/04/2018							Initial version
***************************************************************************************************************************/

@isTest
public class CCL_RequestIntegrationControllerTest {
    @testSetup static void CCL_createTestData() {
        //Id CCL_RTInterfaceInsert = [SELECT Id FROM Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records'].Id;
        String CCL_randomString = CCL_ClsTestUtility.CCL_generateRandomString(5);   

        CCL_Request__c CCL_requestRecord = new CCL_Request__c(CCL_Batch_Size__c = 200, CCL_Column_Delimiter__c = ',',CCL_BI_Division__c = 'AH',CCL_Description__c = 'Test', CCL_New_Interface_Name__c = CCL_randomString, 
																	CCL_Status__c = 'New', CCL_Object__c = 'Account', CCL_Type__c = 'New', CCL_Number_of_Fields__c = 1,
																	CCL_Text_Qualifier__c = '"', CCL_Operation__c = 'Insert');
        insert CCL_requestRecord;
		
		CCL_Request_Field__c CCL_mappingRecord_User = new CCL_Request_Field__c(CCL_DeveloperName__c = 'name',
																	CCL_csvColumnName__c = 'Name', CCL_Label__c = 'Name', CCL_DataType__c = 'STRING', 
																	CCL_Request__c = CCL_requestRecord.Id);
		insert CCL_mappingRecord_User;

		CCL_Org_Connection__c CCL_orgCon = new CCL_Org_Connection__c(Name = 'Test', CCL_Named_Credential__c = 'TestCredential', CCL_Org_Active__c = true, 
																	CCL_Org_Id__c = UserInfo.getOrganizationId(), CCL_Org_Type__c = 'Developer Edition', CCL_Request_Dev_Org__c = true);

		insert CCL_orgCon;
	}

    static testMethod void CCL_testCallout(){ 
        String CCL_randomString = CCL_ClsTestUtility.CCL_generateRandomString(5);     
        String CCL_body = '{"allowPartialSuccess" : true, "batchSize" : "200", "division" : "AH", "delimiter" : ",", "description" : "TestClassMappings", "newInterfaceName" : "' + CCL_randomString + '", "numberOfFields" : "1", "selectedObject" : "Account", "operation": "Insert", "requestedBy" : "testUser", "countryTeam" : "Team1", "sfOrg" : "testOrg", "sourceInterfaceName" : "testsourceinterface", "status" : "New",  "textQualifier" : "", "requestType" : "New", "ws_mappings" : [{ "columnName" : "name", "dataType" : "STRING", "developerName" : "name", "externalKey" : null, "label" : "name", "referenceType" : null, "relatedTo" : null, "required" : false}]}';  
        Test.setMock(HttpCalloutMock.class, new CCL_MockHttpResponse(200, 'OK', '{"Body message"}', new Map<String, String>{'Content-Type' => 'text/plain'}));

        RestRequest CCL_request = new RestRequest();
        RestResponse CCL_response = new RestResponse();
        CCL_request.requestUri = URL.getSalesforceBaseUrl().toexternalForm() + '/services/apexrest/v1/CCL_CSVDataLoaderRequest';
        CCL_request.addHeader('Content-Type', 'text-plain');
        CCL_request.httpMethod = 'PUT';
        CCL_request.requestBody = Blob.valueOf(CCL_body);

        RestContext.request = CCL_request;
        RestContext.response = CCL_response;

        Test.startTest();
        
        CCL_RequestIntegrationController.ws_import();    
        
        Test.stopTest();
        
    }
    
    static testMethod void CCL_testCalloutException(){ 
        String CCL_randomString = CCL_ClsTestUtility.CCL_generateRandomString(5);     
        String CCL_body = '{"allowPartialSuccess" : true, "batchSize" : "200", [{"division" : "AH", "delimiter" : ",", "description" : "TestClassMappings", "newInterfaceName" : "' + CCL_randomString + '"}], "numberOfFields" : "1", "selectedObject" : "Account", "operation": "Insert", "requestedBy" : "testUser", "countryTeam" : "Team1", "sfOrg" : "testOrg", "sourceInterfaceName" : "testsourceinterface", "status" : "New",  "textQualifier" : "", "requestType" : "New", "ws_mappings" : [{ "columnName" : "name", "dataType" : "STRING", "developerName" : "name", "externalKey" : null, "label" : "name", "referenceType" : null, "relatedTo" : null, "required" : false}]}';  
        Test.setMock(HttpCalloutMock.class, new CCL_MockHttpResponse(200, 'OK', '{"Body message"}', new Map<String, String>{'Content-Type' => 'text/plain'}));

        RestRequest CCL_request = new RestRequest();
        RestResponse CCL_response = new RestResponse();
        CCL_request.requestUri = URL.getSalesforceBaseUrl().toexternalForm() + '/services/apexrest/v1/CCL_CSVDataLoaderRequest';
        CCL_request.addHeader('Content-Type', 'text-plain');
        CCL_request.httpMethod = 'PUT';
        CCL_request.requestBody = Blob.valueOf(CCL_body);

        RestContext.request = CCL_request;
        RestContext.response = CCL_response;

        Test.startTest();
        
        CCL_RequestIntegrationController.ws_import();    
        
        Test.stopTest();
        
    }

    static testMethod void CCL_testCalloutExport(){	
		CCL_Request__c CCL_requestRecord = [SELECT Id FROM CCL_Request__c WHERE CCL_Status__c = 'New' LIMIT 1];

        Test.startTest();

		String CCL_orgIdResponse = '{"totalSize":1,"done":true,"records":[{"attributes":{"type":"Organization","url":"/services/data/v37.0/sobjects/Organization/'+ UserInfo.getOrganizationId() + '"},"Id":"' + UserInfo.getOrganizationId() + '"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');
        CCL_responseHeaders.put('i_oks', '1');
        CCL_responseHeaders.put('i_noks', '0');
        CCL_responseHeaders.put('m_oks', '1');
        CCL_responseHeaders.put('m_noks', '0');
        CCL_responseHeaders.put('i_r', 'Ok');
        CCL_responseHeaders.put('m_r', 'Ok');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_orgIdResponse, CCL_responseHeaders));

		String CCL_response = CCL_RequestIntegrationController.ws_export(CCL_requestRecord.Id, true);
		
		Test.stopTest();

    }

    static testMethod void CCL_testCalloutExportFailed(){	
		CCL_Request__c CCL_requestRecord = [SELECT Id FROM CCL_Request__c WHERE CCL_Status__c = 'New' LIMIT 1];

        Test.startTest();

		String CCL_orgIdResponse = '{"totalSize":1,"done":true,"records":[{"attributes":{"type":"Organization","url":"/services/data/v37.0/sobjects/Organization/'+ UserInfo.getOrganizationId() + '"},"Id":"' + UserInfo.getOrganizationId() + '"}]}';
		Map<String, String> CCL_responseHeaders = new Map<String, String>();
		CCL_responseHeaders.put('Content-Type', 'application/json');
        CCL_responseHeaders.put('i_oks', '0');
        CCL_responseHeaders.put('i_noks', '1');
        CCL_responseHeaders.put('m_oks', '0');
        CCL_responseHeaders.put('m_noks', '1');
        CCL_responseHeaders.put('i_r', 'Ok');
        CCL_responseHeaders.put('m_r', 'Ok');

		Test.setMock(HttpCalloutMock.class, new CCL_WSDataLoadMapping_CalloutMock(200, 'OK', CCL_orgIdResponse, CCL_responseHeaders));

		String CCL_response = CCL_RequestIntegrationController.ws_export(CCL_requestRecord.Id, true);
		
		Test.stopTest();

    }
}