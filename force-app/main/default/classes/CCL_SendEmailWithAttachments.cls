/***************************************************************************************************************************
Apex Class Name : 	CCL_SendEmailWithAttachments
Version : 			1.0
Created Date : 		11/07/2018
Function : 			Process plugin to send emails with attachments of a specified record.

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date    				Description
* -----------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops						  		11/07/2018		     	Initial Class Creation      
***************************************************************************************************************************/
global with sharing class CCL_SendEmailWithAttachments implements Process.Plugin {
    
    public String status;
    
    global Process.PluginResult invoke(Process.PluginRequest request) {    
        String recordID = (String) request.inputParameters.get('recordID'); //recordId where all linked attachments are fetched
        String emailAddress = (String) request.inputParameters.get('emailAddress'); 
        String ccEmailAddress = (String) request.inputParameters.get('ccEmailAddress'); 
        String subject = (String) request.inputParameters.get('subject'); 
        String body = (String) request.inputParameters.get('body');
        
        Map<String,Object> result = new Map<String,Object>();
        try {
            SendEmail(recordID,emailAddress,ccEmailAddress,subject,body);
            result.put('Status', status);
        }
        catch (Exception error) {
            result.put('Status',ERROR);
        }
        return new Process.PluginResult(result); 
    }
    
    global void sendEmail(String recordId, String emailAddress, String ccEmailAddress, String subject, String body) {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        
        if (emailAddress != null && emailAddress.length() > 0) {
            message.setToAddresses(new String[] {emailAddress});
        }
        
        if (ccEmailAddress != null && ccEmailAddress.length() > 0) {
            message.setCcAddresses(new String[] {ccEmailAddress});
        }
        
        message.setSubject(subject);
        message.setHtmlBody(body != null ? body : '');
        message.setUseSignature(true);
        
        List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
        List<Attachment> attachmentList = [SELECT Name, Body FROM Attachment WHERE ParentId =: recordId];
        
        if (attachmentList != null) {
            for(attachment at : attachmentList){
                Messaging.EmailFileAttachment emailAtt = new Messaging.EmailFileAttachment();
                emailAtt.setBody(at.Body);
                emailAtt.setContentType('text/csv');
                emailAtt.setFileName(at.Name);
                emailAtt.setinline(false); 
                emailAttachments.add(emailAtt);
            }
        }
        
        message.setFileAttachments(emailAttachments);
        Messaging.SendEmailResult result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {message})[0];
        
        if (result.isSuccess()) {
            status =  'SUCCESS';
        } else {
            status = 'ERROR';
            //result.getErrors()[0].getMessage();
        }
    }        
    
    global Process.PluginDescribeResult describe() { 
        Process.PluginDescribeResult result = new Process.PluginDescribeResult();     
        result.tag = 'Emails with Attachments';
        
        result.inputParameters = new List<Process.PluginDescribeResult.InputParameter>{
            new Process.PluginDescribeResult.InputParameter('recordID', Process.PluginDescribeResult.ParameterType.STRING, false),
                new Process.PluginDescribeResult.InputParameter('emailAddress', Process.PluginDescribeResult.ParameterType.STRING, true),
                new Process.PluginDescribeResult.InputParameter('ccEmailAddress', Process.PluginDescribeResult.ParameterType.STRING, false),
                new Process.PluginDescribeResult.InputParameter('subject', Process.PluginDescribeResult.ParameterType.STRING, true),            
                new Process.PluginDescribeResult.InputParameter('body', Process.PluginDescribeResult.ParameterType.STRING, true)                                                                                                                 
                };  
                    
        result.outputParameters = new List<Process.PluginDescribeResult.OutputParameter>{
            new Process.PluginDescribeResult.OutputParameter('Status', Process.PluginDescribeResult.ParameterType.STRING)                       
            };     
        return result;
    }    
}