/***************************************************************************************************************************
Apex Class Name : 	CCL_SendEmailWithAttachmentsTest
Version : 			1.0
Created Date : 		11/07/2018
Function : 			Test class for CCL_SendEmailWithAttachments.

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date    				Description
* -----------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops						  		11/07/2018		     	Initial Class Creation      
***************************************************************************************************************************/
@isTest
public class CCL_SendEmailWithAttachmentsTest {
    
    private static Case createCase(){
        Case record = new Case();
        record.Subject = 'Test';
        insert record;
        Attachment att = new Attachment();
        att.Name = 'ATTNAME';
        att.Body = Blob.valueOf('Attachment Body');
        att.ParentId = record.Id;
        insert att;
        return record;
    }
    
    static testmethod void sendEmailTest(){
        Case record = createCase();
        CCL_SendEmailWithAttachments plugin = new CCL_SendEmailWithAttachments();
        Map<String, Object> inputParams = new Map<String, Object>();
        string recordID = record.Id;		//50063000003AEik-SBX | 500o000000GCryb-PROD
        string emailAdd = 'wouter.jacops@c-clearpartners.com';
        string emailAddcc = 'wouter.jacops@c-clearpartners.com';
        string subject = 'SUBJECT';
        string body = 'BODY';
        
        inputParams.put('recordID', recordID);
        inputParams.put('emailAddress', emailAdd);
        inputParams.put('ccEmailAddress', emailAddcc);
        inputParams.put('subject', subject);
        inputParams.put('body', body);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        plugin.invoke(request);
        Process.PluginDescribeResult result = plugin.describe();
    }
    
    static testmethod void sendEmailTestFail(){
        Case record = createCase();
        CCL_SendEmailWithAttachments plugin = new CCL_SendEmailWithAttachments();
        Map<String, Object> inputParams = new Map<String, Object>();
        string recordID = record.Id;		//50063000003AEik-SBX | 500o000000GCryb-PROD
        string emailAdd = 'not a valid address';
        string emailAddcc = 'not a valid address';
        string subject = 'SUBJECT';
        string body = 'BODY';
        
        inputParams.put('recordID', recordID);
        inputParams.put('emailAddress', emailAdd);
        inputParams.put('ccEmailAddress', emailAddcc);
        inputParams.put('subject', subject);
        inputParams.put('body', body);
        
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        
        plugin.invoke(request);
        Process.PluginDescribeResult result = plugin.describe();
    }
    
}