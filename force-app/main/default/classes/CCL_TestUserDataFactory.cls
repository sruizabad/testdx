/***************************************************************************************************************************
Apex Class Name :	CCL_DataLoaderRegistrationHandlerTest
Version : 			1.0
Created Date : 		13/04/2018
Function : 			Test Data Factory for Users
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Robin Wijnen								13/04/2018								Initial version
***************************************************************************************************************************/

@isTest
public class CCL_TestUserDataFactory {
    public static Profile CCL_getUserProfile(String CCL_profileName) {
        Profile CCL_profile = [SELECT Id, Name FROM Profile WHERE Name =: CCL_profileName LIMIT 1];
        return CCL_profile;
    }

    public static UserRole CCL_getUserRole(String CCL_roleName) {
        return [SELECT Id, Name FROM UserRole WHERE DeveloperName =: CCL_roleName];
    }

    public static User CCL_getUser(String CCL_alias, String CCL_userName, String CCL_profileName, String CCL_roleName, Boolean CCL_insert) {
        UserRole CCL_ur;
        User CCL_user;

        if(CCL_roleName != null) {
            CCL_ur = CCL_getUserRole(CCL_roleName);
        }

        CCL_user = new User(Alias = CCL_alias, Email = CCL_userName, EmailEncodingKey = 'UTF-8', Lastname = 'last', Firstname = 'first', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', ProfileId = CCL_getUserProfile(CCL_profileName).Id, TimeZoneSidKey='Europe/Brussels', Username = CCL_userName, CCL_DataLoaderSourceUserName__c = CCL_username);
        if(CCL_roleName != null) CCL_user.UserRoleId = CCL_ur.Id;

        if(CCL_insert) insert CCL_user;

        return CCL_user;
    }
}