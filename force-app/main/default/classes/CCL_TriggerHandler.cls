/***************************************************************************************************************************
Apex Trigger Name : CCL_trHandler_DataloadMapping
Version : 1.0
Created Date : 19/10/2015	
Function :  Trigger Handler that can be used for all Trigger handler classes

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois					  			19/10/2015      		        		Initial Creation
***************************************************************************************************************************/

public virtual class CCL_TriggerHandler {
  
  // Initialize static booleans to track if this is the first time 
  public   static  boolean isBeforeInsertFirstRun   = true;
  public   static  boolean isBeforeUpdateFirstRun   = true;

 
  protected final integer batchSize;

  
  private class HandlerException extends Exception {}

  /* Constructor */
  public CCL_TriggerHandler() {

    if (Trigger.isExecuting != true && Test.isRunningTest() == false) {
      throw new HandlerException('This class may only be instantiated within a Trigger-based ' 
                                +'Execution Context.');
    }

    batchSize = Trigger.size;
  }

  @testVisible
  protected boolean beforeInsertHasRun() {
    if (isBeforeInsertFirstRun) {
      return isBeforeInsertFirstRun = false;
    }
    return true;
  }
 
  @testVisible
  protected boolean beforeUpdateHasRun() {
    if (isBeforeUpdateFirstRun) {
      return isBeforeUpdateFirstRun = false;
    }
    return true;
  }
}