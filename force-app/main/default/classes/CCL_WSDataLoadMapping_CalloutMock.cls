/***************************************************************************************************************************
Apex Class Name :   CCL_WSDataLoadMapping_CalloutMock
Version :           1.0.0
Created Date :      22/11/2016
Function :          REST HTTP Mockout class for CCL_WS_DataLoadMapping
            
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                                 Date                                    Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Steve van Noort                       22/11/2016                              Initial creation
***************************************************************************************************************************/

@isTest
public class CCL_WSDataLoadMapping_CalloutMock implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public CCL_WSDataLoadMapping_CalloutMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        for (String key : this.responseHeaders.keySet()) {
            res.setHeader(key, this.responseHeaders.get(key));
        }
        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }

}