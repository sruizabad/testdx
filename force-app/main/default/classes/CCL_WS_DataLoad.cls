/***************************************************************************************************************************
Apex Class Name :   CCL_WS_DataLoadInterface
Version :           1.0.0
Created Date :      4/11/2016
Function :          This is a Web Services class "CCL_WS_DataLoad" to support Apex REST http(PUT) requests for upsert 
                    of CCL_DataLoadInterface__c transactions. 
            
Modification Log:
---------------------------------------------------------------------------------------------------------------------------
* Developer                                 Date                                    Description
* -------------------------------------------------------------------------------------------------------------------------
* Steve van Noort                           04/11/2016                              First version
*                                                                                   
***************************************************************************************************************************/
@RestResource(urlMapping='/CCL_CSVDataLoader/*')
global with sharing class CCL_WS_DataLoad {

    @HttpPut
    global static String upsertDataLoad(String RecordTypeId, String batchSize, String externalId, 
                                    String delimiter, String textQualifier,
                                    String interfaceStatus, String name,  
                                    String description, String typeOfUpload,
                                    String selectedObjectName, Boolean unlockRecord                                     
                                   ){                                           
        CCL_DataLoadInterface__c thisDataLoad = new CCL_DataLoadInterface__c();
            thisDataLoad.RecordTypeId = RecordTypeId;
            thisDataLoad.CCL_Batch_Size__c = Decimal.valueOf(batchSize);
            thisDataLoad.CCL_Delimiter__c = delimiter;
            thisDataLoad.CCL_External_ID__c = externalId;                                       
            thisDataLoad.CCL_Interface_Status__c = interfaceStatus;
            thisDataLoad.CCL_Name__c = name;
            thisDataLoad.CCL_Text_Qualifier__c = textQualifier;            
            thisDataLoad.CCL_Description__c = description;
            thisDataLoad.CCL_Type_of_Upload__c = typeOfUpload;                           
            thisDataLoad.CCL_Selected_Object_Name__c = selectedObjectName;
            thisDataLoad.CCL_Unlock_Record__c = unlockRecord;
            thisDataLoad.CCL_Deploy_Overide__c = true;
                                      
        // Verify if there are active Tasks
        List<CCL_DataLoadInterface_DataLoadJob__c> CCL_activeJobs = [SELECT Id FROM CCL_DataLoadInterface_DataLoadJob__c 
                                                                    WHERE CCL_Data_Load_Interface__r.CCL_External_ID__c = :externalId AND CCL_Number_of_Pending_Tasks__c > 0];
        if(CCL_activeJobs.isEmpty()) {
            try {
                upsert thisDataLoad CCL_External_ID__c;
                return 'Success';
            } catch(Exception e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
                return e.getMessage();
            }
        } else {
            return 'The Interface has pending Jobs.';
        }
        
        
        return null;
    }    
}