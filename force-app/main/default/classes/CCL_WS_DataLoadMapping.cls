/***************************************************************************************************************************
Apex Class Name :   CCL_WS_DataLoadMapping
Version :           1.0.0
Created Date :      6/11/2016
Function :          This is a Web Services class "CCL_WS_DataLoadMap" to support Apex REST http(PUT) requests for upsert 
                    of CCL_DataLoadInterface_DataLoadMapping__c transactions. 
            
Modification Log:
---------------------------------------------------------------------------------------------------------------------------
* Developer                                 Date                                    Description
* -------------------------------------------------------------------------------------------------------------------------
* Steve van Noort                           06/11/2016                              First version
*                                                                                   
***************************************************************************************************************************/
@RestResource(urlMapping='/CCL_CSVDataLoadMapping/*')
global with sharing class CCL_WS_DataLoadMapping {

    @HttpPut
    global static String upsertDataLoadMapping( String columnName,
                                            String dataLoadInterface,
                                            String defaultValue,
                                            Boolean deployOveride,
                                            Boolean mapDefault, 
                                            String fieldType, 
                                            Boolean isReferenceToExternalId, 
                                            Boolean isUpsertId,
                                            String referenceType, 
                                            String sObjectExternalId,
                                            Boolean required, 
                                            String sObjectField, 
                                            String sObjectMappedField,                                   
                                            String sObjectName,
                                            String externalId,
                                            String parentInterfaceExternalId                                                
                                        ){
           System.debug('Show parent external Id: ' + parentInterfaceExternalId + ' ** sObjectField ** ' + sObjectField + ' **externalID** ' + externalID);                           
           CCL_DataLoadInterface__c CCL_parent = new CCL_DataLoadInterface__c(CCL_External_Id__c = parentInterfaceExternalId);
                                            
           CCL_DataLoadInterface_DataLoadMapping__c thisDataLoadMapping = new CCL_DataLoadInterface_DataLoadMapping__c();
                thisDataLoadMapping.CCL_Column_Name__c = columnName;
                thisDataLoadMapping.CCL_Data_Load_interface__r = CCL_parent;
                thisDataLoadMapping.CCL_Default_Value__c = defaultValue;
                thisDataLoadMapping.CCL_Deploy_Overide__c = true;                            
                thisDataLoadMapping.CCL_Default__c = mapDefault;
                thisDataLoadMapping.CCL_Field_Type__c = fieldType;
                thisDataLoadMapping.CCL_isReferenceToExternalId__c = isReferenceToExternalId; 
                thisDataLoadMapping.CCL_isUpsertId__c = isUpsertId;
                thisDataLoadMapping.CCL_ReferenceType__c = referenceType;                            
                thisDataLoadMapping.CCL_SObjectExternalId__c = sObjectExternalId;
                thisDataLoadMapping.CCL_Required__c = required;
                thisDataLoadMapping.CCL_SObject_Field__c = sObjectField;                                            
                thisDataLoadMapping.CCL_SObject_Mapped_Field__c = sObjectMappedField;
                thisDataLoadMapping.CCL_SObject_Name__c = sObjectName;
                thisDataLoadMapping.CCL_External_ID__c = externalID;
                                    
        // Verify if there are active Tasks
        List<CCL_DataLoadInterface_DataLoadJob__c> CCL_activeJobs = [SELECT Id FROM CCL_DataLoadInterface_DataLoadJob__c 
                                                                    WHERE CCL_Data_Load_Interface__r.CCL_External_ID__c = :parentInterfaceExternalId AND CCL_Number_of_Pending_Tasks__c > 0];
        if(CCL_activeJobs.isEmpty()) {
            try {
               upsert thisDataLoadMapping CCL_External_ID__c;
                return 'Success';
            } catch(Exception e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
                return e.getMessage();
            }
        } else {
            return 'The Interface has pending Jobs.';
        }
        return null;

    }    
}