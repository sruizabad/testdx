/***************************************************************************************************************************
Apex Class Name :   CCL_WS_DataLoadTest
Version :           1.0.0
Created Date :      6/11/2016
Function :          Test class for CCL_WS_DataLoad 
            
Modification Log:
---------------------------------------------------------------------------------------------------------------------------
* Developer                                 Date                                    Description
* -------------------------------------------------------------------------------------------------------------------------
* Steve van Noort                           06/11/2016                              First version
*                                                                                   
***************************************************************************************************************************/

@isTest
private class CCL_WS_DataLoadTest {
	@testSetup static void CCL_createTestData() {
		Id CCL_RTInterfaceInsert = [SELECT Id FROM Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records'].Id;

        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
																	CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'userterritory',
																	CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert',
																	CCL_External_Id__c = 'EXTTEST1', RecordtypeId = CCL_RTInterfaceInsert);
        insert CCL_interfaceRecord;
		System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_User = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'userid',
																	CCL_Column_Name__c = 'User ID', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = false, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'userid', CCL_SObject_Name__c = 'userterritory', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id,
																	CCL_External_Id__c = 'EXTMAP1');
		insert CCL_mappingRecord_User;
		System.Debug('===CCL_mappingRecord_User: ' + CCL_mappingRecord_User.Id);
		
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Terr = new CCL_DataLoadInterface_DataLoadMapping__c(CCL_SObject_Field__c = 'territoryid',
																	CCL_Column_Name__c = 'Territory ID', CCL_Field_Type__c = 'STRING', CCL_isReferenceToExternalId__c = false,
																	CCL_isUpsertId__c = true, CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null, 
																	CCL_SObject_Mapped_Field__c = 'territoryid', CCL_SObject_Name__c = 'userterritory', 
																	CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id, CCL_Required__c = true);
		insert CCL_mappingRecord_Terr;
		System.Debug('===CCL_mappingRecord_Terr: ' + CCL_mappingRecord_Terr.Id);

		CCL_Org_Connection__c CCL_orgCon = new CCL_Org_Connection__c(Name = 'Test', CCL_Named_Credential__c = 'TestCredential', CCL_Org_Active__c = true, 
																	CCL_Org_Id__c = UserInfo.getOrganizationId(), CCL_Org_Type__c = 'Developer Edition');

		insert CCL_orgCon;

		CCL_CSVDataLoaderExport__c CCL_export = new CCL_CSVDataLoaderExport__c(CCL_Org_Connection__c = CCL_orgCon.Id, CCL_Source_Data__c = 'Mappings',
																				CCL_Status__c = 'In Progress');
		insert CCL_export;
	}

	@isTest static void CCL_upsertInterface() {
		Id CCL_RTId = [SELECT Id FROm Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records' LIMIT 1].Id;

		String CCL_result = CCL_WS_DataLoad.upsertDataLoad(CCL_RTId, '20', 'EXTTEST2', ',', '"', 
															'Active', 'Test', 'Descipriont', 'Insert', 
															'Account', true);

		System.assertEquals('Success', CCL_result);
	}

	@isTest static void CCL_upsertInterfaceFailed() {
		Id CCL_RTId = [SELECT Id FROm Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records' LIMIT 1].Id;

		String CCL_result = CCL_WS_DataLoad.upsertDataLoad(CCL_RTId, '20', 'EXTTEST1', ',', '"', 
															'Active', 'Test', 'Descipriont', 'Insert', 
															'Account', true);

		System.assert(CCL_result.contains('EXCEPTION'));
	}

	@isTest static void CCL_upsertMappingPendingJobs() {
		CCL_CSVDataLoaderExport__c CCL_export = [SELECT Id FROM CCL_CSVDataLoaderExport__c LIMIT 1];
		CCL_DataLoadInterface__c CCL_int = [SELECT Id, CCL_Interface_Status__c FROM CCL_DataLoadInterface__c LIMIT 1];
		CCL_int.CCL_Interface_Status__c = 'Active';
		update CCL_int;

		CCL_DataLoadInterface_DataLoadJob__c CCL_job = new CCL_DataLoadInterface_DataLoadJob__c(CCL_Data_Load_Interface__c = CCL_int.Id,
																								CCL_Name__c = 'Test', CCL_Job_Status__c = 'Active');
		insert CCL_job;

		CCL_DataLoadJob_DataLoadTask__c CCL_task = new CCL_DataLoadJob_DataLoadTask__c(CCL_Data_Load_Job__c = CCL_job.Id, CCL_Status__c = 'Pending');
		insert CCL_task;

		Id CCL_RTId = [SELECT Id FROm Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records' LIMIT 1].Id;

		String CCL_result = CCL_WS_DataLoad.upsertDataLoad(CCL_RTId, '20', 'EXTTEST1', ',', '"', 
															'Active', 'Test', 'Descipriont', 'Insert', 
															'Account', true);

		System.assertEquals('The Interface has pending Jobs.', CCL_result);
	}	
}