/***************************************************************************************************************************
Apex Class Name :	CCL_csvValues
Version : 			1.0
Created Date : 		1/02/2015
Function : 			This class contains all the details of a certain value in the csv File. It also implements a method to keep
					track of the size of the field. 
			
Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois							  	01/02/2015      		        		Class creation
***************************************************************************************************************************/

public without sharing class CCL_csvValues {
	public string CCL_field;
	public Boolean CCL_enclosed;
	public String CCL_delimiter;
	public String CCL_textQualifier;
	
	public CCL_csvValues(){
		this.CCL_field = '';
		this.CCL_delimiter = '';
		this.CCL_textQualifier = '';
	}
	
	/*
	public CCL_csvValues(String CCL_field, Boolean CCL_enclosed, String CCL_delimiter, String CCL_textQualifier) {
        this.CCL_field = CCL_field;
        this.CCL_enclosed = CCL_enclosed;
        this.CCL_delimiter = CCL_delimiter;
        this.CCL_textQualifier = CCL_textQualifier;
    }
    */
    
    
    /**
    *	Method to determine the bite size of a field. This can be checked against the remaining of a CSV file,
    *	to easily check if it was the last field of the file.
    **/
	public Integer CCL_biteSize() {
		System.Debug('===Sizes==='+CCL_field.length()+CCL_delimiter.length()+CCL_textQualifier.length());
  		Integer biteSize = CCL_field.length() + CCL_delimiter.length();
  		
  		// It has to be taken into account that if the value is enclosed, the bitesize is larger
  		if (CCL_enclosed) {
	    	biteSize += CCL_textQualifier.length() * 2;
  		}	
  		System.debug('biteSize: ' + biteSize);
  
  		return biteSize;
	}
}