/***************************************************************************************************************************
Apex Class Name : CCL_dataLoadUtilityClass
Version : 2.4.2
Created Date : 12/02/2015
Function :  A clear description of the class should be provided here. Containing the explation of the different methods and what kind of functionality they represent. This to ensure that future developers can easily understand what the code is about. 

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            	Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois							12/02/2015								Initial Creation
* Robin Wijnen							13/03/2015								Bug fixes with standard reference fields
* Joris Artois							19/10/2015								Update the class with new methods for the calculation
																				of fields needed for the DataLoadMapping 
																				(called in trigger handler CCL_trHandler_DataLoadMapping)
* Robin Wijnen							05/02/2016								Added utitily class to determine salesforce production/sandbox
																				evironment
***************************************************************************************************************************/

global without sharing class CCL_dataLoadUtilityClass {
	public static Map<String, Schema.SObjectField> CCL_getMap_fieldName_sObjectField(String CCL_stringObjectAPI) {
		if(CCL_stringObjectAPI != null) {
			Map<String, Schema.SObjectType> CCL_schemaMap = Schema.getGlobalDescribe();
			Schema.SObjectType CCL_objectSchema = CCL_schemaMap.get(CCL_stringObjectAPI);
			
			return CCL_objectSchema.getDescribe().fields.getMap();
		}
		return null;
	}
	
	public static List<Schema.sObjectType> CCL_getList_referencesObject (String CCL_stringObjectAPI, String CCL_fieldAPIName) {
		if(CCL_fieldAPIName != null && CCL_fieldAPIName != '' && CCL_stringObjectAPI != '' && CCL_stringObjectAPI != null) {
			System.Debug('===CCL_stringObjectAPI::' + CCL_stringObjectAPI + ', CCL_fieldAPIName::' + CCL_fieldAPIName);
			Map<String, Schema.SObjectField> CCL_mapFields;			
			Map<String, Schema.SObjectType> CCL_schemaMap = Schema.getGlobalDescribe();
			
			Schema.SObjectType CCL_objectSchema = CCL_schemaMap.get(CCL_stringObjectAPI);			
			CCL_mapFields = CCL_objectSChema.getDescribe().fields.getMap();
			
			return CCL_mapFields.get(CCL_fieldAPIName).getDescribe().getReferenceTo();
		}
		return null;
	}
	
	public static boolean CCL_isUpsertField(String CCL_stringObjectAPI, String CCL_stringFieldAPI) {
		System.Debug('===CCL_stringObjectAPI::'+CCL_stringObjectAPI + ', CCL_stringFieldAPI::'+CCL_stringFieldAPI);
		if(CCL_stringObjectAPI != null && CCL_stringObjectAPI != '' && CCL_stringFieldAPI != null && CCL_stringFieldAPI != '') {
            if(CCL_stringFieldAPI == 'id' || CCL_stringFieldAPI == 'Id' || CCL_stringFieldAPI == 'ID') {
                return true;
            } else {
                Schema.SObjectType CCL_objectSchema = Schema.getGlobalDescribe().get(CCL_stringObjectAPI);
				Map<String, Schema.SObjectField> CCL_mapFields = CCL_objectSchema.getDescribe().fields.getMap();
				return CCL_mapFields.get(CCL_stringFieldAPI).getDescribe().isExternalID();
            }
			return null;
		}
		return null;
	}
	
	public static String CCL_getMappedField_Name (boolean CCL_isReference, boolean CCL_isExternal, String CCL_fieldAPIName, String CCL_externalId, boolean CCL_isCustom) {
        System.Debug('===CCL_isReference::'+CCL_isReference+', CCL_isExternal::'+CCL_isExternal+', CCL_fieldAPINAme::'+CCL_fieldAPIName+', CCL_externalId::'+CCL_externalId+', CCL_isCustom::'+CCL_isCustom);
		if(CCL_isReference != null && CCL_isExternal != null && CCL_fieldAPIName != '' && CCL_externalId != '' && CCL_isCustom != null) {
			if(!CCL_isReference || !CCL_isExternal){
				return CCL_fieldAPIName;
			} else if (CCL_isExternal){
				/**
				* Author: Robin Wijnen
				* Date: 13/03/2015
				* Description: Bug fix on standard reference field. Check first if field is Standard or Custom and then perform the magic
				**/
				if(CCL_isCustom) {
					return (CCL_fieldAPIName.substring(0,CCL_fieldAPIName.length()-3) + '__r.' + CCL_externalId);
				} else {
					// Remove the 'id' and replace with a dot
					return (CCL_fieldAPINAme.substring(0,CCL_fieldAPIName.length()-2) + '.' + CCL_externalId);
				}
			} 
		}
		return '';
	}
    
    public static String CCL_formatWhereList(List<String> CCL_lstStringList) {
        String CCL_returnStr = '';
        
        if(CCL_lstStringList != null && CCL_lstStringList.size() > 0) {
            for(String CCL_str : CCL_lstStringList) {
                CCL_returnStr += '\'' + CCL_str + '\',';
            }
        }
        return CCL_returnStr.subStringBeforeLast(',');
    }
	
	Webservice static String CCL_RecordTypeIDInterface(String recordTypeName, String idRecord){		
		Schema.DescribeSObjectResult describeResult = Schema.getGlobalDescribe().get('CCL_DataLoadInterface__c').getDescribe();
		Map<String, Schema.RecordTypeInfo> CCL_recordType = describeResult.getRecordTypeInfosByName();
        
        String query = 'SELECT Id, RecordTypeId FROM CCL_DataLoadInterface__c WHERE Id=: '+idRecord+' LIMIT 1';
        List<CCL_DataLoadInterface__c> test = [SELECT Id, RecordTypeId FROM CCL_DataLoadInterface__c WHERE Id =: idRecord LIMIT 1];
        
        test[0].RecordTypeId = CCL_recordType.get(recordTypeName).getRecordTypeId();
        update test[0];
        
		return CCL_recordType.get(recordTypeName).getRecordTypeId();
	}
	
	Webservice static String CCL_RecordTypeIDJob(String recordTypeName, String idRecord, String jobStatus){		
		Schema.DescribeSObjectResult describeResult = Schema.getGlobalDescribe().get('CCL_DataLoadInterface_DataLoadJob__c').getDescribe();
		Map<String, Schema.RecordTypeInfo> CCL_recordType = describeResult.getRecordTypeInfosByName();
        
        String query = 'SELECT Id, RecordTypeId FROM CCL_DataLoadInterface_DataLoadJob__c WHERE Id=: '+idRecord+' LIMIT 1';
        List<CCL_DataLoadInterface_DataLoadJob__c> test = [SELECT Id, RecordTypeId, CCL_Job_Status__c FROM CCL_DataLoadInterface_DataLoadJob__c WHERE Id =: idRecord LIMIT 1];
        
        test[0].RecordTypeId = CCL_recordType.get(recordTypeName).getRecordTypeId();
        test[0].CCL_Job_Status__c = jobStatus;
        update test[0];
		return CCL_recordType.get(recordTypeName).getRecordTypeId();
	}
	
	
	/* 
	 * Data Load Utility class methods for the calculation of fields for the Mapping record. 
	 * Called by both the Trigger Handler (CCL_trHandler_DataLoadMapping) 
	 * and the Mapping extension (CCL_DataLoadMapping_newEditPage_Ext)
	 */
	
	// Method to get the selected object
	public static void CCL_SelectedObject(
								List<CCL_DataLoadInterface_DataLoadMapping__c> mappingRecord_lst){
		List<ID> interfaceID_lst = new List<ID>();
		List<CCL_DataLoadInterface__c> interface_lst = new List<CCL_DataLoadInterface__c>();
		Map<CCL_DataLoadInterface_DataLoadMapping__c, String> errorMessages = new Map<CCL_DataLoadInterface_DataLoadMapping__c, String>();

		// Get the list of all interface IDs
		for(CCL_DataLoadInterface_DataLoadMapping__c mappingRecord : mappingRecord_lst){
			interfaceID_lst.add(mappingRecord.CCL_Data_Load_Interface__c);
		}

		// Create a list, based on the Interface IDs, with the interface records 
		// and the needed sObject Name
		interface_lst = [SELECT 	CCL_Selected_Object_Name__c 
						 FROM		CCL_DataLoadInterface__c
						 WHERE		Id =: interfaceID_lst];
		
		// Populate the Mapping Record with the sObject Name from the corresponding Interface
		for(CCL_DataLoadInterface_DataLoadMapping__c mappingRecord : mappingRecord_lst){
				for (CCL_DataLoadInterface__c interfaceRec : interface_lst){
					if (interfaceRec.Id == mappingRecord.CCL_Data_Load_Interface__c){
						mappingRecord.CCL_Sobject_Name__c = interfaceRec.CCL_Selected_Object_Name__c;
					}
				}
		}
	}
	
	// Method to get the field type
	public static Map<CCL_DataLoadInterface_DataLoadMapping__c, String> CCL_FieldType(List<CCL_DataLoadInterface_DataLoadMapping__c> mappingRecord_lst){		
		Map<String, Schema.SObjectType> CCL_schemaMap = Schema.getGlobalDescribe();
		Map<String, Schema.DescribeSObjectResult> describeObject = new Map<String, Schema.DescribeSObjectResult>();
		Map<CCL_DataLoadInterface_DataLoadMapping__c, String> errorMessages = new Map<CCL_DataLoadInterface_DataLoadMapping__c, String>();
		
		for(CCL_DataLoadInterface_DataLoadMapping__c mappingRecord : mappingRecord_lst){
			try{
				Schema.SObjectType CCL_objectSchema = CCL_schemaMap.get(mappingRecord.CCL_Sobject_Name__c);
                System.Debug(mappingRecord.CCL_SObject_Mapped_Field__c);
				if (describeObject.get(mappingRecord.CCL_Sobject_Name__c) == null){
					describeObject.put(mappingRecord.CCL_Sobject_Name__c, CCL_objectSchema.getDescribe());
				}
				Schema.DisplayType myType = describeObject.get(mappingRecord.CCL_Sobject_Name__c).fields.getMap()
											.get(mappingRecord.CCL_SObject_Field__c).getDescribe().getType();
	
				mappingRecord.CCL_Field_Type__c = String.valueOf(myType);
				System.Debug('---> field Type: '+String.valueOf(myType));
				if(myType != Schema.DisplayType.Reference){
					mappingRecord.CCL_ReferenceType__c = null;
					mappingRecord.CCL_SObjectExternalID__c = null;
				}
			}
			catch (Exception e){
				errorMessages.put(mappingRecord, e.getMessage());
			}
		}
		return errorMessages;
	}
	
	// Method to set the Mapped Object Field and to set the Upsert ID field.
	public static void CCL_MappedObjectField(
							List<CCL_DataLoadInterface_DataLoadMapping__c> mappingRecord_lst){
		boolean isCustom = false;
		boolean isReference = false;
		boolean isExternal = false;
		
		for (CCL_DataLoadInterface_DataLoadMapping__c mappingRecord : mappingRecord_lst){
			isCustom = false;
			isReference = false;
			isExternal = false;
			
			if (mappingRecord.CCL_Field_Type__c == 'ID' || mappingRecord.CCL_Field_Type__c == 'REFERENCE'){
				isReference = true;
				if (mappingRecord.CCL_ReferenceType__c == 'External ID'){
					isExternal = true;
				}
				if (String.valueOf(mappingRecord.CCL_Sobject_Field__c).endsWith('__c')){
					isCustom = true;
				}
				mappingRecord.CCL_SObject_Mapped_Field__c = CCL_getMappedField_Name(
									isReference, isExternal, mappingRecord.CCL_Sobject_Field__c, 
									mappingRecord.CCL_SObjectExternalId__c, isCustom);
			} else {
				mappingRecord.CCL_SObject_Mapped_Field__c = mappingRecord.CCL_Sobject_Field__c;
			}
			
			mappingRecord.CCL_isUpsertId__c = CCL_isUpsertField(mappingRecord.CCL_Sobject_Name__c, 
																mappingRecord.CCL_SObject_Field__c);
			System.Debug('---> SObject Mapped Field: '+mappingRecord.CCL_SObject_Mapped_Field__c);
		}
	}
	
	/**
	* Author: 			Robin Wijnen <robin.wijnen@c-clearpartners.com>
	* Date: 			05/02/2016
	* Description:		Utility method which checks if the current org is a sandbox or a production org
	* @return boolean: 	returns TRUE if the environment is a Sandbox
	**/
	public static boolean CCL_isSandbox() {
		return [SELECT isSandbox FROM Organization LIMIT 1].isSandbox;
	}
}