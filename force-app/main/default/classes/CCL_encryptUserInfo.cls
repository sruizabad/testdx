public without sharing class CCL_encryptUserInfo {
    public String CCL_USERNAME{get;set;}
    public String CCL_PASSWORD{get;set;}
    public String CCL_CONSUMERKEY{get;set;}
    public String CCL_CONSUMERSECRET{get;set;}
    
    public void save(){
        Blob key = Crypto.generateAesKey(128);
        CCL_DataLoader__c customSetting = CCL_DataLoader__c.getOrgDefaults();

        customSetting.CCL_username__c = CCL_encrypt(CCL_USERNAME,key);
        customSetting.CCL_password__c = CCL_encrypt(CCL_PASSWORD,key);
        customSetting.CCL_ClientID__c = CCL_encrypt(CCL_CONSUMERKEY,key);
        customSetting.CCL_ClientSecret__c = CCL_encrypt(CCL_CONSUMERSECRET,key);
        customSetting.CCL_encrKey__c = EncodingUtil.base64encode(key);
        
        update customSetting;
        
        Blob base64 = EncodingUtil.base64decode(customSetting.CCL_password__c);
        Blob decryptedVal = Crypto.decryptWithManagedIV('AES128', key, base64);
        String decryptedStr = decryptedVal.toString();
        System.assertEquals(decryptedStr, CCL_PASSWORD);
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record Created Successfully.Thank you!'));
    }
	
    private String CCL_encrypt(String value, Blob key){
        Blob data = Blob.valueOf(value);
        Blob encrypted = Crypto.encryptWithManagedIV('AES128', key, data);
        return EncodingUtil.base64encode(encrypted);
    }
}