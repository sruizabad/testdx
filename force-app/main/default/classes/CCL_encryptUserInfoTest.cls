/***************************************************************************************************************************
Apex Trigger Name : CCL_encryptUserInfoTest
Version : 1.0
Created Date : 05/02/2016	
Function :  Test class for the CCL_encryptUserInfoTest class

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Robin Wijnen					  			05/02/2016      		        		Initial Creation
***************************************************************************************************************************/

@isTest
private class CCL_encryptUserInfoTest {
	static testMethod void encryptCustomSettings() {
		CCL_DataLoader__c customSettingRecord = new CCL_DataLoader__c();
		customSettingRecord.CCL_username__c = 'user@name.com';
		customSettingRecord.CCL_password__c = 'password';
		customSettingRecord.CCL_clientID__c = 'idididididididididi';
		customSettingRecord.CCL_ClientSecret__c = 'dfsdffsddsfsfd';
		customSettingRecord.CCL_templateURL__c = 'http://google.be';
		
		insert customSettingRecord;
		
		Test.startTest();
		
		CCL_encryptUserInfo CCL_encryptInst = new CCL_encryptUserInfo();
		CCL_encryptInst.CCL_USERNAME = 'user@name.com';
		CCL_encryptInst.CCL_PASSWORD = 'password';
		CCL_encryptInst.CCL_CONSUMERKEY = 'idididididididididi';
		CCL_encryptInst.CCL_CONSUMERSECRET = 'dfsdffsddsfsfd';
		CCL_encryptInst.save();
		
		Test.stopTest();	
	}
}