/***************************************************************************************************************************
Apex Class Name :   CCL_publicGroup_InterfaceState
Version :           1.0.00
Created Date :      21/05/2018
Function :          Class handling and querying public groups.
            
Modification Log:
-----------------------------------------------------------------------------------------------------------------------------
* Developer                                 Date                                    Description
* ---------------------------------------------------------------------------------------------------------------------------
* Robin Wijnen                            21/05/2018                              Initial creation
***************************************************************************************************************************/
global class CCL_publicGroup_InterfaceState {
    @future
    webservice static void setPuplicGroups() {
        
        List<CCL_DataLoadInterface__c> dlIntLst = new List<CCL_DataLoadInterface__c>();
        List<CCL_DataLoadInterface__Share> dlIntShareLst = new List<CCL_DataLoadInterface__Share>();
        List<Id> dlIntIds = new List<Id>();
        
        dlIntShareLst = [SELECT Id, RowCause, UserOrGroup.Name, ParentId  FROM CCL_DataLoadInterface__Share WHERE RowCause = 'Manual' AND UserOrGroup.Type = 'Queue'];
        for(CCL_DataLoadInterface__Share dls : dlIntShareLst){
            dlIntIds.add(dls.ParentId);
        }
        
        dlIntLst = [SELECT Id, CCL_Public_Groups__c FROM CCL_DataLoadInterface__c WHERE Id IN :dlIntIds];
        
        for(CCL_DataLoadInterface__c cdli : dlIntLst){
            cdli.CCL_Public_Groups__c = '';
            cdli.CCL_Number_of_Public_Groups__c = 0;
        }
        
        for(CCL_DataLoadInterface__Share dlis : dlIntShareLst){
            for(CCL_DataLoadInterface__c dli : dlIntLst ){
                if(dlis.ParentId == dli.Id && dlis.UserOrGroup.Name != '' && dlis.UserOrGroup.Name != null){
                    if(dli.CCL_Public_Groups__c == ''){
                    	dli.CCL_Public_Groups__c = dlis.UserOrGroup.Name;
                        dli.CCL_Number_of_Public_Groups__c =+ 1;
                    } else {
                        dli.CCL_Public_Groups__c = dli.CCL_Public_Groups__c + ';' +dlis.UserOrGroup.Name;
                        dli.CCL_Number_of_Public_Groups__c =+ 1;
                    }
                }
            }
        }
        update dlIntLst;
    }
}