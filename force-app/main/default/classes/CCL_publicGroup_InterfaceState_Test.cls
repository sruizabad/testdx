/***************************************************************************************************************************
Apex Class Name :	CCL_publicGroup_InterfaceState_Test
Version : 			1.0
Created Date : 		29/05/2018
Function :  		Test class for Apex class CCL_publicGroup_InterfaceState

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops						  		29/05/2018      		        		Initial Class Creation       
***************************************************************************************************************************/
@isTest
public class CCL_publicGroup_InterfaceState_Test {
    
    @testSetup static void CCL_createTestData() {
        Id CCL_RTInterfaceInsert = [SELECT Id FROM Recordtype WHERE SobjectType = 'CCL_DataLoadInterface__c' AND DeveloperName = 'CCL_DataLoadInterface_Insert_Records'].Id;
        
        CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(CCL_Name__c = 'Test', CCL_Batch_Size__c = 25, CCL_Delimiter__c = ',',
                                                                                    CCL_Interface_Status__c = 'In Development', CCL_Selected_Object_Name__c = 'userterritory',
                                                                                    CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert',
                                                                                    CCL_External_Id__c = 'EXTTEST1', RecordtypeId = CCL_RTInterfaceInsert);
        insert CCL_interfaceRecord;
        System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord.Id);
        
        Group testgroup1 = new Group(Name = 'Test Group1');
        insert testgroup1;
        System.Debug('===CCL_testGroup1: ' + testgroup1.Id);
        
        Group testgroup2 = new Group(Name = 'Test Group2');
        insert testgroup2;
        System.Debug('===CCL_testGroup2: ' + testgroup2.Id);
        
        CCL_DataLoadInterface__Share CCL_interfaceShareRecord = new CCL_DataLoadInterface__Share(RowCause = 'Manual', AccessLevel = 'Edit', UserOrGroupId = testgroup1.Id, ParentId = CCL_interfaceRecord.Id);
        insert CCL_interfaceShareRecord;
        System.Debug('===CCL_interfaceShareRecord: ' + CCL_interfaceShareRecord.Id);
        
        CCL_DataLoadInterface__Share CCL_interfaceShareRecord2 = new CCL_DataLoadInterface__Share(RowCause = 'Manual', AccessLevel = 'Edit', UserOrGroupId = testgroup2.Id, ParentId = CCL_interfaceRecord.Id);
        insert CCL_interfaceShareRecord2;
        System.Debug('===CCL_interfaceShareRecord: ' + CCL_interfaceShareRecord2.Id);
    }
    
    @isTest static void CCL_testExt() {
        Test.startTest();
        CCL_publicGroup_InterfaceState.setPuplicGroups();
        Test.stopTest();
    }  
}