/***************************************************************************************************************************
Apex Class Name : 	CCL_scheduledBatchable
Version : 			1.0
Created Date : 		5/02/2015
Function :   		Creates an APEX Batch class which is scheduled for a future time.

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois		  						5/02/2015		        				Creation of class 
* Joris Artois								9/09/2015								Updated class with User Session ID.
***************************************************************************************************************************/

global without sharing class CCL_scheduledBatchable implements Schedulable{	
	private Id CCL_recordId;
	private String CCL_delimiter;
	private String CCL_textQualifier;
	private Integer CCL_batchSize;	
    private String CCL_sessionId;
	
	public CCL_scheduledBatchable(Id CCL_recordId, String CCL_delimiter, String CCL_textQualifier, Integer CCL_batchSize, string CCL_sessionId){
		this.CCL_recordId = CCL_recordId;
		this.CCL_delimiter = CCL_delimiter;
		this.CCL_textQualifier = CCL_textQualifier;
		this.CCL_batchSize = CCL_batchSize;
        this.CCL_sessionId = CCL_sessionId;
	}	
	
	/**
	*	Method to call the apex batch class and execute it.
	**/
	global void execute(SchedulableContext sc){
		CCL_batchCSVReader CCL_batch = new CCL_batchCSVreader(CCL_recordId, CCL_delimiter, CCL_textQualifier, CCL_sessionId);
		database.executebatch(CCL_batch, CCL_batchSize);
	}
}