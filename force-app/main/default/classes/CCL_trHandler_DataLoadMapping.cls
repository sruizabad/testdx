/***************************************************************************************************************************
Apex Trigger Name : CCL_trHandler_DataloadMapping
Version : 1.0
Created Date : 19/10/2015	
Function :  Handler for the triger CCL_tr_DataLoadMapping to calculate hidden fields such as the sObject name and field, before
			insert or update

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois					  			19/10/2015      		        		Initial Creation
***************************************************************************************************************************/

public class CCL_trHandler_DataLoadMapping extends CCL_TriggerHandler{
	
	public void beforeInsert(List<SObject> newSObjects, 
							 Map<ID, CCL_DataLoadInterface_DataLoadMapping__c> newSObjectsMap) {
		// Prevent recursion. Functionality is implemented by the parent class.
		if (beforeInsertHasRun()){
			return;
		}
		
		// Handler logic
		System.Debug('---> CCL_trHandler_DataLoadMapping - beforeInsert enter');
		
		// Set the object, coming from the interface
		
		CCL_dataLoadUtilityClass.CCL_SelectedObject(newSObjects);

		// Set the Field Type of the chosen field
		Map<CCL_DataLoadInterface_DataLoadMapping__c, String> errorMessages = CCL_dataLoadUtilityClass.CCL_FieldType(newSObjects);
		if(!errorMessages.isEmpty()){
			for(CCL_DataLoadInterface_DataLoadMapping__c record : errorMessages.keySet()){
				for(SObject sObjectRec : newSObjects){
					if(sObjectRec == record){
						SObject errorRec = sObjectRec;
						System.Debug('---> ERROR: ' + errorMessages.get(record));
						errorRec.addError(errorMessages.get(record));
					}
				}
			}
			return;
		}
		// Set the Mapped field name and check if the field is a possible upsert field
		CCL_dataLoadutilityClass.CCL_MappedObjectField(newSObjects);
		
		// Done
		System.Debug('---> CCL_trHandler_DataLoadMapping - beforeInsert exit');
		System.Debug(System.LoggingLevel.DEBUG, '\n -->isBeforeInsertFirstRun: ' + isBeforeInsertFirstRun);
		return;
	}
	
	public void beforeUpdate(List<SObject> oldSObjects,
							 Map<ID, SObject> oldSObjectMap,
							 List<SObject> newSObjects,
							 Map<ID, SObject> updatedSObjectsMap){
		// Prevent recursion. Functionality is implemented by the partent class.
		if (beforeUpdateHasRun()){
			return;
		}

		// Handler logic
		// Set the object, coming from the interface
		CCL_dataLoadUtilityClass.CCL_SelectedObject(newSObjects);

		// Set the Field Type of the chosen field
		Map<CCL_DataLoadInterface_DataLoadMapping__c, String> errorMessages = CCL_dataLoadUtilityClass.CCL_FieldType(newSObjects);
		if(!errorMessages.isEmpty()){
			for(CCL_DataLoadInterface_DataLoadMapping__c record : errorMessages.keySet()){
				for(SObject sObjectRec : newSObjects){
					if(sObjectRec == record){
						SObject errorRec = sObjectRec;
						System.Debug('---> ERROR: ' + errorMessages.get(record));
						errorRec.addError(errorMessages.get(record));
					}
				}
			}
			return;
		}
		// Set the Mapped field name and check if the field is a possible upsert field
		CCL_dataLoadutilityClass.CCL_MappedObjectField(newSObjects);
		
		// Done
		
		System.Debug(System.LoggingLevel.DEBUG, '\n -->isBeforeUpdateFirstRun: ' + isBeforeUpdateFirstRun);
		return;
	}
	

}