/***************************************************************************************************************************
Apex Trigger Name : CCL_trHandler_DataloadMapping_TEST
Version : 1.0
Created Date : 19/10/2015	
Function :  Test class for the CCL_trHandler_DataLoadMapping class

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois					  			19/10/2015      		        		Initial Creation
***************************************************************************************************************************/

@isTest
private class CCL_trHandler_DataLoadMapping_TEST {
	// Insert an Account Interface and a mapping to the Name field of Account
	static testMethod void beforeInsert_accountName(){
		
		// Implement Unit Test
		
		// Create and insert a new interface
		CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(
							CCL_Name__c = 'Test Interface', CCL_Batch_Size__c = 25, 
							CCL_Delimiter__c = ',', CCL_Interface_Status__c = 'In Development', 
							CCL_Selected_Object_Name__c = 'Account', CCL_Text_Qualifier__c = '"', 
							CCL_Type_Of_Upload__c = 'Insert');
							
        insert CCL_interfaceRecord;
		System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord);
		
		
		Test.startTest();
		// Create a new Mapping Record with only fields used in a DataLoad Insert
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Name = new CCL_DataLoadInterface_DataLoadMapping__c(
							CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id,
							CCL_SObject_Field__c = 'name', CCL_Column_Name__c = 'Name',
							CCL_ReferenceType__c = null, CCL_SObjectExternalId__c = null,
							CCL_Default__c = false, CCL_Default_Value__c = null);
							
		insert CCL_mappingRecord_Name;
		System.Debug('===CCL_mappingRecord_Name: ' + CCL_mappingRecord_Name);
		
		// Now check if the other hidden fields were set correctly and the mapping is there
		CCL_DataLoadInterface_DataLoadMapping__c mappingRecord_Name = 
							[SELECT CCL_Data_Load_Interface__c, CCL_Default__c, CCL_Field_Type__c,
									CCL_isReferenceToExternalId__c, CCL_isUpsertId__c, 
									CCL_ReferenceType__c, CCL_SObjectExternalId__c, 
									CCL_SObject_Mapped_Field__c, CCL_SObject_Name__c
							 FROM	CCL_DataLoadInterface_DataLoadMapping__c
							 WHERE	ID =: CCL_mappingRecord_Name.Id];
							 
		System.assert(mappingRecord_Name.CCL_Data_Load_Interface__c 		== CCL_interfaceRecord.Id);
		System.assert(mappingRecord_Name.CCL_Default__c 					== false);
		System.assert(mappingRecord_Name.CCL_Field_Type__c					== 'STRING');
		System.assert(mappingRecord_Name.CCL_isReferenceToExternalId__c		== false);
		System.assert(mappingRecord_Name.CCL_isUpsertId__c					== false);
		System.assert(mappingRecord_Name.CCL_ReferenceType__c				== null);
		System.assert(mappingRecord_Name.CCL_SObjectExternalId__c			== null);
		System.assert(mappingRecord_Name.CCL_SObject_Mapped_Field__c		== 'name');
		System.assert(mappingRecord_Name.CCL_SObject_Name__c				== 'Account');


		mappingRecord_Name.CCL_Column_Name__c = 'Namz2';
		update mappingRecord_Name;
		
		Test.stopTest();
		
		// Now check if the other hidden fields were set correctly and the mapping is there
		mappingRecord_Name = 
							[SELECT CCL_Column_Name__c
							 FROM	CCL_DataLoadInterface_DataLoadMapping__c
							 WHERE	ID =: CCL_mappingRecord_Name.Id];
							 
		System.assert(mappingRecord_Name.CCL_Column_Name__c == 'Namz2');
	}
	
	// Insert custom object Data Load Mapping and reference to Salesforce ID of Interface
	static testMethod void beforeInsert_mappingInterface(){
				
		// Implement Unit Test
		
		// Create and insert a new interface
		CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(
							CCL_Name__c = 'Test Interface', CCL_Batch_Size__c = 25, 
							CCL_Delimiter__c = ',', CCL_Interface_Status__c = 'In Development', 
							CCL_Selected_Object_Name__c = 'CCL_DataLoadInterface_DataLoadMapping__c', 
							CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert');
							
        insert CCL_interfaceRecord;
		System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord);
		
		
		Test.startTest();
		// Create a new Mapping Record with only fields used in a DataLoad Insert
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Interface = new CCL_DataLoadInterface_DataLoadMapping__c(
							CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id,
							CCL_SObject_Field__c = 'CCL_Data_Load_Interface__c', 
							CCL_Column_Name__c = 'Interface', CCL_ReferenceType__c = 'Salesforce ID', 
							CCL_SObjectExternalId__c = null, CCL_Default__c = false, 
							CCL_Default_Value__c = null);
							
		insert CCL_mappingRecord_Interface;
		System.Debug('===CCL_mappingRecord_Interface: ' + CCL_mappingRecord_Interface);
		
		// Now check if the other hidden fields were set correctly and the mapping is there
		CCL_DataLoadInterface_DataLoadMapping__c mappingRecord_Interface = 
							[SELECT CCL_Data_Load_Interface__c, CCL_Default__c, CCL_Field_Type__c,
									CCL_isReferenceToExternalId__c, CCL_isUpsertId__c, 
									CCL_ReferenceType__c, CCL_SObjectExternalId__c, 
									CCL_SObject_Mapped_Field__c, CCL_SObject_Name__c
							 FROM	CCL_DataLoadInterface_DataLoadMapping__c
							 WHERE	ID =: CCL_mappingRecord_Interface.Id];
							 
		System.assert(mappingRecord_Interface.CCL_Data_Load_Interface__c 	== CCL_interfaceRecord.Id);
		System.assert(mappingRecord_Interface.CCL_Default__c 				== false);
		System.assert(mappingRecord_Interface.CCL_Field_Type__c				== 'REFERENCE');
		System.assert(mappingRecord_Interface.CCL_isReferenceToExternalId__c== false);
		System.assert(mappingRecord_Interface.CCL_isUpsertId__c				== false);
		System.assert(mappingRecord_Interface.CCL_ReferenceType__c			== 'Salesforce ID');
		System.assert(mappingRecord_Interface.CCL_SObjectExternalId__c		== null);
		System.assert(mappingRecord_Interface.CCL_SObject_Mapped_Field__c	== 'CCL_Data_Load_Interface__c');
		System.assert(mappingRecord_Interface.CCL_SObject_Name__c			== 'CCL_DataLoadInterface_DataLoadMapping__c');

		System.debug('----> Invalid Update');
		mappingRecord_Interface.CCL_SObject_Field__c = 'Wrong';

		try {
			update mappingRecord_Interface;
		} catch(Exception CCL_e) {
			System.assert(CCL_e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
		}
		
		
		
		Test.stopTest();
		
		return;
	}
	
	
	static testMethod void beforeUpdate_ValidData_Succes(){
		// Implement Unit Test
		
		return;
	}

	// Insert custom object Data Load Mapping and reference to Salesforce ID of Interface
	static testMethod void invalidsObject(){
				
		// Implement Unit Test
		
		// Create and insert a new interface
		CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(
							CCL_Name__c = 'Test Interface', CCL_Batch_Size__c = 25, 
							CCL_Delimiter__c = ',', CCL_Interface_Status__c = 'In Development', 
							CCL_Selected_Object_Name__c = 'Test_Something__c', 
							CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert');
							
        insert CCL_interfaceRecord;
		System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord);
		
		
		Test.startTest();
		// Create a new Mapping Record with only fields used in a DataLoad Insert
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Interface = new CCL_DataLoadInterface_DataLoadMapping__c(
							CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id,
							CCL_SObject_Field__c = 'CCL_Data_Load_Interface__c', 
							CCL_Column_Name__c = 'Interface', CCL_ReferenceType__c = 'Salesforce ID', 
							CCL_SObjectExternalId__c = null, CCL_Default__c = false, 
							CCL_Default_Value__c = null);
							
		try {
			insert CCL_mappingRecord_Interface;	
		} catch(Exception CCL_e) {
			System.assert(CCL_e.getMessage().contains('Attempt to de-reference a null object'));
		}
	}

	// Insert custom object Data Load Mapping and reference to Salesforce ID of Interface
	static testMethod void invalidInterface(){
				
		// Implement Unit Test
		
		// Create and insert a new interface
		CCL_DataLoadInterface__c CCL_interfaceRecord = new CCL_DataLoadInterface__c(
							CCL_Name__c = 'Test Interface', CCL_Batch_Size__c = 25, 
							CCL_Delimiter__c = ',', CCL_Interface_Status__c = 'In Development', 
							CCL_Selected_Object_Name__c = null, 
							CCL_Text_Qualifier__c = '"', CCL_Type_Of_Upload__c = 'Insert');
							
        insert CCL_interfaceRecord;
		System.Debug('===CCL_interfaceRecord: ' + CCL_interfaceRecord);
		
		
		Test.startTest();
		// Create a new Mapping Record with only fields used in a DataLoad Insert
		CCL_DataLoadInterface_DataLoadMapping__c CCL_mappingRecord_Interface = new CCL_DataLoadInterface_DataLoadMapping__c(
							CCL_Data_Load_Interface__c = CCL_interfaceRecord.Id,
							CCL_SObject_Field__c = 'CCL_Data_Load_Interface__c', 
							CCL_Column_Name__c = 'Interface', CCL_ReferenceType__c = 'Salesforce ID', 
							CCL_SObjectExternalId__c = null, CCL_Default__c = false, 
							CCL_Default_Value__c = null);
							
		try {
			insert CCL_mappingRecord_Interface;	
		} catch(Exception CCL_e) {
			System.assert(CCL_e.getMessage().contains('Attempt to de-reference a null object'));
		}
	}
}