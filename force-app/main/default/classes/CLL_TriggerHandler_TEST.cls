/***************************************************************************************************************************
Apex Trigger Name : CCL_trHandler_DataloadMapping
Version : 1.0
Created Date : 19/10/2015	
Function :  Trigger Handler that can be used for all Trigger handler classes after example of the Simple Trigger Pattern

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois					  			19/10/2015      		        		Initial Creation
***************************************************************************************************************************/

@isTest
private class CLL_TriggerHandler_TEST {
  /**
  * ───────────────────────────────────────────────────────────────────────────────────────────────┐
  * Use one test method to ensure that all "recursion check" methods properly flip their answers
  * from FALSE, to TRUE, and that they remain TRUE for at least one more method call after detecting
  * the flip.
  * ───────────────────────────────────────────────────────────────────────────────────────────────┘
  */
  static testMethod void recursionCheckMethods_NormalOperation_Success() {
 
    CCL_TriggerHandler handler = new CCL_TriggerHandler();


    // Before Insert.
    System.AssertEquals(false,  handler.beforeInsertHasRun());
    System.AssertEquals(true,   handler.beforeInsertHasRun());
    System.AssertEquals(true,   handler.beforeInsertHasRun());


    // Before Update.
    System.AssertEquals(false,  handler.beforeUpdateHasRun());
    System.AssertEquals(true,   handler.beforeUpdateHasRun());
    System.AssertEquals(true,   handler.beforeUpdateHasRun());

    return;

  }
}