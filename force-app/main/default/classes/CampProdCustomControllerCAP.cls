/*
 * Created By:   Siddharth Jain
 * Created Date: 7/17/2014
 * Description:  Custom Contoller for the VisualForce Page used for Creation and Editing of Campgain Product
 *                 
 *              
 */


public with sharing class CampProdCustomControllerCAP{

 
  public Campaign_Product_MVN__c cs {get;set;}
  public Campaign_Product_MVN__c cs1{get; set;}
  public Boolean cs1Value{get; set;}
  public Boolean csValue{get; set;}
  public String ccid;
  ApexPages.standardController m_sc = null;
  
  public CampProdCustomControllerCAP() {
    // cs = new Campaign_Product_MVN__c();
   
  }
  public CampProdCustomControllerCAP(ApexPages.standardController sc) {
   ccid=ApexPages.currentPage().getParameters().get('id');
     System.debug('CCID'+ccid);
     if(ccid!=null){
     cs1=[SELECT Id,Campaign_MVN__c,Quantity_MVN__c,Product_MVN__c from Campaign_Product_MVN__c where id=:ccid];
     cs1Value=true;
     }else{
     cs = new Campaign_Product_MVN__c();
     csValue=true;
     }
    m_sc = sc;
     
    try
    {
    String urlVal = Apexpages.currentPage().getUrl();
    System.debug('URL is :='+urlVal);
    urlVal = urlVal.split('_lkid=')[1].substring(0,15);
    system.debug('URL ID is :='+ urlVal );
    
    Campaign_vod__c cv = [select name,id from Campaign_vod__c where id=:urlVal ];
     system.debug('Name is  :='+ cv.name);
    cs.Campaign_MVN__c = cv.id;
    
    }
    catch(Exception ase)
    {
    }
     
     
  }
  
   public PageReference doSave() {
  
  PageReference pageRef ;
          try { 
     if(cs1Value==true)
     {
     upsert cs1;
     pageRef = new PageReference('/'+cs1.Id);}
     
     else if(csValue==true){upsert cs;pageRef = new PageReference('/'+cs.Id);}
         
       
     
    } catch(System.DMLException e) {
        ApexPages.addMessages(e);
        return null;
    }   
       
         return pageRef; 
        
        
    }
public Pagereference doSaveAndNew()
  { 
      try { 
    if(cs1Value==true)upsert cs1;
     else if(csValue==true)upsert cs; 
     PageReference pg=new PageReference('/apex/CampProdCustomPageCAP');
    pg.setRedirect(true);
    return pg;        
    
    } catch(System.DMLException e) {
        ApexPages.addMessages(e);
        return null;
    }   
    
  }

  }