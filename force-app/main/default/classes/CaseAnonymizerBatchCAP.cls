/*
    * CaseAnonymizerBatchCAP
    * Created By:      Gunasagar Pradhan and Rakshit Shah
    * Created Date:    14/12/2015
    * Description:     Removes personally identifiable consumer information from Cases/Calls for countries
    *                  where this type of information cannot be stored.
    */
    global class CaseAnonymizerBatchCAP implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    /*
     * Case Anonymizer
     * When this class runs it find all Consumer or unvalidated Accounts where the Account Country Code is in a Country
     * that require anonymization (specified as a list of comma separated 
     * country codes in the Service Cloud Settings custom setting).
     *
     * If an Account falls into this category, Cases, Calls and Child Accounts related to the account are pulled.  The AccountId, ContactId, and
     * Address are removed from any Closed cases/calls and the case is also flagged as needing
     * anonymization which removes all other consumer data from the case
     * via a workflow.  If the account is no longer linked to any cases, the account and its related child accounts are deleted.
     *  
     */

    public static Boolean testSendEmail = false;   
    List<Case> failedInteractions = new List<Case>();    
    List<Id> failedAccounts = new List<Id>();
    List<Call2_vod__c> failedCalls = new List<Call2_vod__c>();   
    public static final String consumerRecordType = Service_Cloud_Settings_MVN__c.getInstance().Consumer_Record_Type_MVN__c;
    public static final Datetime excludeDate;
    public static  List<String> anonymizeCountriesList = new List<String>();
    public static  List<String> anonymizeCaseList = new List<String>();
    
    static 
    {
        Service_Cloud_Settings_MVN__c scs = Service_Cloud_Settings_MVN__c.getInstance();
        Decimal tempAccountHoldPeriod = scs.Temporary_Account_Hold_Period_Days_MVN__c;
        excludeDate = Datetime.now().addDays(tempAccountHoldPeriod.intValue() * -1);
        anonymizeCountriesList.addAll(UtilitiesMVN.splitCommaSeparatedString(scs.Interaction_Anonymize_Countries_MVN__c));
        anonymizeCaseList.addAll(UtilitiesMVN.splitCommaSeparatedString(scs.Case_Anonymizer_Record__c));
     }
            
    /************************************************************************************
        SCHEDULABLE METHOD
    ************************************************************************************/
    global void execute(SchedulableContext sc)
    {
        CaseAnonymizerBatchCAP anonymizer = new CaseAnonymizerBatchCAP();
        Database.executeBatch(anonymizer,200);
    }

    /************************************************************************************
        SCHEDULABLE METHOD INITIALIZER
    ************************************************************************************/
    public static String scheduleHourlyJob() 
    {
        CaseAnonymizerBatchCAP anonymizer = new CaseAnonymizerBatchCAP();
        String schedule = '0 0 * * * ?';
        return System.schedule('Case Anonymizer', schedule, anonymizer);    
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {    
        String query = 'SELECT Id,IsClosed,CaseNumber,Date_Closed_MVN__c,parent.Date_Closed_MVN__c,parent.Recordtype.DeveloperName,Status,parent.status,Recordtype.DeveloperName,RecordtypeID,case_Account_Email_MVN__c,Anonymize_MVN__c,Address_MVN__c,AccountId,Business_Account_MVN__c,Account__c,ContactId,ParentId,case_Account_Phone_MVN__c,Order_MVN__c,Order_MVN__r.Account_vod__c,Order_MVN__r.Ship_to_Address_vod__c,Order_MVN__r.Anonymize_MVN__c,Order_MVN__r.Override_Lock_vod__c FROM CASE WHERE RecordType.DeveloperName in:anonymizeCaseList AND Account.Country_Code_BI__c in:anonymizeCountriesList AND Account.CreatedDate <= :excludeDate AND Account.Recordtype.DeveloperName =:consumerRecordType AND Anonymize_MVN__c = false';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) 
    {
        System.debug(' \n code enters to execute method \n');
        UtilitiesMVN.isAnonymizing = true;
        List<Case> casesToClean = new List<Case>();
        map<Id,Call2_vod__c> callsToClean = new map<Id,Call2_vod__c>();
        set<Id> accountall = new set<Id>();
        List<Call2_vod__c> Allcalls = new List<Call2_vod__c>();
        List<Call2_vod__c> callstoupdate = new List<Call2_vod__c>();
        //System.debug('\n call are:'+ Allcalls + accountall+ callstoupdate  );
        
        //Filtering all the cases according to condition
        List<case> caseToAdd = new List<case>();
        for(Case ca : (List<Case>)scope){
                if(ca.Recordtype.DeveloperName == 'Request_Closed_MVN' && ca.Date_Closed_MVN__c  < excludeDate && ca.parent.Date_Closed_MVN__c < excludeDate){
                casesToClean.add(ca);
                }
                if(ca.Recordtype.DeveloperName == 'Interaction_Closed_MVN' && ca.Date_Closed_MVN__c < excludeDate){
                casesToClean.add(ca);
                }
            }
       for(case cv : casesToClean){
              accountall.add(cv.AccountId); // All Consumer Accounts list
              callsToClean.put(cv.Order_MVN__c, cv.Order_MVN__r);
            }
        Allcalls = callsToClean.Values();
        system.debug('******call to be updated'+Allcalls);
        system.debug('Call values: ' +callstoupdate );
        // Filter Accounts which dont have cases
        List <Case> accNotDel = new list <Case>();
        accNotDel = [SELECT Id,AccountId FROM Case WHERE AccountId IN : accountall and Status ='Open'];
        System.debug('Accounts with open cases size: '+ accNotDel.size() );
        set <Id> accNotDelId = new set <Id>();
        for (Case c1 : accNotDel)
        { 
         accNotDelId.add(c1.AccountId);
       // System.debug('Accounts with open cases  : '+  c1.AccountId );
        }
       List <Case> accDel = new list <Case>();
       accDel = [SELECT Id, AccountId  FROM Case WHERE AccountId  IN : accountall AND AccountId NOT IN : accNotDelId];
       System.debug('Accounts with closed cases :'+ accDel.size() );
       List<id> accToDel = new LIst<id>();
       for(Case caseAcc : accDel)
       {
          accToDel.add(caseAcc.AccountId);
          system.debug('*****final accounts to be deleted are:'+accToDel);
       }
       List<case> caseCleaned = new List<case>();
       for(Case cs : casesToClean)
       {
       cs.AccountId = Null;
       cs.ContactId = Null;
       cs.Address_MVN__c = Null;
       cs.case_Account_Email_MVN__c = Null;
       cs.case_Account_Phone_MVN__c =  Null;
       cs.Anonymize_MVN__c = TRUE;
       caseCleaned.add(cs);
       }
       

        // Anonymize Cases
        Database.SaveResult[] saveResults = Database.update(caseCleaned, false);
        System.debug('CASE CLEAN UPDATE RESULTS: ' + saveResults);
        for (Integer i=0; i<saveResults.size(); i++) 
        {
            if (!saveResults[i].isSuccess() || testSendEmail)
            {
                failedInteractions.add(caseCleaned[i]);
            }
        }
        
        //Clean Calls
        for (Call2_vod__c call : Allcalls) 
        {
          if (call!= null)
          {
            call.Account_vod__c = null;
            call.Ship_to_Address_vod__c = null;
            call.Override_Lock_vod__c = true;
            call.Anonymize_MVN__c = true;
            callstoupdate.add(call);
          }
        }
        //Update Calls
        Database.SaveResult[] updateCalls = Database.update(callstoupdate , false);
        for (Integer i=0; i<updateCalls.size(); i++)
        {
            if (!updateCalls[i].isSuccess() || testSendEmail)
            {
                failedCalls.add(callstoupdate[i]);
            }
        }
       

        // Delete anonymous accounts
        Database.DeleteResult[] deleteAccounts = Database.delete(accToDel, false);
        for (Integer i=0; i<deleteAccounts.size(); i++)
        {
            if (!deleteAccounts[i].isSuccess() || testSendEmail)
            {
                failedAccounts.add(accToDel[i]);
            }
        }
        
        
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        if (!failedInteractions.isEmpty()  || !failedAccounts.isEmpty() || !failedCalls.isEmpty()) 
        {
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String adminEmailAddress = Service_Cloud_Settings_MVN__c.getInstance().Administrator_Email_MVN__c;
            mail.setToAddresses(new String[] {adminEmailAddress});
            mail.setSenderDisplayName('Case Anonymizer Error');
            mail.setSubject('Error(s) in the Case Anonymizer Job');
            mail.setBccSender(false);
            mail.setUseSignature(false);
            String plainTextBody = '';
            
            if(failedInteractions.size() > 0)
            {
                plainTextBody += '\nThe following Cases could not be Anonymized:\n';
                for (Case c : failedInteractions)
                {
                    plainTextBody += 'Case Id: ' + c.Id + ' ---- Case Number: ' + c.CaseNumber + '\n';
                }
            }
            if(failedAccounts.size() > 0)
            {
                plainTextBody += '\nThe following Accounts could not be deleted:\n';
                for (Account acct : [select Id, Name FROM Account WHERE Id in :failedAccounts])
                {
                    plainTextBody += 'Account Id: ' + acct.Id + ' ---- Account Name: ' + acct.Name + '\n';
                }
            }
            if(failedCalls.size() > 0){
                plainTextBody += '\nThe following Calls could not be updated:\n';
                for (Call2_vod__c failedCall : failedCalls) {
                    plainTextBody += 'Call Id: ' + failedCall.Id + '\n';
                }  
            }
            mail.setPlainTextBody(plainTextBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } 
    }
}