/*
 * CaseAnonymizerBatchMVN
 * Created By:      Thomas Hajcak
 * Created Date:    6/7/2013
 * Description:     Removes personally identifiable consumer information from Cases/Calls for countries
 *                  where this type of information cannot be stored.
 */
global class CaseAnonymizerBatchMVN implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    /*
     * Case Anonymizer
     * When this class runs it find all Consumer or unvalidated Accounts where the Account Country Code is in a Country
     * that require anonymization (specified as a list of comma separated 
     * country codes in the Service Cloud Settings custom setting).
     *
     * If an Account falls into this category, Cases, Calls and Child Accounts related to the account are pulled.  The AccountId, ContactId, and
     * Address are removed from any Closed cases/calls and the case is also flagged as needing
     * anonymization which removes all other consumer data from the case
     * via a workflow.  If the account is no longer linked to any cases, the account and its related child accounts are deleted.
     *  
     */

    public static Boolean testSendEmail = false;
    
    List<Case> failedInteractions = new List<Case>();    
    List<Id> failedAccounts = new List<Id>();
    List<Call2_vod__c> failedCalls = new List<Call2_vod__c>();
    List<Id> failedEmailMessages = new List<Id>();
    List<Id> failedTasks = new List<Id>();
    List<Id> failedChildAccounts = new List<Id>();
    
    public List<String> anonymizeCountriesList = new List<String>();
    public static final String consumerRecordType = Service_Cloud_Settings_MVN__c.getInstance().Consumer_Record_Type_MVN__c;
    public static final String hcpRecordType = Service_Cloud_Settings_MVN__c.getInstance().HCP_Record_Type_MVN__c;
    public static final String businessRecordType = Service_Cloud_Settings_MVN__c.getInstance().Business_Account_Record_Type_MVN__c;
    public static final Datetime excludeDate;
    public static final Set<String> invalidStatusCodes = new Set<String>();

    static {
        Service_Cloud_Settings_MVN__c scs = Service_Cloud_Settings_MVN__c.getInstance();
        Decimal tempAccountHoldPeriod = scs.Temporary_Account_Hold_Period_Days_MVN__c;
        excludeDate = Datetime.now().addDays(tempAccountHoldPeriod.intValue() * -1);

        invalidStatusCodes.addAll(UtilitiesMVN.splitCommaSeparatedString(scs.Interaction_Anonymize_Status_Codes_MVN__c));
    }

    public final List<String> closedStatuses;
    
    /************************************************************************************
        SCHEDULABLE METHOD
    ************************************************************************************/
    global void execute(SchedulableContext sc) {
        CaseAnonymizerBatchMVN anonymizer = new CaseAnonymizerBatchMVN();
        Database.executeBatch(anonymizer);
    }

    /************************************************************************************
        SCHEDULABLE METHOD INITIALIZER
    ************************************************************************************/
    public static String scheduleHourlyJob() {
        CaseAnonymizerBatchMVN anonymizer = new CaseAnonymizerBatchMVN();
        String schedule = '0 0 * * * ?';
        return System.schedule('Case Anonymizer', schedule, anonymizer);
    }
    
    global CaseAnonymizerBatchMVN() {        
        String countriesString = Service_Cloud_Settings_MVN__c.getInstance().Interaction_Anonymize_Countries_MVN__c;        

        if (countriesString != null) {
            anonymizeCountriesList = UtilitiesMVN.splitCommaSeparatedString(countriesString);
        }
        for(Integer x=0; x<anonymizeCountriesList.size(); x++){
            if(anonymizeCountriesList[x] != null){
                anonymizeCountriesList[x] = anonymizeCountriesList[x].trim();
            }
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Service_Cloud_Settings_MVN__c scs = Service_Cloud_Settings_MVN__c.getInstance();

        String query = 'SELECT Id, ' +
                               '(SELECT Id, CaseNumber, AccountId, ContactId, Address_MVN__c, Anonymize_MVN__c, Status, IsClosed, Date_Closed_MVN__c FROM Cases), ' +
                               '(SELECT Id, CaseNumber, AccountId, ContactId, Address_MVN__c, Anonymize_MVN__c, Status, IsClosed, Date_Closed_MVN__c FROM Cases1__r), ' +
                               '(SELECT Id, Account_vod__c, Override_Lock_vod__c, Anonymize_MVN__c, Ship_to_Address_vod__c FROM Call2_vod__r) ' +
                        'FROM Account ' +
                        'WHERE Country_Code_BI__c in :anonymizeCountriesList ' +
                               'AND CreatedDate <= :excludeDate ' +
                               'AND (' +
                                  '(' +
                                    '(RecordType.DeveloperName = :hcpRecordType OR RecordType.DeveloperName = :businessRecordType) ' +
                                    'AND OK_Status_Code_BI__c IN :invalidStatusCodes' +
                                  ') ' +                                  
                                  'OR RecordType.DeveloperName = :consumerRecordType ' +                                  
                               ')';
        System.debug('\n\n\nQuery String is: \n' + query + '\n\n\n');                      

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        UtilitiesMVN.isAnonymizing = true;      
        
        List<Id> accountIdsToDelete = new List<Id>();
        List<Case> casesToClean = new List<Case>();
        List<Call2_vod__c> callsToClean = new List<Call2_vod__c>();

        for(Account account : (List<Account>)scope){
            Boolean allClosed = true;
            List<Case> casesToAdd = new List<Case>();

            //check each case. If it isn't closed or wasn't closed far enough in the past...
            for(Case cs : account.Cases){
                if(!cs.isClosed || cs.Date_Closed_MVN__c > excludeDate){
                    allClosed = false;
                    break;
                }
                else {
                    casesToAdd.add(cs);
                }
            }

            if(allClosed){
                for(Case cs : account.Cases1__r){
                    if(!cs.isClosed || cs.Date_Closed_MVN__c > excludeDate){
                        allClosed = false;
                        break;
                    }
                }
            }

            if(allClosed){
                accountIdsToDelete.add(account.Id);
                casesToClean.addAll(casesToAdd);
                callsToClean.addAll(account.Call2_vod__r);
            }
        }

        // Clean Cases
        for (Case cs : casesToClean) {
            cs.AccountId = null;
            cs.ContactId = null;
            cs.Address_MVN__c = null;
            cs.Anonymize_MVN__c = true;            
        }
        
        // Anonymize Cases
        Database.SaveResult[] saveResults = Database.update(casesToClean, false);
        System.debug('CASE CLEAN UPDATE RESULTS: ' + saveResults);
        for (Integer i=0; i<saveResults.size(); i++) {
            if (!saveResults[i].isSuccess() || testSendEmail) {
                failedInteractions.add(casesToClean[i]);
            }
        }
        
        // Clean Calls
        for (Call2_vod__c call : callsToClean) {
            call.Account_vod__c = null;
            call.Ship_to_Address_vod__c = null;
            call.Override_Lock_vod__c = true;
            call.Anonymize_MVN__c = true;
        }

        // update calls
        Database.SaveResult[] updateCalls = Database.update(callsToClean, false);
        for (Integer i=0; i<updateCalls.size(); i++) {
            if (!updateCalls[i].isSuccess() || testSendEmail) {
                failedCalls.add(callsToClean[i]);
            }
        }

        // Delete the Child Accounts related to the Accounts to delete
        List<Child_Account_vod__c> childAccountsToDelete = [select Id from Child_Account_vod__c where Child_Account_vod__c in :accountIdsToDelete
                                                                or Parent_Account_vod__c in :accountIdsToDelete or (OK_Status_Code_BI__c IN :invalidStatusCodes
                                                                and CreatedDate <= :excludeDate)];
        // Delete anonymous accounts
        Database.DeleteResult[] deleteChildAccounts = Database.delete(childAccountsToDelete, false);
        for (Integer i=0; i<deleteChildAccounts.size(); i++) {
            if (!deleteChildAccounts[i].isSuccess() || testSendEmail) {
                failedChildAccounts.add(childAccountsToDelete[i].Id);
            }
        }

        // Delete anonymous accounts
        Database.DeleteResult[] deleteAccounts = Database.delete(accountIdsToDelete, false);
        for (Integer i=0; i<deleteAccounts.size(); i++) {
            if (!deleteAccounts[i].isSuccess() || testSendEmail) {
                failedAccounts.add(accountIdsToDelete[i]);
            }
        }
        
        List<EmailMessage> emailMessagesToDelete = [select Id from EmailMessage where ParentId in :casesToClean];
        
        // Delete email messages
        Database.DeleteResult[] deleteEmailMessages = Database.delete(emailMessagesToDelete, false);
        for (Integer i=0; i<deleteEmailMessages.size(); i++) {
            if (!deleteEmailMessages[i].isSuccess() || testSendEmail) {
                failedEmailMessages.add(deleteEmailMessages[i].Id);
            }
        }
        
        List<Task> tasksToDelete = [select Id from Task where WhatId in :casesToClean];
        
        // Delete tasks
        Database.DeleteResult[] deleteTasks = Database.delete(tasksToDelete, false);
        for (Integer i=0; i<deleteTasks.size(); i++) {
            if (!deleteEmailMessages[i].isSuccess() || testSendEmail) {
                failedTasks.add(deleteTasks[i].Id);
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        if (!failedInteractions.isEmpty()  || !failedAccounts.isEmpty() ||
            !failedEmailMessages.isEmpty() || !failedTasks.isEmpty()) {
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            String adminEmailAddress = Service_Cloud_Settings_MVN__c.getInstance().Administrator_Email_MVN__c;
            mail.setToAddresses(new String[] {adminEmailAddress});
            mail.setSenderDisplayName('Case Anonymizer Error');
            mail.setSubject('Error(s) in the Case Anonymizer Job');
            mail.setBccSender(false);
            mail.setUseSignature(false);

            String plainTextBody = '';
            
            if(failedInteractions.size() > 0){
                plainTextBody += '\nThe following Cases could not be Anonymized:\n';
                for (Case c : failedInteractions) {
                    plainTextBody += 'Case Id: ' + c.Id + ' ---- Case Number: ' + c.CaseNumber + '\n';
                }
            }
            if(failedChildAccounts.size() > 0){
                plainTextBody += '\nThe following Child Accounts could not be deleted:\n';
                for (Child_Account_vod__c childAccount : [select Id, Name from Child_Account_vod__c where Id in :failedChildAccounts]) {
                    plainTextBody += 'Child Account Id: ' + childAccount.Id + ' ---- Child Account Name: ' + childAccount.Name + '\n';
                }
            }
            if(failedAccounts.size() > 0){
                plainTextBody += '\nThe following Accounts could not be deleted:\n';
                for (Account acct : [select Id, Name from Account where Id in :failedAccounts]) {
                    plainTextBody += 'Account Id: ' + acct.Id + ' ---- Account Name: ' + acct.Name + '\n';
                }
            }
            if(failedCalls.size() > 0){
                plainTextBody += '\nThe following Calls could not be updated:\n';
                for (Call2_vod__c failedCall : failedCalls) {
                    plainTextBody += 'Call Id: ' + failedCall.Id + '\n';
                }  
            }
            if(failedEmailMessages.size() > 0){
                plainTextBody += '\nThe following Email Messages could not be deleted:\n';
                for (Id emailMessageId : failedEmailMessages) {
                    plainTextBody += 'Email Message Id: ' + emailMessageId + '\n';
                }
            }
            if(failedTasks.size() > 0){
                plainTextBody += '\nThe following Tasks could not be deleted:\n';
                for (Id failedTaskId : failedTasks) {
                    plainTextBody += 'Task Id: ' + failedTaskId + '\n';
                }
            }
            
            mail.setPlainTextBody(plainTextBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } 
    }
    
}