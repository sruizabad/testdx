/*
 * CaseAnonymizerBatchMVN
 * Created By:      Thomas Hajcak
 * Created Date:    6/10/2013
 * Updated By:      Kyle Thornton
 * Updated Date:    9/12/2013
 * Description:     This is a test class for CaseAnonymizerBatchTestMVN.
 *                  Updated to add new unit tests for Invalid HCPs, Temporary HCPs and remove fulfillments
 */
@isTest
private class CaseAnonymizerBatchTestMVN {

    static List<Case> interactions;
    
    static {
        TestDataFactoryMVN.createSettings();
        
        interactions = TestDataFactoryMVN.createInteractions();
    }

    @isTest static void verifyScheduleJobWorks() {  
        String jobId;
        Test.startTest();
        try {
            jobId = CaseCloserBatchMVN.scheduleHourlyJob();
        } catch(System.AsyncException ex) {
          System.debug('Job already scheduled');
          return;
        }
        Test.stopTest();

        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
    
        //The next execution should be at the top of the hour. Need to create that time stampe to compare to and account 
        //for the rare chance that the tests execute between 23:00 and 0:00 GMT
        Integer hour = DateTime.now().hour() == 23 ? 0 : DateTime.now().hour() + 1;
        Date nextDate = DateTime.now().hour() == 23 ? Date.today() + 1 : Date.today();
        Time nextHour = Time.newInstance(hour, 0, 0, 0 );
         
        System.assert(ct.NextFireTime == DateTime.newInstance(nextDate, nextHour), 'Job not scheduled for the next hour');
        System.assert(0 == ct.TimesTriggered, 'Batch job already fired... not good');
    }

    @isTest static void verifyAnonymizerScheduleJobWorks() {  
        String jobId;
        Test.startTest();
        try {
            jobId = CaseAnonymizerBatchMVN.scheduleHourlyJob();
        } catch(System.AsyncException ex) {
             System.debug('Job already scheduled');
          return;
        }
        Test.stopTest();

        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
    
        //The next execution should be at the top of the hour. Need to create that time stampe to compare to and account 
        //for the rare chance that the tests execute between 23:00 and 0:00 GMT
        Integer hour = DateTime.now().hour() == 23 ? 0 : DateTime.now().hour() + 1;
        Date nextDate = DateTime.now().hour() == 23 ? Date.today() + 1 : Date.today();
        Time nextHour = Time.newInstance(hour, 0, 0, 0 );
         
        System.assert(ct.NextFireTime == DateTime.newInstance(nextDate, nextHour), 'Job not scheduled for the next hour');
        System.assert(0 == ct.TimesTriggered, 'Batch job already fired... not good');
    }

    @isTest static void verifyConsumer_Anonymize_Delete() {
        Account consumerAccount = TestDataFactoryMVN.createTestConsumer();
        consumerAccount = [SELECT Id, PersonContactId, Country_Code_BI__c FROM Account WHERE Id = :consumerAccount.Id];
        consumerAccount.Country_Code_BI__c = 'DE';
        update consumerAccount;
        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(consumerAccount);

        for (Case interaction : interactions) {
            interaction.AccountId = consumerAccount.Id;
            interaction.ContactId = consumerAccount.PersonContactId;
            interaction.Address_MVN__c = testAddress.Id;
            interaction.Status = 'Closed';
        }
        update interactions;

        Test.startTest();
        Database.executeBatch(new CaseAnonymizerBatchMVN());
        Test.stopTest();

        interactions = [SELECT Id, AccountId, ContactId, Address_MVN__c FROM Case where Id IN :interactions];

        for (Case interaction : interactions) {
            System.debug('INTERACTION: ' + interaction);
            system.AssertEquals(null, interaction.AccountId);
            system.AssertEquals(null, interaction.ContactId);
            system.AssertEquals(null, interaction.Address_MVN__c);
        }

        Integer accountCount = [SELECT COUNT() FROM Account WHERE Id = :consumerAccount.Id];
       // System.AssertEquals(0, accountCount);
    }
    
    @isTest static void verifyConsumer_Delete_With_Email_And_Task() {
        Account consumerAccount = TestDataFactoryMVN.createTestConsumer();
        consumerAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :consumerAccount.Id];
        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(consumerAccount);

        List<EmailMessage> emailMessages = new List<EmailMessage>();
        List<Task> tasks = new List<Task>();
        List<Fulfillment_MVN__c> fulfillments = new List<Fulfillment_MVN__c>();
        for (Case interaction : interactions) {
            interaction.AccountId = consumerAccount.Id;
            interaction.ContactId = consumerAccount.PersonContactId;
            interaction.Address_MVN__c = testAddress.Id;
            
            emailMessages.add(new EmailMessage(ParentId = interaction.Id, ToAddress = 'sample'));
            tasks.add(new Task(Subject='Test', WhatId = interaction.Id));
            
            fulfillments.add(new Fulfillment_MVN__c(Case_MVN__c = interaction.Id, 
                                                    Account_Name_MVN__c = interaction.AccountId, 
                                                    Status_MVN__c='Closed'));
        }
        update interactions;
        insert fulfillments;
        insert emailMessages;
        insert tasks;
        
        for(Case interaction : interactions){
            interaction.Status = 'Closed';
        }
        update interactions;

        Test.startTest();
        Database.executeBatch(new CaseAnonymizerBatchMVN());
        Test.stopTest();

        interactions = [SELECT Id, AccountId, ContactId, Address_MVN__c, 
                            (select Id, Account_Name_MVN__c from Fulfillment__r),
                            (select Id from Tasks),
                            (select Id from EmailMessages) FROM Case where Id IN :interactions];

        for (Case interaction : interactions) {
            system.AssertEquals(null, interaction.AccountId);
            system.AssertEquals(null, interaction.ContactId);
            system.AssertEquals(null, interaction.Address_MVN__c);
            
            for(Fulfillment_MVN__c fulfill:interaction.Fulfillment__r){
               // system.AssertEquals(null, fulfill.Account_Name_MVN__c);
            }
            
            system.AssertEquals(0, interaction.Tasks.size());
            system.AssertEquals(0, interaction.EmailMessages.size());
        }

        Integer accountCount = [SELECT COUNT() FROM Account WHERE Id = :consumerAccount.Id];
       // System.AssertEquals(0, accountCount);
    }
    
    @isTest static void verifyNonConsumer_NoAnonymize_NoDelete() {
        Account hcpAccount = TestDataFactoryMVN.createTestHCP();
        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(hcpAccount);
        hcpAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :hcpAccount.Id];

        for (Case interaction : interactions) {
            interaction.AccountId = hcpAccount.Id;
            interaction.ContactId = hcpAccount.PersonContactId;
            interaction.Address_MVN__c = testAddress.Id;
            interaction.Status = 'Closed';
        }
        update interactions;

        Test.startTest();
        Database.executeBatch(new CaseAnonymizerBatchMVN());
        Test.stopTest();

        interactions = [SELECT Id, AccountId, ContactId, Address_MVN__c FROM Case where Id IN :interactions];

        for (Case interaction : interactions) {
            system.AssertEquals(hcpAccount.Id, interaction.AccountId);
            system.AssertEquals(hcpAccount.PersonContactId, interaction.ContactId);
            system.AssertEquals(testAddress.Id, interaction.Address_MVN__c);
        }

        Integer accountCount = [SELECT COUNT() FROM Account WHERE Id = :hcpAccount.Id];
        System.AssertNotEquals(0, accountCount);
    }

    @isTest static void verifyNotClosed_NoAnonymize_NoDelete() {
        Account consumerAccount = TestDataFactoryMVN.createTestConsumer();
        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(consumerAccount);
        consumerAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :consumerAccount.Id];

        for (Case interaction : interactions) {
            interaction.AccountId = consumerAccount.Id;
            interaction.ContactId = consumerAccount.PersonContactId;
            interaction.Address_MVN__c = testAddress.Id;
        }
        update interactions;

        Test.startTest();
        Database.executeBatch(new CaseAnonymizerBatchMVN());
        Test.stopTest();

        interactions = [SELECT Id, AccountId, ContactId, Address_MVN__c FROM Case where Id IN :interactions];

        for (Case interaction : interactions) {
            system.AssertEquals(consumerAccount.Id, interaction.AccountId);
            system.AssertEquals(consumerAccount.PersonContactId, interaction.ContactId);
            system.AssertEquals(testAddress.Id, interaction.Address_MVN__c);
        }

        Integer accountCount = [SELECT COUNT() FROM Account WHERE Id = :consumerAccount.Id];
        System.AssertNotEquals(0, accountCount);
    }

    @isTest static void verifyNotAllClosed_AnonymizeSome_NoDelete() {
        Account consumerAccount = TestDataFactoryMVN.createTestConsumer();
        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(consumerAccount);
        consumerAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :consumerAccount.Id];

        Boolean closeCase = false;
        Id openCaseId;
        for (Case interaction : interactions) {
            interaction.AccountId = consumerAccount.Id;
            interaction.ContactId = consumerAccount.PersonContactId;
            interaction.Address_MVN__c = testAddress.Id;
            if (closeCase) {
                interaction.Status = 'Closed';
                openCaseId = interaction.Id;
            }
        }
        update interactions;

        Test.startTest();
        Database.executeBatch(new CaseAnonymizerBatchMVN());
        Test.stopTest();

        interactions = [SELECT Id, AccountId, ContactId, Address_MVN__c FROM Case where Id IN :interactions];

        for (Case interaction : interactions) {
            if (interaction.Id == openCaseId) {
                system.AssertEquals(consumerAccount.Id, interaction.AccountId);
                system.AssertEquals(consumerAccount.PersonContactId, interaction.ContactId);
                system.AssertEquals(testAddress.Id, interaction.Address_MVN__c);
            } else {
                system.AssertEquals(consumerAccount.Id, interaction.AccountId);
                system.AssertEquals(consumerAccount.PersonContactId, interaction.ContactId);
                system.AssertEquals(testAddress.Id, interaction.Address_MVN__c);
            }
        }

        Integer accountCount = [SELECT COUNT() FROM Account WHERE Id = :consumerAccount.Id];
        System.AssertNotEquals(0, accountCount);
    }

    @isTest static void verify_Invalid_HCP_Gets_Anonymized() {
        Account hcp = TestDataFactoryMVN.createTestHCP();
        hcp = [SELECT Id, PersonContactId, OK_Status_Code_BI__c FROM Account WHERE Id = :hcp.id];
        hcp.OK_Status_Code_BI__c = 'Rejected';
        hcp.Country_Code_BI__c = 'DE';
        update hcp;

        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(hcp);

        for (Case interaction : interactions) {
            interaction.AccountId = hcp.Id;
            interaction.ContactId = hcp.PersonContactId;
            interaction.Address_MVN__c = testAddress.Id;
            interaction.Status = 'Closed';
        }
        update interactions;

        Test.startTest();
        Database.executeBatch(new CaseAnonymizerBatchMVN());
        Test.stopTest();

        interactions = [SELECT Id, AccountId, ContactId, Address_MVN__c FROM Case where Id IN :interactions];

        for (Case interaction : interactions) {            
            system.AssertEquals(null, interaction.AccountId);
            system.AssertEquals(null, interaction.ContactId);
            system.AssertEquals(null, interaction.Address_MVN__c);
        }

        Integer accountCount = [SELECT COUNT() FROM Account WHERE Id = :hcp.Id];
        //System.AssertEquals(0, accountCount);

    }

    @isTest static void verify_Invalid_Business_Gets_Anonymized() {
        Account business = TestDataFactoryMVN.createTestBusinessAccount();
        business = [SELECT Id, PersonContactId, OK_Status_Code_BI__c FROM Account WHERE Id = :business.id];
        business.OK_Status_Code_BI__c = 'Rejected';
        business.Country_Code_BI__c = 'DE';
        update business;

        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(business);

        for (Case interaction : interactions) {
            interaction.AccountId = business.Id;
            interaction.Business_Account_MVN__c = business.Id;
            interaction.Address_MVN__c = testAddress.Id;
            interaction.Status = 'Closed';
        }
        update interactions;

        Test.startTest();
        Database.executeBatch(new CaseAnonymizerBatchMVN());
        Test.stopTest();

        interactions = [SELECT Id, AccountId, ContactId, Address_MVN__c FROM Case where Id IN :interactions];

        for (Case interaction : interactions) {            
            system.AssertEquals(null, interaction.AccountId);
            system.AssertEquals(null, interaction.ContactId);
            system.AssertEquals(null, interaction.Address_MVN__c);
        }

        //System.AssertEquals(0, [SELECT COUNT() FROM Account WHERE Id = :business.Id]);
        //System.assertEquals(0, [SELECT COUNT() FROM Address_vod__c WHERE Id = :testAddress.Id]);
    }

    @isTest static void exerciseSendEmailFunctionaltiy() {
        Account consumerAccount = TestDataFactoryMVN.createTestConsumer();
        consumerAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :consumerAccount.Id];
        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(consumerAccount);

        List<EmailMessage> emailMessages = new List<EmailMessage>();
        List<Task> tasks = new List<Task>();
        List<Fulfillment_MVN__c> fulfillments = new List<Fulfillment_MVN__c>();
        for (Case interaction : interactions) {
            interaction.AccountId = consumerAccount.Id;
            interaction.ContactId = consumerAccount.PersonContactId;
            interaction.Address_MVN__c = testAddress.Id;
            
            emailMessages.add(new EmailMessage(ParentId = interaction.Id,ToAddress = 'sample'));
            tasks.add(new Task(Subject='Test', WhatId = interaction.Id));
            
            fulfillments.add(new Fulfillment_MVN__c(Case_MVN__c = interaction.Id, 
                                                    Account_Name_MVN__c = interaction.AccountId, 
                                                    Status_MVN__c='Closed'));
        }
        Test.startTest();
        update interactions;
        insert fulfillments;
        insert emailMessages;
        insert tasks;

        insert new Call2_vod__c(Account_vod__c = consumerAccount.id, Status_vod__c='Submitted_vod__c');
        
        for(Case interaction : interactions){
            interaction.Status = 'Closed';
        }
        update interactions;

        //Test.startTest();
        CaseAnonymizerBatchMVN batch = new CaseAnonymizerBatchMVN();
        CaseAnonymizerBatchMVN.testSendEmail = true;
        Database.executeBatch(batch);
        Test.stopTest();
    }

    @isTest static void validate_Accounts_with_calls_get_anonymized() {
        Account hcp = TestDataFactoryMVN.createTestHCP();
        hcp = [SELECT Id, PersonContactId, OK_Status_Code_BI__c FROM Account WHERE Id = :hcp.id];
        hcp.OK_Status_Code_BI__c = 'Rejected';
        hcp.Country_Code_BI__c = 'DE';
        update hcp;

        List<Case> requests = TestDataFactoryMVN.createRequests(interactions, 'Open');
        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(hcp);
        
        List<Call2_vod__c> calls = new List<Call2_vod__c>();
        for (Case request : requests) {
            Call2_vod__c testCall = new Call2_vod__c(
                Account_vod__c = hcp.id,
                Status_vod__c = 'Submitted_vod__c',
                Ship_to_Name_MVN__c = 'Test Name',
                Ship_Address_Line_1_vod__c = 'ship to address line 1',
                Ship_Address_Line_2_vod__c = 'ship to address line 2',
                Ship_Address_Line_3_MVN__c = 'ship to address line 3',
                Ship_Address_Line_4_MVN__c = 'ship to address line 4',
                Ship_City_vod__c = 'ship to city',
                Ship_State_vod__c = 'ship to state',
                Ship_Country_vod__c = 'ship to country',
                Ship_Zip_vod__c = 'ship zip',
                Ship_To_Address_vod__c = testAddress.id
            );
            calls.add(testCall);
        }

        insert calls;
        for (Integer i=0; i<calls.size(); i++) {
            requests[i].Order_MVN__c = calls[0].id;
            requests[i].Status = 'Closed';
        }

        update requests;

        for (Case interaction : interactions) {
            interaction.AccountId = hcp.Id;
            interaction.ContactId = hcp.PersonContactId;
            interaction.Address_MVN__c = testAddress.Id;
            interaction.Status = 'Closed';
        }
        update interactions;

        Test.startTest();
        Database.executeBatch(new CaseAnonymizerBatchMVN());
        Test.stopTest();

        interactions = [SELECT Id, AccountId, ContactId, Address_MVN__c, 
                            (SELECT Order_MVN__r.Ship_Address_Line_1_vod__c, Order_MVN__r.Ship_Address_Line_2_vod__c, 
                                    Order_MVN__r.Ship_Address_Line_3_MVN__c, Order_MVN__r.Ship_Address_Line_4_MVN__c, 
                                    Order_MVN__r.Ship_City_vod__c, 
                                    Order_MVN__r.Ship_State_vod__c, Order_MVN__r.Ship_Country_vod__c, 
                                    Order_MVN__r.Ship_Zip_vod__c, Order_MVN__r.Ship_To_Address_vod__c, 
                                    Order_MVN__r.Ship_To_Address_Text_vod__c, Order_MVN__r.Ship_to_Name_MVN__c,
                                    Order_MVN__r.Account_vod__c
                            FROM Cases) 
                            FROM Case where Id IN :interactions];

        //(SELECT Ship_to_Name_MVN__c, Ship_Address_Line_1_vod__c, Ship_Address_Line_2_vod__c, Ship_Address_Line_3_MVN__c, Ship_Address_Line_4_MVN__c, Ship_City_vod__c, Ship_State_vod__c, Ship_Country_vod__c, Ship_Zip_vod__c, Ship_To_Address_vod__c, Ship_To_Address_Text_vod__c from Call2_vod__r)
        for (Case interaction : interactions) {            
            system.AssertEquals(null, interaction.AccountId);
            system.AssertEquals(null, interaction.ContactId);
            system.AssertEquals(null, interaction.Address_MVN__c);
            for (Case request : interaction.Cases){
                system.AssertEquals(null, request.Order_MVN__r.Ship_To_Address_vod__c);
                system.AssertEquals(null, request.Order_MVN__r.Account_vod__c);
                system.AssertEquals(null, request.Order_MVN__r.Ship_to_Name_MVN__c);
                system.AssertEquals(null, request.Order_MVN__r.Ship_Address_Line_1_vod__c);
                system.AssertEquals(null, request.Order_MVN__r.Ship_Address_Line_2_vod__c);
                system.AssertEquals(null, request.Order_MVN__r.Ship_Address_Line_3_MVN__c);
                system.AssertEquals(null, request.Order_MVN__r.Ship_Address_Line_4_MVN__c);
                system.AssertEquals(null, request.Order_MVN__r.Ship_City_vod__c);
                system.AssertEquals(null, request.Order_MVN__r.Ship_State_vod__c);
                system.AssertEquals(null, request.Order_MVN__r.Ship_Country_vod__c);
                system.AssertEquals(null, request.Order_MVN__r.Ship_Zip_vod__c);                
            }
        }

        Integer accountCount = [SELECT COUNT() FROM Account WHERE Id = :hcp.Id];
       // System.AssertEquals(0, accountCount);
    }

    @isTest static void validate_accounts_younger_than_hold_period_do_not_get_deleted() {
        //set the hold period to something in the past, for tests, we set to -1 by default
        Service_Cloud_Settings_MVN__c scs = [select Id, Temporary_Account_Hold_Period_Days_MVN__c from Service_Cloud_Settings_MVN__c where SetupOwnerId = :UserInfo.getOrganizationId() limit 1];
        scs.Temporary_Account_Hold_Period_Days_MVN__c = 15;
        update scs;

        Account business = TestDataFactoryMVN.createTestBusinessAccount();
        business = [SELECT Id, PersonContactId, OK_Status_Code_BI__c FROM Account WHERE Id = :business.id];
        business.OK_Status_Code_BI__c = 'Rejected';
        business.Country_Code_BI__c = 'DE';
        update business;

        Address_vod__c testAddress = TestDataFactoryMVN.createTestAddress(business);

        for (Case interaction : interactions) {
            interaction.AccountId = business.Id;
            interaction.ContactId = business.PersonContactId;
            interaction.Address_MVN__c = testAddress.Id;
            interaction.Status = 'Closed';
        }
        update interactions;

        Test.startTest();
        Database.executeBatch(new CaseAnonymizerBatchMVN());
        Test.stopTest();

        interactions = [SELECT Id, AccountId, ContactId, Address_MVN__c FROM Case where Id IN :interactions];

        for (Case interaction : interactions) {            
            system.AssertEquals(business.Id, interaction.AccountId);            
            system.AssertEquals(testAddress.id, interaction.Address_MVN__c);
        }

        System.AssertEquals(1, [SELECT COUNT() FROM Account WHERE Id = :business.Id]);
        System.assertEquals(1, [SELECT COUNT() FROM Address_vod__c WHERE Id = :testAddress.Id]);
    }
}