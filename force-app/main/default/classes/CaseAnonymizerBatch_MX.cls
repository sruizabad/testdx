/*
 * CaseAnonymizerBatch_MX
 * Created By:      Garima Agarwal
 * Created Date:    7/24/2014
 * UpdatedBy : Krishna Suram.
 * Description:     Removes personally identifiable consumer information from Mexico Accounts
 */
 
 global class CaseAnonymizerBatch_MX implements Database.Batchable<sObject>, Database.Stateful {
   
   
    global CaseAnonymizerBatch_MX() {}
    List<Account> failedRequests = new List<Account>();
      List<Address_vod__c> failedAddress = new List<Address_vod__c>();
    global Database.QueryLocator start(Database.BatchableContext BC) {   
        id recordtypid =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Interaction - Closed').getRecordTypeId();
        id AccountRcrtpe = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        
        Boolean IsClosed = true;
         Datetime excludeDate; 
         Service_Cloud_Settings_MVN__c scs = Service_Cloud_Settings_MVN__c.getInstance();
        Decimal tempRequestPeriod = scs.Days_to_anonymize_MX_Accounts__c;
         String country = scs.Interaction_Anonymize_Countries_MVN__c;
        excludeDate = Datetime.now().addDays(tempRequestPeriod.intValue() * -1);
    
        String query = 'select Id,Date_Closed_MVN__c,Accountid,Account.RecordType.Name,Recordtype.Name from Case where RecordtypeId =:recordtypid   AND Date_Closed_MVN__c <=:excludeDate and Account.RecordtypeId=:AccountRcrtpe and Account.Country_Code_BI__c=:country  and IsClosed =:IsClosed';

        return Database.getQueryLocator(query);

      
    }

    global void execute(Database.BatchableContext BC, List<Case> scope) { 
        list<Account>updateaccountlist=new list<Account>();  
        list<Account>Finalupdateaccountlist=new list<Account>();
        
        List<Id> csIds= new List<Id>();
         List<Case> updateCaseList= new List<Case>();
        for(Case cs: scope){
           csIds.add(cs.AccountId);
            cs.Interaction_Notes__c = null;
           updateCaseList.add(cs);
        }
        update updateCaseList;
        updateaccountlist = [select id  from Account where id  in:csIds];
   
        list<Address_vod__c>addresslist=[select Id, name from Address_vod__c where Account_vod__c in :updateaccountlist];
        system.debug('addresslist'+addresslist);
       
        for(Account account : updateaccountlist){
           system.debug('debug checking');
            account.CRC_Phone_MVN__c = null;
            account.FirstName='NA';
            account.LastName='NA';
            account.Middle_vod__c=null;
            account.CRC_Fax_MVN__c=null;
            account.Personal_Address_Line_1_BI__c=null;
            account.Personal_Address_line_2_BI__c=null;
            account.PersonEmail=null;
            account.CRC_Email_MVN__c=null;
            account.PersonBirthdate=null;
            Finalupdateaccountlist.add(account);
            
        }
         
         Database.SaveResult[] saveResults = database.update(Finalupdateaccountlist,false);
        for (Integer i=0; i<saveResults.size(); i++) {
            if (!saveResults[i].isSuccess() ) {
                failedRequests.add(Finalupdateaccountlist[i]);
            }
        }
     list<Address_vod__c>updateaddresslist=new list<address_vod__c>();
    
        for(Address_vod__c address: addresslist)
        {system.debug('check for address loop');
        
         address.Name='NA'; 
         address.Address_line_2_vod__c=null;
         address.OK_State_Province_BI__c=null;
         updateaddresslist.add(address);               
        }
         
         Database.SaveResult[] saveResults1 = database.update(updateaddresslist,false);
        for (Integer i=0; i<saveResults1.size(); i++) {
            if (!saveResults1[i].isSuccess() ) {
                failedAddress.add(updateaddresslist[i]);
            }
        }

    }
    
    global void finish(Database.BatchableContext BC) {
         
         if ((!failedRequests.isEmpty()) || (!failedAddress.isEmpty())) {
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            String adminEmailAddress = Service_Cloud_Settings_MVN__c.getInstance().Administrator_Email_MVN__c;
            mail.setToAddresses(new String[] {adminEmailAddress});
            mail.setSenderDisplayName('Interaction Anonymize Error');
            mail.setSubject('Error(s) in the Interaction Anonymize Job');
            mail.setBccSender(false);
            mail.setUseSignature(false);
             

            String plainTextBody = 'The following accounts could not be Anonymized:\n';
             if(!failedRequests.isEmpty()){
             for (Account acc : failedRequests) {
                plainTextBody += 'Account Id: ' + acc.Id + ' ---- Account Name: ' + acc.Name + '\n';
            }
             }else {
                  for (Address_vod__c add : failedAddress) {
                plainTextBody += 'Account Id: ' + add.Id + ' ---- Account Name: ' + add.Name + '\n';
            }
             }
            mail.setPlainTextBody(plainTextBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        } 
    }
    
}