global class CaseAnonymizerBatch_MX_Scheduler implements Schedulable{

    global void execute(SchedulableContext sc){
    CaseAnonymizerBatch_MX anonymizer = new CaseAnonymizerBatch_MX();
        Database.executeBatch(anonymizer,200);
        }
}