/*
* CaseArticleCopyTriggerMVN
* Created By: Kai Amundsen
* Created Date: April 23, 2013
* Description: Trigger to copy the knowledge article information onto the CaseArticleData shadow object based on the related CaseArticle
*   Assumptions: multiple Case_Article_Data objects are not inserted simultaniously with the same Knowledge article but different versions
*/


public with sharing class CaseArticleCopyTriggerMVN implements TriggersMVN.HandlerInterface {
    public static String commonArticleFields = 'Preview_MVN__c'; // custom fields only, no spaces

    public Map<String,String> articleTypes = KnowledgeSearchUtilityMVN.knowledgeTypes();
     public Map<String,String> MITSarticleTypes= KnowledgeSearchUtilityMITS.knowledgeTypes();
    public List<String> articleFieldList = new List<String>();

    public CaseArticleCopyTriggerMVN() {
        articleFieldList = commonArticleFields.split(',');
    }

    public void handle() {

        Map<Case_Article_Data_MVN__c,CaseArticle> caseArticles = new Map<Case_Article_Data_MVN__c,CaseArticle>();
        List<Case_Article_Data_MVN__c> articleData = (List<Case_Article_Data_MVN__c>) Trigger.new;

        //Populate Case Article Data
        for(Case_Article_Data_MVN__c cad : articleData) {

            // Create Case Article, if duplicate, fail out
            CaseArticle cs = new CaseArticle();

            // Copy CaseArticle Fields: cs.CaseId
            if(cad.Article_Version_Number_MVN__c != null) {
                cs.ArticleVersionNumber = cad.Article_Version_Number_MVN__c.intValue();
            }
            cs.KnowledgeArticleId = cad.Knowledge_Article_ID_MVN__c;
            cs.ArticleLanguage = cad.Article_Language_MVN__c;
            cs.CaseId = cad.Case_MVN__c;

            caseArticles.put(cad,cs);
        }

        insert caseArticles.values();

        for(Case_Article_Data_MVN__c cad : caseArticles.keySet()) {
            cad.Case_Article_ID_MVN__c = caseArticles.get(cad).Id;
        }
    }
}