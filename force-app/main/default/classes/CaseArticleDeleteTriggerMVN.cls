/*
* CaseArticleDeleteTriggerMVN
* Created By: Kai Amundsen
* Created Date: April 18, 2013
* Description: Delete associated Case Articles when the Case Article Data object is deleted
*/

public with sharing class CaseArticleDeleteTriggerMVN implements TriggersMVN.HandlerInterface{
	public CaseArticleDeleteTriggerMVN() {
		
	}

	public void handle() {
		Set<Id> articleIds = new Set<Id>();

		for(Case_Article_Data_MVN__c cad : (List<Case_Article_Data_MVN__c>) Trigger.old) {
			articleIds.add(cad.Case_Article_ID_MVN__c);
		}

		List<CaseArticle> delArticles = [select Id from CaseArticle where Id in :articleIds];

		delete delArticles;
	}
}