/*
* CaseArticleDeleteTriggerMVN
* Created By: Kai Amundsen
* Created Date: April 18, 2013
* Description: This is a test class for CaseArticleDeleteTriggerMVN.
*/
@isTest
private class CaseArticleTriggerTestMVN {

    private static Case theCase;
    private static List<FAQ_MVN__kav> faqs;
    private static List<Medical_Letter_MVN__kav> medicalLetters;
    private static List<CaseArticle> cas;
    private static List<ID> articleIds;

    static {
        TestDataFactoryMVN.createSettings();
        articleIds = TestDataFactoryMVN.setupArticles();
        theCase = TestDataFactoryMVN.createTestCase();

        faqs = [select ArticleNumber, UrlName, Title, KnowledgeArticleId, Id, Language, Summary, ArticleType, VersionNumber, Indication_MVN__c, Category_MVN__c from FAQ_MVN__kav where Id in :articleIds];
        medicalLetters = [select ArticleNumber, UrlName, Title, KnowledgeArticleId, Id, Language, Summary, ArticleType, VersionNumber from Medical_Letter_MVN__kav where Id in :articleIds];
    }
    
    @isTest static void test_CaseArticle_Deleted() {
        Case_Article_Data_MVN__c cad = KnowledgeSearchUtilityMVN.createCaseArticle(theCase.Id, faqs[1]);

        insert cad;
        List<CaseArticle> ca = [select Id from CaseArticle where CaseId = :theCase.Id];

        System.assertEquals(1,ca.size());

        Test.startTest();
        delete cad;
        Test.stopTest();

        List<CaseArticle> results = [select Id from CaseArticle where CaseId = :theCase.Id];

        System.assertEquals(0,results.size());
    }
    /*
    @isTest static void test_all_Case_Article_Data_Fields_Populated() {
        Case_Article_Data_MVN__c cad = KnowledgeSearchUtilityMVN.createCaseArticle(theCase.Id, faqs[1]);

        Test.startTest();
        insert cad;
        Test.stopTest();

        Case_Article_Data_MVN__c result = [select Id,Article_Title_MVN__c from Case_Article_Data_MVN__c where Id = :cad.Id limit 1];

        System.assert(result.Article_Title_MVN__c.contains('test'));
    }

    @isTest static void test_Missing_Fields() {
        FAQ_MVN__kav faq = [select KnowledgeArticleId, Id from FAQ_MVN__kav where Id in :articleIds limit 1];
        Case_Article_Data_MVN__c cad = new Case_Article_Data_MVN__c();
        cad.Case_MVN__c = theCase.Id;
        cad.Knowledge_Article_ID_MVN__c = faq.KnowledgeArticleId;
        
        String correcException = 'Wrong or no exception';

        Test.startTest();
        try {
            Database.SaveResult sr = Database.insert(cad);
        } catch (Exception e) {
            System.debug('!!! ' + e.getMessage());
            System.assert(e.getMessage().contains('Required'));
            correcException = 'Correct exception thrown';
        }
        Test.stopTest();

        List<CaseArticle> ca = [select Id from CaseArticle where CaseId = :theCase.Id];

        System.assertEquals(0,ca.size());
        
        System.assertEquals('Correct exception thrown',correcException);
    }*/
}