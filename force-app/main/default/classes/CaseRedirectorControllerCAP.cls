/*
 * Created By:   Siddharth Jain
 * Created Date: 7/17/2014
 * Description:  Handler for Case Object so that 
 *                  1.  If Case is created through New Case button then without Account Id Case Created. 
 *                  2.  If Case is created through Account Page then Account ID gets  copied in the Case.
 *              
 */

public with sharing class CaseRedirectorControllerCAP{

public transient  Case newCase { get; set; }
    public transient  String accountId;

    public CaseRedirectorControllerCAP(ApexPages.StandardController controller) {
        Case c = (Case)controller.getRecord();
        System.debug('account id='+ApexPages.currentPage().getParameters().get('def_account_id'));
        if(ApexPages.currentPage().getParameters().get('def_account_id') != null){
            accountId = ApexPages.currentPage().getParameters().get('def_account_id');
            system.debug('****case'+c);
        }
    }


 public PageReference pageRedirectMethod(){
    
       
        String accountId = ApexPages.currentPage().getParameters().get('def_account_id');
         System.debug('account in redirect method='+ApexPages.currentPage().getParameters().get('def_account_id'));
       
        PageReference pageRef;
        
       
        //Passing Account ID to Page
        if(accountId!=null && accountId!='')
            pageRef = new PageReference('/apex/CreateCaseRedirectMVN?accid='+accountId);
        else
            pageRef = new PageReference('/apex/CreateCaseRedirectMVN');
            
        pageRef.setReDirect(true);
        
        return pageRef;
    }
    
}