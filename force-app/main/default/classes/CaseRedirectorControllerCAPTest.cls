@isTest
public class CaseRedirectorControllerCAPTest{
      
      static testmethod void caseRedirect(){
      
        TestDataFactoryMVN.createSettings();
        Account testAccount = new Account();

        String hcpRecordTypeName = Service_Cloud_Settings_MVN__c.getInstance().HCP_Record_Type_MVN__c;
        testAccount.RecordTypeId = [select Id from RecordType where SObjectType='Account' and Name=:hcpRecordTypeName limit 1].Id;
        testAccount.FirstName = 'Test';
        testAccount.LastName = 'Account';

        insert testAccount;
        Case cs = new Case();
        cs.AccountId = testAccount.Id;
        insert cs;
        
        
       /* ApexPages.StandardController con = new ApexPages.StandardController(cs);
        ApexPages.currentPage().getParameters().put('accid', testAccount.Id);
        CaseRedirectorControllerCAP extension = new CaseRedirectorControllerCAP(con); */
         Test.StartTest(); 
          ApexPages.StandardController sc = new ApexPages.StandardController(cs);
          CaseRedirectorControllerCAP extension = new CaseRedirectorControllerCAP(sc);
        
          PageReference pageRef = Page.CaseRedirectorPageCAP; // Add your VF page Name here
          Test.setCurrentPage(pageRef);
          pageRef.getParameters().put('def_account_id', testAccount.Id);
        
          //testAccPlan.save(); call all your function here
          extension.pageRedirectMethod();
         Test.StopTest();
        
      }
    
    static testmethod void caseRedirect2(){
      
        TestDataFactoryMVN.createSettings();
        Case cs = new Case();
        cs.AccountId = null;
        
        insert cs;
        
        
       /* ApexPages.StandardController con = new ApexPages.StandardController(cs);
        ApexPages.currentPage().getParameters().put('accid', testAccount.Id);
        CaseRedirectorControllerCAP extension = new CaseRedirectorControllerCAP(con); */
         Test.StartTest(); 
          ApexPages.StandardController sc = new ApexPages.StandardController(cs);
          CaseRedirectorControllerCAP extension = new CaseRedirectorControllerCAP(sc);
        
          PageReference pageRef = Page.CaseRedirectorPageCAP; // Add your VF page Name here
          Test.setCurrentPage(pageRef);
          pageRef.getParameters().put('def_account_id', null);
        
          //testAccPlan.save(); call all your function here
          extension.pageRedirectMethod();
         Test.StopTest();
        
      }
}