/*
 * Created By:   Siddharth Jain
 * Created Date: 7/17/2014
 * Description:  Handler for Case Object so that Case can be shared with same Country Users.
 *                 
 *              
 */


public with sharing class Case_Share_CAP {


public void share(List<Case> csc)
{
       
        User country;
        country= [Select Country_Code_BI__c from User where Id = :UserInfo.getUserId()];
List<CaseShare> sharesToCreate = new List<CaseShare>();
    List<ID> shareIdsToDelete = new List<ID>();
    
        for (Case csj : csc) {
        CaseShare cs = new CaseShare();
        cs.CaseAccessLevel = 'Edit';
        cs.CaseId = csj.Id;
        String owner=csj.OwnerId;
      Service_Cloud_Settings_MVN__c settings;
         settings = Service_Cloud_Settings_MVN__c.getInstance();
    
    if(csj.User_Country_Code_MVN__c == 'DE')
        cs.UserOrGroupId =[select Id from Group where Name = :settings.Public_DE_Users_CAP__c].Id;
        
      else if(csj.User_Country_Code_MVN__c == 'NL')
        cs.UserOrGroupId = [select Id from Group where Name = :settings.Public_NL_Users_CAP__c].Id ;
        
      else  if(csj.User_Country_Code_MVN__c == 'AU')
         cs.UserOrGroupId = [select Id from Group where Name = :settings.Public_AU_NZ_Users_CAP__c].Id;
        
     else   if(csj.User_Country_Code_MVN__c == 'IT')
         cs.UserOrGroupId = [select Id from Group where Name = :settings.Public_IT_Users_CAP__c].Id ;
         
   /*   else if(csj.User_Country_Code_MVN__c == 'MX')
         cs.UserOrGroupId = [select Id from Group where Name = :settings.Public_MX_Users_CAP__c].Id ;
     */ 
      else
      {
      Profile pf=[select id from Profile where name='CRC - System Administrator'];
         for(user u : [select Id from User where PROFILEID=:pf.id limit 1])
         cs.UserOrGroupId = u.Id;
      }
  
       System.debug('Iteratred Query is :'+ cs);
        sharesToCreate.add(cs);
}
if (!shareIdsToDelete.isEmpty()&& country.Country_Code_BI__c != 'MX')
      delete [select id from CaseShare where CaseId IN :shareIdsToDelete and RowCause = 'Manual'];

    // do the DML to create shares
    if (!sharesToCreate.isEmpty() && country.Country_Code_BI__c != 'MX')
    try
    {
       
      upsert sharesToCreate;
      }
      catch(Exception ae)
      {
        System.debug('Exception is :'+ ae);
      }
      
      }
        
    

      }