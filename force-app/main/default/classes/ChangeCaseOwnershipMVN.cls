/*
  * ChangeCaseOwnershipMVN
  *    Created By:     Kai Chen   
  *    Created Date:    September 25, 2013
  *    Description:     Change case ownership from queue to user when modified by a user.
 */
public class ChangeCaseOwnershipMVN implements TriggersMVN.HandlerInterface{
    
    public void execute(Map<Id, Case> newCases, Map<Id, Case> oldCases){

        Set<Id> casesToCheck = new Set<Id>();

        for(Case newCase : newCases.values()){
            Case oldCase = oldCases.get(newCase.Id);
            System.debug('NEW CASE OWNER ID: ' + newCase.OwnerId);
            System.debug('OLD CASE OWNER ID: ' + oldCase.OwnerId);
            if(newCase.OwnerId == oldCase.OwnerId){
                casesToCheck.add(newCase.Id);
            }
        }

        if(!casesToCheck.isEmpty()){
            List<Case> cases = [select Id, Owner.Type,IsAssignedToQueueMITS__c from Case where Id in :casesToCheck];
            List<Id> casesToUpdate = new List<Id>();
            for(Case caseToCheck : cases){
                System.debug('CASETOCHECK: ' + caseToCheck);
                System.debug('caseToCheck : ' + caseToCheck.IsAssignedToQueueMITS__c  );
                System.debug('caseToCheck.Owner.Type: ' + caseToCheck.Owner.Type  );
                if(caseToCheck.Owner.Type == 'Queue'){
                    casesToUpdate.add(caseToCheck.Id);
                }
            }
            if(casesToUpdate.size() > 0) {
                updateOwnership(casesToUpdate,UserInfo.getUserId());
            }
        }
    }

    public void handle() {
        if(!System.isBatch()) {
            execute((Map<Id, Case>) trigger.newMap, (Map<Id, Case>) trigger.oldMap);
        }
    }

    @future 
    public static void updateOwnership(List<Id> caseIDs, Id ownerId) { 
        Database.DMLOptions dmo = new Database.DMLOptions(); 
        dmo.EmailHeader.triggerUserEmail = false; 

        List<Case> casesToUpdate = [select OwnerId from Case where Id in :caseIDs]; 

        for(Case c : casesToUpdate) { 
            c.OwnerId = ownerId;
        } 

        Database.update(casesToUpdate, dmo); // Actual DML operation is performed instead of implicit update
    }
}