/*
  * ChangeCaseOwnershipTestMVN
  *    Created By:     Kai Chen   
  *    Created Date:    September 26, 2013
  *    Description:     Unit tests for CaseCaseOwnershipMVN class.
 */
@isTest
private class ChangeCaseOwnershipTestMVN {

      static Group queue;
      static User callCenterUser;
      static Case newCase;

      static {
            Profile p = [select Id from Profile where Name='CRC - Customer Request Agent']; 
            UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Germany'];
            callCenterUser = new User(Alias = 'heiditst', Email='heidibergen@oehringertest.com', 
                            EmailEncodingKey='UTF-8', FirstName = 'Heidi', LastName='Bergen', LanguageLocaleKey='de', 
                            LocaleSidKey='de', ProfileId = p.Id, UserRoleId = ur.Id, 
                            TimeZoneSidKey='Europe/Berlin', UserName='heidibergen@oehringertest.com',
                            Country_Code_BI__c = 'DE', External_ID_BI__c='12345678');

            insert callCenterUser;
                  
            System.runAs(callCenterUser){
                  TestDataFactoryMVN.createSettings();

                  queue = new Group();
                  queue.Type = 'Queue';

                  queue.Name = 'Test Queue';

                  insert queue;

                  QueueSobject queueSObject = new queueSobject();
                  queueSObject.QueueId = queue.Id;
                  queueSObject.SobjectType = 'Case';

                  insert queueSObject;

            }
      }
      
      @isTest static void changeOwnerShipFromQueueToUser() {
            System.runAs(callCenterUser){
                  Account testAccount = new Account();
            testAccount.RecordTypeId = [select id from RecordType where SObjectType = 'Account' AND DeveloperName ='Professional_vod'].Id;
            testAccount.FirstName = 'Test';
            testAccount.LastName = 'Account';
            testAccount.Country_Code_BI__c = 'NL';

            insert testAccount;

            Customer_Request_Product_BI__c product = new Customer_Request_Product_BI__c();
            product.Name = 'Pradaxa';

            insert product;

            Medical_Inquiry_vod__c inquiry = new Medical_Inquiry_vod__c();

            inquiry.Account_vod__c = testAccount.Id;
            
            inquiry.Address_Line_1_vod__c = '123 Main St.';
            inquiry.Address_Line_2_vod__c = 'Apt. 456';
            inquiry.Status_vod__c = UtilitiesMVN.medicalInquirySubmittedStatus;
            inquiry.Delivery_Method_vod__c = 'Mail_vod';
            inquiry.Inquiry_Text__c = 'Test Inquiry Text';
            inquiry.Phone_Number_vod__c = '123-456-7890';
            inquiry.Fax_Number_vod__c = '987-654-4321';
            inquiry.City_vod__c = 'Chicago';
            inquiry.State_vod__c = 'IL';
            inquiry.Zip_vod__c = '60606';
            inquiry.Country_vod__c = 'us';
            inquiry.Product_BI__c = product.Id;
            inquiry.Email_vod__c = 'testemail@test.com';

            insert inquiry;

            List<Case> childCases = [select Id, Interaction_Notes_MVN__c, RecordTypeId, case_AddressLine1_MVN__c, case_AddressLine2_MVN__c,
                                                Delivery_Method_MVN__c, ParentId, Origin, Request_MVN__c, Sales_Rep_MVN__c, Transfer_To_MVN__c,
                                                case_Account_Phone_MVN__c, case_Account_Fax_MVN__c, case_Account_Email_MVN__c, case_State_MVN__c, case_City_MVN__c, case_Postal_Code_MVN__c
                                                from Case where Medical_Inquiry_MVN__c = :inquiry.Id];

            childCases[0].OwnerId = queue.Id;
            childCases[0].Transfer_To_MVN__c = 'test';

            update childCases[0];

            childCases[0].Interaction_Notes_MVN__c = 'TEST NOTES';

            Test.startTest();
            update childCases[0];
            Test.stopTest();

            newCase = [select Id, Owner.Type, Transfer_To_MVN__c from Case where Id = :childCases[0].Id];

            System.assertEquals(callCenterUser.Id, newCase.OwnerId);
            System.assertEquals('test', newCase.Transfer_To_MVN__c);
            }           
      }

      @isTest static void dontChangeOwnershipIfOwnerDifferent() {
            System.runAs(callCenterUser){
                  Account testAccount = new Account();
            testAccount.RecordTypeId = [select id from RecordType where SObjectType = 'Account' AND DeveloperName ='Professional_vod'].Id;
            testAccount.FirstName = 'Test';
            testAccount.LastName = 'Account';
            testAccount.Country_Code_BI__c = 'NL';

            insert testAccount;

            Customer_Request_Product_BI__c product = new Customer_Request_Product_BI__c();
            product.Name = 'Pradaxa';

            insert product;

            Medical_Inquiry_vod__c inquiry = new Medical_Inquiry_vod__c();

            inquiry.Account_vod__c = testAccount.Id;
            
            inquiry.Address_Line_1_vod__c = '123 Main St.';
            inquiry.Address_Line_2_vod__c = 'Apt. 456';
            inquiry.Status_vod__c = UtilitiesMVN.medicalInquirySubmittedStatus;
            inquiry.Delivery_Method_vod__c = 'Mail_vod';
            inquiry.Inquiry_Text__c = 'Test Inquiry Text';
            inquiry.Phone_Number_vod__c = '123-456-7890';
            inquiry.Fax_Number_vod__c = '987-654-4321';
            inquiry.City_vod__c = 'Chicago';
            inquiry.State_vod__c = 'IL';
            inquiry.Zip_vod__c = '60606';
            inquiry.Country_vod__c = 'us';
            inquiry.Product_BI__c = product.Id;
            inquiry.Email_vod__c = 'testemail@test.com';

            insert inquiry;

            List<Case> childCases = [select Id, Interaction_Notes_MVN__c, RecordTypeId, case_AddressLine1_MVN__c, case_AddressLine2_MVN__c,
                                                Delivery_Method_MVN__c, ParentId, Origin, Request_MVN__c, Sales_Rep_MVN__c,
                                                case_Account_Phone_MVN__c, case_Account_Fax_MVN__c, case_Account_Email_MVN__c, case_State_MVN__c, case_City_MVN__c, case_Postal_Code_MVN__c
                                                from Case where Medical_Inquiry_MVN__c = :inquiry.Id];
                  System.debug('CHILD CASES' + childCases);

                  childCases[0].OwnerId = queue.Id;
                  childCases[0].Interaction_Notes_MVN__c = 'TEST NOTES';

                  Test.startTest();
                  update childCases[0];
                  Test.stopTest();

                  newCase = [select Id, Owner.Type from Case where Id = :childCases[0].Id];

                  System.assertEquals('Queue', newCase.Owner.Type);
            }           
      }
      
}