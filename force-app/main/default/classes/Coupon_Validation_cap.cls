/*
 * Created By:   Garima Agarwal
 * Created Date: 7/17/2014
 * Description:  Check for Coupon Validity and allow only Patients to redeem coupons.                             
 */
public without sharing class Coupon_Validation_cap {
public void validate(list<Case> caselist)
{   
    list<Exception_List__c>exceptionlist=[select Id,Coupon_Number__c,VAS_CODE__c from Exception_List__c where VAS_CODE__r.Active_BI__c=true];
    list<case>midrequestlist=new list<case>();
    list<case>requestlist=new list<case>();
    list<String> caseCoupon = new list<String>();
    list<String> couponCaseNumber = new list<String>();
    
    for (case currentcoupon : caselist)
    {
    caseCoupon.add(currentcoupon.coupon__c);
    couponCaseNumber.add(currentcoupon.Id);
    
    }
    list<Case>usedcouponlist=[select Id,coupon__c from Case where Coupon__c IN :caseCoupon and Type='coupon management' and (Owner.Profile.Name='CRC-Warehouse Agent' or Owner.Profile.Name='CRC - MX - Customer Request Agent' or Owner.Name='Warehouse Queue' or Owner.Name='MX Call Center Admin' or owner.Profile.Name= 'CRC - VAS Admin' or owner.Profile.Name= 'Coupon Management Profile') and id!=:couponCaseNumber];
    system.debug('@@@@@@@'+couponCaseNumber);
    list<vas_bi__c>activevaslist=[SELECT Active_BI__c,Id,Name,Product_BI__c,VAS_Code_BI__c FROM VAS_BI__c where Active_BI__c=true ];
    set<string>formattedname=new set<string>();
    set<string>couponlist=new set<string>(); 
    set<string>exceptionitemlist=new set<string>(); 
    set<string>activevasnamelist=new set<string>();
    list<User>userlist=[select Id,Name from User where IsActive=true and Profile.Name='CRC - MX - Customer Request Agent' ];
    RecordType RecType=[Select Id From RecordType Where SobjectType = 'Case' and DeveloperName ='Request_MVN'];
    //Service_Cloud_Settings_MVN__c scs = Service_Cloud_Settings_MVN__c.getInstance();
    //string pn=scs.System_Administrator__c;
    //system.debug('pn'+pn);
    string label2 = system.label.Profile_Name_System_Admin;
    list<Profile> profilelist=[SELECT Id,Name FROM Profile where Name=: label2];
    //list<Profile> profilelist=[SELECT Id,Name FROM Profile where Id='00ed00000013C1VAAU'];
    system.debug('profilelist'+profilelist);
    system.debug('**profilelist'+profilelist.size());
   for(Integer i=0; i<usedcouponlist.size(); i++) {
    string usedcoupon = usedcouponlist.get(i).coupon__c;
    couponlist.add(usedcoupon );
}
system.debug('couponlist'+couponlist);

    for(Integer i=0; i<exceptionlist.size(); i++) {
    string exceptionitem = exceptionlist.get(i).Coupon_Number__c;
    system.debug('exceptionitem'+exceptionitem);
    list<string>midexceptionitemlist=exceptionitem.split(',');
    system.debug('midexceptionitemlist'+midexceptionitemlist);
    system.debug('size is'+midexceptionitemlist.size());
    for(Integer j=0; j<midexceptionitemlist.size(); j++)
    {string item= midexceptionitemlist.get(j);
    exceptionitemlist.add(item);
    }
    }
system.debug('exceptionitemlist'+exceptionitemlist);
system.debug('exceptionlist'+exceptionlist);

    for(Integer i=0; i<activevaslist.size(); i++) {
    string activevascode = activevaslist.get(i).VAS_Code_BI__c;
    activevasnamelist.add(activevascode);

}
system.debug('activevasnamelist'+activevasnamelist);
                        
        for (case requestcase: caselist)
        {system.debug('requestcase'+requestcase);
            if((requestcase.RecordTypeId==RecType.Id))
                {system.debug('The case is of type'+requestcase.RecordTypeId);
                    midrequestlist.add(requestcase);
                }
          }
          
       for(case c:midrequestlist)
        {
           for(user r:userlist) 
           {
           if(c.Ownerid==r.id)
           requestlist.add(c);
           }
 
        }
        try{
        for(case c:midrequestlist)         
        {string UserId=userinfo.getProfileId();
        system.debug('UserId'+UserId);
         String ProfileiId = profilelist[0].Id;
         system.debug('ProfileiId'+ProfileiId);
         if(UserId !=ProfileiId)
         {
         system.debug('c.Type'+c.Type);
         system.debug('c.Requester_Type_MVN__c'+c.Requester_Type_MVN__c);
               if(c.Type=='Coupon Management'&& c.Requester_Type_MVN__c=='Patient') 
                {                   
                if(c.Category_MVN__c==null)
                    {
                    c.adderror(label.select_category);
                    }
                else if(c.Category_MVN__c=='Coupon Management')
                    {
                    if(c.coupon__c ==Null)
                    c.adderror(label.enter_a_valid_coupon);
                        else
                            {system.debug('(c.coupon__c).length()'+(c.coupon__c).length());
                            if((c.Coupon_code__c!=Null) && (c.coupon__c).length()>6)
                            {   
                            string vaspart=c.coupon__c.substring(0,6);
                            system.debug('vaspart'+vaspart);
                            //if(formattedname.contains(vaspart))
                            if(activevasnamelist.contains(vaspart))
                            {system.debug('Entered the loop');
                            system.debug('check in list'+exceptionitemlist);
                            system.debug('check Number'+c.coupon__c);
                                if(usedcouponlist.size()>0)
                                {system.debug('used coupon');
                                c.adderror(label.Coupon_used);
                                }
                                else if(exceptionitemlist.contains(c.coupon__c))
                                {system.debug('expired coupon');
                                c.adderror(label.Coupon_is_in_exceptionlist);
                                }
                            }
                            else
                            {
                            c.adderror(label.Coupon_is_not_associated_to_a_active_VAS);
                            }           
                                 
                            }
                            else{
                            c.adderror(label.Select_the_Coupon_Code_to_which_coupon_is_related);
                            }
                            }
                    }
                    }
        
            else if(c.Type!='Coupon Management'&&c.Type!=null)
                {
                if(c.coupon__c!=null)
                c.adderror(label.Cannot_enter_a_Coupon_Number_for_Non_Coupon_type_Cases);          
                }
           else if (c.Type=='Coupon Management'&& c.Requester_Type_MVN__c!='Patient')  
           {
           c.adderror(label.Only_Patients_can_redeem_coupon);
           }  
        
         }
             }  
             } 
              catch(Exception ae)
      {
        System.debug('Exception is :'+ ae);
      }      

}

}