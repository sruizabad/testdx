/*
* CreateCaseRedirectControllerMVN
* Created By: Roman Lerman
* Created Date: Jan 1, 2013
* Description: This class creates a new interaction whenever the "New" button is clicked on the Case list view
*/
public with sharing class CreateCaseRedirectControllerMVN {
    public Case newCase { get; set; }
    public String accountId { get; set; }
    

    public CreateCaseRedirectControllerMVN (ApexPages.StandardController controller) {
        Case c = (Case)controller.getRecord();
        //System.debug('account id create case redirect controller:'+ApexPages.currentPage().getParameters().get('def_account_id'));
        System.debug('acct id id create case redirect controller:'+ApexPages.currentPage().getParameters().get('accid'));
        if(ApexPages.currentPage().getParameters().get('accid') != null){
            accountId = ApexPages.currentPage().getParameters().get('accid');
        }
        //System.debug('account id create case redirect controller: after if '+ApexPages.currentPage().getParameters().get('def_account_id'));
         System.debug('case:'+c);
     // getRedirect ();                         
    }
    
    public  PageReference getCaseRedirect(){
          
           try{
            insert newCase;
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
           /* PageReference pageref; 
            pageref=  new PageReference('https://c.cs10.visual.force.com/500/o?nooverride=1');
            PageReference CaseRedirect=  new PageReference('/apex/MITSNewCasePage');
            system.debug('##'+CaseRedirect );
            
           CaseRedirect.setReDirect(true);
           return CaseRedirect;*/
           return null;
           } 
   
  
    public PageReference getRedirect () {
    
         newCase = new Case();
         //Created by Mallareddy for redirecting page based on profile for MITS application dated 19/05/2015
         User userprofile= [Select Id,Profile.Name from User where Id = :UserInfo.getUserId()];
         
         string pid =[select ProfileId from User where Id=:UserInfo.getUserId()].ProfileId;
       
          system.debug('Profile Id of the User---->'+pid);
         
          if(userprofile.Profile.Name =='MITS-BASIC MEDICAL INFORMATION KNOWLEDGE ADMINISTRATOR' || userprofile.Profile.Name == 'MITS- BASIC MEDICAL INFORMATION USER' || userprofile.Profile.Name=='MITS-GLOBAL BUSINESS ADMINISTRATOR' ||userprofile.Profile.Name=='MITS-GLOBAL IT ADMINISTRATOR'||userprofile.Profile.Name=='MITS-REGIONAL MEDICAL INFORMATION USER'||userprofile.Profile.Name=='MITS-REGIONAL MEDICAL INFORMATION KNOWLEDGE ADMINISTRATOR'){
           system.debug('If Condition1 satisfied');
                newCase.RecordTypeId = [select Id from RecordType where SObjectType='Case' and DeveloperName =:'MITS_Record'].Id;
                  system.debug('record type id'+ newCase.RecordTypeId);
                     system.debug('If Condition satisfied');
                 return getCaseRedirect();
                                  //PageReference pageRef = new PageReference('/apex/MITS_NewPage');
                 //pageRef.setReDirect(true);         
                } 
                
                //End of code by Mallarededy
                else{
                                            
               newCase.RecordTypeId = [select Id from RecordType where SObjectType='Case' and DeveloperName =: UtilitiesMVN.interactionRecordType].Id;
               }
               System.debug('account id create case redirect before if:'+ApexPages.currentPage().getParameters().get('accid'));

             if(accountId != null){
             newCase.AccountId = accountId;
           
           //Prepopulate the address field when Interaction getting created
           Address_vod__c addId=[select Id from Address_vod__c where Account_vod__c= :accountId limit 1];
           
           newCase.Address_MVN__c=addId.Id;
           

           List<Account> accounts = [select isPersonAccount from Account where Id = :accountId];
           
           if(accounts != null && accounts.size() > 0 && !accounts[0].isPersonAccount){
               newCase.Business_Account_MVN__c = accountId;
           }
           system.debug('Inside If block');
        }
                
    
  try{
            insert newCase;
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
 
 } 
  public PageReference pageRedirectMethod(){
    
        Id pid=userinfo.getProfileId();
        system.debug('profileid++++'+pid);
        String accountId = ApexPages.currentPage().getParameters().get('def_account_id');
        System.debug('Checking:'+accountId);
        String Vas_country = Service_Cloud_Settings_MVN__c.getInstance(pid).VAS_Settings_Cap__c;
        system.debug('countrycode'+Vas_country);
        PageReference pageRef;
        
        if (Vas_country=='MX')
        {
            pageRef = new PageReference('/apex/case_type_cap');
            pageRef.setReDirect(true);            
            
        }else{
            pageRef = new PageReference('/apex/CreateCaseRedirectMVN?def_account_id='+accountId);
            pageRef.setReDirect(true);
        }
        return pageRef;
    }
    
}