/*
* CreateCaseRedirectControllerTestMVN
* Created By: Roman Lerman
* Created Date: 3/6/2013
* Description: Test class for CreateCaseRedirectControllerMVN
*/
@isTest
private class CreateCaseRedirectControllerTestMVN {
	static testMethod void testCreateCase(){
		TestDataFactoryMVN.createSettings();
		
		Case cs = new Case();
		
		ApexPages.StandardController con = new ApexPages.StandardController(cs);

        CreateCaseRedirectControllerMVN extension = new CreateCaseRedirectControllerMVN(con);
    	
    	Test.startTest();
    		extension.getRedirect();
    	Test.stopTest();
    	
    	System.assertNotEquals(null, extension.newCase.Id);

        Case newCase = [select Id, RecordType.DeveloperName from Case limit 1];
  
    	System.assertEquals(newCase.RecordType.DeveloperName, UtilitiesMVN.interactionRecordType);
		
	}

    static testMethod void createCaseWithPersonAccount(){
        TestDataFactoryMVN.createSettings();
        
        Case cs = new Case();

        Account testAccount = new Account();

        String hcpRecordTypeName = Service_Cloud_Settings_MVN__c.getInstance().HCP_Record_Type_MVN__c;
        testAccount.RecordTypeId = [select Id from RecordType where SObjectType='Account' and Name=:hcpRecordTypeName limit 1].Id;
        testAccount.FirstName = 'Test';
        testAccount.LastName = 'Account';

        insert testAccount;
        
        ApexPages.StandardController con = new ApexPages.StandardController(cs);

        ApexPages.currentPage().getParameters().put('def_account_id', testAccount.Id);

        CreateCaseRedirectControllerMVN extension = new CreateCaseRedirectControllerMVN(con);
        
        Test.startTest();
            extension.getRedirect();
        Test.stopTest();
        
        System.assertNotEquals(null, extension.newCase.Id);

        System.assertEquals(testAccount.Id, extension.newCase.AccountId);
        System.assertEquals(null, extension.newCase.Business_Account_MVN__c);
        
        Case newCase = [select Id, RecordType.DeveloperName from Case where Id=:extension.newCase.Id];
  
        System.assertEquals(newCase.RecordType.DeveloperName, UtilitiesMVN.interactionRecordType);
    }

    static testMethod void createCaseWithBusinessAccount(){
        TestDataFactoryMVN.createSettings();
        
        Case cs = new Case();

        Account testAccount = new Account();

        String businessRecordTypeName = Service_Cloud_Settings_MVN__c.getInstance().Business_Account_Record_Type_MVN__c;
        testAccount.RecordTypeId = [select Id from RecordType where SObjectType='Account' and Name=:businessRecordTypeName limit 1].Id;
        testAccount.Name = 'Test';        

        insert testAccount;
        
        ApexPages.StandardController con = new ApexPages.StandardController(cs);

        ApexPages.currentPage().getParameters().put('def_account_id', testAccount.Id);

        CreateCaseRedirectControllerMVN extension = new CreateCaseRedirectControllerMVN(con);
        
        Test.startTest();
            extension.getRedirect();
        Test.stopTest();
        
        System.assertNotEquals(null, extension.newCase.Id);

        System.assertEquals(testAccount.Id, extension.newCase.AccountId);
        System.assertEquals(testAccount.Id, extension.newCase.Business_Account_MVN__c);
        
        Case newCase = [select Id, RecordType.DeveloperName from Case where Id=:extension.newCase.id];
  
        System.assertEquals(newCase.RecordType.DeveloperName, UtilitiesMVN.interactionRecordType);
    }
}