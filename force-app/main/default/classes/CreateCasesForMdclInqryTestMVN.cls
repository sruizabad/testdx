/*
* CreateCasesForMdclInqryTestMVN
* Created By: Kai Chen
* Created Date: 7/28/2013
* Description: Test cases for CreateCasesForMedicalInquiryMVN class
*/
@isTest
private class CreateCasesForMdclInqryTestMVN {
    
    static User adminUser;
    static { 
        Profile p = [select Id from Profile where Name='CRC - Customer Request Agent']; 
        UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Germany'];
        adminUser = new User(alias='ccusysad', email= 'callcentertestusermvn@callcenter.com', External_ID_BI__c='123456789', Country_Code_BI__c = 'DE',
                      emailencodingkey='UTF-8', firstName='Reginald', lastname='Wellington', languagelocalekey='en_US', 
                      localesidkey='en_US', profileid = [select Id from Profile where Name = 'System Administrator'].Id, 
                      isActive = true, timezonesidkey='America/Los_Angeles', username='callcentertestusermvn@callcenter.com',
                      Default_Article_Language_MVN__c = KnowledgeArticleVersion.Language.getDescribe().getPicklistValues()[0].getLabel());

    }

    @isTest static void createMedicalInquiry() {
        System.runAs(adminUser){
            TestDataFactoryMVN.createSettings();
            Account testAccount = new Account();
            testAccount.RecordTypeId = [select id from RecordType where SObjectType = 'Account' AND DeveloperName ='Professional_vod'].Id;
            testAccount.FirstName = 'Test';
            testAccount.LastName = 'Account';
            testAccount.Country_Code_BI__c = 'NL';

            insert testAccount;

            Customer_Request_Product_BI__c product = new Customer_Request_Product_BI__c();
            product.Name = 'Pradaxa';

            insert product;

            Medical_Inquiry_vod__c inquiry = new Medical_Inquiry_vod__c();

            inquiry.Account_vod__c = testAccount.Id;
            
            inquiry.Address_Line_1_vod__c = '123 Main St.';
            inquiry.Address_Line_2_vod__c = 'Apt. 456';
            inquiry.Status_vod__c = UtilitiesMVN.medicalInquirySubmittedStatus;
            inquiry.Delivery_Method_vod__c = 'Mail_vod';
            inquiry.Inquiry_Text__c = 'Test Inquiry Text';
            inquiry.Phone_Number_vod__c = '123-456-7890';
            inquiry.Fax_Number_vod__c = '987-654-4321';
            inquiry.City_vod__c = 'Chicago';
            inquiry.State_vod__c = 'IL';
            inquiry.Zip_vod__c = '60606';
            inquiry.Country_vod__c = 'us';
            inquiry.Product_BI__c = product.Id;
            inquiry.Email_vod__c = 'testemail@test.com';

            insert inquiry;

            inquiry = [select Id, CreatedById, Account_vod__c, Address_Line_1_vod__c, Address_Line_2_vod__c, Status_vod__c, Delivery_Method_vod__c,
                        Inquiry_Text__c, Phone_Number_vod__c, Fax_Number_vod__c, City_vod__c, State_vod__c, Zip_vod__c, Country_vod__c,
                        Product_BI__c, Email_vod__c from Medical_Inquiry_vod__c where Id = :inquiry.Id];

            List<Case> childCases = [select Id, Interaction_Notes_MVN__c, RecordTypeId, case_AddressLine1_MVN__c, case_AddressLine2_MVN__c,
                                    Delivery_Method_MVN__c, ParentId, Origin, Request_MVN__c, Sales_Rep_MVN__c,
                                    case_Account_Phone_MVN__c, case_Account_Fax_MVN__c, case_Account_Email_MVN__c, case_State_MVN__c, case_City_MVN__c, case_Postal_Code_MVN__c
                                    from Case where Medical_Inquiry_MVN__c = :inquiry.Id];

            System.assertEquals(2, childCases.size());

            Case interaction;
            Case request;

            for(Case childCase : childCases){
                if(childCase.RecordTypeId == [select Id from RecordType where SOBjectType = 'Case' AND DeveloperName = :UtilitiesMVN.interactionRecordType].ID){
                    interaction = childCase;
                } else if(childCase.RecordTypeId == [select Id from RecordType where SOBjectType = 'Case' AND DeveloperName = :UtilitiesMVN.requestRecordType].ID){
                    request = childCase;
                }
            }

            System.assert(interaction.Interaction_Notes_MVN__c.contains('Mail'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('123 Main St.'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('Chicago'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('IL'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('60606'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('Pradaxa'));
            System.assert(!interaction.Interaction_Notes_MVN__c.contains('testEmail@test.com'));
            System.assert(!interaction.Interaction_Notes_MVN__c.contains('123-456-7890'));
            System.assert(!interaction.Interaction_Notes_MVN__c.contains('987-654-4321'));
            System.assertEquals(inquiry.CreatedById, interaction.Sales_Rep_MVN__c);

            System.assertEquals(interaction.Origin, Service_Cloud_Settings_MVN__c.getInstance().Medical_Inquiry_Source_MVN__c);
            System.assertEquals(interaction.case_AddressLine1_MVN__c, inquiry.Address_Line_1_vod__c);
            System.assertEquals(interaction.case_AddressLine2_MVN__c, inquiry.Address_Line_2_vod__c);
            System.assertEquals(interaction.case_City_MVN__c, inquiry.City_vod__c);
            System.assertEquals(interaction.case_State_MVN__c, inquiry.State_vod__c);
            System.assertEquals(interaction.case_Postal_Code_MVN__c, inquiry.Zip_vod__c);
            System.assertEquals(interaction.case_Account_Phone_MVN__c, inquiry.Phone_Number_vod__c);
            System.assertEquals(interaction.case_Account_Fax_MVN__c, inquiry.Fax_Number_vod__c);
            System.assertEquals(interaction.case_Account_Email_MVN__c, inquiry.Email_vod__c);
            
            System.assertEquals(request.Delivery_Method_MVN__c, 'Mail');
            System.assertEquals(request.ParentId, interaction.Id);
            System.assertEquals(inquiry.CreatedById, request.Sales_Rep_MVN__c);
            //System.assertEquals(request.Request_MVN__c, inquiry.Inquiry_Text__c);

            inquiry.Status_vod__c = 'Test';

            update inquiry;

            inquiry.Status_vod__c = UtilitiesMVN.medicalInquirySubmittedStatus;

            update inquiry;

            childCases = [select Id from Case where Medical_Inquiry_MVN__c = :inquiry.Id];

            System.assertEquals(2, childCases.size());
        }
    }
    
    @isTest static void updateMedicalInquiry() {
        System.runAs(adminUser){
            TestDataFactoryMVN.createSettings();
            Account testAccount = new Account();
            testAccount.RecordTypeId = [select id from RecordType where SObjectType = 'Account' AND DeveloperName ='Professional_vod'].Id;
            testAccount.FirstName = 'Test';
            testAccount.LastName = 'Account';
            testAccount.Country_Code_BI__c = 'NL';

            insert testAccount;

            Customer_Request_Product_BI__c product = new Customer_Request_Product_BI__c();
            product.Name = 'Pradaxa';

            insert product;

            Medical_Inquiry_vod__c inquiry = new Medical_Inquiry_vod__c();

            inquiry.Account_vod__c = testAccount.Id;
            
            inquiry.Address_Line_1_vod__c = '123 Main St.';
            inquiry.Address_Line_2_vod__c = 'Apt. 456';
            inquiry.Status_vod__c = 'Save_vod';
            inquiry.Delivery_Method_vod__c = 'Urgent_Mail_vod';
            inquiry.Inquiry_Text__c = 'Test Inquiry Text';
            inquiry.Phone_Number_vod__c = '123-456-7890';
            inquiry.Fax_Number_vod__c = '987-654-4321';
            inquiry.City_vod__c = 'Chicago';
            inquiry.State_vod__c = 'IL';
            inquiry.Zip_vod__c = '60606';
            inquiry.Country_vod__c = 'us';
            inquiry.Product_BI__c = product.Id;
            inquiry.Email_vod__c = 'testEmail@test.com';

            insert inquiry; 

            List<Case> childCases = [select Id, Interaction_Notes_MVN__c from Case where Medical_Inquiry_MVN__c = :inquiry.Id];

            System.assertEquals(0, childCases.size());

            inquiry.Status_vod__c = UtilitiesMVN.medicalInquirySubmittedStatus;

            update inquiry;

            childCases = [select Id, Interaction_Notes_MVN__c, RecordTypeID, Delivery_Method_MVN__c, ParentId, case_AddressLine1_MVN__c, case_AddressLine2_MVN__c,  Origin from Case where Medical_Inquiry_MVN__c = :inquiry.Id];  

            System.assertEquals(2, childCases.size());

            Case interaction;
            Case request;

            for(Case childCase : childCases){
                if(childCase.RecordTypeId == [select Id from RecordType where SOBjectType = 'Case' AND DeveloperName = :UtilitiesMVN.interactionRecordType].ID){
                    interaction = childCase;
                } else if(childCase.RecordTypeId == [select Id from RecordType where SOBjectType = 'Case' AND DeveloperName = :UtilitiesMVN.requestRecordType].ID){
                    request = childCase;
                }
            }

            System.assert(interaction.Interaction_Notes_MVN__c.contains('Mail'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('123 Main St.'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('Chicago'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('IL'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('60606'));
            System.assert(interaction.Interaction_Notes_MVN__c.contains('Pradaxa'));
            System.assert(!interaction.Interaction_Notes_MVN__c.contains('testEmail@test.com'));
            System.assert(!interaction.Interaction_Notes_MVN__c.contains('123-456-7890'));
            System.assert(!interaction.Interaction_Notes_MVN__c.contains('987-654-4321'));
            System.assertEquals(interaction.Origin, Service_Cloud_Settings_MVN__c.getInstance().Medical_Inquiry_Source_MVN__c);
            System.assertEquals(interaction.case_AddressLine1_MVN__c, inquiry.Address_Line_1_vod__c);
            System.assertEquals(interaction.case_AddressLine2_MVN__c, inquiry.Address_Line_2_vod__c);
            
            System.assertEquals(request.Delivery_Method_MVN__c, 'Mail');
            System.assertEquals(request.ParentId, interaction.Id);
        }
    }

    @isTest static void bulkTest() {
        System.runAs(adminUser){
            TestDataFactoryMVN.createSettings();
            Account testAccount = new Account();
            testAccount.RecordTypeId = [select id from RecordType where SObjectType = 'Account' AND DeveloperName ='Professional_vod'].Id;
            testAccount.FirstName = 'Test';
            testAccount.LastName = 'Account';
            testAccount.Country_Code_BI__c = 'NL';

            insert testAccount;

            Customer_Request_Product_BI__c product = new Customer_Request_Product_BI__c();
            product.Name = 'Pradaxa';

            insert product;

            List<Medical_Inquiry_vod__c> inquiriesToInsert = new List<Medical_Inquiry_vod__c>();

            for(Integer i = 0; i < 10; i++){
                Medical_Inquiry_vod__c inquiry = new Medical_Inquiry_vod__c();

                inquiry.Account_vod__c = testAccount.Id;
                
                inquiry.Address_Line_1_vod__c = '123 Main St.';
                inquiry.Address_Line_2_vod__c = 'Apt. 456';
                inquiry.Status_vod__c = UtilitiesMVN.medicalInquirySubmittedStatus;
                inquiry.Delivery_Method_vod__c = 'Fax_vod';
                inquiry.Inquiry_Text__c = 'Test Inquiry Text';
                inquiry.Phone_Number_vod__c = '123-456-7890';
                inquiry.Fax_Number_vod__c = '987-654-4321';
                inquiry.City_vod__c = 'Chicago';
                inquiry.State_vod__c = 'IL';
                inquiry.Zip_vod__c = '60606';
                inquiry.Country_vod__c = 'us';
                inquiry.Product_BI__c = product.Id;
                inquiry.Email_vod__c = 'testEmail@test.com';

                inquiriesToInsert.add(inquiry);
            }

            insert inquiriesToInsert;

            Map<Id, Medical_Inquiry_vod__c> inquiriesMap = new Map<Id, Medical_Inquiry_vod__c>(inquiriesToInsert);

            List<Case> childCases = [select Id, Interaction_Notes_MVN__c from Case where Medical_Inquiry_MVN__c = :inquiriesMap.keySet()];

            System.assertEquals(20, childCases.size());
        }
    }

    @isTest static void casesCreatedWhenNotSalesforceLicense(){
        Profile p = [select Id from Profile where Name='US_SALES']; 
        UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Netherlands'];
        User salesUser = new User(Alias = 'rdonk', Email='rdonk@oehringertest.com', 
                        EmailEncodingKey='UTF-8', FirstName = 'Ryan', LastName='Donk', LanguageLocaleKey='de', 
                        LocaleSidKey='de', ProfileId = p.Id, UserRoleId = ur.Id, 
                        TimeZoneSidKey='Europe/Berlin', UserName='heidibergen@oehringertest.com',
                        Country_Code_BI__c = 'DE', External_ID_BI__c='12345678');

        System.runAs(salesUser){
            TestDataFactoryMVN.createSettings();
            Account testAccount = new Account();
            testAccount.RecordTypeId = [select id from RecordType where SObjectType = 'Account' AND DeveloperName ='Professional_vod'].Id;
            testAccount.FirstName = 'Test';
            testAccount.LastName = 'Account';
            testAccount.Country_Code_BI__c = 'DE';

            insert testAccount;

            Customer_Request_Product_BI__c product = new Customer_Request_Product_BI__c();
            product.Name = 'Pradaxa';

            insert product;

            Medical_Inquiry_vod__c inquiry = new Medical_Inquiry_vod__c();

            inquiry.Account_vod__c = testAccount.Id;
            
            inquiry.Address_Line_1_vod__c = '123 Main St.';
            inquiry.Address_Line_2_vod__c = 'Apt. 456';
            inquiry.Status_vod__c = UtilitiesMVN.medicalInquirySubmittedStatus;
            inquiry.Delivery_Method_vod__c = 'Mail_vod';
            inquiry.Inquiry_Text__c = 'Test Inquiry Text';
            inquiry.Phone_Number_vod__c = '123-456-7890';
            inquiry.Fax_Number_vod__c = '987-654-4321';
            inquiry.City_vod__c = 'Chicago';
            inquiry.State_vod__c = 'IL';
            inquiry.Zip_vod__c = '60606';
            inquiry.Country_vod__c = 'us';
            inquiry.Product_BI__c = product.Id;
            inquiry.Email_vod__c = 'testemail@test.com';

            insert inquiry;

            inquiry = [select Id, CreatedById, Account_vod__c, Address_Line_1_vod__c, Address_Line_2_vod__c, Status_vod__c, Delivery_Method_vod__c,
                        Inquiry_Text__c, Phone_Number_vod__c, Fax_Number_vod__c, City_vod__c, State_vod__c, Zip_vod__c, Country_vod__c,
                        Product_BI__c, Email_vod__c from Medical_Inquiry_vod__c where Id = :inquiry.Id];

            List<Case> childCases = [select Id, Interaction_Notes_MVN__c, RecordTypeId, case_AddressLine1_MVN__c, case_AddressLine2_MVN__c,
                                    Delivery_Method_MVN__c, ParentId, Origin, Request_MVN__c, Sales_Rep_MVN__c,
                                    case_Account_Phone_MVN__c, case_Account_Fax_MVN__c, case_Account_Email_MVN__c, case_State_MVN__c, case_City_MVN__c, case_Postal_Code_MVN__c
                                    from Case where Medical_Inquiry_MVN__c = :inquiry.Id];

            System.assertEquals(2, childCases.size());
        }
    }

    @isTest static void noCasesCreatedWhenNotInDefinedCountry(){
        Profile p = [select Id from Profile where Name='US_SALES']; 
        UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Netherlands'];
        User salesUser = new User(Alias = 'rdonk', Email='rdonk@oehringertest.com', 
                        EmailEncodingKey='UTF-8', FirstName = 'Ryan', LastName='Donk', LanguageLocaleKey='de', 
                        LocaleSidKey='de', ProfileId = p.Id, UserRoleId = ur.Id, 
                        TimeZoneSidKey='Europe/Berlin', UserName='heidibergen@oehringertest.com',
                        Country_Code_BI__c = 'DE', External_ID_BI__c='12345678');

        System.runAs(salesUser){
            TestDataFactoryMVN.createSettings();
            Account testAccount = new Account();
            testAccount.RecordTypeId = [select id from RecordType where SObjectType = 'Account' AND DeveloperName ='Professional_vod'].Id;
            testAccount.FirstName = 'Test';
            testAccount.LastName = 'Account';
            testAccount.Country_Code_BI__c = 'IT';

            insert testAccount;

            Customer_Request_Product_BI__c product = new Customer_Request_Product_BI__c();
            product.Name = 'Pradaxa';

            insert product;

            Medical_Inquiry_vod__c inquiry = new Medical_Inquiry_vod__c();

            inquiry.Account_vod__c = testAccount.Id;
            
            inquiry.Address_Line_1_vod__c = '123 Main St.';
            inquiry.Address_Line_2_vod__c = 'Apt. 456';
            inquiry.Status_vod__c = UtilitiesMVN.medicalInquirySubmittedStatus;
            inquiry.Delivery_Method_vod__c = 'Mail_vod';
            inquiry.Inquiry_Text__c = 'Test Inquiry Text';
            inquiry.Phone_Number_vod__c = '123-456-7890';
            inquiry.Fax_Number_vod__c = '987-654-4321';
            inquiry.City_vod__c = 'Chicago';
            inquiry.State_vod__c = 'IL';
            inquiry.Zip_vod__c = '60606';
            inquiry.Country_vod__c = 'us';
            inquiry.Product_BI__c = product.Id;
            inquiry.Email_vod__c = 'testemail@test.com';

            insert inquiry;

            List<Case> childCases = [select Id, Interaction_Notes_MVN__c, RecordTypeId, case_AddressLine1_MVN__c, case_AddressLine2_MVN__c,
                                    Delivery_Method_MVN__c, ParentId, Origin, Request_MVN__c, Sales_Rep_MVN__c,
                                    case_Account_Phone_MVN__c, case_Account_Fax_MVN__c, case_Account_Email_MVN__c, case_State_MVN__c, case_City_MVN__c, case_Postal_Code_MVN__c
                                    from Case where Medical_Inquiry_MVN__c = :inquiry.Id];

            System.assertEquals(0, childCases.size());
        }
    }

    @isTest static void noCasesCreatedWhenCustomSettingNotDefined(){
        Profile p = [select Id from Profile where Name='US_SALES']; 
        UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Netherlands'];
        User salesUser = new User(Alias = 'rdonk', Email='rdonk@oehringertest.com', 
                        EmailEncodingKey='UTF-8', FirstName = 'Ryan', LastName='Donk', LanguageLocaleKey='de', 
                        LocaleSidKey='de', ProfileId = p.Id, UserRoleId = ur.Id, 
                        TimeZoneSidKey='Europe/Berlin', UserName='heidibergen@oehringertest.com',
                        Country_Code_BI__c = 'DE', External_ID_BI__c='12345678');
        System.runAs(salesUser){
            Service_Cloud_Settings_MVN__c mainSettings = new Service_Cloud_Settings_MVN__c();
        
            mainSettings.Knowledge_Search_Article_Types_MVN__c = 'FAQ_MVN__kav, Medical_Letter_MVN__kav';
            mainSettings.Knowledge_Search_Max_Results_MVN__c = 50;
            mainSettings.Interaction_Record_Type_MVN__c = 'Interaction_MVN';
            mainSettings.Interaction_Create_Origin_MVN__c = 'Email';
            mainSettings.Interaction_Create_Case_Record_Type_MVN__c = 'Request_Email_Case_MVN';
            mainSettings.Interaction_Anonymize_Countries_MVN__c = 'US,DE';
            mainSettings.Request_Record_Type_MVN__c = 'Request_MVN';
            mainSettings.Person_Search_Default_Record_Type_MVN__c = 'Professional_vod';
            mainSettings.Person_Search_Record_Types_MVN__c = 'Professional_vod,Consumer_MVN,Employee_MVN';
            mainSettings.Administrator_Email_MVN__c = 'test@test.com';
            mainSettings.Open_Status_MVN__c = 'Open';
            mainSettings.Do_Not_Default_Country_MVN__c = false;
            mainSettings.Person_Search_Default_Record_Type_MVN__c = 'All';
            mainSettings.SAP_Order_Country_Codes_MVN__c = 'DE';
            mainSettings.Order_Search_Product_Types_MVN__c = 'Promotional';
            mainSettings.Medical_Inquiry_Countries_MVN__c = null;
            
            insert mainSettings;
            
            List<Case_Article_Fields_MVN__c> caseFields = new List<Case_Article_Fields_MVN__c>();
            caseFields.add(new Case_Article_Fields_MVN__c(Name='1',Case_Article_Data_Field_MVN__c='Product_ID_MVN__c',Knowledge_Article_Field_MVN__c='Product_ID_MVN__c',Knowledge_Article_Type_MVN__c='FAQ_MVN__kav'));
            caseFields.add(new Case_Article_Fields_MVN__c(Name='2',Case_Article_Data_Field_MVN__c='Product_ID_MVN__c',Knowledge_Article_Field_MVN__c='Product_ID_MVN__c',Knowledge_Article_Type_MVN__c='Medical_Letter_MVN__kav'));
            caseFields.add(new Case_Article_Fields_MVN__c(Name='3',Case_Article_Data_Field_MVN__c='Article_Indication_MVN__c',Knowledge_Article_Field_MVN__c='Indication_MVN__c',Knowledge_Article_Type_MVN__c='FAQ_MVN__kav'));
            caseFields.add(new Case_Article_Fields_MVN__c(Name='4',Case_Article_Data_Field_MVN__c='Article_Category_MVN__c',Knowledge_Article_Field_MVN__c='Category_MVN__c',Knowledge_Article_Type_MVN__c='FAQ_MVN__kav'));
            insert caseFields;
            
            insert new Knipper_Settings__c();

            insert Case_Delivery_Method_Mapping_MVN__c.getInstance();

            Data_Connect_Setting__c dataConnector = new Data_Connect_Setting__c(Name='AdminEmail', Value__c='test@test.com');

            insert dataConnector;

            System.debug('CUSTOM SETTINGS: ' + mainSettings);

            Account testAccount = new Account();
            testAccount.RecordTypeId = [select id from RecordType where SObjectType = 'Account' AND DeveloperName ='Professional_vod'].Id;
            testAccount.FirstName = 'Test';
            testAccount.LastName = 'Account';
            testAccount.Country_Code_BI__c = 'DE';

            insert testAccount;

            Customer_Request_Product_BI__c product = new Customer_Request_Product_BI__c();
            product.Name = 'Pradaxa';

            insert product;

            Medical_Inquiry_vod__c inquiry = new Medical_Inquiry_vod__c();

            inquiry.Account_vod__c = testAccount.Id;
            
            inquiry.Address_Line_1_vod__c = '123 Main St.';
            inquiry.Address_Line_2_vod__c = 'Apt. 456';
            inquiry.Status_vod__c = UtilitiesMVN.medicalInquirySubmittedStatus;
            inquiry.Delivery_Method_vod__c = 'Mail_vod';
            inquiry.Inquiry_Text__c = 'Test Inquiry Text';
            inquiry.Phone_Number_vod__c = '123-456-7890';
            inquiry.Fax_Number_vod__c = '987-654-4321';
            inquiry.City_vod__c = 'Chicago';
            inquiry.State_vod__c = 'IL';
            inquiry.Zip_vod__c = '60606';
            inquiry.Country_vod__c = 'us';
            inquiry.Product_BI__c = product.Id;
            inquiry.Email_vod__c = 'testemail@test.com';

            insert inquiry;

            List<Case> childCases = [select Id
                                    from Case where Medical_Inquiry_MVN__c = :inquiry.Id];

            System.assertEquals(0, childCases.size());
        }
    }
    
}