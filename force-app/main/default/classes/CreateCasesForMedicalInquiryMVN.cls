/*
* CreateCasesForMedicalInquiryMVN
* Created By: Kai Chen
* Created Date: 7/28/2013
* Description: This class creates an interaction and request for a medical inquiry
* Modified on 2/17/2015 for the CR 1251 to create an interaction and request only for the product manufactured by BI
*/
public class CreateCasesForMedicalInquiryMVN implements TriggersMVN.HandlerInterface
{   
    public Id interactionRecordTypeId {get;set;}
    public Id requestRecordTypeId {get;set;}

    public CreateCasesForMedicalInquiryMVN(){
        interactionRecordTypeId = [select Id from RecordType where SObjectType='Case' and DeveloperName =: UtilitiesMVN.interactionRecordType].Id;
        requestRecordTypeId = [select Id from RecordType where SObjectType='Case' and DeveloperName =: UtilitiesMVN.requestRecordType].Id;
    }

    public void execute(List<Medical_Inquiry_vod__c> inquiries) {
        Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance(UserInfo.getUserId());
        if(!String.isBlank(settings.Medical_Inquiry_Countries_MVN__c)){
            Set<String> countries = new Set<String>(UtilitiesMVN.splitCommaSeparatedString(settings.Medical_Inquiry_Countries_MVN__c));
            System.debug('Countries: ' + countries);
          
            List<Case> interactionsToCreate = new List<Case>();

            Map<Id, Medical_Inquiry_vod__c> inquiriesMap = new Map<Id, Medical_Inquiry_vod__c>(inquiries);

            Map<Id, Medical_Inquiry_vod__c> relatedCases = new Map<Id, Medical_Inquiry_vod__c>([select Id, (select Id from Cases__r) from Medical_Inquiry_vod__c where Id in :inquiriesMap.keySet()]);
// To check conditions to create interaction and request in case object         
            for(Medical_Inquiry_vod__c inquiry : inquiries){
            
                if (inquiry.Product_BI__c != NULL)
                {               
                    List<Customer_Request_Product_BI__c> listProducts = [SELECT Id, Name, Manufacturer_BI__c from Customer_Request_Product_BI__c WHERE id =: inquiry.Product_BI__c];
                    System.debug('ProductManufacturer:' + listProducts[0].Manufacturer_BI__c);
                        if(inquiry.Status_vod__c == UtilitiesMVN.medicalInquirySubmittedStatus 
                            && relatedCases.get(inquiry.Id).Cases__r.isEmpty()
                            && countries.contains(inquiry.stamp_Country_Code_MVN__c)
                            && listProducts[0].Manufacturer_BI__c == 'Boehringer-Ingelheim'){
                            interactionsToCreate.add(createInteraction(inquiry));
                        }
                }
            }
            if(!interactionsToCreate.isEmpty()){
                createCases(interactionsToCreate, inquiriesMap);
            }
        }
    }

    public void execute(List<Medical_Inquiry_vod__c> newInquiries, Map<Id, Medical_Inquiry_vod__c> oldInquiries) {

        Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance(UserInfo.getUserId());
        if(!String.isBlank(settings.Medical_Inquiry_Countries_MVN__c)){
            Set<String> countries = new Set<String>(UtilitiesMVN.splitCommaSeparatedString(settings.Medical_Inquiry_Countries_MVN__c));
            System.debug('Countries: ' + countries);

            List<Case> interactionsToCreate = new List<Case>();

            Map<Id, Medical_Inquiry_vod__c> inquiriesMap = new Map<Id, Medical_Inquiry_vod__c>(newInquiries);

            Map<Id, Medical_Inquiry_vod__c> relatedCases = new Map<Id, Medical_Inquiry_vod__c>([select Id, (select Id from Cases__r) from Medical_Inquiry_vod__c where Id in :inquiriesMap.keySet()]);
// To check conditions to create interaction and case 
            for(Medical_Inquiry_vod__c newInquiry : newInquiries){
                
                if (newInquiry.Product_BI__c != NULL)
                {
                    List<Customer_Request_Product_BI__c> newlistProducts = [SELECT Id, Name, Manufacturer_BI__c from Customer_Request_Product_BI__c WHERE id =: newInquiry.Product_BI__c];
                    System.debug('ProductManufacturer:' + newlistProducts[0].Manufacturer_BI__c);
                        if(oldInquiries.get(newInquiry.Id).Status_vod__c != UtilitiesMVN.medicalInquirySubmittedStatus 
                            && newInquiry.Status_vod__c == UtilitiesMVN.medicalInquirySubmittedStatus
                            && relatedCases.get(newInquiry.Id).Cases__r.isEmpty()
                            && countries.contains(newInquiry.stamp_Country_Code_MVN__c)
                            && newlistProducts[0].Manufacturer_BI__c == 'Boehringer-Ingelheim'){
                            
                            interactionsToCreate.add(createInteraction(newInquiry));
                        }
                }
            }
            
            if(!interactionsToCreate.isEmpty()){
                createCases(interactionsToCreate, inquiriesMap);
            }
        }
    }
// Method to insert interaction and case 
    public void createCases(List<Case> interactionsToCreate, Map<Id, Medical_Inquiry_vod__c> inquiriesMap){
        if(!interactionsToCreate.isEmpty()){
            System.debug('CASES TO CREATE: ' + interactionsToCreate);

            insert interactionsToCreate;

            List<Case> requestsToCreate = new List<Case>();

            for(Case interaction : interactionsToCreate){
                requestsToCreate.add(createRequest(inquiriesMap.get(interaction.Medical_Inquiry_MVN__c), interaction));
            }

            insert requestsToCreate;
        }   
    }
// Method to assign values to create interaction
    public Case createInteraction(Medical_Inquiry_vod__c inquiry){
        Case_Delivery_Method_Mapping_MVN__c deliveryMethodMapping = Case_Delivery_Method_Mapping_MVN__c.getInstance();
        
        Case interaction = new Case();
        interaction.RecordTypeId = interactionRecordTypeId;
        interaction.Medical_Inquiry_MVN__c = inquiry.Id;
        interaction.AccountId = inquiry.Account_vod__c;
        interaction.Sales_Rep_MVN__c = inquiry.CreatedById;
        interaction.Origin = Service_Cloud_Settings_MVN__c.getInstance().Medical_Inquiry_Source_MVN__c;
        interaction.Delivery_Method_MVN__c = Schema.SObjectType.Case_Delivery_Method_Mapping_MVN__c.fields.getMap().get(inquiry.Delivery_Method_vod__c + '__c') != null ? (String) deliveryMethodMapping.get(inquiry.Delivery_Method_vod__c + '__c') : inquiry.Delivery_Method_vod__c;
        return interaction;
    }
// Method to assign values to create request
    public Case createRequest(Medical_Inquiry_vod__c inquiry, Case interaction){
        Case_Delivery_Method_Mapping_MVN__c deliveryMethodMapping = Case_Delivery_Method_Mapping_MVN__c.getInstance();

        Case request = new Case();
        request.RecordTypeId = requestRecordTypeId;
        request.Medical_Inquiry_MVN__c = inquiry.Id;
        request.AccountId = inquiry.Account_vod__c;
        request.Sales_Rep_MVN__c = inquiry.CreatedById;
        request.ParentId = interaction.Id;
        request.Delivery_Method_MVN__c = Schema.SObjectType.Case_Delivery_Method_Mapping_MVN__c.fields.getMap().get(inquiry.Delivery_Method_vod__c + '__c') != null ? (String) deliveryMethodMapping.get(inquiry.Delivery_Method_vod__c + '__c') : inquiry.Delivery_Method_vod__c;
        request.Origin = Service_Cloud_Settings_MVN__c.getInstance().Medical_Inquiry_Source_MVN__c;
        //request.Request_MVN__c = inquiry.Inquiry_Text__c;

        return request;
    }
    
    public void handle() {
        if(Trigger.isInsert)// creating new Customer Request
            execute((List<Medical_Inquiry_vod__c>) trigger.new); 
        else if(Trigger.isUpdate)// updating existing Customer request
            execute((List<Medical_Inquiry_vod__c>) trigger.new, (Map<Id, Medical_Inquiry_vod__c>) trigger.oldMap);
    }
}