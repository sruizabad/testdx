/*
* CreateInteractionForRequestMVN
* Created By: Roman Lerman
* Created Date: 3/4/2013
* Description: This class creates an interaction for an email-to-case request
*/
public with sharing class CreateInteractionForRequestMVN implements TriggersMVN.HandlerInterface
{   
    public void execute(List<Case> newCases) {
    	Map <Case, Case> requestToInteractionMap = new Map <Case, Case> ();
			
		for (Case newRequest : newCases) {
			System.debug('Origin: '+newRequest.Origin);
			System.debug('Rec Type Id: '+newRequest.RecordTypeId);
			System.debug('Origin: '+UtilitiesMVN.interactionCreateRecordType);
			if (newRequest.Origin != null && 
				UtilitiesMVN.interactionCreateOrigin.contains(newRequest.Origin) && 
				UtilitiesMVN.matchCaseRecordTypeIdToName(newRequest.RecordTypeId, UtilitiesMVN.interactionCreateRecordType)) {
					
				Case newInteraction = newRequest.clone(false, true);
				newInteraction.RecordTypeId = [select Id from RecordType where SObjectType='Case' and DeveloperName =: UtilitiesMVN.interactionRecordType].Id;
				newInteraction.Interaction_Notes_MVN__c = Label.Email_Subject + ' ' 
															+ (newInteraction.Subject != null ? + newInteraction.Subject : '') 
															+ '\n\n' + Label.Email_Body + ' ' 
															+ (newInteraction.Description != null ? newInteraction.Description : '');
				requestToInteractionMap.put(newRequest, newInteraction);
			}
		}

		if (requestToInteractionMap.values().size() > 0) {
			Database.Insert(requestToInteractionMap.values());

			for (Case request : requestToInteractionMap.keySet()) {
				request.ParentId = requestToInteractionMap.get(request).id;
			}
		}
    }
    
    public void handle() {
        execute((List<Case>) trigger.new); 
    }
}