/*
* CreateInteractionForRequestTestMVN
* Created By: Roman Lerman
* Created Date: 6/21/2013
* Description: This is a test class for CreateInteractionForRequestMVN
*/
@isTest
private class CreateInteractionForRequestTestMVN {
	static Case emailRequest;
	static{
		TestDataFactoryMVN.createSettings();
	}
	
	static testMethod void testCreateInteraction(){
		Test.startTest();
			emailRequest = TestDataFactoryMVN.createTestEmailRequest();
		Test.stopTest();
		
		Case request = [select ParentId from Case where Id = :emailRequest.Id];
		Case interaction = [select Id, Interaction_Notes_MVN__c from Case where Id = :request.ParentId];
		
		System.assertEquals(request.ParentId, interaction.Id);
		System.assertEquals(Label.Email_Subject+' Test\n\n'+Label.Email_Body+' Test', interaction.Interaction_Notes_MVN__c);
	}
}