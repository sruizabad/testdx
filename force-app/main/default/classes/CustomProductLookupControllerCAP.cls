/*
 * Created By:   Siddharth Jain
 * Created Date: 7/17/2014
 * Description:  Custom Contoller for the VisualForce Page used for Custom Lookup
 *                 
 *              
 */


public with sharing class CustomProductLookupControllerCAP {

    
  public List<Product_vod__c> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  
  public CustomProductLookupControllerCAP () {
   
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    //  searchString ='sample';
    runSearch();  
  }
   
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
  
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 
  
  // run the search and return the records found. 
  private List<Product_vod__c> performSearch(string searchString) {

  //   String str=TEXT(YEAR(Date__c))+"-" +TEXT(MONTH(Date__c))+"-" +TEXT(DAY(Date__c));
    // System.debug(str);
    // Date d = Date.newInstance(str);
    // System.debug(d);
     Datetime d=System.now();
     
     string Date_Time = string.valueof(d);
     Date_Time = Date_Time.split(' ')[0].Trim();
     System.debug('System date :' + Date_Time );
    String soql = 'select id, name,Product_Type_vod__c,Parent_Product_vod__c,Description_vod__c,Country_Code_BI__c,External_ID_vod__c from Product_vod__c';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%'+ searchString+ +'%\'' +'and Orderable_MVN__c=true and Product_Type_vod__c!=\'Detail\' and (End_Date_MVN__c=null OR End_Date_MVN__c >='+Date_Time+')';
    soql = soql + ' limit 25';
   
    System.debug('SOQL is :' + soql);
    return database.query(soql); 

  }
  
  
  
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 
}