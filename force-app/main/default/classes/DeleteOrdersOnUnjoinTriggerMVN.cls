public with sharing class DeleteOrdersOnUnjoinTriggerMVN implements TriggersMVN.HandlerInterface {
	public DeleteOrdersOnUnjoinTriggerMVN() {
		
	}

	public void handle() {
		Set<Id> callIds = new Set<Id>();
		Map<Id,Campaign_Target_vod__c> newMap = (Map<Id,Campaign_Target_vod__c>)Trigger.newMap;

		for(Campaign_Target_vod__c ct : (List<Campaign_Target_vod__c>)Trigger.old) {
			if(String.isNotBlank(ct.Order_MVN__c) && (Trigger.isDelete || (ct.Joined_MVN__c == true && newMap.get(ct.Id).Joined_MVN__c == false ))) {
				callIds.add(ct.Order_MVN__c);
			}
		}

		if(callIds.size() > 0) {
			List<Call2_vod__c> callsToDelete = new List<Call2_vod__c>();
			List<Call2_vod__c> targetCalls = [select Id,Status_vod__c from Call2_vod__c where Id in :callIds];

			for(Call2_vod__c eachCall : targetCalls) {
				if(eachCall.Status_vod__c == Service_Cloud_Settings_MVN__c.getInstance().Call_Submitted_Status_MVN__c) {
					for(Campaign_Target_vod__c ct : (List<Campaign_Target_vod__c>)Trigger.old) {
						if(ct.Order_MVN__c == eachCall.Id) {
							ct.addError(System.label.Cannot_Unjoin_with_Submitted_Call);
						}
					}
				} else {
					callsToDelete.add(eachCall);
				}
			}

			if(callsToDelete.size() > 0) {
				delete callsToDelete;
			}
		}
	}
}