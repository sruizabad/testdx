@isTest
private class DeleteOrdersOnUnjoinTriggerTestMVN {

	static Campaign_vod__c c;
	static Campaign_Target_vod__c target;
	static Account a;
	static Call2_vod__c call;

	static {
		TestDataFactoryMVN.createSettings();

		c = TestDataFactoryMVN.createTestCampaign();
		a = TestDataFactoryMVN.createTestHCP();
		target = TestDataFactoryMVN.createTestCampaignTarget(c,a);
		call = TestDataFactoryMVN.createSavedCall();
		call.Account_vod__c = a.Id;
		update call;

		target.Order_MVN__c = call.Id;
		target.Joined_MVN__c = true;
		update target;
	}
	
	@isTest static void test_dont_delete_on_normal_update() {
		Test.startTest();
		target.Date_Joined_MVN__c = Date.today();
		update target;
		Test.stopTest();

		List<Call2_vod__c> results = [select Id from Call2_vod__c where Id = :call.Id];
		System.assertEquals(1,results.size());
	}

	@isTest static void test_dont_delete_on_submittd_update() {
		call.Status_vod__c = 'Submitted_vod';
		update call;

		Test.startTest();
		target.Date_Joined_MVN__c = Date.today();
		update target;
		Test.stopTest();

		List<Call2_vod__c> results = [select Id from Call2_vod__c where Id = :call.Id];
		System.assertEquals(1,results.size());
	}
	
	@isTest static void test_delete_on_normal_unjoin() {
		Test.startTest();
		target.Joined_MVN__c = false;
		update target;
		Test.stopTest();

		List<Call2_vod__c> results = [select Id from Call2_vod__c where Id = :call.Id];
		System.assertEquals(0,results.size());
	}

	@isTest static void test_delete_on_target_delete() {
		Test.startTest();
		delete target;
		Test.stopTest();

		List<Call2_vod__c> results = [select Id from Call2_vod__c where Id = :call.Id];
		System.assertEquals(0,results.size());
	}

	@isTest static void test_error_on_submitted_delete() {
		call.Status_vod__c = 'Submitted_vod';
		update call;

		Boolean hadError = false;
		Test.startTest();
		try {
			delete target;
		} catch (Exception e) {
			hadError = true;
		}
		Test.stopTest();

		System.assert(hadError);

		List<Call2_vod__c> results = [select Id from Call2_vod__c where Id = :call.Id];
		System.assertEquals(1,results.size());
	}
	
}