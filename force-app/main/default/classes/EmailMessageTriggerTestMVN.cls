/*
 * EmailMessageTriggerTestMVN
 * Created By:      Sathya
 * Created Date:    25/10/2018
 * Description:     This is a test class for TriggerTestMVN Trigger.
  */
  
@isTest
private class EmailMessageTriggerTestMVN{

        static List<Case> interactions;
    
        static {
            TestDataFactoryMVN.createSettings();
            interactions = TestDataFactoryMVN.createInteractions();
        }


     @isTest static void EmailMessageInsert() {    
        List<EmailMessage> emailMessages = new List<EmailMessage>();
        for (Case interaction : interactions) {
            emailMessages.add(new EmailMessage(ParentId = interaction.Id, ToAddress = 'sample'));
        }
        insert emailMessages;   
     }
}