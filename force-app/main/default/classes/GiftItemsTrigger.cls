public with sharing class GiftItemsTrigger{

    public void executeTrigger(Case c){
        if(c.Coupon_Code__c!=null){
        VAS_BI__c vas=[SELECT id,Promotional_Items_MX_Cap__c from VAS_BI__c where id =:c.Coupon_Code__c];
        c.All_Gifts_c__c=vas.Promotional_Items_MX_Cap__c;
        }else if(c.Coupon_Code__c==null) c.All_Gifts_c__c='';
    }
}