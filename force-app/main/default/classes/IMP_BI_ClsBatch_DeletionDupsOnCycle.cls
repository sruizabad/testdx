/**
* ===================================================================================================================================
*                                   IMMPaCT BI                                                     
* ===================================================================================================================================
*  Decription:      Clean up cycle data duplicated on current cycles
*  @author:         Jefferson Escobar
*  @created:        18-March-2015
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          33.0 (Force.com ApiVersion) 
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         18-March-2015                jescobar                   Construction of the class.
*/ 
global class IMP_BI_ClsBatch_DeletionDupsOnCycle implements Database.Batchable<SObject>, Database.Stateful{

    /** final variables */
    global final String query;
    global Set<Id> immpactUsers;
    global Set<Id> setMatrices;
    global Set<Id> setCycles;
    
    //TODO: Delete var
    global List<Cycle_Data_BI__c> dupsSummary;
    
    global IMP_BI_ClsBatch_DeletionDupsOnCycle(String q){
        this.query = q;
        system.debug(':: Query: ' + query);
        this.setMatrices = new Set<Id>();
        this.setCycles = new Set<Id>();
    }
    
    /**
    * Retrieve...
    *
    * @param BC batchable context
    * @return
    *
    * @changelog
    * 18-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
    }
    
    /**
    * Get records duplicated and delete them
    *
    * @param BC batchable context
    * @param List<Objects>...
    * @return
    *
    * @changelog
    * 18-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    global void execute(Database.BatchableContext BC, List<Cycle_Data_BI__c> listCD){
        List<Cycle_Data_BI__c> dups2Delete = new List<Cycle_Data_BI__c>();
        //Query all cycle data and detect duplicates
        for(Cycle_Data_BI__c cd : listCD){
            
            Id keyAccId = (Id) cd.UniqueKey_BI__c.substring(19, 37);
            Id idAcc = (Id) cd.Account_BI__c;
    
            if(idAcc != keyAccId){
                dups2Delete.add(cd);//Add duplicated
        		
                if(cd.Matrix_BI__c != null){// Matrices to reset values
                    setMatrices.add(cd.Matrix_BI__c);
                }
                
                if(cd.Matrix_1_BI__c != null){// Matrices to reset values
                    setMatrices.add(cd.Matrix_1_BI__c);
                }
                
                if(cd.Matrix_2_BI__c != null){// Matrices to reset values
                    setMatrices.add(cd.Matrix_2_BI__c);
                }
                
                if(cd.Matrix_3_BI__c != null){// Matrices to reset values
                    setMatrices.add(cd.Matrix_3_BI__c);
                }
                
                if(cd.Matrix_4_BI__c != null){// Matrices to reset values
                    setMatrices.add(cd.Matrix_4_BI__c);
                }
                
                if(cd.Matrix_5_BI__c != null){// Matrices to reset values
                    setMatrices.add(cd.Matrix_5_BI__c);
                }
                
                if(cd.Cycle_BI__c != null)// cycles to reset values
                    setCycles.add(cd.Cycle_BI__c);
                } 
            }
          
          //Deleting records duplicated
          if(dups2Delete.size()>0){
          	database.delete(dups2Delete);
            database.emptyRecycleBin(dups2Delete);
            //system.debug(':: Dups deleted: ' + dups2Delete.size());
          }  
     }
   
    /**
    * Set information batch
    *
    * @param BC batchable context
    * @return
    *
    * @changelog
    * 18-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    global void finish(Database.BatchableContext BC){
        //Del dups, mark matrices and cycles as contained duplicates
        syncCyclesOnDups();
      }
    
    /**
    * Updating values for specialties
    *
    * @return
    *
    * @changelog
    * 27-Mar-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */  
    global void syncCyclesOnDups(){
        Savepoint sp = Database.setSavepoint();
        try{
            //Update matrices which cycle data records used to belong
            if(setMatrices.size()>0){
                List<Matrix_BI__c> matrices2Update = [Select Id From Matrix_BI__c where Id in :setMatrices and Current_BI__c = true]; 
                //Get list of matrix to update
                for(Matrix_BI__c matrix : matrices2Update){
                	//matrices2Update.add(new Matrix_BI__c (Id = matrixId,Has_Been_Merged_BI__c = true));
                	matrix.Has_Been_Merged_BI__c = true;
                    //system.debug('::  Id Matrix: ' +  matrixId);
                }
                if(matrices2Update.size()>0){
                    system.debug('::  matrices2Update: ' +  matrices2Update.size());
                    update matrices2Update;
                }
            }
            
            //Update cycles which cycle data records used to belong
            if(setCycles.size()>0){
                List<Cycle_BI__c> cycles2Update = new List<Cycle_BI__c>();
                for(Id cycleId : setCycles){
                    cycles2Update.add(new Cycle_BI__c (Id=cycleId, Has_Been_Merged_BI__c = true));
                }
                
                if(cycles2Update.size()>0){
                    system.debug('::  cycles2Update: ' +  cycles2Update.size());
                    update cycles2Update; 
                }
             }
        }catch(Exception e){
            Database.rollback(sp);
            system.debug('[ERROR] ' + e.getMessage());
        }
    }
}