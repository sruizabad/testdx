/**
 *  This batch class is used to update cycle data
 *
 @author Xiong Yao
 @created 2015-04-09
 @version 1.0
 @since 31.0 (Force.com ApiVersion)
 *
 @changelog
 * 2015-04-09 Xiong Yao <xiong.yao@itbconsult.com>
 * - Create
 *
 */
global class IMP_BI_ClsBatch_NumberInfoFieldGrouping implements Database.Batchable<SObject>, Database.Stateful{

    global String query;
    global String filter;  
    global resMInfoB MInfoB;
    
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        String queryStr = query + filter +'\''+ MInfoB.cId + '\'';
        system.debug('yaoxiongyu: '+queryStr);
        return Database.getQueryLocator(queryStr);  
    }

    global void execute(Database.BatchableContext BC, list<Cycle_Data_BI__c> list_cycleData){
        for(Integer i = 0; i < list_cycleData.size(); i++) {
            Cycle_Data_BI__c cd = list_cycleData.get(i);
            Decimal num = (Decimal)cd.get(MInfoB.numberField);
            for(Integer j = 0; j < MInfoB.listreqMInfo.size(); j++) {
                reqMInfo rmf = MInfoB.listreqMInfo.get(j);
                if(num != null && num >= rmf.f){
                    if(num <= rmf.t) {
                        if(j != MInfoB.listreqMInfo.size() -1 && num != rmf.t) {
                            cd.put(MInfoB.textField, rmf.v);
                        }else{
                            cd.put(MInfoB.textField, rmf.v);
                        }
                    }
                }
            }
        }
        system.debug('yaoxiongyu####################' + list_cycleData);
        update list_cycleData;
    }

    global void finish(Database.BatchableContext BC){
        Cycle_BI__c cb = new Cycle_BI__c();
        cb.Id = MInfoB.cId;
        cb.put(MInfoB.textField, MInfoB.tfLabel);
        cb.Text_Number_Combination_Value__c = MInfoB.tncv;
        update cb;
    }
    
    public class reqMInfo {
        public Decimal f {get; set;}
        public Decimal t {get; set;}
        public String v {get; set;}
    }
    
    public class resMInfoB {
        public List<reqMInfo> listreqMInfo {get; set;}
        public String numberField {get; set;}
        public String textField {get; set;}
        public String tfLabel {get; set;}
        public String tncv {get; set;}
        public Id cId {get; set;}
    }
}