/**
*   Test class for class IMP_BI_ClsBatch_calculateDataCycleView.
*
@author Di Chen
@created 2013-06-14
@version 1.0
@since 20.0
*
@changelog
* 2013-06-14 Di Chen <di.chen@itbconsult.com>
* - Created
*- Test coverage 89%
*/
@isTest
private class IMP_BI_ClsBatch_calcDataCycView_Test {
    static testMethod void testBatch() {
        
        	Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
            c.Specialty_API_BI__c = 'Specialty_1_vod__c';
            c.Country_Code_BI__c = 'Z0';
            insert c;
        
            Account acc = IMP_BI_ClsTestHelp.createTestAccount();
            acc.Name = '123e';
            acc.Specialty_1_vod__c = 'Cardiology';
            insert acc;
        
            Cycle_BI__c cycle2 = new Cycle_BI__c();
            cycle2.Country_BI__c = 'USA';
            cycle2.Start_Date_BI__c = date.today() - 10;
            cycle2.End_Date_BI__c = date.today() + 10;
            cycle2.IsCurrent_BI__c = false;
            cycle2.Country_Lkp_BI__c = c.Id;
            insert cycle2;
        
            Product_vod__c p2 = IMP_BI_ClsTestHelp.createTestProduct();
            p2.Country_BI__c = c.Id;
            insert p2;
            
            Matrix_BI__c ma = IMP_BI_ClsTestHelp.createTestMatrix();
            ma.Cycle_BI__c = cycle2.Id;
            ma.Product_Catalog_BI__c = p2.Id;
            ma.Intimacy_Levels_BI__c = 11;
            ma.Potential_Levels_BI__c = 10;
            ma.Size_BI__c = '10x11';
            ma.Column_BI__c = 11;
            ma.Specialization_BI__c = 'GP';//Peng Zhu 2013-10-14
            ma.Status_BI__c = 'Draft';
            insert ma;
            
            Lifecycle_Template_BI__c mt = IMP_BI_ClsTestHelp.createTestLifecycleTemplateBI();
            mt.Name = 'mt';
            mt.Country_BI__c = c.Id;
            mt.Active_BI__c = true;
            insert mt;
        
            Matrix_Cell_BI__c mc = IMP_BI_ClsTestHelp.createTestMatrixCell();
            mc.Matrix_BI__c = ma.Id;
            insert mc;
        
            Cycle_Data_BI__c cd = new Cycle_Data_BI__c();
            cd.Product_Catalog_BI__c = p2.Id;
            cd.Account_BI__c = acc.Id;
            cd.Account_BI__r = acc;
            cd.Cycle_BI__c = cycle2.Id;
            cd.Potential_BI__c = 12;
            cd.Intimacy_BI__c = 12;
            cd.Current_Update_BI__c = true;
            insert cd;    
            
            list<Cycle_Data_BI__c> scope = new list<Cycle_Data_BI__c>();
            scope.add(cd);
            
            Test.startTest();
            
            IMP_BI_ClsBatch_calculateDataCycleView batchCls = new IMP_BI_ClsBatch_calculateDataCycleView(cycle2.Id); 
            batchCls.filter = ' limit 1';
        
            Database.BatchableContext bc;
            batchCls.start(bc);
            batchCls.execute(bc, scope);
            batchCls.finish(bc);
        
            system.assert(true); 
            Test.stopTest();  
    }
    
   	/**
    *   Test method is used to test the function about generate list of cycle data overview.
    *
    @author Di Chen
    @created 2013-06-14
    @version 1.0
    @since 20.0
    *
    @changelog
    * 2013-06-14 Di Chen <di.chen@itbconsult.com>
    * - Created
    */
    static testMethod void test_generateListOfCycleDataOverview() {
            Id fakeCycleId = generateFakeId(Cycle_BI__c.sObjectType);
            Id fakeProductId = generateFakeId(Product_vod__c.sObjectType);
        
            IMP_BI_ClsBatch_calculateDataCycleView clsInst = new IMP_BI_ClsBatch_calculateDataCycleView(String.valueOf(fakeCycleId));
        
            Test.startTest();
        
            String matchKeyHCP = String.valueOf(fakeCycleId) + ':' + String.valueOf(fakeProductId) + ':HCP';
            String matchKeyHCO = String.valueOf(fakeCycleId) + ':' + String.valueOf(fakeProductId) + ':HCO';
        
            clsInst.map_matchKey_counter.put(matchKeyHCP, 2);
            clsInst.map_matchKey_potential.put(matchKeyHCP, 3);
            clsInst.map_matchKey_adoption.put(matchKeyHCP, 4);
            clsInst.map_matchKey_ams.put(matchKeyHCP, new set<String>{'AAA', 'BBB', 'CCC'});
        
            clsInst.map_matchKey_counter.put(matchKeyHCO, 5);
            clsInst.map_matchKey_potential.put(matchKeyHCO, 6);
            clsInst.map_matchKey_adoption.put(matchKeyHCO, 7);
            clsInst.map_matchKey_ams.put(matchKeyHCO, new set<String>{'AAA', 'BBB', 'DDD'});
        
            list<Cycle_Data_Overview_BI__c> list_cdo = clsInst.generateListOfCycleDataOverview();
        
            system.assertEquals(2, list_cdo.size());
        
            Test.stopTest();    
    }
    
    /**
    *   Test method is used to test the function about convert StringSet To String.
    *
    @author Di Chen
    @created 2013-06-14
    @version 1.0
    @since 20.0 
    *
    @changelog
    * 2013-06-14 Di Chen <di.chen@itbconsult.com>
    * - Created
    */
    static testMethod void test_convertStringSetToString() {
            Id fakeCycleId = generateFakeId(Cycle_BI__c.sObjectType);
        
            IMP_BI_ClsBatch_calculateDataCycleView clsInst = new IMP_BI_ClsBatch_calculateDataCycleView(String.valueOf(fakeCycleId));
        
            set<String> set_str = new set<String>{'AAA', 'BBB', 'CCC'};
        
            Test.startTest();
        
            String convStr = clsInst.convertStringSetToString(set_str, ';');
        
            system.assertEquals('AAA;BBB;CCC', convStr);
        
            convStr = clsInst.convertStringSetToString(set_str, ':');
        
            system.assertEquals('AAA:BBB:CCC', convStr);
        
            Test.stopTest();
    }   
    
    //********************************* -=BEGIN test help functions=- **********************************        
    private static Integer fakeIdCount = 0;
    private static final String ID_PATTERN = '000000000000';
    
    /**
     * Generate a fake Salesforce Id for the given sObjectType
     */
    private static Id generateFakeId(Schema.sObjectType sObjectType) {
        String keyPrefix = sObjectType.getDescribe().getKeyPrefix();
        
        fakeIdCount++;

        String fakeIdPrefix = ID_PATTERN.substring(0, 12 - fakeIdCount.format().length());

        return Id.valueOf(keyPrefix + fakeIdPrefix + fakeIdCount);
    } 
    //********************************* -=END test help functions=- ************************************    
}