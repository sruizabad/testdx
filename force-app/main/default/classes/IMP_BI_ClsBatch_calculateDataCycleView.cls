/**
 *  This batch class is used to calculate cycle data overview by using related cycle data
 *
 @author Yuanyuan Zhang
 @created 2013-05-29
 @version 1.0
 @since 27.0 (Force.com ApiVersion)
 *
 @changelog
 * 2015-01-29 Peng Zhu <peng.zhu@itbconsult.com>
 * - Modified  1. distinguish the HCP and HCO cycle data   2. Only run once for same cycyle at the same time 
 *
 * 2014-04-11 Jefferson Escobar <jescobar@omegacrmconsulting.com>
 * - Modified
 */

global class IMP_BI_ClsBatch_calculateDataCycleView implements Database.Batchable<SObject>, Database.Stateful{

    global String query; 
    global String filter;  
    
        // Begin : added by Peng Zhu 2015-01-29 - distinguish the HCP and HCO cycle data
        @TestVisible    
        private map<String, Integer> map_matchKey_counter = new map<String, Integer>();

        @TestVisible
        private map<String, Decimal> map_matchKey_potential = new map<String, Decimal>();
    
        @TestVisible
        private map<String, Decimal> map_matchKey_adoption = new map<String, Decimal>();

        @TestVisible
        private map<String, set<String>> map_matchKey_ams = new map<String, set<String>>();
        
        // Begin : added by Hely 2015-02-12 - add the departmentType for (distinguish the HCP and HCO cycle data)
        @TestVisible
        private map<String, set<String>> map_matchKey_dType = new map<String, set<String>>();
        // End : added by Hely 2015-02-12 - add the departmentType for (distinguish the HCP and HCO cycle data)
        // End : added by Peng Zhu 2015-01-29 - distinguish the HCP and HCO cycle data
        
        //Count number of specialties
        @TestVisible
        private map<String,Set<String>> map_specialtiesByCycle_counter = new map<String,Set<String>>();

    private Id cycleId;
    private String countryCode;
    private Id countryID;
    private String specialtyAPI;     
    
    /**
    *Default constructor
    */ 
    public IMP_BI_ClsBatch_calculateDataCycleView(String cycId) {    
            cycleId = getFullId(cycId);
        
            list<Cycle_Data_Overview_BI__c> list_cdo = new list<Cycle_Data_Overview_BI__c>();
            for(Cycle_Data_Overview_BI__c cdo : [SELECT Id,Cycle_BI__c,  Cycle_BI__r.Country_Lkp_BI__r.Specialty_API_BI__c, Cycle_BI__r.Country_Lkp_BI__r.Country_Code_BI__c FROM Cycle_Data_Overview_BI__c WHERE Cycle_BI__c = :cycId]){
                list_cdo.add(cdo);
            }
            
            //Get parameters to load specialties at cyecle level
            List<Cycle_BI__c> cycles = [Select Country_Lkp_BI__r.Specialty_API_BI__c, Country_Lkp_BI__r.Country_Code_BI__c, Country_Lkp_BI__c From Cycle_BI__c
                                    where Id = :cycleId];
            if(cycles!=null&&!cycles.isEmpty())
            {   
                Cycle_BI__c cycle = cycles.get(0);
                specialtyAPI = cycle.Country_Lkp_BI__r.Specialty_API_BI__c;
                countryCode = cycle.Country_Lkp_BI__r.Country_Code_BI__c;
                countryID = cycle.Country_Lkp_BI__c;
            }                                   
            
            if(list_cdo.size() != 0){
                delete list_cdo;
            }
            
            //Delete specialties related to the cycle
            List<Specialty_by_Cycle__c> specialties2Delete = [Select Id From Specialty_by_Cycle__c where Cycle_BI__c = :cycleId];
            if(!specialties2Delete.isEmpty())
                delete specialties2Delete;
        }   
    
    /**
    *Batch start
    */ 
        global Database.QueryLocator start(Database.BatchableContext BC){ 
         if(query == null || query == '') {
            //Begin: added by Hely 2015-02-12 - add Customer_Role_BI__c field and Department_Type_BI__c field in query.
             query = 'SELECT Department_Type_BI__c, Customer_Role_BI__c, Cycle_BI__c,Account_BI__c, Is_Person_Account_BI__c, Product_Catalog_BI__c,Potential_BI__c,Intimacy_BI__c,Account_Matrix_Split_BI__c, Account_BI__r.'+specialtyAPI
                        +' FROM Cycle_Data_BI__c ';
             //End: added by Hely 2015-02-12 - add Customer_Role_BI__c Field in query.
             if(filter != null) query += filter;
         }  
        
            system.debug(':: Query: ' + query);
            return Database.getQueryLocator(query);  
     }
    
    /**
    *Batch execute
    */ 
    global void execute(Database.BatchableContext BC, list<Cycle_Data_BI__c> list_cycleData){
            calculateCycleDataOverview(list_cycleData);
            //Begin: added by Hely 2015-02-12 - Used to add customer role.  
            saveCustomerRole(list_cycleData, cycleId);
            //End: added by Hely 2015-02-12 - Used to add customer role. 
        }

    /**
    *Batch finish
    */ 
        global void finish(Database.BatchableContext BC){
            set<Id> set_cycleId = getSetOfCycleIdFromMap();
            
            list<Cycle_Data_Overview_BI__c> list_cdo2Del = [SELECT Id FROM Cycle_Data_Overview_BI__c WHERE Cycle_BI__c IN :set_cycleId];
    
            if(!list_cdo2Del.isEmpty()) 
                delete list_cdo2Del;
            
            list<Cycle_Data_Overview_BI__c> list_cdo = generateListOfCycleDataOverview();
            
            //Get list of specialties
            list<Specialty_by_Cycle__c> specialties = generateSpecialtiesOverview();
            
            if(!specialties.isEmpty())
                insert specialties;
            
            if(!list_cdo.isEmpty()) 
                insert list_cdo;
        
            // Begin: added by Peng Zhu 2015-02-02 - delete the batch job info in custom setting 
            Id batchJobId = (BC != NULL ? BC.getJobId() : NULL);
        
            if(batchJobId != NULL) {
                String batchJobName = 'CycleDataOverview-' + String.valueOf(cycleId);
                
                    IMP_BI_Default_Setting__c dsInst = IMP_BI_Default_Setting__c.getValues(batchJobName);
                    
                    if(dsInst != NULL && String.isNotBlank(dsInst.Id_Value_BI__c) && dsInst.Id_Value_BI__c.contains(String.valueOf(batchJobId))) {
                delete dsInst;
                }       
        }
        
            //get eamil templae id
            Id emailTemplateId = fetchEmailTemplateId();
            
            if(UserInfo.getUserId() != NULL && emailTemplateId != NULL) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(UserInfo.getUserId());
                mail.setTemplateId(emailTemplateId);
                mail.setSaveAsActivity(false);
                mail.setUseSignature(false);
                //if(!Test.isRunningTest()) Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
            // End: added by Peng Zhu 2015-02-02 - delete the batch job info in custom setting
    }
    
    /**
    * Test method is used to calculate each cycle data overview.
    *
    * @author Yuanyuan Zhang
    * @created 2013-05-29
    * @param cycle data list to be calculated
    * @return
    *
    * @changelog
    * 2013-05-29 Yuanyuan Zhang <yuanyuan.zhang@itbconsult.com>
    * - Created
    */
    @TestVisible
    private void calculateCycleDataOverview(list<Cycle_Data_BI__c> list_cd) {
            if(list_cd != NULL && !list_cd.isEmpty()) {
                for(Cycle_Data_BI__c cd : list_cd) {
                    calculateEachCycleData(cd);
                }   
            }
    }

    /**
    * Test method is used to calculate each cycle data.
    *
    * @author Yuanyuan Zhang
    * @created 2013-05-29
    * @param cycle data list to be calculated
    * @return
    *
    * @changelog
    * 2013-05-29 Yuanyuan Zhang <yuanyuan.zhang@itbconsult.com>
    * - Created
    */
    @TestVisible    
    private void calculateEachCycleData(Cycle_Data_BI__c cd) {
        if(cd != NULL && cd.Cycle_BI__c != NULL && cd.Product_Catalog_BI__c != NULL) {
            Id cycleId = cd.Cycle_BI__c;
            Id prodId = cd.Product_Catalog_BI__c;
            
            String cdType = ((cd.Is_Person_Account_BI__c == TRUE) ? 'HCP' : 'HCO');
            
            String matchKey = String.valueOf(cycleId) + ':' + String.valueOf(prodId) + ':' + cdType;
            
            //Counter
            if(map_matchKey_counter.get(matchKey) == NULL) {
                map_matchKey_counter.put(matchKey, 1);
            }
            else {
                Integer counter = map_matchKey_counter.get(matchKey);
                map_matchKey_counter.put(matchKey, ++counter);
            }
            
            //Potential
            if(cd.Potential_BI__c != NULL) {
                Decimal cdPotential = cd.Potential_BI__c;
                
                if(map_matchKey_potential.get(matchKey) == NULL) {
                    map_matchKey_potential.put(matchKey, cdPotential);
                }
                else {
                    cdPotential += map_matchKey_potential.get(matchKey);
                    map_matchKey_potential.put(matchKey, cdPotential);
                }
            }
            
            //Adoption
            if(cd.Intimacy_BI__c != NULL) {
                Decimal cdAdoption = cd.Intimacy_BI__c;
                
                if(map_matchKey_adoption.get(matchKey) == NULL) {
                    map_matchKey_adoption.put(matchKey, cdAdoption);
                }
                else {
                    cdAdoption += map_matchKey_adoption.get(matchKey);
                    map_matchKey_adoption.put(matchKey, cdAdoption);
                }               
            }
            
            //AMS
            if(String.isNotBlank(cd.Account_Matrix_Split_BI__c) && !cd.Is_Person_Account_BI__c) {
                String ams = cd.Account_Matrix_Split_BI__c;
                
                if(map_matchKey_ams.get(matchKey) == NULL) {
                    map_matchKey_ams.put(matchKey, new set<String>());
                }               
                
                map_matchKey_ams.get(matchKey).add(ams);    
            }
            
            //Counter Specialties
            if(cd.Is_Person_Account_BI__c && cd.getSObject('Account_BI__r') != null && cd.getSObject('Account_BI__r').get(specialtyAPI) != null && cd.getSObject('Account_BI__r').get(specialtyAPI)!=''){
                String specialty = (String)cd.getSObject('Account_BI__r').get(specialtyAPI);
                Id accID = (Id)cd.Account_BI__c;
                //system.debug(':: Specialty: ' + cd.getSObject('Account_BI__r').get(specialtyAPI));
                if(map_specialtiesByCycle_counter.containsKey(specialty)){
                	Set<String> setAccounts = map_specialtiesByCycle_counter.get(specialty);
                	setAccounts.add(cd.Account_BI__c);
                    map_specialtiesByCycle_counter.put(specialty,setAccounts);
                }else{
                    map_specialtiesByCycle_counter.put(specialty,new Set<String>{cd.Account_BI__c});
                }
            }
            
            //Begin: add by Hely 2015-02-12 - add Department_Type_BI__c like Account_Matrix_Split_BI__c
            //DType
            if(String.isNotBlank(cd.Department_Type_BI__c) && !cd.Is_Person_Account_BI__c) {
                String dType = cd.Department_Type_BI__c;
                
                if(map_matchKey_dType.get(matchKey) == NULL) {
                    map_matchKey_dType.put(matchKey, new set<String>());
                }               
                
                map_matchKey_dType.get(matchKey).add(dType);    
            }
            //End: add by Hely 2015-02-12 - add Department_Type_BI__c like Account_Matrix_Split_BI__c
        }
    }
    
    /**
    * Test method is used to generate list of cycle data overview.
    *
    * @author Yuanyuan Zhang
    * @created 2013-05-29
    * @param 
    * @return cycle data overview list be generated
    *
    * @changelog
    * 2013-05-29 Yuanyuan Zhang <yuanyuan.zhang@itbconsult.com>
    * - Created
    */
    @TestVisible
    private list<Cycle_Data_Overview_BI__c> generateListOfCycleDataOverview() {
        list<Cycle_Data_Overview_BI__c> list_cdo = new list<Cycle_Data_Overview_BI__c>();
        
        if(!map_matchKey_counter.isEmpty()) {
            for(String matchKey : map_matchKey_counter.keySet()) {
                String[] matchKeys = matchKey.split(':');
                Id cycleId = Id.valueOf(matchKeys[0]);
                Id prodId = Id.valueOf(matchKeys[1]);
                String cdType = matchKeys[2];
                
                Cycle_Data_Overview_BI__c cdo = new Cycle_Data_Overview_BI__c();
                cdo.Cycle_BI__c = cycleId;
                    cdo.Product_Catalog_BI__c = prodId;
                    cdo.Type_BI__c = cdType;
                
                    cdo.Count_BI__c = map_matchKey_counter.get(matchKey);
                
                    if(map_matchKey_potential.get(matchKey) != NULL) cdo.Total_Potential_BI__c = map_matchKey_potential.get(matchKey);
                    if(map_matchKey_adoption.get(matchKey) != NULL) cdo.Total_Adoption_BI__c = map_matchKey_adoption.get(matchKey);
                
                    if(map_matchKey_ams.get(matchKey) != NULL) cdo.Account_Matrix_Split_BI__c = convertStringSetToString(map_matchKey_ams.get(matchKey), ';');
                    //Begin: add by Hely 2015-02-12 - add Department_Type_BI__c like Account_Matrix_Split_BI__c
                    if(map_matchKey_dType.get(matchKey) != NULL) cdo.Department_Types_BI__c = convertStringSetToString(map_matchKey_dType.get(matchKey), ';');
                    //End: add by Hely 2015-02-12 - add Department_Type_BI__c like Account_Matrix_Split_BI__c
                    list_cdo.add(cdo);
            }
        }
        return list_cdo;
    }
    
    
    /**
    * Getting Summary Specialty Overview.
    *
    * @author Jefferson Escobar
    * @created 2015-12-01
    * @param 
    * @return List of Specialties by Cycle
    *
    * @changelog
    * 2015-12-01 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    @TestVisible
    private list<Specialty_by_Cycle__c> generateSpecialtiesOverview() {
        List<Specialty_by_Cycle__c> list_specialties = new List<Specialty_by_Cycle__c>();
        
        if(!map_specialtiesByCycle_counter.isEmpty()) {
            
            Map<String,Specialty_Grouping_Config__c> mapSpecialtyConfig = Specialty_Grouping_Config__c.getAll();
            if(countryCode!=null && mapSpecialtyConfig.containsKey(countryCode)){//Validate if the country is utilizing specialties as a picklist
                for(Specialty_Grouping_BI__c specialty : [Select Id, Name From Specialty_Grouping_BI__c where Name in :map_specialtiesByCycle_counter.keySet() and Country_BI__c  = :this.countryID]){
                    list_specialties.add(new Specialty_by_Cycle__c(Cycle_BI__c = this.cycleId, Summary_Specialties_Account_Number_BI__c = map_specialtiesByCycle_counter.get(specialty.Name).size(), 
                                            Specialty_Grouping_BI__c = specialty.Id));
                }       
            }else{
                for(Customer_Attribute_BI__c specialty : [Select Id, Name, Group_txt_BI__c From Customer_Attribute_BI__c where Id in :map_specialtiesByCycle_counter.keySet()]){
                    list_specialties.add(new Specialty_by_Cycle__c(Cycle_BI__c = this.cycleId,  Customer_Attribute_BI__c = specialty.Id, 
                        Summary_Specialties_Account_Number_BI__c = map_specialtiesByCycle_counter.get(specialty.Id).size()));
                }
            }
        }
        return list_specialties;
    }
    
    /**
    * Test method is used to convert StringSet To String.
    *
    * @author Yuanyuan Zhang
    * @created 2013-05-29
    * @param string set,split used in string
    * @return string from set
    *
    * @changelog
    * 2013-05-29 Yuanyuan Zhang <yuanyuan.zhang@itbconsult.com>
    * - Created
    */  
    @TestVisible
    private String convertStringSetToString(set<String> set_input, String split) {
        String rtStr = NULL;
        if(set_input != NULL && !set_input.isEmpty() && split != NULL) {
            list<String> list_input = new list<String>(set_input);
            list_input.sort();
            
            for(String str : list_input) {
                if(str != NULL) {
                    if(rtStr == NULL) rtStr = str;
                    else rtStr += split + str;
                }
            }
        }
        return rtStr;
    }
    
    /**
    * Test method is used to get set of cycleId from map.
    *
    * @author Yuanyuan Zhang
    * @created 2013-05-29
    * @param 
    * @return cycleId set
    *
    * @changelog
    * 2013-05-29 Yuanyuan Zhang <yuanyuan.zhang@itbconsult.com>
    * - Created
    */  
    @TestVisible    
    private set<Id> getSetOfCycleIdFromMap() {
        set<Id> set_cycleId = new set<Id>();
        
        if(!map_matchKey_counter.isEmpty()) {
            for(String matchKey : map_matchKey_counter.keySet()) {
                String[] keys = matchKey.split(':');
                
                set_cycleId.add(Id.valueOf(keys[0]));
            }
        }
        
        return set_cycleId;
    }
    
    /**
    * Test method is used to convert string to id.
    *
    * @author Yuanyuan Zhang
    * @created 2013-05-29
    * @param  string
    * @return id 
    *
    * @changelog
    * 2013-05-29 Yuanyuan Zhang <yuanyuan.zhang@itbconsult.com>
    * - Created
    */  
    @TestVisible
    private static Id getFullId(String inId) {
        Id rtId;
        
        try {
            rtId = Id.valueOf(inId);
        }
        catch(Exception e) {}
        
        return rtId;
    }
    
    /**
    * Test method is used to fetch email template id from custom setting.
    *
    * @author Yuanyuan Zhang
    * @created 2013-05-29
    * @param  
    * @return email template id 
    *
    * @changelog
    * 2013-05-29 Yuanyuan Zhang <yuanyuan.zhang@itbconsult.com>
    * - Created
    */  
    @TestVisible    
    private static Id fetchEmailTemplateId() {
        Id emailTemplateId;
        
        IMP_BI_Default_Setting__c dsInst = IMP_BI_Default_Setting__c.getValues('CycleDataOverviewEmailTemplateId');
        
            if(dsInst != NULL) {
                emailTemplateId = getFullId(dsInst.Id_Value_BI__c);
            }
        
            return emailTemplateId; 
    }
    
    //Begin: added by Hely 2015-02-12 - Used to add customer role where Is_Person_Account_BI__c = true.
    /**
    * Test method is used to insert customer role grouping.
    *
    * @author Yuanyuan Zhang
    * @created 2013-05-29
    * @param  list cycle data,cycle id
    * @return 
    *
    * @changelog
    * 2013-05-29 Yuanyuan Zhang <yuanyuan.zhang@itbconsult.com>
    * - Created
    */
    @TestVisible    
    private static void saveCustomerRole(list<Cycle_Data_BI__c> list_cycleData, Id cycleId) {
        set<String> set_crName = new set<String>();
        
        for(Cycle_Data_BI__c cd : list_cycleData) {
            if(String.isNotBlank(cd.Customer_Role_BI__c) && cd.Is_Person_Account_BI__c) {
            //if(String.isNotBlank(cd.Customer_Role_BI__c)) {
                set_crName.add(cd.Customer_Role_BI__c);
            }
        }
        
        if(!set_crName.isEmpty()) {
            for(Customer_Role_Grouping_BI__c cr : [SELECT Name FROM Customer_Role_Grouping_BI__c  WHERE Cycle_BI__c = :cycleId AND Name IN :set_crName]) {
                if(set_crName.contains(cr.Name)) set_crName.remove(cr.Name);
            }
        }
        
        if(!set_crName.isEmpty()) {
            list<Customer_Role_Grouping_BI__c> list_cr = new list<Customer_Role_Grouping_BI__c>();
        
            for(String crName : set_crName) {
                Customer_Role_Grouping_BI__c cr = new Customer_Role_Grouping_BI__c();
                cr.Name = crName;
                cr.Cycle_BI__c = cycleId;
                list_cr.add(cr);
            }
            if(!list_cr.isEmpty()) insert list_cr;
        }
    }
    //End: added by Hely 2015-02-12 - Used to add customer role.
}