/**
* ===================================================================================================================================
*                                   IMMPaCT BI                                                     
* ===================================================================================================================================
*  Decription:      Scheduler class to clean up cycle data duplicated on current cycles
*  @author:         Jefferson Escobar
*  @created:        26-Mar-2015
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          33 (Force.com ApiVersion)   
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         26-Mar-2015                 jescobar                    Construction of the class.
*/ 

global class IMP_BI_ClsScheduler_DeletionDupsOnCycle implements Schedulable{
    
    global void execute(SchedulableContext sc) { 
        //Get list of countries that run customer merge process
        Map<String,IMP_BI_Deletion_Dups_Settings__c> deletionDupsSetting = IMP_BI_Deletion_Dups_Settings__c.getAll();
        Integer limitRows = IMP_BI_Limit_Queries__c.getAll().get('Deletion_Dups') != null ? Integer.valueOf(IMP_BI_Limit_Queries__c.getAll().get('Deletion_Dups').Limit_BI__c) : 100000;
        
        String countryCodes='';
        String users ='';
        
       	for(IMP_BI_Deletion_Dups_Settings__c setting : deletionDupsSetting.values()){
       		countryCodes+='\''+setting.Name+'\',';
       		users+='\''+setting.UserId__c+'\',';
       	}
       	//Remove last comma character
       	countryCodes=countryCodes.substring(0,countryCodes.length()-1);
       	users = users.substring(0,users.length()-1);
       	
       	//Execute batch apex batch class        
        String query = 'Select Id, Cycle_BI__c, Matrix_BI__c, Matrix_1_BI__c, Matrix_2_BI__c, Matrix_3_BI__c, Matrix_4_BI__c, Matrix_5_BI__c, Account_BI__c, LastModifiedById, UniqueKey_BI__c '+ 
                        'From Cycle_Data_BI__c where LastModifiedById in ('+users+') and Account_BI__r.Country_Code_BI__c in ('+countryCodes+') And  Cycle_BI__r.IsCurrent_BI__c = true '+ 
                        'and SystemModStamp = THIS_YEAR LIMIT ' + limitRows;
        IMP_BI_ClsBatch_DeletionDupsOnCycle batch =  new IMP_BI_ClsBatch_DeletionDupsOnCycle(query);
        Integer batchSize = 200;
        if(!test.isRunningTest()){
            Database.executeBatch(batch,batchSize); 
        } 
    }
}