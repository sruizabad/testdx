/**
* ===================================================================================================================================
*                                   IMMPaCT BI                                                     
* ===================================================================================================================================
*  Decription:      Scheduler class to manage the especialties for USA based on Especialty_1_VOD_field__c on Account object
*  @author:         Jefferson Escobar
*  @created:        20-Feb-2014
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:			29.0 (Force.com ApiVersion)	
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         20-Feb-2014                 jescobar                 	Construction of the class.
*/ 
 
global class IMP_BI_ClsScheduler_ManageSpecialtiesUSA implements Schedulable{
	
	global void execute(SchedulableContext sc) { 
		//Execute batch apex batch class
		runBatch();
	}
	
	void runBatch(){
		Map<String,Specialty_Grouping_Config__c> mapSpecialtySetting = Specialty_Grouping_Config__c.getAll();
		
		if(mapSpecialtySetting!=null && !mapSpecialtySetting.isEmpty()){
			//Run logic job
			new IMP_BI_ManageSpecialtiesUSA(mapSpecialtySetting);
			
		}
	}
}