/**
* ===================================================================================================================================
*                                   IMMPaCT BI
* ===================================================================================================================================
*  Decription:      Report Concentration Curve
*  @authors:        Jefferson Escobar, Antonio Ferrero
*  @created:        19-Mar-2015
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          33.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         19-Mar-2015                 aferrero                    Construction of the class.
*       1.1         01-Jul-2015                 aferrero                    Added adoption table.
*/

public class IMP_BI_ConcentrationCurveReport {

    /* Variables to store information about the matrix*/
    private Id matrixId;

    public Matrix_BI__c matrix{get; set;}
    public Datacells dataCell; // Wrapper class

    /* Variables to summarize the total customers, potential and adoption */
    public Integer totalCustomers {get; private set;}
    public String totalCustomersString {get; set;}
    public Decimal totalDocsByPotential {get; private set;}
    public String totalDocsByPotentialString {get; private set;}
    public Decimal totalDocsByAdoption {get; private set;}
    public String totalDocsByAdoptionString {get; private set;}

    /* Map to store the information by rows */
    public map<Decimal, DataCells> mapCellsByRow {get; set;}
    /* Map to store the information by columns */
    public map<Decimal, DataCells> mapCellsByColumn {get; set;}

    /* List to retrieve the cells of the matrix*/
    public List<Matrix_Cell_BI__c> listCells;

    /* List to retrieve the data */
    public static List<DataCells> dataCellsRows;
    public static List<DataCells> dataCellsColumns;

    private map<String, String> map_urlParams;

    /* Constructor of the class*/
    public IMP_BI_ConcentrationCurveReport (ApexPages.standardController ctr){
        map_urlParams = ApexPages.currentPage().getParameters();
        listCells = new List<Matrix_Cell_BI__c>();
        dataCellsRows = new List<DataCells> ();
        dataCellsColumns = new List<DataCells> ();
        matrixId = (map_urlParams != null && map_urlParams.containsKey('mId')) ? map_urlParams.get('mId') : null;
        if(matrixId != null){
            matrix = [SELECT Id, Row_BI__c, Column_BI__c, Name_BI__c, Product_Catalog_BI__r.Name FROM Matrix_BI__c where Id =:matrixId];
            /* Matrix cell list with information aboout potential and adoption*/
            listCells = [SELECT Id, Name, Row_BI__c, Column_BI__c,Total_Customers_BI__c,Total_Intimacy_BI__c,Total_Potential_BI__c FROM Matrix_Cell_BI__c WHERE Matrix_BI__c =: MatrixId];
        }
    }

    /**
    * Loads the data of the cells by rows and calculates the data for the table
    *
    @authors  Jefferson Escobar, Antonio Ferrero
    @created 23-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return  mapCellsByRow map<Decimal, DataCells>
    *
    @changelog
    * 23-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * 23-March-2015 Antonio Ferrero Varas <aferrero@omegacrmconsulting.com>
    * - Created
    */
    public List<DataCells> getDataCellsRows(){
        dataCellsRows = new List<DataCells>();
        totalCustomers = 0;
        totalDocsByPotential = 0.0;

        if(listCells!=null && listCells.size()>0){

            /* Saving the data of the cells by rows in the object dataCells */
            Map<Decimal,DataCells> mapCellsByRow = new Map<Decimal,DataCells>();

            /* Data by rows for the potential*/
            for (Matrix_Cell_BI__c cell : listCells){

                /* Check if there are data about one row, if there are not enter a new row, if not, updates the existing*/
                if(!mapCellsByRow.containsKey(cell.Row_BI__c)){
                    dataCell = new DataCells();
                    dataCell.row = cell.Row_BI__c;
                    dataCell.totalCustomers = cell.Total_Customers_BI__c == null ? 0 : cell.Total_Customers_BI__c;
                    dataCell.totalCustomersString = (dataCell.totalCustomers.setScale(0, RoundingMode.HALF_UP)).format();
                    dataCell.Potential = cell.Total_Potential_BI__c == null ? 0 : cell.Total_Potential_BI__c;
                    dataCell.averagePotential = (dataCell.totalCustomers !=0) ? (dataCell.Potential / dataCell.totalCustomers) : 0;
                    datacell.averagePotentialString = (dataCell.averagePotential.setScale(0, RoundingMode.HALF_UP)).format();
                    dataCell.docsByPotential = dataCell.totalCustomers * dataCell.averagePotential;
                    datacell.docsByPotentialString = (dataCell.docsByPotential.setScale(0, RoundingMode.HALF_UP)).format();
                    mapCellsByRow.put(cell.Row_BI__c, dataCell);
                }
                else{
                    DataCells dataCell = mapCellsByRow.get(cell.Row_BI__c);
                    dataCell.totalCustomers += (cell.Total_Customers_BI__c == null ? 0 : cell.Total_Customers_BI__c );
                    dataCell.totalCustomersString = (dataCell.totalCustomers.setScale(0, RoundingMode.HALF_UP)).format();
                    dataCell.Potential += (cell.Total_Potential_BI__c == null ? 0 : cell.Total_Potential_BI__c);
                    dataCell.averagePotential = (dataCell.totalCustomers !=0) ? (dataCell.Potential / dataCell.totalCustomers) : 0;
                    datacell.averagePotentialString = (dataCell.averagePotential.setScale(0, RoundingMode.HALF_UP)).format();
                    dataCell.docsByPotential = dataCell.totalCustomers * dataCell.averagePotential;
                    datacell.docsByPotentialString = (dataCell.docsByPotential.setScale(0, RoundingMode.HALF_UP)).format();
                    mapCellsByRow.put(cell.Row_BI__c, dataCell);
                }
            }

            /* Ordering the data by row */
            Integer numRows = (Integer) matrix.Row_BI__c;
            for(Integer i =numRows ; i>0; i--){
                if(mapCellsByRow.get(i) != null) dataCellsRows.add(mapCellsByRow.get(i));
            }

            /* It adds the summatory of the potential and the number of doctors / accounts */
            Decimal acc=0;
            Decimal accPot=0;

            for(DataCells cells : dataCellsRows){

                acc+=cells.totalCustomers;
                accPot+=cells.docsByPotential;
                cells.accumDoctors +=acc;
                cells.accumPotential += accPot;
                cells.accumDoctorsString = (cells.accumDoctors.setScale(0, RoundingMode.HALF_UP)).format();
                cells.accumPotentialString = (cells.accumPotential.setScale(0, RoundingMode.HALF_UP)).format();
            }

            for(DataCells cells : dataCellsRows){
                totalCustomers += (Integer)cells.totalCustomers;
                totalCustomersString = totalCustomers.format();
                totalDocsByPotential += cells.docsByPotential;
                totalDocsByPotentialString = (totalDocsByPotential.setScale(0, RoundingMode.HALF_UP)).format();
            }

            /* Calculates the percentage of the number of accumulated doctors and potential with the totals */
            if(totalCustomers != 0 && totalDocsByPotential != 0 ){
                for(DataCells cells : dataCellsRows){
                    cells.docsPercentage = (Integer)((cells.accumDoctors / totalCustomers) * 100);
                    cells.potentialPercentage = (Integer)((cells.accumPotential / totalDocsByPotential) * 100);
                }
            }
        }

        return dataCellsRows;
    }

    /**
    * Loads the data of the cells by columns and calculates the data for the table
    *
    @authors  Jefferson Escobar, Antonio Ferrero
    @created 23-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return  mapCellsByColumn map<Decimal, DataCells>
    *
    @changelog
    * 23-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * 23-March-2015 Antonio Ferrero Varas <aferrero@omegacrmconsulting.com>
    * - Created
    */
    public List<DataCells> getDataCellsColumns(){
        dataCellsColumns = new List<DataCells>();
        totalCustomers = 0;
        totalDocsByAdoption = 0.0;

        if(listCells!=null && listCells.size()>0){

            /* Saving the data of the cells by columns in the object dataCells */
            Map<Decimal,DataCells> mapCellsByColumn = new Map<Decimal,DataCells>();

            /* Data by columns for the adoption */
            for (Matrix_Cell_BI__c cell : listCells){

                /* Check if there are data about one row, if there are not enter a new row, if not, updates the existing*/
                if(!mapCellsByColumn.containsKey(cell.Column_BI__c)){
                    dataCell = new DataCells();
                    dataCell.column = cell.Column_BI__c;
                    dataCell.totalCustomers = cell.Total_Customers_BI__c == null ? 0 : cell.Total_Customers_BI__c;
                    dataCell.totalCustomersString = (dataCell.totalCustomers.setScale(0, RoundingMode.HALF_UP)).format();
                    dataCell.Adoption = (cell.Total_Intimacy_BI__c == null ? 0 : cell.Total_Intimacy_BI__c);
                    dataCell.averageAdoption = (dataCell.totalCustomers !=0) ? (dataCell.Adoption / dataCell.totalCustomers) : 0;
                    datacell.averageAdoptionString = (dataCell.averageAdoption.setScale(0, RoundingMode.HALF_UP)).format();
                    dataCell.docsByAdoption = dataCell.totalCustomers * dataCell.averageAdoption;
                    datacell.docsByAdoptionString = (dataCell.docsByAdoption.setScale(0, RoundingMode.HALF_UP)).format();
                    mapCellsByColumn.put(cell.Column_BI__c, dataCell);
                }
                else{
                    DataCells dataCell = mapCellsByColumn.get(cell.Column_BI__c);
                    dataCell.totalCustomers += cell.Total_Customers_BI__c == null ? 0 : cell.Total_Customers_BI__c;
                    dataCell.totalCustomersString = (dataCell.totalCustomers.setScale(0, RoundingMode.HALF_UP)).format();
                    dataCell.Adoption += (cell.Total_Intimacy_BI__c == null ? 0 : cell.Total_Intimacy_BI__c);
                    dataCell.averageAdoption = (dataCell.totalCustomers !=0) ? (dataCell.Adoption / dataCell.totalCustomers) : 0;
                    datacell.averageAdoptionString = (dataCell.averageAdoption.setScale(0, RoundingMode.HALF_UP)).format();
                    dataCell.docsByAdoption = dataCell.totalCustomers * dataCell.averageAdoption;
                    datacell.docsByAdoptionString = (dataCell.docsByAdoption.setScale(0, RoundingMode.HALF_UP)).format();
                    mapCellsByColumn.put(cell.Column_BI__c, dataCell);
                }
            }

            /* Ordering the data by column */
            //Set<Decimal> cellsSortColumn =   mapCellsByColumn.keySet();
            Integer numColumns = (Integer) matrix.Column_BI__c;
            for(Integer i = numColumns - 1; i>=0; i--){
                if(mapCellsByColumn.get(i) != null) dataCellsColumns.add(mapCellsByColumn.get(i));
            }

            /* It adds the summatory of the potential and the number of doctors / accounts */
            Decimal acc=0;
            Decimal accAdo=0;
            for(DataCells cells : dataCellsColumns){
                acc+=cells.totalCustomers;
                accAdo+=cells.docsByAdoption;
                cells.accumDoctors +=acc;
                cells.accumAdoption += accAdo;
                cells.accumDoctorsString = (cells.accumDoctors.setScale(0, RoundingMode.HALF_UP)).format();
                cells.accumAdoptionString = (cells.accumAdoption.setScale(0, RoundingMode.HALF_UP)).format();
            }
            for(DataCells cells : dataCellsColumns){
                totalCustomers += (Integer)cells.totalCustomers;
                totalCustomersString = totalCustomers.format();
                totalDocsByAdoption += cells.docsByAdoption;
                totalDocsByAdoptionString = (totalDocsByAdoption.setScale(0, RoundingMode.HALF_UP)).format();

            }

            /* Calculates the percentage of the number of accumulated doctors and potential with the totals */
            if(totalCustomers != 0 && totalDocsByAdoption !=0){
                for(DataCells cells : dataCellsColumns){
                    cells.docsPercentage = (Integer)((cells.accumDoctors / totalCustomers) * 100);
                    cells.adoptionPercentage = (Integer)((cells.accumAdoption / totalDocsByAdoption) * 100);
                }
            }
        }

        return dataCellsColumns;
    }

    /**
    * Creates the data chart for the Potential with the information by rows
    * @return
    */
    public String getDataRows() {
        return IMP_BI_ConcentrationCurveReport.getChartDataRows();
    }

    // Make the chart data available via JavaScript remoting
    @RemoteAction
    public static String getRemoteData() {
        return IMP_BI_ConcentrationCurveReport.getChartDataRows();
    }

    // The actual chart data; needs to be static to be
    // called by a @RemoteAction method
    /**
    * Loads in the data List the data from the arrays that contain the information about potential and
    * adoption in order to create the chart
    *
    @authors  Jefferson Escobar, Antonio Ferrero
    @created 23-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)

    @return  data List<Data>
    */

    public static String getChartDataRows() {
        List<Data> data = new List<Data>();

        for(DataCells cells : dataCellsRows){
            data.add(new Data(cells.docsPercentage, cells.potentialPercentage));
        }
        return JSON.serialize(data);
    }

    /**
    * Creates the data chart for the Adoption with the information by columns
    * @return
    */
    public String getDataColumns() {
        return IMP_BI_ConcentrationCurveReport.getChartDataColumns();
    }

    // Make the chart data available via JavaScript remoting
    @RemoteAction
    public static String getRemoteDataColumns() {
        return IMP_BI_ConcentrationCurveReport.getChartDataColumns();
    }

    // The actual chart data; needs to be static to be
    // called by a @RemoteAction method
    /**
    * Loads in the data List the data from the arrays that contain the information about potential and
    * adoption in order to create the chart
    *
    @authors  Jefferson Escobar, Antonio Ferrero
    @created 23-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)

    @return  data List<Data>
    */

    public static String getChartDataColumns() {
        List<Data> data = new List<Data>();
       //List<Data> dataAdoption = new List<Data>();

        for(DataCells cells : dataCellsColumns){
            data.add(new Data(cells.docsPercentage, cells.adoptionPercentage));
        }
        return JSON.serialize(data);
    }

    // Wrapper class
    public class Data {
        public Integer data1 { get; set; }
        public Integer data2 { get; set; }
        public Data(Integer data1, Integer data2){//, Integer data3) {
            this.data1 = data1;
            this.data2 = data2;
        }
    }

    // Wrapper class for the data of the cells in the matrix
    public class DataCells{
        public Decimal row {get; set;}
        public Decimal column {get; set;}
        public Decimal totalCustomers {get; set;}
        public String totalCustomersString {get; set;}
        public Decimal Potential {get; set;}
        public Decimal Adoption {get; set;}
        public Decimal averagePotential {get; set;}
        public String averagePotentialString {get; set;}
        public Decimal averageAdoption {get; set;}
        public String averageAdoptionString {get; set;}
        public Decimal docsByPotential{get;set;} // customers * potential
        public String docsByPotentialString {get; set;}
        public Decimal docsByAdoption{get;set;} // customers * adoption
        public String docsByAdoptionString {get; set;}
        public Decimal accumDoctors{get;set;}
        public String accumDoctorsString {get; set;}
        public Decimal accumPotential{get;set;}
        public String accumPotentialString {get; set;}
        public Decimal accumAdoption{get;set;}
        public String accumAdoptionString {get; set;}
        public Integer docsPercentage{get;set;}
        public Integer potentialPercentage{get;set;}
        public Integer adoptionPercentage{get;set;}
        public DataCells(){
            this.totalCustomers = 0;
            this.accumDoctors = 0;
            this.accumPotential = 0;
            this.accumAdoption = 0;
        }
    }
}