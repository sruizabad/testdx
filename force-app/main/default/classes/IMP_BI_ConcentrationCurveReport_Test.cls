/**
*   Test class for class IMP_BI_ConcentrationCurveReport.
*
@author Antonio Ferrero
@created 2015-08-17
@version 1.0
@since 20.0
*
@changelog
* 2015-08-17 Antonio Ferrero <antonio.ferrero_varas.ext@boehringer-ingelheim.com>
* - Created
* - Test coverage  
*/
@isTest
private class IMP_BI_ConcentrationCurveReport_Test {
	 static testMethod void testGetData() {
         Matrix_BI__c ma = IMP_BI_ClsTestHelp.createTestMatrix();
         ma.Intimacy_Levels_BI__c = 11;
         ma.Potential_Levels_BI__c = null;
         ma.Size_BI__c = '10x11';
         ma.Column_BI__c = 11;
         ma.Row_BI__c = 10;
         ma.Status_BI__c = 'Draft';
         insert ma;
         
         Matrix_Cell_BI__c mc = IMP_BI_ClsTestHelp.createTestMatrixCell();
         mc.Matrix_BI__c = ma.Id;
         mc.Row_BI__c = 2;
         mc.Column_BI__c = 3;
         mc.Total_Customers_BI__c = 25;
         mc.Total_Intimacy_BI__c = 250;
         mc.Total_Potential_BI__c = 2500;
         insert mc;
         
         Matrix_Cell_BI__c mc1 = IMP_BI_ClsTestHelp.createTestMatrixCell();
         mc1.Matrix_BI__c = ma.Id;
         mc1.Row_BI__c = 2;
         mc1.Column_BI__c = 3;
         mc1.Total_Customers_BI__c = 25;
         mc1.Total_Intimacy_BI__c = 250;
         mc1.Total_Potential_BI__c = 2500;
         insert mc1;
         
         Test.startTest();
         ApexPages.StandardController ctrl = new ApexPages.StandardController(ma);
         ApexPages.currentPage().getParameters().put('mId', ma.Id);
         IMP_BI_ConcentrationCurveReport con = new IMP_BI_ConcentrationCurveReport(ctrl);
        
         con.getDataCellsRows();
         con.getDataCellsColumns();
         con.getDataRows();
         con.getDataColumns();
         
         Test.stopTest();
     }
}