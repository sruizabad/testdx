/**
 *	Controller class for VF page IMP_BI_CtrlApplyFilter.page
 *
 @author  Peng Zhu
 @created 2015-01-09
 @version 1.0
 @since   30.0 (Force.com ApiVersion)
 *
 @changelog
 * 2015-01-09 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 */
public with sharing class IMP_BI_CtrlApplyFilter {
   
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=BEGIN public members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	public string message {get; set;}
	public string msgType {get; set;}
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=END public members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=BEGIN private members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	private static set<Id> set_globalBAId;	
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=END private members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	/////////////////////////////////// -=BEGIN CONSTRUCTOR=- /////////////////////////////////////
   /**
	* The contsructor
	*
	@author  Peng Zhu
	@created 2015-01-09
	@version 1.0
	@since   30.0 (Force.com ApiVersion)
	*
	@changelog
	* 2015-01-09 Peng Zhu <peng.zhu@itbconsult.com>
	* - Created
	*/
    public IMP_BI_CtrlApplyFilter() {}
	/////////////////////////////////// -=END CONSTRUCTOR=- ///////////////////////////////////////
    
    
    //********************************* -=BEGIN public methods=- **********************************
    @RemoteAction
    public static String fetchMapOfProductInfo(String input) {
    	ClsResponse res = new ClsResponse();
    	
    	try {
	    	Id sourceId = getFullId(input);
	    	
	    	map<Id, ClsProductInfo> map_id_cdo = fetchMapOfProductInfoByCycleIdOrMatrixId(sourceId);

	    	res.result = JSON.serialize(map_id_cdo);
    	}
    	catch(Exception e) {
    		res.status = 'ERROR';
    		res.result = e.getMessage();
    	}
    	
    	return JSON.serialize(res);    
    }

    //@ReadOnly
    @RemoteAction    
    public static String calculateForCycleDataByProductInfo(String inProdInfo, String inType) {
		ClsResponse res = new ClsResponse();
    	
    	try {
    		ClsProductInfo cpi = (ClsProductInfo) JSON.deserialize(inProdInfo, ClsProductInfo.class);
    		
    		Id prodId = cpi.prodId;
    		Id cycleId = cpi.cycleId;
    		Id lastCycleDataId = cpi.lastCycleDataId;
    		String contryCode = cpi.countryCode;
    		
	    	String calcType = ('update'.equalsIgnoreCase(inType) ? 'update' : 'counter');
	    	
	    	map<String, String> map_type_amount = new map<String, String>();
	    	map_type_amount.put('Selected', '0');
	    	map_type_amount.put('NotSelected', '0');	
	    	map_type_amount.put('Counter', '0');
	    	map_type_amount.put('UpdateCounter', '0');
	    	map_type_amount.put('Finished', 'yes');
	    	 
	    	if(lastCycleDataId != NULL) map_type_amount.put('LastCycleDataId', String.valueOf(lastCycleDataId));   
	    	
			if(prodId != NULL && 'Product_vod__c'.equalsIgnoreCase(prodId.getSObjectType().getDescribe().getName())) {
				//TODO : fetch PA from Cycle Data with limit 2000
				list<Cycle_Data_BI__c> list_paCycleData = fetchListOfPersonAccountByProductInfo(cpi);
				
				//TODO : fetch PA BA mapping for Affliation or...
				set<Id> set_paId = new set<Id>();
				
				for(Cycle_Data_BI__c cd : list_paCycleData) {
					set_paId.add(cd.Account_BI__c);
					
					lastCycleDataId = cd.Id;
				}
								
				//TODO : for each PA, check is selected
				set<String> set_matchKey = fetchMatchKeyByAffliation(contryCode, set_paId);
				
				set<Id> set_baId = fetchBusinessAccountIdByProductInfoAndAccountId(cpi, set_globalBAId);
				
				Integer paCounter = 0;
				
				list<Cycle_Data_BI__c> list_cdUpdate = new list<Cycle_Data_BI__c>();
				//TODO : update unselected PA to false 
				
				Boolean isSelected = false;
				
				for(Cycle_Data_BI__c cd : list_paCycleData) {
					isSelected = false;
					
					for(Id baId : set_baId) {
						String matchKey = cd.Account_BI__c + ':' + baId;
						
						if(set_matchKey.contains(matchKey)) {
							//paCounter ++;
							isSelected = true;
							break;
						}
//						else if(cd.Selected_BI__c == TRUE) {
//							cd.Selected_BI__c = false;
//							list_cdUpdate.add(cd);
//						}
					}
					
					if(isSelected == true) {
						paCounter ++;
					}
					else if(cd.Selected_BI__c == TRUE) {
						cd.Selected_BI__c = false;
						list_cdUpdate.add(cd);						
					} 
				}
				
				
		    	map_type_amount.put('Selected', String.valueOf(paCounter));
		    	map_type_amount.put('Counter', String.valueOf(list_paCycleData.size()));
		    	map_type_amount.put('NotSelected', String.valueOf((list_paCycleData.size() - paCounter)));
		    	map_type_amount.put('UpdateCounter', String.valueOf(list_cdUpdate.size()));
		    	
		    	//if(list_paCycleData.size() == 2000) map_type_amount.put('Finished', 'no');
		    	if(list_paCycleData.size() == 200) map_type_amount.put('Finished', 'no');
		    	
		    	if(lastCycleDataId != NULL) map_type_amount.put('LastCycleDataId', String.valueOf(lastCycleDataId));   				
				
	 			if(calcType == 'update' && !list_cdUpdate.isEmpty()) update list_cdUpdate; 
			}
			res.result = JSON.serialize(map_type_amount);
    	}
    	catch(Exception e) {
    		res.status = 'ERROR';
    		res.result = e.getMessage();    		
    	}    
    	
    	return JSON.serialize(res);   	    	    	
	}

    /**
	 * Method to show the error message on the page
	 *
	 @author  Peng Zhu
	 @created 2015-01-09
	 @version 1.0
	 @since   30.0 (Force.com ApiVersion)
	 *
	 @param   void
	 *
	 @return  void
	 *
	 @changelog
	 * 2015-01-09 Peng Zhu <peng.zhu@itbconsult.com>
	 * - Created
	 */   
    public void showMessage(){
    	if(message != NULL && message.trim() != '') {
	    	if('CONFIRM'.equalsIgnoreCase(msgType)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, message));
	    	}
	    	else if('FATAL'.equalsIgnoreCase(msgType)) {
	    	
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, message));
	    	}
	    	else if('INFO'.equalsIgnoreCase(msgType)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, message));
	    	}
	    	else if('WARNING'.equalsIgnoreCase(msgType)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, message));
	    	}
	    	else {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
	    	}
    	}
    }
    //********************************* -=END public methods=- ************************************
    
    
    //********************************* -=BEGIN private methods=- *********************************
    @TestVisible
    private static set<Id> fetchBusinessAccountIdByProductInfoAndAccountId(ClsProductInfo cpi, set<Id> set_inBaId) {
    	set<Id> set_baId = new set<Id>();
    	
    	if(cpi != NULL && cpi.prodId != NULL && cpi.cycleId != NULL && set_inBaId != NULL && !set_inBaId.isEmpty()) {
    		for(Cycle_Data_BI__c cdb : [SELECT Account_BI__c FROM Cycle_Data_BI__c 
    		                                                WHERE Cycle_BI__c = :cpi.cycleId 
    		                                                  AND Product_Catalog_BI__c = :cpi.prodId 
    		                                                  AND Selected_BI__c = TRUE 
    		                                                  AND Account_BI__c IN :set_inBaId
    		                                                  AND Account_BI__r.isPersonAccount = FALSE 
    		                                                  AND Matrix_Cell_1_BI__r.Matrix_BI__r.Account_Matrix_Type_BI__c = 'Filter on HCP'
    		                                                  AND Matrix_Cell_1_BI__r.Matrix_BI__r.Account_Matrix_BI__c = TRUE]) {
    		    // Begin : for loop
 				set_baId.add(cdb.Account_BI__c);
 			}	
    	}
    	
    	return set_baId;
    }
    
    @TestVisible 
    private static list<Cycle_Data_BI__c> fetchListOfPersonAccountByProductInfo(ClsProductInfo cpi) {
    	list<Cycle_Data_BI__c> list_cd;
    	
		if(cpi != NULL && cpi.prodId != NULL && cpi.cycleId != NULL) {
    		
    		String query = ' SELECT Id, Selected_BI__c, Account_BI__c FROM Cycle_Data_BI__c ' 
    					 + ' WHERE Cycle_BI__c = \'' + cpi.cycleId + '\' AND Product_Catalog_BI__c = \'' + cpi.prodId + '\' AND Account_BI__r.isPersonAccount = TRUE AND HCO_BI__C = NULL ';
    					 
    		if(cpi.lastCycleDataId != NULL)  query += (' AND Id > \'' + cpi.lastCycleDataId + '\'');
    		
    		//query += ' ORDER BY Id LIMIT 2000';
    		query += ' ORDER BY Id LIMIT 200';
 		
 			list_cd = Database.query(query);
    	}
    	else {
    		list_cd = new list<Cycle_Data_BI__c>();
    	}
    	
    	return list_cd;  	
    }

    
    @TestVisible
    private static map<Id, ClsProductInfo> fetchMapOfProductInfoByCycleIdOrMatrixId(Id sourceId) {
    	map<Id, ClsProductInfo> map_id_cpi = new map<Id, ClsProductInfo>();
    	
    	if(isValidSourceId(sourceId)) {
    		
    		for(Matrix_BI__c matrix : [SELECT Id
    										, Name
    										, Cycle_BI__c
    										, Cycle_BI__r.Country_Code_BI__c
    										, Product_Catalog_BI__c
    										, Product_Catalog_BI__r.Name 
    								     FROM Matrix_BI__c 
    								    WHERE Account_Matrix_BI__c = TRUE 
    								      AND Account_Matrix_Type_BI__c = 'Filter on HCP'
    								      AND IsDeleted = FALSE 
    								      AND Status_BI__c != 'Draft'
    								      AND (Id = :sourceId OR Cycle_BI__c = :sourceId)]) {
    			// Begin : for loop
    			if(map_id_cpi.get(matrix.Product_Catalog_BI__c) == NULL) {
    				ClsProductInfo cpi = new ClsProductInfo();
    				cpi.prodId = matrix.Product_Catalog_BI__c;
    				cpi.prodName = matrix.Product_Catalog_BI__r.Name;
    				cpi.cycleId = matrix.Cycle_BI__c;
    				cpi.countryCode = matrix.Cycle_BI__r.Country_Code_BI__c;
    				
    				map_id_cpi.put(matrix.Product_Catalog_BI__c, cpi);
    			} 
    		}
    	}
    	
    	return map_id_cpi;
    }
    
	@TestVisible
	private static set<String> fetchMatchKeyByAffliation(String countryCode, set<Id> set_paId) {
		if(set_globalBAId == NULL) set_globalBAId = new set<Id>();
		else set_globalBAId.clear();
		
		set<String> set_matchKey = new set<String>();
		
		Account_Relation_Setting_BI__c inst = getAccountRelationSettingByCountry(countryCode);
	
		try {
			if(inst != NULL && String.isNotBlank(inst.Object_BI__c) && String.isNotBlank(inst.Person_Account_Field_BI__c) && String.isNotBlank(inst.Business_Account_Field_BI__c) && set_paId != NULL && !set_paId.isEmpty()) {
				String query = 'SELECT ' + inst.Person_Account_Field_BI__c + ', ' + inst.Business_Account_Field_BI__c + ' FROM ' + inst.Object_BI__c + ' WHERE ' + inst.Person_Account_Field_BI__c + ' IN :set_paId AND ' + inst.Person_Account_Field_BI__c + ' != NULL AND ' + inst.Business_Account_Field_BI__c + ' != NULL' ;
				
	 			for(sObject sobj : Database.query(query)) {
	 				Id paId = getFullId(String.valueOf(sobj.get(inst.Person_Account_Field_BI__c)));
	 				Id baId = getFullId(String.valueOf(sobj.get(inst.Business_Account_Field_BI__c)));
	 				
	 				if(paId != NULL && baId != NULL) {
	 					set_matchKey.add((paId+ ':' + baId));
	 					
	 					set_globalBAId.add(baId);
	 				} 
	 			}				
			}
		}
		catch(Exception e) {}
		
		return set_matchKey;
	}


    /**
	 * Method to check whether the given id is a valid id for Cycle or Matrix
	 *
	 @author  Peng Zhu
	 @created 2015-01-09
	 @version 1.0
	 @since   30.0 (Force.com ApiVersion)
	 *
	 @param   sourceId   given id
	 *
	 @return  true/false
	 *
	 @changelog
	 * 2015-01-09 Peng Zhu <peng.zhu@itbconsult.com>
	 * - Created
	 */     
    @TestVisible    
    private static boolean isValidSourceId(Id sourceId) {
    	boolean rtValue = false;
    	
    	set<String> set_objApiName = new set<String>{'Cycle_BI__c', 'Matrix_BI__c'};
    	
    	if(sourceId != NULL && set_objApiName.contains(sourceId.getSObjectType().getDescribe().getName())) {
    		rtValue = true;
    	}
    	
    	return rtValue;
    }

    /**
	 * Method to fetch the account relation setting from custom setting according the given country iso code
	 *
	 @author  Peng Zhu
	 @created 2015-01-09
	 @version 1.0
	 @since   30.0 (Force.com ApiVersion)
	 *
	 @param   input  the given country iso code
	 *
	 @return  an instance of the related custom setting record
	 *
	 @changelog
	 * 2015-01-09 Peng Zhu <peng.zhu@itbconsult.com>
	 * - Created
	 */ 
    @TestVisible    
    private static Account_Relation_Setting_BI__c getAccountRelationSettingByCountry(String input) {
    	Account_Relation_Setting_BI__c inst;
    	
    	if(input != NULL && String.isNotBlank(input)) {
    		inst = Account_Relation_Setting_BI__c.getInstance(input);
    		
    		if(inst == NULL) inst = Account_Relation_Setting_BI__c.getInstance('Default');
    	}
    	 
    	return inst;
    }
    //********************************* -=END private methods=- ***********************************
    
    
    //********************************* -=BEGIN help functions=- **********************************
    /**
	 * Method to convert the string to salesforce id
	 *
	 @author  Peng Zhu
	 @created 2015-01-09
	 @version 1.0
	 @since   30.0 (Force.com ApiVersion)
	 *
	 @param   inputId   the string type of an Id
	 *
	 @return  the Id type of the given string
	 *
	 @changelog
	 * 2015-01-09 Peng Zhu <peng.zhu@itbconsult.com>
	 * - Created
	 */    
    @TestVisible
    private static Id getFullId(String inputId) {
    	Id rtId;
    	
    	try {
    		rtId = Id.valueOf(inputId);
    	}
    	catch(Exception e) {}
    	
    	return rtId;
    }
    
    //********************************* -=END help functions=- ************************************
    
    //********************************* -=BEGIN inner classes=- ***********************************
    ///*>>>WrapperClass*/
    /** Wrapper Class for remote action call response
     *
     @changelog
     * 2015-01-14 Peng Zhu <peng.zhu@itbconsult.com>
     * - Created
     */
    public class ClsResponse {
    	public String status;
    	public String result;
    	
    	public ClsResponse() {
    		status = 'SUCCESS';
    		result = '';
    	}
    }
    
    public class ClsProductInfo {
    	public Id prodId;
    	public Id cycleId;
    	public Id lastCycleDataId;
    	public String prodName;
    	public String countryCode;
    }
	///*<<<WrapperClass*/
    //********************************* -=END inner classes=- *************************************
}