/**
 *
 */
@isTest
private class IMP_BI_CtrlApplyFilter_Test {

	static testMethod void test_fetchMapOfProductInfo() {
		
		Id fakeCycleId = generateFakeId(Cycle_BI__c.sObjectType);
		
		IMP_BI_CtrlApplyFilter.fetchMapOfProductInfo('xxxxx');
		
		IMP_BI_CtrlApplyFilter.fetchMapOfProductInfo(String.valueOf(fakeCycleId));
		
		system.assert(true);
	}
	
	static testMethod void test_calculateForCycleDataByProductInfo() {
		Id fakeCycleId = generateFakeId(Cycle_BI__c.sObjectType);
		Id fakeProductId = generateFakeId(Product_vod__c.sObjectType);
		
		String prodInfo = '{"prodId":"' + String.valueOf(fakeProductId) + '","prodName":"xxx","cycleId":"' + String.valueOf(fakeCycleId) + '","countryCode":"US"}';
		
		IMP_BI_CtrlApplyFilter.calculateForCycleDataByProductInfo('xxxxx', 'counter');
		IMP_BI_CtrlApplyFilter.calculateForCycleDataByProductInfo(prodInfo, 'counter');
		IMP_BI_CtrlApplyFilter.calculateForCycleDataByProductInfo(prodInfo, 'update');
		
		system.assert(true);
	}
	
    static testMethod void test_showMessage() {
        IMP_BI_CtrlApplyFilter inst = new IMP_BI_CtrlApplyFilter();
        
        inst.message = 'Msg';
        inst.msgType = 'INFO';
        
        inst.showMessage();
        
        inst.msgType = 'CONFIRM';
        inst.showMessage();
        
        inst.msgType = 'FATAL';
        inst.showMessage();
        
        inst.msgType = 'WARNING';
        inst.showMessage();
        
        inst.msgType = 'ERROR';
        inst.showMessage();
        
        system.assert(true);   
    }
   
    static testMethod void test_isValidSourceId() {
    	Boolean isValid = IMP_BI_CtrlApplyFilter.isValidSourceId(generateFakeId(Matrix_BI__c.sObjectType));

    	system.assert(true);    	
    }
    
    static testMethod void test_getAccountRelationSettingByCountry() {
    	Account_Relation_Setting_BI__c inst = IMP_BI_CtrlApplyFilter.getAccountRelationSettingByCountry('DE');
    	
    	system.assert(true);
    }
    
    static testMethod void test_getFullId() {
    	Id fakeAccountId = generateFakeId(Account.sObjectType);
    	
    	system.assertEquals(null, IMP_BI_CtrlApplyFilter.getFullId(null));
    	system.assertEquals(null, IMP_BI_CtrlApplyFilter.getFullId(''));
    	system.assertEquals(null, IMP_BI_CtrlApplyFilter.getFullId('InvalidId'));
    	system.assertEquals(fakeAccountId, IMP_BI_CtrlApplyFilter.getFullId(String.valueOf(fakeAccountId)));
    }
    
    static testMethod void test_fe() {
        Account acc = IMP_BI_ClsTestHelp.createTestAccount();
        acc.Name = '123e';    
        insert acc; 
        
        Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
        insert c;
                
        Product_vod__c p2 = IMP_BI_ClsTestHelp.createTestProduct();
        p2.Country_BI__c = c.Id;
        insert p2;
        
        Cycle_BI__c cycle2 = new Cycle_BI__c();
        cycle2.Country_BI__c = 'USA';
        cycle2.Start_Date_BI__c = date.today() - 10;
        cycle2.End_Date_BI__c = date.today() + 10;
        cycle2.Country_Lkp_BI__c = c.Id;
        cycle2.IsCurrent_BI__c = false;
        insert cycle2;
                
        Lifecycle_Template_BI__c mt = new Lifecycle_Template_BI__c();
        mt.Name = 'mt';
        mt.Country_BI__c = c.Id;
        mt.Active_BI__c = true;
        mt.isLaunch_Phase_BI__c = true;
        mt.Adoption_Status_01_BI__c = '1';
        mt.Adoption_Status_02_BI__c = '2';
        mt.Adoption_Status_03_BI__c = '3';
        mt.Adoption_Status_04_BI__c = '4';
        mt.Adoption_Status_05_BI__c = '5';
        insert mt;
                        
        Matrix_BI__c ma = IMP_BI_ClsTestHelp.createTestMatrix();
        ma.Cycle_BI__c = cycle2.Id;
        ma.Intimacy_Levels_BI__c = 11;
        ma.Potential_Levels_BI__c = null;
        ma.Size_BI__c = '10x11';
        ma.Lifecycle_Template_BI__c = mt.Id;
        ma.Specialization_BI__c = 'Cardiologist;GP';//Peng Zhu 2013-10-14
        ma.Column_BI__c = 11;
        ma.Product_Catalog_BI__c = p2.Id;
        ma.Filter_Field_1_BI__c = 'Rep Access';
        ma.Filter_Field_2_BI__c = 'Rep Access';
        ma.Filter_Field_3_BI__c = 'Rep Access';
        ma.Account_Matrix_BI__c = true;
        ma.Account_Matrix_Type_BI__c = 'Filter on HCP';
        ma.Status_BI__c = 'Calculated';
        insert ma;
        
        IMP_BI_CtrlApplyFilter.fetchMapOfProductInfoByCycleIdOrMatrixId(ma.Id);
    }

    //********************************* -=BEGIN test help functions=- **********************************	    
	private static Integer fakeIdCount = 0;
	private static final String ID_PATTERN = '000000000000';
	
	/**
	 * Generate a fake Salesforce Id for the given sObjectType
	 */
	private static Id generateFakeId(Schema.sObjectType sObjectType) {
		String keyPrefix = sObjectType.getDescribe().getKeyPrefix();
		
		fakeIdCount++;

		String fakeIdPrefix = ID_PATTERN.substring(0, 12 - fakeIdCount.format().length());

		return Id.valueOf(keyPrefix + fakeIdPrefix + fakeIdCount);
	} 
    //********************************* -=END test help functions=- ************************************	   
}