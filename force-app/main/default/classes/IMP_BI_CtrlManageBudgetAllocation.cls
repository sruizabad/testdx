/**
* ===================================================================================================================================
*                                   IMMPaCT BI
* ===================================================================================================================================
*  Decription:      New/Edit Budget Allocation Page
*  @author:         Hely
*  @created:        2015-03-06
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          29.0 (Force.com ApiVersion)
*
*/

public class IMP_BI_CtrlManageBudgetAllocation {
    public Id prodId {get; set;}

    //Page attributes
    public ClsCycleProductBudget clsCycleProductBudget {get; set;}

    //Errors attributes
    public boolean hasError {get; private set;}
    private String errMsg = '';

    //Page details
    public static final String LABEL_BUDGET_ALLOCATION = Schema.SObjectType.Budget_Allocation_BI__c.getLabel();
    public static final String KEYPREFIX_CYCLE = Schema.SObjectType.Cycle_BI__c.getKeyPreFix();
    public String pageTitle {get; private set;}
    public String sectionHeaderTitle {get; private set;}
    public String sectionHeaderSubTitle {get; private set;}
    private Map<String, String> map_urlParams;

    //Class attributes
    public String productId {get; set;}
    public String cycleId {get; set;}
    public String total {get; set;}

    //Components Form
    public List<Selectoption> productsByCycle {get;private set;}
    private static List<ClsBudgetAllocation> list_clsBudgetAllocations {get; set;}
    //private Id countryId ;

	//private
    private Set<String> typeSetting = IMP_BI_Product_Catalog_Setting__c.getAll().keySet();


    public IMP_BI_CtrlManageBudgetAllocation(ApexPages.standardController ctr){

        hasError = false;
        clsCycleProductBudget = new ClsCycleProductBudget();
        //system.debug('---------------------------------------------Edit'+ctr.getId());
        if(ctr.getId()!=null){//Edit  Budget Allocation
        	//system.debug('---------------------------------------------Edit');
            List<Cycle_Product_Budget_BI__c> list_cyclePB = [select Id, Name, Cycle_BI__c, Product_Catalog_BI__c, Overall_Budget_BI__c from Cycle_Product_Budget_BI__c where Id = :ctr.getId()];
			Budget_Allocation_BI__c budgetAllocation = new Budget_Allocation_BI__c();
        	if(list_cyclePB == null || list_cyclePB.size() == 0){
        		budgetAllocation = [Select Id,Name,Cycle_BI__c,Product_Catalog_BI__c,Channel_BI__c From Budget_Allocation_BI__c where Id = :ctr.getId()];
        		list_cyclePB = [select Id
        							, Name
									, Cycle_BI__c
									, Product_Catalog_BI__c
									, Overall_Budget_BI__c
									from Cycle_Product_Budget_BI__c
									where Product_Catalog_BI__c = :budgetAllocation.Product_Catalog_BI__c
									and Cycle_BI__c = :budgetAllocation.Cycle_BI__c order by Id];
        	}
			cycleId = list_cyclePB[0].Cycle_BI__c;
        	clsCycleProductBudget.id = list_cyclePB[0].Id;
        	clsCycleProductBudget.cycle = list_cyclePB[0].Cycle_BI__c;
        	clsCycleProductBudget.product = list_cyclePB[0].Product_Catalog_BI__c;
        	if(Userinfo.getLocale().containsIgnoreCase('de')){
        		String budget = (list_cyclePB[0].Overall_Budget_BI__c+'').replace('.',',');
        		clsCycleProductBudget.budget = (budget == '') ? '0,00' : budget;
            }else{
            	clsCycleProductBudget.budget = (list_cyclePB[0].Overall_Budget_BI__c != null) ? list_cyclePB[0].Overall_Budget_BI__c+'' : '0';
            }
            map<Id, Budget_Allocation_BI__c> map_id_budgetAllocation = new map<Id, Budget_Allocation_BI__c>();
            for(Budget_Allocation_BI__c ba : [select Id
                                                    ,Name
                                                    ,Cycle_BI__c
                                                    ,Unit_BI__c
                                                    ,Total_Cost_BI__c
                                                    ,Product_Catalog_BI__c
                                                    ,Channel_BI__c
                                                    ,Unit_Costs_BI__c
                                                    From Budget_Allocation_BI__c
                                                    where Cycle_BI__c = :clsCycleProductBudget.cycle
                                                    and Product_Catalog_BI__c = :clsCycleProductBudget.product
                                                    and Unit_Costs_BI__c != null ]){
                map_id_budgetAllocation.put(ba.Channel_BI__c, ba);
            }

			List<ClsBudgetAllocation> list_budgetAllocation = new List<ClsBudgetAllocation>();
            //system.debug('++++++++++++++++++'+getChannelInfo()+''+cycleId);
            for(ClsBudgetAllocation cba : getChannelInfo()){
                if(map_id_budgetAllocation.get(cba.cId)!=null){
                	Budget_Allocation_BI__c ba = map_id_budgetAllocation.get(cba.cId);
                    cba.baId = ba.Id;
		            if(Userinfo.getLocale().containsIgnoreCase('de')){
		            	//cba.unitCost = (ba.Unit_Costs_BI__c != null) ? (ba.Unit_Costs_BI__c+'').replace('.',',') : '0';
		            	//cba.totalCost = (ba.Total_Cost_BI__c != null) ? (ba.Total_Cost_BI__c+'').replace('.',',') : '0';
		            	if(ba.Unit_Costs_BI__c != null){
		            		String uc = (ba.Unit_Costs_BI__c+'').replace('.',',');
		            		cba.unitCost = (uc.trim() == '') ? '0,00' : uc;
		            	}else{
		            		cba.unitCost = '0,00';
		            	}

		            	if(ba.Total_Cost_BI__c != null){
		            		String tc = (ba.Total_Cost_BI__c+'').replace('.',',');
		            		cba.totalCost = (tc.trim() == '') ? '0,00' : tc;
		            	}else{
		            		cba.totalCost = '0,00';
		            	}
		            }else{
		            	cba.unitCost = (ba.Unit_Costs_BI__c != null && ba.Unit_Costs_BI__c+'' != '') ? ba.Unit_Costs_BI__c+'' : '0';
		            	cba.totalCost = (ba.Total_Cost_BI__c != null && ba.Total_Cost_BI__c+'' != '') ? ba.Total_Cost_BI__c+'' : '0';
		            }
	            	cba.unit = (ba.Unit_BI__c != null) ? Integer.valueOf(ba.Unit_BI__c) : 0;
                }else{
                	if(Userinfo.getLocale().containsIgnoreCase('de')){
                		cba.unitCost = '0,00';
                		cba.totalCost = '0,00';
                	}else{
                		cba.unitCost = '0.00';
                		cba.totalCost = '0.00';
                	}
                	cba.unit = 0;
                }
                list_budgetAllocation.add(cba);
            }
            clsCycleProductBudget.list_cBudgetAllocation = list_budgetAllocation;
            //initialize the page title
            sectionHeaderSubTitle = list_cyclePB[0].Name;
            sectionHeaderTitle = LABEL_BUDGET_ALLOCATION + ' Edit';
            pageTitle = LABEL_BUDGET_ALLOCATION + ' ' + sectionHeaderTitle;

        }else{// New Budget Allocation
        	cycleId = ctr.getId();
            map_urlParams = ApexPages.currentPage().getParameters();
            cycleId= map_urlParams.get('cId');
        	clsCycleProductBudget = new ClsCycleProductBudget();
        	//initialize clsCycleProductBudget
        	searchByCycleProduct();
            //initialize the page title
            sectionHeaderSubTitle = 'New ' + LABEL_BUDGET_ALLOCATION;
            sectionHeaderTitle = LABEL_BUDGET_ALLOCATION + ' Edit';
            pageTitle = LABEL_BUDGET_ALLOCATION + ' ' + sectionHeaderTitle;
            list_clsBudgetAllocations = getChannelInfo();
        }

        //Get select options
        productsByCycle = getProductsByCycle(clsCycleProductBudget.cycle);

    }

    //********************************* -=BEGIN public methods=- **********************************
   /**
    * This method is used to get list of Product by country id
    *
    @author  Hely <hely.lin@itbcousult.com>
    @created 2015-03-06
    @version 1.0
    @since   29.0 (Force.com ApiVersion)
    *
    @return  products   list<SelectOption>
    *
    @changelog
    * 2015-03-06 Hely <hely.lin@itbcousult.com>
    * - Created
    */
    public List<SelectOption> getProductsByCycle(String idCycle){
        List<SelectOption> products = new List<SelectOption>();
        set<Id> set_productId = new set<Id>();
        List<AggregateResult> list_cycleDataOverview = [SELECT Product_Catalog_BI__c FROM Cycle_Data_Overview_BI__c where Cycle_BI__c = :idCycle and Product_Catalog_BI__c != null group by Product_Catalog_BI__c];
        for(AggregateResult result : list_cycleDataOverview){
        	set_productId.add((Id)result.get('Product_Catalog_BI__c'));
        }
        products.add(new SelectOption('', '--None--'));

        for(Product_vod__c p : [SELECT Id, Name FROM Product_vod__c WHERE Active_BI__c = true AND Id in :set_productId ORDER BY Name]){
            if(p.Name != null)
                products.add(new SelectOption(p.Id, p.Name));
        }

        if(products==null||products.isEmpty()){
            if(!hasError){
                hasError = true;
                errMsg = 'There is no available Products for this Cycle!';
            }

            products.add(new SelectOption('', '--None--'));
        }

        return products;
    }

    /**
    * Return true if the cylcle contains matrices Ready for NTL
    * @author  Jefferson Escobar
    * @created 2017-07-26
    */
    public boolean hasReadyForNTL(){
      List<Matrix_BI__c> matricesNTL = [SELECT Id From Matrix_BI__c
        WHERE Cycle_BI__c = :cycleId AND	Product_Catalog_BI__c = :productId AND Status_BI__c = 'Ready for NTL' Limit 1];

        return (matricesNTL != null && matricesNTL.size() > 0) ? true : false;
    }

  /**
    * This method is used to search cycleProductBudget by cycleId and productId
    *
    @author  Hely <hely.lin@itbcousult.com>
    @created 2015-03-06
    @version 1.0
    @since   29.0 (Force.com ApiVersion)
    *
    @return
    *
    @changelog
    * 2015-03-06 Hely <hely.lin@itbcousult.com>
    * - Created
    */
    public void searchByCycleProduct(){
    	decimal allTotal = 0;
    	ClsCycleProductBudget clsCPB = new ClsCycleProductBudget();
		clsCPB.cycle = cycleId;
		clsCPB.product = productId;
		List<Cycle_Product_Budget_BI__c> list_cpb = [select Id
					        							, Name
														, Cycle_BI__c
														, Product_Catalog_BI__c
														, Overall_Budget_BI__c
														from Cycle_Product_Budget_BI__c
														where Product_Catalog_BI__c = :productId
														and Cycle_BI__c = :cycleId order by Id];
		if(list_cpb!=null && list_cpb.size()!=0){
			clsCPB.id = list_cpb[0].Id;
            clsCPB.budget = (Userinfo.getLocale().containsIgnoreCase('de')) ? (list_cpb[0].Overall_Budget_BI__c+'').replace('.',',') : list_cpb[0].Overall_Budget_BI__c+'';
		}

		map<Id, Budget_Allocation_BI__c> map_id_budgetAllocation = new map<Id, Budget_Allocation_BI__c>();
        for(Budget_Allocation_BI__c ba : [select Id
                                                ,Name
                                                ,Cycle_BI__c
                                                ,Channel_BI__c
                                                ,Unit_BI__c
                                                ,Total_Cost_BI__c
                                                ,Unit_Costs_BI__c
                                                ,Product_Catalog_BI__c
                                                From Budget_Allocation_BI__c
                                                where Cycle_BI__c = :clsCPB.cycle
                                                and Product_Catalog_BI__c = :clsCPB.product
                                                order by ID
                                                //and Unit_Costs_BI__c != null
                                                ]){
            map_id_budgetAllocation.put(ba.Channel_BI__c, ba);
        }
        List<ClsBudgetAllocation> list_budgetAllocation = new List<ClsBudgetAllocation>();
        for(ClsBudgetAllocation cba : getChannelInfo()){
            if(map_id_budgetAllocation.get(cba.cId)!=null){
                Budget_Allocation_BI__c ba = map_id_budgetAllocation.get(cba.cId);
                cba.baId = ba.Id;

	            if(Userinfo.getLocale().containsIgnoreCase('de')){
	            	cba.unitCost = (ba.Unit_Costs_BI__c != null) ? (ba.Unit_Costs_BI__c+'').replace('.',',') : '0';
	            	//cba.unit = (ba.Unit_BI__c != null) ? Integer.valueOf(ba.Unit_BI__c) : 0;
	            	cba.totalCost = (ba.Total_Cost_BI__c != null) ? (ba.Total_Cost_BI__c+'').replace('.',',') : '0';
	            }else{
	            	cba.unitCost = (ba.Unit_Costs_BI__c != null) ? ba.Unit_Costs_BI__c+'' : '0';
	            	cba.totalCost = (ba.Total_Cost_BI__c != null) ? (ba.Total_Cost_BI__c+'') : '0';
	            }
	            //System.debug(cba.totalCost.replace('.',','));
	            if(ba.Total_Cost_BI__c != null){
	            	allTotal = allTotal + ba.Total_Cost_BI__c;
	            }
            	 //(ba.Total_Cost_BI__c != null) ? (ba.Total_Cost_BI__c) : 0;
            	cba.unit = (ba.Unit_BI__c != null) ? Integer.valueOf(ba.Unit_BI__c) : 0;
                //cba.totalCost = (ba.Budget_BI__c!=null && ba.Unit_Costs_BI__c!=null)? ba.Budget_BI__c*ba.Unit_Costs_BI__c : 0;
            }else{

        		cba.unit = 0;
            	if(Userinfo.getLocale().containsIgnoreCase('de')){
            		cba.unitCost = '0,00';
            		cba.totalCost = '0,00';
            	}else{
            		cba.unitCost = '0.00';
            		cba.totalCost = '0.00';
            	}
            }
            list_budgetAllocation.add(cba);
        }
		clsCPB.list_cBudgetAllocation = list_budgetAllocation;
		//initialize clsCycleProductBudget
    	clsCycleProductBudget = clsCPB;
    	 if(Userinfo.getLocale().containsIgnoreCase('de')){
    	 	total = (allTotal+'').replace('.',',');
    	 }else{
	    	total = allTotal+'';
    	 }
    	//showing warning if the cyclce contains matrices ready for NTL
      if(hasReadyForNTL()){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, System.Label.IMP_BI_WarningNTLBudget));
      }
    }

    /**
    * This method is used to get list of channels
    *
    @author  Hely <hely.lin@itbcousult.com>
    @created 2015-03-06
    @version 1.0
    @since   29.0 (Force.com ApiVersion)
    *
    @return  products   list<ClsBudgetAllocation>
    *
    @changelog
    * 2015-03-06 Hely <hely.lin@itbcousult.com>
    * - Created
    */
    public List<ClsBudgetAllocation> getChannelInfo(){

        List<ClsBudgetAllocation> list_clsBudgetAllocation = new List<ClsBudgetAllocation>();
        List<Cycle_BI__c> list_cycle = [SELECT Id, Name, Country_Lkp_BI__c FROM Cycle_BI__c where Id = :cycleId];
        Id cId = null;
        if(list_cycle != null && list_cycle.size()>0){
        	cId = list_cycle[0].Country_Lkp_BI__c;
        }
        map<String, Country_Channel_BI__c> map_channelName_countryChannel = new map<String, Country_Channel_BI__c>();
        for(Country_Channel_BI__c cc : [SELECT Channel_BI__c, Channel_BI__r.Name, Country_BI__c FROM Country_Channel_BI__c where Country_BI__c = :cId]){
        	if(map_channelName_countryChannel == null || map_channelName_countryChannel.get(cc.Channel_BI__r.Name)==null){
        		map_channelName_countryChannel.put(cc.Channel_BI__r.Name, cc);
        	}
        }

        //for(Channel_BI__c ch : [SELECT Id, Name, Unit_Costs_BI__c, Total_Budget_BI__c, IsDeleted, Cost_Rate_BI__c FROM Channel_BI__c where IsDeleted = false]){
        for(String name : map_channelName_countryChannel.keySet()){
        	Country_Channel_BI__c cc = map_channelName_countryChannel.get(name);
            ClsBudgetAllocation clsBudgetAllocation = new ClsBudgetAllocation();
            clsBudgetAllocation.cId = cc.Channel_BI__c;
            clsBudgetAllocation.cName = cc.Channel_BI__r.Name;
            list_clsBudgetAllocation.add(ClsBudgetAllocation);
        }
        return list_clsBudgetAllocation;
    }


    /**
    * This method is used to cancel back to cycle detail page
    *
    @author  Hely <hely.lin@itbcousult.com>
    @created 2015-03-06
    @version 1.0
    @since   29.0 (Force.com ApiVersion)
    *
    @return  Pagereference
    *
    @changelog
    * 2015-03-06 Hely <hely.lin@itbcousult.com>
    * - Created
    */
    public Pagereference cancel(){
        Pagereference page;

        if(clsCycleProductBudget != null && clsCycleProductBudget.cycle != null){
            page = new Pagereference('/' + clsCycleProductBudget.cycle);
        }
        else{
            page = new Pagereference('/' + KEYPREFIX_CYCLE);
        }
        page.setRedirect(true);
        return page;
    }

    /**
    * Save a new cycleProductBudget and budgetAllocations
    *
    @author  Hely <hely.lin@itbcousult.com>
    @created 2015-03-06
    @version 1.0
    @since   29.0 (Force.com ApiVersion)
    *
    @param budget JSON Cycle Product Budget Class
    *
    @return  Pagereference
    *
    @changelog
    * 2015-03-06 Hely <hely.lin@itbcousult.com>
    * - Created
    */
    @RemoteAction
    public static String saveBudget(String budgetJSON){
        Response r = new Response();
        SavePoint sp;
        try{
            system.debug(':: JSON Object: ' + budgetJSON);
            sp = Database.setSavepoint();

            ClsCycleProductBudget clsCyclePB = (ClsCycleProductBudget)JSON.deserialize(budgetJSON, ClsCycleProductBudget.class);
            system.debug(':: Budget Object to save: ' + clsCyclePB);

            Cycle_Product_Budget_BI__c upsert_cycleProductBudget = new Cycle_Product_Budget_BI__c();
            upsert_cycleProductBudget.Id = (clsCyclePB.id !=null && clsCyclePB.id != '') ? clsCyclePB.id : null;

            if(clsCyclePB.id == null || clsCyclePB.id == '')
                upsert_cycleProductBudget.Cycle_BI__c = clsCyclePB.cycle;
            //if(clsCyclePB.country != null && clsCyclePB.country=='DE'){
            if(Userinfo.getLocale().containsIgnoreCase('de')){
            	upsert_cycleProductBudget.Overall_Budget_BI__c = decimal.valueOf(((clsCyclePB.budget+'').replace('.','')).replace(',','.'));
            }else{
            	upsert_cycleProductBudget.Overall_Budget_BI__c = decimal.valueOf((clsCyclePB.budget+'').replace(',',''));
            }
            upsert_cycleProductBudget.Product_Catalog_BI__c = clsCyclePB.product;
            List<Budget_Allocation_BI__c> list_upsertBudgetAllocation = new List<Budget_Allocation_BI__c>();
            List<Budget_Allocation_BI__c> list_deleteBudgetAllocation = new List<Budget_Allocation_BI__c>();
            for(ClsBudgetAllocation loop_clsBA : clsCyclePB.list_cBudgetAllocation){
                Budget_Allocation_BI__c budgetAllocation = new Budget_Allocation_BI__c();
                budgetAllocation.Id = (loop_clsBA.baId !=null) ? loop_clsBA.baId : null;
                //budgetAllocation.Id = loop_clsBA.baId;
                if(budgetAllocation.Id == null){
                    budgetAllocation.Cycle_BI__c = clsCyclePB.cycle;
                }
                budgetAllocation.Product_Catalog_BI__c = clsCyclePB.product;
                if(Userinfo.getLocale().containsIgnoreCase('de')){
	            	budgetAllocation.Unit_Costs_BI__c = (loop_clsBA.unitCost != null && loop_clsBA.unitCost != '') ? decimal.valueOf(((loop_clsBA.unitCost).replace('.','')).replace(',','.')) : 0;
	            	budgetAllocation.Total_Cost_BI__c = (loop_clsBA.totalCost != null && loop_clsBA.totalCost != '') ? decimal.valueOf(((loop_clsBA.totalCost).replace('.','')).replace(',','.')) : 0;
	            }else{
	            	budgetAllocation.Unit_Costs_BI__c = (loop_clsBA.unitCost != null && loop_clsBA.unitCost != '') ? decimal.valueOf((loop_clsBA.unitCost).replace(',','')) : 0;
                	budgetAllocation.Total_Cost_BI__c = (loop_clsBA.totalCost != null && loop_clsBA.totalCost != '') ? decimal.valueOf((loop_clsBA.totalCost).replace(',','')) : 0;
	            }
            	budgetAllocation.Unit_BI__c = (loop_clsBA.unit != null) ? Integer.valueOf(loop_clsBA.unit) : 0;
                budgetAllocation.Channel_BI__c = loop_clsBA.cId;
                if(budgetAllocation.Total_Cost_BI__c != null && budgetAllocation.Total_Cost_BI__c != 0){
	                list_upsertBudgetAllocation.add(budgetAllocation);
                }else{
                	if(budgetAllocation.Id != null){
                		list_deleteBudgetAllocation.add(budgetAllocation);
                	}
                }
            }
            //Insert/Update record object
            upsert upsert_cycleProductBudget;
            upsert list_upsertBudgetAllocation;
            delete list_deleteBudgetAllocation;

        }catch(DmlException de){
            Database.rollback(sp);
            r.success = false;
            r.message = de.getMessage();
            return JSON.serialize(r);
        }
        catch(Exception e){
            r.success = false;
            r.message = e.getMessage();
            return JSON.serialize(r);
        }

        r.success = true;
        r.message = 'OK';
        return JSON.serialize(r);
    }

/**
==================================================================================================================================
                                                Wrapper Classes
==================================================================================================================================
*/
    public class ClsCycleProductBudget{
        public String id {get;set;}
        public String cycle {get;set;}
        public String product {get;set;}
        //public string country {get; set;}
        public String budget {get;set;}
        public List<ClsBudgetAllocation> list_cBudgetAllocation {get; set;}

        public ClsCycleProductBudget(){
        	this.id = null;
        	this.cycle = null;
        	this.product = null;
        	//this.country = null;
        	this.budget = null;
        	this.list_cBudgetAllocation = new List<ClsBudgetAllocation>();
        }
    }

    public class ClsBudgetAllocation{
        public Id cId {get; set;}
        public String cName {get; set;}
        public Id baId {get; set;}
        public String unitCost {get; set;}
        public Integer unit {get; set;}
        public String totalCost {get; set;}

        public ClsBudgetAllocation(){
        	this.cId = null;
        	this.cName = null;
        	this.baId = null;
        	this.unitCost = null;
        	this.unit = null;
        	this.totalCost = null;
        }
    }


    public class Response{
        public boolean success;
        public string message;

        public Response(){
            success = true;
            message = '';
        }
    }
}