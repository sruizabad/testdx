/**
 *	Test class used for IMP_BI_CtrlManageBudgetAllocation
 *
 @author 	Hely
 @created 	2015-03-06
 @version 	1.0
 @since 	27.0 (Force.com ApiVersion)
 *
 @changelog
 * - Created
 *- Test coverage
 */
@isTest
private class IMP_BI_CtrlManageBudgetAllocation_Test {

    static testMethod void methodsTest() {
    	List<User> list_user = [select Id, LocaleSidKey from User where id = :UserInfo.getUserId()];
    	
    	Product_vod__c product = new Product_vod__c();
    	product.Name = 'productName';
    	product.Schedule_vod__c = '0';
    	product.Controlled_Substance_vod__c = true;
    	insert product;
    	
    	Cycle_BI__c cycle = new Cycle_BI__c();
    	cycle.Name_BI__c = 'cycleName';
    	cycle.Start_Date_BI__c = system.today();
    	cycle.End_Date_BI__c = system.today();
    	insert cycle;
    	
    	List<Channel_BI__c> list_channel = new List<Channel_BI__c>();
    	Channel_BI__c channel1 = new Channel_BI__c();
    	channel1.Name = 'channel1';
    	channel1.Cost_Rate_BI__c = 1;
    	channel1.Unit_BI__c = '100';
    	list_channel.add(channel1);
    	Channel_BI__c channel2 = new Channel_BI__c();
    	channel2.Name = 'channel2';
    	channel2.Cost_Rate_BI__c = 1;
    	channel2.Unit_BI__c = '200';
    	list_channel.add(channel2);
    	Channel_BI__c channel3 = new Channel_BI__c();
    	channel3.Name = 'channel3';
    	channel3.Cost_Rate_BI__c = 1;
    	channel3.Unit_BI__c = '300';
    	list_channel.add(channel3);
    	insert list_channel;
    	
    	Budget_Allocation_BI__c budgetAllocation1 = new Budget_Allocation_BI__c();
    	budgetAllocation1.Cycle_BI__c = cycle.Id;
    	budgetAllocation1.Product_Catalog_BI__c = cycle.Product_BI__c;
    	budgetAllocation1.Channel_BI__c = channel1.Id;
    	budgetAllocation1.Unit_Costs_BI__c = 100;
    	insert budgetAllocation1;
    	
        Cycle_Product_Budget_BI__c cycleProductBudget = new Cycle_Product_Budget_BI__c();
        cycleProductBudget.Cycle_BI__c = cycle.Id;
        cycleProductBudget.Product_Catalog_BI__c = cycle.Product_BI__c;
        cycleProductBudget.Overall_Budget_BI__c = 10000;
        insert cycleProductBudget;
        
        Test.startTest();
        
    	list_user[0].LocaleSidKey = 'en_US';
    	update list_user[0];
    	
        ApexPages.currentPage().getParameters().put('cId',cycle.Id);
        IMP_BI_CtrlManageBudgetAllocation cmba = new IMP_BI_CtrlManageBudgetAllocation(new ApexPages.StandardController(cycleProductBudget));
    	cmba.cycleId = cycle.Id;
    	cmba.productId = product.Id;
    	cmba.getProductsByCycle(cycle.Id);
    	cmba.getChannelInfo();
    	cmba.prodId = 'a00J00000051l28IAA';
    	cmba.total = '1000';
    	cmba.searchByCycleProduct();
    	IMP_BI_CtrlManageBudgetAllocation.saveBudget('{{"list_cBudgetAllocation":[{"cId":"a37J00000008ehDIAQ","baId":"a38J0000000ZOVAIA4","unitCost":"20000.00","unit":"1","totalCost":"20000.00"},{"cId":"a37J00000008ehIIAQ","baId":"a38J0000000ZOUNIA4","unitCost":"2.00","unit":"3","totalCost":"6.00"},{"cId":"a37J00000008eh3IAA","baId":"a38J0000000ZOVBIA4","unitCost":"1000.00","unit":"1","totalCost":"1000.00"},{"cId":"a37J00000008eh8IAA","baId":"a38J0000000ZOUOIA4","unitCost":"4.00","unit":"1","totalCost":"4.00"},{"cId":"a37J0000000AngbIAC","baId":null,"unitCost":"0.00","unit":"0","totalCost":"0.00"}],"Id":"a4mJ0000000K3dpIAC","cycle":"a39J0000000LxJr","product":"a00J00000051l28IAA","budget":"100000000.00"}}');
    	cmba.cancel();
    	
    	list_user[0].LocaleSidKey = 'de';
    	update list_user[0];
    	cycleProductBudget.Id = null;
    	ApexPages.StandardController ctrl = new ApexPages.StandardController(cycleProductBudget);
    	IMP_BI_CtrlManageBudgetAllocation cmba2 = new IMP_BI_CtrlManageBudgetAllocation(ctrl);
    	cmba2.cycleId = cycle.Id;
    	cmba2.productId = product.Id;
    	cmba2.getProductsByCycle(cycle.Id);
    	cmba2.getChannelInfo();
    	cmba.prodId = product.Id;
    	cmba.total = '1000';
    	cmba2.searchByCycleProduct();
    	IMP_BI_CtrlManageBudgetAllocation.saveBudget('{{"list_cBudgetAllocation":[{"cId":"a37J00000008ehDIAQ","baId":"a38J0000000ZOVAIA4","unitCost":"20,00","unit":"1","totalCost":"20000,00"},{"cId":"a37J00000008ehIIAQ","baId":"a38J0000000ZOUNIA4","unitCost":"2,00","unit":"3","totalCost":"6,00"},{"cId":"a37J00000008eh3IAA","baId":"a38J0000000ZOVBIA4","unitCost":"1000,00","unit":"1","totalCost":"1000.00"},{"cId":"a37J00000008eh8IAA","baId":"a38J0000000ZOUOIA4","unitCost":"4,00","unit":"1","totalCost":"4.00"},{"cId":"a37J0000000AngbIAC","baId":null,"unitCost":"0,00","unit":"0","totalCost":"0,00"}],"Id":"a4mJ0000000K3dpIAC","cycle":"a39J0000000LxJr","product":"a00J00000051l28IAA","budget":"100000000,00"}}');
    	Test.stopTest();
    	
    }
    
    static testMethod void nullTest() {
    	List<User> list_user = [select Id, LocaleSidKey from User where id = :UserInfo.getUserId()];
    	
    	Product_vod__c product = new Product_vod__c();
    	product.Name = 'productName';
    	product.Schedule_vod__c = '0';
    	product.Controlled_Substance_vod__c = true;
    	insert product;
    	
    	Cycle_BI__c cycle = new Cycle_BI__c();
    	cycle.Name_BI__c = 'cycleName';
    	cycle.Start_Date_BI__c = system.today();
    	cycle.End_Date_BI__c = system.today();
    	insert cycle;
    	
    	List<Channel_BI__c> list_channel = new List<Channel_BI__c>();
    	Channel_BI__c channel1 = new Channel_BI__c();
    	channel1.Name = 'channel1';
    	channel1.Cost_Rate_BI__c = 1;
    	channel1.Unit_BI__c = '100';
    	list_channel.add(channel1);
    	Channel_BI__c channel2 = new Channel_BI__c();
    	channel2.Name = 'channel2';
    	channel2.Cost_Rate_BI__c = 1;
    	channel2.Unit_BI__c = '200';
    	list_channel.add(channel2);
    	Channel_BI__c channel3 = new Channel_BI__c();
    	channel3.Name = 'channel3';
    	channel3.Cost_Rate_BI__c = 1;
    	channel3.Unit_BI__c = '300';
    	list_channel.add(channel3);
    	insert list_channel;
    	
    	Budget_Allocation_BI__c budgetAllocation1 = new Budget_Allocation_BI__c();
    	budgetAllocation1.Cycle_BI__c = cycle.Id;
    	budgetAllocation1.Product_Catalog_BI__c = cycle.Product_BI__c;
    	budgetAllocation1.Channel_BI__c = channel1.Id;
    	budgetAllocation1.Unit_Costs_BI__c = 100;
    	insert budgetAllocation1;
    	
        Cycle_Product_Budget_BI__c cycleProductBudget = new Cycle_Product_Budget_BI__c();
        cycleProductBudget.Cycle_BI__c = cycle.Id;
        cycleProductBudget.Product_Catalog_BI__c = cycle.Product_BI__c;
        cycleProductBudget.Overall_Budget_BI__c = 10000;
        
        Test.startTest();
        
    	list_user[0].LocaleSidKey = 'en_US';
    	update list_user[0];
    	
        ApexPages.currentPage().getParameters().put('cId',cycle.Id);
        IMP_BI_CtrlManageBudgetAllocation cmba = new IMP_BI_CtrlManageBudgetAllocation(new ApexPages.StandardController(cycleProductBudget));
    	cmba.cycleId = cycle.Id;
    	cmba.productId = product.Id;
    	cmba.getProductsByCycle(cycle.Id);
    	cmba.getChannelInfo();
    	cmba.prodId = 'a00J00000051l28IAA';
    	cmba.total = '1000';
    	cmba.searchByCycleProduct();
    	IMP_BI_CtrlManageBudgetAllocation.saveBudget('{{"list_cBudgetAllocation":[{"cId":"a37J00000008ehDIAQ","baId":"a38J0000000ZOVAIA4","unitCost":"20000.00","unit":"1","totalCost":"20000.00"},{"cId":"a37J00000008ehIIAQ","baId":"a38J0000000ZOUNIA4","unitCost":"2.00","unit":"3","totalCost":"6.00"},{"cId":"a37J00000008eh3IAA","baId":"a38J0000000ZOVBIA4","unitCost":"1000.00","unit":"1","totalCost":"1000.00"},{"cId":"a37J00000008eh8IAA","baId":"a38J0000000ZOUOIA4","unitCost":"4.00","unit":"1","totalCost":"4.00"},{"cId":"a37J0000000AngbIAC","baId":null,"unitCost":"0.00","unit":"0","totalCost":"0.00"}],"Id":"a4mJ0000000K3dpIAC","cycle":"a39J0000000LxJr","product":"a00J00000051l28IAA","budget":"100000000.00"}}');
    	cmba.cancel();
    	
    	list_user[0].LocaleSidKey = 'de';
    	update list_user[0];
    	cycleProductBudget.Id = null;
    	ApexPages.StandardController ctrl = new ApexPages.StandardController(cycleProductBudget);
    	IMP_BI_CtrlManageBudgetAllocation cmba2 = new IMP_BI_CtrlManageBudgetAllocation(ctrl);
    	cmba2.cycleId = cycle.Id;
    	cmba2.productId = product.Id;
    	cmba2.getProductsByCycle(cycle.Id);
    	cmba2.getChannelInfo();
    	cmba.prodId = product.Id;
    	cmba.total = '1000';
    	cmba2.searchByCycleProduct();
    	IMP_BI_CtrlManageBudgetAllocation.saveBudget('{{"list_cBudgetAllocation":[{"cId":"a37J00000008ehDIAQ","baId":"a38J0000000ZOVAIA4","unitCost":"20,00","unit":"1","totalCost":"20000,00"},{"cId":"a37J00000008ehIIAQ","baId":"a38J0000000ZOUNIA4","unitCost":"2,00","unit":"3","totalCost":"6,00"},{"cId":"a37J00000008eh3IAA","baId":"a38J0000000ZOVBIA4","unitCost":"1000,00","unit":"1","totalCost":"1000.00"},{"cId":"a37J00000008eh8IAA","baId":"a38J0000000ZOUOIA4","unitCost":"4,00","unit":"1","totalCost":"4.00"},{"cId":"a37J0000000AngbIAC","baId":null,"unitCost":"0,00","unit":"0","totalCost":"0,00"}],"Id":"a4mJ0000000K3dpIAC","cycle":"a39J0000000LxJr","product":"a00J00000051l28IAA","budget":"100000000,00"}}');
    	Test.stopTest();
    	
    }
}