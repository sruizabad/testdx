/**
 *	Controller class used for VF Page IMP_BI_CtrlManageCustomerRole
 *
 @author 	Hely
 @created 	2015-01-23
 @version 	1.0
 @since 	27.0 (Force.com ApiVersion)
 *
 @changelog
 * 
 */
public class IMP_BI_CtrlManageCustomerRole {
	 public static final string KEYPREFIX_CYCLE =Schema.SObjectType.Cycle_BI__c.getKeyPrefix();
	 
	 public string message {get; set;}
	 public string msgType {get; set;}
	 public String pageTitle {get; set;}
	 public String sectionHeaderTitle {get; set;}
	 public String sectionHeaderSubTitle {get; set;}
	 public String cycleName {get; set;}
	 public Id id {get; set;}
	 public Integer groupIndex {get; set;}
	 
	 public List<ClsRole> list_role {get; set;}
	 public static List<ClsGroup> list_group {get; set;}
	 private map<String, String> map_urlParams ;
	 
	/**
	* Initialization role
	*
	@author Hely
	@created 2015-01-23
	@version 1.0
	@since 27.0 (Force.com ApiVersion)
	*
	@changelog
	* 
	* - 
	*/
	 public IMP_BI_CtrlManageCustomerRole(ApexPages.StandardController controller) {
	  	
	  	id = ApexPages.currentPage().getParameters().get('id');
	  	Cycle_BI__c cycle = [select Id, Name from Cycle_BI__c where Id = :id];
	  	this.pageTitle = cycle.Name;
	  	this.sectionHeaderTitle = cycle.Name;
	  	map_urlParams = ApexPages.currentPage().getParameters();
	  	List<Customer_Role_Grouping_BI__c> list_allRole = [select Id, Name, Cycle_BI__c, Group_BI__c from Customer_Role_Grouping_BI__c where Cycle_BI__c =:id ];
		list_role = new List<ClsRole>();
	  	list_group = new List<ClsGroup>();
	  	
	  	
	  	Integer rId = 0;
	  	for(Customer_Role_Grouping_BI__c crg: list_allRole){
	  		ClsRole cr = new ClsRole();
	  		cr.rId = crg.Id;
	  		cr.rName = crg.Name;
	  		
	  		ClsGroup cg = new ClsGroup();
	  		cg.gName = crg.Group_BI__c;
	  		Integer gNum = 0;//用来判断list_group是否完全遍历
	  		
	  		if(cg.gName!=null && cg.gName.trim()!='' ){//判断gName是否为空，若不为空则hasGroup为true
	  			cr.hasGroup = true;
  				cg.list_ro.add(cr);
	  			for(Integer i=0; i<list_group.size(); i++){
	  				if(cg.gName.equals(list_group[i].gName)){//判断list_group中是否已存在gName，若已存在则该ClsGroup.list_ro add该CloRole
	  					list_group[i].list_ro.add(cr);
		  				break;
	  				}else{
		  				gNum++;
	  				}
	  			}
		  		if( gNum == list_group.size()){
		  			list_group.add(cg);
		  		}
	  		}
	  		list_role.add(cr);
	  		rId++;
	  	}
	  	groupIndex = list_group.size();
	  	
	}
	
	/**
	* Modifying role of group name
	*
	@author Hely
	@created 2015-01-23
	@version 1.0
	@since 27.0 (Force.com ApiVersion)
	*
	@changelog
	* 
	* - 
	*/
	@RemoteAction
	public static String saveRolesData(String finalResult){
		Response r = new Response();
    	Savepoint sp = Database.setSavepoint();
    	
		try{
	    	List<ClsRoleJson> list_crj = (List<ClsRoleJson>)JSON.deserialize(finalResult, List<ClsRoleJson>.class);
	    	map<ID, String> map_rId_rGroup = new map<ID, String>();
	    	for(ClsRoleJson crj :list_crj){
	    		map_rId_rGroup.put(crj.rId, crj.rGroup);
	    	}
	    	
	    	List<Customer_Role_Grouping_BI__c> list_upCrgbc = new List<Customer_Role_Grouping_BI__c>();
		 	List<Customer_Role_Grouping_BI__c> list_crgbc = [select Id, Group_BI__c from Customer_Role_Grouping_BI__c where Id in :map_rId_rGroup.keySet()];
	    	
	    	for(Customer_Role_Grouping_BI__c chooseRole :list_crgbc){
	    		String gName = map_rId_rGroup.get(chooseRole.Id);
    			//if(!chooseRole.Group_BI__c.equals(gName)){
	    			chooseRole.Group_BI__c = gName;
		    		list_upCrgbc.add(chooseRole);
    			//}
	    	}
	    	
	    	update list_upCrgbc;
	    	
    	}catch(DmlException de){
    		Database.rollback(sp);
    		r.success = false;
			r.message = de.getMessage();
			return JSON.serialize(r);
    	}
    	catch(Exception e){
    		r.success = false;
			r.message = e.getMessage();
			return JSON.serialize(r);
    	}
    	
		r.success = true;
		r.message = 'successful';
		return JSON.serialize(r);
	}
	
	/**
	* Cancel ,reback cycle
	*
	@author Hely
	@created 2015-01-23
	@version 1.0
	@since 27.0 (Force.com ApiVersion)
	*
	@changelog
	* 
	* - 
	*/
	public Pagereference cancel(){
		Pagereference page;
		
		if(map_urlParams.containsKey('retURL')){
			page = new Pagereference(map_urlParams.get('retURL'));
		}
		else if(id != null){
			page = new Pagereference('/' + id);
		}
		else{
			page = new Pagereference('/' + KEYPREFIX_CYCLE);
		}
		
		page.setRedirect(true);
		
		return page;
	}
	
	
	public void showMessage(){
    	if(message != NULL && message.trim() != '') {
	    	if('CONFIRM'.equalsIgnoreCase(msgType)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, message));
	    	}
	    	else if('FATAL'.equalsIgnoreCase(msgType)) {
	    	
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, message));
	    	}
	    	else if('INFO'.equalsIgnoreCase(msgType)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, message));
	    	}
	    	else if('WARNING'.equalsIgnoreCase(msgType)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, message));
	    	}
	    	else {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
	    	}
    	}
    }
	
	
	public class ClsRole{
		public String rId {get; set;}
    	public String rName {get; set;}
    	public boolean hasGroup {get; set;}
    	
    	public ClsRole(){
    		hasGroup = false;
    	}
	}
	
	public class ClsGroup{
    	public String gName {get; set;}
    	public Boolean isNew {get; set;}
    	
    	public list<ClsRole> list_ro {get; set;}
    	
    	public ClsGroup(){
    		isNew = true;
    		list_ro = new list<ClsRole>();
    	}
    }
    
    public class ClsRoleJson{
    	public String rId {get; set;}
    	public String rName {get; set;}
    	public String rGroup {get; set;}
    }
	
	public class Response{
    	public boolean success;
    	public string message;
    	
    	public Response(){
    		success = true;
    		message = '';
    	}
    }
	
}