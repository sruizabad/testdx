/**
 *	Test class used for VF Page IMP_BI_CtrlManageCustomerRole
 *
 @author 	Hely
 @created 	2015-01-30
 @version 	1.0
 @since 	27.0 (Force.com ApiVersion)
 *
 @changelog
 * - Created
 *- Test coverage
 */
@isTest
private class IMP_BI_CtrlManageCustomerRole_Test {

    static testMethod void mehtodsTest() {
    	
		Cycle_BI__c cycle= IMP_BI_ClsTestHelp.createTestCycle();
        cycle = new Cycle_BI__c();
        cycle.Name = 'hely';
        cycle.Start_Date_BI__c = System.today();
        cycle.End_Date_BI__c = System.today();
        insert cycle;
        
        Customer_Role_Grouping_BI__c customerRole = new Customer_Role_Grouping_BI__c();
		customerRole.Name = 'role1';
		customerRole.Cycle_BI__c = cycle.Id;
		customerRole.Group_BI__c = 'group1';
		insert customerRole;
        
        Test.startTest();
        
        IMP_BI_CtrlManageCustomerRole.ClsGroup clsGroup= new IMP_BI_CtrlManageCustomerRole.ClsGroup();
        ApexPages.currentPage().getParameters().put('id',cycle.Id);
        ApexPages.StandardController ctrl = new ApexPages.StandardController(customerRole);
        IMP_BI_CtrlManageCustomerRole cmcr = new IMP_BI_CtrlManageCustomerRole(ctrl);  
        cmcr.showMessage();
        cmcr.cancel();
        String json = '[{"rId":"'+customerRole.Id+'","rName":"Peng","rGroup":"test0705"},{"rId":"'+customerRole.Id+'","rName":"Di Test 2","rGroup":"Di Grop"}]"';
    	IMP_BI_CtrlManageCustomerRole.saveRolesData(json);
     	Test.stopTest();
    }
    
     static testMethod void nullTest() {
		Cycle_BI__c cycle = IMP_BI_ClsTestHelp.createTestCycle();
        cycle = new Cycle_BI__c();
        cycle.Name = 'hely';
        cycle.Start_Date_BI__c = System.today();
        cycle.End_Date_BI__c = System.today();
        insert cycle;
        
		Customer_Role_Grouping_BI__c customerRole = new Customer_Role_Grouping_BI__c();
		customerRole.Name = 'role1';
		customerRole.Cycle_BI__c = cycle.Id;
		customerRole.Group_BI__c = 'group1';
		//insert customerRole;
        
        Test.startTest();
        
        IMP_BI_CtrlManageCustomerRole.ClsGroup clsGroup= new IMP_BI_CtrlManageCustomerRole.ClsGroup();
        ApexPages.currentPage().getParameters().put('id',cycle.Id);
        ApexPages.StandardController ctrl = new ApexPages.StandardController(customerRole);
        IMP_BI_CtrlManageCustomerRole cmcr = new IMP_BI_CtrlManageCustomerRole(ctrl);  
        cmcr.showMessage();
        cmcr.cancel();
        String json = '[{"rId":"'+customerRole.Id+'","rName":"Peng","rGroup":"test0705"},{"rId":"'+customerRole.Id+'","rName":"Di Test 2","rGroup":"Di Grop"}]"';
    	IMP_BI_CtrlManageCustomerRole.saveRolesData(json);
     	Test.stopTest();
    }
    
    
}