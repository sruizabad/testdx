/**
 * 
 */
@isTest
private class IMP_BI_CycleDataTrigger_Test {

    static testMethod void testInsert() {
        
        Account account = new Account();
        account.Name = 'account';
        insert account;
        
        Cycle_BI__c cycle = new Cycle_BI__c();
        cycle.Name_BI__c = 'cycle';
        cycle.Start_Date_BI__c = system.today();
        cycle.End_Date_BI__c = system.today();
        insert cycle;
        
        Matrix_BI__c matrix = new Matrix_BI__c(Name_BI__c='matrix1', Row_BI__c=10, Column_BI__c=11);
        insert matrix;
        
        Matrix_Cell_BI__c matrixCell = new Matrix_Cell_BI__c();
        matrixCell.Matrix_BI__c = matrix.Id; 
        matrixCell.Row_BI__c = 10;
        matrixCell.Column_BI__c = 11;
        insert matrixCell;
        
        Cycle_Data_BI__c cycleData = new Cycle_Data_BI__c();
        cycleData.Account_BI__c = account.Id;
        cycleData.Cycle_BI__c = cycle.Id;
        cycleData.Matrix_Cell_1_BI__c = matrixCell.Id;
        cycleData.Matrix_1_BI__c = matrix.Id;
        cycleData.Matrix_Cell_2_BI__c = matrixCell.Id;
        cycleData.Matrix_2_BI__c = matrix.Id;
        cycleData.Matrix_Cell_3_BI__c = matrixCell.Id;
        cycleData.Matrix_3_BI__c = matrix.Id;
        Test.startTest();
        insert cycleData;
        Test.stopTest();
        
        List<Cycle_Data_BI__c> list_cycleData = [SELECT Matrix_Cell_2_BI__c, Matrix_Cell_3_BI__c, Matrix_Cell_4_BI__c, Matrix_Cell_5_BI__c, Matrix_1_BI__c, Matrix_2_BI__c, Matrix_3_BI__c, Matrix_4_BI__c, Matrix_5_BI__c, Matrix_Cell_1_BI__c FROM Cycle_Data_BI__c where Matrix_Cell_1_BI__c = :matrixCell.Id];
        
        
        System.assertEquals(list_cycleData.size(), 1);
        System.assert(String.isNotBlank(list_cycleData[0].Matrix_Cell_1_BI__c));
        System.assert(String.isNotBlank(list_cycleData[0].Matrix_Cell_2_BI__c));
        System.assert(String.isNotBlank(list_cycleData[0].Matrix_Cell_2_BI__c));
    }
    
    static testMethod void testUpdate() {
    	Account account = new Account();
        account.Name = 'account';
        insert account;
        
        Cycle_BI__c cycle = new Cycle_BI__c();
        cycle.Name_BI__c = 'cycle';
        cycle.Start_Date_BI__c = system.today();
        cycle.End_Date_BI__c = system.today();
        insert cycle;
        
        Matrix_BI__c matrix = new Matrix_BI__c(Name_BI__c='matrix1', Row_BI__c=10, Column_BI__c=11);
        insert matrix;
        
        Matrix_Cell_BI__c matrixCell = new Matrix_Cell_BI__c();
        matrixCell.Matrix_BI__c = matrix.Id; 
        matrixCell.Row_BI__c = 10;
        matrixCell.Column_BI__c = 11;
        insert matrixCell;
        
        Cycle_Data_BI__c cycleData = new Cycle_Data_BI__c();
        cycleData.Account_BI__c = account.Id;
        cycleData.Cycle_BI__c = cycle.Id;
        cycleData.Matrix_Cell_1_BI__c = matrixCell.Id;
        cycleData.Matrix_1_BI__c = matrix.Id;
        cycleData.Matrix_Cell_2_BI__c = matrixCell.Id;
        cycleData.Matrix_2_BI__c = matrix.Id;
        cycleData.Matrix_Cell_3_BI__c = matrixCell.Id;
        cycleData.Matrix_3_BI__c = matrix.Id;
        insert cycleData;
        
        cycleData.Matrix_Cell_1_BI__c = null;
        Test.startTest();
        update cycleData;
        Test.stopTest();
        
        List<Cycle_Data_BI__c> list_cycleData = [SELECT Matrix_Cell_2_BI__c, Matrix_Cell_3_BI__c, Matrix_Cell_4_BI__c, Matrix_Cell_5_BI__c, Matrix_1_BI__c, Matrix_2_BI__c, Matrix_3_BI__c, Matrix_4_BI__c, Matrix_5_BI__c, Matrix_Cell_1_BI__c FROM Cycle_Data_BI__c where Matrix_Cell_2_BI__c = :matrixCell.Id];
        
        
        System.assertEquals(list_cycleData.size(), 1);
        System.assert(String.isBlank(list_cycleData[0].Matrix_Cell_1_BI__c));
        System.assert(String.isNotBlank(list_cycleData[0].Matrix_Cell_2_BI__c));
        System.assert(String.isNotBlank(list_cycleData[0].Matrix_Cell_2_BI__c));
    }
}