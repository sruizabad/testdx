/**
* ===================================================================================================================================
*                                   IMMPaCT BI                                                     
* ===================================================================================================================================
*  Decription:      Test for IMP_BI_ClsBatch_DeletionDupsOnCycle and IMP_BI_ClsScheduler_DeletionDupsOnCycle classes
*  @author:         Jefferson Escobar
*  @created:        30-Mar-2015
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          33.0 (Force.com ApiVersion) 
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         30-mar-2015                 jescobar                    Construction of the class.
*/ 
@isTest
private class IMP_BI_DeletionDups_Test {

    static testMethod void testDeletionDupsJob() {
        Test.startTest();
		
		//Set up custom setting
		IMP_BI_Deletion_Dups_Settings__c deletionDupsSetting = new IMP_BI_Deletion_Dups_Settings__c();
		deletionDupsSetting.Name = 'Z0';
		deletionDupsSetting.UserId__c = UserInfo.getUserId();
		insert deletionDupsSetting;
		
        // Schedule the test job
        String jobId = System.schedule('Manage specialties',
        '0 0 0 3 9 ? 2050', 
        new IMP_BI_ClsScheduler_DeletionDupsOnCycle());

        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
        NextFireTime
        FROM CronTrigger WHERE id = :jobId];

        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }

     static testMethod void testDeletionDupsBatch() {

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'immpTest', Email='immpactItegrationTest@immp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='immpactItegrationTest@immp.com');
        
        String  cycleId;
        IMP_BI_Deletion_Dups_Settings__c deletionDupsSetting = new IMP_BI_Deletion_Dups_Settings__c();
         
        System.runAs(u) {
        	Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
            c.Country_Code_BI__c = 'Z0';
            insert c;
            
             //Set up custom setting
			deletionDupsSetting.Name = 'Z0';
			deletionDupsSetting.UserId__c = deletionDupsSetting.UserId__c = UserInfo.getUserId();
			insert deletionDupsSetting;
            
            //Veeva implementation
            insert new Knipper_Settings__c();
            Account acc = IMP_BI_ClsTestHelp.createTestAccount();
            acc.Name = '123e';
            acc.Country_Code_BI__c = 'Z0';
            insert acc;
            
            Cycle_BI__c cycle = ClsTestHelp.createTestCycle();
            cycle.Name = 'Test-Dups_0001';
            cycle.Country_Lkp_BI__c = c.Id;
            cycle.isCurrent_BI__c = true;
            insert cycle;
            cycleId = cycle.Id;
            
            Product_vod__c p2 = ClsTestHelp.createTestProduct();
            p2.Name = '234';
            p2.Country_BI__c = c.Id;
            insert p2;
            
            Matrix_BI__c ma = ClsTestHelp.createTestMatrix();
            ma.Cycle_BI__c = cycle.Id;
            ma.Product_Catalog_BI__c = p2.Id;
            ma.Current_BI__c = true;
            insert ma;
            
            Cycle_Data_BI__c cd = new Cycle_Data_BI__c();
            cd.Product_Catalog_BI__c = p2.Id;
            cd.Account_BI__c = acc.Id;
            cd.Cycle_BI__c = cycle.Id;
            cd.Potential_BI__c = 12;
            cd.Intimacy_BI__c = 12;
            cd.Matrix_BI__c = ma.Id;
            insert cd;
              
        }
        
        //Start test context
        Test.startTest();
            //Run the batch
            String query = 'Select Id, Cycle_BI__c, Matrix_BI__c, Matrix_1_BI__c, Matrix_2_BI__c, Matrix_3_BI__c, Matrix_4_BI__c, Matrix_5_BI__c, Account_BI__c, LastModifiedById, UniqueKey_BI__c '+ 
                        'From Cycle_Data_BI__c where LastModifiedById in (\''+deletionDupsSetting.UserId__c+'\') and Account_BI__r.Country_Code_BI__c in  (\''+deletionDupsSetting.Name+'\')  And  Cycle_BI__r.IsCurrent_BI__c = true '+
            			'and isDeleted = false and SystemModStamp = THIS_YEAR ';
            Database.executeBatch(new IMP_BI_ClsBatch_DeletionDupsOnCycle(query));
        Test.stopTest();
        
     }
}