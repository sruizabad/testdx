public class IMP_BI_ExtChannelBudgetDefFilterFixData {

	public String strMatrixId {get;set;}


	public IMP_BI_ExtChannelBudgetDefFilterFixData() {
		// Set strMatrixId value
		getParameters();
	}

	/**
	 *  Delete all matrix cell details related to passed matrix Id. DML operation is
	 *	limited and will force another call if not all detailed are deleted.
	 */
	@RemoteAction
	public static String delCellDetailsInBatch(Id matrixId){
			Integer MAX_ROWS = 5000;
			String response = 'end';
			//String matrixId = (String) JSON.deserialize(jsonMatrixId, String.class);
			if (matrixId != null) {

					List<Matrix_Cell_Detail_BI__c> cellDetails = Database.query(
						'SELECT Id FROM Matrix_Cell_Detail_BI__c WHERE Matrix_Cell_BI__r.Matrix_BI__c = :matrixId LIMIT ' + MAX_ROWS
					);

					if(cellDetails != NULL && cellDetails.size() == MAX_ROWS){
						response = 'goToNext';
					}

					//Delete matrix cell details
					if(cellDetails != NULL && !cellDetails.isEmpty()){
							delete cellDetails;
					}

			}

			return response;
	}

	public void FixData()
	{
		List<List<Matrix_Cell_Detail_BI__c>> lstMatrixCellDetailDel=new List<List<Matrix_Cell_Detail_BI__c>>();
		getParameters();
		List<Matrix_Cell_Detail_BI__c> lstMatCellDet=[SELECT Id, Matrix_Filter_Value_BI__c
																									FROM Matrix_Cell_Detail_BI__c
																									WHERE Matrix_Cell_BI__r.Matrix_BI__c =:strMatrixId];

		Integer intCont=0;
		Integer intIteration=0;
		for(Matrix_Cell_Detail_BI__c objMatrixCellDet:lstMatCellDet)
		{
			if(0<intCont && intCont<9000)
			{
				lstMatrixCellDetailDel.get(intIteration-1).add(objMatrixCellDet);
				intCont++;
			}
			else
			{
				intCont=1;
				intIteration++;
				List<Matrix_Cell_Detail_BI__c> lstMatrixCellDetailTemp=new List<Matrix_Cell_Detail_BI__c>();
				lstMatrixCellDetailTemp.add(objMatrixCellDet);
				lstMatrixCellDetailDel.add(lstMatrixCellDetailTemp);
			}
		}
		for(List<Matrix_Cell_Detail_BI__c>lstMatrixCellDetailToDel:lstMatrixCellDetailDel)
		{
			try
			{
				database.delete(lstMatrixCellDetailToDel);
			}
			catch(System.exception e)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
			}
		}
	}
	public void getParameters()
	{
		if(ApexPages.currentPage().getParameters().containsKey('Id'))
		{
			strMatrixId=ApexPages.currentPage().getParameters().get('Id');
		}
	}

	/**
	* Go back resource allocation PageReference
	*/
	public Pagereference returnToResource()
	{
		PageReference objPageReference=new PageReference('/apex/IMP_BI_ExtChannelBudgetDefinitionFilter');
		try{
			//Restore Matrix Filter
			update new Matrix_BI__c(Id=strMatrixId, Matrix_Filter_BI__c = null);
			objPageReference.getParameters().put('id',strMatrixId);
			objPageReference.setRedirect(true);
		}catch(System.Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
		}

		return objPageReference;
	}
}