/**
*   Test class for class IMP_BI_ExtManageScenarios.
*
@author Yuanyuan Zhang
@created 2015-01-29
@version 1.0
@since 32.0
*
@changelog
* 2015-01-29 Yuanyuan Zhang <Yuanyuan.Zhang@itbconsult.com>
* - Created
*- Test coverage
*/
@isTest
private class IMP_BI_ExtManageScenarios_Test {
    static testmethod void test1 () {
        	Account acc = ClsTestHelp.createTestAccount();
        	acc.Name = '123e';
        	//acc.Specialization_BI__c = 'wer';
        	insert acc;

        	Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
		c.Country_Code_BI__c = 'Z0';
        	insert c;



        	Account acc1 = ClsTestHelp.createTestAccount();
        	acc.Name = '34tf34f';
        	//acc.Specialization_BI__c = 'wer';
        	insert acc1;

    	    Cycle_BI__c cycle = ClsTestHelp.createTestCycle();
        cycle.Country_Lkp_BI__c = c.Id;
    	    cycle.Country_BI__c = 'Z0';
    	    insert cycle;

    	    Product_vod__c p2 = ClsTestHelp.createTestProduct();
    	    p2.Name = '234';
    	    p2.Country_BI__c = c.Id;
    	    insert p2;

    	    Matrix_BI__c ma = ClsTestHelp.createTestMatrix();
    	    ma.Cycle_BI__c = cycle.Id;
        ma.Product_Catalog_BI__c = p2.Id;
    	    ma.Intimacy_Levels_BI__c = 11;
    	    ma.Potential_Levels_BI__c = 10;
    	    ma.Size_BI__c = '10x11';
    	    ma.Row_BI__c = 10;
    	    ma.Column_BI__c = 11;
    	    ma.Specialization_BI__c = 'Cardiologist;GP';
        ma.Scenario_BI__c = '1';
    	    ma.Current_BI__c = false;
    	    ma.Status_BI__c = 'Draft';
    	    insert ma;

    	    Matrix_BI__c ma1 = ClsTestHelp.createTestMatrix();
        ma1.Cycle_BI__c = cycle.Id;
    	    ma1.Product_Catalog_BI__c = p2.Id;
    	    ma1.Intimacy_Levels_BI__c = 11;
    	    ma1.Potential_Levels_BI__c = 10;
    	    ma1.Size_BI__c = '10x11';
    	    ma1.Row_BI__c = 10;
    	    ma1.Column_BI__c = 11;
    	    ma1.Specialization_BI__c = 'Cardiologist;GP';
    	    ma1.Scenario_BI__c = '2';
        ma1.Current_BI__c = true;
    	    ma1.First_Scenario_BI__c = ma.Id;
    	    insert ma1;

    	    Matrix_Cell_BI__c mc = ClsTestHelp.createTestMatrixCell();
    	    mc.Matrix_BI__c = ma.Id;
    	    insert mc;

    	    Matrix_Cell_BI__c mc1 = ClsTestHelp.createTestMatrixCell();
    	    mc1.Matrix_BI__c = ma1.Id;
    	    insert mc1;

        Cycle_Data_BI__c cd = new Cycle_Data_BI__c();
    	    cd.Product_Catalog_BI__c = p2.Id;
    	    cd.Account_BI__c = acc.Id;
    	    cd.Cycle_BI__c = cycle.Id;
    	    cd.Potential_BI__c = 12;
    	    cd.Intimacy_BI__c = 12;
    	    cd.Matrix_Cell_1_BI__c = mc.Id;
    	    cd.Matrix_Cell_2_BI__c = mc1.Id;
    	    cd.Current_Scenario_BI__c = 2;
    	    //cd.UniqueKey_BI__c = 'aaaaaaaaaaaefewf34gerwgbg34sss';
        insert cd;

    	    Cycle_Data_BI__c cd1 = new Cycle_Data_BI__c();
    	    cd1.Product_Catalog_BI__c = p2.Id;
    	    cd1.Account_BI__c = acc1.Id;
    	    cd1.Cycle_BI__c = cycle.Id;
    	    cd1.Matrix_Cell_1_BI__c = mc.Id;
        cd1.Matrix_Cell_2_BI__c = mc1.Id;
    	    cd1.Current_Scenario_BI__c = 2;
        	//cd1.UniqueKey_BI__c = 'bbbbbbbbbb454h56u76idfbsttj';
        	insert cd1;

        	Lifecycle_Template_BI__c mt = new Lifecycle_Template_BI__c();
        	mt.Name = 'mt';
    	    mt.Country_BI__c = c.Id;
    	    mt.Active_BI__c = true;
    	    insert mt;

		Customer_Attribute_BI__c sp = new Customer_Attribute_BI__c();
		sp.Country_BI__c = c.Id;
    	  	sp.Group_txt_BI__c = 'ENT';
    	  	sp.Name = 'SP-Test';
      	insert sp;

      	Channel_BI__c channel = new Channel_BI__c();
      	channel.Name = 'Face to Face';
      	channel.Cost_Rate_BI__c = 11;
      	channel.Unit_BI__c = 'asdfasfd';
      	insert channel;

      	Matrix_Template_BI__c matrixTemplate = new Matrix_Template_BI__c(Name = 'M-Template Testing',Country_BI__c=c.Id,Lifecycle_Template_BI__c=mt.Id,
        	Specialties_BI__c=sp.Name,Specialty_Ids_BI__c=sp.Id,Product_Catalog_BI__c=p2.Id);
      	insert matrixTemplate;

        	test.startTest();
        	PageReference pageRef = Page.IMP_BI_ExtManageScenarios;
        	Test.setCurrentPage(pageRef);
        	ApexPages.currentPage().getParameters().put('Id',ma.Id);
        	ApexPages.currentPage().getParameters().put('cId',cycle.Id);
       	ApexPages.StandardController ctrl = new ApexPages.StandardController(ma);
        	IMP_BI_ExtManageScenarios ext = new IMP_BI_ExtManageScenarios(ctrl);
        	ext.getProductsByCountry();
        	ext.rerenderEditMatrix();
        	ext.getTemplatesByCountry();
        	ext.getMatrixTemplatesByCountry(p2.Id);
        	//ext.getListOfSpecializations();
        	//ext.getListOfSpecializationsUSA();
          ext.getGlobalSpecialties();
        	ext.generateSelectOfMTByCountry();
        	ext.cancel();

        	ext.calculationMatrix();
        	IMP_BI_ExtManageScenarios.copyMatrice(ma.Id, mc.Id);
        	ext.scenNumCellString = mc.Id+'yyyy1,2,3;2,1,3';	// 2015-04-01 Modified by Hely
        	ext.callBatch();
        	String jason = '{"productId":"'+p2.Id+'","cid":"'+cycle.Id+'","list_cm":[{"mid":"'+ma.Id+'","name":"Peng Validate Test 1","special":"ENT",'+
        	' "tid":"'+mt.Id+'","row":4,"column":5,"set_sId":["'+sp.Id+'"],"dpa":true,"toCalculate":true},{"name":"Peng JSON Test","special":"Cardiology","tid":"'+
                mt.Id+'","row":10,"column":11,"set_sId":["'+sp.Id+'"],"dpa":true,"toCalculate":true}],"countryId":"'+c.Id+'","set_mtIds":["'+mt.Id+'"],"accountMatrix":true}';
        	IMP_BI_ExtManageScenarios.saveMatrixData(jason);

        	String jason2 = '{"productId":"'+p2.Id+'","cid":"'+cycle.Id+'","list_cm":[{"mid":"'+ma.Id+'","name":"Peng Validate Test 1","special":"ENT","tid":"'+mt.Id+'",'+
        	' "row":4,"column":5,"set_sId":["'+sp.Id+'"],"dpa":true,"toCalculate":true},{"name":"Peng JSON Test","special":"Cardiology","tid":"'+
                mt.Id+'","row":10,"column":11,"set_sId":["'+sp.Id+'"],"dpa":true,"toCalculate":true}],"countryId":"'+c.Id+'","countryCode":"'+c.Country_Code_BI__c+'","set_mtIds":["'+mt.Id+'"],"accountMatrix":true}';
        	IMP_BI_ExtManageScenarios.saveMatrixData(jason2);

        	IMP_BI_ExtManageScenarios.deleteMatrix(ma.Id);
        	IMP_BI_ExtManageScenarios.ClsMatrix clm = new IMP_BI_ExtManageScenarios.ClsMatrix();
        	IMP_BI_ExtManageScenarios.ClsMatrixs clms = new IMP_BI_ExtManageScenarios.ClsMatrixs();
        	IMP_BI_ExtManageScenarios.ClsLifeCylceTemplate clsLifecycleTemplate = new IMP_BI_ExtManageScenarios.ClsLifeCylceTemplate();
        	IMP_BI_ExtManageScenarios.ClsMatrixEdit clsMatrixEdit = new IMP_BI_ExtManageScenarios.ClsMatrixEdit();
        	IMP_BI_ExtManageScenarios.ClsSpecialization clsSpecialization = new IMP_BI_ExtManageScenarios.ClsSpecialization();
        	IMP_BI_ExtManageScenarios.ClsMatrixSpecial clsMatrixSpecial = new IMP_BI_ExtManageScenarios.ClsMatrixSpecial();
        	IMP_BI_ExtManageScenarios.Response clsResponse = new IMP_BI_ExtManageScenarios.Response();
        	test.stopTest();
    }

    static testmethod void test2 () {
        	Account acc = ClsTestHelp.createTestAccount();
        	acc.Name = '123e';
        	//acc.Specialization_BI__c = 'wer';
        	insert acc;

        	Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
     	c.Country_Code_BI__c = 'Z0';
     	insert c;

        	Account acc1 = ClsTestHelp.createTestAccount();
	    	acc.Name = '34tf34f';
        	//acc.Specialization_BI__c = 'wer';
        	insert acc1;

        	Cycle_BI__c cycle = ClsTestHelp.createTestCycle();
        	cycle.Country_Lkp_BI__c = c.Id;
        	cycle.Country_BI__c = 'Z0';
        	insert cycle;

        	Product_vod__c p2 = ClsTestHelp.createTestProduct();
        	p2.Name = '234';
        	p2.Country_BI__c = c.Id;
        	insert p2;

        	Matrix_BI__c ma = ClsTestHelp.createTestMatrix();
        	ma.Cycle_BI__c = cycle.Id;
        	ma.Product_Catalog_BI__c = p2.Id;
        	ma.Intimacy_Levels_BI__c = 11;
        	ma.Potential_Levels_BI__c = 10;
        	ma.Size_BI__c = '10x11';
        	ma.Row_BI__c = 10;
        	ma.Column_BI__c = 11;
        	ma.Specialization_BI__c = 'Cardiologist;GP';
        	ma.Scenario_BI__c = '1';
        	ma.Current_BI__c = false;
        	ma.Status_BI__c = 'Draft';
        	insert ma;

        	Matrix_BI__c ma1 = ClsTestHelp.createTestMatrix();
        	ma1.Cycle_BI__c = cycle.Id;
        	ma1.Product_Catalog_BI__c = p2.Id;
        	ma1.Intimacy_Levels_BI__c = 11;
        	ma1.Potential_Levels_BI__c = 10;
        	ma1.Size_BI__c = '10x11';
        ma1.Row_BI__c = 10;
        	ma1.Column_BI__c = 11;
        	ma1.Specialization_BI__c = 'Cardiologist;GP';
        	ma1.Scenario_BI__c = '2';
        	ma1.Current_BI__c = true;
        	ma1.First_Scenario_BI__c = ma.Id;
        	insert ma1;

        Matrix_Cell_BI__c mc = ClsTestHelp.createTestMatrixCell();
        	mc.Matrix_BI__c = ma.Id;
        	insert mc;

        	Matrix_Cell_BI__c mc1 = ClsTestHelp.createTestMatrixCell();
        	mc1.Matrix_BI__c = ma1.Id;
        	insert mc1;

        	Cycle_Data_BI__c cd = new Cycle_Data_BI__c();
        cd.Product_Catalog_BI__c = p2.Id;
        	cd.Account_BI__c = acc.Id;
        	cd.Cycle_BI__c = cycle.Id;
        	cd.Potential_BI__c = 12;
        	cd.Intimacy_BI__c = 12;
        	cd.Matrix_Cell_1_BI__c = mc.Id;
        	cd.Matrix_Cell_2_BI__c = mc1.Id;
        	cd.Current_Scenario_BI__c = 2;
        	//cd.UniqueKey_BI__c = 'aaaaaaaaaaaefewf34gerwgbg34sss';
        	insert cd;

        	Cycle_Data_BI__c cd1 = new Cycle_Data_BI__c();
        	cd1.Product_Catalog_BI__c = p2.Id;
        cd1.Account_BI__c = acc1.Id;
        	cd1.Cycle_BI__c = cycle.Id;
        	cd1.Matrix_Cell_1_BI__c = mc.Id;
        	cd1.Matrix_Cell_2_BI__c = mc1.Id;
        	cd1.Current_Scenario_BI__c = 2;
        	//cd1.UniqueKey_BI__c = 'bbbbbbbbbb454h56u76idfbsttj';
        insert cd1;

        	Lifecycle_Template_BI__c mt = new Lifecycle_Template_BI__c();
        	mt.Name = 'mt';
        	mt.Country_BI__c = c.Id;
        	mt.Active_BI__c = true;
        insert mt;

        	Customer_Attribute_BI__c sp = new Customer_Attribute_BI__c();
      	sp.Country_BI__c = c.Id;
      	sp.Group_txt_BI__c = 'ENT';
      	sp.Name = 'SP-Test';
      	insert sp;

      	Channel_BI__c channel = new Channel_BI__c();
      	channel.Name = 'Face to Face';
      	channel.Cost_Rate_BI__c = 11;
      	channel.Unit_BI__c = 'asdfasfd';
      	insert channel;

      	Matrix_Template_BI__c matrixTemplate = new Matrix_Template_BI__c(Name = 'M-Template Testing',Country_BI__c=c.Id,Lifecycle_Template_BI__c=mt.Id,
        	Specialties_BI__c=sp.Name,Specialty_Ids_BI__c=sp.Id,Product_Catalog_BI__c=p2.Id);
      	insert matrixTemplate;

        	test.startTest();
        	PageReference pageRef = Page.IMP_BI_ExtManageScenarios;
        	Test.setCurrentPage(pageRef);
        	ApexPages.currentPage().getParameters().put('Id',ma.Id);
        	ApexPages.currentPage().getParameters().put('cId',cycle.Id);


        	ApexPages.StandardController ctrl = new ApexPages.StandardController(ma);
        	IMP_BI_ExtManageScenarios ext = new IMP_BI_ExtManageScenarios(ctrl);


		list<Specialty_Grouping_BI__c> list_specialtiesGroup = ext.list_specialtiesGroup;
		list<Matrix_BI__c> list_matrix2Edit = ext.list_matrix2Edit;
		//Matrix_BI__c matrixDescription = ext.matrixDescription;

    		system.assert(true);
    		Test.stopTest();
    }
}