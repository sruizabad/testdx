/**
 *  Scenario Management Enhancements(Edit)
 *
 @author    Hely
 @created   2015-02-02
 @version   1.0
 @since     27.0 (Force.com ApiVersion)
 *
 @changelog
 * 2015-02-02 Hely <hely.lin@itbconsult.com>
 * - Created
 */
public class IMP_BI_ExtMatrixEditSwitch {
	
	
	private Matrix_BI__c matrix;
	
	public IMP_BI_ExtMatrixEditSwitch(ApexPages.standardController stdCtrl){
		Id matrixId = stdCtrl.getId();
		if(matrixId != null){
			matrix = [select Id, Scenario_BI__c, Account_Matrix_BI__c from Matrix_BI__c where Id = :matrixId];
		}
	}
	
	public PageReference editSwitch(){
		set<String> set_scenario = new set<String>{'1', '2', '3', '4', '5'};
		String urlParam = '';
		String scenario = matrix.Scenario_BI__c;
		if(matrix.Account_Matrix_BI__c || !set_scenario.contains(scenario)){
			urlParam = '/apex/IMP_BI_ExtMatrixDefinition?id=' + matrix.Id;
		}else{
			if(scenario == '1'){
				List<Matrix_BI__c> list_matrix =[select Id from Matrix_BI__c where First_Scenario_BI__c = :matrix.Id limit 1];
				if(list_matrix.size() > 0){
					urlParam = '/apex/IMP_BI_ExtManageScenarios?id=' + matrix.Id;
				}else{
					urlParam = '/apex/IMP_BI_ExtMatrixDefinition?id=' + matrix.Id;
				}
			}else if(scenario == '2' || scenario == '3' || scenario == '4' || scenario == '5'){
				urlParam = '/apex/IMP_BI_ExtManageScenarios?id=' + matrix.Id;
			}
		}
		
		PageReference page =  new PageReference(urlParam);
		page.setRedirect(true);
		return page; 
	}
	
}