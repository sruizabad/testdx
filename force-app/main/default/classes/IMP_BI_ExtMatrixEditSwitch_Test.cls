/** 
 *
 @author    Hely
 @created   2015-02-03
 @version   1.0
 @since     27.0 (Force.com ApiVersion)
 *
 @changelog
 * 2015-02-03 Hely <hely.lin@itbconsult.com>
 * - Created
 */
@isTest
private class IMP_BI_ExtMatrixEditSwitch_Test {
	 
    static testMethod void myUnitTest() {
    	
    	Matrix_BI__c matrix = new Matrix_BI__c();
    	matrix.Name_BI__c = 'testMatrix';
    	insert matrix;
    	
    	test.startTest();
    	ApexPages.StandardController ctrl = new ApexPages.StandardController(matrix);
    	IMP_BI_ExtMatrixEditSwitch emes = new IMP_BI_ExtMatrixEditSwitch(ctrl);
    	emes.editSwitch();
    	test.stopTest();
    	
    }
}