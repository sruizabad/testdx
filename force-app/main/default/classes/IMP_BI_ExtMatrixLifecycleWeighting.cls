/**
 *	Controller class for vf page IMP_BI_ExtMatrixLifecycleWeighting
 *
 @author  Peng Zhu
 @created 2015-02-04
 @version 1.0
 @since   30.0 (Force.com ApiVersion)
 *
 @changelog
 * 2015-02-04 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 */
public with sharing class IMP_BI_ExtMatrixLifecycleWeighting {
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=BEGIN public members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	public string message {get; set;}
	public string msgType {get; set;}
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=END public members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=BEGIN private members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    /**
	 * map to store global describtion due to limit on describe methodes.
	 */
	//private map<String, String> map_urlParams;
	
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -=END private members=- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	/////////////////////////////////// -=BEGIN CONSTRUCTOR=- /////////////////////////////////////
   /**
	* The contsructor
	*
	@author  Peng Zhu
	@created 2015-02-04
	@version 1.0
	@since   30.0 (Force.com ApiVersion)
	*
	@changelog
	* 2015-02-04 Peng Zhu <peng.zhu@itbconsult.com>
	* - Created
	*/
    public IMP_BI_ExtMatrixLifecycleWeighting(ApexPages.standardController stdCtrl) {
    	
    }
	/////////////////////////////////// -=END CONSTRUCTOR=- ///////////////////////////////////////
    
    
    //********************************* -=BEGIN public methods=- **********************************
    
    /**
	* Show message
	*
	@author  Peng Zhu
	@created 2015-02-04
	@version 1.0
	@since   30.0 (Force.com ApiVersion)
	*
	@changelog
	* 2015-02-04 Peng Zhu <peng.zhu@itbconsult.com>
	* - Created
	*/
    public void showMessage(){
	    	if(message != NULL && message.trim() != '') {
		    	if('CONFIRM'.equalsIgnoreCase(msgType)) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, message));
		    	}
		    	else if('FATAL'.equalsIgnoreCase(msgType)) {
		    	
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, message));
		    	}
		    	else if('INFO'.equalsIgnoreCase(msgType)) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, message));
		    	}
		    	else if('WARNING'.equalsIgnoreCase(msgType)) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, message));
		    	}
		    	else {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
		    	}
	    	}
    }
    
        
   /**
	* This method is used to fetch information for matrix, matrix cell
	*
	@author  Peng Zhu
	@created 2015-02-04
	@version 1.0
	@since   30.0 (Force.com ApiVersion)
	*
	@param 	 input		will be string type of matrix id
	*
	@return  string value of response
	*
	@changelog
	* 2015-02-05 Hely <hely.lin@itbconsult.com>
	* - changed
	*/    
    @TestVisible
    @RemoteAction
    public static String fetchMatrixInfoByMatrixId(String input) {
	    	ClsResponse cr = new ClsResponse();
	    	map<Id,Decimal> map_mcId_quantity = new map<Id,Decimal>();
	    	try {
	    		Id matrixId = Id.valueOf(input);
	    		for(AggregateResult aResult: [select c.Matrix_Cell_BI__c
	    											,sum(c.Quantity_BI__c) quantity
													from Matrix_Cell_Detail_BI__c c 
													where Matrix_Cell_BI__r.Matrix_BI__c = :matrixId and Channel_BI__c = 'Face to Face' AND Quantity_BI__c != NULL AND Quantity_BI__c != 0 
													group by c.Matrix_Cell_BI__c]){
	    			map_mcId_quantity.put((Id)aResult.get('Matrix_Cell_BI__c'),(Decimal)aResult.get('quantity'));
	    		}
	    		
	    		//Begin: Modified by Hely 2015-03-02 -- Used to obtain the Name field RecordType object.
	    		List<Matrix_BI__c> list_matrix = [SELECT Id
	    										, Column_BI__c
	    										, Row_BI__c
	    										, Adoption_Factor_Numbers_BI__c
	    										, Potential_Factor_Numbers_BI__c
	    										, Apply_Minimum_Threshold_BI__c
	    										, RecordTypeId
	    										, (SELECT Id, Row_BI__c, Column_BI__c, Total_Customers_BI__c, Number_of_Doctors_BI__c, Segment_BI__c, Total_Potential_BI__c, Total_Intimacy_BI__c, Strategic_Weight_BI__c FROM Matrix_Cells__r)   										
	    									  FROM Matrix_BI__c WHERE Id = :matrixId];
	    		Id recordTypeId = list_matrix[0].RecordTypeId;
	    		List<RecordType> list_recordType = [SELECT Id,Name FROM RecordType where Id = :recordTypeId];
	    		//End: Modified by Hely 2015-03-02 -- Used to obtain the Name field RecordType object.
	    		
	    		for(Matrix_BI__c matrix : list_matrix) {
	    			// Begin : for loop
	    			ClsMatrix cm = new ClsMatrix(matrix.Id);
	    			cm.mRow = matrix.Row_BI__c;
	    			cm.mColumn = matrix.Column_BI__c;
	    			cm.mAdoptionFactorNumbers = matrix.Adoption_Factor_Numbers_BI__c;
	    			cm.mPotentialFactorNumbers = matrix.Potential_Factor_Numbers_BI__c;
	    			cm.isMinimumThreshold = matrix.Apply_Minimum_Threshold_BI__c;
	    			
	    			for(Matrix_Cell_BI__c mc : matrix.Matrix_Cells__r) {
	    				ClsMatrixCell cmc = new ClsMatrixCell(mc.Id);
	    				//Begin: Modified by Hely 2015-03-02 -- When RecordType for HCP, cmc.numberOfDoctor's value Total_Customers_BI__c, otherwise Number_of_Doctors_BI__c
	    				if(list_recordType[0].Name.equals('HCP'))
	    					cmc.numberOfDoctor = mc.Total_Customers_BI__c;
	    				else
	    					cmc.numberOfDoctor = mc.Number_of_Doctors_BI__c;
	    				//End: Modified by Hely 2015-03-02 -- When RecordType for HCP, cmc.numberOfDoctor's value Total_Customers_BI__c, otherwise Number_of_Doctors_BI__c
	    				cmc.mcRow = mc.Row_BI__c;
	    				cmc.mcColumn = mc.Column_BI__c;
	    				cmc.totalAdoption = mc.Total_Intimacy_BI__c;
	    				cmc.totalPotential = mc.Total_Potential_BI__c;
	    				cmc.strategicWeight = mc.Strategic_Weight_BI__c;
	    				cmc.quantity = map_mcId_quantity.get(mc.Id);
	    				cmc.segment = mc.Segment_BI__c;
	    				
	    				cm.list_cmc.add(cmc);
	    			}
	    			
	    			cm.list_cmc.sort();
	    			cr.result = JSON.serialize(cm);
	    		}
	    		
	    	}
	    	catch(Exception e) {
	    		cr.status = 'ERROR';
	    		cr.result = e.getMessage();
	    	}
	    	
	    	return JSON.serialize(cr);
    }
    
    /**
	* This method is used to modify Matrix,MatrixCell and add LifecycleTemplate
	*
	@author  Peng Zhu
	@created 2015-02-05
	@version 1.0
	@since   30.0 (Force.com ApiVersion)
	*
	@param 	 matrixInfo		to add or change information, json format
	@param 	 saveType		used to determine how to save
	*
	@return  string value of response
	*
	@changelog
	* 2015-02-06 Hely <hely.lin@itbconsult.com>
	* - changed
	*/ 
    @RemoteAction
    public static String saveMatrixInfo(String matrixInfo, String saveType) {
	    	ClsResponse cr = new ClsResponse();
	    	try {
	    		if(String.isBlank(saveType)) saveType = 'matrix'; 
	    		List<Matrix_BI__c> list_matrix = new List<Matrix_BI__c>();
	    		List<Matrix_Cell_BI__c> list_matrixCell = new List<Matrix_Cell_BI__c>();
	    		ClsMatrix clsMatrix = (ClsMatrix)JSON.deserialize(matrixInfo, IMP_BI_ExtMatrixLifecycleWeighting.ClsMatrix.class);
	    		List<Matrix_BI__c> list_queryMatrix = [SELECT Id
															, Column_BI__c
															, Row_BI__c
															, Adoption_Factor_Numbers_BI__c
															, Potential_Factor_Numbers_BI__c
															, Lifecycle_Template_BI__c
															, Cycle_BI__c
															, Country_Code_BI__c
															, (SELECT Id, Row_BI__c, Column_BI__c, Number_of_Doctors_BI__c, Total_Potential_BI__c, Total_Intimacy_BI__c, Strategic_Weight_BI__c FROM Matrix_Cells__r)   										
														  FROM Matrix_BI__c WHERE Id = :clsMatrix.mId];
				
				Id cycleId = list_queryMatrix[0].Cycle_BI__c;
				
				List<Cycle_BI__c> list_cycle = [SELECT Country_Lkp_BI__c FROM Cycle_BI__c where Id = :cycleId];
				
	    		Id countryId = list_cycle[0].Country_Lkp_BI__c;
	    		
	    		for(Matrix_BI__c m : list_queryMatrix){
					// Begin : for loop
	    			if(saveType.equals('lifecyle')){
	    				Lifecycle_Template_BI__c lcTemplate = new Lifecycle_Template_BI__c();
	    				lcTemplate.Type_BI__c = 'New';
	    				lcTemplate.Name = clsMatrix.lifecycleTempalteName;
	    				lcTemplate.Country_BI__c = countryId;
	    				lcTemplate.Country_Code_BI__c = m.Country_Code_BI__c;
	    				lcTemplate.Active_BI__c = true;
	    				lcTemplate.Adoption_Factor_Numbers_BI__c = clsMatrix.mAdoptionFactorNumbers!=null ? clsMatrix.mAdoptionFactorNumbers : '0';
	    				lcTemplate.Potential_Factor_Numbers_BI__c = clsMatrix.mPotentialFactorNumbers!=null ? clsMatrix.mPotentialFactorNumbers : '0';
	    				lcTemplate.Row_BI__c = list_queryMatrix[0].Row_BI__c;
	    				lcTemplate.Column_BI__c = list_queryMatrix[0].Column_BI__c;
	    				insert lcTemplate;
	    				m.Lifecycle_Template_BI__c = lcTemplate.Id;
	    			}
	    			m.Adoption_Factor_Numbers_BI__c = clsMatrix.mAdoptionFactorNumbers;
	    			m.Potential_Factor_Numbers_BI__c = clsMatrix.mPotentialFactorNumbers;
	    			list_matrix.add(m);
	    			
	    			map<Id,ClsMatrixCell> map_mcId_clsMatrixCell = new map<Id,ClsMatrixCell>();
	    			//Begin : for get all ClsMatrixCell under current ClsMatrix
	    			for(ClsMatrixCell clsMCell : clsMatrix.list_cmc){
	    				map_mcId_clsMatrixCell.put(clsMCell.mcId,clsMCell);
	    			}
	    			//End : for get all ClsMatrixCell under current ClsMatrix
	    			//system.debug(m.Matrix_Cells__r);
	    			//Begin : for update Matrix_Cell_BI__c list
	    			
	    			for(Matrix_Cell_BI__c mc : m.Matrix_Cells__r) {
	    				ClsMatrixCell cmc = map_mcId_clsMatrixCell.get(mc.Id);
	    				mc.Strategic_Weight_BI__c = cmc.strategicWeight;
	    				list_matrixCell.add(mc);
	    			}
	    			//End : for update Matrix_Cell_BI__c list
	    			
	    		}
	    		
	    		update list_matrixCell;
	    		update list_matrix;
	    		cr.status = 'SUCCESS';
	    	}
	    	catch(Exception e) {
	    		cr.status = 'ERROR';
	    		cr.result = e.getMessage()+ '['+e.getLineNumber()+']';    		
	    	}
	    	return JSON.serialize(cr);
    }
    
    
    /**
    *	This method is used to calculate strategicWeight
    *
	@author  Hely
	@created 2015-02-09
	@version 1.0
	@since   30.0 (Force.com ApiVersion)
	*
	@param 	 calculateInfo		Data needs to be calculated
	@param 	 calculateType		Algorithms used to distinguish
	*
	@return  decimal value of strategicWeight
	*
	@changelog
	* 2015-02-09 Hely <hely.lin@itbconsult.com>
	* - created
	*/ 
	/*
    @RemoteAction
    public static decimal calculateStrategicWeight(String calculateInfo, String calculateType) {
    	decimal StrategicWeight = 0;
		ClsCalculate clsCalculate = (ClsCalculate)JSON.deserialize(calculateInfo, IMP_BI_ExtMatrixLifecycleWeighting.ClsCalculate.class);
    	if(calculateType == 'lifecycle'){
    		decimal potential_weight = 0, adoption_weight = 0,mcRow = 0, mcColumn = 0, lPotentialWeightFactor = 0, lAdoptionWeightFactor = 0;
    		
    		potential_weight = (100.0/clsCalculate.mRow).setScale(2);
			if(clsCalculate.mColumn > 1) adoption_weight =  (100.0/(clsCalculate.mColumn - 1)).setScale(2);
			
    		mcRow = clsCalculate.mcRow;
    		mcColumn = clsCalculate.mcColumn;
    		lPotentialWeightFactor = clsCalculate.lPotentialWeightFactor;
    		lAdoptionWeightFactor = clsCalculate.lAdoptionWeightFactor;
    		StrategicWeight = lPotentialWeightFactor*potential_weight*mcRow + lAdoptionWeightFactor*adoption_weight*mcColumn;
    	}else{
    		if(clsCalculate.lAdoptionFactorNumbers!=null && clsCalculate.lAdoptionFactorNumbers!='' && clsCalculate.lPotentialFactorNumbers!=null && clsCalculate.lPotentialFactorNumbers!=''){
				decimal lAdoptionFactorNumbers = decimal.valueOf(clsCalculate.lAdoptionFactorNumbers);
				decimal lPotentialFactorNumbers = decimal.valueOf(clsCalculate.lPotentialFactorNumbers);
	    		StrategicWeight = lAdoptionFactorNumbers*lPotentialFactorNumbers;
    		}
    	}
    	return StrategicWeight;
    }
    */
    //********************************* -=END public methods=- ************************************
    
    
    //********************************* -=BEGIN private methods=- *********************************
    //********************************* -=END private methods=- ***********************************
    
    
    //********************************* -=BEGIN help functions=- **********************************
    //********************************* -=END help functions=- ************************************
    
    //********************************* -=BEGIN inner classes=- ***********************************
    ///*>>>WrapperClass*/
    public class ClsMatrix {
	    	Id mId;
	    	Decimal mRow;
	    	Decimal mColumn;
	    	String mAdoptionFactorNumbers;
	    	String mPotentialFactorNumbers;
	    	String lifecycleTempalteName;
	    	boolean isMinimumThreshold;
	    	list<ClsMatrixCell> list_cmc;
	    	
	    	public ClsMatrix() {
	    		this.mRow = 0;
	    		this.mColumn = 0;
	    		this.mAdoptionFactorNumbers = '';
	    		this.mPotentialFactorNumbers = '';
	    		this.lifecycleTempalteName = '';
	    		this.list_cmc = new list<ClsMatrixCell>();
	    	}
	    	
	    	public ClsMatrix(Id mtrixId) {
	    		this.mId = mtrixId;
	    		this.mRow = 0;
	    		this.mColumn = 0;
	    		this.mAdoptionFactorNumbers = '';
	    		this.mPotentialFactorNumbers = '';
	    		this.lifecycleTempalteName = '';
	    		this.list_cmc = new list<ClsMatrixCell>();
	    	}
    }
    
    public class ClsMatrixCell implements Comparable {
	    	Id mcId;
	    	Decimal mcRow;
	    	Decimal mcColumn;
	    	Decimal numberOfDoctor;
	    	Decimal quantity;
	    	Decimal strategicWeight;
	    	Decimal totalAdoption;
	    	Decimal totalPotential;
	    	String segment;
	    	
	    	public ClsMatrixCell() {
	    		this.mcRow = 0;
	    		this.mcColumn =0;
	    		this.numberOfDoctor = 0;
	    		this.quantity = 0;
	    		this.strategicWeight = 0;
	    		this.totalAdoption = 0;
	    		this.totalPotential = 0;
	    		this.segment = 'Blank';
	    	}
	    	
	    	public ClsMatrixCell(Id mcId) {
	    		this.mcId = mcId;
	    		this.mcRow = 0;
	    		this.mcColumn =0;
	    		this.numberOfDoctor = 0;
	    		this.quantity = 0;
	    		this.strategicWeight = 0;
	    		this.totalAdoption = 0;
	    		this.totalPotential = 0;
	    		this.segment = 'Blank';
	    	}
    	
		public Integer compareTo(Object compareTo) {
	        ClsMatrixCell cmc = (ClsMatrixCell) compareTo;
	        
	        if (this.mcRow == cmc.mcRow) {
	        	if (this.mcColumn == cmc.mcColumn) return 0;
				else if (this.mcColumn > cmc.mcColumn) return 1;
	        	else return -1;	        	
	        }
	        else if(this.mcRow > cmc.mcRow) return -1;
	        else return 1;
	    }     	
    }
    
    public class ClsCalculate{
	    	Decimal mRow;
	    	Decimal mColumn;
	    	Decimal mcRow;
	    	Decimal mcColumn;
	    	Decimal lPotentialWeightFactor;
	    	Decimal lAdoptionWeightFactor;
	    	String lAdoptionFactorNumbers;
	    	String lPotentialFactorNumbers;
		
		public ClsCalculate(){
			this.mRow = 0;
			this.mColumn = 0;
			this.mcRow = 0;
			this.mcColumn = 0;
			this.lPotentialWeightFactor = 0;
			this.lAdoptionWeightFactor = 0;
			this.lAdoptionFactorNumbers = '';
			this.lPotentialFactorNumbers = '';
		}
		
    	
    }
    
    public class ClsResponse {
	    	String status; // SUCCESS, ERROR
	    	String result; 
	    	
	    	public ClsResponse() {
	    		status = 'SUCCESS';
	    		result = '';
	    	}
    }
	///*<<<WrapperClass*/
    //********************************* -=END inner classes=- *************************************
}