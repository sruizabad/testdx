/**
*   Test class for class IMP_BI_ExtMatrixLifecycleWeighting.
*
@author Peng Zhu
@created 2015-02-09
@version 1.0
@since 30.0 (Force.com ApiVersion)
*
@changelog
* 2015-02-09 Hely <hely.lin@itbconsult.com>
* - Changed
*- Test coverage 85%
*/
@isTest
private class IMP_BI_ExtMatrixLifecycleWeighting_Test {

    static testMethod void methodsTest() {
        
        
        Lifecycle_Template_BI__c lcTemplate = new Lifecycle_Template_BI__c();
		lcTemplate.Name = 'testLT';
		lcTemplate.Country_Code_BI__c = '12';
		lcTemplate.Active_BI__c = true;
		lcTemplate.Adoption_Factor_Numbers_BI__c = '';
		lcTemplate.Potential_Factor_Numbers_BI__c = '';
		insert lcTemplate;
        
        Matrix_BI__c matrix = new Matrix_BI__c();
        matrix.Name_BI__c = 'testM';
        matrix.Row_BI__c = 10;
        matrix.Column_BI__c = 10;
        matrix.Lifecycle_Template_BI__c = lcTemplate.Id;
        insert matrix;
        
        Matrix_Cell_BI__c matrixCell = new Matrix_Cell_BI__c();
        //matrixCell.Name = 'testMC';
        matrixCell.Matrix_BI__c = matrix.Id;
        matrixCell.Column_BI__c = 1;
        matrixCell.Row_BI__c = 2;
        insert matrixCell;
        
        Matrix_Cell_BI__c matrixCell2 = new Matrix_Cell_BI__c();
        matrixCell2.Matrix_BI__c = matrix.Id;
        matrixCell2.Column_BI__c = 2;
        matrixCell2.Row_BI__c = 2;
        insert matrixCell2;
        
        Matrix_Cell_Detail_BI__c matrixCellDetail = new Matrix_Cell_Detail_BI__c();
        matrixCellDetail.Name = 'testMCD';
        matrixCellDetail.Channel_BI__c = 'Face to Face';
        matrixCellDetail.Quantity_BI__c = 10;
        matrixCellDetail.Matrix_Cell_BI__c = matrixCell.Id;
        insert matrixCellDetail;
        
        Matrix_Cell_Detail_BI__c matrixCellDetail2 = new Matrix_Cell_Detail_BI__c();
        matrixCellDetail2.Name = 'testMCD2';
        matrixCellDetail2.Channel_BI__c = 'Face to Face';
        matrixCellDetail2.Quantity_BI__c = 10;
        matrixCellDetail2.Matrix_Cell_BI__c = matrixCell2.Id;
        insert matrixCellDetail2;
        
        
        Test.startTest();
        ApexPages.StandardController ctrl = new ApexPages.StandardController(matrix); 
        IMP_BI_ExtMatrixLifecycleWeighting emlw = new IMP_BI_ExtMatrixLifecycleWeighting(ctrl);
        IMP_BI_ExtMatrixLifecycleWeighting.fetchMatrixInfoByMatrixId(matrix.Id);
        
        String matrixInfo = '{"mId":"a35J00000009FE7","mRow":"10","mColumn":"10","mAdoptionFactorNumbers":"3","mPotentialFactorNumbers":"5","list_cmc":[{"mcId":"a3CJ00000005gAy","mcRow":"5","mcColumn":"5","numOfDoctor":"3","quantity":"9","strategicWeight":"60","totalAdoption":"8","totalPotential":"6"},{"mcId":"a3CJ00000005gAy","mcRow":"5","mcColumn":"5","numOfDoctor":"3","quantity":"9","strategicWeight":"60","totalAdoption":"8","totalPotential":"6"}]}';
        IMP_BI_ExtMatrixLifecycleWeighting.saveMatrixInfo(matrixInfo,null);
        IMP_BI_ExtMatrixLifecycleWeighting.saveMatrixInfo(matrixInfo,'lifecyle');
        
        emlw.message = 'messageInfo';
        emlw.msgType = 'CONFIRM';
        emlw.showMessage();
        emlw.msgType = 'FATAL';
        emlw.showMessage();
        emlw.msgType = 'INFO';
        emlw.showMessage();
        emlw.msgType = 'WARNING';
        emlw.showMessage();
        
        IMP_BI_ExtMatrixLifecycleWeighting.ClsMatrix clsMatrix = new IMP_BI_ExtMatrixLifecycleWeighting.ClsMatrix();
        IMP_BI_ExtMatrixLifecycleWeighting.ClsMatrixCell clsMatrixCell = new IMP_BI_ExtMatrixLifecycleWeighting.ClsMatrixCell();
        IMP_BI_ExtMatrixLifecycleWeighting.ClsCalculate clsCalculate = new IMP_BI_ExtMatrixLifecycleWeighting.ClsCalculate();
        IMP_BI_ExtMatrixLifecycleWeighting.ClsResponse clsResponse = new IMP_BI_ExtMatrixLifecycleWeighting.ClsResponse();
        
        system.assert(true);
        Test.stopTest();
        
    }
    
}