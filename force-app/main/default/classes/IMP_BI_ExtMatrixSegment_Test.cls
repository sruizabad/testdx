/**
*   Test class for class IMP_BI_ExtMatrixSegment.
*
@author Di Chen
@created 2013-05-28
@version 1.0
@since 20.0
*
@changelog
* 2013-05-28 Di Chen <di.chen@itbconsult.com>
* - Created
*- Test coverage 85%
*/
@isTest
private class IMP_BI_ExtMatrixSegment_Test {
	static testMethod void testMostMethods() {
    	Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
    	insert c;

        Account acc = IMP_BI_ClsTestHelp.createTestAccount();
        acc.Name = '123e';
        insert acc;

        Cycle_BI__c cycle = IMP_BI_ClsTestHelp.createTestCycle();
        cycle.Country_Lkp_BI__c = c.Id;
        insert cycle;

        Product_vod__c p2 = IMP_BI_ClsTestHelp.createTestProduct();
        p2.Country_BI__c = c.Id;
        insert p2;

        Lifecycle_Template_BI__c mt = IMP_BI_ClsTestHelp.createTestLifecycleTemplateBI();
        mt.Name = 'mt';
        mt.Country_BI__c = c.Id;
        mt.Active_BI__c = true;
        mt.isLaunch_Phase_BI__c = true;
        mt.Column_BI__c = 6;
        insert mt;

        Matrix_BI__c ma = IMP_BI_ClsTestHelp.createTestMatrix();
        ma.Cycle_BI__c = cycle.Id;
        ma.Product_Catalog_BI__c = p2.Id;
        ma.Intimacy_Levels_BI__c = 11;
        ma.Potential_Levels_BI__c = 10;
        ma.Size_BI__c = '10x11';
        ma.Row_BI__c = 10;
        ma.Column_BI__c = 11;
        ma.Specialization_BI__c = 'GP';//Peng Zhu 2013-10-14
        ma.Lifecycle_Template_BI__c = mt.Id;
        ma.Scenario_BI__c = '1';
        insert ma;

        //Begin: 2015-04-27 Add by Hely <hely.lin@itbconsult.com>
        Matrix_Cell_BI__c mc = new Matrix_Cell_BI__c();
        mc.Matrix_BI__c = ma.Id;
        mc.Row_BI__c = 1;
        mc.Column_BI__c = 2;
        insert mc;

        Matrix_Cell_BI__c mc_2 = new Matrix_Cell_BI__c();
        mc_2.Matrix_BI__c = ma.Id;
        mc_2.Row_BI__c = 1;
        mc_2.Column_BI__c = 1;
        insert mc_2;
        //End: 2015-04-27 Add by Hely <hely.lin@itbconsult.com>

        Cycle_Data_BI__c cd = new Cycle_Data_BI__c();
        cd.Product_Catalog_BI__c = p2.Id;
        cd.Account_BI__c = acc.Id;
        cd.Cycle_BI__c = cycle.Id;
        cd.Potential_BI__c = 12;
        cd.Intimacy_BI__c = 12;
        cd.Matrix_Cell_BI__c = mc_2.Id;
        cd.Matrix_Cell_1_BI__c = mc_2.Id;
        cd.Matrix_BI__c = ma.Id;
        cd.Matrix_1_BI__c = ma.Id;
        cd.Row_BI__c = 1;
        cd.Column_BI__c = 1;
        insert cd;

        Test.startTest();

        ApexPages.StandardController ctrl = new ApexPages.StandardController(ma);
        IMP_BI_ExtMatrixSegment ext = new IMP_BI_ExtMatrixSegment(ctrl);

        ext.cycleDataId = 'a3BJ0000002Rwmn';
        ext.matrixCellIds = mc.Id+','+mc_2.Id;
        ext.matrixSelected = '';

        ext.matrix = ma;

        ext.getRows();
        ext.getColumns();
        ext.getChannels();
        ext.saveMatrix();
        ext.initPageHeightFields();
        ext.cancelMatrix();


        ext.getFilterCDData();
        ext.saveCD();
        ext.updateCycleData();
        ext.updateMatrixCellSelected();
        IMP_BI_ExtMatrixSegment.calculateKPI(ma.Id, null);
        IMP_BI_ExtMatrixSegment.ClsFinalResultConfig clsFinalResultConfig = new IMP_BI_ExtMatrixSegment.ClsFinalResultConfig();
        IMP_BI_ExtMatrixSegment.kpiClass kpiClass = new IMP_BI_ExtMatrixSegment.kpiClass();

        /**Testing threshold configuration*/
        IMP_BI_ExtMatrixSegment.ClsMatrixThreshold mThreshold = new IMP_BI_ExtMatrixSegment.ClsMatrixThreshold();

        //Matrix threshold info
        mThreshold.matrixId = ma.Id;
    	mThreshold.potentialThreshold = 1;
    	mThreshold.adoptionThreshold = 1;
    	mThreshold.map_matrixCell = JSON.serialize(ext.map_matrixCell);//Parse arrayList to JSON Object
    	mThreshold.maxThresholdPotential = 50;
    	mThreshold.maxThresholdAdoption = 50;
    	mThreshold.lastCDId = '';
    	IMP_BI_ExtMatrixSegment.applyThreshold(JSON.serialize(mThreshold));

    	//KPI Threshold fill info
    	map<Id, IMP_BI_ExtMatrixSegment.kpiClass> mapSummaryCells = new map<Id, IMP_BI_ExtMatrixSegment.kpiClass>();
    	mapSummaryCells.put(mc_2.Id,kpiClass);
    	ext.jsonMapSummaryCells = JSON.serialize(mapSummaryCells);
    	ext.action_updateMatrixCell();

    	system.assert(true);
        Test.stopTest();
    }
	static testMethod void testMostMethods2() {
    	Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
    	insert c;

        Account acc = IMP_BI_ClsTestHelp.createTestAccount();
        acc.Name = '123e';
        insert acc;

        Cycle_BI__c cycle = IMP_BI_ClsTestHelp.createTestCycle();
        cycle.Country_Lkp_BI__c = c.Id;
        insert cycle;

        Product_vod__c p2 = IMP_BI_ClsTestHelp.createTestProduct();
        p2.Country_BI__c = c.Id;
        insert p2;

        Lifecycle_Template_BI__c mt = IMP_BI_ClsTestHelp.createTestLifecycleTemplateBI();
        mt.Name = 'mt';
        mt.Country_BI__c = c.Id;
        mt.Active_BI__c = true;
        mt.isLaunch_Phase_BI__c = true;
        mt.Adoption_Status_01_BI__c = '1';
        mt.Adoption_Status_02_BI__c = '1';
        mt.Adoption_Status_03_BI__c = '1';
        mt.Adoption_Status_04_BI__c = '1';
        mt.Adoption_Status_05_BI__c = '1';
        mt.Column_BI__c = 6;
        insert mt;

        Matrix_BI__c ma = IMP_BI_ClsTestHelp.createTestMatrix();
        ma.Cycle_BI__c = cycle.Id;
        ma.Product_Catalog_BI__c = p2.Id;
        ma.Intimacy_Levels_BI__c = 11;
        ma.Potential_Levels_BI__c = 10;
        ma.Size_BI__c = '10x11';
        ma.Row_BI__c = 10;
        ma.Column_BI__c = 11;
        ma.Specialization_BI__c = 'GP';//Peng Zhu 2013-10-14
        ma.Lifecycle_Template_BI__c = mt.Id;
        insert ma;

        Test.startTest();

        ApexPages.StandardController ctrl = new ApexPages.StandardController(ma);
        IMP_BI_ExtMatrixSegment ext = new IMP_BI_ExtMatrixSegment(ctrl);

        ext.matrix = ma;

        ext.getRows();
        ext.getColumns();
        ext.getChannels();
        ext.saveMatrix();
        ext.initPageHeightFields();
        ext.cancelMatrix();

        ext.getFilterCDData();
        ext.saveCD();
        ext.updateCycleData();
        ext.updateMatrixCellSelected();
        IMP_BI_ExtMatrixSegment.calculateKPI(ma.Id, null);
        IMP_BI_ExtMatrixSegment.ClsFinalResultConfig clsFinalResultConfig = new IMP_BI_ExtMatrixSegment.ClsFinalResultConfig();
        IMP_BI_ExtMatrixSegment.kpiClass kpiClass = new IMP_BI_ExtMatrixSegment.kpiClass();

        system.assert(true);
        Test.stopTest();
    }

	static testMethod void testMostMethods3() {
    	Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
    	c.Name = 'TC';
    	insert c;

        Account acc = IMP_BI_ClsTestHelp.createTestAccount();
        acc.Name = '123e';
        insert acc;

        Cycle_BI__c cycle = IMP_BI_ClsTestHelp.createTestCycle();
        cycle.Country_Lkp_BI__c = c.Id;
        insert cycle;

        Product_vod__c p2 = IMP_BI_ClsTestHelp.createTestProduct();
        p2.Country_BI__c = c.Id;
        insert p2;

        Lifecycle_Template_BI__c mt = IMP_BI_ClsTestHelp.createTestLifecycleTemplateBI();
        mt.Name = 'mt';
        mt.Country_BI__c = c.Id;
        mt.Active_BI__c = true;
        mt.isLaunch_Phase_BI__c = true;
        mt.Column_BI__c = 6;
        insert mt;

        Matrix_BI__c ma = IMP_BI_ClsTestHelp.createTestMatrix();
        ma.Cycle_BI__c = cycle.Id;
        ma.Product_Catalog_BI__c = p2.Id;
        ma.Intimacy_Levels_BI__c = 11;
        ma.Potential_Levels_BI__c = 10;
        ma.Size_BI__c = '10x11';
        ma.Row_BI__c = 10;
        ma.Column_BI__c = 11;
        ma.Specialization_BI__c = 'GP';//Peng Zhu 2013-10-14
        ma.Lifecycle_Template_BI__c = mt.Id;
        insert ma;

        ma = [Select Cycle_BI__c, Country_BI__c, Intimacy_Levels_BI__c, Name, Name_BI__c, Potential_Levels_BI__c, Channel_1_BI__c, Channel_2_BI__c, Channel_3_BI__c, Channel_4_BI__c, Channel_5_BI__c, Channel_6_BI__c, Channel_7_BI__c, Channel_8_BI__c, Channel_9_BI__c, Channel_10_BI__c,
                        Product_Catalog_BI__c, Product_Catalog_BI__r.Name, Id, Size_BI__c, Specialization_BI__c, Status_BI__c, Cycle_BI__r.Country_BI__c, Cycle_BI__r.Name, Cycle_BI__r.End_Date_BI__c, Cycle_BI__r.IsCurrent_BI__c, Cycle_BI__r.OwnerId, Cycle_BI__r.Start_Date_BI__c,
                        //Begin: added by Peng Zhu 2013-05-10 to add two new fields
                        Dimension_1_Name_BI__c, Dimension_2_Name_BI__c, Row_BI__c, Column_BI__c, Matrix_Description_BI__c, Cycle_BI__r.Country_Lkp_BI__r.Name,Cycle_BI__r.Country_Lkp_BI__r.Country_Code_BI__c,Potential_data_Label_BI__c,Adoption_Data_Label_BI__c,
                        Segment_1_Label_BI__c, Segment_2_Label_BI__c, Segment_3_Label_BI__c, Segment_4_Label_BI__c, Segment_5_Label_BI__c, Cycle_BI__r.Country_Lkp_BI__r.Matrix_Drill_Down_Report_ID_BI__c,
                        Lifecycle_Template_BI__c, Lifecycle_Template_BI__r.isLaunch_Phase_BI__c, Lifecycle_Template_BI__r.Adoption_Status_01_BI__c, Lifecycle_Template_BI__r.Adoption_Status_02_BI__c,
                        Lifecycle_Template_BI__r.Adoption_Status_03_BI__c, Lifecycle_Template_BI__r.Adoption_Status_04_BI__c, Lifecycle_Template_BI__r.Adoption_Status_05_BI__c,
                        Lifecycle_Template_BI__r.Potential_Status_01_BI__c, Lifecycle_Template_BI__r.Potential_Status_02_BI__c, Lifecycle_Template_BI__r.Potential_Status_03_BI__c,
                        Lifecycle_Template_BI__r.Potential_Status_04_BI__c, Lifecycle_Template_BI__r.Potential_Status_05_BI__c,
                        //End: added by Peng Zhu
                        //Begin: added by Yuanyuan Zhang 2015-01-05
                        Current_BI__c, First_Scenario_BI__c, Scenario_BI__c, Account_Matrix_BI__c, Scenario_Description_BI__c, Account_Matrix_Type_BI__c,
                        //End
                        //Begin: added by Peng Zhu 2015-03-24 for 10 Number Info Field Label
                        Cycle_BI__r.Number_Info_Field_1__c,
                        Cycle_BI__r.Number_Info_Field_2__c,
                        Cycle_BI__r.Number_Info_Field_3__c,
                        Cycle_BI__r.Number_Info_Field_4__c,
                        Cycle_BI__r.Number_Info_Field_5__c,
                        Cycle_BI__r.Number_Info_Field_6_BI__c,
                        Cycle_BI__r.Number_Info_Field_7_BI__c,
                        Cycle_BI__r.Number_Info_Field_8_BI__c,
                        Cycle_BI__r.Number_Info_Field_9_BI__c,
                        Cycle_BI__r.Number_Info_Field_10_BI__c,
                        //End: added by Peng Zhu 2015-03-24 for 10 Number Info Field Label
                        Threshold_1_BI__c,Threshold_2_BI__c,Threshold_3_BI__c,Threshold_4_BI__c,Threshold_5_BI__c,Threshold_6_BI__c,Threshold_7_BI__c,Threshold_8_BI__c,Threshold_9_BI__c,Threshold_10_BI__c  FROM Matrix_BI__c WHERE Id =:ma.Id][0];

        //Begin: 2015-04-27 Add by Hely <hely.lin@itbconsult.com>
        Matrix_Cell_BI__c mc = new Matrix_Cell_BI__c();
        mc.Matrix_BI__c = ma.Id;
        mc.Row_BI__c = 3;
        mc.Column_BI__c = 2;
        insert mc;
        //End: 2015-04-27 Add by Hely <hely.lin@itbconsult.com>
        system.debug('##@@ Peng ma ' + ma.Country_BI__c);
		IMP_BI_Final_Result_Config__c csInst = IMP_BI_Final_Result_Config__c.getInstance(ma.Country_BI__c.trim());

		if(csInst == NULL) {
			csInst = new IMP_BI_Final_Result_Config__c();
			csInst.Name = ma.Country_BI__c.trim();
			csInst.Active_BI__c = true;
			insert csInst;
		}


		Test.startTest();

        ApexPages.StandardController ctrl = new ApexPages.StandardController(ma);
		IMP_BI_ExtMatrixSegment inst = new IMP_BI_ExtMatrixSegment(ctrl);

		inst.matrix = new Matrix_BI__c();

		inst.matrix.Intimacy_Levels_BI__c = 100.0;

		inst.initPageWidthFields();

		inst.matrix = new Matrix_BI__c();

		inst.matrix.Size_BI__c = '10x11';
		inst.matrix.Column_BI__c = 1.0;

		inst.initPageWidthFields();
		inst.initPageHeightFields();

		inst.matrix = new Matrix_BI__c();
		inst.initIsMatrixOlRendered();

		boolean isShow = inst.isMarketShareShow;

		system.assert(true);
		Test.stopTest();
	}
}