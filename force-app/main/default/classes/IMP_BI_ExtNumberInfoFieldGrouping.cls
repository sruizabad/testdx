/**
 *  extension class for page "IMP_BI_ExtNumberInfoFieldGrouping"
 *
 @author  Xiong Yao
 @created 2015-03-30
 @version 1.0
 @since   30.0 (Force.com ApiVersion)
 *
 @changelog
 * 2015-03-30 Xiong Yao <xiong.yao@itbconsult.com>
 * - Created 
 */
public with sharing class IMP_BI_ExtNumberInfoFieldGrouping {
    
    public String thousandSep {get; set;}
    public String decimalSep {get; set;}
    public Integer timeZoneOffset {get; set;} 
    public Id cycleId {get; set;}
    public ExtMatrixConfig emConfig{get;set;}
    
    public IMP_BI_ExtNumberInfoFieldGrouping(ApexPages.StandardController controller) {
        Decimal value = 1000.10;
        String formattedValue = value.format();
        thousandSep = formattedValue.substring(1,2);
        decimalSep = formattedValue.substring(5,6);
        timeZoneOffset = -UserInfo.getTimeZone().getOffset(Date.today());
        emConfig = new ExtMatrixConfig();
        cycleId = controller.getId();
    }
    
   /**
    * This Remote Action method is used to init the first information.
    *
    @author  Xiong Yao
    @created 2015-03-31
    @version 1.0
    @since   30.0 (Force.com ApiVersion)
    *
    @return  NumberInfoFG
    *
    @changelog
    * 2015-03-31 Xiong Yao <xiong.yao@itbconsult.com>
    * - Created
    */  
    @RemoteAction
    
    public static resMInfo saveListreqMInfo (IMP_BI_ClsBatch_NumberInfoFieldGrouping.resMInfoB MInfoB) {
        resMInfo res = new resMInfo();
        String queryCycleDateStr = 'SELECT ';
        
        for(Integer i = 1; i <= 25; i++ ) {
            if(i <= 5) {
                queryCycleDateStr += ' Text_Info_Field_' + i + '__c, ';
                queryCycleDateStr += ' Number_Info_Field_' + i + '__c, ';
            }else{
                if(i <= 10) {
                    queryCycleDateStr += ' Number_Info_Field_' + i + '_BI__c, ';
                }
                queryCycleDateStr += ' Text_Info_Field_' + i + '_BI__c, ';
            }
        }
        if(MInfoB.cId != null) {
            IMP_BI_ClsBatch_NumberInfoFieldGrouping ibbathc = new IMP_BI_ClsBatch_NumberInfoFieldGrouping();
            ibbathc.query = queryCycleDateStr;
            ibbathc.MInfoB = MInfoB;
            ibbathc.filter = ' Id FROM Cycle_Data_BI__c WHERE Cycle_BI__c =';
            if(!test.isRunningTest()){
                try{
                    Database.executeBatch(ibbathc);
                    res.message = 'execute batch success!';
                }catch(Exception e) {
                    res.isSuccess = false;
                    res.message = e.getMessage();
                }
            }
        }
        
        
        return res;
    }
    
   /**
    * This Remote Action method is used to init the first information.
    *
    @author  Xiong Yao
    @created 2015-03-31
    @version 1.0
    @since   30.0 (Force.com ApiVersion)
    *
    @return  NumberInfoFG
    *
    @changelog
    * 2015-03-31 Xiong Yao <xiong.yao@itbconsult.com>
    * - Created
    */  
    @RemoteAction
    public static initInfortionRespose initInfortion(initInfortionRequest req){
    	initInfortionRespose respose = new initInfortionRespose();
    	respose.cycleId = req.cycleId;
        NumberInfoFG ninfo = req.ninfo; //new NumberInfoFG();
    	if(req.ninfo == null){
    		ninfo = new NumberInfoFG();
    	}
        
        String queryCycleDateStr = 'SELECT ';
        
        String queryStr = 'SELECT ';
        
        for(Integer i = 1; i <= 25; i++ ) {
            if(i <= 5) {
                queryCycleDateStr += ' Text_Info_Field_' + i + '__c, ';
                queryCycleDateStr += ' Number_Info_Field_' + i + '__c, ';
                
                queryStr += ' Text_Info_Field_' + i + '__c, ';
                queryStr += ' Number_Info_Field_' + i + '__c, ';
            }else{
                if(i <= 10) {
                    queryCycleDateStr += ' Number_Info_Field_' + i + '_BI__c, ';
                    
                    queryStr += ' Number_Info_Field_' + i + '_BI__c, ';
                }
                queryCycleDateStr += ' Text_Info_Field_' + i + '_BI__c, ';
                
                queryStr += ' Text_Info_Field_' + i + '_BI__c, ';
            }
        }
        Id cycleId = req.cycleId;
        Id cycledateId = req.cycledateId;
        if(req.cycledateId == null){
        	queryCycleDateStr += ' Id FROM Cycle_Data_BI__c WHERE Cycle_BI__c =:cycleId order by Id  limit 20000';
        }else{
        	queryCycleDateStr += ' Id FROM Cycle_Data_BI__c WHERE Cycle_BI__c =:cycleId and Id >:cycledateId order by Id limit 20000';
        }
        queryStr += ' Text_Number_Combination_Value__c, Id FROM Cycle_BI__c WHERE Id =:cycleId';
        if(req.cycleId != null) {
        	Integer j = 1;
        	Map<String, MinMaxInfo> map_num_m = new Map<String, MinMaxInfo>();
            for(Cycle_Data_BI__c cdb : Database.query(queryCycleDateStr)) {
        		respose.islast = true;
        		++j;
            	if(j >= 20000) {
            		respose.cycledateId = cdb.Id;
            		respose.islast = false;
            	}
                for(Integer i = 1; i <= 10; i++ ) {
                    if(i <= 5) {
                        if(!map_num_m.containsKey('Number_Info_Field_' + i + '__c')) {
                            map_num_m.put('Number_Info_Field_' + i + '__c', new MinMaxInfo());
                        }
                        MinMaxInfo mmi = map_num_m.get('Number_Info_Field_' + i + '__c');
                        if(cdb.get('Number_Info_Field_' + i + '__c') != null){
                            Decimal ni = (Decimal)(cdb.get('Number_Info_Field_' + i + '__c'));
                            if(ni > mmi.maxNum || mmi.maxNum == null){
                                mmi.maxNum = ni;
                            }
                            if(ni < mmi.minNum || mmi.minNum == null){
                                mmi.minNum = ni;
                            }
	                        map_num_m.put('Number_Info_Field_' + i + '__c', mmi);
                        }
                    }else{
                        if(!map_num_m.containsKey('Number_Info_Field_' + i + '_BI__c')) {
                            map_num_m.put('Number_Info_Field_' + i + '_BI__c', new MinMaxInfo());
                        }
                        MinMaxInfo mmi = map_num_m.get('Number_Info_Field_' + i + '_BI__c');
                        if(cdb.get('Number_Info_Field_' + i + '_BI__c') != null){
                            Decimal ni = (Decimal)(cdb.get('Number_Info_Field_' + i + '_BI__c'));
                            if(ni > mmi.maxNum || mmi.maxNum == null){
                                mmi.maxNum = ni;
                            }
                            if(ni < mmi.minNum || mmi.minNum == null){
                                mmi.minNum = ni;
                            }
	                        map_num_m.put('Number_Info_Field_' + i + '_BI__c', mmi);
                        }
                    }
                }
            }
            for(String key : ninfo.map_num_m.keySet()) {
            	if(map_num_m.get(key) == null) {
            		map_num_m.put(key, ninfo.map_num_m.get(key));
            	}
            }
            ninfo.map_num_m = map_num_m;
            if(j == 1){
            	respose.islast = true;
            }
            
            if(req.ishaveCycle == false) {
	            for(Cycle_BI__c cb : Database.query(queryStr)) {
	            	respose.ishaveCycle = true;
	                if(cb.Text_Number_Combination_Value__c != null){
	                	Map<String, Map<String, String>> map_tf_map_nf_v = new Map<String, Map<String, String>>();
	                	for(String key : ninfo.map_tf_map_nf_v.keySet()) {
	                		map_tf_map_nf_v.put(key, ninfo.map_tf_map_nf_v.get(key));
	                	}
	                    ninfo.map_tf_map_nf_v = getTFmapNFV(map_tf_map_nf_v, cb.Text_Number_Combination_Value__c);
	                }
	                for(Integer i = 1; i <= 25; i++ ) {
	                    if(i <= 5) {
	                        //if(cb.get('Text_Info_Field_' + i + '__c') == null) {
	                            NIF tif = new NIF();
	                            tif.APIlabel = 'Text Info Field ' + i;
	                            tif.APIname = 'Text_Info_Field_' + i + '__c';
	                            ninfo.listtif.add(tif);
	                        //}
	                        if(cb.get('Number_Info_Field_' + i + '__c') != null) {
	                            NIF nif = new NIF();
	                            nif.APIlabel = 'Number Info Field ' + i;
	                            nif.APIname = 'Number_Info_Field_' + i + '__c';
	                            ninfo.listnif.add(nif);
	                        }
	                    }else{
	                        if(i <= 10) {
	                            if(cb.get('Number_Info_Field_' + i + '_BI__c') != null) {
	                                NIF nif = new NIF();
	                                nif.APIlabel = 'Number Info Field ' + i;
	                                nif.APIname = 'Number_Info_Field_' + i + '_BI__c';
	                                ninfo.listnif.add(nif);
	                            }
	                        }
	                        //if(cb.get('Text_Info_Field_' + i + '_BI__c') == null) {
	                            NIF tif = new NIF();
	                            tif.APIlabel = 'Text Info Field ' + i;
	                            tif.APIname = 'Text_Info_Field_' + i + '_BI__c';
	                            ninfo.listtif.add(tif);
	                        //}
	                    }
	                }
	            }
            }else{
            	respose.ishaveCycle = req.ishaveCycle;
            }
        }
        respose.ninfo = ninfo;
        return respose;
    }
    
    /**
    * This method is used to get TF map NF V
    *
    @author  Xiong Yao
    @created 2015-03-31
    @version 1.0
    @since   30.0 (Force.com ApiVersion)
    *
    @return  map_tf_map_nf_v
    *
    @changelog
    * 2015-03-31 Xiong Yao <xiong.yao@itbconsult.com>
    * - Created
    */  
    public static Map<String, Map<String, String>> getTFmapNFV(Map<String, Map<String, String>> map_tf_map_nf_v, String tncv) {
        
        for(String tnv : tncv.split(';') ) {
            if(!String.isEmpty(tnv)) {
                map<String,String> map_name_value = new map<String, String>();
                for(String fv: tnv.split(',')) {
                    if(fv != null) {
                        map_name_value.put(fv.split(':')[0], fv.split(':')[1]);
                    }
                }
                if(map_name_value.get('TF') != null && map_name_value.get('NF') != null && map_name_value.get('V') != null){
                    if(map_tf_map_nf_v.get(map_name_value.get('TF')) == null){
                        map_tf_map_nf_v.put(map_name_value.get('TF'), new Map<String, String>());
                    }
                    map_tf_map_nf_v.get(map_name_value.get('TF')).put(map_name_value.get('NF'), map_name_value.get('V'));
                }
            }
        }
        
        return map_tf_map_nf_v;
    }
    
    //********************************* -=BEGIN inner classes=- ***********************************
    ///*>>>WrapperClass*/
    public class NumberInfoFG{
        public List<NIF> listnif {get; set;}
        public List<NIF> listtif {get; set;}
        public Map<String, MinMaxInfo> map_num_m {get; set;}
        public Map<String, Map<String, String>> map_tf_map_nf_v {get; set;}
        public NumberInfoFG(){
            listnif = new List<NIF>();
            listtif = new List<NIF>();
            map_num_m = new Map<String, MinMaxInfo>();
            map_tf_map_nf_v = new Map<String, Map<String, String>>();
        }
    }
    
    public class initInfortionRequest{
    	public Id cycleId {get;set;}
    	public Id cycledateId {get;set;}
    	public NumberInfoFG ninfo{get;set;}
    	public Boolean ishaveCycle{get;set;}
    	
    	public initInfortionRequest() {
    		cycledateId = null;
    		ninfo = new NumberInfoFG();
    		ishaveCycle = false;
    	}
    }
    
    public class initInfortionRespose{
    	public Id cycleId{get;set;}
    	public Id cycledateId {get;set;}
    	public NumberInfoFG ninfo{get;set;}
    	public Boolean ishaveCycle{get;set;}
    	
    	public Boolean islast {get;set;}
    	
    	public initInfortionRespose() {
    		ishaveCycle = false;
    		ninfo = new NumberInfoFG();
    		islast = false;
    	}
    }
    
    public class MinMaxInfo{
        public Decimal minNum {get; set;}
        public Decimal maxNum {get; set;}
        public MinMaxInfo() {
            //minNum = 0;
            //maxNum = 0;
        }
    }
    public class reqMInfo {
        public Decimal f {get; set;}
        public Decimal t {get; set;}
        public String v {get; set;}
    }
    
    public class resMInfoB {
        public List<reqMInfo> listreqMInfo {get; set;}
        public Id cId {get; set;}
    }
    
    public class resMInfo {
        public Boolean isSuccess {get; set;}
        public String message {get; set;}
        public resMInfo () {
            isSuccess = true;
        }
    }
    
    public class NIF {
        public String APIname{get; set;}
        public String APIlabel{get; set;}
        public NIF(){
            APIname = '';
            APIlabel = '';
        }
    }
    
    public class ExtMatrixConfig{
        public String jsPath {get; set;}
        public String locale {get; set;}
        public ExtMatrixConfig(){
            this.locale= Userinfo.getLocale();
            try{
                List<String> list_s = locale.split('_');
                this.locale = list_s[0]+'-'+list_s[1];
            }catch(Exception e){
                this.locale = 'de-DE';
            } 
            this.jsPath = '/glob-cultures/cultures/globalize.culture.'+locale+'.js';
        }
    }
	///*<<<WrapperClass*/
    //********************************* -=END inner classes=- *************************************    
}