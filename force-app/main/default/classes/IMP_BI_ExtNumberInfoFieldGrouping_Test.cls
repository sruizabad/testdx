/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class IMP_BI_ExtNumberInfoFieldGrouping_Test {

    static testMethod void myUnitTest() {
        Cycle_BI__c cycle = IMP_BI_ClsTestHelp.createTestCycle();
        insert cycle;
        
        Account account = IMP_BI_ClsTestHelp.createTestAccount();
        insert account;
        
        Cycle_Data_BI__c cd =  IMP_BI_ClsTestHelp.createTestCycleData();
        cd.Number_Info_Field_1__c = 10;
        cd.Cycle_BI__c = cycle.Id;
        cd.Account_BI__c = account.Id;
        insert cd;
        ApexPages.StandardController ctrl = new ApexPages.StandardController(cycle); 
        IMP_BI_ExtNumberInfoFieldGrouping enif = new IMP_BI_ExtNumberInfoFieldGrouping(ctrl);
        IMP_BI_ExtNumberInfoFieldGrouping.initInfortionRequest iniInFoRequest = new IMP_BI_ExtNumberInfoFieldGrouping.initInfortionRequest();
        iniInFoRequest.cycleId = cycle.Id;
        IMP_BI_ExtNumberInfoFieldGrouping.initInfortion(iniInFoRequest);
        Map<String, Map<String, String>> map_tf_map_nf_v = new Map<String, Map<String, String>>();
        map_tf_map_nf_v.put('a',new Map<String, String>());
        map_tf_map_nf_v.get('a').put('b', 'c');
        IMP_BI_ExtNumberInfoFieldGrouping.getTFmapNFV(map_tf_map_nf_v, 'a:12,b:13,c:15;d:12,g:12,f:12');
        
        IMP_BI_ClsBatch_NumberInfoFieldGrouping.resMInfoB MInfoB = new IMP_BI_ClsBatch_NumberInfoFieldGrouping.resMInfoB();
        MInfoB.cId = cycle.Id;
        List<IMP_BI_ClsBatch_NumberInfoFieldGrouping.reqMInfo> reqlist = new List<IMP_BI_ClsBatch_NumberInfoFieldGrouping.reqMInfo>();
        IMP_BI_ClsBatch_NumberInfoFieldGrouping.reqMInfo req = new IMP_BI_ClsBatch_NumberInfoFieldGrouping.reqMInfo();
        req.f = 1;
        req.t = 100;
        req.v = 'test';
        reqlist.add(req);
        MInfoB.listreqMInfo = reqlist;
        MInfoB.numberField = 'a';
        MInfoB.textField = 'b';
        MInfoB.tfLabel = 'tst';
        MInfoB.tncv = 'te';
        IMP_BI_ExtNumberInfoFieldGrouping.saveListreqMInfo(MInfoB);
         
    }
}