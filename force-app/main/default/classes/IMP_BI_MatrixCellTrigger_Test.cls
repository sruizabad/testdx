/**
*   Test class for trigger IMP_BI_MatrixCellTrigger.
*
@author Hely
@created 2015-03-03
@version 1.0
@since 20.0
*
@changelog
* 
*/
@isTest
private class IMP_BI_MatrixCellTrigger_Test {

    static testMethod void myUnitTest() {
    	Lifecycle_Template_BI__c lt = new Lifecycle_Template_BI__c();
    	lt.Name = 'ltName';
    	lt.Type_BI__c = 'New';
    	lt.Potential_Factor_Numbers_BI__c = '20;20;20;20;20;20';
    	lt.Adoption_Factor_Numbers_BI__c = '20;20;20;20;20;20';
    	lt.Row_BI__c = 6;
    	lt.Column_BI__c = 6;
    	lt.Product_Lifecycle_Weight_BI__c = 0;
    	lt.Adoption_Weight_Factor_BI__c = 0;
    	insert lt;
    	
    	Matrix_BI__c m = new Matrix_BI__c();
    	m.Name_BI__c = 'matrix';
    	m.Lifecycle_Template_BI__c = lt.Id;
    	m.Row_BI__c = 6;
    	m.Column_BI__c = 6;
    	insert m;
    	
        Matrix_Cell_BI__c mc = new Matrix_Cell_BI__c();
        mc.Matrix_BI__c = m.Id;
        mc.Row_BI__c = 2;
        mc.Column_BI__c = 2;
        
        Test.startTest();
        insert mc;
        Test.stopTest();
        
    }
}