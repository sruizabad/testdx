/**
* ===================================================================================================================================
*                                   IMMPaCT BI                                                     
* ===================================================================================================================================
*  Decription:      Show NTL Comparison Report
*  @author:         Jefferson Escobar
*  @created:        20-Aug-2015
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          33.0 (Force.com ApiVersion) 
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         20-Aug-2015                 jescobar                    Construction of the class.
*/ 

public class IMP_BI_NTLComparisonReport {
    private Id countryId;
    
    /**Ids selectoptions*/
    public String oldCycleID {get;set;}
    public String newCycleID {get;set;}
    public String oldPortfolioID {get;set;}
    public String newPortfolioID {get;set;}
    public Portfolio_BI__c oldPortfolioDesc {get;set;}
    public Portfolio_BI__c newPortfolioDesc {get;set;}
    
    /**Selectoption lists*/
    public List<Selectoption> newCycles {get;private set;}
    public List<Selectoption> newPortfolios {get;private set;}
    public List<Selectoption> oldPortfolios {get;private set;}
    
    /**params of the page*/
    public PortfolioNTLOverview oldPortfolioOverview {get;private set;}
    public PortfolioNTLOverview newPortfolioOverview {get;private set;}
    public LocaleConfig localeConfig{get;set;}
    public Country_BI__c country {get;private set;}
    
    /**Map variables */
    public Map<Id,Portfolio_BI__c> mapPortfolioOldOne;
    public Map<Id,Portfolio_BI__c> mapPortfolioNewOne;
    private Map<String, String> map_urlParams;
    private Map<Id,String> mapCountryCycles;
    
        
    /**
    *   StandardController constructor
    */
    public IMP_BI_NTLComparisonReport (ApexPages.standardController ctr){
        map_urlParams = ApexPages.currentPage().getParameters();
        countryId = (map_urlParams != null && map_urlParams.containsKey('cId')) ? map_urlParams.get('cId') : null; 
       	 
        if(countryId != null){
            initComponents();
            country = [Select Name From Country_BI__c where Id = :countryId];
        }
    }
    
    
    /** 
    * Initialize all the component of the age
    * @return
    */
    void initComponents(){
        oldPortfolioID = '';
        newCycleID = '';
        newPortfolioID = '';
        newCycles = new List<Selectoption>{new Selectoption('','--None--')};
        oldPortfolios = new List<Selectoption>{new Selectoption('','--None--')};
        newPortfolios = new List<Selectoption>{new Selectoption('','--None--')};
        localeConfig = new LocaleConfig();
        
        oldPortfolioDesc = new Portfolio_BI__c();
    	newPortfolioDesc = new Portfolio_BI__c();
        
        //wrapper classes
        oldPortfolioOverview = new PortfolioNTLOverview();
        newPortfolioOverview = new PortfolioNTLOverview();
    }
    
    /**
    * Load firt list of cycles for the country
    *
    @author  Jefferson Escobar
    @created 20-Aug-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return  oldCycles  List<SelectOption>
    *
    @changelog
    * 20-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public List<SelectOption> getOldCycles (){
    	List<SelectOption> oldCycles = new List<SelectOption>{new Selectoption ('','--None--')};
    	mapCountryCycles = new Map<Id,String>();
    	 
		//Filtering cycles wiht at least one portofolio created     
        for(Portfolio_BI__c p : [Select Cycle_BI__c, Cycle_BI__r.Name from Portfolio_BI__c where Cycle_BI__r.Country_Lkp_BI__c = :this.countryId and Status_BI__c = 'Calculated'
        							order by Cycle_BI__r.Name]){
            
            //Adding list of cycle with portfolios under the country
            mapCountryCycles.put(p.Cycle_BI__c, p.Cycle_BI__r.Name);
		}
        
        if(mapCountryCycles.size()>0){
        	for(Id cID : mapCountryCycles.keySet()){
            	oldCycles.add(new Selectoption (cID, mapCountryCycles.get(cID)));
        	}
        } 
         
        
        return oldCycles;
    }
    
    /**
    * Load list of portfolios according to the old cycle selected
    *
    @author  Jefferson Escobar
    @created 20-Aug-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return
    *
    @changelog
    * 20-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public void action_loadOldPortfolios (){
        initComponents();
        
        if(oldCycleID != null && oldCycleID.trim().length() > 0){
            this.oldPortfolios = getOldPortfolios(oldCycleID);
        }
    }
    
    /**
    * Get list of portfolios
    *
    @author  Jefferson Escobar
    @created 20-Aug-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return List<Selectoption>
    *
    @changelog
    * 20-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created<>
    */
    public List<Selectoption> getOldPortfolios (Id idCycle){
        List<Selectoption> portfolios = new List<Selectoption>{new Selectoption ('','--None--')};
        mapPortfolioOldOne = new Map<Id,Portfolio_BI__c>();
        
        for(Portfolio_BI__c portfolio : [Select Id, Name  from Portfolio_BI__c where Cycle_BI__c = :idCycle order by Name]){
            portfolios.add(new Selectoption (portfolio.Id, portfolio.Name));
            mapPortfolioOldOne.put(portfolio.Id, portfolio);
        }
        return portfolios;
    }
    
    /**
    * Action to get new list of cycles to compare
    *
    @author  Jefferson Escobar
    @created 20-Aug-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return
    *
    @changelog
    * 20-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public void action_loadNewCycles(){
        /** Initialize select list options*/
        newCycleID = '';
        newPortfolioID = '';
        oldPortfolioDesc = new Portfolio_BI__c();
        newPortfolioDesc = new Portfolio_BI__c();
        newCycles = new List<Selectoption>{new Selectoption('','--None--')};
        newPortfolios = new List<Selectoption>{new Selectoption('','--None--')};
        newPortfolioOverview = new PortfolioNTLOverview();
        
        if(oldPortfolioID != null && oldPortfolioID.trim().length() > 0){
            oldPortfolioDesc = this.mapPortfolioOldOne.get(oldPortfolioID);
            newCycles = getNewCycles(oldCycleID);
        }
        
        //Load portfolio NTL overview info
        //this.oldPortfolioOverview = getPortfolioNTL(oldPortfolioID);
    }
    
    /**
    * Get list of new cycles
    *
    @author  Jefferson Escobar
    @created 20-Aug-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return List<Selectoption>
    *
    @changelog
    * 20-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public List<Selectoption> getNewCycles (Id cycleID){
        List<Selectoption> cycles = new List<Selectoption>{new Selectoption ('','--None--')};
        
        if(this.mapCountryCycles.size()>0){
        	for(Id cID : mapCountryCycles.keySet()){
        		if(cycleID != cID)
            		cycles.add(new Selectoption (cID, mapCountryCycles.get(cID)));
        	}
         }
        return cycles;
    }
    
    
    /**
    * Action to get new list of portfolios to compare
    *
    @author  Jefferson Escobar
    @created 20-Aug-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return
    *
    @changelog
    * 20-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public void action_loadNewPortfolios(){
        /** Initialize select list options*/
        newPortfolioID = '';
        newPortfolioDesc = new Portfolio_BI__c();
        newPortfolioOverview = new PortfolioNTLOverview();
        newPortfolios = new List<Selectoption>{new Selectoption('','--None--')};

        if(newCycleID != null && newCycleID.trim().length() > 0){
        	this.newPortfolios = getnewPortfolios(newCycleID);
        }
    }
    
    /**
    * Get list of new portfolios by cycle
    *
    @author  Jefferson Escobar
    @created 20-Aug-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return List<Selectoption>
    *
    @changelog
    * 20-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public List<Selectoption> getNewPortfolios(Id idCycle){
        List<Selectoption> portfolios = new List<Selectoption>{new Selectoption ('','--None--')};
        mapPortfolioNewOne = new Map<Id,Portfolio_BI__c>();
        
        for(Portfolio_BI__c portfolio : [Select Id, Name from Portfolio_BI__c where Cycle_BI__c = :idCycle 
        									order by Name]){
            portfolios.add(new Selectoption (portfolio.Id, portfolio.Name));
            mapPortfolioNewOne.put(portfolio.Id, portfolio);
        }
        return portfolios;
    }
    
    
    /**
    * Action to getting new portfolio 
    *
    @author  Jefferson Escobar
    @created 20-Aug-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return
    *
    @changelog
    * 20-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public void action_getNewPortfolio(){
        /** Initialize select list options*/
        newPortfolioDesc = new Portfolio_BI__c();
        
        if(newPortfolioID != null && newPortfolioID.trim().length() > 0){
            newPortfolioDesc = this.mapPortfolioNewOne.get(newPortfolioID);
        }
        
        //Load portfolio NTL overview info
        //this.newPortfolioOverview = getPortfolioNTL(newPortfolioID);
    }
    
    
    /**
    * Comparing process to compare portfolios by segments
    *
    @author  Jefferson Escobar
    @created 20-Aug-2015
    @version 1.0
    @since   34.0 (Force.com ApiVersion)
    *
    @return Set of values after comparing portfolios 
    *
    @changelog
    * 20-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    @RemoteAction
    @ReadOnly
    public static String comparePortfolioNTL(String jsonPortfolioComparison){
        PortfolioComparison pComparison = (PortfolioComparison) JSON.deserialize(jsonPortfolioComparison, PortfolioComparison.class);
        Map<String, Integer> mapPortfolioComparison = (pComparison.mapPortfolioComparison == null) ? new Map<String, Integer>() : pComparison.mapPortfolioComparison;
        Map<String, Integer> mapTotalQuintileOldPortfolio = (pComparison.mapTotalQuintileOldPortfolio == null) ? new Map<String, Integer>{'Q1'=>0,'Q2'=>0,'Q3'=>0,'Q4'=>0,'Q5'=>0} : pComparison.mapTotalQuintileOldPortfolio;
        Map<String, Integer> mapTotalQuintileNewPortfolio = (pComparison.mapTotalQuintileNewPortfolio == null) ? new Map<String, Integer>{'Q1'=>0,'Q2'=>0,'Q3'=>0,'Q4'=>0,'Q5'=>0} : pComparison.mapTotalQuintileNewPortfolio;
        String oldPortfolioID = pComparison.oldPortfolioID;
        String newPortfolioID = pComparison.newPortfolioID;
        String lastAccID = pComparison.lastAccID != null ? pComparison.lastAccID : null;
        String lastCycleDataID = pComparison.lastCycleDataID != null ? pComparison.lastCycleDataID : null;
        Integer limitRows = IMP_BI_Limit_Queries__c.getAll().get('Report_Rows') != null ? Integer.valueOf(IMP_BI_Limit_Queries__c.getAll().get('Report_Rows').Limit_BI__c) : 50000;
        
        //system.debug(':: lastID: ' + mapPortfolioComparison);
        try{
            if(oldPortfolioID != null && newPortfolioID != null){
                Portfolio_BI__c oldPortfolio;
                Portfolio_BI__c newPortfolio;
                
                system.debug(':: Ids: ' + oldPortfolioID+ '- ' + newPortfolioID);
                //Assign matrix according to the previous and current one in order to query once
                for(Portfolio_BI__c p : [Select Id, Version_BI__c From Portfolio_BI__c where Id in (:oldPortfolioID, :newPortfolioID)]){
                    
                    if((Id) p.Id == (Id) oldPortfolioID){
                        oldPortfolio = p;          
                    }
                    else{
                        newPortfolio = p;
                    }
                }
               
                String fisrtStageQuery = 'Select Account_BI__c, Quintile_Level__c  From Target_Account_BI__c where  Portfolio_BI__c = :oldPortfolioID and Version_BI__c = ' + oldPortfolio.Version_BI__c;
                String secondStageQuery = 'Select count(Id) cust, Quintile_Level__c quintile From Target_Account_BI__c where Account_BI__c in :accs and  Portfolio_BI__c = :newPortfolioID '+ 
                							' and Version_BI__c = '+ newPortfolio.Version_BI__c +' group by Quintile_Level__c ';
                
                String portfolioSummaryQuery = 'Select Id, Quintile_Level__c  From Target_Account_BI__c where  Portfolio_BI__c = :newPortfolioID and Version_BI__c = ' + newPortfolio.Version_BI__c;
                
                //Next iteration from previous cycle data ID
                if(lastAccID!=null){
                    fisrtStageQuery+='AND Account_BI__c > :lastAccID ';
                }
                
                //Next interation summary total quintile second portfolio
                if(lastCycleDataID != null){
                	portfolioSummaryQuery += ' AND Id > :lastCycleDataID '; 
                }
                	
                //Order and limit query by Account_BI__c
                fisrtStageQuery+='order by Account_BI__c limit :limitRows';
                portfolioSummaryQuery += 'order by Id limit :limitRows';
                
                Map<String, Set<Id>> mapAccsByQuitile = new Map<String,Set<Id>>{'Q1'=>new Set<Id>(),'Q2'=>new Set<Id>(),'Q3'=>new Set<Id>(),'Q4'=>new Set<Id>(),'Q5'=>new Set<Id>()};
                
                Integer counter=0;
                system.debug(':: Query: ' + fisrtStageQuery);
                for(Target_Account_BI__c tacc : Database.query(fisrtStageQuery)){
                    String quintile = tacc.get('Quintile_Level__c') != null ? 'Q' + tacc.get('Quintile_Level__c') : null;
                    
                    if(quintile!=null && mapAccsByQuitile.containsKey(quintile)){
                        Set<Id> accs = mapAccsByQuitile.get(quintile);
                        accs.add(tacc.Account_BI__c);
                        mapAccsByQuitile.put(quintile,accs);
                        
                     	Integer cust = mapTotalQuintileOldPortfolio.get(quintile) != null ? Integer.valueOf(mapTotalQuintileOldPortfolio.get(quintile)) : 0;
                        mapTotalQuintileOldPortfolio.put(quintile, (cust + 1));
                    }
                    counter++;
                    
                    //Determine next iteration of process
                    if(counter == limitRows){
                        pComparison.gotonext = true;
                    }
                    pComparison.lastAccID=tacc.Account_BI__c;
                }
                
                //Summary level quintile second portfolio
                counter = 0;
                for(Target_Account_BI__c tacc : Database.query(portfolioSummaryQuery)){
                	String quintile = tacc.get('Quintile_Level__c') != null ? 'Q' + tacc.get('Quintile_Level__c') : null;
                	
                	Integer cust = mapTotalQuintileNewPortfolio.get(quintile) != null ? Integer.valueOf(mapTotalQuintileNewPortfolio.get(quintile)) : 0;
                    mapTotalQuintileNewPortfolio.put(quintile, (cust + 1));
                    counter++;
                    
                    //Determine next iteration of process
                    if(counter == limitRows){
                        pComparison.gotonext = true;
                    }
                    pComparison.lastCycleDataID=tacc.Id;
                }
               	system.debug(':: mapTotalQuintileOldPortfolio totals: '+ mapTotalQuintileOldPortfolio);
               	system.debug(':: mapTotalQuintileNewPortfolio totals: '+ mapTotalQuintileNewPortfolio);
                for(Integer i= 1, q=5; i<=q;i++){
                    Set<Id> accs = mapAccsByQuitile.get('Q'+i);
                    if(accs!=null&&accs.size()>0){
                        for(AggregateResult pQuintile : Database.query(secondStageQuery)){
                            String key = 'Q' + i +'_'+ 'Q' + pQuintile.get('quintile');
                            Integer cust = pQuintile.get('cust') != null ? Integer.valueOf(pQuintile.get('cust')) : 0;
                            //system.debug(':: key: ' + key + ' Q: ' + cust);
                            Integer acuCustomers = mapPortfolioComparison.get(key) != null ? Integer.valueOf(mapPortfolioComparison.get(key)) : 0;
                            mapPortfolioComparison.put(key,(cust+acuCustomers));
                        }
                    }
                }
                system.debug(':: counter: ' + counter);
                //system.debug(':: mapTotalQuintileOldPortfolio: ' + mapTotalQuintileOldPortfolio);
            }
        }catch(DmlException de){
            pComparison.message = '[ERROR] ' + de.getMessage();
            pComparison.isException = true;
            system.debug('[ERROR] ' + de.getMessage());
        }
        catch(Exception ex){
            pComparison.message = '[ERROR] ' + ex.getMessage();
            pComparison.isException = true;
            system.debug('[ERROR] ' + ex.getMessage());
        }
        //Accumulate values
        if(mapPortfolioComparison != null)
        	 pComparison.mapPortfolioComparison = mapPortfolioComparison;
		
		if(mapTotalQuintileOldPortfolio != null)
			pComparison.mapTotalQuintileOldPortfolio = mapTotalQuintileOldPortfolio;  
        	
        if(mapTotalQuintileNewPortfolio != null)
        	pComparison.mapTotalQuintileNewPortfolio = mapTotalQuintileNewPortfolio;  
        

        pComparison.cds =  [Select Id, Name From Cycle_Data_BI__c limit 100];
        return JSON.Serialize(pComparison);
    }

/**
==================================================================================================================================
                                                Wrapper Classes                                                     
==================================================================================================================================
*/  
    
    public class PortfolioNTLOverview {
        public Map<String, String> quintileOverview {get;set;}
        public Integer totalCovered {get;set;}
        
        /**Percent covered for each segment*/
        public Long firstQuintile{get;set;}
        public Long secondQuintile{get;set;}
        public Long thirdQuintile{get;set;}
        public Long fourthQuintile{get;set;}
        public Long fifthQuintile{get;set;}
        
        public PortfolioNTLOverview(){
            quintileOverview = new Map<String, String>{'Q1'=>'0','Q2'=>'0','Q3'=>'0','Q4'=>'0','Q5'=>'0'};
            firstQuintile = 0;
            secondQuintile = 0;
            thirdQuintile = 0;
            fourthQuintile = 0;
            fifthQuintile = 0;
        }
    }
    
    public class PortfolioComparison{
        public Map<String, Integer> mapPortfolioComparison {get;set;}
        public Map<String, Integer> mapTotalQuintileOldPortfolio {get;set;}
        public Map<String, Integer> mapTotalQuintileNewPortfolio {get;set;}
        public String oldPortfolioID;
        public String newPortfolioID;
        public String lastAccID;
        public String lastCycleDataID;
        public boolean isException;
        public boolean gotonext;
        public string message;
        public List<Cycle_Data_BI__c> cds {get;set;}
        
        public PortfolioComparison(){
            isException=false;
        }
    }
    
    public class LocaleConfig{
        public String jsPath {get; set;}
        public String locale {get; set;}
        public LocaleConfig(){
            this.locale= Userinfo.getLocale();
            try{
                List<String> list_s = locale.split('_');
                this.locale = list_s[0]+'-'+list_s[1];
            }catch(Exception e){
                this.locale = 'de-DE';
            } 
            this.jsPath = '/glob-cultures/cultures/globalize.culture.'+locale+'.js';
        }
        
    }
}