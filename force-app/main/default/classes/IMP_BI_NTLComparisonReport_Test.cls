/**
* ===================================================================================================================================
*                                   IMMPaCT BI                                                     
* ===================================================================================================================================
*  Decription:      Test for IMP_BI_NTLComparisonReport class
*  @author:         Jefferson Escobar
*  @created:        31-August-2015
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          34.0 (Force.com ApiVersion) 
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         31-August-2015              jescobar                    Construction of the class.
*/ 
@isTest
private class IMP_BI_NTLComparisonReport_Test {

    static testMethod void testRunReport() {
        Country_BI__c country = IMP_BI_ClsTestHelp.createTestCountryBI();
        insert country;
        
        Account acc = IMP_BI_ClsTestHelp.createTestAccount();
        insert acc;
        
        Cycle_BI__c c1 = IMP_BI_ClsTestHelp.createTestCycle();
        c1.Name = 'c1 test';
        c1.Country_Lkp_BI__c = country.Id;
        insert c1;
        
        Cycle_BI__c c2 = IMP_BI_ClsTestHelp.createTestCycle();
        c2.Name = 'c2 test';
        c2.Country_Lkp_BI__c = country.Id;
        insert c2;
        
        Portfolio_BI__c p1 = IMP_BI_ClsTestHelp.createTestPortfolio();
        p1.Cycle_BI__c = c1.Id;
        p1.Status_BI__c = 'Calculated';
        p1.Version_BI__c = 1;
        insert p1;
        
        Portfolio_BI__c p2 = IMP_BI_ClsTestHelp.createTestPortfolio();
        p2.Cycle_BI__c = c2.Id;
        p2.Status_BI__c = 'Calculated';
        p2.Version_BI__c = 1;
        insert p2;
        
        Target_Account_BI__c ta1 = IMP_BI_ClsTestHelp.createTestTargetAccountBI();
        ta1.Account_BI__c = acc.Id;
        ta1.Portfolio_BI__c = p1.Id;
        ta1.Version_BI__c = 1;
        ta1.Quintile_Level__c = '1';
        insert ta1;
        
        Target_Account_BI__c ta2 = IMP_BI_ClsTestHelp.createTestTargetAccountBI();
        ta2.Account_BI__c = acc.Id;
        ta2.Portfolio_BI__c = p2.Id;
        ta2.Version_BI__c = 1;
        ta2.Quintile_Level__c = '1';
        insert ta2;
        
        //Start testing
    	Test.startTest();
    	
    	////Init controller
    	ApexPages.currentPage().getParameters().put('cId', country.Id);
    	IMP_BI_NTLComparisonReport ntlReport = new IMP_BI_NTLComparisonReport(new Apexpages.Standardcontroller(country));
    	
       	//Execute every action method
    	ntlReport.getOldcycles();
    	ntlReport.oldCycleID = c1.Id;
    	ntlReport.action_loadOldPortfolios();
    	ntlReport.oldPortfolioID = p1.Id;
    	ntlReport.action_loadNewCycles();
    	ntlReport.newCycleID = c2.Id;
    	ntlReport.action_loadNewPortfolios();
    	ntlReport.newPortfolioID = p2.Id;
    	ntlReport.action_getNewPortfolio();
    	
    	//Build json object
    	String jsonPortfolioComparison = '{"oldPortfolioID":"'+p1.Id+'","newPortfolioID":"'+p2.Id+'"}';
    	//Remote action compare portfolios
    	IMP_BI_NTLComparisonReport.comparePortfolioNTL(jsonPortfolioComparison);
    	
    	//Stop testing 
    	Test.stopTest();	
    }
}