/**
* ===================================================================================================================================
*                                   IMMPaCT BI                                                     
* ===================================================================================================================================
*  Decription:      Show all related list of products loaded for Cycle
*  @author:         Jefferson Escobar
*  @created:        11-Feb-2015
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          32.0 (Force.com ApiVersion) 
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         11-Feb-2014                 jescobar                    Construction of the class.
*/ 

public class IMP_BI_ProdctDataLoad {
    
    /** Public members*/
    public boolean showTable {get;set;}
    public String cycleDataOverview_klp {get;private set;}
    
    /** Private members*/ 
    private Map<String,IMP_BI_Default_Setting__c> customSettings = IMP_BI_Default_Setting__c.getAll();
    private List<Cycle_Data_Overview_BI__c> products; 
    private Id cycleId;
    
    /**Status bar*/
    private Id jobId;
    public Integer progress {get; private set;}
    public boolean enableSync {get;set;}
    
    public IMP_BI_ProdctDataLoad (Apexpages.Standardcontroller ctr){
        if(ctr.getId()!=null){
            cycleId = ctr.getId();
            
            //Get Id for cycle lookup field on Cycle data overview 
            cycleDataOverview_klp = customSettings.get('CycleDataOverview_lkp') != null ? customSettings.get('CycleDataOverview_lkp').Id_Value_BI__c : null;  
        }
    }
    
    /**
    * Get list of overview products
    * @return List<Cycle_Data_Overview_BI__c>
    *
    * @changelog
    *  16-Feb-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    *  - Created
    */
    public List<Cycle_Data_Overview_BI__c> getProducts(){
        
        products = new  List<Cycle_Data_Overview_BI__c>();
        if(cycleId!=null){
             products = [Select Id, Product_Catalog_BI__c, Product_Catalog_BI__r.Name, Total_Adoption_BI__c, Total_Potential_BI__c, Type_BI__c, Account_Matrix_Split_BI__c, Count_BI__c, Department_Types_BI__c 
                        From Cycle_Data_Overview_BI__c where Cycle_BI__c = :cycleId order by Product_Catalog_BI__r.Name asc];
                        
            system.debug(':: Products: ' + products);
        }
        return products;            
    }
    
    /**
    * Run status bar progress
    *
    * @return Pagereference
    *
    * @changelog
    *  13-Feb-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    *  - Created
    */
    public Pagereference runStatusProgress(){
        try{
            String csName = 'CycleDataOverview-' + cycleId;
            
            //Get job Id for current cycle
            jobId = customSettings.get(csName) != null ? customSettings.get(csName).Id_Value_BI__c : null;
            system.debug(':: Job Id: ' + jobId + ' csName: ' + csName);
                        
            if(jobId != null){
                syncProgressBar();
                enableSync = true;
            }else{
                showTable = true;
            }
            
        }catch(Exception e){
            system.debug('[ERROR] - ' + e.getMessage());
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, e.getMessage()));
        }
        return null;
    }
        
    
    /**
    * Synchronize status of deletion
    * @return Pagereference
    *
    * @changelog
    *  13-Feb-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    *  - Created
    */
    public Pagereference syncProgressBar(){
        try{
            Double itemsProcessed = 0;
            Double totalItems = 0;
            String status;
            
            List<AsyncApexJob> jobs = [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id, CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob 
                                    where Id = :jobId]; 
            //Query the Batch apex jobs
            for(AsyncApexJob a : jobs){
                system.debug(':: Total Items: ' + totalItems + ' Items processed: ' + itemsProcessed + ' Status: ' + a.Status);
                status = a.Status;
                itemsProcessed += a.JobItemsProcessed != null ? a.JobItemsProcessed : 0;
                totalItems += a.TotalJobItems != null ? a.TotalJobItems : 0;
            }
            
            system.debug(':: Progress: ' + progress + ' Total Items: ' + totalItems +' Items Processed: ' + itemsProcessed );
            progress = (totalItems == 0) ? 1 : ((itemsProcessed  / totalItems) * 100.0).intValue();
            progress = (progress == null || progress == 0) ? 1 : progress;
            
            if(progress>99 || status == 'Completed'){
                //Set bar progress by 100 in case all the jobs are completed
                progress = 100;
                this.enableSync = false;
            }
        }catch(Exception e){
            system.debug('[ERROR] - ' + e.getMessage());
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, e.getMessage()));
        }
        return null;
    }
    
    /**
    * Refresh block section
    * @return Pagereference
    *
    * @changelog
    *  13-Feb-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    *  - Created
    */
    public PageReference refreshPageBlock(){
        enableSync = false;
        showtable = true;
        system.debug(':: Sync: ' + enableSync);
        return null;
    }
    
    /**
    * Refresh overview of products
    * @return Pagereference
    *
    * @changelog
    *  23-Feb-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    *  - Created
    */
    public Pagereference refreshOverview (){
        return new Pagereference ('/'+cycleId);
    }
}