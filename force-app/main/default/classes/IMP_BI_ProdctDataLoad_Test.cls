/**
* ===================================================================================================================================
*                                   IMMPaCT BI                                                     
* ===================================================================================================================================
*  Decription:      Test for IMP_BI_ProdctDataLoad (All) class
*  @author:         Jefferson Escobar
*  @created:        26-Mar-2015
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          33.0 (Force.com ApiVersion) 
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         26-Mar-2015                 jescobar                    Construction of the class.
*/
@isTest
private class IMP_BI_ProdctDataLoad_Test {

    static testMethod void tstProductDataLoad() {
        Account acc = IMP_BI_ClsTestHelp.createTestAccount();
        acc.Name = '123e';    
        insert acc;
        
        Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
        insert c;
        
        Cycle_BI__c cycle2 = new Cycle_BI__c();
        cycle2.Country_BI__c = 'USA';
        cycle2.Start_Date_BI__c = date.today() - 10;
        cycle2.End_Date_BI__c = date.today() + 10;
        cycle2.IsCurrent_BI__c = false;
        cycle2.Country_Lkp_BI__c = c.Id;
        insert cycle2;
                
        Product_vod__c p2 = IMP_BI_ClsTestHelp.createTestProduct();
        p2.Country_BI__c = c.Id;
        insert p2;
        
        Cycle_Data_Overview_BI__c productLoad = new Cycle_Data_Overview_BI__c(  Product_Catalog_BI__c = p2.Id, Cycle_BI__c = cycle2.Id);
        insert productLoad;
        
        IMP_BI_Default_Setting__c dsInst = new IMP_BI_Default_Setting__c(Name = 'CycleDataOverview-'+cycle2.Id, Id_Value_BI__c = cycle2.Id);
        insert dsInst;
        
        //Start test process
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id',cycle2.Id);
        //ApexPages.currentPage().getParameters().put('cycid',cycle2.Id);
        
        //Run data load overview batch
        //IMP_BI_CtrlCalculateCycleDataOverview batchOverview = new IMP_BI_CtrlCalculateCycleDataOverview();
        //batchOverview.runBatch();
        
        //Start data overview load
        IMP_BI_ProdctDataLoad dataLoadOverview = new IMP_BI_ProdctDataLoad(new ApexPages.StandardController(cycle2));
        
        dataLoadOverview.getProducts();
        dataLoadOverview.runStatusProgress();
        
        dataLoadOverview.refreshPageBlock();
        dataLoadOverview.refreshOverview();
        
        Test.stopTest();
    }

}