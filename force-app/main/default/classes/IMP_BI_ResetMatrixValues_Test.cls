/**
*   Test class for class IMP_BI_ResetMatrixValues.
*
@author Antonio Ferrero
@created 2015-03-30
@version 1.0
@since 20.0
*
@changelog
* 2013-06-25 Antonio Ferrero <aferrro@omegacrmconsulting.com>
* - Created
*- Test coverage  %
*/
@isTest
private class IMP_BI_ResetMatrixValues_Test {
    static testMethod void testResetMatrixValues() {
       /* Commented by MCH problem with map_mcId_filter_quantityString in child2 18012018  //insert new Knipper_Settings__c();
        Account acc = IMP_BI_ClsTestHelp.createTestAccount();
        acc.Call_BI__c = true;
        acc.BI_Speaker_BI__c = true;
        insert acc;
        
        Account acc1 = IMP_BI_ClsTestHelp.createTestAccount();
        acc1.Name = '456f';
        acc1.Call_BI__c = true;    
        insert acc1;
        
        Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
        insert c;
    
        Product_vod__c p2 = IMP_BI_ClsTestHelp.createTestProduct();
        p2.Country_BI__c = c.Id;
        insert p2;            
    
        Cycle_BI__c cycle2 = new Cycle_BI__c();
        cycle2.Country_BI__c = 'USA';
        cycle2.Start_Date_BI__c = date.today() - 10;
        cycle2.End_Date_BI__c = date.today() + 10;
        cycle2.Country_Lkp_BI__c = c.Id;
        cycle2.IsCurrent_BI__c = false;
        cycle2.Text_Info_Field_1__c = 'Last Cycle Segment';
        cycle2.Text_Info_Field_2__c = 'Last Cycle Segment';
        insert cycle2;
        
        Lifecycle_Template_BI__c mt = new Lifecycle_Template_BI__c();
        mt.Name = 'mt';
        mt.Country_BI__c = c.Id;
        mt.Active_BI__c = true;
        mt.isLaunch_Phase_BI__c = true;
        mt.Adoption_Status_01_BI__c = '1';
        mt.Adoption_Status_02_BI__c = '2';
        mt.Adoption_Status_03_BI__c = '3';
        mt.Adoption_Status_04_BI__c = '4';
        mt.Adoption_Status_05_BI__c = '5';
        insert mt;
                        
        Matrix_BI__c ma = IMP_BI_ClsTestHelp.createTestMatrix();
        ma.Cycle_BI__c = cycle2.Id;
        ma.Intimacy_Levels_BI__c = null;
        ma.Potential_Levels_BI__c = null;
        ma.Size_BI__c = '10x11';
        ma.Lifecycle_Template_BI__c = mt.Id;
        ma.Specialization_BI__c = 'Cardiologist;GP';//Peng Zhu 2013-10-14
        ma.Column_BI__c = 11;
        ma.Product_Catalog_BI__c = p2.Id;
        ma.Filter_Field_1_BI__c = null;
        ma.Filter_Field_2_BI__c = null;
        ma.Filter_Field_3_BI__c = null;
        ma.Status_BI__c = 'Draft';
        ma.Scenario_BI__c = '1';
        ma.Account_Matrix_BI__c = false;
        insert ma;
        
        Matrix_BI__c ma1 = IMP_BI_ClsTestHelp.createTestMatrix();
        ma1.Cycle_BI__c = cycle2.Id;
        ma1.Intimacy_Levels_BI__c = null;
        ma1.Potential_Levels_BI__c = null;
        ma1.Size_BI__c = '10x11';
        ma1.Lifecycle_Template_BI__c = mt.Id;
        ma1.Specialization_BI__c = 'Cardiologist;GP';//Peng Zhu 2013-10-14
        ma1.Column_BI__c = 11;
        ma1.Product_Catalog_BI__c = p2.Id;
        ma1.Filter_Field_1_BI__c = null;
        ma1.Filter_Field_2_BI__c = null;
        ma1.Filter_Field_3_BI__c = null;
        ma1.Status_BI__c = 'Draft';
        ma1.Scenario_BI__c = '1';
        ma1.Account_Matrix_Type_BI__c = 'HCO only';
        insert ma1;
        
        Matrix_Cell_BI__c mc = IMP_BI_ClsTestHelp.createTestMatrixCell();
        mc.Matrix_BI__c = ma.Id;
        mc.Row_BI__c = 1;
        mc.Column_BI__c = 0;
        mc.Total_Customers_BI__c = 1;
        mc.Total_Intimacy_BI__c = 1;
        mc.Total_Potential_BI__c = 1;
        insert mc;
        
        Matrix_Cell_Detail_BI__c cellDetail = new Matrix_Cell_Detail_BI__c(Matrix_Cell_BI__c = mc.Id, Matrix_Filter_Value_BI__c = 'no;high', Quantity_BI__c = 1, Channel_BI__c='Face to Face',
                                                Account_Counter_BI__c = 1);
        insert cellDetail;
        
        Matrix_Cell_Detail_BI__c  cellDetail2 = cellDetail.clone(false,true);
        insert cellDetail2;
        
        
        Matrix_Cell_BI__c mc1 = IMP_BI_ClsTestHelp.createTestMatrixCell();
        mc1.Matrix_BI__c = ma1.Id;
        mc1.Row_BI__c = 1;
        mc1.Column_BI__c = 3;
        mc1.Total_Customers_BI__c = 20;
        mc1.Total_Intimacy_BI__c = 1000;
        mc1.Total_Potential_BI__c = 2000;
        insert mc1;
        
        Cycle_Data_BI__c cd = new Cycle_Data_BI__c();
        cd.Product_Catalog_BI__c = p2.Id;
        cd.Account_BI__c = acc.Id;
        cd.Cycle_BI__c = cycle2.Id;
        cd.Potential_BI__c = 12;
        cd.Intimacy_BI__c = 12;
        cd.Current_Update_BI__c = true;
        cd.Matrix_Cell_1_BI__c = mc.Id;
        cd.Matrix_BI__c = ma.Id;
        cd.Text_Info_Field_1__c = 'Detail';
        cd.Text_Info_Field_2__c = 'NON-Detail';
        insert cd;
        
        Cycle_Data_BI__c cd1 = new Cycle_Data_BI__c();
        cd1.Product_Catalog_BI__c = p2.Id;
        cd1.Account_BI__c = acc1.Id;
        cd1.Cycle_BI__c = cycle2.Id;
        cd1.Potential_BI__c = 12;
        cd1.Intimacy_BI__c = 12;
        cd1.Current_Update_BI__c = true;
        cd1.Matrix_Cell_1_BI__c = mc1.Id;
        cd1.Matrix_BI__c = ma.Id;
        cd1.Text_Info_Field_1__c = 'Detail';
        cd1.Text_Info_Field_2__c = 'NON-Detail';
        insert cd1;
        
        IMP_BI_ResetMatrixValues.ClsMatrixCellDetail cmd = new IMP_BI_ResetMatrixValues.ClsMatrixCellDetail(); 
        cmd.channel = 'test channel';
        cmd.filterField1 = '';
        cmd.filterField2 = '';
        cmd.filterField3 = '';
        cmd.cycleDataCounter = 2;
        cmd.filterValue = '';
       
        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('mId',ma.Id);
        ApexPages.StandardController ctrl = new ApexPages.StandardController(ma); 
        
        IMP_BI_ResetMatrixValues.quantityMapClass JSONCellDetail = new IMP_BI_ResetMatrixValues.quantityMapClass();
        JSONCellDetail.lastMcdId = cellDetail.Id;
        JSONCellDetail.matrixId = ma.Id;
        
        IMP_BI_ResetMatrixValues.ClsMatrixFilterNew r =  new IMP_BI_ResetMatrixValues.ClsMatrixFilterNew();
        r.matrixId = ma.Id;
        //r.cycleDataId = cd.Id;
        r.isEnd = false;
        r.set_mcId = mc1.Id;
        r.filterStr = 'BI_Speaker_BI__c;Text_Info_Field_1__c';
        r.mapSummary = null;
        r.scenario = '1';
         
        Set<String> setCellIds = new Set<String>();
        setCellIds.add(mc.Id);
        IMP_BI_ResetMatrixValues.jsonCellDetailIds = JSON.serialize(setCellIds);
        
        IMP_BI_ResetMatrixValues.generateDetailQuantityMap(r.matrixId, r.cycleDataId);
        IMP_BI_ResetMatrixValues.generateDetailQuantityMap2(JSON.Serialize(JSONCellDetail));
        IMP_BI_ResetMatrixValues.generateMatrixCellDetailsNewWithCycle(JSON.Serialize(r), cycle2.Id);
        
        //Query Filter for HCOs 
        r.filterStr = 'BI_Speaker_BI__c';
        IMP_BI_ResetMatrixValues.generateMatrixCellDetailsNew(JSON.Serialize(r));
        
        IMP_BI_ResetMatrixValues.delCellDetailsInBatch(JSON.serialize(setCellIds));
        
        Map<Integer, list<Matrix_Cell_Detail_BI__c>> map_idx_mcds = new Map<Integer, list<Matrix_Cell_Detail_BI__c>>();
        map_idx_mcds.put(1,new list<Matrix_Cell_Detail_BI__c>{cellDetail.clone(false,true)});
        map_idx_mcds.put(2,new list<Matrix_Cell_Detail_BI__c>{cellDetail2.clone(false,true)});
        IMP_BI_ResetMatrixValues rMatVal = new IMP_BI_ResetMatrixValues(ctrl);
        rMatVal.filterComb = 'Last Cycle Segment';
        rMatVal.map_idx_mcds=map_idx_mcds;
        rMatVal.saveMCDInStep();
        
        map<String, map<String, String>> map_mcId_filter_quantity = new map<String, map<String, String>>();
        IMP_BI_ResetMatrixValues.quantityMapClass quantityMapClass = new IMP_BI_ResetMatrixValues.quantityMapClass();
        map_mcId_filter_quantity.put(cellDetail.Id, new map<String, String>{'No'=>'1'});
        String map_mcId_filter_quantityString = JSON.Serialize(map_mcId_filter_quantity);
        quantityMapClass.map_mcId_filter_quantityString_wrapper  = map_mcId_filter_quantity;
        
        IMP_BI_ResetMatrixValues.map_mcId_filter_quantityString  = map_mcId_filter_quantityString;
        IMP_BI_ResetMatrixValues.matrixCellCounterMap = map_mcId_filter_quantityString;
        IMP_BI_ResetMatrixValues.ClsMatrixCellDetail clsCellDetail = new IMP_BI_ResetMatrixValues.ClsMatrixCellDetail();
        clsCellDetail.channel= 'Face to Face';
        clsCellDetail.filterField1 = 'no;yes';
        clsCellDetail.cycleDataCounter = 1;
        clsCellDetail.filterValue = 'No';
        List<IMP_BI_ResetMatrixValues.ClsMatrixCellDetail> jsonMatrixFilters = new  List<IMP_BI_ResetMatrixValues.ClsMatrixCellDetail>{clsCellDetail};
        IMP_BI_ResetMatrixValues.jsonMatrixFilters  = JSON.Serialize(jsonMatrixFilters);
        rMatVal.genAndSaveMatrixCellDetailNew();
        
        //Update  Cells
        IMP_BI_ResetMatrixValues.MatrixSummary mSum = new IMP_BI_ResetMatrixValues.MatrixSummary();
        mSum.totalCustomers = 56;
        mSum.totalAdoption = 1025;
        mSum.totalPotential = 520;
        Map<String, IMP_BI_ResetMatrixValues.MatrixSummary> mapSummary = new Map<String, IMP_BI_ResetMatrixValues.MatrixSummary>{mc.Id=>mSum};
        IMP_BI_ResetMatrixValues.map_mcData_quantityString = JSON.Serialize(mapSummary);
        rMatVal.updateMatrixCells();
        
        //Response call
        IMP_BI_ResetMatrixValues.ClsResponse response = new IMP_BI_ResetMatrixValues.ClsResponse();
        response.status = 'OK';
        response.message = 'msg';
        response.goToNext = 'yes';
     
        Test.stopTest();*/
    }

}