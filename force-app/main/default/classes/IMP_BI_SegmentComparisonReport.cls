/**
* ===================================================================================================================================
*                                   IMMPaCT BI                                                     
* ===================================================================================================================================
*  Decription:      Show Segment Comparison Report
*  @author:         Jefferson Escobar
*  @created:        09-Mar-2015
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          33.0 (Force.com ApiVersion) 
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         09-Mar-2015                 jescobar                    Construction of the class.
*/ 

public class IMP_BI_SegmentComparisonReport {
    private Id countryId;
    
    /**Ids selectoptions*/
    public String IdOldCycle {get;set;}
    public String IdNewCycle {get;set;}
    public String IdOldMatrix {get;set;}
    public String IdNewMatrix {get;set;}
    public Matrix_BI__c oldMatrixDesc {get;set;}
    public Matrix_BI__c newMatrixDesc {get;set;}
    
    /**Selectoption lists*/
    public List<Selectoption> newCycles {get;private set;}
    public List<Selectoption> newMatrices {get;private set;}
    public List<Selectoption> oldMatrices {get;private set;}
    
    /**params of the page*/
    public MatrixSegmentOverview oldMatrixOverview {get;private set;}
    public MatrixSegmentOverview newMatrixOverview {get;private set;}
    public LocaleConfig localeConfig{get;set;}
    public Country_BI__c country {get;private set;}
    
    /**Map variables */
    public Map<Id,Matrix_BI__c> mapProductbyMatrixOldOne;
    public Map<Id,Matrix_BI__c> mapProductbyMatrixNewOne;
    private map<String, String> map_urlParams;
    
        
    /**
    *   StandardController constructor
    */
    public IMP_BI_SegmentComparisonReport (ApexPages.standardController ctr){
        map_urlParams = ApexPages.currentPage().getParameters();
        countryId = (map_urlParams != null && map_urlParams.containsKey('cId')) ? map_urlParams.get('cId') : null; 
       	 
        if(countryId != null){
            initComponents();
            country = [Select Name From Country_BI__c where Id = :countryId];
        }
    }
    
    
    /** 
    * Initialize all the component of the age
    * @return
    */
    void initComponents(){
        IdOldMatrix = '';
        IdNewCycle = '';
        IdNewMatrix = '';
        newCycles = new List<Selectoption>{new Selectoption('','--None--')};
        oldMatrices = new List<Selectoption>{new Selectoption('','--None--')};
        newMatrices = new List<Selectoption>{new Selectoption('','--None--')};
        localeConfig = new LocaleConfig();
        
        oldMatrixDesc = new Matrix_BI__c();
    	newMatrixDesc = new Matrix_BI__c();
        
        //wrapper classes
        oldMatrixOverview = new MatrixSegmentOverview();
        newMatrixOverview = new MatrixSegmentOverview();
        
        List<Selectoption> cycles = new List<Selectoption>();
        Map<Id, String> mapCycles = new Map<Id, String>();
        cycles.add(new Selectoption ('','--None--'));
    }
    
    /**
    * Load firt list of cycles for the country
    *
    @author  Jefferson Escobar
    @created 11-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return  oldCycles  List<SelectOption>
    *
    @changelog
    * 11-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public List<SelectOption> getOldCycles (){
    	List<SelectOption> oldCycles = new List<SelectOption>{new Selectoption ('','--None--')};
    	Map<Id,String> mapCountryCycles = new Map<Id,String>();
    	 
		//Filtering cycles under the same product and record type of kind of matrix     
        for(Matrix_BI__c m : [Select Cycle_BI__c, Cycle_BI__r.Name from Matrix_BI__c where Cycle_BI__r.Country_Lkp_BI__c = :this.countryId 
        						and RecordTypeId IN (SELECT Id FROM RecordType WHERE SobjectType = 'Matrix_BI__c' AND Name LIKE '%HCP%')
        						and Current_BI__c = true
        						order by Cycle_BI__r.Name]){
            
            //Adding list of cycle with HCPs matrices under the country
            mapCountryCycles.put(m.Cycle_BI__c, m.Cycle_BI__r.Name);
		}
        
        if(mapCountryCycles.size()>0){
        	for(Id cID : mapCountryCycles.keySet()){
            	oldCycles.add(new Selectoption (cID, mapCountryCycles.get(cID)));
        	}
        } 
         
        
        return oldCycles;
    }
    
    /**
    * Load list of matrices according to the old cycle selected
    *
    @author  Jefferson Escobar
    @created 11-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return
    *
    @changelog
    * 11-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public void action_loadOldMatrices (){
        initComponents();
        
        if(IdOldCycle != null && IdOldCycle.trim().length() > 0){
            this.oldMatrices = getOldMatrices(IdOldCycle);
        }
    }
    
    /**
    * Get list of matrices
    *
    @author  Jefferson Escobar
    @created 11-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return List<Selectoption>
    *
    @changelog
    * 11-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created<>
    */
    public List<Selectoption> getOldMatrices (Id idCycle){
        List<Selectoption> matrices = new List<Selectoption>();
        mapProductbyMatrixOldOne = new Map<Id,Matrix_BI__c>();
        matrices.add(new Selectoption ('','--None--'));
        
        for(Matrix_BI__c matrix : [Select Id, Name_BI__c, Product_Catalog_BI__c, RecordTypeId,Product_Catalog_BI__r.Name, RecordType.Name, Specialization_BI__c from Matrix_BI__c where Cycle_BI__c = :idCycle
        							and RecordTypeId IN (SELECT Id FROM RecordType WHERE SobjectType = 'Matrix_BI__c' AND Name LIKE '%HCP%')
        							and Current_BI__c = true ]){
            matrices.add(new Selectoption (matrix.Id, matrix.Name_BI__c));
            mapProductbyMatrixOldOne.put(matrix.Id, matrix);
        }
        return matrices;
    }
    
    /**
    * Action to get new list of cycles to compare
    *
    @author  Jefferson Escobar
    @created 11-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return
    *
    @changelog
    * 11-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public void action_loadNewCycles(){
        /** Initialize select list options*/
        IdNewCycle = '';
        IdNewMatrix = '';
        oldMatrixDesc = new Matrix_BI__c();
        newMatrixDesc = new Matrix_BI__c();
        newCycles = new List<Selectoption>{new Selectoption('','--None--')};
        newMatrices = new List<Selectoption>{new Selectoption('','--None--')};
        
        if(IdOldMatrix != null && IdOldMatrix.trim().length() > 0){
            this.oldMatrixDesc = mapProductbyMatrixOldOne.get(IdOldMatrix);
            newCycles = getNewCycles(IdOldCycle, oldMatrixDesc.Product_Catalog_BI__c, oldMatrixDesc.RecordTypeId);
        }
        
        //Load matrix segment overview info
        this.oldMatrixOverview = getMatrixSegment(IdOldMatrix);
    }
    
     /**
    * Get Matrix segment overview
    *
    @author  Jefferson Escobar
    @created 22-July-2015
    @version 1.0
    @since   34.0 (Force.com ApiVersion)
    *
    @return List<Selectoption>
    *
    @changelog
    * 22-July-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public MatrixSegmentOverview getMatrixSegment(String idMatrix){
        MatrixSegmentOverview mso = new MatrixSegmentOverview();
        Map<String, Integer> segmentOverview = new Map<String, Integer>();
        Decimal totalSegment = 0;
        
        for(AggregateResult mSegment : [Select Sum(Total_Customers_BI__c) cust, Segment_BI__c From Matrix_Cell_BI__c 
                                        where Matrix_BI__c = :idMatrix and  Segment_BI__c in ('Gain','Build', 'Defend', 'Observe','Maintain') 
                                        group by Segment_BI__c]){
            
            String segment = mSegment.get('Segment_BI__c') != null ? (String) mSegment.get('Segment_BI__c') : null;
            Integer customers = mSegment.get('cust') != null ? Integer.valueOf(mSegment.get('cust')) : 0;
            
            //Assign value of customer for each segment
            if(segment!=null){
                segmentOverview.put(segment, customers);
                totalSegment+=customers;    
            }
                    
        }
        
        //Calculate weight each segment
        if(totalSegment>0){
            Integer gain=segmentOverview.get('Gain')!=null ? Integer.valueOf(segmentOverview.get('Gain')) : 0;
            Integer build=segmentOverview.get('Build')!=null ? Integer.valueOf(segmentOverview.get('Build')) : 0;
            Integer defend=segmentOverview.get('Defend')!=null ? Integer.valueOf(segmentOverview.get('Defend')) : 0;
            Integer observe=segmentOverview.get('Observe')!=null ? Integer.valueOf(segmentOverview.get('Observe')) : 0;
            Integer maintain=segmentOverview.get('Maintain')!=null ? Integer.valueOf(segmentOverview.get('Maintain')) : 0;
            
            mso.gainCovered = ((gain*100)/totalSegment).round(System.RoundingMode.HALF_DOWN);
            mso.buildCovered = ((build*100)/totalSegment).round(System.RoundingMode.HALF_DOWN);
            mso.defendCovered = ((defend*100)/totalSegment).round(System.RoundingMode.HALF_DOWN);
            mso.observeCovered = ((observe*100)/totalSegment).round(System.RoundingMode.HALF_DOWN);
            mso.maintainCovered = ((maintain*100)/totalSegment).round(System.RoundingMode.HALF_DOWN);
        }
        
        //Assign values matrix overview
        mso.segmentOverview = new Map<String,String>{'Gain'=>'0','Build'=>'0','Defend'=>'0','Observe'=>'0','Maintain'=>'0'}; 
        for(String segment : segmentOverview.keySet()){
            mso.segmentOverview.put(segment,segmentOverview.get(segment).format()); 
        }
        
        return mso;
    }
    
    /**
    * Get list of new cycles
    *
    @author  Jefferson Escobar
    @created 11-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return List<Selectoption>
    *
    @changelog
    * 11-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public List<Selectoption> getNewCycles (Id cycleID, String productID, String mRecordType){
        List<Selectoption> cycles = new List<Selectoption>();
        Map<Id, String> mapCycles = new Map<Id, String>();
        cycles.add(new Selectoption ('','--None--'));
		
		//Filtering cycles under the same product and record type of kind of matrix     
        for(Matrix_BI__c m : [Select Id, Name_BI__c, Product_Catalog_BI__c, Cycle_BI__c, Cycle_BI__r.Name from Matrix_BI__c where Product_Catalog_BI__c = :productID
        						and Cycle_BI__r.Country_Lkp_BI__c = :this.countryId and RecordTypeId = :mRecordType and Current_BI__c = true
        						order by Cycle_BI__r.Name]){
            if(m.Cycle_BI__c != cycleID)
                mapCycles.put(m.Cycle_BI__c, m.Cycle_BI__r.Name);
        }
        
        //Setting list of cycles filtered
        for(Id cID : mapCycles.keySet()){
        	cycles.add(new Selectoption (cID, mapCycles.get(cID)));
        }
        return cycles;
    }
    
    
    /**
    * Action to get new list of matrices to compare
    *
    @author  Jefferson Escobar
    @created 11-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return
    *
    @changelog
    * 11-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public void action_loadNewMatrices(){
        /** Initialize select list options*/
        IdNewMatrix = '';
        newMatrixDesc = new Matrix_BI__c();
        newMatrixOverview = new MatrixSegmentOverview();
        newMatrices = new List<Selectoption>{new Selectoption('','--None--')};

        if(IdNewCycle != null && IdNewCycle.trim().length() > 0){
        	this.newMatrices = getNewMatrices(IdNewCycle, this.oldMatrixDesc.Product_Catalog_BI__c, this.oldMatrixDesc.RecordTypeId);
        }
    }
    
    /**
    * Get list of new matrices by cycle and product
    *
    @author  Jefferson Escobar
    @created 11-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return List<Selectoption>
    *
    @changelog
    * 11-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public List<Selectoption> getNewMatrices (Id idCycle, Id productID, String mRecordTypeId){
        List<Selectoption> matrices = new List<Selectoption>();
        matrices.add(new Selectoption ('','--None--'));
        mapProductbyMatrixNewOne = new Map<Id,Matrix_BI__c>();
        
        for(Matrix_BI__c matrix : [Select Id, Name_BI__c, Product_Catalog_BI__c, Product_Catalog_BI__r.Name, RecordType.Name, Specialization_BI__c from Matrix_BI__c where Cycle_BI__c = :idCycle and Product_Catalog_BI__c = :productID and RecordTypeId = :mRecordTypeId
        							order by Name]){
            matrices.add(new Selectoption (matrix.Id, matrix.Name_BI__c));
            mapProductbyMatrixNewOne.put(matrix.Id, matrix);
        }
        return matrices;
    }
    
    
    /**
    * Action to getting new matrix 
    *
    @author  Jefferson Escobar
    @created 12-March-2015
    @version 1.0
    @since   33.0 (Force.com ApiVersion)
    *
    @return
    *
    @changelog
    * 12-March-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    public void action_getNewMatrix(){
        /** Initialize select list options*/
        newMatrixDesc = new Matrix_BI__c();
        
        if(IdNewMatrix != null && IdNewMatrix.trim().length() > 0){
            newMatrixDesc = this.mapProductbyMatrixNewOne.get(IdNewMatrix);
        }
        
        //Load matrix segment overview info
        this.newMatrixOverview = getMatrixSegment(IdNewMatrix);
    }
    
    /**
    * Comparing process to compare matrices by segments
    *
    @author  Jefferson Escobar
    @created 07-Aug-2015
    @version 1.0
    @since   34.0 (Force.com ApiVersion)
    *
    @return Set of values after comparing matrices 
    *
    @changelog
    * 07-Aug-2015 Jefferson Escobar <jescobar@omegacrmconsulting.com>
    * - Created
    */
    @RemoteAction
    @ReadOnly
    public static String compareMatrixSegment(String jsonMatrixComparison){
        MatrixComparison mComparison = (MatrixComparison) JSON.deserialize(jsonMatrixComparison, MatrixComparison.class);
        Map<String, Integer> mapMatrixComparison = (mComparison.mapMatrixComparison == null) ? new Map<String, Integer>() : mComparison.mapMatrixComparison;
        String IdOldMatrix = mComparison.IdOldMatrix;
        String IdNewMatrix = mComparison.IdNewMatrix;
        String lastAccID = mComparison.lastAccID != null ? mComparison.lastAccID : null;
        Integer limitRows = IMP_BI_Limit_Queries__c.getAll().get('Report_Rows') != null ? Integer.valueOf(IMP_BI_Limit_Queries__c.getAll().get('Report_Rows').Limit_BI__c) : 50000;
        
        //system.debug(':: lastID: ' + mapMatrixComparison);
        try{
            if(IdOldMatrix != null && IdNewMatrix != null){
                List<String> segments = new List<String>{'Gain','Build','Defend','Observe','Maintain'};
                
                Matrix_BI__c oldMatrix;
                Matrix_BI__c newMatrix;
                
                system.debug(':: Ids: ' + IdOldMatrix+ '- ' + IdNewMatrix);
                //Assign matrix according to the previous and current one in order to query once
                for(Matrix_BI__c m : [Select Id, Scenario_BI__c From Matrix_BI__c where Id in (:IdOldMatrix, :IdNewMatrix)]){
                    
                    if((Id)m.Id == (Id) IdOldMatrix){
                        oldMatrix = m;          
                    }
                    else{
                        newMatrix = m;
                    }
                }
                
                String mOldScenario = (oldMatrix.Scenario_BI__c != null && oldMatrix.Scenario_BI__c.trim().length()>0) ? '_'+oldMatrix.Scenario_BI__c : '';
                String mNewScenario = (newMatrix.Scenario_BI__c != null && newMatrix.Scenario_BI__c.trim().length()>0) ? '_'+newMatrix.Scenario_BI__c : ''; 
                
                String fisrtStageQuery = 'Select Account_BI__c, Matrix_Cell'+mOldScenario+'_BI__r.Segment_BI__c From Cycle_Data_BI__c where  Matrix'+mOldScenario+'_BI__c = :IdOldMatrix ';
                String secondStageQuery = 'Select count(Id) cust, Matrix_Cell'+mNewScenario+'_BI__r.Segment_BI__c seg From Cycle_Data_BI__c where Account_BI__c in :accs and  Matrix'+mNewScenario+'_BI__c = :IdNewMatrix '+  
                                                    'and Matrix_Cell'+mNewScenario+'_BI__r.Segment_BI__c in :segments '+
                                                    'group by Matrix_Cell'+mNewScenario+'_BI__r.Segment_BI__c ';
                
                //Next iteration from previous cycle data ID
                if(lastAccID!=null){
                    fisrtStageQuery+='AND Account_BI__c > :lastAccID ';
                }
                
                //Order and limit query by Account_BI__c
                fisrtStageQuery+='order by Account_BI__c limit :limitRows';
                
                Map<String, Set<Id>> mapAccsBySegment = new Map<String,Set<Id>>{'Gain'=>new Set<Id>(),'Build'=>new Set<Id>(),'Defend'=>new Set<Id>(),'Observe'=>new Set<Id>(),'Maintain'=>new Set<Id>()};
                Integer counter=0;
                //system.debug(':: Query: ' + fisrtStageQuery);
                for(Cycle_Data_BI__c cd : Database.query(fisrtStageQuery)){
                    String segment = cd.getSObject('Matrix_Cell'+mOldScenario+'_BI__r').get('Segment_BI__c') != null ? (String) cd.getSObject('Matrix_Cell'+mOldScenario+'_BI__r').get('Segment_BI__c') : null;
                    
                    if(segment!=null && mapAccsBySegment.containsKey(segment)){
                        Set<Id> accs = mapAccsBySegment.get(segment);
                        accs.add(cd.Account_BI__c);
                        mapAccsBySegment.put(segment,accs);
                    }
                    counter++;
                    
                    //Determine next iteration of process
                    if(counter == limitRows){
                        mComparison.lastAccID=cd.Account_BI__c;
                        mComparison.gotonext = true;
                    }
                }
                
                system.debug(':: Counter: ' + counter);
                for(String segment :  segments){
                    Set<Id> accs = mapAccsBySegment.get(segment);
                    if(accs!=null&&accs.size()>0){
                        for(AggregateResult mSegment : Database.query(secondStageQuery)){
                            //system.debug(':: Segment: ' + mSegment.get('seg') + ' - ' + mSegment.get('cust'));     
                            String key = segment+'_'+mSegment.get('seg');
                            Integer cust = mSegment.get('cust') != null ? Integer.valueOf(mSegment.get('cust')) : 0;
                            Integer acuCustomers = mapMatrixComparison.get(key) != null ? Integer.valueOf(mapMatrixComparison.get(key)) : 0;
                            
                            mapMatrixComparison.put(key,(cust+acuCustomers));
                        }
                    }
                }
                system.debug(':: counter: ' + counter);
            }
        }catch(DmlException de){
            mComparison.message = '[ERROR] ' + de.getMessage();
            mComparison.isException = true;
            system.debug('[ERROR] ' + de.getMessage());
        }
        catch(Exception ex){
            mComparison.message = '[ERROR] ' + ex.getMessage();
            mComparison.isException = true;
            system.debug('[ERROR] ' + ex.getMessage());
        }
        //Accumulate values
        if(mapMatrixComparison!=null)
            mComparison.mapMatrixComparison = mapMatrixComparison;
        
        return JSON.Serialize(mComparison);
    }

/**
==================================================================================================================================
                                                Wrapper Classes                                                     
==================================================================================================================================
*/  
    
    public class MatrixSegmentOverview {
        public Map<String, String> segmentOverview {get;set;}
        public Integer totalSegment {get;set;}
        
        /**Percent covered for each segment*/
        public Long gainCovered{get;set;}
        public Long buildCovered{get;set;}
        public Long defendCovered{get;set;}
        public Long observeCovered{get;set;}
        public Long maintainCovered{get;set;}
        
        public MatrixSegmentOverview(){
            segmentOverview = new Map<String, String>{'Gain'=>'0','Build'=>'0','Defend'=>'0','Observe'=>'0','Maintain'=>'0'};
            gainCovered = 0;
            buildCovered = 0;
            defendCovered = 0;
            observeCovered = 0;
            maintainCovered = 0;
        }
    }
    
    public class MatrixComparison{
        public Map<String, Integer> mapMatrixComparison {get;set;}
        public String IdOldMatrix;
        public String IdNewMatrix;
        public String lastAccID;
        public boolean isException;
        public boolean gotonext;
        public string message;
        
        public MatrixComparison(){
            isException=false;
        }
    }
    
    public class LocaleConfig{
        public String jsPath {get; set;}
        public String locale {get; set;}
        public LocaleConfig(){
            this.locale= Userinfo.getLocale();
            try{
                List<String> list_s = locale.split('_');
                this.locale = list_s[0]+'-'+list_s[1];
            }catch(Exception e){
                this.locale = 'de-DE';
            } 
            this.jsPath = '/glob-cultures/cultures/globalize.culture.'+locale+'.js';
        }
        
    }
}