/**
*   Test class for class  IMP_BI_SegmentComparisonReport.
*
@author Antonio Ferrero
@created 2015-08-28
@version 1.0
@since 20.0
*
@changelog
* 2015-08-28 Antonio Ferrero <antonio.ferrero_varas.ext@boehringer-ingelheim.com>
* - Created
* - Test coverage  
*/

@isTest
public class IMP_BI_SegmentComparisonReport_Test {
    static testMethod void testGetData() {
        
        Country_BI__c cntry = IMP_BI_ClsTestHelp.createTestCountryBI();
        insert cntry;
        
        Account acc = IMP_BI_ClsTestHelp.createTestAccount();
        insert acc;
        
        Cycle_BI__c c1 = IMP_BI_ClsTestHelp.createTestCycle();
        c1.Name = 'c1 test';
        c1.Country_Lkp_BI__c = cntry.Id;
        insert c1;
        
        Cycle_BI__c c2 = IMP_BI_ClsTestHelp.createTestCycle();
        c2.Name = 'c2 test';
        c2.Country_Lkp_BI__c = cntry.Id;
        insert c2;
        
        Product_vod__c p = IMP_BI_ClsTestHelp.createTestProduct();
        insert p;
        
        RecordType mType = [SELECT Id FROM RecordType WHERE SobjectType = 'Matrix_BI__c' AND Name LIKE '%HCP%' limit 1];
        
        Matrix_BI__c m1 = IMP_BI_ClsTestHelp.createTestMatrix();
        m1.Size_BI__c = '10x11';
        m1.Column_BI__c = 11;
        m1.Row_BI__c = 10;
        m1.Status_BI__c = 'Draft';
        m1.Cycle_BI__c = c1.Id;
        m1.Product_Catalog_BI__c = p.Id;
        m1.RecordTypeId = mType.Id;
        m1.Current_BI__c = true;
        insert m1;
        
        Matrix_BI__c m2 = IMP_BI_ClsTestHelp.createTestMatrix();
        m2.Size_BI__c = '10x11';
        m2.Column_BI__c = 11;
        m2.Row_BI__c = 10;
        m2.Status_BI__c = 'Draft';
        m2.Cycle_BI__c = c2.Id;
        m2.Product_Catalog_BI__c = p.Id;
        m2.RecordTypeId = mType.Id;
        m2.Current_BI__c = true;
        insert m2;
        
        Matrix_Cell_BI__c mc1 = IMP_BI_ClsTestHelp.createTestMatrixCell();
        mc1.Matrix_BI__c = m1.Id;
        mc1.Row_BI__c = 2;
        mc1.Column_BI__c = 3;
        mc1.Total_Customers_BI__c = 25;
        mc1.Segment_BI__c = 'Build';
        insert mc1;
        
        Matrix_Cell_BI__c mc2 = IMP_BI_ClsTestHelp.createTestMatrixCell();
        mc2.Matrix_BI__c = m2.Id;
        mc2.Row_BI__c = 2;
        mc2.Column_BI__c = 3;
        mc2.Total_Customers_BI__c = 23;
        mc2.Segment_BI__c = 'Defend';
        insert mc2;
        
        Cycle_Data_BI__c cd1 = IMP_BI_ClsTestHelp.createTestCycleData();
        cd1.Account_BI__c = acc.Id;
        cd1.Matrix_1_BI__c = m1.Id;
        cd1.Matrix_Cell_1_BI__c = mc1.Id;
        cd1.Cycle_BI__c = c1.Id;
        cd1.Product_Catalog_BI__c= p.Id;
        insert cd1;
        
        Cycle_Data_BI__c cd2 = IMP_BI_ClsTestHelp.createTestCycleData();
        cd2.Account_BI__c = acc.Id;
        cd2.Matrix_1_BI__c = m2.Id;
        cd2.Matrix_Cell_1_BI__c = mc2.Id;
        cd2.Cycle_BI__c = c2.Id;
        cd2.Product_Catalog_BI__c= p.Id;
        insert cd2;
        
        Test.startTest();
        
        ApexPages.StandardController ctrl = new ApexPages.StandardController(cntry);
        ApexPages.currentPage().getParameters().put('cId', cntry.Id);
        IMP_BI_SegmentComparisonReport seg = new IMP_BI_SegmentComparisonReport(ctrl);
        
        seg.getOldCycles();
        seg.IdOldCycle = c1.Id;
        seg.action_loadOldMatrices();
        seg.IdOldMatrix = m1.Id;
        seg.action_loadNewCycles();
        seg.IdNewCycle = c2.Id;
        seg.action_loadNewMatrices();
        seg.IdNewMatrix = m2.Id;
        seg.action_getNewMatrix();
        
        //Remote action starting compare matrices
        String jsonMatrixComparison =  '{"IdOldMatrix":"'+m1.Id+'","IdNewMatrix":"'+m2.Id+'"}';
        IMP_BI_SegmentComparisonReport.compareMatrixSegment(jsonMatrixComparison);
        
        Test.stopTest();
    }
}