/**
* ===================================================================================================================================
*                                   IMMPaCT BI
* ===================================================================================================================================
*  Decription:      Class to handle Target Detail logic on trigger
*  @author:         Jefferson Escobar
*  @created:        17-Sep-2016
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:			    37.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         17-Sep-2016                 jescobar                 	  Construction of the handler class.
*/
public without sharing class IMP_BI_TargetDetailHandler extends IMP_BI_TriggerHandler{

  public IMP_BI_TargetDetailHandler() {
    //this.setMaxLoopCount(1);
  }

  public override void beforeInsert() {
    setICEFormatDates((List<Target_Detail_BI__c>) Trigger.new);
  }

  public override void beforeUpdate() {
    setICEFormatDates((List<Target_Detail_BI__c>) Trigger.new);
  }

  public void setICEFormatDates(List<Target_Detail_BI__c> targetDetails){
    if(targetDetails != null && !targetDetails.isEmpty()){
      //Get Start Data and End date Cycle
      Portfolio_BI__c portfolio = [SELECT Id, Cycle_BI__r.Start_Date_BI__c, Cycle_BI__r.End_Date_BI__c
                                    FROM Portfolio_BI__c WHERE Id = :targetDetails.get(0).Portfolio_BI__c];
      //Parse dates to UNIX format
      Date starDate = portfolio.Cycle_BI__r.Start_Date_BI__c;
      Date endDate = portfolio.Cycle_BI__r.End_Date_BI__c;
      Long xStartDate = DateTime.newInstanceGmt(starDate.year(), starDate.month(),starDate.day()).getTime();
      Long xEndDate = DateTime.newInstanceGmt(endDate.year(), endDate.month(),endDate.day()).getTime();

      for(Target_Detail_BI__c detail :  targetDetails){
          detail.x_cyclestartdate_BI__c = (xStartDate/1000); //Divide by milisecods
          detail.x_cycleenddate_BI__c = (xEndDate/1000);//Divide by milisecods
      }
    }
  }
}