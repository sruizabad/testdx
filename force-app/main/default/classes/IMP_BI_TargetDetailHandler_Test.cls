/**
* ===================================================================================================================================
*                                   IMMPaCT BI
* ===================================================================================================================================
*  Decription:      Test for IMP_BI_TargetDetailHandler class
*  @author:         Jefferson Escobar
*  @created:        20-Sep-2016
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:          37.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         20-Sep-2016                 jescobar                    Construction of the class.
*/
@isTest
public class IMP_BI_TargetDetailHandler_Test {

  static testMethod void testSetActivePortfolio() {
      Country_BI__c c = IMP_BI_ClsTestHelp.createTestCountryBI();
      insert c;

      Account acc = IMP_BI_ClsTestHelp.createTestAccount();
      acc.Name = 'Acc-test';
      insert acc;

      Cycle_BI__c cycle2 = new Cycle_BI__c();
      cycle2.Country_BI__c = 'US';
      cycle2.Start_Date_BI__c = date.today() - 10;
      cycle2.End_Date_BI__c = date.today() + 10;
      cycle2.IsCurrent_BI__c = false;
      cycle2.Country_Lkp_BI__c = c.Id;
      insert cycle2;

      Portfolio_BI__c portfolio = IMP_BI_ClsTestHelp.createTestPortfolio();
      portfolio.Cycle_BI__c = cycle2.Id;
      insert portfolio;

      Target_Account_BI__c targetAcc = IMP_BI_ClsTestHelp.createTestTargetAccountBI();
      targetAcc.Account_BI__c = acc.Id;
      targetAcc.Portfolio_BI__c = portfolio.Id;
      insert targetAcc;

      Target_Detail_BI__c targeDetail = new Target_Detail_BI__c (Account_BI__c = acc.Id, Portfolio_BI__c = portfolio.Id, Target_Account_BI__c = targetAcc.Id );
      insert targeDetail;

  }

}