/**
 * Batch job to clear scenario number
 * 
 * @author Yuanyuan Zhang
 * @created 2015-01-22
 * @version 1.0
 * @since 32.0
 * 
 * @changelog
 * 2015-01-22 Yuanyuan Zhang <yuanyuan.zhang@itbconsult.com>
 * - Created   
 *
 */
 
 global class IMP_BI_refreshCDForManageScen implements Database.Batchable<SObject>, Database.Stateful{
     global String query;
     global String filter; 
     global String scenCell;
     global String firstScenarioId;
     global set<id> set_cellId;
     global Integer currentNum;
     global map<String, String> map_cellId_matrixId;//use to fill matrix_bi__c field on cycledata for current matrix 
     
     global Database.QueryLocator start(Database.BatchableContext BC){ 
        if(query == null || query == '') {
            
           query = 'select Id,Column_BI__c,Row_BI__c, Matrix_BI__c, Current_Scenario_BI__c, Matrix_Cell_1_BI__c, Matrix_Cell_2_BI__c, Matrix_Cell_3_BI__c, ' + 
                   'Matrix_Cell_4_BI__c, Matrix_Cell_5_BI__c,Matrix_Cell_1_BI__r.Matrix_BI__c, Matrix_Cell_2_BI__r.Matrix_BI__c, ' +
                   'Matrix_Cell_3_BI__r.Matrix_BI__c,Matrix_Cell_4_BI__r.Matrix_BI__c,Matrix_Cell_5_BI__r.Matrix_BI__c FROM Cycle_Data_BI__c '; 
           query += ' where Matrix_Cell_1_BI__c IN :set_cellId OR Matrix_Cell_2_BI__c IN :set_cellId OR Matrix_Cell_3_BI__c IN :set_cellId ' +
                    ' OR Matrix_Cell_4_BI__c IN :set_cellId OR Matrix_Cell_5_BI__c IN :set_cellId';
           if(filter != null) query += filter;
        }  
        map_cellId_matrixId = new map<String, String>();
        for (Matrix_Cell_BI__c mc : [select id, Matrix_BI__c From Matrix_Cell_BI__c where id IN :set_cellId]) {
            map_cellId_matrixId.put(mc.Id, mc.Matrix_BI__c);
        }
        String jobId = BC.getJobId();
        
        //user first scenario matrix id as identify for the whole scenario matrix
        //todo : if first scenario matrix is delete?
        List<IMP_BI_Config_Matrix__c> list_imp = new List<IMP_BI_Config_Matrix__c>();
        
        IMP_BI_Config_Matrix__c ibcm = new IMP_BI_Config_Matrix__c();
        ibcm.Matrix_Id__c = firstScenarioId;
        ibcm.Batch_Process_Id__c = jobId;
        ibcm.Name = firstScenarioId + jobId;
        insert ibcm;
        
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext BC, list<Cycle_Data_BI__c> list_cycleData){
        if (scenCell != null && scenCell != '') {
            
            List<String> list_scencell = scenCell.split('zzzz');
            for (String scencell : list_scencell) {
                String scennum = scencell.split('yyyy')[0];
                String cellCombi = scencell.split('yyyy')[1];
                system.debug('####scennum: ' + scennum);
                map<String, String> map_colRow_cellId = new map<String, String>();
                for (String crc : cellCombi.split(';')) {
                    List<String> list_split = crc.split(',');
                    map_colRow_cellId.put(list_split[0]+';'+list_split[1], list_split[2]);
                }
                system.debug('####map_colRow_cellId: ' + map_colRow_cellId);
                for (Cycle_Data_BI__c cdb : list_cycleData) {//Column_BI__c,Row_BI__c,
                    String key = cdb.Column_BI__c.intValue() + ';' + cdb.Row_BI__c.intValue();
                    system.debug('####key: ' + key);
                    system.debug('####map_colRow_cellId.containsKey(key): ' + map_colRow_cellId.containsKey(key));
                    if (map_colRow_cellId.containsKey(key)) {
                        cdb.put('Matrix_Cell_'+scennum+'_BI__c', map_colRow_cellId.get(key));
                    }
                    
                    cdb.Current_Scenario_BI__c = null;
                    cdb.Matrix_BI__c = null;
                    if (currentNum != 0) {
                        cdb.Current_Scenario_BI__c = currentNum;
                        cdb.Matrix_BI__c = map_cellId_matrixId.get(String.valueOf(cdb.get('Matrix_Cell_'+currentNum+'_BI__c')));
                    }
                    
                }
            }
            system.debug('####list_cycleData: ' + list_cycleData);
            update list_cycleData;
        }
     }
     
     
     
     global void finish(Database.BatchableContext BC){
        for (IMP_BI_Config_Matrix__c cm : [SELECT Matrix_Id__c, Batch_Process_Id__c FROM IMP_BI_Config_Matrix__c WHERE Matrix_Id__c =:firstScenarioId]) {
            delete cm;
        }
        
     }
 }