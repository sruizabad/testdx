/*
* InteractionLogControllerMVN
* Created By: Roman Lerman
* Created Date: 1/18/2013
* Description: This class is used for saving the text in the interaction log as well as for creating requests
*/
public with sharing class InteractionLogControllerMVN {
	public Case parentCase{get; set;}
	public Case childCase{get; set;}

    public Boolean needsToBeSaved {get;set;}
    public Boolean caseIsClosed {get;set;}
    public Boolean caseIsVeeva {get;set;}
    public Boolean caseIsWeb {get;set;}
    public Boolean hideAll {get;set;}
    public String savedMessage {get;set;}
    public String savedErrorMessage {get;set;}
    public String interactionNotes {get;set;}
    public Boolean hasSaveError {get;set;}
    public Boolean attemptedOneSave {get;set;}
    public Boolean isSaving {get;set;}
    public String   lastSavedDateTime {get;set;}
    
	public InteractionLogControllerMVN(ApexPages.StandardController controller){
        needsToBeSaved = false;
        caseIsVeeva = false;
        caseIsClosed = false;
        hideAll = false;
        savedMessage = '';
        savedErrorMessage = '';
        interactionNotes = '';
        hasSaveError = false;
        attemptedOneSave = false;
        isSaving = false;
        
        parentCase = [SELECT Id, Interaction_Notes_MVN__c, Business_Account_MVN__c, Sales_Rep_MVN__c, Requester_Type_MVN__c, Address_MVN__c, RecordTypeId, AccountId, isClosed, Origin FROM Case WHERE Id = :controller.getRecord().Id];
        
        if (parentCase != null) {
            
            if (parentCase.Interaction_Notes_MVN__c != null) {
                interactionNotes = parentCase.Interaction_Notes_MVN__c;
            }
           
            caseIsClosed = parentCase != null ? parentCase.isClosed : true;
        }
    }
    
    public PageReference createChildCaseFromInteractionLog(){
        childCase = new Case();
    	childCase.ParentId = parentCase.Id;
    	childCase.Origin = parentCase.Origin;
    	
        if (parentCase.AccountId != null) {
        	childCase.AccountId = parentCase.AccountId;
        	childCase.Address_MVN__c = parentCase.Address_MVN__c;
        }
        if(parentCase.Business_Account_MVN__c != null) {
            childCase.Business_Account_MVN__c = parentCase.Business_Account_MVN__c;
        }
        
        childCase.Sales_Rep_MVN__c = parentCase.Sales_Rep_MVN__c;
        childCase.Requester_Type_MVN__c = parentCase.Requester_Type_MVN__c;

        childCase.Origin = parentCase.Origin;

    	childCase.RecordTypeId = [select Id from RecordType where SObjectType='Case' and DeveloperName =: UtilitiesMVN.requestRecordType].Id;
		
		try{
			insert childCase;
		}catch(Exception e){
			ApexPages.addMessages(e);
		}
		return null;
    }

    public PageReference setToTrue () {
        needsToBeSaved = true;
        savedMessage = '';
        return null;
    }
    
    public PageReference showStatusMessage () {
        if(needsToBeSaved) {
            isSaving = true;
        }
        return null;
    }
    
    public PageReference saveInteractionNotes () {
        if (needsToBeSaved) {
            attemptedOneSave = true;
            hasSaveError = false;
            parentCase.Interaction_Notes_MVN__c = interactionNotes;
            try { 
                update parentCase;
                needsToBeSaved = false;
                savedErrorMessage = '';
                lastSavedDateTime = system.now().format();
            }
            catch (Exception e) {
                if (e.getMessage().contains('STRING_TOO_LONG')) {
                    //savedErrorMessage = System.Label.Interaction_Log_To_Long;
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.Interaction_Log_To_Long));
                } else {
                    //savedErrorMessage = System.Label.Generic_Interaction_Log_Save_Error;
                    //savedErrorMessage = e.getMessage();
                    ApexPages.addMessages(e);
                }
                needsToBeSaved = true;
                hasSaveError = true;
                savedMessage = '';
            }
        }
        
        isSaving = false;
        return null;
    }
}