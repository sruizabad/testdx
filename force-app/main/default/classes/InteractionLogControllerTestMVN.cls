/*
* InteractionLogControllerTestMVN
* Created By: Roman Lerman
* Created Date: 1/18/2013
* Description: This is the test class for the InteractionLogControllerMVN
*/
@isTest
private class InteractionLogControllerTestMVN {
	static InteractionLogControllerMVN extension;
	static Case cs;

	static{
		TestDataFactoryMVN.createSettings();
		
		cs = TestDataFactoryMVN.createTestCase();
		
		ApexPages.StandardController con = new ApexPages.StandardController(cs);
		extension = new InteractionLogControllerMVN(con);
	}
	static testMethod void testCreateChildRequest(){
		Test.startTest();	
			extension.createChildCaseFromInteractionLog();
		Test.stopTest();
		
		cs = [select AccountId, Address_MVN__c, Origin, Requester_Type_MVN__c from Case where Id = :cs.Id];
		Case newRequest = [select AccountId, Address_MVN__c, ContactId, Origin, Requester_Type_MVN__c from Case where ParentId = :cs.Id];
		
		System.assertEquals(cs.AccountId, newRequest.AccountId);
		System.assertEquals(cs.Address_MVN__c, newRequest.Address_MVN__c);
		System.assertEquals(cs.Origin, newRequest.Origin);
	}
	static testMethod void testMessages(){
		Test.startTest();
			extension.setToTrue();
			extension.showStatusMessage();
		Test.stopTest();
		
		System.assertEquals(true, extension.needsToBeSaved);
		System.assertEquals('', extension.savedMessage);
		System.assertEquals(true, extension.isSaving);
		
	}
	static testMethod void testSaveInteractionNotes(){
		extension.needsToBeSaved = true;
		extension.interactionNotes = 'This is a test.';
		Test.startTest();
			extension.saveInteractionNotes();
		Test.stopTest();
		
		System.assertEquals([select Interaction_Notes_MVN__c from Case where Id = :cs.Id].Interaction_Notes_MVN__c, 'This is a test.');
	}
	static testMethod void testSaveInteractionNotesTooLong(){
		extension.needsToBeSaved = true;
		extension.interactionNotes = 'This is a test.';
		
		for(Integer x = 0; x < 3000; x++){
			extension.interactionNotes += 'This is a test.';
		}
		
		Test.startTest();
			extension.saveInteractionNotes();
		Test.stopTest();
		
		System.assertEquals(true, extension.hasSaveError);
	}
}