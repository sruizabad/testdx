public with sharing class KnowledgeSearchControllerMITS {

   

    //public PageReference knowledgeSearch() {
        //return null;
   // }

public String articleSearchText{get;set;}
public String productName_MITS{get;set;}
//public String knowledgeSearchaction{get;set;}
//public String products{get;set;}
public String articleType{get;set;}
public List<String> articleTypes {get;set;}
public Integer maximumResults{get;set;}
public String selectedArticleId {get;set;}
 //public Case presentcase {get;set;}
//public String items{get;set;}
public Case presentCase{get;set;}
public String language{get;set;}
public String productnamestr{get;set;}
public List<SelectOption>   languages {get; set;}
//public String knowledgeSearch{get;set;}
//public String knowledgeSearchMITS{get;set;}
public  List<Case_Article_Data_MVN__c> knowledgeList{get;set;}
public integer size{get;set;}
public String knowledgeProductType {get; set;}
public string queryString{get;set;}
public user currentuser{get;set;}

          private KnowledgeSearchUtilityMITS knowledgeSearchUtility = new KnowledgeSearchUtilityMITS();
          private Map<String,Case_Article_Fields_MVN__c> articleFields = Case_Article_Fields_MVN__c.getAll();
   
   public KnowledgeSearchControllerMITS(ApexPages.StandardController controller) {
   currentuser=new User();
   currentuser=[Select Id,Name,Country_Code_BI__c from User where Id=:userinfo.getuserId()];
   List<CaseArticle> csart= new List<CaseArticle>();
   
        csart=[Select Id,KnowledgeArticleId,CaseId from CaseArticle];
       system.debug('cs values are------->'+csart);
   
   knowledgeList = new List<Case_Article_Data_MVN__c>();
        languages = new List<SelectOption>();
        this.presentcase= [SELECT Id, CaseNumber, Product_New_MITS__c, Product_New_MITS__r.External_ID_MITS__c FROM Case where Id = :controller.getId()];
        system.debug('Present Case values are---->'+this.presentcase);
   
      if (this.presentCase.Product_New_MITS__c != null) {
      system.debug('Yes in if loop');
            productName_MITS = presentcase.Product_New_MITS__c;
            system.debug('Product Name is ---->'+productName_MITS);
        }
        
         for (Schema.PicklistEntry entry : KnowledgeArticleVersion.Language.getDescribe().getPicklistValues()) {
         system.debug('^^^^^^^^^'+entry);
            if(entry.isActive()){
                string label = entry.getLabel();
                system.debug('Label is---->'+label);
                string value = entry.getValue();
                system.debug('value is---->'+value);
                language = 'en_US';
                languages.add(new SelectOption(value,Label));
                //languages.add(new SelectOption());
              system.debug('Languages are--->'+languages) ; 
            }
        
        }
        
      setCustomSettings();  
    }

      public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('All','All'));
            for (String articleType:articleTypes){
                options.add(new SelectOption(articleType, Schema.getGlobalDescribe().get(articleType).getDescribe().getLabel()));
            }
  
            return options;
    }
    
      public List<SelectOption> getproducts(){
        List<Product_MITS__c> productList = [SELECT Id, Name, IsDeleted, External_ID_MITS__c FROM Product_MITS__c ORDER BY Name];
        system.debug('Product List Values are*******'+productList);
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','All'));
        for (Product_MITS__c p : productList){
            options.add(new SelectOption(p.External_ID_MITS__c != null ? p.External_ID_MITS__c : p.Id, p.Name));
            system.debug('option values are------>'+options);
        }
        
        return options;
    }

    public PageReference selectArticleMITS(){
    for(Case_Article_Data_MVN__c ksr : knowledgeList) {
            if(ksr.Knowledge_Article_ID_MVN__c == selectedArticleId) {
                knowledgeSearchUtility.selectArticle(ksr, presentcase.Id);
            }
        }
    return null;
    }
    
     public void setCustomSettings(){
        Service_Cloud_Settings_MITS__c serviceCloudCustomSettings = Service_Cloud_Settings_MITS__c.getInstance();
        articleTypes = UtilitiesMITS.splitCommaSeparatedString(serviceCloudCustomSettings.Knowledge_Search_Article_Types_MITS__c);
        maximumResults = (Integer)serviceCloudCustomSettings.Knowledge_Search_Max_Results_MITS__c;
        //knowledgeProductType=serviceCloudCustomSettings.Knowledge_Product_Name__c;
        //knowledgeProductType = serviceCloudCustomSettings.Knowledge_Product_Type_MITS__c;
        //system.debug('knowledgeProductType value is+++++++'+knowledgeProductType);
    }
    
    
     public PageReference knowledgeSearchMITS() {
     system.debug('Yes in Method');
     if((articleSearchText == null || articleSearchText == '') && (productName_MITS== null || productName_MITS== '')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Search Term must have 2 Characters'));
            return null;
        }
      if(articleSearchText != null && articleSearchText.replaceAll('\\*', '').length() == 1){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'SEARCH TERM MUST HAVE 2 CHARACTERS'));
            return null;
        }  
      
      //articleSearchText = String.escapeSingleQuotes(articleSearchText);

        System.debug('Article Search Text value: '+articleSearchText);
        
        
//String.valueOf(productname[0].Name)
//1. name get productName_MITS


        
        
        
        String queryString = knowledgeSearchUtility.buildQueryStringMITS(articleType, articleSearchText,productName_MITS, language);
        System.debug('Query String: '+queryString);  
        
        knowledgeList = knowledgeSearchUtility.knowledgeSearch(queryString,presentcase.Id);
        
        System.debug('Knowledge Articles Found: '+knowledgeList);
        
        /* Start 
        
        system.debug('+++++++'+productName_MITS);
        system.debug('******'+articleType);
        if((articleSearchText == null || articleSearchText == ''))
        {
        knowledgeList = [SELECT Id,Article_Title_MVN__c,Article_Number_MVN__c,Article_URL_Name_MVN__c,Article_Type_API_MVN__c,
                                                                Article_Summary_MVN__c,Article_Type_MVN__c,Knowledge_Article_ID_MVN__c,Article_Language_MVN__c,Article_Indication_MVN__c,Article_Category_MVN__c 
                                                                FROM Case_Article_Data_MVN__c];
         System.debug('Query String=>>>>>>>>>>>: '+knowledgeList );  
         }
        */
      
     
        return null;
    }

    
    
}