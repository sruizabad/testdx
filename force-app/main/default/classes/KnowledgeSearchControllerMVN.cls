/*
* KnowledgeSearchControllerMVN
* Created By: Vincent Reeder
* Created Date: January 20, 2012
* Description: Controller Article search
*/

public with sharing class KnowledgeSearchControllerMVN {
    public String               articleType {get;set;}
    public String               productName {get;set;}
    public String               articleSearchText {get;set;}
    public String               language {get;set;}
    public Integer              maximumResults{get;set;}
    public String               selectedArticleId {get;set;}
    public Case                 currentCase {get;set;}
    public List<String>         articleTypes {get;set;}
    public List<Case_Article_Data_MVN__c> knowledgeList{get; set;}
    public List<SelectOption>   languages {get; set;}
    public String               knowledgeProductType {get; set;}
    
    private KnowledgeSearchUtilityMVN knowledgeSearchUtility = new KnowledgeSearchUtilityMVN();
    private Map<String,Case_Article_Fields_MVN__c> articleFields = Case_Article_Fields_MVN__c.getAll();

    public KnowledgeSearchControllerMVN(ApexPages.StandardController controller) {
        knowledgeList = new List<Case_Article_Data_MVN__c>();
        languages = new List<SelectOption>();
        this.currentCase = [SELECT Id, Subject, isClosed, CaseNumber, AccountId, Product_MVN__c, Product_MVN__r.External_ID_vod__c FROM Case where Id = :controller.getId()];
        
        if (currentCase.Product_MVN__c != null) {
            productName = currentCase.Product_MVN__r.External_ID_vod__c;
        }
        
        String languageLabel = [select Default_Article_Language_MVN__c from User where Id = :UserInfo.getUserId()].Default_Article_Language_MVN__c;
        
        for (Schema.PicklistEntry entry : KnowledgeArticleVersion.Language.getDescribe().getPicklistValues()) {
            if(entry.isActive()){
                string label = entry.getLabel();
                string value = entry.getValue();
                languages.add(new SelectOption(value, label));
                
                if(languageLabel == value){
                    language = value;
                }
            }
        }
        
        setupCustomSettings();
    }

    public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('All','All'));
            for (String articleType:articleTypes){
                options.add(new SelectOption(articleType, Schema.getGlobalDescribe().get(articleType).getDescribe().getLabel()));
            }
  
            return options;
    }
     
     public List<SelectOption> getProducts(){
        List<Product_vod__c> productList = [SELECT Id, Name, IsDeleted, External_ID_vod__c FROM Product_vod__c WHERE Product_Type_vod__c = :knowledgeProductType AND (End_Date_MVN__c>=TODAY OR End_Date_MVN__c=null) ORDER BY Name];
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','All'));
        for (Product_vod__c p : productList){
            options.add(new SelectOption(p.External_ID_vod__c != null ? p.External_ID_vod__c : p.Id, p.Name));
        }
        
        return options;
    }
    
    public PageReference selectArticle(){
        for(Case_Article_Data_MVN__c ksr : knowledgeList) {
            if(ksr.Knowledge_Article_ID_MVN__c == selectedArticleId) {
                knowledgeSearchUtility.selectArticle(ksr, currentCase.Id);
            }
        }
        return null;
    }
    
    public void setupCustomSettings(){
        Service_Cloud_Settings_MVN__c serviceCloudCustomSettings = Service_Cloud_Settings_MVN__c.getInstance();
        articleTypes = UtilitiesMVN.splitCommaSeparatedString(serviceCloudCustomSettings.Knowledge_Search_Article_Types_MVN__c);
        maximumResults = (Integer)serviceCloudCustomSettings.Knowledge_Search_Max_Results_MVN__c;
        knowledgeProductType = serviceCloudCustomSettings.Knowledge_Product_Type_MVN__c;
    }

    public PageReference knowledgeSearch(){
        if((articleSearchText == null || articleSearchText == '') && (productName == null || productName == '')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Search_Term_Must_Have_2_Characters));
            return null;
        }
        if(articleSearchText != null && articleSearchText.replaceAll('\\*', '').length() == 1){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Search_Term_Must_Have_2_Characters));
            return null;
        }
        
        articleSearchText = String.escapeSingleQuotes(articleSearchText);
        
        String queryString = knowledgeSearchUtility.buildQueryString(articleType, articleSearchText, productName, language);
        System.debug('Query String: '+queryString);
        
        knowledgeList = knowledgeSearchUtility.knowledgeSearch(queryString,currentCase.Id);
        
        System.debug('Knowledge Articles Found: '+knowledgeList);
        
        return null;
    }
}