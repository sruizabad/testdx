/*
* KnowledgeSearchControllerTestMVN
* Created By: Roman Lerman
* Created Date: 3/6/2013
* Description: This is the test class for the KnowledgeSearchControllerMVN
*/
@isTest
private class KnowledgeSearchControllerTestMVN {
	// Setup the data
	static {
		TestDataFactoryMVN.createSettings();
	    Test.setFixedSearchResults(TestDataFactoryMVN.setupArticles());
    }
	
	static Case request;
	static KnowledgeSearchControllerMVN knowledgeSearchController;
	static Case_Article_Data_MVN__c foundArticle;
	//***********	
	// Scenario 1: Knowledge Articles should be searched by a search term
	static testMethod void knowledgeArticlesShouldBeSearchedByTerm(){
		customerAsksQuestion();
		enterASearchTerm();
		searchTheArticles();
		theArticlesReturnedShouldContainTerm();
	}
	
	// Given a customer asks a question
	static void customerAsksQuestion(){
		// Create a new request
		Case interaction = TestDataFactoryMVN.createTestCase();
		request = TestDataFactoryMVN.createTestRequest(interaction);
	}
	
	// And I enter a search term
	static void enterASearchTerm(){
		// Enter a search term
		ApexPages.standardController stdController = new ApexPages.standardController(request);
		knowledgeSearchController = new KnowledgeSearchControllerMVN(stdController);
		knowledgeSearchController.articleType = 'All';
		knowledgeSearchController.language = 'en_US';
		knowledgeSearchController.articleSearchText = 'test';
	}
	
	// When I search the articles
	static void searchTheArticles(){
		// Search the articles
		knowledgeSearchController.knowledgeSearch();
	}
	
	// Then only articles that contain the search term of type FAQ which also contain the search term should be displayed
	static void theArticlesReturnedShouldContainTerm(){
		// Verify that the correct articles were returned
		System.debug('KnowledgeSearchList: '+knowledgeSearchController.knowledgeList);
		for(Case_Article_Data_MVN__c knowledgeResultArticle:knowledgeSearchController.knowledgeList){
			System.assert(knowledgeResultArticle.Article_Title_MVN__c.contains('test'));
		}
	}
//***********	
	// Scenario 2: Knowledge Articles should be filtered for quicker searching
	static testMethod void knowledgeArticlesShouldBeSearchedByTermAndType(){
		customerAsksQuestion();
		enterASearchTerm();
		filterForFAQs();
		searchTheArticles();
		theArticlesReturnedShouldBeFAQsAndContainTerm();
	}
	
	// And I filter for FAQs
	static void filterForFAQs(){
		// Specify a filter
		knowledgeSearchController.articleType = 'FAQ_MVN__kav';
	}
	
	// Then only articles that contain the search term of type FAQ which also contain the search term should be displayed
	static void theArticlesReturnedShouldBeFAQsAndContainTerm(){
		// Verify that the correct articles were returned
		for(Case_Article_Data_MVN__c knowledgeResultArticle:knowledgeSearchController.knowledgeList){
			System.assertEquals('FAQ', knowledgeResultArticle.Article_Type_MVN__c);
			System.assert(knowledgeResultArticle.Article_Title_MVN__c.contains('test'));
		}
	}
//*******************
	// Scenario 3:  Knowledge Articles should be attached once found
	static testMethod void knowledgeArticlesShouldBeAttachedOnceFound(){
		customerAsksQuestion();
		enterASearchTerm();
		searchTheArticles();
		findTheArticleImLookingFor();
		selectTheArticle();
		theArticleIsAttached();
	}
	// And I find the article I'm looking for
	static void findTheArticleImLookingFor(){
		foundArticle = knowledgeSearchController.knowledgeList[0];
	}
	// When select the article
	static void selectTheArticle(){
		knowledgeSearchController.selectedArticleId = foundArticle.Knowledge_Article_Id_MVN__c;
		knowledgeSearchController.selectArticle();
	}
	// The article is attached
	static void theArticleIsAttached(){
		List<Case_Article_Data_MVN__c> attachedArticle = [select Id,Knowledge_Article_Id_MVN__c from Case_Article_Data_MVN__c where Case_MVN__c = :request.Id];
		System.assertEquals(attachedArticle[0].Knowledge_Article_Id_MVN__c, foundArticle.Knowledge_Article_Id_MVN__c);
		System.assertEquals(1,[Select Id from Case_Article_Data_MVN__c].size());
	}

//*******************
	// Scenario 4:  The article is selected twice
	static testMethod void knowledgeArticlesErrorsWhenSelectedTwice(){
		customerAsksQuestion();
		enterASearchTerm();
		searchTheArticles();
		findTheArticleImLookingFor();
		selectTheArticle();
		theArticleIsAttached();
		selectTheArticleAgainAndReceiveError();
	}
	static void selectTheArticleAgainAndReceiveError(){
		try{
			knowledgeSearchController.selectArticle();
		}catch(Exception e){
			System.assert(e.getMessage().contains(Label.Service_Cloud_Knowledge_Search_Article_Already_Attached));
		}
	}

//*******************
	// Scenario 5:  Search for an article without a product defined
	static testMethod void knowledgeArticlesNoProduct(){
		customerAsksQuestion();
		enterASearchTerm();
		knowledgeSearchController.productName = 'test';
		searchTheArticles();
		theArticlesReturnedShouldContainTerm();
	}
	
//********************
	static testMethod void testControllerMethods(){
		Case interaction = TestDataFactoryMVN.createTestCase();
		request = TestDataFactoryMVN.createTestRequest(interaction);
		
		
		
		System.runAs(TestDataFactoryMVN.createTestAdmin()){
			ApexPages.standardController stdController = new ApexPages.standardController(request);
			knowledgeSearchController = new KnowledgeSearchControllerMVN(stdController);
			System.assertEquals(KnowledgeArticleVersion.Language.getDescribe().getPicklistValues()[0].getValue(), knowledgeSearchController.language);
		}
		
		
		
		knowledgeSearchController.getItems();
		System.assertEquals(knowledgeSearchController.articleTypes.size() + 1, knowledgeSearchController.getItems().size());
		
		System.assertEquals(KnowledgeArticleVersion.Language.getDescribe().getPicklistValues().size(), knowledgeSearchController.languages.size());
		
		insert new Product_vod__c(Name='Test Product', Product_Type_vod__c = 'Detail');
		
		knowledgeSearchController.getProducts();
		System.assertEquals([SELECT count() FROM Product_vod__c WHERE IsDeleted = false] + 1, knowledgeSearchController.getProducts().size());
	}
}