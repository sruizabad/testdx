/*
* KnowledgeSearchUtilityMITS
* Created By: Kiran Kumar
* Created Date: 8/26/2015
* Description: This class is used for building out the quries for the knowledge search in MITS
*/
public with sharing class KnowledgeSearchUtilityMITS {

   public static final String StandardFields= 'ArticleNumber, UrlName, Title, KnowledgeArticleId, Id, Language, Summary, ArticleType,VersionNumber';
   public static final Map<String,Case_Article_Fields_MVN__c> articleFields = Case_Article_Fields_MVN__c.getAll();
   
    public List<String> MITSarticleTypes;
    public Service_Cloud_Settings_MITS__c serviceCloudCustomSettingsMITS;
    public Integer maximumResults;
    
    public KnowledgeSearchUtilityMITS () {
        serviceCloudCustomSettingsMITS = Service_Cloud_Settings_MITS__c.getInstance();
        MITSarticleTypes = UtilitiesMVN.splitCommaSeparatedString(serviceCloudCustomSettingsMITS.Knowledge_Search_Article_Types_MITS__c);
        system.debug('**********'+MITSarticleTypes );
        maximumResults = (Integer)serviceCloudCustomSettingsMITS.Knowledge_Search_Max_Results_MITS__c;
    }
    
    public PageReference removeArticle(Case_Article_Data_MVN__c cad) {
        delete cad;

        return null;
    }
    
    public List<Case_Article_Data_MVN__c> knowledgeSearch(String queryString, ID caseID){
        List<Case_Article_Data_MVN__c> knowledgeList = new List<Case_Article_Data_MVN__c>();
        list<list<SObject>> allResults = new list<list<SObject>> ();
        try {
            allResults = search.query(queryString);
        }
        catch (Exception e) {
            ApexPages.addMessages(e);
            return knowledgeList;
        }
       Integer totalRecordSize = 0;
        
        for(List<SObject> lst:allResults){
            totalRecordSize += lst.size();
        }

        System.debug('Total number of articles found: '+totalRecordSize);

        if(totalRecordSize > maximumResults){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Service_Cloud_Knowledge_Search_Max_Records_Found));
            return null;
        }

        for (list<SObject> objectResults : allResults) {
            for (SObject obj : objectResults) {
                Case_Article_Data_MVN__c result = createCaseArticle(caseID, obj);
          system.debug('@@@@@@'+result);
                knowledgeList.add(result);
            }
        }
        return knowledgeList;
    }
    
    public String buildQueryStringMITS(String articleType, String articleSearchText, String productName_MITS,  String language){
        boolean firstReturningClause = true;
        boolean isAll = articleType == 'All';
        system.debug('####@@@'+articleType);
        system.debug('@@@@@@@'+articleSearchText);
        system.debug('*****@@@@'+productName_MITS);
        system.debug('%%%%%@@@@'+language);
        
     List<Product_MITS__c> productnamelist = [Select Id,Name,External_ID_MITS__c from Product_MITS__c where Id=:productName_MITS Limit 50] ;
      system.debug('List of Products%%%%%%'+productnamelist);
      string productnamestr= '';
      if(productnamelist.size()>0){
      //productnamestr=productnamelist[0].Name;
      
            for(integer i=0;i<productnamelist.size();i++)
            {
                productnamestr= productnamelist[i].Name;
                 system.debug('In For Loop List of Products%%%%%%'+productnamestr);
            }
          }  
        system.debug('Out For Loop List of Products%%%%%%'+productnamestr);
        
        String whereClause = buildWhereClauseMITS(language, productName_MITS);
         System.debug('>>>>>>>>>%%%%%%%whereClause %%%%%%:'+whereClause );
        if((articleSearchText == null || articleSearchText == '') && productName_MITS != null &&productName_MITS != '' && productName_MITS != 'All'){
            articleSearchText = productnamestr; 
            system.debug('$$$$$$$MITS'+articleSearchText);
        }
        
        String findQuery = 'find \'' + articleSearchText + '\'';
        system.debug('!!!!@@@@####'+findQuery);
        
        for(String aType : MITSarticleTypes) {
            if (isAll || articleType.equalsIgnoreCase(aType)) {
                if (firstReturningClause) {
                    findQuery += ' returning ';
                    firstReturningClause = false;
                } else {
                    findQuery += ', ';
                }
system.debug('Testing1*****aType *****'+aType );
                //build type field list
                String typeFields = buildTypeFields(aType);
                
                findQuery += aType + ' (' + StandardFields + typeFields + whereClause + ' ORDER BY Title)';
                System.debug('>>>>>findQuery >>>'+findQuery );
                
            }
        }
        //system.debug('Testing1*****aType *****'+aType );
        findQuery += ' limit ' + maximumResults;
        system.debug('Find Query: '+findquery);
        return findQuery;
    }
    
    public String buildTypeFields(String articleType) {
        String typeFields = '';

        for(Case_Article_Fields_MVN__c caf: articleFields.values()) {
            if(articleType.equalsIgnoreCase(caf.Knowledge_Article_Type_MVN__c)) {
                typeFields += ', ';
                typeFields += caf.Knowledge_Article_Field_MVN__c;
            }
        }

        return typeFields;
    }
    
    public String buildWhereClauseMITS(String language, String productName_MITS) {
        string whereClause = ' where PublishStatus = \'Online\' and IsLatestVersion=true ';
        System.debug('>>>>>>>>>%%%%%%%%%%%%%:'+whereClause );
        if(language != null && language != '') {
            whereClause += ' and Language = \'' + language + '\'';
        }

        if(productName_MITS != null && productName_MITS != '' && productName_MITS != 'All') {
            whereClause += ' and (Product_MITS__c= \'' + productName_MITS + '\')';
            //whereClause += ' and (Product_ID_MVN__c = \'' + productName_MITS + '\'';
            //system.debug('Product Name:'+ whereClause);
           // whereClause += ' or Product_ID_MVN__c = \'\')';
           //system.debug('Product Name:'+ whereClause);
        }  
        
        System.debug('KnowledgeSearchUtility%%%%%%%%%%%%%: '+whereClause);

        return whereClause;
    }
    
    public PageReference selectArticle(Case_Article_Data_MVN__c selectedArticle, Id caseId){
        List<Case_Article_Data_MVN__c> attachedKnowledgeList = queryExistingCaseArticles(caseId);
        for (Case_Article_Data_MVN__c kaa : attachedKnowledgeList) {
            if (kaa.Knowledge_Article_ID_MVN__c == selectedArticle.Knowledge_Article_ID_MVN__c) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Service_Cloud_Knowledge_Search_Article_Already_Attached));
                return null;
            }
        }
    SavePoint sp = Database.setSavePoint();

        try {
            Case_Article_Data_MVN__c articleToInsert = selectedArticle.clone(true, false);
            
            insert articleToInsert;
        } catch (Exception e) {
            Database.rollback(sp);
            ApexPages.addMessages(e);
        }

        return null;
    }

    //Lots of queries here. cache? We're not setting the Knowledge Article version
    public List<Case_Article_Data_MVN__c> queryExistingCaseArticles(Id caseId) {
        List<CaseArticle> caseArticles = [SELECT CaseId, KnowledgeArticleId FROM CaseArticle where CaseId = :caseId];
        List<Case_Article_Data_MVN__c> attachedKnowledgeList = [SELECT Id,Article_Title_MVN__c,Article_Number_MVN__c,Article_URL_Name_MVN__c,Article_Type_API_MVN__c,
                                                                Article_Summary_MVN__c,Article_Type_MVN__c,Knowledge_Article_ID_MVN__c,Article_Language_MVN__c
                                                                FROM Case_Article_Data_MVN__c where Case_MVN__c = :caseId];

        return attachedKnowledgeList;
    }
    
    
    //Populates a new case article but does not insert it
    public static Case_Article_Data_MVN__c createCaseArticle(Id caseID, SObject theKav) {
        //Make sure theKav is of a valid type
        Case_Article_Data_MVN__c cad = new Case_Article_Data_MVN__c();
        cad = fillCaseArticleData(cad,caseID,theKav);
        return cad;
    }
    
   public static Case_Article_Data_MVN__c fillCaseArticleData(Case_Article_Data_MVN__c cad,Id caseID, SObject theKav) {

        //Standard fields
        cad.Case_MVN__c = caseID;
        cad.Knowledge_Article_ID_MVN__c = (Id) theKav.get('KnowledgeArticleId');
        cad.Knowledge_Article_Version_ID_MVN__c = (Id) theKav.Id;
        cad.Article_Version_Number_MVN__c = (Integer) theKav.get('VersionNumber');
        cad.Article_Type_API_MVN__c = (String) theKav.get('ArticleType');
        cad.Article_Type_MVN__c = knowledgeTypes().get(cad.Article_Type_API_MVN__c.toLowerCase());
        cad.Article_Language_MVN__c = (String) theKav.get('Language');
        cad.Article_Number_MVN__c = (String) theKav.get('ArticleNumber');
        cad.Article_Title_MVN__c = (String) theKav.get('Title');
        cad.Article_Summary_MVN__c = (String) theKav.get('Summary');
        cad.Article_URL_Name_MVN__c = (String) theKav.get('UrlName');

        //Handle custom fields
        for(Case_Article_Fields_MVN__c caf: articleFields.values()) {
            if(cad.Article_Type_API_MVN__c == caf.Knowledge_Article_Type_MVN__c) {
                try {
                    cad.put(caf.Case_Article_Data_Field_MVN__c,theKav.get(caf.Knowledge_Article_Field_MVN__c));
                } catch (Exception e) {
                    //don't need to do anything, a bad custom field will not prevent the copy
                }
            }
        }

        return cad;
    } 
    
     //Returns a Map of knowledge Article Type api names to labels
    public static Map<String,String> knowledgeTypes() {
        Map<String,String> MITSarticleTypes = new Map<String,String>();
        Map<String, Schema.SObjectType> objects = Schema.getGlobalDescribe();
        for(String sot : objects.keySet()) {
            if(sot.contains('__kav')){
                MITSarticleTypes.put(sot,objects.get(sot).getDescribe().getLabel());
            }
        }
        System.debug('!!! Article Types: ' + MITSarticleTypes);
        return MITSarticleTypes;
    }
    
 }