/*
* KnowledgeSearchUtilityMVN
* Created By: Roman Lerman
* Created Date: 3/12/2013
* Description: This class is used for building out the quries for the knowledge search
*/
public with sharing class KnowledgeSearchUtilityMVN {

    public static final String StandardFields       = 'ArticleNumber, UrlName, Title, KnowledgeArticleId, Id, Language, Summary, ArticleType, VersionNumber';
    public static final Map<String,Case_Article_Fields_MVN__c> articleFields = Case_Article_Fields_MVN__c.getAll();

    public List<String> articleTypes;
    public Service_Cloud_Settings_MVN__c serviceCloudCustomSettings;
    public Integer maximumResults;

    public KnowledgeSearchUtilityMVN () {
        serviceCloudCustomSettings = Service_Cloud_Settings_MVN__c.getInstance();
        articleTypes = UtilitiesMVN.splitCommaSeparatedString(serviceCloudCustomSettings.Knowledge_Search_Article_Types_MVN__c);
        maximumResults = (Integer)serviceCloudCustomSettings.Knowledge_Search_Max_Results_MVN__c;
    }

    public PageReference removeArticle(Case_Article_Data_MVN__c cad) {
        delete cad;

        return null;
    }

    public List<Case_Article_Data_MVN__c> knowledgeSearch(String queryString, ID caseID){
    	List<Case_Article_Data_MVN__c> knowledgeList = new List<Case_Article_Data_MVN__c>();
        list<list<SObject>> allResults = new list<list<SObject>> ();
        try {
            allResults = search.query(queryString);
        }
        catch (Exception e) {
            ApexPages.addMessages(e);
            return knowledgeList;
        }
        Integer totalRecordSize = 0;
        
        for(List<SObject> lst:allResults){
            totalRecordSize += lst.size();
        }

        System.debug('Total number of articles found: '+totalRecordSize);

        if(totalRecordSize > maximumResults){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Service_Cloud_Knowledge_Search_Max_Records_Found));
            return null;
        }

        for (list<SObject> objectResults : allResults) {
            for (SObject obj : objectResults) {
                Case_Article_Data_MVN__c result = createCaseArticle(caseID, obj);
        
                knowledgeList.add(result);
            }
        }
        return knowledgeList;
    }

    public String buildQueryString(String articleType, String articleSearchText, String productName,  String language){
        boolean firstReturningClause = true;
        boolean isAll = articleType == 'All';
        String whereClause = buildWhereClause(language, productName);
        
        if((articleSearchText == null || articleSearchText == '') && productName != null && productName != '' && productName != 'All'){
            articleSearchText = productName;
        }
        
        String findQuery = 'find \'' + articleSearchText + '\'';
        
        for(String aType : articleTypes) {
            if (isAll || articleType.equalsIgnoreCase(aType)) {
                if (firstReturningClause) {
                    findQuery += ' returning ';
                    firstReturningClause = false;
                } else {
                    findQuery += ', ';
                }

                //build type field list
                String typeFields = buildTypeFields(aType);

                findQuery += aType + ' (' + StandardFields + typeFields + whereClause + ' ORDER BY Title)';
            }
        }
        
        findQuery += ' limit ' + maximumResults;
        system.debug('Find Query: '+findquery);
        return findQuery;
    }

    public String buildTypeFields(String articleType) {
        String typeFields = '';

        for(Case_Article_Fields_MVN__c caf: articleFields.values()) {
            if(articleType.equalsIgnoreCase(caf.Knowledge_Article_Type_MVN__c)) {
                typeFields += ', ';
                typeFields += caf.Knowledge_Article_Field_MVN__c;
            }
        }

        return typeFields;
    }

    public String buildWhereClause(String language, String productName) {
        string whereClause = ' where PublishStatus = \'Online\' and IsLatestVersion=true ';
        
        if(language != null && language != '') {
            whereClause += ' and Language = \'' + language + '\'';
        }

        if(productName != null && productName != '' && productName != 'All') {
            whereClause += ' and (Product_ID_MVN__c = \'' + productName + '\'';
            whereClause += ' or Product_ID_MVN__c = \'\')';
        }

        
        
        System.debug('KnowledgeSearchUtility: '+whereClause);

        return whereClause;
    }
    
    public PageReference selectArticle(Case_Article_Data_MVN__c selectedArticle, Id caseId){
        List<Case_Article_Data_MVN__c> attachedKnowledgeList = queryExistingCaseArticles(caseId);
        for (Case_Article_Data_MVN__c kaa : attachedKnowledgeList) {
            if (kaa.Knowledge_Article_ID_MVN__c == selectedArticle.Knowledge_Article_ID_MVN__c) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Service_Cloud_Knowledge_Search_Article_Already_Attached));
                return null;
            }
        }
        
        SavePoint sp = Database.setSavePoint();

        try {
            Case_Article_Data_MVN__c articleToInsert = selectedArticle.clone(true, false);
            
            insert articleToInsert;
        } catch (Exception e) {
            Database.rollback(sp);
            ApexPages.addMessages(e);
        }

        return null;
    }

    //Lots of queries here. cache? We're not setting the Knowledge Article version
    public List<Case_Article_Data_MVN__c> queryExistingCaseArticles(Id caseId) {
        List<CaseArticle> caseArticles = [SELECT CaseId, KnowledgeArticleId FROM CaseArticle where CaseId = :caseId];
        List<Case_Article_Data_MVN__c> attachedKnowledgeList = [SELECT Id,Article_Title_MVN__c,Article_Number_MVN__c,Article_URL_Name_MVN__c,Article_Type_API_MVN__c,
                                                                Article_Summary_MVN__c,Article_Type_MVN__c,Knowledge_Article_ID_MVN__c,Article_Language_MVN__c
                                                                FROM Case_Article_Data_MVN__c where Case_MVN__c = :caseId];

        return attachedKnowledgeList;
    }

    //Populates a new case article but does not insert it
    public static Case_Article_Data_MVN__c createCaseArticle(Id caseID, SObject theKav) {
        //Make sure theKav is of a valid type
        Case_Article_Data_MVN__c cad = new Case_Article_Data_MVN__c();
        cad = fillCaseArticleData(cad,caseID,theKav);
        return cad;
    }

    public static Case_Article_Data_MVN__c fillCaseArticleData(Case_Article_Data_MVN__c cad,Id caseID, SObject theKav) {

        //Standard fields
        cad.Case_MVN__c = caseID;
        cad.Knowledge_Article_ID_MVN__c = (Id) theKav.get('KnowledgeArticleId');
        cad.Knowledge_Article_Version_ID_MVN__c = (Id) theKav.Id;
        cad.Article_Version_Number_MVN__c = (Integer) theKav.get('VersionNumber');
        cad.Article_Type_API_MVN__c = (String) theKav.get('ArticleType');
        cad.Article_Type_MVN__c = knowledgeTypes().get(cad.Article_Type_API_MVN__c.toLowerCase());
        cad.Article_Language_MVN__c = (String) theKav.get('Language');
        cad.Article_Number_MVN__c = (String) theKav.get('ArticleNumber');
        cad.Article_Title_MVN__c = (String) theKav.get('Title');
        cad.Article_Summary_MVN__c = (String) theKav.get('Summary');
        cad.Article_URL_Name_MVN__c = (String) theKav.get('UrlName');

        //Handle custom fields
        for(Case_Article_Fields_MVN__c caf: articleFields.values()) {
            if(cad.Article_Type_API_MVN__c == caf.Knowledge_Article_Type_MVN__c) {
                try {
                    cad.put(caf.Case_Article_Data_Field_MVN__c,theKav.get(caf.Knowledge_Article_Field_MVN__c));
                } catch (Exception e) {
                    //don't need to do anything, a bad custom field will not prevent the copy
                }
            }
        }

        return cad;
    }

    //Returns a Map of knowledge Article Type api names to labels
    public static Map<String,String> knowledgeTypes() {
        Map<String,String> articleTypes = new Map<String,String>();
        Map<String, Schema.SObjectType> objects = Schema.getGlobalDescribe();
        for(String sot : objects.keySet()) {
            if(sot.contains('__kav')){
                articleTypes.put(sot,objects.get(sot).getDescribe().getLabel());
            }
        }
        System.debug('!!! Article Types: ' + articleTypes);
        return articleTypes;
    }
}