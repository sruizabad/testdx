/*
* LockRelatedRecordsMVN
* Created By: Roman Lerman
* Created Date: 4/8/2013
* Description: This is class is used for locking Cases/Fulfillments, namely when either of these records are closed
*			   the call center agents are prevented from adding Tasks/Events/Attachments to these records
*/
global with sharing class LockRelatedRecordsMVN implements TriggersMVN.HandlerInterface {
	// The lookup from the child object to the parent
	private String parentIdFieldName;
	
	// Name of the object that will be locked
	private String objectName;
	
	// The boolean field on the parent object that will specify when it is locked
	private String lockedFieldName;
	
	// The first three characters in the id of the parent record
	private String parentKeyPrefix;
	
	// What gets displayed back to the user
	private String error;
	
	public LockRelatedRecordsMVN(String parentIdFieldName, String objectName, String lockedFieldName, 
									String parentKeyPrefix, String error){
		this.parentIdFieldName = parentIdFieldName;
		this.objectName = objectName;
		this.lockedFieldName = lockedFieldName;
		this.parentKeyPrefix = parentKeyPrefix;
		this.error = error;
	}
	
	public void execute(List<SObject> records) {
		System.debug('Anonymizing: '+UtilitiesMVN.isAnonymizing);
		if(UtilitiesMVN.isAnonymizing){
			return;
		}
		
		List<SObject> relatedRecords = new List<SObject>();
		Set<Id> recordIds = new Set<Id>();
		
		// If the child is related to the object of interest let's add it to a list and its parent id to a set
    	for(SObject record:records){
    		String parentId = returnParentId(record);
    		if(parentId != null && parentId.startsWith(parentKeyPrefix)){
    			relatedRecords.add(record);
    			recordIds.add(parentId);
    		}
    	}
    	if(recordIds != null && recordIds.size() > 0){
    		// Grab the parent field that determines that a parent is locked and check, if it is locked then display error
    		Map<Id, SObject> sObjectMap = new Map<Id, SObject>(Database.query('select '+lockedFieldName+' from '+objectName+' where Id in :recordIds'));
    		for(SObject record:relatedRecords){
    			String parentId = returnParentId(record);
	    		if(parentId != null && (boolean) sObjectMap.get(parentId).get(lockedFieldName)){
	    			record.addError(error);
	    		}
    		}
    	}
    }
    
    public String returnParentId(SObject record){
    	Object parentId = record.get(parentIdFieldName);
		return parentId != null ? (String) parentId : null;
    }
    
    public void handle() {
    	execute(Trigger.isDelete ? (List<SObject>) trigger.old : (List<SObject>) trigger.new); 
    }
}