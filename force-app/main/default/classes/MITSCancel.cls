global class MITSCancel{

webservice static void caseCancel(Id CaseId){

List<Case> caseval = new List<Case>();

caseval=[Select Id,CaseNumber,Status from Case where Id=:CaseId];
system.debug('CASE VALUES ARE------>'+caseval);
       if(caseval.size()>0){
          PageReference pageRef;
          
  if(caseval[0].Status=='In Process' || caseval[0].Status=='Closed'){
  pageRef = new PageReference('/apex/MITSCloseHomeTab');
        }
  
    else{
   delete caseval;
   system.debug('CASE IS DELETED SUCCESSFULLY');
     }
    } 
  }
}