public with sharing class MITSCaseChangeforProfiles {
public String recordid {get ;set;}
Public Case csval {get; set;}
    public MITSCaseChangeforProfiles(ApexPages.StandardController controller) {
   recordid =apexpages.currentpage().getparameters().get('id');
   system.debug('Record id is******'+recordid);
    }
    
    public  PageReference pageRedirectMethod(){
     PageReference pgRf;
     User profilename =[Select Profile.Name from User where Id=:UserInfo.getUserId()];
     
          if(profilename.Profile.Name=='MITS- BASIC MEDICAL INFORMATION USER' || 
                profilename.Profile.Name=='MITS-BASIC MEDICAL INFORMATION KNOWLEDGE ADMINISTRATOR' ||
                profilename.Profile.Name=='MITS-GLOBAL IT ADMINISTRATOR' ||  profilename.Profile.Name=='MITS-REGIONAL MEDICAL INFORMATION KNOWLEDGE ADMINISTRATOR' ||
                profilename.Profile.Name=='MITS-REGIONAL MEDICAL INFORMATION USER' || profilename.Profile.Name=='MITS-GLOBAL BUSINESS ADMINISTRATOR')
              {
      
              pgRf=new PageReference('/apex/MITSNewCasePage?id='+recordid) ;                     
              pgRf.setRedirect(true);
              Map<String, String> m1 = pgRf .getParameters();
              m1.put('nooverride', '1');
           return pgRf;
           }
        
        else {
              pgRf  = new PageReference('/' + Case.SObjectType.getDescribe().getKeyPrefix() + '/e');

                Map<String, String> m = pgRf .getParameters();
                m.putAll(ApexPages.currentPage().getParameters());                      
                m.put('nooverride', '1');                       
                pgRf .setRedirect(true);

                return pgRf ;
              }  
        
    }

}