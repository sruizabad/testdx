public without sharing class MITSCreateCasesForCustomerMITSrequest implements TriggersMVN.HandlerInterface{
    
    public Id MITSRecordTypeId {get;set;}
    
    public MITSCreateCasesForCustomerMITSrequest(){
    MITSRecordTypeId = [select Id from RecordType where SObjectType='Case' and DeveloperName =: UtilitiesMITS.mitsRecordType].Id;   
    }
    public void execute(List<Medical_Inquiry_vod__c> inquiries) {
        Service_Cloud_Settings_MITS__c settings = Service_Cloud_Settings_MITS__c.getInstance(UserInfo.getUserId());
        if(!String.isBlank(settings.MITS_Country_code__c)){
            Set<String> countries = new Set<String>(UtilitiesMITS.splitCommaSeparatedString(settings.MITS_Country_code__c));
            System.debug('Countries: ' + countries);

            //List<Case> Createinteractions = new List<Case>();
            List<Case> Createinteractions= new List<Case>();

            Map<Id, Medical_Inquiry_vod__c> inquiriesMap = new Map<Id, Medical_Inquiry_vod__c>(inquiries);

// To check conditions to create interaction and request in case object         
            for(Medical_Inquiry_vod__c inquiry : inquiries){
                            
                        if(inquiry.Status_vod__c == UtilitiesMITS.medicalInquirySubmittedStatus
                            && countries.contains(inquiry.stamp_Country_Code_MVN__c)
                            ){
                            Createinteractions.add(createInteraction(inquiry));
                        }
              // Createinteractions.add(createInteraction(inquiry));
            }
            if(!Createinteractions.isEmpty()){
                createCases(Createinteractions, inquiriesMap);
            }
        }
    }

    public void execute(List<Medical_Inquiry_vod__c> newInquiries, Map<Id, Medical_Inquiry_vod__c> oldInquiries) {

        Service_Cloud_Settings_MITS__c settings = Service_Cloud_Settings_MITS__c.getInstance(UserInfo.getUserId());
        if(!String.isBlank(settings.MITS_Country_code__c)){
            Set<String> countries = new Set<String>(UtilitiesMITS.splitCommaSeparatedString(settings.MITS_Country_code__c));
            System.debug('Countries: ' + countries);

            List<Case> Createinteractions = new List<Case>();

            Map<Id, Medical_Inquiry_vod__c> inquiriesMap = new Map<Id, Medical_Inquiry_vod__c>(newInquiries);

            //Map<Id, Medical_Inquiry_vod__c> relatedCases = new Map<Id, Medical_Inquiry_vod__c>([select Id, (select Id from Cases__r) from Medical_Inquiry_vod__c where Id in :inquiriesMap.keySet()]);
// To check conditions to create interaction and case 
            for(Medical_Inquiry_vod__c newInquiry : newInquiries){
                
               
                    //List<Customer_Request_Product_BI__c> newlistProducts = [SELECT Id, Name, Manufacturer_BI__c from Customer_Request_Product_BI__c WHERE id =: newInquiry.Product_BI__c Limit 1];
                    //System.debug('ProductManufacturer:' + newlistProducts[0].Manufacturer_BI__c);
                        if(oldInquiries.get(newInquiry.Id).Status_vod__c != UtilitiesMITS.medicalInquirySubmittedStatus 
                            && newInquiry.Status_vod__c == UtilitiesMITS.medicalInquirySubmittedStatus
                            && countries.contains(newInquiry.stamp_Country_Code_MVN__c)
                            ){
                            }
                            Createinteractions.add(createInteraction(newInquiry));
                        
                
            }
            
           /* if(!Createinteractions.isEmpty()){
                createCases(Createinteractions, inquiriesMap);
            }*/
        }
    }
// Method to insert interaction and case 
   public void createCases(List<Case> Createinteractions, Map<Id, Medical_Inquiry_vod__c> inquiriesMap){
        if(!Createinteractions.isEmpty()){
            System.debug('CASES TO CREATE: ' + Createinteractions);
            insert Createinteractions;
                   }   
    }
// Method to assign values to create interaction
    public Case createInteraction(Medical_Inquiry_vod__c inquiry){
        Case_Delivery_Method_Mapping_MITS__c deliveryMethodMapping = Case_Delivery_Method_Mapping_MITS__c.getInstance();
        Map<Id, Account> accMap = new Map<Id, Account>();
       //List<Account> accids=[Select Id,Name from Account];
       
        //List<Medical_Inquiry_vod__c> csvalues=[Select Id,Account_vod__r.Name,Account_vod__r.FirstName from Medical_Inquiry_vod__c where Account_vod__r.Id IN:accids];
        //List<Account> mivals=[select id,Name,Account.FirstName,Account.LastName,Account.PersonMobilePhone,Account.Fax,Account.Primary_Email_BI__c,(select id from Medical_Inquiry_vod__r),(select Id,Name,City_vod__c,Zip_vod__c from Address_vod__r) from Account ];
        List<Account> mivals=[select id,Name,Account.FirstName,Account.LastName,Account.PersonMobilePhone,Account.Phone,Account.Fax,Account.Primary_Email_BI__c,Salutation,(select Id,Name,Address_line_2_vod__c,City_vod__c,Zip_vod__c,State_vod__c from Address_vod__r ) from Account where Id=:inquiry.Account_vod__c];
        system.debug('Account Values after add--->'+mivals);
        //List<Account> addrvals =[Select Id,(Select Id from Address_vod__c where Address_vod__r.id=:'accMap.Id') from Account];
            String prdlook;    
        if(inquiry.Product_BI__r.Name==null)
        {
        prdlook=inquiry.Product__c;
        }
        else{
        prdlook=inquiry.IRMS_product_BI__c;
        }
         system.debug('$$$$$$$'+prdlook);       
       List<Product_MITS__c> prodlist = [Select Id,Name from Product_MITS__c where Name=:prdlook];
       system.debug('Product List ------>'+prodlist);
       system.debug('mi are---->'+mivals);
        
        Case interaction = new Case();

        interaction.RecordTypeId = MITSRecordTypeId;
        interaction.Medical_Inquiry_MVN__c = inquiry.Id;
        if(prodlist.size()>0){
            if(inquiry.IRMS_product_BI__c !=null){
      interaction.Product_New_MITS__c=inquiry.IRMS_product_BI__c;
         }
      else{
    interaction.Product_New_MITS__c=prdlook;
       }
   }
        /*interaction.Product_New_MITS__c=inquiry.IRMS_product_BI__c;
        system.debug('Product Value is ----->'+interaction.Product_New_MITS__c);*/
        User u = [select Id,Profile.Name,Country_Code_BI__c from User where Id = :UserInfo.getUserId()];
        String queueFormate = 'MITS_' + u.Country_Code_BI__c + '_Queue';
        List<Group> queueIDList = [select ID from Group where type='Queue' AND DeveloperName=: queueFormate];
        system.debug('@@@@@@@@@'+queueIDList);
          if(queueIDList.size()>0)  {
        interaction.OwnerId = queueIDList[0].Id;
        for(integer i=0;i<queueIDList.size();i++){
        interaction.OwnerId = queueIDList[i].Id;
           }
        }
        
        interaction.AccountId = inquiry.Account_vod__c;
        interaction.Status='In Process';
        interaction.Case_Type_MITS__c='Medical Information Inquiry';
        interaction.Case_Source_MITS__c='CRM System';
        interaction.Response_Delivery_MITS__c=inquiry.Delivery_Method_vod__c;
        interaction.Inquiry_MITS__c='Medical Inquiry Number is:' +inquiry.Name +'\r\n' +'MI Product is:' + inquiry.IRMS_product_BI__c+ '\r\n' + 'Inquiry Text is:' + inquiry.Inquiry_Text__c;
        interaction.Origin = Service_Cloud_Settings_MITS__c.getInstance().MITS_Medical_Inquiry_Source__c;
        interaction.First_Name_MITS__c = mivals[0].FirstName;
        interaction.Last_Name_MITS__c= mivals[0].LastName;
        interaction.Telephone_Number_MITS__c=mivals[0].Phone;
        interaction.Fax_Number_MITS__c=mivals[0].Fax;
        interaction.Email_Address_MITS__c =  mivals[0].Primary_Email_BI__c;
        if(inquiry.Delivery_Method_vod__c=='Phone_vod'){
           if(inquiry.Phone_Number_vod__c!=null){
           interaction.Telephone_Number_MITS__c =inquiry.Phone_Number_vod__c;
        }
        else
           interaction.Telephone_Number_MITS__c = mivals[0].Phone;
        }
        if(inquiry.Delivery_Method_vod__c=='Fax_vod'){
           if(inquiry.Fax_Number_vod__c!=null){
        interaction.Fax_Number_MITS__c=inquiry.Fax_Number_vod__c;
          }
          else
            interaction.Fax_Number_MITS__c = mivals[0].Fax;
        }
        
        if(inquiry.Delivery_Method_vod__c=='Email_vod'){
           if(inquiry.Email_vod__c!=null){
        interaction.Email_Address_MITS__c=inquiry.Email_vod__c;
          }
          else
            interaction.Email_Address_MITS__c =  mivals[0].Primary_Email_BI__c;
        }
         
        if(inquiry.Delivery_Method_vod__c=='Mail_vod'){
           if(inquiry.Address_Line_1_vod__c!=null || inquiry.Address_Line_2_vod__c!=null){
        interaction.Address_MITS__c=inquiry.Address_Line_1_vod__c +'\r\n' + inquiry.Address_Line_2_vod__c;
        }
          else
            interaction.Address_MITS__c = mivals[0].Address_vod__r==null?Null:mivals[0].Address_vod__r[0].Name +'\r\n' + mivals[0].Address_vod__r[0].Address_line_2_vod__c;
           
          if(inquiry.City_vod__c!=null){
        interaction.City_MITS__c=inquiry.City_vod__c;
        }
        else
          interaction.City_MITS__c = mivals[0].Address_vod__r==null?Null:mivals[0].Address_vod__r[0].City_vod__c;
          
        if(inquiry.State_vod__c!=null){
        interaction.State_Province_MITS__c=inquiry.State_vod__c;
        }else
            interaction.State_Province_MITS__c=mivals[0].Address_vod__r==null?Null:mivals[0].Address_vod__r[0].State_vod__c;
            
            if(inquiry.Zip_vod__c!=null){
          interaction.Postal_Code_MITS__c=inquiry.Zip_vod__c;
           }
          else
            interaction.Postal_Code_MITS__c=mivals[0].Address_vod__r==null?Null:mivals[0].Address_vod__r[0].Zip_vod__c;
            
        
        }
        
        interaction.Salutation_MITS__c= mivals[0].Salutation;
        system.debug('First Name value'+interaction.First_Name_MITS__c);
        //interation.Case_Source_MITS__c= Service_Cloud_Settings_MITS__c.getInstance().MITS_Medical_Inquiry_Source__c;
        interaction.Response_Delivery_MITS__c = Schema.SObjectType.Case_Delivery_Method_Mapping_MITS__c.fields.getMap().get(inquiry.Delivery_Method_vod__c + '__c') != null ? (String) deliveryMethodMapping.get(inquiry.Delivery_Method_vod__c + '__c') : inquiry.Delivery_Method_vod__c;
        return interaction;
    }
    
    public void handle() {
        if(Trigger.isInsert)// creating new Customer Request
            execute((List<Medical_Inquiry_vod__c>) trigger.new); 
        else if(Trigger.isUpdate)// updating existing Customer request
            execute((List<Medical_Inquiry_vod__c>) trigger.new, (Map<Id, Medical_Inquiry_vod__c>) trigger.oldMap);
    }
}