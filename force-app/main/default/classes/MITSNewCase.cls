public with sharing class MITSNewCase{
  public Case newCase { get; set; }
  public boolean isInput{get;set;}
  //public ID accountId { get; set; }
  public transient string filename{get;set;}
  public transient blob filebody{get;set;}
  public transient Attachment  attach{get;set;}
  public Boolean refreshPage {get; set;}
  public Boolean showattachment{get; set;}
  public Boolean afterthreeattachments{get; set;}
  public Boolean casetypeshowhide{get; set;}
  public Boolean statusupdate{get; set;}
  public user loggedinuser{get;set;}
  
  public string str {get; set;}
  public string readOnlyProduct{get; set;}
  public List<SelectOption> regionsteam {get; set;}

  public boolean show{get;set;}
  public String mes{get;set;}
  public String recordid {get ; set;}
  public String scrrecordid {get ; set;}
  
  public void hidevalue(){
  if(newCase.Case_Type_MITS__c == 'Potential Technical Product Complaint' || newCase.Case_Type_MITS__c == 'Potential Adverse Event'){
   
  newCase.Response_Delivery_MITS__c='';
  system.debug('Response Delivery value^^^^^^^^^^'+newCase.Response_Delivery_MITS__c);
      }
  }
 
 public MITSNewCase(ApexPages.StandardController controller) {
 str = 'Medical Doctor';
 readOnlyProduct = 'testProduct';
 casetypeshowhide=false;
 
 List<String> fields = new List<String>(Case.SObjectType.getDescribe().fields.getMap().keySet());
  recId=apexpages.currentpage().getparameters().get('id');
  User pname =[Select Profile.Name from User where Id=:UserInfo.getUserId()];
  if(pname.Profile.Name=='MITS-GLOBAL BUSINESS ADMINISTRATOR'){
      statusupdate=true;
  }

  
if(!Test.isRunningTest()) {
 controller.addFields(fields);
}       isInput=true; 
        refreshPage =false;
        showattachment=true;
        
      if(pname.Profile.Name=='MITS-GLOBAL IT ADMINISTRATOR')  
      showattachment=false;
        
        newCase = (Case)controller.getRecord();
        system.debug('###'+newCase); 
        if(newCase==null){
            newCase = new Case();
        }
        if(newCase.Status== 'Closed') {
            isInput=false;
        }
                
    }
  
  public List<SelectOption> regionsteam () {
  List<SelectOption> options = new List<SelectOption>();
return options;
  }
  
    public PageReference cancelCase(){ 
        PageReference pageRef;  
        //pageRef= new PageReference('https://cs10.salesforce.com/500/o');
        pageRef = new PageReference('/apex/MITSCloseHomeTab'); 
        pageRef.setReDirect(true);
        if(newcase.Status=='In Process' || newCase.Status=='Closed'){
            return pageRef;
        }else {          
           delete newcase;
           return pageRef;
        }         
    }
    public Pagereference caseCompleted(){
    //webservice static void caseCompleted(Id CaseId)
    PageReference pageRef1;
      String recId1=apexpages.currentpage().getparameters().get('id');
     system.debug('new case values are----->'+newcase);
     system.debug('new case values are----->'+newcase.Case_Type_MITS__c);
     if(newCase.Case_Type_MITS__c == 'Potential Technical Product Complaint' || newCase.Case_Type_MITS__c == 'Potential Adverse Event'){
     
      List<Attachment> caseAttachements = [SELECT Id from Attachment where ParentId=:recId1];
      system.debug('case attachments values are----->'+caseAttachements);
      if(caseAttachements == null || caseAttachements.isEmpty()){
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Add the Attachment'));
         return pageRef1; 
      }
      
      /*if(caseAttachements.size()>3){
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Attachments Limit should be 3 not more than that'));
      
      }*/
      system.debug('After adding error mesage');
    }
         
     /* if(newCase.Response_Type_MITS__c=='Off-label' && newCase.Inquiry_Type_MITS__c== 'Solicited')   {
      system.debug('yes the page in Loop');
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Request is solicited and off-label, please verify the selections of Inquiry Type and Response Type'));
          return;
          
          }    */
          
          
      if(newCase.Case_Type_MITS__c == 'Potential Technical Product Complaint' || newCase.Case_Type_MITS__c == 'Potential Adverse Event' || newCase.Case_Type_MITS__c =='Medical Information Inquiry') {
      
      if(newcase.Country_MITS__c==null){
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select the Country'));
          
          }
      
      }   
          
          if(newCase.Case_Type_MITS__c == 'Potential Technical Product Complaint' || newCase.Case_Type_MITS__c == 'Potential Adverse Event'){
            newCase.Response_Delivery_MITS__c='';
            newCase.Response_Category_MITS__c='';
            newCase.Customer_Type_MITS__c='';
            newCase.Case_Source_MITS__c='';
            newCase.Salutation_MITS__c='';
            }
          
                  
    isInput= false;
    
    if(newCase.Status==null){
    newCase.Status='Closed';
     newCase.Case_Closed_MITS__c=System.Now();
       system.debug('********Before1*******');
        if(newCase.Status=='Closed'){
                newCase.RecordTypeId = [select Id from RecordType where SObjectType='Case' and DeveloperName =:'MITS_Case_Closed'].Id;
             system.debug('Record Type is updated');
             system.debug('Record Type is $$$$$$$'+newCase.RecordTypeId);
             update newCase;
             system.debug('********After1*******');
           }    
     //newCase.Case_Status_MITS__c= 'Completed';
    insert newCase;
    
    pageRef1 = new PageReference('/apex/MITSCloseHomeTab'); 
            pageRef1.setReDirect(true);
            return pageRef1;
            //refreshPage =true;
    
    }else{
    newCase.Status='Closed';
    newCase.Case_Closed_MITS__c=System.Now();
    //newCase.Case_Status_MITS__c= 'Completed';
    system.debug('********Before2*******');
        if(newCase.Status=='Closed'){
                newCase.RecordTypeId = [select Id from RecordType where SObjectType='Case' and DeveloperName =:'MITS_Case_Closed'].Id;
             system.debug('Record Type is updated');
             system.debug('Record Type is $$$$$$$'+newCase.RecordTypeId);
             system.debug('********After2*******');
           }    
    
           update newCase;  
           
           pageRef1 = new PageReference('/apex/MITSCloseHomeTab'); 
            pageRef1.setReDirect(true);
            return pageRef1;
           
           }  
           refreshPage=true;
           return pageRef1;
    }
    
    
    public PageReference caseInProcess(){
    loggedinuser=new User();     
    loggedinuser=[Select Id,Name,Profile.Name from User where Id=:userinfo.getuserId()];
    if(loggedinuser.Profile.Name=='MITS-GLOBAL BUSINESS ADMINISTRATOR'){
    newCase.Case_Closed_MITS__c=null;
      }
        Integer currentYear = System.Today().year();  
         
         system.debug('new case values are----->'+newcase);
         
            isInput=false;
            PageReference pageRef;
            
            
            if(newCase.Status==null || newCase.Status=='Open'){
            newCase.Status='In Process';
            //newCase.Case_Status_MITS__c = 'In Process';
          
            if(newCase.Case_Type_MITS__c == 'Potential Technical Product Complaint' || newCase.Case_Type_MITS__c == 'Potential Adverse Event'){
            newCase.Response_Delivery_MITS__c='';
            newCase.Response_Category_MITS__c='';
            newCase.Customer_Type_MITS__c='';
            newCase.Case_Source_MITS__c='';
            newCase.Product_MITS__c='';
            newCase.Salutation_MITS__c='';
            }
          
            update newCase;
           
            //pageRef = new PageReference('https://cs10.salesforce.com/500/o'); 
            pageRef = new PageReference('/apex/MITSCloseHomeTab'); 
            pageRef.setReDirect(true);
            return pageRef;
            refreshPage =true;
       }
       else{
       
       if(newCase.Case_Type_MITS__c == 'Potential Technical Product Complaint' || newCase.Case_Type_MITS__c == 'Potential Adverse Event'){
            newCase.Response_Delivery_MITS__c='';
            newCase.Response_Category_MITS__c='';
            newCase.Product_MITS__c='';
            newCase.Salutation_MITS__c='';
            }
            update newCase;
             //pageRef = new PageReference('https://cs10.salesforce.com/500/o');
             pageRef = new PageReference('/apex/MITSCloseHomeTab'); 
             pageRef.setReDirect(true);
             refreshPage =true;
             //update newCase;
             return pageRef;
            }
   return pageRef;
  } 
            
    
    public PageReference searcharticles(){
        
            if(newCase.Status==null || newCase.Status=='Open'){
            newCase.Status='In Process';
             if(newCase.Case_Type_MITS__c == 'Potential Technical Product Complaint' || newCase.Case_Type_MITS__c == 'Potential Adverse Event'){
            newCase.Response_Delivery_MITS__c='';
            newCase.Response_Category_MITS__c='';
            newCase.Customer_Type_MITS__c='';
            newCase.Case_Source_MITS__c='';
            newCase.Salutation_MITS__c='';
            }
            update newCase;
            }
       scrrecordid=ApexPages.currentpage().getParameters().get('id');
       system.debug('%%%%%%%%%'+scrrecordid);
       PageReference pageRef = new PageReference('/apex/KnowledgeSearchMITS?id='+scrrecordid);
       system.debug('######'+pageRef);
     return pageRef;
    
    }
    

    public  PageReference pageRedirectMethod(){
        isInput = true;
        if(newCase == null) 
        newCase = new Case();
        
        profile  proid=[select Id from Profile where Name ='MITS- BASIC MEDICAL INFORMATION USER' OR Name='MITS-REGIONAL MEDICAL INFORMATION KNOWLEDGE ADMINISTRATOR' OR Name='MITS-GLOBAL BUSINESS ADMINISTRATOR'];
        Id pid1='00eJ0000000MGwpIAG';
       //String accountId = ApexPages.currentPage().getParameters().get('def_account_id');
       // System.debug('Checking:'+accountId);
        string pid =[select ProfileId from User where Id=:UserInfo.getUserId()].ProfileId;
        string cc =[select Country_Code_BI__c from User where Id=:UserInfo.getUserId()].Country_Code_BI__c;
        recordid=apexpages.currentpage().getparameters().get('id');
        
        PageReference pageRef;
        
        if(pid ==proid.Id )
         {
         
          pageRef = new PageReference('/apex/CreateCaseRedirectMVN');
          pageRef.setReDirect(true);          
           
          }  
       
        else{
           pageRef = new PageReference('/apex/MITS_NewPage');
           pageRef.setReDirect(true);   
          
        }
         return pageRef;
     }
      
public id recId{get;set;}
public transient string parentid{get;set;}
public transient string filename1{get;set;}
public transient blob filebody1{get;set;}
public PageReference AddAttachments(){
 afterthreeattachments=true;
 casetypeshowhide=true;
    try{
        PageReference pr;
        if(fileBody1 != null && fileName1!= null)  {
            attach = new attachment();
            attach.parentId = recId;
            system.debug('attachment value is----->'+attach.parentId);
            attach.body=filebody1;
            system.debug('attachment body value is----->'+attach.body);
            
            attach.name=filename1;
            system.debug('attachment file value is----->'+attach.name);
            insert attach;
            system.debug('attachment value is----->'+attach);
            pr= new PageReference('/'+attach.id);
            system.debug('Pagereference value is----->'+pr);
            
            /*Integer count= [Select Count() from Attachment where parentId =: recId];
            system.debug('++++++'+count);
            
            if(count >=3){
            showattachment=false;
           // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Cannot Add more than 3 Attachments'));
            }  
            else*/
            pr.setRedirect(true);
            //newCase.Case_Type_MITS__c = '';
            return new PageReference('/apex/MITSNewCasePage?='+recId); 
             showattachment=true;                    
        }
        return null;   
    }catch(Exception ex){
       
       return null;
    }     
 }
  
 
 }