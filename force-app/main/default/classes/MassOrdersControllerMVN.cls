/*
  * MassOrdersControllerMVN
  *    Created By:     Kai Amundsen   
  *    Created Date:    August 28, 2013
  *    Description:     Controller for the Campaign mass order page.  Handles marking people as joined and sending large orders
 */

public without sharing class MassOrdersControllerMVN {

    public Map<Id,smallMember> allMembers {get;set;}
    public String additionalWhere {get;set;}

    // Filter lists
    public String lastNameFilter {get;set;}
    public String firstNameFilter {get;set;}
    public String statusFilter {get;set;}
    public List<SelectOption> statusFilterOptions {get;set;}
    public String dateJoinedFilter {get;set;}
    public String errorFilter {get;set;}
    public List<SelectOption> errorFilterOptions {get;private set;}
    public Set<Id> errorRows {get;set;}

    //pagination
    public Integer currentPage {get;set;}
    public Integer currentListSize {get;set;}
    public Integer recordsPerPage {get;private set;}
    public Integer recordsInList {get{return members.size();}}
    public Integer totalListSize {get{return allMembers.size();}}
    public Integer pageStartNumber {
        get{
            if(recordsInList == 0) {
                return 0;
            }
            return currentPage*recordsPerPage + 1;
        }
    }
    public Integer pageEndNumber {
        get{
            Integer endNumber = 0;
            if((currentPage+1)*recordsPerPage < currentListSize) {
                endNumber = (currentPage+1)*recordsPerPage;
            } else {
                endNumber = currentListSize;
            }
            return endNumber;
        }
    }

    public Boolean selectAllBool {get;set;}
    public List<Member> members {get; set;}
    public Id targetID {get;set;}
    public Id callLinkId {get;set;}
    public Map<Id, Integer> campaignProducts {get; private set;}
    public Boolean hasProducts {get; private set;} { hasProducts = true; }
    public AsyncApexJob batchJob {get; private set;}
    public Boolean hasRunSubmit {get; set;}
    public Boolean hasOrdersWithNoAddress{get; set;}
    
    private Map<Id,Call2_vod__c> submitCallList;
    private Set<Id> selectedMemberIds = new Set<Id>();
    private Campaign_vod__c c;
    private OrderUtilityMVN ou;
    private Service_Cloud_Settings_MVN__c settings;
    private List<OrderUtilityMVN.OrderError> tempOrderErrors = new List<OrderUtilityMVN.OrderError>();
    private final Set<String> asyncRunningStatuses =  new Set<String>{'Queued', 'Processing', 'Preparing'};
    private String baseTargetQuery = 'select Id,Target_Account_vod__c,Campaign_vod__c,Date_Joined_MVN__c,Date_Joined_Text_MVN__c,Joined_MVN__c,Order_MVN__c,Order_MVN__r.SAP_Errors_MVN__c,Status_Formula_MVN__c,Order_MVN__r.Ship_Address_Line_1_vod__c from Campaign_Target_vod__c';

    private String sortDirection = 'ASC';
    private String sortExp = 'Target_Account_vod__r.LastName';

    public String sortExpression
    {
        get {
        return sortExp;
        }
        set {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp) {
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            }
            else {
                sortDirection = 'ASC';
            }
            sortExp = value;
        }
    }

    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '') {
            return 'ASC';
        }
        else {
            return sortDirection;
        }
    }

    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }

    public Boolean hasRunningBatchJob {
        get { 
            if(batchJob != null) {
                return asyncRunningStatuses.contains(batchJob.Status);
            }
            return false;
        }
    }

    public String last24HourStatus {
        get {
            if (batchJob != null && !hasRunningBatchJob ) {
                if ( batchJob.CompletedDate != null ) 
                {
                    if (batchJob.CompletedDate > DateTime.now().addDays(-1)) { return c.SAP_Batch_status_MVN__c; } 
                } 
                else if (batchJob.CreatedDate > DateTime.now().addDays(-1)) { return c.SAP_Batch_status_MVN__c; }   
            } else if(hasRunSubmit && String.isNotBlank(c.SAP_Batch_Status_MVN__c)) {
                return c.SAP_Batch_status_MVN__c;
            }

            return null;
        }
    }

    public Integer selectedCount {get{return updateSelectedCount();}}

    public MassOrdersControllerMVN(ApexPages.StandardController controller) {
    	hasOrdersWithNoAddress = false;
        hasRunSubmit = false;
        c = (Campaign_vod__c)controller.getRecord();
        refreshCampaignStatus();

        settings = Service_Cloud_Settings_MVN__c.getInstance();
        if(settings.Mass_Order_Records_Per_Page_MVN__c == null) {
            recordsPerPage = 20;
        } else {
            recordsPerPage = settings.Mass_Order_Records_Per_Page_MVN__c.intValue();
        }
        
        ou = new OrderUtilityMVN();
        ou.campaignId = c.Id;
        
        campaignProducts = new Map<Id,Integer>();
        List<Campaign_Product_MVN__c> productList = [select Id,Product_MVN__c,Quantity_MVN__c from Campaign_Product_MVN__c where Campaign_MVN__c = :c.Id];

        for(Campaign_Product_MVN__c campaignProduct : productList) {
            campaignProducts.put(campaignProduct.Product_MVN__c,campaignProduct.Quantity_MVN__c.intValue());
        }

        baseTargetQuery += ' where Campaign_vod__c = \'' + c.Id + '\' ';
        allMembers = new Map<Id,smallMember>();
        List<Campaign_Target_vod__c> allTargets = (List<Campaign_Target_vod__c>)Database.query(baseTargetQuery);
        Set<String> statusOptions = new Set<String>();
        for(Campaign_Target_vod__c ct : allTargets) {
            smallMember sm = new smallMember();
            sm.selected = false;
            sm.target = ct;
            if(String.isNotBlank(ct.Order_MVN__r.SAP_Errors_MVN__c)) {
                sm.errorStrings.add(ct.Order_MVN__r.SAP_Errors_MVN__c);
            }
            statusOptions.add(sm.target.Status_Formula_MVN__c);
            allMembers.put(ct.Id,sm);
        }

        currentPage = 0;
        currentListSize = allMembers.size();

        errorFilterOptions = new List<SelectOption>();
        errorFilterOptions.add(new SelectOption('',''));
        errorFilterOptions.add(new SelectOption('True',System.Label.Errors_Column_Has_Error_Filter_Option));

        statusFilterOptions = new List<SelectOption>();
        statusFilterOptions.add(new SelectOption('',''));
        List<String> sortedStatusOptions = new List<String>(statusOptions);
        sortedStatusOptions.sort();
        for(String aStatus : sortedStatusOptions) {
            statusFilterOptions.add(new SelectOption(aStatus,aStatus));
        }

        refreshMembers();
    }

    public void refreshCampaignStatus () {
        c = [select Id, Campaign_Letter_MVN__c, SAP_Batch_ID_MVN__c, SAP_Batch_Status_MVN__c from Campaign_vod__c where id = :c.Id];

        if (c.SAP_Batch_ID_MVN__c != null) {
            try {
                batchJob = [SELECT Id, Status, CreatedDate, CompletedDate from AsyncApexJob where Id = :c.SAP_Batch_ID_MVN__c ];
            } 
            catch (System.QueryException ex) 
            {
                System.debug('\n\n\nNo batch job found for this campaign.\n\n\n');
            }
        }
    }

    public void refreshMembers() {
        if ( campaignProducts.isEmpty() ) {
            ApexPages.addMessage( new ApexPages.message(ApexPages.severity.WARNING,Label.Campaign_Has_No_Products) );
            hasProducts = false;
        }

        currentListSize = Database.countQuery('select count() from Campaign_Target_vod__c where Campaign_vod__c = \'' + c.Id + '\' ' + getWhereClause());

        //Make sure current page is not greater than new list size
        if((currentPage)*recordsPerPage > currentListSize) {
            currentPage = getLastPage();
        }

        String targetQuery = baseTargetQuery + getWhereClause() + getOrderByClause() + ' limit ' + recordsPerPage + ' OFFSET ' + recordsPerPage*currentPage;
        System.debug(Logginglevel.ERROR, '!!! TargetQuery: ' + targetQuery);
        List<Campaign_Target_vod__c> targets = (List<Campaign_Target_vod__c>)Database.query(targetQuery);

        Set<Id> actIds = new Set<Id>();
        Set<Id> callIds = new Set<Id>();

        for(Campaign_Target_vod__c ct : targets) {
            actIds.add(ct.Target_Account_vod__c);
            callIds.add(ct.Order_MVN__c);
        } 

        String actQuery = 'select ' + OrderUtilityMVN.accountQueryFields + ', (select ' + OrderUtilityMVN.addressQueryFields + ' from Address_vod__r) from Account where Id in :actIds';
        String callQuery = 'select ' + OrderUtilityMVN.callQueryFields + ' from Call2_vod__c where Id in :callIds';

        Map<Id,Account> acts = new Map<Id,Account>((List<Account>)Database.query(actQuery));
        Map<Id,Call2_vod__c> calls = new Map<Id,Call2_vod__c>((List<Call2_vod__c>)Database.query(callQuery));
        Map<Id,Boolean> accountValidation = ou.validateAccounts(acts.values());

        members = new List<Member>();
        for(Campaign_Target_vod__c ct : targets) {
            Member m = new Member();
            if(allMembers.containsKey(ct.Id)) {
                m.selected = allMembers.get(ct.Id).selected;
                m.addErrors(allMembers.get(ct.Id).errorStrings);
            }
            m.order = calls.get(ct.Order_MVN__c);
            m.target = ct;
            m.validated = accountValidation.get(ct.Target_Account_vod__c);
            m.act = acts.get(ct.Target_Account_vod__c);
            m.addresses = m.act.Address_vod__r;
            if(m.order == null) {
                m.order = ou.newCallWithAddress(m.act,  m.act.Address_vod__r);
                m.order.Order_Letter_MVN__c = c.Campaign_Letter_MVN__c;
            }
            members.add(m);
        }
    }

    public String getWhereClause() {
        String whereClause = '';

        if(String.isNotBlank(lastNameFilter)) {
            whereClause += ' and Target_Account_vod__r.LastName like \'%' + String.escapeSingleQuotes(lastNameFilter) + '%\' ';
        }
        if(String.isNotBlank(firstNameFilter)) {
            whereClause += ' and Target_Account_vod__r.FirstName like \'%' + String.escapeSingleQuotes(firstNameFilter) + '%\' ';
        }
        if(String.isNotBlank(dateJoinedFilter)) {
            whereClause += ' and Date_Joined_Text_MVN__c like \'%' + String.escapeSingleQuotes(dateJoinedFilter) + '%\' ';
        }
        if(String.isNotBlank(statusFilter)) {
            whereClause += ' and Status_Formula_MVN__c = \'' + statusFilter + '\' ';
        }
        if(String.isNotBlank(errorFilter)) {
            errorRows = new Set<Id>();
            Set<Id> accountIds = new Set<Id>();
            for(Id smId : allMembers.keySet()) {
                accountIds.add(allMembers.get(smId).target.Target_Account_vod__c);
            }

            List<Address_vod__c> addresses = [select Id,Name,Primary_vod__c,Account_vod__c from Address_vod__c where Primary_vod__c = true and Account_vod__c in :accountIds];
            Map<Id,Boolean> actHasPrimary = new Map<Id,Boolean>();
            for(Address_vod__c addr : addresses) {
                if(actHasPrimary.containsKey(addr.Account_vod__c)) {
                    if(addr.Primary_vod__c) {
                        actHasPrimary.put(addr.Account_vod__c,addr.Primary_vod__c);
                    }
                } else {
                    actHasPrimary.put(addr.Account_vod__c,addr.Primary_vod__c);
                }
            }

            for(Id smId : allMembers.keySet()) {
                Campaign_Target_vod__c ct = allMembers.get(smId).target;
                if(allMembers.get(smId).errorStrings.size() > 0 || ((!actHasPrimary.containsKey(ct.Target_Account_vod__c) || !actHasPrimary.get(ct.Target_Account_vod__c)) && String.isBlank(ct.Order_MVN__r.Ship_Address_Line_1_vod__c) && ct.Joined_MVN__c)) {
                    errorRows.add(smId);
                }
            }
            whereClause += ' and Id in :errorRows ';
        }
        return whereClause;
    }

    public String getOrderByClause() {
        String orderBy = '';
        orderBy = ' Order By ' + sortExpression  + ' ' + sortDirection;
        return orderBy;
    }

    /* Navigation controlls */

    public PageReference firstPage() {
        currentPage = 0;
        refreshMembers();
        return null;
    }

    public PageReference previousPage() {
        if(currentPage > 0) {
            currentPage--;
        }
        refreshMembers();
        return null;
    }

    public PageReference nextPage() {
        currentPage++;
        if(currentPage > getLastPage()) {
            currentPage = getLastPage();
        }
        refreshMembers();
        return null;
    }

    public PageReference lastPage() {
        currentPage = getLastPage();
        refreshMembers();
        return null; 
    }

    public Integer getLastPage() {
        Integer lastPageNumber = 0;

        Decimal currentListDecimal = Decimal.valueOf(currentListSize);
        Decimal lastPageDec = currentListDecimal/recordsPerPage;
        if(lastPageDec == Decimal.valueOf(lastPageDec.intValue())) {
            lastPageNumber = (currentListDecimal/recordsPerPage).intValue() - 1;
        } else {
            lastPageNumber = (currentListDecimal/recordsPerPage).intValue();
        }

        if(lastPageNumber  <= 0) {
            lastPageNumber = 0;
        }
        return lastPageNumber;
    }

    public Integer updateSelectedCount() {
        Integer sCount = 0;
        for(Member m : members) {
            allMembers.get(m.target.Id).selected = m.selected;
        }

        for(smallMember sm : allMembers.values()) {
            if(sm.selected){
                sCount++;
            }
        }
        return sCount;
    }

    public PageReference toggleSelectAll() {
        Set<Id> callIds = new Set<Id>();
        for(smallMember sm : allMembers.values()) {
            if(!selectAllBool) {
                sm.selected = false;
            }
            if(sm.target.Joined_MVN__c && sm.target.Order_MVN__c != null) {
                callIds.add(sm.target.Order_MVN__c);
            }
        }

        if(selectAllBool) {
            Map<Id,Call2_vod__c> campaignCalls = new Map<Id,Call2_vod__c>();
            if(callIds.size() > 0) {
                campaignCalls = new Map<Id,Call2_vod__c>([select Id,Status_vod__c from Call2_vod__c where Id in :callIds]);
            }
            for(smallMember sm : allMembers.values()) {
                if(sm.target.Joined_MVN__c && (sm.target.Order_MVN__c == null || campaignCalls.get(sm.target.Order_MVN__c).Status_vod__c == Service_Cloud_Settings_MVN__c.getInstance().Call_Saved_Status_MVN__c)){
                    sm.selected = true;
                }
            }
        }

        refreshMembers();
        return null;
    }

    public PageReference checkOrders () 
    {
    	c.SAP_Batch_Status_MVN__c = null;
    	try{
    		update c;
    	}catch(Exception e){
    		ApexPages.addMessages(e);
    	}

        submitCallList = new Map<Id,Call2_vod__c>();
        updateSelectedCount();
        tempOrderErrors = new List<OrderUtilityMVN.OrderError>();

        selectedMemberIds = new Set<Id>();      
        for(Id m : allMembers.keySet()) {
            if(allMembers.get(m).selected) {
                selectedMemberIds.add( m );
            }
        }

        List<Call2_vod__c> orders = createOrders(selectedMemberIds, false);
        if (hasOrdersWithNoAddress) {
            return null;
        }

        tempOrderErrors = ou.saveFullOrders(orders);
        submitCallList = new Map<Id,Call2_vod__c>(orders);
        processOrderErrors(tempOrderErrors);

        return null;
    }

    public PageReference submitOrders() {
        List<Call2_vod__c> orders = submitCallList.values();
        
        if(!orders.isEmpty()) {
            tempOrderErrors.addAll( ou.submitFullOrders(orders) );
        }

        for(smallMember sm : allMembers.values()) {
            sm.selected = false;
        }
        selectAllBool = false;

        refreshMembers();

        if(tempOrderErrors.size() > 0 ) {
            processOrderErrors(tempOrderErrors);
        }

        hasRunSubmit = true;

        refreshCampaignStatus();
        return null;
    }

    public PageReference memberJoin() {
        smallMember m = allMembers.get(targetID);
        m.target.Joined_MVN__c = true;  
        update m.target;

        //no need to requery to get this value that will be set by workflow. 
        //just need it to show up on rerender.
        m.target.Date_Joined_MVN__c = Date.today();

        refreshMembers();

        return null;
    }

    public PageReference editOrder() {

        List<Call2_vod__c> callList = createOrders( new Set<Id>{targetId}, true );

        if (callList != null && !callList.isEmpty() && callList[0].Id != null ) {
            callLinkId = callList[0].Id;
        }

        return null;
    }

    private void processOrderErrors (List<OrderUtilityMVN.OrderError> errors) {
        for(OrderUtilityMVN.OrderError error : errors)
        {   
            Boolean orderError = false;
            if(String.isNotBlank(error.errorOrder)) {
                for (smallMember m : allMembers.values()) 
                {
                    if(m.target.Order_MVN__c == error.errorOrder)
                    {
                        m.errorStrings.add(error.errorMessage);
                        m.selected = false;
                        selectedMemberIds.remove(m.target.Id);
                        orderError = true;
                    }
                }
                submitCallList.remove(error.errorOrder);
            }

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,error.errorMessage));
        }
    }

    @TestVisible
    private List<Call2_vod__c> createOrders (Set<Id> memberIdsToOrder, Boolean editing) {
        hasOrdersWithNoAddress = false;

        String submitTargetQuery = baseTargetQuery + ' and Id in :memberIdsToOrder';
        List<Campaign_Target_vod__c> submitTargetList = (List<Campaign_Target_vod__c>)Database.query(submitTargetQuery);
        Set<Id> accountIdSet = new Set<Id>();
        
        for (Campaign_Target_vod__c ct : submitTargetList ) 
        {   
            //no need to check for address if we are editing as user will be passed to single order page.
            if (!editing && ct.Order_MVN__c != null && String.isBlank( ct.Order_MVN__r.Ship_Address_Line_1_vod__c ))
            {
                hasOrdersWithNoAddress = true;
                //ApexPages.addMessage( new ApexPages.message(ApexPages.severity.WARNING, Label.All_orders_must_have_an_address ));
                return null;
            }
            accountIdSet.add(ct.Target_Account_vod__c);
        }

        String actQuery = 'select ' + OrderUtilityMVN.accountQueryFields + ', (select ' + OrderUtilityMVN.addressQueryFields + ' from Address_vod__r) from Account where Id in :accountIdSet';
        Map<Id,Account> acts = new Map<Id,Account>((List<Account>)Database.query(actQuery));
        Set<Id> orders = new Set<Id>();
        Map<Id,Call2_vod__c> newOrders = new Map<Id,Call2_vod__c>();
        for (Campaign_Target_vod__c ct : submitTargetList ) {
            if(ct.Order_MVN__c != null) {
                orders.add(ct.Order_MVN__c);
            } 
            else 
            {
                Call2_vod__c newCall = ou.newCallWithAddress(acts.get(ct.Target_Account_vod__c),  acts.get(ct.Target_Account_vod__c).Address_vod__r);
                newCall.Order_Letter_MVN__c = c.Campaign_Letter_MVN__c;
                newOrders.put(ct.Id,newCall);
                if(!editing && newCall.Ship_Address_Line_1_vod__c == null) {
                    hasOrdersWithNoAddress = true;
                    //ApexPages.addMessage( new ApexPages.message(ApexPages.severity.WARNING, Label.All_orders_must_have_an_address ));
                    return null;
                }
            }
        }

        if(newOrders.size() > 0 ) {
            try {
                insert newOrders.values();
            } catch (Exception e) {
                hasOrdersWithNoAddress = true;
                ApexPages.addMessages(e);
                return null;
            }
            
            for(Call2_vod__c c : newOrders.values()) {
                orders.add(c.Id);
            }
        }

        String callQuery = 'select ' + OrderUtilityMVN.callQueryFields + ', (select ' + OrderUtilityMVN.callSampleQueryFields + ' from Call2_Sample_vod__r) from Call2_vod__c where Id in :orders';
        Map<Id,Call2_vod__c> submitCalls = new Map<Id,Call2_vod__c>((List<Call2_vod__c>)Database.query(callQuery));

        List<Call2_vod__c> needsSamples = new List<Call2_vod__c>();
        List<Campaign_Target_vod__c> newOrderTargets = new List<Campaign_Target_vod__c>();
        
        for (Campaign_Target_vod__c ct : submitTargetList ) {
            if(ct.Order_MVN__c == null) {
                ct.Order_MVN__c = newOrders.get(ct.Id).Id;
                newOrderTargets.add(ct);
            }

            Call2_vod__c order = submitCalls.get(ct.Order_MVN__c);
            if(order.Call2_Sample_vod__r == null || order.Call2_Sample_vod__r.isEmpty()) {
                needsSamples.add(order);
            }

            allMembers.get(ct.Id).target = ct;
        }

        List<OrderUtilityMVN.OrderError> sampleErrors = ou.createSamples( needsSamples, campaignProducts.clone());
        processOrderErrors(sampleErrors);
        update newOrderTargets;

        return submitCalls.values();
    }

    public class smallMember {
        public Boolean selected {get;set;}
        public Campaign_Target_vod__c target {get;set;}
        public Set<String> errorStrings;

        public smallMember () {
            errorStrings = new Set<String>();
        }
    }

    public class Member {
        public Boolean selected {get;set;}
        public Boolean validated {get;set;}
        public Call2_vod__c order {get;set;}
        public Account act {get;set;}
        public List<Address_vod__c> addresses {get;set;}
        public Campaign_Target_vod__c target {get;set;}

        private Set<String> errorStrings;

        public Member() {
            selected = false;
            errorStrings = new Set<String>();
        }

        public String getStatus() {
            return target.Status_Formula_MVN__c;
        }

        public void addError(String newError) {
            errorStrings.add(newError);
        }

        public void addErrors(Set<String> newErrors) {
            errorStrings.addAll(newErrors);
        }

        public String getErrors() {
            String errorString = '';

            if(order != null) {
                errorStrings.add(order.SAP_Errors_MVN__c);
            }

            if(String.isBlank(order.Ship_Address_Line_1_vod__c) && target.Joined_MVN__c && validated) {
                errorStrings.add(System.Label.Order_Address_Required);
            }

            for(String anError : errorStrings) {
                if(String.isBlank(errorString)) {
                    errorString = anError;
                } else {
                    errorString += '\n';
                    errorString += anError;
                }
            }

            return errorString;
        }

        public Boolean getLocked() {
            Boolean locked = false;
            if(!validated || (order != null && order.Status_vod__c == Service_Cloud_Settings_MVN__c.getInstance().Call_Submitted_Status_MVN__c) || !OrderUtilityMVN.userHasCreateEdit) {
                locked = true;
            }
            return locked;
        }
    }
}