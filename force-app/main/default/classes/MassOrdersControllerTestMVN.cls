/*
  * MassOrdersControllerTestMVN
  *    Created By:     Kyle Thorton   
  *    Created Date:    October 4, 2013
  *    Description:     Unit tests for MassOrderControllerMVN
 */
@isTest
private class MassOrdersControllerTestMVN {
  
  static User nonSAPUser;
  static Campaign_vod__c campaign;
  static List<Account> accounts;
  static List<Campaign_Target_vod__c> targets;
  static Product_vod__c product;
  static MassOrdersControllerMVN controller;
  static Campaign_Product_MVN__c campProd;
  static Service_Cloud_Settings_MVN__c settings;

  static {
    TestDataFactoryMVN.createSettings();
    settings = Service_Cloud_Settings_MVN__c.getInstance();

    String fakeCountryCode = 'ZZ';
    
    product  = TestDataFactoryMVN.createTestProduct(fakeCountryCode);
    accounts = TestDataFactoryMVN.createTestHCPs();
    campaign = TestDataFactoryMVN.createTestCampaign();
    targets  = TestDataFactoryMVN.createTestCampaignTargets(campaign, accounts);

    System.runAs(new User(Id = UserInfo.getUserId())) {
      nonSAPUser = TestDataFactoryMVN.createTestCallCenterUser();

      insert new PermissionSetAssignment(AssigneeId = nonSAPUser.Id, PermissionSetId = [select Id from PermissionSet where Name ='CRC_Order_Manager_MVN'].Id);
      insert new PermissionSetAssignment(AssigneeId = nonSAPUser.Id, PermissionSetId = [select Id from PermissionSet where Name='CRC_Campaign_Manager_MVN'].Id);
      
      //since criteria based sharing rules can't be tested, just make the rep the owner of the records that we are using.
      campaign.OwnerId = nonSAPUser.Id;
      campaign.Campaign_Letter_MVN__c = '01';
      update campaign;
    }  

    product.OwnerId = nonSAPUser.id;
    update product;

    for (Account a : accounts) {
      a.OwnerId = nonSAPUser.id;
    }
    update accounts;
    
    campProd = new Campaign_Product_MVN__c(Campaign_MVN__c = campaign.id, 
                                         Product_MVN__c = product.id, 
                                         Quantity_MVN__c=1);
    insert campProd;


    OrderUtilityMVN.userCountryCode = fakeCountryCode;
    OrderUtilityMVN.SAPOrder = false;
  }

  @isTest static void test_basic_page_load() {
    System.runAs(nonSAPUser) {
      controller = repOpensMassOrdersPage();

      System.assertEquals(targets.size(), controller.members.size());
      System.assertEquals(1, controller.campaignProducts.size());
    }
  }
  
  @isTest static void test_member_can_join_campaign() {
    System.runAs(nonSAPUser) {
      controller = repOpensMassOrdersPage();

      controller.targetId = controller.members[0].target.id;
      controller.memberJoin();
      System.assertEquals(1, [select Count() from Campaign_Target_vod__c where id=:controller.members[0].target.id AND Joined_MVN__c=true]);
    }
  }

  @isTest static void test_rep_can_edit_order() {
    System.runAs(nonSAPUser) {
      controller = repOpensMassOrdersPage();

      controller.targetId = controller.members[0].target.id;
      controller.memberJoin();
      controller.editOrder();
      controller.refreshMembers();
      System.assert(controller.members[0].order.id != null);
      System.assertEquals(controller.members[0].order.id, controller.callLinkId);
    }
  }

  @isTest static void test_rep_cannot_place_orders_unless_all_have_an_address() {
    System.runAs(nonSAPUser) {
      controller = repOpensMassOrdersPage();
      repJoinsAndSelectsFirst5();

      controller.checkOrders();
      controller.submitOrders();

      System.assert( controller.hasOrdersWithNoAddress);
      
    }
  }

  @isTest static void test_rep_cannot_place_orders_unless_all_have_a_full_address() {
    //the point of this test is to get the errors to fire in the order utility and cause
    //process orders to run
    System.runAs(nonSAPUser) {
      controller = repOpensMassOrdersPage();
      repJoinsAndSelectsFirst5();
      for (Integer i=0; i<5; i++) {
        controller.members[i].order.Ship_Address_Line_1_vod__c = 'test';
      }

      controller.checkOrders();
      controller.submitOrders();
      for (Integer i=0; i<5; i++) {
        System.assertEquals(Label.Order_Address_Required, controller.members[i].getErrors());  
      }
    }
  }

  @isTest static void test_rep_can_place_orders() {
    System.runAs(nonSAPUser) {
      OrderUtilityMVN.SAPOrder = false;
      controller = repOpensMassOrdersPage();
      Test.startTest();
      repJoinsAndSelectsFirst5();
      repAddsAddressToFirst5();

      controller.checkOrders();
      controller.submitOrders();
      Test.stopTest();
      List<Call2_vod__c> calls = [select Id from Call2_vod__c where Status_vod__c='Submitted_vod'];
      System.assertEquals( 5, calls.size());      
      System.assertEquals( 5, [select Count() from Call2_Sample_vod__c where Call2_vod__c IN :calls and Delivery_Status_vod__c = 'Submitted']);
    }
  }

  @isTest static void test_page_lets_rep_know_about_campaign_without_products() {
    delete campProd;
    System.runAs(nonSAPUser) {
      controller = repOpensMassOrdersPage();
      System.assertEquals(Label.Campaign_Has_No_Products,ApexPages.getMessages()[0].getSummary());
    }

  }

  @isTest static void test_default_records_per_page_is_20() {
    Service_Cloud_Settings_MVN__c changedSettings = [select Id, Mass_Order_Records_Per_Page_MVN__c from Service_Cloud_Settings_MVN__c limit 1];
    changedSettings.Mass_Order_Records_Per_Page_MVN__c = null;
    update changedSettings;

    System.runAs(nonSAPUser) {
      controller = repOpensMassOrdersPage();
      System.assertEquals(20, controller.recordsPerPage);
    }
  }

  @isTest static void test_member_methods() {
    System.runAs(nonSAPUser) {
      OrderUtilityMVN.SAPOrder = false;
      controller = repOpensMassOrdersPage();
      System.assert( !controller.members[0].getLocked() );
      System.assertEquals(System.Label.Campaign_New_Member_Status, controller.members[0].getStatus());

      Test.startTest();
      repJoinsAndSelectsFirst5();      
      System.assertEquals(System.Label.Campaign_Joined_Status, controller.members[0].getStatus());
      repAddsAddressToFirst5();

      controller.checkOrders();
      controller.submitOrders(); 
      Test.stopTest(); 
      Integer orderedCount = 0;
      for ( MassOrdersControllerMVN.Member member : controller.members) {
        System.debug(LoggingLevel.error,'!!!! ' + member.getStatus() + ' ' + member.getErrors());
        if (member.getStatus() == System.Label.Order_Delivery_Status_Submitted_MVN) {
          orderedCount++;
        }
      }
      System.assertEquals(5, orderedCount);

      System.assert( controller.members[0].getLocked() );
    }
  }

  @isTest static void test_member_methods_for_ineligible_account() {
    for (Account account : accounts) {
      account.OK_Status_Code_BI__c = 'Rejected';
    }
    update accounts;
    System.runAs(nonSAPUser) {
      controller = repOpensMassOrdersPage();      
      System.assertEquals(System.Label.Campaign_Ineligible_Status, controller.members[0].getStatus());
    }
  }

  @isTest static void test_batch_status_methods() {
    campaign.SAP_Batch_ID_MVN__c = 'notARealId';
    update campaign;
    System.runAs(nonSAPUser) {
      controller = repOpensMassOrdersPage();
      System.assert( !controller.hasRunningBatchJob );
      System.assertEquals( null, controller.last24HourStatus );
    }
  }

  @isTest static void test_navigation_controlls() {
    //set page size to one
    Service_Cloud_Settings_MVN__c scs = [select Id,Mass_Order_Records_Per_Page_MVN__c from Service_Cloud_Settings_MVN__c limit 1];
    scs.Mass_Order_Records_Per_Page_MVN__c =1;
    update scs;

    // members must be joined to be selected
    for(Campaign_Target_vod__c ct : targets) {
      ct.Joined_MVN__c = true;
    }
    update targets;

    controller = repOpensMassOrdersPage();

    //select first member
    controller.members[0].selected = true;
    Integer i = controller.selectedCount;

    System.assertEquals(1,controller.pageStartNumber);
    System.assertEquals(1,controller.pageEndNumber);

    controller.nextPage();
    System.assertEquals(1,controller.currentPage);
    controller.lastPage();
    System.assertEquals(4,controller.currentPage);
    System.assertEquals(5,controller.pageEndNumber);
    controller.nextPage();
    System.assertEquals(4,controller.currentPage);
    System.assertEquals(5,controller.pageEndNumber);
    controller.firstPage();
    System.assertEquals(0,controller.currentPage);
    System.assert(controller.members[0].selected);
    controller.previousPage();
    System.assertEquals(0,controller.currentPage);
    System.assert(controller.members[0].selected);

    controller.selectAllBool = true;
    controller.toggleSelectAll();
    System.assertEquals(5,controller.selectedCount);
    controller.lastPage();
    System.assert(controller.members[0].selected);
    controller.selectAllBool = false;
    controller.toggleSelectAll();
    System.assertEquals(0,controller.selectedCount);


    //add filters
    controller.firstNameFilter = 'Test';
    controller.lastNameFilter = '2';
    controller.dateJoinedFilter = Date.today().day().format();
    System.debug(LoggingLevel.Error,'!!! Joined Status: ' + controller.allMembers.values()[0].target.Status_Formula_MVN__c + ' : ' + System.Label.Campaign_Joined_Status);
    //controller.statusFilter = System.Label.Campaign_Joined_Status;
    controller.errorFilter = '';
    controller.refreshMembers();
    System.assertEquals(1,controller.currentListSize);
    System.assertEquals(0,controller.currentPage);

    //test error filter
    controller.firstNameFilter = '';
    controller.lastNameFilter = '';
    controller.dateJoinedFilter = '';
    controller.statusFilter = '';
    controller.errorFilter = 'True';
    Address_vod__c addr = TestDataFactoryMVN.createTestAddress( controller.members[0].act);
    controller.refreshMembers();
    System.assertEquals(4,controller.currentListSize);
  }
  
  private static MassOrdersControllerMVN repOpensMassOrdersPage() {    
    Test.setCurrentPage(Page.MassOrdersMVN);
    ApexPages.StandardController sc = new ApexPages.StandardController(campaign);
    return new MassOrdersControllerMVN(sc);
  }

  private static void repJoinsAndSelectsFirst5() {
    //join the first five to the campaign and select them
    for (Integer i = 0; i<5; i++) {
      for (Integer j = 0; j<5; j++) {
        // needed because of the member refresh on each join could potentialy change the order
        if(!controller.members[j].target.Joined_MVN__c) {
          controller.targetID = controller.members[j].target.id;
          controller.members[j].selected = true;
          Integer k = controller.selectedCount;
          controller.memberJoin();
          break;
        }
      }
    }

    for (Integer j = 0; j<5; j++) {
      System.assert(controller.members[j].selected);
    }
  }

  private static void repAddsAddressToFirst5() {
    for (Integer i = 0; i<5; i++) {
      Address_vod__c addr = TestDataFactoryMVN.createTestAddress( controller.members[i].act);
      controller.members[i].order.Ship_Address_Line_1_vod__c = addr.Name;
      controller.members[i].order.Ship_to_Name_MVN__c = 'a name';
      controller.members[i].order.Ship_City_vod__c = addr.City_vod__c;
      controller.members[i].order.Ship_zip_vod__c = addr.Zip_vod__c;
      controller.members[i].order.Ship_Country_vod__c = addr.Country_Code_BI__c;
      controller.members[i].order.Ship_To_Salutation_MVN__c = 'Hello Dr. Smith: ';

    }
  }
}