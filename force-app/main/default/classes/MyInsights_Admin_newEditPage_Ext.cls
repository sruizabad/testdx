/*
* Controller extension Class for VisualForce Page: MyInsights_Admin_NewEditPage.vfp
* 
* 02/06/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Initial Creation (#CCL DataLoader)
* 01/09/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Added Multiselect Picklist
* 20/10/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Added Deep Clone functionality
* 01/06/2018 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Added Tab creation + modified deep clone to include Tabs
*
*/
public without sharing class MyInsights_Admin_newEditPage_Ext {
    
    private MyInsights_Admin__c MyInsights_AdminRecord {get;set;}
    private MyInsights_Admin__c MyInsights_AdminRecordID;
    private MyInsights_Admin__c MyInsights_AdminRecordIDs;
    
    private List<SelectOption> recordTypeNames = new List<SelectOption>();    
    
    public SelectOption[] selectedProfiles { get; set; }
    public SelectOption[] allProfiles { get; set; }
    public SelectOption[] selectedAccountRTs { get; set; }
    public SelectOption[] allAccountRTs { get; set; }
    
    public String sProfiles { get; set; }
    public String sAccountRTs { get; set; }
    public Boolean empty;
    public Boolean isAccountRT = true; 
    private String listViewRecordType;
    
    
    public MyInsights_Admin_newEditPage_Ext(ApexPages.StandardController MyInsights_stdCon) {
        this.MyInsights_AdminRecord = (MyInsights_Admin__c) MyInsights_stdCon.getRecord();
        
        //needed for populating Profile__c
        MyInsights_Admin__c[] MyInsights_AdminRecordIDs = [SELECT Id, Profile__c, Account_Record_Types__c FROM MyInsights_Admin__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        
        //Populating Selected profiles
        selectedProfiles = new List<SelectOption>();
        
        empty = true;
        if (!MyInsights_AdminRecordIDs.isEmpty()){
            empty = false; 
            
            if(!String.isBlank(MyInsights_AdminRecordIDs[0].Profile__c)){
                
                List<String> strSelectedProfiles = MyInsights_AdminRecordIDs[0].Profile__c.split(',');//removing delimiter between profiles
                for(String s : strSelectedProfiles){
                    selectedProfiles.add(new SelectOption(s,s));
                }
            }
        }
        
        //querying and ordering Profiles
        List<Profile> profiles = [SELECT Name, Id FROM Profile ORDER BY NAME ASC];    
        allProfiles = new List<SelectOption>();
        
        //checking if field already populated, remove already selected profiles
        if(!empty && MyInsights_AdminRecordIDs[0].Profile__c != null){
            for ( Profile p : profiles ) {                
                if(!MyInsights_AdminRecordIDs[0].Profile__c.contains(p.Name)){                  
                    allProfiles.add(new SelectOption(p.Name, p.Name));
                }
            }
        }
        //If field is empty, add all profiles
        Else{
            for ( Profile p : profiles ) {         
                allProfiles.add(new SelectOption(p.Name, p.Name));                
            }
        }
        
        //Populating Selected AccountRecordTypes
        selectedAccountRTs = new List<SelectOption>();
        if(!empty){
            if(!String.isBlank(MyInsights_AdminRecordIDs[0].Account_Record_Types__c)){
                
                List<String> strSelectedAccRTs = MyInsights_AdminRecordIDs[0].Account_Record_Types__c.split(',');//removing delimiter between AccountRTs
                for(String s : strSelectedAccRTs){
                    selectedAccountRTs.add(new SelectOption(s,s));
                }
            }
        }
        
        
        //querying Account RTs
        List<RecordType> recordTypes = [SELECT DeveloperName, Id FROM RecordType where SobjectType = 'Account' ORDER BY DeveloperName ASC];
        allAccountRTs = new List<SelectOption>();
        
        //checking if field already populated, remove already selected accountRTs
        if(!empty && MyInsights_AdminRecordIDs[0].Account_Record_Types__c != null){
            for ( RecordType r : recordTypes) {                
                if(!MyInsights_AdminRecordIDs[0].Account_Record_Types__c.contains(r.DeveloperName)){                  
                    allAccountRTs.add(new SelectOption(r.DeveloperName, r.DeveloperName));
                }
            }
        }
        //If field is empty, add all accountRTs
        Else{
            for ( RecordType r : recordTypes ) {
                allAccountRTs.add(new SelectOption(r.DeveloperName, r.DeveloperName));
            }
        }
        
    }
    
    
    
    public Boolean getisAccountRT(){
        return isAccountRT;
    }
    
    
    //Save button: customized for saving the Profile(s) ans Account RTs
    public PageReference save() {        
        Boolean firstPro = true;
        Boolean firstAcc = true;
        sProfiles = '';
        sAccountRTs = '';
        
        for ( SelectOption so : selectedProfiles ) {
            if (!firstPro) {
                sProfiles += ', '; //adding delimiter between profiles
            }
            sProfiles += so.getLabel();
            firstPro = false;
        }
        
        for ( SelectOption ar : selectedAccountRTs ) {
            if (!firstAcc) {
                sAccountRTs += ', '; //adding delimiter between Account RTs
            }
            sAccountRTs += ar.getLabel();
            firstAcc = false;
        }
        
        //Checking if the record is new
        if(!empty){
            MyInsights_AdminRecordID = [SELECT Id, Profile__c FROM MyInsights_Admin__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')][0];
        }
        Else{
            MyInsights_AdminRecordID = new MyInsights_Admin__c();            
        }
        
        MyInsights_AdminRecordID.Profile__c = sProfiles;
        MyInsights_AdminRecordID.Status__c = MyInsights_AdminRecord.Status__c;
        MyInsights_AdminRecordID.Description__c = MyInsights_AdminRecord.Description__c;
        MyInsights_AdminRecordID.Country_Code__c = MyInsights_AdminRecord.Country_Code__c;
        //MyInsights_AdminRecordID.Ownerid = MyInsights_AdminRecord.Ownerid
        MyInsights_AdminRecordID.RecordTypeId = MyInsights_AdminRecord.RecordTypeId;
        MyInsights_AdminRecordID.HTML_Report__c = MyInsights_AdminRecord.HTML_Report__c;
        MyInsights_AdminRecordID.Account_Record_Types__c = sAccountRTs;
        MyInsights_AdminRecordID.MyInsights_Previous_Months__c = MyInsights_AdminRecord.MyInsights_Previous_Months__c;
        MyInsights_AdminRecordID.MyInsights_Next_Months__c = MyInsights_AdminRecord.MyInsights_Next_Months__c;
        MyInsights_AdminRecordID.MyInsights_Use_Cycle_Plan_Dates__c = MyInsights_AdminRecord.MyInsights_Use_Cycle_Plan_Dates__c;
        
        upsert MyInsights_AdminRecordID;
        
        //If the record is new: create a default Tab record
        if(empty){
            MyInsights_Tab__c MyInsightsTab = New MyInsights_Tab__c();
            MyInsightsTab.MyInsights_Layout__c = MyInsights_AdminRecordID.Id;
            MyInsightsTab.MyInsights_Order__c = '1';
            MyInsightsTab.MyInsights_Tab_Name__c = 'Default';
            
            insert MyInsightsTab;
        }
        
        //return to Detail Page
        pagereference pr = new pagereference('/' + MyInsights_AdminRecordID.id);
        
        return pr;
    }   
    
    
    
    // Get RecordType Names
    public List<SelectOption> getMyInsights_recordtypesNames() {       
        List<RecordType> allRecordType = [Select Name From RecordType Where SobjectType = 'MyInsights_Admin__c'];
        recordTypeNames.clear();
        for(RecordType rtp: allRecordType){
            String rtpName = rtp.Name;
            recordTypeNames.add(new SelectOption(rtp.Id, rtpName));
            
            // Put Default RecordType when record is new
            if(rtpName == 'Account_Reports_vod' && empty){
                MyInsights_AdminRecord.RecordTypeId = rtp.Id;
            }       
        }
        recordTypeNames.sort();
        return recordTypeNames;
    }
    
    
    
    //Clone Button: Deep clone
    public PageReference cloneWithItems() {
        
        // setup the save point for rollback
        Savepoint sp = Database.setSavepoint();
        MyInsights_Admin__c newMA;
        
        try {
            
            //copy the MyInsights Admin record order - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
            MyInsights_AdminRecord = [select Id, Country_Code__c, Profile__c, Description__c, RecordTypeId, OwnerId, HTML_Report__c, Account_Record_Types__c, MyInsights_Use_Cycle_Plan_Dates__c, MyInsights_Previous_Months__c, MyInsights_Next_Months__c from MyInsights_Admin__c where id = :MyInsights_AdminRecord.id];
            newMA = MyInsights_AdminRecord.clone(false);
            insert newMA;

			// copy over the Tabs - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
			Map<ID,ID> TabIds = new Map<ID,ID>();
            for(MyInsights_Tab__c lt : [Select t.Id, t.MyInsights_Layout__c, t.MyInsights_Order__c, t.MyInsights_Tab_Name__c From MyInsights_Tab__c t where MyInsights_Layout__c = :MyInsights_AdminRecord.id]){
                MyInsights_Tab__c newLT = lt.clone(false);
                newLT.MyInsights_Layout__c = newMA.id;
                insert newLT;
                TabIds.put(lt.Id,newLT.Id);
            }

            // copy over the list view items - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
            for (MyInsights_List_View__c lv : [Select l.Id, l.List_View__c, l.MyInsights_Selected_Object_Name__c, l.Displayed_On__c From MyInsights_List_View__c l where MyInsights_Admin__c = :MyInsights_AdminRecord.id]) {
                MyInsights_List_View__c newLV = lv.clone(false);
                newLV.MyInsights_Admin__c = newMA.id;
                //listItems.add(newLV);
                insert newLV;
                
                // copy over the list view definition items - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
                List<MyInsights_List_View_Definition__c> listDefItems = new List<MyInsights_List_View_Definition__c>();
                for (MyInsights_List_View_Definition__c lvd : [Select d.Id, d.MyInsights_SObject_Name__c, d.MyInsights_SObject_Field__c, d.MyInsights_Field_Type__c, d.MyInsights_Reference_SObject_Name__c, d.MyInsights_Reference_SObject_Field__c, d.MyInsights_Display__c, d.MyInsights_Order__c, d.MyInsights_Sorting__c, d.MyInsights_Icon__c From MyInsights_List_View_Definition__c d where MyInsights_List_View__c = :lv.id]) {
                    MyInsights_List_View_Definition__c newLVD = lvd.clone(false);
                    newLVD.MyInsights_List_View__c = newLV.id;
                    listDefItems.add(newLVD);
                } 
                insert listDefItems;
                
                // copy over the Tabs - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
				//List<MyInsights_Tab__c> tabList = new List<MyInsights_Tab__c>();
                //for(MyInsights_Tab__c lt : )
                
                // copy over the reporting block items with list view - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
                List<MyInsights_Reporting_Block_Lay_Out__c> blockItems = new List<MyInsights_Reporting_Block_Lay_Out__c>();
                for (MyInsights_Reporting_Block_Lay_Out__c rb : [Select p.Id, p.Order__c, p.MyInsights_Tab__c, p.Chart_Title__c, p.Chart_Title_Veeva_Message__c, p.Column_Number__c, p.MyInsights_Block_Custom_Parameters__c, p.Description__c, p.MyInsights_Reporting_Block__c, p.Row_Number__c From MyInsights_Reporting_Block_Lay_Out__c p where MyInsights_Admin__c = :MyInsights_AdminRecord.id and MyInsights_List_View__c =:lv.id]) {
                    MyInsights_Reporting_Block_Lay_Out__c newRBL = rb.clone(false);
					newRBL.MyInsights_Tab__c = TabIds.get(rb.MyInsights_Tab__c);
                    newRBL.MyInsights_Admin__c = newMA.id;
                    newRBL.MyInsights_List_View__c = newLV.id;
                    blockItems.add(newRBL);
                }
                insert blockItems;
            }
            
            // copy over the reporting block items without list view - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
            List<MyInsights_Reporting_Block_Lay_Out__c> blockItems = new List<MyInsights_Reporting_Block_Lay_Out__c>();
            for (MyInsights_Reporting_Block_Lay_Out__c rb : [Select p.Id, p.Order__c, p.MyInsights_Tab__c, p.Chart_Title__c, p.Chart_Title_Veeva_Message__c, p.Column_Number__c, p.MyInsights_Block_Custom_Parameters__c, p.Description__c, p.MyInsights_Reporting_Block__c, p.Row_Number__c From MyInsights_Reporting_Block_Lay_Out__c p where MyInsights_Admin__c = :MyInsights_AdminRecord.id and MyInsights_List_View__c ='']) {
                MyInsights_Reporting_Block_Lay_Out__c newRBL = rb.clone(false);
                newRBL.MyInsights_Tab__c = TabIds.get(rb.MyInsights_Tab__c);
                newRBL.MyInsights_Admin__c = newMA.id;
                blockItems.add(newRBL);
            }
            insert blockItems;
            
        } catch (Exception e){
            // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
        }
        
        //return new PageReference('/'+newMA.id+'/e?retURL=%2F'+newMA.id);
        return new PageReference('/'+newMA.id);
    }
    
}