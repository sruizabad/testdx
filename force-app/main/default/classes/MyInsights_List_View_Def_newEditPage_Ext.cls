/*
* Controller extension Class for VisualForce Page: MyInsights_List_View_Def_NewEditPage.vfp
* 
* 02/06/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Initial Creation (#CCL DataLoader)
* 06/09/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Added Save and New button to redirect with fields prepopulated
* 24/10/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Added Check if profiles have Read access. --- REMOVED
* 
*/
public without sharing class MyInsights_List_View_Def_newEditPage_Ext {
    
    private ApexPages.StandardController controller;
    private MyInsights_List_View_Definition__c MyInsights_listViewDefRecord;
    private String selectedObject;
    private String MyInsights_fieldType = '';
    private String s;
    private List<SelectOption> fieldNames = new List<SelectOption>();
    private List<SelectOption> referenceFields = new List<SelectOption>();  
    
    public Boolean isReference = false;
    public Boolean isAccount = false;
    //public Boolean isNotReadableByProfiles = false;   
    
    public MyInsights_List_View_Def_newEditPage_Ext(ApexPages.StandardController MyInsights_stdCon){
        this.controller = MyInsights_stdCon;
        this.MyInsights_listViewDefRecord = (MyInsights_List_View_Definition__c) MyInsights_stdCon.getRecord();
        
        if(MyInsights_listViewDefRecord.MyInsights_List_View__c != null) {
            
            List<MyInsights_List_View_Definition__c> listViewDefRecord_lst = new List<MyInsights_List_View_Definition__c>();
            listViewDefRecord_lst.add(MyInsights_listViewDefRecord);
            MyInsights_UtilityClass.MyInsights_SelectedObject(listViewDefRecord_lst);
            //System.Debug('--> sObject Name: ' + MyInsights_listViewDefRecord.MyInsights_SObject_Name__c);
            
            selectedObject = MyInsights_listViewDefRecord.MyInsights_SObject_Name__c;
            
            if(selectedObject == 'Account'){
                isAccount = true;
            }
        }
        
        //Retrieve Fields of object
        if(selectedObject != '' && selectedObject != null) {
            fieldNames.clear();
            fieldNames.add(new SelectOption('--None--','--None--'));
            for(SObjectField field:MyInsights_UtilityClass.MyInsights_getMap_fieldName_sObjectField(selectedObject).values()){
                String fieldName = field.getDescribe().getName();
                fieldNames.add(new SelectOption(fieldName, fieldName));
            }
            fieldNames.sort();
        }
        FieldType();
        
    }
    
    
    public String selectedField {get;}
    
    
    public List<SelectOption> getObjectFields() {
        return fieldNames;
    }
    
    public List<SelectOption> getReferenceFields() {
        return referenceFields;
    }
    
    // Get Field Type
    public void FieldType(){
        if (MyInsights_listViewDefRecord.MyInsights_SObject_Field__c == '--None--'){
            MyInsights_fieldType = '';
            MyInsights_listViewDefRecord.MyInsights_Field_Type__c = MyInsights_fieldType;
            isReference = false;
            //isNotReadableByProfiles = false;
        }
        else if(MyInsights_listViewDefRecord.MyInsights_SObject_Field__c != null){
            
            List<MyInsights_List_View_Definition__c> listViewDefRecord_lst = new List<MyInsights_List_View_Definition__c>();
            listViewDefRecord_lst.add(MyInsights_listViewDefRecord);
            MyInsights_UtilityClass.MyInsights_FieldType(listViewDefRecord_lst);
            
            MyInsights_fieldType = MyInsights_listViewDefRecord.MyInsights_Field_Type__c;
            
            //IsReadable();
            
            if(MyInsights_fieldType == 'REFERENCE'){
                isReference = true;
                
                //Retrieve related object via lookup field
                MyInsights_listViewDefRecord.MyInsights_Reference_SObject_Name__c = MyInsights_UtilityClass.findObjectNameFromLookup(MyInsights_listViewDefRecord.MyInsights_SObject_Name__c, MyInsights_listViewDefRecord.MyInsights_SObject_Field__c);
                
                if(MyInsights_listViewDefRecord.MyInsights_Reference_SObject_Name__c == 'Group')
                {
                    MyInsights_listViewDefRecord.MyInsights_Reference_SObject_Name__c = 'User';
                }
                
                
                //Retrieve Fields of related object
                if(MyInsights_listViewDefRecord.MyInsights_Reference_SObject_Name__c != '' && MyInsights_listViewDefRecord.MyInsights_Reference_SObject_Name__c != null) {	
                    referenceFields.clear();
                    referenceFields.add(new SelectOption('--None--','--None--'));
                    for(SObjectField field:MyInsights_UtilityClass.MyInsights_getMap_fieldName_sObjectField(MyInsights_listViewDefRecord.MyInsights_Reference_SObject_Name__c).values()){
                        String reffieldName = field.getDescribe().getName();
                        referenceFields.add(new SelectOption(reffieldName, reffieldName));
                        referenceFields.sort();
                    }
                    fieldNames.sort();
                }                  
            }
            else {
                isReference = false;
            }
            //IsReadable();
            
        }
        else {
            System.Debug('===emptySObject==='); 
        }
        
    }
    
    //Check if selected field is accessible for the profiles on the MyInsights admin record
    /*
    public void IsReadable(){
        isNotReadableByProfiles = false;
        MyInsights_listViewDefRecord.MyInsights_Profile_Access_Warning__c = '';
        //Excluding fields that are always readable: No FieldPermission record existing
        List<String> fieldsAlwaysReadable = new List<String>{'Id','CreatedById','CreatedDate','IsDeleted','LastModifiedById','LastModifiedDate','SystemModStamp','Name','OwnerId','ParentId'};
            
        if(String.join(fieldsAlwaysReadable,',').contains(MyInsights_listViewDefRecord.MyInsights_SObject_Field__c)){
                isNotReadableByProfiles = false;
            }
        Else{
            //Getting All profiles that have Read access to the selected field
            s = MyInsights_listViewDefRecord.MyInsights_SObject_Name__c + '.' + MyInsights_listViewDefRecord.MyInsights_SObject_Field__c;
            List <PermissionSet> profileRead = [SELECT Profile.Name FROM PermissionSet WHERE IsOwnedByProfile = true AND Id IN (SELECT ParentId FROM FieldPermissions WHERE PermissionsRead = true AND Field =:s AND SobjectType =: MyInsights_listViewDefRecord.MyInsights_SObject_Name__c)];
            List <String> profileNamesAvailable = new List<String>();
            for(PermissionSet ps: profileRead){
                profileNamesAvailable.add(ps.Profile.Name);
            }
            String profileNamesAv = String.join(profileNamesAvailable,',');
            
            //Getting All profiles selected in the MyInsights+ Admin record
            MyInsights_Admin__c adminRecord = [SELECT Profile__c FROM MyInsights_Admin__c WHERE Id IN (SELECT MyInsights_Admin__c FROM MyInsights_List_View__c WHERE Id =: MyInsights_listViewDefRecord.MyInsights_List_View__c)][0];
            List <String> profileNamesToCheck = adminRecord.Profile__c.split(', ');
            
            //Compare profiles and add to list if no Read access
            List <String> profileNamesNoAccess = new List<String>();
            //profileNamesNoAccess.clear();
            for(String pnc:profileNamesToCheck)
            {
                if(!profileNamesAv.contains(pnc))
                {
                    isNotReadableByProfiles = true;
                    profileNamesNoAccess.add(pnc);
                }
            }
            
            //Populate profiles with no Read access to the selected field in MyInsights_Profile_Access_Warning__c field.
            MyInsights_listViewDefRecord.MyInsights_Profile_Access_Warning__c = String.join(profileNamesNoAccess,', ');
        }
        

    }*/
    
    
    public Boolean getisReference(){
        return isReference;
    }
    
    public Boolean getisAccount(){
        return isAccount;
    }
    
    //public Boolean getisNotReadableByProfiles(){
    //    return isNotReadableByProfiles;
    //}
    
    //Save button
    public PageReference save(){
        try{
            this.controller.save();
        } catch(System.DMLException e) {
            System.Debug('======e: ' + e);
        } catch(exception MyInsights_e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, MyInsights_e.getDmlMessage(0)));
        }
        
        if(!ApexPages.hasMessages()) {
            PageReference MyInsights_returnPage = new PageReference('/'+MyInsights_listViewDefRecord.MyInsights_List_View__c);		
            MyInsights_returnPage.setRedirect(true);
            return MyInsights_returnPage;
        }
        return null;
    }    
    
    //Save and New button
    public PageReference saveNew() {
        try{
            this.controller.save();
        } catch(System.DMLException e) {
            System.Debug('======e: ' + e);
        } catch(exception MyInsights_e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, MyInsights_e.getDmlMessage(0)));
        }
        
        if(!ApexPages.hasMessages()) {
            
            //Added Queries to redirect with fields prepopulated
            EntityDefinition ed = [Select DeveloperName, DurableId, EditUrl, NewUrl From EntityDefinition Where DeveloperName = 'MyInsights_List_View_Definition'];
            System.Debug('====== EntityDefinition: ' + ed);
            FieldDefinition fd = [Select DeveloperName, DurableId, EntityDefinitionId From FieldDefinition Where EntityDefinitionId = :ed.DurableId And DeveloperName = 'MyInsights_List_View'];
            System.Debug('====== FieldDefinition: ' + fd);
            String fieldID = fd.DurableId.substringAfter('.');
            System.Debug('====== FieldID: ' + fieldID);
            
            PageReference pg = new PageReference('/apex/MyInsights_List_View_Def_NewEditPage?CF' + fieldID + '=' + MyInsights_listViewDefRecord.MyInsights_List_View__c + '&CF' + fieldID + '_lkid=' + MyInsights_listViewDefRecord.MyInsights_List_View__c + '&scontrolCaching=1&retURL=%2F' + MyInsights_listViewDefRecord.MyInsights_List_View__c + '&sfdc.override=1');
            
            pg.setRedirect(true);
            return pg;
            
        }
        
        return null;
    }
}