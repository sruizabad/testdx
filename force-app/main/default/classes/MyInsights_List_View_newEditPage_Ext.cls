/*
* Controller extension Class for VisualForce Page: MyInsights_List_View_NewEditPage.vfp
* 
* 02/06/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Initial Creation (#CCL DataLoader)
* 05/09/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Fix for SelectOption bigger than 1000
* 24/10/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Added Check if profiles have Read access. --- REMOVED
* 
*/
public without sharing class MyInsights_List_View_newEditPage_Ext {
    
    private ApexPages.StandardController controller;
    private MyInsights_List_View__c MyInsights_listViewRecord;
    //public Boolean isNotReadableByProfiles = false;   
    private static Map<String, Schema.SObjectType> MyInsights_map_stringKeyName_schemasObjects = Schema.getGlobalDescribe();
    
    public MyInsights_List_View_newEditPage_Ext(ApexPages.StandardController MyInsights_stdCon) {
        this.controller = MyInsights_stdCon;
        this.MyInsights_listViewRecord = (MyInsights_List_View__c) MyInsights_stdCon.getRecord();
        //IsReadable();
    }
    
    public MyInsights_PaginatedSelectList MyInsights_sObjectNames{
        
        get{
            List<MyInsights_schemaSObjectTypeWrapper> MyInsights_list_wrappedOptions = new List<MyInsights_schemaSObjectTypeWrapper>(); 
            
            for(Schema.SObjectType MyInsights_schemaSObjectType : MyInsights_map_stringKeyName_schemasObjects.values()) {
                MyInsights_list_wrappedOptions.add(new MyInsights_schemaSObjectTypeWrapper(MyInsights_schemaSObjectType));                
            }
            
            MyInsights_list_wrappedOptions.sort();
            
            if(MyInsights_sObjectNames==null){
                MyInsights_sObjectNames=new MyInsights_PaginatedSelectList();
                for(MyInsights_schemaSObjectTypeWrapper MyInsights_schemaSObjectType : MyInsights_list_wrappedOptions) {
                    Schema.DescribeSObjectResult MyInsights_describeSObject = MyInsights_schemaSObjectType.MyInsightsObj.getDescribe();
                    MyInsights_sObjectNames.add(new SelectOption(MyInsights_describeSObject.getName(),MyInsights_describeSObject.getLabel()));
                } 
            }
            return MyInsights_sObjectNames;
            
        }
        set;
    }
    
    //Check if selected object is accessible for the profiles on the MyInsights admin record
    /*public void IsReadable(){
        isNotReadableByProfiles = false;
        MyInsights_listViewRecord.MyInsights_Profile_Access_Warning__c = '';
        
        //Getting All profiles that have Read access to the selected object
        List <PermissionSet> profileRead = [SELECT Profile.Name FROM PermissionSet WHERE IsOwnedByProfile = true AND Id IN (SELECT ParentId FROM ObjectPermissions WHERE PermissionsRead = true AND SobjectType =: MyInsights_listViewRecord.MyInsights_Selected_Object_Name__c)];
        List <String> profileNamesAvailable = new List<String>();
        for(PermissionSet ps: profileRead){
            profileNamesAvailable.add(ps.Profile.Name);
        }
        String profileNamesAv = String.join(profileNamesAvailable,',');
        
        //Getting All profiles selected in the MyInsights+ Admin record
        MyInsights_Admin__c adminRecord = [SELECT Profile__c FROM MyInsights_Admin__c WHERE Id =: MyInsights_listViewRecord.MyInsights_Admin__c];
        List <String> profileNamesToCheck = adminRecord.Profile__c.split(', ');
        
        //Compare profiles and add to list if no Read access
        List <String> profileNamesNoAccess = new List<String>();
        //profileNamesNoAccess.clear();
        for(String pnc:profileNamesToCheck)
        {
            if(!profileNamesAv.contains(pnc))
            {
                isNotReadableByProfiles = true;
                profileNamesNoAccess.add(pnc);
            }
        }
        
        //Populate profiles with no Read access to the selected object in MyInsights_Profile_Access_Warning__c field.
        MyInsights_listViewRecord.MyInsights_Profile_Access_Warning__c = String.join(profileNamesNoAccess,', ');        
    }

    
    public Boolean getisNotReadableByProfiles(){
        return isNotReadableByProfiles;
    }*/
    
}