@isTest
private class MyInsights_MultiselectControllerTest {
    static testMethod void testMultiselectController() {
        MyInsights_MultiselectController c = new MyInsights_MultiselectController();
        
        c.leftOptions = new List<SelectOption>();
        c.rightOptions = new List<SelectOption>();

        c.leftOptionsHidden = 'A&a&b&b&C&c';
        c.rightOptionsHidden = '';
        
        System.assertEquals(c.leftOptions.size(), 3);
        System.assertEquals(c.rightOptions.size(), 0);
    }
}