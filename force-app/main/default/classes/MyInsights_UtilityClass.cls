/*
* Helper Class for Controller extension Class MyInsights_List_View_Def_newEditPage_Ext.apxc
* 
* 02/06/2017 - Wouter Jacops <wouter.jacops@c-clearpartners.com>: Initial Creation (#CCL DataLoader)
* 
*/
public class MyInsights_UtilityClass {
    
    // Method to get the fields od the selected object
    public static Map<String, Schema.SObjectField> MyInsights_getMap_fieldName_sObjectField(String MyInsights_stringObjectAPI) {
        if(MyInsights_stringObjectAPI != null) {
            Map<String, Schema.SObjectType> MyInsights_schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType MyInsights_objectSchema = MyInsights_schemaMap.get(MyInsights_stringObjectAPI);
            
            return MyInsights_objectSchema.getDescribe().fields.getMap();
        }
        return null;
    }
    
    // Method to get the Object API Name where the lookup field is pointing to
    public static String findObjectNameFromLookup(String objectName, String fieldName){
        String lookupObjectName = '';
        try{
            lookupObjectName = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(fieldName).getDescribe().getReferenceTo().get(0).getDescribe().getName();
        }catch(Exception e){
            System.debug(e);
        }
        return lookupObjectName;
    }
    
    // Method to get the selected object
    public static void MyInsights_SelectedObject(
        List<MyInsights_List_View_Definition__c> listViewDefRecord_lst){
            List<ID> listViewID_lst = new List<ID>();
            List<MyInsights_List_View__c> listView_lst = new List<MyInsights_List_View__c>();
            Map<MyInsights_List_View_Definition__c, String> errorMessages = new Map<MyInsights_List_View_Definition__c, String>();
            
            // Get the list of all MyInsightslistView IDs
            for(MyInsights_List_View_Definition__c listViewDefRecord : listViewDefRecord_lst){
                listViewID_lst.add(listViewDefRecord.MyInsights_List_View__c);
            }
            
            // Create a list, based on the MyInsightslistView IDs, with the MyInsightslistView records 
            // and the needed sObject Name
            listView_lst = [SELECT 	MyInsights_Selected_Object_Name__c 
                            FROM		MyInsights_List_View__c
                            WHERE		Id =: listViewID_lst];
            
            // Populate the MyInsightslistViewDefinition Record with the sObject Name from the corresponding MyInsightslistView
            for(MyInsights_List_View_Definition__c listViewDefRecord : listViewDefRecord_lst){
                for (MyInsights_List_View__c listViewRec : listView_lst){
                    if (listViewRec.Id == listViewDefRecord.MyInsights_List_View__c){
                        listViewDefRecord.MyInsights_Sobject_Name__c = listViewRec.MyInsights_Selected_Object_Name__c;
                    }
                }
            }
        }
    
    // Method to get the field type
    public static Map<MyInsights_List_View_Definition__c, String> MyInsights_FieldType(List<MyInsights_List_View_Definition__c> listViewDefRecord_lst){		
        Map<String, Schema.SObjectType> MyInsights_schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.DescribeSObjectResult> describeObject = new Map<String, Schema.DescribeSObjectResult>();
        Map<MyInsights_List_View_Definition__c, String> errorMessages = new Map<MyInsights_List_View_Definition__c, String>();
        
        for(MyInsights_List_View_Definition__c listViewDefRecord : listViewDefRecord_lst){
            try{
                Schema.SObjectType MyInsights_objectSchema = MyInsights_schemaMap.get(listViewDefRecord.MyInsights_Sobject_Name__c);
                if (describeObject.get(listViewDefRecord.MyInsights_Sobject_Name__c) == null){
                    describeObject.put(listViewDefRecord.MyInsights_Sobject_Name__c, MyInsights_objectSchema.getDescribe());
                }
                Schema.DisplayType myType = describeObject.get(listViewDefRecord.MyInsights_Sobject_Name__c).fields.getMap()
                    .get(listViewDefRecord.MyInsights_SObject_Field__c).getDescribe().getType();
                
                listViewDefRecord.MyInsights_Field_Type__c = String.valueOf(myType);
                System.Debug('---> field Type: '+String.valueOf(myType));
            }
            catch (Exception e){
                errorMessages.put(listViewDefRecord, e.getMessage());
            }
        }
        return errorMessages;
    }   
}