global class MyInsights_schemaSObjectTypeWrapper implements comparable{
    public Schema.SObjectType MyInsightsObj;
    
    public MyInsights_schemaSObjectTypeWrapper(Schema.SObjectType obj){
        MyInsightsObj = obj;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        MyInsights_schemaSObjectTypeWrapper compareToMyInsightsObj = (MyInsights_schemaSObjectTypeWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (MyInsightsObj.getdescribe().getLabel() > compareToMyInsightsObj.MyInsightsObj.getdescribe().getLabel()) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (MyInsightsObj.getdescribe().getLabel() < compareToMyInsightsObj.MyInsightsObj.getdescribe().getLabel()) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}