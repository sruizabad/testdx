global without sharing class OneKey_Repair_GET_ALL_ACCOUNTS
    implements Database.Batchable<SObject> {

    private List<OneKey_AccountAddress_Repair__c> accountFixes;

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String selStmt =
            'SELECT'
            + '  OK_External_ID_BI__c, isPersonAccount, Country_Code_BI__c'
            + ' FROM Account'


            + ' WHERE Country_Code_BI__c = \'CA\''
            + '  AND OK_External_ID_BI__c != null'

            + '  AND isPersonAccount = true'

            + '  AND OK_Status_Code_BI__c != \'Invalid\''
            + '  AND (Status_BI__c = \'Active\' OR Status_BI__c = \'Open\' OR Status_BI__c = \'Valid\' OR Status_BI__c = \'Provisional\')'
            + '  AND OK_External_ID_BI__c LIKE \'W%\''


            /*
            + ' WHERE Country_Code_BI__c != null'
            + '  AND OK_External_ID_BI__c != null'
            + '  AND OK_Status_Code_BI__c != \'Invalid\''
            + '  AND (Status_BI__c = \'Active\' OR Status_BI__c = \'Open\' OR Status_BI__c = \'Valid\' OR Status_BI__c = \'Provisional\')'
            + '  AND OK_External_ID_BI__c LIKE \'W%\''
            */
            ;

        return Database.getQueryLocator(selStmt);

    }

    global void execute(Database.BatchableContext BC, List<Account> oneKeyAccounts) {

        accountFixes = new List<OneKey_AccountAddress_Repair__c>();

        for (Account oneKeyAccount : oneKeyAccounts) {
            OneKey_AccountAddress_Repair__c accountFix = new OneKey_AccountAddress_Repair__c(
                Name = oneKeyAccount.OK_External_ID_BI__c,
                Country_Code_BI__c = oneKeyAccount.Country_Code_BI__c,
                hasBeenCorrected_BI__c = false,
                // This is the default (hasBeenCorrected - false) - not doing this
                // as not to overwrite completed work
                isPersonAccount_BI__c = oneKeyAccount.isPersonAccount
            );

            accountFixes.add(accountFix);
        }

        Database.upsert(accountFixes, OneKey_AccountAddress_Repair__c.Name, true);

    }

    global void finish(Database.BatchableContext BC) {
    }

}