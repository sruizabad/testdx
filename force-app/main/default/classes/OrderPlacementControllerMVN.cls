/*
  * OrderPlacementControllerMVN
  *    Created By:     Kai Amundenden   
  *    Created Date:    August 12, 2013
  *    Description:     Controller for the single order page, handles updating and sending single orders for CRC
 */

public without sharing class OrderPlacementControllerMVN {

    public Boolean orderLocked {get; private set;}
    public Boolean validatedHCP {get; private set;}
    public Boolean hasCall {get; private set;}
    public Boolean hasAccount {get; private set;}
    public Case orderCase {get; private set;}
    public Account orderAccount {get; private set;}
    public Call2_vod__c call {get; private set;}
    public Map<Id,OrderLine> orderLines {get; set;}
    public List<OrderUtilityMVN.OrderError> errors {get; private set;} { errors = new List<OrderUtilityMVN.OrderError>(); }
    public Id callSampleID {get; set;}
    public String actId {get; private set;}
    public List<Address_vod__c> addresses{get; set;}
    public Boolean deleteSuccess {get;set;}
    
    //Product Search variables
    public String searchTerm  { get; set; } { searchTerm = ''; }   //holds value for the search string on the page
    public String productType { get; set; }                        //holds value from the productType filter on the page

    private OrderUtilityMVN ou;
    private String caseId;
    private String callId;
    private Map<Id, WrappedProduct> products          = new Map<Id, WrappedProduct>();
    private Map<Id, Boolean>        validatedAccounts = new Map<Id,Boolean>();
    private User currentUser = [select Id, Country_Code_BI__c from User where Id =:UserInfo.getUserId()];    
    private Boolean orderCheckErrors = false; //used to tell submit whether or not to execute based on the check that was made when submitting.
    private Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();
    private List<Campaign_Target_vod__c> campaigns;
    // Product Search variables
    private final String productQueryFields = 'Id, Name, Product_Type_vod__c, Description_vod__c, ' + //used to configure the fields to be queried
                                              'Bundled_Product_MVN__c, Bundled_Product_MVN__r.Name, Max_Order_Limit_MVN__c';  
    private final Set<String> productTypes  = new Set<String>(); //used to configure the product types that can be searched for all
    private final Set<String> valProdTypes  = new Set<String>(); //used to configure the product types only validated accounts can search

    public Boolean isSAP { get{return OrderUtilityMVN.SAPOrder;}}
    public Boolean productsFound
    {
        get { return products.size() > 0; }
    } 

    public Boolean getHasOrderLines() {
        return orderLines.size() > 0;
    }

    public Boolean hasPageMessages {
        get { return ApexPages.getMessages().size() > 0; }
    }

    public Boolean hasAddress {
        get { return call != null && !String.isBlank(call.Ship_Address_Line_1_vod__c); }
    }

    public Boolean orderHasErrors {
        get {                                    
            if (orderCheckErrors)
            {
                for (OrderLine ol : orderLines.values()) {
                    if (String.isNotBlank(ol.error)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public List<SelectOption> productTypeList
    {
        get
        {
            if (productTypeList == null)
            {
                productTypeList = new List<SelectOption> { new SelectOption( '', System.Label.Person_Search_All_Record_Types ) };
                Map<String, Schema.PickListEntry> ples = new Map<String, Schema.PickListEntry>();
                for (Schema.PickListEntry ple : Product_vod__c.Product_Type_vod__c.getDescribe().getPicklistValues() )
                {                    
                    if (ple.isActive() 
                        && ( productTypes.contains(ple.getValue())
                            || (validatedHCP && valProdTypes.contains(ple.getValue()))
                           )
                        )
                    {
                        ples.put(ple.getLabel(), ple);
                    }
                }
                List<String> sortedPles = new List<String>();
                sortedPles.addAll(ples.keySet());
                sortedPles.sort();

                for (String ple : sortedPles) {
                    productTypeList.add( new SelectOption(ples.get(ple).getValue(), ples.get(ple).getLabel()) );
                }
            }            
            return productTypeList;
        }
        private set;
    }

    public List<SelectOption> orderLetterOptions
    {
        get
        {
            if (orderLetterOptions == null)
            {
                orderLetterOptions = new List<SelectOption>();
                Map<String, String> ples = new Map<String, String>();
                for (Schema.PickListEntry ple : Call2_vod__c.Order_Letter_MVN__c.getDescribe().getPicklistValues() )
                {                    
                    if (ple.isActive() )
                    {
                        ples.put(ple.getValue(),ple.getLabel());
                    }
                }
                List<String> sortedPles = new List<String>();
                sortedPles.addAll(ples.keySet());
                sortedPles.sort();

                for (String ple : sortedPles) {
                    orderLetterOptions.add( new SelectOption(ple, ples.get(ple)) );
                }

                // If this order is for a campaign, add the campaign letter to the list
                if(call.Id != null) {
                    for(Campaign_Target_vod__c cm : campaigns) {
                        if(!ples.keySet().contains(cm.Campaign_vod__r.Campaign_Letter_MVN__c)) {
                            orderLetterOptions.add( new SelectOption(cm.Campaign_vod__r.Campaign_Letter_MVN__c, cm.Campaign_vod__r.Campaign_Letter_MVN__c) );
                        }
                    }
                }
            }            
            return orderLetterOptions;
        }
        private set;
    } 

    public OrderPlacementControllerMVN() {
        ou = new OrderUtilityMVN();
        orderLines = new Map<Id,OrderLine>();
        addresses = new List<Address_vod__c>();
        hasCall = false;
        hasAccount = true;
        deleteSuccess = false;

        if ( settings.Order_Search_Product_Types_MVN__c != null) {
            productTypes.addAll(UtilitiesMVN.splitCommaSeparatedString(settings.Order_Search_Product_Types_MVN__c));
        }
        if (settings.Order_Restricted_Product_Types_MVN__c != null) {
            valProdTypes.addAll(UtilitiesMVN.splitCommaSeparatedString(settings.Order_Restricted_Product_Types_MVN__c));
        }

        setupOrder();
    }

    public void setupOrder() {
        callId = ApexPages.currentPage().getParameters().get('call');
        caseId = ApexPages.currentPage().getParameters().get('case');

        if ( String.isNotBlank(caseId) || String.isNotBlank(callId) )
        {            
            if(String.isNotBlank(caseId)) {
                refreshCase();
                callId = orderCase.Order_MVN__c;
                actId = orderCase.AccountId;
            }

            if(String.isNotBlank(callId)) 
            {
                if ( !refreshCall() ) {
                    return;
                }
                if(String.isBlank(actId)) {
                    actId = call.Account_vod__c;
                } else if (String.isBlank(call.Account_vod__c)) {
                    call.Account_vod__c = actId;
                }
                refreshSamples();
                hasCall = true;
            }

            if(String.isNotBlank(actId)) {
                refreshAccount();
                Map<Id,Boolean> validation = ou.validateAccounts(new List<Account>{orderAccount});
                validatedHCP = validation.get(actId);
            } else {
                validatedHCP = false;
            }

            if (String.isNotBlank(caseID) && String.isBlank(callId) && String.isNotBlank(actId)) {
                call = ou.newCallWithAddress(orderAccount,addresses);
                call.Account_vod__c = orderCase.AccountId;                        
                hasCall = true;
            }

            orderLocked = false;
            if(callId != null) {
                campaigns = [select Id,Campaign_vod__c,Campaign_vod__r.Campaign_Letter_MVN__c,Order_MVN__c from Campaign_Target_vod__c where Order_MVN__c = :callId];
            } else {
                campaigns = new List<Campaign_Target_vod__c>();
            }
                    
            if (!OrderUtilityMVN.userHasCreateEdit) {
                orderLocked = true;
            } else if(callId != null && campaigns.isEmpty()) {
                UserRecordAccess ura = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserID() AND RecordId = :callId];
                if(!ura.HasEditAccess) {
                    orderLocked = true;
                }
            }

            if(call.Status_vod__c == settings.Call_Submitted_Status_MVN__c) {
                orderLocked = true;
            }
        }

        if(String.isBlank(actId)) {
            hasAccount = false;
            orderLocked = true;
        }
    }

    public void refreshSamples () {
        orderLines = new Map<Id,OrderLine>();
        String sampleQuery = 'select ' + OrderUtilityMVN.callSampleQueryFields + ' from Call2_Sample_vod__c where Call2_vod__c = :callId';
        List<Call2_Sample_vod__c> callSamples = Database.query(sampleQuery);
        for(Call2_Sample_vod__c cs : callSamples) {
            cs.Account_vod__c = actId;
            orderLines.put(cs.Id,new OrderLine(cs));
        }   
    }

    public void refreshAccount () {
        String accountQuery = 'select ' + OrderUtilityMVN.accountQueryFields + ' from Account where Id = :actId';
        orderAccount = Database.query(accountQuery);

        String callShipAddress = '';
        if(call != null){
            callShipAddress = call.Ship_To_Address_vod__c;
        }
        
        String addressQuery = 'select ' + OrderUtilityMVN.addressQueryFields + ' from Address_vod__c where Account_vod__c = :actId OR Id = :callShipAddress Order By Name';
        addresses = Database.query(addressQuery);
    }

    public Boolean refreshCall () {
        String callQuery = 'select ' + OrderUtilityMVN.callQueryFields + ' from Call2_vod__c where Id = :callId';
        try 
        {
            call = Database.query(callQuery);
        } 
        catch (System.QueryException ex) 
        {
            System.debug( '\n\nError Mesage: ' + ex + '\n\n');
            ApexPages.addMessage( new ApexPages.message(ApexPages.severity.ERROR, Label.Order_Not_Visible));
            return false;
        }

        if (actId != null && actId != '') 
        {
            call.Account_vod__c = actId;
        }
        return true;
    }

    public void refreshCase () {
        orderCase = [select Id, AccountId, Account.Name, Order_MVN__c from Case where Id = :caseId];
    }

    public PageReference setQuantity() {                        
        orderCheckErrors = true;
        orderLines.get(callSampleID).error = null;    
        Call2_Sample_vod__c cs = orderLines.get(callSampleID).sample;
        String q = orderLines.get(callSampleID).quantity;
        
        if ( !validateQuantity(orderLines.get(callSampleID)) ) {            
            return null;
        }

        try {
            cs.Quantity_vod__c = Decimal.valueOf(orderLines.get(callSampleID).quantity);
            update cs;
            orderCheckErrors = false;            
        } catch ( System.TypeException ex) {  
            orderLines.get(callSampleId).error = ex.getMessage();            
        } catch ( System.DMLException ex)  {                        
            orderLines.get(callSampleId).error = ex.getDmlMessage(0);
        }

        return null;
    }

    private Boolean validateQuantity(OrderLine line) {        
        line.error = null;
        if (line.quantity == null || line.quantity == '') {            
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Quantity_cannot_be_null_for + ' ' + line.sample.Product_vod__r.Name);
            line.error = Label.Quantity_cannot_be_null_for;
            return false;
        }

        Boolean allZeros = false;
        if (line.quantity.containsOnly('0')) {
            allZeros = true;
        }

        Pattern nonInteger = Pattern.compile('^\\d+$');        
        if (!nonInteger.matcher(line.quantity).matches() || allZeros || Decimal.valueOf(line.quantity) > 999) {
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, productName + ': ' + Label.Quantity_must_be_a_positive_integer));
            line.error = Label.Quantity_must_be_a_positive_integer;
            return false;
        }

        try {
            Decimal d = Decimal.valueOf(line.quantity);
        } catch ( System.TypeException ex) {  
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
            line.error = ex.getMessage();
            return false;
        }

        return true;
    }

    public PageReference deleteCallSample() {
        Call2_Sample_vod__c cs = orderLines.remove(callSampleID).sample;
        delete cs;

        if(searchTerm != null && searchTerm.replace('*','').replace('?','').length() >= 2) {            
            searchForProducts();
        }
        
        return null;
    }

    public PageReference deleteCall() {
        if(call.Id != null) {
            try {
                delete call;
                deleteSuccess = true;
            } catch (exception e) {
                deleteSuccess = false;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
            }
        }
        
        return null;
    }

    public PageReference addProductsToCall(Map<Id,Integer> samplesToAdd) { 
        if(upsertCallBool()) {
            List<OrderUtilityMVN.OrderError> errors = ou.createSamples( new List<Call2_vod__c>{call}, samplesToAdd);
            processErrors(errors);
            refreshSamples();
        }
        return null;
    }

    public List<WrappedProduct> getProducts() 
    {
        
        List<WrappedProduct> sortProducts = products.values();
        System.debug('PRODUCTS PRE-SORT' + sortProducts);
        sortProducts.sort();
        System.debug('PRODUCTS POST-SORT' + sortProducts);
        return sortProducts;
    }

    public String getOrderForName() {
        if (orderCase != null && orderCase.Account != null) {
            return orderCase.Account.Name;
        } else if (call != null && call.Account_vod__c != null) {
            return Call.Account_vod__r.Name;
        }

        return '';
    }  

    public PageReference checkOrder() {        
        orderCheckErrors = true;
        errors = new List<OrderUtilityMVN.OrderError>();
        List<Call2_Sample_vod__c> samplesToSave = new List<Call2_Sample_vod__c>();
        Boolean callSampleErrors = false;

        for (OrderLine orderLine : orderLines.values()) {
            orderLine.error = '';
            Call2_Sample_vod__c cs = orderLine.sample;
            String q = orderLine.quantity;
            if (!validateQuantity(orderLine)) {
                callSampleErrors = true; //errors are displayed as an exclamation mark on row. No page message needed.                ;
            } else if (!validatedHCP && valProdTypes.contains(cs.Product_vod__r.Product_Type_vod__c)) {
                callSampleErrors = true;
                OrderUtilityMVN.OrderError oe = new OrderUtilityMVN.OrderError();
                oe.type = OrderUtilityMVN.ErrorType.SALESFORCE;
                oe.errorMessage = cs.Product_vod__r.Product_Type_vod__c + ': ' +cs.Product_vod__r.Name + ': ' + Label.Invalid_Product_for_account;
                errors.add(oe);
            } else {
                cs.Quantity_vod__c = Decimal.valueOf(q);
                samplesToSave.add(cs);
            }
        }

        if (callSampleErrors) {            
            return null;
        }

        try {
            update samplesToSave;
        } 
        catch( System.DMLException ex) 
        {        
            for (Integer i=0; i<ex.getNumDml(); i++) 
            {
                Boolean foundLine = false;
                for (OrderLine orderLine : orderLines.values()) 
                {
                    if (orderLine.sample.id == ex.getDmlId(i))
                    {
                        orderLine.error = ex.getDmlMessage(i);
                        foundLine = true;
                        continue;
                    }
                }

                if(!foundLine) {
                    OrderUtilityMVN.OrderError oe = new OrderUtilityMVN.OrderError();
                    oe.type = OrderUtilityMVN.ErrorType.SALESFORCE;
                    oe.errorMessage = ex.getDmlMessage(i);
                    errors.add(oe);
                }
            }
            return null;
        }

        errors.addAll(ou.saveFullOrders(new List<Call2_vod__c>{call}));
        if (!errors.isEmpty()) {            
            return null;
        }

        orderCheckErrors = false;
        return null;
    }

    public PageReference submitOrder() {
        if (!orderCheckErrors)
        {
            errors.addAll( ou.submitFullOrders(new List<Call2_vod__c>{call}) );
        }

        processErrors(errors);

        if( !refreshCall() ) {
            return null;
        }
        if(call.Status_vod__c == settings.Call_Submitted_Status_MVN__c) {
            orderLocked = true;
        }
        return null;
    }

    public void processErrors (List<OrderUtilityMVN.OrderError> allErrors) {
        for(OrderUtilityMVN.OrderError oe : allErrors) {            
            Boolean sampleError = false;
            for(OrderLine ol : orderLines.values()) {
                if ( (ol.sample.product_vod__r.SAP_Material_Number_MVN__c != null && ol.sample.product_vod__r.SAP_Material_Number_MVN__c == oe.errorSAPProduct) 
                     || ( ol.sample.Product_vod__c !=null && ol.sample.Product_vod__c == oe.errorProduct ))
                {
                    sampleError = true;
                    if (ol.error == null) ol.error = '';
                    ol.error += oe.errorMessage + '\n';
                }
            }

            if(!sampleError) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,oe.errorMessage));
            }
        }
    }

    public PageReference addSelectedProducts() 
    {
        Map<Id, Integer> productQuantityMap = new Map<Id, Integer>();
        for ( WrappedProduct product : products.values() )
        {
            if ( product.selected )
            {
                productQuantityMap.put(product.detail.id, 1);
                products.remove( product.detail.id );
            }
        }

        addProductsToCall( productQuantityMap );

        return null;
    }

    public PageReference removeSelectedProducts()
    {
        List<Call2_Sample_vod__c> removeSamples = new List<Call2_Sample_vod__c>();
        for (OrderLine ol : orderLines.values()) {
            if (ol.selected) {
                removeSamples.add( ol.sample );
            }
        }

        try {
            delete removeSamples;
        } catch (System.DMLException e) {
            
        }

        for (Call2_Sample_vod__c cs : removeSamples) {
            if (orderLines.containsKey(cs.id)) {
                orderLines.remove(cs.id);
            }
        }
        if(searchTerm != null && searchTerm.replace('*','').replace('?','').length() >= 2) {
            searchForProducts();
        }
        return null;
    }

    public PageReference upsertCall() {
        upsertCallBool();

        return null;
    }

    public Boolean upsertCallBool() {
        try {
            upsert call;
            if(orderCase != null && orderCase.Order_MVN__c == null) {
                orderCase.Order_MVN__c = call.Id;
                update orderCase;
            }
            
            callId = call.Id;
        } catch (Exception e) {
            ApexPages.addMessages(e);
            return false;
        }

        return true;
    }

    public PageReference searchForProducts()
    {        
        if (searchTerm == null || searchTerm.replaceAll('\\*','').replaceAll('\\?','').length() < 2)
        {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, Label.Search_Term_Must_Have_2_Characters));
            return null;
        }
        
        String searchString = 'FIND \'' + String.escapeSingleQuotes(searchTerm) + '\' IN ALL FIELDS RETURNING Product_vod__c ('+ productQueryFields;

        searchString += buildWhereClause();
        searchString += ' ORDER BY Name)';

        system.debug( '\n\n\nSearch String : '+ searchString);
        
        //Start with a fresh list
        products.clear();
        System.debug('SEARCH RESULTS' + search.query( searchString ));
        for( Product_vod__c product : (List<Product_vod__c>) search.query( searchString )[0] ){            
            Boolean alreadySelected = false;
            System.debug('FOUND PRODUCT: ' + product);
            for ( Id orderLineId : orderLines.keySet() ) {
                if ( orderLines.get(orderLineId).sample.Product_vod__c != null &&
                     orderLines.get(orderLineId).sample.Product_vod__c == product.id )
                {
                    System.debug('ORDER LINE: ' + orderLines.get(orderLineId).sample.Product_vod__c);
                    alreadySelected = true;
                    break;
                }
            }
            if ( !alreadySelected ) {
                products.put( product.id, new WrappedProduct( product, false ));    
            }
        }

        System.debug('PRODUCTS: ' + products);
        return null;
    }

    private String buildWhereClause() 
    {
        String whereClause = ' WHERE ';

        // Product Type is limited to Specific types to start. Further limit if the product type filter is applied
        if (productType != null && productType != '')
        {
            whereClause += 'Product_Type_vod__c=\'' + productType + '\'';            
        } 
        else if ( productTypes.size() > 0 )
        {
            whereClause += '(';
            for ( String prodtype : productTypes )
            {
                whereClause += 'Product_Type_vod__c=\'' + prodType + '\' OR ';
            }
            if ( validatedHCP )
            {
                for ( String prodType : valProdTypes )
                {
                    whereClause += 'Product_Type_vod__c=\'' + prodType + '\' OR ';
                }
            }
            whereClause = whereClause.substringBeforeLast(' OR ');            
            whereClause += ')';
        }

        if (OrderUtilityMVN.SAPOrder) {
            whereClause += ' AND SAP_Material_Number_MVN__c != null';
        }

        whereClause += ' AND (End_Date_MVN__c>=TODAY OR End_Date_MVN__c=null)';
        whereClause += ' AND Country_Code_BI__c=\'' + currentUser.Country_Code_BI__c + '\'';
        whereClause += ' AND Orderable_MVN__c=true';

        return whereClause;
    }

    public class OrderLine {
        public Call2_Sample_vod__c sample {get;set;}
        public String error {get;set;}
        public Boolean selected { get; set; }
        public String quantity { get; set; }

        public orderLine (Call2_Sample_vod__c cs) {
            this.selected = false;
            this.sample = cs;
            this.quantity = String.valueOf(cs.Quantity_vod__c);
        }
    }

    public class WrappedProduct implements Comparable
    {
        public Product_vod__c detail   { get; set; }
        public Boolean        selected { get; set; }

        public WrappedProduct( Product_vod__c product, Boolean selected)
        {
            this.detail   = product;
            this.selected = selected;
        }

        public Integer compareTo(Object compareTo) {
            OrderPlacementControllerMVN.WrappedProduct compareToProduct = (OrderPlacementControllerMVN.WrappedProduct) compareTo;
            return detail.Name.compareTo(compareToProduct.detail.Name);
        }
    }

}