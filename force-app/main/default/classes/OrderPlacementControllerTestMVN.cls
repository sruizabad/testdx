/*
  * OrderPlacementControllerTestMVN
  *    Created By:     Kai Chen   
  *    Created Date:   September 19, 2013
  *    Description:     Unit tests for OrderPlacementControllerMVN
 */
@isTest
private class OrderPlacementControllerTestMVN {

	static OrderPlacementControllerMVN controller;
	static User callCenterUser;
	static User adminUser;
	static Product_vod__c testProduct;
	static Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();

	static { 
		Profile p = [select Id from Profile where Name='CRC - Customer Request Agent']; 
		UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Germany'];
		callCenterUser = new User(Alias = 'heiditst', Email='heidibergen@oehringertest.com', 
		                EmailEncodingKey='UTF-8', FirstName = 'Heidi', LastName='Bergen', LanguageLocaleKey='de', 
		                LocaleSidKey='de', ProfileId = p.Id, UserRoleId = ur.Id, 
		                TimeZoneSidKey='Europe/Berlin', UserName='heidibergen@oehringertest.com',
		                Country_Code_BI__c = 'DE', External_ID_BI__c='12345678');

		adminUser = new User(alias='ccusysad', email= 'callcentertestusermvn@callcenter.com', External_ID_BI__c='123456789', Country_Code_BI__c = 'DE',
		              emailencodingkey='UTF-8', firstName='Reginald', lastname='Wellington', languagelocalekey='en_US', 
		              localesidkey='en_US', profileid = [select Id from Profile where Name = 'System Administrator'].Id, 
		              isActive = true, timezonesidkey='America/Los_Angeles', username='callcentertestusermvn@callcenter.com',
		              Default_Article_Language_MVN__c = KnowledgeArticleVersion.Language.getDescribe().getPicklistValues()[0].getLabel());

		System.runAs(adminUser){
			insert callCenterUser;

			PermissionSet permissions = [select Id from PermissionSet where Name = 'CRC_Order_Manager_MVN'];

			PermissionSetAssignment assignment = new PermissionSetAssignment(AssigneeID = callCenterUser.Id, PermissionSetId = permissions.Id);

			insert assignment;

			TestDataFactoryMVN.createSettings();

			
		}

		System.runAs(callCenterUser){
			testProduct = TestDataFactoryMVN.createTestProduct(callCenterUser.Country_Code_BI__c);

			Test.setFixedSearchResults(new List<Id> {testProduct.Id});
			
		}

		OrderUtilityMVN.userCountryCode = callCenterUser.Country_Code_BI__c;
		
	}

	@isTest static void noAccountNoCaseNoCall(){
		Test.setCurrentPage(new PageReference('/apex/OrderPlacement'));
		controller = new OrderPlacementControllerMVN();

		System.assertEquals(false, controller.hasCall);
		System.assertEquals(false, controller.hasPageMessages);
	}
	
	// Page is accesed with a call passed in.  Search for a product, then add a quantity of the product to the call
	@isTest static void noAccountWithCallNoCase() {
		System.runAs(callCenterUser){
			testProduct = TestDataFactoryMVN.createTestProduct(callCenterUser.Country_Code_BI__c);
			Account consumer = TestDataFactoryMVN.createTestConsumer();
			Call2_vod__c call = TestDataFactoryMVN.createSavedCall();

			call.Account_vod__c = consumer.Id;

			update call;

			System.debug('TEST PRODUCT: ' + testProduct);

			Test.setCurrentPage(new PageReference('apex/OrderPlacement?call=' + call.Id));

			controller = new OrderPlacementControllerMVN();

			System.assertEquals(consumer.Id, controller.actId);
			System.assertEquals(true, controller.hasCall);
			System.assertEquals(false, controller.getHasOrderLines());
			System.assertEquals(false, controller.hasAddress);
			System.assertEquals(false, controller.productTypeLIst.isEmpty());

			controller.searchTerm = 'p';

			controller.searchForProducts();

			System.assertEquals(true, ApexPages.hasMessages());
			System.assert(ApexPages.getMessages()[0].getSummary().contains(Label.Search_Term_Must_Have_2_Characters));

			controller.searchTerm = 'Pra*';
			controller.productType = 'Promotional';

			controller.searchForProducts();

			System.assertEquals(true, controller.productsFound);

			System.assertEquals(1, controller.getProducts().size());

			controller.getProducts()[0].selected = true;

			controller.addSelectedProducts();

			System.assertEquals(1, controller.orderLines.size());

			Call2_Sample_vod__c sample = [select Id from Call2_Sample_vod__c where Call2_vod__c = :call.Id];

			controller.orderLines.get(sample.Id).quantity = null;

			controller.callSampleID = sample.Id;

			controller.setQuantity();

			controller.orderLines.get(sample.Id).quantity = String.valueOf(-2);

			controller.setQuantity();

			controller.orderLines.get(sample.Id).quantity = String.valueOf(2);

			controller.setQuantity();

			sample = [select Id, Quantity_vod__c from Call2_Sample_vod__c where Call2_vod__c = :call.Id];

			System.assertEquals(2, sample.Quantity_vod__c);

			consumer = [select Id, Name from Account where Id = :consumer.Id];

			System.assertEquals(consumer.Name, controller.getOrderForName());

			controller.orderLines.get(sample.Id).selected = true;

			controller.removeSelectedProducts();

			System.assertEquals(false, controller.getHasOrderLines());

			controller.deleteCall();

		}
	}

	@isTest static void noAccountWithNewCallWithCase() {
		System.runAs(callCenterUser){
			testProduct.Product_Type_vod__c = 'Sample';
			update testProduct;

			Account hcp = TestDataFactoryMVN.createTestHCP();
			hcp.OK_Status_Code_BI__c = 'Valid';
			update hcp;
			
			Case caseRecord = TestDataFactoryMVN.createTestCase(hcp.Id);

			Test.setCurrentPage(new PageReference('apex/OrderPlacement?case=' + caseRecord.Id));

			controller = new OrderPlacementControllerMVN();

			System.assertEquals(hcp.Id, controller.actId);
			System.assertEquals(true, controller.hasCall);
			System.assertEquals(false, controller.getHasOrderLines());
			System.assertEquals(false, controller.hasAddress);
			System.assertEquals(false, controller.orderLocked);

			controller.searchTerm = 'Pra*';
			
			controller.searchForProducts();

			System.assertEquals(1, controller.getProducts().size());

			controller.getProducts()[0].selected = true;

			controller.addSelectedProducts();

			System.assertEquals(1, controller.orderLines.size());

			caseRecord = [select Id, Order_MVN__c from Case where Id = :caseRecord.Id];

			System.assertEquals(controller.call.Id, caseRecord.Order_MVN__c);

			controller.call.Order_Letter_MVN__c = '01';

			//Search again
			controller.searchTerm = 'Pra*';
			
			controller.searchForProducts();

			System.assertEquals(0, controller.getProducts().size());

			controller.checkOrder();
			controller.submitOrder();

			
		}
	}

	@isTest static void failCheckOrder() {
		System.runAs(callCenterUser){
			testProduct.Product_Type_vod__c = 'Sample';
			testProduct.Max_Order_Limit_MVN__c = 2;
			update testProduct;

			Account hcp = TestDataFactoryMVN.createTestHCP();
			hcp.OK_Status_Code_BI__c = 'Valid';
			update hcp;
			
			Case caseRecord = TestDataFactoryMVN.createTestCase(hcp.Id);

			Test.setCurrentPage(new PageReference('apex/OrderPlacement?case=' + caseRecord.Id));

			controller = new OrderPlacementControllerMVN();

			controller.searchTerm = 'Pra*';
			
			controller.searchForProducts();

			controller.getProducts()[0].selected = true;

			controller.addSelectedProducts();

			Call2_Sample_vod__c sample = [select Id from Call2_Sample_vod__c where Call2_vod__c = :controller.call.Id];
			controller.orderLines.get(sample.Id).quantity = String.valueOf(3);

			caseRecord = [select Id, Order_MVN__c from Case where Id = :caseRecord.Id];

			controller.call.Order_Letter_MVN__c = '01';

			controller.checkOrder();
		}
	}

	@isTest static void invalidHCPCheckOrder() {
		System.runAs(callCenterUser){
			testProduct.Product_Type_vod__c = 'Sample';
			testProduct.Max_Order_Limit_MVN__c = 2;
			update testProduct;

			Account hcp = TestDataFactoryMVN.createTestHCP();
			hcp.OK_Status_Code_BI__c = 'Valid';
			update hcp;
			
			Case caseRecord = TestDataFactoryMVN.createTestCase(hcp.Id);

			Test.setCurrentPage(new PageReference('apex/OrderPlacement?case=' + caseRecord.Id));

			controller = new OrderPlacementControllerMVN();

			controller.searchTerm = 'Pra*';
			controller.searchForProducts();

			controller.getProducts()[0].selected = true;

			controller.addSelectedProducts();

			Call2_Sample_vod__c sample = [select Id from Call2_Sample_vod__c where Call2_vod__c = :controller.call.Id];
			controller.orderLines.get(sample.Id).quantity = String.valueOf(1);

			caseRecord = [select Id, Order_MVN__c from Case where Id = :caseRecord.Id];

			controller.call.Order_Letter_MVN__c = '01';

			hcp.OK_Status_Code_BI__c = 'Invalid';
			update hcp;

			controller.setupOrder();

			controller.checkOrder();
		}
	}

	@isTest static void accountWithCallWithCase() {
		System.runAs(callCenterUser){
			Account consumer = TestDataFactoryMVN.createTestConsumer();
			consumer = [select Id, Name, FirstName, LastName from Account where Id=:consumer.Id];

			Call2_vod__c call = TestDataFactoryMVN.createSavedCall();

			Case caseRecord = TestDataFactoryMVN.createTestCase(consumer.Id);

			Test.setCurrentPage(new PageReference('apex/OrderPlacement?case=' + caseRecord.Id + '&call=' + call.Id));

			controller = new OrderPlacementControllerMVN();

			List<SelectOption> options = controller.orderLetterOptions;
			
			System.assertEquals(consumer.Id, controller.actId);
			System.assertEquals(true, controller.hasCall);
			System.assertEquals(false, controller.getHasOrderLines());
			System.assertEquals(false, controller.hasAddress);
			System.assertEquals(consumer.Name, controller.getOrderForName());

			controller.searchTerm = 'Pra*';
			controller.productType = 'Promotional';

			controller.searchForProducts();

			System.assertEquals(1, controller.getProducts().size());

			controller.getProducts()[0].selected = true;

			controller.addSelectedProducts();

			System.assertEquals(1, controller.orderLines.size());

			Call2_Sample_vod__c sample = [select Id from Call2_Sample_vod__c where Call2_vod__c = :controller.call.Id];

			controller.callSampleID = sample.Id;

			controller.deleteCallSample();

		}
	}

	@isTest static void accountWithCallNoCase() {
		System.runAs(callCenterUser){
			Account consumer = TestDataFactoryMVN.createTestConsumer();

			Call2_vod__c call = TestDataFactoryMVN.createSavedCall();

			call.Account_vod__c = consumer.Id;

			update call;

			Test.setCurrentPage(new PageReference('apex/OrderPlacement?call=' + call.Id));

			controller = new OrderPlacementControllerMVN();
			
		}
	}

	@isTest static void callNoAccountNoCase() {
		System.runAs(callCenterUser){
			Call2_vod__c call = TestDataFactoryMVN.createSavedCall();
			Test.setCurrentPage(new PageReference('apex/OrderPlacement?call=' + call.Id));

			controller = new OrderPlacementControllerMVN();
			
			System.assertEquals(false, controller.validatedHCP);
			
		}
	}

	@isTest static void submittedCallNoAccountNoCase() {
		System.runAs(callCenterUser){
			Call2_vod__c call = TestDataFactoryMVN.createSavedCall();
			call.Status_vod__c = 'Submitted_vod';

			update call;

			Test.setCurrentPage(new PageReference('apex/OrderPlacement?call=' + call.Id));

			controller = new OrderPlacementControllerMVN();
			
			System.assertEquals(true, controller.orderLocked);


			
		}
	}

	@isTest static void testExceptions() {
		System.runAs(callCenterUser){
			Account consumer = TestDataFactoryMVN.createTestHCP();
			consumer = [select Id, FirstName, LastName from Account where Id=:consumer.Id];

			testProduct.Max_Order_Limit_MVN__c = 2;

			update testProduct;

			OrderPlacementControllerMVN.WrappedProduct wrappedProduct = new OrderPlacementControllerMVN.WrappedProduct(testProduct, false);

			wrappedProduct.compareTo(wrappedProduct);

			Call2_vod__c call = TestDataFactoryMVN.createSavedCall();

			Case caseRecord = TestDataFactoryMVN.createTestCase(consumer.Id);

			Test.setCurrentPage(new PageReference('apex/OrderPlacement?case=' + caseRecord.Id + '&call=' + call.Id));

			controller = new OrderPlacementControllerMVN();
			
			System.assertEquals(consumer.Id, controller.actId);
			System.assertEquals(true, controller.hasCall);
			System.assertEquals(false, controller.getHasOrderLines());
			System.assertEquals(false, controller.hasAddress);
			System.assertEquals(false, controller.orderHasErrors);
			System.assertEquals(consumer.FirstName + ' ' + consumer.LastName, controller.getOrderForName());

			controller.searchTerm = 'Pra*';
			controller.productType = 'Promotional';

			controller.searchForProducts();

			System.assertEquals(1, controller.getProducts().size());

			controller.getProducts()[0].selected = true;
			
			controller.addSelectedProducts();

			System.assertEquals(1, controller.orderLines.size());

			Call2_Sample_vod__c sample = [select Id from Call2_Sample_vod__c where Call2_vod__c = :controller.call.Id];

			controller.orderLines.get(sample.Id).quantity = String.valueOf('asdf');

			try{
				controller.checkOrder();
			} catch(Exception e){
				System.assertEquals(true, controller.orderHasErrors);
			}

			try{
				controller.setQuantity();
			} catch(Exception e){}

			controller.orderLines.get(sample.Id).quantity = String.valueOf(-1);

			try{
				controller.setQuantity();
			} catch(Exception e){}

			try{
				controller.checkOrder();
			} catch(Exception e){}

			controller.orderLines.get(sample.Id).quantity = String.valueOf(1);

			consumer.OK_Status_Code_BI__c = 'Invalid';
			update consumer;
			controller.setupOrder();

			try{
				controller.setQuantity();
			} catch(Exception e){}

			try{
				controller.checkOrder();
			} catch(Exception e){}

			controller.orderLines.get(sample.Id).quantity = String.valueOf(3);

			consumer.OK_Status_Code_BI__c = 'Valid';
			update consumer;
			controller.setupOrder();

			try{
				controller.checkOrder();
			} catch(Exception e){}

			controller.callSampleID = sample.Id;

			Test.setReadOnlyApplicationMode(true);
			
			try{
				controller.setQuantity();
			} catch(Exception e){}
			
			try{
				controller.checkOrder();
				controller.submitOrder();
			} catch(Exception e) {}

			controller.call.Order_Letter_MVN__c = '01';

			try{
				controller.checkOrder();
				controller.submitOrder();
			} catch(Exception e) {}

			try{
				controller.removeSelectedProducts();
			} catch(Exception e) {}

			try{
				controller.deleteCall();
			} catch(Exception e) {}

			
		}
	}
	
}