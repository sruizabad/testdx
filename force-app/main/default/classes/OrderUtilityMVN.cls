/*
  * OrderUtilityMVN
  *    Created By:     Kai Amundsen   
  *    Created Date:    August 12, 2013
  *    Description:     Utility class to help submit orders and handle the integration to SAP
 */

public class OrderUtilityMVN {
	public enum ErrorType {INVENTORY,LIMITS,SALESFORCE,SAP,ADMIN}
	public Id SAPBatchJobID;
	public Id campaignId;
	public Map<Id,Boolean> validatedAccounts = new Map<Id,Boolean>();

	public static String callQueryFields = 'Id, Name, No_Disbursement_vod__c, SAP_Errors_MVN__c, Web_Order_Number_MVN__c, toLabel(Delivery_Status_MVN__c),External_ID__c,' +
	                                       'Ship_To_Address_vod__c, Account_vod__r.Name, Account_vod__r.LastName, Ship_To_Address_Text_vod__c,' +
	                                       'Ship_to_Name_MVN__c, Ship_Address_Line_1_vod__c, Ship_Address_Line_2_vod__c, Ship_City_vod__c, Ship_State_vod__c,' +
	                                       'Ship_Country_vod__c, Ship_Zip_vod__c, Account_vod__c, Call_Type_vod__c, Status_vod__c, Call_Date_vod__c,' +
	                                       'Call_DateTime_vod__c, Order_Letter_MVN__c, Country_Code_BI__c, Ship_Address_Line_3_MVN__c, Ship_Address_Line_4_MVN__c,Ship_To_Salutation_MVN__c';
	public static String accountQueryFields = 'Id,Name,FirstName,LastName,OK_External_ID_BI__c,OK_Status_Code_BI__c,IsPersonAccount,PersonTitle,OK_Stdopening_BI__c';
	public static String addressQueryFields = 'Id,Name,Address_Line_2_vod__c,City_vod__c,State_vod__c,Zip_vod__c,Country_Code_BI__c,Account_vod__c,Primary_vod__c,Formatted_Address_BI__c,OK_State_Province_BI__r.Name';
	public static String callSampleQueryFields = 'Id, Call2_vod__c, Delivery_Status_vod__c, Quantity_vod__c, Product_vod__c, Product_vod__r.External_ID_vod__c, Product_vod__r.Name, External_ID__c,' +
	                                             'Product_vod__r.Product_Type_vod__c, Product_vod__r.Description_vod__c, Product_vod__r.SAP_Material_Number_MVN__c,' +
	                                       	     'Product_vod__r.Bundled_Product_MVN__c, Product_vod__r.Bundled_Product_MVN__r.Name, Product_vod__r.Max_Order_Limit_MVN__c, Line_Number_MVN__c,Account_vod__c';
	public static Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();
	public static String userCountryCode;
	public static Id callRecordTypeId;
	public static Boolean setRecTypeIdOnCalls = false;
	public static Boolean userHasCreateEdit;
	public final Integer SAPCHARLIMIT = 35;
	public static Boolean SAPOrder;

	static {

		Id uId = UserInfo.getUserId();
		User u = [select Id,Country_Code_BI__c from User where Id = :uId];
		userCountryCode = u.Country_Code_BI__c;

		String recordTypeDeveloperName = 'Sample_Only';
		if(String.isNotBlank(settings.Call_Order_Record_Type_MVN__c)) {
			recordTypeDeveloperName = settings.Call_Order_Record_Type_MVN__c;
		}
		List<RecordType> rts = [select Id,DeveloperName from RecordType where DeveloperName = :recordTypeDeveloperName and SObjectType='Call2_vod__c'];
		//Specify a record type if it is active and available to the user		
		if (!rts.isEmpty() && rts[0] != null) {
			if (Schema.SObjectType.Call2_vod__c.getRecordTypeInfosByID().get(rts[0].id).isAvailable()) {
				callRecordTypeId= rts[0].Id;
				setRecTypeIdOnCalls = true;
			}
		}

		if(Schema.sObjectType.Call2_vod__c.isCreateable() && Schema.sObjectType.Call2_vod__c.isUpdateable()) {
			userHasCreateEdit = true;
		} else {
			userHasCreateEdit = false;
		}

		SAPOrder = isSAPOrder();
	}

	public OrderUtilityMVN() {
		
	} 

	public static Boolean isSAPOrder () {
		Boolean isSAP = false;

		if(!String.isBlank(settings.SAP_Order_Country_Codes_MVN__c)) {
			for(String cc : UtilitiesMVN.splitCommaSeparatedString(settings.SAP_Order_Country_Codes_MVN__c)) {
				if(userCountryCode == cc) {
					isSAP = true;
					break;
				}
			}
		}

		return isSAP;
	}

	

	public Map<Id,Boolean> validateAccounts(List<Account> accounts) {
		validatedAccounts = new Map<Id,Boolean>();

		Set<String> validStatusCodes;
		if(String.isBlank(settings.HCP_Valid_to_Sample_Status_Codes_MVN__c)) {
			validStatusCodes = new Set<String>{'Valid'};
		} else {
			validStatusCodes = new Set<String>(UtilitiesMVN.splitCommaSeparatedString(settings.HCP_Valid_to_Sample_Status_Codes_MVN__c));
		}

		for( Account act : accounts ) 
		{	
			if(act.IsPersonAccount && validStatusCodes.contains(act.OK_Status_Code_BI__c)) {
				validatedAccounts.put(act.Id, true);
			} else {
				validatedAccounts.put(act.Id, false);
			}
		}

		return validatedAccounts;
	}

	public static Call2_vod__c newCall() {
		Call2_vod__c call = new Call2_vod__c();
		call.Status_vod__c = settings.Call_Saved_Status_MVN__c;
		if(String.isBlank(settings.Call_Order_Call_Type_MVN__c)) {
			call.Call_Type_vod__c = 'Sample Only';
		} else {
			call.Call_Type_vod__c = settings.Call_Order_Call_Type_MVN__c;
		}
		call.Call_Date_vod__c = Date.today();
		call.Call_DateTime_vod__c = System.now();
		call.Country_Code_BI__c = userCountryCode;
		call.No_Disbursement_vod__c = true;
		call.Delivery_Status_MVN__c = settings.Call_Order_Delivery_Status_New_MVN__c;
		if(String.isBlank(call.Delivery_Status_MVN__c)) {
			call.Delivery_Status_MVN__c = 'New';
		}

		if (setRecTypeIdOnCalls) {
			call.RecordTypeId = callRecordTypeId;
		}

		return call;
	}

	public Call2_vod__c newCallWithAddress (Account account, List<Address_vod__c> addresses) {
		Call2_vod__c call = newCall();
		call.Account_vod__c = account.Id;
		for(Address_vod__c addr : addresses) {
			if (addr.Primary_vod__c) {
				if(!String.isBlank(account.OK_Stdopening_BI__c) && !String.isBlank(account.LastName)){
					call.Ship_To_Salutation_MVN__c = account.OK_Stdopening_BI__c + ' ' + account.LastName + ',';
				} else{
					call.Ship_To_Salutation_MVN__c = System.Label.Letter_Salutation_Value;
				}
				if(String.isBlank(account.PersonTitle)) {
					call.Ship_to_Name_MVN__c = account.Name;
				} else {
					call.Ship_to_Name_MVN__c = account.PersonTitle + ' ' + account.Name;
				}
				call.Ship_To_Address_vod__c = addr.Id;
				call.Ship_Address_Line_1_vod__c = addr.Name;
				call.Ship_Address_Line_2_vod__c = addr.Address_line_2_vod__c;
				call.Ship_Zip_vod__c = addr.Zip_vod__c;
				call.Ship_State_vod__c = addr.State_vod__c;
				call.Ship_City_vod__c = addr.City_vod__c;
				call.Ship_Country_vod__c = addr.Country_Code_BI__c;
			}
		}

		if(!isCallAddressValid(call)) {
			call = newCall();
			call.Account_vod__c = account.Id;
		}

		return call;
	}

	public Boolean isCallAddressValid(Call2_vod__c call) {
		Boolean isValid = true;

		if(String.isBlank(call.Ship_Address_Line_1_vod__c) || String.isBlank(call.Ship_to_Name_MVN__c) 
				 || String.isBlank(call.Ship_City_vod__c) || String.isBlank(call.Ship_Zip_vod__c)) {
			isValid = false;
		}

		if(SAPOrder) {
			if(String.isBlank(call.Ship_To_Salutation_MVN__c) || String.isBlank(call.Ship_Country_vod__c)) {
				isValid = false;
			}

			List<String> sapFields = new List<String>{'Ship_To_Salutation_MVN__c','Ship_to_Name_MVN__c','Ship_Address_Line_1_vod__c','Ship_Address_Line_2_vod__c','Ship_Address_Line_3_MVN__c','Ship_Address_Line_4_MVN__c','Ship_City_vod__c'};
			for(String fieldName : sapFields) {
				String fieldValue = (String) call.get(fieldName);
				if(fieldValue != null && fieldValue.length() > SAPCHARLIMIT) {
					isValid = false;
				}
			}
		}


		return isValid;
	}

	public List<OrderError> createSamples( List<Call2_vod__c> calls, Map<Id, Integer> samples) {
		// Collect bundled items
		Set<Id> sampleIds = samples.keySet();
		Map<Id,Boolean> samplesRequireValidation = new Map<Id,Boolean>();
		Set<String> valProdTypes = new Set<String>();
		
		if (settings.Order_Restricted_Product_Types_MVN__c != null) 
		{
            valProdTypes.addAll(UtilitiesMVN.splitCommaSeparatedString(settings.Order_Restricted_Product_Types_MVN__c));
        }

		List<Product_vod__c> newProducts = [SELECT Id,Product_Type_vod__c,End_Date_MVN__c,Country_Code_BI__c,Orderable_MVN__c,SAP_Material_Number_MVN__c,
												   Bundled_Product_MVN__c,Bundled_Product_MVN__r.Product_Type_vod__c,Bundled_Product_MVN__r.End_Date_MVN__c,
												   Bundled_Product_MVN__r.Country_Code_BI__c,Bundled_Product_MVN__r.Orderable_MVN__c,Bundled_Product_MVN__r.SAP_Material_Number_MVN__c
		 									FROM Product_vod__c 
		 									WHERE Id IN :sampleIds];
		for (Product_vod__c p : newProducts) 
		{
			if((p.End_Date_MVN__c >= Date.today() || p.End_Date_MVN__c == null) && p.Orderable_MVN__c && p.Country_Code_BI__c == userCountryCode && (!SAPOrder || p.SAP_Material_Number_MVN__c != null)) 
			{	
				samplesRequireValidation.put(p.Id,valProdTypes.contains(p.Product_Type_vod__c));
				if(p.Bundled_Product_MVN__c!=null && !samples.containsKey(p.Bundled_Product_MVN__c)) 
				{
					if((p.Bundled_Product_MVN__r.End_Date_MVN__c >= Date.today() || p.Bundled_Product_MVN__r.End_Date_MVN__c == null)&& p.Bundled_Product_MVN__r.Orderable_MVN__c && p.Bundled_Product_MVN__r.Country_Code_BI__c == userCountryCode && (!SAPOrder || p.Bundled_Product_MVN__r.SAP_Material_Number_MVN__c != null)) 
					{
						samples.put(p.Bundled_Product_MVN__c,1);
						samplesRequireValidation.put(p.Bundled_Product_MVN__c,valProdTypes.contains(p.Bundled_Product_MVN__r.Product_Type_vod__c));
					}
				}	
			} else {
				samples.remove(p.Id);
			}
		}


		// Collect existing samples to prevent duplicates
		List<Call2_Sample_vod__c> newCallSamples = new List<Call2_Sample_vod__c>();
		Set<String> uniqueCallSampleKeys = new Set<String>();
		List<OrderError> errors = new List<OrderError>();
		
		for(Call2_Sample_vod__c cs : [SELECT Id, Call2_vod__c, Product_vod__c FROM Call2_Sample_vod__c WHERE Call2_vod__c IN :calls]) 
		{
			String uniqueKey = cs.Call2_vod__c + '_' + cs.Product_vod__c;
			uniqueCallSampleKeys.add(uniqueKey);
		}

		for(Call2_vod__c theCall : calls) 
		{
			for (Id product : samples.keySet()) 
			{
				if(samplesRequireValidation.containsKey(product) && (!samplesRequireValidation.get(product) || (samplesRequireValidation.get(product) && validatedAccounts.get(theCall.Account_vod__c)))) //Check eligability
				{
					if(uniqueCallSampleKeys.add(theCall.Id+'_'+product)) // check uniqueness
					{
						Call2_Sample_vod__c newSample = 
						    	new Call2_Sample_vod__c(Call2_vod__c            = theCall.Id
														,Account_vod__c         = theCall.Account_vod__c
														,Product_vod__c 	    = product
														,Quantity_vod__c        = samples.get(product)
														,Apply_Limit_vod__c     = false
														,Limit_Applied_vod__c   = false
														,Call_Date_vod__c       = theCall.Call_Date_vod__c
														,Delivery_Status_vod__c = settings.Call_Order_Delivery_Status_New_MVN__c
								);
						
						if(String.isBlank(newSample.Delivery_Status_vod__c)) 
						{
							newSample.Delivery_Status_vod__c = 'New';
						}
						newSample.Override_Lock_vod__c = true;
						newCallSamples.add(newSample);
					}
				}	
			}
		}

		try {
			insert newCallSamples;	
		} catch (Exception e) {
			OrderError oe = new OrderError();
			oe.errorException = e;
			oe.type = ErrorType.SALESFORCE;
			oe.errorMessage = e.getMessage();
			errors.add(oe);
		}

		return errors;
	}

	@TestVisible
	private List<OrderError> submitCallSamples(List<Call2_Sample_vod__c> callSamples) {
		List<OrderError> errors = new List<OrderError>();
		Set<Id> callIds = new Set<Id>();

		for (Call2_Sample_vod__c cs : callSamples) 
		{
			if (cs.Account_vod__c != null) 
			{
				cs.Apply_Limit_vod__c = true;
				cs.Limit_Applied_vod__c = false;
				callIds.add(cs.Call2_vod__c);
			}
			cs.Delivery_Status_vod__c = settings.Call_Order_Delivery_Status_Ordered_MVN__c;
			
			if(String.isBlank(cs.Delivery_Status_vod__c)) 
			{
				cs.Delivery_Status_vod__c = 'Submitted';
			}
		}

		try{
			update callSamples;
		} catch (Exception e){
			OrderError oe = new OrderError();
			oe.errorException = e;
			oe.type = ErrorType.SALESFORCE;
			oe.errorMessage = e.getMessage();
			errors.add(oe);
		}

		List<Sample_Limit_Transaction_vod__c> slts = [SELECT Id, Sample_Limit_vod__c, Remaining_Quantity_vod__c, Quantity_To_Disperse_vod__c, 
															Product_vod__c, Product_vod__r.Name,Call2_vod__c
													  FROM Sample_Limit_Transaction_vod__c 
													  WHERE Call2_vod__c in :callIds];

		for(Sample_Limit_Transaction_vod__c slt : slts) {
			if(slt.Remaining_Quantity_vod__c < slt.Quantity_To_Disperse_vod__c) {
				OrderError oe = new OrderError();
				oe.errorOrder = slt.Call2_vod__c;
				oe.errorProduct = slt.Product_vod__c;
				oe.type = ErrorType.LIMITS;
				oe.errorMessage = String.format(System.Label.Sample_Limit_Exceeded_MVN, new string[] {slt.Remaining_Quantity_vod__c.format(),slt.Product_vod__r.Name});
				errors.add(oe);
			}
		}

		return errors;
	}

	public List<OrderError> checkSampleLimits(List<Call2_vod__c> orders) {
		List<Call2_Sample_vod__c> css = Database.Query('select ' + callSampleQueryFields + ' from Call2_Sample_vod__c where Call2_vod__c in :orders');

		Savepoint sp1 = Database.setSavepoint();
		List<OrderError> errors = submitCallSamples(css);
		
		Database.rollback(sp1);

		return errors;
	}

	public List<OrderError> saveFullOrders(List<Call2_vod__c> orders) {
		String callQuery = 'select ' + callQueryFields + ', (select ' + callSampleQueryFields + ' from Call2_Sample_vod__r) from Call2_vod__c where Id in :orders';
		List<OrderError> errors = new List<OrderError>();
		Map<Id,Call2_vod__c> callMap = new Map<Id,Call2_vod__c>((List<Call2_vod__c>)Database.query(callQuery));

		//Do a call pre-check to make sure all rules are valid
		List<Call2_Sample_vod__c> allCallSamples = getCallSamples(callMap.values());
		
		for(Call2_vod__c eachCall : (List<Call2_vod__c>)callMap.values()) 
		{
			System.debug('!!! ' + eachCall);
			if(String.isBlank(eachCall.Ship_Address_Line_1_vod__c) || String.isBlank(eachCall.Ship_to_Name_MVN__c) 
				 || String.isBlank(eachCall.Ship_City_vod__c) || String.isBlank(eachCall.Ship_Zip_vod__c)) 
			{
				OrderError oe = new OrderError();
				oe.errorOrder = eachCall.Id;
				oe.type = ErrorType.SAP;
				oe.errorMessage = System.Label.Order_Address_Required;
				errors.add(oe);
			}

			if(SAPOrder) {
				system.debug('!!! is sap order');
				if(String.isBlank(eachCall.Ship_To_Salutation_MVN__c) || String.isBlank(eachCall.Ship_Country_vod__c) || String.isBlank(eachCall.Country_Code_BI__c)) {
					OrderError oe = new OrderError();
					oe.errorOrder = eachCall.Id;
					oe.type = ErrorType.SAP;
					oe.errorMessage = System.Label.Order_Address_Required;
					errors.add(oe);
				}

				if(String.isBlank(eachCall.Order_Letter_MVN__c)) {
					OrderError oe = new OrderError();
					oe.errorOrder = eachCall.Id;
					oe.type = ErrorType.SAP;
					oe.errorMessage = System.Label.Order_Letter_Required;
					errors.add(oe);
				}
			}
		}

		if(!errors.isEmpty()) {
			for(OrderError oe : errors) {
				callMap.remove(oe.errorOrder);
			}
		}

		//Check for sample limits violations and remove calls that exceed limits from further processing		
		errors.addAll( checkSampleLimits(callMap.values()) );

		return errors;
	}

	public List<OrderError> submitFullOrders(List<Call2_vod__c> orders) {
		String callQuery = 'select ' + callQueryFields + ', (select ' + callSampleQueryFields + ' from Call2_Sample_vod__r) from Call2_vod__c where Id in :orders';
		List<OrderError> errors = new List<OrderError>();
		Map<Id,Call2_vod__c> callMap = new Map<Id,Call2_vod__c>((List<Call2_vod__c>)Database.query(callQuery));
		Boolean massSubmitErrors = false;

		if(SAPOrder) 
		{
			//Do inventory check
			List<Call2_Sample_vod__c> sapSamples = getCallSamples(callMap.values());
			List<OrderError> invtErrors = checkSAPInventory(sapSamples);

			if(invtErrors.isEmpty()) 
			{
				if (callMap.size() == 1)
				{
					List<OrderError> sapErrors = sendSAPOrder(callMap.values());
					errors.addAll(sapErrors);
					
					if (sapErrors.isEmpty()) {
						errors.addAll( submitCalls(callMap.values()) );
					}
				} 
				else 
				{
					errors.addAll(submitCalls(callMap.values()));
					for(OrderError oe : errors) {
						if(oe.errorOrder != null) {
							callMap.remove(oe.errorOrder);
						}
					}

					if(callMap.size() > 0) {
						SAPOrderBatchMVN orderProcessor = new SAPOrderBatchMVN(callMap.keySet(),campaignId);
						ID batchprocessid = Database.executeBatch(orderProcessor,1);// Batch size must be 1 dues to callout limitations
						Campaign_vod__c massOrderCampaign = new Campaign_vod__c(Id = campaignId);
						massOrderCampaign.SAP_Batch_ID_MVN__c = batchprocessid;
						try {
							update massOrderCampaign;
						} catch (Exception e) {
							OrderError oe = new OrderError();
							oe.type = ErrorType.ADMIN;
							oe.errorMessage = e.getMessage();
							oe.errorException = e;
							errors.add(oe);
						}
					}
				}
			} 
			else 
			{
				errors.addAll(invtErrors);
			}
		} 
		else 
		{
			// If it is not an SAP Order, actually apply the limits and submit the calls
			Savepoint sp2 = Database.setSavepoint();

			List<Call2_Sample_vod__c> finalSamples       = getCallSamples( callMap.values() );
			List<OrderError>          submitSampleErrors = submitCallSamples( finalSamples );

			if (!submitSampleErrors.isEmpty()) 
			{
				Database.rollback(sp2);
				errors.addAll(submitSampleErrors);
			} 
			else 
			{
				errors.addAll(submitCalls(callMap.values()));
			}
		}


		if(String.isNotBlank(campaignId)) {
			if(massSubmitErrors || !SAPOrder || (SAPOrder && callMap.size() == 1)) {
				String successCount = '1';
				if(errors.isEmpty()) {
					successCount = orders.size().format();
				} else {
					Set<Id> failedOrders = new Set<Id>();
					for(OrderError oe : errors) {
						if(oe.type == ErrorType.SAP) {
							successCount = '0';
							break;
						} else if(oe.errorOrder != null) {
							failedOrders.add(oe.errorOrder);
						}
					}
					if(successCount == '1') {
						Integer successCountInt = orders.size() - failedOrders.size();
						successCount = successCountInt.format();
					}
				}

				Campaign_vod__c massOrderCampaign = new Campaign_vod__c(Id = campaignId);
				String resultMessage = String.format(System.Label.SAP_Mass_Order_Status_Message, new string[] {successCount,orders.size().format()});

				// Update campaign if needed
				massOrderCampaign.SAP_Batch_Status_MVN__c = resultMessage;
				massOrderCampaign.SAP_Batch_ID_MVN__c = null;
				try{	
					update massOrderCampaign;
				}catch(Exception e){
					OrderError oe = new OrderError();
					oe.type = ErrorType.ADMIN;
					oe.errorMessage = e.getMessage();
					oe.errorException = e;
					errors.add(oe);
				}
			}
		}

		return errors;
	}

	public List<OrderError> submitCalls(List<Call2_vod__c> orders) {
		List<OrderError> errors = new List<OrderError>();

		for(Call2_vod__c c : orders){
			c.Status_vod__c = settings.Call_Submitted_Status_MVN__c;
			c.No_Disbursement_vod__c = true;
			c.Unlock_vod__c = false;
			c.Call_Date_vod__c = Date.today();
			c.Call_DateTime_vod__c = System.now();
			c.SAP_Errors_MVN__c = '';
			c.Delivery_Status_MVN__c = settings.Call_Order_Delivery_Status_Ordered_MVN__c;
			if(String.isBlank(c.Delivery_Status_MVN__c)) {
				c.Delivery_Status_MVN__c = 'Submitted';
			}
		}

		Database.SaveResult[] lsr = Database.update(orders, false);
		for(Integer i = 0;i<lsr.size(); i++){
			Database.SaveResult sr = lsr[i];
			if (!sr.isSuccess()) {
				for(Database.Error e : sr.getErrors()) {
					OrderError oe = new OrderError();
					oe.type = ErrorType.SALESFORCE;
					oe.errorOrder = orders[i].Id;
					oe.errorMessage = e.getMessage();
					errors.add(oe);
				}
		    }
		}


		return errors;
	}

	private List<Call2_Sample_vod__c> getCallSamples(List<Call2_vod__c> calls) {
		List<Call2_Sample_vod__c> css = Database.Query('select ' + callSampleQueryFields + ' from Call2_Sample_vod__c where Call2_vod__c in :calls');
		return css;
	}

	public List<OrderError> checkSAPInventory (Map<String,Integer> products) {
		List<OrderError> errors = new List<OrderError>();

		SAPErpAvailabilityMVN.HTTP_Port2 inventoryConnection = new SAPErpAvailabilityMVN.HTTP_Port2();
		inventoryConnection.endpoint_x = settings.SAP_Inventory_Check_Endpoint_MVN__c;
		if(settings.SAP_Integration_Timeout_MVN__c != null) {
			inventoryConnection.timeout_x = settings.SAP_Integration_Timeout_MVN__c.intValue();
		}
		//Build list 
		List<SAPOrderAvailabilityMVN.Item> itemList = new List<SAPOrderAvailabilityMVN.Item>();
		Map<Integer,String> lineNumbers = new Map<Integer,String>();
		Integer currentLine = 1;
		for(String product : products.keySet()) {
			SAPOrderAvailabilityMVN.Item newItem = new SAPOrderAvailabilityMVN.Item();
			newItem.SAPArticleId = product;
			newItem.Quantity = products.get(product).format();
			newItem.LineItem = currentLine.format();
			itemList.add(newItem);

			lineNumbers.put(currentLine,product);
			currentLine++;
		}

		SAPOrderAvailabilityMVN.ListOfItems itemListWrapper = new SAPOrderAvailabilityMVN.ListOfItems();
		itemListWrapper.Item = itemList;
		System.debug('!!! 1. Availability Request: ' + itemListWrapper);

		try {
			SAPOrderAvailabilityResponseMVN.SiebelOrderCheckAvailResponse response = inventoryConnection.MI_AvailabilityCheck_out_syn(itemListWrapper);
			System.debug('!!! 2. Availability Response: ' + response);

			for(SAPOrderAvailabilityResponseMVN.Item itemResponse : response.ListOfItems.Item) {
				if(Integer.valueOf(itemResponse.Quantity) > Integer.valueOf(itemResponse.AvailableQuantity)) {
					OrderError itemError = new OrderError();
					itemError.errorMessage = itemResponse.LineStatusMessage;
					itemError.type = ErrorType.INVENTORY;
					itemError.errorSAPProduct = lineNumbers.get(Integer.valueOf(itemResponse.LineItem));
					itemError.availableQuantity = Integer.valueOf(itemResponse.AvailableQuantity);
					errors.add(itemError);
				}
			}
		} catch (exception e) {
			OrderError itemError = new OrderError();
			itemError.errorMessage = System.Label.SAP_Connection_Error;
			itemError.type = ErrorType.SAP;
			itemError.errorException = e;
			itemError.errorException = e;
			errors.add(itemError);
		}
		

		return errors;

		/*Sample material number 60385863*/
	}

	public List<OrderError> checkSAPInventory (List<Call2_Sample_vod__c> callSamples) {

		Map<String,Integer> products = new Map<String,Integer>();

		for(Call2_Sample_vod__c cs : callSamples) {
			String prd = cs.Product_vod__r.SAP_Material_Number_MVN__c;
			if(!products.containsKey(prd)) {
				products.put(prd,cs.Quantity_vod__c.intValue());
			} else {
				products.put(prd,products.get(prd) + cs.Quantity_vod__c.intValue());
			}
		}

		List<OrderError> invtErrors = checkSAPInventory(products);

		return invtErrors;
	}

	public List<OrderError> sendSAPOrder (Call2_vod__c order) {
		List<OrderError> errors = new List<OrderError>();
		List<Call2_Sample_vod__c> callSamples = order.Call2_Sample_vod__r;
		// build request 
		List<SAPOrderMVN.Item> productList = new List<SAPOrderMVN.Item>();
		Integer lineCount = 1;
		for(Call2_Sample_vod__c callSample : order.Call2_Sample_vod__r) {
			SAPOrderMVN.Item lineItem = new SAPOrderMVN.Item();
			lineItem.SAPArticleID = callSample.Product_vod__r.SAP_Material_Number_MVN__c;
			lineItem.Quantity = callSample.Quantity_vod__c.intValue().format();
			lineItem.LineItem = lineCount.format();
			callSample.Line_Number_MVN__c = lineCount;
			lineCount++;
			productList.add(lineItem);
		}

		SAPOrderMVN.ListOfItems listOfItems = new SAPOrderMVN.ListOfItems();
		listOfItems.Item = productList;

		SAPOrderMVN.SiebelOrder theSAPOrder = new SAPOrderMVN.SiebelOrder();
		theSAPOrder.ListOfItems = listOfItems;
		theSAPOrder.SiebelOrderNum = order.Id;
		theSAPOrder.SiebelId = order.Account_vod__c;
		theSAPOrder.City = order.Ship_City_vod__c;
		theSAPOrder.ZipCode = order.Ship_Zip_vod__c;
		theSAPOrder.StreetAddress = order.Ship_Address_Line_1_vod__c;
		theSAPOrder.Country = order.Ship_Country_vod__c;
		theSAPOrder.LetterText = order.Ship_To_Salutation_MVN__c;
		theSAPOrder.Correspondence = order.Order_Letter_MVN__c;
		if(String.isBlank(settings.SAP_Order_Priority_MVN__c)) {
			theSAPOrder.Priority = '15';
		} else {
			theSAPOrder.Priority = settings.SAP_Order_Priority_MVN__c;
		}
		if(String.isBlank(settings.SAP_Order_Default_Customer_MVN__c)) {
			theSAPOrder.SAPCustomerID = 'IMVAISPLUS';
		} else {
			theSAPOrder.SAPCustomerID = settings.SAP_Order_Default_Customer_MVN__c;
		}

		theSAPOrder.DeliveryDate = Datetime.now().addDays(1).format('MM/dd/yyyy'); //From SAP team

		List<String> callFields = new List<String>{'Ship_to_Name_MVN__c','Ship_Address_Line_4_MVN__c','Ship_Address_Line_3_MVN__c','Ship_Address_Line_2_vod__c'};
		for(String eachCallField : callFields) {
			if(String.isNotBlank((String) order.get(eachCallField))) {
				if(String.isBlank(theSAPOrder.Line4)) {
					theSAPOrder.Line4 = (String) order.get(eachCallField);
				} else if(String.isBlank(theSAPOrder.Line3)) {
					theSAPOrder.Line3 = (String) order.get(eachCallField);
				} else if(String.isBlank(theSAPOrder.Line2)) {
					theSAPOrder.Line2 = (String) order.get(eachCallField);
				} else if(String.isBlank(theSAPOrder.Line1)) {
					theSAPOrder.Line1 = (String) order.get(eachCallField);
				}
			}
		}

		System.debug('!!! 3. Order Request: ' + theSAPOrder);
		Boolean hasErrors = false;
		SAPOrderResponseMVN.SiebelOrderResponse response;
		try {
			//attempt to send order here
			SAPErpOrderMVN.HTTP_Port2 request = new SAPErpOrderMVN.HTTP_Port2();
			request.endpoint_x = settings.SAP_Order_Placement_Endpoint_MVN__c;
			if(settings.SAP_Integration_Timeout_MVN__c != null) {
				request.timeout_x = settings.SAP_Integration_Timeout_MVN__c.intValue();
			}
			response = request.MI_SiebelOrder_out_sync(theSAPOrder);
			System.debug('!!! 4. Order Response: ' + response);
		} catch (exception e) {
			OrderError itemError = new OrderError();
			itemError.errorMessage = System.Label.SAP_Connection_Error;
			itemError.type = ErrorType.SAP;
			itemError.errorException = e;
			itemError.errorOrder = order.Id;
			errors.add(itemError);
			hasErrors = true;
		}

		if(!hasErrors) {
			if(response.OrderStatus.contains('9')) {
				OrderError itemError = new OrderError();
				itemError.errorMessage = response.OrderStatusMessage;
				itemError.type = ErrorType.SAP;
				itemError.errorOrder = order.Id;
				errors.add(itemError);
				hasErrors = true;
			}
		}

		if(!hasErrors) {
			try {
				order.External_ID__c = order.Ship_Country_vod__c + response.SAPOrderNumber;
				order.Web_Order_Number_MVN__c = response.SAPOrderNumber;
				order.Override_lock_vod__c = true;
				order.Status_vod__c = settings.Call_Saved_Status_MVN__c;
				update order;
				for(Call2_Sample_vod__c cs : callSamples) { 
					cs.External_ID__c = order.Ship_Country_vod__c + response.SAPOrderNumber + cs.Line_Number_MVN__c;
				}

				errors.addAll(submitCallSamples(callSamples));
				errors.addAll(submitCalls(new List<Call2_vod__c>{order}));
			} catch (exception e) {
				OrderError commError = new OrderError();
				commError.errorException = e;
				commError.type = ErrorType.SALESFORCE;
				commError.errorMessage = e.getMessage();
				commError.errorOrder = order.Id;
				errors.add(commError);
				hasErrors = true;
			}
		}

		return errors;
	}

	public class OrderError {
		public ErrorType type;
		public Id errorOrder;
		public String errorProduct;
		public String errorSAPProduct;
		public String errorMessage;
		public Integer availableQuantity;
		public Exception errorException;
	}
}