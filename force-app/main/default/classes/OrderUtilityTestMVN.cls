/*
  * OrderUtilityTestMVN
  *    Created By:     Kai Amundsen   
  *    Created Date:    September 13, 2013
  *    Description:     Unit tests for OrderUtilityMVN
 */
@isTest
private class OrderUtilityTestMVN {

    static Account act;
    static List<Product_vod__c> prds;
    static User callCenterUser;
    
      
    static {        
              
        callCenterUser = TestDataFactoryMVN.createTestCallCenterUser();
        System.runAs(callCenterUser) {
            TestDataFactoryMVN.createSettings();
            act = TestDataFactoryMVN.createTestHCP();
            act.OK_Status_Code_BI__c = 'Valid';
            act.OK_Stdopening_BI__c = 'Dear ';
            update act;

            act = [select Id,OK_Status_Code_BI__c,IsPersonAccount,OK_Stdopening_BI__c, PersonTitle,LastName, Name from Account where Id = :act.Id];
        }

        prds = new List<Product_vod__c>();

        for(Integer i=0;i<2;i++) {
            Product_vod__c prd = new Product_vod__c();
            prd.Name = 'Pradaxa ' + i + 'mg';
            prd.Product_Type_vod__c = 'BRC';
            prd.Orderable_MVN__c = true;
            prd.Country_Code_BI__c = 'DE';
            prd.SAP_Material_Number_MVN__c = '1234' + i;
            prd.OwnerId = callCenterUser.Id;  // Required so that sap user can see the product in the test
            prds.add(prd);
        }

        insert prds;

        prds[0].Bundled_Product_MVN__c = prds[1].Id;
        update prds[0];
        

        List<Sample_Limit_vod__c> limits = new List<Sample_Limit_vod__c>();
        for(Product_vod__c p : prds) {
            Sample_Limit_vod__c sl = new Sample_Limit_vod__c();
            sl.Account_vod__c = act.Id;
            sl.Enforce_Limit_vod__c = true;
            sl.Limit_Quantity_vod__c = 5;
            sl.Start_Date_vod__c = Date.today().addDays(-5);
            sl.End_Date_vod__c = Date.today().addDays(5);
            sl.Product_vod__c = p.Id;
            limits.add(sl);
        }
        
        insert limits; 
    }    

    @isTest static void testUserIsInSAPCountry () {
        Boolean isThisSAPContext;

        Test.startTest();
        System.runAs(callCenterUser) {
            isThisSAPContext = OrderUtilityMVN.isSAPOrder();
        }
        Test.stopTest();

        System.assert(isThisSAPContext);
    }

    @isTest static void testUserIsNotInSAPCountry () {
        Boolean isThisSAPContext;

        User notSAPUser;
        User currentUser = [select Id from User where Id =:UserInfo.getUserId()];
        System.runAs(currentUser) {
            notSAPUser = callCenterUser.clone(false,true,false,false);
            notSAPUser.Country_Code_BI__c = 'US';
            notSAPUser.UserName = 'nonSAPU@orderutilitytestmvn.com';
            notSAPUser.alias = 'nonSAPU';
            insert notSAPUser;
        }

        Test.startTest();
        System.runAs(notSAPUser) {
            isThisSAPContext = OrderUtilityMVN.isSAPOrder();
        }
        Test.stopTest();

        System.assert(!isThisSAPContext);
    }

    @isTest static void testAccountValidation () {
        OrderUtilityMVN ou = new OrderUtilityMVN();
        Account validAccount = TestDataFactoryMVN.createTestHCP();
        validAccount.OK_Status_Code_BI__c = 'Valid';
        update validAccount;
        Id actID = validAccount.Id;

        validAccount = [select Id,OK_Status_Code_BI__c,IsPersonAccount from Account where Id = :actId];

        Account invalidAccount = TestDataFactoryMVN.createTestConsumer();

        Map<Id,Boolean> validationCheckResults;

        Test.startTest();
        validationCheckResults = ou.validateAccounts(new List<Account>{validAccount,invalidAccount});
        Test.stopTest();

        System.assertEquals(2,validationCheckResults.size());
        System.assertEquals(true,validationCheckResults.get(validAccount.Id));
        System.assertEquals(false,validationCheckResults.get(invalidAccount.Id));
    }

    @isTest static void testCreateNewCall () {
        Call2_vod__c newCall;

        Test.startTest();
        System.runAs(callCenterUser) {
            newCall = OrderUtilityMVN.newCall();
        }
        Test.stopTest();

        System.assertEquals('DE',newCall.Country_Code_BI__c);
        System.assertEquals('Sample Only',newCall.Call_Type_vod__c);
        System.assert(String.isBlank(newCall.Id));
    }

    @isTest static void testCreateSamples () {
        List<OrderUtilityMVN.OrderError> errors;

        System.runAs(callCenterUser) {
            System.debug('!!! Acount ID: ' + act.Id);
            List<Call2_vod__c> manyCalls = new List<Call2_vod__c>();
            for (Integer i = 0; i < 50; i++) {
                Call2_vod__c c = OrderUtilityMVN.newCall();
                c.Account_vod__c = act.Id;
                manyCalls.add(c);
            }

            insert manyCalls;

            System.assertEquals(50,manyCalls.size());

            Map<Id,Integer> orderProducts = new Map<Id,Integer>();
            
            orderProducts.put(prds[0].Id,1);
            
            Test.startTest();
            
                OrderUtilityMVN ou = new OrderUtilityMVN();
                ou.validateAccounts(new List<Account>{act});
                System.debug('!!! ' + ou.validatedAccounts);
                errors = ou.createSamples(manyCalls,orderProducts);
            Test.stopTest();
                

            System.debug(errors);
            System.assert(errors.isEmpty());

            List<Call2_Sample_vod__c> sampleResults = [select Id,Quantity_vod__c from Call2_Sample_vod__c];

            System.assertEquals(100,sampleResults.size());

            for(Call2_Sample_vod__c cs : sampleResults) {
                System.assertEquals(1,cs.Quantity_vod__c);
            }
        }
    }

    @isTest static void test_check_sample_limits() {
        List<OrderUtilityMVN.OrderError> errors;

        
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = false;
        List<Call2_Sample_vod__c> csl = new List<Call2_Sample_vod__c>();

        Call2_vod__c call = createBasicCall();

        Call2_Sample_vod__c cs = new Call2_Sample_vod__c();
        cs.Account_vod__c = act.Id;
        cs.Call_Date_vod__c = Date.today();
        cs.Call2_vod__c = call.Id;
        cs.Product_vod__c = prds[1].Id;
        cs.Quantity_vod__c = 6;
        insert cs;
        

        Test.startTest();
        errors = ou.saveFullOrders(new List<Call2_vod__c>{call});
        Test.stopTest();
        

        System.assertEquals(1,errors.size());
        System.assertEquals(OrderUtilityMVN.ErrorType.LIMITS,errors[0].type);

        List<Call2_Sample_vod__c> sampleResults = [select Id,Delivery_Status_vod__c,Apply_Limit_vod__c,Limit_Applied_vod__c from Call2_Sample_vod__c];
        for(Call2_Sample_vod__c csr : sampleResults) {
            System.assertEquals(Null,csr.Delivery_Status_vod__c);
            System.assertEquals(false,csr.Apply_Limit_vod__c);
            System.assertEquals(false,csr.Limit_Applied_vod__c);
        }
    }

    @isTest static void test_check_submit_call_samples() {
        List<OrderUtilityMVN.OrderError> errors;

        
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = false;
        List<Call2_vod__c> calls = new List<Call2_vod__c>();
        List<Call2_Sample_vod__c> csl = new List<Call2_Sample_vod__c>();

        for(Integer i=0;i<2;i++) {
            Call2_vod__c call = new Call2_vod__c();
            call.Account_vod__c = act.Id;
            call.Call_Date_vod__c = Date.today();
            call.Status_vod__c = 'Saved_vod';
            call.Ship_Address_Line_1_vod__c = '123 Main St.';
            call.Ship_to_Name_MVN__c = 'John Doe';
            call.Ship_City_vod__c = 'Chicago';
            call.Ship_Zip_vod__c = '66666';
            calls.add(call);
        }
        insert calls;

        Call2_Sample_vod__c cs = new Call2_Sample_vod__c();
        cs.Account_vod__c = act.Id;
        cs.Call_Date_vod__c = Date.today();
        cs.Call2_vod__c = calls[0].Id;
        cs.Product_vod__c = prds[0].Id;
        cs.Quantity_vod__c = 1;
        csl.add(cs);

        Call2_Sample_vod__c cs2 = cs.clone(false,true,false,false);
        cs2.Product_vod__c = prds[1].id;
        cs2.Quantity_vod__c = 1;
        csl.add(cs2);

        Call2_Sample_vod__c cs3 = cs2.clone(false,true,false,false);
        cs3.Call2_vod__c = calls[1].Id;
        csl.add(cs3);

        insert csl;
        

        Test.startTest();
        errors = ou.submitCallSamples(csl);
        Test.stopTest();
        

        System.assertEquals(0,errors.size());

        List<Sample_Limit_Transaction_vod__c> slts = [select Remaining_Quantity_vod__c ,Quantity_To_Disperse_vod__c from Sample_Limit_Transaction_vod__c];
        System.assertEquals(3,slts.size());

        List<Sample_Limit_vod__c> sls = [select Disbursed_Quantity_vod__c,Product_vod__c from Sample_Limit_vod__c];

        for(Sample_Limit_vod__c sl : sls) {
            if(sl.Product_vod__c == prds[0].Id) {
                System.assertEquals(1,sl.Disbursed_Quantity_vod__c);
            } else if(sl.Product_vod__c == prds[0].Id) {
                System.assertEquals(2,sl.Disbursed_Quantity_vod__c);
            }
        }
    }

    @isTest static void test_submit_not_sap_success() {
        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'ZZ';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = false;

        Call2_vod__c call = createBasicCall();

        Test.startTest();
        errors = ou.submitFullOrders(new List<Call2_vod__c>{call});
        Test.stopTest();

        System.assertEquals(0,errors.size());
        call = [select Id,Status_vod__c,Delivery_Status_MVN__c from Call2_vod__c where Id = :call.Id];

        System.assertEquals('Submitted_vod',call.Status_vod__c);
        System.assertEquals('Submitted',call.Delivery_Status_MVN__c);
    }

    @isTest static void test_submit_not_sap_fail() {
        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'ZZ';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = false;

        Call2_vod__c call = createBasicCall();

        Test.startTest();
        Test.setReadOnlyApplicationMode(true);
        errors = ou.submitFullOrders(new List<Call2_vod__c>{call});
        Test.stopTest();

        System.assertEquals(1,errors.size());
    }


    /*  Section to test callouts  */

    @isTest static void test_submit_sap_success() {
        Test.setMock(WebServiceMock.class, new OrderResponseSuccessMockMVN());
        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'DE';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = true;

        Call2_vod__c call = createBasicCall();

        Test.startTest();
        errors = ou.submitFullOrders(new List<Call2_vod__c>{call});
        Test.stopTest();

        System.assertEquals(0,errors.size());
        call = [select Id,Status_vod__c,Delivery_Status_MVN__c,External_ID__c from Call2_vod__c where Id = :call.Id];

        System.assertEquals('Submitted_vod',call.Status_vod__c);
        System.assertEquals('Submitted',call.Delivery_Status_MVN__c);
        System.assertEquals('DESAP'+call.Id,call.External_ID__c);
    }

    @isTest static void test_successfull_inventory_check () {
        Test.setMock(WebServiceMock.class, new OrderAvailabilityResponseSuccessMockMVN());

        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'DE';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = true;
        
        Call2_vod__c call = createBasicCall();
        List<Call2_Sample_vod__c> csl = call.Call2_Sample_vod__r;

        Test.startTest();
        errors = ou.checkSAPInventory(csl);
        Test.stopTest();

        System.assertEquals(0,errors.size());
    }

    @isTest static void test_failed_inventory_check () {
        Test.setMock(WebServiceMock.class, new OrderAvailabilityResponseFailMockMVN());

        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'DE';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = true;
        
        Call2_vod__c call = createBasicCall();
        List<Call2_Sample_vod__c> csl = call.Call2_Sample_vod__r;

        Test.startTest();
        errors = ou.checkSAPInventory(csl);
        Test.stopTest();

        System.assertEquals(1,errors.size());
        System.assertEquals('Failed',errors[0].errorMessage);
    }

    @isTest static void test_successfull_order_send () {
        Test.setMock(WebServiceMock.class, new OrderResponseSuccessMockMVN());

        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'DE';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = true;
        
        Call2_vod__c call = createBasicCall();

        Test.startTest();
        errors = ou.sendSAPOrder(call);
        Test.stopTest();

        System.assertEquals(0,errors.size());

        call = [select Id,Status_vod__c,Delivery_Status_MVN__c from Call2_vod__c where Id = :call.Id];

        System.assertEquals('Submitted_vod',call.Status_vod__c);
        System.assertEquals('Submitted',call.Delivery_Status_MVN__c);
    }

    @isTest static void test_failed_order_send () {
        Test.setMock(WebServiceMock.class, new OrderResponseFailMockMVN());

        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'DE';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = true;
        
        Call2_vod__c call = createBasicCall();

        Test.startTest();
        errors = ou.sendSAPOrder(call);
        Test.stopTest();

        System.assertEquals(1,errors.size());

        call = [select Id,Status_vod__c,Delivery_Status_MVN__c from Call2_vod__c where Id = :call.Id];

        System.assertEquals('Saved_vod',call.Status_vod__c);
        System.assertEquals('New',call.Delivery_Status_MVN__c);

    }

    @isTest static void test_failed_order_save () {
        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = true;

        Call2_vod__c call = createBasicCall();
        call.Ship_Address_Line_1_vod__c = '';
        update call;

        Test.startTest();
        errors = ou.saveFullOrders(new List<Call2_vod__c>{call});

        call.Ship_Address_Line_1_vod__c = 'Test';
        call.Ship_To_Salutation_MVN__c = '';
        update call;

        errors = ou.saveFullOrders(new List<Call2_vod__c>{call});

        call.Ship_To_Salutation_MVN__c = 'Dear Test,';
        call.Country_Code_BI__c = 'DE';
        call.Order_Letter_MVN__c = null;
        update call;

        errors = ou.saveFullOrders(new List<Call2_vod__c>{call});
        
        Test.stopTest();

    }

    @isTest static void testFailCreateSamples () {
        List<OrderUtilityMVN.OrderError> errors;

        System.runAs(callCenterUser) {
            System.debug('!!! Acount ID: ' + act.Id);
            List<Call2_vod__c> manyCalls = new List<Call2_vod__c>();
            for (Integer i = 0; i < 50; i++) {
                Call2_vod__c c = OrderUtilityMVN.newCall();
                c.Account_vod__c = act.Id;
                manyCalls.add(c);
            }

            insert manyCalls;

            System.assertEquals(50,manyCalls.size());

            Map<Id,Integer> orderProducts = new Map<Id,Integer>();
            
            orderProducts.put(prds[0].Id,1);
            
            Test.startTest();
                Test.setReadOnlyApplicationMode(true);
                OrderUtilityMVN ou = new OrderUtilityMVN();
                ou.validateAccounts(new List<Account>{act});
                errors = ou.createSamples(manyCalls,orderProducts);
            Test.stopTest();
            System.assert(!errors.isEmpty());

        }
    }

    @isTest static void testFailSubmitCalls () {
        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'ZZ';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = false;

        Call2_vod__c call = createBasicCall();

        Test.startTest();
        Test.setReadOnlyApplicationMode(true);
        errors = ou.submitCalls(new List<Call2_vod__c>{call});
        Test.stopTest();

        System.assertEquals(1,errors.size());
    }

    @isTest static void testFailSubmitFullOrders () {
        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'ZZ';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = false;

        Call2_vod__c call = createBasicCall();

        Test.startTest();
        Test.setReadOnlyApplicationMode(true);
        errors = ou.submitFullOrders(new List<Call2_vod__c>{call});
        Test.stopTest();

        System.assertEquals(1,errors.size());
    }

    @isTest static void testFailOrderSend () {
        Test.setMock(WebServiceMock.class, new OrderResponseSuccessMockMVN());

        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'DE';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = true;
        
        Call2_vod__c call = createBasicCall();

        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrderResponseFailMockMVN());
        errors = ou.sendSAPOrder(call);

        Test.setMock(WebServiceMock.class, new OrderResponseSuccessMockMVN());
        Test.setReadOnlyApplicationMode(true);
        errors = ou.sendSAPOrder(call);
        Test.stopTest();

        System.assertEquals(1,errors.size());
    }

    @isTest static void testFailInventoryCheck () {
        Test.setMock(WebServiceMock.class, new OrderAvailabilityResponseFailMockMVN());

        List<OrderUtilityMVN.OrderError> errors;

        OrderUtilityMVN.userCountryCode = 'DE';
        OrderUtilityMVN ou = new OrderUtilityMVN();
        OrderUtilityMVN.SAPOrder = true;
        
        Call2_vod__c call = createBasicCall();
        List<Call2_Sample_vod__c> csl = call.Call2_Sample_vod__r;

        Test.startTest();
        errors = ou.checkSAPInventory(csl);
        Test.stopTest();

        System.assertEquals(1,errors.size());
    }

    @isTest static void callUtils () {
        List<Address_vod__c> addresses = new List<Address_vod__c>{TestDataFactoryMVN.createTestAddress(act)};
        OrderUtilityMVN ou = new OrderUtilityMVN();
        Call2_vod__c call = ou.newCallWithAddress(act, addresses);

        ou.isCallAddressValid(call);

        call.Ship_Address_Line_1_vod__c = '';

        ou.isCallAddressValid(call);

        call.Ship_Address_Line_1_vod__c = 'test';

        OrderUtilityMVN.SAPOrder = true;

        call.Ship_To_Salutation_MVN__c = '';

        ou.isCallAddressValid(call);

        call.Ship_To_Salutation_MVN__c = 'testtesttesttesttesttesttesttesttest';

        ou.isCallAddressValid(call);
    }

    // test helper methods
    private static Call2_vod__c createBasicCall () {
        Call2_vod__c call = new Call2_vod__c();
        call.Status_vod__c = 'Saved_vod';
        call.Account_vod__c = act.Id;
        call.Call_Date_vod__c = Date.today();
        call.Ship_Address_Line_1_vod__c = '123 Main St.';
        call.Ship_to_Name_MVN__c = 'John Doe';
        call.Ship_City_vod__c = 'Chicago';
        call.Ship_Zip_vod__c = '66666';
        call.Ship_Address_Line_2_vod__c = 'Dr. John Doe';
        call.Ship_Address_Line_3_MVN__c = 'Department';
        call.Ship_Address_Line_4_MVN__c = 'Main Hospital';
        call.Ship_Country_vod__c = 'DE';
        insert call;

        List<Call2_Sample_vod__c> csl = new List<Call2_Sample_vod__c>();
        Call2_Sample_vod__c cs = new Call2_Sample_vod__c();
        cs.Account_vod__c = act.Id;
        cs.Call_Date_vod__c = Date.today();
        cs.Call2_vod__c = call.Id;
        cs.Product_vod__c = prds[0].Id;
        cs.Quantity_vod__c = 1;
        csl.add(cs);
        insert csl;

        String callQuery = 'select ' + OrderUtilityMVN.callQueryFields + ', (select ' + OrderUtilityMVN.callSampleQueryFields + ' from Call2_Sample_vod__r) from Call2_vod__c where Id = \'' + call.Id +'\'';
        List<Call2_vod__c> callMap = (List<Call2_vod__c>)Database.query(callQuery);
        call = callMap[0];

        return call;
    }
}