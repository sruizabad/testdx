/**
* Skyvva,
* Helper class is defined to helpe developer use some need functions
*/
public with sharing class OutboundHelper {
    
    public class CustomException extends Exception{}
    public class CusLimitException extends Exception{}
    
    //Minus 10 second back for upperDatetime to find CDD
    public final static Integer nSecondBack=-10;
    
    //getLimitDMLRows: 10000
    private final static Integer MAX_DML_ROW=Limits.getLimitDMLRows();
    //Limits.getLimitQueryRows 50000
    private final static Integer MAX_QUERY_ROW=Limits.getLimitQueryRows();
    
    //Max DML row required
    private static Integer defMaxDMLrows=MAX_DML_ROW-1;//-1: for update interface
    
    //Where clause condition to find CDD
    private final static String dateRange='(LastModifiedDate>:fromDT AND LastModifiedDate<=:untilDT)';
    
    //namesapce
    public final static String ns='skyvvasolutions__';
    final static String C_RELATIONSHIP='RELATIONSHIP_X';
    
    final static String BATCHAPEX_JOBTYPE='BatchApex';
    static Integer MAX_CONCURRENT_JOB{
        get{
            
            CDD_CDT_Config__c cs=CDD_CDT_Config__c.getValues('CONCURRENT_JOB');
            if(cs==null || cs.Value__c==null)return 5;
            
            return Integer.valueOf(cs.Value__c);
        }
    }
    
    
    
    final static DateTime now=System.now();
    
    final static Set<String> NON_PENDING_JOB_STATUS= new Set<String>{'Completed', 'Aborted', 'Failed'};
    
    //Message allow to update to status new
    final static Set<String> msgStatusToUpdate=new Set<String>{'Failed', 'Pending'};
    //Cache
    final static Map<Id,Integer> nCDDcounter= new  Map<Id,Integer>();
    
    //for test the functions
    public static Map<String,Integer> testProperty{
        get;
        set{
            if(value!=null){
                testProperty=value;
                defMaxDMLrows=testProperty.get('TestMaxDMLrows');
                
            }
        }
    }
    
    //Describe Global,Map<>{key: sobject name lowercase, value: Schema.SObjectType}
    private static Map<String, Schema.SObjectType> mSONameSchemaSObjectType {
        get {
            if(mSONameSchemaSObjectType == null)mSONameSchemaSObjectType = Schema.getGlobalDescribe();
            return mSONameSchemaSObjectType;
        }
    }
    
    
    //static final String C_SELECT     ='SELECT';
    static final String C_FROM       ='FROM';
    static final String C_WHERE      ='WHERE';
    static final String C_ORDER_BY   ='ORDER BY';
    static final String C_LIMIT      ='LIMIT';
    static final String C_OFFSET     ='OFFSET';
    static final String C_ALL_ROWS   ='ALL ROWS';
    private static Map<String, Pattern> mapSQLElementPattern  {
        
        get {
            //The embeded expression (?i) at the beginning of the pattern will make it case insensitive. 
            //(?iu) will do the same thing for the Unicode characters.
            if(mapSQLElementPattern==null) {
                mapSQLElementPattern = new Map<String, Pattern> {
                    
                C_ALL_ROWS    => Pattern.compile('(?i)(.*?)[\\p{Space}]+?ALL ROWS(.*?)')
                ,C_OFFSET     => Pattern.compile('(?i)(.*?)[\\p{Space}]+?OFFSET[\\p{Space}]+?(.*?)')
                ,C_LIMIT      => Pattern.compile('(?i)(.*?)[\\p{Space}]+?LIMIT[\\p{Space}]+?(.*?)')
                ,C_ORDER_BY   => Pattern.compile('(?i)(.*?)[\\p{Space}]+?ORDER[\\p{Space}]+?BY(.*?)')
                ,C_WHERE      => Pattern.compile('(?i)(.*?)[\\p{Space}]+?WHERE[\\p{Space}]+?(.*?)')
                ,C_FROM       => Pattern.compile('(?i)(.*?)[\\p{Space}]+?FROM[\\p{Space}]+?(.*?)')
                                   
                /*,'SELECT FROM'=> Pattern.compile('(?i)SELECT[\\p{Space}]+?(.*?)[\\p{Space}]+?FROM')
               ,'INNER SELECT'=> Pattern.compile('(?i)SELECT[\\p{Space}]+?(.*?)SELECT[\\p{Space}]+?(.*?)FROM[\\p{Space}]+?(.*?)[\\p{Space}]+?FROM[\\p{Space}]+?(.*?)')              
                */
                   
                };
            } 
            return mapSQLElementPattern;
        }
    }
    
    static Integer getFreeQueryRow(){
        return (MAX_QUERY_ROW-Limits.getQueryRows());
    }
    
    //eg: sql: 'select Id,Name from Account where Name!=null order by Name limit 10'
    //output: SELECT=select Id,Name; FROM=from Account; 
    //        WHERE= where Name!=null; ORDER BY= order by Name; LIMIT= limit 10
    private static Map<String, String> getSqlElements(String sql) {
        ///System.debug('>getSqlElements: sql0:'+sql);
        Map<String, String> r = new Map<String, String>();
        
        if(String.isNotBlank(sql)) {
            
            sql = Pattern.compile('(?i)[\\p{Space}]+?FROM[\\p{Space}]+?').matcher(sql).replaceAll(' FROM ');  
            ///System.debug('>getSqlElements: sql1:'+sql);          
            Integer i = sql.indexOfIgnoreCase(' FROM ');
            if(i>0) {
                // FROM ...WHERE...ORDER BY...LIMIT...
                sql = sql.substring(i);
                sql = getSqlElementPattern(C_ALL_ROWS , sql, r);   //sql from last "FROM" eg: "FROM Contact  where id=null  ALL ROWS " 
                ///System.debug('>getSqlElements: ALL ROWS:'+sql); 
                sql = getSqlElementPattern(C_OFFSET   , sql, r);
                ///System.debug('>getSqlElements: LIMIT:'+sql);           
                sql = getSqlElementPattern(C_LIMIT    , sql, r);
                ///System.debug('>getSqlElements: LIMIT:'+sql);           
                sql = getSqlElementPattern(C_ORDER_BY , sql, r);
                ///System.debug('>getSqlElements: ORDER BY:'+sql);              
                sql = getSqlElementPattern(C_WHERE    , sql, r);
                ///System.debug('>getSqlElements: WHERE:'+sql);   
                sql = getSqlElementPattern(C_FROM     , sql, r);
                
            }
        }
        return r;
    }
    
    static String getSqlElementPattern(String elementName, String sql, Map<String, String> r) {
        ///System.debug('>getSqlElementPattern: '+elementName+':sql0:'+sql+'<<<');
        Matcher m =  mapSQLElementPattern.get(elementName).matcher(sql);
        while (m.find()) {
            r.put(elementName, sql.substring(m.end(1)).trim());
            return sql.substring(0, m.end(1));
        } 
        r.put(elementName, '');
        return sql;
    }
    
    public static Map<Id,AsyncApexJob> getRunningBatchJobs() {
        return new Map<Id, AsyncApexJob>(
                        [Select Id, Status, JobType, ApexClass.Name From AsyncApexJob 
                        where JobType=:BATCHAPEX_JOBTYPE 
                        and Status NOT IN : NON_PENDING_JOB_STATUS]);
    }
    
    public static Integer getFreeBatchJob(){
        
        Integer runningJob=getRunningBatchJobs().size();
        
        if(MAX_CONCURRENT_JOB==null || MAX_CONCURRENT_JOB<=runningJob)return 0;
        return (MAX_CONCURRENT_JOB-runningJob);
    }
    
    public static Map<Id,skyvvasolutions__Interfaces__c> getAllInterfaceOutbounds(Set<ID> integIDs){
        
        Map<Id,skyvvasolutions__Interfaces__c> m=new Map<Id,skyvvasolutions__Interfaces__c>([Select Id,skyvvasolutions__Name__c,LastRun__c, skyvvasolutions__Integration__c, skyvvasolutions__Query__c,
          (Select skyvvasolutions__ParentInterfaceId__c,skyvvasolutions__Parent_Relationship_Name__c,skyvvasolutions__ChildInterfaceId__c From skyvvasolutions__Parent_IChained_Interfaces__r) From skyvvasolutions__Interfaces__c 
            WHERE skyvvasolutions__Integration__c IN:integIDs AND skyvvasolutions__Type__c='OutBound']);
            
        return m;
        
    }
    
    //Remove child interface in IChains from Interface Group, because no need child-interface for CDD-worker
    public static List<skyvvasolutions__Interfaces__c> removeChildInterfaceFromGroup(List<skyvvasolutions__Interfaces__c> interfacesInIG){
    	
        List<skyvvasolutions__Interfaces__c> temps=new List<skyvvasolutions__Interfaces__c>();
        
        //Remove child interface
        for(skyvvasolutions__Interfaces__c inf:interfacesInIG){
            
            //Parent means ichains references to interface inf itself
            List<skyvvasolutions__IChained_Interfaces__c> ics=inf.getSObjects(ns+'IChained_Interfaces__r');
            
            //Parent
            if(ics==null)temps.add(inf);
        }
        
        return temps;
        
    }
    
    
    //Find last chile level in hierarchy
    private static void findLastChildInterfaces(skyvvasolutions__Interfaces__c parent, Map<Id,skyvvasolutions__Interfaces__c> mInterfaces,List<skyvvasolutions__Interfaces__c> childInterfaces){
        
        List<skyvvasolutions__IChained_Interfaces__c> ichains= getIChains(parent);
        
        //Parent has no childrend
        if(ichains==null || ichains.size()==0)childInterfaces.add(parent);
        else{
            for(skyvvasolutions__IChained_Interfaces__c ic: ichains){
                
                skyvvasolutions__Interfaces__c chInf=mInterfaces.get(ic.skyvvasolutions__ChildInterfaceId__c);
                
                if(chInf!=null)findLastChildInterfaces(chInf,mInterfaces,childInterfaces);
            }
            
        }
        
    }
    
    /*
    * Get child interfaces
    */
    private static List<skyvvasolutions__IChained_Interfaces__c>getIChains(skyvvasolutions__Interfaces__c parent){
        return (List<skyvvasolutions__IChained_Interfaces__c>)parent.getSObjects(ns+'Parent_IChained_Interfaces__r');
    }
    
    private static void findIChainsOfChildToParent(skyvvasolutions__Interfaces__c parent, Map<Id,skyvvasolutions__Interfaces__c> mInterfaces
                                                   ,Map<Id,List<skyvvasolutions__IChained_Interfaces__c>> icChildToParent)
    {
        
        List<skyvvasolutions__IChained_Interfaces__c> ichains= getIChains(parent);
        if(ichains==null)return;
        
        for(skyvvasolutions__IChained_Interfaces__c ic :ichains){
            
            //One child inteface can defined 2 chains with deffernt Parent interfaces
            if(icChildToParent.get(ic.skyvvasolutions__ChildInterfaceId__c)==null)icChildToParent.put(ic.skyvvasolutions__ChildInterfaceId__c, new List<skyvvasolutions__IChained_Interfaces__c>());
            
            icChildToParent.get(ic.skyvvasolutions__ChildInterfaceId__c).add(ic);
            
            skyvvasolutions__Interfaces__c chInf=mInterfaces.get(ic.skyvvasolutions__ChildInterfaceId__c);
            if(chInf!=null){
                findIChainsOfChildToParent(chInf,mInterfaces,icChildToParent);
                
            }
        }
        
    }
    
    /*
    * if last run does not specify then now()-10second
    */
    public static DateTime getLastRunDateTime(skyvvasolutions__Interfaces__c interfaceOut){
        
        if(interfaceOut.LastRun__c==null)return now.addSeconds(nSecondBack);//(-10)
        else return interfaceOut.LastRun__c;
    }
    
    public static DateTime getTimestampHigh(){
    	
    	if(Test.isRunningTest())return now;
    	else return now.addSeconds(nSecondBack);
    }
    
    /*
    * Calculate new UpperDateTime
    */
    public static DateTime calculateUpperTime(DateTime fromDT, DateTime untilDT){
        
        Long mlFromDT=fromDT.getTime();
        
        Long milliseconds = (untilDT.getTime()-mlFromDT)/2;
        
        return DateTime.newInstance(mlFromDT+milliseconds);
        
    }
    
    /*
    * Get sObject api name from interface's query
    */
    public static String getSObjectFromQuery(skyvvasolutions__Interfaces__c interfaceOut){
        //Query: Select Id,Name, BillingStreet From Account a, a.Parent p   WHERE Name=\'TestDynaicQuery2\'s'...
        if(String.isBlank(interfaceOut.skyvvasolutions__Query__c))return null;
        
        //getQueryElementValue(Map<String,String> sqlElement,String element)
        
        String sObj=getQueryElementValue(getSqlElements(interfaceOut.skyvvasolutions__Query__c),C_FROM);
        
        
        return (getQuerySObject(sObj));
    }
    
    /*
    selected sObject in query can be: Account a, a.Parent p
    e.g: select id,name from  Account a, a.Parent p
    */
    static String getQuerySObject(String sObj){
        ////sObj: Account a, a.Parent p
        //return Account
        if(String.isNotBlank(sObj))return (sObj.normalizeSpace().split(' ')[0]);
        
        return null;
    }
    
    /*
    * Get part of text of element
    */
    public static String getQueryElementValue(Map<String,String> sqlElement,String element){
        
        //element: WHERE
        //s: where    xxx
        String s=sqlElement.get(element);
        if(String.isBlank(s))return '';
        
        //replace element to empty eg:    xxx
        String v=Pattern.compile('(?i)'+element+'[\\p{Space}]+?').matcher(s).replaceAll('');
        
        return v.trim();//xxx
        

    }
    
    /**
    ** Find the relationship name between 2 SObjects
    ** e.g: baseObjectName: Contact; refObjectName: Acccount (in Contact looks up to Account)
    */
    private static String getRelationshipName(String childSObject, String parentSObject) {   
        
        if(childSObject!=null && childSObject.trim()!='') {
            
            Schema.SObjectType sObjType=mSONameSchemaSObjectType.get(childSObject);
            
            if(sObjType!=null){
                for(Schema.SObjectField sof : sObjType.newSObject().getSObjectType().getDescribe().fields.getMap().values()){       
                    Schema.DescribeFieldResult dfr = sof.getDescribe();
                    if(dfr.getType()==Schema.DisplayType.REFERENCE) {
                        for(Schema.SObjectType sot:dfr.getReferenceTo()) {
                            if((sot+'')==parentSObject) {
                                return dfr.getRelationshipName();
                            }
                        }
                    }
                }
            }
        }
        return null;
        
    }
    
    /*
    * Extend list of interface
    */
    public static void extendExecute(List<skyvvasolutions__Interfaces__c> interfacesInIG, Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration, DateTime dtUpperLimit){
        
        //
        Integer totalCDD=0;
        for(skyvvasolutions__Interfaces__c f: interfacesInIG){
            totalCDD+=OutboundHelper.countCDD(f ,infOfIntegration, dtUpperLimit);
        }
        
        //result: 4 of 49985/9999
        Integer result=totalCDD/defMaxDMLrows;
        System.debug('>>>extendExecute>result: '+result +' of '+totalCDD+'/'+defMaxDMLrows);
        if(result>=1){
            //math.mod(49985, 9999) =9989
            //Integer remainder = math.mod(totalCDD, defMaxDMLrows);
            
            //No need add remainder =>[(nExecute(4) + remainder(9989)) * interfacesInIG.size(8)] =59964 => [QUERY_TIMEOUT] Your query request was running for too long
            Integer nExecute=result;//+remainder; 
            
            //N-executions for an interface
            nExecute=nExecute*(interfacesInIG.size());
            System.debug('>>>extendExecute>nExecute: '+nExecute);
            for(Integer i=1;i<=nExecute;i++){
                interfacesInIG.add(new skyvvasolutions__Interfaces__c());
            }
            
        }
        System.debug('>>>extendExecute>extended>interfacesInIG-size: '+interfacesInIG.size());
    }
    
    /*
    * Count CDD of interfaces
    */
    public static Integer countCDD(skyvvasolutions__Interfaces__c interfaceOut,Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration, DateTime dtUpperLimit){
        
        
        nCDDcounter.clear();
        //Range DateTime
        DateTime untilDT=dtUpperLimit;
        //If Apex job pushed to Queue, So interface may be updated
        interfaceOut=infOfIntegration.get(interfaceOut.Id);
        
        if(interfaceOut==null)throw new OutboundHelper.CustomException('countCDD: Interface is deleted');
        DateTime fromDT=getLastRunDateTime(interfaceOut);
        //dtUpperLimit.format('MM-dd-yyyy hh:mm:ss a')
        if(untilDT<fromDT)throw new OutboundHelper.CustomException('TimestampHigh['+untilDT.format('MM-dd-yyyy hh:mm:ss S a')+'] < Last Run['+fromDT.format('MM-dd-yyyy hh:mm:ss S a')+'] for interface: '+interfaceOut.Id);
        
        //Find last interface (leaf)
        List<skyvvasolutions__Interfaces__c> lastChildInterfaces =new List<skyvvasolutions__Interfaces__c>();
        //findLastChildInterfaces(skyvvasolutions__Interfaces__c parent, Map<Id,skyvvasolutions__Interfaces__c> mInterfaces,List<skyvvasolutions__Interfaces__c> childInterfaces)
        findLastChildInterfaces(interfaceOut, infOfIntegration,lastChildInterfaces);
        
        //A child can refference more interfaces interfaces by ichains
        Map<Id,List<skyvvasolutions__IChained_Interfaces__c>> chainsOfChildToParent= new  Map<Id,List<skyvvasolutions__IChained_Interfaces__c>>();
        //findIChainsOfChildToParent(Interfaces__c parent, Map<Id,Interfaces__c> mInterfaces,Map<Id,List<skyvvasolutions__IChained_Interfaces__c>> icChildToParent)
        findIChainsOfChildToParent(interfaceOut,infOfIntegration,chainsOfChildToParent);
        
        //Find CDD from the lowest level of childrent to Parent
        for(skyvvasolutions__Interfaces__c cIF:lastChildInterfaces){
            
            System.debug('>>>countCDD>chlild: '+cIF.skyvvasolutions__Name__c);
            recurseCount(cIF,chainsOfChildToParent,infOfIntegration,fromDT, untilDT);
            
        }
        
        System.debug('>>>countCDD>nCDDcounter: '+nCDDcounter);
        
        Integer counter=0;
        for(Integer i: nCDDcounter.values()){
            counter+=i;
        }
        
        System.debug('>>>countCDD>count: '+counter);
        return counter;//counter;
    }
    //
    private static void recurseCount(skyvvasolutions__Interfaces__c cIF
                                            ,Map<Id,List<skyvvasolutions__IChained_Interfaces__c>> chainsOfChildToParent
                                            ,Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration
                                            ,DateTime fromDT, DateTime untilDT){
        
        Map<String, String> sqlEle=getSqlElements(cIF.skyvvasolutions__Query__c);
        String fromSObj=sqlEle.get(C_FROM);
        
        
        System.debug('>>>recurseCount>Interface-Id: '+cIF.Id+'>Name:'+cIF.skyvvasolutions__Name__c+'>sObject: '+fromSObj);
        if(String.isBlank(fromSObj))throw new CustomException('recurseCount: Interface['+cIF.Id+'], Invalid query :'+fromSObj);
        
        //IChains are refferences to Interfaces-Upper level
        List<skyvvasolutions__IChained_Interfaces__c> ics=chainsOfChildToParent.get(cIF.Id);
        
        String whereCondition='   WHERE  '+dateRange;
        //root, applies query filter if exist
        if(ics==null || ics.size()==0){
            //Dont String.escapeSingleQuotes, because it is whole condition and does not work
            String cond=getQueryElementValue(sqlEle,C_WHERE);
            whereCondition+=(String.isNotBlank(cond)?'    AND ('+ cond +')' :    '');
        }
        
        
        Integer freeQueryRow=getFreeQueryRow();
        //No need query, if freeQueryRow<0 then query error
        if(freeQueryRow<=0)return;
        
        String sqlCount='SELECT COUNT() '+ fromSObj + whereCondition +' LIMIT '+ freeQueryRow;
        System.debug('>>>recurseCDD>freeQueryRow: '+freeQueryRow+'>sqlCount:'+sqlCount);
        
        //interface has not queried yet
        if(nCDDcounter.get(cIF.Id)==null){
            Integer n=(Database.countQuery(sqlCount));
            nCDDcounter.put(cIF.Id, n);
        }
        
        //Count for upper interface of tree
        if(ics!=null && ics.size()>0){
            System.debug('>>>recurseCount>Next upper level');
            
            for(skyvvasolutions__IChained_Interfaces__c ic:ics){
                skyvvasolutions__Interfaces__c parentIF=infOfIntegration.get(ic.skyvvasolutions__ParentInterfaceId__c);
                if(parentIF!=null)recurseCount(parentIF,chainsOfChildToParent, infOfIntegration, fromDT, untilDT);
            }
        }
        
    }
    
    /**
    * Find CDD then upsert message with status new and interface's last run
    */
    public static DateTime findCDDAndUpsert(skyvvasolutions__Interfaces__c interfaceOut,Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration, DateTime dtUpperLimit){
        
        return findCDDAndUpsert2(interfaceOut,infOfIntegration, dtUpperLimit, false);
    }
    
    //@param isTest: true will not upsert any record for call in dev console
    public static DateTime findCDDAndUpsert2(skyvvasolutions__Interfaces__c interfaceOut,Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration, DateTime dtUpperLimit, Boolean isTest){
        
        try{
            //dtUpperLimit will apply for all interfaces
            //Find CDD limited by defMaxDMLrows:10000-1
            //OutboundHelper.findCDD(skyvvasolutions__Interfaces__c interfaceOut,Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration, DateTime dtUpperLimit)
            List<SObject> cddSObjects=OutboundHelper.findCDD(interfaceOut, infOfIntegration, dtUpperLimit);
            
            Integer foundCDD=cddSObjects.size();
            //Records found
            if(foundCDD>0){
	            if(!isTest)OutboundHelper.upsertMessageCDD(interfaceOut, cddSObjects);//dml row:9999    
            }
            
            //-10second back because records found with limit defMaxDMLrows, so other record with the same LastModifiedDate may available for next exection
            if(foundCDD>=defMaxDMLrows)interfaceOut.LastRun__c=((DateTime)cddSObjects.get(foundCDD-1).get('LastModifiedDate')).addSeconds(nSecondBack);
            //Record found<foundCDD or Not found
        	else interfaceOut.LastRun__c=dtUpperLimit;
        	if(!isTest)update interfaceOut;
        	
        	//Next execution of interfaceOut will find CDD between  [new LastRun-10second unitill dtUpperLimit]
        	infOfIntegration.get(interfaceOut.Id).LastRun__c=interfaceOut.LastRun__c;
            
        }catch(OutboundHelper.CusLimitException lmtEx){
            //new dtUpperLimit for next execute
            return OutboundHelper.calculateUpperTime(OutboundHelper.getLastRunDateTime(interfaceOut) ,dtUpperLimit);
        }
        
        //Here every thing success
        return interfaceOut.LastRun__c;
    }
    
    /**
    * Find CDD of givent interface outbound
    */
    public static List<SObject> findCDD(skyvvasolutions__Interfaces__c interfaceOut,Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration, DateTime dtUpperLimit){
        //Range DateTime
        DateTime untilDT=dtUpperLimit;
        //If Apex job pushed to Queue, So interface may be updated
        skyvvasolutions__Interfaces__c tmpInterface=infOfIntegration.get(interfaceOut.Id);
        
        if(tmpInterface==null)throw new OutboundHelper.CustomException('findCDD: Interface is deleted');
        DateTime fromDT=getLastRunDateTime(tmpInterface);
        
        //fromDT: interface.LastRun__c
        //if(untilDT<fromDT)throw new OutboundHelper.CustomException('Timestame of UpperLimit < Last Run for interface:'+tmpInterface.Id);
        if(untilDT<fromDT)throw new OutboundHelper.CustomException('TimestampHigh['+untilDT.format('MM-dd-yyyy hh:mm:ss S a')+'] < Last Run['+fromDT.format('MM-dd-yyyy hh:mm:ss S a')+'] for interface: '+tmpInterface.Id);
        
        interfaceOut=tmpInterface;
        
        //Find last interface (leaf)
        List<skyvvasolutions__Interfaces__c> lastChildInterfaces =new List<skyvvasolutions__Interfaces__c>();
        //findLastChildInterfaces(skyvvasolutions__Interfaces__c parent, Map<Id,skyvvasolutions__Interfaces__c> mInterfaces,List<skyvvasolutions__Interfaces__c> childInterfaces)
        findLastChildInterfaces(tmpInterface, infOfIntegration,lastChildInterfaces);
        
        //A child can refference more interfaces interfaces by ichains
        Map<Id,List<skyvvasolutions__IChained_Interfaces__c>> chainsOfChildToParent= new  Map<Id,List<skyvvasolutions__IChained_Interfaces__c>>();
        //findIChainsOfChildToParent(Interfaces__c parent, Map<Id,Interfaces__c> mInterfaces,Map<Id,List<skyvvasolutions__IChained_Interfaces__c>> icChildToParent)
        findIChainsOfChildToParent(tmpInterface,infOfIntegration,chainsOfChildToParent);
        
        //List<SObject> parentsObjs =new List<SObject>();
        //Parent interface (Account) has 2 IChains (Opportunity, Contact), Oppornity or Conact may Modify
        //SObject is Account
        Map<Id,Map<Id,SObject>> ifIdMapSObjParents =new Map<Id,Map<Id,SObject>>();
        
        //Find CDD from the lowest level of childrent to Parent
        for(skyvvasolutions__Interfaces__c cIF:lastChildInterfaces){
            
            //findChangedRecords(skyvvasolutions__Interfaces__c cIF,Map<Id,List<IChained_skyvvasolutions__Interfaces__c>> chainsOfChildToParent,
                                                     //Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration,
                                                    //DateTime fromDT, DateTime dtUpperLimit, Map<Id,Map<Id,SObject>> ifIdMapSObjParents)
            
            recurseCDD(cIF,chainsOfChildToParent,infOfIntegration,fromDT, untilDT,ifIdMapSObjParents);
        }
        if(!ifIdMapSObjParents.containsKey(tmpInterface.Id))return new List<SObject>();
        return ifIdMapSObjParents.get(tmpInterface.Id).values();
    }
    
    private static void recurseCDD(skyvvasolutions__Interfaces__c cIF
                                            ,Map<Id,List<skyvvasolutions__IChained_Interfaces__c>> chainsOfChildToParent
                                            ,Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration
                                            ,DateTime fromDT, DateTime untilDT, Map<Id,Map<Id,SObject>> ifIdMapSObjParents){
        
        Map<String, String> sqlEle=getSqlElements(cIF.skyvvasolutions__Query__c);
        
        System.debug('>>>recurseCDD>Interface-Id: '+cIF.Id+'>Name:'+cIF.skyvvasolutions__Name__c);
        
        skyvvasolutions__Interfaces__c parentIF=null;
        Map<Id,SObject> pSObj =new Map<Id,SObject>();
        
        //IChains are refference to Interfaces-Upper level
        List<skyvvasolutions__IChained_Interfaces__c> ics=chainsOfChildToParent.get(cIF.Id);
        
        //System.debug('>>recurseCDD>ifIdMapSObjParents: '+ifIdMapSObjParents);
        //Chians of current interface
        
        //mergeParentSObject(skyvvasolutions__Interfaces__c ifOut,Map<ID,Map<ID,SObject>> ifIDToIdSObject)
        Map<ID, SObject> tmpParentSObj=mergeParentSObject(cIF ,ifIdMapSObjParents);
        
        System.debug('>>recurseCDD>tmpParentSObj: '+tmpParentSObj.size());
        
        //CDD of root in hierarchy
        if(ics==null || ics.size()==0){
            //queryCDD(Map<String,String> sqlElement,DateTime fromDT, DateTime untilDT, List<SObject> parentsObjs)
            List<SObject> l=queryCDD(sqlEle,  fromDT, untilDT, tmpParentSObj.values());
            put(l,null, pSObj);
        }
        
        else{
            String chSObject=getQuerySObject(getQueryElementValue(sqlEle,C_FROM));
            //CDD of children in hierarchy
            //One child can reffernce to different Parents with 2 Ichains
            //These ichains are refference to interfaces upper level, BUT the same ParentSObject
            for(skyvvasolutions__IChained_Interfaces__c ic:ics){
               
                parentIF=infOfIntegration.get(ic.skyvvasolutions__ParentInterfaceId__c);
                
                String parentSObject=getSObjectFromQuery(parentIF);
                
                //Get relation between parent-child interface
                String relationShipName =(String.isNotBlank(ic.skyvvasolutions__Parent_Relationship_Name__c)? 
                                       ic.skyvvasolutions__Parent_Relationship_Name__c : getRelationshipName(chSObject,parentSObject));
                
                if(String.isBlank(relationShipName)) throw new CustomException('recurseCDD: No relationship defined between parent: '+parentSObject+' and child:'+chSObject);
                
                //Child query requires thier parent data with relationShipName
                sqlEle.put(C_RELATIONSHIP,relationShipName);
                //queryCDD(Map<String,String> sqlElement,DateTime fromDT, DateTime untilDT, List<SObject> parentsObjs)
                List<SObject> l=queryCDD(sqlEle, fromDT, untilDT, tmpParentSObj.values());
                put(l,relationShipName, pSObj);
            }
        }
        
        //Map <Interface-Level-ID,Map<sObject-ID, sObject-Parent-Found>
        if(!ifIdMapSObjParents.containsKey(cIF.Id))ifIdMapSObjParents.put(cIF.Id, pSObj);
        else ifIdMapSObjParents.get(cIF.Id).putAll(pSObj);
        System.debug('>>>recurseCDD>new parentsObjs-size:'+ifIdMapSObjParents.get(cIF.Id).size());
        
        parentIF=null;
        //if pSObj.clear() will cleared value in map ifIdMapSObjParents
        pSObj=null;
        tmpParentSObj=null;
        
        if(ics!=null && ics.size()>0){
            //Change Data Detection of current parent, Parent can have chained to the upper parent
            for(skyvvasolutions__IChained_Interfaces__c ic:ics){
                
                //Parent of current child
                parentIF=infOfIntegration.get(ic.skyvvasolutions__ParentInterfaceId__c);
                
                System.debug('>>>recurseCDD>Recurse-Interface Upper level:'+parentIF.Id+'>Name:'+parentIF.skyvvasolutions__Name__c);
                
                if(parentIF!=null)recurseCDD(parentIF, chainsOfChildToParent, infOfIntegration, fromDT, untilDT, ifIdMapSObjParents);
            }
        }
    }
    
    private static List<SObject> queryCDD(Map<String,String> sqlElement,DateTime fromDT, DateTime untilDT, List<SObject> parentsObjs)
    {
        
        return queryCDD2(sqlElement,fromDT, untilDT, parentsObjs);
        
    }
    
    /*
    Query CDD Order By LastModifiedDate ASC, 
    public for test in console
    */
    public static List<SObject> queryCDD2(Map<String,String> sqlElement,DateTime fromDT, DateTime untilDT, List<SObject> parentsObjs)
    {
        
        Integer freeQueryRow=getFreeQueryRow();
        //There may be CDD rows 10000, but less free query rows are allowed by salesforce
        if(freeQueryRow<defMaxDMLrows)throw new CusLimitException('queryCDD2: Free query row '+freeQueryRow+' is less then required query row '+defMaxDMLrows);
        
        String fromSObj=sqlElement.get(C_FROM);
        if(String.isBlank(fromSObj))throw new CustomException('queryCDD2: Invalid query sObject['+sqlElement+']');
        
        
        String relationShipName=sqlElement.get(C_RELATIONSHIP);
        //if relationshipName is not empty then RelationshipName.Id
        String selectionField =(String.isNotBlank(relationShipName)? relationShipName+'.Id' : 'Id')+',LastModifiedDate ';
        
        //dateRange: (LastModifiedDate>:fromDT AND LastModifiedDate<=:untilDT)
        String cddWhereClause='  WHERE  ('+dateRange+'  OR  Id  IN:parentsObjs)';
        
        //Root, apply query condition to root level
        if(String.isBlank(relationShipName)){
            String cond=getQueryElementValue(sqlElement,C_WHERE);
            cddWhereClause+=(String.isNotBlank(cond)?'  AND ('+ cond +')' :    '');
        }
        
        //Limt defMaxQueryRow: 10000-1
        String sql='SELECT '+selectionField + fromSObj + cddWhereClause +' Order By LastModifiedDate ASC LIMIT '+defMaxDMLrows;
        System.debug('>>>queryCDD2: '+sql);
        
        return Database.query(sql);
        
    }
    
    static void put(List<SObject> lso, String relationShipName, Map<Id,SObject> mSObject){
        for(SObject o:lso){
            
            sObject p=null;
            if(String.isNotBlank(relationShipName))p=o.getSObject(relationShipName);
            else p=o;
            
            if(p!=null)mSObject.put(p.Id,p);
        }
    }
    
    static Map<ID, SObject> mergeParentSObject(skyvvasolutions__Interfaces__c ifOut,Map<ID,Map<ID,SObject>> ifIDToIdSObject){
        
        
        Map<Id,SObject> tmpParentSObj =new Map<Id,SObject>();
        List<skyvvasolutions__IChained_Interfaces__c> icsOfCIF=getIChains(ifOut);
        
        if(icsOfCIF!=null){
            for(skyvvasolutions__IChained_Interfaces__c ic: icsOfCIF){//null????
                //parent sObject are found group by ChildInterfaceId
                if(ifIDToIdSObject.containsKey(ic.skyvvasolutions__ChildInterfaceId__c)){
                    
                    //remove no need to cache to avoid heapsize
                    //merge the parentSObject
                    tmpParentSObj.putAll(ifIDToIdSObject.remove(ic.skyvvasolutions__ChildInterfaceId__c));
                }
                
            }
        }
        
        return tmpParentSObj;
        
    }
    
    public static List<Database.UpsertResult> upsertMessageCDD(skyvvasolutions__Interfaces__c interfaceOut, List<SObject> objects){
        
        //Existing messages (Failed/Pending)
        
        //Mar 18, 2016:fixed? QUERY_TIMEOUT Your query request was running for too long.
        //?Map<String,skyvvasolutions__IMessage__c> msgExist=getMessagesExisting(interfaceOut, objects);
        
        List<skyvvasolutions__IMessage__c> listMsg =new List<skyvvasolutions__IMessage__c>();
        Integer c=0;
        for(SObject o: objects) {
            
            //exist?
            String key=('CDD_NEW#'+interfaceOut.Id)+o.Id;
            skyvvasolutions__IMessage__c msg =null;//?msgExist.get(key);
            
            //Fix: First error: Upsert failed. First exception on row 39 with id a5fJ00000008ez1IAA; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Cannot update status from Failed to New: []
            //?if(msg!=null && msg.skyvvasolutions__Status__c == 'Failed')continue;
            
            if(msg==null){
                
                c++;
                
                msg=new skyvvasolutions__IMessage__c(
                Name= 'IM#' + System.now().format('yyyy-MM-dd HH:mm:ss.SSS') + (c)
                ,skyvvasolutions__Type__c = 'Outbound'
                ,skyvvasolutions__Integration__c = interfaceOut.skyvvasolutions__Integration__c
                ,skyvvasolutions__Interface__c = interfaceOut.id
                ,skyvvasolutions__Related_To__c = o.Id
                
                ,skyvvasolutions__Comment__c=null
                ,skyvvasolutions__Is_Alerted__c=false
                
                //Unique
                ,skyvvasolutions__External_Id2__c=key//?(interfaceOut.Id+''+o.Id)+System.now().getTime()
                );
            }
            
            msg.skyvvasolutions__Status__c = 'New';
            msg.skyvvasolutions__Modification_Date__c=System.now();
            
            listMsg.add(msg);
      
        }
        
        //true: if one error then not allow partall success
        //List<Database.UpsertResult> upsertResults= Database.upsert(listMsg, externalIDField, true);
        
        //one failed then all will failed
        upsert listMsg skyvvasolutions__External_Id2__c;
        return null;
        
    }
    
    /*?
    No need query existing message, because upsert with external id
    Fixed: QUERY_TIMEOUT Your query request was running for too long.
    static Map<String,skyvvasolutions__IMessage__c> getMessagesExisting(skyvvasolutions__Interfaces__c interfaceOut, List<SObject> objects){
        
        Integer freeQueryRow=getFreeQueryRow();
        //if free query row less then required
        if(freeQueryRow<objects.size())throw new CusLimitException('getMessagesExisting: Free query row '+freeQueryRow+' is less then required query rows '+objects);
        
        Set<String> recordIds=new Set<String>();
        for(SObject o:objects)recordIds.add(String.valueOf(o.Id));
        
        Map<String,skyvvasolutions__IMessage__c> m=new Map<String,skyvvasolutions__IMessage__c>();
        for(skyvvasolutions__IMessage__c msg: [SELECT skyvvasolutions__Modification_Date__c,skyvvasolutions__Status__c, skyvvasolutions__Interface__c, skyvvasolutions__Related_To__c from skyvvasolutions__IMessage__c WHERE 
            skyvvasolutions__Integration__c=:interfaceOut.skyvvasolutions__Integration__c AND 
            skyvvasolutions__Interface__c=:interfaceOut.Id AND 
            skyvvasolutions__Related_To__c IN:recordIds AND skyvvasolutions__Status__c IN:msgStatusToUpdate 
            //QUERY_TIMEOUT: Your query request was running for too long
            AND Isdeleted=false limit :recordIds.size()]){
            
            //Convert lookup to 18chars
            String key=(''+(ID)msg.skyvvasolutions__Interface__c)+msg.skyvvasolutions__Related_To__c;
            m.put(key, msg);
        }
        
        return m;
    }
    */
    
    /*
    * All scheduled outbound
    */
    public static List<skyvvasolutions__Interfaces__c> getScheduledInterfaces(){
    	
    	skyvvasolutions__Interfaces__c[] l=new skyvvasolutions__Interfaces__c[0];
    	
    	for(skyvvasolutions__Interfaces__c f:[Select Id,skyvvasolutions__Name__c,LastRun__c, skyvvasolutions__Integration__c, skyvvasolutions__Query__c,skyvvasolutions__Number_of_Records_Per_Batch__c,
           //To find which parent of all schedule interfaces
           (Select Id From skyvvasolutions__IChained_Interfaces__r),
           
          (Select skyvvasolutions__ParentInterfaceId__c,skyvvasolutions__Parent_Relationship_Name__c,skyvvasolutions__ChildInterfaceId__c From skyvvasolutions__Parent_IChained_Interfaces__r) From skyvvasolutions__Interfaces__c 
            WHERE Schedule_Outbound__c =true AND skyvvasolutions__Type__c='OutBound'])
        {
        	l.add(f);
        }
        
        return removeChildInterfaceFromGroup(l);
        
    }
    
    public static void configurCDC(){
    	final Map<String,String> mConfig=new Map<String,String>{
    		'CONCURRENT_JOB' =>'5'
    		,'SC_CDD_TIME_INTERVAL_MINUTE' => '15'
    		,'SC_CDT_TIME_INTERVAL_MINUTE' => '10'
    		
    	};
    	
    	//existing
    	Map<String, CDD_CDT_Config__c> m=CDD_CDT_Config__c.getAll();
    	
    	List<CDD_CDT_Config__c> cdcs=new List<CDD_CDT_Config__c>();
    	for(String key: mConfig.keySet()){
    		
    		if(!m.containsKey(key)){
    			cdcs.add(new CDD_CDT_Config__c(Name=key,Value__c=mConfig.get(key)));
    		}
    	}
    	
    	if(cdcs.size()>0)insert cdcs;
    	
    }
    
/*
/*    
List<skyvvasolutions__Interfaces__c> l=OutboundHelper.getScheduledInterfaces();
//System.assertEquals(1, l.size());
Set<ID> ids= new Set<ID>{l[0].skyvvasolutions__Integration__c};
Map<Id,skyvvasolutions__Interfaces__c> m=OutboundHelper.getAllInterfaceOutbounds(ids);
DateTime dtUpperLimit=System.now();
//List<SObject> lo=OutboundHelper.findCDD(l[0],m,dtUpperLimit);
//System.assertEquals(1, lo.size());
OutboundHelper.testProperty=new Map<String,Integer>{'TestMaxDMLrows' =>4};
OutboundHelper.extendExecute(l,m, dtUpperLimit);
System.assertEquals(3,l.size());

//Database.executeBatch(new Skyvva_CDD_Worker(l, new Map<String,Integer>{'TestMaxDMLrows' =>4}), 1);
//Skyvva_CDD_Scheduler.schedule();
//Skyvva_CDT_Worker.executeBatch(l);
*/    
}