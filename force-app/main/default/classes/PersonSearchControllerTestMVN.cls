/*
* PersonSearchControllerTestMVN
* Created By: Roman Lerman
* Created Date: 3/7/2013
* Description: This is the test class for the PersonSearchControllerMVN
*/
@isTest
private class PersonSearchControllerTestMVN {

    static User callCenterUser;
    static {
      Profile p = [select Id from Profile where Name='CRC - Customer Request Agent']; 
      UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Germany'];
      callCenterUser = new User(Alias = 'heiditst', Email='heidibergen@oehringertest.com', 
                        EmailEncodingKey='UTF-8', FirstName = 'Heidi', LastName='Bergen', LanguageLocaleKey='de', 
                        LocaleSidKey='de', ProfileId = p.Id, UserRoleId = ur.Id, 
                        TimeZoneSidKey='Europe/Berlin', UserName='heidibergen@oehringertest.com',
                        Country_Code_BI__c = 'DE', External_ID_BI__c='12345678');

      User adminUser = new User(alias='ccusysad', email= 'callcentertestusermvn@callcenter.com', External_ID_BI__c='123456789', Country_Code_BI__c = 'DE',
                      emailencodingkey='UTF-8', firstName='Reginald', lastname='Wellington', languagelocalekey='en_US', 
                      localesidkey='en_US', profileid = [select Id from Profile where Name = 'System Administrator'].Id, 
                      isActive = true, timezonesidkey='America/Los_Angeles', username='callcentertestusermvn@callcenter.com',
                      Default_Article_Language_MVN__c = KnowledgeArticleVersion.Language.getDescribe().getPicklistValues()[0].getLabel());

      System.runAs(adminUser){
        insert callCenterUser;
      }
      
      System.runAs(callCenterUser){
        TestDataFactoryMVN.createSettings();

        Product_vod__c product = new Product_vod__c(Name='Test Product', External_ID_vod__c = 'testid');
        insert product;

        Account businessAccount = TestDataFactoryMVN.createTestBusinessAccount();  
        Account consumer = TestDataFactoryMVN.createTestConsumer();
        Account hcp = TestDataFactoryMVN.createTestHCP();

        Account deceasedHCP = new Account();
        deceasedHCP.Status_BI__c = 'Deceased';
        deceasedHCP.FirstName = 'Johnny';
        deceasedHCP.LastName = 'No Longer';
        deceasedHCP.RecordTypeId = [select Id from RecordType where SObjectType='Account' and DeveloperName = 'Professional_vod'].Id;
        insert deceasedHCP;

        TestDataFactoryMVN.createTestAddress(hcp);
        TestDataFactoryMVN.createTestAddress(consumer);
        TestDataFactoryMVN.createTestAddress(businessAccount);

        RecordType caRecordType = [select Id from RecordType where SObjectType='Customer_Attribute_BI__c' and Name='OK Local Attribute' limit 1];
        insert new Customer_Attribute_BI__c(Name = 'Admiralstabsarzt', OK_Type_Code_BI__c='TIH', OK_Type_BI__c='Stellung / Funktion');
        insert new Customer_Attribute_BI__c(Name = 'Admiralstabsarzt', OK_Type_Code_BI__c='TIH', CODE_ID_CEGEDIM_BI__c = 'TIH.WDE.G', OK_Type_BI__c='Stellung / Funktion',RecordTypeId=caRecordType.Id, CODE_END_VALID_DATE_OK_BI__c='9999-12-01',Type_Bi__c='CHILD_Role');
      }
  }

  static Case interaction = new Case(); 
  static PersonSearchControllerMVN controller;
  
  /**
   * Narrative
   * In order to: track a customer
   * As a: Call center agent
   * I want to: find contacts and associate an account with the case
   */
  
  /**
   * Scenario 1: Account search should return according to term. 
   * Given: an HCP calls the call center
   * And: They exist in the database
   * When I search for Contacts
   * Then only the contacts that have this information should appear
   */
  
  /* Scenario 1 */
  static testMethod void accountSearchShouldReturnAccordingToTerm(){
    System.runAs(callCenterUser){
      repReceivesCallandOpensSearch();
      repEntersContactInformation();
      searchTheAccounts();
      theAccountsReturnedShouldContainTheTerm();
    }
  }

  public static void repReceivesCallandOpensSearch(){
    //Create a new request and controller
    interaction = TestDataFactoryMVN.createTestCase(null);
    Test.setCurrentPage(new PageReference('/apex/PersonSearchMVN?caseId='+interaction.Id));
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new PersonSearchControllerMVN(stdController);
  }

  public static void repEntersContactInformation(){    
    //"enter" the person's info.
    controller.firstName  = 'Test';
    controller.lastName = 'Account';
    controller.businessName   = 'Hallshted*';
    controller.city     = 'Muni*';
    controller.country    = 'DE';
    controller.state   = [select Id from Customer_Attribute_BI__c where Name = 'Test State' limit 1].Id;
    controller.postalCode      = '9*';
  }

  public static void searchTheAccounts(){
    //Search The accounts
    controller.doSearch();
  }

  public static void theAccountsReturnedShouldContainTheTerm(){
    //Confirm the results contain test in the first name and account in the last. 
    System.assert(controller.searchResults.size() > 0);
    for (PersonSearchResultMockMVN resultAddress : controller.searchResults){
      System.debug('resultAddress: ' + resultAddress);
      System.assertEquals(true,resultAddress.firstName.containsIgnoreCase('test'));
      System.assertEquals(true,resultAddress.lastName.containsIgnoreCase('account'));
    }
  }
  /**/

  /* Scenario 2 */
  static testMethod void accountSearchShouldReturnAccordingToName(){
    System.runAs(callCenterUser){
      repReceivesCallandOpensSearch();
      repEntersContactInformation();
      searchTheAccounts();
      theAccountsReturnedShouldContainTheTerm();
    }
  }

  public static void repEntersContactName(){    
    //"enter" the person's info.
    controller.firstName  = 'Test';
    controller.lastName = 'Account';
  }
  /**/

  /**
   * Scenario 3: Attach an HCP to a case.
   * Given an HCP identified in a search
   * When I select the address
   * The Account is attached to the case. 
   */
  
  static testMethod void selectedAddressAttachesAccountToCase(){
    System.runAs(callCenterUser){
      repReceivesCallandOpensSearch();
      repEntersContactName();
      searchTheAccounts();
      selectAnAccount();
      theSelectedAddressAttachesTheAccountToTheCase();
    }
  }

  public static void selectAnAccount(){
    //Pick a contact and add it to the case.
    controller.actId = controller.searchResults[0].acctId;
    controller.addId = controller.searchResults[0].addrId;
    controller.updateCase();
  }

  public static void theSelectedAddressAttachesTheAccountToTheCase(){
    System.assertEquals(controller.cs.AccountId,controller.searchResults[0].acctId);
  }
  
  /**
   * Scenario 3: Account search on phone should return according to term. 
   * Given: an HCP calls the call center
   * And: They exist in the database
   * When I search for phone number
   * Then only the contacts that have this information should appear
   */
  static testMethod void accountSearchShouldReturnAccordingToPhone(){
    System.runAs(callCenterUser){
      //Define sosl search results
      Map<Id,Account> fixedSearchResults = new Map<Id,Account>([select Id from Account where Phone='5555555555']);
      Test.setFixedSearchResults(new List<Id>(fixedSearchResults.keySet()));


      repReceivesCallandOpensSearch();
      repEntersPhoneInformation();
      searchTheAccounts();
      theAccountsReturnedShouldContainThePhone();
    }
  }

  public static void repEntersPhoneInformation(){
    //"enter" the person's info.
    controller.phone = '5555555';
  }

  public static void theAccountsReturnedShouldContainThePhone(){
    //Confirm the results contain test in the first name and account in the last. 
    System.assert(controller.searchResults.size() > 0);
  }

  /**********/

  /**
   * Scenario 4: CTI Search should return according to phone 
   * Given: an HCP calls the call center
   * And: They exist in the database
   * When I CTI search for phone number
   * Then only the contacts that have this information should appear
   */
  static testMethod void ctiSearchShouldReturnAccordingToPhone(){
    System.runAs(callCenterUser){
      //Define sosl search results
      Map<Id,Account> fixedSearchResults = new Map<Id,Account>([select Id from Account where Phone='5555555555']);
      Test.setFixedSearchResults(new List<Id>(fixedSearchResults.keySet()));


      repReceivesCallandOpensCTISearch();
      theAccountsReturnedShouldContainThePhone();
    }
  }

  public static void repReceivesCallandOpensCTISearch(){
    //Create a new request and controller
    interaction = TestDataFactoryMVN.createTestCase();
    Test.setCurrentPage(new PageReference('/apex/PersonSearchMVN?caseId='+interaction.Id+'&phoneSearch=5555555555'));
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new PersonSearchControllerMVN(stdController);
  }

  /**********/


  /**
   * Scenario 5: Business Account search on name should return Account. 
   * Given: an Individual calls on behalf of a Business Account
   * And: They exist in the database
   * When I search for Business Account
   * Then only the Business Accounts that have this information should appear
   */
  static testMethod void businessAccountSearchShouldReturnAccordingToNameAndAddress(){
    System.runAs(callCenterUser){
      repReceivesCallAndOpensBusinessSearch();
      repEntersBusinessAccountInformation();
      searchTheAccounts();
      theAccountsReturnedShouldContainTheNameAndAddress();
    }
  }

  public static void repReceivesCallAndOpensBusinessSearch(){
    //Create a new request and controller
    interaction = TestDataFactoryMVN.createNewTestCase(false);
    Test.setCurrentPage(new PageReference('/apex/PersonSearchMVN?caseId='+interaction.Id+'&isPersonSearch=false'));
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new PersonSearchControllerMVN(stdController);
  }

  public static void repEntersBusinessAccountInformation(){
    //"enter" the person's info.
    controller.businessName = 'Smith Pharmacy';
  }

  public static void theAccountsReturnedShouldContainTheNameAndAddress(){
    System.assertEquals(false, controller.isPersonSearch);
    //Confirm the results contain test in the first name and account in the last. 
    System.assert(controller.searchResults.size() == 1);
    for (PersonSearchResultMockMVN resultAddress : controller.searchResults){
      System.debug('resultAddress: ' + resultAddress);
      System.assertEquals('Smith Pharmacy', resultAddress.name);
    }
  }
  
  /**********/

  /**
   * Scenario 6: After selecting a Business Account we can remove the Business 
   * Given: an Individual calls on behalf of a Business Account
   * And: They exist in the database
   * When I search for Business Account
   * Then only the Business Accounts that have this information should appear
   * I can then select them
   * When I remove them they disappear from the Interaction
   */
  static testMethod void whenBusinessAccountIsRemovedItIsRemovedFromTheCase(){
    System.runAs(callCenterUser){
      repReceivesCallAndOpensBusinessSearch();
      repEntersBusinessAccountInformation();
      searchTheAccounts();
      theAccountsReturnedShouldContainTheNameAndAddress();
      selectTheBusinessAccount();
      removeTheBusinessAccount();
      theBusinessAccountIsRemoved();
    }
  }

  public static void selectTheBusinessAccount(){
    controller.actId = controller.searchResults[0].acctId;
    controller.updateCase();
  }

  public static void removeTheBusinessAccount(){
    controller.removeBusiness();
  }

  public static void theBusinessAccountIsRemoved(){
    System.assertEquals(null, [select Business_Account_MVN__c from Case where Id =: interaction.Id].Business_Account_MVN__c);
  }
  /**********/
  
  /*
   * Scenario 7: If HCP is searched and is not found then an HCP is created
   * Given: an HCP calls
   * When I search for HCP Account
   * I do not find them
   * So, I create them
   */

  static testMethod void createHCPAndAttachToCase(){
    repReceivesCallFromHCPandOpensSearch();
    repEntersNewHCPInformation();
    searchTheAccounts();
    hcpNotFound();
    createHCP();
    theCreatedHCPIsAttachedToTheCase();
  }
  public static void repReceivesCallFromHCPandOpensSearch(){
    //Create a new request and controller
    interaction = TestDataFactoryMVN.createTestCase(null);
    Test.setCurrentPage(new PageReference('/apex/PersonSearchMVN?caseId='+interaction.Id));
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new PersonSearchControllerMVN(stdController);
  }
  public static void repEntersNewHCPInformation(){
    controller.lastName   = 'HCP';
    controller.firstName  = 'Peter';
  }
  public static void hcpNotFound(){
    System.assertEquals(0, controller.searchResults.size());
  }
  public static void createHCP(){
    controller.updateNewPerson();
    controller.createAccount.Specialty_BI__c = TestDataFactoryMVN.createIndividualSpecialty().Id;
    controller.createAccount.OKIndividual_Class_BI__c = TestDataFactoryMVN.createIndividualClass().Id;
    controller.createAccount.RecordTypeId = controller.hcpRecordTypeId;
    
    System.assertEquals(controller.createAccount.FirstName, 'Peter');
    System.assertEquals(controller.createAccount.LastName, 'HCP');
    System.assertEquals([select AccountId from Case where Id =: interaction.Id].AccountId, null);
    
    controller.createAccount();

    System.assertEquals([select Account.OK_Status_Code_BI__c from Case where Id =: interaction.Id].Account.OK_Status_Code_BI__c, 'Cannot Validate');
  }
  public static void theCreatedHCPIsAttachedToTheCase(){
    System.debug('ApexPages Messages: '+ApexPages.getMessages());
    System.assertEquals([select Account.FirstName from Case where Id =: interaction.Id].Account.FirstName, 'Peter');
    System.assertEquals([select Account.LastName from Case where Id =: interaction.Id].Account.LastName, 'HCP');
    System.assertEquals([select Account.OK_Status_Code_BI__c from Case where Id =: interaction.Id].Account.OK_Status_Code_BI__c, 'Cannot Validate');
  } 

  /*********/

  /*
   * Scenario 8: If Consumer is searched and is not found then an Consumer is created
   * Given: a Consumer calls
   * When I search for Consumer
   * I do not find them
   * So, I create them
   */

  static testMethod void createConsumerAndAttachToCase(){
    repReceivesCallandOpensSearch();
    repEntersNewConsumerInformation();
    searchTheAccounts();
    consumerNotFound();
    createConsumer();
    theCreatedConsumerIsAttachedToTheCase();
  }
  public static void repEntersNewConsumerInformation(){
    controller.lastName   = 'Consumer';
    controller.firstName  = 'Joe';
  }
  public static void consumerNotFound(){
    System.assertEquals(0, controller.searchResults.size());
  }
  public static void createConsumer(){
    controller.updateNewPerson();
    controller.createAccount.RecordTypeId = controller.consumerRecordTypeId;    
    controller.createAddress.Name = '123 Main St.';
    controller.createAccount();
  }
  public static void theCreatedConsumerIsAttachedToTheCase(){
    System.assertEquals([select Account.FirstName from Case where Id =: interaction.Id].Account.FirstName, 'Joe');
    System.assertEquals([select Account.LastName from Case where Id =: interaction.Id].Account.LastName, 'Consumer');
    System.assertEquals([select Address_MVN__r.Name from Case where Id =: interaction.Id].Address_MVN__r.Name, '123 Main St.');
    System.assertEquals([select Account.OK_Status_Code_BI__c from Case where Id =: interaction.Id].Account.OK_Status_Code_BI__c, 'Temporary');
  } 

  /*********/

  /*
   * Scenario 9: If Business is searched and is not found then a Business is created
   * Given: a person from a Business calls
   * When I search for the Business
   * I do not find it
   * So, I create it
   */

  static testMethod void createBusinessAndAttachToCase(){
    System.runAs(callCenterUser){
      repReceivesCallandOpensBusinessSearch();
      repEntersNewBusinessInformation();
      searchTheAccounts();
      businessNotFound();
      createBusiness();      
      theCreatedBusinessIsAttachedToTheCase();
    }
  }

  public static void repEntersNewBusinessInformation(){
    controller.businessName   = 'Test Pharmacy';
    controller.phone = '6666666666';
    controller.addressLine1 = '123 Main St.';
    controller.city = 'Munich';
    controller.state = [select Id from Customer_Attribute_BI__c where Name = 'Test State' limit 1].Id;
    controller.postalCode = '12345';
    controller.country = 'DE';
  }
  public static void businessNotFound(){
    System.assertEquals(0, controller.searchResults.size());
  }
  public static void createBusiness(){
    controller.updateNewPerson();   
    controller.createAccount.Specialty_BI__c = TestDataFactoryMVN.createWorkplaceSpecialty().Id;
    controller.createAccount.OKWorkplace_Class_BI__c = TestDataFactoryMVN.createWorkplaceClass().Id;
    controller.createAccount();
  }
  public static void theCreatedBusinessIsAttachedToTheCase(){
    System.assertEquals([select Business_Account_MVN__r.Name from Case where Id =: interaction.Id].Business_Account_MVN__r.Name, 'Test Pharmacy');
    System.assertEquals([select Business_Account_MVN__r.Phone from Case where Id =: interaction.Id].Business_Account_MVN__r.Phone, '6666666666');
    System.assertEquals([select Business_Account_MVN__r.OK_Status_Code_BI__c from Case where Id =: interaction.Id].Business_Account_MVN__r.OK_Status_Code_BI__c, 'Temporary');
    System.assertEquals([select Address_MVN__r.Primary_vod__c from Case where Id =: interaction.Id].Address_MVN__r.Primary_vod__c, true);
    System.assertEquals([select Address_MVN__r.Name from Case where Id =: interaction.Id].Address_MVN__r.Name, '123 Main St.');    
    System.assertEquals([select Address_MVN__r.City_vod__c from Case where Id =: interaction.Id].Address_MVN__r.City_vod__c, 'Munich');
    System.assertEquals([select Address_MVN__r.OK_State_Province_BI__c from Case where Id =: interaction.Id].Address_MVN__r.OK_State_Province_BI__c, [select Id from Customer_Attribute_BI__c where Name = 'Test State' limit 1].Id);
    System.assertEquals([select Address_MVN__r.Zip_vod__c from Case where Id =: interaction.Id].Address_MVN__r.Zip_vod__c, '12345');
    System.assertEquals([select Address_MVN__r.Country_Code_BI__c from Case where Id =: interaction.Id].Address_MVN__r.Country_Code_BI__c, 'DE');

    repReturnsToInteraction();

    Case cs = [select AccountId, Address_MVN__c, Address_MVN__r.Name, Address_MVN__r.City_vod__c, Address_MVN__r.OK_State_Province_BI__r.Name, 
                Address_MVN__r.Zip_vod__c, Address_MVN__r.Country_Code_BI__c 
                from Case where Id = :controller.cs.Id limit 1];

    V2OK_Data_Change_Request__c dcr = [select RecordTypeId, HCO_Temp_External_ID_BI__c, OK_Workplace_Class_BI__r.Name, Workplace_Specialty_BI__r.Name,
                      Phone__c, Change_Type__c, Status__c, ADDR_Temp_External_ID_BI__c, New_Address_Line_1__c, City_vod__c,
                      OK_State_Province_BI__r.Name, Zip_vod__c, Country_DS__c
                      from V2OK_Data_Change_Request__c where HCO_Temp_External_ID_BI__c = :cs.AccountId];

    System.assertEquals('123 Main St.', cs.Address_MVN__r.Name);
    System.assertEquals('Munich', cs.Address_MVN__r.City_vod__c);
    System.assertEquals('Test State', cs.Address_MVN__r.OK_State_Province_BI__r.Name);
    System.assertEquals('12345', cs.Address_MVN__r.Zip_vod__c);
    System.assertEquals('DE', cs.Address_MVN__r.Country_Code_BI__c);

    Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();

    System.assertEquals([select Id from RecordType where SObjectType = 'V2OK_Data_Change_Request__c' AND DeveloperName = :settings.DCR_Record_Type_MVN__c].Id,dcr.RecordTypeId);
    System.assertEquals(cs.AccountId, dcr.HCO_Temp_External_ID_BI__c);
    System.assertEquals('Test Workplace Class', dcr.OK_Workplace_Class_BI__r.Name);
    System.assertEquals('Test Workplace Specialty', dcr.Workplace_Specialty_BI__r.Name);
    System.assertEquals('6666666666', dcr.Phone__c);
    System.assertEquals(settings.DCR_Create_Change_Type_MVN__c, dcr.Change_Type__c);
    System.assertEquals(settings.DCR_Status_MVN__c, dcr.Status__c);
    System.assertEquals(cs.Address_MVN__c, dcr.ADDR_Temp_External_ID_BI__c);
    System.assertEquals('123 Main St.', dcr.New_Address_Line_1__c);
    System.assertEquals('Munich', dcr.City_vod__c);
    System.assertEquals('Test State', dcr.OK_State_Province_BI__r.Name);
    System.assertEquals('12345', dcr.Zip_vod__c);
    System.assertEquals('DE', dcr.Country_DS__c);
  } 

  /*********/

  /*
   * Scenario 10: If a new HCP and new Business are created then they must create a child account
   * Given: a new HCP Calls from a new Hospital
   * When I search for HCP
   * I do not find it
   * So, I create it
   * When I search for the Business
   * I do not find it
   * So, I create it
   * Then I return to intraction and choose a role
   * And a child account is created
   */

  static testMethod void createBusinessAndHcpAndAttachToCase(){
    System.runAs(callCenterUser){
      repReceivesCallandOpensSearch();
      repEntersNewHCPInformation();
      searchTheAccounts();      
      createHCP();
      controller.isPersonSearch = false;
      repEntersNewBusinessInformation();
      searchTheAccounts();
      createBusiness();
      repReturnsToInteraction();
      repChoosesRoleAtHospital();

      aNewChildAccountIsCreated();
    }
  }

  static void repReturnsToInteraction(){
    controller.returnToInteraction();
  }
  static void repChoosesRoleAtHospital(){
    controller.createChildAccount.Role_BI__c = [select Id from Customer_Attribute_BI__c where Name = 'Admiralstabsarzt' limit 1].id;
    controller.createChildAccount();
  }

  static void aNewChildAccountIsCreated(){
    System.assertEquals([select Account.OK_Status_Code_BI__c from Case where Id =: interaction.Id].Account.OK_Status_Code_BI__c, 'Temporary');

    Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();
    System.assert([select Id from Child_Account_vod__c 
                   where Child_Account_vod__c =:controller.cs.AccountId
                     AND Parent_Account_vod__c =:controller.cs.Business_Account_MVN__c
                     AND OK_Status_Code_BI__c =:settings.Temporary_Status_MVN__c].size() == 1, String.valueof(controller.createChildAccountError) + ApexPages.getMessages());
  }



  static testMethod void verifyThatDeceasedHCPsAreExcludedFromSearch(){
    repReceivesCallandOpensSearch();

    controller.firstName = 'Johnny';
    controller.lastName = 'No Longer';

    controller.doSearch();

    System.assertEquals(0, controller.searchResults.size());
  }

  static testMethod void testViewRecordTypeSelections(){
    repReceivesCallandOpensSearch();
    
    System.assertEquals('All', controller.recordTypeText);
    System.debug(controller.recordTypeSelectOptions);
    System.assert(1 < controller.recordTypeSelectOptions.size());
  }

  static testMethod void testResultMock () {
    PersonSearchResultMockMVN psr = new PersonSearchResultMockMVN();

    psr.lastName = 'Smith';
    System.assertEquals('Smith',psr.lastName);

    psr.city = 'Chicago';
    System.assertEquals('Chicago',psr.city);

    psr.setAccount(new Account(LastName='Jones'));
    System.assertEquals('Jones',psr.lastName);

    psr.setAddress(new Address_vod__c(Name='123 Main'));
    System.assertEquals('123 Main', psr.addressLine1);

    Account testAccount = TestDataFactoryMVN.createTestHCP();
    testAccount.OK_Status_Code_BI__c = 'test';
    update testAccount;

    PersonSearchResultMockMVN psr2 = new PersonSearchResultMockMVN(testAccount);

    Address_vod__c address = TestDataFactoryMVN.createTestAddress(testAccount);

    PersonSearchResultMockMVN psr3 = new PersonSearchResultMockMVN(address);

    PersonSearchResultMockMVN psr4 = new PersonSearchResultMockMVN(testAccount, address);

  }

  static testMethod void testStandAloneSearch(){
    Case interactionEmpty = new Case();
    ApexPages.standardController stdController = new ApexPages.standardController(interactionEmpty);
    controller = new PersonSearchControllerMVN(stdController);

    System.assertEquals(true, controller.standAloneSearch);
    System.assertEquals(null, controller.cs);

    controller.lastName   = 'Smith';
    controller.firstName  = 'Peter';
    controller.phone  = '5555555';
    controller.addressLine1 = '123 Test St';

    controller.updateNewPerson();
    controller.enterAccount();

    List<Case> noCase = [select Id from Case where AccountId = :controller.createAccount.Id];

    System.assertEquals(true, noCase.isEmpty());

  }

  static testMethod void shortPhoneSearch(){
    repReceivesCallandOpensSearch();
    controller.phone = '5';

    searchTheAccounts();

    System.assert(!ApexPages.getMessages().isEmpty());
  }

  static testMethod void reachSearchLimit(){
    Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();
    settings.Person_Search_Max_Results_MVN__c = 1;
    insert settings;
    
    repReceivesCallandOpensSearch();
    repEntersContactInformation();
    searchTheAccounts();
    
    System.assert(!ApexPages.getMessages().isEmpty());
  }

  static testMethod void testChangeRecordType(){
    interaction = TestDataFactoryMVN.createTestCase();
    Test.setCurrentPage(new PageReference('/apex/PersonSearchMVN?caseId='+interaction.Id));
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new PersonSearchControllerMVN(stdController);

    controller.createAccount.RecordTypeId = controller.hcpRecordTypeId;
    controller.changeRecordType();

    System.assertEquals(false, controller.showNewButton);
    System.assertEquals(null, controller.createAddress.Name);
    System.assertEquals(null, controller.createAddress.OK_State_Province_BI__c);
    System.assertEquals(null, controller.createAddress.Zip_vod__c);
  }

  static testMethod void testRetrievalOfCustomerAttributes(){
    Customer_Attribute_BI__c customerAttribute = new Customer_Attribute_BI__c();
    customerAttribute.Name = 'Test State';
    customerAttribute.Type_BI__c = 'ADDR_State';
    insert customerAttribute;

    TestDataFactoryMVN.createIndividualClass();
    TestDataFactoryMVN.createIndividualSpecialty();
    TestDataFactoryMVN.createWorkplaceClass();
    TestDataFactoryMVN.createWorkplaceSpecialty();

    System.runAs(callCenterUser){
      repReceivesCallandOpensSearch();
      System.assertEquals(1, controller.getStatesProvinces().size());
      System.assertEquals(1, controller.getIndividualSpecialties().size());
      System.assertEquals(1, controller.getIndividualClasses().size());
      System.assertEquals(1, controller.getWorkplaceClasses().size());
      System.assertEquals(1, controller.getWorkplaceSpecialties().size());
      System.assertEquals(1, controller.getRoles().size());
      
      repEntersContactName();
      searchTheAccounts();
      selectAnAccount();

      repReceivesCallAndOpensBusinessSearch();
      repEntersBusinessAccountInformation();
      searchTheAccounts();      
      selectTheBusinessAccount();

      System.assert(controller.getRoleAtBusiness().length() > 0);
    }
  }

  static testMethod void testClearSearchResults(){
    interaction = TestDataFactoryMVN.createTestCase();
    Test.setCurrentPage(new PageReference('/apex/PersonSearchMVN?caseId='+interaction.Id));
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new PersonSearchControllerMVN(stdController);

    controller.clearSearchResults();

    System.AssertEquals(false, controller.didSearch);
    System.AssertEquals(0, controller.searchResults.size());
  }

  static testMethod void testExceptions(){
    repReceivesCallandOpensSearch();

    //Try to update when system is read-only to force error
    Test.setReadOnlyApplicationMode(true);
    controller.removeRequester();

    repEntersContactInformation();
    searchTheAccounts();
    controller.actId = controller.searchResults[0].acctId;
    controller.addId = controller.searchResults[0].addrId;
    controller.updateCase();
    System.assert(ApexPages.getMessages().size() > 0, 'Apex Error not caught correctly');
    Test.setReadOnlyApplicationMode(false);

    controller.updateNewPerson();

    //Test no empty record type
    controller.createAccount.RecordTypeId = null;
    controller.enterAccount();
    System.assertEquals(Label.Create_Person_Record_Type_Required, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());    
    
    //Test First name must be provided
    controller.createAccount.FirstName = null;
    controller.createAccount.LastName =  null;
    controller.createAccount.RecordTypeId = controller.hcpRecordTypeId;
    controller.enterAccount();
    System.assertEquals(Label.Specify_First_and_Last_Name, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());
    controller.createAccount.FirstName  = 'Peter';

    //Test Last name must be provided    
    controller.enterAccount();
    System.assertEquals(Label.Specify_First_and_Last_Name, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());    
    controller.createAccount.LastName = 'Employee';
    
    //Test that phone and fax get formatted to just a number string and email must be vlid
    
    controller.createAccount.CRC_Phone_MVN__c  = '555-555-5555';
    controller.createAccount.CRC_Fax_MVN__c  = '(666)-666-1234';
    controller.createAccount.PersonEmail  = 'asfljaksd';
    controller.enterAccount();
    System.assertEquals('5555555555', controller.createAccount.CRC_Phone_MVN__c);
    System.assertEquals('6666661234', controller.createAccount.CRC_Fax_MVN__c);
    System.assertEquals(Label.Invalid_Email_Address, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());    
    
    //Test that Individual class must be provided
    controller.createAccount.PersonEmail = 'test@email.com';
    controller.enterAccount();
    System.assertEquals(Label.Specify_Specialty_and_Class, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());
  
    //test that Specialty_BI__c must be provided
    Integer currentErrorCount = ApexPages.getMessages().size();
    controller.createAccount.OKIndividual_Class_BI__c = TestDataFactoryMVN.createIndividualClass().Id;
    controller.enterAccount();
    System.assertEquals(Label.Specify_Specialty_and_Class, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());
    
    //Test that address name (line 1) must be provided for non-person create
    controller.isPersonSearch = false;
    controller.createAccount.Specialty_BI__c = TestDataFactoryMVN.createIndividualSpecialty().Id;
    controller.createAddress.Name = null;
    controller.createAddress.City_vod__c = null;
    controller.createAddress.OK_State_Province_BI__c = null;
    controller.createAddress.Country_Code_BI__c = null;

    controller.enterAccount();
    System.assertEquals(Label.Fill_Out_Address_Fields, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());

    //test that city must be provided for non-person create
    controller.createAddress.Name = '123 Test St';
    controller.enterAccount();
    System.assertEquals(Label.Fill_Out_Address_Fields, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());    

    //Test that state must be provided for non-person create
    controller.createAddress.City_vod__c = 'Chicago';
    System.assertEquals(Label.Fill_Out_Address_Fields, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());
    
    //Test that a country code must be provided for non-person create
    controller.createAddress.OK_State_Province_BI__c = [select Id from Customer_Attribute_BI__c where Name = 'Test State' limit 1].Id;
    controller.enterAccount();
    System.assertEquals(Label.Fill_Out_Address_Fields, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());

    //Test Business Acount creation exceptions
    //Test that name is required
    controller.createAccount.RecordTypeId = controller.businessRecordTypeId;
    controller.createAccount.Name = null;
    controller.enterAccount();
    System.assertEquals(Label.Specify_Business_Name, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());

    //Test that phone is required for business account
    controller.createAccount.Name = 'Business Name';
    controller.createAccount.CRC_Phone_MVN__c = null;
    controller.enterAccount();
    System.assertEquals(Label.Error_Phone_Required, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());    

    //Test that Workplace Class and specialty are requred for business accounts
    controller.createAccount.CRC_Phone_MVN__c = '5555555555';
    controller.createAccount.OKWorkplace_Class_BI__c = null;
    controller.createAccount.Specialty_BI__c = null;
    controller.enterAccount();

    controller.clearSearch();
    controller.doSearch();
    System.assertEquals(Label.Search_Term_Required, ApexPages.getMessages()[ApexPages.getMessages().size()-1].getSummary());

    //search Term required for person search too
    controller.isPersonSearch = true;
    controller.doSearch();

    //test that getCaseVaules fails gracefully when caseId is bad
    String theCaseId = controller.caseId;
    controller.caseId = '005000000000001';
    currentErrorCount = ApexPages.getMessages().size();
    controller.getCaseValues();
    System.assertEquals(currentErrorCount + 1, ApexPages.getMessages().size());
    controller.caseId = theCaseId;

    //Test that the removeBusiness method fails gracefully
    theCaseId = controller.cs.id;
    controller.cs.id = null;
    currentErrorCount = ApexPages.getMessages().size();
    controller.removeBusiness();
    System.assertEquals(currentErrorCount + 1, ApexPages.getMessages().size());
    controller.cs.id = theCaseId;

    //test that the createChildAccount fails gracefully
    currentErrorCount = ApexPages.getMessages().size();
    controller.createChildAccount();
    System.assertEquals(currentErrorCount + 1, ApexPages.getMessages().size());
  }

  @isTest static void selectedRecordTypeSetsRecordTypeProperly() {
    repReceivesCallandOpensSearch();
    controller.recordTypeText = controller.businessRecordTypeId;
    controller.updateNewPerson();
    System.assertEquals(controller.businessRecordTypeId, controller.createAccount.RecordTypeId);
  }
}