/*
* PersonSearchControllerMVN
* Created By: Kai Amundsen
* Created Date: April 5, 2013
*   Modified By: Siddharth Jain
* Last Modified Date: July 5, 2014
* Description: Mock class used to store search results for person search.
*/

public without sharing class PersonSearchResultMockMVN {
    static Integer MAXSTRINGLENGTH;
    //Account Fields
    public String name {get; set;}
    public String acctId {get; set;}
    public String lastName {get; set;}
    public String firstName {get; set;}
    public String middleName {get; set;}
    public String recordType {get; set;}
    public Boolean validatedAccount {get; set;}
    public String specialtyType {get; set;}
    public String institutionType {get; set;}

    //Address Fields
    public String addrId {get; set;}
    public String addressLine1 {get; set;}
    public String city {get; set;}
    public String state {get; set;}
    public String zipCode {get; set;}
    public String country {get; set;}
    // Include the primary address field 
    public Boolean prim{get;set;}
    private Account theAccount;
    private Address_vod__c theAddress;

    public PersonSearchResultMockMVN() {
        this(new Account(),new Address_vod__c());
    }

    public PersonSearchResultMockMVN(Account acct) {
        this(acct,new Address_vod__c());
    }

    public PersonSearchResultMockMVN(Address_vod__c addr) {
        this(new Account(),addr);
    }

    public PersonSearchResultMockMVN(Account acct, Address_vod__c addr) {
        theAccount = acct;
        theAddress = addr;

        Decimal maxLength = Service_Cloud_Settings_MVN__c.getInstance().Person_Search_Max_String_Length_MVN__c;
        if(maxLength == null || maxLength < 0) {
            MAXSTRINGLENGTH = 30;
        } else {
            MAXSTRINGLENGTH = maxLength.intValue();
        }
        
        acctId = acct.Id;
        name = acct.Name;
        lastName = acct.LastName;
        firstName = acct.FirstName;
        middleName = acct.Middle_vod__c;
        validatedAccount = isValidHCP(acct);
        if(acct.Specialty_BI__c != null && acct.Specialty_BI__r.Name != null){
            if(acct.Specialty_BI__r.Name.length() > MAXSTRINGLENGTH){
                specialtyType = acct.Specialty_BI__r.Name.left(MAXSTRINGLENGTH) + '...';
            } else{
                specialtyType = acct.Specialty_BI__r.Name;
            }
        }

        if(acct.OKWorkplace_Class_BI__c != null && acct.OKWorkplace_Class_BI__r.Name != null){
            if(acct.OKWorkplace_Class_BI__r.Name.length() > MAXSTRINGLENGTH){
                institutionType = acct.OKWorkplace_Class_BI__r.Name.left(MAXSTRINGLENGTH) + '...';
            } else{
                institutionType = acct.OKWorkplace_Class_BI__r.Name;
            }
        }

        if(addr.Account_vod__r.Specialty_BI__c != null && addr.Account_vod__r.Specialty_BI__r.Name != null){
            if(addr.Account_vod__r.Specialty_BI__r.Name.length() > MAXSTRINGLENGTH){
                specialtyType = addr.Account_vod__r.Specialty_BI__r.Name.left(MAXSTRINGLENGTH) + '...';
            } else{
                specialtyType = addr.Account_vod__r.Specialty_BI__r.Name;
            }
        }

        if(addr.Account_vod__r.OKWorkplace_Class_BI__c != null && addr.Account_vod__r.OKWorkplace_Class_BI__r.Name != null){
            if(addr.Account_vod__r.OKWorkplace_Class_BI__r.Name.length() > MAXSTRINGLENGTH){
                institutionType = addr.Account_vod__r.OKWorkplace_Class_BI__r.Name.left(MAXSTRINGLENGTH) + '...';
            } else{
                institutionType = addr.Account_vod__r.OKWorkplace_Class_BI__r.Name;
            }
        }
        
        recordType = acct.RecordTypeId;

        addrId = addr.Id;
        addressLine1 = addr.Name;
        city = addr.City_vod__c;
        System.debug('State: '+addr.OK_State_Province_BI__r.Name);
        state = addr.OK_State_Province_BI__r.Name;
        zipCode = addr.Zip_vod__c;
        country = addr.id != null && addr.Country_Code_BI__c != null ? addr.Country_Code_BI__c : acct.Country_Code_BI__c;
       // To store the value True for Primary Address otherwise False
        prim=addr.Primary_vod__c;
        system.debug('Primary Add field is :'+prim );
    }

    public void setAccount(Account acct) {
        theAccount = acct;

        acctId = acct.Id;
        lastName = acct.LastName;
        firstName = acct.FirstName;
        middleName = acct.Middle_vod__c;
        validatedAccount = isValidHCP(acct);
    }

    public void setAddress(Address_vod__c addr) {
        theAddress = addr;

        addrId = addr.Id;
        addressLine1 = addr.Name;
        city = addr.City_vod__c;
        state = addr.OK_State_Province_BI__r.Name;
        zipCode = addr.Zip_vod__c;
        country = addr.Country_Code_BI__c;
    }

    private Boolean isValidHCP(Account acct) {      
        if(acct.OK_Status_Code_BI__c != null && acct.OK_Status_Code_BI__c != ''){
            Set<String> validCodes = new Set<String>();
            if ( Service_Cloud_Settings_MVN__c.getInstance().HCP_Valid_to_Sample_Status_Codes_MVN__c != null) {
                validCodes.addAll(UtilitiesMVN.splitCommaSeparatedString(Service_Cloud_Settings_MVN__c.getInstance().HCP_Valid_to_Sample_Status_Codes_MVN__c));
            }

            return validCodes.contains(acct.OK_Status_Code_BI__c);
        }
        return false;
    }
}