public class PlanExpenseUpdateTriggerHandler{

    Plan_Expense_BI__c pexp;
    Plan_Expense_BI__c pexpUpdate;
                                                
    public void EventExpenseCreation(list<Event_Expenses_BI__c> evntinst){
   
    list<Plan_Expense_BI__c> planlstInsert =new list<Plan_Expense_BI__c>();
    list<Event_Expenses_BI__c> eventlst=[select id,name,Amount_BI__c,Description_BI__c,Plan__c,CurrencyIsoCode,
                                       Expense_Date_BI__c,Expense_Type_BI__c,Event_Team_Member_BI__r.name from Event_Expenses_BI__c where id in:evntinst];    
   
    for(Event_Expenses_BI__c evntval :eventlst ){
    if(evntval.CurrencyIsoCode=='CAD' || evntval.CurrencyIsoCode== 'USD')
    {
        pexp = new Plan_Expense_BI__c();
        pexp.EventExpense_Amount__c= evntval.Amount_BI__c;
        
        pexp.Description_BI__c = evntval.Description_BI__c;
        pexp.EventTeamMember__c = evntval.Event_Team_Member_BI__r.name;
        pexp.Expense_Date_BI__c = evntval.Expense_Date_BI__c;
        pexp.Expense_Type_BI__c = evntval.Expense_Type_BI__c;
        pexp.Plan_BI__c=evntval.Plan__c;
        pexp.Event_Expense__c = evntval.id;
        system.debug('vales**************'+pexp);
        planlstInsert .add(pexp);
             
    }
    }
    
     if(planlstInsert !=null && planlstInsert .size()>0){
    
     insert planlstInsert ;
    
     }
    system.debug('Plan Expense data'+planlstInsert);
   
   }
   
   //Updation of Event Expense on Plan Expense
   
   public void EventExpenseUpdation(map<id,Event_Expenses_BI__c> evntUpdate){
   
    //map<id,Event_Expenses_BI__c> mapparent= evntUpdate;

    list<Plan_Expense_BI__c> planexplst=[select id,name,EventExpense_Amount__c,Description_BI__c,EventTeamMember__c,Expense_Date_BI__c,Expense_Type_BI__c,Event_Expense__c
                                        from Plan_Expense_BI__c where Event_Expense__c in:evntUpdate.keyset()];

    list<Plan_Expense_BI__c > planlstUpdate = new list<Plan_Expense_BI__c>();

    for(Plan_Expense_BI__c planobj :planexplst){

        Event_Expenses_BI__c eventref=evntUpdate.get(planobj.Event_Expense__c);

        planobj.EventExpense_Amount__c= eventref.Amount_BI__c;
        
        planobj.Description_BI__c = eventref.Description_BI__c;

        planobj.EventTeamMember__c = eventref.Event_Team_Member_BI__r.name;

        planobj.Expense_Date_BI__c = eventref.Expense_Date_BI__c;

        planobj.Expense_Type_BI__c = eventref.Expense_Type_BI__c;

        planobj.Event_Expense__c = eventref.id;

        system.debug('vales**************'+planobj);

        planlstUpdate .add(planobj);

     }
    // system.debug(');
        
     if(planlstUpdate !=null && planlstUpdate .size()>0){
    
     Update planlstUpdate ;
    
     }
   
   
   }
   
   // Deletion of Event Expense on Plan Expense
   
    public void EventExpenseDeletion(map<id,Event_Expenses_BI__c> evntDelete){
   
      system.debug('plan expense old records******'+evntDelete); 
      system.debug('evntDelete.keyset()'+evntDelete.keyset());//parent record
     // list<Event_Expenses_BI__c> eventExpDellst=[select id,name from Event_Expenses_BI__c where id in:evntDelete.keyset()];
      list<Plan_Expense_BI__c> planexpDellst=[select id,name,Event_Expense__c from Plan_Expense_BI__c where Event_Expense__c in:evntDelete.keyset()];
      
      system.debug('plan expense records******'+planexpDellst);  
            
     if(planexpDellst!=null && planexpDellst.size()>0){
    
        Delete planexpDellst;
    
     }
     
     system.debug('Deleted plan expense records'+planexpDellst);
   
   
    }
}