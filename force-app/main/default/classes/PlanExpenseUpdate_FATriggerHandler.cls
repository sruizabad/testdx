public with sharing class PlanExpenseUpdate_FATriggerHandler{


  public void FixedAllocationCreation(list<Fixed_Allocation_BI__c> FAInsert){
                
     System.debug('****Fixed allocation Inserted records****'+FAInsert); 
      
     Plan_Expense_BI__c FAPexp;//storing positive value

     Plan_Expense_BI__c FANexp;//storing Negative Value

     list<Plan_Expense_BI__c> PlanExpinsert1= new list<Plan_Expense_BI__c>();//positive list

     list<Plan_Expense_BI__c> PlanExpinsert2= new list<Plan_Expense_BI__c>();//negative list

     list<Fixed_Allocation_BI__c > FAlist=[select id,name,Allocated_Amount_BI__c,Fixed_Allocation_Plan_BI__c,Allocation_Date_BI__c,

                                         Plan_BI__c,Testdata__c,PlanDetails__c,Plan_BI__r.Plan_Total_Amount_BI__c,Event_Team_Member_BI__r.name from Fixed_Allocation_BI__c where id in:FAInsert];  
                                         
     System.debug('****Fixed allocation Details****'+FAlist);                                   

     for(Fixed_Allocation_BI__c FAVal : FAlist){

       FAPexp=new Plan_Expense_BI__c();

       FAPexp.FA_Amount__c=FAVal.Allocated_Amount_BI__c;

       FAPexp.Expense_Type_BI__c=FAVal.Fixed_Allocation_Plan_BI__c;

       FAPexp.Expense_Date_BI__c=FAVal.Allocation_Date_BI__c;

       FAPexp.EventTeamMember__c=FAVal.Event_Team_Member_BI__r.name;

       FAPexp.Plan_BI__c=FAVal.Plan_BI__c;
       
       FAPexp.Fixed_Allocation__c=FAVal.id;
       if(FAVal.Plan_BI__c!=null)
       PlanExpinsert1.add(FAPexp);
       
       //System.debug('****PlanExpense  Positive Details****'+PlanExpinsert1);

          //Negative value insert

       FANexp=new Plan_Expense_BI__c();

       FANexp.FA_Amount__c=FAVal.Testdata__c;//Testdata--formula field to get negative value

       FANexp.Expense_Type_BI__c=FAVal.Fixed_Allocation_Plan_BI__c;

       FANexp.Expense_Date_BI__c=FAVal.Allocation_Date_BI__c;

       FANexp.EventTeamMember__c=FAVal.Event_Team_Member_BI__r.name;
       
       FANexp.Fixed_Allocation__c=FAVal.id;
       
       System.debug('****PlanExpense  Negative Details****'+FAVal.Plan_BI__r.Plan_Total_Amount_BI__c);
       
       System.debug('****PlanExpense  Negative Details****'+FAVal.Testdata__c);

       FANexp.Plan_BI__c=FAVal.PlanDetails__c; //planDetails__c --formula field to get plan used in the event
       if(FAVal.Plan_BI__c != null)
       PlanExpinsert2.add(FANexp);

       //System.debug('****PlanExpense  Negative Details****'+PlanExpinsert2);

     }
System.debug('****PlanExpense  Positive Details****'+PlanExpinsert1);
System.debug('****PlanExpense  Negative Details****'+PlanExpinsert2);
      //Positive insert

       if(PlanExpinsert1!=null && PlanExpinsert1.size()>0){
       System.debug('****PlanExpinsert1****'+PlanExpinsert1);
    
       insert PlanExpinsert1;
       System.debug('****PlanExpinsert1id****'+PlanExpinsert1[0].id);
       }

      //Negative Insert;

       if(PlanExpinsert2!=null && PlanExpinsert2.size()>0){
       System.debug('****PlanExpinsert2****'+PlanExpinsert2);
    
       insert PlanExpinsert2;
       System.debug('****PlanExpinsert2id****'+PlanExpinsert2[0].id);
    
       }
    }
    
    
  public void FixedAllocationUpdation(map<ID,Fixed_Allocation_BI__c> FAUpdate){
     
     list<Plan_Expense_BI__c> planexplst=[select id,name,FA_Amount__c,EventTeamMember__c,Expense_Date_BI__c,Expense_Type_BI__c,Fixed_Allocation__c
                                        from Plan_Expense_BI__c where Fixed_Allocation__c in:FAUpdate.keyset()];

   list<Plan_Expense_BI__c > planlstPUpdate = new list<Plan_Expense_BI__c>();

     //list<Plan_Expense_BI__c > planlstNUpdate = new list<Plan_Expense_BI__c>();


     for(Plan_Expense_BI__c planobj :planexplst){

     Fixed_Allocation_BI__c FAref=FAUpdate.get(planobj.Fixed_Allocation__c);
     
     if(planobj.FA_Amount__c!=null && planobj.FA_Amount__c>0){
     
     planobj.FA_Amount__c= FAref.Allocated_Amount_BI__c;
     planobj.Plan_BI__c=FAref.Plan_BI__c;
     
     }
     
     else{
     
     planobj.FA_Amount__c= FAref.Testdata__c;
     planobj.Plan_BI__c=FAref.PlanDetails__c;
     
     }
     

    // planobj.Expense_Date_BI__c= FAref.Allocation_Date_BI__c;

     planobj.Expense_Type_BI__c= FAref.Fixed_Allocation_Plan_BI__c;

     planobj.EventTeamMember__c= FAref.Event_Team_Member_BI__r.name;

     planobj.Fixed_Allocation__c = FAref.id;

     planlstPUpdate.add(planobj);
   
    
     }

       //Positive Update

       if(planlstPUpdate!=null && planlstPUpdate.size()>0){
       System.debug('****planlstPUpdate****'+planlstPUpdate);
    
       Update planlstPUpdate;
       System.debug('****planlstPUpdateid****'+planlstPUpdate[0].id);
       }

      
    
   }
   
  public void FixedAllocationDeletion(map<id,Fixed_Allocation_BI__c> FADelete){
     
     system.debug('plan expense old records******'+FADelete); 
     
     system.debug('evntDelete.keyset()'+FADelete.keyset());//parent record
    
     list<Plan_Expense_BI__c> planexpDellst=[select id,name,Fixed_Allocation__c from Plan_Expense_BI__c
                                             where Fixed_Allocation__c in:FADelete.keyset()];
      
     system.debug('plan expense records******'+planexpDellst);  
            
     if(planexpDellst!=null && planexpDellst.size()>0){
    
        Delete planexpDellst;
    
     }
     
     system.debug('Deleted plan expense records'+planexpDellst);
    
   } 
    
 
  
}