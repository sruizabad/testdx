public class Plan_BIHandler {
    
    public void onBeforeInsert(List<Plan_BI__c> lstNewBudgetStage) {
        Set<Id> setNewInsertedPlanId = new Set<Id>();
        for(Plan_BI__c objPlan : lstNewBudgetStage) {
            setNewInsertedPlanId.add(objPlan.Id);
        }
        List<Plan_BI__c> plans = [select Id, Actuals_Total_Amount_BI__c, Temp_Amount__c,Parent_Plan_BI__c , (select Id, Actuals_Total_Amount_BI__c,Temp_Amount__c from Sub_Plans__r) from Plan_BI__c where Parent_Plan_BI__c in :setNewInsertedPlanId];
        //Code to assign Rollup value from Plan Expense to Actual Total Amount Field
        for(Plan_BI__c objPlan : lstNewBudgetStage) {
            if(objPlan.Type_BI__c == 'position' ){
                objPlan.Temp_Amount__c=objPlan.AmountSpent__c;
                objPlan.Actuals_Total_Amount_BI__c=objPlan.Temp_Amount__c;
            }
            
        }
        //Code to Rollup Total Actual Amount on Region Plan Type from Child Plan and Plan Expense
        double totalAmt = 0;
        for(Plan_BI__c objPlan : lstNewBudgetStage){
            if(plans.size() != 0 && objPlan.Type_BI__c == 'Region' ){                            
                for(Plan_BI__c Plan : plans ) {
                    totalAmt += Plan.Actuals_Total_Amount_BI__c ;
                    objPlan.Temp_Amount__c=objPlan.AmountSpent__c + totalAmt  ;
                    objPlan.Actuals_Total_Amount_BI__c=objPlan.Temp_Amount__c;
                }
            }
            else if(plans.size() == 0 && objPlan.Type_BI__c == 'Region' ){
                objPlan.Temp_Amount__c=objPlan.AmountSpent__c  ;
                objPlan.Actuals_Total_Amount_BI__c=objPlan.Temp_Amount__c;
            }
        }
        
        //Code to Assign Plan Total Amount to Unallocated Amount for Position Type Plan
        List<Plan_BI__c> lstChildPlan = [SELECT Id, Name,Parent_Plan_BI__c FROM Plan_BI__c where Parent_Plan_BI__c =: setNewInsertedPlanId ];
        
        for(Plan_BI__c objPlan : lstNewBudgetStage) {
            if(lstChildPlan == null || lstChildPlan.size() == 0 || objPlan.id == null){
                objPlan.Unallocated_Amount_BI__c = objPlan.Plan_Total_Amount_BI__c ;
                
            }
        }
    }   
    
    //Method to rollup the Actual Total Amount of all parent Plan
    public void onAfterInsert(List<Plan_BI__c> lstNewBudgetStage) {
       
        List<Plan_BI__c> lstPlanToUpdate = new List<Plan_BI__c>();
        Map<Id,Plan_BI__c> mapPlanIdToPlan = new Map<Id,Plan_BI__c>();
        Set<Id> setNewInsertedPlanId = new Set<Id>();
        
        for(Plan_BI__c objPlan : lstNewBudgetStage) {
            
            if(objPlan.Parent_Plan_BI__c != null)
            {                 
                mapPlanIdToPlan.put(objPlan.Parent_Plan_BI__c, objPlan);
               setNewInsertedPlanId.add(objPlan.Parent_Plan_BI__c);  
                               
          }    
            
            
        }
        
        map<id,Decimal> mapToUpdateplan = new map<id,Decimal>();
        map<id,Double> mapToUpdateunallocatedPlan = new map<id,Double>();
        
        List<Plan_BI__c> plans = [select Id, Plan_Total_Amount_BI__c, Unallocated_Amount_BI__c, (select Id, Plan_Total_Amount_BI__c,Temp_Amount__c from Sub_Plans__r) from Plan_BI__c where Id in :setNewInsertedPlanId];
        for(Plan_BI__c parentPlan:plans){
            Decimal total = 0;
            Double unallocatedTotal = 0;
          //  unallocatedTotal = parentPlan.Unallocated_Amount_BI__c+parentPlan.Plan_Total_Amount_BI__c;
            
          for(Plan_BI__c childPlan:parentPlan.Sub_Plans__r){
                if(childPlan.Temp_Amount__c != null){
                    total += childPlan.Temp_Amount__c;            
               }
              //  if(childPlan.Plan_Total_Amount_BI__c != null ){
              //      unallocatedTotal += childPlan.Plan_Total_Amount_BI__c;  
               // }
            } 
            mapToUpdateplan.put(parentPlan.id,total);
            mapToUpdateunallocatedPlan.put(parentPlan.id,unallocatedTotal );
        }
         for(Plan_BI__c objPlan : [SELECT Id, HO_Amount__c, FixedAllocationAmount__c, EventExpenseAmount__c, Temp_Amount__c, Unallocated_Amount_BI__c,
                                  AmountSpent__c,Plan_Total_Amount_BI__c , Parent_Plan_BI__c, Type_BI__c 
                                  FROM Plan_BI__c 
                                  WHERE Id IN: setNewInsertedPlanId]) {
                                      
                                      Double tempAmt = 0;
                                      Double amount =0;
                                      Double planAmt =0;
                                    //  Double unallocatedAmt = 0;
                                      amount = objPlan.HO_Amount__c + objPlan.FixedAllocationAmount__c + objPlan.EventExpenseAmount__c; 
                                      tempAmt = mapPlanIdToPlan!=null && mapPlanIdToPlan.containsKey(objPlan.Id) ? mapPlanIdToPlan.get(objPlan.Id).Temp_Amount__c : 0;
                                      planAmt = mapToUpdateplan!=null && mapToUpdateplan.containsKey(objPlan.Id) ? mapToUpdateplan.get(objPlan.Id) : 0;
                                    //  unallocatedAmt = mapToUpdateunallocatedPlan!=null && mapToUpdateunallocatedPlan.containsKey(objPlan.Id) ? mapToUpdateunallocatedPlan.get(objPlan.Id) : 0; 
                                      
                                      System.debug('===========objPlan=='+objPlan);
                                     System.debug('===========amount=='+amount);
                                      System.debug('===========planAmt=='+planAmt);
                                      //System.debug('===========unallocatedAmt =='+unallocatedAmt );
                                      if(amount == null)
                                          amount=0;
                                     if(tempAmt == null)
                                         tempAmt=0;
                                      if(planAmt == null)
                                          planAmt=0;    
                                   //   if(unallocatedAmt== null)
                                       //   unallocatedAmt=0;  
                                          
                                     // objPlan.Unallocated_Amount_BI__c = objPlan.Plan_Total_Amount_BI__c - unallocatedAmt;
                                      objPlan.Temp_Amount__c =  amount +planAmt;
                                     system.debug('test1');
                                      objPlan.Actuals_Total_Amount_BI__c= objPlan.Temp_Amount__c;
                                      //objplan.Plan_Total_Amount_BI__c=objPlan.Unallocated_Amount_BI__c+planAmt;
               
                                      lstPlanToUpdate.add(objPlan);
                                      }
        if(!lstPlanToUpdate.isEmpty())    
        {
            system.debug('update');
            update lstPlanToUpdate;
        }
        
    }   
    
}