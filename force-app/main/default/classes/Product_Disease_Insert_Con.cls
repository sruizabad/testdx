public with sharing class Product_Disease_Insert_Con {
    // All variables
    String caseId;
    Boolean selectCheck{get; set;}
    Boolean selectCheck1{get; set;}
    public  Product_Diseases_CAP__c pro{get; set;}
    public String newPro{get; set;}
    public String newDis{get;set;}
    public List<String> SelectedProducts{get;set;}
    public List<String> SelectedNewDiseases{get;set;}
    public List<String> SelectedDiseases{get; set;}
    public List<String> SelectedCompetitorProducts{get; set;}
    private PageReference pageRef = ApexPages.currentPage();
    //constructor
    public Product_Disease_Insert_Con(ApexPages.StandardController controller) {
    pro=new Product_Diseases_CAP__c();
    caseId=ApexPages.currentPage().getParameters().get('CaseId');
    }
    //NewProduct Checkbox function
    public void setselectCheck1(Boolean selectCheck1){
    this.selectCheck1=selectCheck1;
    }
    public Boolean getselectCheck1(){
    return selectCheck1;
    }
    public void setselectCheck(Boolean selectCheck){
    this.selectCheck=selectCheck;
    }
    
    public Boolean getselectCheck(){
    return selectCheck;
    }
        
    public Product_Disease_Insert_Con(){
        
    }
    //PRODUCTS
    public List<SelectOption> getProducts() {
        List<SelectOption> Options = new List<SelectOption>();
        User u=[SELECT  Country_Code_BI__c from user where id =: UserInfo.getUserId()];
        String prodtype='Detail';
        List<Product_vod__c> ple=[SELECT Name from Product_vod__c where Country_Code_BI__c like :u.Country_Code_BI__c and Product_Type_vod__c = :prodtype];
        System.debug('**'+u.Country_Code_BI__c+'##'+ple.size());
        ple.sort();if(ple.size()>0){
        for(Product_vod__c f : ple){
       
          options.add(new SelectOption(f.name, f.name));
        } } else Options.add(new SelectOption('none','--None--'));
       return Options;
    }
    // DISEASES
    public List<SelectOption> getDiseases() {
        List<SelectOption> Options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult =Call2_Discussion_vod__c.Disease_State_BI__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple){
       
          options.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
       return Options;
    }
    //New Diseases
    public List<SelectOption> getNewDiseases() {
        List<SelectOption> Options = new List<SelectOption>();
        List<Admin_Diseases__c> ple=[SELECT Diseases__c from Admin_Diseases__c];
        ple.sort();if(ple.size()>0){
        for(Admin_Diseases__c f : ple){
       
          if((f.Diseases__c!=null)&(f.Diseases__c!=''))Options.add(new SelectOption(f.Diseases__c, f.Diseases__c));
          else {Options.add(new SelectOption('none','--None--'));  break;}
        } } else Options.add(new SelectOption('none','--None--'));
       return Options;
    }
    
    //Competitor Products
    public List<SelectOption> getCompetitorProducts() {
        List<SelectOption> Options = new List<SelectOption>();
        List<Admin_Diseases__c> ple=[SELECT Product__c from Admin_Diseases__c];
        ple.sort();
        if(ple.size()>0){
        for(Admin_Diseases__c f : ple){
       
          if((f.Product__c!=null)&(f.Product__c!=''))Options.add(new SelectOption(f.Product__c, f.Product__c));
          else {Options.add(new SelectOption('none','--None--'));  break;}
        } } else Options.add(new SelectOption('none','--None--'));
       return Options;
    }
    
    //SAVE BUTTON CALLS THIS METHOD
    public PageReference done(){
        String prod,dis;
        String str='https://'+ApexPages.currentPage().getHeaders().get('X-Salesforce-Forwarded-To')+'/';
        String pageurl=str+caseId;
        PageReference page=new PageReference(pageurl);
        
        if(SelectedProducts != null){    
        for(String s:SelectedProducts){
        if(prod!=null) prod=prod+s+';'; else prod=s+';';        
        }
        }
        
        if(SelectedCompetitorProducts != null){    
        for(String s:SelectedCompetitorProducts){
        if(prod!=null) prod=prod+s+';'; else prod=s+';';       
        }
        }
        
        if(SelectedDiseases != null){    
        for(String s:SelectedDiseases){
        if(dis!=null) dis=dis+s+';'; else dis=s+';';        
        }
        }
        
        if(SelectedNewDiseases != null){    
        for(String s:SelectedNewDiseases){
        if(dis!=null) dis=dis+s+';'; else dis=s+';';        
        }
        }
           
        Product_Diseases_CAP__c pd=new Product_Diseases_CAP__c();
        pd.Case_CAP__c=caseId;
        pd.Product_CAP__c=prod;
        pd.Diseases_CAP__c=dis;
        pd.New_Product__c=newPro;
        pd.New_Disease__c=newDis;
        pd.Product_Diseases__c=pd.Product_CAP__c+';##'+pd.Diseases_CAP__c;
        pd.New_Product_CAP__c=selectCheck;
        pd.New_Disease_CAP__c=selectCheck1;
                
          if(((pd.Product_CAP__c!=null)&(pd.Diseases_CAP__c!=null)))insert pd;
        else if(((pd.Product_CAP__c==null)|(pd.Product_CAP__c=='None')) & ((pd.Diseases_CAP__c==null)|(pd.Diseases_CAP__c=='None'))){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Select_Product_Disease_CAP));
        page=pageRef;
        } else if((pd.Product_CAP__c==null)|(pd.Product_CAP__c=='None')){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Select_Product_CAP));
        page=pageRef;
        }else if((pd.Diseases_CAP__c==null)|(pd.Diseases_CAP__c=='None')){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Select_Disease_CAP));
        page=pageRef;
        }  
                       
        return page;
    }    
}