global without sharing class RK_FIX_PREFERRED implements Database.Batchable<SObject>
{

    global Database.QueryLocator start(Database.BatchableContext BC) { 
        // Create base query
        String selStmt = 'Select Id, bi_preferred_address_bi__c From Address_vod__c where ok_external_id_bi__c != null and primary_vod__c = true and bi_preferred_address_bi__c = false limit 500000';
        system.debug('Batch select statement: ' + selStmt);
        return Database.getQueryLocator(selStmt);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> batch) {
        List<Address_vod__c> addresses = (List<Address_vod__c>) batch;
        for (Integer i = 0; i < addresses.size(); i++) addresses[i].bi_preferred_address_bi__c = true;
        
        update addresses;
    }
    
    global void finish(Database.BatchableContext BC) {    
    }
    
}