global without sharing class RK_REMOVE_BAD_CHILD_ACCOUNT implements Database.Batchable<SObject>
{

    global Database.QueryLocator start(Database.BatchableContext BC) { 
        // Create base query
        String selStmt = 'SELECT ID FROM Child_Account_vod__c where External_ID2__c = null and Child_Account_vod__r.OK_External_ID_BI__c like \'WCA%\' AND Parent_Account_vod__r.OK_External_ID_BI__c like \'WCA%\' and CreatedBy.Name = \'VDC Integration\' limit 500000';
        system.debug('Batch select statement: ' + selStmt);
        return Database.getQueryLocator(selStmt);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> batch) {
        List<Child_Account_vod__c> child_accounts = (List<Child_Account_vod__c>) batch;
        delete child_accounts;
    }
    
    global void finish(Database.BatchableContext BC) {
    
    }
    
}