/*
* RecordTypeControllerTestMVN
* Created By: Roman Lerman
* Created Date: April 5, 2013
* Description: This is the test class for all of the classes that end in "RecordTypeControllerMVN."
*			   These classes display the record type name for Accounts, Cases, and Fulfillments.
*/
@isTest
private class RecordTypeControllerTestMVN {
	static Account acct;
	static Case cs;
	static Fulfillment_MVN__c fulfillment;
	
	static{
		TestDataFactoryMVN.createSettings();
		
		acct = TestDataFactoryMVN.createTestConsumer();
		
		cs = TestDataFactoryMVN.createTestCase();
		
		fulfillment = TestDataFactoryMVN.createTestFulfillment(cs.AccountId, cs.Id);
	}
	static testMethod void testAccountRecordTypeController(){
		ApexPages.StandardController con = new ApexPages.StandardController(acct);
    	AccountRecordTypeControllerMVN extension = new AccountRecordTypeControllerMVN(con);
    	
    	System.assertEquals([select toLabel(RecordType.Name) from Account where Id = :acct.Id].RecordType.Name,
    						extension.recordTypeName);	
	}
	static testMethod void testCaseRecordTypeController(){	
		ApexPages.StandardController con = new ApexPages.StandardController(cs);
    	CaseRecordTypeControllerMVN extension = new CaseRecordTypeControllerMVN(con);
    	
    	System.assertEquals([select toLabel(RecordType.Name) from Case where Id = :cs.Id].RecordType.Name,
    						extension.recordTypeName);
	}
	static testMethod void testFulfillmentRecordTypeController(){		
		ApexPages.StandardController con = new ApexPages.StandardController(fulfillment);
    	FulfillmentRecordTypeControllerMVN extension = new FulfillmentRecordTypeControllerMVN(con);
    	
    	System.assertEquals([select toLabel(RecordType.Name) from Fulfillment_MVN__c where Id = :fulfillment.Id].RecordType.Name,
    						extension.recordTypeName);
	}
}