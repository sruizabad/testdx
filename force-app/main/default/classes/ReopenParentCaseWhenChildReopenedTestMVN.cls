/*
* ReopenParentCaseWhenChildReopenedTestMVN
* Created By:    Kai Amundsen
* Created Date:  6/23/2013
* Modified By: Pranita Bolisetty
* Modification Date: 30/07/2018
* Description:   This is the test class for ReopenParentCaseWhenChildReopenedMVN
*/
@isTest
private class ReopenParentCaseWhenChildReopenedTestMVN {
	
	static Case parent1;
	static Case child1;
	static Case child2;

	static Case parent2;
	static Case child3;
	static Case child4;

	static List<Case> casesList;

	static {
		TestDataFactoryMVN.createSettings();

		parent1 = TestDataFactoryMVN.createClosedTestCase();
		child1 = TestDataFactoryMVN.createClosedTestRequest(parent1);
		child2 = TestDataFactoryMVN.createClosedTestRequest(parent1);

		parent2 = TestDataFactoryMVN.createClosedTestCase();
		child3 = TestDataFactoryMVN.createClosedTestRequest(parent2);
		child4 = TestDataFactoryMVN.createClosedTestRequest(parent2);

		casesList = new List<Case> {parent1, parent2, child1, child2, child3, child4};
	}

	static void updateCases() {
		For (Case thisCase : [SELECT Id, Status, RecordType.Id FROM Case WHERE Id in :casesList]) {
			if (thisCase.Id == parent1.Id) {
				parent1 = thisCase;
			}
			else if (thisCase.Id == parent2.Id) {
				parent2 = thisCase;
			}
			else if (thisCase.Id == child1.Id) {
				child1 = thisCase;
			}
			else if (thisCase.Id == child2.Id) {
				child2 = thisCase;
			}
			else if (thisCase.Id == child3.Id) {
				child3 = thisCase;
			}
			else if (thisCase.Id == child4.Id) {
				child4 = thisCase;
			}
		}
	}

	@isTest static void verifyParentOneReopened() {
		for (Case thisCase : casesList) {
			System.AssertEquals('Closed', thisCase.Status);
		}

		Test.StartTest();

		child1.Status = 'Open';
		update child1;

		Test.StopTest();

		updateCases();

		System.AssertEquals('Open', parent1.Status);
		System.AssertEquals(TestDataFactoryMVN.interactionRecordTypeId, parent1.RecordTypeId);
		System.AssertEquals('Open', child1.Status);
		System.AssertEquals('Closed', child2.Status);

		System.AssertEquals('Closed', parent2.Status);
		System.AssertEquals('Closed', child3.Status);
		System.AssertEquals('Closed', child4.Status);
	}
	
}