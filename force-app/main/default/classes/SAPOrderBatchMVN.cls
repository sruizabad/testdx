/*
  * OrderUtilityMVN
  *    Created By:     Kai Amundsen   
  *    Created Date:    September 18, 2013
  *    Description:     Batch job to submit orders to SAP using a single callout per order
 */

global class SAPOrderBatchMVN implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

	public List<OrderUtilityMVN.OrderError> batchErrors;
	public Integer totalCalls;
	public Integer failedCalls;

	global final Id campaignId;
	global final Set<Id> callIds;
	global final String query;
	global final OrderUtilityMVN ou;
	
	global SAPOrderBatchMVN(Set<Id> orderIds,Id campaign) {
		campaignId = campaign;
		callIds = orderIds;
		ou = new OrderUtilityMVN();
		query = 'select ' + OrderUtilityMVN.callQueryFields + ', (select ' + OrderUtilityMVN.callSampleQueryFields + ' from Call2_Sample_vod__r) from Call2_vod__c where Id in :callIds';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		batchErrors = new List<OrderUtilityMVN.OrderError>();
		totalCalls = 0;
		failedCalls = 0;
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		List<Call2_vod__c> batchCalls = (List<Call2_vod__c>) scope;

   		for(Call2_vod__c eachCall : batchCalls) {
   			totalCalls++;
   			List<OrderUtilityMVN.OrderError> callErrors = new List<OrderUtilityMVN.OrderError>();

   			callErrors.addAll(ou.submitFullOrders(new List<Call2_vod__c>{eachCall}));

   			if(!callErrors.isEmpty()) {
   				failedCalls++;
   				for(OrderUtilityMVN.OrderError oe : callErrors) {
   					if(String.isBlank(eachCall.SAP_Errors_MVN__c)) {
   						eachCall.SAP_Errors_MVN__c = oe.errorMessage;
   					} else {
   						eachCall.SAP_Errors_MVN__c += ' \n' + oe.errorMessage;
   					}
   					eachCall.Override_Lock_vod__c = true;
   					eachCall.Status_vod__c = Service_Cloud_Settings_MVN__c.getInstance().Call_Saved_Status_MVN__c;
   					eachCall.Delivery_Status_MVN__c = Service_Cloud_Settings_MVN__c.getInstance().Call_Order_Delivery_Status_New_MVN__c;
   				}
   				try {
   					update eachCall;
				} catch (Exception e) {
					OrderUtilityMVN.OrderError oe = new OrderUtilityMVN.OrderError();
					oe.errorException = e;
					oe.errorOrder = eachCall.Id;
					oe.type = OrderUtilityMVN.ErrorType.ADMIN;
					oe.errorMessage = e.getMessage() + '\n Adding errors to call: ' + eachCall.Id + '\n Call Error: ' + eachCall.SAP_Errors_MVN__c;
					batchErrors.add(oe);
				}
   				
   			}

			batchErrors.addAll(callErrors);	
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id = :BC.getJobId()];
		
		Integer successfulCalls = totalCalls - failedCalls;
		Campaign_vod__c massOrderCampaign = [select Id,SAP_Batch_Status_MVN__c,Name from Campaign_vod__c where Id = :campaignId];

		String resultMessage = String.format(System.Label.SAP_Mass_Order_Status_Message, new string[] {successfulCalls.format(),totalCalls.format()});

		// Update campaign if needed
		String campaignError = '';
		massOrderCampaign.SAP_Batch_Status_MVN__c = resultMessage;
		try {
			update massOrderCampaign;
		} catch (Exception e) {
			campaignError = e.getMessage();
		}



		//Send email to admin if there are failures
		if (!batchErrors.isEmpty() || campaignError != '') {
			String plainTextBody = '';
            
            if(!batchErrors.isEmpty()) {
            	Boolean firstError = true;
	            for (OrderUtilityMVN.OrderError oe : batchErrors) {
	            	if(oe.type == OrderUtilityMVN.ErrorType.ADMIN) {
	            		if(firstError) {
	            			plainTextBody += '\nThe following Orders could not placed to SAP:\n';
	            			firstError = false;
	            		}
		            	if(oe.errorOrder != null) {
		            		plainTextBody += 'Call Id: ' + oe.errorOrder + ' ---- Reason: ' + oe.errorMessage + '\n';
		        		} else {
		        			plainTextBody += 'General Error: ' + oe.errorMessage + '\n';
		        		}
		        	}
	            }
            }

            if(plainTextBody != '' || campaignError != '') {
	            Messaging.reserveSingleEmailCapacity(1);
	            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

	            String adminEmailAddress = Service_Cloud_Settings_MVN__c.getInstance().Administrator_Email_MVN__c;
	            mail.setToAddresses(new String[] {adminEmailAddress});
	            mail.setSenderDisplayName('SAP Batch Order Error');
	            mail.setSubject('Error(s) in the SAP Batch Order Job');
	            mail.setBccSender(false);
	            mail.setUseSignature(false);

	            if(campaignError != '') {
	            	plainTextBody += 'Error: Saving status to campaign: ' + massOrderCampaign.Name + ' ---- Reason: ' + campaignError + '\n';
	            }
	            
	            mail.setPlainTextBody(plainTextBody);
	            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	        }
        } 
    }
}