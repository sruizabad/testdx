/*
  * SAPOrderBatchTestMVN
  *    Created By:     Kai Amundsen   
  *    Created Date:    October 2, 2013
  *    Description:     Unit tests for SAPOrderBatchMVN
 */
@isTest
private class SAPOrderBatchTestMVN {

	static Account act;
	static Product_vod__c product;
	static Campaign_vod__c campaign;
	static User adminUser;

    static {
      Profile p = [select Id from Profile where Name='CRC - Customer Request Agent']; 
      UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Germany'];
      
      adminUser = new User(alias='ccusysad', email= 'callcentertestusermvn@callcenter.com', External_ID_BI__c='123456789', Country_Code_BI__c = 'DE',
                      emailencodingkey='UTF-8', firstName='Reginald', lastname='Wellington', languagelocalekey='en_US', 
                      localesidkey='en_US', profileid = [select Id from Profile where Name = 'System Administrator'].Id, 
                      isActive = true, timezonesidkey='America/Los_Angeles', username='callcentertestusermvn@callcenter.com',
                      Default_Article_Language_MVN__c = KnowledgeArticleVersion.Language.getDescribe().getPicklistValues()[0].getLabel());

      System.runAs(adminUser){
        TestDataFactoryMVN.createSettings();
        act = TestDataFactoryMVN.createTestHCP();
		act.OwnerId = adminUser.Id;
		update act;

		act = [select Id,OK_Status_Code_BI__c,IsPersonAccount from Account where Id = :act.Id];

		product = TestDataFactoryMVN.createTestProduct('DE');
		product.OwnerID = adminUser.Id;
		update product;

		//create campaign
		campaign = new Campaign_vod__c();
		campaign.OwnerID = adminUser.Id;
		insert campaign;
      }
  	}
	
	@isTest static void test_simple_batch() {
		List<OrderUtilityMVN.OrderError> errors;

		OrderUtilityMVN.userCountryCode = 'DE';
		OrderUtilityMVN ou = new OrderUtilityMVN();
		OrderUtilityMVN.SAPOrder = true;
		ou.campaignId = campaign.Id;
		
		Map<Id,Call2_vod__c> callList;
		Database.BatchableContext bc;

		callList = new Map<Id,Call2_vod__c>(createCallList(1));
		SAPOrderBatchMVN orderProcessor = new SAPOrderBatchMVN(callList.keySet(),campaign.Id);
		orderProcessor.start(bc);
		Test.startTest();
		Test.setMock(WebServiceMock.class, new OrderResponseSuccessMockMVN());
		orderProcessor.execute(bc, (List<sObject>) callList.values());
		//ID batchprocessid = Database.executeBatch(orderProcessor,1);
		Test.stopTest();
		//orderProcessor.finish(bc);

		checkCallsSentSAP();
	}

	@isTest static void test_batch_with_errors() {
		List<OrderUtilityMVN.OrderError> errors;

		OrderUtilityMVN.userCountryCode = 'DE';
		OrderUtilityMVN ou = new OrderUtilityMVN();
		OrderUtilityMVN.SAPOrder = true;
		ou.campaignId = campaign.Id;

		
		Map<Id,Call2_vod__c> callList;
		Database.BatchableContext bc;

		callList = new Map<Id,Call2_vod__c>(createCallList(1));
		SAPOrderBatchMVN orderProcessor = new SAPOrderBatchMVN(callList.keySet(),campaign.Id);
		orderProcessor.start(bc);
		Test.startTest();
		Test.setMock(WebServiceMock.class, new OrderResponseFailMockMVN());
		orderProcessor.execute(bc, (List<sObject>) callList.values());
		Test.stopTest();
		
	}

	@isTest static void test_full_batch() {
		List<OrderUtilityMVN.OrderError> errors;

		OrderUtilityMVN.userCountryCode = 'DE';
		OrderUtilityMVN ou = new OrderUtilityMVN();
		OrderUtilityMVN.SAPOrder = true;
		ou.campaignId = campaign.Id;

		
		Map<Id,Call2_vod__c> callList;
		Database.BatchableContext bc;

		callList = new Map<Id,Call2_vod__c>(createCallList(1));
		SAPOrderBatchMVN orderProcessor = new SAPOrderBatchMVN(callList.keySet(),campaign.Id);
		orderProcessor.start(bc);
		Test.setMock(WebServiceMock.class, new OrderResponseFailMockMVN());
		ID batchprocessid = Database.executeBatch(orderProcessor,1);
		
		
	}

	private static void checkCallsSentSAP (){
		String callQuery = 'select ' + OrderUtilityMVN.callQueryFields + ', (select ' + OrderUtilityMVN.callSampleQueryFields + ' from Call2_Sample_vod__r) from Call2_vod__c';
		List<Call2_vod__c> resultingCalls = (List<Call2_vod__c>)Database.query(callQuery);

		for(Call2_vod__c call : resultingCalls) {
			System.assertEquals('Submitted_vod',call.Status_vod__c);
			System.assertEquals('Submitted',call.Delivery_Status_MVN__c);
			System.assertEquals('DESAP'+call.Id,call.External_ID__c);

			for(Call2_Sample_vod__c callSample : call.Call2_Sample_vod__r) {
				System.assertEquals(call.External_ID__c + callSample.Line_Number_MVN__c,callSample.External_ID__c);
			}
		}

	}

	private static List<Call2_vod__c> createCallList(Integer numberOfCalls) {
		List<Call2_vod__c> newCalls = new List<Call2_vod__c>();
		for(Integer i=0; i<numberOfCalls;i++){
			Call2_vod__c call = new Call2_vod__c();
			call.Status_vod__c = 'Saved_vod';
			call.Account_vod__c = act.Id;
			call.Call_Date_vod__c = Date.today();
			call.Ship_Address_Line_1_vod__c = '123 Main St.';
			call.Ship_to_Name_MVN__c = 'John Doe';
			call.Ship_City_vod__c = 'Chicago';
			call.Ship_Zip_vod__c = '66666';
			call.Ship_Address_Line_2_vod__c = 'Dr. John Doe';
			call.Ship_Address_Line_3_MVN__c = 'Department';
			call.Ship_Address_Line_4_MVN__c = 'Main Hospital';
			call.Ship_Country_vod__c = 'DE';
			call.Country_Code_BI__c = 'DE';
			call.Ship_To_Salutation_MVN__c = 'Dear Sir or Madam,';
			call.Order_Letter_MVN__c = '01';
			call.OwnerId = adminUser.Id;
			newCalls.add(call);
		}

		insert newCalls;

		List<Call2_Sample_vod__c> newCallSamples = new List<Call2_Sample_vod__c>();

		for(Call2_vod__c call : newCalls) {
			Call2_Sample_vod__c cs = new Call2_Sample_vod__c();
			cs.Account_vod__c = act.Id;
			cs.Call_Date_vod__c = Date.today();
			cs.Call2_vod__c = call.Id;
			cs.Product_vod__c = product.Id;
			cs.Quantity_vod__c = 1;
			newCallSamples.add(cs);
		}

		insert newCallSamples;

		return newCalls;
	}
	
}