//Generated by wsdl2apex

public class SAPOrderMVN {
    public class Item {
        public String LineItem;
        public String SAPArticleId;
        public String Quantity;
        private String[] LineItem_type_info = new String[]{'LineItem','http://www.siebel.com/xml/SiebelOrder',null,'0','1','false'};
        private String[] SAPArticleId_type_info = new String[]{'SAPArticleId','http://www.siebel.com/xml/SiebelOrder',null,'0','1','false'};
        private String[] Quantity_type_info = new String[]{'Quantity','http://www.siebel.com/xml/SiebelOrder',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.siebel.com/xml/SiebelOrder','false','false'};
        private String[] field_order_type_info = new String[]{'LineItem','SAPArticleId','Quantity'};
    }
    public class SiebelOrder {
        public String SiebelOrderNum;
        public String SAPCustomerId;
        public String SiebelId;
        public String City;
        public String ZipCode;
        public String StreetAddress;
        public String Country;
        public String LetterText;
        public String Priority;
        public String DeliveryDate;
        public String Reference;
        public String Correspondence;
        public String Line1;
        public String Line2;
        public String Line3;
        public String Line4;
        public SAPOrderMVN.ListOfItems ListOfItems;
        private String[] SiebelOrderNum_type_info = new String[]{'SiebelOrderNum','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] SAPCustomerId_type_info = new String[]{'SAPCustomerId','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] SiebelId_type_info = new String[]{'SiebelId','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] City_type_info = new String[]{'City','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] ZipCode_type_info = new String[]{'ZipCode','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] StreetAddress_type_info = new String[]{'StreetAddress','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] Country_type_info = new String[]{'Country','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] LetterText_type_info = new String[]{'LetterText','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] Priority_type_info = new String[]{'Priority','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] DeliveryDate_type_info = new String[]{'DeliveryDate','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] Reference_type_info = new String[]{'Reference','http://www.siebel.com/xml/SiebelOrder',null,'0','1','false'};
        private String[] Correspondence_type_info = new String[]{'Correspondence','http://www.siebel.com/xml/SiebelOrder',null,'1','1','false'};
        private String[] Line1_type_info = new String[]{'Line1','http://www.siebel.com/xml/SiebelOrder',null,'0','1','false'};
        private String[] Line2_type_info = new String[]{'Line2','http://www.siebel.com/xml/SiebelOrder',null,'0','1','false'};
        private String[] Line3_type_info = new String[]{'Line3','http://www.siebel.com/xml/SiebelOrder',null,'0','1','false'};
        private String[] Line4_type_info = new String[]{'Line4','http://www.siebel.com/xml/SiebelOrder',null,'0','1','false'};
        private String[] ListOfItems_type_info = new String[]{'ListOfItems','http://www.siebel.com/xml/SiebelOrder',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.siebel.com/xml/SiebelOrder','false','false'};
        private String[] field_order_type_info = new String[]{'SiebelOrderNum','SAPCustomerId','SiebelId','City','ZipCode','StreetAddress','Country','LetterText','Priority','DeliveryDate','Reference','Correspondence','Line1','Line2','Line3','Line4','ListOfItems'};
    }
    public class ListOfItems {
        public SAPOrderMVN.Item[] Item;
        private String[] Item_type_info = new String[]{'Item','http://www.siebel.com/xml/SiebelOrder',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.siebel.com/xml/SiebelOrder','false','false'};
        private String[] field_order_type_info = new String[]{'Item'};
    }
}