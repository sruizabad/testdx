public with sharing class SelectGiftItemCAPController {
    List<String> selectedGifts;
    public Boolean refreshPage {get; set;}
    public String pageRef{get; set;}
    public Boolean buttonShow{get; set;}
    Case cnew;
    String caseId;
    
    public void setSelectedGifts(List<String> selectedGifts){
    this.selectedGifts=selectedGifts;
    }
    public List<String> getSelectedGifts(){
    return selectedGifts;
    }
    
    
    
    //Constructor
    public SelectGiftItemCAPController(ApexPages.StandardController controller) {
    buttonShow=true;
    caseId=ApexPages.currentPage().getParameters().get('id');
    if(caseId!=null)cnew=[SELECT All_Gifts_c__c,Selected_Gifts__c,Status,Transfer_To_MVN__c from case where id =:caseId];
    if(cnew!=null){
    if((cnew.Status=='Closed')|(cnew.Transfer_To_MVN__c!=null))buttonShow=false;
    else buttonShow=true;
    }
    }
    
    //Availabe Gifts
    public List<SelectOption> getgiftItems() {
        try{
        Case cnew=[SELECT All_Gifts_c__c,Selected_Gifts__c,Status from case where id =:caseId];
        if(cnew.Status=='Closed'){
        buttonShow=false;
        }}catch(QueryException e){}
        List<SelectOption> Options = new List<SelectOption>();
         try{
        Case ple=[SELECT All_Gifts_c__c from Case where id =:caseId];
        
        if(ple!=null){
        String str=ple.All_Gifts_c__c;
        List<String> liststr;
        if(str!=null){
        liststr=str.split(';');
        Set<String> setstr=new Set<String>();
        setstr.addAll(liststr);
        List<String> l=new List<String>();
        l.addAll(setstr);
        for(String str1:l){
        options.add(new SelectOption(str1, str1));}          
        }else Options.add(new SelectOption('none','--None--')); } 
        }catch(QueryException e){}
       return Options;
    }
    
    //Method for Add button
    public PageReference done(){
     String selected;
     
     for(String str:selectedGifts){
     if(selected==null)selected=str+';';else selected=selected+str+';';
     }
     cnew.Selected_Gifts__c=selected;
     String str='https://'+ApexPages.currentPage().getHeaders().get('X-Salesforce-Forwarded-To')+'/console';
     pageRef=str;
     refreshPage=true;
     update cnew;
     return null;
    }
}