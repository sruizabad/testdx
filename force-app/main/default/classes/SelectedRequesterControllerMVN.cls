/*
* SelectedRequesterControllerMVN
* Created By:    Roman Lerman
* Created Date:  1/21/2013
* Description:   This controller allows agents to input contact information for the callers such as
*                new addresses, phone numbers, emails, and faxes as well as select from existing 
*                contact information.
*/
public with sharing class SelectedRequesterControllerMVN {
    public Case     currentCase{get; set;}
    public Account  currentCaseAccount {get;set;}
    public Account  currentBusinessAccount {get;set;}
    public String   activeAddressId{get;set;}
    public String   accountEmailText{get;set;}
    public String   accountPhoneText{get;set;}
    public String   accountFaxText{get;set;}
    public Boolean  anonymize{get;set;}
    public Boolean  updatingAddress{get;set;}
    public Boolean  updatingEmail{get;set;}
    public Boolean  updatingPhone{get;set;}
    public Boolean  updatingFax{get;set;}

    public AccountWrapper  primaryAccount {get;set;}
    public AccountWrapper  secondaryAccount {get;set;}

    public Address_vod__c newAddress {get;set;}
    public String   newEmailText{get;set;}
    public String   newPhoneText{get;set;}
    public String   newFaxText{get;set;}

    public Boolean hasSaveError {get; set;}
    public Boolean saveSuccess {get; set;}
    public Boolean showSecondaryInformation {get{
        return secondaryAccount != null && primaryAccount.isPersonAccount;
    }}

    public List<SelectOption> emailAddresses{get; set;}
    public List<SelectOption> phoneNumbers{get; set;}
    public List<SelectOption> faxNumbers{get; set;}
    public List<Address_vod__c> addresses = new List<Address_vod__c>();

    private String userCountry {get;set;}

    public List<SelectOption> getStateOptions(){
        return UtilitiesMVN.getStatesProvinces(newAddress.Country_Code_BI__c);
    }
    
    public SelectedRequesterControllerMVN(ApexPages.StandardController controller){
        updatingAddress = FALSE;
        
        currentCase = (Case)controller.getRecord();
        
        System.debug('Current Case Id: '+currentCase.Id);
        
        List<Case> cases = [select Id, isClosed, CaseNumber, Address_MVN__c, AccountId, Status, case_Account_Email_MVN__c, case_Account_Phone_MVN__c, case_Account_Fax_MVN__c, Business_Account_MVN__c, ContactId,Requester_Type_MVN__c from Case where Id = :currentCase.Id];
        if(cases != null && cases.size() > 0){
            currentCase = cases[0];
        }
        
        System.debug('Current Case Account Id: '+currentCase.AccountId);
        System.debug('Current Case Business Account Id: '+currentCase.Business_Account_MVN__c);

        if(currentCase.AccountId != null){
            primaryAccount = new AccountWrapper(currentCase.AccountId);

            if(currentCase.Business_Account_MVN__c != null){
                secondaryAccount = new AccountWrapper(currentCase.Business_Account_MVN__c);
            }
            setActiveValues();
        }

        hasSaveError = false;
        saveSuccess = false;

        newAddress = new Address_vod__c();
        User runningUser = [Select Country_Code_BI__c from User where Id = :UserInfo.getUserId()];
        userCountry = runningUser.Country_Code_BI__c;
        newAddress.Country_Code_BI__c = userCountry;
    }

    public PageReference updateCase () {
        hasSaveError = false;
        saveSuccess = false;
        try {
            update currentCase;
            saveSuccess = true;
        } catch (Exception e) {
            ApexPages.addMessages(e);
            hasSaveError = true;
        }
        return null;
    }

    public void setActiveValues(){
        accountEmailText = currentCase.case_Account_Email_MVN__c;
        accountPhoneText = currentCase.case_Account_Phone_MVN__c;
        accountFaxText = currentCase.case_Account_Fax_MVN__c;

        if (currentCase.Address_MVN__c == null){
            activeAddressId = '';
            
        } else {
            activeAddressId = currentCase.Address_MVN__c;
        }
    }
    
    public PageReference associateAddress(){
        hasSaveError = false;
        saveSuccess = false;

        if (String.isNotBlank(activeAddressId) && activeAddressId != System.Label.Picklist_New_Option && activeAddressId != System.Label.Picklist_Select_Option){
            Address_vod__c caseAddress = [SELECT Id, Name, Address_line_2_vod__c, City_vod__c, OK_State_Province_BI__c, Zip_vod__c, Country_vod__c FROM Address_vod__c WHERE Id = :activeAddressId];
        
            try {
                currentCase.Address_MVN__c = caseAddress.Id;
                update currentCase;
            }
            catch (Exception e) {
                hasSaveError = true;
                ApexPages.addMessages(e);
                return null;
            }
            saveSuccess = true;
        }
        return null;
    }

    public PageReference createAddressView(){
        updatingAddress = TRUE;
        hasSaveError = false;
        return null;
    }

    public PageReference createAddress(){     
        if(String.isBlank(newAddress.Name)){
            hasSaveError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Schema.sObjectType.Address_vod__c.fields.Name.getLabel() + ' ' + System.Label.New_Address_Required_Error));
            return null;
        }
        if (!primaryAccount.isConsumer) {
            if(String.isBlank(newAddress.City_vod__c)){
                hasSaveError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Schema.sObjectType.Address_vod__c.fields.City_vod__c.getLabel() + ' ' + System.Label.New_Address_Required_Error));
                return null;
            } else if(newAddress.OK_State_Province_BI__c == null){
                hasSaveError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Schema.sObjectType.Address_vod__c.fields.OK_State_Province_BI__c.getLabel() + ' ' + System.Label.New_Address_Required_Error));
                return null;
            } else if(String.isBlank(newAddress.Zip_vod__c)){
                hasSaveError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Schema.sObjectType.Address_vod__c.fields.Zip_vod__c.getLabel() + ' ' + System.Label.New_Address_Required_Error));
                return null;
            } else if(newAddress.Country_Code_BI__c == null){
                hasSaveError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Schema.sObjectType.Address_vod__c.fields.Country_vod__c.getLabel() + ' ' + System.Label.New_Address_Required_Error));
                return null;
            }
        }

        if(secondaryAccount != null && !primaryAccount.isConsumer) {
            newAddress.Account_vod__c = secondaryAccount.account.Id;
        }
        else if(!primaryAccount.isPersonAccount || primaryAccount.isConsumer) {
            newAddress.Account_vod__c = primaryAccount.account.Id; 
        }

        SavePoint sp = Database.setSavePoint();
        try {
            insert newAddress;

            currentCase.Address_MVN__c  = newAddress.Id;
            update currentCase;
        } 
        catch (Exception e) {
            hasSaveError = true;
            ApexPages.addMessages(e);
            Database.rollback(sp);
            newAddress = newAddress.clone(false,true,false,false);
            return null;
        }

        if(!primaryAccount.isConsumer){
            //Create DCR
            UtilitiesMVN.createWorkplaceDataChangeRequest(currentCase.Id, Service_Cloud_Settings_MVN__c.getInstance().DCR_Update_Change_Type_MVN__c);
        }

        saveSuccess = true;

        return null;
    }

    public PageReference associateEmail(){
        hasSaveError = false;
        saveSuccess = false;
        if(String.isNotBlank(accountEmailText)){
            //Account acc = new Account(id=currentCaseAccount.Id);

            try {
                currentCase.case_Account_Email_MVN__c = accountEmailText;
                update currentCase;
            }
            catch (Exception e) {
                hasSaveError = true;
                ApexPages.addMessages(e);
                return null;
            }

            saveSuccess = true;
        }
        return null;
    }

    public PageReference createEmailView(){
        updatingEmail = TRUE;
        return null;
    }

    public PageReference createEmail(){
        hasSaveError = false;
        saveSuccess = false;

        if (newEmailText == null || newEmailText.length() == 0) {
            hasSaveError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.Error_Email_Required));
            return null;
        } 
        if(!UtilitiesMVN.isValidEmail(newEmailText)){
            hasSaveError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.Invalid_Email_Address));
            return null;
        } 

        SavePoint sp = Database.setSavePoint();

        try {
            update primaryAccount.createEmail(newEmailText);
            currentCase.case_Account_Email_MVN__c = newEmailText;
            update currentCase;
        }
        catch (Exception e) {
            hasSaveError = true;
            ApexPages.addMessages(e);
            Database.rollback(sp);
            return null;
        }
        updatingEmail = FALSE;
        saveSuccess = true;

        return null;
    }

    public PageReference associatePhone(){
        hasSaveError = false;
        saveSuccess = false;
        if(String.isNotBlank(accountPhoneText)){
            //Account acc = new Account(id=currentCaseAccount.Id);

            try {
                currentCase.case_Account_Phone_MVN__c = accountPhoneText;
                update currentCase;
            }
            catch (Exception e) {
                hasSaveError = true;
                ApexPages.addMessages(e);
                return null;
            }

            saveSuccess = true;
        }
        return null;
    }

    public PageReference createPhoneView(){
        updatingPhone = TRUE;
        return null;
    }

    public PageReference createPhone(){
        hasSaveError = false;
        saveSuccess = false;

        if (newPhoneText == null || newPhoneText.length() == 0) {
            hasSaveError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.Error_Phone_Required));
            return null;
        }

        newPhoneText = newPhoneText.replaceAll('[^0-9]', '');

        SavePoint sp = Database.setSavePoint();

        try {
            update primaryAccount.createPhone(newPhoneText);
            currentCase.case_Account_Phone_MVN__c = newPhoneText;
            update currentCase;
        }
        catch (Exception e) {
            hasSaveError = true;
            ApexPages.addMessages(e);
            Database.rollback(sp);
            return null;
        }
        
        updatingPhone = FALSE;
        saveSuccess = true;

        return null;
    }

    public PageReference associateFax(){
        hasSaveError = false;
        saveSuccess = false;
        if(String.isNotBlank(accountFaxText)){
            //Account acc = new Account(id=currentCaseAccount.Id);

            try {
                currentCase.case_Account_Fax_MVN__c = accountFaxText;
                update currentCase;
            }
            catch (Exception e) {
                hasSaveError = true;
                ApexPages.addMessages(e);
                return null;
            }

            saveSuccess = true;
        }
        return null;
    }

    public PageReference createFaxView(){
        updatingFax = TRUE;
        return null;
    }

    public PageReference createFax(){
        hasSaveError = false;
        saveSuccess = false;

        if (newFaxText == null || newFaxText.length() == 0) {
            hasSaveError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, System.Label.Error_Fax_Required));
            return null;
        }

        newFaxText = newFaxText.replaceAll('[^0-9]', '');
        SavePoint sp = Database.setSavePoint();
        try {
            update primaryAccount.createFax(newFaxText);
            currentCase.case_Account_Fax_MVN__c = newFaxText;
            update currentCase;
        }
        catch (Exception e) {
            hasSaveError = true;
            ApexPages.addMessages(e);
            Database.rollback(sp);
            return null;
        }

        updatingFax = FALSE;
        saveSuccess = true;

        return null;
    }

    public List <SelectOption> getCountries ()
    {
        List <SelectOption> options = new List <SelectOption> ();
        options.add (new SelectOption ('', System.Label.Picklist_Select_Option));
        
        for (Schema.PickListEntry ple : Schema.sObjectType.Address_vod__c.fields.Country_vod__c.getPicklistValues()) {
            options.add (new SelectOption (ple.getValue(), ple.getLabel()));
        }
        
        return options;
    }

    public void cancelCreate(){
        updatingAddress = FALSE;
        updatingEmail   = FALSE;
        updatingPhone   = FALSE;
        updatingFax     = FALSE;
        hasSaveError = false;
        saveSuccess = false;
        //seupContactInformation();
        setActiveValues();

        newAddress = new Address_vod__c();
        newAddress.Country_vod__c = userCountry;

        newFaxText = '';
        newPhoneText = '';
        newEmailText = '';
    }

    public List<SelectOption> getAddressSelectOptions() {
        List<SelectOption> addressList = new List<SelectOption>();

        if (primaryAccount.isPersonAccount) 
        {               
            if (String.isBlank(activeAddressId)) {
                addressList.add( new SelectOption('', Label.Picklist_Select_Option));
            }
            addressList.addAll( primaryAccount.addressOptions );
            if (secondaryAccount != null) 
            {
                addressList.addAll( secondaryAccount.addressOptions );
                addressList.add ( new SelectOption( Label.Picklist_New_Option, Label.Picklist_New_Option));
            } 
            else if (primaryAccount.isConsumer) {
                addressList.add ( new SelectOption( Label.Picklist_New_Option, Label.Picklist_New_Option));
            } else
            {
                addressList.add ( new SelectOption( Label.Picklist_Select_Business_Account, Label.Picklist_Select_Business_Account));
            }
        }
        else
        {
            addressList.add( new SelectOption('', Label.Picklist_Address_Option));
            addressList.addAll( primaryAccount.addressOptions );
            addressList.add( new SelectOption( Label.Picklist_New_Option, Label.Picklist_New_Option));
        }   

        return addressList;
    }

    public List<SelectOption> getPhoneSelectOptions() {
        Set<String> phoneNumberSet = new Set<String>();

        phoneNumberSet.addAll(primaryAccount.phoneNumbers);
        if (showSecondaryInformation) {
            phoneNumberSet.addAll( secondaryAccount.phoneNumbers);
        }

        if ( String.isNotBlank(currentCase.case_Account_Phone_MVN__c) ) {
            phoneNumberSet.add(currentCase.case_Account_Phone_MVN__c);
        }

        return sortedPicklistOptionsFromSet( phoneNumberSet, accountPhoneText );
    }

    public List<SelectOption> getEmailSelectOptions() {
        Set<String> emailAddressStrings = new Set<String>();
        
        emailAddressStrings.addAll( primaryAccount.emailAddresses );
        if (showSecondaryInformation) {
            emailAddressStrings.addAll( secondaryAccount.emailAddresses );
        }

        if ( String.isNotBlank(currentCase.case_Account_Email_MVN__c) ) {
            emailAddressStrings.add(currentCase.case_Account_Email_MVN__c);
        }

        return sortedPicklistOptionsFromSet( emailAddressStrings, accountEmailText );
    }

    public List<SelectOption> getFaxSelectOptions() {
        Set<String> faxNumberSet = new Set<String>();

        faxNumberSet.addAll( primaryAccount.faxNumbers );
        if (showSecondaryInformation) {
            faxNumberSet.addAll( secondaryAccount.faxNumbers );
        }

        if ( String.isNotBlank(currentCase.case_Account_Fax_MVN__c) ) {
            faxNumberSet.add(currentCase.case_Account_Fax_MVN__c);
        }


        return sortedPicklistOptionsFromSet( faxNumberSet, accountFaxText );        
    }

    private List<SelectOption> sortedPicklistOptionsFromSet(Set<String> theSet, String setterValue) {
        List<String> sortedList = new List<String>();
        sortedList.addAll( theSet );
        sortedList.sort();

        List<SelectOption> optionList = new List<SelectOption>();
        if (String.isBlank(setterValue)) {
            optionList.add( new SelectOption('', Label.Picklist_Select_Option));
        }

        for (String str : sortedList) {
            optionList.add( new SelectOption( str, str));
        }

        optionList.add( new SelectOption( Label.Picklist_New_Option, Label.Picklist_New_Option));

        return optionList;
    }

    public class AccountWrapper{

        public Account account {get;set;}
        public List<Address_vod__c> addresses {get;set;}
        public Boolean isPersonAccount { get{ return account.IsPersonAccount; }}
        public Boolean isConsumer {
            get {
                return account.IsPersonAccount && account.RecordTypeId == [select Id from RecordType where DeveloperName = :UtilitiesMVN.consumerRecordType].Id;
            }
        }

        public List<SelectOption> addressOptions{
            get{
                List<SelectOption> options = new List<SelectOption>();
        
                for (Address_vod__c ad : addresses){
                    options.add(UtilitiesMVN.getSelectOptionAddress(ad));
                }   
                return options;
            }
        }

        public Set<String> phoneNumbers{
            get{
                Set<String> phoneNumbers = new Set<String>();

                if(isPersonAccount){
                    if(account.PersonMobilePhone != null && account.PersonMobilePhone != ''){
                        phoneNumbers.add(account.PersonMobilePhone);
                    }
                }
                if(account.Phone != null && account.Phone != ''){
                    phoneNumbers.add(account.Phone);
                }
                if(account.CRC_Phone_MVN__c != null && account.CRC_Phone_MVN__c != ''){
                    phoneNumbers.add(account.CRC_Phone_MVN__c);
                }
                

                return phoneNumbers;
            }
        }

        public Set<String> faxNumbers{
            get{
                Set<String> faxNumbers = new Set<String>();
                if(account.Fax != null && account.Fax != ''){
                        faxNumbers.add(account.Fax);
                    }
                
                if(account.CRC_Fax_MVN__c != null && account.CRC_Fax_MVN__c != ''){
                    faxNumbers.add(account.CRC_Fax_MVN__c);
                }
                return faxNumbers;
            }
        }

        public Set<String> emailAddresses{
            get{
                
                Set<String> emailAddressStrings = new Set<String>();
                if(isPersonAccount){
                    if(account.PersonEmail != null && account.PersonEmail != ''){
                        emailAddressStrings.add(account.PersonEmail);
                    }
                } else{
                    if(account.CRC_Email_MVN__c != null && account.CRC_Email_MVN__c != ''){
                        emailAddressStrings.add(account.CRC_Email_MVN__c);
                    }
                }

                return emailAddressStrings;
            }
        }

        public AccountWrapper(Id accountId){
            account = [SELECT Name, Id, IsPersonAccount, PersonEmail, Phone, 
                                PersonHomePhone, PersonMobilePhone, PersonOtherPhone, Fax, Specialty_BI__c,
                                CRC_Phone_MVN__c, CRC_Fax_MVN__c, CRC_Email_MVN__c, RecordTypeId
                        FROM Account WHERE Id = :accountId];

            addresses = [SELECT Id, Name, Address_Line_2_vod__c, City_vod__c, OK_State_Province_BI__r.Name, 
                                Zip_vod__c, Country_vod__c, Primary_vod__c 
                        FROM Address_vod__c WHERE Account_vod__c = :accountId];
        }

        public Account createFax(String faxNumber){
            Account account = new Account(Id = account.Id);
            account.CRC_Fax_MVN__c = faxNumber;
                
            return account;
        }

        public Account createPhone(String phoneNumber){
            Account account = new Account(Id = account.Id);
            account.CRC_Phone_MVN__c = phoneNumber;
                
            return account;
        }

        public Account createEmail(String email){
            Account account = new Account(Id = account.Id);
            if(isPersonAccount)
                account.PersonEmail = email;
            else
                account.CRC_Email_MVN__c = email;
            return account;
        }
    }
}