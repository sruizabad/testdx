/*
* SelectedRequesterControllerTestMVN
* Created By:    Roman Lerman
* Created Date:  6/23/2013
* Description:   This is the test class for the SelectedRequesterControllerMVN.
*/
@isTest
private class SelectedRequesterControllerTestMVN {
  static Case interaction = null;
  static SelectedRequesterControllerMVN controller = null;
  
  // setup data
  static{
        TestDataFactoryMVN.createSettings();

    interaction = TestDataFactoryMVN.createTestCase();
    Account testAccount = [select Id from Account where Id =:interaction.AccountId];

    TestDataFactoryMVN.createTestAddress(testAccount);

    interaction.Address_MVN__c = null;

    update interaction;

  }

  // Story:  Contact information is chosen for the caller
  // Narrative: 
  // In order to properly send a fulfillment package/response
  // As a call center agent
  // I want to select an existing address, phone number, email, fax for the caller
  
  // Scenario 1:  Select an address
  static testMethod void selectAnAddress(){
    givenCustomerPreviouslyProvidedAnAddress();
    whenLookCanSeeAndSelectAddress();
    addressAddedToTheInteraction();
  }
  // Given a customer has previously provided an address
  static void givenCustomerPreviouslyProvidedAnAddress(){
    //TestDataFactoryMVN.createTestAddress([select Id from Account]);
    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
        controller.getCountries();
    }
  // When I look, I can see their address and select it
  static void whenLookCanSeeAndSelectAddress(){
    System.assertEquals(2, controller.primaryAccount.addressOptions.size());
    System.assertEquals('Hallshted 123', controller.primaryAccount.addresses[0].Name);
    
    controller.activeAddressId = controller.primaryAccount.addresses[0].Id;
    controller.associateAddress();
    controller.createAddressView();
  }
  // Then it is added to the Interaction
  static void addressAddedToTheInteraction(){
    System.assertEquals([select Address_MVN__c from Case].Address_MVN__c, controller.primaryAccount.addresses[0].Id);
    controller.getStateOptions();
  }
//***************
  // Scenario 2:  Select a phone number
  static testMethod void selectAPhoneNumber(){
    givenCustomerPreviouslyProvidedPhoneNumber();
    whenLookCanSeeAndSelectPhoneNumber();
    phoneAddedToTheInteraction();
  }
  // Given a customer has previously provided a phone number
  static testMethod void givenCustomerPreviouslyProvidedPhoneNumber(){
    Account acct = [select Id, Phone from Account];
    acct.CRC_Phone_MVN__c = '(555) 555-5555';
    update acct;
    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
  }
  // When I looks, I can see their phone number and select it
  static void whenLookCanSeeAndSelectPhoneNumber(){
    System.assertEquals(2, controller.primaryAccount.phoneNumbers.size());
    System.assert(controller.primaryAccount.phoneNumbers.contains('(555) 555-5555'));
    
    controller.accountPhoneText = '(555) 555-5555';
    controller.associatePhone();
    controller.createPhoneView();
  }
  // Then it is added to the Interaction
  static void phoneAddedToTheInteraction(){
    System.assert(controller.primaryAccount.phoneNumbers.contains([select case_Account_Phone_MVN__c from Case].case_Account_Phone_MVN__c));
  }
//***************
  // Scenario 3:  Select an email
  static testMethod void selectAnEmail(){
    givenCustomerPreviouslyProvidedEmail();
    whenLookCanSeeAndSelectEmail();
    emailAddedToTheInteraction();
  }
  // Given a customer has previously provided an email
  static testMethod void givenCustomerPreviouslyProvidedEmail(){
    Account acct = [select Id, Phone from Account];
    acct.PersonEmail = 'testaccount@testaccount.com';
    update acct;
    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
  }
  // When I looks, I can see their email and select it
  static void whenLookCanSeeAndSelectEmail(){
    System.assertEquals(1, controller.primaryAccount.emailAddresses.size());
    System.assert(controller.primaryAccount.emailAddresses.contains('testaccount@testaccount.com'));
    
    controller.accountEmailText = 'testaccount@testaccount.com';
    controller.associateEmail();
    controller.createEmailView();
  }
  // Then it is added to the Interaction
  static void emailAddedToTheInteraction(){
    System.assert(controller.primaryAccount.emailAddresses.contains([select case_Account_Email_MVN__c from Case].case_Account_Email_MVN__c));
  }  
//***************
  // Scenario 4:  Select a fax
  static testMethod void selectAFax(){
    givenCustomerPreviouslyProvidedFax();
    whenLookCanSeeAndSelectFax();
    faxAddedToTheInteraction();
  }
  // Given a customer has previously provided an email
  static void givenCustomerPreviouslyProvidedFax(){
    Account acct = [select Id, Phone from Account];
    acct.Fax = '(777) 777-7777';
    update acct;
    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
        controller.createFaxView();
  }
  // When I looks, I can see their email and select it
  static void whenLookCanSeeAndSelectFax(){
    System.assertEquals(1, controller.primaryAccount.faxNumbers.size());
    System.assert(controller.primaryAccount.faxNumbers.contains('(777) 777-7777'));
    
    controller.accountFaxText = '(777) 777-7777';
    controller.associateFax();
  }
  // Then it is added to the Interaction
  static void faxAddedToTheInteraction(){
    System.assert(controller.primaryAccount.faxNumbers.contains([select case_Account_Fax_MVN__c from Case].case_Account_Fax_MVN__c));
  }  
//*****

  // Scenario 2:  A new phone number is provided
  static testMethod void aNewPhoneIsProvided(){
    givenACustomerProvidesANewPhoneNumber();
    whenISelectThePhoneNumber();
    phoneNumberIsAddedForTheCustomer();
  }
  // Given a customer provides a new address
  static void givenACustomerProvidesANewPhoneNumber(){    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new SelectedRequesterControllerMVN(stdController);
        
    controller.newPhoneText = '(555) 555-5555';
  }
  // When I select the address
  static void whenISelectThePhoneNumber(){    
    controller.createPhone();
  }
  // Then it is added for the customer
  static void phoneNumberIsAddedForTheCustomer(){
    System.assertEquals([select Account.CRC_Phone_MVN__c from Case where Id = :controller.currentCase.Id].Account.CRC_Phone_MVN__c, '5555555555');
    System.assertEquals('5555555555',[select case_Account_Phone_MVN__c from Case].case_Account_Phone_MVN__c);
  }
//*******
  // Scenario 3:  A new email is provided
  static testMethod void aNewEmailIsProvided(){
    givenACustomerProvidesANewEmail();
    whenISelectTheEmail();
    emailIsAddedForTheCustomer();
  }
  // Given a customer provides a new email
  static void givenACustomerProvidesANewEmail(){    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
        
        controller.newEmailText = 'testaccount@testaccount.com';
  }
  // When I select the address
  static void whenISelectTheEmail(){    
    controller.createEmail();
  }
  // Then it is added for the customer
  static void emailIsAddedForTheCustomer(){
    System.assertEquals([select PersonEmail from Account].PersonEmail, 'testaccount@testaccount.com');
    System.assertEquals([select case_Account_Email_MVN__c from Case].case_Account_Email_MVN__c, 'testaccount@testaccount.com');
  }
//*******
  // Scenario 4:  A new fax is provided
  static testMethod void aNewFaxIsProvided(){
    givenACustomerProvidesANewFax();
    whenISelectTheFax();
    faxIsAddedForTheCustomer();
  }
  // Given a customer provides a new email
  static void givenACustomerProvidesANewFax(){    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
        
        controller.newFaxText = '(777) 777-7777';
  }
  // When I select the address
  static void whenISelectTheFax(){    
    controller.createFax();
  }
  // Then it is added for the customer
  static void faxIsAddedForTheCustomer(){
    System.assertEquals('7777777777',[select Account.CRC_Fax_MVN__c from Case where Id =: controller.currentCase.Id].Account.CRC_Fax_MVN__c);
    System.assertEquals('7777777777',[select case_Account_Fax_MVN__c from Case].case_Account_Fax_MVN__c);
  }

//*******
  // Scenario 5:  A new fax is provided then create is canceled
  static testMethod void aFaxAddedThenCanceled(){
    givenACustomerProvidesANewFax();
    cancelFaxSelection();
  }

  static void cancelFaxSelection() {
    controller.cancelCreate();
    System.assertEquals('',controller.newFaxText);
  }
  
//*******
  
  // Story:  New contact information is entered for the caller
  // Narrative: 
    // In order to properly send a fulfillment package/response
  // As a call center agent
    // I want to add a new address
    
  // Scenario 6:  A new address is provided without all of the necessary fields filled in
  static testMethod void aNewAddressIsProvidedWithoutEnoughInformation(){
    givenACustomerProvidesANewAddressWithoutEnoughInformation();
    whenISelectTheAddressIGetAnError();
  }
  // Given a customer provides a new address
  static void givenACustomerProvidesANewAddressWithoutEnoughInformation(){    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
        controller.newAddress.Name = null;
        controller.newAddress.Address_Line_2_vod__c = null;
        controller.newAddress.City_vod__c = null;
        controller.newAddress.State_vod__c = null;
        controller.newAddress.Zip_vod__c = null;
        controller.newAddress.Country_vod__c = null;
  }
  // When I select the address
  static void whenISelectTheAddressIGetAnError(){  
    try{  
      controller.createAddress();
    }catch(Exception e){
      System.assert(e.getMessage().contains(System.Label.Error_Required_Address_Fields));
      return;
    }
  }
//*******
  // Scenario 7:  A new phone number is provided but the agent doesn't enter it
  static testMethod void aNewPhoneIsProvidedButNotEntered(){
    givenACustomerProvidesANewPhoneNumberButItsNotEntered();
    whenISelectThePhoneNumber();
  }
  // Given a customer provides a new address
  static void givenACustomerProvidesANewPhoneNumberButItsNotEntered(){    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
        
        controller.newPhoneText = null;
  }
  // When I select the address
  static void whenISelectThePhoneNumberIGetAnError(){    
    try{  
      controller.createPhone();
    }catch(Exception e){
      System.assert(e.getMessage().contains(System.Label.Error_Phone_Required));
      return;
    }
  }
//*******
  // Scenario 8:  A new email is provided but not entered
  static testMethod void aNewEmailIsProvidedButNotEntered(){
    givenACustomerProvidesANewEmailButNotEntered();
    whenISelectTheEmailIGetAnError();
  }
  // Given a customer provides a new email
  static void givenACustomerProvidesANewEmailButNotEntered(){    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
        
        controller.newEmailText = null;
  }
  // When I select the address
  static void whenISelectTheEmailIGetAnError(){    
    try{  
      controller.createEmail();
    }catch(Exception e){
      System.assert(e.getMessage().contains(System.Label.Error_Email_Required));
      return;
    }
  }
//*******
  // Scenario 9:  A new fax is provided but not entered
  static testMethod void aNewFaxIsProvidedButNotEntered(){
    givenACustomerProvidesANewFaxButNotEntered();
    whenISelectTheFaxIGetAnError();
  }
  // Given a customer provides a new email
  static void givenACustomerProvidesANewFaxButNotEntered(){    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
        controller = new SelectedRequesterControllerMVN(stdController);
        
        controller.newFaxText = null;
  }
  // When I select the fax
  static void whenISelectTheFaxIGetAnError(){    
    try{  
      controller.createFax();
    }catch(Exception e){
      System.assert(e.getMessage().contains(System.Label.Error_Fax_Required));
      return;
    }
  }
//*******

//*******
  // Scenario 10:  A business account is populated 
  static testMethod void businessAccountWithPrimary(){
    givenABusinessAccountIsAdded();
    secondaryAccountIsPopulated();
    whenAddressIsCreatedItsAssociatedWithBusiness();
  }
  static void givenABusinessAccountIsAdded(){  
    Account business = TestDataFactoryMVN.createTestBusinessAccount();
    TestDataFactoryMVN.createTestAddress(business);

    TestDataFactoryMVN.createTestAddress(business);
    interaction.AccountId = business.Id;
    interaction.Business_Account_MVN__c = business.Id;  

    update interaction;

    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new SelectedRequesterControllerMVN(stdController);
        
  }
  static void secondaryAccountIsPopulated(){    
    System.assert(controller.secondaryAccount != null);
    System.assertEquals(false, controller.showSecondaryInformation);
    System.assert(controller.secondaryAccount.addressOptions.size() > 0);
    System.assert(controller.secondaryAccount.phoneNumbers.size() > 0);
    System.assert(controller.secondaryAccount.faxNumbers.size() > 0);
    System.assert(controller.secondaryAccount.emailAddresses.size() > 0);
  }
  static void whenAddressIsCreatedItsAssociatedWithBusiness(){
    Customer_Attribute_BI__c state = new Customer_Attribute_BI__c();
    state.Name = 'Test State';
    state.Type_BI__c = 'ADDR_State';
    insert state;

    controller.newAddress.Name = '456 Fake St.';
    controller.newAddress.Address_Line_2_vod__c = 'Unit 1';
    controller.newAddress.City_vod__c = 'Chicago';
    controller.newAddress.OK_State_Province_BI__c = state.Id;
    controller.newAddress.Zip_vod__c = '12345';
    controller.newAddress.Country_Code_BI__c = 'US';

    controller.createAddress();

    Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();

    Case cs = [select AccountId, Address_MVN__c, Address_MVN__r.Name, Address_MVN__r.Address_Line_2_vod__c, Address_MVN__r.City_vod__c, Address_MVN__r.OK_State_Province_BI__r.Name, 
                Address_MVN__r.Zip_vod__c, Address_MVN__r.Country_Code_BI__c 
                from Case where Id = :controller.currentCase.Id limit 1];

    System.debug('DCR Page Messages: ' + ApexPages.getMessages());

    V2OK_Data_Change_Request__c dcr = [select RecordTypeId, HCO_Temp_External_ID_BI__c, OK_Workplace_Class_BI__r.Name, Workplace_Specialty_BI__r.Name,
                      Phone__c, Change_Type__c, Status__c, ADDR_Temp_External_ID_BI__c, New_Address_Line_1__c, City_vod__c,
                      OK_State_Province_BI__r.Name, Zip_vod__c, Country_DS__c
                      from V2OK_Data_Change_Request__c limit 1];

    System.assertEquals('456 Fake St.', cs.Address_MVN__r.Name);
    System.assertEquals('Unit 1', cs.Address_MVN__r.Address_Line_2_vod__c);
    System.assertEquals('Chicago', cs.Address_MVN__r.City_vod__c);
    System.assertEquals('Test State', cs.Address_MVN__r.OK_State_Province_BI__r.Name);
    System.assertEquals('12345', cs.Address_MVN__r.Zip_vod__c);
    System.assertEquals('US', cs.Address_MVN__r.Country_Code_BI__c);

    System.assertEquals([select Id from RecordType where SObjectType = 'V2OK_Data_Change_Request__c' AND DeveloperName = :settings.DCR_Record_Type_MVN__c].Id,dcr.RecordTypeId);
    System.assertEquals(cs.AccountId, dcr.HCO_Temp_External_ID_BI__c);
    System.assertEquals('Test Workplace Class', dcr.OK_Workplace_Class_BI__r.Name);
    System.assertEquals('Test Workplace Specialty', dcr.Workplace_Specialty_BI__r.Name);
    System.assertEquals('3333333333', dcr.Phone__c);
    System.assertEquals(settings.DCR_Update_Change_Type_MVN__c, dcr.Change_Type__c);
    System.assertEquals(settings.DCR_Status_MVN__c, dcr.Status__c);
    System.assertEquals(cs.Address_MVN__c, dcr.ADDR_Temp_External_ID_BI__c);
    System.assertEquals('456 Fake St.', dcr.New_Address_Line_1__c);
    System.assertEquals('Chicago', dcr.City_vod__c);
    System.assertEquals('Test State', dcr.OK_State_Province_BI__r.Name);
    System.assertEquals('12345', dcr.Zip_vod__c);
    System.assertEquals('US', dcr.Country_DS__c);
  }
//*******

  // Story:  New contact information is entered for the caller
  // Narrative: 
  // In order to properly send a fulfillment package/response
  // As a call center agent
  // I want to add a new address, phone number, email, fax
    
  // Scenario 11:  A new address is provided
  static testMethod void aNewAddressIsProvided(){
    givenACustomerProvidesANewAddress();
    whenISelectTheAddress();
    addressIsAddedForTheCustomer();
  }
  // Given a customer provides a new address
  static void givenACustomerProvidesANewAddress(){    
    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new SelectedRequesterControllerMVN(stdController);

    Customer_Attribute_BI__c state = new Customer_Attribute_BI__c();
    state.Name = 'Test State';
    state.Type_BI__c = 'ADDR_State';
    insert state;

    controller.newAddress.Name = '456 Fake St.';
    controller.newAddress.Address_Line_2_vod__c = 'Unit 1';
    controller.newAddress.City_vod__c = 'Chicago';
    controller.newAddress.OK_State_Province_BI__c = state.Id;
    controller.newAddress.Zip_vod__c = '12345';
    controller.newAddress.Country_Code_BI__c = 'US';
  }
  // When I select the address
  static void whenISelectTheAddress(){    
    controller.createAddress();
  }
  // Then it is added for the customer
  static void addressIsAddedForTheCustomer(){
    System.assertEquals([select count() from Address_vod__c where Name = '456 Fake St.'], 1);
    System.assertEquals([select Address_MVN__c from Case].Address_MVN__c, [select Id from Address_vod__c where Name='456 Fake St.'].Id);
  }
//*******

  static testMethod void testAddressErrorCases(){
    givenABusinessAccountIsAdded();
    secondaryAccountIsPopulated();

    Customer_Attribute_BI__c state = new Customer_Attribute_BI__c();
    state.Name = 'Test State';
    state.Type_BI__c = 'ADDR_State';
    insert state;

    controller.createAddress();

    controller.newAddress.Name = '456 Fake St.';

    controller.createAddress();

    System.assert(ApexPages.getMessages().size() > 0);

    controller.newAddress.Name = '456 Fake St.';
    controller.newAddress.City_vod__c = 'Chicago';

    controller.createAddress();

    System.assert(ApexPages.getMessages().size() > 0);

    controller.newAddress.Name = '456 Fake St.';
    controller.newAddress.City_vod__c = 'Chicago';
    controller.newAddress.OK_State_Province_BI__c = state.Id;

    controller.createAddress();

    System.assert(ApexPages.getMessages().size() > 0);

    controller.newAddress.Name = '456 Fake St.';
    controller.newAddress.City_vod__c = 'Chicago';
    controller.newAddress.OK_State_Province_BI__c = state.Id;
    controller.newAddress.Zip_vod__c = '12345';

    controller.createAddress();

    System.assert(ApexPages.getMessages().size() > 0);
  }

  static testMethod void testsSelectOptions(){

    Account business = TestDataFactoryMVN.createTestBusinessAccount();
    TestDataFactoryMVN.createTestAddress(business);

    interaction.Business_Account_MVN__c = business.Id;
    interaction.case_Account_Phone_MVN__c = '1234567890';
    interaction.case_Account_Email_MVN__c = 'test@test.com';
    interaction.case_Account_Fax_MVN__c = '9876543210';

    update interaction;

    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new SelectedRequesterControllerMVN(stdController);
        

    controller.getAddressSelectOptions();
    controller.getPhoneSelectOptions();
    controller.getEmailSelectOptions();
    controller.getFaxSelectOptions();

    Account hcp = TestDataFactoryMVN.createTestHCP();
    TestDataFactoryMVN.createTestAddress(hcp);

    interaction.AccountId = hcp.Id;
    interaction.ContactId = [select PersonContactId from Account where Id = :hcp.Id].PersonContactId;

    update interaction;

    stdController = new ApexPages.standardController(interaction);
    controller = new SelectedRequesterControllerMVN(stdController);
        
    controller.getAddressSelectOptions();
    controller.getPhoneSelectOptions();
    controller.getEmailSelectOptions();
    controller.getFaxSelectOptions();

    interaction.Business_Account_MVN__c = null;

    update interaction;

    stdController = new ApexPages.standardController(interaction);
    controller = new SelectedRequesterControllerMVN(stdController);
        
    controller.getAddressSelectOptions();
    controller.getPhoneSelectOptions();
    controller.getEmailSelectOptions();
    controller.getFaxSelectOptions();

    interaction.Business_Account_MVN__c = business.Id;
    interaction.AccountId = business.Id;
    interaction.ContactId = null;

    update interaction;

    stdController = new ApexPages.standardController(interaction);
    controller = new SelectedRequesterControllerMVN(stdController);
        
    controller.getAddressSelectOptions();
    
  }

  static testMethod void testExceptions(){
    Account hcp = TestDataFactoryMVN.createTestHCP();
    TestDataFactoryMVN.createTestAddress(hcp);

    interaction.AccountId = hcp.Id;
    interaction.ContactId = [select PersonContactId from Account where Id = :hcp.Id].PersonContactId;

    update interaction;

    ApexPages.standardController stdController = new ApexPages.standardController(interaction);
    controller = new SelectedRequesterControllerMVN(stdController);
    givenACustomerProvidesANewAddress();

    controller.newAddress.Country_Code_BI__c = null;
    
    Test.setReadOnlyApplicationMode(true);
      controller.createAddress();
      controller.associateAddress();

      controller.accountEmailText = 'test@test.com';
      controller.accountPhoneText = '5555555';
      controller.accountFaxText = '5555555';

      
      controller.newPhoneText = '5555555';
      controller.newFaxText = '5555555';

      controller.associateEmail();
      controller.newEmailText = 'testtest.com';
      controller.createEmail();
      controller.newEmailText = 'test@test.com';
      controller.createEmail();
      controller.associatePhone();
      controller.createPhone();  
      controller.associateFax();
      controller.createFax();
      
  }
}