/************************************************************************************
    Class to call Email
*************************************************************************************/
Public class SendEmail
{ 

   List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
   
  // public static String flag;
   /************************************************************************************
   Method to send Email
   ***********************************************************************************/
    //public static List<Product_vod__c> lp=new List<Product_vod__c>();
         
       public static void SendEmail(Boolean flag,String Message)
      // public static void SendEmail()
        {
 
        //for(Product_vod__c vodc :lp)
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
        // Step 2: Set list of people who should get the email
          List<String> sendTo = new List<String>();

          //sendTo.add('kavneet.kaur.ext@boehringer-ingelheim.com');
          //sendTo.add('itskyvva@boehringer-ingelheim.com');
          
          //Recipients list from custom setting
           Map<String, MDM_Product_Recipients__c> mapRecipients= MDM_Product_Recipients__c.getAll();

           System.debug('===========mapRecipients====='+mapRecipients); 
            
            //loop through all values in recipients setting and add them to the sendTo List 
            for (MDM_Product_Recipients__c rec:mapRecipients.values()){
                sendTo.add(rec.Recipients__c);
            }

          mail.setToAddresses(sendTo);
    
       // Step 3: Set who the email is sent from
        mail.setReplyTo('noreply@salesforce.com');
        mail.setSenderDisplayName('noreply@salesforce.com');

      
    
      if(!flag)
    
     {
      mail.setSubject('Product MDM:Product Creation Notification');
      String body = '<br>\r\n The following Product has been created in Veeva CRM from Product MDM:</br>';
                        
              body  +=  Message;   
             //body += 'Product Name: ' + vodc.Name + ', ';
             //body += 'MDM Id/External Id: '+ vodc.External_ID_vod__c + ', ';
            // body += 'Veeva ID:'+ vodc.ID + ',  ';
            // body += 'V external Id:'+ vodc.VExternal_Id_vod__c + ', ';
            // body += 'Parent product name: '+ vodc.Parent_Product_vod__c + ', ';
          // body += 'Parent product MDM ID/external ID:'+ vodc.Country_Code_BI__c + ', ';
           //body += 'Parent product Veeva ID:'+ vodc.Country_Code_BI__c + ', ';
          // String[] filelines = body.split('\n');
             mail.setHtmlBody(body);
    
                                 
          // Step 5. Add your email to the master list
              mails.add(mail);
            
      
    }
    
    if(flag)
    { 
     
       mail.setSubject('Product MDM:Product Update Notification');
       String body = '\r\n The following Product has been updated  in Veeva CRM from Product MDM:';
                        
              body +='<br> </br>'+ Message + '\n'; 
             mail.setHtmlBody(body);
    
                                 
          // Step 5. Add your email to the master list
              mails.add(mail);
              system.debug('=============================mails===================='+mail);
            

  }

  // Step 6: Send all emails in the master list
  Messaging.SendEmail(mails);
  system.debug('=============================mails===================='+mail);
}//End for try loop

 }
//End for Product_Vod___c