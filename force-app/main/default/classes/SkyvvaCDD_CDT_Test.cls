/**
 Test class
 */
@isTest
private class SkyvvaCDD_CDT_Test {
	
	@testSetup static void setup() {
		DateTime dt=System.now();
		
		/**
		Due to customer sObject may have validation rule/trigger/workflow, so test with skyvva sObject
		*/
		
		/*Account acc=new Account(Name='TestCDD');
		insert acc;
		
		Opportunity opp=new Opportunity(Name='TestOppCDD',AccountId=acc.Id, StageName='Open',CloseDate=dt.addDays(1).date());
		
		Product2 prod = new Product2(Name = 'Laptop XX200', Family = 'Hardware');
		insert new SObject[]{opp,prod};
		
		Id pricebookId = Test.getStandardPricebookId();
		PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='CustomPricebookCDD', isActive=true);
        insert new SObject[]{standardPrice,customPB};
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id,UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        OpportunityLineItem itm=new OpportunityLineItem(OpportunityId=opp.Id, PricebookEntryId=customPrice.Id, Quantity=1,unitprice=1500);
        insert itm;
		*/
		
		skyvvasolutions__Adapter__c a=new skyvvasolutions__Adapter__c(Name='TestCDD'+dt,skyvvasolutions__Name__c='Adapter'+dt,skyvvasolutions__Type__c='SAP-PI');
        insert a;
        
        skyvvasolutions__Property__c[] lP=new skyvvasolutions__Property__c[0];
        lP.add(new skyvvasolutions__Property__c(skyvvasolutions__Name__c='endpoint',skyvvasolutions__Value2__c='http://www.test.com',skyvvasolutions__Adapter__c=a.Id));
        lP.add(new skyvvasolutions__Property__c(skyvvasolutions__Name__c='password',skyvvasolutions__Value2__c='test',skyvvasolutions__Adapter__c=a.Id));
        lP.add(new skyvvasolutions__Property__c(skyvvasolutions__Name__c='username',skyvvasolutions__Value2__c='SFORCE',skyvvasolutions__Adapter__c=a.Id));
        insert lP;
		
		
		//SKYVVA Configuration
		CDD_CDT_Config__c cs=CDD_CDT_Config__c.getValues('SC_CDD_TIME_INTERVAL_MINUTE');
		if(cs==null) cs=new CDD_CDT_Config__c(Name='SC_CDD_TIME_INTERVAL_MINUTE');
		cs.Value__c='5';
		upsert cs;
		
		skyvvasolutions__Integration__c integ=new skyvvasolutions__Integration__c(Name=dt+'IntegrationTestCDD');
        insert integ;
        /*
        skyvvasolutions__Interfaces__c intfIntg=new skyvvasolutions__Interfaces__c(LastRun__c=dt,Schedule_Outbound__c=true,skyvvasolutions__Name__c='AccountCDD'+dt+'',skyvvasolutions__Type__c='OutBound',skyvvasolutions__Status__c='Deployed',skyvvasolutions__Source_Name__c='ACCOUNTX',
            skyvvasolutions__Query__c='select name,billingCity from account a, a.Parent p where p.OwnerId NOT IN(Select id from User)',skyvvasolutions__Integration__c=integ.Id,skyvvasolutions__AdapterId__c=a.Id);
        skyvvasolutions__Interfaces__c infInterfac=new skyvvasolutions__Interfaces__c(skyvvasolutions__Name__c='OppCDD'+dt+'',skyvvasolutions__Type__c='OutBound',skyvvasolutions__Status__c='Deployed',skyvvasolutions__Source_Name__c='OPPX',
            skyvvasolutions__Query__c='select id,name from Opportunity',skyvvasolutions__Integration__c=integ.Id,skyvvasolutions__AdapterId__c=a.Id);
        skyvvasolutions__Interfaces__c intfAdp=new skyvvasolutions__Interfaces__c(Schedule_Outbound__c=true,skyvvasolutions__Name__c='ItemCDD'+dt+'',skyvvasolutions__Type__c='OutBound',skyvvasolutions__Status__c='Deployed',skyvvasolutions__Source_Name__c='ITMX',
            skyvvasolutions__Query__c='select id, Quantity from OpportunityLineItem',skyvvasolutions__Integration__c=integ.Id,skyvvasolutions__AdapterId__c=a.Id);
                    
        */
        skyvvasolutions__Interfaces__c intfIntg=new skyvvasolutions__Interfaces__c(LastRun__c=dt,Schedule_Outbound__c=true,skyvvasolutions__Name__c='Integ'+dt+'',skyvvasolutions__Type__c='OutBound',skyvvasolutions__Status__c='Deployed',skyvvasolutions__Source_Name__c='INTEGX',
            skyvvasolutions__Query__c='select id, name from skyvvasolutions__Integration__c',skyvvasolutions__Integration__c=integ.Id,skyvvasolutions__AdapterId__c=a.Id);
        skyvvasolutions__Interfaces__c infInterfac=new skyvvasolutions__Interfaces__c(skyvvasolutions__Name__c='Interface'+dt+'',skyvvasolutions__Type__c='OutBound',skyvvasolutions__Status__c='Deployed',skyvvasolutions__Source_Name__c='INTFX',
            skyvvasolutions__Query__c='select id,name from skyvvasolutions__Interfaces__c',skyvvasolutions__Integration__c=integ.Id,skyvvasolutions__AdapterId__c=a.Id);
        skyvvasolutions__Interfaces__c intfAdp=new skyvvasolutions__Interfaces__c(Schedule_Outbound__c=true,skyvvasolutions__Name__c='Map'+dt+'',skyvvasolutions__Type__c='OutBound',skyvvasolutions__Status__c='Deployed',skyvvasolutions__Source_Name__c='MAPPX',
            skyvvasolutions__Query__c='select id,name from skyvvasolutions__IMapping__c',skyvvasolutions__Integration__c=integ.Id,skyvvasolutions__AdapterId__c=a.Id);
        insert new SObject[]{intfIntg,infInterfac,intfAdp};
        
        /**
        Hierarchy: Integration-Interface-Mapping
        */
        List<SObject> chains=new List<SObject>();
        chains.add(new skyvvasolutions__IChained_Interfaces__c(skyvvasolutions__ParentInterfaceId__c=intfIntg.Id, skyvvasolutions__ChildInterfaceId__c=infInterfac.Id,
        skyvvasolutions__Parent_Relationship_Name__c='skyvvasolutions__Integration__r',skyvvasolutions__Sequence__c=1));
        chains.add(new skyvvasolutions__IChained_Interfaces__c(skyvvasolutions__ParentInterfaceId__c=infInterfac.Id, skyvvasolutions__ChildInterfaceId__c=intfAdp.Id,
        skyvvasolutions__Sequence__c=2));
        insert chains;
        
        
        //skyvvasolutions__Integration__c
        List<skyvvasolutions__IMapping__c> lMapping=new List<skyvvasolutions__IMapping__c>();                 
        
        lMapping.add(new skyvvasolutions__IMapping__c(skyvvasolutions__Type__c='Flat',skyvvasolutions__Target__c='SKYVVA__PARENTID',skyvvasolutions__Target_Type__c='text',
             skyvvasolutions__Target_Object__c='skyvvasolutions__Integration__c',skyvvasolutions__Source_Long__c='Id',skyvvasolutions__Source_Type__c='Id',skyvvasolutions__Interface__c=intfIntg.Id));
        lMapping.add(new skyvvasolutions__IMapping__c(skyvvasolutions__Type__c='Flat',skyvvasolutions__Target__c='Name',skyvvasolutions__Target_Type__c='text',
             skyvvasolutions__Target_Object__c='skyvvasolutions__Integration__c',skyvvasolutions__Source_Long__c='NAME',skyvvasolutions__Source_Type__c='text',skyvvasolutions__Interface__c=intfIntg.Id));
        
        
        //skyvvasolutions__Interfaces__c
        lMapping.add(new skyvvasolutions__IMapping__c(skyvvasolutions__Type__c='Flat',skyvvasolutions__Target__c='SKYVVA__PARENTID',skyvvasolutions__Target_Type__c='text',
             skyvvasolutions__Target_Object__c='skyvvasolutions__Interfaces__c',skyvvasolutions__Source_Long__c='Id',skyvvasolutions__Source_Type__c='Id',skyvvasolutions__Interface__c=infInterfac.Id));
        lMapping.add(new skyvvasolutions__IMapping__c(skyvvasolutions__Type__c='Flat',skyvvasolutions__Target__c='Name',skyvvasolutions__Target_Type__c='text',
             skyvvasolutions__Target_Object__c='skyvvasolutions__Interfaces__c',skyvvasolutions__Source_Long__c='NAME',skyvvasolutions__Source_Type__c='text',skyvvasolutions__Interface__c=infInterfac.Id));
        
        
        //skyvvasolutions__Adapter__c
        lMapping.add(new skyvvasolutions__IMapping__c(skyvvasolutions__Type__c='Flat',skyvvasolutions__Target__c='Id',skyvvasolutions__Target_Type__c='text',
             skyvvasolutions__Target_Object__c='skyvvasolutions__Adapter__c',skyvvasolutions__Source_Long__c='Id',skyvvasolutions__Source_Type__c='Id',skyvvasolutions__Interface__c=intfAdp.Id));
        lMapping.add( new skyvvasolutions__IMapping__c(skyvvasolutions__Type__c='Flat',skyvvasolutions__Target__c='Name',skyvvasolutions__Target_Type__c='text',
             skyvvasolutions__Target_Object__c='skyvvasolutions__Adapter__c',skyvvasolutions__Source_Long__c='NAME',skyvvasolutions__Source_Type__c='text',skyvvasolutions__Interface__c=intfAdp.Id));
                          
        insert lMapping;  
        
		
	}

    static testMethod void cheduleCDD() {
    	Test.startTest();
        Skyvva_CDD_Scheduler.jobName='testCDD';
        ID jobId=Skyvva_CDD_Scheduler.schedule();
        // Get the information from the CronTrigger API object
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,
		NextFireTime
		FROM CronTrigger WHERE id = :jobId];
		
		// Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        // Verify the next time the job will run
		//System.assertEquals('2022-09-03 00:00:00',String.valueOf(ct.NextFireTime));
		
		Test.stopTest();
		
    }
    
    static testMethod void cheduleCDT() {
        Test.startTest();
        Skyvva_CDD_Scheduler.jobName='testCDT';
        ID jobId=Skyvva_CDT_Scheduler.schedule();
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,
        NextFireTime
        FROM CronTrigger WHERE id = :jobId];
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();
    }
}