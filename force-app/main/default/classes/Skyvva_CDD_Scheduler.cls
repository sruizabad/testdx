/*
* CDD Scheduler executes batch apex Skyvva_CDD_Worker
*/
global with sharing class Skyvva_CDD_Scheduler implements Schedulable {
	
	final static Integer defMinute=60;
	@testVisible
	static String jobName{
		get{
			return 'SkyvvaCDD:'+System.now().format('yyyyMMddHHmmssSSS');    
		}
		set;
	}
	
	//if flag true means schedule starts in apex console, not starndard scheduler page
	private Boolean fromConsole=false;
    
    /**
    * Contructor to set flag fromConsole, it was invoked in schedule()
    */
    public Skyvva_CDD_Scheduler(Boolean fromConsole){
        this.fromConsole=fromConsole;
    }
	
    global void execute(SchedulableContext SC){
        
        try{
            List<skyvvasolutions__Interfaces__c> interfaces=OutboundHelper.getScheduledInterfaces();
            Integer freeJob=OutboundHelper.getFreeBatchJob();
            
            System.debug('>>>Skyvva_CDD_Scheduler.execute>interfaces: '+interfaces.size()+'>freeJob: '+freeJob);
            if(interfaces.size()>0 && freeJob>0)Skyvva_CDD_Worker.executeBatch(interfaces);
        }
        finally{
            //reschedule job (in case use like every nbOfMinutes)
            if(fromConsole){
	            System.abortJob(SC.getTriggerId());
	            String newJobId = schedule();
            }
        }
        
        
    }
    
    //Start schedule in console
    public static Id schedule(){
        //the job already created before
        CDD_CDT_Config__c cs=CDD_CDT_Config__c.getValues('SC_CDD_JOB_ID');
        if(cs==null)cs=new CDD_CDT_Config__c(Name='SC_CDD_JOB_ID');
        
        CDD_CDT_Config__c timeInterval=CDD_CDT_Config__c.getValues('SC_CDD_TIME_INTERVAL_MINUTE');
        if(timeInterval==null){
            timeInterval= new CDD_CDT_Config__c(Name='SC_CDD_TIME_INTERVAL_MINUTE', Value__c=''+defMinute);
            insert timeInterval;
        }
        
        Integer minutes= Integer.valueOf(timeInterval.value__c);
        
        CronTrigger[] cts = [select Id,State,NextFireTime From CronTrigger where Id=:cs.Value__c];
            
        if(cts.size()>0) { 
            //the job is running -> no new job create for this interface and return this jobId
            System.debug('>>>Job already existing running job for this interface '+cts);
            if(cts[0].NextFireTime != null)return cs.Value__c;
            //the job exist but not running anymore, so delete it from the list
            else System.abortJob(cs.Value__c);
        }
        
        //job not yet created or existing job but not running, then create a job for the interface          
         
        //create a scheduled job to run at time "nbOfMinutes" later
        DateTime d = DateTime.now().addMinutes(minutes);
        String cronExpression = (d.second()+' '+d.minute()+' '+d.hour()+' '+d.day()+ ' '+d.month()+' ? '+d.year());
        Id theJobId = System.schedule(jobName, cronExpression, new Skyvva_CDD_Scheduler(true));
        //store the created jobId to the intetface to avoid multi-job for same interface
        cs.Value__c = theJobId; 
        upsert cs;
        
        system.debug('>Skyvva_CDD_Scheduler.schedule:>JobId:'+theJobId);
        
        return theJobId;
        
    }
    
}