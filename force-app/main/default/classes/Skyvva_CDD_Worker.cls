/**
* Outbound
* Change Data Dectection
* number of batch execution will managed by custom iterator:
*   scheduled-interfaces.size()*[(totalCDD/OutboundHelper.defMaxDMLrows)+(math.mod(totalCDD, OutboundHelper.defMaxDMLrows))];
*/
global with sharing class Skyvva_CDD_Worker implements Database.Batchable<SObject>, Database.Stateful{
    
    final static Integer MAX_SCOPE_SIZE=1;
    global DateTime dtUpperLimit;
    
    //final private skyvvasolutions__InterfaceGroup__c ig;
    
    private List<skyvvasolutions__Interfaces__c> interfaces;
    private Map<Id,skyvvasolutions__Interfaces__c> interfaceOutbounds;
    
    
    //test
    private Map<String,Integer> testProperty;
    //
    /*
    public Skyvva_CDD_Worker(skyvvasolutions__InterfaceGroup__c ig, Map<String,Integer> testProperty){
        
        this.testProperty=testProperty;
        this.dtUpperLimit=System.now().addSeconds(-10);
        this.ig=ig;
    }
    
    
    //Invoke at CDD-Scheduler
    public Skyvva_CDD_Worker(skyvvasolutions__InterfaceGroup__c ig){
        
        this.dtUpperLimit=System.now().addSeconds(-10);
        this.ig=ig;
    }
    */
    
    
    public Skyvva_CDD_Worker(List<skyvvasolutions__Interfaces__c> interfaces){
    	this.dtUpperLimit=OutboundHelper.getTimestampHigh();
    	this.interfaces=interfaces;
    }
    
    /*Just for test function*/
    public Skyvva_CDD_Worker(List<skyvvasolutions__Interfaces__c> interfaces, Map<String,Integer> testProperty){
        
        this(interfaces);
        this.testProperty=testProperty;
        
    }
    
    global Iterable<sObject> start(Database.BatchableContext context) {
        
        //Product and Price
        //this.interfaces = OutboundHelper.getInterfacesForCDDworker(ig);
        
        Set<Id> ids=new Set<Id>();
        for(skyvvasolutions__Interfaces__c f: this.interfaces){
        	ids.add(f.skyvvasolutions__Integration__c);
        }
        
        //All inerfaces-outbound of integration, They can be ichains of 
        interfaceOutbounds=OutboundHelper.getAllInterfaceOutbounds(ids);
        
        
        if(this.testProperty!=null)OutboundHelper.testProperty=testProperty;
        System.debug('>>>Batch>start>dtUpperLimit: '+dtUpperLimit.format('MM-dd-yyyy hh:mm:ss a')+'>this.testProperty: '+this.testProperty);
        
        
        //CustomIterable(List<Interfaces__c> interfacesInIG, Map<Id,Interfaces__c> infOfIntegration, DateTime dtUpperLimit)
        return new CustomIterable(this.interfaces.deepClone(true,false,false), interfaceOutbounds, this.dtUpperLimit);
        
    }
    
     global void execute(Database.BatchableContext context, List<SObject> scope) {//List<Interface>
        /*
        Query Row Limit 50000
        DML Row Limit 10000
        
        Note: if there is an unhandle error in this scope every setting will rolback to previous state
        */
        
        //test
        if(this.testProperty!=null)OutboundHelper.testProperty=testProperty;
        //
        
        
        System.debug('>>>Batch>execute>dtUpperLimit: '+dtUpperLimit.format('MM-dd-yyyy hh:mm:ss a')+'>this.testProperty: '+this.testProperty+'>interfaces-size:'+interfaces.size());
        
        if(interfaces.size()>0){
            //This interface can be interface of previous failed (Issue of QueryRow/DML Limit/...)
            //So it is still in the list to find CDD agains with new UpperTime
            skyvvasolutions__Interfaces__c interfaceOut=interfaces.get(0);
            
            System.debug('>>>Batch>execute:interfaceOut: '+interfaceOut.skyvvasolutions__Name__c);
            
            DateTime newDTUpperLimit=OutboundHelper.findCDDAndUpsert(interfaceOut,interfaceOutbounds,dtUpperLimit);
            
            //Faile due to custom limit exception, limit row and newDTUpperLimit=(dtUpperLimit-interfaceOut.LastRun)/2
            //then set new value of dtUpperLimit for next execution of batch
            if(newDTUpperLimit<dtUpperLimit)dtUpperLimit=newDTupperLimit;
            //Here every thing success, Remove the interface from list
            else interfaces.remove(0);
            
            
            
            //seetting test
            if(this.testProperty!=null)testProperty.put('TestMaxDMLrows', testProperty.get('TestMaxDMLrows')-1);
            //
        }
        
        
     }
     
     global void finish(Database.BatchableContext context) {
        
        System.debug('>>>Batch>finish>dtUpperLimit: '+dtUpperLimit.format('MM-dd-yyyy hh:mm:ss a')+'>this.testProperty: '+this.testProperty);
     }
     
     
     public static ID executeBatch(List<skyvvasolutions__Interfaces__c> interfaceOuts){
     	return Database.executeBatch(new Skyvva_CDD_Worker(interfaceOuts) ,MAX_SCOPE_SIZE);
     }
     
     
//iterable
global with sharing class CustomIterable implements iterable<sObject>{
    
    List<skyvvasolutions__Interfaces__c> interfacesx{get;set;}
    
    public CustomIterable(List<skyvvasolutions__Interfaces__c> interfacesInIG, Map<Id,skyvvasolutions__Interfaces__c> infOfIntegration, DateTime dtUpperLimit){
        
        this.interfacesx=interfacesInIG;
        //add fake interface to list of interfaces
        OutboundHelper.extendExecute(this.interfacesx, infOfIntegration, dtUpperLimit);
        
    }
    global Iterator<sObject> Iterator(){
        return new CustomIterator(interfacesx);
    }
}
    
//Iterator
global class CustomIterator implements Iterator<sObject>{
    
    
    List<skyvvasolutions__Interfaces__c> sObjects {get; set;}
    Integer i {get; set;}
    
    public CustomIterator(List<skyvvasolutions__Interfaces__c> sObjects){
        this.sObjects=sObjects;
        i = 0; 
   }
   
   global boolean hasNext(){
    
       if(i >= sObjects.size()) {
           return false; 
       } else {
           return true; 
       }
   }    

   global sObject next(){
        i++;
        return sObjects[i-1]; 
   }
   
}
}