global with sharing class Skyvva_CDT_Scheduler implements Schedulable {
    
    final static Integer defMinute=60;
    static String jobName{
    	get{
    		return 'SkyvvaCDT:'+System.now().format('yyyyMMddHHmmssSSS');
    	}
    }
    
    //if flag true means schedule starts in apex console
    private Boolean fromConsole=false;
    /**
    * Contructor to set flag fromConsole, it was invoke in schedule()
    */
    public Skyvva_CDT_Scheduler(Boolean fromConsole){
    	this.fromConsole=fromConsole;
    }
    
    global void execute(SchedulableContext SC){
        
        try{
            List<skyvvasolutions__Interfaces__c> interfaces=OutboundHelper.getScheduledInterfaces();
            Skyvva_CDT_Worker.executeBatch(interfaces);
        }
        finally{
            //reschedule job (in case use like every nbOfMinutes)
            if(fromConsole){
	            System.abortJob(SC.getTriggerId());
	            String newJobId = schedule();
            }
        }
        
        
    }
    
    
    public static Id schedule(){
        //the job already created before
        CDD_CDT_Config__c cs=CDD_CDT_Config__c.getValues('SC_CDT_JOB_ID');
        if(cs==null)cs=new CDD_CDT_Config__c(Name='SC_CDT_JOB_ID');
        
        CDD_CDT_Config__c timeInterval=CDD_CDT_Config__c.getValues('SC_CDT_TIME_INTERVAL_MINUTE');
        if(timeInterval==null){
        	timeInterval= new CDD_CDT_Config__c(Name='SC_CDT_TIME_INTERVAL_MINUTE', Value__c=''+defMinute);
        	insert timeInterval;
        }
        
        Integer minutes= Integer.valueOf(timeInterval.value__c);
                
        CronTrigger[] cts = [select Id,State,NextFireTime From CronTrigger where Id=:cs.Value__c];
            
        if(cts.size()>0) { 
            //the job is running -> no new job create for this interface and return this jobId
            System.debug('>>>Job already existing running job for this interface '+cts);
            
            if(cts[0].NextFireTime != null)return cs.Value__c;
            //the job exist but not running anymore, so delete it from the list
            else System.abortJob(cs.Value__c);
        }
          
        //create a scheduled job to run at time "nbOfMinutes" later
        DateTime d = DateTime.now().addMinutes(minutes);
        String cronExpression = (d.second()+' '+d.minute()+' '+d.hour()+' '+d.day()+ ' '+d.month()+' ? '+d.year());
        Id theJobId = System.schedule(jobName, cronExpression, new Skyvva_CDT_Scheduler(true));
        //store the created jobId to the intetface to avoid multi-job for same interface
        cs.Value__c = theJobId; 
        upsert cs;
        
        system.debug('>Skyvva_CDT_Scheduler.schedule:JobId:'+theJobId);
        return theJobId;
        
    }
    
}