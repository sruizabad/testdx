/*
* Outbound, CDT Worker
* Invoke callout of message status New
* One apex job is one interface
* number of execution base on interface.Number_of_Records_Per_Batch__c,
* if it is not specify default 1 (message/execute)
*/
global with sharing class Skyvva_CDT_Worker implements Database.Batchable<SObject>,Database.AllowsCallouts{
	
	final static String ns=OutboundHelper.ns;
	final static Integer MAX_SCOPE_SIZE=1;
	final static String msgStatus='New';
	//ORDER BY lastmodifieddate ASC to query older record
	final static String query='select  '+ns+'Related_To__c  from  '+ns+'IMessage__c  where  '+ns+'Interface__c=:interfaceId  AND '+ns+'Status__c=:statusNew ORDER BY lastmodifieddate ASC';
	
	private String queryLimit{
		get{
		
			CDD_CDT_Config__c cs=CDD_CDT_Config__c.getValues('CDT_WORKER_QUERY_LIMIT');
			if(cs!=null && String.isNotBlank(cs.Value__c)){
				try{
					return '  LIMIT  '+Integer.valueOf(cs.Value__c);
				}catch(Exception e){}
				
			}
			
			return '  LIMIT  200';
		
		}
	}
	private List<skyvvasolutions__Interfaces__c> interfaces;
	private skyvvasolutions__Interfaces__c interfaceOut=null;
	
	final List<SObject> msgs=new List<SObject>();
	final List<String> objIds=new List<String>();
	
	public Skyvva_CDT_Worker(List<skyvvasolutions__Interfaces__c> interfaces){
		this.interfaces=interfaces;
		this.interfaceOut=interfaces.remove(0);
		
		clear();
	}
	
	void clear(){
		objIds.clear();
        msgs.clear();
	}
	
	global Iterable<sObject> start(Database.BatchableContext context) {
		
		//variables are visible to dynamic query
		ID interfaceId=interfaceOut.Id;
        String statusNew=msgStatus;
		
		return Database.getQueryLocator(query + queryLimit);
	}
	
	global void execute(Database.BatchableContext context, List<SObject> scope){
		
		clear();
		
		for(SObject msg:scope){
			if(msg.get(+ns+'Related_To__c')!=null)objIds.add(''+msg.get(+ns+'Related_To__c'));
			
			msgs.add(msg);
		}
		
		if(objIds.size()>0){
			skyvvasolutions.IServices.invokeCallout(interfaceOut.Id, objIds, 'SYNC');
			//specify false for this parameter and a record fails, the remainder of the DML operation can still succeed
			Database.DeleteResult[] rs=Database.delete(msgs,false);
			System.debug('>>>execute: '+rs);
		}
		
	}
	
	/*Recurse apex batch for available interface*/
	global void finish(Database.BatchableContext context){
		
		executeBatch(this.interfaces);
	}
	
	public static ID executeBatch(List<skyvvasolutions__Interfaces__c> interfaces){
		
		Integer freeJob=OutboundHelper.getFreeBatchJob();
		System.debug('>>>Skyvva_CDT_Worker.executeBatch>interfaces: '+interfaces.size()+'>freeJob: '+freeJob);
		
		if(interfaces.size()==0 ||freeJob<=0)return null;
		
		Decimal maxScopSize=interfaces[0].skyvvasolutions__Number_of_Records_Per_Batch__c;
		System.debug('>>>Skyvva_CDT_Worker.executeBatch: '+maxScopSize);
        return Database.executeBatch(new Skyvva_CDT_Worker(interfaces), (maxScopSize>1? maxScopSize.intValue() : MAX_SCOPE_SIZE));
	}
	
	

}