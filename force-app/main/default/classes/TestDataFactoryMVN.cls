/*
* TestDataFactoryMVN
* Created By:    Roman Lerman
* Created Date:  3/5/2013
* Description:   This is the data factory used to generate data by all of the test classes
*/
public class TestDataFactoryMVN {
    public static Integer TEST_DATA_SIZE = 25;
    public static String interactionRecordTypeId ;
    public static String requestRecordTypeId ;
    public static User runningUser = [select Id,Name from User where Id = :UserInfo.getUserId()];

    static {
        List<RecordType> caseTypes = [select Id,DeveloperName from RecordType where SObjectType='Case'];
        for(RecordType type : caseTypes) {
            if(type.DeveloperName == 'Interaction_MVN') {
                interactionRecordTypeId = type.Id;
            } else if(type.DeveloperName == 'Request_MVN') {
                requestRecordTypeId = type.Id;
            }
        }
    }

    public static Customer_Attribute_BI__c customerAttribute {
        get {
            if (customerAttribute == null) {
                customerAttribute = new Customer_Attribute_BI__c(Name = 'Test State', Type_BI__c = 'ADDR_State'); 
                insert customerAttribute;
            }
            return customerAttribute;
        }
        private set;
    }
    
    public static List<Id> setupArticles(){
        FAQ_MVN__kav faq = new FAQ_MVN__kav(Title='test1', Language='en_US', URLName='testfaq-123');
        insert faq;
        
        FAQ_MVN__kav faq2 = new FAQ_MVN__kav(Title='test2', Language='en_US', URLName='testfaq2-123');
        insert faq2;
        
        Set<Id> faqSet = new Set<Id>();
        faqSet.add(faq.Id);
        faqSet.add(faq2.Id);
        
        Medical_Letter_MVN__kav medicalLetter = new Medical_Letter_MVN__kav(Title='test1', Language='en_US', URLName='testml-123');
        insert medicalLetter;
        
        Medical_Letter_MVN__kav medicalLetter2 = new Medical_Letter_MVN__kav(Title='test2', Language='en_US', URLName='testml2-123');
        insert medicalLetter2;
        
        Set<Id> medicalLetterSet = new Set<Id>();
        medicalLetterSet.add(medicalLetter.Id);
        medicalLetterSet.add(medicalLetter2.Id);
        
        List<FAQ_MVN__kav> faqs = [select KnowledgeArticleId from FAQ_MVN__kav where Id in :faqSet];
        List<Medical_Letter_MVN__kav> medicalLetters = [select KnowledgeArticleId from Medical_Letter_MVN__kav where Id in :medicalLetterSet];
        
        KbManagement.PublishingService.publishArticle(faqs[0].KnowledgeArticleId, false);
        KbManagement.PublishingService.publishArticle(faqs[1].KnowledgeArticleId, false);
        KbManagement.PublishingService.publishArticle(medicalLetters[0].KnowledgeArticleId, false);
        KbManagement.PublishingService.publishArticle(medicalLetters[1].KnowledgeArticleId, false);
        
        List<Id> idList = new List<Id>();
        idList.add(faq.Id);
        idList.add(faq2.Id);
        idList.add(medicalLetter.Id);
        idList.add(medicalLetter2.Id);
        
        return idList;
    }
    
    public static List<Case> createInteractions() {
        List<Case> cases = new List<Case>();
        for(Integer i=0; i<TEST_DATA_SIZE; i++) {
            cases.add(new Case(RecordTypeId = interactionRecordTypeId,
                      Status='Open')
            );
        }
        insert cases;
        
        return cases;
    }
    
    public static List<Case> createRequests(List<Case> interactions, String status){
         List<Case> requests = new List<Case>();
         for (Case c : interactions) {
             Case request = c.clone(false);
             request.RecordTypeId = requestRecordTypeId;
             request.ParentId = c.id;
             request.Status = status;
             
             requests.add(request);
             requests.add(request.clone(false));
         }
         insert requests;
        
         return requests;
    }
    
    public static Account createTestConsumer() {
        Account personAccount = new Account();
        personAccount.FirstName = 'Peter';
        personAccount.LastName = 'Smith';
        personAccount.RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Consumer_MVN' limit 1].Id;
        personAccount.PersonMobilePhone = '4444444444';
        personAccount.Country_Code_BI__c = 'DE';
        
        insert personAccount;
        
        return personAccount;
    }

    public static Account createTestBusinessAccount() {
        Account businessAccount = new Account();
        businessAccount.Name = 'Smith Pharmacy';
        businessAccount.RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Organization_vod'][0].Id;
        businessAccount.Phone = '3333333333';
        businessAccount.Fax = '1231231212';
        businessAccount.CRC_Email_MVN__c = 'admin@smithpharmacy.com';
        businessAccount.Country_Code_BI__c = 'DE';
        businessAccount.Specialty_BI__c = TestDataFactoryMVN.createWorkplaceSpecialty().Id;
        businessAccount.OKWorkplace_Class_BI__c = TestDataFactoryMVN.createWorkplaceClass().Id;
        
        insert businessAccount;
        
        return businessAccount;
    }

    public static Account createTestHCP() {
        Account personAccount = new Account();
        personAccount.FirstName = 'Test';
        personAccount.LastName = 'Account';
        personAccount.Middle_vod__c = 'Little';
        personAccount.RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Professional_vod'][0].Id;
        personAccount.Phone = '5555555555';
        personAccount.Country_Code_BI__c = 'DE';
        personAccount.OK_Status_Code_BI__c = 'Valid';

        insert personAccount;
        
        return personAccount;
    }

    public static List<Account> createTestHCPs() {
        List<Account> accountList = new List<Account>();        
        Id rtId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Professional_vod'][0].Id;
        for (Integer i=0; i<5; i++) {
            accountList.add( new Account(FirstName = 'Test', 
                                         LastName = 'Account'+String.valueOf(i), 
                                         Middle_vod__c = 'Little',
                                         RecordTypeId = rtId,
                                         Phone = '5555555555',
                                         Country_Code_BI__c = 'US',
                                         OK_Status_Code_BI__c = 'Valid'));
        }

        insert accountList;

        return accountList;
    }
    
    public static Fulfillment_MVN__c createTestFulfillment(Id accountId, Id caseId){
        Fulfillment_MVN__c fulfillment = new Fulfillment_MVN__c();
        fulfillment.Account_Name_MVN__c = accountId;
        fulfillment.Case_MVN__c = caseId;
        fulfillment.RecordTypeId = [select Id from RecordType where SObjectType = 'Fulfillment_MVN__c' and DeveloperName = 'Fulfillment' limit 1].Id;
        insert fulfillment;
        
        return fulfillment;
    }
    
    public static Fulfillment_MVN__c createClosedTestFulfillment(Id accountId, Id caseId){
        Fulfillment_MVN__c fulfillment = new Fulfillment_MVN__c();
        fulfillment.Account_Name_MVN__c = accountId;
        fulfillment.Case_MVN__c = caseId;
        fulfillment.RecordTypeId = [select Id from RecordType where SObjectType = 'Fulfillment_MVN__c' and DeveloperName = 'Fulfillment' limit 1].Id;
        fulfillment.Status_MVN__c = 'Closed';
        insert fulfillment;
        
        return fulfillment;
    }
    
    public static Case createTestCase() {
        return createNewTestCase(true);
    }

    public static Case createNewTestCase(Boolean createAccount) {
        Case testCase = new Case( RecordTypeId = interactionRecordTypeId );

        if (createAccount) {
            Account personAccount = createTestConsumer();        
            testCase.AccountId = personAccount.Id;
            testCase.Address_MVN__c = createTestAddress(personAccount).Id;
        }

        insert testCase;
        
        return testCase;
    }

    public static Case createTestCase(Id accountId) {
        Case testCase = new Case();
        testCase.AccountId = accountId;
        testCase.RecordTypeId = interactionRecordTypeId;
        testCase.User_Country_Code_MVN__c = 'DE';

        insert testCase;
        
        return testCase;
    }
    
    public static Case createClosedTestCase() {
        Account personAccount = createTestConsumer();
        
        Case testCase = new Case();
        testCase.AccountId = personAccount.Id;
        testCase.ContactId = [select PersonContactId from Account where Id = :personAccount.Id].PersonContactId;
        testCase.RecordTypeId = interactionRecordTypeId;
        testCase.Address_MVN__c = createTestAddress(personAccount).Id;
        testCase.Status = 'Closed';
        testCase.User_Country_Code_MVN__c = 'DE';

        insert testCase;
        
        return testCase;
    }
    
    public static Case createClosedTestRequest(Case parentCase) {        
        Case testCase = new Case();
        testCase.ParentId = parentCase.Id;
        testCase.AccountId = parentCase.AccountId;
        testCase.ContactId = [select PersonContactId from Account where Id = :parentCase.AccountId].PersonContactId;
        testCase.RecordTypeId = requestRecordTypeId;
        testCase.Address_MVN__c = parentCase.Address_MVN__c;
        testCase.Status = 'Closed';
        testCase.User_Country_Code_MVN__c = 'DE';

        insert testCase;
        
        return testCase;
    }
    
    public static Case createTestRequest(Case parentCase) {
        Case testCase = new Case();
        testCase.ParentId = parentCase.Id;
        testCase.AccountId = parentCase.AccountId;
        testCase.ContactId = parentCase.ContactId;
        testCase.RecordTypeId = requestRecordTypeId;
        testCase.Address_MVN__c = parentCase.Address_MVN__c;
        insert testCase;
        
        return testCase;
    }
    
    public static Address_vod__c createTestAddress(Account acct){
        Address_vod__c address = new Address_vod__c(Name = 'Hallshted 123');
        address.city_vod__c = 'Munich';
        address.zip_vod__c = '94941';
        address.OK_State_Province_BI__c = customerAttribute.Id;
        address.Country_Code_BI__c = 'DE';
        address.Account_vod__c = acct.Id;
        address.Phone_vod__c = '1111111111';
        address.Primary_vod__c = true;
        
        insert address;
        
        return address;
    }

    public static Address_vod__c createTestPrimaryAddress(Account acct){
        Address_vod__c address = new Address_vod__c(Name = 'Hallshted 123');
        address.city_vod__c = 'Munich';
        address.zip_vod__c = '94941';
        address.OK_State_Province_BI__c = customerAttribute.Id;
        address.Country_Code_BI__c = 'DE';
        address.Account_vod__c = acct.Id;
        address.Phone_vod__c = '1111111111';
        address.Primary_vod__c = true;
        insert address;
        
        return address;
    }
    
    public static Case createTestEmailRequest() {
        Case testCase = new Case();
        testCase.Origin = 'Email';
        testCase.RecordTypeId = requestRecordTypeId;
        testCase.Subject = 'Test';
        testCase.Description = 'Test';
        insert testCase;
        
        return testCase;
    }

    public static User createTestAdmin(){
        User user;
        System.runAs(runningUser) {
            user = new User(alias='ccusysad', email= 'callcentertestusermvn@callcenter.com', 
                                    emailencodingkey='UTF-8', firstName='Reginald', lastname='Wellington', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = [select Id from Profile where Name = 'System Administrator'].Id, 
                                    isActive = true, timezonesidkey='America/Los_Angeles', username='callcentertestusermvn@callcenter.com',
                                    Default_Article_Language_MVN__c = KnowledgeArticleVersion.Language.getDescribe().getPicklistValues()[0].getValue());
            insert user;
        }
        
        return user;
    }

    public static Customer_Attribute_BI__c createIndividualClass(){
        Customer_Attribute_BI__c individualClass = new Customer_Attribute_BI__c();
        individualClass.Name = 'Test Individual Specialty';
        individualClass.OK_Type_Code_BI__c = 'TYT';
        individualClass.RecordTypeId = [select Id from RecordType where SObjectType='Customer_Attribute_BI__c' and DeveloperName = 'OK_Local_Attribute' limit 1].Id;
        individualClass.CODE_END_VALID_DATE_OK_BI__c = '9999-12';

        insert individualClass;

        return individualClass;
    }

    public static Customer_Attribute_BI__c createIndividualSpecialty(){
        Customer_Attribute_BI__c individualSpecialty = new Customer_Attribute_BI__c();
        individualSpecialty.Name = 'Test Individual Specialty';
        individualSpecialty.Type_BI__c = 'ACCT_Specialty';
        individualSpecialty.RecordTypeId = [select Id from RecordType where SObjectType='Customer_Attribute_BI__c' and DeveloperName = 'OK_Local_Attribute' limit 1].Id;

        insert individualSpecialty;

        return individualSpecialty;
    }

    public static Customer_Attribute_BI__c createWorkplaceClass(){
        Customer_Attribute_BI__c workplaceClass = new Customer_Attribute_BI__c();
        workplaceClass.Name = 'Test Workplace Class';
        workplaceClass.OK_Type_Code_BI__c = 'FAD';
        workplaceClass.RecordTypeId = [select Id from RecordType where SObjectType='Customer_Attribute_BI__c' and DeveloperName = 'OK_International_Attrubite' limit 1].Id;
        workplaceClass.CODE_END_VALID_DATE_OK_BI__c = '9999-12';

        insert workplaceClass;

        return workplaceClass;
    }

    public static Customer_Attribute_BI__c createWorkplaceSpecialty(){
        Customer_Attribute_BI__c workplaceSpecialty = new Customer_Attribute_BI__c();
        workplaceSpecialty.Name = 'Test Workplace Specialty';
        workplaceSpecialty.OK_Type_Code_BI__c = 'SP';
        workplaceSpecialty.RecordTypeId = [select Id from RecordType where SObjectType='Customer_Attribute_BI__c' and DeveloperName = 'OK_Local_Attribute' limit 1].Id;
        workplaceSpecialty.CODE_END_VALID_DATE_OK_BI__c = '9999-12';

        insert workplaceSpecialty;

        return workplaceSpecialty;
    }

    public static User createTestCallCenterUser(){
        User user;
        System.runAs(runningUser) {
            Profile p = [select Id from Profile where Name='CRC - Customer Request Agent']; 
            UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Germany'];
            
            user = new User(Alias = 'heiditst', Email='heidibergen@oehringertest.com', 
                EmailEncodingKey='UTF-8', FirstName = 'Heidi', LastName='Bergen', LanguageLocaleKey='de', 
                LocaleSidKey='de', ProfileId = p.Id, UserRoleId = ur.Id, 
                TimeZoneSidKey='Europe/Berlin', UserName='heidibergen@oehringertest.com',
                Country_Code_BI__c = 'DE');

            insert user;
        }

        return user;
    }

    public static User createSecondaryTestCallCenterUser(){
        User user;
        System.runAs(runningUser) {
            Profile p = [select Id from Profile where Name='CRC - Customer Request Agent']; 
            UserRole ur = [select Id from UserRole where DeveloperName = 'CRC_Germany'];
            
            user = new User(Alias = 'heidits2', Email='heidibergen2@boehringertest.com', 
                EmailEncodingKey='UTF-8', FirstName = 'Heidi', LastName='Bergen2', LanguageLocaleKey='de', 
                LocaleSidKey='de', ProfileId = p.Id, UserRoleId = ur.Id, 
                TimeZoneSidKey='Europe/Berlin', UserName='heidibergen2@boehringertest.com',
                Country_Code_BI__c = 'DE');

            insert user;
        }

        return user;
    }

    public static Call2_vod__c createSavedCall(){
        Call2_vod__c call = new Call2_vod__c();
        call.Status_vod__c = 'Saved_vod';

        insert call;

        return call;

    }

    public static Campaign_vod__c createTestCampaign(){
        Campaign_vod__c campaign = new Campaign_vod__c();
        campaign.Name = 'Test Campaign';
        campaign.Active_BI__c = true;

        insert campaign;

        return campaign;
    }

    public static Campaign_Target_vod__c createTestCampaignTarget(Campaign_vod__c campaign, Account account){
        Campaign_Target_vod__c target = new Campaign_Target_vod__c(Campaign_vod__c = campaign.Id);

        target.Target_Account_vod__c = account.Id;

        insert target;

        return target;
    }

    public static List<Campaign_Target_vod__c> createTestCampaignTargets( Campaign_vod__c campaign, List<Account> accounts) {        
        List<Campaign_Target_vod__c> targets = new List<Campaign_Target_vod__c>();
        
        for (Account acct : accounts) {
            targets.add( new Campaign_Target_vod__c(Campaign_vod__c = campaign.Id, Target_Account_vod__c = acct.Id));
        }
        insert targets;
        return targets;
    }

    public static Product_vod__c createTestProduct(String countryCode){
        Product_vod__c product = new Product_vod__c();

        product.Name = 'Pradaxa 30mg';
        product.Country_Code_BI__c = countryCode;
        product.Product_Type_vod__c = UtilitiesMVN.splitCommaSeparatedString(Service_Cloud_Settings_MVN__c.getInstance().Order_Search_Product_Types_MVN__c)[0];
        product.Description_vod__c = 'Product for testing';
        product.Orderable_MVN__c = true;
        product.End_Date_MVN__c = null;
        product.SAP_Material_Number_MVN__c = 'anSapNumber';
        insert product;

        return product;
    }

    public static Call2_Sample_vod__c createTestCallSample(Call2_vod__c call, Product_vod__c product){
        Call2_Sample_vod__c sample = new Call2_Sample_vod__c(Call2_vod__c = call.Id, Product_vod__c = product.Id);
        insert sample;
        return sample;
    } 

    public static Customer_Attribute_BI__c createTestAttribute(String OKTypeCodeBI, String country){

        Customer_Attribute_BI__c attribute = new Customer_Attribute_BI__c();
        attribute.Name = 'Test';
        attribute.OK_Type_Code_BI__c = OKTypeCodeBI;
        attribute.OK_Country_ID_BI__c = country;
        insert attribute;
        return attribute;
    }

    public static void createSettings(){
        Service_Cloud_Settings_MVN__c mainSettings = new Service_Cloud_Settings_MVN__c();
        
        mainSettings.Knowledge_Search_Article_Types_MVN__c = 'FAQ_MVN__kav, Medical_Letter_MVN__kav';
        mainSettings.Knowledge_Search_Max_Results_MVN__c = 50;
        mainSettings.Interaction_Record_Type_MVN__c = 'Interaction_MVN';
        mainSettings.Interaction_Create_Origin_MVN__c = 'Email';
        mainSettings.Interaction_Create_Case_Record_Type_MVN__c = 'Request_MVN';
        mainSettings.Interaction_Anonymize_Countries_MVN__c = 'US,DE';
        mainSettings.Request_Record_Type_MVN__c = 'Request_MVN';
        mainSettings.Person_Search_Default_Record_Type_MVN__c = 'Professional_vod';
        mainSettings.Person_Search_Record_Types_MVN__c = 'Professional_vod, Consumer_MVN';
        mainSettings.Administrator_Email_MVN__c = 'test@test.com';
        mainSettings.Open_Status_MVN__c = 'Open';
        mainSettings.Do_Not_Default_Country_MVN__c = false;
        mainSettings.Person_Search_Default_Record_Type_MVN__c = 'All';
        mainSettings.SAP_Order_Country_Codes_MVN__c = 'DE';
        mainSettings.Order_Search_Product_Types_MVN__c = 'Promotional';
        mainSettings.Order_Restricted_Product_Types_MVN__c = 'Sample,BRC';
        mainSettings.Medical_Inquiry_Countries_MVN__c = 'DE, NL';
        mainSettings.HCP_Valid_to_Sample_Status_Codes_MVN__c = 'Valid';
        mainSettings.Temporary_Account_Hold_Period_Days_MVN__c = -2; //for batch job, need to simulate created date older than temp hold period
        mainSettings.State_Province_Filter_MVN__c = 'OK_Type_Code_BI__c = \'DPT\'';
        mainSettings.Workplace_Class_Filter_MVN__c = 'OK_Type_Code_BI__c = \'FAD\'';
        mainSettings.Workplace_Specialty_Filter_MVN__c = 'OK_Type_Code_BI__c = \'SP\'';
        mainSettings.Individual_Class_Filter_MVN__c = 'OK_Type_Code_BI__c = \'TYT\'';
        mainSettings.Individual_Specialty_Filter_MVN__c = 'OK_Type_Code_BI__c = \'ACCT\'';
        mainSettings.Role_Filter_MVN__c = 'OK_Type_Code_BI__c = \'TIH\'';
        mainSettings.DCR_Update_Change_Type_MVN__c = 'Update';
        mainSettings.Call_Submitted_Status_MVN__c = 'Submitted_vod';
        mainSettings.Call_Order_Delivery_Status_Ordered_MVN__c = 'Submitted';

        
        insert mainSettings;

        List<Case_Article_Fields_MVN__c> caseFields = new List<Case_Article_Fields_MVN__c>();
        caseFields.add(new Case_Article_Fields_MVN__c(Name='1',Case_Article_Data_Field_MVN__c='Product_ID_MVN__c',Knowledge_Article_Field_MVN__c='Product_ID_MVN__c',Knowledge_Article_Type_MVN__c='FAQ_MVN__kav'));
        caseFields.add(new Case_Article_Fields_MVN__c(Name='2',Case_Article_Data_Field_MVN__c='Product_ID_MVN__c',Knowledge_Article_Field_MVN__c='Product_ID_MVN__c',Knowledge_Article_Type_MVN__c='Medical_Letter_MVN__kav'));
        caseFields.add(new Case_Article_Fields_MVN__c(Name='3',Case_Article_Data_Field_MVN__c='Article_Indication_MVN__c',Knowledge_Article_Field_MVN__c='Indication_MVN__c',Knowledge_Article_Type_MVN__c='FAQ_MVN__kav'));
        caseFields.add(new Case_Article_Fields_MVN__c(Name='4',Case_Article_Data_Field_MVN__c='Article_Category_MVN__c',Knowledge_Article_Field_MVN__c='Category_MVN__c',Knowledge_Article_Type_MVN__c='FAQ_MVN__kav'));
        insert caseFields; 
        
        insert new Knipper_Settings__c();

        insert Case_Delivery_Method_Mapping_MVN__c.getInstance();

        Data_Connect_Setting__c dataConnector = new Data_Connect_Setting__c(Name='AdminEmail', Value__c='test@test.com');

        insert dataConnector;

        insert new Veeva_Settings_vod__c();
    }
}