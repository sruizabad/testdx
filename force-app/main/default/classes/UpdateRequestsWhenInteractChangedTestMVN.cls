/*
* UpdateRequestsWhenInteractChangedTestMVN
* Created By:    Roman Lerman
* Created Date:  3/4/2013
* Description:   This is the test class for UpdateRequestsWhenInteractionChangedMVN
*/
@isTest
private class UpdateRequestsWhenInteractChangedTestMVN {
	static Case cs;
	static Case request;
	static Case request2;
	
	static Case cs2;
	static Case request3;
	static Case request4;
	
	static List<Case> requestList;
	
	static Account personAccount;
	
	static{
		TestDataFactoryMVN.createSettings();
		
		requestList = new List<Case>();
		
		cs = TestDataFactoryMVN.createTestCase();
		request = TestDataFactoryMVN.createTestRequest(cs);
		request2 = TestDataFactoryMVN.createTestRequest(cs);
		
		cs2 = TestDataFactoryMVN.createTestCase();
		request3 = TestDataFactoryMVN.createTestRequest(cs2);
		request4 = TestDataFactoryMVN.createTestRequest(cs2);
		
		requestList.add(request);
		requestList.add(request2);
		requestList.add(request3);
		requestList.add(request4);
		
		personAccount = TestDataFactoryMVN.createTestConsumer();
	}
	
	static testMethod void testRequestUpdate(){
		Id addressId = TestDataFactoryMVN.createTestAddress(personAccount).Id;
		
		String email = 'johnsmith@test.com';
		String fax = '123-123-1221';
		String phone = '123-345-2323';
		
		cs.AccountId = personAccount.Id;
		cs.Address_MVN__c = addressId;
		
		cs.case_Account_Email_MVN__c = email;
		cs.case_Account_Fax_MVN__c = fax;
		cs.case_Account_Phone_MVN__c = phone;

		cs.Origin = 'Email';
		
		cs2.AccountId = personAccount.Id;
		cs2.Address_MVN__c = addressId;
		
		cs2.case_Account_Email_MVN__c = email;
		cs2.case_Account_Fax_MVN__c = fax;
		cs2.case_Account_Phone_MVN__c = phone;
		cs2.Origin = 'Email';
		
		List<Case> cases = new List<Case>();
		cases.add(cs);
		cases.add(cs2);
		
		Test.startTest();
			update cases;
		Test.stopTest();
		
		for(Case req:[select AccountId, Parent.AccountId, Address_MVN__c,
							 case_Account_Email_MVN__c, case_Account_Fax_MVN__c, case_Account_Phone_MVN__c, Requester_Type_MVN__c, Origin
							 from Case where Id in :requestList]){
			System.assertEquals(req.AccountId, personAccount.Id);
			System.assertEquals(req.Address_MVN__c, addressId);
			
			System.assertEquals(req.case_Account_Email_MVN__c, email);
			System.assertEquals(req.case_Account_Fax_MVN__c, fax);
			System.assertEquals(req.case_Account_Phone_MVN__c, phone);

			System.assertEquals(req.Origin, 'Email');
		}
	}

	static testMethod void testInteractionRequetorTypeUpdate(){
		
		String email = 'johnsmith@test.com';
		String fax = '123-123-1221';
		String phone = '123-345-2323';
		
		cs.AccountId = null;
		
		cs.case_Account_Email_MVN__c = email;
		cs.case_Account_Fax_MVN__c = fax;
		cs.case_Account_Phone_MVN__c = phone;

		
		cs.Origin = 'Email';
		
		cs2.AccountId = null;
		
		cs2.case_Account_Email_MVN__c = email;
		cs2.case_Account_Fax_MVN__c = fax;
		cs2.case_Account_Phone_MVN__c = phone;
		cs2.Origin = 'Email';
		
		
		List<Case> cases = new List<Case>();
		cases.add(cs);
		cases.add(cs2);
		update cases;

		cs2.Requester_Type_MVN__c = 'NewType';
		cs.Requester_Type_MVN__c = 'NewType';

		cases = new List<Case>();
		cases.add(cs);
		cases.add(cs2);

		Test.startTest();
			update cases;
		Test.stopTest();
		
		for(Case req:[select case_Account_Email_MVN__c, AccountID, ParentId, case_Account_Fax_MVN__c, case_Account_Phone_MVN__c, Requester_Type_MVN__c, Origin
							 from Case where Id in :requestList]){
			
			System.assertEquals(null,req.AccountId);
			System.assertEquals(req.case_Account_Email_MVN__c, email);
			System.assertEquals(req.case_Account_Fax_MVN__c, fax);
			System.assertEquals(req.case_Account_Phone_MVN__c, phone);

			System.assertEquals('NewType',req.Requester_Type_MVN__c);
			System.assertEquals(req.Origin, 'Email');
		}
	}
}