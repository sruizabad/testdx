/*
* UpdateRequestsWhenInteractionChangedMVN
* Created By:    Roman Lerman
* Created Date:  3/4/2013
* Description:   This class updates the Person and Address associated with the requests whenever
*				 those fields are updates on the corresponding Interaction.
*/
public with sharing class UpdateRequestsWhenInteractionChangedMVN implements TriggersMVN.HandlerInterface
{   
    public void execute(map<Id, Case> newCaseMap, map<Id, Case> oldCaseMap) {
    	Map <Id, Case> interactionsBeingUpdated = new Map <Id, Case> ();
    	
		for (Case newCase : newCaseMap.values()) {
			if (UtilitiesMVN.matchCaseRecordTypeIdToName(newCase.RecordTypeId, UtilitiesMVN.interactionRecordType)) {
				Case oldCase = oldCaseMap.get(newCase.Id);
				if (oldCase.AccountId != newCase.AccountId
					|| oldCase.Business_Account_MVN__c != newCase.Business_Account_MVN__c
					|| oldCase.Address_MVN__c != newCase.Address_MVN__c
					|| oldCase.case_Account_Email_MVN__c != newCase.case_Account_Email_MVN__c
					|| oldCase.case_Account_Fax_MVN__c != newCase.case_Account_Fax_MVN__c
					|| oldCase.case_Account_Phone_MVN__c != newCase.case_Account_Phone_MVN__c
				    || oldCase.Sales_Rep_MVN__c != newCase.Sales_Rep_MVN__c
				    || oldCase.Origin != newCase.Origin
				    || oldCase.Requester_Type_MVN__c != newCase.Requester_Type_MVN__c) {
					interactionsBeingUpdated.put(newCase.Id, newCase);
				}
			}
		}
		
		if (interactionsBeingUpdated.size() > 0) {
			List <Case> requestsToUpdate = [Select Id, ParentId, AccountId from Case where ParentId in :interactionsBeingUpdated.keySet() and isClosed=false];

			for (Case request : requestsToUpdate) {
				Case interaction = interactionsBeingUpdated.get(request.ParentId);
				
				request.AccountId = interaction.AccountId;
				request.Business_Account_MVN__c = interaction.Business_Account_MVN__c;
				request.Address_MVN__c = interaction.Address_MVN__c;
				request.case_Account_Email_MVN__c = interaction.case_Account_Email_MVN__c;
				request.case_Account_Fax_MVN__c = interaction.case_Account_Fax_MVN__c;
				request.case_Account_Phone_MVN__c = interaction.case_Account_Phone_MVN__c;
				request.Sales_Rep_MVN__c = interaction.Sales_Rep_MVN__c;
				request.Origin = interaction.Origin;
				request.Requester_Type_MVN__c = interaction.Requester_Type_MVN__c;
			}

			update requestsToUpdate;
		}
    }
    
    public void handle() {
        execute((Map<Id, Case>) trigger.newMap, (Map<Id, Case>) trigger.oldMap); 
    }
}