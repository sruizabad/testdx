public class Update_PlanExpense_On_PlanHandler {

    public void onAfterInsert(Map<id,Plan_Expense_BI__c> newMap){
        
        set<id> planIdSet = new set<id>();
        
        for(Plan_Expense_BI__c biPlans : newMap.values()){
        
            planIdSet.add(biPlans.Plan_BI__c);
            
        }
        
        list<Plan_BI__c> plansToUpdate = new list<Plan_BI__c>();
        
        List<Plan_Expense_BI__c> planList = [select id,EventExpense_Amount__c,FA_Amount__c,Amount_BI__c,Plan_BI__c,Plan_BI__r.Actuals_Total_Amount_BI__c from Plan_Expense_BI__c where Plan_BI__c in : planIdSet]; 
        
        list<Plan_BI__c> plansToCheckChilds = [select id,(select id,Actuals_Total_Amount_BI__c from Sub_Plans__r) from Plan_BI__c where id in:planIdSet];
        
        Map<id,Plan_BI__c> childPlanMap = new map<id,Plan_BI__c>();
        
        for(Plan_BI__c planObj : plansToCheckChilds){
            
            childPlanMap.put(planObj.id,planObj);
        
        } 
        
        Map<id,list<Plan_Expense_BI__c>> planMap = new Map<id,list<Plan_Expense_BI__c>>();
        
        for(Plan_Expense_BI__c planX : planList ){
            
            if(!planMap.containsKey(planX.Plan_BI__c)){
                
                planMap.put(planX.Plan_BI__c, new list<Plan_Expense_BI__c>{planX});
            
            }    
            
            else planMap.get( planX.Plan_BI__c).add(planX);
        }
        
        for(id tempVar : planMap.keyset()){
            
            Plan_BI__c planObj = new Plan_BI__c();
            
            planObj.id=tempVar;
            
            double amount =0 ;
            
            if(childPlanMap.containsKey(tempVar) && (!childPlanMap.get(tempVar).Sub_Plans__r.isEmpty())){
            
                for(Plan_BI__c planObjForChild : childPlanMap.get(tempVar).Sub_Plans__r)
                
                    amount += planObjForChild.Actuals_Total_Amount_BI__c;
                    
            }
            else amount = 0;
            
            
            for(Plan_Expense_BI__c tempPlanExpense : planMap.get(tempVar)){
                
                if(null != tempPlanExpense.EventExpense_Amount__c)
                    amount += tempPlanExpense.EventExpense_Amount__c;
                    
                if(null != tempPlanExpense.FA_Amount__c) 
                    amount += tempPlanExpense.FA_Amount__c;
                    
                if(null != tempPlanExpense.Amount_BI__c)
                    amount += tempPlanExpense.Amount_BI__c;
                
            }
            
            planObj.Actuals_Total_Amount_BI__c = amount;
            
            plansToUpdate.add(planObj);
        }
        
        if(!plansToUpdate.isempty())
            
            update plansToUpdate;
    }
    
}