/*
  * UserTerritoryControllerMVN
  *    Created By:     Kai Amundsen   
  *    Created Date:    July 24, 2013
  *    Description:     Displays all territories which a user belongs to
 */
public with sharing class UserTerritoryControllerMVN {
    private final Account acct;
    private List<UserTerritoryRow> theList = null;
    private Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();
    
    public UserTerritoryControllerMVN(ApexPages.StandardController controller) {
        this.acct = (Account)controller.getRecord();
    }

    public List<UserTerritoryRow> getUserTerritories() {
        if (theList != null)
            return theList;
            
        String acctId = acct.id;
        if ((acctId == null) || (acctId == ''))
            return null;
        
        Set<String> groupIds = new Set<String>();
        List<String> rowCauses = new List<String>();
        if(String.isNotBlank(settings.User_Territory_Causes_MVN__c)){
            rowCauses = UtilitiesMVN.splitCommaSeparatedString(settings.User_Territory_Causes_MVN__c);
        }
        for (AccountShare acctShare : [Select UserOrGroupId From AccountShare Where AccountId=:acctId And RowCause In :rowCauses]) {
            groupIds.add(acctShare.UserOrGroupId);
        }
        
        if (groupIds.size() <= 0)
            return null;
                
        Set<String> territoryIds = new Set<String>();
        for (Group gr : [Select RelatedId From Group Where Id In :groupIds]) {
            territoryIds.add(gr.RelatedId);
        }
        
        if (territoryIds.size() <= 0)
            return null;
                
        Map<Id,UserTerritory> userTerritories = new Map<Id,UserTerritory>([Select Id,UserId,TerritoryId From UserTerritory Where TerritoryId In :territoryIds And IsActive=true]);

        Set<String> userIds = new Set<String>();
        for (UserTerritory ut : userTerritories.values()) {
            userIds.add(ut.UserId);
        }

        Map<Id,User> users = null;
        if (userIds.size() > 0)
            users = new Map<Id,User>([Select Id,Name,MobilePhone,Email From User Where Id In :userIds]);
        else
            return null;
        
        Map<Id,Territory> territories = new Map<Id,Territory>([Select Id,Name,Description From Territory Where Id In :territoryIds]);
        
        Map<String,UserTerritoryRow> tempMap = new Map<String,UserTerritoryRow>();
        for (Territory terr : territories.values()) {
            UserTerritoryRow utr = new UserTerritoryRow();
            utr.Territory = terr.Name;
            
            if (terr.Description != null)
                utr.Description = terr.Description;
            else
                utr.Description = '';
            tempMap.put(terr.Name+'   '+terr.Id, utr);  // just in case Name is not unique
        }

        theList = new List<UserTerritoryRow>();

        for (UserTerritory ut : userTerritories.values()) {
            Territory tempTerr = territories.get(ut.TerritoryId);
            if (tempTerr == null)
                continue;
            String keyValue = tempTerr.Name+'   '+tempTerr.Id;

            UserTerritoryRow terrRow = tempMap.get(keyValue);

            UserTerritoryRow userTerrRow = new UserTerritoryRow();
                
            User usr = users.get(ut.UserId);


            userTerrRow.Territory = terrRow.Territory;
            userTerrRow.Description = terrRow.Description;
            userTerrRow.User = usr.Name;
            userTerrRow.MobilePhone = usr.MobilePhone;
            userTerrRow.Email = usr.Email;
            theList.add(userTerrRow);
        }
        
        /*List<String> keys = new List<String>(tempMap.keySet());
        keys.sort();
        
        theList = new List<UserTerritoryRow>();
        for (String key : keys)
            theList.add(tempMap.get(key));*/

        return theList;
    }
    
    public String getOnLoadJS() {
        List<UserTerritoryRow> terrs = getUserTerritories();
        return 'if(document.documentElement&&document.documentElement.scrollHeight){window.resizeBy(0,document.documentElement.scrollHeight-200);}else if(document.body.scrollHeight){window.resizeBy(0,document.body.scrollHeight-200);}';
    }
    
    public class UserTerritoryRow {
        public String territory{get; set;}
        public String user{get; set;}
        public String description{get; set;}
        public String MobilePhone{get; set;}
        public String Email{get; set;}
    }

}