/*
  * UserTerritoryControllerTestMVN
  *    Created By:     Kai Chen   
  *    Created Date:    August 5, 2013
  *    Description:     Unit Test for UserTerritoryControllerMVN
  *	   Currently in Salesforce, no DML is allowed on the UserTerritory object.  This restricts the ability for us to test the UserTerritoryControllerMVN class.
 */
@isTest
private class UserTerritoryControllerTestMVN {
	
	@isTest static void testGetUserTerritories() {
		/*Account testAccount = new Account();
		testAccount.RecordTypeId = [select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'PersonAccount'].Id;
		testAccount.FirstName = 'Test';
		testAccount.LastName = 'Account';

		insert testAccount;

		User testUser = new User();
		testUser.FirstName = 'Test';
		testUser.LastName = 'User';
		testUser.Phone = '123-456-7890';
		testUser.Email = 'testUser@email.com';

		insert testUser;

		Territory testTerritory = new Territory();

		testTerritory.Name = 'Test Territory';
		testTerritory.Description = 'Territory for Unit Test';

		insert testTerritory;

		AccountShare share = new AccountShare();

		share.AccountId = testAccount.Id;
		share.UserOrGroupId = testTerritory.Id;

		insert share;

		System.runAs(testUser){

			UserTerritory userTerr = new UserTerritory();

			userTerr.UserId = testUser.Id;
			userTerr.TerritoryId = testTerritory.Id;
		
			insert userTerr;
		}*/
		TestDataFactoryMVN.createSettings();
		Account acct = TestDataFactoryMVN.createTestConsumer();

		ApexPages.StandardController controller = new ApexPages.StandardController(acct);
    	UserTerritoryControllerMVN extension = new UserTerritoryControllerMVN(controller);
    	extension.getUserTerritories();


	}
	
	
}