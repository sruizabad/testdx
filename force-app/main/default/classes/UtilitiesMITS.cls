public with sharing class UtilitiesMITS {
    
        public static Service_Cloud_Settings_MITS__c mitssettings = Service_Cloud_Settings_MITS__c.getInstance();
        public static String mitsRecordType = mitssettings.MITS_Record_Type__c;
        public static String mitsclosedRecordType = mitssettings.MITS_Case_Closed__c;
        public static Map<Id,RecordType> typeMap = new Map<Id,RecordType>([select Id,Name,DeveloperName from RecordType where SObjectType = 'Case']);
        public static String medicalInquirySubmittedStatus = mitssettings.Medical_Inquiry_Submitted_Status__c;
    
    
    public static List<String> splitCommaSeparatedString(String stringToSplit){
        if(String.isBlank(stringToSplit)){
            return new List<String>();
        } else{
            List<String> splitStrings = stringToSplit.split(',');
            List<String> stringsToReturn = new List<String>();
            for(String splitString : splitStrings){
                System.debug('SPLIT STRING: ' + splitString);
                stringsToReturn.add(splitString.trim());
            }
            return stringsToReturn;
          }
    }
    
    public static Boolean  matchMITSRecordTypeIdToName (Id recordTypeId, String recordTypeName){     //Method being used in MITSCloseMedicalInquiryWhenCaseClosed
     for(String recTypeName:recordTypeName.split(',')){
                if(recTypeName != null && recTypeName != ''){
                recTypeName = recTypeName.trim();
                if(typeMap.containsKey(recordTypeId) && typeMap.get(recordTypeId).DeveloperName == recordTypeName){
                    return true;
                }
            }
        }
        return false;
    }
}