/*
* UtilitiesMVN
* Created By:    Roman Lerman
* Created Date:  3/4/2013
* Description:   This class provides an easy way to get at the various Interaction/Request record type as well as
*				 matching the record type developer name to the Id.
*/
public with sharing class UtilitiesMVN {
    public static Service_Cloud_Settings_MVN__c settings = Service_Cloud_Settings_MVN__c.getInstance();

	public static String medicalInquirySubmittedStatus = settings.Medical_Inquiry_Submitted_Status_MVN__c;
	public static String interactionRecordType = settings.Interaction_Record_Type_MVN__c;
	public static String interactionClosedRecordType = settings.Interaction_Closed_Record_Type_MVN__c;
	public static String interactionCreateRecordType = settings.Interaction_Create_Case_Record_Type_MVN__c;
	public static String interactionCreateOrigin = settings.Interaction_Create_Origin_MVN__c;
	public static String requestRecordType = settings.Request_Record_Type_MVN__c;
	public static Map<Id,RecordType> typeMap = new Map<Id,RecordType>([select Id,Name,DeveloperName from RecordType where SObjectType = 'Case']);
	public static String consumerRecordType = settings.Consumer_Record_Type_MVN__c;
	public static Boolean isAnonymizing = false;
	
	public static Boolean matchCaseRecordTypeIdToName(Id recordTypeId, String recordTypeName){
		System.debug('RecordTypeId: '+recordTypeId);
		System.debug('RecordTypeName: '+recordTypeName);
		for(String recTypeName:recordTypeName.split(',')){
			if(recTypeName != null && recTypeName != ''){
				recTypeName = recTypeName.trim();
				if(typeMap.containsKey(recordTypeId) && typeMap.get(recordTypeId).DeveloperName == recordTypeName){
					return true;
				}
			}
		}
		return false;
	}
	
	public static List<SelectOption> getStatesProvinces(String country){
    	List<Customer_Attribute_BI__c> attributes = Database.query('select Id, Name from Customer_Attribute_BI__c where '+settings.State_Province_Filter_MVN__c+' and OK_Country_ID_BI__c = \''+country+'\' order by Name');
    	List<SelectOption> stateValues = new List<SelectOption>();
    	stateValues.add(new SelectOption('', Label.None));
    	for(Customer_Attribute_BI__c attribute:attributes){
    		stateValues.add(new SelectOption(attribute.Id, attribute.Name));
    	}
    	
    	return stateValues;
    }

    public static List<SelectOption> getWorkplaceClasses(String country){
    	List<Customer_Attribute_BI__c> attributes = Database.query('select Id, Name from Customer_Attribute_BI__c where '+settings.Workplace_Class_Filter_MVN__c+' and OK_Country_ID_BI__c = \''+country+'\' order by Name');
    	List<SelectOption> workplaceClassValues = new List<SelectOption>();
    	workplaceClassValues.add(new SelectOption('', Label.None));
    	for(Customer_Attribute_BI__c attribute:attributes){
    		workplaceClassValues.add(new SelectOption(attribute.Id, attribute.Name));
    	}
    	
    	return workplaceClassValues;
    }

    public static List<SelectOption> getWorkplaceSpecialties(String country){
    	List<Customer_Attribute_BI__c> attributes = Database.query('select Id, Name from Customer_Attribute_BI__c where '+settings.Workplace_Specialty_Filter_MVN__c+' and OK_Country_ID_BI__c = \''+country+'\' order by Name');
    	List<SelectOption> workplaceSpecialtyValues = new List<SelectOption>();
    	workplaceSpecialtyValues.add(new SelectOption('', Label.None));
    	for(Customer_Attribute_BI__c attribute:attributes){
    		workplaceSpecialtyValues.add(new SelectOption(attribute.Id, attribute.Name));
    	}
    	
    	return workplaceSpecialtyValues;
    }

    public static List<SelectOption> getIndividualClasses(String country){
    	List<Customer_Attribute_BI__c> attributes = Database.query('select Id, Name from Customer_Attribute_BI__c where '+settings.Individual_Class_Filter_MVN__c+' and OK_Country_ID_BI__c = \''+country+'\' order by Name');
    	List<SelectOption> individualClassValues = new List<SelectOption>();
    	individualClassValues.add(new SelectOption('', Label.None));
    	for(Customer_Attribute_BI__c attribute:attributes){
    		individualClassValues.add(new SelectOption(attribute.Id, attribute.Name));
    	}
    	
    	return individualClassValues;
    }

    public static List<SelectOption> getIndividualSpecialties(String country){
    	List<Customer_Attribute_BI__c> attributes = Database.query('select Id, Name from Customer_Attribute_BI__c where '+settings.Individual_Specialty_Filter_MVN__c+' and OK_Country_ID_BI__c = \''+country+'\' order by Name');
    	List<SelectOption> individualSpecialtyValues = new List<SelectOption>();
    	individualSpecialtyValues.add(new SelectOption('', Label.None));
    	for(Customer_Attribute_BI__c attribute:attributes){
    		individualSpecialtyValues.add(new SelectOption(attribute.Id, attribute.Name));
    	}
    	
    	return individualSpecialtyValues;
    }

    public static List<SelectOption> getRoles(String country){
        List<Customer_Attribute_BI__c> attributes = Database.query('select Id, Name from Customer_Attribute_BI__c where '+settings.Role_Filter_MVN__c+' and OK_Country_ID_BI__c = \''+country+'\' order by Name');
        List<SelectOption> roles = new List<SelectOption>();
        roles.add(new SelectOption('', Label.None));
        for(Customer_Attribute_BI__c attribute:attributes){
            roles.add(new SelectOption(attribute.Id, attribute.Name));
        }
        
        return roles;
    }

    public static void createWorkplaceDataChangeRequest(Id caseId, String changeType){
        List<Case> cases = [select Id, Business_Account_MVN__c, Business_Account_MVN__r.Name, Business_Account_MVN__r.OKWorkplace_Class_BI__c, 
                            Business_Account_MVN__r.Specialty_BI__c, Business_Account_MVN__r.Phone, Business_Account_MVN__r.CRC_Phone_MVN__c,
                            Address_MVN__c, Address_MVN__r.Name, Address_MVN__r.Address_Line_2_vod__C, Address_MVN__r.City_vod__c, 
                            Address_MVN__r.OK_State_Province_BI__c, Address_MVN__r.Zip_vod__c, Address_MVN__r.Country_Code_BI__c, CaseNumber
                            from Case where Id = :caseId limit 1];

        System.debug('DCR Cases: '+cases);
        if(cases != null && cases.size() > 0){
            Case cs = cases[0];

            System.debug('DCR Account Id: '+cs.Business_Account_MVN__c);
            System.debug('DCR Address Id: '+cs.Address_MVN__c);
            System.debug('DCR Change Type: '+changeType);
            if(cs.Business_Account_MVN__c != null && cs.Address_MVN__c != null && (changeType != null && !String.isBlank(changeType))){
                V2OK_Data_Change_Request__c dcr = new V2OK_Data_Change_Request__c();
                dcr.RecordTypeId = [select Id from RecordType where SObjectType = 'V2OK_Data_Change_Request__c' AND DeveloperName = :settings.DCR_Record_Type_MVN__c].Id;

                if(changeType == settings.DCR_Update_Change_Type_MVN__c){
                    dcr.Organisation_Account__c = cs.Business_Account_MVN__c;
                }
                
                dcr.HCO_Temp_External_ID_BI__c = cs.Business_Account_MVN__c;
                dcr.New_Account_Name__c = cs.Business_Account_MVN__r.Name;

                dcr.OK_Workplace_Class_BI__c = cs.Business_Account_MVN__r.OKWorkplace_Class_BI__c;
                dcr.Workplace_Specialty_BI__c = cs.Business_Account_MVN__r.Specialty_BI__c;
                
                dcr.Phone__c = cs.Business_Account_MVN__r.Phone;
                
                dcr.Change_Type__c = changeType;
                dcr.Status__c = settings.DCR_Status_MVN__c;

                dcr.ADDR_Temp_External_ID_BI__c = cs.Address_MVN__c;
                dcr.New_Address_Line_1__c = cs.Address_MVN__r.Name;
                dcr.Address_line_2_BI__c = cs.Address_MVN__r.Address_line_2_vod__c;
                dcr.City_vod__c = cs.Address_MVN__r.City_vod__c;
                dcr.OK_State_Province_BI__c = cs.Address_MVN__r.OK_State_Province_BI__c;
                dcr.Zip_vod__c = cs.Address_MVN__r.Zip_vod__c;
                dcr.Country_DS__c = cs.Address_MVN__r.Country_Code_BI__c;

                try{
                    insert dcr;
                }catch(Exception e){
                    System.debug('DCR Create Error: '+e.getMessage());
                    
                    String action = '';
                    if(changeType == settings.DCR_Update_Change_Type_MVN__c){
                        action = 'An error occurred when trying to add an address to an existing workplace';
                    }else{
                        action = 'An error occurred when trying to create a new workplace';
                    }

                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                    String adminEmailAddress = Service_Cloud_Settings_MVN__c.getInstance().Administrator_Email_MVN__c;
                    mail.setToAddresses(new String[] {adminEmailAddress});
                    mail.setSenderDisplayName('Workplace Data Change Request Error');
                    mail.setSubject('Error(s): A data change request could not be created for a workplace');

                    String plainTextBody = '';
                    
                    plainTextBody += '\n'+action+'\n';
                    plainTextBody += '\n'+e.getMessage()+'\n\n';
                    plainTextBody += 'Workplace Id: ' + cs.Business_Account_MVN__c + ' ---- Workplace Address Id: ' + cs.Address_MVN__c + '\n';
                    plainTextBody += 'Case for which change request creation was attempted: ' + cs.Id + ' (Case Id) ---- ' + cs.CaseNumber + ' (Case Number)\n';
                    
                    mail.setPlainTextBody(plainTextBody);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }
        }
    }

    public static SelectOption getSelectOptionAddress(Address_vod__c address){
        return new SelectOption(address.Id, 
                                address.Name + (address.Address_line_2_vod__c != null ? ', ' + address.Address_line_2_vod__c : '') +
                                               (address.City_vod__c != null ? ', ' + address.City_vod__c : '') + 
                                               (address.OK_State_Province_BI__r.Name != null ? ', ' + address.OK_State_Province_BI__r.Name : '') + 
                                               (address.Zip_vod__c != null ? ', ' + address.Zip_vod__c : ''));
    }

    public static Boolean isValidEmail(String email){
    	if(email == null || email == ''){
    		return true;
    	}

    	return Pattern.matches('([a-zA-Z0-9_\\-\\.\\+\\$\\!\\~\\`\\#\\%\\^\\&\\*\\(\\)]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})', email);
    }

    public static List<String> splitCommaSeparatedString(String stringToSplit){
        if(String.isBlank(stringToSplit)){
            return new List<String>();
        } else{
            List<String> splitStrings = stringToSplit.split(',');
            List<String> stringsToReturn = new List<String>();
            for(String splitString : splitStrings){
                System.debug('SPLIT STRING: ' + splitString);
                stringsToReturn.add(splitString.trim());
            }
            return stringsToReturn;
        }
    }
}