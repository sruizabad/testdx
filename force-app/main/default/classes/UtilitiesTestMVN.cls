@isTest
private class UtilitiesTestMVN {

	static{
		TestDataFactoryMVN.createSettings();
	}
	
	@isTest 
	static void recordTypeIdToName() {
		Map<Id,RecordType> typeMap = new Map<Id,RecordType>([select Id,Name,DeveloperName from RecordType where SObjectType = 'Case']);

		for(Id key : typeMap.keySet()){
			UtilitiesMVN.matchCaseRecordTypeIdToName(key, typeMap.get(key).DeveloperName);
		}

		UtilitiesMVN.matchCaseRecordTypeIdToName(Id.valueOf('a0Ga000000O5ceJEAR'), 'fakeName');
	}
	
	@isTest 
	static void testSelectOptions() {
		List<Customer_Attribute_BI__c> attributes = new List<Customer_Attribute_BI__c>();
		String countryCode = 'DE';

		Customer_Attribute_BI__c workplaceSpecialty = TestDataFactoryMVN.createTestAttribute('SP', countryCode);
		attributes.add(workplaceSpecialty);

		Customer_Attribute_BI__c workplaceClass = TestDataFactoryMVN.createTestAttribute('FAD', countryCode);
		attributes.add(workplaceClass);		

		Customer_Attribute_BI__c individualSpecialty = TestDataFactoryMVN.createTestAttribute('TYT', countryCode);
		attributes.add(individualSpecialty);				

		Customer_Attribute_BI__c individualClass = TestDataFactoryMVN.createTestAttribute('ACCT', countryCode);
		attributes.add(individualClass);		

		Customer_Attribute_BI__c stateProvince = TestDataFactoryMVN.createTestAttribute('DPT', countryCode);
		attributes.add(stateProvince);

		Customer_Attribute_BI__c role = TestDataFactoryMVN.createTestAttribute('TIH', countryCode);
		attributes.add(role);

		List<SelectOption> options = UtilitiesMVN.getStatesProvinces(countryCode);
		System.assert(!options.isEmpty());

		options = UtilitiesMVN.getWorkplaceClasses(countryCode);
		System.assert(!options.isEmpty());

		options = UtilitiesMVN.getWorkplaceSpecialties(countryCode);
		System.assert(!options.isEmpty());

		options = UtilitiesMVN.getIndividualClasses(countryCode);
		System.assert(!options.isEmpty());	

		options = UtilitiesMVN.getIndividualSpecialties(countryCode);
		System.assert(!options.isEmpty());
		
		options = UtilitiesMVN.getRoles(countryCode);
		System.assert(!options.isEmpty());
	}

	@isTest
	static void testCreateWDCR(){
		Account business = TestDataFactoryMVN.createTestBusinessAccount();

		Address_vod__c address = TestDataFactoryMVN.createTestAddress(business);

		Case testCase = TestDataFactoryMVN.createTestCase();

		testCase.Business_Account_MVN__c = business.Id;
		testCase.Address_MVN__c = address.Id;

		update testCase;

		UtilitiesMVN.createWorkplaceDataChangeRequest(testCase.Id, 'Update');

		Test.setReadOnlyApplicationMode(true);

		UtilitiesMVN.createWorkplaceDataChangeRequest(testCase.Id, 'Update');
	}

	@isTest
	static void testAddressOptions(){
		Account business = TestDataFactoryMVN.createTestBusinessAccount();

		Address_vod__c address = TestDataFactoryMVN.createTestAddress(business);

		UtilitiesMVN.getSelectOptionAddress(address);
	}

	@isTest
	static void testValidEmail(){
		System.assertEquals(true, UtilitiesMVN.isValidEmail('test@test.com'));
		System.assertEquals(true, UtilitiesMVN.isValidEmail(''));
		System.assertEquals(false, UtilitiesMVN.isValidEmail('test@test'));
		System.assertEquals(false, UtilitiesMVN.isValidEmail('test'));
		System.assertEquals(false, UtilitiesMVN.isValidEmail('@test.com'));
	}

	@isTest
	static void testStringSplit(){
		Set<String> splitStrings = new Set<String>(UtilitiesMVN.splitCommaSeparatedString('test , test2,test3, test 4'));

		System.assert(splitStrings.contains('test'));
		System.assert(splitStrings.contains('test2'));
		System.assert(splitStrings.contains('test3'));
		System.assert(splitStrings.contains('test 4'));

		splitStrings = new Set<String>(UtilitiesMVN.splitCommaSeparatedString(''));

		System.assert(splitStrings.isEmpty());
	}
}