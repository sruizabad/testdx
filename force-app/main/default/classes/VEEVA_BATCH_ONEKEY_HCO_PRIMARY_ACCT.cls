/**
 * VEEVA_BATCH_ONEKEY_HCO_PRIMARY_ACCT
 *
 * Author: Raphael Krausz <raphael.krausz@veeva.com>
 * Date:   2015-06-01
 * Description: 
 *  Check for OneKey Accounts which have no primary parent but is a member of an another account.
 *  Once found, make the first OneKey parent account the primary parent.
 *  
 */

global without sharing class VEEVA_BATCH_ONEKEY_HCO_PRIMARY_ACCT implements Database.Batchable<SObject>, Database.Stateful {

    private final Id jobId;
    private final Datetime lastRunTime;

    private final String country;
    private final String countryPredicate;

    global Integer  successes = 0;
    global Integer  failures = 0;
    global Integer  total = 0;


    private List<Account> accountsToUpdateList = new List<Account>();


    public VEEVA_BATCH_ONEKEY_HCO_PRIMARY_ACCT() {
        this(null, null, null);
    }

    public VEEVA_BATCH_ONEKEY_HCO_PRIMARY_ACCT(Id jobId, Datetime lastRunTime) {
        this(jobId, lastRunTime, null);
    }

    public VEEVA_BATCH_ONEKEY_HCO_PRIMARY_ACCT(Id jobId, Datetime lastRunTime, String country) {

        if (lastRunTime == null)
            lastRunTime = DateTime.newInstance(1970, 1, 1);

        if ( ! String.isBlank(country)) {
            countryPredicate = ' AND Country_Code_BI__c = \'' + country + '\'';
        }

        this.jobId        = jobId;
        this.country      = country;
        this.lastRunTime  = lastRunTime;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String selStmt =
            'SELECT'
            + '  Id,'
            + '  OK_External_ID_BI__c,'
            + '  ('
            + '    SELECT'
            + '      Parent_Account_vod__c,'
            + '      Parent_Account_vod__r.OK_External_ID_BI__c'
            + '    FROM Child_Account_vod__r'
            + '    WHERE Parent_Account_vod__r.OK_External_ID_BI__c != NULL'
            + '  )'
            + ' FROM Account'
            + ' WHERE Primary_Parent_vod__c = NULL'
            + countryPredicate
            + ' AND OK_External_ID_BI__c LIKE \'W%\''
            + ' ORDER BY LastModifiedDate ASC'
            ;

        system.Debug('SELECT statement: ' + selStmt);
        return Database.getQueryLocator(selStmt);
    }

    global void execute(Database.BatchableContext BC, List<Account> accountList) {

        System.debug('execute batch.size(): ' + accountList.size());

        for (Account anAccount : accountList) {
            processAccount(anAccount);
        }

        writeUpdates();

    }


    private void processAccount(Account theAccount) {

        // If there are no parent accounts then skip
        if (theAccount.Child_Account_vod__r.isEmpty()) {
            return;
        }

        // Otherwise mark the first parent as primary
        Id parentId = theAccount.Child_Account_vod__r[0].Parent_Account_vod__c;
        theAccount.Primary_Parent_vod__c = parentId;
        updateAccount(theAccount);

    }

    private void updateAccount(Account theAccount) {
        accountsToUpdateList.add(theAccount);

        if (accountsToUpdateList.size() >= 1000) {
            writeUpdates();
        }
    }

    private void writeUpdates() {

        if (accountsToUpdateList.isEmpty() ) {
            return;
        }



        List<Database.SaveResult> results = Database.update(accountsToUpdateList, false);
        total += results.size();
        for (Database.SaveResult result : results) {
            if (result.isSuccess()) {
                successes += 1;
            } else {
                failures += 1;
                System.debug('Upsert error: ' + result.getErrors());
            }
        }


        System.debug('Upsert results - successes: ' + successes + '; failures: ' + failures);


        accountsToUpdateList = new List<Account>();
    }


    global void finish(Database.BatchableContext BC) {
        if (jobId == null) return;

        String message =
            'Total upserted: ' + total
            + ', Successes: ' + successes
            + ', Failures: ' + failures
            ;

        System.debug(message);
        VEEVA_BATCH_ONEKEY_BATCHUTILS.setErrorMessage(jobId, message);
        VEEVA_BATCH_ONEKEY_BATCHUTILS.setCompleted(jobId, lastRunTime);
    }

}