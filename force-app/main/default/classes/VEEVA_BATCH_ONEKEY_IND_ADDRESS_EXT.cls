/*********************************************************************************************************
Class calculate the address of an INDIVIDUAL  based on the ACTIVITY file and WKP_ADDRESS_RELATION file
 - Examines the addresses which have changed first.
*********************************************************************************************************/
global without sharing class VEEVA_BATCH_ONEKEY_IND_ADDRESS_EXT
    implements Database.Batchable<SObject>, Database.Stateful {

    private final Id jobId;
    private final Datetime lastRunTime;
    private final String lastRunTimeString;

    private final String country;
    private final String countryPredicate;

    private VEEVA_BATCH_ONEKEY_UPDATE_IND_ADDRESS updateIndAddress;

    global Integer  successes = 0;
    global Integer  failures = 0;
    global Integer  total = 0;



    public VEEVA_BATCH_ONEKEY_IND_ADDRESS_EXT() {
        this(null, null, null);
    }

    public VEEVA_BATCH_ONEKEY_IND_ADDRESS_EXT(Id jobId, Datetime lastRunTime) {
        this(jobId, lastRunTime, null);
    }

    public VEEVA_BATCH_ONEKEY_IND_ADDRESS_EXT(Id jobId, Datetime lastRunTime, String country) {

        if (lastRunTime == null)
            lastRunTime = DateTime.newInstance(1970, 1, 1);

        // temp - entire dataset
        // lastRunTime = DateTime.newInstance(1970, 1, 1);


        if (String.isBlank(country)) {
            countryPredicate = '';
        } else {
            countryPredicate =
                '  AND ('
                + '  OK_STAGE_ADDRESS__r.Country_Code_BI__c = \'' + country + '\''
                + '  OR OK_STAGE_ADDRESS__r.Country_vod__c = \'' + country + '\''
                + '  )'
                ;
        }

        this.jobId        = jobId;
        this.lastRunTime  = lastRunTime;
        this.country      = country;
        lastRunTimeString = lastRunTime.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.\'000Z\'');

        updateIndAddress = new VEEVA_BATCH_ONEKEY_UPDATE_IND_ADDRESS(jobId, lastRunTime, country);

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {


        String selStmt =
            'SELECT'
            + '  OK_Stage_Workplace__c'
            + ' FROM OK_STAGE_ADDRESS_INT__c'
            + ' WHERE ('
            + '    SystemModStamp >= ' + lastRunTimeString
            + '    OR OK_STAGE_ADDRESS__r.SystemModStamp >= ' + lastRunTimeString
            + '    OR OK_Stage_Workplace__r.SystemModStamp >= ' + lastRunTimeString
            + ' )'
            + countryPredicate
            + ' ORDER BY OK_Stage_Workplace__c, LastModifiedDate ASC'
            ;


        system.Debug('SELECT statement: ' + selStmt);
        return Database.getQueryLocator(selStmt);
    }

    global void execute(Database.BatchableContext BC, List<OK_STAGE_ADDRESS_INT__c> stageAddressRelationship) {

        List<OK_Stage_Activity__c> activityStageRecords = getActivities(stageAddressRelationship);

        if (
            activityStageRecords != null
            && activityStageRecords.size() > 0
        ) {
            updateIndAddress.processBatch(activityStageRecords);
            updateIndAddress.upsertAddresses();

            total     = updateIndAddress.total;
            successes = updateIndAddress.successes;
            failures  = updateIndAddress.failures;
        }

    }


    List<OK_Stage_Activity__c> getActivities(List<OK_STAGE_ADDRESS_INT__c> addressWorkplaceList) {
        Set<Id> workplaceIdSet = new Set<Id>();

        for (OK_STAGE_ADDRESS_INT__c addressWorkplace : addressWorkplaceList) {
            workplaceIdSet.add(addressWorkplace.OK_Stage_Workplace__c);
        }

        List<OK_Stage_Activity__c> activities =
            [
                SELECT
                Stage_Workplace__r.External_Id__c,
                OK_Process_Code__c,
                External_Id__c,
                Stage_Workplace__c,
                Stage_Workplace__r.Workplace_External_Id__c,
                Stage_Workplace__r.Workplace_Name__c,
                OK_ACT_PREF_MAIL_FLAG_BI__c,
                Do_Not_Mail__c,
                Individual_External_Id__c,
                Mailing__c,
                Primary__c,
                Active_Status__c,
                ACT_CORE_OK_Activity_ID__c,
                OK_Act_Phone__c,
                OK_Act_Fax__c,
                OK_Phone_Extension__c,
                OK_PAD_BI__c,
                OK_PAD_OLD_BI__c,
                OK_PVA_BI__c,
                OK_PVA_DELTA_BI__c,
                OK_PVIRV_BI__c,
                OK_PVACT6M_BI__c,
                OK_PVACT3M_BI__c,
                OK_PVACT1M_BI__c
                FROM  OK_Stage_Activity__c
                WHERE Stage_Workplace__c in :workplaceIdSet
                ORDER BY Individual_External_Id__c, Stage_Workplace__c, LastModifiedDate ASC
            ];

        return activities;

    }


    global void finish(Database.BatchableContext BC) {
        if (jobId == null) return;

        String message =
            'Total upserted: ' + total
            + ', Successes: ' + successes
            + ', Failures: ' + failures
            ;

        System.debug(message);
        VEEVA_BATCH_ONEKEY_BATCHUTILS.setErrorMessage(jobId, message);
        VEEVA_BATCH_ONEKEY_BATCHUTILS.setCompleted(jobId, lastRunTime);
    }

}