/*************************************************************************************************
This class DELETEs  the records which were marked having OK_PROCESS_CODE = 'D' or state 'INACTIVE'

Modified date:  2015-06-01
Author:         Raphael Krausz <raphael.krausz@veeva.com>
Description:
    Fixed SOQL statement. I made it so that Child_Account_vod records are deleted if either the
    parent or child account is inactive.

Modified date:  2015-06-02
Author:         Raphael Krausz <raphael.krausz@veeva.com>
Description:
    Re-ordered SOQL statement to be more efficient and increase reliability

Modified date:  2015-06-04
Author:         Raphael Krausz <raphael.krausz@veeva.com>
Description:
    Moved the functionality to delete Child Account records where parent or child Account is
    inactive to a separate class. This batch class was taking too long to run and continually
    had problems.

*************************************************************************************************/
global without sharing class VEEVA_BATCH_ONEKEY_RUNDELETES implements Database.Batchable<SObject> {

    private final String initialState;
    private final static String sInbound = 'Inbound';

    private final Id jobId;
    private final Datetime lastRunTime;
    private final String country;

    public VEEVA_BATCH_ONEKEY_RUNDELETES() {
        this(null, null, null);
    }

    public VEEVA_BATCH_ONEKEY_RUNDELETES(Id JobId, Datetime lastRunTime) {
        this(jobId, lastRunTime, null);
    }

    public VEEVA_BATCH_ONEKEY_RUNDELETES(Id JobId, Datetime lastRunTime, String country) {
        this.jobId = JobId;

        if (lastRunTime == null) {
            this.lastRunTime = DateTime.newInstance(1970, 01, 01);
        } else {
            this.lastRunTime = lastRunTime;
        }

        this.country = country;
    }

    //collect the records to be deleted ****************************
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // Create base query


        String lastRunTimeString = lastRunTime.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.\'000Z\'');

        String selStmt =
            'SELECT Id FROM Child_Account_vod__c'
            + ' WHERE LastModifiedDate >= ' + lastRunTimeString
            ;

        if ( ! String.isBlank(country) ) {

			//2016.01.26 - OK_Country_Code_BI__c not working for secondary countries like MC, AD, CY, IE so change condition to External_ID2__c. 
            selStmt += ' AND External_ID2__c LIKE \'W' + country + '%\' ';  //' AND ( OK_Country_Code_BI__c = \'' + country + '\''	
                	
                	
			/* No need of this, because we check above the External_ID2__c: WUK%
            if ( country.equalsIgnoreCase('UK') ) {
                selStmt +=
                    ' OR OK_Country_Code_BI__c = \'GB\''
                    + ' OR OK_Country_Code_BI__c = \'IE\''
                    ;
            }

            selStmt += ' )';
            */
        }


        selStmt +=
            ' AND ('
            + '     ('
            + '   Child_Account_vod__r.OK_Status_Code_BI__c IN (\'STA.9\', \'Inactive\', \'Closed\')'
            + '   OR Parent_Account_vod__r.OK_Status_Code_BI__c IN (\'STA.9\', \'Inactive\', \'Closed\')'
            + '  )'
            + '  OR ('
            + '    OK_Process_Code_BI__c = \'D\''
            + '    OR OK_Active_Status_BI__c = \'Invalid\''
            + '    OR OK_Active_Status_BI__c = \'STA.9\''
            + '    OR ('
            + '      OK_ACT_END_DAT_TXT_BI__c != null'
            + '      AND (NOT OK_ACT_END_DAT_TXT_BI__c LIKE \'9999%\')'
            + '    )'
            + '  )'
            + ')'
            ;



        system.debug('Batch select statement: ' + selStmt);
        return Database.getQueryLocator(selStmt);

    }



    /*********************************************************************
    Delete records having OK_PROCESS_CODE = 'D' from the following objects
    Child_Account_vod__c and sometimes (Account)

    //Address_vod__c  was handled separatelly
    *********************************************************************/
    global void execute(Database.BatchableContext BC, List<sObject> batch) {

        List<Child_Account_vod__c> CAsTODelete = (List<Child_Account_vod__c>) batch;
        delete CAsTODelete;

        return;

        /*
        //sometimes  Inactive accounts need to be deleted  but as a base rule we are not deleting  accounts
        List<String> myDistObj = new List<String>();

        myDistObj.add('Account');

        //Generate a list of queries for each object
        for(String objName: myDistObj)
        {
                String sSOQL = 'Select Id from ' + objName + ' where OK_Process_Code_BI__c = \'D\' ';
                List<sObject> s = Database.query(sSOQL);
                delete s;
        }
        */

    }

    /***********************************************
    fire an update trigger o Batch Job  object which
    kick of the Next job.
    ***********************************************/
    global void finish(Database.BatchableContext BC) {
        //VEEVA_BATCH_ONEKEY_BATCHUTILS.setCompleted(jobId,lastRunTime);
        setCompleted(jobId, lastRunTime);
    }

    /******************************* 2012.11.21. ********************************************/
    /* Add this here from batchutil class  only  to avoid cross-refference deployment error */
    /*******************************************
     Updates the job status to STATUS_COMPLETED
     and populates the end time with the current
     system date/time.
     This  function will initiate a trigger which
     will  kick of the next  job  later
     *******************************************/
    public static void setCompleted(Id jobId, DateTime LRT) {
        if (jobId != null) {
            List<V2OK_Batch_Job__c> jobs = [SELECT Id FROM V2OK_Batch_Job__c
                                            WHERE Id = :jobId
                                           ];
            if (!jobs.isEmpty()) {
                V2OK_Batch_Job__c job = jobs.get(0);
                job.Status__c = 'Completed';
                job.End_Time__c = Datetime.now();
                job.LastRunTime__c = LRT;
                update job;
            }
        }
    }

    /***********************************************************
    insert a record  into a custom object:   Batch_Job_Error__c
    ***********************************************************/
    public static void setErrorMessage(Id jobId, String Message) {
        if (jobId != null) {
            //Create an error message
            Batch_Job_Error__c jobError = new Batch_Job_Error__c();
            jobError.Error_Message__c = Message;
            jobError.Veeva_To_One_Key_Batch_Job__c = jobId;
            jobError.Date_Time__c = Datetime.now();
            insert jobError;
        }
    }
    /* Add this here from batchutil class  only  to avoid cross-refference deployment error */
    /******************************* 2012.11.21. ********************************************/
}