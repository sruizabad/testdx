/****************************************************************************************************
This class  iterates through  the records  in the staging obj OK_STAGE_ADDRESS_INT__c  and calculates
the addresses  of the workplaces
Class  needs change.  Different customers  might need different  Address fileds  to integrate.
Also  the OneKey  ExternalID  fileds  might be different  form customer to customer.


Modified: 2015-04-28
Author:   Raphael Krausz <raphael.krausz@veeva.com>
Description:

    Fixed bug.

    Primary addresses flag was being calculated incorrectly - inactive OK_STAGE_ADDRESS_INT__c records
    were still being marked as primary. This meant that multiple addresses were beinng marked as primary.
    (Both the actual active primary and the inactive ones.)

    As both were being marked as primary, the standard Veeva triggers were then ensuring only one was
    being marked as primary - often the incorrect one.

****************************************************************************************************/
global without sharing class VEEVA_BATCH_ONEKEY_UPDATE_ADDRESS
    implements Database.Batchable<SObject>, Database.Stateful {

    private Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    private static String constSELECT = 'SELECT ';

    private final Id jobId;
    private final Datetime lastRunTime;
    private final String country;
    //Viktor added country - 2013.05.07

    global Integer  SuccessNumber = 0;
    global Integer  FailureNumber = 0;
    global Integer  totalNumber = 0;

    private Map<String, Address_vod__c> workplaceToPrimaryAddressMap;

    public VEEVA_BATCH_ONEKEY_UPDATE_ADDRESS() {
        this(null, null, null);
    }

    public VEEVA_BATCH_ONEKEY_UPDATE_ADDRESS(Id jobId, Datetime lastRunTime) {
        this(jobId, lastRunTime, null);
    }

    public VEEVA_BATCH_ONEKEY_UPDATE_ADDRESS(Id JobId, Datetime lastRunTime, String country) {
        this.jobId = JobId;


        if (lastRunTime == null) {
            lastRunTime = DateTime.now().addDays(-30); //lastRunTime = DateTime.newInstance(1970, 1, 1);
        }

        this.lastRunTime = lastRunTime;

        this.country = country;

        System.Debug('CONSTRUCTOR BATCH VEEVA_BATCH_ONEKEY_UPDATE_ADDRESS');
    }


    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.Debug('START BATCH VEEVA_BATCH_ONEKEY_UPDATE_ADDRESS'); //2012.02.25.

        String selStmt = ' ';
        String sAddressStage = 'OK_STAGE_ADDRESS__c';
        String sAddressStageRel = 'OK_STAGE_ADDRESS__r';
        String sAddressRel = 'OK_STAGE_ADDRESS_INT__c';
        // Create base query
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sAddressStage).getDescribe().fields.getMap();
        Map <String, String> StageFieldMap = new Map <String, String>();


        for (Schema.SObjectField sfield : fieldMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (!dfield.getName().equals('null') || dfield.getName() != null) {
                selStmt = selStmt + sAddressStageRel + '.' + dfield.getName() + ',';
            }
        }

        String lastRunTimeString = lastRunTime.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.\'000Z\'');

        //Complete SOQL
        //Remove the last comma
        selStmt =
            'SELECT '
            + selStmt
            + '  OK_Process_Code__c,'
            + '  External_Id__c,'
            + '  OK_Stage_Workplace__r.Home__c,'
            + '  OK_Stage_Workplace__c,'
            + '  OK_Stage_Workplace__r.Workplace_External_Id__c,'
            + '  OK_Stage_Workplace__r.Workplace_Name__c,'
            + '  OK_Stage_Workplace__r.External_Id__c,'
            + '  OK_Stage_Workplace__r.Individual_Account__c,'
            + '  OK_Stage_Workplace__r.Individual_Account__r.OK_External_ID_BI__c,'
            + '  Primary__c,'
            + '  Billing__c,'
            + '  Mailing__c,'
            + '  Business__c,'
            + '  OK_End_Date__c,'
            + '  OK_Address_Type_Code__c,'
            + '  OK_Building_Name_BI__c,'
            + '  OK_Po_Box__c,'
            + '  OK_STAGE_ADDRESS__r.OK_State_Province_BI__r.Name'
            + ' FROM OK_STAGE_ADDRESS_INT__c'
            + ' WHERE ('
            + '    LastModifiedDate >= ' + lastRunTimeString
            + '    OR OK_STAGE_ADDRESS__r.LastModifiedDate >= ' + lastRunTimeString
            + '    OR OK_Stage_Workplace__r.LastModifiedDate >= ' + lastRunTimeString
            + ' )'
            ;

        if ( ! String.isBlank(country) ) {
            selStmt +=
                ' AND ('
                + '   OK_STAGE_ADDRESS__r.Country_Code_BI__c = \'' + country + '\''
                + '   OR OK_STAGE_ADDRESS__r.Country_vod__c = \'' + country + '\''
                + ' )'
                ;
        }

        selStmt += '  ORDER BY LastModifiedDate ASC, OK_Process_Code__c ASC';

        System.Debug('SQLU: ' + selStmt);  //2012.02.25.


        return Database.getQueryLocator(selStmt);
    }

    private void addNewPrimary(String workplaceExternalId, Address_vod__c theAddress) {
        // This class ensures only one primary address (the latest) is upserted
        // If a new primary address is generated, the older primary is marked as non-primary
        // This works as we are ordering by LastModified Date and as salesforce/Apex passes by reference

        // We are ONLY concerned with primary addresses
        if (theAddress.Primary_vod__c != true) {
            return;
        }

        // If there is already a primary marked, then only the newer record should be primary
        // so set the old primary to be false
        if ( workplaceToPrimaryAddressMap.containsKey(workplaceExternalId) ) {
            Address_vod__c oldPrimary = workplaceToPrimaryAddressMap.get(workplaceExternalId);
            oldPrimary.Primary_vod__c = false;
        }

        workplaceToPrimaryAddressMap.put(workplaceExternalId, theAddress);
    }

    //global void execute(Database.BatchableContext BC, List<sObject> batch) {
    global void execute(Database.BatchableContext BC, List<OK_STAGE_ADDRESS_INT__c> addressStageRecords) {

        // Initialise workplaceToPrimaryAddressMap
        workplaceToPrimaryAddressMap = new Map<String, Address_vod__c>();

        //Build a list based on the SQL from the previous method
        // List<OK_STAGE_ADDRESS_INT__c> addressStageRecords = (List<OK_STAGE_ADDRESS_INT__c>) batch;

        System.Debug('EXECUTE BATCH VEEVA_BATCH_ONEKEY_UPDATE_ADDRESS bsize = ' + addressStageRecords.size());

        List<Address_vod__c> addressRecs = new List<Address_vod__c>(); //Addresses to Upsert later.
        String AccountExtId;
        //Get the list of cegedim address Id's in the table

        for (OK_STAGE_ADDRESS_INT__c addressStageRecord : addressStageRecords) {
            boolean bContinue = true;
            boolean isHome = false;   //CSABA 2012.05.07.  we ignore this filed.  AL WKPS  are treathed in the same way.

            if (addressStageRecord.OK_Stage_Workplace__r.Home__c == TRUE &&  addressStageRecord.OK_Stage_Workplace__r.Individual_Account__c != null) {
                //code never reach this point because Cegedim WorkPlace file does not contain Individual Account. We not even map this
                // change the OK external ID to be the implementation specific one
                AccountExtId = (String)addressStageRecord.OK_Stage_Workplace__r.Individual_Account__r.OK_External_ID_BI__c;
                isHome =  true;
            } else if (addressStageRecord.OK_Stage_Workplace__r.Workplace_External_Id__c != null
                       //2012.10.21. && addressStageRecord.OK_Stage_Workplace__r.Workplace_Name__c != null
                      ) {
                //2012.05.07.  since there will be no more EMPTY NAME   code always goes here
                AccountExtId = (String) addressStageRecord.OK_Stage_Workplace__r.Workplace_External_Id__c;
                System.Debug('ACCOUNT EXT ID= ' + AccountExtId);
            } else {
                bContinue = false; //Skip record no WKP Ext Id  or no Name.  2012.05.07.  NEVER REACH THIS part
                System.Debug('SKIP TRUE');
            }


            if (bContinue) {
                System.Debug('Create Address record for: ' + addressStageRecord.OK_STAGE_ADDRESS__r.Name);
                //Create the association to the Account

                Account acct = new Account(OK_External_ID_BI__c = AccountExtId);  //EXTERNAL ID NAME TO BE CHECKED. TAKE CARE TO USE THAT ExternalId  which holds  the OneKey ID  customer depenmdent

                totalNumber = totalNumber + 1;

                Address_vod__c address = new Address_vod__c();


                String processCode = addressStageRecord.OK_Process_Code__c;
                Boolean primaryFlag = addressStageRecord.Primary__c;
                
                //if (addressStageRecord.OK_End_Date__c != 'Active') {
                //    processCode = 'D';
                //    primaryFlag = false;
                //}

				//We invalidate the record if 3 main flags are false and the end date is Inactive     
	            if (
	                addressStageRecord.Billing__c == false
	                && addressStageRecord.Primary__c == false
	                && addressStageRecord.Mailing__c == false
	                && addressStageRecord.OK_End_Date__c == 'Inactive'	            		
	            ) 
	            {
	            	processCode = 'D';
	                primaryFlag = false;
	            }	
					


                if (country != 'CA') {

                    System.debug('Non-CA adddress');

                    //WAVE 2 MAPPING

                    String stageCountryVodField = addressStageRecord.OK_STAGE_ADDRESS__r.Country_vod__c;
                    String countryVodField;

                    if ( String.isBlank(stageCountryVodField) || stageCountryVodField.equalsIgnoreCase('UK') ) {
                        countryVodField = addressStageRecord.OK_STAGE_ADDRESS__r.Country_Code_BI__c;
                    } else {
                        countryVodField = addressStageRecord.OK_STAGE_ADDRESS__r.Country_vod__c;
                    }

                    address = new Address_vod__c(
                        //Best_Times_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.Best_Times_vod__c,
                        Name = addressStageRecord.OK_STAGE_ADDRESS__r.Name,
                        City_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.City_vod__c,
                        Country_vod__c = countryVodField,
                        Country_Code_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.Country_Code_BI__c,
                        Zip_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.Zip_vod__c,
                        //State_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.State_vod__c,
                        State_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_State_Province_BI__r.Name,
                        OK_State_Province_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_State_Province_BI__c,
                        // BI does not use the County field
                        //ACT_CORE_County__c = addressStageRecord.OK_STAGE_ADDRESS__r.County__c,
                        RecordTypeId = addressStageRecord.OK_STAGE_ADDRESS__r.RecordTypeId__c,
                        Account_vod__r = acct,
                        Primary_vod__c = primaryFlag,
                        // Change process code to the implementation dependant name
                        OK_Process_Code_BI__c = processCode,  //To clarify
                        Home_vod__c = isHome,
                        // change the Integration time field to the implementation specific one
                        OK_IntegrationTime_BI__c = system.now(),
                        // BI Specific:
                        OK_Address_Type_Code__c = addressStageRecord.OK_Address_Type_Code__c,
                        OK_Building_Name_BI__c = addressStageRecord.OK_Building_Name_BI__c,
                        OK_Po_Box__c = addressStageRecord.OK_Po_Box__c,
                        //BI specific Wave 2:
                        Brick_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.Brick_Conversion_JJ__c,
                        OK_Cegedim_Post_Ref_ID_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_Cegedim_Post_Ref_ID_BI__c,
                        OK_Address_Label_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_Address_Label_BI__c,
                        Address_line_2_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.Address_line_2_vod__c,
                        OK_Street_Number_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_Street_Number_BI__c,
                        OK_City_Code_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_City_Code_BI__c,
                        OK_OFF_SUBDIVISION_COD_3_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_OFF_SUBDIVISION_COD_3_BI__c,
                        OK_OFF_SUBDIVISION_COD_5__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_OFF_SUBDIVISION_COD_5__c,
                        OK_DISPATCH_LBL_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_DISPATCH_LBL_BI__c,
                        OK_SUBDIVISION_COD_5_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_SUBDIVISION_COD_5_BI__c,
                        OK_SUBDIVISION_LABEL_2_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_SUBDIVISION_LABEL_2_BI__c,
                        OK_SUBDIVISION_LABEL_5_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_SUBDIVISION_COD_5_BI__c,
                        OK_BRICK_NUMBER_2_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_2_BI__c,
                        OK_BRICK_NUMBER_3_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_3_BI__c,
                        OK_BRICK_NUMBER_4_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_4_BI__c,
                        OK_BRICK_NUMBER_7_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_7_BI__c,
                        //RCV brick
                        OK_Brick_Name_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_Brick_Name_BI__c,
                        Mini_Brick_Name_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.Mini_Brick_Name_BI__c,
                        Mini_Brick_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.Mini_Brick_BI__c,
                        OK_MiniBrick_Name_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_MiniBrick_Name_BI__c,
                        //GR mapping
                        OK_Keywordstreet_BI__c  = addressStageRecord.OK_STAGE_ADDRESS__r.OK_Keywordstreet_BI__c,
                        OK_AREA_LBL_BI__c  = addressStageRecord.OK_STAGE_ADDRESS__r.OK_AREA_LBL_BI__c,
                        //AU geoloc
                        OK_Geo_Type_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_Geo_Type_BI__c,
                        OK_Latitude_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_Latitude_BI__c,
                        OK_Longitude_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_Longitude_BI__c,
                        OK_Geo_Level_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_Geo_Level_BI__c,
                        //Nordic+Belu private address
                        Private_BI__c = addressStageRecord.OK_Stage_Workplace__r.Home__c
                    );

                } else {
                    //OLD MAPPING
                    address = new Address_vod__c(
                        Name = addressStageRecord.OK_STAGE_ADDRESS__r.Name,
                        //Brick_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.Brick_vod__c,
                        City_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.City_vod__c,
                        Country_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.Country_Code_BI__c,
                        Country_Code_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.Country_Code_BI__c,
                        Zip_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.Zip_vod__c,
                        //State_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.State_vod__c,
                        State_vod__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_State_Province_BI__r.Name,
                        OK_State_Province_BI__c = addressStageRecord.OK_STAGE_ADDRESS__r.OK_State_Province_BI__c,
                        // BI does not use the County field
                        //ACT_CORE_County__c = addressStageRecord.OK_STAGE_ADDRESS__r.County__c,
                        RecordTypeId = addressStageRecord.OK_STAGE_ADDRESS__r.RecordTypeId__c,
                        Account_vod__r = acct,
                        Primary_vod__c = primaryFlag,
                        // Change process code to the implementation dependant name
                        OK_Process_Code_BI__c = processCode,
                        Home_vod__c = isHome,
                        // change the Integration time field to the implementation specific one
                        OK_IntegrationTime_BI__c = system.now(),
                        // BI Specific:
                        OK_Address_Type_Code__c = addressStageRecord.OK_Address_Type_Code__c,
                        OK_Building_Name_BI__c = addressStageRecord.OK_Building_Name_BI__c
                    );
                }

                //ADD HERE CODE  TO HANDLE DIFFEWRENT  ADDRESS TYPES(billing/Mailing/secondary/...).  CUSTOMER DEPENDENT
                address.Billing_vod__c = addressStageRecord.Billing__c;
                address.Mailing_vod__c = addressStageRecord.Mailing__c;
                address.Business_vod__c = addressStageRecord.Business__c;

                // Change Process code to the implementation specific one.
                if (primaryFlag == false && address.OK_Process_Code_BI__c <> 'D') {
                    // Change Process code to the implementation specific one.
                    address.OK_Process_Code_BI__c = 'D0';
                }

                //ADD HERE CODE  TO HANDLE DIFFERENT  ADDRESS TYPES(billing/Mailing/secondary/...).  CUSTOMER DEPENDENT

                System.Debug('Create Address: NAME: ' + address.Name + ' Primary: ' + address.Primary_vod__c);


                //Calculate EXTERNAL ID.  Take  care that different  customers  might
                //use different  naming  convention for ExtID (ACT_CORE_OneKey_Id__c)

                //Split the External Id
                String AddrRelExtId = (String) addressStageRecord.External_Id__c;

				//remove STAGE if available
				if (AddrRelExtId != null && AddrRelExtId.contains('STAGE')) {
					AddrRelExtId = AddrRelExtId.remove('STAGE_').remove('STAGE');
				}

				address.OK_External_ID_BI__c = AddrRelExtId;
				
				/*
                if (AddrRelExtId != null && AddrRelExtId.contains('STAGE')) {
                    //Split the records with the word Stage
                    List<String> extId = AddrRelExtId.split('_');

                    address.OK_External_ID_BI__c = extId[extId.size() - 1];
                } else {
                    address.OK_External_ID_BI__c = AddrRelExtId;
                    // We do NOT use External_ID_vod__c for OneKey IDs in BI
                    //if (country != 'CA' && country != 'DK' && country != 'FO' && country != 'GL') {
                    //    address.External_ID_vod__c = AddrRelExtId;
                    //}
                    
                }
				*/

                System.debug(
                    'External ID: ' + address.OK_External_ID_BI__c
                    + ', Process code: ' + address.OK_Process_Code_BI__c
                    + ', Primary: ' + address.Primary_vod__c
                    + ', Address name: ' + address.Name
                );

                addressRecs.add(address);

                if (address.Primary_vod__c == true) {
                    addNewPrimary(AccountExtId, address);
                }
            }
        }

        //if we have items in the list do an upsert
        if (!addressRecs.isEmpty()) {
            System.Debug('UPSERT Address_vod__c');


            //Database.Upsertresult[] results = Database.upsert(addressRecs,Address_vod__c.External_ID_vod__c,false);  //TAKE CARE TO USE THAT EXTERNAL ID WHICH HOLDS  THE ONEKEY IDs
            Database.Upsertresult[] results = Database.upsert(addressRecs, Address_vod__c.OK_External_ID_BI__c, false); //TAKE CARE TO USE THAT EXTERNAL ID WHICH HOLDS  THE ONEKEY IDs


            String ErrorMessage = '';
            if (results != null) {
                for (Database.Upsertresult result : results) {
                    if (!result.isSuccess()) {
                        Database.Error[] errs = result.getErrors();
                        for (Database.Error err : errs) {
                            ErrorMessage = ErrorMessage + err.getStatusCode() + ' : ' + err.getMessage() + '\r';
                        }
                        FailureNumber = FailureNumber + 1;
                    } else
                        SuccessNumber = SuccessNumber + 1;
                }
            }
            if (ErrorMessage.length() > 1) {
                VEEVA_BATCH_ONEKEY_BATCHUTILS.setErrorMessage(jobId, ErrorMessage);
            }


        }

    }

    /***********************************************
    set status to completed  then send an email then
    trigger the next JOB.
    ***********************************************/
    global void finish(Database.BatchableContext BC) {
        VEEVA_BATCH_ONEKEY_BATCHUTILS.setErrorMessage(jobId, 'TotalBatchSize =' + this.totalNumber + ' Success Upsert = ' + this.SuccessNumber + ' Failed Upsert = ' + this.FailureNumber);
        //VEEVA_BATCH_ONEKEY_BATCHUTILS.setCompleted(jobId,lastRunTime);
        setCompleted(jobId, lastRunTime);
    }


    /******************************* 2012.11.21. ********************************************/
    /* Add this here from batchutil class  only  to avoid cross-refference deployment error */
    /*******************************************
     Updates the job status to STATUS_COMPLETED
     and populates the end time with the current
     system date/time.
     This  function will initiate a trigger which
     will  kick of the next  job  later
     *******************************************/
    public static void setCompleted(Id jobId, DateTime LRT) {
        if (jobId != null) {
            List<V2OK_Batch_Job__c> jobs = [SELECT Id FROM V2OK_Batch_Job__c
                                            WHERE Id = :jobId
                                           ];
            if (!jobs.isEmpty()) {
                V2OK_Batch_Job__c job = jobs.get(0);
                job.Status__c = 'Completed';
                job.End_Time__c = Datetime.now();
                job.LastRunTime__c = LRT;
                update job;
            }
        }
    }

    /***********************************************************
    insert a record  into a custom object:   Batch_Job_Error__c
    ***********************************************************/
    public static void setErrorMessage(Id jobId, String Message) {
        if (jobId != null) {
            //Create an error message
            Batch_Job_Error__c jobError = new Batch_Job_Error__c();
            jobError.Error_Message__c = Message;
            jobError.Veeva_To_One_Key_Batch_Job__c = jobId;
            jobError.Date_Time__c = Datetime.now();
            insert jobError;
        }
    }
    /* Add this here from batchutil class  only  to avoid cross-refference deployment error */
    /******************************* 2012.11.21. ********************************************/
}