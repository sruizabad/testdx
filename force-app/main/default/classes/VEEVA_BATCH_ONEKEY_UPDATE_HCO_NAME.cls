/**
 * Name:   VEEVA_BATCH_ONEKEY_UPDATE_HCO_NAME
 * Author: Raphael Krausz <raphael.krausz@veeva.com>
 * Date:   2014-08-28
 * Description: 
 * 
 * Batch class to update the Name of the HCO account.
 *
 * Only for AU, NZ, CA, BE, NL, LU, NO, DK, SE, FI, FO, GL and GR - HCO accounts which have parents
 *
 * For all countries other than Canada: if it is an HCO account with a parent, its name field
 * becomes a concatenation of its parent's name and its own - [parent's name] : [name]
 *
 * OneKey_Name_BI__c holds it's original name before concatenation
 *
 *
 * CANADA NOTES
 * ============
 *
 * WKP_STR_TYPE_COD - HIE LEVEL for BEST PARENT NAME - Description
 * TSR.WCA.DIV - LEVEL 1 DIVISION   - Level 1 Parent can be added
 * TSR.WCA.DPT - LEVEL 1 DEPARTMENT - Requires Level 1 Parent
 * TSR.WCA.GOR - N/A                - GROUP OF ORGANIZATIONS - Name Stand-alone
 * TSR.WCA.ORG - LEVEL 1            - ORGANIZAION - Name Stand-alone
 * TSR.WCA.SUB - LEVEL 2 FOR WKP TET (HOF, EUD, UNI)
 * TSR.WCA.SUB - LEVEL 1 FOR BALANCE - SUBDEPARTENTS:  Specific Workplace Spec -
 *                                   Main Parent Name needs Level 2 HIE record balance Level 1 HIE record
 *
 * WKP TET = OK_Workplace_Type_BI__r.CODE_ID_CEGEDIM_BI__c
 *
 * OK_Structure_Type_BI__c
 * COD_ID_CEGEDIM  COD_LONG_LBL
 * TSR.WCA.DIV     'Division'
 * TSR.WCA.DPT     'Department'
 * TSR.WCA.ORG     'Organisation'
 * TSR.WCA.SUB     'Sub Department'
 *
 *
 * Modified: 2014-09-04
 * Author:   Raphael Krausz <raphael.krausz@veeva.com>
 * Description:
 *      Made multi-country, added code for AU
 *
 *
 * Modified: 2014-09-05
 * Author:   Raphael Krausz <raphael.krausz@veeva.com>
 * Description:
 *      Finished adding code for remaining countries as per above
 *
 *
 * Modified: 2014-09-10
 * Author:   Raphael Krausz <raphael.krausz@veeva.com>
 * Description:
 *      Enhanced code so that having OneKey_Name_BI__c field is no longer mandatory. 
 *      - The VDC does not need to populate the OneKey_Name_BI__c field
 *      - Historical data will automatically have the OneKey_Name_BI__c populated
 *
 *      As before, the OneKey_Name_BI__c field will contain the original name of the HCO.
 *
 *
 * Modified: 2015-06-02
 * Author:   Raphael Krausz <raphael.krausz@veeva.com>
 * Description:
 *      Attempting to improve SOQL and hence performance
 *
 *
 * Modified: 2015-10-15
 * Author:   Raphael Krausz <raphael.krausz@veeva.com>
 * Description:
 *      Modified SOQL to ensure if a parent or grandparent account changes name,
 *      then the account names also gets updated correctly.
 *
 */

global without sharing class VEEVA_BATCH_ONEKEY_UPDATE_HCO_NAME implements Database.Batchable<SObject>, Database.Stateful {

    private final Id jobId;
    private final Datetime lastRunTime;

    global String country;

    global Integer successfulUpdates        = 0;
    global Integer failedUpdates            = 0;
    global Integer totalAccountsProcessed   = 0;
    global Integer totalUpdateAttempts      = 0;

    private Map<Id, Account> accountsToUpdate = new Map<Id, Account>();

    public class WrongCountryException extends Exception {}

    private enum departmentAncestor { PARENT, GRANDPARENT }

    public static final Set<String> allowedCountries = new Set<String> {
        'AU',
        'CA',
        'NZ',
        'BE',
        'NL',
        'LU',
        'NO',
        'DK',
        'SE',
        'FI',
        'FO',
        'GL',
        'GR'
    };

    private final Set<String> structureTypeCountriesSet = new Set<String> {
        // 'AU',
        'CA'
    };

    private final Map<String, Set<String>> structureTypeMapSet = new Map<String, Set<String>> {

        /*
        'AU' => new Set<String> {
            'Organisation',
            'Department'
        },
        */

        'CA' => new Set<String> {
            'Division',
            'Department',
            'Organisation',
            'Sub Department'
        }

    };

    private final Map<String, Set<String>> workplaceTypeGrandparentCodeMapSet = new Map<String, Set<String>> {

        /*
        'AU' => new Set<String> {
        },
        */

        'CA' => new Set<String> {
            'TET.WCA.HOF',
            'TET.WCA.EUD',
            'TET.WCA.UNI'
        }
    };

    private Boolean isStructureTypeCountry;

    private final Set<String> workplaceTypeGrandparentCodeSet;

    public VEEVA_BATCH_ONEKEY_UPDATE_HCO_NAME() {
        // If the class is called with no constructor parameters,
        // then call the single country constructor and let the
        // blank country exception be thrown.

        this('');
    }


    public VEEVA_BATCH_ONEKEY_UPDATE_HCO_NAME(String country) {
        // if we are called with no parameters except country,
        // then call the correct constructor with defaults filled
        // in as to go over the entire data set for the given country

        this(null, DateTime.newInstance(2014, 1, 1), country);
    }

    public VEEVA_BATCH_ONEKEY_UPDATE_HCO_NAME(Id jobId, DateTime lastRunTime, String country) {

        // set up batch job details
        this.jobId = jobId;
        this.lastRunTime = lastRunTime;
        this.country = country.toUpperCase();

        Boolean countryIsBlank = String.isBlank(country);

        if ( countryIsBlank || ! allowedCountries.contains(country) ) {
            // If we are called for a country other than Canada, then move on without processing.
            // Make sure the error is reported, and mark the batch job as complete.
            String errorMessage = 'Wrong Country Exception. ';
            if ( countryIsBlank ) {
                errorMessage += 'Country cannot be blank.';
            } else {
                errorMessage += 'Wrong country (' + country + ').';
            }

            VEEVA_BATCH_ONEKEY_BATCHUTILS.setErrorMessage(jobId, errorMessage);
            VEEVA_BATCH_ONEKEY_BATCHUTILS.setCompleted(jobId, lastRunTime);

            WrongCountryException countryException = new WrongCountryException(errorMessage);
            throw countryException;

        }

        isStructureTypeCountry = structureTypeCountriesSet.contains(this.country);

        workplaceTypeGrandparentCodeSet = workplaceTypeGrandparentCodeMapSet.get(country);

        System.Debug('CONSTRUCTOR BATCH VEEVA_BATCH_ONEKEY_UPDATE_HCO_NAME');

    }


    global Database.QueryLocator start(Database.BatchableContext bc) {

        String lastRunTimeString = lastRunTime.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.\'000Z\'');

        Set<String> structureTypeSet = structureTypeMapSet.get(country);

        String whereClausePart;

        if ( isStructureTypeCountry ) {
            whereClausePart = '   AND OK_Structure_Type_BI__c IN :structureTypeSet';
        } else {
            whereClausePart = '';
        }

        // N.B. I get the parent, grandparent and great-grandparent accounts' names for departments
        String selectStatement =
            'SELECT'
            + '  Id,'
            + '  Name,'
            + '  OneKey_Name_BI__c,'
            + '  Primary_Parent_vod__r.OneKey_Name_BI__c,'
            + '  Primary_Parent_vod__r.Primary_Parent_vod__r.OneKey_Name_BI__c,'
            + '  Primary_Parent_vod__r.Name,'
            + '  Primary_Parent_vod__r.Primary_Parent_vod__r.Name,'
            + '  OK_Workplace_Type_BI__r.CODE_ID_CEGEDIM_BI__c,'
            + '  OK_Structure_Type_BI__c,'
            + '  OK_External_ID_BI__c'
            + ' FROM Account'
            + '   WHERE ('
            + '     OneKey_Name_BI__c = null'
            + '     OR ('
            + '         SystemModStamp >= ' + lastRunTimeString
            + '         OR Primary_Parent_vod__r.SystemModStamp >= ' + lastRunTimeString
            + '         OR Primary_Parent_vod__r.Primary_Parent_vod__r.SystemModStamp >= ' + lastRunTimeString
            + '     )'
            + '   )'
            + '   AND Country_Code_BI__c = \'' + country + '\''
            + '   AND isPersonAccount = false'
            + '   AND Primary_Parent_vod__c != null'
            + whereClausePart
            + '   AND OK_External_ID_BI__c LIKE \'W' + country + '%\''
            ;

        System.Debug('SQLU: ' + selectStatement);


        return Database.getQueryLocator(selectStatement);
    }


    global void execute(Database.BatchableContext bc, List<sObject> batch) {


        totalAccountsProcessed += batch.size();

        List<Account> accountList = (List<Account>) batch;


        for (Account theAccount : accountList) {
            if ( String.isBlank(theAccount.OneKey_Name_BI__c) ) {
                theAccount.OneKey_Name_BI__c = theAccount.Name;
                accountsToUpdate.put(theAccount.Id, theAccount);
            }
        }


        processDepartments(accountList);

        updateAccounts();
    }

    private void updateAccounts() {

        Integer numberOfAccountsToUpdate = accountsToUpdate.size();
        totalUpdateAttempts += numberOfAccountsToUpdate;

        if ( numberOfAccountsToUpdate == 0 ) {
            // If there isn't anything to do - don't even try to do it.
            return;
        }

        List<Account> accountsToUpdateList = accountsToUpdate.values();

        Database.SaveResult[] saveResults = Database.update(accountsToUpdateList, false);

        for (Database.SaveResult result : saveResults) {
            Boolean wasSaveSuccessful = result.isSuccess();

            String message =
                (
                    ( wasSaveSuccessful )
                    ? 'Successfully updated '
                    : 'Problem updating '
                )
                + 'Account Id: '
                + result.getId()
                ;

            if ( wasSaveSuccessful ) {
                message += '\r\n\r\nNo errors.';
            } else {
                for (Database.Error err : result.getErrors()) {
                    message +=
                        '\r\n\r\nError:\r\n'
                        + err.getMessage()
                        + ' ('
                        + err.getStatusCode()
                        + ').\r\n'
                        + 'Field(s): '
                        + err.getFields()
                        ;
                }
            }

            System.debug(message);
            VEEVA_BATCH_ONEKEY_BATCHUTILS.setErrorMessage(jobId, message);


            accountsToUpdate = new Map<Id, Account>();
        }
    }


    private Boolean isEmpty(List<Account> anAccountList) {
        if ( anAccountList == null )
            return true;

        if ( anAccountList.size() < 1 )
            return true;

        return false;
    }


    private Map<Id, Address_vod__c> getAccountAddressMap(List<Account> accounts) {
        Set<Id> accountIdSet = new Set<Id>();
        for (Account theAccount : accounts) {
            accountIdSet.add(theAccount.Id);
        }

        List<Address_vod__c> addressList =
            [
                SELECT Account_vod__c, City_vod__c
                FROM Address_vod__c
                WHERE Account_vod__c IN :accountIdSet
                AND Primary_vod__c = true
            ];

        Map<Id, Address_vod__c> accountIdToAddressMap = new Map<Id, Address_vod__c>();

        for (Address_vod__c theAddress : addressList) {
            Id accountId = theAddress.Account_vod__c;
            accountIdToAddressMap.put(accountId, theAddress);
        }

        return accountIdToAddressMap;
    }

    private departmentAncestor getAncestorLevel(Account theAccount) {
        String workplaceTypeCode = theAccount.OK_Workplace_Type_BI__r.CODE_ID_CEGEDIM_BI__c;
        String structureType     = theAccount.OK_Structure_Type_BI__c;


        // For most countries just the parent is enough
        if ( ! isStructureTypeCountry ) {
            return departmentAncestor.PARENT;
        }


        if ( structureType.equals('Sub Department') ) {
            if ( workplaceTypeGrandparentCodeSet.contains(workplaceTypeCode) ) {
                return departmentAncestor.GRANDPARENT;
            }
        }

        // others must be parents
        return departmentAncestor.PARENT;
    }

    private void processDepartments(List<Account> departmentAccounts) {
        if ( isEmpty(departmentAccounts) ) {
            return;
        }

        for (Account department : departmentAccounts) {

            departmentAncestor parentLevel = getAncestorLevel(department);

            String ancestorName;

            if ( parentLevel == departmentAncestor.GRANDPARENT ) {

                ancestorName = department.Primary_Parent_vod__r.Primary_Parent_vod__r.OneKey_Name_BI__c;
                if ( String.isBlank(ancestorName) ) {
                    ancestorName = department.Primary_Parent_vod__r.Primary_Parent_vod__r.Name;
                }

            } else {
                // ( parentLevel == deparmentAncestor.PARENT ) -> TRUE
                ancestorName = department.Primary_Parent_vod__r.OneKey_Name_BI__c;
                if ( String.isBlank(ancestorName) ) {
                    ancestorName = department.Primary_Parent_vod__r.Name;
                }
            }

            String departmentName   = department.OneKey_Name_BI__c;
            String concatenatedName = ancestorName + ' : ' + departmentName;

            if ( String.isBlank(ancestorName) ) {
                String message =
                    'Could not find ancestor name! - Id: '
                    + department.Id
                    + ' ('
                    + department.OK_External_ID_BI__c
                    + ')'
                    ;
                System.debug(message);
            } else {
                updateAccountName(department, concatenatedName);
            }
        }
    }

    private void updateAccountName(Account theAccount, String newName) {
        String debugMessage;
        if ( theAccount.Name.equals(newName) ) {
            // The new name for tha account matches what the account already has
            // Do nothing except set the debug message to say so.
            debugMessage = 'Account already has the desired name';
        } else {
            // the new name for the account doesn't match the existing name
            // update the account
            theAccount.Name = newName;
            accountsToUpdate.put(theAccount.Id, theAccount);
            debugMessage = 'Account updated with new name';
        }

        debugMessage +=
            ' - Id: '
            + theAccount.Id
            + ', External ID: '
            + theAccount.OK_External_ID_BI__c
            ;

        System.debug(debugMessage);

        // Now if we're starting to get close to the limit of how many accounts we can update
        // then update the accounts now. (This will also be done later for any remaining accounts.)
        if (accountsToUpdate.size() > 9000) {
            updateAccounts();
        }

    }


    global void finish(Database.BatchableContext bc) {
        // Update the batch job with the status of events
        // and tell the batch utilities the job is complete

        String message =
            'Total addresses processed: '
            + totalAccountsProcessed
            + ';\r\n'
            + 'Total accounts attempted to update: '
            + totalUpdateAttempts
            + ';\r\n'
            + 'Total accounts succesfully updated: '
            + successfulUpdates
            + ';\r\n'
            + 'Total accounts failed to update: '
            + failedUpdates
            + ';\r\n'
            ;

        VEEVA_BATCH_ONEKEY_BATCHUTILS.setErrorMessage(jobId, message);
        VEEVA_BATCH_ONEKEY_BATCHUTILS.setCompleted(jobId, lastRunTime);
    }

}