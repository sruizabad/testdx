/*********************************************************************************************************
Class calculate the address of an INDIVIDUAL  based o the ACTIVITY file and WKP_ADDRESS_RELATION file

*********************************************************************************************************/
global without sharing class VEEVA_BATCH_ONEKEY_UPDATE_IND_ADDRESS implements Database.Batchable<SObject>, Database.Stateful {

    private final Id jobId;
    private final Datetime lastRunTime;
    private final String stringLastRunTime;

    private final String country;
    private final String countryPredicate;

    global Integer  successes = 0;
    global Integer  failures = 0;
    global Integer  total = 0;


    // For storing addresses from the database
    // Map<OneKey id, address>
    private Map<String, Address_vod__c> dbAddresses;

    // For storing addresses to be upserted
    // Map<OneKey id, address>
    private Map<String, Address_vod__c> toUpsert = new Map<String, Address_vod__c>();


    // Current activity/child account and address records - when processing in for loops
    private OK_Stage_Activity__c    stageActivity;
    private OK_STAGE_ADDRESS_INT__c stageAddressInt;


    private Map<String, Address_vod__c> individualIdToPrimaryAddressMap;




    public VEEVA_BATCH_ONEKEY_UPDATE_IND_ADDRESS() {
        this(null, null, null);
    }

    public VEEVA_BATCH_ONEKEY_UPDATE_IND_ADDRESS(Id jobId, Datetime lastRunTime) {
        this(jobId, lastRunTime, null);
    }

    public VEEVA_BATCH_ONEKEY_UPDATE_IND_ADDRESS(Id jobId, Datetime lastRunTime, String country) {

        if (lastRunTime == null)
            lastRunTime = DateTime.newInstance(1970, 1, 1);

        if (String.isBlank(country)) {
            countryPredicate = ' AND External_Id__c LIKE \'W%\'';
        } else {
            countryPredicate = ' AND External_Id__c LIKE \'W' + country + '%\'';
        }

        this.jobId        = jobId;
        this.lastRunTime  = lastRunTime;
        this.country      = country;

        stringLastRunTime = lastRunTime.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.\'000Z\'');
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String selStmt =
            'SELECT'
            + '  Stage_Workplace__r.External_Id__c,'
            + '  OK_Process_Code__c,'
            + '  External_Id__c,'
            + '  Stage_Workplace__c,'
            + '  Stage_Workplace__r.Workplace_External_Id__c,'
            + '  Stage_Workplace__r.Workplace_Name__c,'
            + '  OK_ACT_PREF_MAIL_FLAG_BI__c,'
            + '  Do_Not_Mail__c,'
            + '  Individual_External_Id__c,'
            + '  Mailing__c,'
            + '  Primary__c,'
            + '  Active_Status__c,'
            + '  ACT_CORE_OK_Activity_ID__c,'
            + '  OK_Act_Phone__c,'
            + '  OK_Act_Fax__c,'
            + '  OK_Phone_Extension__c,'
            + '  OK_PAD_BI__c,'
            + '  OK_PAD_OLD_BI__c,'
            + '  OK_PVA_BI__c,'
            + '  OK_PVA_DELTA_BI__c,'
            + '  OK_PVIRV_BI__c,'
            + '  OK_PVACT6M_BI__c,'
            + '  OK_PVACT3M_BI__c,'
            + '  OK_PVACT1M_BI__c'
            + ' FROM  OK_Stage_Activity__c'
            + ' WHERE SystemModstamp >= ' + stringLastRunTime
            + countryPredicate
            ;

        system.Debug('SELECT statement: ' + selStmt);
        return Database.getQueryLocator(selStmt);
    }

    global void execute(Database.BatchableContext BC, List<OK_Stage_Activity__c> activityStageRecords) {

        System.debug('execute batch.size(): ' + activityStageRecords.size());

        processBatch(activityStageRecords);

        // And upsert the remaining results
        upsertAddresses();
    }


    public void upsertAddresses() {

        System.debug('toUpsert.size(): ' + toUpsert.size());

        List<Database.UpsertResult> results;
        if (toUpsert.size() > 0) {
            List<Address_vod__c> addressesForUpsert = toUpsert.values();
            results = Database.upsert(addressesForUpsert, Address_vod__c.OK_External_ID_BI__c, false);

            // Debugging
            Address_vod__c firstAddress = addressesForUpsert[0];
            System.debug('External ID of first address: ' + firstAddress.OK_External_ID_BI__c);


            total += results.size();
            for (Database.UpsertResult result : results) {
                if (result.isSuccess()) {
                    successes += 1;
                } else {
                    failures += 1;
                    System.debug('Upsert error: ' + result.getErrors());
                }
            }

            System.debug('Upsert results - successes: ' + successes + '; failures: ' + failures);
        }

        // This method may be called more than once during an execution
        // In which case we add all of the already upserted addresses to
        // the list of what is in the database, and reset the toUpsert map
        dbAddresses.putAll(toUpsert);
        toUpsert = new Map<String, Address_vod__c>();

    }

    public void processBatch(List<OK_Stage_Activity__c> activityStageRecords) {
        //Csabas SP and PT

        // This needs to be here, as this method may be called directly 
        // e.g. by VEEVA_BATCH_ONEKEY_IND_ADDRESS_EXT
        individualIdToPrimaryAddressMap = new Map<String, Address_vod__c>();

        System.Debug('Result size = ' + activityStageRecords.size() );  //CSABA

        Set<Id> stageWkpIds = new Set<Id>();

        //Will need both the individual id and wkpid
        for (OK_Stage_Activity__c activity : activityStageRecords) {
            String stageWorkplaceId = activity.Stage_Workplace__c;
            stageWkpIds.add(stageWorkplaceId);
            System.debug('stageWorkplaceId: ' + stageWorkplaceId);
        }

        //COLLECT all addresses  belonging to the above list of WKPs
        List<OK_STAGE_ADDRESS_INT__c> addrStageRecords = getStagedAddresses(stageWkpIds);

        System.debug('addrStageRecords.size(): ' + addrStageRecords.size());

        // Get all the workplaces addresses
        StageAddressMap workplaceStageAddressMap = new StageAddressMap();
        for (OK_STAGE_ADDRESS_INT__c stageAddressInt : addrStageRecords) {
            String stageWorkplace = stageAddressInt.OK_Stage_Workplace__c;
            System.debug('stageAddressInt.OK_Stage_Workplace__c: ' + stageWorkplace);
            workplaceStageAddressMap.put(stageWorkplace, stageAddressInt);
        }

        System.debug('workplaceStageAddressMap.size(): ' + workplaceStageAddressMap.size());

        // Get all OneKey address ids for the addresses that already exist in the database
        Set<String> addressExternalIds = new Set<String>();
        for (OK_Stage_Activity__c stageActivity : activityStageRecords) {
            // External_ID_vod__c  = stageActivity.Individual_External_Id__c + stageAddressInt.OK_STAGE_ADDRESS__r.External_ID_vod__c

            String individualId = stageActivity.Individual_External_Id__c;

            if ( String.isBlank(individualId) ) continue;

            System.debug('stageActivity.Stage_Workplace__c: ' + stageActivity.Stage_Workplace__c);

            List<OK_STAGE_ADDRESS_INT__c> stageAddresses =
                workplaceStageAddressMap.get(stageActivity.Stage_Workplace__c);
            if (stageAddresses == null) continue;

            for (OK_STAGE_ADDRESS_INT__c stageAddressInt : stageAddresses) {
                String addressOneKeyId = stageAddressInt.OK_STAGE_ADDRESS__r.External_ID_vod__c;
                String addressExternalId = makeAddressExternalId(addressOneKeyId, individualId);

                System.debug('addressOneKeyId: ' + addressOneKeyId);
                System.debug('addressExternalId: ' + addressExternalId);

                addressExternalIds.add(addressExternalId);
            }
        }

        System.debug('addressExternalIds.size(): ' + addressExternalIds.size());

        dbAddresses = getDbAddresses(addressExternalIds);

        System.debug('dbAddresses.size(): ' + dbAddresses.size());

        // Now we process all of the activities
        for (OK_Stage_Activity__c stageActivity : activityStageRecords) {

            if (stageActivity.Individual_External_Id__c == null) continue;

            System.debug('stageActivity.Individual_External_Id__c: ' + stageActivity.Individual_External_Id__c);

            List<OK_STAGE_ADDRESS_INT__c> stageAddresses =
                workplaceStageAddressMap.get(stageActivity.Stage_Workplace__c);

            if (stageAddresses == null) continue;

            System.debug('stageAddresses.size(): ' + stageAddresses.size());

            for (OK_STAGE_ADDRESS_INT__c stageAddressInt : stageAddresses) {

                this.stageActivity   = stageActivity;
                this.stageAddressInt = stageAddressInt;

                processActivity();
            }
        }
    }



    private static Address_vod__c makeAddress(
        Account acct,
        String externalId,
        String processCode,
        String externalAccountId,
        OK_STAGE_ADDRESS_INT__c stageAddressInt,
        OK_Stage_Activity__c stageActivity
    ) {

        // external ID
        // stageAddressInt.OK_STAGE_ADDRESS__r.External_ID_vod__c + actvStageRecord.Individual_External_Id__c,  //2012.07.21.

        System.Debug('CSABA:  makeAddress');

        Address_vod__c address;
        DateTime now = system.now();

        if ( externalAccountId.startsWith('WCA') ) {
            // if ( country.equalsIgnoreCase('CA') ) {
            // Old CA Mapping
            address = new Address_vod__c(
                Name = stageAddressInt.OK_STAGE_ADDRESS__r.Name,
                City_vod__c = stageAddressInt.OK_STAGE_ADDRESS__r.City_vod__c,

                // Country_Code_BI must be copied also
                Country_vod__c = stageAddressInt.OK_STAGE_ADDRESS__r.Country_Code_BI__c,
                Country_Code_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.Country_Code_BI__c,
                Zip_vod__c = stageAddressInt.OK_STAGE_ADDRESS__r.Zip_vod__c,

                OK_State_Province_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_State_Province_BI__c,
                State_vod__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_State_Province_BI__r.name,

                RecordTypeId = stageAddressInt.OK_STAGE_ADDRESS__r.RecordTypeId__c,
                Account_vod__r = acct,

                OK_IntegrationTime_BI__c = now, // Change field name to be installation dependent.
                OK_External_ID_BI__c = externalId,

                // NO - we do not use External_ID_vod__c for BI we only use OK_External_ID_BI__c
                // External_ID_vod__c = externalId,

                OK_Process_Code_BI__c = processCode
            );
        } else {
            String stageCountryVodField = stageAddressInt.OK_STAGE_ADDRESS__r.Country_vod__c;
            String countryVodField;

            Boolean preferredAddress = stageActivity.OK_ACT_PREF_MAIL_FLAG_BI__c;
            String parentClass = 'FAD.' + stageActivity.Stage_Workplace__r.Workplace_External_Id__c.substring(3, 4);


            if ( String.isBlank(stageCountryVodField) || stageCountryVodField.equalsIgnoreCase('UK') ) {
                countryVodField = stageAddressInt.OK_STAGE_ADDRESS__r.Country_Code_BI__c;
            } else {
                countryVodField = stageCountryVodField;
            }

            address = new Address_vod__c(

                Name = stageAddressInt.OK_STAGE_ADDRESS__r.Name,
                City_vod__c = stageAddressInt.OK_STAGE_ADDRESS__r.City_vod__c,
                // Country_Code_BI must be copied also
                Country_vod__c = countryVodField,
                Country_Code_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.Country_Code_BI__c,
                Zip_vod__c = stageAddressInt.OK_STAGE_ADDRESS__r.Zip_vod__c,
                // State_vod__c = aStageAddressRel.OK_STAGE_ADDRESS__r.State_vod__c,
                // State_vod__c not used for BI OK.
                // BI Canada uses OK_State_Province_BI__c for Provinces - county's are not used
                OK_State_Province_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_State_Province_BI__c,
                State_vod__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_State_Province_BI__r.name,
                //ACT_CORE_County__c = aStageAddressRel.OK_STAGE_ADDRESS__r.County__c,
                RecordTypeId = stageAddressInt.OK_STAGE_ADDRESS__r.RecordTypeId__c,
                Account_vod__r = acct,
                //Primary_vod__c = actvStageRecord.Primary__c,  //not true!!
                OK_IntegrationTime_BI__c = now, // Change field name to be installation dependent.
                OK_External_ID_BI__c = externalId,

                // NO - we do not use External_ID_vod__c for BI we only use OK_External_ID_BI__c
                // External_ID_vod__c = externalId,

                // Change the Process code name to the implementation specific one
                OK_Process_Code_BI__c = processCode,
                //BI specific Wave 2:
                //Best_Times_vod__c = aStageAddressRel.OK_STAGE_ADDRESS__r.Best_Times_vod__c, //in a separate class, not to be used
                Brick_vod__c = stageAddressInt.OK_STAGE_ADDRESS__r.Brick_Conversion_JJ__c,
                OK_Cegedim_Post_Ref_ID_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_Cegedim_Post_Ref_ID_BI__c,
                OK_Address_Label_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_Address_Label_BI__c,
                Address_line_2_vod__c = stageAddressInt.OK_STAGE_ADDRESS__r.Address_line_2_vod__c,
                OK_Street_Number_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_Street_Number_BI__c,
                OK_City_Code_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_City_Code_BI__c,
                OK_DISPATCH_LBL_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_DISPATCH_LBL_BI__c,
                OK_SUBDIVISION_COD_5_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_SUBDIVISION_COD_5_BI__c,
                OK_SUBDIVISION_LABEL_2_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_SUBDIVISION_LABEL_2_BI__c,
                OK_SUBDIVISION_LABEL_5_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_SUBDIVISION_COD_5_BI__c,
                OK_OFF_SUBDIVISION_COD_3_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_OFF_SUBDIVISION_COD_3_BI__c,
                OK_OFF_SUBDIVISION_COD_5__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_OFF_SUBDIVISION_COD_5__c,
                OK_BRICK_NUMBER_2_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_2_BI__c,
                OK_BRICK_NUMBER_3_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_3_BI__c,
                OK_BRICK_NUMBER_4_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_4_BI__c,
                OK_BRICK_NUMBER_7_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_7_BI__c,
                OK_Address_Type_Code__c = stageAddressInt.OK_Address_Type_Code__c,
                OK_Po_Box__c = stageAddressInt.OK_Po_Box__c,
                OK_Building_Name_BI__c = stageAddressInt.OK_Building_Name_BI__c,
                //BI_Preferred_Address_BI__c = actvStageRecord.OK_ACT_PREF_MAIL_FLAG_BI__c,
                BI_Preferred_Address_BI__c = preferredAddress,
                //RCV brick
                OK_Brick_Name_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_Brick_Name_BI__c,
                Mini_Brick_Name_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.Mini_Brick_Name_BI__c,
                Mini_Brick_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.Mini_Brick_BI__c,
                OK_MiniBrick_Name_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_MiniBrick_Name_BI__c,
                //GR mapping
                OK_Keywordstreet_BI__c  = stageAddressInt.OK_STAGE_ADDRESS__r.OK_Keywordstreet_BI__c,
                OK_AREA_LBL_BI__c  = stageAddressInt.OK_STAGE_ADDRESS__r.OK_AREA_LBL_BI__c,
                //AU geoloc
                OK_Geo_Type_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_Geo_Type_BI__c,
                OK_Latitude_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_Latitude_BI__c,
                OK_Longitude_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_Longitude_BI__c,
                OK_Geo_Level_BI__c = stageAddressInt.OK_STAGE_ADDRESS__r.OK_Geo_Level_BI__c,
                //FR Consolidated data
                OK_PAD_BI__c = stageActivity.OK_PAD_BI__c,
                OK_PAD_OLD_BI__c = stageActivity.OK_PAD_OLD_BI__c,
                OK_PVA_BI__c = stageActivity.OK_PVA_BI__c,
                OK_PVA_DELTA_BI__c = stageActivity.OK_PVA_DELTA_BI__c,
                OK_PVIRV_BI__c = stageActivity.OK_PVIRV_BI__c,
                OK_PVACT6M_BI__c = stageActivity.OK_PVACT6M_BI__c,
                OK_PVACT3M_BI__c = stageActivity.OK_PVACT3M_BI__c,
                OK_PVACT1M_BI__c = stageActivity.OK_PVACT1M_BI__c,
                //FR CD outbound
                OK_ACT_ID_BI__c = stageActivity.External_Id__c,
                OK_ACT_Parent_ID_BI__c =  stageActivity.Stage_Workplace__r.Workplace_External_Id__c,
                OK_ACT_Parent_Name_BI__c = stageActivity.Stage_Workplace__r.Workplace_Name__c,
                OK_ACT_Parent_Class_BI__c = parentClass
            );
        }

        return address;
    }


    private static String makeAddressExternalId(String addressId, String individualId) {

        addressId = addressId.remove('STAGE_');
        addressId = addressId.remove('STAGE');
        individualId = individualId.remove('STAGE_');
        individualId = individualId.remove('STAGE');

        // String externalId = addressId + '-' + individualId;
        // String externalId = individualId + '_' + addressId;

        String externalId = addressId + individualId;

        return externalId;
    }


    private Address_vod__c makeAddressFromActivityStage() {

        Address_vod__c theAddress = makeAddressFromActivityStage(stageActivity, stageAddressInt);
        System.debug('theAddress.OK_External_ID_BI__c: ' + theAddress.OK_External_ID_BI__c);
        return theAddress;

    }

    public static Address_vod__c makeAddressFromActivityStage(
        OK_Stage_Activity__c stageActivity, OK_STAGE_ADDRESS_INT__c stageAddressInt
    ) {

        System.debug('makeAddressFromActivityStage');

        System.debug('stageActivity.External_Id__c: ' + stageActivity.External_Id__c);
        System.debug('stageAddressInt.External_Id__c: ' + stageAddressInt.External_Id__c);


        String addressId = stageAddressInt.OK_STAGE_ADDRESS__r.External_ID_vod__c;
        String individualId = stageActivity.Individual_External_Id__c;
        String externalId = makeAddressExternalId(addressId, individualId);

        Account acct = new Account(OK_External_ID_BI__c = individualId);

        System.debug('addressId: ' + addressId);
        System.debug('individualId: ' + individualId);
        System.debug('externalId: ' + externalId);


        String processCode;

        // Change the Process code name to the implementation specific one
        // OK_Process_Code_BI__c =  actvStageRecord.Active_Status__c == 'Invalid' ?  'D' : stageAddressInt.OK_Process_Code__c


        if (
            stageAddressInt.OK_End_Date__c == 'Inactive'
            || stageActivity.Active_Status__c == 'Invalid'
            || (
                stageAddressInt.Billing__c == false
                && stageAddressInt.Primary__c == false
                && stageAddressInt.Mailing__c == false
            )
        ) {
            processCode = 'D';
        } else {
            processCode = stageAddressInt.OK_Process_Code__c;
        }

        System.debug('externalId: ' + externalId);
        System.debug('stageActivity.Active_Status__c: ' + stageActivity.Active_Status__c + ' (external ID: ' + externalId + ')');
        System.debug('stageAddressInt.OK_Process_Code__c: ' + stageAddressInt.OK_Process_Code__c + ' (external ID: ' + externalId + ')');
        System.debug('processCode: ' + processCode + ' (external ID: ' + externalId + ')');

        String externalAccountId = stageActivity.Individual_External_Id__c;

        Address_vod__c address = makeAddress(acct, externalId, processCode, externalAccountId, stageAddressInt, stageActivity);

        address.OK_ACT_Parent_ID_BI__c = stageActivity.Stage_Workplace__r.Workplace_External_Id__c;

        Boolean primary = false;


        System.debug('stageActivity.Primary__c: ' + stageActivity.Primary__c + ' (external ID: ' + externalId + ')');
        System.debug('A workplace ext ID: ' + address.OK_ACT_Parent_ID_BI__c);
        System.debug('B stageActivity.Stage_Workplace__r.Workplace_External_Id__c: ' + stageActivity.Stage_Workplace__r.Workplace_External_Id__c);

        System.debug('stageAddressInt.Primary__c: ' + stageAddressInt.Primary__c + ' (external ID: ' + externalId + ')');

        if ( stageAddressInt.Primary__c && stageActivity.Primary__c ) primary = true;

        if ( processCode == 'D' ) primary = false;

        address.Primary_vod__c = primary;

        System.debug('primary: ' + primary + ' (external ID: ' + externalId + ')');
        System.debug('address.OK_External_ID_BI__c: ' + address.OK_External_ID_BI__c);

        return address;
    }

    // private makeAddress(OK_STAGE_ADDRESS_INT__c aStageAddressRel, Account acct, String processCode, )

    private class StageAddressMap {
        // Maps a Workplace to stage address records

        // Matches on: OK_Stage_Workplace__c.Id
        // Contains:   List<OK_STAGE_ADDRESS_INT__c>

        private Map<Id, List<OK_STAGE_ADDRESS_INT__c>> theMap;

        StageAddressMap() {
            theMap = new Map<Id, List<OK_STAGE_ADDRESS_INT__c>>();
        }

        public void put(Id key, OK_STAGE_ADDRESS_INT__c theObject) {
            if ( ! theMap.containsKey(key) ) {
                theMap.put(key, new List<OK_STAGE_ADDRESS_INT__c>());
            }

            theMap.get(key).add(theObject);
        }

        public List<OK_STAGE_ADDRESS_INT__c> get(Id key) {
            return theMap.get(key);
        }

        public Integer size() {
            return theMap.size();
        }

    }


    private Map<String, Address_vod__c> getDbAddresses(Set<String> oneKeyIds) {

        List<Address_vod__c> addresses =
            [
                SELECT External_ID_vod__c, OK_External_ID_BI__c, OK_ACT_Parent_ID_BI__c, OK_Process_Code_BI__c, Primary_vod__c
                FROM Address_vod__c
                WHERE OK_External_ID_BI__c IN :oneKeyIds
                AND OK_Process_Code_BI__c != 'D'
                ORDER BY LastModifiedDate ASC
            ];

        // The AND OK_Process_Code_BI__c != 'D' line is optional


        Map<String, Address_vod__c> addressMap = new Map<String, Address_vod__c>();
        for (Address_vod__c address : addresses) {
            addressMap.put(address.OK_External_ID_BI__c, address);
        }
        return addressMap;
    }

    private void processActivity() {
        Address_vod__c oldAddress;
        Address_vod__c newAddress = makeAddressFromActivityStage();

        String oneKeyId = newAddress.OK_External_ID_BI__c;


        if ( String.isBlank(oneKeyId) ) {
            System.debug('oneKeyId is blank!!!');
            return;
        } else {
            System.debug('oneKeyId: ' + oneKeyId);
        }

        if (toUpsert.containsKey(oneKeyId)) {
            // This means we've already processed a record
            // that is replacing the database record
            oldAddress = toUpsert.get(oneKeyId);
        } else {
            // Otherwise we haven't processed this record before
            oldAddress = dbAddresses.get(oneKeyId);
            // oldAddress will either be null or the old address
        }

        Boolean useNewAddress = useNewAddress(oldAddress, newAddress);

        System.debug('useNewAddress: ' + useNewAddress);

        if ( useNewAddress ) {
            toUpsert.put(oneKeyId, newAddress);

            if ( newAddress.Primary_vod__c == true ) {
                String individualId = stageActivity.Individual_External_Id__c;
                addNewPrimary(individualId, newAddress);
            }
        }

        if (toUpsert.size() > 9999) {
            upsertAddresses();
        }

    }

    private void addNewPrimary(String individualExternalId, Address_vod__c theAddress) {
        // This class ensures only one primary address (the latest) is upserted
        // If a new primary address is generated, the older primary is marked as non-primary
        // This works as we are ordering by LastModified Date and as salesforce/Apex passes by reference

        // We are ONLY concerned with primary addresses
        if (theAddress.Primary_vod__c != true) {
            return;
        }

        if ( individualIdToPrimaryAddressMap.containsKey(individualExternalId) ) {
            Address_vod__c oldPrimary = individualIdToPrimaryAddressMap.get(individualExternalId);
            oldPrimary.Primary_vod__c = false;
        }

        individualIdToPrimaryAddressMap.put(individualExternalId, theAddress);

    }


    public class addressComparisonException extends Exception {}


    private Boolean useNewAddress(Address_vod__c oldAddress, Address_vod__c newAddress) {
        // A return value of true  - means use the new address
        // A return value of false - means use the old address
        // If this is the first activity for this individual and workplace
        //   - then there isn't any old addresses, oldAddress will be null

        System.Debug('CSABA useNewAddress');

        // There should always be a newAddress
        if (newAddress == null)
            throw new addressComparisonException('Cannot pass a null value for newAddress in useNewAddress()');

        if ( (oldAddress != null) && (newAddress.OK_External_ID_BI__c != oldAddress.OK_External_ID_BI__c) )
            throw new addressComparisonException('The new and old adddresses must have the same OneKey external ID in useNewAddress()');


        // No old address - first activity, use the new
        if (oldAddress == null)
            return true;

        // If we've never populated the workplace id - use the new address so that it gets populated
        if (oldAddress.OK_ACT_Parent_ID_BI__c == null || oldAddress.OK_ACT_Parent_ID_BI__c == '') {
            System.debug('Using new address - old address missing workplace ID');
            return true;
        }

        // If the two addresses are for the same workplace,
        // then the new address is an update to it, so use the new address
        if (oldAddress.OK_ACT_Parent_ID_BI__c == newAddress.OK_ACT_Parent_ID_BI__c) {
            System.debug('Using new address - workplaces are the same, new address is an update');
            return true;
        }

        // From this point on - the workplaces are different

        // If the old address is being deleted, use the new address
        if (oldAddress.OK_Process_Code_BI__c == 'D') {
            System.debug('Using new address - old address will be deleted');
            return true;
        }


        // If the newer address is a delete (and the old one isnt') - ignore it
        // We don't want a delete for a different workplace (same address) to be deleted
        if (newAddress.OK_Process_Code_BI__c == 'D') {
            System.debug('Using old address - new address will be deleted');
            return false;
        }

        // If the new address is primary - replace the old address
        // as it now must be a non-primary
        if (newAddress.Primary_vod__c == true) {
            System.debug('Using new address - as it is primary');
            return true;
        }

        // If the old address is primary (and the new one isn't)
        // keep the old address and ignore the new address
        if (oldAddress.Primary_vod__c == true) {
            System.debug('Using old address - the old address is primary, and the new isn\'t');
            return false;
        }

        // By the time we reach here. The workplace ID must be filled in;
        // The workplaces must be different;
        // Neither the old or the new addresses are deletes
        // Neither the old or the new addresses are primary
        // I don't think we'll ever actually reach this point - as there should always be a primary.
        System.debug('Using new address - ran out of options');
        return true;
    }


    private List<OK_STAGE_ADDRESS_INT__c> getStagedAddressesByOneKeyId(Set<String> oneKeyIds) {
        List<OK_STAGE_ADDRESS_INT__c> stagedAddresses =
            [
                SELECT

                External_Id__c,

                OK_STAGE_ADDRESS__r.Name,

                OK_STAGE_ADDRESS__r.Brick_vod__c,
                OK_STAGE_ADDRESS__r.Brick_Name__c,
                OK_STAGE_ADDRESS__r.Brick_Conversion_JJ__c,

                OK_STAGE_ADDRESS__r.City_vod__c,
                OK_STAGE_ADDRESS__r.Country_vod__c,
                OK_STAGE_ADDRESS__r.Country_Code_BI__c,
                OK_STAGE_ADDRESS__r.zip_vod__c,

                OK_STAGE_ADDRESS__r.OK_State_Province_BI__c,
                OK_STAGE_ADDRESS__r.OK_State_Province_BI__r.name,
                OK_STAGE_ADDRESS__r.RecordTypeId__c,
                OK_STAGE_ADDRESS__r.External_ID_vod__c,

                OK_STAGE_ADDRESS__r.OK_Cegedim_Post_Ref_ID_BI__c,
                OK_STAGE_ADDRESS__r.OK_Address_Label_BI__c,
                OK_STAGE_ADDRESS__r.Address_line_2_vod__c,
                OK_STAGE_ADDRESS__r.OK_Street_Number_BI__c,
                OK_STAGE_ADDRESS__r.OK_City_Code_BI__c,
                OK_STAGE_ADDRESS__r.OK_OFF_SUBDIVISION_COD_3_BI__c,
                OK_STAGE_ADDRESS__r.OK_OFF_SUBDIVISION_COD_5__c,
                OK_STAGE_ADDRESS__r.Best_Times_vod__c,

                OK_STAGE_ADDRESS__r.OK_Brick_Name_BI__c,
                OK_STAGE_ADDRESS__r.Mini_Brick_Name_BI__c,
                OK_STAGE_ADDRESS__r.OK_MiniBrick_Name_BI__c,
                OK_STAGE_ADDRESS__r.Mini_Brick_BI__c,
                OK_Address_Type_Code__c,
                OK_Po_Box__c,
                OK_Building_Name_BI__c,

                OK_STAGE_ADDRESS__r.OK_Keywordstreet_BI__c,
                OK_STAGE_ADDRESS__r.OK_AREA_LBL_BI__c,

                OK_STAGE_ADDRESS__r.OK_Geo_Level_BI__c,
                OK_STAGE_ADDRESS__r.OK_Longitude_BI__c,
                OK_STAGE_ADDRESS__r.OK_Latitude_BI__c,
                OK_STAGE_ADDRESS__r.OK_Geo_Type_BI__c,
                OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_2_BI__c,
                OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_3_BI__c,
                OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_4_BI__c,
                OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_7_BI__c,
                OK_STAGE_ADDRESS__r.OK_SUBDIVISION_LABEL_2_BI__c,
                OK_STAGE_ADDRESS__r.OK_SUBDIVISION_LABEL_5_BI__c,
                OK_STAGE_ADDRESS__r.OK_SUBDIVISION_COD_5_BI__c,
                OK_STAGE_ADDRESS__r.OK_DISPATCH_LBL_BI__c,
                OK_Stage_Workplace__c,
                OK_Process_Code__c,
                OK_End_Date__c,
                Primary__c,
                Billing__c,
                Mailing__c,
                Business__c


                FROM OK_STAGE_ADDRESS_INT__c
                WHERE OK_Stage_Workplace__r.Workplace_External_ID__c IN :oneKeyIds
                ORDER BY LastModifiedDate ASC
            ];

        return stagedAddresses;
    }



    private List<OK_STAGE_ADDRESS_INT__c> getStagedAddresses(Set<Id> ids) {
        List<OK_STAGE_ADDRESS_INT__c> stagedAddresses =
            [
                SELECT

                External_Id__c,

                OK_STAGE_ADDRESS__r.Name,

                OK_STAGE_ADDRESS__r.Brick_vod__c,
                OK_STAGE_ADDRESS__r.Brick_Name__c,
                OK_STAGE_ADDRESS__r.Brick_Conversion_JJ__c,

                OK_STAGE_ADDRESS__r.City_vod__c,
                OK_STAGE_ADDRESS__r.Country_vod__c,
                OK_STAGE_ADDRESS__r.Country_Code_BI__c,
                OK_STAGE_ADDRESS__r.zip_vod__c,

                OK_STAGE_ADDRESS__r.OK_State_Province_BI__c,
                OK_STAGE_ADDRESS__r.OK_State_Province_BI__r.name,
                OK_STAGE_ADDRESS__r.RecordTypeId__c,
                OK_STAGE_ADDRESS__r.External_ID_vod__c,

                OK_STAGE_ADDRESS__r.OK_Cegedim_Post_Ref_ID_BI__c,
                OK_STAGE_ADDRESS__r.OK_Address_Label_BI__c,
                OK_STAGE_ADDRESS__r.Address_line_2_vod__c,
                OK_STAGE_ADDRESS__r.OK_Street_Number_BI__c,
                OK_STAGE_ADDRESS__r.OK_City_Code_BI__c,
                OK_STAGE_ADDRESS__r.OK_OFF_SUBDIVISION_COD_3_BI__c,
                OK_STAGE_ADDRESS__r.OK_OFF_SUBDIVISION_COD_5__c,
                OK_STAGE_ADDRESS__r.Best_Times_vod__c,

                OK_STAGE_ADDRESS__r.OK_Brick_Name_BI__c,
                OK_STAGE_ADDRESS__r.Mini_Brick_Name_BI__c,
                OK_STAGE_ADDRESS__r.OK_MiniBrick_Name_BI__c,
                OK_STAGE_ADDRESS__r.Mini_Brick_BI__c,
                OK_Address_Type_Code__c,
                OK_Po_Box__c,
                OK_Building_Name_BI__c,

                OK_STAGE_ADDRESS__r.OK_Keywordstreet_BI__c,
                OK_STAGE_ADDRESS__r.OK_AREA_LBL_BI__c,

                OK_STAGE_ADDRESS__r.OK_Geo_Level_BI__c,
                OK_STAGE_ADDRESS__r.OK_Longitude_BI__c,
                OK_STAGE_ADDRESS__r.OK_Latitude_BI__c,
                OK_STAGE_ADDRESS__r.OK_Geo_Type_BI__c,
                OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_2_BI__c,
                OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_3_BI__c,
                OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_4_BI__c,
                OK_STAGE_ADDRESS__r.OK_BRICK_NUMBER_7_BI__c,
                OK_STAGE_ADDRESS__r.OK_SUBDIVISION_LABEL_2_BI__c,
                OK_STAGE_ADDRESS__r.OK_SUBDIVISION_LABEL_5_BI__c,
                OK_STAGE_ADDRESS__r.OK_SUBDIVISION_COD_5_BI__c,
                OK_STAGE_ADDRESS__r.OK_DISPATCH_LBL_BI__c,
                OK_Stage_Workplace__c,
                OK_Process_Code__c,
                OK_End_Date__c,
                Primary__c,
                Billing__c,
                Mailing__c,
                Business__c,
                LastModifiedDate

                FROM OK_STAGE_ADDRESS_INT__c
                WHERE OK_Stage_Workplace__c IN :ids
                ORDER BY LastModifiedDate ASC
            ];

        return stagedAddresses;
    }




    global void finish(Database.BatchableContext BC) {
        if (jobId == null) return;

        String message =
            'Total upserted: ' + total
            + ', Successes: ' + successes
            + ', Failures: ' + failures
            ;

        System.debug(message);
        VEEVA_BATCH_ONEKEY_BATCHUTILS.setErrorMessage(jobId, message);
        VEEVA_BATCH_ONEKEY_BATCHUTILS.setCompleted(jobId, lastRunTime);
    }

}