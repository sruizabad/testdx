/*


TO RUN: 
VEEVA_BATCH_REPAIR_CAs b = new VEEVA_BATCH_REPAIR_CAs (); database.executebatch(b,100);

Created by: Viktor 2013-05-17

*/
global class VEEVA_BATCH_REPAIR_CAs implements Database.Batchable<sObject>{
    
    global VEEVA_BATCH_REPAIR_CAs () {
        system.debug('VEEVA_BATCH_REPAIR_CAs STARTED');
    }
    
      
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // This is the base query that dirves the chunking.
        
        String query = 'SELECT Id, Child_Account_vod__r.OK_External_ID_BI__c, Parent_Account_vod__r.OK_External_ID_BI__c FROM Child_Account_vod__c where external_id2__c = \'\' ' ;  

        system.debug('query: ' +query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> batch) {
        system.debug('VEEVA_BATCH_REPAIR_CAs execute process starting');
        system.debug('batch size: ' + batch.size());
   
        List <Child_Account_vod__c> CAs = (List <Child_Account_vod__c>) batch;
        
        if(CAs.size()==0){
            return;
        } 
        
        String extid = '';
        
       for (Child_Account_vod__c CA : CAs){
       		extid = CA.Child_Account_vod__r.OK_External_ID_BI__c + CA.Parent_Account_vod__r.OK_External_ID_BI__c;
            CA.external_id2__c = extid;
       }
            
            
            
       update CAs;
        
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('VEEVA_BATCH_REPAIR_CAs FINISHED');
        // That's all folks!
    }

}