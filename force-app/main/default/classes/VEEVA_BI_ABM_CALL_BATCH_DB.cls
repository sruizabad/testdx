/*********************************************************************************************
2016.11.14.  Call Correction  class  update  based on Blaski request
Code  is caled after  the  CALL BACTH  and do the usual correction  only  for those ABS  which
where recently  updated
Class is intended to fix  errors  generated  by the Merge  process
This batch  will be called  with 1 Batch size
*********************************************************************************************/
global without sharing class VEEVA_BI_ABM_CALL_BATCH_DB implements Database.batchable<sObject> 
{   
	Boolean TestOnly = false;
    ID ABM_ID = NULL; 
 
    public VEEVA_BI_ABM_CALL_BATCH_DB()        
    {
	TestOnly = false;
    ABM_ID = NULL; 
    }

    public VEEVA_BI_ABM_CALL_BATCH_DB(Boolean istestu)        
    {
	TestOnly = istestu;
    ABM_ID = NULL; 
    }    
    
    public VEEVA_BI_ABM_CALL_BATCH_DB(ID AMBIDu,Boolean istestu)        
    {
     ABM_ID = AMBIDu;
     TestOnly = istestu;
    }
    
    public Integer GetMonthByName(String m)
    {
    	Integer retval = 0;
    	
    	if(m == 'January')
    	   retval = 1;
    	else if(m == 'February')    
    	     retval = 2;
     	else if(m == 'March')    
    	     retval = 3;
    	else if(m == 'April')    
    	     retval = 4;
    	else if(m == 'May')    
    	     retval = 5;    	         	     
    	else if(m == 'June')    
    	     retval = 6;    	
    	else if(m == 'July')    
    	     retval = 7;
    	else if(m == 'August')    
    	     retval = 8;    	     
    	else if(m == 'September')    
    	     retval = 9;    	     
    	else if(m == 'October')    
    	     retval = 10;
    	else if(m == 'November')    
    	     retval = 11;
    	else if(m == 'December')    
    	     retval = 12;    	         	     
    	         	
    	return retval;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)   
    {
        
        String SelStmt = 'SELECT ID,Month_BI__c, Year_of_Activity_BI__c, User_BI__c, User_Country_BI__c, ';
        SelStmt = selStmt + ' CallSubmitdaysList__c, Activity_Count_BI__c,Activity_count_Daily_BI__c,Repair_Helper_BI__c ';
        SelStmt = selStmt + ' ,Helper_CallDayList_BI__c,Helper_CallTotalCount_BI__c,Helper_CallDailyCount_BI__c ' ;
        SelStmt = selStmt + ' FROM Activity_Benchmark_BI__c ';
        
        if(ABM_ID == NULL)     
           SelStmt = SelStmt + 'where lastmodifieddate = today and Createddate != today';  
        else
           SelStmt = SelStmt + 'where id = \'' + ABM_ID + '\'';  
           
        //selStmt = selStmt + ' and User_BI__c= \'005900000037qZ0\' '; 
        selStmt = SelStmt + ' order by User_BI__c '; 
        SelStmt = SelStmt + ' Limit 5000000 ';
        
        System.Debug('SQLU = ' + selStmt);
        
        return Database.getQueryLocator(selStmt);          
        
    }
    
    
    global void execute(Database.BatchableContext BC, List<sObject> batch)  
    {
        
        List<Activity_Benchmark_BI__c> ABMs = (List<Activity_Benchmark_BI__c>) batch;
        
        Set<ID> OwnerIDs = new Set<ID>();
        String SYear;
        String SMonth;
        for(Activity_Benchmark_BI__c ABM :ABMs)
        {
            OwnerIDs.add(ABM.User_BI__c);
            SYear = ABM.Year_of_Activity_BI__c;
            SMonth = ABM.Month_BI__c;
            
            
        }
        System.Debug('CSABA Owners = ' + OwnerIds.size() + ' TestOnly = ' + TestOnly);
    
         Date thisday = system.date.today();
         thisday = thisday.addDays(-1);
         
         Integer Yeru = thisday.year();
         Integer thisMonth = thisday.month();
         
         Date Dstart = date.newInstance(Yeru,thisMonth,01);
         Date Dend = thisday; 
         
         Dstart = date.newInstance(Integer.valueof(SYear),GetMonthByName(SMonth),01); 
         Dend = Dstart.addMonths(1);
 

         
         system.Debug('CSABA Dstart = ' + Dstart + ' Dend = ' + Dend);
        
         AggregateResult[] groupedCalls  = [SELECT Call_Date_vod__c, ownerid, count(ID) nrofcalls  
                                           FROM Call2_vod__c
                                           where ownerid in :OwnerIDs
                                           and Status_vod__c = 'Submitted_vod' and Parent_Call_vod__c = null 
                                           and Call_Date_vod__c >= :Dstart 
                                           and Call_Date_vod__c < :Dend
                                           GROUP BY ownerid, Call_Date_vod__c
                                           order by ownerid, Call_Date_vod__c
                                           ];


system.Debug('CSABA groupedCalls = ' + groupedCalls.size());

Map<ID,String> mapTDaylist = new Map<ID,String>(); //2016.11.14
Map<ID,Integer> mapu = new Map<ID,Integer>(); 
Map<ID,Integer> mapuTotu = new Map<ID,Integer>();   //2014.11.09.  used  for  fixing Activity_Count_BI__c

string TakenDays = '';
Integer TotalCalls = 0;
Integer counteru = 0; 
ID  currentOwner = NULL; 
ID  prevOwner = NULL;                                     
for (AggregateResult ar : groupedCalls)  
    {
     currentOwner = (ID)ar.get('ownerid'); 
         	
     Date CallDatele = (Date)ar.get('Call_Date_vod__c');   
     string theday = string.valueOf(CallDatele.day());
     if(theday.length() == 1)
        theday = '0' + theday;
   
     string TDList =  mapTDaylist.get(currentOwner);  
     if(TDList != NULL)
     {
     	if(TDList.contains(theday) ==  false)
     	{
          TDList = TDList + ',' + theday;
          mapTDaylist.put(currentOwner,TDList);  //not sure is needed since might be reference
         }
     }   
     else
     {
     	 mapTDaylist.put(currentOwner,theday);
     }
     
     System.Debug('CSABA:  CurrentOwneru ' + currentOwner + ' prevOwner ' + prevOwner + ' counteru = ' +  counteru + ' Mapsize = ' + mapu.size());
     if(prevOwner == NULL || currentOwner == prevOwner)
        {
            counteru = counteru + 1;
            TotalCalls = TotalCalls + (Integer)ar.get('nrofcalls');  //2014.11.09.
            
        } 
     else
        {
             counteru = 1;
             TotalCalls = (Integer)ar.get('nrofcalls');              //2014.11.09.
        }   
        
     prevOwner = currentOwner;
     mapu.put(prevOwner,counteru);
     
     mapuTotu.put(prevOwner,TotalCalls);                             //2014.11.09.
    }
    
    
    
//now we have to call counter /  User lets  update ABM
List<Activity_Benchmark_BI__c> ABM_2Update = new List<Activity_Benchmark_BI__c>();
for (Activity_Benchmark_BI__c ABM :ABMs)
    {
    decimal currentCounter = ABM.Activity_count_Daily_BI__c;
    decimal calculatedCount = mapu.get(ABM.User_BI__c); 
    system.Debug('CSABA1 currentCounter = ' + currentCounter + ' calculatedCount = ' + calculatedCount);

    decimal currentTotCounter = ABM.Activity_Count_BI__c;    
    decimal calculatedTotcount = maputotu.get(ABM.User_BI__c); 
    system.Debug('CSABA1 currentTotCounter = ' + currentTotCounter + ' calculatedTotcount = ' + calculatedTotcount);
    
    String CurentTDList = ABM.CallSubmitdaysList__c;
    String CalculatedTDLidt = mapTDaylist.get(ABM.User_BI__c);
    system.Debug('CSABA1 CurentTDList = ' + CurentTDList + ' CalculatedTDLidt = ' + CalculatedTDLidt);
    
    Boolean needU = false; 

    if(currentCounter != calculatedCount || TestOnly == true)
       {
       System.Debug('CSABA NEED Chnage for currentCounter');		
       System.Debug('useru = ' + ABM.User_BI__c + 'ABM Id = ' + ABM.ID + '  CC = ' +  currentCounter + ' RC = ' + calculatedCount);   
       
       if(TestOnly)
       {
          ABM.Helper_CallDailyCount_BI__c = calculatedCount;
       }
       else   
          ABM.Activity_count_Daily_BI__c = calculatedCount;
          
       needU = true;
       }
       
    if(currentTotCounter != calculatedTotcount || TestOnly == true)
      {
       System.Debug('CSABA NEED Change for currentTotCounter');	
       if(TestOnly)	
         {
         	ABM.Helper_CallTotalCount_BI__c = calculatedTotcount;
         }
       else  
         	ABM.Activity_Count_BI__c = calculatedTotcount;          
       
       needU = true;      	
      }
    
    if(CurentTDList != CalculatedTDLidt || TestOnly == true)
      {
       System.Debug('CSABANEED Change for CurentTDList');		
       if(TestOnly)	
         {
          ABM.Helper_CallDayList_BI__c = CalculatedTDLidt;
         }
       else  
    	  ABM.CallSubmitdaysList__c = CalculatedTDLidt;
    	
    	needU = true;
      }
    
    if(needU == true) 
       {
       	System.Debug('CSABA  we need update');
        ABM_2Update.add(ABM);
        needU = false;
       }  
    
    } 

if(ABM_2Update.size() > 0)
{
   System.Debug('CSABa ABM_2Update.size() = ' + ABM_2Update.size());	
   update ABM_2Update; 
}
//add some erro handling here  and  store it into  the V2OK_Batch Job class if possible   
        
}   
    
    
global void finish(Database.BatchableContext BC)
    {
        
    }   
}