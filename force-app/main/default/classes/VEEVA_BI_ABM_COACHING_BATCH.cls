/**
 * VEEVA_BI_ABM_COACHING_BATCH
 *
 * Author: Raphael Krausz <raphael.krausz@veeva.com>
 * Date:   2015-10-03
 * Description:
 *      Examines Coaching reports, creates activity benchmarks for each user (manager on each coaching report)
 *      It counts:
 *          - activity  - the total amount of activity measured in days
 *          - day count - the total number of days activities occur on
 *          - list of active days - a semi-colon separated string of days of the month there are activities on
 *
 *      Currently designed to be run daily, adding in the previous days activity, creating and updating
 *      activity benchmarks as needed. Activity benchmark records may be identified upon user, month and year.
 *
 *
 *  Modified: 2015-10-06
 *  By: Raphael Krausz
 *  Description:
 *      Added in more options for calculating duration length and handling blank/none durations.
 *
 */


global without sharing class VEEVA_BI_ABM_COACHING_BATCH implements Database.batchable<sObject> {

    // N.B. The index for this list is 0..11, date objects index their months 1..12
    private final static List<String> MONTHS = new List<String> {
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    };

    private Map<String, Activity_Benchmark_BI__c> activityBenchmarkUpdateMap;
    private Map<String, Activity_Benchmark_BI__c> activityBenchmarkInsertMap;
    private Map<String, Activity_Benchmark_BI__c> activityBenchmarkDatabaseMap;

    private VEEVA_HLP_WorkingDay workingDayHelper = new VEEVA_HLP_WorkingDay();

    global List<Coaching_Report_vod__c> start(Database.BatchableContext BC) {

        List<Coaching_Report_vod__c> coachingReportList =
            [
                SELECT
                ID,
                CreatedByID,
                Coaching_Duration_BI__c,
                Review_Date__c,
                Manager_vod__c ,
                OwnerId
                FROM Coaching_Report_vod__c
                WHERE LastModifiedDate = YESTERDAY
                //WHERE LastModifiedDate >= 2015-12-01T00:00:00.000Z
        		//AND LastModifiedDate < 2016-01-03T00:00:00.000Z
                AND Status__c = 'Approved'
                AND Manager_vod__c != null
                AND Review_Date__c != null
                                LIMIT 5000000
            ];
//                 WHERE LastModifiedDate <= YESTERDAY

        /*

        List<Coaching_Report_vod__c> coachingReportList =
            [
                SELECT
                ID,
                CreatedByID,
                Coaching_Duration_BI__c,
                Review_Date__c,
                Manager_vod__c ,
                OwnerId
                FROM Coaching_Report_vod__c
                WHERE Status__c = 'Approved'
                                  AND LastModifiedDate < 2015-10-08T00:00:00.000Z
            ];

        */

        return coachingReportList;
    }


    global void execute(Database.BatchableContext BC, List<Coaching_Report_vod__c> coachingReportList) {

        Integer batchSize = coachingReportList.size();
        system.Debug('coachingReport Batch size = ' + batchSize);


        activityBenchmarkUpdateMap = new Map<String, Activity_Benchmark_BI__c>();
        activityBenchmarkInsertMap = new Map<String, Activity_Benchmark_BI__c>();

        Set<Id>     userIdSet  = new Set<Id>();
        Set<String> activeMonths = new Set<String>();
        Set<String> activeYears  = new Set<String>();

        Set<String> countryCodeSet = new Set<String>();


        for (Coaching_Report_vod__c coachingReport : coachingReportList) {

            if ( String.isBlank(coachingReport.Manager_vod__c) ) {
                continue;
            }

            userIdSet.add(coachingReport.Manager_vod__c);

            Date reviewDate = coachingReport.Review_Date__c;

            // N.B. Date.month() returns 1 - 12, but MONTHS List is 0 - 11
            Integer month = reviewDate.month();
            String monthName = MONTHS.get(month - 1);
            activeMonths.add(monthName);


            Integer year = reviewDate.year();

            String yearString = String.valueOf(year);
            activeYears.add(yearString);

            if (month == 12) {
                activeMonths.add(MONTHS.get(1));
                yearString = String.valueOf(year + 1);
                activeYears.add(yearString);
            }

        }


        Map<Id, User> userMap = new Map<Id, User>(
            [
                SELECT Id, Name, Country_Code_BI__c
                FROM User
                WHERE Id = :userIdSet
            ]
        );

        for (User userObject : userMap.values()) {
            countryCodeSet.add(userObject.Country_Code_BI__c);
        }


        populateWorkingDayMap(countryCodeSet, activeYears);

        getExistingActivityBenchmarksFromDatabase(userIdSet, activeYears, activeMonths);


        for (Coaching_Report_vod__c coachingReport : coachingReportList) {

            Date reviewDate = coachingReport.Review_Date__c;


            System.debug('reviewDate: ' + reviewDate);


            String callDateDay = String.valueOf(reviewDate.day());


            //calculate duration
            Decimal decimalDuration = getDuration(coachingReport);
            if (decimalDuration == null) {
                System.debug('Unable to determine duration! Skipping!');
                continue;
            }
            Integer year = reviewDate.year();
            Integer month = reviewDate.month();
            Integer quarter = getQuarter(month);

            String currentMonthYearString = String.valueOf(year);
            String nextMonthYearString;

            System.debug('year: ' + year);
            System.debug('currentMonthYearString: ' + currentMonthYearString);


            //get user info
            Id  userID = coachingReport.Manager_vod__c;
            User theUser = userMap.get(userId);

            //get country code
            String countryCode = theUser.Country_Code_BI__c;

            if ( String.isBlank(countryCode) ) {
                System.Debug('User missing country code - skipping!');
                continue;
            }


            String workingDayKey = countryCode + '_' + currentMonthYearString;

            if ( ! workingDayMap.containsKey(workingDayKey) ) {
                System.debug('Missing working day - ' + workingDayKey + ' - skipping!');
                continue;
            }

            String currentMonth = MONTHS[month - 1];

            System.Debug('Raphi 1.0 CR reviewDate ' + reviewDate + ', duration: ' +  decimalDuration + ', OwnerID: ' + UserID);

            Decimal currentMonthDuration = decimalDuration;
            Decimal nextMonthDuration = 0;

            String nextMonth;   //use it only in case is Overflow

            Integer nextQuarter; //use it only in case is Overflow

            Boolean durationGoesOverMonthsEnd = false;

            if (decimalDuration > 1.0) {


                Decimal overflowDays = getNumberOfOverflowDays(coachingReport);

                if (overflowDays > 0.0) {
                    durationGoesOverMonthsEnd = true;

                    currentMonthDuration = decimalDuration - overflowDays;
                    nextMonthDuration = overflowDays;

                    System.debug(
                        'overflowDays: ' + overflowDays
                        + ', decimalDuration: ' + decimalDuration
                        + ', currentMonthDuration: ' + currentMonthDuration
                        + ', nextMonthDuration: ' + nextMonthDuration
                    );

                    if (month == 12) {
                        nextMonth = MONTHS.get(1);
                        nextQuarter = getQuarter(1);
                        nextMonthYearString = String.valueOf(year + 1);
                    } else {
                        nextMonth = MONTHS.get(month);
                        nextMonthYearString  = String.valueOf(year);
                        nextQuarter = getQuarter(month + 1);
                    }


                    workingDayKey = countryCode + '_' + nextMonthYearString;

                    if ( ! workingDayMap.containsKey(workingDayKey) ) {
                        System.debug('Missing working day (for next month) - ' + workingDayKey + ' - skipping!');
                        continue;
                    }

                }

            }

            System.debug(
                'XXX - currentMonthDuration: ' + currentMonthDuration
                + ', nextMonthDuration: ' + nextMonthDuration
                + ', nextMonth: ' + nextMonth
                + ', nextMonthYearString: ' + nextMonthYearString
            );


            Integer workingDaysInPeriod = getWorkingDaysInPeriod(countryCode, currentMonth, currentMonthYearString);

            Integer reviewStartDay  = reviewDate.day();

            Integer numberOfDaysDuration = (Integer) currentMonthDuration;
            if ( currentMonthDuration > (Decimal) numberOfDaysDuration ) {
                numberOfDaysDuration += 1;
            }

            String daysListString = getDaysListString(reviewStartDay, numberOfDaysDuration);
            Integer daysCount = getDailyCountFromDaysListString(daysListString);


            Activity_Benchmark_BI__c currentActivityBenchmark = new Activity_Benchmark_BI__c(
                User_BI__c = userId,
                OwnerId = userId,
                Month_BI__c = currentMonth,
                Quarter_BI__c =  'Quarter ' + String.valueOf(quarter),
                Year_of_Activity_BI__c = currentMonthYearString,
                Working_Days_in_Period_BI__c = workingDaysInPeriod,
                Activity_Count_BI__c = currentMonthDuration,
                Activity_count_Daily_BI__c = daysCount,
                CallSubmitdaysList__c = daysListString
            );

            addActivityBenchmark(currentActivityBenchmark);

            if ( durationGoesOverMonthsEnd ) {

                workingDaysInPeriod = getWorkingDaysInPeriod(countryCode, nextMonth, nextMonthYearString);

                reviewStartDay  = 1;

                numberOfDaysDuration = (Integer) nextMonthDuration;
                if ( nextMonthDuration > (Decimal) numberOfDaysDuration ) {
                    numberOfDaysDuration += 1;
                }


                daysListString = getDaysListString(reviewStartDay, numberOfDaysDuration);
                daysCount = getDailyCountFromDaysListString(daysListString);


                Activity_Benchmark_BI__c nextActivityBenchmark = new Activity_Benchmark_BI__c(
                    User_BI__c = userId,
                    Month_BI__c = nextMonth,
                    Quarter_BI__c =  'Quarter ' + String.valueOf(nextQuarter),
                    Year_of_Activity_BI__c = nextMonthYearString,
                    Working_Days_in_Period_BI__c = workingDaysInPeriod,
                    Activity_Count_BI__c = nextMonthDuration,
                    Activity_count_Daily_BI__c = daysCount,
                    CallSubmitdaysList__c = daysListString
                );

                addActivityBenchmark(nextActivityBenchmark);
            }

        }

        System.debug('Activity benchmarks to insert: ' + activityBenchmarkInsertMap.size());
        System.debug('Activity benchmarks to update: ' + activityBenchmarkUpdateMap.size());

        if ( ! activityBenchmarkInsertMap.isEmpty() ) {
            insert activityBenchmarkInsertMap.values();
        }

        if ( ! activityBenchmarkUpdateMap.isEmpty() ) {
            update activityBenchmarkUpdateMap.values();
        }

    }


    private Map<String, Working_Day_BI__c> workingDayMap = new Map<String, Working_Day_BI__c>();
    private void populateWorkingDayMap(Set<String> countryCodeSet, Set<String> yearStringSet) {

        Set<String> nameSet = new Set<String>();

        for (String year : yearStringSet) {
            for (String countryCode : countryCodeSet) {
                String name = countryCode + '_' + year;
                nameSet.add(name);
            }
        }

        List<Working_Day_BI__c> workingDayList =
            [
                SELECT  Id, Name
                FROM    Working_Day_BI__c
                WHERE   Name IN :nameSet
                ORDER BY Name ASC
            ];

        for (Working_Day_BI__c workingDay : workingDayList) {
            workingDayMap.put(workingDay.Name, workingDay);
        }

    }

    private String makeActivityBenchmarkCustomKey(Activity_Benchmark_BI__c activityBenchmark) {

        Id userId    = activityBenchmark.User_BI__c;
        String month = activityBenchmark.Month_BI__c;
        String year  = activityBenchmark.Year_of_Activity_BI__c;

        return makeActivityBenchmarkCustomKey(userId, month, year);
    }

    private String makeActivityBenchmarkCustomKey(Id userId, String month, String year) {
        String customId = '' + userId + '::' + month + '::' + year;
        return customId;
    }

    private Decimal getDuration(Coaching_Report_vod__c coachingReport) {
        String duration = coachingReport.Coaching_Duration_BI__c;

        if ( String.isBlank(duration) || duration.containsIgnoreCase('None') ) {
            System.debug(
                'Coaching report has blank or "None" duration! Coaching Report Id:'
                + coachingReport.Id
            );
            return null;
        }


        System.debug('duration (String): ' + duration);

        //calculate duration
        Decimal decimalDuration;

        if ( duration.contains('AM') || duration.contains('PM') ) {
            decimalDuration = 0.5;
            return decimalDuration;
        }

        if ( duration == '1 day' || duration.contains('All') ) {
            decimalDuration = 1.0;
            return decimalDuration;
        }

        if ( duration == '2 days' || duration.contains('Two') ) {
            decimalDuration = 2.0;
            return decimalDuration;
        }

        Pattern durationPattern = Pattern.compile('\\s*(\\d+(\\.\\d+){0,1}).*');

        Matcher durationMatcher = durationPattern.matcher(duration);

        if ( durationMatcher.matches() ) {
            String StringDuration = durationMatcher.group(1);
            System.debug('StringDuration: ' + StringDuration);
            decimalDuration = Decimal.valueOf(StringDuration.trim());
        } else {
            System.debug('DID NOT MATCH REGULAR EXPRESSION!!!!');
        }

        return decimalDuration;

    }


    // Returns the number of days into the next month a coaching report may go for.
    private Decimal getNumberOfOverflowDays(Coaching_Report_vod__c coachingReport) {

        // Number of days over the review date.
        Decimal extraDays;

        {
            Decimal duration = getDuration(coachingReport);

            if (duration <= 1.0) {
                return 0;
            }

            // extra days over the review date.
            extraDays = duration - 1;
        }

        System.debug('extraDays: ' + extraDays);


        Date reviewDate = coachingReport.Review_Date__c;

        System.debug('reviewDate: ' + reviewDate);

        Integer month = reviewDate.month();
        Integer year = reviewDate.year();
        Integer day  = reviewDate.day();

        Decimal numberOfDaysInMonth = (Decimal) Date.daysInMonth(year, month);

        System.debug('numberOfDaysInMonth: ' + numberOfDaysInMonth);

        Decimal endDay = extraDays + (Decimal) day;

        System.debug('endDay: ' + endDay);


        // If the number of Days in the month is greater than the calculated end day
        // then return 0
        if (numberOfDaysInMonth > endDay) {
            System.debug('endDay less than the numberOfDaysInMonth');
            return 0;
        }

        // Otherwise return the difference
        Decimal overflowDays = endDay - numberOfDaysInMonth;

        System.debug('overflowDays: ' + overflowDays);

        return overflowDays;
    }

    public Integer getQuarter(Integer month) {
        Integer quarter = month / 3;

        if ( Math.mod(month, 3) > 0 ) {
            quarter += 1;
        }

        return quarter;
    }

    private Integer getWorkingDaysInPeriod(String countryCode, String month, String year) {
        Integer workingDays = workingDayHelper.GetWorkingDay(countryCode, year, month);
        return workingDays;
    }

    private void addActivityBenchmark(Activity_Benchmark_BI__c activityBenchmarkToAdd) {
        /*
        An existing record matching the activity benchmark may already exist in memory
        or in the database.
        We have already preloaded the database records into memory.

        The three places records may live are:
            activityBenchmarkInsertMap
            activityBenchmarkUpdateMap
            activityBenchmarkDatabaseMap

        This method searches for a matching record in the three locations and combines the existing and matching
        records, and then places the new combined record into the appropriate map.
        */


        System.debug('In addActivityBenchmark');

        String customKey = makeActivityBenchmarkCustomKey(activityBenchmarkToAdd);


        System.debug('customKey: ' + customKey);

        if ( activityBenchmarkUpdateMap.containsKey(customKey) ) {
            // Merge the two records
            // Update activityBenchmarkUpdateMap

            System.debug('activityBenchmarkUpdateMap.containsKey(customKey): TRUE');

            Activity_Benchmark_BI__c existing = activityBenchmarkUpdateMap.get(customKey);
            Activity_Benchmark_BI__c mergedActivityBenchmark = mergeActivityBenchmarks(existing, activityBenchmarkToAdd);

            activityBenchmarkUpdateMap.put(customKey, mergedActivityBenchmark);

        } else if ( activityBenchmarkDatabaseMap.containsKey(customKey) ) {
            // Merge the two records
            // Add it to the activityBenchmarkUpdateMap

            System.debug('activityBenchmarkDatabaseMap.containsKey(customKey): TRUE');

            Activity_Benchmark_BI__c existing = activityBenchmarkDatabaseMap.get(customKey);
            Activity_Benchmark_BI__c mergedActivityBenchmark = mergeActivityBenchmarks(existing, activityBenchmarkToAdd);

            activityBenchmarkUpdateMap.put(customKey, mergedActivityBenchmark);


        } else if ( activityBenchmarkInsertMap.containsKey(customKey) ) {
            // Both records are new - and to be inserted
            // Merge the two records
            // Update activityBenchmarkInsertMap

            System.debug('activityBenchmarkInsertMap.containsKey(customKey): TRUE');

            Activity_Benchmark_BI__c existing = activityBenchmarkInsertMap.get(customKey);
            Activity_Benchmark_BI__c mergedActivityBenchmark = mergeActivityBenchmarks(existing, activityBenchmarkToAdd);

            activityBenchmarkInsertMap.put(customKey, mergedActivityBenchmark);


        } else {
            // Totally new record

            System.debug('ActivityBenchmark not already in for insert, update nor in database');

            activityBenchmarkInsertMap.put(customKey, activityBenchmarkToAdd);
        }

        System.debug('addActivityBenchmark DONE');

    }

    private Activity_Benchmark_BI__c mergeActivityBenchmarks(Activity_Benchmark_BI__c existing, Activity_Benchmark_BI__c toAdd) {

        Id existingId = existing.Id;
        Id userId = existing.User_BI__c;
        String month = existing.Month_BI__c;
        String quarter = existing.Quarter_BI__c;
        String year = existing.Year_of_Activity_BI__c;
        Decimal workingDaysInPeriod = existing.Working_Days_in_Period_BI__c;

        Decimal currentActivityCount = existing.Activity_Count_BI__c;
        Decimal activityCountToAdd   = toAdd.Activity_Count_BI__c;

        if (currentActivityCount == null) {
            currentActivityCount = 0;
        }

        if (activityCountToAdd == null) {
            activityCountToAdd = 0;
        }

        Decimal activityCount = currentActivityCount + activityCountToAdd;

        String existingDaysListSetString = existing.CallSubmitdaysList__c;

        Set<String> daysListSet;

        if ( String.isBlank(existingDaysListSetString) ) {
            daysListSet = new Set<String>();
        } else {
            daysListSet = new Set<String>(existingDaysListSetString.split('\\s*;\\s*'));
        }

        String toAddDaysListSetString = toAdd.CallSubmitdaysList__c;
        if ( ! String.isBlank(toAddDaysListSetString) ) {
            daysListSet.addAll(toAddDaysListSetString.split('\\s*;\\s*'));
        }

        daysListSet.remove('');
        daysListSet.remove(null);

        String daysListString = getDaysListString(daysListSet);
        Integer activityCountDaily = getDailyCountFromDaysListString(daysListString);



        Activity_Benchmark_BI__c mergedActivityBenchmark = new Activity_Benchmark_BI__c(
            User_BI__c = userId,
            OwnerId = userId,
            Month_BI__c = month,
            Quarter_BI__c =  quarter,
            Year_of_Activity_BI__c = year,
            Working_Days_in_Period_BI__c = workingDaysInPeriod,
            Activity_Count_BI__c = activityCount,
            Activity_count_Daily_BI__c = activityCountDaily,
            CallSubmitdaysList__c = daysListString
        );

        if (existingId != null) {
            mergedActivityBenchmark.Id = existingId;
        }

        return mergedActivityBenchmark;

    }


    private void getExistingActivityBenchmarksFromDatabase(Set<Id> userIdSet, Set<String> activeYears, Set<String> activeMonths) {
        activityBenchmarkDatabaseMap =
            new Map<String, Activity_Benchmark_BI__c>();

        List<Activity_Benchmark_BI__c> activityBenchmarkList =
            [
                SELECT
                Id,
                Activity_Count_BI__c,
                Coaching_Activity_Count_BI__c,
                Details_in_Period_BI__c,
                Name,
                Comment__c,
                Quarter_BI__c,
                LastModifiedDate,
                Activity_count_Daily_BI__c,
                CallSubmitdaysList__c,
                User_BI__c,
                Month_BI__c,
                Year_of_Activity_BI__c,
                Working_Days_in_Period_BI__c
                FROM Activity_Benchmark_BI__c
                WHERE User_BI__c IN :userIdSet
                AND Year_of_Activity_BI__c IN :activeYears
                AND Month_BI__c IN :activeMonths
            ];

        for (Activity_Benchmark_BI__c activityBenchmark : activityBenchmarkList) {
            Id userId    = activityBenchmark.User_BI__c;
            String month = activityBenchmark.Month_BI__c;
            String year  = activityBenchmark.Year_of_Activity_BI__c;

            String customKey = makeActivityBenchmarkCustomKey(userId, month, year);

            activityBenchmarkDatabaseMap.put(customKey, activityBenchmark);
        }

    }

    private String getDaysListString(Integer startDay, Integer duration) {

        System.debug('In getDaysListString - startDay: ' + startDay + ', duration: ' + duration);

        Set<String> daySet = new Set<String>();
        for (Integer i = 0; i < duration; i++) {
            String s = convertIntegerToString(i + startDay);
            daySet.add(s);
            System.debug('Added day: ' + s);
        }

        System.debug('daySet: ' + daySet);
        return getDaysListString(daySet);
    }


    private String convertIntegerToString(Integer i) {
        String s = String.valueOf(i);

        if ( s.length() == 1 ) {
            s = '0' + s;
        }

        return s;
    }

    private String getDaysListString(Set<String> dayStringSet) {
        List<String> dayStringList = new List<String>(dayStringSet);
        dayStringList.sort();

        System.debug('dayStringSet: ' + dayStringSet);
        System.debug('dayStringList: ' + dayStringList);

        Boolean started = false;
        String daysListString = '';

        for (String s : dayStringList) {
            if ( started ) {
                daysListString += '; ';
            } else {
                // not yet started
                started = true;
            }

            daysListString += s;
        }

        System.debug('daysListString: ' + daysListString);
        return daysListString;
    }

    private Integer getDailyCountFromDaysListString(String daysListString) {
        List<String> stringList = daysListString.split('\\s*;\\s*');
        Integer length = stringList.size();
        return length;
    }

    global void finish(Database.BatchableContext BC) {
        VEEVA_BI_ABM_CALL_BATCH b = new VEEVA_BI_ABM_CALL_BATCH();
        database.executebatch(b, 10);
    }

}