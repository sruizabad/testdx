/*


TO RUN: 
VEEVA_BI_BATCH_Address_Preferred_flag b = new VEEVA_BI_BATCH_Address_Preferred_flag ('CA'); database.executebatch(b,200);

Created by: Viktor 2013-05-17

*/
global class VEEVA_BI_BATCH_Address_Preferred_flag implements Database.Batchable<sObject>{
    String country = null;
    private Id jobId;
    Datetime lastRunTime;
    
    global VEEVA_BI_BATCH_Address_Preferred_flag () {
        system.debug('BI_BATCH_Address_Preferred_flag STARTED');
        this.lastRunTime = DateTime.newInstance(1970, 01, 01);
    }
    
    global VEEVA_BI_BATCH_Address_Preferred_flag (String country) {
        system.debug('BI_BATCH_Address_Preferred_flag STARTED with country');
        this.country = country;
        this.lastRunTime = DateTime.newInstance(1970, 01, 01);
    }
    
    global VEEVA_BI_BATCH_Address_Preferred_flag (Id JobId, Datetime lastRunTime, String country) {
        this();
        system.debug('BI_BATCH_Address_Preferred_flag STARTED with country and stuff');
        this.jobId = JobId;
        this.lastRunTime = lastRunTime;
        //this.lastRunTime = DateTime.newInstance(1970, 01, 01);
        this.country = country;
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // This is the base query that dirves the chunking.
        String query = '';
        
        query = ' SELECT id, BI_Preferred_Address_BI__c, Primary_vod__c ' + 
                        ' FROM Address_vod__c WHERE'+
                        '((Primary_vod__c = true AND BI_Preferred_Address_BI__c = false) OR (Primary_vod__c = false AND BI_Preferred_Address_BI__c = true))' + 
                        ' AND Country_code_bi__c!=\'BE\' AND Country_code_bi__c!=\'LU\' ' +
                        //' and BI_Preferred_Address_BI__c != true ' +
                        //' and lastmodifiedbyId in ( \'005d0000001t6MCAAY\' , \'005J0000000yTyBIAU\', \'005K0000001dSGyIAM\' ) ' + 
                        ' and (LastModifiedDate >=' + lastRunTime.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.\'000Z\' )')
               ;  
        // Country_Code_BI__c = \'CA\'
        
        if(country!=null) 
        	query += 'and Country_Code_BI__c = \'' + country + '\' limit 50000000';
        
        system.debug('query: ' +query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> batch) {
        system.debug('batch process starting');
        system.debug('batch size: ' + batch.size());
   		
        List <Address_vod__c> addrs = (List <Address_vod__c>) batch;
        map<Id, Address_vod__c> addrstoupd = new map<Id, Address_vod__c>();
        if(addrs.size()==0){
            return;
        }
               
        //check all the addresses
        for (Address_vod__c addr : addrs){
        	//if primary, set preferred true
        	if(addr.Primary_vod__c==true&&addr.BI_Preferred_Address_BI__c!=true){
        		addr.BI_Preferred_Address_BI__c = true;
        		addrstoupd.put(addr.Id, addr);
        	}
       		//if not primary, remove preferred true
        	if(addr.Primary_vod__c==false&&addr.BI_Preferred_Address_BI__c!=false){
        		addr.BI_Preferred_Address_BI__c = false;
        		addrstoupd.put(addr.Id, addr);
        	}
        }
                
        update addrstoupd.values();
        
    }
    
    global void finish(Database.BatchableContext BC) {
    	//VEEVA_BATCH_ONEKEY_BATCHUTILS.setCompleted(jobId,lastRunTime); 
   		setCompleted(jobId,lastRunTime);    
    	
        System.debug('BI_BATCH_Address_Preferred_flag FINISHED');
        // That's all folks!
    }



/******************************* 2012.11.21. ********************************************/
/* Add this here from batchutil class  only  to avoid cross-refference deployment error */
   /*******************************************
    Updates the job status to STATUS_COMPLETED 
    and populates the end time with the current
    system date/time.
    This  function will initiate a trigger which
    will  kick of the next  job  later
    *******************************************/
    public static void setCompleted(Id jobId, DateTime LRT) 
    {
        if (jobId != null) 
        {
            List<V2OK_Batch_Job__c> jobs = [SELECT Id FROM V2OK_Batch_Job__c 
                                            WHERE Id = :jobId
                                            ];
            if (!jobs.isEmpty()) 
            {
                V2OK_Batch_Job__c job = jobs.get(0);
                job.Status__c = 'Completed';
                job.End_Time__c = Datetime.now();
                job.LastRunTime__c = LRT;   
                update job;
            }
        }
    }
    
     /***********************************************************
     insert a record  into a custom object:   Batch_Job_Error__c
     ***********************************************************/
     public static void setErrorMessage(Id jobId, String Message) 
     {
        if (jobId != null) 
        {
            //Create an error message  
                Batch_Job_Error__c jobError = new Batch_Job_Error__c();
                jobError.Error_Message__c = Message;
                jobError.Veeva_To_One_Key_Batch_Job__c = jobId;
                jobError.Date_Time__c = Datetime.now();
                insert jobError;        
        }
    }
/* Add this here from batchutil class  only  to avoid cross-refference deployment error */    
/******************************* 2012.11.21. ********************************************/

}