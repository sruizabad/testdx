/**
* @description: Batch class to unflag Author_for_BI_Publications_BI__c on person accounts
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
*/
global without sharing class VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG implements Database.batchable<sObject>, Database.Stateful {
    
    integer success = 0;
    integer total = 0;
    integer errors = 0;
    string[] toAddresses = new String[] {'attila.hagelmayer@veeva.com','daniel.blaski@veeva.com'};
	string errorMessage = '';       
    
    public VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG(){
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String SelStmt = 'SELECT ID, Name, Author_for_BI_Publications_BI__c';
        SelStmt = selStmt + ' FROM Account';
        SelStmt = SelStmt + ' WHERE IsPersonAccount = true AND Author_for_BI_Publications_BI__c = true';   
        SelStmt = SelStmt + ' LIMIT 5000000 ';
        
        System.Debug('Attila select: ' + selStmt);
        
        return Database.getQueryLocator(selStmt); 
    }   
  
    global void execute(Database.BatchableContext BC, List<sObject> batch){
        
        List<Account> batchAccountList = (List<Account>) batch;
        List<Account> listUpdate = new List<Account>();
        
        System.Debug('Attila batchAccountList: ' + batchAccountList.size());
        
        for (Account a :batchAccountList){
			listUpdate.add(new Account(id = a.Id, Author_for_BI_Publications_BI__c = false));
        }
        
        system.debug('Attila listUpdate: ' + listUpdate.size());
        total += listUpdate.size();
        
        try{

            Database.SaveResult[] srList;
            Database.Error[] errs = new list <Database.Error>(); 
            
            if (listUpdate.size() > 0) srList = Database.update(listUpdate, false);

            if (srList != null){
                for (Database.SaveResult result : srList){
                    if (!result.isSuccess()) errs = result.getErrors();
                }
                
                if (errs.size() > 0){
                    success += total - errs.size();
                    errors += errs.size();
                }
            } 
            
        } catch (Exception ex){
            System.Debug('VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG::Unexpected DML error appeared.');
            System.Debug('VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG::Message: ' + ex.getMessage());
            System.Debug('VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG::Cause: ' + ex.getCause());
            System.Debug('VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG::Stacktrace: ' + ex.getStackTraceString()); 
            errorMessage = 'DML error: ' + ex.getMessage() + ' Cause: ' + ex.getCause() + ' Stacktrace: ' + ex.getStackTraceString();
        }      
    }  
	 
    global void finish(Database.BatchableContext BC) {
        
        if (!toAddresses.isEmpty() && (!String.isBlank(errorMessage) || Errors > 0)){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setSenderDisplayName('VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG info');
            mail.setSubject('Info from Org: ' + UserInfo.getOrganizationId() + ' User: ' + Userinfo.getUserId());
            
            String body = '<html lang="en"><body>' +
                '<br>'+
                '<b>' + 'VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG batch class has been failed!' + '</b>' +
                '<br><br>' +              				
                'Number of updated records: ' + success +
                '<br><br>' +              				
                'Number of failed records: ' + errors + 
                '<br><br>' +
                'Error Message:' +
                '<br>' +
                + errorMessage +
                '</body></html>';
            
         	mail.setHtmlBody(body);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });     
        }  
    }
}