/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This batch class is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: This batch class checks the Public_Group_Helper_BI__c on User and if it's populated, adds User to a Public Group where 
				that Public Group Name matches the User's Profile Name + '_MyInsights'.
                In case of profile change (field starts with 'update_'), removes user from old Public Group whose name is that of a old Profile name + '_MyInsights'.
* @scheduler class: VEEVA_BI_SCHEDULE_UPDATE_USER_PG
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
**/
global class VEEVA_BI_BATCH_UPDATE_USER_PG implements Database.Batchable<sObject>{
    
    map<string, User> objMap = new map<string, User>(); 
    List<GroupMember> insertGroupMember = new List<GroupMember>(); 
    List<User> updateUserList = new List<User>(); 
    set<id> userSet = new set<id>();
    set<string> groupSet = new set<string>();
    string concat = '_MyInsights';
    
    //Start method
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query = 'SELECT Id, Profile_Name_vod__c, Public_Group_Helper_BI__c FROM User WHERE Public_Group_Helper_BI__c != null';     
        
        return Database.getQueryLocator(query);
    }
    
    //Execute method
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        for (sObject objScope: scope) { 
            User newObjScope = (User)objScope;
            updateUserList.add(newObjScope);
            if (newObjScope.Public_Group_Helper_BI__c.startsWith('update_')){
                string oldProfile = newObjScope.Public_Group_Helper_BI__c.substringAfter('update_');
                string newProfile = newObjScope.Profile_Name_vod__c;
                
                objMap.put(newProfile + concat, newObjScope);
                userSet.add(newObjScope.Id);
                groupSet.add(oldProfile + concat);
            }else if(newObjScope.Public_Group_Helper_BI__c.startsWith('insert_')){
                objMap.put(newObjScope.Profile_Name_vod__c + concat, newObjScope);
            } 
            
        }
        
        List<Group> groupList = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN:objMap.KeySet()]; 
        system.debug('Attila groupList: ' + groupList);
        
        for (Group g: groupList){
            User obj = ObjMap.get(g.DeveloperName);
            
            GroupMember gm = new GroupMember(); 
            gm.GroupId = g.id;
            gm.UserOrGroupId = obj.id;
            insertGroupMember.add(gm);
        }    
        system.debug('Attila insertGroupMember: ' + insertGroupMember);
        if (!insertGroupMember.isEmpty()) insertGroup(insertGroupMember);
        if (!userSet.isEmpty() && !groupSet.isEmpty()) deleteFromGroup(userSet, groupSet);  
        if (!updateUserList.isEmpty()) updateUsers(updateUserList);  
    }
    
    //Finish method
    global void finish(Database.BatchableContext BC){
        
    }
    
    public void insertGroup(List<GroupMember> gmList){

        try{
            Database.SaveResult[] srListInsert = Database.insert(gmList, false);
            
            if (srListInsert != null){
                for (Database.SaveResult resultInsert : srListInsert){
                    if (!resultInsert.isSuccess()) {
                        system.debug(LoggingLevel.INFO,'Attila errsUpdate: ' + resultInsert.getErrors());                            
                    }
                }
            }   
            
        }catch (system.DmlException Ex){
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Unexpected DML error appeared during insert.');
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Message: ' + Ex.getMessage());
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Cause: ' + Ex.getCause());
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Stacktrace: ' + Ex.getStackTraceString());  
        }

    }
    
    public void deleteFromGroup(set<id> userSet, set<string> groupSet){
        
        List<GroupMember> listGM = [SELECT Id, GroupId FROM GroupMember where UserOrGroupID IN:userSet AND Group.Name IN:groupSet];
        
        try{
            Database.DeleteResult[] srListDelete = Database.delete(listGM, false);
            
            if (srListDelete != null){
                for (Database.DeleteResult resultDelete : srListDelete){
                    if (!resultDelete.isSuccess()) {
                        system.debug(LoggingLevel.INFO,'Attila errsInsert: ' + resultDelete.getErrors());                            
                    }
                }
            }   
            
        }catch (system.DmlException Ex){
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Unexpected DML error appeared during delete.');
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Message: ' + Ex.getMessage());
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Cause: ' + Ex.getCause());
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Stacktrace: ' + Ex.getStackTraceString());  
        }
    } 
    
    public void updateUsers(List<User> userList){
        
        for (User u: userList){
            u.Public_Group_Helper_BI__c = null;
        }
        
        try{
            Database.SaveResult[] srListUpdate = Database.update(userList, false);
            
            if (srListUpdate != null){
                for (Database.SaveResult resultUpdate : srListUpdate){
                    if (!resultUpdate.isSuccess()) {
                        system.debug(LoggingLevel.INFO,'Attila errsUpdate: ' + resultUpdate.getErrors());                            
                    }
                }
            }   
            
        }catch (system.DmlException Ex){
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Unexpected DML error appeared during update.');
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Message: ' + Ex.getMessage());
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Cause: ' + Ex.getCause());
            System.Debug(LoggingLevel.ERROR,'VEEVA_BI_BATCH_UPDATE_USER_PG::Stacktrace: ' + Ex.getStackTraceString());  
        }
    }
}