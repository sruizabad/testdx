/*
 * Changelog
 *
 * Date: 2015-09-25
 * By:   Raphael Krausz <raphael.krausz@veeva.com>
 * Description:
 *   Cloned from VEEVA_BI_CR_Extension
 *   New changes:
 *     Add new functionality (or clone and alter) to VEEVA_CR_PAGE VF - Need a similar screen
 *     but without the filters at the top. It should be a mass entry form for managers to add
 *     their rating for each behavior (in this case all behaviors will be presented to the user)
 *     similar to what we have in Lynx for entering expense line items (only one field will be
 *     editable). Once they are done all the behaviors will be added to the coaching report
 *     into the coaching behavior object (same as we have today) with the addition of the field
 *     the users will fill out in the mass entry form.
 *
 *     Records per page: at least 50 (All behaviours should appear on page)
 *     No checkboxes required, all behaviours on the VF page should be added
 *     All ratings should be one of the following labels (stored with English version):
 *        Rating_Developing
 *        Rating_Demonstrating
 *        Rating_Influencing
 *        Rating_Leading
 *
 */

public without sharing class VEEVA_BI_CFE_QUARTERLY_REPORT_EXTENSION {

  //context object
  private final Coaching_Report_vod__c CRobj;

  public Integer total_size = 0; //used to show user the total size of the List
  private Integer counter = 0; //keeps track of the offset
  /************************************************
  CHANGE THIS VALUE IF YOU WANT TO CHANGE List SIZE
  *************************************************/
  private Integer list_size = 100; //sets the page size or number of rows
  // We no longer want/need pagination

  //collection of objects displayed on the GRID
  public List<DisplaySmtg> ObjectsToDisplay { get; set; }
    
  public List<SelectOption> GetRatings { get; set; }
  public List<SelectOption> optionsR  { get; set; }
  public boolean IsUpdatedFlag { get; set; }  //CR 386
 
  public List<CFE_Rep_Behavior_BI__c> initialBehaviourList { get; set; } 

  private Map<CFE_Rep_Behavior_BI__c, String>  templateList;  //probably not needed anymore

  Set<Id> compfilter = new Set<Id>();
  Set<Id> actfilter = new Set<Id>();

  //Added by Istvan for CR208 - For collecting previous Quarter behaviour lists for the current coaching report
  Map<String,String> PrevBehaviourList = new Map<String,String>();
  
  //CR 386 - Attila
  Map<String,ID> PrevBehaviourIDList = new Map<String,ID>();
  Map<String,ID> CurrBehaviourIDList = new Map<String,ID>();  
  Map<String,boolean> ChangedIDList = new Map<String,Boolean>();
  //Map<String,boolean> ChangedIDListUpd = new Map<String,Boolean>();
  Map<String,String> CurrBehaviourList = new Map<String,String>();
  //public id PrevBehaviourID { get; set; }  
  public string PQ { get; set; }  
  public string CQ { get; set; }  
   
  //just to be displayed on the header
  public String ContextObject { get; set; }

  //boolean deciding if the page is approved and read-only
  public boolean approved { set; get; }
  public boolean IsDeUser { set; get; }

  //custom labels
  public String CR_TITLE { set; get; }
  public String CR_ADD { set; get; }
  public String CR_ADV { set; get; }
  public String CR_TEM { set; get; }
  public String CR_ACT { set; get; }
  public String CR_COMP { set; get; }
  public String CR_COLBEHNUMBER { set; get; }
  public String CR_COLBEH { set; get; }
  public String CR_COLACT { set; get; }
  public String CR_COLCOMP { set; get; }
  public String CR_NTEM { set; get; }
  public String CR_NACT { set; get; }
  public String CR_NCOMP { set; get; }
  public String CR_TOTAL { set; get; }
  public String CR_RETURN { set; get; }
  public String CR_ADDED { set; get; }
  public String CR_APPROVED { set; get; }
  public String CR_PREVRATING { set; get; } //Added for CR 208
      
  private Set<Id> existingBehaviourIdSet;

  //get user country code
  Id id1 = userinfo.getUserId();
  User[] u = [SELECT Country_code_BI__c, LanguageLocaleKey FROM User WHERE Id = :id1 LIMIT 1];
  String c = u[0].Country_code_BI__c;
  String l = u[0].LanguageLocaleKey; //Added for CR 208

  //Do not allow non-regional managers to add Behaviors to CRs created by Regional Managers
  public Boolean RegionalManagerDisable { get; set; }
  
  //CR 208 for behavior filter in InitgridB()
  String employeeOrg = '';
  String employeeOrg2 = '';  
  
  //CR 386
  boolean IscurrBehavior = true;

  /*******************************************************************
  CONSTRUCTOR
  *******************************************************************/
  public VEEVA_BI_CFE_QUARTERLY_REPORT_EXTENSION(ApexPages.StandardController stdController) {
    //display everything normally
    IsUpdatedFlag = false;
    approved = false;
    System.debug('constructor started');
    //get context object form Controller
    this.CRobj = (Coaching_Report_vod__c) stdController.getRecord();
     
    IsDeUser = false;
    if (c=='DE' || c=='NL' || c=='PT') IsDeUser = true;

    Coaching_Report_vod__c myCR = [Select Id, CreatedById, Manager_vod__c, Employee_vod__c, Employee_vod__r.Profile.Name, Year_Quarter_BI__c  from Coaching_Report_vod__c where Id = :CRobj.Id]; //Added more columns by Istvan for CR 208

    //set employee organization - CR 208
    String employeeProfile = myCR.Employee_vod__r.Profile.Name;
      if (employeeProfile!=null) {
            /*if (employeeProfile.indexOf('BELUX_SALES_KAM')>-1
                || employeeProfile.indexOf('NETHERLANDS_SALES_KAM')>-1
                || employeeProfile.indexOf('US_HYBRID_KAM_SALES')>-1) 
            {
                employeeOrg = 'Commercial Sales Rep';*/
            //CR 388
            if (employeeProfile.indexOf('US_HYBRID_KAM_SALES')>-1) 
            {
                employeeOrg = 'Commercial Sales Rep';            
            } else if (employeeProfile.indexOf('BELUX_SALES_KAM')>-1
                      || employeeProfile.indexOf('NETHERLANDS_SALES_KAM')>-1
                      || employeeProfile.indexOf('NORDICS_SALES')>-1
                      || employeeProfile.indexOf('NORDICS_SAMPLING')>-1
                      || employeeProfile.indexOf('DE_SALES')>-1
                      || employeeProfile.indexOf('DE_KAM')>-1
                      || employeeProfile.indexOf('PT_SALES')>-1
                      || employeeProfile.indexOf('PT_KAM')>-1
                      || employeeProfile.indexOf('GR_KAM')>-1
                      || employeeProfile.indexOf('GR_SALES')>-1) { 
                employeeOrg2 = 'KAM';  
                employeeOrg = 'Commercial Sales Rep';                  
            } else if (employeeProfile.indexOf('MSL')>-1) {
                employeeOrg = 'MSL';
            } else if (employeeProfile.indexOf('KAM')>-1) { 
                employeeOrg = 'KAM';
            } else if (employeeProfile.indexOf('MARKET_ACCESS')>-1) { 
                employeeOrg = 'KAM';
            } else if (employeeProfile.indexOf('SALES_MANAGER')>-1) { 
                employeeOrg = 'DM';
            } else if (employeeProfile.endsWith('SALES')) { 
                employeeOrg = 'Commercial Sales Rep';
            } else if (employeeProfile.endsWith('SALES_MSR')) { 
                employeeOrg = 'Commercial Sales Rep';
            } else if (employeeProfile.endsWith('SALES_SSR')) { 
                employeeOrg = 'Commercial Sales Rep';
            }
      }     

    //WA-9/4/2014
    //Setup Regional Manager logic

    // Updated by Raphael Krausz 2014-09-18 changed name for PermissionSet
    //Id RMPermSet = [Select Id from PermissionSet where Name = 'Regional_Manager' LIMIT 1].Id;
    Id RMPermSet = [Select Id from PermissionSet where Name = 'CORE_NSM_COACHING_PERMISSION' LIMIT 1].Id;
    List<PermissionSetAssignment> currentUserPSA = [Select Id from PermissionSetAssignment where AssigneeId = :UserInfo.GetUserId() and PermissionSetId = :RMPermSet];
    List<PermissionSetAssignment> creatingUserPSA = [Select Id from PermissionSetAssignment where AssigneeId = :myCR.CreatedById and PermissionSetId = :RMPermSet];

    if (creatingUserPSA.IsEmpty()) {
      RegionalManagerDisable = false;
    } else if (currentUserPSA.IsEmpty()) {
      RegionalManagerDisable = true;
    } else {
      RegionalManagerDisable = false;
    }

    //get coaching report status to disable page if necessary
    String status = CRobj.Status__c;
    if (status == 'Approved') approved = true;

    //define used maps&List
    templateList  = new Map<CFE_Rep_Behavior_BI__c, String>();

    initialBehaviourList = new List<CFE_Rep_Behavior_BI__c>();

    //InitBehaviorListWithTemplates();  //mayee no longer needed //confirmed :)

    //build template relationship
    //BulidB_T_Relationship();


    // do label translation
    collectLanguageSpecificLabels();

    // Find previously ranked behaviours
    List<CFE_Report_Behavior_BI__c> existingBehaviourList =
      [
        SELECT
        Id,
        Behavior_BI__c
        FROM CFE_Report_Behavior_BI__c
        WHERE Coaching_Report_BI__c = :CRobj.Id
      ];

    existingBehaviourIdSet = new Set<Id>();

    for (CFE_Report_Behavior_BI__c existingBehaviour : existingBehaviourList) {
      existingBehaviourIdSet.add(existingBehaviour.Behavior_BI__c);
    }



    //load to bg
    InitgridB();
    System.debug('refresh List');
    
    //Added by Istvan for CR 208 - get previous quarter ratings
    InitPreviousQuarterBehaviourList(myCR);

    //render eleements with the refresh command
    RefreshFfilterdList();
  }

  //Added by Erik Dozsa, 2013.05.06.
  //Custom label translation according to user's language setup - logic from 'Global Account Search' solution
  public void collectLanguageSpecificLabels() {

    String lang = UserInfo.getLanguage();

    //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'User Language is: ' + lang));

    // Get labels from Message_vod__c for user's language
    Message_vod__c [] labels =
      [
        select Name,
        Text_vod__c
        from   Message_vod__c
        where  Language_vod__c = :lang
                                 and    Category_vod__c = 'Coach Report'
                                     //and  Name in ('CR_ADDED','CR_TITLE','CR_ADD','CR_ADV','CR_TEM','CR_ACT','CR_COMP','CR_COLBEH','CR_COLACT','CR_COLCOMP','CR_NTEM','CR_NACT','CR_NCOMP','CR_TOTAL', 'CR_RETURN')
                                     and    Active_vod__c = true
      ];

    Map <String, String> labelMap = new Map <String, String> ();

    for (Message_vod__c label : labels) {
      labelMap.put(label.Name, label.Text_vod__c);
    }
    //handle the situation when the Label does not exists in the Message obj
    //x ? y : z
    CR_TITLE = labelMap.get('CR_TITLE') != null ? labelMap.get('CR_TITLE') : 'Coaching Report';
    CR_ADD = labelMap.get('CR_ADD') != null ? labelMap.get('CR_ADD') : 'Add';
    CR_ADV = labelMap.get('CR_ADV') != null ? labelMap.get('CR_ADV') : 'Apply Filters for the grid below.';
    CR_TEM = labelMap.get('CR_TEM') != null ? labelMap.get('CR_TEM') : 'Template:';
    CR_ACT = labelMap.get('CR_ACT') != null ? labelMap.get('CR_ACT') : 'Activity:';
    CR_COMP = labelMap.get('CR_COMP') != null ? labelMap.get('CR_COMP') : 'Competency:';
    CR_COLBEHNUMBER = labelMap.get('CR_COLBEHNUMBER') != null ? labelMap.get('CR_COLBEHNUMBER') : 'Behavior #';
    CR_COLBEH = labelMap.get('CR_COLBEH') != null ? labelMap.get('CR_COLBEH') : 'Behavior';
    CR_COLACT = labelMap.get('CR_COLACT') != null ? labelMap.get('CR_COLACT') : 'Activity';
    CR_COLCOMP = labelMap.get('CR_COLCOMP') != null ? labelMap.get('CR_COLCOMP') : 'Competency';
    CR_NTEM = labelMap.get('CR_NTEM') != null ? labelMap.get('CR_NTEM') : 'None - Select Template';
    CR_NACT = labelMap.get('CR_NACT') != null ? labelMap.get('CR_NACT') : 'None - Select Activity';
    CR_NCOMP = labelMap.get('CR_NCOMP') != null ? labelMap.get('CR_NCOMP') : 'None - Select Competency';
    CR_TOTAL = labelMap.get('CR_TOTAL') != null ? labelMap.get('CR_TOTAL') : 'Showing Page # ';
    CR_RETURN = labelMap.get('CR_RETURN') != null ? labelMap.get('CR_RETURN') : 'Return to ';
    CR_ADDED = labelMap.get('CR_ADDED') != null ? labelMap.get('CR_ADDED') : 'Behaviors have been added.';
    CR_APPROVED = labelMap.get('CR_APPROVED') != null ? labelMap.get('CR_APPROVED') : 'The Coaching Report cannot be modified/deleted because it has already been approved.';
    CR_PREVRATING = labelMap.get('CR_PREVRATING') != null ? labelMap.get('CR_PREVRATING') : 'Previous Quarter Rating';
  }
  //end of translation section




  public string Selected_ActivityFilter;

  public string getSelected_ActivityFilter() {
    System.debug('Getting Selected_ActivityFilter: ' + Selected_ActivityFilter);
    return Selected_ActivityFilter;
  }
  public string setSelected_ActivityFilter(String s) {
    System.debug('Setting Selected_ActivityFilter: ' + s);
    Selected_ActivityFilter = s;
    return Selected_ActivityFilter;
  }

  /************************************************
  Collect all  activities from custom object
  CFE_Rep_Activity_BI__c
  ************************************************/
  public List<SelectOption> getListActivityItems() {
    System.debug('getListActivityItems');
    System.debug('actfilter size: ' + actfilter.size());
    System.debug('Selected_ActivityFilter: ' + Selected_ActivityFilter);

    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('', CR_NACT));
    CFE_Rep_Activity_BI__c[] Cs;

    if ( actfilter.isEmpty() ) {
      Cs =
        [
          SELECT Id, Name
          FROM CFE_Rep_Activity_BI__c
          WHERE Country_Code_BI__c = :c
                                     ORDER BY Name
        ];

    } else {
      Cs =
        [
          SELECT Id, Name
          FROM CFE_Rep_Activity_BI__c
          WHERE Country_Code_BI__c = :c
                                     AND Id IN :actfilter
                                     ORDER BY Name
        ];
    }


    for (CFE_Rep_Activity_BI__c c : Cs) {
      options.add(new SelectOption(c.Id, c.Name));
    }

    //RefreshFfilterdList();
    return options;
  }

    
  public String Selected_CompetencyFilter { get; set; }

  /************************************************
  Collect all  competencies from custom object
  CFE_Rep_Competency_BI__c
  ************************************************/
  public List<SelectOption> getListCompetencyItems() {
    System.debug('getListCompetencyItems');
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('', CR_NCOMP));
    CFE_Rep_Competency_BI__c[] Cs;

    if (compfilter.size() < 1) {
      Cs = [select Id, Name  from CFE_Rep_Competency_BI__c
            where Country_Code_BI__c = :c

                                       order by Name
           ];
    } else {
      Cs = [select Id, Name  from CFE_Rep_Competency_BI__c
            where Country_Code_BI__c = :c
                                       and Id IN :compfilter
                                       order by Name
           ];
    }


    for (CFE_Rep_Competency_BI__c c : Cs) {
      options.add(new SelectOption(c.Id, c.Name));
    }

    return options;
  }

  public string Selected_TemplateFilter { get; set; }

  /************************************************
  Collect all  competencies from custom object
  CFE_Rep_Competency_BI__c
  ************************************************/


  public List<SelectOption> getListTemplateItems() {

    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('none', CR_NTEM));

    Coaching_Template_BI__c[] Ts = [select Id, Name  from Coaching_Template_BI__c
                                    where Country_Code_BI__c = :c
                                        order by Name
                                   ];

    for (Coaching_Template_BI__c t : Ts) {
      options.add(new SelectOption(t.Name, t.Name));
    }

    return options;
  }

  /********************************************
  List PAGINATION FUNCTIONS
  ********************************************/
  //CR 386 not needed 
  /*public PageReference UpdateFlag() { 
    
    for (DisplaySmtg obj : ObjectsToDisplay) {

        if (obj.thePreviousRating == obj.theRating) obj.IsChanged = false;
        else obj.IsChanged = true;
    }
      
    return null;
  }*/
    
  public PageReference Beginning() { //user clicked beginning
    counter = 0;
    RefreshFfilterdList();

    return null;
  }

  public PageReference Previous() { //user clicked previous button
    counter -= list_size;
    RefreshFfilterdList();

    return null;
  }

  public PageReference Next() { //user clicked next button
    counter += list_size;
    RefreshFfilterdList();

    return null;
  }

  public PageReference End() { //user clicked end
    counter = total_size - math.mod(total_size, list_size);
    RefreshFfilterdList();

    return null;
  }

  public Boolean getDisablePrevious() {
    //this will disable the previous and beginning buttons
    if (counter > 0) return false; else return true;
  }

  public Boolean getDisableNext() { //this will disable the next and end buttons
    if (counter + list_size < total_size) return false; else return true;
  }

  public Integer getTotal_size() {
    return total_size;
  }

  public Integer getPageNumber() {
    return counter / list_size + 1;
  }

  public Integer getTotalPages() {
    if (math.mod(total_size, list_size) > 0) {
      return total_size / list_size + 1;
    } else {
      return (total_size / list_size);
    }
  }




  /******************************************************** *********************
  get the Data from DB.
  /* ****************************************************************************/
  //heap excessive
  private void InitgridB() {
    System.debug('InitgridB started');

    List<CFE_Rep_Behavior_BI__c> behaviourList =
      [
        SELECT
        Id,
        Name,
        Activity_BI__c,
        Competency_BI__c,
        Activity_BI__r.Name,
        Competency_BI__r.Name,
        Behavior_Number_BI__c
        FROM CFE_Rep_Behavior_BI__c
        WHERE Country_Code_BI__c = :c
        AND Active_BI__c = true
        AND (Language_Code_BI__c = null or Language_Code_BI__c = :l) //Added for CR 208
        //AND (Organization_BI__c = null or Organization_BI__c = :employeeOrg) //Added for CR 208
        AND (Organization_BI__c = null or Organization_BI__c = :employeeOrg or Organization_BI__c = :employeeOrg2) //Added for CR 388
        ORDER BY Behavior_Number_BI__c
      ];

    ObjectsToDisplay = new List<DisplaySmtg>();


    for (CFE_Rep_Behavior_BI__c behavior : behaviourList) {
      // if the behaviour is already there - don't add it twice
      /*if ( ! existingBehaviourIdSet.contains(behavior.Id) ) {
        initialBehaviourList.add(behavior);
      }*/
        // CR 386
        initialBehaviourList.add(behavior);
    }
      
    if (initialBehaviourList.isEmpty()) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'There are no new behaviours to add'));
    }


  }


  /******************************************************** *********************
     Fill up previous Quarter Behaviour List
  /* ****************************************************************************/
  private void InitPreviousQuarterBehaviourList(Coaching_Report_vod__c myCR) {

        //get Coaching reports for the same Manager+Employee
        List<Coaching_Report_vod__c> crList = [
                SELECT 
                Id, 
                Year_Quarter_BI__c
                FROM Coaching_Report_vod__c 
                WHERE Manager_vod__c =:myCR.Manager_vod__c
                AND Employee_vod__c =:myCR.Employee_vod__c
                AND Recordtype.developerName = 'Quarterly_Report_BI'
            ];

        //put in a map with simplified quarter format
        Map<String,Id> crListQuarter = new Map<String,Id>(); 
        
        for (Coaching_Report_vod__c cr : crList) {
            String[] quarterDate = cr.Year_Quarter_BI__c.split(' ');
            if (quarterDate.size() != 2) continue; //wrong format go to next
            crListQuarter.put(quarterDate[1]+quarterDate[0],cr.Id);  //change it to 2015Q2 format - to be able to sort it alphabetically            
        }   
        
        //convert to list because of sorting
        List<String> QuarterSortedList = new List<String>(crListQuarter.keySet());
        QuarterSortedList.sort(); //asc
        //find previous quarter
        String prevQuarter = null;
        
        String[] currentQuarterDate = myCR.Year_Quarter_BI__c.split(' ');
        if (currentQuarterDate.size() != 2) return; //cannot split - wrong format   
        String currentQuarter = currentQuarterDate[1]+currentQuarterDate[0]; //simplified format (2015Q2)
        string prevQ ='';
        
        for (String quarter: QuarterSortedList) {
            if (quarter == currentQuarter) {
                break;
            } else prevQuarter = quarter; 
        }
        if (prevQuarter == null) 
        {
            prevQuarter = currentQuarter;
        }
        prevQ = prevQuarter.right(2) + ' ' + prevQuarter.left(4);
        
        PQ = prevQ;
        CQ = currentQuarter.right(2) + ' ' + currentQuarter.left(4);
		
        if (prevQ != '') 
        {
            //PrevBehaviourID = [SELECT Id, Name FROM Coaching_Report_vod__c Where Year_Quarter_BI__c =:prevQ limit 1].Id;
            List<CFE_Report_Behavior_BI__c > CbehaviourList =
              [
                SELECT
                Id,
                Behavior__c,
                Rating_BI__c,
                Is_Changed_BI__c
                FROM CFE_Report_Behavior_BI__c
                WHERE Coaching_Report_BI__c = :crListQuarter.get(currentQuarter)
              ];
             //fill up prev behaviour list
             for (CFE_Report_Behavior_BI__c Cbehavior : CbehaviourList) {
                 String CbehaviorName = Cbehavior.Behavior__c.substring(Cbehavior.Behavior__c.indexOf(' ')+1);
                 CurrBehaviourList.put(CbehaviorName,Cbehavior.Rating_BI__c);
                 CurrBehaviourIDList.put(CbehaviorName, Cbehavior.Id);
                 ChangedIDList.put(CbehaviorName, Cbehavior.Is_Changed_BI__c);
             }            
        }

        if (CurrBehaviourList.size() == 0) IscurrBehavior = false;
      
        if (prevQuarter != null) { 
            //get previous quarter's coaching report behaviour list
            List<CFE_Report_Behavior_BI__c > behaviourList =
              [
                SELECT
                Id,
                Behavior__c,
                Rating_BI__c,
                Is_Changed_BI__c
                FROM CFE_Report_Behavior_BI__c
                WHERE Coaching_Report_BI__c = :crListQuarter.get(prevQuarter)
              ];
             //fill up prev behaviour list
             for (CFE_Report_Behavior_BI__c behavior : behaviourList) {
                 String behaviorName = behavior.Behavior__c.substring(behavior.Behavior__c.indexOf(' ')+1);
                 PrevBehaviourList.put(behaviorName,behavior.Rating_BI__c);
                 PrevBehaviourIDList.put(behaviorName, behavior.Id);
                 if (ChangedIDList.isEmpty() == true) ChangedIDList.put(behaviorName, behavior.Is_Changed_BI__c);
             }
             //set prev quarter to display
             CR_PREVRATING += ' ('+prevQuarter+')'; 
        }
            
  }



  /************************************************************************************************
  Build a comma separated string from templates to which the Behavior belongs to
  ************************************************************************************************/
  public void BuildTempListMap(CFE_Rep_Behavior_BI__c pB, Coaching_Template_Behavior_BI__c[] pCTs) {
    String st = ',';

    for (Coaching_Template_Behavior_BI__c CT : pCTs) {
      st = st  + CT.Name + ',';
    }

    templateList.put(pB, st);
  }

  //CR 386 - Display ratings in selectoption dinamically - Attila Hagelmayer 
 public List<SelectOption> GetRatings() {
           
    List<SelectOption> optionsR = new List<SelectOption>();
                  
    Schema.DescribeFieldResult fieldResult = CFE_Report_Behavior_BI__c.Rating_BI__c.getDescribe();
      
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

    optionsR.add(new SelectOption('-', '-', false));    
      
    for( Schema.PicklistEntry f : ple)
        {
            //optionsR.add(new SelectOption(f.getLabel(), f.getValue()));
            optionsR.add(new SelectOption(f.getValue(),f.getLabel()));
        } 
        
    return optionsR;
  }


  /***********************************************************************************************
  REFRESH-RENDER List
  Iterate through the InitList  and
  sort out the filtered items.
  ***********************************************************************************************/
  public void RefreshFfilterdList() {
    System.debug('RefreshFfilterdList');

    String Act;
    String Comp;
    String Tempu;
    //Integer rec = 0;
    //Boolean mod = true;
    Integer cycle = 0;
    total_size = 0;
    compfilter = new Set<Id>();
    actfilter = new Set<Id>();
    map<id, String> CTBmap = new map<id, String>();

    ObjectsToDisplay = new List<DisplaySmtg>();

    System.debug('initialBehaviourList.size():' + initialBehaviourList.size());
    if (this.initialBehaviourList.size() > 0) {
      //for(Integer i=counter; i<initialBehaviourList.size() && i<(counter+list_size); i++)

      //total_size = initialBehaviourList.size();

      //if the template is selected, get the appropriate map for it
      if (this.Selected_TemplateFilter != 'none' && this.Selected_TemplateFilter != null) {
        Coaching_Template_Behavior_BI__c[] CTBs = [Select Behavior_BI__c, Coaching_Template_BI__r.Name
            From Coaching_Template_Behavior_BI__c where Coaching_Template_BI__r.Name = :this.Selected_TemplateFilter];

        for (Coaching_Template_Behavior_BI__c CTB : CTBs) {
          CTBmap.put(CTB.Behavior_BI__c, CTB.Coaching_Template_BI__r.Name);
        }

      }

        
      for (CFE_Rep_Behavior_BI__c Bu : initialBehaviourList) {
        //if match the filter add to display List
        //CFE_Rep_Behavior_BI__c Bu = initialBehaviourList[i];

        Act = Bu.Activity_BI__c;
        Comp = Bu.Competency_BI__c;


        DisplaySmtg Dobj = new DisplaySmtg(Bu);
        //Added for CR 208
        //Dobj.thePreviousRating = PrevBehaviourList.get(Bu.Name) == null ? '' : PrevBehaviourList.get(Bu.Name);
        //CR 386
        /*if (IscurrBehavior == true) 
        {
            Dobj.thePreviousRating = CurrBehaviourList.get(Bu.Name) == null ? '' : CurrBehaviourList.get(Bu.Name);
            Dobj.thePreviousID = CurrBehaviourIDList.get(bu.name);
            //Dobj.theRating = CurrBehaviourList.get(Bu.Name) == null ? '' : CurrBehaviourList.get(Bu.Name);
            Dobj.thePreviousQuarterRating = PrevBehaviourList.get(Bu.Name) == null ? '' : PrevBehaviourList.get(Bu.Name);    
        }
        else 
        {
             Dobj.thePreviousRating = PrevBehaviourList.get(Bu.Name) == null ? '' : PrevBehaviourList.get(Bu.Name);            
            // Dobj.theRating = PrevBehaviourList.get(Bu.Name) == null ? '' : PrevBehaviourList.get(Bu.Name);
             Dobj.thePreviousID = PrevBehaviourIDList.get(bu.name);
        }*/

        Dobj.thePreviousRating = CurrBehaviourList.get(Bu.Name) == null ? '' : CurrBehaviourList.get(Bu.Name);
        Dobj.thePreviousID = CurrBehaviourIDList.get(bu.name);
        //Dobj.theRating = CurrBehaviourList.get(Bu.Name) == null ? '' : CurrBehaviourList.get(Bu.Name);
        Dobj.thePreviousQuarterRating = PrevBehaviourList.get(Bu.Name) == null ? '' : PrevBehaviourList.get(Bu.Name);    
          
        //Dobj.thePreviousID = PrevBehaviourIDList.get(bu.name);
        //Dobj.isChanged = ChangedIDList.get(bu.name);
        Dobj.isChanged = false;
          
        System.debug('Selected_ActivityFilter:' + this.Selected_ActivityFilter);
        if (this.Selected_ActivityFilter != '' && this.Selected_ActivityFilter != null) {
          if (Selected_ActivityFilter != Act)
            continue;

        }
        //System.debug('Selected_CompetencyFilter: ' + this.Selected_CompetencyFilter);
        if (this.Selected_CompetencyFilter != '' && this.Selected_CompetencyFilter != null) {
          if (this.Selected_CompetencyFilter != Comp)
            continue;
        }
        //System.debug('Selected_TemplateFilter' + this.Selected_TemplateFilter);
        //replace this part with a more effective code  - get it from a map -DONE

        if (this.Selected_TemplateFilter != 'none' && this.Selected_TemplateFilter != null) {
          //if (this.FindBT_Match(Bu.Name,Selected_TemplateFilter ) == false)
          if (CTBmap.get(Bu.Id) == null || CTBmap.get(Bu.Id) == '')
            continue;
        }
        //System.debug('Adding element: ' + Dobj);
        cycle++;
        total_size++;

        if (Bu.Activity_BI__c != null)
          actfilter.add(Bu.Activity_BI__c);

        if (Bu.Competency_BI__c != null)
          compfilter.add(Bu.Competency_BI__c);

        //cycle counts added List elements,
        //when it gets over the currently displayed number
        //it adds the next list_size elements
        if (cycle > counter && cycle <= (counter + list_size) ) {
          ObjectsToDisplay.add(Dobj);
        }
        //only cycle throught the necessary elements not to break action limits
        //if(rec==list_size) break;


      }

      System.debug('total size: ' + total_size);
      System.debug('counter: ' + counter);
      if (counter > total_size) Beginning();
    }
  }

  /**********************************************************************************************************************************
  Create new CFE_Report_Behavior_BI__c records based
  on the contextObject and the selected Behaviors
  from the grid
  ***********************************************************************************************************************************/
  public PageReference CreateNewCoachingReportBehaviors() {

    List<CFE_Report_Behavior_BI__c>  behaviourList = new List<CFE_Report_Behavior_BI__c>();

    for (DisplaySmtg obj : ObjectsToDisplay) {

      // Lets add the new behaviour
      CFE_Report_Behavior_BI__c newBehaviour = new CFE_Report_Behavior_BI__c();
        
      newBehaviour.Coaching_Report_BI__c = CRobj.id;     
      //newBehaviour.Coaching_Report_BI__c = PrevBehaviourID;

      if (IscurrBehavior == true) newBehaviour.id = obj.thePreviousID;
      else
          newBehaviour.Previous_Quarter_Behavior_BI__c = obj.thePreviousID;
        
      if (obj.CTB != null) // if CTB
        newBehaviour.Behavior_BI__c = obj.CTB.Behavior_BI__c;
      else                 // if B
        newBehaviour.Behavior_BI__c = obj.theBehaviorbjID;
        
      //String rating = obj.theRating;
      //CR 386
      String rating = obj.thePreviousRating; 

      /*if ( String.isBlank(rating) || rating == '-' ) {
        String message = 'You must fill out all the ratings for every behaviour';

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
        return null;
      }*/

      newBehaviour.Rating_BI__c = rating;
      //newBehaviour.Old_Coaching_Behavior_BI__c = obj.thePreviousID;
      behaviourList.add(newBehaviour);
    }

    if ( ! behaviourList.isEmpty() ) {
      try
      {
          //insert behaviourList;
          if (IscurrBehavior == true)
              update behaviourList;
          else
          {
              insert behaviourList;
          }    
          //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, CR_ADDED));
          //return null;
      }
      catch (Exception Ex)
      {
 		  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'MESSAGE: ' + Ex.getMessage() + ' CAUSE: ' + Ex.getCause() + ' STRACKTRACE: ' + Ex.getStackTraceString()));
          return null;
     }
    }

    PageReference pageRef = new PageReference('/' + CRobj.Id);
    return pageRef;
  }

  /**************************************************

  **************************************************/
  public Boolean HasBeh_Template(ID pId, String pTempl) {
    try {

      if (ObjectsToDisplay.size() == 0) {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'Exception occured: '));
      } else
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'Exsfds '));


      for (DisplaySmtg obj : ObjectsToDisplay) {


        if (obj.theBehaviorbjID != pID)
          continue;

        if (obj.HasTemplate(pTempl))
          return true;
      }

      return false;
    } catch (Exception myex) {
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'Exception occured: ' + myex.getMessage()));
      return false;
    }


  }

  /***************************
  Inner class storing the Accs

  Coaching_Template_Behavior_BI__c
  ***************************/
  public class DisplaySmtg {
    Map<String, String>  templateList = new Map<String, String>();

    Coaching_Template_Behavior_BI__c CTB;
    ID theBehaviorbjID;
    CFE_Rep_Behavior_BI__c theBehaviorObj;

    //the below columns will be displayed in the grid
    public String theBehaviornumber { get; set; }
    public String theBehavior { get; set; }
    public String theActivity  { get; set; }
    public String theCompetency { get; set; }
    public String theRating { get; set; }
    public String thePreviousRating { get; set; } //Added for CR 208
    public ID thePreviousID { get; set; } //Added for CR 386
    public Boolean isChanged { get; set; } //Added for CR 386  
    public String thePreviousQuarterRating { get; set; } //Added for CR 386  
    
    /*******************************************************

    *******************************************************/
    public DisplaySmtg(Coaching_Template_Behavior_BI__c pCTB) {
      CTB = pCTB;
      theBehaviorbjID = pCTB.Behavior_BI__c;
      theBehaviorObj = pCTB.Behavior_BI__r;

      theBehavior = pCTB.Behavior_BI__r.Name;
      theActivity = pCTB.Behavior_BI__r.Activity_BI__r.Name;
      theCompetency = pCTB.Behavior_BI__r.Competency_BI__r.Name;
      theBehaviornumber = pCTB.Behavior_BI__r.Behavior_Number_BI__c;
    }

 
    /********************************************

    ********************************************/
    public DisplaySmtg(CFE_Rep_Behavior_BI__c pB) {

      theBehaviorbjID = pB.Id;
      theBehaviorObj = pB;
        
      theBehavior = pB.Name;
      theActivity = pB.Activity_BI__r.Name;
      theCompetency = pB.Competency_BI__r.Name;
      theBehaviornumber = pB.Behavior_Number_BI__c;

    }


    /************************************************************************************

    ************************************************************************************/
    public DisplaySmtg(CFE_Rep_Behavior_BI__c pB, Coaching_Template_Behavior_BI__c[] CTs) {

      theBehaviorbjID = pB.Id;
      theBehaviorObj = pB;

      theBehavior = pB.Name;
      theActivity = pB.Activity_BI__r.Name;
      theCompetency = pB.Competency_BI__r.Name;
      theBehaviornumber = pB.Behavior_Number_BI__c;
        
      if (CTs != null) {
        for (Coaching_Template_Behavior_BI__c CT : CTS) {
          templateList.put(CT.Name, pB.Name);
        }
      }
    }


    public Boolean HasTemplate(String templateName) {
      if (templateList.containsKey(templateName) == true)
        return true;
      else
        return false;
    }


  }





}