public without sharing class VEEVA_BI_CHC_Order_List_Extension {
    public Id OId {get;set;} //the ID we send from the calling code to the template, and receive it from there
    public List<Order_Line_vod__c> getOLs() {
        List<Order_Line_vod__c> OLs;
        //Add any fields to the select
        OLs = [SELECT Id, 
                      Quantity_vod__c, 
                      Name, 
                      Product_vod__c, 
                      Product_vod__r.Name, 
                      Product_vod__r.UPC_EAN_BI__c,
                      Product_vod__r.Gift_BI__c, //Attila Jira: CRM1-47
                      Reason_BI__c, //Attila
                      Wholesaler_product_id_BI__c, //Attila
                      List_Price_Rule_vod__c, //Attila - CR 713
                      List_Amount_vod__c, 
                      Net_Amount_vod__c, 
                      Line_Discount_vod__c, 
                      Free_Goods_vod__c,
                      Discounted_Amount_BI__c, //Ryan Jira: CRM1-1377
                      zvod_PI_Suggested_Order_Quantity_BI__c //Ryan Jira: CRM1-1377
                      FROM Order_Line_vod__c 
                      WHERE Order_vod__c = :OId
                      AND Product_vod__r.Gift_BI__c = false];  //Attila Jira: CRM1-47
        return OLs;
    }

}