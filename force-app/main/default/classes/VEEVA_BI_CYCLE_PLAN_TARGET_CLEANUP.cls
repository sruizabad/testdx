/*
Deletes all the Cycle_Plan_Target_vod__c -s where the cycle_plan_account_vod__c is empty,
due to the account merges done by cap.

TO RUN:
VEEVA_BI_CYCLE_PLAN_TARGET_CLEANUP b = new VEEVA_BI_CYCLE_PLAN_TARGET_CLEANUP(); database.executebatch(b,200);

---

VEEVA_BI_CYCLE_PLAN_TARGET_CLEANUP

New requirement for 18th of September Change Requests

Author: Raphael Krausz <raphael.krausz@veeva.com>
Date:   2015-08-25
New description:
  After account mergers, there are duplicate cycle plan targets in active cycle plans, the duplicates
  should be removed so that only one target remains per account in a cycle plan.  

*/

global class VEEVA_BI_CYCLE_PLAN_TARGET_CLEANUP implements Database.Batchable<SObject>, Schedulable {

    global void execute(SchedulableContext sc) {
        VEEVA_BI_CYCLE_PLAN_TARGET_CLEANUP b = new VEEVA_BI_CYCLE_PLAN_TARGET_CLEANUP();
        database.executebatch(b, 500);
    }

    global VEEVA_BI_CYCLE_PLAN_TARGET_CLEANUP () {
        system.debug('VEEVA_BI_CYCLE_PLAN_TARGET_CLEANUP STARTED');
    }

    global List<AggregateResult> start(Database.BatchableContext BC) {
        List<AggregateResult> allDuplicateTargets =
            [
                SELECT Cycle_Plan_vod__c, Cycle_Plan_Account_vod__c
                FROM Cycle_Plan_Target_vod__c
                WHERE Cycle_Plan_vod__r.Active_vod__c = true
                        GROUP BY Cycle_Plan_vod__c, Cycle_Plan_Account_vod__c
                HAVING Count(Id) > 1
            ];

        return allDuplicateTargets;
    }


    global void execute(Database.BatchableContext BC, List<AggregateResult> duplicateTargetList) {

        Set<Id> cyclePlanIdSet = new Set<Id>();
        Set<Id> accountIdSet   = new Set<Id>();

        for (AggregateResult duplicateTarget : duplicateTargetList) {
            Id cyclePlanId = (Id) duplicateTarget.get('Cycle_Plan_vod__c');
            Id accountId   = (Id) duplicateTarget.get('Cycle_Plan_Account_vod__c');

            cyclePlanIdSet.add(cyclePlanId);
            accountIdSet.add(accountId);
        }

        List<Cycle_Plan_Target_vod__c> allTargetsList =
            [
                SELECT Id, Cycle_Plan_vod__c, Cycle_Plan_Account_vod__c
                FROM Cycle_Plan_Target_vod__c
                WHERE Cycle_Plan_vod__c IN :cyclePlanIdSet
                AND Cycle_Plan_Account_vod__c IN :accountIdSet
                ORDER BY Cycle_Plan_vod__c, Cycle_Plan_Account_vod__c
            ];

        Id lastCyclePlanId;
        Id lastAccountId;
        List<Cycle_Plan_Target_vod__c> toDelete = new List<Cycle_Plan_Target_vod__c>();

        for (Cycle_Plan_Target_vod__c target : allTargetsList) {

            Id cyclePlanId = target.Cycle_Plan_vod__c;
            Id accountId   = target.Cycle_Plan_Account_vod__c;

            // When the target's cycle plan and account are the same as the ones in the last target
            // then we know that the target is a duplicate.

            // N.B. Due to the way we grabbed the cycle plan targets, duplicates will
            // definitely be present, but non-duplicated targets may be returned.


            // If the cycle plan is not the same as the last cycle plan
            // then keep this target, note it down and move on to the next
            if (cyclePlanId != lastCyclePlanId) {
                lastCyclePlanId = cyclePlanId;
                lastAccountId   = accountId;
                continue;
            }

            // At this point the cycle plan must be the same as the last cycle plan
            // If the account is not the same as the last account
            // then this target is not a duplicate
            // We note the new account and move to the next target
            if (accountId != lastAccountId) {
                lastAccountId = accountId;
                continue;
            }

            // By this point in the loop, the cycle plan and the account of the target
            // must be the same as the last one examined, and hence the target is a duplicate
            toDelete.add(target);
        }

        delete toDelete;

    }

    global void finish(Database.BatchableContext BC) {
        System.debug('VEEVA_BI_CYCLE_PLAN_TARGET_CLEANUP FINISHED');
    }
}