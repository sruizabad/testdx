/**
 * VEEVA_BI_DEACTIVATE_CLM_KEYMESSAGE
 *
 * Author: Raphael Krausz <raphael.krausz@veeva.com>
 * Date:   2015-08-27
 * Description:
 *  Deactivate active key messages where the CLM Id is 'CLM ID' OR there is no media file
 *  AND where the Key Mesage is ot used in any CLM Presentation Slides.
 *
 *
 */

global class VEEVA_BI_DEACTIVATE_CLM_KEYMESSAGE implements Database.Batchable<SObject>, Schedulable {

  global void execute(SchedulableContext sc) {
    VEEVA_BI_DEACTIVATE_CLM_KEYMESSAGE b = new VEEVA_BI_DEACTIVATE_CLM_KEYMESSAGE();
    database.executebatch(b, 500);
  }

  global VEEVA_BI_DEACTIVATE_CLM_KEYMESSAGE () {
    system.debug('VEEVA_BI_DEACTIVATE_CLM_KEYMESSAGE STARTED');
  }


  global List<Key_Message_vod__c> start(Database.BatchableContext BC) {

    List<Key_Message_vod__c> keyMessageList =
      [
        SELECT Id, Active_vod__c
        FROM Key_Message_vod__c
        WHERE Active_vod__c = true
        AND Is_Shared_Resource_vod__c = FALSE AND
                              (
                                CLM_ID_vod__c = 'CLM ID'
                                    OR Media_File_Name_vod__c != null
                              )
                              AND Id NOT IN (
                                SELECT Key_Message_vod__c
                                FROM Clm_Presentation_Slide_vod__c
                              )
      ];


    return keyMessageList;
  }



  global void execute(Database.BatchableContext BC, List<Key_Message_vod__c> keyMessageList) {

    for (Key_Message_vod__c keyMessage : keyMessageList) {
      keyMessage.Active_vod__c = false;
    }

    update keyMessageList;

  }

  global void finish(Database.BatchableContext BC) {
    System.debug('VEEVA_BI_DEACTIVATE_CLM_KEYMESSAGE FINISHED');
  }
}