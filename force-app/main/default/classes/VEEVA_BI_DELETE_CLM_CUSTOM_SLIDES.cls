/**
 * VEEVA_BI_DELETE_CLM_CUSTOM_SLIDES
 *
 * Author: Raphael Krausz <raphael.krausz@veeva.com>
 * Date:   2015-08-27
 * Description:
 *  Delete CLM Presentation Slide records which have key messages that belong only to
 *  Custom CLM Presentation slides (and not 'HQ')
 *
 */

global class VEEVA_BI_DELETE_CLM_CUSTOM_SLIDES implements Database.Batchable<SObject>, Schedulable {

  global void execute(SchedulableContext sc) {
    VEEVA_BI_DELETE_CLM_CUSTOM_SLIDES b = new VEEVA_BI_DELETE_CLM_CUSTOM_SLIDES();
    database.executebatch(b, 500);
  }

  global VEEVA_BI_DELETE_CLM_CUSTOM_SLIDES () {
    system.debug('VEEVA_BI_DELETE_CLM_CUSTOM_SLIDES STARTED');
  }


  global List<Key_Message_vod__c> start(Database.BatchableContext BC) {

    List<Key_Message_vod__c> keyMessageList =
      [
        SELECT Id
        FROM Key_Message_vod__c
        WHERE Id IN (
          SELECT Key_Message_vod__c
          FROM Clm_Presentation_Slide_vod__c
          WHERE Clm_Presentation_vod__r.Type_vod__c = 'Custom'
        )
        AND Id NOT IN (
          SELECT Key_Message_vod__c
          FROM Clm_Presentation_Slide_vod__c
          WHERE Clm_Presentation_vod__r.Type_vod__c = 'HQ'
        )
      ];


    return keyMessageList;
  }



  global void execute(Database.BatchableContext BC, List<Key_Message_vod__c> keyMessageList) {


    Set<Id> keyMessageIdSet = new Set<Id>();
    for (Key_Message_vod__c keyMessage : keyMessageList) {
      keyMessageIdSet.add(keyMessage.Id);
    }

    List<Clm_Presentation_Slide_vod__c> slidesToDeleteList =
      [
        SELECT Id
        FROM Clm_Presentation_Slide_vod__c
        WHERE Key_Message_vod__c IN :keyMessageIdSet
      ];

    if ( ! slidesToDeleteList.isEmpty() ) {
      delete slidesToDeleteList;
    }

  }

  global void finish(Database.BatchableContext BC) {
    System.debug('VEEVA_BI_DELETE_CLM_CUSTOM_SLIDES FINISHED');
  }
}