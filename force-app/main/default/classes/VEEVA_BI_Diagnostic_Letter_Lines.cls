public with sharing class VEEVA_BI_Diagnostic_Letter_Lines {
	
	public Id dId {get;set;} //the ID we send from the calling code to the template, and receive it from there
    
    //public List<Diagnostic_Management_Lines_BI__c> getDLs() 
    public List<Requested_Diagnostic_Test_BI__c> getRDTs()
    {
        List<Diagnostic_Management_Lines_BI__c> DLs;
        List<Requested_Diagnostic_Test_BI__c> RDTs;
        set<id> DLines = new set<id>();
        map <id, string> RDTmap = new map <id, string>();
        
        //Add any fields to the select
        DLs = [SELECT Id, 
        			  Name, 
        			  Nr_of_Samples_BI__c, 
               		  Sample_Type_BI__c,
        			  Test_Type_BI__c,
        			  Test_Type_BI__r.Name, 
        			  Test_Type_BI__r.Sample_Type_BI__c,
        			  Test_Type_BI__r.Species_BI__c,
					  Diagnostic_Management_BI__r.Sub_Species_BI__r.Name, 
               		  Comment_BI__c,
               		  Diagnostic_Management_Lab_Test_BI__c
        	   FROM   Diagnostic_Management_Lines_BI__c 
        	   WHERE  Diagnostic_Management_BI__c = :dId];
        
        for (Diagnostic_Management_Lines_BI__c d : DLs)
        {
			DLines.add(d.id);         
        }          
        
	    /*for (Requested_Diagnostic_Test_BI__c r : [select Id, Diagnostic_Management_Lab_Test_BI__r.name,Diagnostic_Mananagement_Line_BI__c from Requested_Diagnostic_Test_BI__c where Diagnostic_Mananagement_Line_BI__c in:DLines]) {
        	RDTmap.put(r.Diagnostic_Mananagement_Line_BI__c, r.Diagnostic_Management_Lab_Test_BI__r.name);
    	}        
        	   
        for (Diagnostic_Management_Lines_BI__c l : DLs)
        {
            l.Diagnostic_Management_Lab_Test_BI__c = RDTmap.get(l.id);
			//string a = RDTmap.get(l.id);						         
        }*/
        RDTs = [SELECT id,
                    Diagnostic_Management_Lab_Test_BI__r.name,
                    Diagnostic_Mananagement_Line_BI__c,
                    Diagnostic_Mananagement_Line_BI__r.Nr_of_Samples_BI__c,
                    Diagnostic_Mananagement_Line_BI__r.Sample_Type_BI__c, 
                    Diagnostic_Mananagement_Line_BI__r.Diagnostic_Management_BI__r.Sub_Species_BI__r.Name,
                    Diagnostic_Mananagement_Line_BI__r.Comment_BI__c
				FROM Requested_Diagnostic_Test_BI__c 
				WHERE Diagnostic_Mananagement_Line_BI__c in:DLines];
        
        //return DLs; 
        return RDTs; 
    }
    
    public List<Requested_Diagnostic_Test_BI__c> getDTs() 
    {
        List<Requested_Diagnostic_Test_BI__c> DTs;
        //Add any fields to the select
        DTs = [SELECT Id, 
        			  Name, 
        			  Diagnostic_Management_Lab_Test_BI__r.name, 
               		  Diagnostic_Mananagement_Line_BI__c
        	   FROM   Requested_Diagnostic_Test_BI__c 
        	   WHERE  Diagnostic_Mananagement_Line_BI__r.Diagnostic_Management_BI__c = :dId];
      	   
        return DTs;
    }
    
    public Address_vod__c getLabDetails()
    {
    	//Id labId = [select Laboratory_BI__c from Diagnostic_Management_Lines_BI__c where Diagnostic_Management_BI__c =: dId limit 1].Laboratory_BI__c;
        Id labId = [select Laboratory_BI__c from Diagnostic_Management_Lines_BI__c where Diagnostic_Management_BI__c =: dId limit 1].Laboratory_BI__c;
    	
    	Address_vod__c labDetails = [select Account_vod__r.Name, Name, City_vod__c, Zip_vod__c, Account_vod__r.Primary_Email_BI__c, Account_vod__r.Phone, Account_vod__r.Fax 
    								 from 	Address_vod__c 
    								 where	Account_vod__c =: labId
    								 order by Primary_vod__c
    								 limit 1];
    	return labDetails;
    }
    
    public Diagnostic_Management_BI__c getDiagDetails()
    {
    	Diagnostic_Management_BI__c diagDetails = [select Id, Name, Request_Date_BI__c, Farm_Name_Location_BI__c, Responsible_BI__r.Name, 
                                                   Account_BI__r.Name, Letter_Goes_To_BI__c
    											   from	  Diagnostic_Management_BI__c
    											   where  Id =: dId
    											   limit  1];
    	return diagDetails;
    }
    
    public Address_vod__c getAccDetails()
    {
    	Id accId = [select Account_BI__c from Diagnostic_Management_BI__c where Id =: dId limit 1].Account_BI__c;
    	
    	Address_vod__c accDetails = [select Account_vod__r.Name, Name, City_vod__c, Zip_vod__c, Account_vod__r.Primary_Email_BI__c, 
                                     Account_vod__r.Phone, Account_vod__r.Fax
    								 from 	Address_vod__c 
    								 where	Account_vod__c =: accId
    								 order by Primary_vod__c
    								 limit 1];
		return accDetails;
    }
    
    public User getRespDetails()
    {
    	Id respId = [select Responsible_BI__c from Diagnostic_Management_BI__c where Id =: dId limit 1].Responsible_BI__c;
    	
    	User respDetails = [select Id, Name, Email, Phone 
    						from   User 
    						where  Id =: respId
    						limit  1];
    						
    	return respDetails;
    }
    
    public User getCreatorDetails()
    {
    	Id creatorId = [select CreatedById from Diagnostic_Management_BI__c where Id =: dId limit 1].CreatedById;
    	
    	User creatorDetails = [select Id, Name, Email, Phone 
    						   from   User 
    						   where  Id =: creatorId
    						   limit  1];
    						   
    	return creatorDetails;
    }

}