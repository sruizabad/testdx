global class VEEVA_BI_Order_Approval_Reminder implements Database.Batchable<SObject>, Schedulable{
	global void execute(SchedulableContext sc) {
      VEEVA_BI_Order_Approval_Reminder b = new VEEVA_BI_Order_Approval_Reminder(); 
      database.executebatch(b,200);
    }
     global VEEVA_BI_Order_Approval_Reminder () {
        system.debug('VEEVA_BI_Order_Approval_Reminder STARTED');
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = '';
        
        query = ' SELECT id, Status_BI__c, Lastmodifiedby.Email, Order_BI__r.Name, Approving_District_Manager_BI__r.Id, Approving_District_Manager_BI__c,Approving_National_Sales_Manager_BI__c, Order_Approval_Criteria_BI__r.First_Treshold_BI__c, Order_Approval_Criteria_BI__r.Second_Threshold_BI__c ';
        query += ' FROM Order_Approval_BI__c WHERE Status_BI__c in  ( \'Pending\',\'Approved by District Manager\' )'; //only pending ones
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> batch) {
        system.debug('VEEVA_BI_Order_Approval_Reminder queried batch size: ' + batch.size());
        list<Order_Approval_BI__c> OAs = (list<Order_Approval_BI__c>) batch;
        //DEFINE WHICH TEMPLATE TO USE HERE
		Id tempid = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'CHC_Approval' LIMIT 1].Id;
		list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
		
		//SEND NOTIF FOR ALL
        for(Order_Approval_BI__c OA : OAs){
        	set<Id> targets = new set<Id>();
        	targets.add(OA.LastmodifiedbyId);
        	if(OA.Approving_District_Manager_BI__c!=null && OA.Status_BI__c == 'Pending') targets.add(OA.Approving_District_Manager_BI__c);//manager
        	if(OA.Approving_National_Sales_Manager_BI__c!=null && OA.Status_BI__c == 'Approved by District Manager') targets.add(OA.Approving_National_Sales_Manager_BI__c);//manager

			for(Id T : targets){
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setTargetObjectId(T);
				//System.debug('Targeted: '+T);
				mail.setSaveAsActivity(false);
				mail.setTemplateID(tempid);
				mail.setReplyTo(OA.Lastmodifiedby.Email);
        		mail.setSenderDisplayName(System.Label.CHC_SenderName);
        		mail.setWhatId(OA.Id);
        		mails.add(mail);
			}
        }
        Messaging.sendEmail(mails);
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('VEEVA_BI_Order_Approval_Reminder FINISHED');
        // That's all folks!
    }
}