public with sharing class VEEVA_BI_Order_Confiramtion_Logo {
    
    public Id orgId;
    public Id logoId;
    public String environmentUrl;
    
    // Get current enviroment url, for host    
    public String getEnvironmentUrl(){
    	
       PageReference currentpageurl = ApexPages.currentPage();
       String orgURL = currentpageurl.getUrl();
 
        environmentUrl = orgURL.split('/')[0];
        
        return environmentUrl;
    }
    
    // Get the logo id for logo called: BI_LOGO_ORDER_CONF
    public Id getLogoId(){
    	
    	Document logo = [SELECT Id, 
                       			DeveloperName                       			
		                        FROM 	Document 
		        			    WHERE 	DeveloperName = 'BI_LOGO_ORDER_CONF'
		        			    limit 1];
		logoId = logo.Id;
		return logoId;     	
    }
    
    // Get the organization id for BI Global
    public Id getOrgId(){
    	
    	/*
    	Organization org = [SELECT 	Id, 
                       				Name                       			
			                        FROM 	Organization 
			        			    WHERE 	Name = 'BI Global'
			        			    limit 1];
		orgId = org.Id;
		*/
		orgId = UserInfo.getOrganizationId();
		return orgId;     
    	
    }
    
}