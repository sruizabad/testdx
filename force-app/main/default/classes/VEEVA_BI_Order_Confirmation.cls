public without sharing class VEEVA_BI_Order_Confirmation 
{
	public Id oId {get;set;}
	
	public List<Order_vod__c> getChildOrders() 
	{
        List<Order_vod__c> childOrders;
        
        childOrders = [SELECT 	Id, 
                       			Name,
                       			Delivery_Date_vod__c,
                       			Delivery_Date_Formatted_BI__c,
                       			Delivery_Location_vod__r.Name
                       FROM 	Order_vod__c 
        			   WHERE 	Parent_Order_vod__c =: oId
        			   AND		Order_Category_BI__c = 'Child Order'
        			   ORDER BY Delivery_Date_vod__c];
        
        system.debug('ERIK CHILD orders: ' + childOrders );
        
        return childOrders;
    }
	
}