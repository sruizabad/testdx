public with sharing class VEEVA_BI_Order_Confirmation_Disclaimer 
{
	public Id OId {get;set;}
	public String disclaimer {get;set;}
	public string disclaimerDefault {get;set;}
	
	public Order_Confirmation_Settings__c disclaimerSetting;
	public Order_Confirmation_Settings__c disclaimerSettingDefault;
	public String disclaimerName;
	public String disclaimerNameDefault;
	public Id profileID;
	public String language;
	Message_vod__c disclaimerRecord = new Message_vod__c();
	Message_vod__c defaultDisclaimerRecord = new Message_vod__c();
	
	
	public VEEVA_BI_Order_Confirmation_Disclaimer()
	{
		disclaimer = '';
		profileId = [select profileid from user where id =: UserInfo.getUserId()].profileid;
		language = [select LanguageLocaleKey from User where id =: UserInfo.getUserId()].LanguageLocaleKey;
		
		disclaimerSetting = Order_Confirmation_Settings__c.getInstance(profileId);
		disclaimerSettingDefault = Order_Confirmation_Settings__c.getOrgDefaults();
		
		disclaimerName = disclaimerSetting.ORDER_CONFIRMATION_DISCLAIMER__c;
		disclaimerNameDefault = disclaimerSettingDefault.ORDER_CONFIRMATION_DISCLAIMER__c;

		
		if(disclaimerName != null)
		{
			//Message_vod__c disclaimerRecord = new Message_vod__c();
            try
            {
                disclaimerRecord = [select id, Text_vod__c from Message_vod__c where Name =: disclaimerName and language_vod__c =: language limit 1];
            }
            catch(Exception ex)
            {
                disclaimerRecord = null;
            }
            
			
			if(disclaimerRecord != null)
				disclaimer = disclaimerRecord.Text_vod__c;
		}
		
		if(disclaimerNameDefault != null && disclaimerRecord == null)
		{
            //Message_vod__c defaultDisclaimerRecord = new Message_vod__c();
            try
            {
                defaultDisclaimerRecord = [select Text_vod__c from Message_vod__c where Name =: disclaimerNameDefault limit 1];
            }    
			catch(Exception ex)
            {
                defaultDisclaimerRecord = null;
            }
			
			if(defaultDisclaimerRecord != null)
				disclaimer = defaultDisclaimerRecord.Text_vod__c;
		}
			
		//if(disclaimer == null || disclaimer == '' || disclaimer.length() == 0)
		//	disclaimer = '';

	}

	
	
}