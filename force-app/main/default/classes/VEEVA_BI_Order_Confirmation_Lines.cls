public with sharing class VEEVA_BI_Order_Confirmation_Lines 
{
    public Id OId {get;set{
                    childOs = [SELECT id, Delivery_Date_Formatted_BI__c, (select  Id, 
                                      Quantity_vod__c, 
                                      Name, 
                                      Product_vod__c, 
                                      Product_vod__r.Name, 
                                      Product_vod__r.UPC_EAN_BI__c,
									  Reason_BI__c,	                                                                          
                                      List_Price_Rule_vod__c, //Attila - CR 713
                                      List_Amount_vod__c, 
                                      Net_Amount_vod__c, 
                                      Line_Discount_vod__c, 
                                      Free_Goods_vod__c ,
                                      Order_vod__r.Name
                              from    Order_Lines_vod__r order by Product_vod__r.Name)
                    FROM  Order_vod__c WHERE Parent_Order_vod__c = :value]; 
                        
                    childOsSize = childOs.size();
                    init();
                    Oid=value;
    
                }} //the ID we send from the calling code to the template, and receive it from there
    public list<BI_ORDER_CONFIRMATION_GRID> childOrdersAndLines {get; set;}
    public set<Id> childOrderIds;
    public List<Order_vod__c> childOs;
    public integer childOsSize {get;set;}
    public id orderID {get;set;}

   
    public VEEVA_BI_Order_Confirmation_Lines()
    {
        
        childOrdersAndLines = new list<BI_ORDER_CONFIRMATION_GRID>();
        childOrderIds = new set<Id>();
        
        orderID = OId;
        /* childOs = [SELECT id,(select  Id, 
                                      Quantity_vod__c, 
                                      Name, 
                                      Product_vod__c, 
                                      Product_vod__r.Name, 
                                      List_Amount_vod__c, 
                                      Net_Amount_vod__c, 
                                      Line_Discount_vod__c, 
                                      Free_Goods_vod__c ,
                                      Order_vod__r.Name
                              from    Order_Lines_vod__r)
                    FROM  Order_vod__c WHERE Parent_Order_vod__c = :OId]; */
                    //FROM  Order_vod__c WHERE Parent_Order_vod__c = 'a1FK0000004RL3FMAW'];
        
        
        
     //   childOsSize = childOs.size();
        
       // init();
    }   
    

    
  
    
    public void init()
    {
    
        for(Order_vod__c o : childOs)
        {
            //system.debug('ERIK');
            BI_ORDER_CONFIRMATION_GRID g = new BI_ORDER_CONFIRMATION_GRID(o);
             
            list<Order_Line_vod__c> oltest = o.Order_Lines_vod__r;
            
            g.addChild(oltest);
            childOrdersAndLines.add(g);  
        }
    }
    
    
    
    /* OLD CODE
    //list to expose the order lines of the Child Order records
    public List<Order_Line_vod__c> getChildOLs() 
    {
        List<Order_Line_vod__c> childOLs;
        set<Id> childOrderIds = new set<Id>();
        
        for(Order_vod__c o : [select id from Order_vod__c where Parent_Order_vod__c =: OId])
        {
            childOrderIds.add(o.Id);
        }
        
        childOLs = [SELECT  Id, 
                            Quantity_vod__c, 
                            Name, 
                            Product_vod__c, 
                            Product_vod__r.Name, 
                            List_Amount_vod__c, 
                            Net_Amount_vod__c, 
                            Line_Discount_vod__c, 
                            Free_Goods_vod__c ,
                            Order_vod__r.Name
                    FROM    Order_Line_vod__c 
                    WHERE   Order_vod__c in: childOrderIds
                    ORDER BY Order_vod__r.Delivery_Date_vod__c];
        
        system.debug('ERIK CHILD ORDER IDs: ' + childOrderIds);
        system.debug('ERIK CHILD ORDER LINES: ' + childOLs);
        
        return childOLs;
    }
    
    */
    
    
    //list to expose the TOTAL orders
    public List<Order_Line_vod__c> getOLs() 
    {
        List<Order_Line_vod__c> OLs;
            
        OLs = [SELECT Id, 
                      Quantity_vod__c, 
                      Name, 
                      Product_vod__c, 
               		  List_Price_Rule_vod__c, //Attila - CR 713
               		  Reason_BI__c,	
                      Product_vod__r.Name, 
                      Product_vod__r.UPC_EAN_BI__c,
                      List_Amount_vod__c, 
                      Net_Amount_vod__c, 
                      Line_Discount_vod__c, 
                      Free_Goods_vod__c,
                      Order_vod__r.Name
               FROM   Order_Line_vod__c 
               WHERE  Order_vod__c = :OId
               ORDER BY Product_vod__r.Name];
            
        return OLs;
     }
    
}