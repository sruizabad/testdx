public with sharing class VEEVA_BI_Order_Confirmation_Logo {
	
	public Id orgId;
    public Id logoId;
    public String environmentUrl;
    
    // Get current enviroment url, for host    
    public String getEnvironmentUrl(){ 
        environmentUrl = System.URL.getSalesforceBaseURL().getHost();
        
        return environmentUrl;
    }
    
    // Get the logo id for logo called: BI_LOGO_ORDER_CONF
    public Id getLogoId(){
    	
    	Document logo = [SELECT Id, 
                       			DeveloperName                       			
                         FROM 	Document 
        			     WHERE 	DeveloperName = 'BI_LOGO_ORDER_CONF'
        			     limit 1];
		logoId = logo.Id;
		return logoId;     	
    }
    
    // Get the organization id for BI Global
    public Id getOrgId(){
    	    	
		orgId = UserInfo.getOrganizationId();
		return orgId;     
    	
    }
    
}