public class VEEVA_BI_RECORD_TYPES {

public static void setRecordTypes(String objName) { 

    Map<String, Id> recordTypeIdByDevName = new Map<String, Id>();
    Map<Id, String> recordTypeNameById = new Map<Id, String>();
    List<RecordType> recordTypes = [SELECT Id, DeveloperName from RecordType where SobjectType = :objName];
    for (RecordType recordType : recordTypes) {
        recordTypeIdByDevName.put(recordType.DeveloperName, recordType.Id);
        recordTypeNameById.put(recordType.Id, recordType.DeveloperName);
    }
    for (Integer i=0; i < Trigger.size; i++) {
        SObject newRecord = Trigger.new[i];
        SObject oldRecord = (Trigger.isInsert ? null : Trigger.old[i]);
        
        if ((Trigger.isInsert && newRecord.get('Record_Type_Name_BI__c') != null) ||
            (Trigger.isUpdate && newRecord.get('Record_Type_Name_BI__c') != oldRecord.get('Record_Type_Name_BI__c'))){
        
            Id recordTypeId = recordTypeIdByDevName.get((String)newRecord.get('Record_Type_Name_BI__c'));
            if (recordTypeId != null) {
                newRecord.put('RecordTypeId', recordTypeId);
            }
        } else if ((Trigger.isInsert && newRecord.get('RecordTypeId') != null) ||
                   (Trigger.isUpdate && newRecord.get('RecordTypeId') != oldRecord.get('RecordTypeId'))) {
            
            String devName = recordTypeNameById.get((String)newRecord.get('RecordTypeId'));
            if (devName != null) {
                newRecord.put('Record_Type_Name_BI__c', devName);                           
            }
        }    
    } 
  }
}