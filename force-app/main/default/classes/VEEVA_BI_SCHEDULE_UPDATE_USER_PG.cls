/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This class is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: scheduler class for VEEVA_BI_BATCH_UPDATE_USER_PG
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
**/
global class VEEVA_BI_SCHEDULE_UPDATE_USER_PG implements Schedulable{
    
    global void execute(SchedulableContext sc){   
        VEEVA_BI_BATCH_UPDATE_USER_PG b = new VEEVA_BI_BATCH_UPDATE_USER_PG(); 
        database.executebatch(b,10); 
    }

}