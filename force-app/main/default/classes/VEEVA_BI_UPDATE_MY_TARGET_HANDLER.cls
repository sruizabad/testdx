/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This class is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: trigger handler class for VEEVA_BI_UPDATE_MY_TARGET trigger
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
**/
public with sharing class VEEVA_BI_UPDATE_MY_TARGET_HANDLER {
    
    public static boolean hasExecuted = false;
    private boolean m_isExecuting = false;
    
    public VEEVA_BI_UPDATE_MY_TARGET_HANDLER(boolean isExecuting){
        m_isExecuting = isExecuting;
    }
    
    list<TSF_vod__c> TFSToUpdate = new list<TSF_vod__c>();
    
    public void updateGroup(map<id, TSF_vod__c> objMap){
        
        system.debug('Attila TFSToUpdate: ' + objMap);      
        if (objMap.size() > 0){
            TFSToUpdate.addAll(objMap.values());
            update TFSToUpdate;  
        }     
    }
}