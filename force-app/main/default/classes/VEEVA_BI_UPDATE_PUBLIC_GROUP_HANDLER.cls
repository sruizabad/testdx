/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This class is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: trigger handler class for VEEVA_BI_UPDATE_USER_PUBLIC_GROUP trigger
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
**/
public with sharing class VEEVA_BI_UPDATE_PUBLIC_GROUP_HANDLER{
    
    public static boolean hasExecuted = false;
    private boolean m_isExecuting = false;
        
    public VEEVA_BI_UPDATE_PUBLIC_GROUP_HANDLER(boolean isExecuting){
        m_isExecuting = isExecuting;
    }

    public void deleteFromGroup(set<id> userSet, set<string> groupSet){
        
        List<GroupMember> listGM = [SELECT Id, GroupId FROM GroupMember where UserOrGroupID IN:userSet AND Group.Name IN:groupSet];
        
        if (!listGM.isEmpty()) delete listGM;
    }
    
    public void insertGroup(map<string, User> objMap){
        
        List<GroupMember> listGroupMember = new List<GroupMember>(); 
        List<Group> groupList = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN:objMap.KeySet()]; 
        system.debug('Attila groupList: ' + groupList);
        for (Group g: groupList){
            User obj = ObjMap.get(g.DeveloperName);
            
            GroupMember gm = new GroupMember(); 
            gm.GroupId = g.id;
            gm.UserOrGroupId = obj.id;
            listGroupMember.add(gm);
        }    
        system.debug('Attila listGroupMember: ' + listGroupMember);
        if (!listGroupMember.isEmpty()) insert listGroupMember;
    }
}