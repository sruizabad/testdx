/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This trigger is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: Extension class - Deep-Clone to clone Vaccincation details as well - Jira: CRM1-569
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
*/
public with sharing class VEEVA_BI_VP_DEEPCLONE_EXTENSION {
    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Vaccination_Program_BI__c vp {get;set;}
    // set the id of the record that is created 
    public ID newRecordId {get;set;}
    public List<Vaccination_Program_Details_BI__c> VPDId = new List<Vaccination_Program_Details_BI__c>{};

    public id VPDIDs;
 
    // initialize the controller
    public VEEVA_BI_VP_DEEPCLONE_EXTENSION(ApexPages.StandardController controller) {
 
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        vp = (Vaccination_Program_BI__c)controller.getRecord();
    }
 
    // method called from the VF's action attribute to clone the po
    public PageReference cloneWithItems() {
     
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Vaccination_Program_BI__c newVP; 
        
        //get all the fields
         try {
            
            Map <String, Schema.SObjectField> Fields = Schema.getGlobalDescribe().get('Vaccination_Program_BI__c').getDescribe().fields.getMap();
            system.debug('Fields size: ' + Fields.size());
            
            String query = 'SELECT ';
            
            for(Schema.SObjectField sfield : Fields.Values()){
                String Fname = sfield.getDescribe().getName();
                //EXCLUDE FIELDS HERE YOU DONT WANT CLONED
                if (Fname == 'Mobile_ID_vod__c') continue;
                query += Fname + ',';

            }
            //Remove last comma
            query = query.removeEnd(',');
            query += ' FROM Vaccination_Program_BI__c WHERE ID = \'' + vp.id + '\' LIMIT 1';
            system.debug('Query: ' + query);
            
            //get the sObject       
            vp = Database.query(query);
            //Clone and insert the parent object
             
            newVP = vp.clone(false);
            //pre-set some values if needed  
            /*newVP.Case_related_to_adverse_event_BI__c ='';
            newVP.Status_BI__c = 'New';
            newVP.Letter_to_Send_BI__c = false;
            newVP.Letter_to_send_to_Lab_BI__c = false;
            newVP.Letter_to_send_to_User_BI__c = false;*/
            insert newVP;
             
            // set the id of the new DM created for further use
            newRecordId = newVP.id;

            //get the fieldlists for them 
            List<String> childs = new List<String>();
                //ADD THE OBJECTS YOU WANT TO HAVE CLONED HERE
                childs.add('Vaccination_Program_Details_BI__c');

            List<sObject> objs =  new List<sObject>();
            List<sObject> temp =  new List<sObject>();
            
            for(String o : childs){
                //sharing doesnt work on sobjects, have to check manually if current user can create it
                if(Schema.getGlobalDescribe().get(o).getDescribe().isCreateable()==false) continue;
                
                Map <String, Schema.SObjectField> ChildFields = Schema.getGlobalDescribe().get(o).getDescribe().fields.getMap();
                system.debug('ChildFields size: ' + ChildFields.size());
                
                query = 'SELECT ';
                for(Schema.SObjectField sfield : ChildFields.Values()){
                    String Fname = sfield.getDescribe().getName();
                    //EXCLUDE FIELDS HERE YOU DONT WANT CLONED FROM CHILD OBJECTS
                        if(Fname == 'Mobile_ID_vod__c') continue;                    
                    
                    query += Fname + ',';
                    
                }
                query = query.removeEnd(',');

                VPDId = [SELECT Id, Name FROM Vaccination_Program_Details_BI__c WHERE Vaccination_Program_BI__c =:vp.id];
                
                query += ' FROM '+ o + ' WHERE Vaccination_Program_BI__c = \'' + vp.Id +'\'';
                system.debug('Query: ' + query);
                temp = Database.query(query);
                system.debug('Temp size: ' + temp.size() + ' for sOBject: ' + o);
                objs.addAll(temp);
                system.debug('objs size: ' + objs.size());
            }
            
            List<sObject> newObjs =  new List<sObject>();
                                                            
            for(sObject so : objs){
                
                sObject newSO = so.clone(false);
                
                String objName = newSO.getSObjectType().getDescribe().getName();
                
                newSO.put('Vaccination_Program_BI__c',newRecordId);
                newObjs.add(newSO);
            }
             
             system.debug('newObjs size: ' + newObjs.size());
             insert newObjs;

         } catch (Exception e){
             // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }

    return new PageReference('/'+newVP.id+'/e?retURL=%2F'+newVP.id);
    }
}