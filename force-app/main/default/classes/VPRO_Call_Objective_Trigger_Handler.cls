public class VPRO_Call_Objective_Trigger_Handler extends VPRO_Trigger_Handler {
/*
 * class: VPRO_DCR_Trigger_Handler 
 * description: The trigger handler for the DCR object
 *
 * history:
 *    - 2016-OCT-13 - Veeva Professional Services (wa) - initial creation
 * 
*/
    private map<Id, Call_Objective_vod__c> m_newMap;
    private map<Id, Call_Objective_vod__c> m_oldMap;
    private list<Call_Objective_vod__c> m_newList;
    
    //constructor
    //set up trigger new list, new Map, old Map, RecordTypeMap, Account and Address schemas    
    public VPRO_Call_Objective_Trigger_Handler () {
        if(trigger.new != null) m_newList = (list<Call_Objective_vod__c>) trigger.new;          
        if(trigger.newMap != null) m_newMap = (map<Id, Call_Objective_vod__c>) trigger.newMap;
        if(trigger.oldMap != null) m_oldMap = (map<Id, Call_Objective_vod__c>) trigger.oldMap;
    }
    
    /***************************************
    * context methods
    ***************************************/
    public override void beforeInsert() {
        populatePlanTactic();
    }
    public override void afterInsert() {
    }
    public override void beforeUpdate() {
        populatePlanTactic();
    }
    public override void afterUpdate() {
    }
    public override void beforeDelete() {
    }
    public override void afterDelete() {    
    }
    public override void afterUndelete() {
    }

    private void populatePlanTactic() {
        Set<Id> accountTacticIds = new Set<Id>();
        for(Call_Objective_vod__c co : m_newList) {
            if(co.Account_Tactic_vod__c != null) {
                accountTacticIds.add(co.Account_Tactic_vod__c);
            }
        }
        List<Account_Tactic_vod__c> accountTactics = [Select Id, Plan_Tactic_vod__c from Account_Tactic_vod__c where Id in :accountTacticIds];
        system.debug(LoggingLevel.INFO, 'Number of Account Tactics: ' + accountTactics.size());
        Map<Id, Id> accountTacticIdToPlanTacticId = new Map<Id,Id>();
        for(Account_Tactic_vod__c at : accountTactics) {
            if(at.Plan_Tactic_vod__c != null) {
                accountTacticIdToPlanTacticId.put(at.id, at.Plan_Tactic_vod__c);
            }
        }

        for(Call_Objective_vod__c co : m_newList) {
            if(co.Account_Tactic_vod__c != null) {
                system.debug(LoggingLevel.INFO, 'Map:' + accountTacticIdToPlanTacticId);
                Id planTactic = accountTacticIdToPlanTacticId.get(co.Account_Tactic_vod__c);
                system.debug(LoggingLevel.INFO, 'Plan Tactic Id is : ' + planTactic);
                if(planTactic != null) {
                    co.Plan_Tactic_vod__c = planTactic;
                }
            }
        }

    }

}