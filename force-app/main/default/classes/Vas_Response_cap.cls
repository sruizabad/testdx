/*
 * Created By:   Garima Agarwal
 * Created Date: 7/17/2014
 * Description:  Incerement the Actual Response and Effective Response once a case is created.                             
 */

 public without sharing class Vas_Response_cap
 {public void updateresponse(List<Case>caselist)
 {list<id>vaslist=new list<id>();
list<vas_transaction_bi__c>vastranlist=new list<vas_transaction_bi__c>();
list<id>vasidlist=new list<id>();
list<vas_transaction_bi__c>updateresponse=new list<vas_transaction_bi__c>();
list<case>updateeffectivecall=new list<case>();

//map<case,list<vas_transaction_Bi__c>>vasmap=new map<case,list<vas_transaction_Bi__c >>();

/* for(Integer i=0; i<vaslist.size(); i++) {
    Id vasid = vaslist.get(i).id;
    vasidlist.add(vasid);
}*/

    for(case c: caselist)
    vaslist.add(c.Coupon_Code__c);
    vastranlist=[select id, Name,Actual_Responses_BI__c,Effective_Responses_BI__c, Account_BI__c,VAS_BI__r.Product_BI__c from Vas_transaction_bi__c where vas_bi__c in: vaslist ];
    system.debug('vastranlist'+vastranlist);
    for(case c: caselist)
        {if(c.Account__c!=null)
            {
             for(vas_transaction_bi__c vastransaction: vastranlist)
                {
                if(vastransaction.Account_BI__c==c.Account__c)
                    {if(vastransaction.Actual_Responses_BI__c==null)
                    vastransaction.Actual_Responses_BI__c=0;
                    if(vastransaction.Effective_Responses_BI__c==null)
                    vastransaction.Effective_Responses_BI__c=0;
                    system.debug('account is same');
                    if(c.Product_MVN__r.Name==vastransaction.VAS_BI__r.Product_BI__c)
                    
                        {system.debug('product is same');
                        {
                        vastransaction.Actual_Responses_BI__c++;
                        c.Effective_Call__c= true;
                        }
                        system.debug('Effective Value='+ c.Effective_Call_cap__c);
                        if(c.Effective_Call_cap__c )
                        {
                        vastransaction.Effective_Responses_BI__c++;
                        c.Effective_Call__c=true;
                        }
                        
                        system.debug('vastransaction.Actual_Responses_BI__c'+vastransaction.Actual_Responses_BI__c);
                        system.debug('vastransaction.Effective_Responses_BI__c'+vastransaction.Effective_Responses_BI__c);
                        
                        }
                    else
                        {system.debug('product is different but account is same');
                       
                        vastransaction.Actual_Responses_BI__c=vastransaction.Actual_Responses_BI__c+1;
                        c.Effective_Call__c= true;                        
                        system.debug('vastransaction.Actual_Responses_BI__c'+vastransaction.Actual_Responses_BI__c);
                        
                        }
                    updateresponse.add(vastransaction);
                    system.debug('reee='+updateresponse);
                    updateeffectivecall.add(c);  
                    system.debug('ceee='+updateeffectivecall);                  
                    }
                }
            }
        }
    try{update updateresponse;
       update updateeffectivecall;
    }
          catch(Exception ae)
      {
        System.debug('Exception is :'+ ae);
      }
    } 
 }