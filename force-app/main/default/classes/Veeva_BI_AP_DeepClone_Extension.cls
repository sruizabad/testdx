public class Veeva_BI_AP_DeepClone_Extension {
    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Account_Plan_vod__c ap {get;set;}
    // set the id of the record that is created 
    public ID newRecordId {get;set;}
 
    // initialize the controller
    public Veeva_BI_AP_DeepClone_Extension(ApexPages.StandardController controller) {
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        ap = (Account_Plan_vod__c)controller.getRecord();
    }
 
    // method called from the VF's action attribute to clone the po
    public PageReference cloneWithItems() {
        Id currentUserId = UserInfo.GetUserId();
 
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Account_Plan_vod__c newAP;
        
        //get all the fields
         try {
            
            Map <String, Schema.SObjectField> Fields = Schema.getGlobalDescribe().get('Account_Plan_vod__c').getDescribe().fields.getMap();
            system.debug('Fields size: ' + Fields.size());
            
            String query = 'SELECT ';
            
            for(Schema.SObjectField sfield : Fields.Values()){
                String Fname = sfield.getDescribe().getName();
                //EXCLUDE FIELDS HERE YOU DONT WANT CLONED
                if(Fname == 'CreatedById' ||
                   Fname == 'CreatedDate' ||
                   Fname == 'CurrencyIsoCode' ||
                   Fname == 'IsDeleted' ||
                   Fname == 'IsLocked' ||
                   Fname == 'LastModifiedById' ||
                   Fname == 'LastModifiedDate' ||
                   Fname == 'SetupOwnerId' ||
                   Fname == 'MayEdit' ||
                   //Fname == 'Name' ||
                   Fname == 'Percent_Complete_vod__c' ||
                   Fname == 'Completed_Desired_Outcome_BI__c' ||
                   Fname == 'Status__c' ||
                   Fname == 'Mobile_ID_vod__c' ||
                   Fname == 'Id' ||
                   Fname == 'SystemModstamp') {
                    continue;
                }
                
                query += Fname + ',';

            }
            //Remove last comma
            query = query.removeEnd(',');
            query += ' FROM Account_Plan_vod__c WHERE ID = \'' + ap.id + '\' LIMIT 1';
            system.debug('Query: ' + query);
            
            //get the sObject       
            ap = Database.query(query);
            //Clone and insert the parent object
            newAP = ap.clone(false);
            newAp.OwnerId = currentUserId;
            insert newAP;
 
            // set the id of the new AP created for further use
            newRecordId = newAP.id;

            //get the fieldlists for them 
            List<String> children = new List<String>();
                //ADD THE OBJECTS YOU WANT TO HAVE CLONED HERE
                children.add('Outcome_BI__c');
                children.add('Drivers_BI__c');
                children.add('Barriers_BI__c');
                children.add('Critical_Success_Factors_BI__c');
                children.add('Additional_Resource_Needed_BI__c');
                children.add('Stakeholders_BI__c');
                //children.add('Account_Tactic_vod__c'); //Attila - BI has requested not to clone these objects - 2018.11.13.
                //children.add('Call_Objective_vod__c'); //Attila - BI has requested not to clone these objects - 2018.11.13.
                children.add('Key_Stakeholder_vod__c');
                children.add('Plan_Tactic_vod__c');
                children.add('Account_Team_Member_vod__c');

            List<sObject> objs =  new List<sObject>();
            List<sObject> temp =  new List<sObject>();
            
            for(String o : children){
                //sharing doesnt work on sobjects, have to check manually if current user can create it
                if(Schema.getGlobalDescribe().get(o).getDescribe().isCreateable()==false) {
                    continue;
                }
                Map <String, Schema.SObjectField> ChildFields = Schema.getGlobalDescribe().get(o).getDescribe().fields.getMap();
                system.debug('ChildFields size: ' + ChildFields.size());
                
                query = 'SELECT ';
                for(Schema.SObjectField sfield : ChildFields.Values()){
                    String Fname = sfield.getDescribe().getName();
                    //EXCLUDE FIELDS HERE YOU DONT WANT CLONED FROM CHILD OBJECTS
                    if(Fname == 'CreatedById' ||
                       Fname == 'CreatedDate' ||
                       Fname == 'CurrencyIsoCode' ||
                       Fname == 'IsDeleted' ||
                       Fname == 'IsLocked' ||
                       Fname == 'LastModifiedById' ||
                       Fname == 'LastModifiedDate' ||
                       Fname == 'SetupOwnerId' ||
                       Fname == 'MayEdit' ||
                       Fname == 'Completed_Date_BI__c' ||
                       Fname == 'Mobile_ID_vod__c' ||
                       Fname == 'Outcome_BI__c' ||
                       //Fname == 'Name' ||
                       Fname == 'Id' ||
                       Fname == 'SystemModstamp') {
                        continue;
                    }     
                    
                    query += Fname + ',';
                }
                query = query.removeEnd(',');

                if (//o == 'Account_Tactic_vod__c' || 
                    o == 'Key_Stakeholder_vod__c' || 
                    //o == 'Call_Objective_vod__c' ||
                    o == 'Plan_Tactic_vod__c' ||
                    o == 'Account_Team_Member_vod__c'
                    ) {
                    query += ' FROM '+ o + ' WHERE Account_Plan_vod__c = \'' + ap.Id +'\'';
                } else {
                    query += ' FROM '+ o + ' WHERE Account_Plan__c = \'' + ap.Id +'\'';
                }
                
                system.debug('Query: ' + query);
                temp = Database.query(query);
                system.debug('Temp size: ' + temp.size() + ' for sOBject: ' + o);
                objs.addAll(temp);
                system.debug('objs size: ' + objs.size());
            } //end for
            
            //Create new deep clone objects
            List<sObject> newObjs =  new List<sObject>();                                 
            for(sObject so : objs){
                sObject newSO = so.clone(false);
                String objName = newSO.getSObjectType().getDescribe().getName();
                if(//objName =='Account_Tactic_vod__c' ||
                   objName == 'Key_Stakeholder_vod__c' ||
                   //objName == 'Call_Objective_vod__c' ||
                   objName == 'Plan_Tactic_vod__c') {
                    newSO.put('OwnerId',currentUserId);
                }

                if (//objName=='Account_Tactic_vod__c' || 
                    objName == 'Key_Stakeholder_vod__c' || 
                    //objName == 'Call_Objective_vod__c' ||
                    objName == 'Plan_Tactic_vod__c' ||
                    objName == 'Account_Team_Member_vod__c') {
                    newSO.put('Account_Plan_vod__c',newRecordId);
                } 
                else {
                    newSO.put('Account_Plan__c',newRecordId);
                }
                newObjs.add(newSO);
            }
            system.debug('newObjs size: ' + newObjs.size());
            if(newObjs.size() > 0) {
                insert newObjs;
            }
            //update references on objects
            Id planTacticId = null;
            //Id accountTacticId = null;
            Id keyStakeHolderId = null;
            Id outcomeId = null;
            for(sObject no : newObjs) {
                String objName = no.getSObjectType().getDescribe().getName();
                if(objName == 'Plan_Tactic_vod__c') {
                    planTacticId = no.Id; 
                }
                /*else if(objName == 'Account_Tactic_vod__c') {
                    accountTacticId = no.Id;
                }*/
                else if(objName == 'Key_Stakeholder_vod__c') {
                    keyStakeHolderId = no.Id;
                }
                else if(objName == 'Outcome_BI__c') {
                    outcomeId = no.Id;
                }
                else {
                    system.debug(LoggingLevel.INFO, 'Not interested in this object. Skipping');
                }
            }//end for
            
            List<sObject> updObjs =  new List<sObject>();
            /*for(sObject no : newObjs) {
                String objName = no.getSObjectType().getDescribe().getName();
                if(objName == 'Account_Tactic_vod__c') {
                    no.put('Plan_Tactic_vod__c',null);
                    //no.put('Outcome_BI__c',outcomeId);
                 	updObjs.add(no);
                }
                else if(objName == 'Call_Objective_vod__c') {
                    //no.put('Plan_Tactic_vod__c',planTacticId);
                    no.put('Plan_Tactic_vod__c',null);
                    no.put('Account_Tactic_vod__c',null);
                    //no.put('Account_Tactic_vod__c',accountTacticId);
                    //if(no.get('Key_Stakeholder_BI__c')!=null) {
                    //    no.put('Key_Stakeholder_BI__c',keyStakeHolderId);
                    //}
                    updObjs.add(no);
                }
                else {
                    system.debug(LoggingLevel.INFO, 'Not interested in this object. Skipping');
                }
            }*/
            if(updObjs.size() > 0) {
                update updObjs;
            }
         } //end try
         catch (Exception e){
             //roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }
        return new PageReference('/'+newAP.id+'/e?retURL=%2F'+newAP.id);
    }
}