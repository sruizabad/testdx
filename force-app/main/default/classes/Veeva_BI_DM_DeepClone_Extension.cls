public with sharing class Veeva_BI_DM_DeepClone_Extension {
    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Diagnostic_Management_BI__c dm {get;set;}
    // set the id of the record that is created 
    public ID newRecordId {get;set;}
    public List<Diagnostic_Management_Lines_BI__c> DMLId = new List<Diagnostic_Management_Lines_BI__c>{};
    public id DMLIDs;
 
    // initialize the controller
    public Veeva_BI_DM_DeepClone_Extension(ApexPages.StandardController controller) {
 
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        dm = (Diagnostic_Management_BI__c)controller.getRecord();
        
    }
 
    // method called from the VF's action attribute to clone the po
    public PageReference cloneWithItems() {
     
         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Diagnostic_Management_BI__c newDM;
        
        //get all the fields
         try {
            
            Map <String, Schema.SObjectField> Fields = Schema.getGlobalDescribe().get('Diagnostic_Management_BI__c').getDescribe().fields.getMap();
            system.debug('Fields size: ' + Fields.size());
            
            String query = 'SELECT ';
            
            for(Schema.SObjectField sfield : Fields.Values()){
                String Fname = sfield.getDescribe().getName();
                //EXCLUDE FIELDS HERE YOU DONT WANT CLONED
                if (Fname == 'Account_BI__c' ||
                    Fname == 'Account_Primary_Email_Bi__c' ||
                    Fname == 'ADE_Number_BI__c' ||
                    Fname == 'Animal_Age_BI__c' ||
                    Fname == 'Animal_Breed_BI__c' ||
                    Fname == 'Animal_Name_BI__c' ||
                    //Fname == 'Case_related_to_adverse_event_BI__c' ||
                    Fname == 'Cause_BI__c' ||
                    Fname == 'Diagnostic_ID_BI__c' ||
                    Fname == 'End_User_BI__c' ||
                    Fname == 'BI_End_User_Email__c' ||
                    Fname == 'Project_ID_BI__c' ||
                    Fname == 'Solution_BI__c' ||
                    Fname == 'Id' ||
                    Fname == 'Status_BI__c' ||
                    Fname == 'Subject_BI__c' ||
                    Fname == 'BI_Date_Letter_Sent_to_Laboratory__c' ||
                    Fname == 'BI_Date_Customer_Informed__c' ||
                    Fname == 'Customer_Informed_BI__c' ||
                    Fname == 'BI_Date_Letter_sent_to_Customer__c') continue;
                query += Fname + ',';

            }
            //Remove last comma
            query = query.removeEnd(',');
            query += ' FROM Diagnostic_Management_BI__c WHERE ID = \'' + dm.id + '\' LIMIT 1';
            system.debug('Query: ' + query);
            
            //get the sObject       
            dm = Database.query(query);
            //Clone and insert the parent object
            newDM = dm.clone(false);
            newDM.Case_related_to_adverse_event_BI__c ='';
            newDM.Status_BI__c = 'New';
            newDM.Letter_to_Send_BI__c = false;
            newDM.Letter_to_send_to_Lab_BI__c = false;
            newDM.Letter_to_send_to_User_BI__c = false;
            insert newDM;
 
            // set the id of the new DM created for further use
            newRecordId = newDM.id;

            //get the fieldlists for them 
            List<String> childs = new List<String>();
                //ADD THE OBJECTS YOU WANT TO HAVE CLONED HERE
                /*childs.add('Outcome_BI__c');
                childs.add('Drivers_BI__c');
                childs.add('Barriers_BI__c');
                childs.add('Critical_Success_Factors_BI__c');
                childs.add('Additional_Resource_Needed_BI__c');
                childs.add('Stakeholders_BI__c');
                childs.add('Account_Tactic_vod__c');*/
                childs.add('Diagnostic_Management_Lines_BI__c');

            List<sObject> objs =  new List<sObject>();
            List<sObject> temp =  new List<sObject>();
            
            for(String o : childs){
                //sharing doesnt work on sobjects, have to check manually if current user can create it
                if(Schema.getGlobalDescribe().get(o).getDescribe().isCreateable()==false) continue;
                
                Map <String, Schema.SObjectField> ChildFields = Schema.getGlobalDescribe().get(o).getDescribe().fields.getMap();
                system.debug('ChildFields size: ' + ChildFields.size());
                
                query = 'SELECT ';
                for(Schema.SObjectField sfield : ChildFields.Values()){
                    String Fname = sfield.getDescribe().getName();
                    //EXCLUDE FIELDS HERE YOU DONT WANT CLONED FROM CHILD OBJECTS
                        if(Fname == 'Laboratory_diagnostic_identifier_BI__c' ||
                           Fname == 'Sample_Group_BI__c' ||
                           Fname == 'Test_Voucher_ID_BI__c' ||
                           Fname == 'Id') continue;                    
                    
                        /*if(Fname == 'CreatedById' ||
                        Fname == 'CreatedDate' ||
                        Fname == 'CurrencyIsoCode' ||
                        Fname == 'IsDeleted' ||
                        Fname == 'IsLocked' ||
                        Fname == 'LastModifiedById' ||
                        Fname == 'LastModifiedDate' ||
                        Fname == 'SetupOwnerId' ||
                        Fname == 'MayEdit' ||
                        Fname == 'Complete_vod__c' ||
                        Fname == 'Completed_Date_BI__c' ||
                        Fname == 'Mobile_ID_vod__c' ||
                        Fname == 'Outcome_BI__c' ||
                        //Fname == 'Name' ||
                        Fname == 'Id' ||
                        Fname == 'Template_BI__c' ||
                        Fname == 'SystemModstamp') continue;*/
                    
                    query += Fname + ',';
                    
                }
                query = query.removeEnd(',');

                DMLId = [SELECT Id, Name FROM Diagnostic_Management_Lines_BI__c 
                         WHERE Diagnostic_Management_BI__c =:dm.id];
                
                /*
                if (o == 'Account_Tactic_vod__c') {
                    query += ' FROM '+ o + ' WHERE Account_Plan_vod__c = \'' + ap.Id +'\'';
                } else {
                    query += ' FROM '+ o + ' WHERE Account_Plan__c = \'' + ap.Id +'\'';
                }*/
                
                query += ' FROM '+ o + ' WHERE Diagnostic_Management_BI__c = \'' + dm.Id +'\'';
                system.debug('Query: ' + query);
                temp = Database.query(query);
                system.debug('Temp size: ' + temp.size() + ' for sOBject: ' + o);
                objs.addAll(temp);
                system.debug('objs size: ' + objs.size());
                /*if (O.Payment_vod__c != O.Payment_Rule_vod__c)
                    approvalNeeded = true;

                   for (Order_Approval_Criteria_BI__c OACc : OACs) {
                    if (OAcc.Order_Record_Type_BI__c == ORTName ) {
                        OAC = OACc;
                        OACfound = true;
                        if (Trigger.isBefore) {
                            System.Debug('CSABa call Viktor, CSABA Call Viktor  Hello Mello');
                            O.Approval_Status_BI__c = 'Pending';
                        }
                    }
                }

                */
            }
            
            List<sObject> newObjs =  new List<sObject>();
                                                            
            for(sObject so : objs){
                
                sObject newSO = so.clone(false);
                
                String objName = newSO.getSObjectType().getDescribe().getName();
                /*if (objName=='Account_Tactic_vod__c') {
                    newSO.put('Account_Plan_vod__c',newRecordId);
                } else {
                    newSO.put('Account_Plan__c',newRecordId);
                }*/
                
                newSO.put('Diagnostic_Management_BI__c',newRecordId);
                newObjs.add(newSO);
            }
             
             system.debug('newObjs size: ' + newObjs.size());
             insert newObjs;
             
             integer i = 0;
             
             for(sObject so : newObjs){
                 //DMLIDs.add(so.id);
                 //DMLIDs = so.id;
                 if (newObjs.size() > 0 && DMLId.size() > 0) {
                     cloneGrandChild(so.id, DMLId[i].id);
                     i++;
                 }
            }

         } catch (Exception e){
             // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }

        return new PageReference('/'+newDM.id+'/e?retURL=%2F'+newDM.id);

    }
    
    
    //Attila 2017.02.20.
    public void cloneGrandChild(Id DMLIDs, id DMLcloneId) {
        
        String grandChild = 'Requested_Diagnostic_Test_BI__c';

        List<sObject> objsGrand =  new List<sObject>();
        List<sObject> temp =  new List<sObject>();
        
        if(Schema.getGlobalDescribe().get(grandChild).getDescribe().isCreateable()==false) return;
        
        Map <String, Schema.SObjectField> ChildFields = Schema.getGlobalDescribe().get(grandChild).getDescribe().fields.getMap();
        system.debug('ChildFields size: ' + ChildFields.size());
        
        string query = 'SELECT ';
        for(Schema.SObjectField sfield : ChildFields.Values()){
            String Fname = sfield.getDescribe().getName();
            //EXCLUDE FIELDS HERE YOU DONT WANT CLONED FROM CHILD OBJECTS
            if(Fname == 'External_ID_BI__c' ||
               Fname == 'Id') continue;
            
            query += Fname + ',';
        }
        query = query.removeEnd(',');
        
        //query += ' FROM '+ grandChild + ' WHERE Diagnostic_Mananagement_Line_BI__r.Diagnostic_Management_BI__c = \'' + dm.id +'\'';
        query += ' FROM '+ grandChild + ' WHERE Diagnostic_Mananagement_Line_BI__c = \'' + DMLcloneId +'\'';
        temp = Database.query(query);
        objsGrand.addAll(temp);
        
        List<sObject> newObjs =  new List<sObject>();
        
        for(sObject so : objsGrand){
            sObject newSO = so.clone(false);
            
            String sObjName = so.getSObjectType().getDescribe().getName();
            
            newSO.put('Diagnostic_Mananagement_Line_BI__c',DMLIDs);
            
            newObjs.add(newSO);
        }
        
        insert newObjs;        
    }   
    
            
}