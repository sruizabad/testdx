public class WS_RequestJsonDeserializator {
    
    public class Ws_request {
        public Boolean allowPartialSuccess;
        public String batchSize;
        public String division;
        public String delimiter;
        public String description;
        public String newInterfaceName;
        public String numberOfFields;
        public String selectedObject;
        public String operation;
        public String requestedBy;
        public String countryTeam;
        public String sfOrg;
        public String sourceInterfaceName;
        public String status;
        public String textQualifier;
        public String requestType;
        public List<Ws_mapping> ws_mappings;
    }

    public class Ws_mapping {
        public String columnName;
        public String dataType;
        public String developerName;
        public String externalKey;
        public String label;
        public String referenceType;
        public String relatedTo;
        public Boolean required;
    }
    
    public static Ws_request parse(String json) {
        return (Ws_request) System.JSON.deserialize(json, Ws_request.class);
    }
}