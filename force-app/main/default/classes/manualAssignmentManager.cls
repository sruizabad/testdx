global class manualAssignmentManager implements Database.Batchable<Sobject> {
    
    global Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id FROM BI_TM_Account_To_Territory__c WHERE BI_TM_Country_Code__c = 'US' AND BI_TM_End_Date__c = 2017-09-15]);
    }
    global void execute(Database.BatchableContext BC, list<BI_TM_Account_To_Territory__c> scope) {
        for(BI_TM_Account_To_Territory__c ma : scope){
            ma.BI_TM_End_Date__c = Date.newInstance(2017, 12, 31);
        }
        Database.update(scope,false);
    }
    global void finish(Database.BatchableContext BC) {
    }
}