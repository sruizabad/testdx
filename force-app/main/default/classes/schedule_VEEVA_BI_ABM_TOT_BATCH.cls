global class schedule_VEEVA_BI_ABM_TOT_BATCH implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {   
        VEEVA_BI_ABM_TOT_BATCH b = new VEEVA_BI_ABM_TOT_BATCH(); 
        database.executebatch(b,10); 
    }
}