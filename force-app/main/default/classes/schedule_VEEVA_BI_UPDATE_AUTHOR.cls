/**
* @description: scheduler class for VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG batch class
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
*/
global class schedule_VEEVA_BI_UPDATE_AUTHOR implements Schedulable{
	
    global void execute(SchedulableContext sc){   
        VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG b = new VEEVA_BI_BATCH_UPDATE_AUTHOR_FLAG(); 
        database.executebatch(b,100); 
    }
    
}