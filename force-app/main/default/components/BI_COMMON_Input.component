<apex:component >
     <script type="text/x-template" id="vf-input">
        <span :class="containerClass">
            <label v-if="!childrenHandleLabel && label !== null" :class="theme.label" :for="elementId">{{label}}</label>
            <template :is="controlTag" :class="controlClass">
                <template v-if="schema !== undefined || type !== null">
                    <input :class="theme.input" v-if="(schema && schema.type == 'string') || type == 'text'" type="text" :value="value" @input="$emit('input', $event.target.value)" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>

                    <input :class="theme.input" v-else-if="(schema && schema.type == 'number') || type == 'number'" type="text" :value="value" @input="$emit('input', parseInt($event.target.value))" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>
                    
                    <input :class="theme.select" v-else-if="(schema && schema.type == 'int')" type="number" :value="value" @input="$emit('input', parseInt($event.target.value))" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>

                    <input :class="theme.input" v-else-if="(schema && schema.type == 'double') || type == 'double'" type="number" step="0.01" :value="value" @input="$emit('input', parseFloat($event.target.value))" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>

                    <input :class="theme.input" v-else-if="(schema && schema.type == 'currency') || type == 'currency'" type="text" :value="value" @input="$emit('input', parseFloat($event.target.value))" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>


                    <input :class="theme.input" v-else-if="(schema && schema.type == 'phone') || type == 'phone'" type="tel" :value="value" @input="$emit('input', $event.target.value)" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>

                    <input :class="theme.input" v-else-if="(schema && schema.type == 'url') || type == 'url'" type="url" :value="value" @input="$emit('input', $event.target.value)" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>

                    <template v-if="supportsDateTypes">
                        <input :class="theme.input" v-if="(schema && schema.type == 'date') || type == 'date'" type="date" :value="auxDate" @input="$emit('input', parseDate($event.target.value))" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>
                        <input :class="theme.input" v-else-if="(schema && schema.type == 'datetime') || type == 'datetime'" type="datetime-local" :value="auxDateTime" @input="$emit('input', parseDateTime($event.target.value))" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>
                    </template>


                    <template v-if="!supportsDateTypes">
                        <input :class="theme.input" class="vf-input-datepicker" v-if="(schema && schema.type == 'date') || type == 'date'" type="text" :value="auxDate" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>
                        <input :class="theme.input" class="vf-input-datepicker" v-else-if="(schema && schema.type == 'datetime') || type == 'datetime'" type="text" :value="auxDateTime" :placeholder="placeholder" :disabled="disabled" :id="elementId"/>
                    </template>

                    <span v-else-if="(schema && schema.type == 'id') || type == 'id'" >{{value}}</span>

                    <template v-else-if="(schema && schema.type == 'reference') || type == 'reference'">
                        <vf-lookup   
                            :sobject="schema.referenceTo[0]" 
                            :as-dropdown="asDropdown"
                            fields="Id, Name"
                            v-model="auxObject"
                            :query-on-create="true"
                            :styleclass="theme.input"
                            :buttonstyleclass="theme.button.primary"
                            :disabled="disabled" :id="elementId">                                   
                        </vf-lookup>
                    </template>

                    <template v-else-if="(schema && schema.type == 'textarea') || type == 'textarea'">
                        <textarea v-model="value" :class="theme.input" :placeholder="placeholder" :disabled="disabled" :id="elementId"></textarea>
                    </template>

                    <select :class="theme.select" v-else-if="(schema && schema.type == 'picklist') || type == 'picklist'" :value="value" @input="$emit('input', $event.target.value)" :disabled="disabled" :id="elementId">
                        <option :value="null" :selected="value == null" v-if="placeholder != null"> {{placeholder}} </option>
                        <option v-for="option in picklistValues" :value="option.value" selected="option.value == value">
                            {{option.label}}
                        </option>
                    </select>

                    <template  v-else-if="(schema && schema.type == 'multipicklist')" >
                        <vf-multi-select :options="picklistValues" :value="value"  @input="$emit('input', $event)" value-separator=";" :label="label"/>
                    </template>

                    
                    <span v-if="(schema && schema.type == 'boolean') || type == 'checkbox'" :class="theme.checkbox">
                        <input type="checkbox" :id="uid" :value="value" :checked="value" @input="$emit('input', $event.target.checked)">
                        <!--SLDS required -->
                        <label class="slds-checkbox__label" :for="uid">
                            <span class="slds-checkbox_faux"></span>
                            <span v-if="label !== null" class="slds-form-element__label">{{label}}</span>
                        </label>

                    </span>

                </template>
                <template v-else-if="field !== undefined && schema === null">
                    <vf-svg-icon path="utility/puzzle" /> {{labels.schemaNotFound}}
                </template>
                <template v-else-if="field === '' && type === null">
                    <vf-svg-icon path="utility/puzzle" /> Invalid type
                </template>
            </template :is="controlTag">
        </span>
    </script>

    <script>

        VueForce.VfInput = {
                // propDoc
                name : 'VfInput',
                introduction: 'General purpose input component',
                description: ``,

                template : '#vf-input',
                props : {
                    value : {
                        type : [Array, Object, String, Number, Boolean],
                        default : null,
                        note : 'v-model'
                    },
                    field : {
                        type : String,
                        default : '',
                        note : 'Full API name of the Salesforce field. Ex. "Account.Name"'
                    },
                    type : {
                        type : String,
                        default : null,
                        note : 'HTML-5 type. Required if \'field \' is empty'
                    },
                    placeholder : {
                        type : String,
                        default : null,
                        note : 'Input placeholder'
                    },
                    notInclude : {
                        type : Array, 
                        default : null,
                        note : 'For picklist/select field types, API names of values to exclude from available options'
                    },
                    asDropdown : {
                        type : Boolean, 
                        default : false,
                        note : 'For reference (lookup, master-detail) field types, whether the lookup should display as SLDS dropdown'
                    },
                    format : {
                        type : String, 
                        default : 'timestamp',
                        note : 'For date field types, default output format'
                    },
                    disabled : {
                        type : Boolean, 
                        default : false,
                        note : 'Whether the input is disabled'
                    },
                    label : {
                        type : String, 
                        default : null,
                        note : 'Label of the input. This will cause some CSS elements to apply.'
                    }
                },
                created : function() {
                    var datefield = document.createElement("input");
                    datefield.setAttribute("type", "date");
                    if (datefield.type != "date") {
                        this.supportsDateTypes = false;
                    }
                },
                data : function() {
                    return {
                        uid : this._uid,
                        //Auxiliar object to be used in lookups
                        auxObject : {},

                        auxValue : null,
                        auxDate : null,
                        auxDateTime : null,
                        supportsDateTypes : true
                    }
                },
                mounted : function() {
                    var self = this;
                    if(this.objectName) this.$vfstore.dispatch('getSchemaDefinition', this.objectName);
                    //Set auxiliar object
                    if(this.schema && this.schema.type == 'reference' && this.value) {
                        //Query to get the name of the related record. 
                        //TODO: Handle polimorphism
                        if(this.schema.referenceTo){
                            _.each(this.schema.referenceTo, function(ref){
                                if(ref == 'Group'){
                                    return
                                } else {

                                    BI_COMMON_VueForceController.getRecords('SELECT Id, Name FROM ' + ref + ' WHERE Id = \''+ self.value +'\' LIMIT 1' , null, null, null, 1, 200, null, false, false, false, function(response, event) {
                                        self.auxObject = response.records[0];
                                    }, { escape : false });
                                }
                            });
                        }
                        // BI_COMMON_VueForceController.getRecords('SELECT Id, Name FROM ' + this.schema.referenceTo[0] + ' WHERE Id = \''+ this.value +'\' LIMIT 1' , null, null, null, 1, 200, null, false, false, false, function(response, event) {
                        //     self.auxObject = response.records[0];
                        // }, { escape : false });
                    }
                    //set auxiliar value (for dates and datetimes)
                    if(this.value){
                        this.auxDate = JSON.parse(JSON.stringify(this.value));
                    }

                    this.fixIEDates();
                },
                methods : {
                    parseDate : function(rawDate) {
                        return this.format == 'locale' 
                            ? new Date(rawDate).toLocaleDateString(this.userLocaleForDate)
                            : this.formatDate(rawDate);
                    },
                    parseDateTime : function(rawDatetime){
                        return this.format == 'locale' 
                            ? new Date(rawDatetime).toLocaleDateString(this.userLocaleForDate) + ' ' + new Date(rawDatetime).toLocaleTimeString(this.userLocaleForDate)
                            : this.formatDate(rawDatetime);
                    },
                    formatDate : function(rawDate) {
                        var parsed;
                        switch (this.format) {
                            case 'timestamp':
                                parsed = new Date(rawDate).getTime();
                                break;
                            case 'locale':
                                parsed = new Date(rawDate).toLocaleDateString();
                                break;
                            case 'iso':
                                parsed = new Date(rawDate).toISOString();
                                break;
                            case 'utc':
                                parsed = new Date(rawDate).toUTCString();
                                break;
                            default:
                                parsed = new Date(rawDate).getTime();
                                break;
                        }
                        return parsed;
                    },
                    getRecord : function() {
                        return JSON.parse(JSON.stringify(this.auxObject));
                    },
                    formatToInput : function(date) {
                      var yyyy = date.getUTCFullYear().toString();
                      var mm = (date.getUTCMonth()+1).toString();
                      var dd  = date.getUTCDate().toString();

                      var mmChars = mm.split('');
                      var ddChars = dd.split('');
                      return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
                    },

                    fixIEDates : function(){

                       if(!this.supportsDateTypes) {
                            if(!jQuery){
                                throw 'Is necessary use JQuery'
                            }
                            if ( this.schema && this.schema.type == 'date' || type == 'date' ) {
                                var inputElement = $(this.$el.querySelector('input'));
                                inputElement.datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
                                var self = this;
                                inputElement.change(function(elem) {
                                    if(elem.target.value && elem.target.value != ""){
                                        self.$emit('input', self.parseDate(elem.target.value));
                                    }else{
                                        self.$emit('input', null);
                                    }
                                });
                                inputElement.keydown(function(e) {
                                    return false;
                                });
                            }
                        
                       }
                    }
                },
                watch : {
                    auxObject : function(newValue) {
                        console.log('auxObject watcher');
                        if(newValue)
                            this.$emit('input', newValue.Id);
                        else 
                            this.$emit('input', undefined);

                    },

                    value : function(newValue){

                        if(this.auxDate != newValue) {
                            if(newValue == null){
                                this.auxDate = null;
                            }else{
                                this.auxDate = this.formatToInput(new Date(newValue));
                            }
                        }
                    },

                    schema : function(){
                        this.$nextTick(function(){
                            this.fixIEDates();
                        });
                    }
                },
                computed : {
                    elementId : function(){
                        //Change this form accesibility
                        return this.id ? this.id : this._uid+'-vf-input';
                    },
                    childrenHandleLabel : function() {
                        return (this.schema && this.schema.type == 'multipicklist') || this.type == 'checkbox';
                    },

                    controlTag : function(){
                        return this.label !== null ? 'div' : 'span';
                    },

                    containerClass : function(){
                        return this.label !== null ? this.theme.element : '';
                    },
                    controlClass : function(){
                        return this.label !== null ? this.theme.control : '';
                    },
                    objectName : function(){
                        return (this.field != '' ? this.field.split('.')[0] : '');
                    },
                    schema : function(){
                        var sch = this.$vfstore.getters.getSchemaFor(this.field);
                        return sch ? sch : null;
                    },
                    theme : function() {
                        return this.$vfstore.getters.getTheme;
                    },
                    labels : function() {
                        return this.$vfstore.getters.getLabels;
                    },
                    userLocaleForDate : function() {
                        var u = this.$vfstore.getters.getUserInfo;
                        return u.locale.split('_')[0];
                    },

                    userDateLocale : function() {
                        var u = this.$vfstore.getters.getUserInfo;
                        return u.dateLocale;
                    },

                    //For picklist fields
                    picklistValues : function(){
                        if(this.schema.type == 'picklist' || this.schema.type == 'multipicklist') {
                            if(this.notInclude != null) {
                                var self = this;
                                return this.schema.picklistValues.filter(function(plValue) {
                                    return self.notInclude.indexOf(plValue.value) === -1;
                                });
                            } else {
                                return this.schema.picklistValues;
                            }
                        }
                    }
                }
                
        };

        Vue.component('vf-input', VueForce.VfInput);

    </script>

</apex:component>