<apex:component >
     <script type="text/x-template" id="vf-multi-select">
        <div class="slds-form-element" role="group" aria-labelledby="picklist-group-label">
		  <span v-if="label !== null" id="picklist-group-label" class="slds-form-element__label slds-form-element__legend">{{label}}</span>
		  <div class="slds-form-element__control">
		    <div class="slds-dueling-list">
		      <div class="slds-assistive-text" id="drag-live-region" aria-live="assertive"></div>
		      <div class="slds-assistive-text" id="option-drag-label">Press space bar when on an item, to move it within the list. CMD plus left and right arrow keys, to move items between lists.</div>
		      <div class="slds-dueling-list__column">
		        <span class="slds-form-element__label" id="label-1">{{leftLabel}}</span>
		        <div class="slds-dueling-list__options">
		          <ul aria-describedby="option-drag-label" aria-labelledby="label-1" aria-multiselectable="true" class="slds-listbox slds-listbox_vertical" role="listbox">
		            <li role="presentation" class="slds-listbox__item" v-for="option in leftOptions" v-on:click="toggleSelect(option)">
		              <div class="slds-listbox__option slds-listbox__option_plain slds-media slds-media_small slds-media_inline" :aria-selected="option.selected" draggable="true" role="option" tabindex="0">
		                <span class="slds-media__body">
                          <span class="slds-truncate" :title="option.label">{{option.label}}</span>
		                </span>
		              </div>
		            </li>
		          </ul>
		        </div>
		      </div>
		      <div class="slds-dueling-list__column">

		        <button class="slds-button slds-button_icon slds-button_icon-container" title="Move Selection to Second Category" v-on:click="moveToRight">
		        	<vf-svg-icon path="utility/right" size="button"/>
		            <span class="slds-assistive-text">Move Selection to Second Category</span>
		        </button>
		        <button class="slds-button slds-button_icon slds-button_icon-container" title="Move Selection to First Category" v-on:click="moveToLeft">
		        	<vf-svg-icon path="utility/left" size="button"/>
		          <span class="slds-assistive-text">Move Selection to First Category</span>
		        </button>
		      </div>
		      <div class="slds-dueling-list__column">
		        <span class="slds-form-element__label" id="label-2">{{rightLabel}}</span>
		        <div class="slds-dueling-list__options">
		          <ul aria-describedby="option-drag-label" aria-labelledby="label-2" aria-multiselectable="true" class="slds-listbox slds-listbox_vertical" role="listbox">
		            
		            <li role="presentation" class="slds-listbox__item" v-for="option in rightOptions" v-on:click="toggleSelect(option)">
		              <div class="slds-listbox__option slds-listbox__option_plain slds-media slds-media_small slds-media_inline" :aria-selected="option.selected" draggable="true" role="option" tabindex="0">
		                <span class="slds-media__body">
		                  <span class="slds-truncate" :title="option.label">{{option.label}}</span>
		                </span>
		              </div>
		            </li>
		          </ul>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
    </script>

    <script>

        VueForce.VfMultiSelect = {
                // options
                template : '#vf-multi-select',
                props : {
                    value : {type : [Array, String], default : null,  note : 'v-model - "input" will be always emitted with right options '},
                    placeholder : {type : String, default : '', note : 'Input placeholder'},
                    options : {type : Array, default : null, note : 'All options available' },
                    // selectedOptions : {type : Array, default : null, note : 'Selected options. Synced.' },
                    label : {type : String, default : null, note : 'Input label'},
                    leftLabel : {type : String, default : 'Available', note : 'Available options label'},
                    rightLabel : {type : String, default : 'Selected', note : 'Selected options label'},
                    disabled : {type : Boolean, default : false, note : 'Whether both inputs and buttons should be disabled'},
                    moveOnClick : {type : Boolean, default : false, note : 'Whether should select or deselect right after clicking on the item'},
                    valueSeparator : {type : String, default : null, note : 'If v-model is passed as string, indicate values separator'},
                },
                created : function() {
                    
                },
                data : function() {
                    return {

                        leftOptions : [],
                        rightOptions : [],

                        auxValue : null,
                        auxDate : null,
                        auxDateTime : null,
                        supportsDateTypes : true
                    }
                },
                mounted : function() {
                    var self = this;
                    if(this.leftOptions.length == 0 && this.options && this.options.length > 0) 
                        this.leftOptions = JSON.parse(JSON.stringify(this.options));
                    // this.$vfstore.dispatch('getSchemaDefinition', this.objectName);

                },
                methods : {
                   getOptionsFromValues : function(values) {
                    var self = this;
                      var splitValue = values.split(this.valueSeparator);
                      var opts = [];
                        _.each(splitValue, function(spV) {
                            if(spV === '') return;

                            _.each(self.options, function(opt) {
                                if(spV === opt.value) {
                                    opts.push(opt);
                                    return;
                                }
                            });
                        });
                        return opts;
                   },
                   getValueStringFromOptions : function(values) {
                     if(!values) return '';
                      var self = this;
                      var opts = ''+this.valueSeparator;

                        _.each(values, function(opt) {
                            opts += (opt.value + self.valueSeparator);
                        });
                        return opts;
                   },
                   toggleSelect : function(opt) {
                      var sel = !opt || !opt.selected || opt.selected === false ? true : false;
                      this.$set(opt, 'selected', sel);
                      this.handleInput();
                      
                   },

                   moveToLeft : function(opt) {
                        var self = this;
                        if(this.selectedRightOptions && this.selectedRightOptions.length > 0) {
                            _.each(this.selectedRightOptions, function(v){
                                self.$set(v, 'selected', false);
                                self.leftOptions.push(v);
                                var idx = _.findIndex(self.rightOptions, function(lv){ return lv.value === v.value});
                                if(idx !== -1) self.rightOptions.splice(idx, 1);
                            });
                        }                       
                        this.handleInput();
                        
                   },
                   moveToRight : function(opt) {
                        var self = this;
                        if(this.selectedLeftOptions && this.selectedLeftOptions.length > 0) {
                            _.each(this.selectedLeftOptions, function(v){
                                self.$set(v, 'selected', false);
                                self.rightOptions.push(v);
                                var idx = _.findIndex(self.leftOptions, function(lv){ return lv.value === v.value});
                                if(idx !== -1) self.leftOptions.splice(idx, 1);
                            });
                        }

                        this.handleInput();
                   },

                   handleInput : function() {
                        if(this.valueSeparator) {
                            this.$emit('input', this.getValueStringFromOptions(this.rightOptions));
                        } else {
                            this.$emit('input', this.rightOptions)
                        }
                   },

                   getLeftOptions : function(valueOptions) {
                        var opts = [];
                        var self = this;
                        _.each(this.options, function(opt) {
                            var match = false;
                            if(valueOptions && valueOptions.length) {
                                _.each(valueOptions, function(val){
                                    if(val.value === opt.value) {
                                        match = true;
                                        return;
                                    }
                                });
                            }
                            if(!match){
                                opts.push(_.cloneWith(opt, function(val) { self.$set(val, 'selected', val.selected ? val.selected : false); return val; }));
                            } 
                        });

                        return opts;
                   }



                },
                watch : {
                   value : {
                        deep : true,
                        handler : function(nv, ov) {
                            if(nv && (!ov || nv.length !== ov.length)) {
                                if(this.valueSeparator){
                                    this.rightOptions = this.getOptionsFromValues(nv);
                                } else {
                                    this.rightOptions = JSON.parse(JSON.stringify(nv));
                                }
                                this.leftOptions = this.getLeftOptions(this.rightOptions);
                            }
                        },
                        immediate : true
                   }
                },
                computed : {
                    objectName : function(){
                        return (this.field != '' ? this.field.split('.')[0] : '');
                    },
                    schema : function(){
                        return this.$vfstore.getters.getSchema;
                    },
                    theme : function() {
                        return this.$vfstore.getters.getTheme;
                    },
                    labels : function() {
                        return this.$vfstore.getters.getLabels;
                    },
                    userLocaleForDate : function() {
                        var u = this.$vfstore.getters.getUserInfo;
                        return u.locale.split('_')[0];
                    },

                    userDateLocale : function() {
                        var u = this.$vfstore.getters.getUserInfo;
                        return u.dateLocale;
                    },

                    selectedLeftOptions : function() {
                        return this.leftOptions.filter(function(opt) {
                            return opt.selected === true;
                        });
                    },
                    selectedRightOptions : function() {
                        return this.rightOptions.filter(function(opt) {
                            return opt.selected === true;
                        });
                    }

                    //For picklist fields
                    // picklistValues : function(){
                    //     if(this.schema.type == 'picklist') {
                    //         if(this.notInclude != null) {
                    //             var self = this;
                    //             return this.schema.picklistValues.filter(function(plValue) {
                    //                 return self.notInclude.indexOf(plValue.value) === -1;
                    //             });
                    //         } else {
                    //             return this.schema.picklistValues;
                    //         }
                    //     }
                    // }
                }
                
        };

        Vue.component('vf-multi-select', VueForce.VfMultiSelect);

    </script>

</apex:component>