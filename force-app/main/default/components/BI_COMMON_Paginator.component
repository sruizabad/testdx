<apex:component >

    <style>
        
        div.vf-paginator-controls {
            display: flex;
            margin-right: 1.2em;
            margin-left: 1.2em;
            align-items: center;
        }

        div.vf-paginator-controls .displaying {
            flex: 1;
        }
        div.vf-paginator-controls .buttons {
            flex: 3;
            text-align : center !important
        }
        div.vf-paginator-controls .show {
            flex: 1;
            text-align : right !important;
            display: flex !important;
            align-items: center;
        }

        div.vf-paginator-controls .show>label {
            flex: 2;
            margin-right: 1.2em;
        }
        div.vf-paginator-controls .show>select {
            flex: 1;
        }

        .vf-record-selector {
            max-width: 75px;
            display: inline-block !important;
        }

        ul.inline-list {
            display: inline-block;
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

        ul.inline-list>li {
            display: inline;
        }

        ul.inline-list>.active>a, ul.inline-list>.active>a:focus, ul.inline-list>.active>a:hover, ul.inline-list>.active>span, ul.inline-list>.active>span:focus, ul.inline-list>.active>span:hover {
            z-index: 3;
            color: #fff !important;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7;
        }

        ul.inline-list>li>a, ul.inline-list>li>span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

    </style>

    <script type="text/x-template" id="vf-paginator">
        <div  class="vf-paginator-controls" v-if="showControls">
            <div class="displaying" v-show="showDisplayingMessage">
                {{labels.displaying}} {{pageMessage}}
            </div>
            <div class="displaying" v-show="showDisplayTotalRecords">
                {{labels.totalRecords}} ({{value.totalRecords}})
            </div>

            <div class="buttons" >
                <ul class="inline-list" style="margin-top: 0px !important">
                    <li class="previous" v-bind:class="{ disabled : value.currentPage <= 1}" role="button">
                        <a v-on:click="previousPage()">{{labels.previous}}</a>
                    </li>

                    <template v-for="pageNumber in totalPages" v-if="showPageNumbers">
                        <li v-bind:class="{active : pageNumber == value.currentPage}"
                            v-on:click="goToPage(pageNumber)"
                            class=""
                            v-if="totalPages <= 5 
                                || (value.currentPage == pageNumber || pageNumber == 1 || pageNumber == 2 || pageNumber == totalPages || pageNumber == totalPages - 1)" role="button">
                                <a v-on:click="value.currentPage = pageNumber">{{pageNumber}}</a>
                        </li>
                        <li v-else-if="totalPages > 5 && (pageNumber == totalPages - 2 || pageNumber == 3)" role="button">
                            <a>...</a>
                        </li>
                    </template>
                    
                    <li class="next" v-bind:class="{ disabled : value.currentPage >= totalPages}" role="button">
                        <a v-on:click="nextPage()">{{labels.next}}</a>
                    </li>
                </ul>
            </div>
            <div class="show" v-if="showPageSizeSelector">
                <label>
                    {{labels.show}}
                </label>
                <select v-bind:class="theme.select + ' vf-record-selector'" v-model.number="value.pageSize" >
                    <option v-for="item in items" v-bind:value="item">{{item}}</option>
                    <!-- <option value="2">2</option>
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option> -->
                </select>
            </div>
            <div class="show" v-if="!showPageSizeSelector">
            </div>
        </div>
    </script>

    <script>
        VueForce.VfPaginator = {
            template : '#vf-paginator',
            props : {
                recordsToPaginate : {type : [Array, Object], default : null, note : 'Full collection of items to paginate'},
                showAllRecords : {type : Boolean, default : false, note : 'Ig paginator should retrun all records or not'},
                showControls : {type : Boolean, default : true, note : 'Whether the bottom bar with controls should be displayed'},
                showPageNumbers : {type : Boolean, default : true, note : 'Whether the page number navigation buttons should be displayed'},
                showDisplayingMessage : {type : Boolean, default : true, note : 'Whether the page message should be displayed'},
                showDisplayTotalRecords : {type : Boolean, default : false, note : 'Whether to show the collection total counter'},
                showPageSizeSelector : {type : Boolean, default : true, note : 'if true, page size selector will be displayed'},
                extraOptions : {type : [Number, Array], default: null, note : 'A number or an array of numbers with new values to add to the paginator'},
                defaultOptionValue : {type: Number, default: null, note : 'Default page size'},

                value : { 
                    type : Object, 
                    default : function() {
                        return {
                            totalPages : 0,
                            totalRecords : 0,
                            currentPage : 1,
                            currentPageSize : 1,
                            pageSize : 200
                        }
                    } ,
                    note : 'v-model'
                },
                isRemote : { type : Boolean, default : false, note : '- deprecated -'}
            },
            data : function(){
                return {
                    // recordsPerPage : 25,
                    // currentPage : 1
                    paginatedRecords : [],
                    items: [2,10,25,50,100]
                }
            },
            mounted : function() {
                //Ensure records flowing to parent whn mounted
                // if(this.recordsToPaginate) {
                //     this.$emit('paginate-records', this.paginatedRecords);
                // }
                // 
                // if(this.recordsToPaginate) {
                //     this.paginateRecords();
                // }

                //Needed if recordsToPaginate already exist when mounting the component, watcher doent not evaluate
                if(this.recordsToPaginate 
                    && (this.recordsToPaginate.length > 0 || Object.keys(this.recordsToPaginate).length > 0)) {
                    this.paginateRecords();
                }

                if(this.value.currentPage <= 0) {
                    this.value.currentPage = 1;
                }

            },
            computed : {
                theme : function() { return this.$vfstore.getters.getTheme; },
                labels : function() { return this.$vfstore.getters.getLabels; },

                pageMessage : function() {
                    var msg = ' 0 ' + this.labels.of + ' 0 ';
                    if(this.recordsToPaginate){
                        msg = (this.paginatedRecords && this.paginatedRecords.length ? this.paginatedRecords.length : 0) + ' ' + this.labels.of + ' ' + (this.recordsToPaginate ? (this.recordsToPaginate.length ? this.recordsToPaginate.length : Object.keys(this.recordsToPaginate).length) : 0);
                    }
                    else {
                        var ps = this.value.currentPageSize ? this.value.currentPageSize : this.value.pageSize;
                        var cpIndex = this.value.currentPage - 1;
                        var initialCount = cpIndex == 0 ? 1 : (cpIndex * this.value.pageSize) + 1;
                        var endCount = (initialCount + this.value.pageSize) - 1;
                        // msg = ps + ' ' + this.labels.of + ' ' + this.value.totalRecords;
                        msg = initialCount + ' - ' + endCount + ' ' + this.labels.of + ' ' + this.value.totalRecords;
                    }
                     
                    return msg;
                },
                /**
                 * @return     {Integer}  { Total pages for the filtered records  }
                 */
                totalPages : function(){
                    if(this.value.totalPages) {
                        return this.value.totalPages;
                    }
                    if(!this.recordsToPaginate) return 0;

                    if(this.isRemote && this.value.totalPages) return this.value.totalPages;

                    var len = (this.recordsToPaginate.length ? this.recordsToPaginate.length : Object.keys(this.recordsToPaginate).length);
                    var totalp = this.recordsToPaginate ? Math.ceil( len / this.value.pageSize) : 0;
                
                    return totalp;
                },
            },


            watch : {

                recordsToPaginate : {
                    handler : function(newValue, oldValue) {
                        //Recalculate number of pages
                        // if(newValue.length != oldValue.length) {
                        //     this.value.totalPages = undefined;
                        //     this.value.totalPages = this.totalPages;
                        // }
                        // console.time('##VFPaginator recordsToPaginate handler');
                        if(this.value)
                            this.paginateRecords();
                        // console.timeEnd('##VFPaginator recordsToPaginate handler');
                    },
                    deep : true
                },

                // paginatedRecords : function(newValue) {
                //    this.$emit('paginate-records', newValue);
                // },

                showAllRecords : function() {
                    this.paginateRecords();
                },

                value : {
                    handler : function(newValue, oldValue) {
                        if(this.totalPages < newValue.currentPage){
                            newValue.currentPage = this.totalPages;
                        }
                        this.paginateRecords();
                    },
                    deep : true
                },

                extraOptions : {
                    handler : function(newValue, oldValue) {
                        if (newValue != null) {
                            if (Array.isArray(newValue)) {
                                for (var i = 0; i < newValue.length; i++) {
                                    if (!_.includes(this.items, newValue[i])) {
                                        this.items.push(newValue[i]);
                                    }
                                }
                            } else if (!_.includes(this.items, newValue)) {
                                this.items.push(newValue);
                            }
                        }
                    },
                    immediate : true
                },

                defaultOptionValue : {
                    handler : function(newValue, oldValue) {
                        if (newValue != null) {
                            if (newValue != oldValue) {
                                var v = JSON.parse(JSON.stringify(this.value));
                                v.pageSize = newValue;
                                this.$emit('input', v);
                            }
                        }
                    },
                    immediate : true
                }
            },
            methods : {

                paginateRecords : function(){
                    // console.time('##VFPaginator paginateRecords');
                    var ret = [];
                    if(this.recordsToPaginate instanceof Array) {
                        var recPage = (this.showAllRecords == true) ? this.recordsToPaginate.length : this.value.pageSize;
                        var currPage = (this.showAllRecords == true) ? 1 : this.value.currentPage;
                        var initialRecordIndex = currPage == 1 ? 0 : (currPage * recPage) - recPage;
                        var endRecordIndex = (this.value.currentPage * recPage);
                        // return this.recordsToPaginate.slice(initialRecordIndex, endRecordIndex);
                        ret = this.recordsToPaginate.slice(initialRecordIndex, endRecordIndex);

                    } else if(this.recordsToPaginate instanceof Object) {
                        //must return an object only with paginated keys
                        var keys = Object.keys(this.recordsToPaginate);
                        var recPage = (this.showAllRecords == true) ? keys.length : this.value.pageSize;
                        if(recPage > keys.length) recPage = keys.length;
                        var currPage = (this.showAllRecords == true) ? 1 : this.value.currentPage;
                        var initialRecordIndex = currPage == 1 ? 0 : (currPage * recPage) - recPage;
                        var endRecordIndex = (this.value.currentPage * recPage);
                        ret = {};
                        for(var i = initialRecordIndex; i < endRecordIndex; i++) {
                            var pagKey = keys[i];
                            // ret[pagKey] = this.recordsToPaginate[pagKey];
                            Vue.set(ret, pagKey, this.recordsToPaginate[pagKey]);
                        }

                        //HACK!!!!!??????
                        // this.$emit('paginate-records', ret);

                        // return ret;
                    }

                    this.paginatedRecords = ret;
                    this.$emit('paginate-records', ret);
                    // console.timeEnd('##VFPaginator paginateRecords');

                },
                previousPage : function() {
                    if(this.value.currentPage != 1 && this.value.currentPage <= this.totalPages){
                        this.value.currentPage--;
                        this.$emit('previous-page');
                    }
                },
                nextPage : function() {
                    if(this.value.currentPage != this.totalPages && this.value.currentPage >= 1){
                        this.value.currentPage++;
                        this.$emit('next-page');
                    }
                },
                goToPage : function(pageNumber) {
                    if(pageNumber >= 1 && pageNumber <= this.totalPages) {
                        this.value.currentPage = pageNumber;
                    }
                },
                resetCurrentPage : function() {
                    console.log('**** resetCurrentPage called');
                    // this.goToPage(1);
                    this.value.currentPage = 1;
                }
            }
        };
        Vue.component('vf-paginator', VueForce.VfPaginator);
    </script>

</apex:component>