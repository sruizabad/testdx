<apex:component >
    
    <script id="vf-path-item" type="text/x-template">
      <li class="slds-path__item" :class="getItemClass()" role="presentation" @click="$emit('click')">
        <a aria-selected="false" class="slds-path__link" href="javascript:void(0);" >
          <vf-svg-icon class="slds-path__stage" path="utility/check" size="x-small" color="white"/>
          <span class="slds-path__title">{{label}}</span>
        </a>
      </li>
    </script>
    <script>
        Vue.component('vf-path-item', {
        // options
        template : '#vf-path-item',
        props : {
          label : {type : String, default : ''},
          isWon : {type : Boolean, default : false},
          isLost : {type : Boolean, default : false},
          isComplete : {type : Boolean, default : false},
          isActive : {type : Boolean, default : false},
          isCurrent : {type : Boolean, default : false}
        },
        mounted : function() {

        },
        data : function() {
          return {

          }
        },
        computed : {
          theme : function() {
            return this.$vfstore.getters.getTheme;
          }
        },
        methods : {          
          getItemClass : function() {
            if(this.isWon){
              return 'slds-is-complete slds-is-won slds-is-active';
            }else if(this.isLost){
              return 'slds-is-lost slds-is-active';
            }else if(this.isComplete){
              return 'slds-is-complete';
            }else if(this.isActive){
              return ' slds-is-incomplete slds-is-active';
            }else if(this.isCurrent){
              return 'slds-is-current';
            }else if(!this.isComplete){
              return 'slds-is-incomplete';
            }else{
              return 'slds-is-incomplete';
            }
          }
        }
    });
    </script>

    <style type="text/css">
        .vertical_item_border_radious{
            border-radius:  15rem !important;
        } 
        .vertical-grid{
          display: grid;
          max-height: max-content;
        }
        .vertical-button{
          max-height:   60px;
          min-height:   60px;
        }
    </style>
    <script type="text/x-template" id="vf-path">
    	<div class="slds-path" :class="getCoachingClass()">
		    <div class="slds-grid slds-path__track" :class="getOverflowClass()">
		      <div class="slds-grid slds-path__scroller-container">

		        <button v-if="coaching && horizontalOrientation" @click="openCoaching" class="slds-button slds-button_icon slds-button_icon-border-filled slds-path__trigger" title="Collapse Sales Coaching Tab Panels">
		          <vf-svg-icon v-if="showCoaching" path="utility/chevronup" size="button" />
		          <vf-svg-icon v-else path="utility/chevrondown" size="button" />
		          <span class="slds-assistive-text">Collapse Sales Coach Tab Panels</span>
		        </button>

		        <div class="slds-path__scroller" role="application">
		          <div class="slds-path__scroller_inner">
		            <ul v-if="horizontalOrientation" class="slds-path__nav" role="tablist">
		              <li v-for="item in items" class="slds-path__item" :class="getItemClass(item)" role="presentation" @click="$emit('item-click', item)">
		                <a aria-selected="false" class="slds-path__link" href="javascript:void(0);" >
		                  <vf-svg-icon class="slds-path__stage" path="utility/check" size="x-small" color="white"/>
		                  <span class="slds-path__title">{{item.label}}</span>
		                </a>
		              </li>
                  <slot name="items">

                  </slot>
                </ul>

                <ul v-if="verticalOrientation" v-for="item in items" class="slds-path__nav slds-m-bottom_xx-small" role="tablist">
                  <li class="slds-path__item vertical_item_border_radious" :class="getItemClass(item)" role="presentation" @click="$emit('item-click', item)">
                    <a aria-selected="false" class="slds-path__link" href="javascript:void(0);" >
                      <vf-svg-icon class="slds-path__stage" path="utility/check" size="x-small" color="white"/>
                      <span class="slds-path__title">{{item.label}}</span>
                    </a>
                  </li>
                </ul>
                <slot v-if="verticalOrientation" name="items" >

                </slot>
                <!-- <div v-if="scrolling" class="slds-path__scroll-controls">
                  <button class="slds-button slds-button_icon slds-button_icon-border-filled" title="Scroll left" tabindex="-1">
                    <vf-svg-icon path="utility/left" size="x-small"/>
                    <span class="slds-assistive-text">Scroll left</span>
                  </button>
                  <button class="slds-button slds-button_icon slds-button_icon-border-filled" title="Scroll right" tabindex="-1">
                    <vf-svg-icon path="utility/right" size="x-small"/>
                    <span class="slds-assistive-text">Scroll right</span>
                  </button>
                </div> -->
		          </div>
		        </div>
		      </div>
		      <div v-if="horizontalOrientation" class="slds-grid slds-path__action">
		        <span class="slds-path__stage-name">Stage: Unqualified</span>
		        <button v-if="showStandardButton" class="slds-button slds-button_brand slds-path__mark-complete" @click="$emit('button-click')">
		          <vf-svg-icon path="utility/check" size="button" color="white"/> {{standardButtonLabel}}
            </button>
            <slot name="buttons">

            </slot>
		      </div>
          <div v-if="verticalOrientation" class="vertical-grid">
            <span class="slds-path__stage-name">Stage: Unqualified</span>
            <button v-if="showStandardButton" class="slds-button slds-button_brand slds-path__mark-complete vertical-button slds-m-bottom_xx-small" @click="$emit('button-click')">
              <vf-svg-icon path="utility/check" size="button" color="white"/> {{standardButtonLabel}}
            </button>
            <slot name="buttons">

            </slot>
          </div>
		    </div>
		    <div v-if="showCoaching">
          <slot name="coaching">

          </slot>
        </div>
		  </div>

	</script>
	<script>
	    Vue.component('vf-path', {
		    // options
		    template : '#vf-path',
		    props : {
		    	coaching : {type : Boolean, default : false},
          orientation : {type : String, default : 'horizontal'},
          // scrolling : {type : Boolean, default : false},
          showStandardButton : {type : Boolean, default : true},
          standardButtonLabel : {type : String, default : 'Mark Status as Complete'},
		    	items : {type : Array, default : null}
		    },
		    data : function() {
		        return {
		        	showCoaching : false,
		        }
		    },
		    methods : {
		    	getItemClass : function(item) {
            if(item.won){
              return 'slds-is-complete slds-is-won slds-is-active';
            }else if(item.lost){
              return 'slds-is-lost slds-is-active';
            }else if(item.complete){
              return 'slds-is-complete';
            }else if(item.active){
              return ' slds-is-incomplete slds-is-active';
            }else if(item.current){
              return 'slds-is-current';
            }else if(!item.complete){
              return 'slds-is-incomplete';
            }else{
              return 'slds-is-incomplete';
            }
		    	},
		    	getCoachingClass : function() {
		    		return {
		    			'slds-is-expanded' : this.showCoaching
		    		}
		    	},
          getOverflowClass : function(){
            if(this.horizontalOrientation){
              return 'slds-has-overflow';
            }
            return '';
          },

		    	openCoaching : function() {
		    		this.showCoaching = !this.showCoaching;
		    	}

          
		    },
		    computed : {
		        theme : function() {
		            return this.$vfstore.getters.getTheme;
		        },
            horizontalOrientation : function(){
              if(this.orientation === 'horizontal'){
                return true;
              }
              return false;
            },
            verticalOrientation : function(){
              if(this.orientation === 'vertical'){
                return true;
              }
              return false;
            }
		    }
		    
		});
	</script>
	<style>
	</style>
</apex:component>