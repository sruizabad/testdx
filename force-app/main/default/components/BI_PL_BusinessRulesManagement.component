<apex:component controller="BI_PL_BusinessRulesCtrl">
<style>

.vf-specialties-selector-clear {
	position: absolute;
	right: 1em;
	top: 0;
	bottom: 3px;
	height: 14px;
	margin: auto;
	font-size: 14px;
	cursor: pointer;
	color: #ccc;
}
.vf-specialties-selector-container{
	display: flex;
}
.vf-specialties-selector-container>div {
	width: 100%;
	display: block;
	position: relative;
	margin-right: 0.5em;
}
.vf-specialties-selector-container .vf-specialties-selector-container-input {
	width: 100%;
	display: block;
	position: relative;
	margin-right: 0.5em;

}
.vf-specialties-selector-container .vf-specialties-selector-container-input input{
	margin-right: 0.5em;
	width: 100% !important;

}

</style>
<c:BI_PL_SpecialtySelector ></c:BI_PL_SpecialtySelector>
<script type="text/x-template" id="bi-pl-business-rules-management">
	<div>
		<vf-action-status v-model="status">
			<strong>{!$Label.BI_PL_SelectCycleHierarchyChannel}</strong>
		</vf-action-status>
		<vf-card title="{!$Label.BI_PL_Creation_p1p2_br}" icon-class="slds-icon-action-script" icon="action/update_status">
			<div slot="title">
                {!$Label.BI_PL_Creation_p1p2_br}
                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconCyclePath" />&nbsp;<strong>{!$Label.BI_PL_Cycle}</strong></span>
                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconMultiHierarchyPath" />&nbsp;<strong>{!$Label.BI_PL_One_many_hierarchies}</strong></span>
                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconChannelPath" />&nbsp;<strong>{!$Label.BI_PL_Channel_card}</strong></span>
            </div>
			<div slot="buttons"> 
				<button v-bind:class="theme.button.primary" v-bind:disabled="selectedPrimaryProduct === null || !selectedFilters" @click="runPrimarySecondaryProductProcess()"> {!$Label.BI_PL_Run_process} </button> 
			</div>
			<div slot="body" class="slds-p-around_medium"> 
				<div class="slds-grid slds-wrap">
					<div class="slds-col slds-medium-size_9-of-12 slds-grid slds-m-around_xx-small">
						<div class="slds-col slds-medium-size_3-of-12 slds-text-align_right">
							<label>{!$Label.BI_PL_Primary_product}</label> 
						</div>
						<div class="slds-col slds-medium-size_9-of-12">
							<vf-lookup   
							sobject="Product_vod__c" 
							fields="Name, Active_BI__c"
							:filters="productFilters"
							v-model="selectedPrimaryProduct"
							:query-on-create="false"
							:styleclass="theme.input"
							:buttonstyleclass="theme.button.primary"></vf-lookup>
						</div>
					</div>
					<div class="slds-col slds-medium-size_3-of-12 slds-grid "></div>
					<div v-if="settings.BI_PL_Use_secondary_product__c" class="slds-col slds-medium-size_9-of-12 slds-grid slds-m-around_xx-small">
						<div class="slds-col slds-medium-size_3-of-12 slds-text-align_right">
							<label>{!$Label.BI_PL_Secondary_product}</label> 
						</div>
						<div class="slds-col slds-medium-size_9-of-12">
							<vf-lookup   
							sobject="Product_vod__c" 
							fields="Name, Active_BI__c"
							:filters="productFilters"
							v-model="selectedSecondaryProduct"
							:query-on-create="false"
							:styleclass="theme.input"
							:disabled="!selectedPrimaryProduct"
							:buttonstyleclass="theme.button.primary"></vf-lookup>
						</div>
					</div>
					<div class="slds-col slds-medium-size_3-of-12 slds-grid "></div>
				</div> 
			</div>
		</vf-card>
		<vf-action-status v-model="statusSpecialty">
			<strong>{{ specialtyResultMessage }}</strong>
		</vf-action-status>
		<vf-card v-if="useSpecialtyBR" title="{!$Label.BI_PL_Creation_customer_sp_br}" icon-class="slds-icon-action-script" icon="action/update_status">
			<div slot="title">
                {!$Label.BI_PL_Creation_customer_sp_br}
                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconCyclePath" />&nbsp;<strong>{!$Label.BI_PL_Cycle}</strong></span>
                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconMultiHierarchyPath" />&nbsp;<strong>{!$Label.BI_PL_One_many_hierarchies}</strong></span>
                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconChannelPath" />&nbsp;<strong>{!$Label.BI_PL_Channel_card}</strong></span>
            </div>
			<div slot="buttons"> 
				<button v-bind:class="theme.button.primary" v-bind:disabled="!selectedFilters || !selectedSpecialtyPrimaryProduct || !specialtyListFiltered" @click="createCustomerSpecialtyProcess()"> {!$Label.BI_PL_Create_br} </button> 
			</div>
			<div slot="body" class="slds-p-around_medium"> 
				<div class="slds-grid slds-wrap">
					<div class="slds-col slds-medium-size_9-of-12 slds-grid slds-m-around_xx-small">
						<div class="slds-col slds-medium-size_3-of-12 slds-text-align_right">
							<label>{!$Label.BI_PL_Product_Label}</label> 
						</div>
						<div class="slds-col slds-medium-size_9-of-12">
							<vf-lookup   
							sobject="Product_vod__c" 
							fields="Name, Active_BI__c"
							:filters="productFilters"
							v-model="selectedSpecialtyPrimaryProduct"
							:query-on-create="false"
							:styleclass="theme.input"
							:buttonstyleclass="theme.button.primary"></vf-lookup>
						</div>
					</div>
					<div class="slds-col slds-medium-size_3-of-12 slds-grid "></div>
					<div class="slds-col slds-medium-size_9-of-12 slds-grid slds-m-around_xx-small">
						<div class="slds-col slds-medium-size_3-of-12 slds-text-align_right">
							<label>{!$Label.BI_PL_Speciality_Label}</label> 
						</div>
						<div class="slds-col slds-medium-size_9-of-12">
							<bi-pl-specialty-selector :filterSelection="filterSelection" v-model="specialtiesList" :multiple="true"/>
			            </div>
			        </div>
			        <div class="slds-col slds-medium-size_3-of-12 slds-grid "></div>
			    </div> 
			</div>
		</vf-card>
	</div>
</script>

<script>
Vue.component('bi-pl-business-rules-management', {
	template : '#bi-pl-business-rules-management',
	props: {
		selectedFilters: false,
		channel: null,
		hierarchy: null,
		cycle: null,
		hierarchies : null
	},
	data : function() {
		return {
			// Primary and Secondary Product
			selectedPrimaryProduct: null,
			selectedSecondaryProduct: null,
			
			// Customer Specialty 
			selectedSpecialtyPrimaryProduct: null,

			showSpecialtyResult: false,
			specialtyResult: String,
			specialtyResultMessage: String,

			specialtiesList: []

		}
	},
	computed : {
		theme : function() {
			return this.$store.getters.getTheme;
		},
		settings : function() {
			return this.$store.getters.getSettings;
		},
		productFilters : function() {
			return 'Product_Type_vod__c = \'Detail\' and Country_Code_BI__c = \'' + this.settings.BI_PL_Country_code__c + '\'';
		},
		status : function() {
			return {
				level : 'warning',
				show: !this.selectedFilters
			};
		},
		statusSpecialty : function() {
			return {
				level : this.specialtyResult,
				show: this.showSpecialtyResult
			};
		},
		selectedSecondaryProductId : function() {
			return this.selectedSecondaryProduct!=null?this.selectedSecondaryProduct.Id:null;
		},
		specialtyListFiltered : function(){
			return _.filter(this.specialtiesList, function(sub) {return sub.length;});
		},
		filterSelection : function(){
			return [{'cycle':this.cycle},{'hierarchy':this.hierarchy}];
		},
        useSpecialtyBR : function() {
            return this.settings.BI_PL_Use_specialty_business_rule__c;
        },
        iconCyclePath : function() {
            return !this.cycle ? 'utility/error' : 'utility/check';
        },
        iconMultiHierarchyPath : function() {
            return !this.hierarchies || (this.hierarchies && this.hierarchies.length == 0) ? 'utility/error' : 'utility/check';
        },
        iconChannelPath : function(){
        	return !this.channel ? 'utility/error' : 'utility/check';
        }
	},
	methods : {
		runPrimarySecondaryProductProcess : function(){
			var batchName = 'BI_PL_PrimarySecondaryProductBatch';
			var params = {
				'primaryProduct': this.selectedPrimaryProduct.Id,
				'secondaryProduct': this.selectedSecondaryProductId
			};
			this.$emit('run', batchName, params);
		},
		createCustomerSpecialtyProcess : function(){
			
			var self = this;
			BI_PL_BusinessRulesCtrl.createCustomerSpecialtyBR(
				this.channel, 
				this.settings.BI_PL_Country_code__c,
				this.selectedSpecialtyPrimaryProduct.Id,
				JSON.stringify(this.specialtyListFiltered), 
				function(result, event){

					self.showSpecialtyResult = true;
					self.selectedSpecialtyPrimaryProduct = null;
					setTimeout(function(){
						self.showSpecialtyResult = false;
					}, 3000);
					if(event.statusCode==200){
						self.specialtyResult = 'success';
						self.specialtyResultMessage = '{!$Label.BI_PL_BusinessRuleCreated}';
					}else{
						self.specialtyResult = 'error';
						self.specialtyResultMessage = '{!$Label.BI_PL_ErrorCreatingBusinessRule}';
					}
			});

		}
	}
});
</script>
</apex:component>