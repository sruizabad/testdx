<apex:component >

    <style>
    </style>

    <!-- Sl string 'bi-pl-dashboard' ha de ser unico en Planit. Se ha de reemplazar en 3 lugares en este fichero. El id de template (tanto en el marcado como en JS) y el nombre del componente -->
    <script type="text/x-template" id="bi-pl-dashboard">
    	<span class="slds-scope">

            <vf-action-status ref="status" v-model="status" />
            
    		<div class="slds-box slds-theme_shade">
	                <!-- text-class="slds-text-title_caps" -->
                <cycle-hierarchy-selector 
                    ref="cycleHierarchySelector"
                    :vertical="false"
	                :text-class="theme.label"
	                :show-country="true"
	                :show-position="true"
                    
                    :country.sync="selectedCountry"
                    :cycle.sync="selectedCycle"
                    :channel.sync="selectedChannel"
                    :position.sync="selectedPosition"

                    :hierarchies.sync="selectedHierarchies"
                    :hierarchy.sync="selectedHierarchy"
                    :only-hierarchy-channels="false"
                    :enable-all-hierarchies-option="false"

                    :query-plans="true"
                    :plans.sync="preparations"

                    :rep-id="user.Id"
                    :show-options="permissions.isAdmin"
                    :editable-position="permissions.isDS || permissions.isAdmin"

                    @error="onSelectorError" >
	            </cycle-hierarchy-selector>
	        </div>

            <div class="slds-m-top_small">


            	<vf-tab-set default-tab="general" :re-render="true" :vertical="true">
                    <!-- <vf-tab name="hierarchy" role="button" label="View hierarchy">
                        
                        <bi-pl-hierarchy-tree :hierarchy="positionHierarchy" />

                    </vf-tab> -->
                    <vf-tab name="general" label="{!$Label.BI_PL_Country_Dashboard_Title}">
                        <vf-card title="{!$Label.BI_PL_Transfer_share}" icon-class="slds-icon-action-script" icon="utility/change_record_type" v-if="checkVisibility('12')">
                            <div slot="body">
                                <transfer-and-share-requests :channel="selectedChannel" :preparations-id="preparationIds" > </transfer-and-share-requests>
                            </div>
                        </vf-card>

                        <vf-card title="{!$Label.BI_PL_Summary_Sales_Chart_Title}" icon-class="slds-icon-action-script" icon="utility/summary">
                            <div slot="body">
                                <bi-pl-summary-sales  :channel="selectedChannel" :show-chart="true" :cycle="selectedCycle" :position="selectedPositionId" :hierarchy="selectedHierarchy">
                                </bi-pl-summary-sales>
                            </div>
                        </vf-card>

                        <vf-card title="{!$Label.BI_PL_Call_Plan_Overview_Label}" icon-class="slds-icon-action-script" icon="utility/summarydetail">
                            <div slot="body">
                                <bi-pl-summary-products-details :channel="selectedChannel" :cycle="selectedCycle" :cycle-id="selectedCycleId" :position="selectedPositionId" :hierarchy="selectedHierarchy" :data-from-veeva="false" >
                                </bi-pl-summary-products-details>
                            </div>
                        </vf-card>
                        
                    </vf-tab>
                    <vf-tab name="veevaDashboard" v-if="checkVisibility('2')" label="{!$Label.BI_PL_Veeva_Dashboard_Button}" >
                        <vf-card title="{!$Label.BI_PL_Call_Plan_Overview_Label} - Veeva" icon-class="slds-icon-action-script" icon="utility/summarydetail">
                            <div slot="body"> 
                                <bi-pl-summary-products-details :channel="selectedChannel" :cycle-id="selectedCycleId" :position="selectedPositionId" :hierarchy="selectedHierarchy" :data-from-veeva="true" />
                            </div>
                        </vf-card>

                        <vf-card title="Planit Vs Veeva" icon-class="slds-icon-action-script" icon="utility/table">
                            <div slot="body"> 
                                <bi-pl-detail-table 
                                    field-set="BI_PL_Detail_view_pva"
                                    v-bind:channel="selectedChannel" 
                                    v-bind:cycle="selectedCycleId" 
                                    v-bind:hierarchy="selectedHierarchy" 
                                    v-bind:position="selectedPositionId"
                                    v-bind:searchable="true"
                                    > 
                                </bi-pl-detail-table>
                            </div>
                        </vf-card>
                    </vf-tab>
			
                    <vf-tab name="cyclePlan" v-if="checkVisibility('5')" label="{!$Label.BI_PL_Cycle_Plan_Summary_Title}"> 
                        <!-- <h2> {!$Label.BI_PL_Cycle_Plan_Summary_Title} </h2> -->
                        <vf-card title="{!$Label.BI_PL_Cycle_Plan_Summary_Title}">
                            <div slot="body"> 
                                <bi-pl-detail-table :channel="selectedChannel"  :cycle="selectedCycleId"  :hierarchy="selectedHierarchy"  :position="selectedPositionId" field-set="BI_PL_Detail_view_lift" max-width="83vw" />
                            </div>
                        </vf-card>
                    </vf-tab>
                    <vf-tab name="overlaps" v-if="checkVisibility('6')" label="{!$Label.BI_PL_Overlap_Label}">
                        <vf-card title="{!$Label.BI_PL_Overlap_Label}">
                            <div slot="body"> 
                                <bi-pl-position-overlap-report 
                                    :channel="selectedChannel" 
                                    :cycle="selectedCycleId" 
                                    :hierarchy="selectedHierarchy" 
                                    :position="selectedPositionId"
                                    :searchable="true"
                                    > 
                                </bi-pl-position-overlap-report>
                            </div>
                        </vf-card>
                    </vf-tab>
                    <vf-tab name="counters" v-if="checkVisibility('11')" label="{!$Label.BI_PL_Call_Reason_Code_Section}">
                        <vf-card title="{!$Label.BI_PL_Call_Reason_Code_Section}">
                            <div slot="body">
                                <bi-pl-target-reason-count :preparations="preparationIds" />
                            </div>
                        </vf-card>
                        <vf-card title="{!$Label.BI_PL_Target_Counter}">
                            <div slot="body">
                                <div class="container-fluid slds-grid slds-gutters slds-wrap" style="display:flex;" id="target_count_section"> 
                                    <div class="slds-col slds-medium-size_6-of-12 col-md-6"> <bi-pl-target-numbers :channel="selectedChannel" :cycle="selectedCycleId" :hierarchy="selectedHierarchy" :preparations="preparationIds" /> </div> 
                                    <div class="slds-col slds-medium-size_6-of-12 col-md-6"> <dynamic-target-numbers :preparations="preparationIds" /> </div>
                                </div>
                            </div>
                        </vf-card>
                    </vf-tab>
                    <vf-tab name="chCharts"  label="Channel detail charts">
                        <vf-card title="Control tower">
                            <div slot="body">
                                <bi-pl-channel-detail-charts :show-filters="false" :channel="selectedChannel" :cycle="selectedCycle" :position-id="selectedPositionId" :hierarchy="selectedHierarchy"/>
                            </div>
                        </vf-card>
                    </vf-tab>
                </vf-tab-set>
            </div>
	    </span>
    </script>

    <script>
        Vue.component('bi-pl-dashboard', {
            template : '#bi-pl-dashboard',
            //Llamado cuando el componente se crea
            created() {},

            //Llamado cuando el componente se monta 
            mounted() {},

            //Usar este metodo para limpiar propiedades de 'data' y elminar posible 'memory leaks'
            beforeDestroy() {
                this.preparationids = [];
                this.selectedHierarchies = null;
            },

            //Si utiliza funcionalidad de business rules obrowser settings
            // mixins : [],
            props : {
                
            },
            data : function() {
                return {
                	selectedCountry : null,
                    selectedCycle : null,
                    selectedHierarchies : null,
                    selectedHierarchy : null,
                    selectedChannel : null,
                    selectedPosition : null,

                    //subject to change
                    // preparationIds : [],
                    preparations : [],

                    status : {
                        show : false,

                    }
                }
            },
            watch : {
                
            },
            computed : {
                user() { return this.$store.getters.getuserInfo },
                selectedPositionId() { return this.selectedPosition && this.selectedPosition.record && this.selectedPosition.record.BI_PL_Position__c ? this.selectedPosition.record.BI_PL_Position__c : null},
                selectedCycleId() { return this.selectedCycle && this.selectedCycle.Id ? this.selectedCycle.Id : null},
                preparationIds() { 
                    if(this.preparations)
                        return this.preparations.map(function(prep){
                            return prep.Id;
                        })
                    return [];
                },
                dashboardCountrySettings() {
                    if(this.settings && this.settings.BI_PL_Dashboard_Information__c)
                        return this.settings.BI_PL_Dashboard_Information__c.split(';');
                    return [];
                },
            	//VueForce vars
            	theme() { return this.$store.getters.getTheme },
            	schema() { return this.$store.getters.getSchema },

            	//Planit
            	settings() { return this.$store.getters.getSettings },
            	permissions() { return this.$store.getters.getPermissions},
            	user() { return this.$store.getters.getUser },
            	workingCountry() { return this.$store.getters.getWorkingCountry },
            },

            methods : {
            	checkVisibility : function(n) { 
                    if(this.dashboardCountrySettings)
                        return this.dashboardCountrySettings.indexOf(n) !== -1;
                    return false;
                },

                onSelectorError : function(err) {
                    if(err){
                        this.$refs.status.showMessage('error', err);
                    }
                    else {
                        this.status.show = false;
                    } 

                }
            }
        });
    </script>

</apex:component>