<apex:component >

    <style>

    </style>
    
    <script type="text/x-template" id="paginator">

        <div class="row" v-if="showControls">
            <div class="col-md-3">
                {!$Label.BI_PL_Displaying} {{pageMessage}}
            </div>
            <div class="col-md-6 text-center">
                <ul class="pagination" style="margin-top: 0px !important">
                    <li class="paginate_button previous" v-bind:class="{ disabled : value.currentPage <= 1}" role="button">
                        <a v-on:click="previousPage()">{!$Label.BI_PL_Previous}</a>
                    </li>

                    <template v-for="pageNumber in totalPages">
                        <li v-bind:class="{active : pageNumber == value.currentPage}"
                            v-on:click="goToPage(pageNumber)"
                            class="paginate_button"
                            v-if="totalPages <= 5 
                                || (value.currentPage == pageNumber || pageNumber == 1 || pageNumber == 2 || pageNumber == totalPages || pageNumber == totalPages - 1)" role="button">
                                <a v-on:click="value.currentPage = pageNumber">{{pageNumber}}</a>
                        </li>
                        <li v-else-if="totalPages > 5 && (pageNumber == totalPages - 2 || pageNumber == 3)" role="button">
                            <a>...</a>
                        </li>
                    </template>
                    
                    <li class="paginate_button next" v-bind:class="{ disabled : value.currentPage >= totalPages}" role="button">
                        <a v-on:click="nextPage()">{!$Label.BI_PL_NextButton}</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 text-right">
                <label>
                    {!$Label.BI_PL_RecordsPerPage}
                </label>
                <select class="form-control input-sm" v-model.number="value.pageSize" style="display: inline-block !important">
                    <option value="2">2</option>
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>
        </div>
    </script>

    <script>
        Vue.component('paginator', {
            template : '#paginator',
            props : {
                recordsToPaginate : [Array, Object],
                showAllRecords : Boolean,
                pageSize : Number,
                showControls : {type : Boolean, default : true},

                value : { 
                    type : Object, 
                    default : function() {
                        return {
                            currentPage : 1,
                            pageSize : 10
                        }
                    } 
                },
                isRemote : { type : Boolean, default : false}
            },
            data : function(){
                return {
                    // recordsPerPage : 25,
                    // currentPage : 1
                    paginatedRecords : []
                }
            },
            mounted : function() {
                //Ensure records flowing to parent whn mounted
                // if(this.recordsToPaginate) {
                //     this.$emit('paginate-records', this.paginatedRecords);
                // }
                // 
                // if(this.recordsToPaginate) {
                //     this.paginateRecords();
                // }

                //Needed if recordsToPaginate already exist when mounting the component, watcher doent not evaluate
                if(this.recordsToPaginate 
                    && (this.recordsToPaginate.length > 0 || Object.keys(this.recordsToPaginate).length > 0)) {
                    this.paginateRecords();
                }

                if(this.value.currentPage <= 0) {
                    this.value.currentPage = 1;
                }

            },
            computed : {
                pageMessage : function() {
                    var msg = (this.paginatedRecords? (this.paginatedRecords.length ? this.paginatedRecords.length : Object.keys(this.paginatedRecords).length) : 0) + ' {!$Label.BI_SP_Of} ' + (this.recordsToPaginate ? (this.recordsToPaginate.length ? this.recordsToPaginate.length : Object.keys(this.recordsToPaginate).length) : 0);
                     
                    return msg;
                },
                /**
                 * @return     {Integer}  { Total pages for the filtered records  }
                 */
                totalPages : function(){
                    if(!this.recordsToPaginate) return 0;

                    if(this.isRemote && this.value.totalPages) return this.value.totalPages;

                    var len = (this.recordsToPaginate.length ? this.recordsToPaginate.length : Object.keys(this.recordsToPaginate).length);
                    var totalp = this.recordsToPaginate ? Math.ceil( len / this.value.pageSize) : 0;
                    

                    // if(this.value.totalPages && this.value.totalPages != totalp){
                    //     return this.value.totalPages;
                    // }  else {
                    //     this.value.totalPages = totalp;
                    // }

                    return totalp;
                },
                /**
                 * For filtered records, gets the corresponding to the curren page number and records per page.
                 * This is what is going to be displayed on main table
                 *
                 * @return     {Array}  { Array of paginated, ordered and filtered targets }
                 */
                // paginatedRecords : function(){
                //     console.log('paginator paginatedRecords computed');
                //     if(this.recordsToPaginate instanceof Array) {
                //         var recPage = (this.showAllRecords == true) ? this.recordsToPaginate.length : this.value.pageSize;
                //         var currPage = (this.showAllRecords == true) ? 1 : this.value.currentPage;
                //         var initialRecordIndex = currPage == 1 ? 0 : (currPage * recPage) - recPage;
                //         var endRecordIndex = (this.value.currentPage * recPage);
                //         return this.recordsToPaginate.slice(initialRecordIndex, endRecordIndex);

                //     } else if(this.recordsToPaginate instanceof Object) {
                //         //must return an object only with paginated keys
                //         var keys = Object.keys(this.recordsToPaginate);
                //         var recPage = (this.showAllRecords == true) ? keys.length : this.value.pageSize;
                //         if(recPage > keys.length) recPage = keys.length;
                //         var currPage = (this.showAllRecords == true) ? 1 : this.value.currentPage;
                //         var initialRecordIndex = currPage == 1 ? 0 : (currPage * recPage) - recPage;
                //         var endRecordIndex = (this.value.currentPage * recPage);
                //         var ret = {};
                //         for(var i = initialRecordIndex; i < endRecordIndex; i++) {
                //             var pagKey = keys[i];
                //             // ret[pagKey] = this.recordsToPaginate[pagKey];
                //             Vue.set(ret, pagKey, this.recordsToPaginate[pagKey]);
                //         }

                //         //HACK!!!!!??????
                //         this.$emit('paginate-records', ret);

                //         return ret;
                //     }
                // },
            },


            watch : {

                recordsToPaginate : {
                    handler : function(newValue, oldValue) {
                        //Recalculate number of pages
                        // if(newValue.length != oldValue.length) {
                        //     this.value.totalPages = undefined;
                        //     this.value.totalPages = this.totalPages;
                        // }
                        //console.log('**** recordsToPaginate watcher handler');
                        if(this.value) {
                            this.paginateRecords();
                        }
                    },
                    deep : true
                },

                paginatedRecords : function(newValue) {
                    //console.log('**** paginatedRecords watcher handler');
                   this.$emit('paginate-records', newValue);
                },

                showAllRecords : function() {
                    this.paginateRecords();
                },

                value : {
                    handler : function(newValue, oldValue) {
                        //console.log('**** value watcher handler');
                        if(this.totalPages < newValue.currentPage){
                            newValue.currentPage = this.totalPages;
                            this.$emit('input', this.value);
                        }
                        this.paginateRecords();
                    },
                    deep : true
                }
            },
            methods : {

                paginateRecords : function(){
                    //console.log('Paginator ## paginateRecords executed');
                    var ret = [];
                    if(this.recordsToPaginate instanceof Array) {
                        var recPage = (this.showAllRecords == true) ? this.recordsToPaginate.length : this.value.pageSize;
                        var currPage = (this.showAllRecords == true) ? 1 : this.value.currentPage;
                        var initialRecordIndex = currPage == 1 ? 0 : (currPage * recPage) - recPage;
                        var endRecordIndex = (this.value.currentPage * recPage);
                        // return this.recordsToPaginate.slice(initialRecordIndex, endRecordIndex);
                        ret = this.recordsToPaginate.slice(initialRecordIndex, endRecordIndex);

                    } else if(this.recordsToPaginate instanceof Object) {
                        //must return an object only with paginated keys
                        var keys = Object.keys(this.recordsToPaginate);
                        var recPage = (this.showAllRecords == true) ? keys.length : this.value.pageSize;
                        if(recPage > keys.length) recPage = keys.length;
                        var currPage = (this.showAllRecords == true) ? 1 : this.value.currentPage;
                        var initialRecordIndex = currPage == 1 ? 0 : (currPage * recPage) - recPage;
                        var endRecordIndex = (this.value.currentPage * recPage);
                        ret = {};
                        for(var i = initialRecordIndex; i < endRecordIndex; i++) {
                            var pagKey = keys[i];

                            // ret[pagKey] = this.recordsToPaginate[pagKey];
                            if(typeof pagKey != 'undefined')
                                Vue.set(ret, pagKey, this.recordsToPaginate[pagKey]);
                        }

                        //HACK!!!!!??????
                        // this.$emit('paginate-records', ret);

                        // return ret;
                    }

                    this.paginatedRecords = ret;
                },

                previousPage : function() {
                    if(this.value.currentPage != 1 && this.value.currentPage <= this.totalPages){
                        this.value.currentPage--;
                        this.$emit('input', this.value);
                    }
                    // this.paginateRecords();
                },
                nextPage : function() {
                    if(this.value.currentPage != this.totalPages && this.value.currentPage >= 1){
                        this.value.currentPage++;
                        this.$emit('input', this.value);
                    }
                    // this.paginateRecords();
                },
                goToPage : function(pageNumber) {
                    if(pageNumber >= 1 && pageNumber <= this.totalPages) {
                        this.value.currentPage = pageNumber;
                        this.$emit('input', this.value);
                    }
                    // this.paginateRecords();
                },
                resetCurrentPage : function() {
                    //console.log('**** resetCurrentPage called');
                    // this.goToPage(1);
                    this.value.currentPage = 1;
                }
            }
        });
    </script>

</apex:component>