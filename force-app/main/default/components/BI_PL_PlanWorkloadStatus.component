<!-- El nombre del componente VisualForce debería ser CamelCase -->
<apex:component >

    <style>
        .target-marker {
                /* transition: unset !important; */
                height: 25px;
                margin-top: -2px;
                border-right: 2px solid black;
                /* z-index: 5; */
                position: absolute;
        }
    </style>

    <script type="text/x-template" id="bi-pl-plan-workload-status">
    	<span >
            <div class="row slds-grid slds-wrap">
                <div class="slds-col slds-small-size_6-of-12 col-xs-6">
                    <label>Adjusted Workload</label>
                </div>
                <div class="slds-col slds-small-size_6-of-12 col-xs-6">
                    <span>{{actualWorkload}}</span>
                </div>
            </div>
            <div class="row slds-grid slds-wrap">
                <div class="slds-col slds-small-size_6-of-12 col-xs-6">
                    <label>Goal</label>
                </div>
                <div class="slds-col slds-small-size_6-of-12 col-xs-6">
                    <span>{{workload}}</span>
                </div>
            </div>
            <div class="row slds-grid slds-wrap">
                <div class="slds-col slds-small-size_6-of-12 col-xs-6">
                    <label>Workload Capacity</label>
                </div>
                <div class="slds-col slds-small-size_6-of-12 col-xs-6">
                    <span>{{actualWorkloadVsExpectedPercent}}</span>
                </div>
            </div><br/><br/>

    		<div class="progress">
              <div class="progress-bar progress-bar-striped active" :class="progressBarClass" :style="progresBarStyle" style="transition: unset !important">
                  <template v-if="workload === -1">
                      Workload business rule not defined for {{plan.BI_PL_Field_force__c}}
                    </template>
                    <template v-else>
                        {{actualWorkloadVsExpectedPercent}}% ({{actualWorkload}} Min:{{minValue}})
                    </template>
                </div>
                <div class="target-marker"  :style="markerStyle" style="transition: unset !important"></div>
            </div>
	    </span>
    </script>

    <script>
        Vue.component('bi-pl-plan-workload-status', {
            template : '#bi-pl-plan-workload-status',
            //Llamado cuando el componente se crea
            created : function() {},

            //Llamado cuando el componente se monta 
            mounted : function() {},

            //Usar este metodo para limpiar propiedades de 'data' y elminar posible 'memory leaks'
            beforeDestroy : function() {},

            //Si utiliza funcionalidad de business rules obrowser settings
            // mixins : [],
            props : {
                plan : {type : Object, default : null, },
                actualWorkload : {type : Number, default : 0, required : true},
                workload : {type : Number, default : -1},
                minValue : {type : Number, default : 0},
                maxValue : {type : Number, default : 0},
            },

            data : function() {
                return {
                	mipoct : 'test string. Delete',
                    progressclass : 'progress-bar-info'
                }
            },
            watch : {
                
            },
            computed : {
            	finalMaxValue : function() {
            		return this.actualWorkload > this.maxValue ? this.actualWorkload : this.maxValue;
            	},

            	actualWorkloadPercent : function() {
            		return ((this.actualWorkload / this.finalMaxValue) * 100).toFixed(2);
                },
                targetWorkloadPercent : function() {
            		return ((this.workload / this.finalMaxValue) * 100).toFixed(2);
            	},

            	actualWorkloadVsExpectedPercent : function() {
            		return ((this.actualWorkload / this.workload) * 100).toFixed(2);
            	},

				progresBarStyle : function() {
					var w = this.workload === -1 ? 100 : this.actualWorkloadPercent;
					return 'width: ' + w + '% !important';
                },
                markerStyle : function() {
					var w = this.workload === -1 ? 100 : this.targetWorkloadPercent;
					return 'width: ' + w + '% !important';
				},

            	progressBarClass : function() {
            		if(this.workload === -1)
            			return 'progress-bar-warning';
            		if(this.actualWorkload < this.minValue || this.actualWorkload > this.maxValue)
            			return 'progress-bar-danger';
            		if(this.actualWorkload >= this.minValue && this.actualWorkload <= this.maxValue && this.actualWorkload !== this.workload)
            			return 'progress-bar-success';
            			// return 'progress-bar-warning';
            		if(this.actualWorkload === this.workload)
            			return 'progress-bar-success';

            	},

            	//VueForce vars
            	theme : function() { return this.$store.getters.getTheme },
            	schema : function() { return this.$store.getters.getSchema },

            	//Planit
            	settings : function() { return this.$store.getters.getSettings },
            	permissions : function() { return this.$store.getters.getPermissions},
            	user : function() { return this.$store.getters.getUser },
            	workingCountry : function() { return this.$store.getters.getWorkingCountry },
            },

            methods : {

            }
        });
    </script>

</apex:component>