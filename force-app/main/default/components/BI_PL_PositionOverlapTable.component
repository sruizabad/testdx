<apex:component controller="BI_PL_ReportService">
    
    <script type="text/x-template" id="bi-pl-position-overlap-report">
        <span>
    		<vf-action-status v-model="status" ref="status">
                <vf-spinner v-if="inlineColumns.length == 0 || currentPageResponse === null"  />   
            </vf-action-status>
            
        	<div v-if="inlineColumns.length > 0 && currentPageResponse != null" id="table-wrapper">

                <vf-table
                	:items="currentPageResponse.records"
                	:remote="false"
                	:loader="status.working"
                	:columns="inlineColumns"
                	:exportable="true"
                	:hidable="false"
                    :searchable="searchable"
                	max-container-height="400px">
                	
                </vf-table>
            </div>
        </span>
    </script>
    <script>
        Vue.component('bi-pl-position-overlap-report', {
            template : '#bi-pl-position-overlap-report',


            props : {
                cycle : String,
                hierarchy : String,
                channel : String,
                position : {type : String, default : null},
                //Values : DEFAULT, PVV, LIFT
                fieldSet : {type : String, default : 'DEFAULT'},
                searchable : {type : Boolean, default : false}
            },
            data : function() {
               
                return {

                    fieldForces : [],


                    currentPageResponse : null,

                    status : {
                    	working : true,
                    	level : 'info',
                    	show : true
                    },

                    paginatorModel : {
                    	currentPage : 1,
						totalRecords : 0,
						totalPages : 10,
						pageSize : 20
                    }
                }
            },
            computed : {

                theme : function() { return this.$store.getters.getTheme; },
            	totalRecords : function() {
            		return this.currentPageResponse ? this.currentPageResponse.totalRecords : 'unknown'; 
            	},
            	limitWarning : function() {
            		return this.currentPageResponse ? this.currentPageResponse.totalRecords > 50000 : false;
            	},

            	pageItems : function() {
            		if(this.currentPageResponse) {
            			return this.currentPageResponse.records;
            		}
            		return [];
            	},

            	inlineColumns : function() {
                    var cols = [
                        {label : 'Position', field : 'name'}

                    ];

                    if(this.fieldForces.length > 0) {
                        _.each(this.fieldForces, function(ff){
                            cols.push({label : ff, field : 'fieldForceCount.'+ff });
                        });
                    }

                    cols = cols.concat([ 
                        {label : 'Total', field : 'total'},
                        {label : 'Overlaps', field : 'overlap'},
                        {
                            label : 'Pctg',
                            value : function(item){
                                return ((item.overlap / item.total) * 100).toFixed(2) + '%';
                            }
                        }
                    ]);
            		return cols;
            	}


            },
            mounted : function() {
            	// this.getColumns(this.cycle, this.hierarchy, this.channel, this.position);
				this.getTablePage(this.cycle, this.hierarchy, this.channel, this.position);
            },

            beforeDestroy : function() {
            	this.currentPageResponse = null;
            	this.allColumns = [];
            },

            watch : {
            	currentPageResponse : {
            		handler : function(nv){
            			if(nv) {
	            			if(nv.totalRecords > 50000) {
	            				// this.status.level = 'warning';
                                this.$refs.status.showMessage('warning', 'Number of records > 50000. ');
	            			}
	            			this.paginatorModel.totalPages = nv.totalPages;
	            			this.paginatorModel.totalRecords = nv.totalRecords;
            			} 
            		},
            		deep : true
            	}
            },

            methods : {
            	onNextPage : function() {
            		this.getTablePage(this.cycle, this.hierarchy, this.channel, this.position);
            	},

               getTablePage : function(cycle, hier, channel, position){
           			var self = this;
    				this.status.working = true;
           			var offset = this.lastReponse  ? this.lastReponse.lastIdOffset : null;
           			var pageSize = this.paginatorModel  ? this.paginatorModel.pageSize : null;

           			function handleResponse(resp, ev) {
                        if(ev.statusCode === 200) {
                            var ffs = [];
                            _.each(resp.records, function(rec){
                                ffs = _.union(ffs, _.keys(rec.fieldForceCount));
                            });

                            self.fieldForces = ffs;
                            self.currentPageResponse = resp;
                            self.status.working = false;
                            self.status.show = false;
                        } else {
                            self.status.working = false;
             //                self.status.show = true;
             //                self.status.level = 'error';
    					    // self.status.messages = [ev.message];
                                this.$refs.status.showMessage('error', 'Error getting overlap page. ' + ev.message);

                        }
           			}

               		BI_PL_ReportService.getPositionOverlapReport(null, cycle, hier, channel, position, null, null, null, 
                        handleResponse, 
                        {escape : false, timeout : 120000, buffer : false}
                    );
               },

            }
        });
    </script>

    <style type="text/css">
    	.spinnerWrapperCenter {
    		text-align: center;
    	}
    </style>
</apex:component>