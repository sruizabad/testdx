<apex:component controller="BI_PL_PlanitAdminCtrl" >
	   
    <style>
        textarea#actionReport {
            width: 100% !important;
        }
        label.detail-label {
            font-weight: bold !important;
            margin-right: 0.5em;
        }
        div.panel-spaced {
            margin-top: 1em;
        }
        .inline-b {
            display: inline-block !important;
            margin-right: 0.5em;
        }

        .margin-bottom {
            margin-bottom: 0.5em;
        }
        .margin-right {
            margin-right: 0.5em;
        }

    </style>
    
    <apex:actionStatus id="reportStatus">
        <apex:facet name="start">
            <img src="/img/loading32.gif" />
        </apex:facet>
        <apex:facet name="stop">
        </apex:facet>
    </apex:actionStatus>

    <apex:form id="filtersForm">

        <div class="panel panel-default panel-spaced">
            <div class="panel-body">
        		<div class="row">
                    <div class="col-md-6">
                        <div class="row margin-bottom">    
                            <div class="col-md-2 text-right">
                                 <label class="control-label detail-label">{!$Label.BI_SAP_DM_Cycle}</label> 
                            </div>
                            <div class="col-md-10 text-left">
                                <apex:outputPanel id="cycleArea" styleClass="inline-b">
                                    <apex:selectList id="selectCycleActionReport" value="{!selectedCycle}" size="1" styleClass="form-control navbar-dropdown">
                                        <apex:selectOptions value="{!lstCycles}" />     
                                        <apex:actionSupport event="onchange" action="{!cycleChanged}" reRender="cycleDetails, hierarchyContainer" status="reportStatus"/>
                                    </apex:selectList>
                                </apex:outputPanel>
                            </div>
                        </div>
                        
                        <div class="row margin-bottom">    
                            <div class="col-md-2 text-right">
                                <label class="control-label detail-label">{!$ObjectType.BI_PL_Position_cycle__c.fields.BI_PL_Hierarchy__c.Label}</label>
                            </div>
                            <div class="col-md-10 text-left">
                                <apex:outputPanel id="hierarchyContainer"  layout="block">
                                    <apex:selectList id="selectHierarchyActionReport" value="{!selectedHierarchy}" size="1" styleClass="form-control navbar-dropdown"  disabled="{!selectedCycle == null}" onchange="onHierarchyChange(this); return false">
                                        <apex:selectOptions value="{!lstHierarchies}" />
                                        <apex:actionSupport event="onchange" status="reportStatus"/>     
                                    </apex:selectList>
                                </apex:outputPanel>
                            </div>
                        </div>

                        <div class="row margin-bottom">    
                            <div class="col-md-2 text-right">
                                <label class="control-label detail-label">{!$ObjectType.BI_PL_Channel_detail_preparation__c.fields.BI_PL_Channel__c.Label}</label>
                            </div>
                            <div class="col-md-10 text-left">
                                <apex:selectList id="selectChannelActionReport" value="{!selectedChannel}" size="1" styleClass="form-control navbar-dropdown" >
                                    <apex:selectOptions value="{!activeChannels}" />
                                </apex:selectList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <apex:outputPanel id="reportForm" layout="none">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="submit" onclick="getReport('transfer'); return false;" value= "{!$Label.BI_PL_transfer_share_file}" class="btn btn-default btn-block" />
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" onclick="getReport('adds'); return false;" value="{!$Label.BI_PL_adds_file}" class="btn btn-default btn-block" />
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" onclick="getReport('drops'); return false;" value="{!$Label.BI_PL_drops_file}" class="btn btn-default btn-block"/>
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" onclick="getReport('alignment'); return false;" value="{!$Label.BI_PL_alignment_HCO_file}" class="btn btn-default btn-block"/>
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" onclick="getReport('alignment'); return false;" value="{!$Label.BI_PL_alignment_HCO_proposed_file}" class="btn btn-default btn-block"/>
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" onclick="getReport('details', 'planned', null); return false;" value="{!$Label.BI_PL_callplan_proposed_file}" class="btn btn-default btn-block" />
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" onclick="getReport('details', 'adjusted', null); return false;" value="{!$Label.BI_PL_callplan_revised_file}" class="btn btn-default btn-block" />
                                </div>

                            </div>
                        </apex:outputPanel>
                    </div>
                </div>
            </div>
        </div>
    </apex:form>

    <!--Filltered data summary -->
    <apex:outputPanel id="cycleDetails" layout="none">
        <apex:outputPanel rendered="{!selectedCycle != null || selectedCycle != ''}" layout="block" styleClass="inline-b">
            <div class="badge margin-right">
                <label class="control-label detail-label">{!$ObjectType.BI_PL_Cycle__c.fields.BI_PL_Field_force__c.Label}</label>
                <apex:outputText value="{!IF(selectedCycle!='',mapFieldForce[selectedCycle],'')}" id="outputFieldForce" styleClass="text-left"/>
            </div>
            <div class="badge margin-right">
                <label class="control-label detail-label">{!$ObjectType.BI_PL_Cycle__c.fields.BI_PL_Start_date__c.Label}</label>
                <apex:outputText value="{0, date, MMMM d','  yyyy}" id="outputStartDate" styleClass="text-left">
                    <apex:param value="{!IF(selectedCycle!='',mapStartDates[selectedCycle],"")}" />
                </apex:outputText>
            </div>
            <div class="badge margin-right">
                <label class="control-label detail-label">{!$ObjectType.BI_PL_Cycle__c.fields.BI_PL_End_date__c.Label}</label>
                <apex:outputText value="{0, date, MMMM d','  yyyy}" id="outputEndDate" styleClass="text-left">
                    <apex:param value="{!IF(selectedCycle!='',mapEndDates[selectedCycle],"")}" />
                </apex:outputText>
            </div>
        </apex:outputPanel>
    </apex:outputPanel>

             
    <apex:outputPanel id="reportArea" layout="none">

        <apex:pageMessages id="reportMessages"/>

        <apex:outputPanel styleclass="panel panel-default panel-spaced" layout="block">
            
            <div class="panel-heading">
                <button type="button" class="btn btn-primary" onclick="saveTextAsFile()" id="actionReportExport" style="display: none !important"> Export </button>
                <span id="actionReportLoader" style="display : none">
                    <img src="/img/loading32.gif" />
                </span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <apex:outputPanel id="actionReport" layout="block">
                            <textarea id="actionReport" name="textarea" rows="50" cols="50" disabled="true">{!actionReport}</textarea>
                        </apex:outputPanel>
                    </div>
                </div>
            </div>
        </apex:outputPanel>
    </apex:outputPanel>


    <script>
        function disableButtons() {
            document.querySelectorAll('[id$="filtersForm"] input[type=submit]').forEach(function(btn) {
                btn.disabled = true;
            });
        }

        function enableButtons(){
            document.querySelectorAll('[id$="filtersForm"] input[type=submit]').forEach(function(btn) {
                btn.disabled = false;
            });
        }


        function finishReportLoad() {
            $('#actionReportLoader').hide();
            $('#actionReportExport').show();
            enableButtons();
        }


        function getReport(type, poa, offset) {

            disableButtons();

            $('#actionReportLoader').show();

            var selectedCycle = document.querySelector('[id$="selectCycleActionReport"]').value;
            var selectedChannel = document.querySelector('[id$="selectChannelActionReport"]').value;
            var selectedHierarchy = document.querySelector('[id$="selectHierarchyActionReport"]').value;
            //clear value
            
            if(!offset)
                document.getElementById('actionReport').value = '';


            switch(type) {
                case 'transfer':
                    BI_PL_PlanitAdminCtrl.getActionTransferShareReport(selectedCycle, selectedHierarchy, selectedChannel, function(resp, ev){
                        document.getElementById('actionReport').value += resp;
                        finishReportLoad();
                    }, {escape : false, timeout : 120000});
                break;

                case 'drops':
                    BI_PL_PlanitAdminCtrl.getActionDropReport(selectedCycle, selectedHierarchy, selectedChannel, function(resp, ev){
                        document.getElementById('actionReport').value += resp;
                        finishReportLoad();
                    }, {escape : false, timeout : 120000});
                break;

                case 'adds':
                    BI_PL_PlanitAdminCtrl.getActionAddReport(selectedCycle, selectedHierarchy, selectedChannel, function(resp, ev){
                        document.getElementById('actionReport').value += resp;
                        finishReportLoad();
                    }, {escape : false, timeout : 120000});
                break;

                case 'alignment':
                     finishReportLoad();
                break;

                case 'details':
                    BI_PL_PlanitAdminCtrl.getDetailsReport(selectedCycle, selectedHierarchy, selectedChannel,  poa,  offset, function(resp, ev){
                        if(resp.offset != null) {
                            document.getElementById('actionReport').value += resp.body;
                            getReport('details', poa, resp.offset);
                        } else {
                            document.getElementById('actionReport').value += resp.body;
                            finishReportLoad();
                            
                        }
                    }, {escape : false, timeout : 120000});
                break;
            }

        }

    	function saveTextAsFile() {
          var textToWrite = document.getElementById('actionReport').value.replace(/([^\r])\n/g, "$1\r\n");

          var textFileAsBlob = new Blob([ textToWrite ], { type: 'text/plain' });
          var fileNameToSaveAs = "actionReport.txt";

          var downloadLink = document.createElement("a");
          downloadLink.download = fileNameToSaveAs;
          downloadLink.innerHTML = "Download File";
          if (window.webkitURL != null) {
            // Chrome allows the link to be clicked without actually adding it to the DOM.
            downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
          } else {
            // Firefox requires the link to be added to the DOM before it can be clicked.
            downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
            downloadLink.onclick = destroyClickedElement;
            downloadLink.style.display = "none";
            document.body.appendChild(downloadLink);
          }

          downloadLink.click();
        }

        function onChannelChange(elm) {
            if(elm.value)
                var btn = $('[id$="dropsButton"]')[0].disabled = false;
        }

        function onHierarchyChange(elm) {
            if(elm.value)
                enableButtons();
        }


        //begin with buttons disabled
        disableButtons();

	</script>



</apex:component>