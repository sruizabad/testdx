<apex:component controller="BI_PL_PreparationCloneCtrl">
    <style>
        textarea#actionReport {
            width: 100% !important;
        }
    </style>
    <script id="bi-pl-cycle-clone" type="text/x-template">
        <vf-card title="{!$Label.BI_SP_Clone}" icon-class="slds-icon-action-script" icon="action/clone">
            <div slot="title"> 
                {!$Label.BI_SP_Clone}
                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconCyclePath" />&nbsp;<strong>{!$Label.BI_PL_Cycle}</strong></span>
                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconSingleHierarchyPath" />&nbsp;<strong>{!$Label.BI_PL_Single_hierarchy}</strong></span>
            </div>
            <div slot="buttons">
                <button type="button" :class="theme.button.primary" v-on:click="showCardBody = true" v-if="showCardBody === false" > {!$Label.BI_PL_Begin} </button>
                <button type="button" :class="theme.button.neutral" v-on:click="showCardBody = false" v-if="showCardBody === true"> {!$Label.BI_PL_Cancel}</button>
            </div>
            <div slot="body" id="preparationClone" v-if="showCardBody">

                <vf-action-status v-model="status" ref="status">

                    <!--  ERROR MESSAGE -->
                    <p v-if="!cycle || !hierarchy">{!$Label.BI_PL_Select_cycle_hierarchy}</p>
                    <p v-else-if="!hasAllPreparationsApproved && cycle && hierarchy">{!$Label.BI_SAP_DM_Not_Approved}</p>
                    <p v-else-if="!selectedTargetCycle">{!$Label.BI_PL_Select_target_cycle}</p>
                    <p v-else-if="cycle && selectedTargetCycle && cyclesAreTheSame">{!$Label.BI_PL_Cycles_cant_be_same}</p>
                    <p v-else-if="selectedTargetHierarchy && !allPreparationsWithNoPositionReasigned">{!$Label.BI_PL_All_preparations_target_position}</p>
                    <div v-else-if="!loading && completed" class="alert alert-success">
                        {!$Label.BI_PL_Process_completed}
                    </div>

                </vf-action-status>
                <!-- <cycle-hierarchy-selector 
                    :vertical="true"
                    text-class="slds-text-title_caps"
                    :totals.sync="selectedTotals"
                    :country.sync="country"
                    :cycle.sync="cycle"
                    :hierarchy.sync="hierarchy"
                    :hierarchies.sync="selectedHierarchies"
                    :show-country="true"
                    :show-channel="false"
                    v-on:error="addError">
                </cycle-hierarchy-selector> -->



                <cycle-hierarchy-selector 
                    class="slds-theme_shade slds-p-top_small slds-p-bottom_small"
                    :vertical="false"
                    text-class="slds-text-title_caps"
                    :totals.sync="targetTotals"
                    :country="country"
                    :cycle.sync="selectedTargetCycle"
                    :hierarchy.sync="selectedTargetHierarchy"
                    :hierarchies.sync="selectedTargetHierarchies"
                    cycle-label="Target cycle"
                    hierarchy-label="Target hierarchy"
                    :show-country="false"
                    :show-channel="false"
                    :enable-all-hierarchies-option="false"
                    v-on:error="addError">
                </cycle-hierarchy-selector>



                <!-- NORMAL CLONE TABLE -->
                <span v-show="!selectedTargetHierarchy">
                    <vf-table
                            :selectable="false"
                            :exportable="false"
                            :searchable="true"
                            :items="sourcePreparations"
                            sobject="BI_PL_Preparation__c"
                            sobject-fields="Name, BI_PL_Status__c, BI_PL_Start_date__c, BI_PL_End_date__c, Owner.Name, BI_PL_Position_name__c"
                        >

                    </vf-table>
                </span>

                <span v-show="selectedTargetHierarchy">
                    <!-- CLONE WITH MERGING TABLE -->
                    <vf-table
                            :selectable="false"
                            :exportable="false"
                            :searchable="true"
                            :items="sourcePreparations"
                            sobject="BI_PL_Preparation__c"
                            sobject-fields="Name, BI_PL_Status__c, BI_PL_Start_date__c, BI_PL_End_date__c, Owner.Name"
                        >
                        <template slot="header">
                            <vf-thead :column="columns[0]" :order-by="orderByNewPosition">
                                {!$Label.BI_PL_New_position}
                            </vf-thead>
                        </template>
                        <template slot="body" scope="row">
                            <vf-tdata :column="columns[0]">
                                <!-- Display the picklist if the position is not found in the target hierarchy -->

                                <vf-spinner v-if="loading" size="small"/>

                                <template v-if="!loading">
                                    <select :class="theme.input" v-if="!availableTargetPositionCycles[row.item.BI_PL_Position_name__c]" v-model="newPreparationPositionsMap[row.item.Id]" :disabled="!hasAllPreparationsApproved">
                                        <option :value="undefined">{!$Label.Common_None_vod}</option>
                                        <option v-if="availableTargetPositionCycles" v-for="targetPositionCycle in availableTargetPositionCycles" :value="targetPositionCycle.PositionCycleExternalId" >{{targetPositionCycle.PositionName}}</option>
                                    </select>
                                    <template v-if="availableTargetPositionCycles[row.item.BI_PL_Position_name__c]">
                                        {{row.item.BI_PL_Position_name__c}}
                                    </template>
                                </template>
                            </vf-tdata>
                        </template>

                    </vf-table>
                </span>
           

                <button type="button" :class="theme.button.primary" class="slds-m-left_small" :disabled="!allowedToClone" v-on:click="executeClone">{!$Label.BI_SAP_DM_Clone}</button>

            </div>
            <div slot="body" class="slds-p-around_medium" v-if="!showCardBody">
                {!$Label.BI_PL_Clone_info}
            </div>
        </vf-card>
    </script>
    <script>
        var vueInstance = Vue.component('bi-pl-cycle-clone', {
            template : '#bi-pl-cycle-clone',
            // el: '#splitCycle',
            created : function(){
            },

            mounted : function(){
                // this.status = this.$refs.status.createModel();
                this.checkStatus();
            },

            updated : function() {
                this.checkStatus();
            },

            props : {
                country : {type : String, default : null},
                cycle : {type : Object, default : null},
                hierarchy : {type : String, default : null},
                hierarchies : {type: Array, default: null},
            },

            data: function(){
                return {
                    showCardBody : false,
                    showProgressBar : false,

                    status : {
                        show : false,
                        level : 'error',
                        messages : []
                    },
                    columns : [
                        {
                            key : 'position',
                            hidden : false,
                            hiddable : false
                        }
                    ],
                    /**
                     *  key = preparationId, value = new position Id.
                     */
                    newPreparationPositionsMap : {},

                    // cycle : null,
                    // hierarchy : null,

                    selectedTargetCycle : null,
                    selectedTargetHierarchy : null,

                    loading : false,
                    completed : false,

                    sourcePreparations : [],
                    targetPositionCycles : {},

                    targetTotals : {},
                    selectedTargetCountry : null,
                    selectedTargetHierarchies :  null,

                    orderByNewPosition : function(preparations, col) {
                        console.log('orderByNewPosition');
                        var self = this;
                        return preparations.sort(function(a, b) {
                            var aValue = (self.availableTargetPositionCycles[a.BI_PL_Position_name__c]) ? a.BI_PL_Position_name__c : self.newPreparationPositionsMap[a.Id];
                            var bValue = (self.availableTargetPositionCycles[b.BI_PL_Position_name__c]) ? b.BI_PL_Position_name__c : self.newPreparationPositionsMap[b.Id];
                            return aValue - bValue;
                        })
                    }
                }
            },
            methods : {
                checkStatus : function() {
                    if(this.status.show === false && ( 
                        (!this.hasAllPreparationsApproved && this.cycle && this.hierarchy) 
                        || !this.selectedTargetCycle 
                        || (!this.cycle || !this.hierarchy) 
                        || (this.cycle && this.selectedTargetCycle && this.cyclesAreTheSame)
                        || (this.selectedTargetHierarchy && !this.allPreparationsWithNoPositionReasigned))) {
                        
                        this.status.show = true;
                        this.status.level = 'error';
                    }
                },
                addError : function(msg) {
                    this.showMessage('error', msg);
                },
                showMessage : function(lv, msg) {
                    this.$refs.status.showMessage(lv, msg);
                },
                clearNewPreparationPositionsMap : function(){
                    this.newPreparationPositionsMap = {};
                },

                getTargetPositionCycles : function(){
                    console.log('getTargetPositionCycles');
                    var self = this;

                    this.loading = true;

                    if(this.hierarchy && this.selectedTargetHierarchy && this.selectedTargetCycle){
                        BI_PL_PreparationCloneCtrl.getTargetPositionCycles(this.selectedTargetHierarchy, this.selectedTargetCycle.Id, function(resp, evt){
                            if(evt.statusCode == 200){
                                self.targetPositionCycles = resp;
                                console.log('getTargetPositionCycles Completed ',resp);

                                self.loading = false;
                            }
                        });
                    }
                },

                getSourcePreparations : function(){
                    console.log('getSourcePreparations');
                    var self = this;

                    if(this.hierarchy){
                        BI_PL_PreparationCloneCtrl.getPreparations(this.hierarchy, this.cycle.Id, function(resp, evt){
                            if(evt.statusCode == 200){
                                self.sourcePreparations = resp;
                                console.log('getSourcePreparations Completed ',resp);
                            }
                        });
                    }
                },

                executeClone : function(){
                    var self = this;

                    BI_PL_PreparationCloneCtrl.executeClone(this.cycle.Id, this.hierarchy, this.selectedTargetCycle.Id, this.selectedTargetHierarchy, this.selectedTargetHierarchy != null, this.newPreparationPositionsMap, function(resp, evt){
                        if(evt.statusCode == 200){
                            self.targetHierarchies = resp;
                        }
                    });
                }
            },

            watch : {
                cycle : function(nv, oldv){
                   if(nv !== undefined){                    
                    this.clearNewPreparationPositionsMap();
                    this.targetPositionCycles = {};
                   }
                },
                hierarchy : function(nv){
                    if(nv !== undefined){                       
                        this.getSourcePreparations();
                       }
                },
                selectedTargetHierarchy : function(nv){
                    if(nv !== undefined){                       
                        this.getTargetPositionCycles();
                   }
                }
            },

            computed : {
                theme : function() { return this.$store.getters.getTheme; },

                allPreparationsWithNoPositionReasigned : function(){
                    for(var i=0;i<this.sourcePreparations.length;i++){
                        var p = this.sourcePreparations[i];
                        if(!this.availableTargetPositionCycles[p.BI_PL_Position_name__c] && !this.newPreparationPositionsMap[p.Id])
                            return false;
                    }

                    return true;
                },
                availableTargetPositionCycles : function(){
                    var output = {};

                    for(var k in this.targetPositionCycles){
                        if(this.newPreparationPositionsMap[k] == "" || this.newPreparationPositionsMap[k] == undefined)
                            output[k] = this.targetPositionCycles[k];
                    }

                    return output;
                },
                cyclesAreTheSame : function(){
                    return this.cycle == this.selectedTargetCycle;
                },
                cycleRecord : function(){
                    return this.cycles[this.cycle];
                },
                selectedTargetCycleRecord : function(){
                    return this.targetCycles[this.selectedTargetCycle];
                },
                targetCycles : function(){
                    return this.cycles;
                },
                hasAllPreparationsApproved : function(){
                    for(var i = 0;i<this.sourcePreparations.length;i++){
                        if(this.sourcePreparations[i].BI_PL_Status__c != 'Approved')
                            return false;
                    }
                    return true;
                },
                allowedToClone : function(){
                    return this.cycle && this.hierarchy && this.selectedTargetCycle && !this.cyclesAreTheSame
                            && (!this.selectedTargetHierarchy || (this.selectedTargetHierarchy && this.allPreparationsWithNoPositionReasigned)) && this.hierarchies.length == 1;
                },
                iconCyclePath : function() {
                    return !this.cycle ? 'utility/error' : 'utility/check';
                },
                iconSingleHierarchyPath  : function() {
                    return !this.hierarchies || (this.hierarchies && this.hierarchies.length !== 1)  ? 'utility/error' : 'utility/check';

                }
            },

        });
    </script>
</apex:component>