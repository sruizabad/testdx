<apex:component controller="BI_PL_PreparationTargetAdjModalCtrl">

<script type="text/x-template" id="preparation-target-adjustment-modal">
        <!-- <div class="bs modal fade" id="preparationTargetAdjustmentModal" tabindex="-1" role="dialog" aria-hidden="true"> -->
        <span>
            <vf-modal v-if="modalDisplayed" width="60vw" v-on:close="cancel()">
                <!-- Global actions -->
                <template slot="header">
                    <h2 class="modal-title" id="addTargetModalLabel">{{target ? target.record.BI_PL_Target_customer__r.Name : ''}} {!$Label.BI_PL_Adjustments}</h2>
                </template>
                <template slot="body">
                    <div v-if="remoteError != null" class="alert alert-danger" role="alert">
                        {{remoteError}}
                    </div>



                    <vf-table 
                            :columns="columns"
                            :items="target.currentTargetChannel.details"
                            :exportable="false"
                            :hidable="false"
                            :actionable ="editMode">

                            <template slot="globalActions">
                                <button v-if="showAddProduct" class="btn btn-default btn-sm" style="padding : 0px !important; border : 0px !important">
                                    <label>{!$Label.BI_SAP_Add_Product}</label>
                                    <select class="form-control navbar-dropdown" style="display: inline-block !important" v-model="selectedProductToAdd" :disabled="addingProduct">
                                        <option v-bind:value="undefined" v-if="availableProductPairs.length == 0">- {!$Label.BI_PL_No_products_available} -</option>
                                        <option v-bind:value="undefined" v-else>- {!$Label.BI_PL_Choose_product} -</option>
                                        <option v-for="productPair in availableProductPairs" v-bind:value="productPair"> {{productPair.Name}}</option>
                                    </select>
                                    <button type="button" class="btn btn-default" v-on:click="addNewProduct()" :disabled="addingProduct || selectedProductToAdd == undefined">
                                        <!-- <div style="float:left" class="loader" v-if="addingProduct"></div> -->
                                        <vf-spinner size="small" v-if="addingProduct" />
                                        <template v-if="!addingProduct">
                                            {!$Label.BI_PL_Add_Label}
                                        </template>
                                    </button>
                                </button>
                            </template>

                            <template slot="header">
                                <vf-thead :column="adjustmentColumn"> {!$Label.BI_PL_Adjusted_Label} </vf-thead>
                            </template>


                            
                            <template slot="actions" scope="row" >
                        
                                <vf-dropdown v-model="selectedAction" v-on:change="applyAction(row.item, selectedAction)">
                                    <!-- None -->
                                    <vf-dropdown-option label="{!$Label.Common_None_vod}" value="none" />
                                    <!-- Delete Product -->
                                    <vf-dropdown-option v-if="availableToDelete(row.item)"
                                        label="{!$Label.BI_PL_Delete_Label}"
                                        image="{!URLFOR($Resource.BI_PL_Icons,'remove.png')}"
                                        v-bind:value="'Delete'" />
                            
                                
                                </vf-dropdown>

                            </template>
                          
                            

                            <template slot="body" scope="row">
                                <vf-tdata :column="adjustmentColumn">

                                    <span style="max-width: 70px">
                                        <detail-adjustment-input 
                                            v-model="row.item"
                                            :target="target"
                                            :edit-mode="editMode"
                                            :business-rules-parent="businessRulesParent"
                                            @update="onUpdateAdjustment"
                                            />
                                    </span>
                                </vf-tdata>
                            </template>



                        </vf-table>
                </template>
                <template slot="footer">
                    <button type="button" class="btn btn-default" v-on:click="cancel()">{!$Label.BI_SAP_Close}</button>
                </template>
            </vf-modal>
        </span>
    </script>
    <script>
        var vueTargetTransferAndShareModal = Vue.component('preparation-target-adjustment-modal', {
            template : '#preparation-target-adjustment-modal',
            props : {
                // allowedToAddProducts : Boolean,
                preparation : Object,
                target : Object,
                businessRulesParent : Object,
                detailMode : { type : Boolean, default : true },
                editMode : { type : Boolean, default : false },
                possibleProductPairsFromBusinessRules : Object,
                setup : Object,
                positionId : String,
                currentView : Object,
            },
            mixins : [BI_PL_BusinessRuleMixin],

            data : function(){
                return {    
                    selectedAction : null, 
                    selectedProductToAdd : undefined,     
                    columns : [
                        { hidable : true, hidden : false, field : 'BI_PL_Planned_details__c', label : 'P'},
                        { hidable : true, hidden : false, field : 'BI_PL_Product__r.Name', label : '{!$Label.BI_PL_Primary_product}'},
                        { hidable : true, hidden : false, field : 'BI_PL_Secondary_product__r.Name', label : '{!$Label.BI_PL_Secondary_product}'},
                    ],

                    adjustmentColumn : { hidable : true, hidden : false, field : 'BI_PL_Adjusted_details__c', key : 'adjustment'},


                    modalDisplayed : false,
                    newDetailCreated : false,
                    addingProduct : false,
                    remoteError : null
                }
            },
            watch : {
                businessRulesParent : {
                    handler : function(newValue) {
                        console.log("businessRulesParent", newValue);
                        if(newValue) this.businessRulesGrouped = newValue;
                    },
                    immediate : true
                },
                modalDisplayed : function(){
                    this.newDetailCreated = false;
                }
            },
            computed : {
                
                showAddProduct : function() {
                    if(this.preparation.BI_PL_Position_cycle__r.BI_PL_Confirmed__c == false){
                        if(this.allowedToAddProducts){
                            if(this.allowedToAddProductsOnlyAdded ) {
                                if(this.target.record.BI_PL_Added_manually__c){
                                    return true;
                                } else {
                                    return false;
                                }
                            } else {
                                return true;
                            }
                        }
                    }

                    return false;
                },

                allowedToAddProducts : function() {
                    if(this.currentView !== undefined) {
                        return this.currentView.BI_PL_Allowed_to_add_products__c;
                    }
                    return false;
                },

                allowedToAddProductsOnlyAdded : function() {
                    if(this.currentView !== undefined) {
                        return this.currentView.BI_PL_Allow_add_prod_only_added_target__c;
                    }
                    return false;
                },
                availableProductPairs : function(){
                    var output = [];
                    if(this.target != undefined){

                        var brAvailable;
                        if(this.setup.settings.BI_PL_Allow_all_products_default__c){
                            /*for(var pairId in this.possibleProductPairsFromBusinessRules){
                                var productPair = this.possibleProductPairsFromBusinessRules[pairId];
                                if(_.find(this.target.currentTargetChannel.details, function(o){
                                            return  o.BI_PL_Product__c == productPair.primary.Id && (!productPair.hasSecondary
                                                                                    || (productPair.hasSecondary && o.BI_PL_Secondary_product__c == productPair.secondary.Id));
                                        }
                                    ) == undefined)
                                    output.push(productPair);
                            }*/
                            console.log("availableProductPairs allow all", this.target.record.BI_PL_Specialty__c, this.positionId)
                            brAvailable = this.getPrimaryAndSecondaryProductPairsWithoutForbidden(this.target.record.BI_PL_Specialty__c, this.positionId);

                        }else{
                            console.log("availableProductPairs don't allow");
                            brAvailable =  this.getProductPairsBySpecialty(this.target.record.BI_PL_Specialty__c);
                        }

                        var fromTarget = this.getTargetProductPairs();

                        console.log('availableProductPairs ', brAvailable, fromTarget);

                        output = _.differenceBy(brAvailable, fromTarget, 'Id')
                    }

                    return output;
                }
            },
            mounted : function(){
                this.modalDisplayed = true;
            },
            beforeDestroy : function(){
                this.modalDisplayed = false;
            },

            methods : {

                applyAction: function(record, action) {
                    if(action == 'Delete'){
                        console.log(record.Id);

                        this.deleteProduct(record);
                    }
                    
                    var self = this;
                    Vue.nextTick(function () {
                        self.selectedAction = 'none';
                    });
                },

                availableToDelete : function(record){
                    return record.BI_PL_Added_manually__c;
                    
                },

                deleteProduct : function(record){

                    var self = this;

                    
                    if(confirm('Are you sure you want to delete this product combination?')){

                        BI_PL_PreparationExt.deleteProductRecord(record, function(response,event){
                            if(event.statusCode == 200) {

                                var index = _.findIndex(self.target.currentTargetChannel.details, function(o){
                                    return record.Id == o.Id;
                                })
                                //var A = record.BI_PL_Adjusted_details__c;                                
                                self.$delete(self.target.currentTargetChannel.details,index);
                                if(self.target.currentTargetChannel.details.length == 0){
                                    self.target.currentTargetChannel.record.BI_PL_Number_details__c = 0;
                                    
                                }else{
                                    self.target.currentTargetChannel.record.BI_PL_Number_details__c = self.target.currentTargetChannel.details.length;
                                }                                
                                self.onUpdateAdjustment();
                                
                            } else {
                                console.error('Remoting error when deleting detail record');
                            }


                        });
                    }
                },
                checkNumberOfProductsBeforeDelete : function(){


                },

                getTargetProductPairs : function() {
                    let output = [];
                    if(this.target && this.target.currentTargetChannel){
                        for(var i=0;i<this.target.currentTargetChannel.details.length;i++){
                            var detail = this.target.currentTargetChannel.details[i];
                            if(detail.BI_PL_Product__c){
                                var productPair = {};

                                productPair.primary = {
                                    Id : this.target.currentTargetChannel.details[i].BI_PL_Product__c,
                                    Name : this.target.currentTargetChannel.details[i].BI_PL_Product__r.Name
                                };

                                if(this.target.currentTargetChannel.details[i].BI_PL_Secondary_product__c){
                                    productPair.secondary = {
                                        Id : this.target.currentTargetChannel.details[i].BI_PL_Secondary_product__c,
                                        Name : this.target.currentTargetChannel.details[i].BI_PL_Secondary_product__r.Name
                                    };
                                }
                                productPair.hasSecondary = productPair.secondary != undefined;
                                productPair.Id = productPair.primary.Id + ((productPair.hasSecondary) ? productPair.secondary.Id : '');
                                productPair.Name = productPair.primary.Name + ((productPair.hasSecondary) ? ' - '+productPair.secondary.Name : '');
                            }
                            output.push(productPair);
                        }
                    }
                    return output;
                },

                onUpdateAdjustment : function() {

                  
                    this.target.currentTargetChannel.record.BI_PL_Edited__c = true;

                    var maxValue = -Infinity;
                    var sumValue = 0;

                    for(var i=0;i<this.target.currentTargetChannel.details.length;i++){
                        var adj = this.target.currentTargetChannel.details[i].BI_PL_Adjusted_details__c;
                        sumValue += adj;

                        // console.log("DETAIL : ", target.currentTargetChannel.details[i])
                        // console.log(target.currentTargetChannel.details[i].BI_PL_Adjusted_details__c )
                        if(adj > maxValue)
                            maxValue = adj;
                    }
                    // console.log(maxValue);
                    //if(target.currentTargetChannel.record.BI_PL_Max_adjusted_interactions__c < adjustedValue)
                    this.target.currentTargetChannel.record.BI_PL_Max_adjusted_interactions__c = maxValue;
                    this.target.currentTargetChannel.record.BI_PL_Sum_adjusted_interactions__c = sumValue;

                    // this.$emit('detail-adjusted', target.currentTargetChannel);
                   // this.$emit('update', target, detail);



                    this.$emit('update', this.target);
                },

				cancel : function(){
                    this.modalDisplayed = false;
                    this.$emit('close', this.newDetailCreated);
                },
                addNewProduct : function(){
                    var self = this;

                    this.addingProduct = true;
                    this.remoteError = null;

                    var channel = this.target.currentTargetChannel.record.BI_PL_Channel__c;

                    console.log('addNewProduct: ', channel, this.target, this.preparation.BI_PL_External_id__c, this.target.currentTargetChannel.record.BI_PL_External_id__c, this.selectedProductToAdd.primary, (this.selectedProductToAdd.hasSecondary) ? this.selectedProductToAdd.secondary : null);
                    //var productPairId = this.selectedProductToAdd.primary.Id + ((this.selectedProductToAdd.hasSecondary) ? this.selectedProductToAdd.secondary.Id : '');  
                    var productPairId = this.$planit.generateProductPairId(this.selectedProductToAdd.primary,this.selectedProductToAdd.secondary);
                    var a = 0;          
                    if(this.businessRulesParent.bySegment.undefined && this.businessRulesParent.bySegment.undefined[productPairId]){
                        a = this.businessRulesParent.bySegment.undefined[productPairId].BI_PL_Min_value__c;
                    }
                    BI_PL_PreparationTargetAdjModalCtrl.addNewDetail(channel, this.target, this.preparation.BI_PL_External_id__c, this.target.currentTargetChannel.record.BI_PL_External_id__c, this.selectedProductToAdd.primary, (this.selectedProductToAdd.hasSecondary) ? this.selectedProductToAdd.secondary : null, a,
                            function(response, event){
                                if(event.statusCode == 200) {
                                    console.log("BI_PL_PreparationTargetAdjModalCtrl - RESPONSE: ", response);
                                    for(var i = 0; i<response.details.length; i++){
                                        self.checkBusinessRulesForDetail(response.details[i],false);
                                    }

                                    delete response.record.BI_PL_Details_Preparation__r;

                                    
                                    self.$set(self.target, 'currentTargetChannel', response);
                                    self.$set(self.target.targetChannels, channel, response);
                                    //self.target.targetChannels[self.target.currentTargetChannel.record.Id] = self.target.currentTargetChannel;
                                    
                                    
                                    
                                    self.target.currentTargetChannel.record.BI_PL_Number_details__c = self.target.currentTargetChannel.details.length;
                                    
                                    self.newDetailCreated = true;
                                    self.selectedProductToAdd = undefined;
                                    self.onUpdateAdjustment();

                                } else {
                                    console.error('Remoting error when adding the new detail.');
                                    self.remoteError = event.message;
                                }
                                self.addingProduct = false;
                            }
                    );
                }
            }
        });

    </script>

</apex:component>