<!--
	14-06-2017	
-->
<apex:component controller="BI_PL_TrimHierarchyCtrl" allowDML="true">
	<!-- Requires jQuery in the parent page -->

		<style>
			.node {
    			border-radius: 10px;
				padding:10px;
				margin:1px;
				display:inline-block;
			}
			.nodeBackground {
				background-color:#87bae5;
			}
			.trimmed {
				background-color:#c3c3c6;
			}
			.parentTrimmed {
				background-color:#e1dbb1;
			}
			.node input {
				vertical-align: text-bottom;
			}
			.collapseUncolapseButton {
    			border-radius: 15px;
				padding:5px;
				background-color:white;
				display:inherit;
			}
			.nodesHiearchy{
				list-style:none;
				padding-left: 2rem !important;
			}
			.legendColorBlock {
			    width: 10px;
			    height: 10px;
			    display: inline-block;
			    border-radius: 10px;
			    margin-right: 5px
			}
		</style>

    	<script type="text/x-template" id="bi-pl-trim-hierarchy">		
			<span>
				<vf-card title="{!$Label.BI_PL_TrimHierarchy}" icon-class="slds-icon-action-script" icon="utility/cut">
				<!-- 	<div slot="buttons">
	        		<button type="button" :class="theme.button.primary" v-on:click="showCardBody = true" v-if="showCardBody === false" > Begin </button>
	        		<button type="button" :class="theme.button.neutral" v-on:click="showCardBody = false" v-if="showCardBody === true"> Cancel</button>
	        	</div> -->
	        	<div slot="title">
	                {!$Label.BI_PL_TrimHierarchy}
	                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconCyclePath" />&nbsp;<strong>{!$Label.BI_PL_Cycle}</strong></span>
	                <span class="slds-badge slds-badge_lightest"><vf-svg-icon size="xx-small" v-bind:path="iconSingleHierarchyPath" />&nbsp;<strong>{!$Label.BI_PL_Single_hierarchy}</strong></span>
	               
            	</div>
			    	<div slot="body" class="slds-p-around_x-small">
						<h4>{!$Label.BI_PL_ImportBitmanStep2}</h4>
						<p style="margin:10px 0 10px !important">{!$Label.BI_PL_After_trimming_update_owner}</p>
						<div>

					        <!-- LEGEND -->
					        <div>
						        <span class="legendColorBlock nodeBackground"></span>
								<span>{!$Label.BI_SAP_Position_Label}</span>

						        <span class="legendColorBlock trimmed"></span>
								<span>{!$Label.BI_PL_Position_to_remove}</span>

						        <span class="legendColorBlock parentTrimmed"></span>
								<span>{!$Label.BI_PL_Position_to_update_parent}</span>
							</div>

							<div class="slds-box slds-box_x-small slds-m-around_x-small">
								<p v-if="errorMessage" class="not-approved-error text-center">
									{{errorMessage}}
								</p>
		                      
		                        <div class="loader" v-else-if="loading"></div>

								<div v-else>
									<ul class="nodesHiearchy">
										<bi-pl-tree-node v-if="rootNodeId && hierarchyNodes[rootNodeId]"
											:is-root-node="true"
											:nodeId="rootNodeId"
											:hierarchy-nodes="hierarchyNodes"
											:nodes-to-replace-map="nodesToReplaceMap"
											/>
									</ul>
								</div>
							</div>

							<button type="button" :class="theme.button.primary" v-bind:disabled="!cycle || !hierarchy || loading || errorMessage" v-on:click="executeTrimHierarchy">
								{!$Label.BI_PL_TrimCycle}
							</button>
						</div>
					</div>
					<!-- <div slot="body" class="slds-p-around_medium" v-if="!showCardBody">
			    		{!$Label.BI_PL_ImportBitmanStep2}
			    	</div> -->
			    </vf-card>
				
			</span>
		</script>

        
        <!-- TEMPLATES -->
        <script type="text/x-template" id="bi-pl-tree-node">
        	<li >
        		<span :class="{node : true, nodeBackground : true, trimmed : childTrimmed && !isRootNode, parentTrimmed : parentTrimmed && !childTrimmed && !isRootNode}">
        			<button v-if="hasChildren" class="collapseUncolapseButton" :class="theme.button.icon.primary" @click="open=!open">
        				<!-- {{(open) ? '-' : '+'}} -->
        				<vf-svg-icon path="utility/add"  size="button" v-if="!open" />
        				<vf-svg-icon path="utility/dash"  size="button" v-if="open" />
        			</button>
	        		<label :class="theme.label">{{hierarchyNodes[nodeId].positionName}}</label>
	        		<input type="checkbox" v-model="nodesToReplaceMap[hierarchyNodes[nodeId].positionCycleId]" :disabled="isRootNode || !hasChildren" />
        		</span>
	        	<ul v-show="open" class="nodesHiearchy">
					<bi-pl-tree-node v-for="childId in hierarchyNodes[nodeId].childrenIds"
						:key="childId"
						:nodeId="childId"
						:hierarchy-nodes="hierarchyNodes"
						:nodes-to-replace-map="nodesToReplaceMap"
						:parent-trimmed="childTrimmed"
					/>
	        	</ul> 
        	</li>
        </script>


		<script>

            // define the item component
            Vue.component('bi-pl-tree-node', {
            	template: '#bi-pl-tree-node',
            	props: {
            		isRootNode : Boolean,
            		parentTrimmed : Boolean,
            		hierarchyNodes : Object,
            		nodeId : String,
            		nodesToReplaceMap : Object
            	},
            	data: function () {
            	  	return {
        	    		open: true,
        	    		childNodes : []
        	  		}
        		},
        		created : function(){
        			console.log("CREATED");

        			if(this.hierarchyNodes[this.nodeId].childrenIds && this.hierarchyNodes[this.nodeId].childrenIds.length > 3){
        				this.open = false;
        			}

        			this.childNodes = this.$children;

        		},

        		computed : {
					theme : function() { return this.$store.getters.getTheme },

        			hasChildren: function(){
        				return this.hierarchyNodes[this.nodeId].childrenIds && this.hierarchyNodes[this.nodeId].childrenIds.length > 0;
        			},
        			childTrimmed : function(){
        				console.time('childTrimmed');
        				// The node itself is trimmed:
        				if(this.nodesToReplaceMap[this.hierarchyNodes[this.nodeId].positionCycleId]){
        					console.timeEnd('childTrimmed');
        					return true;
        				}

        				// A child is trimmed:
    					var trimmed = false;
    					if(this.childNodes){
    						_.each(this.childNodes, function(child){
								if(child.childTrimmed){
        							trimmed = true;
		    						return false;
		    					}
    						})
    					}
        				console.timeEnd('childTrimmed');
        				return trimmed;
        			}
        		}
          	});


	        var vueInstance = Vue.component('bi-pl-trim-hierarchy', {
	            template : '#bi-pl-trim-hierarchy',
	            props : {
	            	batchStatus : String,
	            	country : {type : String, default : null},
					cycle : {type : Object, default : function() { return {}}},
					hierarchy : {type : String, default : null},
					hierarchies : {type : Array, default : null},
	            },

				mounted : function() {
					// this.$vfstore.dispatch('setTheme', VueForce.themes.bootstrapTheme);
					// this.getCycles();
				},
				beforeDestroy : function() {
					this.nodesToReplaceMap = {};
					this.hierarchyNodes = {};
					this.rootNodeId = undefined;
				},
				data : function() {
					return {
						loading : false,
						errorMessage : undefined,
						
						hierarchyNodes : {},
						rootNodeId : undefined,

						nodesToReplaceMap : {},

						showCardBody : false,
					}
				},
				computed : {
					theme : function() { return this.$store.getters.getTheme },
					iconCyclePath : function() {
	                    return !this.cycle ? 'utility/error' : 'utility/check';
	                },
	                iconSingleHierarchyPath  : function() {
                        return !this.hierarchies || (this.hierarchies && this.hierarchies.length !== 1) ? 'utility/error' : 'utility/check';

                    }
				},
				watch : {
					hierarchy : {
						handler : function(nv) {
							if(nv){
								this.getHierarchyNodes(nv);
							} else {
								this.errorMessage = 'Hierarchy not selected';
								this.hierarchyNodes = {};
								this.rootNodeId = undefined;
							}
						},
						immediate : true
					}
				},
				methods :  {

					getHierarchyNodes : function(herarc){
						console.log("BI_PL_TrimHierarchy : getHierarchyNodes()");
						var self = this;

						this.hierarchyNodes = {};
						this.errorMessage = undefined;
						this.loading = true;

						this.nodesToReplaceMap = {};

						BI_PL_TrimHierarchyCtrl.getHierarchyNodes(this.cycle.Id, herarc, function(r, ev){
							console.log('BI_PL_TrimHierarchy getHierarchyNodes() completed ',r);
							
							self.loading = false;
							
							if(ev.statusCode == 200) {
								self.hierarchyNodes = r.tree;
								self.rootNodeId = r.rootNodeId;

								console.log('self.hierarchyNodes', self.hierarchyNodes);

							} else {
								self.errorMessage = ev.message;
							}
						}, { escape : false })
					},

					executeTrimHierarchy : function(){
						console.log("BI_PL_TrimHierarchy : executeTrimHierarchy()");
						var self = this;

						this.loading = true;
						
						BI_PL_TrimHierarchyCtrl.executeTrimHierarchy(this.cycle.Id, this.hierarchy, this.nodesToReplaceMap, function(r, ev){
							console.log('BI_PL_TrimHierarchy executeTrimHierarchy() completed'+r);
							console.log(r);
							
							self.loading = false;
							
							if(ev.statusCode == 200) {
								self.getHierarchyNodes(self.hierarchy);
							} else {

							}
						}, { escape : false })
					}
				}
			});
		</script>
</apex:component>