<apex:component >
    <script type="text/x-template" id="chart-line">
        <div>
            <div><h3>{{title}}</h3></div>
            <div>{{description}}</div>
            <canvas></canvas>
        </div>
    </script>
    <script type="text/x-template" id="chart-bar">
        <div>
            <div><h3>{{title}}</h3></div>
            <div>{{description}}</div>
            <canvas v-bind:style="(maxHeight ? ' max-heigth : ' + maxHeigth + ' !important' : '') + (maxWidth ? ' max-width : ' + maxWidth + ' !important ' : '')"></canvas>
        </div>
    </script>
    <script type="text/x-template" id="chart-polar">
        <div>
            <div><h3>{{title}}</h3></div>
            <div>{{description}}</div>
            <canvas></canvas>
        </div>
    </script>
    <script type="text/x-template" id="chart-pie">
        <div>
            <div><h3>{{title}}</h3></div>
            <div>{{description}}</div>
            <canvas></canvas>
        </div>
    </script>
    <script type="text/x-template" id="chart-radar">
        <div>
            <div><h3>{{title}}</h3></div>
            <div>{{description}}</div>
            <canvas></canvas>
        </div>
    </script>

    <script>
        Vue.component('chart-line', {
            template : '#chart-line',
            props : {
                title : String,
                description : String,
                labels : Array,
                chartData : Array,
                options : Object
            },
            data : function() {
                return {
                    chart : null
                }
            },
            watch : {
                chartData : function() {
                    this.displayChart();
                }
            },
            computed : {
                opts : function() {
                    if(this.options) {
                        return this.options;
                    } else {
                        return {
                            responsive: true,
                            scales: {
                                xAxes: [{
                                    gridLines: { 
                                        offsetGridLines: true
                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    }
                }
            },
            mounted : function() {
                this.displayChart();
            },
            methods : {
                displayChart : function() {

                    if(!this.chartData) return; 

                    this.chart = new Chart(this.$el.querySelector('canvas'), {
                        type: 'line',
                        data: {
                            labels : this.labels,

                            datasets : this.chartData
                        },
                        options : this.opts
                    });
                },

                clearCanvas : function() {
                    var canvas = this.$el.querySelector('canvas');
                    var context = canvas.getContext('2d');
                    context.clearRect(0, 0, canvas.width, canvas.height);
                }
            }
        });
         Vue.component('chart-bar', {
            template : '#chart-bar',
            props : {
                title : String,
                description : String,
                labels : Array,
                chartData : Array,
                options : Object,
                maxHeight : String,
                maxWidth : String
            },
            data : function() {
                return {
                    chart : null
                }
            },
            watch : {
                chartData : function() {
                    this.displayChart();
                },
                labels : function() {
                }
            },
            computed : {
                opts : function() {
                    if(this.options) {
                        return this.options;
                    } else {
                        return {
                            responsive: true,
                            scales: {
                                xAxes: [{
                                    gridLines: { 
                                        offsetGridLines: true
                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    }
                }
            },
            mounted : function() {
                this.displayChart();
            },
            methods : {
                displayChart : function() {
                    if(!this.chartData) return; 
                    if(this.chart) this.chart.destroy();
                    
                    this.chart = new Chart(this.$el.querySelector('canvas'), {
                        type: 'bar',
                        data: {
                            labels : this.labels,
                            datasets : this.chartData
                        },
                        options : this.opts
                    });
                }
            }
        });

        Vue.component('chart-polar', {
            template : '#chart-polar',
            props : {
                title : String,
                description : String,
                labels : Array,
                chartData : Array,
                options : Object
            },
            data : function() {
                return {
                    chart : null
                }
            },
            watch : {
                chartData : function() {
                    this.chart.update();
                },
                labels : function() {
                    this.chart.update();
                }
            },
            mounted : function() {
                this.displayChart();
            },
            beforeDestroy : function() {
                this.chart = null;
            },
            computed : {
                opts : function() {
                    if(this.options) {
                        return this.options;
                    } else {
                        return {
                            scales : {
                                ticks : {
                                    min : 0
                                }
                            }
                        }
                    }
                }
            },
            methods : {
                displayChart : function() {
                    this.chart = new Chart(this.$el.querySelector('canvas'), {
                        type: 'polarArea',
                        data: {
                            labels : this.labels,
                            datasets : this.chartData
                        },
                        options : this.opts
                    });
                }
            }
        });

        Vue.component('chart-pie', {
            template : '#chart-pie',
            props : {
                title : String,
                description : String,
                labels : Array,
                chartData : Array,
                options : Object
            },
            data : function() {
                return {
                    chart : null
                }
            },
            watch : {
                chartData : function() {
                    this.chart.update();
                },
                labels : function() {
                    this.chart.update();
                }
            },
            computed : {
                opts : function() {
                    if(this.options) {
                        return this.options;
                    } else {
                        return {
                            ticks : {
                                min : 0
                            }
                        }
                    }
                }
            },
            mounted : function() {
                this.displayChart();
            },
            methods : {
                displayChart : function() {
                    this.chart = new Chart(this.$el.querySelector('canvas'), {
                        type: 'pie',
                        data: {
                            labels : this.labels,
                            datasets : this.chartData
                        },
                        options : this.opts
                    });
                }
            }
        });
        Vue.component('chart-radar', {
            template : '#chart-radar',
            props : {
                title : String,
                description : String,
                labels : Array,
                chartData : Array,
                options : Object,
                maxHeight : Number,
                maxWidth : Number
            },
            data : function() {
                return {
                    chart : null
                }
            },
            watch : {
                chartData : function() {
                    this.chart.update();
                },
                labels : function() {
                    this.chart.update();
                }
            },
            computed : {
                opts : function() {
                    if(this.options) {
                        return this.options;
                    } else {
                        return {
                            scale : {
                                beginsAtZero : true,
                                ticks : {
                                    min : 0
                                }
                            }
                        }
                    }
                }
            },
            mounted : function() {
                this.displayChart();
            },
            methods : {
                displayChart : function() {
                    this.chart = new Chart(this.$el.querySelector('canvas'), {
                        type: 'radar',
                        data: {
                            labels : this.labels,
                            datasets : this.chartData
                        },
                        options : this.opts
                    });
                }
            }
        });
    </script>

</apex:component>