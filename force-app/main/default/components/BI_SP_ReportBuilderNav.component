<apex:component >
    <style>
        .bs .navbar {
            z-index: auto !important;
        }
        .bs .my-nav-color {
            background-color: #428bca !important; 
            border-color: #428bca !important;
        }
        .bs .navbar .nav .my-nav-title {
            color: white !important;
            font-size: 18px !important;
            line-height: 1 !important;
        }
        .bs .navbar .my-navbar-right {
            margin-right: 10px !important;
        }
        .bs .navbar .nav .my-nav-p {
            margin-right: 0px !important; 
            margin-left: 0px !important; 
            color: white !important;
        }
        .bs .navbar .nav .my-nav-select{
            margin-left: 0px !important; 
            margin-right: 0px !important; 
            color: #555555 !important;
        }
        .bs .navbar .nav .my-nav-select[disabled]{
		    cursor: not-allowed !important;
		    background-color: #eeeeee !important;
		}
        .bs .navbar .nav li{
            margin-left: 1em !important;
        }
    </style>

    <script type="text/x-template" id="report-builder-nav">

    	<nav class="my-nav-color navbar navbar-inverse">
		    <div class="nav navbar-nav navbar-left">
		      <p class="my-nav-title navbar-text input-sm">{{title}}</p>
		    </div>

		      
	      	<ul class="my-navbar-right nav navbar-nav navbar-right" v-if="!isOnlyWatchNav">
	        	<li>
	        		<p class="my-nav-p navbar-text input-sm">{{userLabel}}</p>
	                <select class="navbar-text input-sm my-nav-select" v-model="value.selectedReportBuilder" v-bind:disabled="!isMdmActive ||isOnlyReportBuilder || isDisabledAll">
	                    <option v-bind:value="true">{!$Label.BI_SP_Report_builder}</option>
	                    <option v-bind:value="false" v-if="isMdmActive && !isOnlyReportBuilder">{!$User.FirstName} {!$User.LastName}</option>
	                </select>
	        	</li>
	        	<li>
	        		<p class="my-nav-p navbar-text input-sm">{!$Label.BI_SP_Region}</p>
	                <select class="navbar-text input-sm my-nav-select" v-model="value.selectedRegion" v-bind:disabled="!value.selectedReportBuilder || isDisabledAll">
	                    <option v-bind:value="null">{!$Label.BI_SP_None}</option>
	                    <option v-for="region in value.regions" v-bind:value="region">{{region}}</option>
	                </select>
	        	</li>
                <li v-if="canSelectCountry">
                    <p class="my-nav-p navbar-text input-sm">{!$Label.BI_SP_Country}</p>
                    <select class="navbar-text input-sm my-nav-select" v-model="value.selectedCountry" v-bind:disabled="!value.selectedReportBuilder || !value.selectedRegion || isDisabledAll">
                        <option v-bind:value="null">{!$Label.BI_SP_None}</option>
                        <option v-for="country in value.countries" v-bind:value="country">{{country}}</option>
                    </select>
                </li>
	      	</ul>

	      	<ul class="my-navbar-right nav navbar-nav navbar-right" v-if="isOnlyWatchNav && isOnlyReportBuilder">
	        	<li>
	        		<p class="my-nav-p navbar-text input-sm">{{userLabel}}</p>
	                <select class="navbar-text input-sm my-nav-select"  v-bind:disabled="true">
	                    <option v-bind:value="true">{!$Label.BI_SP_Report_builder}</option>
	                </select>
	        	</li>
	      	</ul>

            <ul class="my-navbar-right nav navbar-nav navbar-right" v-if="isOnlyWatchNav && !isOnlyReportBuilder">
                <li>
                    <p class="my-nav-p navbar-text input-sm">{{userLabel}}</p>
                    <select class="navbar-text input-sm my-nav-select"  v-bind:disabled="true">
                        <option v-bind:value="true">{!$User.FirstName} {!$User.LastName}</option>
                    </select>
                </li>
            </ul>
		</nav>

	</script>

    <script>

         Vue.component('report-builder-nav', {
         	template : '#report-builder-nav',
            props : {
            	title : { type : String, default : ''},
            	userLabel : { type : String, default : '{!$Label.BI_SP_Viewing_as}'},
            	isOnlyWatchNav : { type : Boolean, default : false},
                isMdmActive : { type : Boolean, default : true},
                isReportBuilder : {type :Boolean, default : true},
                isOnlyReportBuilder : {type :Boolean, default : false},                
                isDisabledAll : {type :Boolean, default : false},
                canSelectCountry : {type : Boolean, default : false},
                value : { 
                    type : Object, 
                    default : function() {
                        return {
                            selectedReportBuilder : false,                   	
                            regions : [],
                            selectedRegion : null,
                            countries : [],
                            selectedCountry : null
                        }
                    } 
                }
            },
            watch : {
                'value.selectedReportBuilder' : function(newValue){
                    this.value.selectedRegion = null;
                    this.value.selectedCountry = null;
                },
                'value.selectedRegion' : function(newValue){
                    this.value.selectedCountry = null;
                }
            },
            data : function(){
                return {

                }
            },
            mounted : function() {
            	if((!this.isMdmActive && this.isReportBuilder) || this.isOnlyReportBuilder){
            		this.value.selectedReportBuilder = true;
            	}

            }
        });
    </script>
</apex:component>