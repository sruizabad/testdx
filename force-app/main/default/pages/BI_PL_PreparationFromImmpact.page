<apex:page controller="BI_PL_PreparationFromImmpactCtrl">
    <!-- JQuery -->
    <apex:includescript value="{!URLFOR($Resource.BI_PL_PLANiT_Dependencies, '/js/jquery-2.2.4.min.js')}" />
    <!-- JQuery DataTable -->
    <apex:stylesheet value="{!URLFOR($Resource.BI_PL_DataTable, 'datatables.css')}" />
    <apex:includescript value="{!URLFOR($Resource.BI_PL_DataTable, 'datatables.min.js')}" />
    <!-- Main Style Code -->
    <apex:includescript value="{!URLFOR($Resource.BI_PL_PlanitAdminJS)}" />
    <apex:stylesheet value="{!URLFOR($Resource.BI_PL_PlanitAdminCSS)}" />
    <!-- Font Awesome -->
    <apex:stylesheet value="{!URLFOR($Resource.BI_PL_DataTable, 'font-awesome-4.7.0/css/font-awesome.min.css')}" />
    <!-- DatePicker -->
    <apex:stylesheet value="{!URLFOR($Resource.BI_PL_PLANiT_Dependencies, 'css/bootstrap-datepicker.min.css')}" />
    <apex:includescript value="{!URLFOR($Resource.BI_PL_PLANiT_Dependencies, 'js/bootstrap-datepicker.min.js')}" />
    <!--Vue js -->
    <apex:includeScript value="{!$Resource.BI_PL_Vue}"/>
    <!--<script src="https://unpkg.com/vue"></script>-->

    <script>

    </script>

    <apex:outputPanel rendered="{!!isImportFromIMMPACTAllowed}">
        <p class="not-approved-error text-center">{!$Label.BI_PL_Preparation_from_impact_not_allowed}</p>
    </apex:outputPanel>

    <apex:outputPanel rendered="{!isImportFromIMMPACTAllowed}">
        <div id="importIMMPACTHierarchy" class="tab-pane bs">

            <!--  ERROR MESSAGE -->
            <apex:outputPanel id="importIMMPACTErrorsPanel">
                <apex:outputPanel rendered="{!ISBLANK(selectedTargetCycle) || ISBLANK(selectedTargetHierarchy)}">
                    <p class="not-approved-error text-center">{!$Label.BI_PL_Select_cycle_hierarchy}</p>
                </apex:outputPanel>

                <apex:outputPanel id="IMMPACTBatchError">
                    <apex:outputPanel rendered="{!!loading && NOT(ISNULL(errors))}">
                        <p class="not-approved-error text-center">{!errors}</p>
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:outputPanel>

            <apex:outputPanel id="IMMPACTSuccessPanel">
                <apex:outputPanel rendered="{!!loading && completed}">
                    <div class="alert alert-success">
                        {!$Label.BI_PL_Process_completed}
                    </div>
                </apex:outputPanel>
            </apex:outputPanel>

            <div id="page-wrapper">

                <apex:form id="importIMMPACTHierarchyPooling">

                    <apex:outputPanel rendered="{!loading}">
                        <apex:actionPoller action="{!checkBatchStatus}" reRender="picklistTargetCycleWrapper,IMMPACTBatchError,refreshSpinner,IMMPACTSuccessPanel,importIMMPACTHierarchyButtonWrapper,cycleForm,hierarchyForm" interval="5"/>
                    </apex:outputPanel>
                </apex:form>
               
                <div class="form-horizontal" style="background-color: #eeeeee;">
                <div class="form-group form-group-sm">
                <div class="row" style="padding-left: 15px; padding-right: 15px; padding-top: 15px; padding-bottom: 15px;">
                 
                    <!-- SELECT CYCLE -->
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="col-lg-6 col-md-4 col-xs-6 text-right" style="margin-left:20px">
                            <p style="font-weight: bold" class="control-label">{!$Label.BI_PL_DM_Import_to_PLANiT_cycle}</p> 
                        </div>
                        <div class="col-lg-5 col-md-7 col-xs-5 text-left" style="padding-left: 0px !important; ">
                            <apex:form id="cycleForm">
                                <apex:selectList value="{!selectedTargetCycle}" size="1" styleClass="form-control navbar-dropdown" disabled="{!loading}">
                                    
                                    <apex:selectOptions value="{!lstCycles}" />     
                                    <apex:actionSupport event="onchange" action="{!cycleChanged}" reRender="IMMPACToutputFieldForce,IMMPACToutputStartDate,IMMPACToutputEndDate,IMMPACThierarchyForm,importIMMPACTHierarchyButtonWrapper, IMMPACTSuccessPanel,IMMPACTBatchError,importIMMPACTErrorsPanel" />
                                </apex:selectList>
                            </apex:form>
                        </div>
                    </div>

                    <!-- OUTPUT INFO CYCLE -->
                    <div class="col-lg-2 col-md-6 col-xs-12">
                        <div class="col-lg-6 col-md-4 col-xs-6 text-right" >
                            <p style="font-weight: bold" class="control-label">{!$ObjectType.BI_PL_Cycle__c.fields.BI_PL_Field_force__c.Label}</p>
                        </div>
                        <div class="col-lg-6 col-md-8 col-xs-6 control-label text-left" style="padding-left: 0px !important; text-align: left !important;">
                            <apex:outputText value="{!IF(selectedTargetCycle!='',cycles[selectedTargetCycle].BI_PL_Field_force__r.Name,'')}" id="IMMPACToutputFieldForce" styleClass="text-left"/>
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-6 col-xs-12">
                        <div class="col-lg-6 col-md-4 col-xs-6 text-right" >
                            <p style="font-weight: bold" class="control-label">{!$ObjectType.BI_PL_Cycle__c.fields.BI_PL_Start_date__c.Label}</p>
                        </div>
                        <div class="col-lg-6 col-md-8 col-xs-6 control-label text-left" style="padding-left: 0px !important; text-align: left !important;">
                            <apex:outputText value="{0, date, MMMM d','  yyyy}" id="IMMPACToutputStartDate" styleClass="text-left">
                                <apex:param value="{!IF(selectedTargetCycle!='',cycles[selectedTargetCycle].BI_PL_Start_date__c,"")}" />
                            </apex:outputText>
                        </div>
                    </div>


                    <div class="col-lg-2 col-md-6 col-xs-12">
                        <div class="col-lg-6 col-md-4 col-xs-6 text-right" >
                            <p style="font-weight: bold" class="control-label">{!$ObjectType.BI_PL_Cycle__c.fields.BI_PL_End_date__c.Label}</p>
                        </div>
                        <div class="col-lg-6 col-md-8 col-xs-6 control-label text-left" style="padding-left: 0px !important; text-align: left !important;">
                            <apex:outputText value="{0, date, MMMM d','  yyyy}" id="IMMPACToutputEndDate" styleClass="text-left">
                                <apex:param value="{!IF(selectedTargetCycle!='',cycles[selectedTargetCycle].BI_PL_End_date__c,"")}" />
                            </apex:outputText>
                        </div>
                    </div>

                    <!-- SELECT HIERARCHY -->
                    <div class="col-lg-2 col-md-6 col-xs-12">
                        <div class="col-lg-6 col-md-4 col-xs-6 text-right" >
                            <p style="font-weight: bold" class="control-label">{!$ObjectType.BI_PL_Position_cycle__c.fields.BI_PL_Hierarchy__c.Label}</p>
                        </div>
                        <div class="col-lg-6 col-md-8 col-xs-6 text-left" style="padding-left: 0px !important;">
                            <apex:form id="IMMPACThierarchyForm">
                                <apex:selectList value="{!selectedTargetHierarchy}" size="1" styleClass="form-control navbar-dropdown" disabled="{!loading}">
                                    <apex:selectOptions value="{!lstHierarchies}" />     
                                    <apex:actionSupport event="onchange" action="{!hierarchyChanged}" reRender="importIMMPACTHierarchyButtonWrapper,IMMPACTSuccessPanel,IMMPACTBatchError,importIMMPACTErrorsPanel" />   
                                </apex:selectList>
                            </apex:form>
                        </div>
                    </div>

                </div>
                </div>
                </div>


                <apex:form id="importIMMPACTHierarchyButtonWrapper">
                    <div class="apexBtn" style="padding-left: 0px !important; height: 50px;">
                        <apex:commandButton disabled="{!loading || !importAllowed}" styleClass="btn btn-primary btn-sm pull-right action-button" action="{!importFromIMMPACT}" value="{!$Label.BI_PL_Import_from_IMMPACT}" rerender="importIMMPACTHierarchyButtonWrapper,importIMMPACTHierarchyPooling,refreshSpinner,IMMPACTBatchError,IMMPACTSuccessPanel,cycleForm,hierarchyForm,picklistTargetCycleWrapper"/>
                    </div>
                </apex:form> 

                <div style="position:absolute;top:50%;left:50%">
                    <apex:outputPanel id="refreshSpinner">
                        <apex:image value="{!$Resource.loading_gif}" rendered="{!loading}" width="20" height="20"/>
                    </apex:outputPanel>
                </div>
                
            </div>
        </div>             
    </apex:outputPanel>
</apex:page>