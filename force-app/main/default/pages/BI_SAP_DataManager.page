<apex:page controller="BI_SAP_DataManagerCtrl">

    <!-- JQuery -->
    <apex:includescript value="{!URLFOR($Resource.BI_SAP_DataTable, '/jQuery-2.2.3/jquery-2.2.3.min.js')}" />
    <!-- JQuery DataTable -->
    <apex:stylesheet value="{!URLFOR($Resource.BI_SAP_DataTable, 'datatables.css')}" />
    <apex:includescript value="{!URLFOR($Resource.BI_SAP_DataTable, 'datatables.min.js')}" />
    <!-- Main Script Code -->
    <apex:includescript value="{!URLFOR($Resource.BI_SAP_Data_Manager, 'BI_SAP_Data_Manager/js/BI_SAP_Data_Manager_JS.js')}" />
    <!-- Main Style Code -->
    <apex:stylesheet value="{!URLFOR($Resource.BI_SAP_Data_Manager, 'BI_SAP_Data_Manager/css/BI_SAP_Data_Manager_CSS.css')}" />
    <!-- Font Awesome -->
    <apex:stylesheet value="{!URLFOR($Resource.BI_SAP_DataTable, 'font-awesome-4.7.0/css/font-awesome.min.css')}" />
    <!-- DatePicker -->
    <apex:stylesheet value="{!URLFOR($Resource.BI_SAP_DatePicker, 'css/bootstrap-datepicker.min.css')}" />
    <apex:includescript value="{!URLFOR($Resource.BI_SAP_DatePicker, 'js/bootstrap-datepicker.min.js')}" />
    
    <script>
        //VARIABLES
        lSAPsToClone = JSON.parse(JSON.stringify({!lSAPsToCloneJSON}));
        lSAPsToSync = JSON.parse(JSON.stringify({!lSAPsToSyncJSON}));
        //Remote methods
        synchronizeFn = '{!$RemoteAction.BI_SAP_DataManagerCtrl.synchronizeSAPs}';
        cloneFn = '{!$RemoteAction.BI_SAP_DataManagerCtrl.cloneSAPs}';
        canClone = {!canClone};

        // Requested by Donald on 09-03-2017
        // For the moment users are not allowed to syncronize SAP Preparations
        // Users are allowed again on 18-04-2017
        canSync = {!canSync};
        //canSync = false;
    </script>

    <body>
        <apex:outputPanel rendered="{!canClone}">
            <!-- CLONE MODAL SECTION -->
            <div class="bs modal fade" id="cloneModal" tabindex="-1" role="dialog" aria-labelledby="cloneModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document" style="left: 0% !important;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="cloneModalLabel"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {!$Label.BI_SAP_DM_Clone_Header}</h3>
                        </div>
                        <div class="modal-body">
                            <h5>{!$Label.BI_SAP_DM_Clone_Title_1} <span>{!lSAPsToClone.size}</span> {!$Label.BI_SAP_DM_Clone_Title_2}</h5>
                            <p class="confirm-msg">{!$Label.BI_SAP_DM_Clone_Form_Request}</p>
                            <form>
                                <div class="input-group-date" style="margin-top: 1em !important">
                                    <label for="startDateInput">{!$label.BI_SAP_Start_Date_Input_Label}: </label>
                                    <input id="startDateInput" type="text" class="form-control input-date" value="" data-name="Start date"/>
                                </div>
                                <div class="input-group-date" style="margin-top: 1em !important">
                                    <label for="endDateInput">{!$label.BI_SAP_End_Date_Input_Label}: </label>
                                    <input id="endDateInput" type="text" class="form-control input-date" value="" data-name="End date"/>
                                </div>
                            </form>
                            <div class="errors-wrapper text-left" style="margin-top: 1em !important"></div>
                            <!--<div class="body-header">
                                <p>You are going to clone the following SAPs:</p>
                            </div>
                            
                            <div class="scrollable-list-wrapper">
                                <ul id="sapsToCloneList"></ul>
                            </div>
                            <div class="body-footer">
                                <p>Total SAPs to clone: {!lSAPsToClone.size}</p>
                            </div>-->
                        </div>
                        <div class="modal-footer" style="margin-top: 0px !important">
                            <div class="text-right">
                                <p><span class="fa fa-exclamation-triangle"></span> {!$Label.BI_SAP_Cannot_Undone}</p>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">{!$Label.Common_CancelButton_vod}</button>
                                <button id="cloneButton" type="button" class="btn btn-secondary">{!$label.BI_SAP_DM_Continue}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </apex:outputPanel>
        <apex:outputPanel rendered="{!canSync}">   
            <!-- SYNCHRONIZE MODAL SECTION -->
            <div class="bs modal fade" id="synchronizeModal" tabindex="-1" role="dialog" aria-labelledby="synchronizeModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document" style="left: 0% !important;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title" id="synchronizeModalLabel"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {!$Label.BI_SAP_DM_Sync_Header}</h3>
                        </div>
                        <div class="modal-body">
                            <h5>{!$Label.BI_SAP_DM_Sync_Title_1} <span>{!lSAPsToSync.size}</span> {!$Label.BI_SAP_DM_Sync_Title_2}</h5>
                            <p class="confirm-msg">{!$Label.BI_SAP_DM_Sync_Msg}</p>
                            <!-- <div class="body-header">
                                <p>You are going to synchronize the following SAPs:</p>
                            </div>
                            <div class="scrollable-list-wrapper">
                                <ul id="sapsToSyncList"></ul>
                            </div>
                            <div class="body-footer">
                                <p>Total SAPs to synchronize: {!lSAPsToClone.size}</p>
                            </div>-->
                            <div class="errors-wrapper text-left" style="margin-top: 1em !important"></div>
                            <p class="confirm-msg">{!$Label.BI_SAP_Synchronize_SAP_Name_Input_Text} <input id="SAPNameInput" type="text" /></p>
                        </div>
                        
                        <div class="modal-footer" style="margin-top: 0px !important">
                            <div class="text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">{!$Label.Common_CancelButton_vod}</button>
                                <button id="syncButton" type="button" class="btn btn-secondary">{!$Label.BI_SAP_DM_Continue}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </apex:outputPanel>
        
            
            
        <!-- MAIN SECTION -->
        <div class="bs">
            
            <!-- PAGE HEADER -->
            <div class="col-xs-12">
                <div class="page-header">
                    <h3>PLANiT {!$Label.BI_SAP_DM_Header}</h3>
                </div>
            </div>
            <ul class="nav nav-tabs">
                <li class="active" style=" margin-top: 8px !important;"><a data-toggle="tab" href="#clone_tab"><h3>{!$Label.BI_SAP_DM_Clone}</h3></a></li>
                <li style=" margin-top: 8px !important;"><a data-toggle="tab" href="#sync_tab"><h3>{!$Label.BI_SAP_DM_Sync}</h3></a></li>
            </ul>

            <div class="tab-content">
                <!-- CLONE TAB -->
                <div id="clone_tab" class="tab-pane fade in active">
                    <div id="page-wrapper" style="padding-left: 15px; padding-right: 15px; padding-top: 15px">
                        <!--  ERROR MESSAGE -->
                        <apex:outputPanel rendered="{!!canClone}">
                            <p class="not-approved-error text-center">{!$Label.BI_SAP_DM_Not_Approved}</p>
                        </apex:outputPanel>
                        
                        <!--  CLONE BUTTON -->
                        <div class="apexBtn col-md-1 col-xs-12" style="padding-left: 0px !important;">
                            <button id="clone"  type="button" class="btn btn-primary btn-sm pull-right action-button" data-toggle="modal" data-target="#cloneModal">
                                <i class="fa fa-clone" aria-hidden="true"></i> {!$Label.BI_SAP_DM_Clone}
                            </button>                            
                        </div>                                
                        
                        <apex:outputPanel >                            
                            <table id="mainTable" class="table table-striped table-bordered">
                                <thead>
                                    <th>{!$Label.BI_SAP_DM_SAP_Name}</th>
                                    <th>{!$ObjectType.SAP_Preparation_BI__c.fields.Status_BI__c.label}</th>
                                    <th>{!$ObjectType.BI_TM_Territory__c.label}</th>
                                    <th>{!$ObjectType.SAP_Preparation_BI__c.fields.Start_Date_BI__c.label}</th>
                                    <th>{!$ObjectType.SAP_Preparation_BI__c.fields.End_Date_BI__c.label}</th>
                                    <th>{!$Label.BI_SAP_Owner}</th>
                                </thead>
                                <tbody>
                                    <apex:repeat value="{!lSAPs}" var="SAP">
                                        <tr>
                                            <td><a target="_blank" href="/{!SAP.Id}">{!SAP.Name}</a></td>
                                            <td>{!SAP.Status_BI__c}</td>
                                            <td>{!SAP.Territory_BI__c}</td>
                                            <td>
                                                <apex:outputText value="{0, date, d'/'MM'/'yyyy}">
                                                    <apex:param value="{!SAP.Start_Date_BI__c}" /> 
                                                </apex:outputText>
                                            </td>
                                            <td>
                                                <apex:outputText value="{0, date, d'/'MM'/'yyyy}">
                                                    <apex:param value="{!SAP.End_Date_BI__c}" /> 
                                                </apex:outputText>
                                            </td>
                                            <td>{!ownerNamesBySAPId[SAP.Id]}</td>
                                        </tr>
                                    </apex:repeat>
                                </tbody>
                            </table>
                        </apex:outputPanel>
                    </div>
                </div>
                
                <!-- SYNC TAB -->
                <div id="sync_tab" class="tab-pane fade">
                    <div id="page-wrapper" style="padding-left: 15px; padding-right: 15px; padding-top: 15px">               
                        <!--  ERROR MESSAGE -->
                        <apex:outputPanel rendered="{!!canSync}">
                            <p class="not-approved-error text-center">{!$Label.BI_SAP_DM_Not_Approved}</p>
                        </apex:outputPanel>
                        
                        <!--  SYNC BUTTON -->
                        <div class="apexBtn col-md-1 col-xs-12" style="padding-left: 0px !important;">
                            <button id="synchronize"  type="button" class="btn btn-primary btn-sm pull-right action-button" data-toggle="modal" data-target="#synchronizeModal">
                                <span class="glyphicon glyphicon-transfer"></span> {!$Label.BI_SAP_DM_Sync}
                            </button>
                        </div>
                        
                        <apex:outputPanel >                            
                            <table id="syncTable" class="table table-striped table-bordered">
                            <thead>
                                <th>{!$Label.BI_SAP_DM_SAP_Name}</th>
                                <th>{!$ObjectType.SAP_Preparation_BI__c.fields.Status_BI__c.label}</th>
                                <th>{!$ObjectType.BI_TM_Territory__c.label}</th>
                                <th>{!$ObjectType.SAP_Preparation_BI__c.fields.Start_Date_BI__c.label}</th>
                                <th>{!$ObjectType.SAP_Preparation_BI__c.fields.End_Date_BI__c.label}</th>
                                <th>{!$Label.BI_SAP_Owner}</th>
                            </thead>
                            <tbody>
                                <apex:repeat value="{!lSyncSAPs}" var="SAP">
                                    <tr>
                                        <td><a target="_blank" href="/{!SAP.Id}">{!SAP.Name}</a></td>
                                        <td>{!SAP.Status_BI__c}</td>
                                        <td>{!SAP.Territory_BI__c}</td>
                                        <td>
                                            <apex:outputText value="{0, date, d'/'MM'/'yyyy}">
                                                <apex:param value="{!SAP.Start_Date_BI__c}" /> 
                                            </apex:outputText>
                                        </td>
                                        <td>
                                            <apex:outputText value="{0, date, d'/'MM'/'yyyy}">
                                                <apex:param value="{!SAP.End_Date_BI__c}" /> 
                                            </apex:outputText>
                                        </td>
                                        <td>{!ownerNamesBySyncSAPId[SAP.Id]}</td>
                                    </tr>
                                </apex:repeat>
                            </tbody>
                        </table>
                        </apex:outputPanel>
                    </div>
                </div>                
            </div>
        </div>
    </body>

</apex:page>