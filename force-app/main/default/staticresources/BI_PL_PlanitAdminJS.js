$(document).ready(function() {
	if(!canSync){
		disableButtons();
	}
	setScreenProperties();
	initMainTable();
	initSyncTable();
	initModals();
});

function disableButtons(){
	$('#synchronize').prop("disabled", !canSync);
}

function setScreenProperties(){
	$('.buttonsBlock').find('input.btn').each(function(){
		console.log($(this));
		$(this).addClass('customBtn');
	});
}

function initMainTable(){
	table = $('#mainTable').DataTable({
        fixedHeader: true,
        "dom":  '<f<t>ipl>',
        "order": [[1, "asc"]],
        language: {
            searchPlaceholder: searchPlaceholderLabel,
			emptyTable: noDataLabel,
			info: showingLabel,
			infoEmpty: infoEmptyLabel,
			infoFiltered: infoFilteredLabel,
			lengthMenu: showEntriesLabel,
            search: " ",
			paginate:{
				next: nextLabel,
				previous: prevLabel
			}
        }
    });
	
	$('#mainTable_length').addClass('col-md-3 col-xs-12 text-right');
	$('#mainTable_paginate').addClass('col-md-6 col-xs-12');
	$('#mainTable_info').addClass('col-md-3 col-xs-12');
}


function initSyncTable(){
	table = $('#syncTable').DataTable({
        fixedHeader: true,
        "dom":  '<f<t>ipl>',
        "order": [[1, "asc"]],
        language: {
            searchPlaceholder: searchPlaceholderLabel,
            emptyTable: noDataLabel,
			info: showingLabel,
			infoEmpty: infoEmptyLabel,
			infoFiltered: infoFilteredLabel,
			lengthMenu: showEntriesLabel,
            search: " ",
			paginate:{
				next: nextLabel,
				previous: prevLabel
			}
        }
    });
	
	$('#syncTable_length').addClass('col-md-3 col-xs-12 text-right');
	$('#syncTable_paginate').addClass('col-md-6 col-xs-12');
	$('#syncTable_info').addClass('col-md-3 col-xs-12');
}

function initModals(){
	// Initialize Modal Lists HTML
	/*for(var i=0; i<lSAPsToClone.length; i++){
		console.log(lSAPsToClone[i].Name);
		var SAPName =  lSAPsToClone[i].Name;
		$('#sapsToCloneList').append('<li>' + SAPName + '</li>');
		$('#sapsToSyncList').append('<li>' + SAPName + '</li>');
	}*/
	
	$('.input-date').each(function() {
		console.log($(this));
		var today = new Date();
		$(this).val(''+ today.getDate() +'/'+ (today.getMonth() + 1) +'/'+ today.getFullYear());
	    $(this).datepicker({
	    	defaultViewDate: new Date(),
	    	startDate: new Date(),
	    	format: 'dd/mm/yyyy',
	    	todayHighlight: true
	    });
	});
	
	$('#syncButton').click(function(){
		var SAPName = ($('#SAPNameInput').val()) ? $('#SAPNameInput').val() : '';
			if(SAPName!=''){
				syncSAPs(SAPName);
			}else{
				$('#synchronizeModal .errors-wrapper').html('');
				$('#synchronizeModal .errors-wrapper').append('<p class="error-msg">SAP Name field can not be empty.</p>');
			}
	});
	
	$('#cloneButton').click(function(){
		var startDateInput = $('#startDateInput');
		var endDateInput = $('#endDateInput');
		var validated = validateDates([startDateInput,endDateInput]);
		if(validated){
			cloneSAPs(startDateInput, endDateInput);
		}
	});
}

function validateDates(datesArr){
	var validated = true;
	var errorsArr = [];
	$('#cloneModal .errors-wrapper').html('');
	for(var i=0;i<datesArr.length; i++){
		datesArr[i].removeClass('has-error');
		try{
			if(datesArr[i].val() != ''){
				var day = datesArr[i].val().split('/')[0];
				var month = datesArr[i].val().split('/')[1];
				var year = datesArr[i].val().split('/')[2];
				var d = new Date(month, day, year);
				if(d == 'Invalid Date'){
					throw 'Invalid Date';
				}
			}else{
				validated = false;
				datesArr[i].addClass('has-error');
				errorsArr.push(datesArr[i].data('name') + ' cannot be empty');
			}
		}catch(err){
			validated = false;
			datesArr[i].addClass('has-error');
			errorsArr.push(datesArr[i].data('name') + ' is invalid');
		}
	}
	
	if(validated){
		var startDay = datesArr[0].val().split('/')[0];
		var startMonth = datesArr[0].val().split('/')[1];
		var startYear = datesArr[0].val().split('/')[2];
		var startDate = new Date(startMonth, startDay, startYear);
		
		var endDay = datesArr[1].val().split('/')[0];
		var endMonth = datesArr[1].val().split('/')[1];
		var endYear = datesArr[1].val().split('/')[2];
		var endDate = new Date(endMonth, endDay, endYear);
		
		var diff = endDate - startDate;
		
		console.log(startDate);
		console.log(endDate);
		console.log(diff);
		
		if(diff <= 0 || isNaN(diff)){
			for(var i=0;i<datesArr.length; i++){
				datesArr[i].addClass('has-error');
			}
			errorsArr.push('End date must be greather than start date');
			validated = false;
		}
	}
	
	if(!validated){
		errorsArr.forEach(function(err){
			$('#cloneModal .errors-wrapper').append('<p class="error-msg">'+ err +'</p>');
		});
	}
	return validated;
}

/*function getNextMonthDate(){
	var now = new Date();
	if (now.getMonth() == 11) {
	    var current = new Date(now.getFullYear() + 1, 1, now.getDate());
	} else {
	    var current = new Date(now.getFullYear(), now.getMonth() + 2, now.getDate());
	}
	return current;
}*/

function syncSAPs(SAPName){
	var toSync = [];
	table.column(8, { search:'applied' } ).data().each(function(value, index) {
		toSync.push(value);
	});
	var remoteFn = this.window.synchronizeFn;
	Visualforce.remoting.Manager.invokeAction(
			remoteFn,
			SAPName,
			toSync, 
			function(result,event){
				if(event.status){
					if(result){
						//$("#synchronizeModal .close").click();
						$("#synchronizeModal").modal('toggle');
						$('body').removeClass('modal-open');
						$('.modal-backdrop').remove();
					}else{
						$('#synchronizeModal .errors-wrapper').append('<p class="error-msg">'+ event.message+'</p>');
					}
				}else{
					$('#synchronizeModal .errors-wrapper').append('<p class="error-msg">'+ event.message+'</p>');
				}
			},
			{escape: true}
        );
	
}

function checkErrors(){
	var status = [];
	table.column(1, { search:'applied' } ).data().each(function(value, index) {
		status.push(value);
	});
	if($("select[id$='selectCycle']").val()!= ''){
		var cycleId = $("select[id$='selectCycle']").val();
		if(canSyncCycle[cycleId] == $("select[id$='selectHierarchy']").val() || canSyncCycle[cycleId] == null){
			$(alreadySynced).hide();
			if($("select[id$='selectHierarchy']").val()!= ''){
				$(notSelectedFilter).hide();
					if(status.length > 0){
						canSync=true;
					} else {
						canSync=false;
					}
					
					for(var i=0;i<status.length;i++){
						if(status[i]!=approvedLabel){
							canSync=false;
						}
					}
					if(canSync){
						$(allApprovedError).hide();
					} else {
						$(allApprovedError).show();
					}
				
			} else {
				canSync = false;
				$(notSelectedFilter).show();
				$(allApprovedError).hide();
			}
		}else{
			$(notSelectedFilter).hide();
			$(alreadySynced).show();
			canSync = false;
		}
	} else {
		canSync = false;
		$(notSelectedFilter).show();
		$(alreadySynced).hide();
		$(allApprovedError).hide();
	}
	disableButtons();
}