var BI_PL_PreparationUtilJS = {
	/**
	 *	Constants
	 */
    FilterTargetsBy : {
        NON_DROPPED : 0,
        DROPPED : 1,
        NEXT_BEST_ACCOUNTS : 2,
        NO_SEE_LIST : 3,
    	DROPPED_AND_NON_DROPPED : 4
    },

    ColumnKeys : {
    	HCOTab : "11",
    	MarketInfo : "5"
    },

    LocalStorageKeys : {
    	PREPARATION_COLUMNS : 'preparation_columns',
    },

    PREPARATION_COLUMNS_SEPARATOR : ';',
    PRODUCT_COLUMN_PREFIX : '#PROD',

    /**
     * Variables
     */
    vue : undefined,
	userBrowserSettings : undefined,
    /**
     *	Methods
     */
    init : function(vue){
    	console.log("BI_PL_PreparationUtilJS init()");
    	this.vue = vue;


        
        // Hide the 
        $(document).ready(function() {
            $("html").css('overflow-x', 'hidden');
        });

/*
        for(var i=0;i<vue.columns.length;i++){
        	var column = vue.columns[i];
        	if(!column.hidden && BI_PL_PreparationUtilJS.isUserBrowserColumnHidden(column.field))
        		column.hidden = true;
        }
*/
    },

    getUserBrowserSettings : function(){
    	var keys = [this.LocalStorageKeys.PREPARATION_COLUMNS];

    	var output = {};
    	for(var i=0;i<keys.length;i++){
    		var key = keys[i];
    		console.log(key, localStorage.getItem(key));
    		output[key] = JSON.parse(localStorage.getItem(key));
    	}
    	return output;
    },
    saveUserBrowserSettings : function(object){
    	var keys = [this.LocalStorageKeys.PREPARATION_COLUMNS];
    	for(var i=0;i<keys.length;i++){
    		var key = keys[i];
    		localStorage.setItem(key, JSON.stringify(object[key]));
    	}
    },

    isUserBrowserColumnHidden : function(column){
    	console.log('isUserBrowserColumnHidden', column, this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS])
    	console.log(this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS] &&
    	this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS].indexOf(column) != -1);
    	return this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS] &&
    	this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS].indexOf(column) != -1;
    },
    isUserBrowserProductColumnHidden : function(productPairId){
    	return this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS] &&
    	this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS].indexOf(this.PRODUCT_COLUMN_PREFIX+productPairId) != -1;
    },
    addHiddenColumn : function(columnToHide){
    	console.log("addHiddenColumn", columnToHide, this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS])
    	if(!this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS])
    		this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS] = [];

    	if(!this.isUserBrowserColumnHidden(columnToHide)){
    		var aux = this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS];
    		aux.push(columnToHide);
    		this.vue.$set(this.vue.userBrowserSettings, this.LocalStorageKeys.PREPARATION_COLUMNS, aux)

    		this.saveUserBrowserSettings(this.vue.userBrowserSettings);
    	}
    },
    addHiddenProductColumn : function(productPairId){
    	this.addHiddenColumn(this.PRODUCT_COLUMN_PREFIX+productPairId);
    },
    clearAllHiddenColumns : function(){
    	this.vue.userBrowserSettings[this.LocalStorageKeys.PREPARATION_COLUMNS] = [];

		this.saveUserBrowserSettings(this.vue.userBrowserSettings);
    },
    resetColumns : function() {
    	console.log("RESET COLUMNS");
        this.clearAllHiddenColumns();

        var self = this.vue;
        Object.values(self.columnsMap).forEach(function(column){
        	if(column != undefined){
	            if(!column.countryColumnKey || (column.countryColumnKey && self.isCountryColumnAvailable(column.countryColumnKey)))
	                column.hidden = false;
	        }
        });
        for (var i = self.productColumns.subcolumns.length - 1; i >= 0; i--) {
            if(!self.productColumns.subcolumns[i].countryColumnKey ||
            		(self.productColumns.subcolumns[i].countryColumnKey && self.isCountryColumnAvailable(self.productColumns.subcolumns[i].countryColumnKey)))
                self.productColumns.subcolumns[i].hidden = false;
        }
        // Displays all added product columns
        self.showAllProductColumns();
    },
    switchToHCP : function(){
    	this.vue.displayingHCP = true;
    },
    switchToHCO : function(){
    	this.vue.displayingHCP = false;
    },
	getFilteredTargets : function(filterBy){
		//console.log("BI_PL_PreparationUtilJS.getFilteredTargets", filterBy);
		//console.time("BI_PL_PreparationUtilJS.getFilteredTargets");

	    if(this.vue.$refs.hcpsTargetsTable)
	        this.vue.$refs.hcpsTargetsTable.resetCurrentPage();
	    if(this.vue.$refs.hcoTargetsTable)
	        this.vue.$refs.hcoTargetsTable.resetCurrentPage();
	    if(this.vue.$refs.sapl_dataTable)
	        this.vue.$refs.sapl_dataTable.resetCurrentPage();
	    if(this.vue.$refs.dropTargetsTable)
	        this.vue.$refs.dropTargetsTable.resetCurrentPage();
	    if(this.vue.$refs.nextBestAccountsTable)
	        this.vue.$refs.nextBestAccountsTable.resetCurrentPage();
	    if(this.vue.$refs.noSeeListTable)
	        this.vue.$refs.noSeeListTable.resetCurrentPage();

	    var droppedAndNonDropped = filterBy == this.FilterTargetsBy.DROPPED_AND_NON_DROPPED;
	    var onlyDeleted = (filterBy == this.FilterTargetsBy.DROPPED);
	    var onlyNextBestAccounts = (filterBy == this.FilterTargetsBy.NEXT_BEST_ACCOUNTS);
	    var onlyNoSeeList = (filterBy == this.FilterTargetsBy.NO_SEE_LIST);
	    var onlyHCO = !this.vue.displayingHCP;
	    
	    var ColumnKeys = this.ColumnKeys;

	    var self = this.vue;
    	var isHCOTabEnabled = self.isCountryColumnAvailable(ColumnKeys.HCOTab);

	    var filteredAndSorted = this.vue.targetsList.filter(function(target){
	    	var isTargetHCO = self.isTargetHCO(target);

	    	// DROPPED
	    	if(!droppedAndNonDropped){
	        	if(onlyDeleted && !target.currentTargetChannel.record.BI_PL_Removed__c || !onlyDeleted && target.currentTargetChannel.record.BI_PL_Removed__c){
	           		return false;
		        }else if(onlyDeleted){
		        	// Do not apply filters to the Dropped ones.
		        	//return true;
		        }
		    }

	        var result = true;

	        // If the country column HCOTab is not set the targets won't be filtered by their type.
	        // Otherwise, filter any list by HCO / HCP:
        	// - Display HCP targets when onlyHCO == false.
        	// - Display HCO targets when onlyHCO == true.
	        if(isHCOTabEnabled)
	            if(onlyHCO && !isTargetHCO || !onlyHCO && isTargetHCO)
	                return false;

	        // NEXT BEST ACCOUNT
	        if(!droppedAndNonDropped){
		        if(onlyNextBestAccounts && (!target.record.BI_PL_Next_best_account__c || target.record.BI_PL_Added_manually__c || target.currentTargetChannel.record.BI_PL_Edited__c))
		            return false;

		        if(!onlyNextBestAccounts && (target.record.BI_PL_Next_best_account__c && (!target.record.BI_PL_Added_manually__c|| !target.currentTargetChannel.record.BI_PL_Edited__c)))
		            return false;

	        	// Do not apply filters to the next best ones.
		        if(onlyNextBestAccounts)
		        	return true;
			}
	        // NO SEE LIST
	    	if(!droppedAndNonDropped){
		        if(onlyNoSeeList && !target.record.BI_PL_No_see_list__c || !onlyNoSeeList && target.record.BI_PL_No_see_list__c){
		            return false;
		        }else if(onlyNoSeeList){
		        	// Do not apply filters to the no see list.
		        	return true;
		        }
		    }

	        /*
	         * Search by serach term (input formSearch records)
	         */
	        if(self.searchTerm) {
	            var lowerSearchTerm = self.searchTerm.toLowerCase();
	            var addressMatch = false;

	            if(target.addresses.length > 0) {
	                addressMatch = (
	                    (target.addresses[0].Name && target.addresses[0].Name.toString().toLowerCase().indexOf(lowerSearchTerm) !== -1)
	                    || ( target.addresses[0].State_vod__c && target.addresses[0].State_vod__c.toString().toLowerCase().indexOf(lowerSearchTerm) !== -1)
	                    || ( target.addresses[0].City_vod__c && target.addresses[0].City_vod__c.toString().toLowerCase().indexOf(lowerSearchTerm) !== -1)
	                    || ( target.addresses[0].Zip_vod__c && target.addresses[0].Zip_vod__c.toString().toLowerCase().indexOf(lowerSearchTerm) !== -1))
	            }

	            result = result && (addressMatch ||
	                (target.record.BI_PL_Target_customer__r.Name && target.record.BI_PL_Target_customer__r.Name.toString().toLowerCase().indexOf(lowerSearchTerm) !== -1)
	                || ( target.record.BI_PL_Primary_parent__c && target.record.BI_PL_Primary_parent__c.toString().toLowerCase().indexOf(lowerSearchTerm) !== -1)
	                || ( target.record.BI_PL_Specialty__c && target.record.BI_PL_Specialty__c.toString().toLowerCase().indexOf(lowerSearchTerm) !== -1));

	            // Search by PLANiT Affiliation Market:

	            if(self.isCountryColumnAvailable(ColumnKeys.MarketInfo) && target.affiliationsPlanit){
	                var affiliationMatch = false;                                    
	                for(var i=0;i<target.affiliationsPlanit.length;i++){
	                    if(target.affiliationsPlanit[i].BI_PL_Market__c && target.affiliationsPlanit[i].BI_PL_Market__c.toString().toLowerCase().indexOf(lowerSearchTerm) != -1)
	                        affiliationMatch = true;
	                        break;
	                }
	                result = result || affiliationMatch;
	            }
	        }


	        /*
	         * Search by field filters (Salesforce listviews style)
	         */

	        result = result && self.checkFieldFilter(target, self.filters.fieldFilter1, self.filters.operatorFilter1, self.filters.inputFilter1);
	        result = result && self.checkFieldFilter(target, self.filters.fieldFilter2, self.filters.operatorFilter2, self.filters.inputFilter2);
	        result = result && self.checkFieldFilter(target, self.filters.fieldFilter3, self.filters.operatorFilter3, self.filters.inputFilter3);

	        /*
	         * Search by product and segment in details (in current channel)
	         */
	        if(self.selectedProductFilter !== 'none' || self.selectedSegmentFilter !== 'none'){

	            if(target.currentTargetChannel && target.currentTargetChannel.details) {
	                let detailsMatch = false;

	                target.currentTargetChannel.details.forEach(function(detail, index){
	                        let productMatch = (self.selectedProductFilter != 'none') ? 
	                            self.generateProductPairId(detail.BI_PL_Product__r, detail.BI_PL_Secondary_product__r) == self.selectedProductFilter : true;
	                        let segmentMatch = (self.selectedSegmentFilter != 'none') ? detail.BI_PL_Segment__c == self.selectedSegmentFilter : true;

	                        detailsMatch = detailsMatch || (productMatch && segmentMatch);
	                });
	                result = result && detailsMatch;
	            }
	        }
	        
	        /*
	         *Search by option in target
	         */
	        if(self.selectedOptionsFilter !== 'all') {
	            switch(self.selectedOptionsFilter) {
	                case 'not_modified':
	                    result = result && (!target.currentTargetChannel.record.BI_PL_Removed__c && !target.currentTargetChannel.record.BI_PL_Edited__c && !target.currentTargetChannel.record.BI_PL_Rejected__c);
	                    break;
	                case 'deleted':
	                    result = result && (target.currentTargetChannel.record.BI_PL_Removed__c);
	                    break;
	                case 'edited':
	                    result = result && (target.currentTargetChannel.record.BI_PL_Edited__c);
	                    break;
	                case 'rejected':
	                    result = result && (target.currentTargetChannel.record.BI_PL_Rejected__c);
	                    break;
	                case 'reviewed':
	                    result = result && (target.currentTargetChannel.record.BI_PL_Reviewed__c);
	                    break;
	                case 'added':
	                    result = result && (target.record.BI_PL_Added_manually__c);
	                    break;
	                default: 
	                    result = false;
	                    break;
	            }
	        }

	        /**
	         * Show deleted items
	         */
	         /*if(self.showDeletedItems === true) {
	             result = result && (target.currentTargetChannel.record.BI_PL_Removed__c);
	         }*/

	        /**
	        * Search by hospital
	        */
	        if(self.selectedHospitalFilter) {
	            result = result && (target.record.BI_PL_Primary_parent__c === self.selectedHospitalFilter);
	        }

	        /**
	         * Search by affiliated account (in Planit affiliations only)
	         */
	         if(self.selectedAffiliatedFilter) {
	            if(target.affiliationsPlanit && target.affiliationsPlanit.length > 0) {
	                var parentMatch = false;
	                for (var i = target.affiliationsPlanit.length - 1; i >= 0; i--) {
	                    if(target.affiliationsPlanit[i].BI_PL_Parent__c == self.selectedAffiliatedFilter) {
	                        parentMatch = true;
	                        break;
	                    }
	                }
	                result = result && (parentMatch);
	            } else {
	                result = false;
	            }
	           
	         }

	        /*
	         * Search by empty details (accounts with no interactions)
	         */
	         if(!self.showEmptyDetails && target.currentTargetChannel.details && target.currentTargetChannel.details.length == 0){
	            result = false;
	         }

	        return result;
	    });


	    /**
	     * Ordering
	     */
	    if(self.orderProperty) {
	        filteredAndSorted = filteredAndSorted.sort(function(a, b) {
	            var aValue = _.get(a, self.orderProperty, '');
	            var bValue = _.get(b, self.orderProperty, '');

	            if(isNaN(aValue)){
	                return aValue.toString().localeCompare(bValue.toString());
	            } else {
	                return aValue - bValue;
	            }
	                
	        });
	        
	        
	    } else if(self.orderProductSegment) {
	        //We have to look into the details of the current target-channel
	        filteredAndSorted = filteredAndSorted.sort(function(a, b) {

	            var aValue;
	            var bValue;
	            _.each(a.currentTargetChannel.details, function(d){
	                if(d.BI_PL_Product__c === self.orderProductSegment) aValue = d.BI_PL_Row__c;
	            });
	            _.each(b.currentTargetChannel.details, function(d){
	                if(d.BI_PL_Product__c === self.orderProductSegment) bValue = d.BI_PL_Row__c;
	            });
	            if(aValue == undefined) aValue = -1;
	            if(bValue == undefined) bValue = -1;

	            if(isNaN(aValue)){
	                return aValue.toString().localeCompare(bValue.toString());
	            } else {
	                return aValue - bValue;
	            }
	                
	        });
	    }else if(self.orderSales)
	    {
	    	console.log("self.orderSalesColumn", self.orderSalesColumn);
	        filteredAndSorted = filteredAndSorted.sort(function(a, b) {

                var affA = _.find(a.affiliationsPlanit, function(o){
                    return o.BI_PL_Product__c == self.orderSales && o.BI_PL_Type__c == 'Sales';
                });
                var affB = _.find(b.affiliationsPlanit, function(o){
                    return o.BI_PL_Product__c == self.orderSales && o.BI_PL_Type__c == 'Sales';
                });


                if(affA == undefined || affB == undefined)
                	return 0;

                aValue = getSubcolumnFieldValue(self.orderSalesColumn, affA);
                bValue = getSubcolumnFieldValue(self.orderSalesColumn, affB);

                function getSubcolumnFieldValue(orderSalesColumn, affiliation){
	                if(orderSalesColumn == 'market')
	                {
	                    return affiliation.BI_PL_Market_Sales__c;
	                }
	                else if(orderSalesColumn == 'net')
	                {
	                    return affiliation.BI_PL_Product_Sales__c;
	                }
	                else if(orderSalesColumn == 'marketName')
	                {
	                    return affiliation.BI_PL_Market__c;
	                }
	                else if(orderSalesColumn == 'netTR')
	                {
	                    return affiliation.BI_PL_Net_sales_TRx__c;
	                }
	                else if(orderSalesColumn == 'unresAggAcc')
	                {
	                    return affiliation.BI_PL_Unrestricted_aggregate_access__c;
	                }
                }

	        	/*
	            var aValue;
	            var bValue;
	            _.each(a.affiliationsPlanit, function(d){
	                var productPair = d.BI_PL_Product__c + ((d.BI_PL_Secondary_product__c != undefined) ? d.BI_PL_Secondary_product__c : '')
	    
	                if(productPair === self.orderSales)
	                {
	                    if(self.orderSalesColumn == 'market')
	                    {
	                        aValue = d.BI_PL_Market_Sales__c;
	                    }
	                    else if(self.orderSalesColumn == 'net')
	                    {
	                        aValue = d.BI_PL_Product_Sales__c;
	                    }
	                    else if(self.orderSalesColumn == 'marketName')
	                    {
	                        aValue = d.BI_PL_Market__c;
	                    }
	                    else if(self.orderSalesColumn == 'netTR')
	                    {
	                        aValue = d.BI_PL_Net_sales_TRx__c;
	                    }
	                    else if(self.orderSalesColumn == 'unresAggAcc')
	                    {
	                        aValue = d.BI_PL_Unrestricted_aggregate_access__c;
	                    }
	                    
	                } 
	            });
	            _.each(b.affiliationsPlanit, function(d){
	                var productPair = d.BI_PL_Product__c + ((d.BI_PL_Secondary_product__c != undefined) ? d.BI_PL_Secondary_product__c : '')
	                if(productPair === self.orderSales)
	                {
	                    if(self.orderSalesColumn == 'market')
	                    {
	                        bValue = d.BI_PL_Market_Sales__c;
	                    }
	                    else if(self.orderSalesColumn == 'net')
	                    {
	                        bValue = d.BI_PL_Product_Sales__c;
	                    }
	                    else if(self.orderSalesColumn == 'marketName')
	                    {
	                        bValue = d.BI_PL_Market__c;
	                    }
	                    else if(self.orderSalesColumn == 'netTR')
	                    {
	                        bValue = d.BI_PL_Net_sales_TRx__c;
	                    }
	                    else if(self.orderSalesColumn == 'unresAggAcc')
	                    {
	                        bValue = d.BI_PL_Unrestricted_aggregate_access__c;
	                    }
	                }
	            });
	          */

	            if(aValue == undefined) aValue = -1;
	            if(bValue == undefined) bValue = -1;

	            if(isNaN(aValue)){
	                return aValue.toString().localeCompare(bValue.toString());
	            } else {
	                return aValue - bValue;
	            }
	                
	        });
	    }
	    if(self.orderDirection === -1) {
	        filteredAndSorted.reverse();
	    }

	    //console.timeEnd("BI_PL_PreparationUtilJS.getFilteredTargets");

	    return filteredAndSorted;
	},
	/**
	 *	Returns the preparation targets.
	 */
	getTargets : function(preparationId, offset, vue, MSLPreparation){
	    //Due to scope change, save a referecnce to Vue instance
	    var self = vue;

	    BI_PL_PreparationExt.prepareTargetsData2(preparationId, offset, 
	        function(response, event){
	        	console.log("PrepareTargetsData2", MSLPreparation);

	            if(event.statusCode == 200) {

	                self.setup.isCurrentUserParent = response.isCurrentUserParent;
	                self.setup.isParentPositionCycleConfirmed = response.isParentPositionCycleConfirmed;
	                
	                //Imporve performance
	                self.temporalTargets.push.apply(self.temporalTargets, response.targets);

	                self.accountIds.push.apply(self.accountIds, response.accountIds);

	                self.addProducts(response.products);
	                
	                self.channels.push.apply(self.channels, response.channels);
	                self.specialities.push.apply(self.specialities, response.specialities);
	                self.preparation = response.preparation;
	                self.totalDetails += response.totalDetails;
	                self.totalTargets += response.totalTargets;

	                //Ensure uniqueness of cahhnels and products
	                // self.products = _.uniqBy(self.products, 'Id');
	                self.channels = _.uniqBy(self.channels, 'value');

	                //Call again the method
	                if(response.lastIdOffset) {
	                    self.remoting_getTargets(response.lastIdOffset);
	                } else {
	                    self.allTargetsReady = true;

	                    var tempObject = {};

	                    for (var h = self.temporalTargets.length - 1; h >= 0; h--) {
	                        var target = self.temporalTargets[h];
	                        
	                        if(target.targetChannels) {
	                            for (var i in target.targetChannels) {
	                                //delete target.targetChannels[i].record.BI_PL_Details_Preparation__r;
	                                /*if(target.targetChannels[i].record.BI_PL_Channel__c === self.defaultChannel){
	                                    target.currentTargetChannel = target.targetChannels[i];
	                                }*/
	                            }
								target.currentTargetChannel = target.targetChannels[self.defaultChannel];
	                        }

                            BI_PL_PreparationUtilJS.cleanRelatedTargetFields(target);

	                        self.fillNewTarget(target);
	                        tempObject[target.record.BI_PL_Target_customer__c] = target;
	                    }
	                    self.targets = tempObject;

	                    //Change current channel if the default channel is not in the prep
	                    if(!_.find(self.channels, { 'value': self.defaultChannel }) ) {
	                        if(self.channels[0])
	                            self.currentChannel = self.channels[0].value;
	                    }

	                    self.remoting_getBusinessRules();
	                }
	                if(MSLPreparation)
	                	self.remoting_getKAMTargetsForCycle();

	            }else {
	                console.error('Remoting error when getting targets data');
	                alert("Remoting error when saving targets")
	                self.status.working = false;
	            }
	        }, 
	        {
	            escape: false,
	            buffer: false,
	            timeout : 120000
	        }
	    )
	},/*
	getPossibleNewTargets: function(accountSearchName, accountSearchCity, accountSearchState, preparationId, countryCode, vue){
        vue.warningTooManyRecordsNewTarget = false;
        vue.searchingNewTargets = true;
        //Call remote to get target and details
        var self = vue;

    	vue.$refs.addNewTargetPaginator.resetCurrentPage();

        BI_PL_PreparationExt.searchAccounts(accountSearchName, accountSearchCity, accountSearchState, preparationId, countryCode, vue.isCountryColumnAvailable("HCOTab"),
            function(response, event){
                // console.log('Posible targets response...');
                if(event.statusCode == 200) {
                    self.warningTooManyRecordsNewTarget = response.length > 500;


                    self.possibleNewTargets = [];
                    for(var i = 0;i < response.length - ((self.warningTooManyRecordsNewTarget) ? 1 : 0);i++){
                        // GLOS-447 - Issue adding an account from Veeva
                        // The Address_vod__r field is retrieved in SOQL but must be removed from the account field (no such column...):
                        delete response[i].record.BI_PL_Target_customer__r.Address_vod__r;

                        self.possibleNewTargets.push(response[i]);
                    }
                } else {
                    alert(event.message);
                }
                self.searchingNewTargets = false;
            }, 
            {
                escape: false,
                timeout : 60000
            }
        )
    },*/
    saveAllTargetsOnly : function(){
        // console.log("remoting_saveAllTargetsOnly");
        //Call remote to get target and details
        var self = this.vue;
        
        var chunkSize = this.vue.findChunkSize(this.vue.preparation);
        // console.log("Final chunk size: ", chunkSize);

        var targetsLen = this.vue.targetsList.length;
        var chunksNumber = Math.ceil(targetsLen / chunkSize);

        // this.showStatus('info', 'Saving all targets...');
        this.vue.status.working = true;

        var startIndex , endIndex ,targetsToSave;

        for (var i = 0; i < chunksNumber; i++) {
            startIndex = i * chunkSize;
            endIndex = startIndex + chunkSize;
            // targetsToSave = JSON.stringify(this.targets.slice(startIndex, endIndex));
            targetsToSave = this.vue.targetsList.slice(startIndex, endIndex);

            for(var w=0;w<targetsToSave.length;w++){
                var target = targetsToSave[w];
                for(var j in target.targetChannels){
                	if(target.targetChannels[j].record != undefined)
                    	delete target.targetChannels[j].record.Id
                }
            }

            // console.log("targetsToSave", targetsToSave);
            
            // console.log('Saving chunk ', i);
            //only send preparation in firs chunk
            var prep = null;

            var numberResponses = 0;

            console.log("remoting_saveAllTargetsOnly ", prep, targetsToSave, this.vue.preparation.BI_PL_External_id__c);

            BI_PL_PreparationExt.saveChanges(prep, targetsToSave, this.vue.preparation.BI_PL_External_id__c,
                function(response, event){
                    // console.log('Response from chunk ');
                    if(event.statusCode == 200) {
                    	console.log('remoting_saveAllTargetsOnly completed: ', numberResponses, chunksNumber)
                        // self.setup = response;
                        numberResponses++;
                        if(numberResponses == chunksNumber) {
                    	console.log('remoting_saveAllTargetsOnly completed')
                            self.status.working = false;
                            self.exitEditMode();
                        }
                    } else {
                        console.error('Remoting error when saving targets');
                        alert("Remoting error when saving targets")
                        self.status.working = false;
                    }
                }, 
                {   
                    buffer : false,
                    escape: true,
                    timeout : 60000
                }
            )
        }
    },
    saveAllTargets : function(){
        //Call remote to get target and details
        var self = this.vue;

        var chunkSize = this.vue.findChunkSize(this.vue.preparation);
        // console.log("Final chunk size: ", chunkSize);

        var targetsLen = this.vue.targetsList.length;
        var chunksNumber = Math.ceil(targetsLen / chunkSize);

        // this.showStatus('info', 'Saving all targets...');
        this.vue.status.working = true;

        var startIndex , endIndex ,targetsToSave;

        for (var i = 0; i < chunksNumber; i++) {
            startIndex = i * chunkSize;
            endIndex = startIndex + chunkSize;

            targetsToSave = this.vue.targetsList.slice(startIndex, endIndex);
            
            for(var w=0;w<targetsToSave.length;w++){
                var target = targetsToSave[w];
                for(var j in target.targetChannels){
                	if(target.targetChannels[j].record != undefined)
                    	delete target.targetChannels[j].record.Id
                }
            }
            // console.log('Saving chunk ', i);
            //only send preparation in firs chunk
            var prep = (i == 0) ? this.vue.preparation : null;

            var numberResponses = 0;

            console.log("remoting_saveAllTargetsOnly ", prep, targetsToSave, this.vue.preparation.BI_PL_External_id__c);
            
            BI_PL_PreparationExt.saveChanges(prep, targetsToSave, this.vue.preparation.BI_PL_External_id__c,
                function(response, event){
                    // console.log('Response from chunk ');
                    if(event.statusCode == 200) {
                    	console.log('remoting_saveAllTargetsOnly completed: ', numberResponses, chunksNumber)
                        // self.setup = response;
                        numberResponses++;
                        if(numberResponses == chunksNumber) {
                    	console.log('remoting_saveAllTargetsOnly completed')
                            self.status.working = false;
                            self.exitEditMode();
                        }
                    } else {
                        console.error('Remoting error when saving all targets');
                    }
                }, 
                {   
                    buffer : false,
                    escape: true,
                    timeout : 60000
                }
            )
        }
    },
    /**
     *	Checks if the adjustments made to the details accomplish the business rules.
     */
    hasPreparationErrors : function(){
        var self = this.vue;
        for(var i=0;i<self.targetsList.length;i++){
            if(self.targetsList[i].targetChannels){
            	try{
	                Object.keys(self.targetsList[i].targetChannels).forEach(function(targetChannelIndex){
	                	if(self.targetsList[i].targetChannels[targetChannelIndex].details){
		                    for(var d=0;d<self.targetsList[i].targetChannels[targetChannelIndex].details.length;d++){
		                        var detail = self.targetsList[i].targetChannels[targetChannelIndex].details[d];
		                        if(detail.BI_PL_Business_rule_ok__c == false)
		                            throw true;
		                    }
	                    }
	                });
	            }catch(e){
	            	return e;
	            }
            }
        }
        return false;
    },
     /**
     * In the "Add new target" modal this function is executed each time a target is selected to be added to the preparation.
     */
    fillNewTarget : function(target){
        if(!target.addresses){
            target.addresses = [];
        }
        if(!target.targetChannels[this.vue.currentChannel])
            target.targetChannels[this.vue.currentChannel] = {};

        if(!target.currentTargetChannel || !target.currentTargetChannel.record){
            target.currentTargetChannel = {
                record : {
                    BI_PL_Reviewed__c : undefined,
                    BI_PL_Removed__c : undefined,
                    BI_PL_Removed_reason__c : undefined,
                    BI_PL_Rejected__c : undefined
                },
                details : []
            };
        }

        target.currentTargetChannel.TgtA = this.vue.calculateTgta(target.currentTargetChannel);
    },
    /**
	 *	Removes the "__r" fields from the target records (needed to save the records in DB without errors).
	 */
    cleanRelatedTargetFields : function(target){
        // Remove BI_PL_Details_Preparation__r field from all channels:
        for(var channel in target.targetChannels)
            delete target.targetChannels[channel].record.BI_PL_Details_Preparation__r;
    },
    onNewTargetAndDetails : function(everythingOverlaps, newTarget){
        console.log('onNewTargetAndDetails', everythingOverlaps, newTarget);
        // If adding this target doesn't generate requests then fill all the data in the page and save.
        // Otherwise, no data about the target is saved and instead the request records are generated in the component.
        this.fillNewTarget(newTarget);

        this.vue.addingTargetToPreparation = true;
        this.vue.errorAddingTargetToPreparation = false;

        var self = this.vue;
        //console.log('remoting_saveNewTargets', newTarget, this.preparation.BI_PL_Country_code__c, this.preparation.BI_PL_Position_name__c, this.preparation.BI_PL_Position_cycle__r.BI_PL_Cycle__c)

        console.log('newTarget',newTarget);

        if(!everythingOverlaps){
	        BI_PL_PreparationExt.saveNewTarget(newTarget, self.preparation.BI_PL_Country_code__c, self.preparation.BI_PL_Position_name__c, self.preparation.BI_PL_Position_cycle__r.BI_PL_Cycle__c, self.preparation.BI_PL_External_id__c, function(response, event) {
	            if(event.statusCode == 200) {
	                // console.log('Saved new target success. Details:', response.targetChannels[0].details);
	                //Add to main targets collection with fresh data (affiliations, planit affiliations, related targets...)
	                response.currentTargetChannel = response.targetChannels[self.currentChannel];
	                
	                self.$set(self.targets, response.record.BI_PL_Target_customer__c, response)
	                //self.targets[response.record.BI_PL_Target_customer__c] = response;

	                // Display new products in the tables:
	                var newProductsFilled = [];
	                for(var i = 0;i<response.targetChannels[self.currentChannel].details.length;i++){
	                    newProductsFilled.push(self.generateProductPair(response.targetChannels[self.currentChannel].details[i].BI_PL_Product__r, response.targetChannels[self.currentChannel].details[i].BI_PL_Secondary_product__r));
	                }
	                self.addProducts(newProductsFilled);

	                $('#targetProductDetailsModal').modal('toggle');

	                self.calculateTgtasForTargets();

	                if(everythingOverlaps && self.$refs.transferAndShare){
	                    self.$refs.transferAndShare.reload();
	                }
	            } else {
	                self.errorAddingTargetToPreparation = true;
	            }
	            self.addingTargetToPreparation = false;
	        });
	    }else{
            $('#targetProductDetailsModal').modal('toggle');
	    }
    },


    //Extract target row functionality
    substractAdjustedValue : function(target, detail) {
        if(detail.BI_PL_Adjusted_details__c !== undefined && detail.BI_PL_Adjusted_details__c >= 1) {
            detail.BI_PL_Adjusted_details__c = detail.BI_PL_Adjusted_details__c - 1;
            this.checkBusinessRulesForDetail(detail);
            this.onUpdateDetailAdjustment(target, detail.BI_PL_Adjusted_details__c);
        }
    },
    addAdjustedValue : function(target, detail) {
        if(detail.BI_PL_Adjusted_details__c  !== undefined && detail.BI_PL_Adjusted_details__c >= 0) {
            detail.BI_PL_Adjusted_details__c = detail.BI_PL_Adjusted_details__c + 1;
            this.checkBusinessRulesForDetail(detail);
            this.onUpdateDetailAdjustment(target, detail.BI_PL_Adjusted_details__c);
        }
    },
    substractMaxAdjustedValue : function(channelDetail) {
            if(channelDetail.record.BI_PL_Max_adjusted_interactions__c !== undefined && channelDetail.record.BI_PL_Max_adjusted_interactions__c >= 1) {
            channelDetail.record.BI_PL_Max_adjusted_interactions__c = channelDetail.record.BI_PL_Max_adjusted_interactions__c - 1;
            this.onChangeTargetAjustement(channelDetail);
        }
    },
    addMaxAdjustedValue : function(channelDetail) {
        if(channelDetail.record.BI_PL_Max_adjusted_interactions__c !== undefined && channelDetail.record.BI_PL_Max_adjusted_interactions__c >= 0) {
                channelDetail.record.BI_PL_Max_adjusted_interactions__c = channelDetail.record.BI_PL_Max_adjusted_interactions__c + 1;
                this.onChangeTargetAjustement(channelDetail);
            }
    },

    // 19/07/2017
    // Fix: Max A is not being updated on the fly
    onUpdateDetailAdjustment : function(target, adjustedValue) {
        // console.log(target, adjustedValue);
        target.currentTargetChannel.record.BI_PL_Edited__c = true;

        var maxValue = -Infinity;
        var sumValue = 0;

        for(var i=0;i<target.currentTargetChannel.details.length;i++){
            var adj = target.currentTargetChannel.details[i].BI_PL_Adjusted_details__c;
            sumValue += adj;

            // console.log("DETAIL : ", target.currentTargetChannel.details[i])
            // console.log(target.currentTargetChannel.details[i].BI_PL_Adjusted_details__c )
            if(adj > maxValue)
                maxValue = adj;
        }
        // console.log(maxValue);
        //if(target.currentTargetChannel.record.BI_PL_Max_adjusted_interactions__c < adjustedValue)
            target.currentTargetChannel.record.BI_PL_Max_adjusted_interactions__c = maxValue;
            target.currentTargetChannel.record.BI_PL_Sum_adjusted_interactions__c = sumValue;

        // this.$emit('detail-adjusted', target.currentTargetChannel);
    },

    onChangeTargetAjustement : function(channelDetail) {
        // console.log('onChangeTargetAjustement')
        channelDetail.record.BI_PL_Edited__c = true;
        let adjusted = channelDetail.record.BI_PL_Max_adjusted_interactions__c;
        for (var i = channelDetail.details.length - 1; i >= 0; i--) {
            var detail = channelDetail.details[i];
            detail.BI_PL_Adjusted_details__c = adjusted;
            this.checkBusinessRulesForDetail(detail);
        }
     },

    /**
     *  Checks if the adjustments made accomplish the Segmentation business rules, which are set by fieldforce, product and segment.
     *  (The fieldforce filtering is made in APEX).
     */
    checkBusinessRulesForDetail : function(detail, businessRulesGrouped){
        var productPairId = this.generateProductPairId(detail.BI_PL_Product__r, detail.BI_PL_Secondary_product__r);
        var br = businessRulesGrouped.bySegment[detail.BI_PL_Segment__c] ? businessRulesGrouped.bySegment[detail.BI_PL_Segment__c][productPairId] : undefined;
        if(br){
            if((br.BI_PL_Min_value__c != undefined && detail.BI_PL_Adjusted_details__c < br.BI_PL_Min_value__c)
                || (br.BI_PL_Max_value__c != undefined && detail.BI_PL_Adjusted_details__c > br.BI_PL_Max_value__c)) {
                detail.BI_PL_Business_rule_ok__c = false;
            } else {
                detail.BI_PL_Business_rule_ok__c = true;
            }
        }else{
            detail.BI_PL_Business_rule_ok__c = true;
        }
        // console.log('checkBusinessRulesForDetail called', detail.BI_PL_Business_rule_ok__c);
    },
    getBusinessRulesMessageForDetail : function(detail, businessRulesGrouped){
        console.log('getBusinessRulesMessageForDetail called');
        var msg ;
        var productPairId = this.generateProductPairId(detail.BI_PL_Product__r, detail.BI_PL_Secondary_product__r);

        var br = businessRulesGrouped.bySegment[detail.BI_PL_Segment__c] ? businessRulesGrouped.bySegment[detail.BI_PL_Segment__c][productPairId] : undefined;
        if(br){
            var max = businessRulesGrouped.bySegment[detail.BI_PL_Segment__c][productPairId] ? businessRulesGrouped.bySegment[detail.BI_PL_Segment__c][productPairId].BI_PL_Max_value__c : '';
            var min = businessRulesGrouped.bySegment[detail.BI_PL_Segment__c][productPairId] ? businessRulesGrouped.bySegment[detail.BI_PL_Segment__c][productPairId].BI_PL_Min_value__c : '';
            msg = "{!$Label.BI_PL_Prod_segment_greater_less}".split("@");
            msg = msg[0] + '(' + detail.BI_PL_Segment__c +')' + msg[1] + min + msg[2] + ' '+ max;
        }
        return msg;

    },
    generateProductPairId : function(product1, product2){
        return product1.Id + ((product2 != undefined) ? product2.Id : '');
    },
 	getInitialSetupDataMSL: function(){
        console.log('getInitialSetupDataMSL');
        //Call remote to get target and details
        var self = this;

        BI_PL_PreparationExt.getSetupDataMSL(function(response, event){
        		self.initialSetupDataCallback(response, event, true);
        	},
            {
                escape: false,
                timeout : 60000
            }
        )
    },
 	getInitialSetupData: function(){
        console.log('getInitialSetupData');
        //Call remote to get target and details
        var self = this;

        BI_PL_PreparationExt.getSetupData( function(response, event){
        		self.initialSetupDataCallback(response, event, false);
        	},
            {
                escape: false,
                timeout : 60000
            }
        )
    },
    initialSetupDataCallback : function(response, event, MSLPreparation){

        if(event.statusCode == 200) {
            // self.setup = response;
            var setup = JSON.parse(response);
            //Mapping values
            this.vue.permissions.isDSM = setup.isDSM;
            this.vue.permissions.isDS = setup.isDS;
            this.vue.permissions.isSR = setup.isSR;
            this.vue.permissions.isSM = setup.isSM;

            this.vue.setup.operators = setup.operators;
            this.vue.setup.fields = setup.fields;
            this.vue.setup.allChannels = setup.allChannels;
            this.vue.setup.options = setup.options;
            this.vue.setup.segments = setup.segments;
            this.vue.setup.preparationStatuses = setup.preparationStatuses;
            this.vue.setup.allowAllProducts = setup.allowAllProducts;
            this.vue.setup.transferAndShareEnabled = setup.transferAndShareEnabled;
            this.vue.setup.editPreparationByDefault = setup.editPreparationByDefault;
            this.vue.setup.addTargetMode = setup.addTargetMode;
            this.vue.setup.sectionsCollapsed = setup.sectionsCollapsed;
            this.vue.setup.mustWinAndHyperTargetFieldforces = setup.mustWinAndHyperTargetFieldforces;
            this.vue.setup.useSum = setup.useSum;

            console.log('allowDroppingTargets', setup.allowDroppingTargets);

            this.vue.setup.allowDroppingTargets = setup.allowDroppingTargets;

            this.vue.countryColumns = setup.countryColumns;
            this.vue.viewColumns = setup.viewColumns;

            this.vue.setupReady = true;

            this.collapseSections(setup.sectionsCollapsed);

            this.getTargets(this.vue.preparationId, null, this.vue, MSLPreparation);
        } else {
            console.error('Remoting error when getting setup data');
        }
    },
    collapseSections : function(collapse){
    	if(!collapse){    		  
    		$("#section1").removeClass();
    		$("#section1").addClass('panel-collapse collapsed collapse in');
    		$("#section2").removeClass();
    		$("#section2").addClass('panel-collapse collapsed collapse in');
    		$("#section3").removeClass();
    		$("#section3").addClass('panel-collapse collapsed collapse in');
    	}else{
    		$("#section1").removeClass();
    		$("#section1").addClass('panel-collapse collapsed collapse');
    		$("#section2").removeClass();
    		$("#section2").addClass('panel-collapse collapsed collapse');
    		$("#section3").removeClass();
    		$("#section3").addClass('panel-collapse collapsed collapse');  
    	}
    }
};