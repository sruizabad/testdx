$(document).ready(function() {
	if(!canClone || !canSync){
		disableButtons();
	}
	setScreenProperties();
	initMainTable();
	initSyncTable();
	initModals();
});

function disableButtons(){
	$('#clone').prop("disabled", !canClone);
	$('#synchronize').prop("disabled", !canSync);
}

function setScreenProperties(){
	//$('.action-button').prop('disabled',!canClone);
	$('.buttonsBlock').find('input.btn').each(function(){
		console.log($(this));
		$(this).addClass('customBtn');
	});
}

function initMainTable(){
	table = $('#mainTable').DataTable({
        fixedHeader: true,
        "dom":  '<f<t>ipl>',
        "order": [[1, "asc"]],
        language: {
            searchPlaceholder: "Search records",
            search: " "
        }
    });
	
	$('#mainTable_length').addClass('col-md-3 col-xs-12 text-right');
	$('#mainTable_paginate').addClass('col-md-6 col-xs-12');
	$('#mainTable_info').addClass('col-md-3 col-xs-12');
}


function initSyncTable(){
	table = $('#syncTable').DataTable({
        fixedHeader: true,
        "dom":  '<f<t>ipl>',
        "order": [[1, "asc"]],
        language: {
            searchPlaceholder: "Search records",
            search: " "
        }
    });
	
	$('#syncTable_length').addClass('col-md-3 col-xs-12 text-right');
	$('#syncTable_paginate').addClass('col-md-6 col-xs-12');
	$('#syncTable_info').addClass('col-md-3 col-xs-12');
}

function initModals(){
	// Initialize Modal Lists HTML
	/*for(var i=0; i<lSAPsToClone.length; i++){
		console.log(lSAPsToClone[i].Name);
		var SAPName =  lSAPsToClone[i].Name;
		$('#sapsToCloneList').append('<li>' + SAPName + '</li>');
		$('#sapsToSyncList').append('<li>' + SAPName + '</li>');
	}*/
	
	$('.input-date').each(function() {
		console.log($(this));
		var today = new Date();
		$(this).val(''+ today.getDate() +'/'+ (today.getMonth() + 1) +'/'+ today.getFullYear());
	    $(this).datepicker({
	    	defaultViewDate: new Date(),
	    	startDate: new Date(),
	    	format: 'dd/mm/yyyy',
	    	todayHighlight: true
	    });
	});
	
	$('#syncButton').click(function(){
		var SAPName = ($('#SAPNameInput').val()) ? $('#SAPNameInput').val() : '';
		if(SAPName!=''){
			syncSAPs(SAPName);
		}else{
			$('#synchronizeModal .errors-wrapper').html('');
			$('#synchronizeModal .errors-wrapper').append('<p class="error-msg">SAP Name field can not be empty.</p>');
		}
	});
	
	$('#cloneButton').click(function(){
		var startDateInput = $('#startDateInput');
		var endDateInput = $('#endDateInput');
		var validated = validateDates([startDateInput,endDateInput]);
		if(validated){
			cloneSAPs(startDateInput, endDateInput);
		}
	});
}

function validateDates(datesArr){
	var validated = true;
	var errorsArr = [];
	$('#cloneModal .errors-wrapper').html('');
	for(var i=0;i<datesArr.length; i++){
		datesArr[i].removeClass('has-error');
		try{
			if(datesArr[i].val() != ''){
				var day = datesArr[i].val().split('/')[0];
				var month = datesArr[i].val().split('/')[1];
				var year = datesArr[i].val().split('/')[2];
				var d = new Date(month, day, year);
				if(d == 'Invalid Date'){
					throw 'Invalid Date';
				}
			}else{
				validated = false;
				datesArr[i].addClass('has-error');
				errorsArr.push(datesArr[i].data('name') + ' cannot be empty');
			}
		}catch(err){
			validated = false;
			datesArr[i].addClass('has-error');
			errorsArr.push(datesArr[i].data('name') + ' is invalid');
		}
	}
	
	if(validated){
		var startDay = datesArr[0].val().split('/')[0];
		var startMonth = datesArr[0].val().split('/')[1];
		var startYear = datesArr[0].val().split('/')[2];
		var startDate = new Date(startMonth, startDay, startYear);
		
		var endDay = datesArr[1].val().split('/')[0];
		var endMonth = datesArr[1].val().split('/')[1];
		var endYear = datesArr[1].val().split('/')[2];
		var endDate = new Date(endMonth, endDay, endYear);
		
		var diff = endDate - startDate;
		
		console.log(startDate);
		console.log(endDate);
		console.log(diff);
		
		if(diff <= 0 || isNaN(diff)){
			for(var i=0;i<datesArr.length; i++){
				datesArr[i].addClass('has-error');
			}
			errorsArr.push('End date must be greather than start date');
			validated = false;
		}
	}
	
	if(!validated){
		errorsArr.forEach(function(err){
			$('#cloneModal .errors-wrapper').append('<p class="error-msg">'+ err +'</p>');
		});
	}
	return validated;
}

/*function getNextMonthDate(){
	var now = new Date();
	if (now.getMonth() == 11) {
	    var current = new Date(now.getFullYear() + 1, 1, now.getDate());
	} else {
	    var current = new Date(now.getFullYear(), now.getMonth() + 2, now.getDate());
	}
	return current;
}*/

function syncSAPs(SAPName){
	console.log(JSON.stringify(lSAPsToSync));
	console.log('SAPName', SAPName);
	var remoteFn = this.window.synchronizeFn;
	Visualforce.remoting.Manager.invokeAction(
			remoteFn,
			SAPName,
			JSON.stringify(lSAPsToSync), 
			function(result,event){
				if(event.status){
					if(result){
						$("#synchronizeModal .close").click();
					}else{
						$('#synchronizeModal .errors-wrapper').append('<p class="error-msg">'+ event.message+'</p>');
					}
				}else{
					$('#synchronizeModal .errors-wrapper').append('<p class="error-msg">'+ event.message+'</p>');
				}
			},
			{escape: true}
        );
	
}

function cloneSAPs(startDateInput,endDateInput){
	console.log(startDateInput.val());
	console.log(endDateInput.val());
	console.log(JSON.stringify(lSAPsToClone));
	var remoteFn = this.window.cloneFn;
	Visualforce.remoting.Manager.invokeAction(
			remoteFn,
			JSON.stringify(lSAPsToClone),
			String(startDateInput.val()), 
			String(endDateInput.val()),
			function(result,event){
				if(event.status){
					if(result){
						$("#cloneModal .close").click();
					}else{
						$('#synchronizeModal .errors-wrapper').append('<p class="error-msg">'+ event.message+'</p>');
					}
				}else{
					$('#synchronizeModal .errors-wrapper').append('<p class="error-msg">'+ event.message+'</p>');
				}
			},
			{escape: true}
        );
}