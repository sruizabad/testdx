function register_event(){	//all event
	j$('#roleSeletorDivId .draggable').off('mouseenter mouseleave').on('mouseleave mouseenter', e_over_draggable);
	j$('#roleSeletorDivId .ui-state-disabled').off('mouseenter mouseleave');
	
	var sortable_options = {containment : '#theTopDivId', cancel : '.ui-state-disabled', cursor : 'move'},
		droppable_options = {hoverClass : 'hoverTarget', activeClass : 'dropTarget', cursor : 'move'};
		
	j$('#roleSeletorDivId ul.dropUlCls').sortable({
		opacity: 0.7,
		delay: 200,
		revert: true,
		cancel: sortable_options.cancel,
		containment: sortable_options.containment,
		connectWith: 'ul.matrixDraggable',
		
		helper: function (e, item){
			if(!item.hasClass('clicked'))
				   item.addClass('clicked');
			var elements = j$('.clicked').not('.ui-sortable-placeholder').clone();//clone().addClass('ui-state-disabled').addClass('role-state-disabled');
			var ul = j$('<ul/>');
			return ul.append(elements);
		},
		start: function (e, ui) {
			var elements = ui.item.siblings('.clicked.hidden').not('.ui-sortable-placeholder');
			ui.item.data('items', elements);
		},
		receive: function (e, ui) {
			ui.item.before(ui.item.data('items'));
		},
		stop: function (e, ui) {
			ui.item.siblings('.clicked').removeClass('hidden');
			j$('.clicked').removeClass('clicked');
		}
	});
	 
	j$("div[id^='groupRole']").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.7, 
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			stop: matrix_stop_fn
		});
	});
	
	j$("div[id^='groupRole']").droppable({
		accept:".draggable, .matrixDraggable",
		hoverClass: droppable_options.hoverClass,
		activeClass: droppable_options.activeClass,
		drop : function( drag, drop ) {

			if(typeof (drop.helper.context) != 'undefined'){
				droppable_drop_fn(drag, drop);
			}else{
				droppable_drop_fn_multiselect(drag,drop);
			}
		}
	});
	
	j$("#roleSeletorDivId ul").droppable({
		accept:".matrixDraggable",
		hoverClass: droppable_options.hoverClass,
		activeClass: droppable_options.activeClass,
		drop : droppable_drop_fn
	});

}


function e_over_draggable(e){
	var j$this = j$(this);
	switch(e.type){
		case 'mouseenter': j$this.addClass('pgover'); break;
		default : j$this.removeClass('pgover'); break;
	}
}

function droppable_drop_fn_multiselect (drag,drop){
	//Iterate every li selected and drop into the group
	jQuery.each( drop.helper.find("li"), function( i, val ){
		
		var j$dragCurrent = j$("#roleSeletorDivId ul li[Id="+j$(val).attr('Id')+"]");//console.log(':: DragCurrent: ', j$dragCurrent);
		var currentId = j$(val).attr('id'), 
		parentId = 'roleSeletorDivId';
		//console.log('currentId: '+ currentId + ', parentId : ' + parentId + ' targetId: ' + targetId + ' conditionId: '  + conditionId);
				 
		//find a available li -- id is null
		var hasFinded = false;
		var j$dragTarget, targetId;
		
		j$(drag.target).find('li').each(function(){
			if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){
				if(!hasFinded){
					hasFinded = true;
					j$dragTarget = j$(this), targetId = j$(this).attr('id');
				}
			}
		});
		
		//if has not find any available li, do nothing
		if(typeof(j$dragTarget) != 'undefined'){
			j$dragTarget.attr({'id': currentId}).addClass('pgover').html(j$(val).find('span').attr('title')).removeClass('ui-state-disabled');
			j$dragCurrent.addClass('ui-state-disabled').addClass('role-state-disabled');
			j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave');
		}
		else{
			j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgover" title="'+j$dragCurrent.find('span').attr('title')+'">' + j$dragCurrent.find('span').attr('title') + '</li>');
			j$dragCurrent.addClass('ui-state-disabled').addClass('role-state-disabled');
			j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave');
			j$(drag.target).find('ul').append(j$dragTarget);
		}
	});
}


function droppable_drop_fn(drag,drop){
	var j$dragCurrent = j$(drop.helper.context), conditionId,
	currentId = j$.trim(j$dragCurrent.attr('id')), 
	parentId = j$.trim(j$dragCurrent.parent().parent().attr('id'));
	
	conditionId = (typeof(parentId) != 'undefined' && j$.trim(parentId) != '' && (parentId.indexOf('groupRole') > -1)) ? 'matrixDivId' : parentId;  
	var targetId = j$(drag.target).attr('id');
	
	if((targetId.indexOf('groupRole') > -1) && (parentId.indexOf('groupRole') > -1)){
		conditionId = 'horizontalMove';
	}
	
	//console.log('currentId: '+ currentId + ', parentId : ' + parentId + ' targetId: ' + targetId + ' conditionId: '  + conditionId);
	
	if(typeof(targetId) != 'undefined' && typeof(parentId) != 'undefined' && j$.trim(targetId) != '' && j$.trim(parentId) != '' && targetId != parentId){
		switch(conditionId){
			case 'horizontalMove' :
				 //console.log('horizontalMove');
				var hasFinded = false;
				var j$dragTarget, targetId;
				
				j$(drag.target).find('li').each(function(){
					if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
						if(!hasFinded){
							hasFinded = true;
							j$dragTarget = j$(this), targetId = j$(this).attr('id');
						}
					}
				});
				//if has not find any available li, do nothing
				if(typeof(j$dragTarget) != 'undefined'){
					j$dragTarget.attr({'id': currentId}).addClass('pgover').html(j$dragCurrent.text()).removeClass('ui-state-disabled');
						
					j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
				}
				else{
					j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgover" title="'+j$dragCurrent.attr('title')+'">' + j$dragCurrent.text() + '</li>');
					j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
					j$(drag.target).find('ul').append(j$dragTarget);
				}
				//resetDivHeight();
				break;
			
			case 'matrixDivId' : 
				var j$lis = j$(drag.target).find('li');
				j$lis.each(function(){
					if(currentId == j$(this).attr('id')){
						j$(this).removeClass('ui-state-disabled').removeClass('role-state-disabled');
						j$(this).off('mouseenter mouseleave').on('mouseleave mouseenter', e_over_draggable);
						j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
					}
				});
				break;
			default :  break;
		}
	}
}

/**
 *  Used to add a new Group 
 *
 @author  Hely
 @created 2015-01-26
 @email	  hely.lin@itbconsult.com
 *
 */
function addGroup(){
	var groupIndex = parseInt(j$('#groupIndex').val()) + 1;
	var tableStr = '';
	
	var tableNum = parseFloat(j$('#matriDivId').children('table.groupTable').length);
	var divWidth = (tableNum + 1) * 250;
	j$('#matriDivId').css("width", divWidth);
	
	tableStr += '<table class="groupTable">' +
					'<tr>' +
						'<td style="display:block;">' +
						//	'Group <span class="groupIndexSpan">' + groupIndex + '</span> <input type="text" class="groupNameInput" name="groupNameInput"/>' +
						//	'<img title="delete this Group" class="deleteMatrixImg" style="cursor:pointer; margin-left:5px; vertical-align:middle; height:15px; width:15px;" onclick="removeGivenTable(this);" src="/img/func_icons/util/recycle.gif"></img>' +
						'<div class="groupNameDiv">' +
						'<span>{!$Label.IMP_BI_Group} </span><span class="groupIndexSpan">' + groupIndex + '</span>' +
						'</div>' +
						'<div style="float:right;margin-right:10px;">' +
						'<div class="requiredInput">' +
							'<div class="requiredBlock"></div>' +
							'<input type="text" class="groupNameInput" name="groupNameInput" onchange="checkAllGroupName(true);"/>' +
							'<img title="delete this Group" class="deleteMatrixImg" style="cursor:pointer; margin-left:5px; vertical-align:middle; height:15px; width:15px;" onclick="removeGivenTable(this);" src="/img/func_icons/util/recycle.gif"></img>' +
						'</div>' +
						'</div>' +
						'<div style="clear:both;"></div>' +
						'<div id="erMsg'+groupIndex+'" name="erMsg" class="errorMsg groupErrMsg" style="display:none;">' +
							'<span style="color:#C00;"><strong>Error: </strong>Group name can not be the same with other roleity name which do not belong to it.</span>' +
						'</div>' +
						'</td>' +
					'</tr>' +
					'<tr>'+
						'<td style="text-align: left;">'+
							'<span class="matrixTableLabel"><b>'+
								'{!$Label.IMP_BI_All_Unselected_Roles}' + ':</b>&nbsp;'+
								'<input type="checkbox"  class="chkAllRolety" id="chkAllRolety" name="chkAllRolety"  onclick="checkAllRoles(this);" />'+
							'</span>'+
						 '</td>'+
					'</tr>'+
					'<tr>' +
						'<td>' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td>' +
							'<div id="groupRole' + groupIndex + '" name="groupRole" style="height: 300px; width: 200px; border: 1px solid #000; overflow:auto;" class="roleCls ui-droppable">' +
								'<ul class="targerDropUlCls matrixDropUlCls ui-sortable">';
									for(var i=0; i<0; i++){
										tableStr +='<li id="" class="matrixDraggable"></li>';
									}
								tableStr +='</ul>' +
							'</div>' +
						'</td>' +
					'</tr>' +
				'</table>';
				
	j$('#matriDivId').append(tableStr);
	
	j$('#groupIndex').val(groupIndex);
}

/**
 *  Used to add a new Group 
 *
 @author  Hely
 @created 2015-01-26
 @email	  hely.lin@itbconsult.com
 *
 */
 
function collectRoleValue(){
	var sArr = new Array();

	j$("#matriDivId table").each(function(){
		var j$this = j$(this);
		
		var gName = j$.trim(j$("input[name='groupNameInput']", j$this).val());
		
		j$("div[id^='groupRole'] li", j$this).each(function(){
			var liId = j$(this).attr('Id');
			if(liId){
				var r = new Object();
				r.rId = liId;
				r.rName = j$.trim(j$(this).text());
				r.rGroup = gName;
				sArr.push(r);
			}
		});
	});
	
	j$("#roleSeletorUlId li").not('.ui-state-disabled').each(function(){
		var j$this = j$(this);
		var sId = j$(this).attr('Id');
		
		if(sId){
			j$this.find("span").each(function(){
				var r = new Object();
				r.rId = sId;
				r.rName = j$.trim(j$(this).text());
				r.rGroup = null;
				sArr.push(r);
			});
		}
	});

	return JSON.stringify(sArr);
}

/**
* 用来验证Group input（是否为空，是否已存在）和clsGroup下的list_ro是否为空
@author  Hely
@created 2015-01-26
@email	  hely.lin@itbconsult.com
*/
function checkAllGroupName(change){
	var groupIndex = 0;
	var hasError = false;
	var arr_GName = new Array();
	j$("#matriDivId table").each(function(){
		groupIndex++;
		var j$this = j$table = j$(this);
		var gName = j$.trim(j$("input[name='groupNameInput']", j$this).val());
		var sIds = '';
			
		if(gName=='' || gName.length<1){
			hasError = true;
			j$("#erMsg"+groupIndex).html("<strong>Error:</strong>Group name can not be empty.");
			j$("#erMsg"+groupIndex, j$table).show();
			return hasError;
		}
		
		for(var i=0;i<arr_GName.length;i++){
			if(arr_GName[i]==gName){
				hasError = true;
				j$("#erMsg"+groupIndex).html("<strong>Error:</strong>Group name already exists.");
				j$("#erMsg"+groupIndex, j$table).show();
				return hasError;
			}
		}
		arr_GName.push(gName);
		if(!change){
			var roleCount = 0;//用来判断该Group 中Role的个数，若小于1则return 并提示
			j$("div[id^='groupRole'] li", j$this).each(function(){
				roleCount++;
				var liId = j$(this).attr('Id');
				if(liId){
					if(sIds == '') sIds = liId;
					else sIds += ';' + liId;
				}
			});
		
			if(roleCount<1){
				hasError = true;
					j$("#erMsg"+groupIndex).html("<strong>Error:</strong>Please select the role of group.");
					j$("#erMsg"+groupIndex, j$table).show();
					return hasError;
			}
		}
			
		j$("div[name=erMsg]", j$table).hide();
		
	});
	return hasError;
}
/*
function checkSingleGroupName(obj){
	var arr_GName = new Array();
	var j$this = j$(obj), j$table = j$(getParent(j$this.get(0), 'TABLE'));
	var gName = j$.trim(j$this.val());
	var sIds = '', hasError = false;
	j$("#matriDivId table").each(function(){
		var j$this = j$table = j$(this);
		var gName = j$.trim(j$("input[name='groupNameInput']", j$this).val());
		var sIds = '';
		if(gName=='' || gName.replace(/(^\s*)|(\s*$)/g, "").length<1){
			hasError = true;
			j$("div[name=erMsg]").html("<strong>Error:</strong>Group name can not be empty.");
			j$("div[name=erMsg]", j$table).show();
			return ;
		}
		for(var i=0;i<arr_GName.length;i++){
			if(arr_GName[i]==gName){
				hasError = true;
				j$("div[name=erMsg]", j$table).show();
				return ;
			}
		}
		arr_GName.push(gName);
		
	   
	});
	j$("div[id^='groupRole'] li", j$table).each(function(){
		var liId = j$(this).attr('Id');
		if(liId){
			if(sIds == '') sIds = liId;
			else sIds += ';' + liId;
		}
	});
	
	if(sIds != ''){
		j$("#roleSeletorUlId li").each(function(){
			var j$this = j$(this);
			var sId = j$(this).attr('Id');
			
			if(sId && sIds.indexOf(sId) == -1){
				j$this.find("span").each(function(){
					if(gName == j$.trim(j$(this).text())){
						hasError = true;
					}
				});
			}
		});
	}
	
	if(hasError) j$("div[name=erMsg]", j$table).show();
	else j$("div[name=erMsg]", j$table).hide();
}
*/

function matrix_stop_fn(event, ui){
	var parentUl = j$(this);
	var counter = 0;
	
	var tagName = j$(this).get(0).tagName;
	if(tagName == 'DIV'){
		parentUl = j$("ul", j$(this));
	}
	

	j$("li", parentUl).each(function(){
		if(!j$(this).text().trim()){
			j$(this).remove();
			counter ++;
		}
	});
	
	for(var i = 0; i < counter; i++){
		 parentUl.append('<li id="" class="matrixDraggable ui-state-disabled"></li>');
	}

}

/**
 *  Used to remove the role group table
 *
 @author  Hely
 @created 2015-01-26
 @email	  hely.lin@itbconsult.com
 *
 */
function removeGivenTable(obj){
	var j$table = j$(getParent(j$(obj).get(0), 'TABLE'));
	
	j$("li", j$table).each(function(){
		if(j$(this).attr('id')){
			var liId = j$(this).attr('id');
			
			j$("#roleSeletorDivId li[id='" + liId + "']").removeClass('ui-state-disabled').removeClass('role-state-disabled');
			j$("#roleSeletorDivId li[id='" + liId + "']").off('mouseenter mouseleave').on('mouseleave mouseenter', e_over_draggable);
		}
	});
	
	j$table.remove();
	
	var groupIndex = parseInt(j$('#groupIndex').val()) - 1;
	j$('#groupIndex').val(groupIndex);
	
	var tableNum = parseFloat(j$('#matriDivId').children('table.groupTable').length);
	var divWidth = (tableNum + 1) * 250;
	j$('#matriDivId').css("width", divWidth);
	
	refreshTableIndex();
}


function matrix_stop_fn_void(){}

function refreshTableIndex(){
	var tableIndex = 0;
	j$("#matriDivId table span[class='groupIndexSpan']").each(function(){
		tableIndex ++;
		j$(this).text(tableIndex);
	});
}

function resetMatriDivIdSize(){
	var windowWidth = 0;
	
	if (window.innerWidth)
		windowWidth = window.innerWidth;
	else if ((document.body) && (document.body.clientWidth))
		windowWidth = document.body.clientWidth;
	else windowWidth = window.screen.width;
	
	if(windowWidth < 800) windowWidth = 800;
	
	var mainDivWidth = windowWidth - j$("#matriDivId").offset().left -80;

	j$('#matriDivId').css("max-width", mainDivWidth);
}

//** assist function to find element's parent with specified tag name **//
function getParent(el,p){
	do{
		el = el.parentNode;
	}while(el && el.nodeName != p)
	return el;
}


/**
 * Select all the specilaties for each group 
 */

function checkAllRoles(obj){
	//get true or false
	if(obj){
		if(obj.checked){
			var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.roleCls'), targetId, j$dragTarget, j$dragCurrent;
			j$("li:not(.role-state-disabled)", j$("#roleSeletorDivId")).each(function(){
				j$dragCurrent = j$(this);
				var currentId = j$dragCurrent.attr('id');
				var hasFound = false;
				//for overflowHidden
				var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
				if(j$div.hasClass('overflowHidden')){
					j$div.removeClass('overflowHidden');
				}
				j$divTarget.find('li').each(function(){
					if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//clear old channel
						if(!hasFound){
							hasFound = true;
							j$dragTarget = j$(this), targetId = j$(this).attr('id');
						}
					}
				});
				 
				//if has not find any available li, do nothing
				/*if(typeof(j$dragTarget) != 'undefined'){
					alert('if');
					j$dragTarget.attr({'id': currentId}).addClass('pgover').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled');
					j$dragCurrent.addClass('ui-state-disabled').addClass('role-state-disabled').css({'opacity': 1});
					j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave');
					j$divTarget.find('ul').append(j$dragTarget);
				}else{*/ 
					j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgover">' + j$dragCurrent.find('span').attr('title') + '</li>');
					j$dragCurrent.addClass('ui-state-disabled').addClass('role-state-disabled').css({'opacity': 1});
					j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave');
					j$divTarget.find('ul').append(j$dragTarget);
				//}
			});
		}
	}
}