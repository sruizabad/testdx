function register_event(){	//all event
	$('body').off('click').on('click',e_click_body);
	$('#channelDivId .draggable').off('mouseenter mouseleave').on('mouseleave mouseenter', e_over_draggable);
	$('#channelDropDivId div.overMenu').off('mouseenter mouseleave click').on('mouseenter mouseleave click', e_over_click_overMenu);
	$('#chartDivId').off('mouseenter mouseleave click').on('mouseenter mouseleave click', e_over_click_chart);
	$('#matrixTotalClsId').off('mouseenter mouseleave').on('mouseenter mouseleave', e_over_overTable);
	$('#tipsnoClickId .tipsTotalDivCls').off('click').on('click', e_click_tipsTotal_avg);

	$('#overTableHideId').off('mouseenter mouseleave click').on('mouseenter mouseleave click', e_over_click_overTable);
	$('#channelDropDivId').off('mouseenter mouseleave').on('mouseenter mouseleave', e_over_channelDropDivId);
	$('#channelButtonTrId input[type="button"]').off('click mousedown').on('click mousedown', e_click_channelButton);
	//$('#channelInputTrId input[type="text"]').off('focus change').on('focus change',  e_focus_change_channelText);

	//Register events for checkboxes
	$('#chkPhys').change(function() {
    if($(this).is(':checked')) {
      $('#matrixData li.ui-selectee div.physDiv').css('display','');
    } else {
      $('#matrixData li.ui-selectee div.physDiv').css('display','none');
    }
  });

	// Begin: Added by Antonio Ferrero 15-Jul-2015
	$('input[id^="chkAvgQty_"]').change(function() {
		var id = $(this).attr('id');
		var idx = id.substring(10);
		if(idx) {
			if($(this).is(':checked')) {
				$(('#matrixData li.ui-selectee div.class_'+ idx )).css('display','block');
				showOrHideNifTds(true, ('class_' + idx));
		  } else {
	    	$(('#matrixData li.ui-selectee div.class_' + idx)).css('display','none');
	    	showOrHideNifTds(false, ('class_' + idx));
		  }
		}
		checkStatusOfCheckbox();
	});

	$totalOn = false;
	var cGlobalType1, cGlobalType2;
    $('input[id^="chkNIF"]').change(function() {
		var id = $(this).attr('id');
		var idx = parseInt(id.substring(6));
		if(idx) {
			if($(this).is(':checked')) {
				$('#matrixData li.ui-selectee div.nif' + idx).css('display','block');
				showOrHideNifTds(true, ('nif' + idx-5));
		    } else {
		    	$('#matrixData li.ui-selectee div.nif' + idx).css('display','none');
		    	showOrHideNifTds(false, ('nif' + idx));
		    }
		}
		checkStatusOfCheckbox();
	});

	$( "#filterInputTips" )
			// don't navigate away from the field on tab when selecting an item
			.on( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).autocomplete( "instance" ).menu.active ) {
					event.preventDefault();
				}
			}).on ( "keyup", function(){
				//Restore the option list if the input text is empty
					if (!$(this).val()){
						tipsShowOption($('#matrixData li.ui-selected'));
					}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
						JSONFilterValues, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: filterTips_selection_fn
			});
    // End
   registerDrop_event();
}

function setStatistic(t){
	//console.log('setStatistic');
		var $elem;
  	var channel2Val, indexChannel2;
  	if($("select[id$='channel2Picklist']").val()){
  		channel2Val = $("select[id$='channel2Picklist']").val();
  	}

  	if(t==1){

  		$elem = $('#cktotal');
  		if(!$elem.hasClass('isTotalOn')) $elem.toggleClass('isTotalOn');
  		$totalOn = true;

  		if(typeof cGlobalType1!=='undefined') change_channelBudget(cGlobalType1, channelGlobal1, null, null);
  		if(typeof cGlobalType2!=='undefined') change_channelBudget(cGlobalType2, channelGlobal2, null, null);

			for(var i=0, max=jsonChannelsList.length; i<max; i++){
  			if((typeof channelGlobal1  != 'undefined' && jsonChannelsList[i].Id != channelGlobal1) || (typeof channelGlobal2  != 'undefined' && jsonChannelsList[i].Id != channelGlobal2)){
  				change_channelBudget(2, jsonChannelsList[i].Id, null, null);
  			}
  		}

  		if(typeof channel2Val !== 'undefined'){
  			change_channelBudget(2, channel2Val, null, null);
  		}

  		if($elem.hasClass('isTotalOn')){
  			$('#ckaverage').removeClass('isAverageOn');
  			$('span[id^="Text_Number_Info_Field_"]').each(function(){
  	  			if($(this).text().substring(0,5) == ' Avg.'){
  		  			var text = $(this).text().substring(5);
  		  			$(this).text(' Total' + text);
  		  		}
  			});
  			$('span[id^="Text_Channel_"]').each(function(){
  	  			if($(this).text().substring(0,5) == ' Avg.'){
  		  			var text = $(this).text().substring(5);
  		  			$(this).text(' Total' + text);
  		  		}
  			});
  		}
  		showTdCheckboxes();

  	}else if(t==2){
  		$elem = $('#ckaverage');
  		if(!$elem.hasClass('isAverageOn')) $elem.toggleClass('isAverageOn');
  		$totalOn = false;

  		if(typeof cGlobalType1!=='undefined') change_channelBudget(cGlobalType1, channelGlobal1, null, null);
  		if(typeof cGlobalType2!=='undefined') change_channelBudget(cGlobalType2, channelGlobal2, null, null);
  		for(var i=0, max=jsonChannelsList.length; i<max; i++){
  			if((typeof channelGlobal1  != 'undefined' && jsonChannelsList[i].Id != channelGlobal1) || (typeof channelGlobal2  != 'undefined' && jsonChannelsList[i].Id != channelGlobal2)){
  				change_channelBudget(2, jsonChannelsList[i].Id, null, null);
  			}
  		}
  		if(typeof channel2Val !== 'undefined'){
  			change_channelBudget(2, channel2Val, null, null);
  		}

  		if($elem.hasClass('isAverageOn')){
  			$('#cktotal').removeClass('isTotalOn');
  			$('span[id^="Text_Number_Info_Field"]').each(function(){
  	  			if($(this).text().substring(0,6) == ' Total'){
  		  			var text = $(this).text().substring(6);
  		  			$(this).text(' Avg.' + text);
  		  		}
  			});
  			$('span[id^="Text_Channel_"]').each(function(){
  	  			if($(this).text().substring(0,6) == ' Total'){
  		  			var text = $(this).text().substring(6);
  		  			$(this).text(' Avg.' + text);
  		  		}
  			});
  		}
  		showTdCheckboxes();
  	}
}
function showTdCheckboxes(){
	$('input[id^="chkAvgQty_"]').each(function() {
		var id = $(this).attr('id');
		var idx = id.substring(10);
		if(idx) {
			if($(this).is(':checked')) {
				$(('#matrixData li.ui-selectee div.class_'+ idx )).css('display','block');
				showOrHideNifTds(true, ('class_' + idx));
		    } else {
		    	$(('#matrixData li.ui-selectee div.class_' + idx)).css('display','none');
		    	showOrHideNifTds(false, ('class_' + idx));
		    }
		}
	});
    $('input[id^="chkNIF"]').each(function() {
		var id = $(this).attr('id');
		var idx = parseInt(id.substring(6));
		if(idx) {
			if($(this).is(':checked')) {
				$('#matrixData li.ui-selectee div.nif' + idx).css('display','block');
				showOrHideNifTds(true, ('nif' + idx-5));
		    } else {
		    	$('#matrixData li.ui-selectee div.nif' + idx).css('display','none');
		    	showOrHideNifTds(false, ('nif' + idx));
		    }
		}
	});
}
/* Begin: Commented by Antonio Ferrero 15-Jul-2015 ---> it is now in the visualforce
 * //Show or Hide legends on matrix
function showOrHideNifTds(isShow, tdCls) {
	if(!tdCls) return;

	if(isShow) {
		$('.kpiTableDiv').find(('td.' + tdCls)).show();
	}
	else {
		$('.kpiTableDiv').find(('td.' + tdCls)).hide();
	}
}

//Enabled or disable legend checkboxes up to 5
function checkStatusOfCheckbox() {
	if(isFullChecked()) {
		// Disable All other checkbox
		$(".lgBdFilter input").not(':checked').attr('disabled', 'disabled');
	}
	else {
		// Active all other checkbox
		$(".lgBdFilter input:disabled").not(':checked').removeAttr('disabled');
	}
}

function isFullChecked() {
	var list_ckb = $(".lgBdFilter input:checked");

	var isFull = false;

	if(list_ckb && list_ckb.length && list_ckb.length > 4) isFull = true;

	return isFull;
}
//End
*/
function e_over_click_chart(e){
	var $this = $(this), this_h = $this.height(), $chart = $('#barChartId'),
		_offset = $this.offset(), _w = $chart.width(), $checkBox = $('#activeChart');

	switch(e.type){
		case 'mouseenter':
				$this.addClass('pgover');
				if(!$chart.hasClass('incheckbox')){
					$chart.css({'opacity': 0.1, 'left': _offset.left, 'top':(_offset.top)})
					.stop().animate({left: (_offset.left-_w), opacity: 1}, 900, function(){
						var $tmp = $(this), tmp_offset = $tmp.addClass('incheckbox').offset();
						if(tmp_offset.left < 10){
							$tmp.animate({left:10}, 900, function(){});
						}
					});
				}
			break;
		case 'click':
			break;
		default:
				$this.removeClass('pgover');
				if(!$checkBox.attr('checked')){
					$chart.stop().animate({left:_offset.left, opacity:0.1}, 900, function(){
						$(this).css({'top':-1000}).removeClass('incheckbox');
					});
				}
			break;
	}
}

function inTipsTotal_avg(_id, $spanBg, $this){
	if($spanBg.hasClass('bg')){
		$spanBg.stop().animate({backgroundColor: '#fff'}, 800, function(){
			$(this).removeClass('bg').addClass('bgf').removeAttr('style');
			$this.removeClass('inbg');
			$(_id).addClass('inbg').find('span:last').removeClass('bgf').addClass('bg');

			selectedCell_sum($('#matrixData li.ui-selected'));
		});
	}else{
		$(_id).removeClass('inbg').find('span:last').removeClass('bg').addClass('bgf');
		$spanBg.stop().animate({backgroundColor: '#cc4444'}, 800, function(){
			$(this).removeClass('bgf').addClass('bg').removeAttr('style');
			$this.addClass('inbg');

			selectedCell_sum($('#matrixData li.ui-selected'));
		});
	}

}

function e_click_tipsTotal_avg(e){
	var $this = $(this), thisId = $this.attr('id'), $spanBg = $this.find('span:last');
	switch(thisId){
		case 'tipsTotalDivId':
			inTipsTotal_avg('#tipsAverageDivId', $spanBg, $this);
			break;
		case 'tipsAverageDivId':
			inTipsTotal_avg('#tipsTotalDivId', $spanBg, $this);
			break;
		default:break;
	}
}
function e_click_channelButton(e){	//tips input[type="button"] 	+  and -
	var $this = $(this),$channelInput = null, selectChannelId = '', isFlg = false, _channelType = '1';
	if($this.hasClass('channel1')){
		$channelInput = $('#channelInput1');
		selectChannelId = $.trim($('#matrixTips-header table thead td span.tipChannel1').attr('id'));

	}else if($this.hasClass('channel2')){
		$channelInput = $('#channelInput2');
		selectChannelId = $.trim($('#matrixTips-header table thead td span.tipChannel2').attr('id'));
		_channelType = '2';
	}
	if(selectChannelId != ''){
		var inputValue = $.trim($channelInput.val()) == '' ? 0 : Globalize.parseInt($.trim($channelInput.val()));

		if($this.hasClass('addCls')){ isFlg = true; }
		else if($this.hasClass('minusCls')){ isFlg = false; }

		if(e.type == 'click'){
			if(isFlg){ inputValue = ++inputValue; }
			else{ if(inputValue > 0){inputValue = --inputValue;} }
			$channelInput.val(inputValue).change();
		}
	}
}

function e_focus_change_channelText(el){	//tips input[type="text"] change event
	//console.log('START e_focus_change_channelText');
	//console.log('el :: ', el);
	var $this;
	if(el)
	$this = $(el);
	else
	$this = $(this);
	var thisVal = $this.data('changeData');
	// Value already set, Go on
	if(thisVal == $this.val())
	return false;

	var ov = 0, validation_qty = /^([1-9]\d{0,16}|0)?$/,
		thisId = $.trim($this.attr('id')),
		tv = Globalize.parseInt($.trim($this.val()));	//focus Event

	//ov = $this.inputFocusValueFormat(validation_qty, e);
	ov = $this.data('changeData');
	if(typeof ov == 'undefined') ov = '';

	//if(el){
	//	thisId = 'channelInput1';
	//}
	//if(e.type == 'change'){

		if($.trim($this.val()) == ''){
			tv = '';
		}else if(!validation_qty.test(tv)){
			$this.val(ov).data('changeData',ov);
			return;
		}

		var channelId = '#matrixTips-header table thead tr td span.', _channelType = '1';

		switch(thisId){
			case 'channelInput1':
				channelId = channelId + 'tipChannel1';
				break;
			case 'channelInput2':
				channelId = channelId + 'tipChannel2';
				_channelType = '2';
				break;
			default:break;
		}

		channelId = $.trim($(channelId).attr('id'));

		if(channelId == ''){
			console.log('No channel id found');
			return;
		}

		change_channelBudget(_channelType, channelId, tv, 'ui-selected',$this.attr('class'));

		$this.val(tv).data('changeData',tv);

	//}
}
function e_click_body(evt){
	var elem = evt.target,elemId,i,j;

	if(elem.nodeName == 'LI') j = 2;
	else j = 7;
	var notInArea = true;
	for(i=0;i<j;i++){
		elemId = $(elem).attr('id');
		if(elemId && (elemId=='matrixData'||elemId=='colors' || elemId == 'colorbody' || elemId == 'colors1' ||
			elemId == 'channelInput1' || elemId == 'channelInput2' || elemId == 'channelAdd1' || elemId == 'channelAdd2' ||
			elemId == 'channelMinus1' || elemId == 'channelMinus2' || elemId == 'matrixTips' || elemId == 'channelDropDivId' ||
			elemId == 'btnDivId' || elemId == 'matrixTotalClsId' || elemId == 'tipsnoClickId' || elemId == 'channelDropDiv'
			 || elemId == 'picklistDiv' || elemId.indexOf('ui-id-') > -1)){
			notInArea = false;
			break;
		}else if(elem.parentNode){
			elem = elem.parentNode;
		}
	}
	if(notInArea){
		var thelocale='other';

		if(/^de/.test(UserContext.locale)){
			thelocale = 'de';
		}

		$("#matrixTabTotal tbody tr[id$='_id']").each(function(){
			$this = $(this);
			var NotSelectclsHideU = $this.find("td .NotSelectclsHideU").val() ? $this.find("td .NotSelectclsHideU").val():0;
			var UnitCosts = $this.find("td input[name='UnitCosts']").val() ? $this.find("td input[name='UnitCosts']").val():0;
			$this.find("td.NotSelectcls").text(FormatNums(0,thelocale,false,0));
			$this.find("td.AvgCustNotSelect").text(FormatNums(0,thelocale,false,1));
		});

		$("#matrixTabTotal tfoot tr").each(function() {
			var totalNotSelect = $.trim($("#matrixTabTotal tfoot tr").find(".totalNotSelect").text());
			totalNotSelect = totalNotSelect == '' ? 0 : Globalize.parseInt(totalNotSelect);

			$this = $(this);
			$this.find("th.NotSelectcls").text(FormatNums(0,thelocale,false,0));
			$this.find("th.totalNotSelect").text(FormatNums(0,thelocale,false,1));
		});

		//TODO: @jescobar-17-05-2017 remove comments $('#colorbody').removeClass('inActive').fadeOut();
		$('#colorbody').removeClass('inActive').fadeOut();
		//clear input filter tips
		$('#filterInputTips').val('');
		$('#matrixData').find('.ui-selected').removeClass('ui-selected');
		filterCombinationNameList_change();
		//$('#cycleDataTable').empty();
        //$('#tableTitle').empty();
	}
}
function registerDrop_event(){	//register all jquery ui table event.
	var sortable_options = {containment : '#channelDropDivId', cancel : '.ui-state-disabled', cursor : 'move'},
		droppable_options = {hoverClass : 'hoverTarget', activeClass : 'dropTarget', cursor : 'move'};

	$("#matrixData" ).selectable({filter:'li', cancel:'.tdhd', stop: select_table_stop_fn});

	var channel_drop = $('#channelDivId ul.dropulCls').sortable({
			opacity: 0.7, revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			stop: channel_stop_fn
    });

    $('#channel1DivContent ul.channelDropulCls, #channel2DivContent ul.channelDropulCls').sortable({
			opacity: 0.7, revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			stop: channelContent_stop_fn
    });

	$("#channel1DivContent, #channel2DivContent").droppable({
        accept:".draggable",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#channel1DivContent, #channel1DivContent",
        drop : droppable_drop_fn
    });
	$("#channelDivId").droppable({
        accept:".channelDraggable",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#channelDivId",
        drop : droppable_drop_fn
    });
    //two channel change
    if(!$("select[id$='channel1Picklist']").val()) $("select[id$='channel1Picklist']").val(selectedChannelId);

    $("select[id$='channel1Picklist']").change();

	matrixTotalTable(false);


    //var dtd = $.Deferred(), d = waitDeferredChannel(dtd, $("select[id$='channel1Picklist']").val());
    //if($('#loading-curtain-div').is(':hidden')) $('#loading-curtain-div').show();
	//$.when(d).done(function(){
	    if($("select[id$='channel2Picklist']").val()){
	    	$("select[id$='channel2Picklist']").change();
	    }
		$('#loading-curtain-div').hide();
	//}).fail(function(){ if(window.console)console.log('fail list_rows'); });


    $('#matrixData li.tdda').unbind('mouseover mouseout').bind('mouseover mouseout', function(e){
	    if(e.type == 'mouseover'){
	        var xx = e.pageX, yy = e.pageY;

			var cls = $(this).attr('class');
				var col = /column_\d+/.exec(cls);
				var row = /row_\d+/.exec(cls);
				col = col[0].replace('column_','');
				row = row[0].replace('row_','');

			var $channel1 = $("select[id$='channel1Picklist']"), $channel2 = $("select[id$='channel2Picklist']");
        	var channel1 = $channel1.get(0), channel2 = $channel2.get(0);
        	var selecValCh1 = $.trim(channel1.value);
        	var	selecTxtCh1 = $.trim(channel1.options[channel1.selectedIndex].text);
        	var	selecValCh2 = '';

        	var	selecTxtCh2 = '';

			if ($channel2 && $channel2.length > 0) {
				selecValCh2 = $.trim(channel2.value);
				selecTxtCh2 = $.trim(channel2.options[channel2.selectedIndex].text);
			}

			var tableStr = '<table border="0" cellpadding="0" cellspacing="0" id="cellPopupTable" class="cellPopupTable" style="background-color:#FFFDCA;"><thead><tr><th style="width:200px;max-width:250px;"></th></tr></thead><tbody>';

			    tableStr += '<tr><td style="width:200px;max-width:250px;">';
			    tableStr += 'Average Quantity';
			    tableStr += '</td></tr>';

			tableStr += '</tbody></table>';


			var $table = $(tableStr);

			var existChannelId = [];
			if(selecValCh1 != '' && selecTxtCh1 != ''){
				var channelName = selecTxtCh1, channelId = selecValCh1;

				existChannelId.push(channelId);

				var thStr = '<th style="width:100px;max-width:200px; text-align:center;">' + channelName + '</th>';
		        $table.find('thead tr th:last').after(thStr);

		        $table.find('tbody tr').each(function(idx){
		            var tdStr = '<td id="' + channelId + '"></td>';
		            $(this).find('td:last').after(tdStr);
		        });
			}

			if(selecValCh2 != '' && selecTxtCh2 != ''){
				var channelName = selecTxtCh2, channelId = selecValCh2;

				existChannelId.push(channelId);

				var thStr = '<th style="width:100px;max-width:200px; text-align:center;">' + channelName + '</th>';
		        $table.find('thead tr th:last').after(thStr);

		        $table.find('tbody tr').each(function(idx){
		            var tdStr = '<td id="' + channelId + '"></td>';
		            $(this).find('td:last').after(tdStr);
		        });
			}

			$("#matrixTabTotal tbody tr[id$='_id']").each(function(){
				var $tr = $(this), trId = $tr.attr('id'), channelId = trId.split('_')[0], channelName = $.trim($tr.find('td:first').text());
				if(channelName && channelName != '' && existChannelId.indexOf(channelId) == -1){
					var thStr = '<th style="width:100px;max-width:200px; text-align:center;">' + channelName + '</th>';
			        $table.find('thead tr th:last').after(thStr);

			        $table.find('tbody tr').each(function(idx){
			            var tdStr = '<td id="' + channelId + '"></td>';
			            $(this).find('td:last').after(tdStr);
			        });
				}

			});

			for(var i=0, max=JSON_CHANNELS.length; i<max; i++){
				var o = JSON_CHANNELS[i];
				if(o.cellRow == row && o.cellColumn == col){
					var cId = o.channelId;

					var totalCust = 0;
					var totalQuan = 0;
					for(var idx in o.map_index_cmcdo){

						if(o.map_index_cmcdo[idx].qty>=0 && o.map_index_cmcdo[idx].accountNum>=0){ // Added >=0 condition Antonio Ferrero 6-Aug-2015
							totalCust += parseFloat(o.map_index_cmcdo[idx].accountNum);
							totalQuan += parseFloat(o.map_index_cmcdo[idx].accountNum) * parseFloat(o.map_index_cmcdo[idx].qty);
						}

					}
					var avgQuan = 0;
					if(totalCust && totalQuan){
						avgQuan = parseFloat(totalQuan/totalCust);
					}

					$table.find("td[id='" + cId + "']").text(avgQuan.toFixed(1));
				}
			}

			//@jescobar: Showing the popUp to get its edge
			$('#cellPopup').html($table).css({'display':''});
			var $pMatrix = $('#matrixData'), cellPopupTable = $('#cellPopupTable');

			//Set border edge to the popUp regardig the matrix edge
			width_mx = $pMatrix.outerWidth(true), left_mx= $pMatrix.offset().left, width_popUp = cellPopupTable.width();
			xx = ((xx+width_popUp) > (width_mx+left_mx)) ? ((width_mx+left_mx) - width_popUp) : (xx-105);

			//show pop up according to the location event
			$table.css({'left': xx, 'top' : (yy-102), 'position':'absolute', 'opacity':0.1}).animate({opacity: 1}, 600, function(){});
	    }
	    else if(e.type == 'mouseout'){
	        $('#cellPopup').css({'display':'none'});
	    }
	});
	//$('#loading-curtain-div').fadeOut();
}

function setSelectedChannelId(){	//first init channel id
	if(selectedChannelId == ''){return;}
	var $channel1 = $('#channel1 li'), $channelId = $(('#channelDivId li#' + selectedChannelId)),
		licost_rate__c = $channelId.attr('licost_rate__c'), liunit__c = $channelId.attr('liunit__c'),
		li_spanText = $.trim($channelId.find('span').text());

	$channel1.attr({'id': selectedChannelId, 'licost_rate__c': licost_rate__c, 'liunit__c': liunit__c})
			 .addClass('pgover').text(li_spanText).removeClass('ui-state-disabled').css({'opacity': 1});
	$channelId.addClass('ui-state-disabled').css({'opacity': 0.4});

	channelPopAssignment('add', '1', null);


	matrixTotalTable(false);
}

/**
*
* @jescobar: Dynamic filter in the show all filter combination popup
*/
function filterTips_selection_fn(event, ui){
		//console.log('## terms: ', this.value);
		var terms = split( this.value );
		var item = ui.item.value;
		var items = [];
		// remove the duplicated item duplicate
		terms.pop();
		//Validate current item in the list
		if($.inArray(item, terms) > -1){
			items = terms;
		}
		else
		{
			terms.push( item );// add the selected item
			items = terms ;
		}

		//Filter out profile filter options
		$('#matrixTips').find('tbody tr').each(function(idx){
		$tr = $(this);
		$td = $tr.find('td.w1');
		var reportUrl = $td.find('a').attr('href');
		var ProfileFilter_text = $td.text();
		var showRow;
		var profileConditions =  ProfileFilter_text.split('&');
		var filterValues = [];
		//Get filter values from profile filter options
		for(var i = 0, max = profileConditions.length; i<max; i++){
			var condition = profileConditions[i];
			var filterValue = condition.substring((condition.indexOf('"')+1),condition.lastIndexOf('"'));
			filterValues.push(filterValue);
		}
		
		//Iterate all item filters and validate if they exist in the profile filter option
		for(var i = 0,  max = items.length; i < max; i++){
				var filterItem = items[i];
				var filterMatch = '"'+filterItem+'"';
				if($.inArray(filterItem, filterValues) > -1 ){
					var reItem = new RegExp(filterMatch,'g');
					ProfileFilter_text = ProfileFilter_text.replace(reItem, '"<em>'+filterItem+'</em>"');
					if(ProfileFilter_text.indexOf("[0]")==-1)
					{
					    showRow = true;
					}
			}else{
				showRow = false;
				break;
			}
		}
		//Show/Hide row option
		if(showRow){
			var filterHTML = (typeof(reportUrl) != 'undefined' && reportUrl != '') ? '<a href="'+reportUrl+'" name="drillDownReportFilter" target="_blank" title="Report">' + ProfileFilter_text +'</a>'
												: ProfileFilter_text;
			$td.html(filterHTML);
			//Show macthed filter
			$tr.show();
		}else{
			//Hide non matched filter
			$tr.hide();
		}
	});

	// add placeholder to get the comma-and-space at the end
	items.push( "" );
	this.value = items.join( ", " );
	return false;
}

/**
*
* @jescobar: Filter tipts input autocomplete
*/
function split( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return split( term ).pop();
}
function channelPopAssignment(executeType, channelType, $channelId){	//add, del set channel tips values
	//console.log('channelPopAssignment');
	switch(executeType){
		case 'add':
			var channelFlg = true, channelId = '',
				$channel1 = $('#channel1DivContent li'),
				$channel2 = $('#channel2DivContent li'),
				channel1Id = $.trim($('#channel1DivContent li').attr('id')),
				channel2Id = $.trim($('#channel2DivContent li').attr('id'));

			if($channelId != null){// oll add channel
				if($channelId.parent().parent().attr('id') == 'channel1DivContent'){
					changeTips('1', $channelId.attr('id'), $channel1);
					channelFlg = true;
				}else{
					changeTips('2', $channelId.attr('id'), $channel2);
					channelFlg = false;
				}
			}else{	//is new channel

				if(typeof(channel1Id) != 'undefined' && channel1Id != ''){
					channelFlg = true;
					changeTips('1', channel1Id, $channel1);
				}
				if(typeof(channel2Id) != 'undefined' && channel2Id != ''){
					changeTips('2', channel2Id, $channel2);
					channelFlg = false;
				}
			}
			if(!($channelId == null && channelType == null)){
				var _channelType = '1';

				if(!channelFlg){
					channelId = $.trim($('#channel2DivContent li').attr('id'));
					_channelType = '2';
					change_channelBudget(_channelType, channelId, null, null);
				}
				else{
					channelId = $.trim($('#channel1DivContent li').attr('id'));
					change_channelBudget(_channelType, channelId, null, null);
				}
			}

			break;
		case 'del':
			var matrixTipsId = '#matrixTips-header .tipChannel'+channelType;
			$(matrixTipsId).each(function(){
				var $this = $(this);
				if($this.hasClass('input')){
					$this.val('');
				}else{
					$this.text(' ').attr({'id':'', 'liUnit__c':''})
				}
			});
			//remove_channelBudget(channelType, $channelId); // Commented by Antonio Ferrero 3-Aug-2015
			break;
		default : break;
	}

}

/* Set matrixTip popUp window with the channel names and ids */
function changeTips(tipChannel, channelId, $channel){	//set tips values
	//console.log('changeTips for ' + tipChannel);
	//console.log('channelId ' + channelId);
	//console.log('channel  ' + $channel);
	var matrixTipsHeader = '#matrixTips-header', t = matrixTipsHeader + ' .tipChannel'+tipChannel,
		$t = $(t), input1 = '', input2 = '';

	$t.each(function(){
		var $this = $(this);
 		if($this.hasClass('head')){
 			$this.text('').text($.trim($channel.text())).attr({'id':channelId});
 		}
 	});

/* Corrado
var matrixTips = '#matrixTips', t = matrixTips + ' .tipChannel'+tipChannel,
	$t = $(t), input1 = '', input2 = '';


	$t.each(function(){
		var $this = $(this);
		if($this.hasClass('head')){
			$this.text('').text($.trim($channel.text())).attr({'id':channelId});
		}else if($this.hasClass('body')){
			$this.text('').text($.trim($channel.attr('liUnit__c')));
		}else if($this.hasClass('input')){
			$this.attr({'liCost_Rate__c' : $.trim($channel.attr('liCost_Rate__c'))});
		}
	});
*/
	if($('#matrixData li.ui-selected').length == 1){
		input1 =  $.trim($('#matrixData li.ui-selected div:first').text());
		input1 = input1 == '' ? '' : Globalize.parseInt(input1);
		input2 = $.trim($('#matrixData li.ui-selected div:last').text());
		input2 = input2 == '' ? '' : Globalize.parseInt(input2);
	}

/*
	$('#channelInputTrId input[type="text"]#channelInput1').val(input1);
	$('#channelInputTrId input[type="text"]#channelInput2').val(input2);
	*/


}
function addQty(evt,el,qty){
	if(evt.preventDefault)
	evt.preventDefault();
	else
	event.returnValue = false;
	if(evt.stopPropagation)
	evt.stopPropagation();
	else
	event.cancelBubble = true;
	var $input,txt;
	if(qty < 0){
		$input = $(el).next();
		txt = $input.val();
		txt = parseInt(txt,10);
		if(isNaN(txt)) txt = 0;
		txt --;
		if(txt < 0) txt = 0;
	}else{
		$input = $(el).prev();
		txt = $input.val();
		txt = parseInt(txt,10);
		if(isNaN(txt)) txt = 0;
		txt ++;
		if(txt < 0) txt = 0;
	}
	//$input.val(txt);
	//modifyed by Peng Zhu 2013-06-19
	$input.val(txt).change();

	//Apply matrix modified
	matrixHasChanged=true;
	//////console.log(':: it has been modified: ' + matrixHasChanged);
}
/*
 * Added by Peng Zhu 2013-06-19 for popup apply button
 */
function applyAllQty(evt,el){
	console.log('applyAllQty');

		$('#loading-curtain-div').show();

		if(evt.preventDefault)
			evt.preventDefault();
		else
			event.returnValue = false;
		if(evt.stopPropagation)
			evt.stopPropagation();
		else
			event.cancelBubble = true;

		//setTimeout(function() {
			var $this = $(el),qty = $this.prev().val(),posIdx = $this.parent().prevAll().length;

			if(!/^\d+$/.test(qty)){
				alert('Invalid number. Please check.');
				$('#loading-curtain-div').hide();
				return;
			}
			var elem;

			var filtersIdx = $('#matrixTips').children('tbody').children().length;
			//console.log('filtersIdx = #' + filtersIdx);
			//console.log('quantity to set = ' + qty);
			//console.log('posIdx or channelId = ' + posIdx);
			if(filtersIdx!=null && filtersIdx == 0){
				alert('Please, apply the filter on the Matrix before assign the resources');
				$('#loading-curtain-div').hide();
				return;
			}

			if(parseInt(qty,10) >= 0 && confirm("Do you want to apply standard values to all filter combinations? This will overwrite all previously assigned values to the selected cells!")){

				updateJSONCHANNELByTipsBulk(qty, posIdx);

				// Copy value into all input elements of the matrixTips (popup window)
				/*
				$('#matrixTips').children('tbody').children().each(function(idx){
					//if(idx > 0){
						//console.log(idx);
						// get the column at index posIdx (1 or 2)
						elem = $(this).children().eq(posIdx).children('input');
						elem.val(qty);

						e_focus_change_channelText(elem.get(0));
					//}
				});
			*/
			} else {
					$('#loading-curtain-div').hide();
			}

			//Apply matrix modified
			matrixHasChanged=true;
			//$('#loading-curtain-div99').hide();
		//}, 500);

}

function updateJSONCHANNELByTipsBulk(qty, posIdx){
	//This is the new quantity; It is the same for every filter (bulk update)
	console.log('qty=' + qty);
	//This is the column or channel to select
	console.log('posIdx=' + posIdx);

	// Iterate for every filter ($('#matrixTips').children('tbody').children() inputs rows)
	//NOTE: posIdx determine the column: 1 as first channel or 2 as second channel
	//		  The filter id o position is passed by the input class value
	var channelId;
	var qtyTypeArr = [];
	qty = Globalize.parseInt($.trim(qty))
	$('#matrixTips').children('tbody').children('tr:visible').each(function(idx){
		var elem = $($(this).children().eq(posIdx).children('input').get(0));

		// Set the value
		elem.val(qty);

		if (channelId == null) {
				var thisId = $.trim(elem.attr('id'));
				if (thisId != null && thisId != '') {
					channelId = getChannelIdByType(thisId);
				}

		}
		// Retrieve the positionId from the class element and push int the array
		qtyTypeArr.push(elem.attr('class'));

	});
	// Update JSON_CHANNELS
	// Get the selected Matrix cells from the Matrix cells class
	var matrixSrc =  '#matrixData li.';
	var matrixChnLayerIndx = posIdx--; // For the first column (channel1) set 0, for the second one 1...

	for(var i = 0, max = JSON_CHANNELS.length; i < max; i++){
		var o = JSON_CHANNELS[i];
		// IF selected channel id == json channel
			// SELECT the json map_index_cmcdo at qtyType position and set the quantity (tips_budget)
		if(o.channelId == channelId && qtyTypeArr.length > 0) {
			var clsName = o.cellRow + '_' + o.cellColumn + '_cls';
			var matrixDataId = matrixSrc + clsName + '.ui-selected';
			//console.log('searched matrixDataId ' + matrixDataId);
			$(matrixDataId).find('div.matrixDataDivCls').eq(posIdx).css({'display':'none'}).animate({}, 0, function() {
				// For every filter
				for (var idx in qtyTypeArr) {
					var qtyType = qtyTypeArr[idx];

					if(o.map_index_cmcdo[''+qtyType]){
						//console.log('Add new quantity=' + qty + ' to qtyType=' + qtyType);
						o.map_index_cmcdo[''+qtyType].qty = parseInt(qty, 10);
					}

				}

			});

		}

	}  // and iterating json elements

	refreshMatrixElementsAndSumTable(channelId, posIdx, qtyTypeArr);

}

/*
 *  Channel Ids are set into the matrixTips popUp window, in the header table.
 *	From the selected input field we can retrieve the inputChannelType and look for the related id.
 */
function getChannelIdByType(inputChannelType) {
	console.log('getChannelIdByType inputChannelType=' + inputChannelType);
	var channelId;
	var channelsSrc = '#matrixTips-header table thead tr td span.';

	switch(inputChannelType){
		case 'channelInput1':
			channelsSrc += 'tipChannel1';
			break;
		case 'channelInput2':
			channelsSrc += 'tipChannel2';
			break;
		default:break;
	}

	channelId = $.trim($(channelsSrc).attr('id'));

	console.log('Retrieved channelId=' + channelId);
	return channelId;
}

function refreshMatrixElementsAndSumTable(channelId, posIdx, qtyTypeArr) {
    	
	var matrixSrc =  '#matrixData li.';
	var precision = ($totalOn) ? 0 : 1;

	for(var i = 0, max = JSON_CHANNELS.length; i < max; i++){
		var o = JSON_CHANNELS[i];

		// For selected ones
		if(o.channelId == channelId && qtyTypeArr.length > 0) {
			var clsName = o.cellRow + '_' + o.cellColumn + '_cls';
			var matrixDataId = matrixSrc + clsName + '.ui-selected';
			//console.log('searched matrixDataId ' + matrixDataId);
			$(matrixDataId).find('div.matrixDataDivCls').eq(posIdx).css({'display':'none'}).animate({}, 0, function() {

				if(filterShowAll){


					var showAllHtml = '<div class="physDiv">' + (o.customers == 'null' ? '0' : roundNumToKM(o.customers,theLocale,9999,0)) + '</div>';
					var showAllHtmlNifs = '';
					if(jsonChannelsList &&  jsonChannelsList.length>0){
							for(var i = 0; i < jsonChannelsList.length; i++){

								if(jsonChannelsList[i].Id.length == 18){
									var cId = jsonChannelsList[i].Id;
									var isChecked = $('#chkAvgQty_'+ cId).is(':checked');
									var nifhidden = 'nifHidden';
									var style = '';
									if(isChecked){
										nifhidden = '';
										style = 'style="display: block;"';
									}
									var initCustomers = 0;
									var idDivChanHtml = '#' + jsonChannelsList[i].Id + '_row_' + o.cellRow + '_col_' + o.cellColumn;
									var units = $(idDivChanHtml).text();
									if(units!=0.0 && units!=0) {
										initCustomers = units;
									}
									showAllHtml += '<div class="class_' + jsonChannelsList[i].Id + ' ' + nifhidden +'" '+ style + 'id="' + jsonChannelsList[i].Id + '_row_' + o.cellRow + '_col_' + o.cellColumn + '">' + roundNumToKM(initCustomers, theLocale, 9999, precision) +'</div>';
								}
							}
					}

					showAllHtmlNifs += '<div class="nif1 nifHidden" >'+ (o.numInfoField1 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField1,theLocale,9999,0) : roundNumToKM(o.numInfoField1/o.customers,theLocale,9999,1)))  + '</div>';
					showAllHtmlNifs += '<div class="nif2 nifHidden" >'+ (o.numInfoField2 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField2,theLocale,9999,0) : roundNumToKM(o.numInfoField2/o.customers,theLocale,9999,1)))  + '</div>';
					showAllHtmlNifs += '<div class="nif3 nifHidden" >'+ (o.numInfoField3 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField3,theLocale,9999,0) : roundNumToKM(o.numInfoField3/o.customers,theLocale,9999,1)))  + '</div>';
					showAllHtmlNifs += '<div class="nif4 nifHidden" >'+ (o.numInfoField4 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField4,theLocale,9999,0) : roundNumToKM(o.numInfoField4/o.customers,theLocale,9999,1)))  + '</div>';
					showAllHtmlNifs += '<div class="nif5 nifHidden" >'+ (o.numInfoField5 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField5,theLocale,9999,0) : roundNumToKM(o.numInfoField5/o.customers,theLocale,9999,1)))  + '</div>';
					showAllHtmlNifs += '<div class="nif6 nifHidden" >'+ (o.numInfoField6 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField6,theLocale,9999,0) : roundNumToKM(o.numInfoField6/o.customers,theLocale,9999,1)))  + '</div>';
					showAllHtmlNifs += '<div class="nif7 nifHidden" >'+ (o.numInfoField7 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField7,theLocale,9999,0) : roundNumToKM(o.numInfoField7/o.customers,theLocale,9999,1)))  + '</div>';
					showAllHtmlNifs += '<div class="nif8 nifHidden" >'+ (o.numInfoField8 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField8,theLocale,9999,0) : roundNumToKM(o.numInfoField8/o.customers,theLocale,9999,1)))  + '</div>';
					showAllHtmlNifs += '<div class="nif9 nifHidden" >'+ (o.numInfoField9 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField9,theLocale,9999,0) : roundNumToKM(o.numInfoField9/o.customers,theLocale,9999,1)))  + '</div>';
					showAllHtmlNifs += '<div class="nif10 nifHidden" >'+ (o.numInfoField10 == 'null' ? roundNumToKM(0, theLocale, 9999, precision) : ($totalOn==true ? roundNumToKM(o.numInfoField10,theLocale,9999,0) : roundNumToKM(o.numInfoField10/o.customers,theLocale,9999,1)))  + '</div>';

					$(this).attr({'id':o.channelId}).hide();
					$(this).attr({'id':o.channelId}).html(showAllHtml + showAllHtmlNifs);

					var totalQty = 0;
					var totalCycleDataCounter = 0;
					for(var i=0; i < filterSize; i++){
						if(o.map_index_cmcdo['' + i] != null && o.map_index_cmcdo['' + i].qty != null && o.map_index_cmcdo['' + i].accountNum != null) {
							totalQty += o.map_index_cmcdo['' + i].qty * o.map_index_cmcdo['' + i].accountNum;
							totalCycleDataCounter += o.map_index_cmcdo['' + i].accountNum;
						}
					}

					var customers = 0;
					if(totalCycleDataCounter > 0){
						if($totalOn){totalCycleDataCounter = 1;}
						customers = parseFloat(totalQty/totalCycleDataCounter).toFixed(precision);
					}
					var idName = '#'+ o.channelId +'_row_' +  o.cellRow + '_col_' + o.cellColumn;
					$(idName).html(roundNumToKM(customers,theLocale,9999,precision));
					$(this).attr({'id':o.channelId}).show();


				} else {

					var showAllHtml = '<div class="physDiv">' + (o.customers == 'null' ? '0' : roundNumToKM(0,theLocale,9999,0)) + '</div>';
					if(jsonChannelsList &&  jsonChannelsList.length>0){
							for(var i = 0; i < jsonChannelsList.length; i++){
								if(jsonChannelsList[i].Id.length == 18){
									var cId = jsonChannelsList[i].Id;
									//console.log('type channel id :: ' + typeof cId);
									var isChecked = $('#chkAvgQty_'+ cId).is(':checked');
									var nifhidden = 'nifHidden';
									var style = '';
									if(isChecked){
										nifhidden = '';
										style = 'style="display: block;"';
									}
									var initCustomers = 0;
									var idDivChanHtml = '#' + jsonChannelsList[i].Id + '_row_' + o.cellRow + '_col_' + o.cellColumn;
									if($(idDivChanHtml).html()!=0.0 && $(idDivChanHtml).html()!=0) initCustomers = parseFloat($(idDivChanHtml).html());

									showAllHtml += '<div class="class_' + jsonChannelsList[i].Id + ' ' + nifhidden +'" '+ style + 'id="' + jsonChannelsList[i].Id + '_row_' + o.cellRow + '_col_' + o.cellColumn + '">' + roundNumToKM(initCustomers,theLocale,9999,0) +'</div>';

								}
							}
					}

					var totalQty = 0;
					var totalCycleDataCounter = 0;
					var custFilter = 0;

					if(o.map_index_cmcdo[filterSelectIndex] != null && o.map_index_cmcdo[filterSelectIndex].qty != null && o.map_index_cmcdo[filterSelectIndex].accountNum != null){
						totalQty += o.map_index_cmcdo[filterSelectIndex].qty * o.map_index_cmcdo[filterSelectIndex].accountNum;
						totalCycleDataCounter += o.map_index_cmcdo[filterSelectIndex].accountNum;
						custFilter = o.map_index_cmcdo[filterSelectIndex].accountNum;

						showAllHtml += '<div class="nif1 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo1,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo1/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif2 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo2,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo2/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif3 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo3,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo3/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif4 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo4,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo4/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif5 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo5,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo5/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif6 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo6,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo6/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif7 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo7,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo7/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif8 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo8,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo8/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif9 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo9,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo9/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif10 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo10,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo10/custFilter,theLocale,9999,1))  + '</div>';
					}

					$(this).attr({'id':o.channelId}).hide();
					$(this).attr({'id':o.channelId}).html(showAllHtml);

					var customers = 0;
					if(totalCycleDataCounter > 0){
						if($totalOn){totalCycleDataCounter = 1;}
						customers = parseFloat(totalQty/totalCycleDataCounter).toFixed(precision);
					}

					var idName = '#'+ o.channelId +'_row_' + o.cellRow + '_col_' + o.cellColumn;
					$(idName).html(roundNumToKM(customers,theLocale,9999,0));
					$(this).attr({'id':o.channelId}).find('.physDiv').text(roundNumToKM(custFilter,theLocale,9999,0));
					$(this).attr({'id':o.channelId}).show();

				}

			}); // End selected
		}
	} // And for

	checkCheckboxLegend(jsonChannelsList);

	showTdCheckboxes();

	var dtd = $.Deferred();
	var d = waitDeferred2(dtd, channelId);

	$.when(d)
		.done(function(){

			matrixTotalTable(true);

			$('#loading-curtain-div').hide();

		}).fail(function(){ if(window.console)console.log('fail list_rows'); });

}
/*MCH HIDE 0 Values 19-12-2017*/
function hide0Values()
{
    $('#matrixTips').children('tbody').children('tr:visible').each(function(idx){
        
        	$tr = $(this);
            $td = $tr.find('td.w1');
            var ProfileFilter_text=$td.text();
            if(ProfileFilter_text.indexOf("[0]")!=-1)
			{
			    $tr.hide();
			}
		
        	
        
    });
}


//TODO: This function is too long and really poorly commented! INVESTIGATE, COMMENT ACCOORDANLY and (please!) divide into smaller functional pieces
function tipsShowOption($lis){
	//console.log('## JSON Channel List: ', allProcess_map);
    
	// TODO: Why is re defining passed paramenters??!! INVESTIGATE AND REMOVE if it is the case
	$lis = $('#matrixData li.ui-selected');
	var displayCls = 'displayCls',
		selected_len = $('#matrixData li.ui-selected').length;

	// Selector of the filters
	var $select = $('#quantityFilterDivId').children('select:first');

	// Array to set popUp filter table rows
	var str = [], $option;


	// First row of the table with the 'applyAllQty' Btn in it
	var headerRow;

	if(filterShowAll){
		headerRow = '<tr><td class="whLabel">Apply to all filter combinations</td><td class="whRow"><input type="text" class="ipt_all" value="" onkeypress="killEnter(event,this);" onkeydown="killEnter(event,this);"/><button onclick="applyAllQty(event,this);">Apply</button></td><td class="whRow"><input type="text" class="ipt_all" value="" onkeypress="killEnter(event,this);" onkeydown="killEnter(event,this);"/><button onclick="applyAllQty(event,this);">Apply</button></td></tr>';
		/*str = ['<tr><td class="w1">Apply to all filter combinations</td><td class="w3"><input type="text" class="ipt_all" value="" onkeypress="killEnter(event,this);" onkeydown="killEnter(event,this);"/>',
			'<button onclick="applyAllQty(event,this);">Apply</button></td><td class="w3"><input type="text" class="ipt_all" value="" onkeypress="killEnter(event,this);" onkeydown="killEnter(event,this);"/><button onclick="applyAllQty(event,this);">Apply</button></td></tr>'];*/
	}

	// TODO: Set a comment to these variables as they are used quite after!
	var initVal;
	var multiVal;
  var multiVal2 = [];
	var channelA = {};
  var total_arr = [];//added by Peng Zhu 2014-02-10
	var selectedMcids = [];


    ////console.log('selected_len: ' + selected_len);
    ////console.log('filterSize: ' + filterSize);

	/*
		Setting map to report filter parameters
	*/
  var reportParamsMapByFields = [];
	for (var i = 1; i <= 25; i++) {
		var fieldName = 'Text_Info_Field_';
		fieldName += (i > 5) ? i + '_BI__c' : i + '__c';

		reportParamsMapByFields[fieldName] = 'pv' + i;
	}
    

	if($lis && $lis.length != 1){
		////console.log('a');
		initVal = '';
		// Matrix cell detail (values for every filter)
		multiVal = [];
		total_arr = [];
		// filterSize from the page javascript
		if(filterSize){
			////console.log('b');
			// Start iterating the selected rows and columns
			$lis.each(function(idx){
				var arrIdx = idx;

				multiVal[arrIdx] = [];

				// Retrieve selected rows and columns looking into class name
				var $this = $(this);
				var cls = $this.attr('class');
				var col = /column_\d+/.exec(cls);
				var row = /row_\d+/.exec(cls);
				col = col[0].replace('column_','');
				row = row[0].replace('row_','');

				// JSON_CHANNELS is the matrix json, loaded at loading the page
				for(var i=0, max=JSON_CHANNELS.length; i<max; i++){
					var o = JSON_CHANNELS[i];
					if(o.cellRow == row && o.cellColumn == col){
						var ca = [];
						if(channelA[o.channelId] && channelA[o.channelId].length > 0) {
							ca = channelA[o.channelId];
						}
						// Add new cell info at the last position only if allocated units is set
						if(o.customers && o.allocatedUnits){
							ca[ca.length] = o;
						}
						channelA[o.channelId] = ca;

					}
				}

				// 'matrixDataDivCls' is the matrix cell
				$this.children('div.matrixDataDivCls').each(function(idx){
					////console.log('c');
					//console.log(JSON_CHANNELS);
					//console.log('this :: ', $(this));
					var kid = $(this).attr('id');
					//console.log('var kid :: ' + kid);
					// TODO: Check if it can be set in the previous JSON_CHANNELS iteraction
					for(var i=0, max=JSON_CHANNELS.length; i<max; i++){
						var o = JSON_CHANNELS[i];
						if(o.channelId == kid && o.cellRow == row && o.cellColumn == col){
							// 'map_index_cmcdo' are the filters
							multiVal[arrIdx][idx] = o.map_index_cmcdo;

							// TODO: Check: Should be set previously set values
							//Begin: added by Peng Zhu 2014-02-11
							if(total_arr[idx] == undefined){
								total_arr[idx] = {};
							}

							for(var oIdx in o.map_index_cmcdo){
								if(total_arr[idx][oIdx] == undefined){
									total_arr[idx][oIdx] = {};
									total_arr[idx][oIdx]['qty'] = o.map_index_cmcdo[oIdx]['qty'];
								}
								else{
									if(total_arr[idx][oIdx]['qty'] != o.map_index_cmcdo[oIdx]['qty']){
										total_arr[idx][oIdx]['qty'] = ''; // TODO: Check WHY it is set to empty value! Only the firs cell should be set?
									}
								}
							}
							//End: added by Peng Zhu 2014-02-11

							// TODO: if the cell is found why we continue iterating all the matrix??!!!
							break;
						}
					}
				}); // END all matrix table iteraction

			}); // END selected cell iteraction
		}

		/**
		console.log('build multival');
		console.log(multiVal);
		console.log('total_arr');
		console.log(total_arr);
		*/

    for(var i = 0; i < multiVal.length; i++){ // Iteration between the selected cells
			for(var j = 0; j < multiVal[i].length; j++){ // iteration between channels
				if(multiVal2[j] == 'undefined' || !multiVal2[j]) multiVal2[j] = [];

				for(var k = 0; k < filterSize; k++){ // number of filter value combination
					if(typeof multiVal2[j][k] == 'undefined' || !typeof multiVal2[j][k]) multiVal2[j][k] = [];
					if(multiVal2[j][k] != null && multiVal[i][j] != null && multiVal[i][j][''+k] != null) { //Changed by leijun 2015-06-01
						if(typeof multiVal2[j][k]['accountCounter'] != 'undefined'){
							multiVal2[j][k]['accountCounter'] = multiVal2[j][k]['accountCounter'] + multiVal[i][j][''+k]['accountCounter'];
						}
						else{
							multiVal2[j][k]['accountCounter'] = multiVal[i][j][''+k]['accountCounter'];
						}

						var parentMcId = multiVal[i][j][''+k]['mcId'].substring(0, 15);
						if (selectedMcids.indexOf(parentMcId) === -1) {
							selectedMcids.push(parentMcId);
						}


						if (multiVal2[j][k]['filterValue'] === undefined) {
							multiVal2[j][k]['filterValue'] = multiVal[i][j][''+k]['filterValue'];

							if (filterMap != null) {
								var filterStr = multiVal[i][j][''+k]['filterValue'];
								if (filterStr != null && filterStr != '') {
									var filterArr = filterStr.split(';');
									var urlParams = '';
									for (var f = 0; f < filterArr.length; f++) {
										urlParams += '&' + reportParamsMapByFields[filterMap[f]] + '=' + filterArr[f];
									}

									multiVal2[j][k]['filterParams'] = urlParams;

  							}
							}

						}
					}
				}
			}
		}
		/**console.log('Total_arr: ' + total_arr);
		console.log('multiVal2: ');
		console.log(multiVal2);
		console.log('filterMap: ');
		console.log(filterMap);*/

		initVal = total_arr;
	}
	else{// if($lis && $lis.length == 1) // This is the case selected cells are different equal 1 TODO: Check why we should separate this case
		////console.log('d');
		initVal = [];
		var cls = $lis.attr('class');
		var col = /column_\d+/.exec(cls);
		var row = /row_\d+/.exec(cls);
		col = col[0].replace('column_','');
		row = row[0].replace('row_','');

		$lis.children('div.matrixDataDivCls').each(function(idx){
			var kid = $(this).attr('id');
			//console.log('this :: ', $(this));
			for(var i=0, max=JSON_CHANNELS.length; i<max; i++){
				var o = JSON_CHANNELS[i];
				if(o.channelId == kid && o.cellRow == row && o.cellColumn == col){
					//console.log('kid :: ', kid);
					//console.log('d1');
					//console.log(o.map_index_cmcdo);
					initVal[idx] = o.map_index_cmcdo;

					var parentMcId = o.mcId.substring(0, 15);
					if (selectedMcids.indexOf(parentMcId) === -1) {
						selectedMcids.push(parentMcId);
					}

					break;
				}
			}
		});

		for(var i=0, max=JSON_CHANNELS.length; i<max; i++){
			var o = JSON_CHANNELS[i];
			if(o.cellRow == row && o.cellColumn == col){
				var ca = [];
				if(channelA[o.channelId] && channelA[o.channelId].length > 0) {
					ca = channelA[o.channelId];
				}
				if(o.customers && o.allocatedUnits){
					ca[ca.length] = o;
				}
				channelA[o.channelId] = ca;

			}
		}
	}

	var thelocale='other';
	// TODO: check what it is doing here; Set comment as it is starting a 'new part' of the function
	if(/^de/.test(UserContext.locale)){
		thelocale = 'de';
	}

	var segmentArr = ['Gain', 'Build', 'Defend', 'Observe', 'Maintain','Blank', 'Threshold'];
	var cha = {};
	for(var key in channelA){
		for(var j =0; j < channelA[key].length; j++) {
			var allcus = channelA[key][j];
			ck = {};
			if(cha[key]){
				ck = cha[key];
			}
			for(var i = 0; i< segmentArr.length; i++ ) {
				if(allcus.channelId == key && allcus.segment == segmentArr[i] ) {
					if(!!!ck[segmentArr[i]]) {
						ck[segmentArr[i]] = 0;
					}
					for(oinde in channelA[key][j]['map_index_cmcdo']) {
						ck[segmentArr[i]] += channelA[key][j]['map_index_cmcdo'][oinde]['accountNum'] * channelA[key][j]['map_index_cmcdo'][oinde]['qty'];
					}
				}
			}
			cha[key] = ck;
		}
	}

	var NotSelectclsHideUT = 0;
	var AvgCustNotSelect = 0;

	// Set total table TODO: Comment what it is going to do; This part should actualize table with the 'totals' with the selection values
	$("#matrixTabTotal tbody tr[id$='_id']").each(function(){
		$this = $(this);
		var Id = $this.attr("id");
		Id = Id.substr(0, Id.length - 3);
		if(!!!cha[Id]) {
			var NotSelectclsHideU = $this.find("td .NotSelectclsHideU").val() ? Globalize.parseInt($this.find("td .NotSelectclsHideU").val()):0;
			var UnitCosts = $this.find("td input[name='UnitCosts']").val() ? $this.find("td input[name='UnitCosts']").val():0;
			$this.find("td.NotSelectcls").text(FormatNums(NotSelectclsHideU ,thelocale,false,0));
			$this.find("td.AvgCustNotSelect").text(FormatNums(NotSelectclsHideU * UnitCosts,thelocale,false,1));
		}
	});

	// TODO: COMMENT what it going to do
	// TODO: This part should be moved into another function
	var AvaBudcls =$.trim($("#matrixTabTotal").find('input[name=totalUnitnum]').val());
	AvaBudcls = AvaBudcls == '' ? 0 : parseInt(AvaBudcls);

	var maptotalu = {
		Plannedcls : 0,
		AllocatedBudgetcls : 0,
		Gaincls : 0,
		totalGain : 0,
		Buildcls : 0,
		totalBuild : 0,
		Defendcls : 0,
		totalDefend : 0,
		Observecls : 0,
		totalObserve : 0,
		Maintaincls : 0,
		totalMaintain : 0,
		Blankcls : 0,
		totalBlank : 0,
		ThresholdBlankcls : 0,
		totalThreshold : 0,
	};

	//TODO: Comment this; Should be the part that fill values of actual total table
	for(var key in cha) {
		var channelId = cha[key];
		var $tr = $("#matrixTabTotal tbody tr[id$='" + key + "_id']");
		var NotSelectclsHideU = $tr.find("td .NotSelectclsHideU").val() ? Globalize.parseInt($tr.find("td .NotSelectclsHideU").val()):0;
		var UnitCosts = $tr.find("td input[name='UnitCosts']").val() ? $tr.find("td input[name='UnitCosts']").val():0;

		var AllocatedU = 0;
		for(var i = 0; i < segmentArr.length; i++) {
			var segment = segmentArr[i];
			if(channelId[segment]) {
				NotSelectclsHideU -= channelId[segment];
				AllocatedU += channelId[segment];

				var unit = $tr.find("." + segment + "cls").text() ?  Globalize.parseInt($tr.find("." + segment + "cls").text()) : 0;
				var AvgCust = $tr.find(".AvgCust" + segment).text() ?  Globalize.parseInt($tr.find(".AvgCust" + segment).text()) : 0;

				$tr.find("." + segment + "cls").text(FormatNums(channelId[segment], thelocale, false, 1));
				$tr.find(".AvgCust" + segment).text(FormatNums(channelId[segment] * UnitCosts, thelocale, false, 1));

				if(!isNaN(maptotalu[segment + "cls"])) {
					maptotalu[segment + "cls"]  += channelId[segment];
				}
				if(!isNaN(maptotalu["total" + segment])) {
					maptotalu["total" + segment]  += channelId[segment] * UnitCosts;
				}

			}else{
				$tr.find("." + segment + "cls").text(FormatNums(0,thelocale,false,1));
				$tr.find(".AvgCust" + segment).text(FormatNums(0,thelocale,false,1));
			}
		}
		NotSelectclsHideUT += parseInt(NotSelectclsHideU, 10);
		AvgCustNotSelect += parseInt(UnitCosts, 10) * NotSelectclsHideU;

		maptotalu["Plannedcls"]  += AllocatedU;
		maptotalu["AllocatedBudgetcls"]  += parseInt(UnitCosts, 10) * Globalize.parseInt(AllocatedU + '');

		$tr.find("td.Plannedcls").text(FormatNums(AllocatedU,thelocale,false,1));
		$tr.find("td.AllocatedBudgetcls").text(FormatNums(parseInt(UnitCosts, 10) * Globalize.parseInt(AllocatedU + ''),thelocale,false,1));


		var Plannedin = 0;
		if(AvaBudcls != 0){
			Plannedin = accMul(AllocatedU / AvaBudcls, 100);
		}
	//console.log(2);
		$tr.find('td.Plannedincls').text(Globalize.format(Plannedin, 'n1')+' %');

		$tr.find("td.NotSelectcls").text(FormatNums(parseInt(NotSelectclsHideU),thelocale,false,0));
		$tr.find("td.AvgCustNotSelect").text(FormatNums(parseInt(UnitCosts, 10) * Globalize.parseInt(NotSelectclsHideU + ''),thelocale,false,1));
	}

	for(var key in maptotalu) {
		$("#matrixTabTotal tfoot tr").find("." + key).text(FormatNums(maptotalu[key],thelocale,false,1));
	}
	//console.log(3);
	var AvaBudclsT =$.trim($("#matrixTabTotal tfoot").find('th input[name=totalUnits]').val());
	AvaBudclsT = AvaBudclsT == '' ? 0 : parseInt(AvaBudclsT);
	var pI = 0;
	if(AvaBudclsT != 0) {
		pI = accMul(maptotalu["Plannedcls"] / AvaBudclsT, 100);
	}
	$("#matrixTabTotal tfoot tr").find(".Plannedincls").text(Globalize.format(pI, 'n1')+' %');

	//$("#matrixTabTotal tfoot tr").find(".NotSelectclsHideU").val(NotSelectclsHideUT);
	//$("#matrixTabTotal tfoot tr").find(".NotSelectclsHideB").val(AvgCustNotSelect);
	$("#matrixTabTotal tfoot tr").find(".NotSelectcls").text(FormatNums(parseInt(NotSelectclsHideUT),thelocale,false,0));
	$("#matrixTabTotal tfoot tr").find(".totalNotSelect").text(FormatNums(AvgCustNotSelect,thelocale,false,1));




	var Plannedcls =$.trim($("#matrixTabTotal tfoot").find('.Plannedcls').text());
	Plannedcls = Plannedcls == '' ? 0 : Globalize.parseInt(Plannedcls);

	var totalNotSelect =$.trim($("#matrixTabTotal tfoot").find('th.NotSelectcls').text());
	totalNotSelect = totalNotSelect == '' ? 0 : Globalize.parseInt(totalNotSelect);

	var OtherMatriceclsU =$.trim($("#matrixTabTotal tfoot").find('th .OtherMatriceclsU').val());
	OtherMatriceclsU = OtherMatriceclsU == '' ? 0 : parseInt(OtherMatriceclsU);

	$("#matrixTabTotal tfoot .AvaBudcls").text(FormatNums(AvaBudclsT - Plannedcls - totalNotSelect - OtherMatriceclsU,thelocale,false,1));//TODO

	//TODO: COMMENT THIS! Here is when we are add all popUp table rows
	if($select.val() == ''){
		//console.log('## All filters...');
		//console.log('## multiVal2: ', multiVal2);
		//Begin: added by Peng Zhu 2013-06-26 for remove [###]
		var temp_optionText = '';
		//End: added by Peng
		// Iterating all the filter members
		$select.children().each(function(){
			$option = $(this);
			if(isNaN(parseInt( $option.val(),10)))  //TODO: What it is?? It is an error?? should be controlled and showed to  user??? Stange behaviour just exit the function!
			return true;

			// Set the row Label (first column value)
			//Begin: added by Peng Zhu 2013-06-26
			temp_optionText = '';
			temp_optionText = $.trim($option.text().substring($option.text().indexOf(']') + 1));
			reportUrl = matrixDrillDownFilterReportUrl;
			if (selectedMcids.length > 0) {
				reportUrl += '?pv0=' + selectedMcids.join(',');
			}

			if($lis.length != 1){
				if(typeof multiVal2 != 'undefined' && typeof multiVal2[0] != 'undefined' && typeof multiVal2[0][0] != 'undefined'){
					//only for f2f case
					var numAcc = multiVal2[0][$option.val()]['accountCounter'];
					temp_optionText = '[' + numAcc + '] ' + temp_optionText;
                    
					if (numAcc > 0 && typeof multiVal2[0][$option.val()]['filterParams'] != 'undefined') {

						reportUrl += '&' + multiVal2[0][$option.val()]['filterParams'];

						temp_optionText = '<a href="' + reportUrl + '" name="drillDownReportFilter" target="_blank" title="Report">' + temp_optionText + '</a>';

					}

				}
			}
			else{
				if(initVal != '' && initVal[0] !=null && initVal[0][$option.val()] != null){
					//only for f2f case
					var numAcc = initVal[0][$option.val()]['accountCounter'];
					temp_optionText = '[' + numAcc + '] ' + temp_optionText;
					var lstPosition=[];
					if (numAcc > 0 && typeof initVal[0][$option.val()]['filterValue'] != 'undefined') {

						if (filterMap != null) {
							var filterStr = initVal[0][$option.val()]['filterValue'];

							if (filterStr != null && filterStr != '') {
								var filterArr = filterStr.split(';');
								var urlParams = '';
								var intParamNumber=1;

								//alert(filterMap);
								for (var f = 0; f < filterArr.length; f++) {
									if(filterArr[f]!='N--A')
									{
										urlParams += '&' + reportParamsMapByFields[filterMap[f]] + '=' + filterArr[f];
										lstPosition[reportParamsMapByFields[filterMap[f]]]='Y';
									}
									else
									{
										urlParams += '&' + reportParamsMapByFields[filterMap[f]] + '=';
										lstPosition[reportParamsMapByFields[filterMap[f]]]='Y';
									}
									//alert(urlParams);
									intParamNumber++;

								}

								if(filterArr.length!=filterMap.length)
								{
									var intNewParams=(filterMap.length-filterArr.length);
									for(var i=1;i<=filterMap.length;i++)
									{
										var intPosition=i;
										var strParam='pv'+i;
										//alert('..'+ lstPosition[strParam]+' '+ strParam+' '+filterMap.length);
										if(lstPosition[strParam]!='Y')
										{
											urlParams+='&pv'+intPosition+'='+'NA&pn'+intPosition+'=ne';
										}
									}

									//alert(urlParams);
								}
								reportUrl +=urlParams;

								temp_optionText = '<a href="' + reportUrl + '" name="drillDownReportFilter" target="_blank" title="Report">' + temp_optionText + '</a>';

							}
						}

					}

				}
			}
			//End: Peng
			// This is the row
			str.push('<tr><td class="w1">',temp_optionText,'</td><td class="w2"><button onclick="addQty(event,this,-1);">-</button><input id="channelInput1" type="text" class="',
			$option.val(),'" value="',initVal==''|| initVal[0] ==null || initVal[0][$option.val()] == null?'':initVal[0][$option.val()]['qty'],'" onchange="e_focus_change_channelText(this);" onkeypress="killEnter(event,this);" onkeydown="killEnter(event,this);"/><button onclick="addQty(event,this,1);">+</button></td><td class="w2"><button onclick="addQty(event,this,-1);">-</button><input id="channelInput2" type="text" class="',
			$option.val(),'" value="',initVal==''?'':initVal.length > 1&& initVal[1][$option.val()] != null? initVal[1][$option.val()]['qty']:'','" onchange="e_focus_change_channelText(this);" onkeypress="killEnter(event,this);" onkeydown="killEnter(event,this);"/><button onclick="addQty(event,this,1);">+</button></td></tr>');
			//Changed by leijun 2015-06-01
		});
		//console.log("str", str);
		// Set all row into the page DOM
		$('#matrixTips').children('tbody').empty().append(str.join(''));
		//console.log('Write popUp table');
		if (headerRow != null) {
			$('#matrixTips-header').children('table').children('tbody').empty().append(headerRow);
		} else {
			$('#matrixTips-header').children('table').children('tbody').empty();
		}


		//$('#matrixTips').children('tfoot').hide();//can be removed
	}else{
		// THIS is the case we have a selected filter
		//console.log('g');
		////console.log($select);
		$select.children().each(function(){
			$option = $(this);
			////console.log($select.val());
			////console.log($option.val());
			if($select.val() == $option.val()){
				//Begin: added by Peng Zhu 2013-06-26
				////console.log('g1');
				temp_optionText = '';
				temp_optionText = $.trim($option.text().substring($option.text().indexOf(']') + 1));
				////console.log($lis.length);
				////console.log(initVal);

				reportUrl = matrixDrillDownFilterReportUrl;
				if (selectedMcids.length > 0) {
					reportUrl += '?pv0=' + selectedMcids.join(',');
				}

				if($lis.length != 1){
					if(typeof multiVal2 != 'undefined' && typeof multiVal2[0] != 'undefined' && typeof multiVal2[0][0] != 'undefined'){

						var numAcc =  multiVal2[0][$option.val()]['accountCounter'];
						temp_optionText = '[' + numAcc + '] ' + temp_optionText;

						if (numAcc > 0 && typeof multiVal2[0][$option.val()]['filterParams'] != 'undefined') {
							reportUrl += '&' + multiVal2[0][$option.val()]['filterParams'];
							temp_optionText = '<a href="' + reportUrl + '" name="drillDownReportFilter" target="_blank" title="Report">' + temp_optionText + '</a>';

						}

					}
				}
				else{
					if(initVal != '' && initVal[0][$option.val()] != null){
						//only for f2f case
						var numAcc = initVal[0][$option.val()]['accountCounter'];
						temp_optionText = '[' + numAcc + '] ' + temp_optionText;
						var lstPosition=[];
						if (numAcc > 0 && typeof initVal[0][$option.val()]['filterValue'] != 'undefined') {

							if (filterMap != null) {
								var filterStr = initVal[0][$option.val()]['filterValue'];

								if (filterStr != null && filterStr != '') {
									var filterArr = filterStr.split(';');
									var urlParams = '';
									var intParamNumber=1;

									for (var f = 0; f < filterArr.length; f++) {
										//urlParams += '&' + reportParamsMapByFields[filterMap[f]] + '=' + filterArr[f];
										if(filterArr[f]!='N--A')
										{
											urlParams += '&' + reportParamsMapByFields[filterMap[f]] + '=' + filterArr[f];
											lstPosition[reportParamsMapByFields[filterMap[f]]]='Y';
										}
										else
										{
											urlParams += '&' + reportParamsMapByFields[filterMap[f]] + '=';
											lstPosition[reportParamsMapByFields[filterMap[f]]]='Y';
										}
										intParamNumber++;
									}

									if(filterArr.length!=filterMap.length)
									{
										var intNewParams=(filterMap.length-filterArr.length);
										for(var i=1;i<=filterMap.length;i++)
										{
											var intPosition=i;
											var strParam='pv'+i;
											if(lstPosition[strParam]!='Y')
											{
												urlParams+='&pv'+intPosition+'='+'NA&pn'+intPosition+'=ne';
											}
										}
									}
									reportUrl += urlParams;

									temp_optionText = '<a href="' + reportUrl + '" name="drillDownReportFilter" target="_blank" title="Report">' + temp_optionText + '</a>';

								}
							}

						}

					}
				}
				//End: Peng
				str.push('<tr><td class="w1">',temp_optionText,'</td><td class="w2"><button onclick="addQty(event,this,-1);">-</button><input id="channelInput1" type="text" class="',
				$option.val(),'" value="',initVal==''|| initVal[0] ==null|| initVal[0][$option.val()] == null?'':initVal[0][$option.val()]['qty'],'" onchange="e_focus_change_channelText(this);" onkeypress="killEnter(event,this);" onkeydown="killEnter(event,this);"/><button onclick="addQty(event,this,1);">+</button></td><td class="w2"><button onclick="addQty(event,this,-1);">-</button><input id="channelInput2" type="text" class="',
				$option.val(),'" value="',initVal==''?'':initVal.length > 1&& initVal[1][$option.val()] != null? initVal[1][$option.val()]['qty']:'','" onchange="e_focus_change_channelText(this);" onkeypress="killEnter(event,this);" onkeydown="killEnter(event,this);"/><button onclick="addQty(event,this,1);">+</button></td></tr>');
				//Changed by leijun 2015-06-01
				return false; // TODO: WHAT it is this?? Investigate and (please) Comment why abrubtly exit the function
			}
		});
		$('#matrixTips').children('tbody').empty().append(str.join(''));
		if (headerRow != null) {
			$('#matrixTips-header').children('table').children('tbody').empty().append(headerRow);
		} else {
			$('#matrixTips-header').children('table').children('tbody').empty();
		}
	}
	//End: added by Fuqi Tan 2013-06-18
	$matrixTips = $('#matrixTips-header');
	$matrixTips.find('table thead td').each(function(idx){
		if(idx > 0){
			var $this = $(this), spanId = $.trim($this.find('span').attr('id'));
			if(!spanId || $.trim(spanId) == ''){ //is empty
				$this.addClass(displayCls);
				subTipsShowOption($matrixTips, idx, false, displayCls);
			}else{
				$this.removeClass(displayCls);
				subTipsShowOption($matrixTips, idx, true, displayCls);
			}
		}
	});

	// TODO: Comment this
	if(selected_len > 0){
		//console.log('h');
		var $colorBody = $('#colorbody');
		if(!$colorBody.hasClass('inActive')){
			var $matrixOl = $('#matrixData'),
				ol_width = $matrixOl.outerWidth(true),
				ol_left = $matrixOl.offset().left,
				li_left = $matrixOl.find('li.ui-selected').last().offset().left,
				remain_width = (ol_width-li_left);

		$colorBody.addClass('inActive').css({'left':li_left, 'display':'', 'opacity': 0.1})
				.animate({left: ol_width+ol_left, opacity: 1}, 600, function(){
				});
		}
	}

	var matrixTipsHeader = $('#matrixTips-header');
	var bodyDiv = $('#matrixTips-body').removeClass('showAll');

	if(filterShowAll && (bodyDiv.height() > 415)){ // body div height
		bodyDiv.removeClass().addClass('showAll');
		matrixTipsHeader.removeClass().addClass('showAll');
	} else {
		bodyDiv.removeClass();
		matrixTipsHeader.removeClass().addClass('notShowAll');
	}

	filterCombinationNameList_change();
}
function subTipsShowOption($table,idx,isRemove,clsname){
	$table = $('#matrixTips');
	$tableHeader = $('#matrixTips-header table');
	if(isRemove){
		$table.children('tbody').children().each(function(){
					$(this).children('td').eq(idx).removeClass(clsname);
		});
		$tableHeader.children('tbody').find('td').eq(idx).removeClass(clsname);

		return;
	}
	$table.children('tbody').children().each(function(){
				$(this).children('td').eq(idx).addClass(clsname);
	});
	$tableHeader.children('tbody').find('td').eq(idx).addClass(clsname);

}
/*
function formateTipChanneInput(){
	$('#channelInputTrId input[type="text"]').each(function(){
		var $this = $(this), val = $.trim($this.val());
		$this.val(formatCurrent(val));
	});
}
*/
function formatCurrent(val){
	return (Globalize.format(roundFn(val, 2), 'n'));
}



function droppable_drop_fn(drag,drop){	//droppable
	var $dragCurrent = $(drop.helper.context),
		currentId = $.trim($dragCurrent.attr('id')),
		parentId = $.trim($dragCurrent.parent().parent().attr('id'));

	switch(parentId){
		case 'channel1DivContent' :
			var $lis = $(drag.target).find('li');
        	$lis.each(function(){
        		if(currentId == $(this).attr('id')){
        			$(this).removeClass('ui-state-disabled').css({'opacity': 1});
        			$(('#channel1DivContent #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
        			channelPopAssignment('del', '1', ('#'+currentId));
        		}
        	});
			break;
		case 'channel2DivContent' :
			var $lis = $(drag.target).find('li');
        	$lis.each(function(){
        		if(currentId == $(this).attr('id')){
        			$(this).removeClass('ui-state-disabled').css({'opacity': 1});
        			$(('#channel2DivContent #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
        			channelPopAssignment('del', '2', ('#'+currentId));
        		}
        	});
			break;
		case 'channelDivId' :
			var $dragTarget = $(drag.target).find('li'), targetId = $dragTarget.attr('id');
			if(typeof(targetId) != 'undefined' && targetId != '' && currentId != targetId){	//cleart old channel
				$(('#channelDivId li#'+targetId)).removeClass('ui-state-disabled').css({'opacity': 1});
			}

			$dragTarget.attr({'id': currentId, 'liUnit__c': $dragCurrent.attr('liUnit__c'), 'liCost_Rate__c': $dragCurrent.attr('liCost_Rate__c')})
				.addClass('pgover').html($dragCurrent.text()).removeClass('ui-state-disabled');
			channelPopAssignment('add', null, $dragTarget);

			break;
		default :  break;
	}
}
function remove_channelBudget(_channelType, channelId){
	//Begin: commented out by Peng Zhu 2013-10-29
	//var matrixIds = '#matrixData li.dataCell', delId = 'div:first';
	//if(_channelType == '2'){
	//	delId = 'div:last';
	//}
	//$(matrixIds).each(function(){
	//	$(this).find(delId).removeAttr('id').text('');
	//});
	//End: commented out by Peng Zhu 2013-10-29

	//Begin: added out by Peng Zhu 2013-10-29
	var matrixIds = '#matrixData li.dataCell', delId = 'div:first', divType = 'div.matrixDataDivCls', eqIndex = 0;
	if(_channelType == '2'){
		//delId = 'div:last';
		eqIndex = 1;
	}
	$(matrixIds).each(function(){
		//$(this).find(delId).removeAttr('id').text('');
		$(this).find(divType).eq(eqIndex).removeAttr('id').text('');
	});
	//End: added out by Peng Zhu 2013-10-29
}

// Added by Antonio Ferrero
function checkCheckboxLegend(jsonChannelsList){
    for(var i = 0; i < jsonChannelsList.length; i++){
		if(jsonChannelsList[i].Id.length == 18){

			var id = $('#chkAvgQty_'+ jsonChannelsList[i].Id).attr('id');
			var idx = id.substring(10);
			if(idx) {
				if($('#chkAvgQty_'+ jsonChannelsList[i].Id).is(':checked')) {
					$(('#matrixData li.ui-selectee div.class_'+ idx )).removeClass('nifHidden');
					$(('#matrixData li.ui-selectee div.class_'+ idx )).css('display','block');
			    } else {
			    	$(('#matrixData li.ui-selectee div.class_'+ idx )).addClass('nifHidden');
			    	$(('#matrixData li.ui-selectee div.class_' + idx)).css('display','none');
			    }
			}
		}
	}
	for(var i = 1; i<=10;i++){
		if($(('#chkNIF'+ i).trim()).is(':checked')) {
			$('#matrixData li.ui-selectee div.nif' + i).removeClass('nifHidden');
			$('#matrixData li.ui-selectee div.nif' + i).css('display','block');
		} else {

	    	$('#matrixData li.ui-selectee div.nif' + i).addClass('nifHidden');
	    	$('#matrixData li.ui-selectee div.nif' + i).css('display','none');
	    }

	}
}

function change_channelBudget(_channelType, channelId, tips_budget, selected_cls, qtyType){

	var list_rows = [], list_column = [];
	var oRow='null', oColumn='null';
	var showAllHtmlNifsAux = '';
	if(_channelType == 1){
		cGlobalType1 = _channelType;
		channelGlobal1 = channelId;
	}

	if(_channelType == 2){
		cGlobalType2 = _channelType;
		channelGlobal2 = channelId;
	}

	for(var i=0, max=JSON_CHANNELS.length; i<max; i++){
		var o = JSON_CHANNELS[i], clsName = '_cls', divType='div:first', matrixDataId = '#matrixData li.',
			row_cls = ('row_' + o.cellRow), column_cls = ('column_'+ o.cellColumn);
		oRow = o.cellRow;
		oColumn = o.cellColumn;
		var eqIndex = 0;

		//console.log(':: JSON: ',  o);

		divType = 'div.matrixDataDivCls';
		if(o.channelId == channelId){
			clsName = o.cellRow + '_' + o.cellColumn + clsName;
			//if(_channelType == '2'){divType = 'div:last';}
			if(_channelType == '2'){eqIndex = 1;}

			matrixDataId = (matrixDataId + clsName);
			if(selected_cls != null && tips_budget != null){
				matrixDataId = matrixDataId + '.'+selected_cls;
			}

			//Corrado remove the animations
			/*$(matrixDataId)
			.animate({opacity: 0.1}, 600)
			.animate({opacity: 1}, 600, function(){
				$(this).removeAttr('style').find('div').removeAttr('style');
			})*/
			$(matrixDataId).find(divType).eq(eqIndex).css({'display':'none'}) // get the divType at eqIndex (0 or 1) depending of the channel
			.animate({}, 0, function(){	//this is change selected values
				if(tips_budget != null){
					//Begin: added by Peng Zhu 2013-06-19
					if(typeof qtyType == 'undefined' || !/^\d/.test(qtyType)){
						qtyType = filterSelectIndex;   //possibly filterSelectIndex is null !!****
					}
					if(o.map_index_cmcdo[''+qtyType]){
						o.map_index_cmcdo[''+qtyType].qty = parseInt(tips_budget, 10);
					}
					//End: added by Peng Zhu 2013-06-19
				}
				//Commented by Peng Zhu 2013-06-19

				//Begin: added by Peng Zhu 2013-06-18 for filter show all
				//show the numbers
				var precision;
				if($totalOn) precision = 0; else precision = 1;

				if(filterShowAll){
					// Begin: Commmented out by Antonio Ferrero 12-Aug-2015
					/*var totalCycleDataCounter = 0;
					for(var i=0; i < filterSize; i++){
						if(o.map_index_cmcdo['' + i] != null && o.map_index_cmcdo['' + i].qty != null && o.map_index_cmcdo['' + i].accountNum != null){ //Changed by leijun 2015-06-01
							totalQty += o.map_index_cmcdo['' + i].qty * o.map_index_cmcdo['' + i].accountNum;
							totalCycleDataCounter += o.map_index_cmcdo['' + i].accountNum;
							//console.log('total quantity :: ' + totalQty);
							//console.log('total cycle data counter :: ' + totalCycleDataCounter );
						}
						else {
							$('.alert').show();
						}
					}
					var customers = 0;
					if(totalCycleDataCounter > 0){
						customers = parseFloat(totalQty/totalCycleDataCounter).toFixed(1);
						//console.log('customers :: ' + customers );
					}
					else customers = 0;*/ //End
					//** here need consider if/how show qty when user change a qty **** //by fuqi
					var showAllHtml = '';
					var showAllHtmlNifs = '';

					//if(_channelType == '1'){ // Commented by Antonio Ferrero

						showAllHtml += '<div class="physDiv">' + (o.customers == 'null' ? '0' : roundNumToKM(o.customers,theLocale,9999,0)) + '</div>';

						if(jsonChannelsList &&  jsonChannelsList.length>0){
					    	for(var i = 0; i < jsonChannelsList.length; i++){

					    		if(jsonChannelsList[i].Id.length == 18){
									var cId = jsonChannelsList[i].Id;
									var isChecked = $('#chkAvgQty_'+ cId).is(':checked');
									var nifhidden = 'nifHidden';
									var style = '';
									if(isChecked){
										nifhidden = '';
										style = 'style="display: block;"';
									}
									var initCustomers = 0;
									var idDivChanHtml = '#' + jsonChannelsList[i].Id + '_row_' + oRow + '_col_' + oColumn;
									var units = $(idDivChanHtml).text();
									//console.log('Units :: ' + units);
									//console.log('div html ::', $(idDivChanHtml));
									if(units!=0.0 && units!=0) initCustomers = units; // ---->>>> attr val div.attr('value')
									showAllHtml += '<div class="class_' + jsonChannelsList[i].Id + ' ' + nifhidden +'" '+ style + 'id="' + jsonChannelsList[i].Id + '_row_' + oRow + '_col_' + oColumn + '">' + roundNumToKM(initCustomers,theLocale,9999,precision) +'</div>';
								}
					    	}
						}

						// Begin: Added by Antonio Ferrero 15-jul-2015
						showAllHtmlNifs += '<div class="nif1 nifHidden" >'+ (o.numInfoField1 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField1,theLocale,9999,0) : roundNumToKM(o.numInfoField1/o.customers,theLocale,9999,1)))  + '</div>';
						showAllHtmlNifs += '<div class="nif2 nifHidden" >'+ (o.numInfoField2 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField2,theLocale,9999,0) : roundNumToKM(o.numInfoField2/o.customers,theLocale,9999,1)))  + '</div>';
						showAllHtmlNifs += '<div class="nif3 nifHidden" >'+ (o.numInfoField3 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField3,theLocale,9999,0) : roundNumToKM(o.numInfoField3/o.customers,theLocale,9999,1)))  + '</div>';
						showAllHtmlNifs += '<div class="nif4 nifHidden" >'+ (o.numInfoField4 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField4,theLocale,9999,0) : roundNumToKM(o.numInfoField4/o.customers,theLocale,9999,1)))  + '</div>';
						showAllHtmlNifs += '<div class="nif5 nifHidden" >'+ (o.numInfoField5 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField5,theLocale,9999,0) : roundNumToKM(o.numInfoField5/o.customers,theLocale,9999,1)))  + '</div>';
						showAllHtmlNifs += '<div class="nif6 nifHidden" >'+ (o.numInfoField6 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField6,theLocale,9999,0) : roundNumToKM(o.numInfoField6/o.customers,theLocale,9999,1)))  + '</div>';
						showAllHtmlNifs += '<div class="nif7 nifHidden" >'+ (o.numInfoField7 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField7,theLocale,9999,0) : roundNumToKM(o.numInfoField7/o.customers,theLocale,9999,1)))  + '</div>';
						showAllHtmlNifs += '<div class="nif8 nifHidden" >'+ (o.numInfoField8 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField8,theLocale,9999,0) : roundNumToKM(o.numInfoField8/o.customers,theLocale,9999,1)))  + '</div>';
						showAllHtmlNifs += '<div class="nif9 nifHidden" >'+ (o.numInfoField9 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField9,theLocale,9999,0) : roundNumToKM(o.numInfoField9/o.customers,theLocale,9999,1)))  + '</div>';
						showAllHtmlNifs += '<div class="nif10 nifHidden" >'+ (o.numInfoField10 == 'null' ? roundNumToKM(0,theLocale,9999,precision) : ($totalOn==true ? roundNumToKM(o.numInfoField10,theLocale,9999,0) : roundNumToKM(o.numInfoField10/o.customers,theLocale,9999,1)))  + '</div>';
						// End
					//}
					$(this).attr({'id':o.channelId}).hide();
					$(this).attr({'id':o.channelId}).html(showAllHtml + showAllHtmlNifs);
					var totalQty = 0;
					var totalCycleDataCounter = 0;
					for(var i=0; i < filterSize; i++){

						if(o.map_index_cmcdo['' + i] != null && o.map_index_cmcdo['' + i].qty != null && o.map_index_cmcdo['' + i].accountNum != null){ //Changed by leijun 2015-06-01
							totalQty += o.map_index_cmcdo['' + i].qty * o.map_index_cmcdo['' + i].accountNum;
							totalCycleDataCounter += o.map_index_cmcdo['' + i].accountNum;
						}
						else {
							$('.alert').show();
						}
					}

					var customers = 0;
					if(totalCycleDataCounter > 0){
						if($totalOn){totalCycleDataCounter = 1;}
						customers = parseFloat(totalQty/totalCycleDataCounter).toFixed(precision);
					}
					else customers = 0;
					var idName = '#'+ o.channelId +'_row_' + oRow + '_col_' + oColumn;
					$(idName).html(roundNumToKM(customers,theLocale,9999,precision));
					$(this).attr({'id':o.channelId}).show();

				}else{
					var showAllHtml = '';

					showAllHtml += '<div class="physDiv">' + (o.customers == 'null' ? '0' : roundNumToKM(0,theLocale,9999,0)) + '</div>';

					if(jsonChannelsList &&  jsonChannelsList.length>0){
				    	for(var i = 0; i < jsonChannelsList.length; i++){
				    		if(jsonChannelsList[i].Id.length == 18){
								var cId = jsonChannelsList[i].Id;
								//console.log('type channel id :: ' + typeof cId);
								var isChecked = $('#chkAvgQty_'+ cId).is(':checked');
								var nifhidden = 'nifHidden';
								var style = '';
								if(isChecked){
									nifhidden = '';
									style = 'style="display: block;"';
								}
								var initCustomers = 0;
								var idDivChanHtml = '#' + jsonChannelsList[i].Id + '_row_' + oRow + '_col_' + oColumn;
								if($(idDivChanHtml).html()!=0.0 && $(idDivChanHtml).html()!=0) initCustomers = parseFloat($(idDivChanHtml).html());
								//$(this).attr({'id':o.channelId}).text( (o.map_index_cmcdo[filterSelectIndex].qty == null ? '0' : o.map_index_cmcdo[filterSelectIndex].qty) );
								showAllHtml += '<div class="class_' + jsonChannelsList[i].Id + ' ' + nifhidden +'" '+ style + 'id="' + jsonChannelsList[i].Id + '_row_' + oRow + '_col_' + oColumn + '">' + roundNumToKM(initCustomers,theLocale,9999,0) +'</div>';

							}
				    	}
					}

					var totalQty = 0;
					var totalCycleDataCounter = 0;
					var custFilter = 0;
                    
					if(o.map_index_cmcdo[filterSelectIndex] != null && o.map_index_cmcdo[filterSelectIndex].qty != null && o.map_index_cmcdo[filterSelectIndex].accountNum != null){ //Changed by leijun 2015-06-01
						totalQty += o.map_index_cmcdo[filterSelectIndex].qty * o.map_index_cmcdo[filterSelectIndex].accountNum;
						totalCycleDataCounter += o.map_index_cmcdo[filterSelectIndex].accountNum;
						custFilter = o.map_index_cmcdo[filterSelectIndex].accountNum;
						//console.log(' :: Number 1', o.map_index_cmcdo[filterSelectIndex].numberInfo1);

						// Begin: Added by jescobar 19-Nov-2015
						//showAllHtml += '<div class="nif1 nifHidden" >'+  ($totalOn==true ? roundNumToKM(o.numberInfo1,theLocale,9999,0) : roundNumToKM(o.numberInfo1/custFilter,theLocale,9999,1)))  + '</div>';
						showAllHtml += '<div class="nif1 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo1,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo1/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif2 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo2,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo2/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif3 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo3,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo3/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif4 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo4,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo4/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif5 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo5,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo5/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif6 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo6,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo6/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif7 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo7,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo7/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif8 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo8,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo8/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif9 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo9,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo9/custFilter,theLocale,9999,1))  + '</div>';
						showAllHtml += '<div class="nif10 nifHidden" >'+ ($totalOn==true ? roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo10,theLocale,9999,0) : roundNumToKM(o.map_index_cmcdo[filterSelectIndex].numberInfo10/custFilter,theLocale,9999,1))  + '</div>';
					}
					else {
						$('.alert').show();
					}

					$(this).attr({'id':o.channelId}).hide();
					$(this).attr({'id':o.channelId}).html(showAllHtml);

					var customers = 0;
					if(totalCycleDataCounter > 0){
						if($totalOn){totalCycleDataCounter = 1;}
						customers = parseFloat(totalQty/totalCycleDataCounter).toFixed(precision);
					}
					else customers = 0;

					var idName = '#'+ o.channelId +'_row_' + oRow + '_col_' + oColumn;
					$(idName).html(roundNumToKM(customers,theLocale,9999,0));
					$(this).attr({'id':o.channelId}).find('.physDiv').text(roundNumToKM(custFilter,theLocale,9999,0));
					$(this).attr({'id':o.channelId}).show();
                    //console.log('_Row: ', oRow, ' _Column: ', oColumn, ' Customers: ', custFilter );
				}
			});
		}
	}

	checkCheckboxLegend(jsonChannelsList);

	showTdCheckboxes();

	var dtd = $.Deferred(),  d = waitDeferred2(dtd, channelId);

	$.when(d)
	.done(function(){

		matrixTotalTable(true);

		$('#loading-curtain-div').hide();

	}).fail(function(){ if(window.console)console.log('fail list_rows'); });
}



function BudgetChannelTotal(channelId){	//Budget Overview by Channel Table total
	var matrixDataId = ('#matrixData #'+channelId),
		PlannedTotal = 0, GainToal = 0, BuildTotal = 0, DefendTotal = 0, ObserveTotal = 0, MaintainTotal = 0; BlankTotal=0, ThresholdTotal=0;
	var totalAllocatedBudget = 0;
	var thelocale='other';

	if(/^de/.test(UserContext.locale)){
		thelocale = 'de';
	}
	$(matrixDataId).each(function(){
		var $this = $(this), t=1, $parent = $this.parent(), v = Globalize.parseInt($.trim($this.text()));
		if(filterShowAll){
			v = Globalize.parseFloat($.trim($this.find('.channelBudgetDiv').text()));
		}

		//Begin: added by Peng Zhu <peng.zhu@itbconsult.com> 2013-06-19
		// -- when calculate "Allocated Units", we need Total_Customers__c
		var tc = $parent.attr('phys');
		if(tc){
			tc = $.trim(tc) != '' ? Globalize.parseInt(tc) : 0;
		}
		else{
			tc = 0;
		}
		//////console.log('tc : ' + tc);
		//////console.log('v : ' + v);
		//End: added by Peng Zhu <peng.zhu@itbconsult.com> 2013-06-19

		//Modified all the value from v to c*tc
		if($parent.hasClass('Gain')){
			GainToal = accAdd(v*tc, GainToal);
		}else if($parent.hasClass('Build')){
			BuildTotal = accAdd(v*tc, BuildTotal);
		}else if($parent.hasClass('Defend')){
			DefendTotal = accAdd(v*tc, DefendTotal);
		}else if($parent.hasClass('Observe')){
			ObserveTotal = accAdd(v*tc, ObserveTotal);
		}else if($parent.hasClass('Maintain')){
			MaintainTotal = accAdd(v*tc, MaintainTotal);
		}else if($parent.hasClass('Blank')){
			BlankTotal = accAdd(v*tc, BlankTotal);
		}else if($parent.hasClass('Threshold')){
			ThresholdTotal = accAdd(v*tc, ThresholdTotal);
		}else if($parent.hasClass(''))

		PlannedTotal = accAdd(v*tc, PlannedTotal);
	});

	var channelTr = '#matrixTabTotal tbody tr#' + channelId + '_id', $channelTotal = $(channelTr), Plannedin = 0;

	channelTotal = $.trim($channelTotal.find('td input[name=totalUnits]').val());
	channelTotal = channelTotal == '' ? 0 : parseInt(channelTotal, 10);

	$channelTotal.find('td.Plannedcls').text(FormatNums(PlannedTotal,thelocale,false,1));

	var UnitCosts = $channelTotal.find("input[name = UnitCosts]").val() ? parseFloat($channelTotal.find("input[name = UnitCosts]").val()) : 0;
	var allocatedBudget = PlannedTotal * UnitCosts;
	$channelTotal.find('td.AllocatedBudgetcls').text(FormatNums(allocatedBudget,thelocale,false,1));

	if(channelTotal != 0){
		Plannedin = accMul((allocatedBudget / channelTotal), 100);
	}
	//console.log(4);
	$channelTotal.find('td.Plannedincls').text(Globalize.format(Plannedin, 'n1')+' %');
	$channelTotal.find('td.Gaincls').text(FormatNums(GainToal,thelocale,false,1));
	$channelTotal.find('td.Buildcls').text(FormatNums(BuildTotal,thelocale,false,1));
	$channelTotal.find('td.Defendcls').text(FormatNums(DefendTotal,thelocale,false,1));
	$channelTotal.find('td.Observecls').text(FormatNums(ObserveTotal,thelocale,false,1));
	$channelTotal.find('td.Maintaincls').text(FormatNums(MaintainTotal,thelocale,false,1));
	$channelTotal.find('td.Blankcls').text(FormatNums(BlankTotal,thelocale,false,1));
	$channelTotal.find('td.Thresholdcls').text(FormatNums(BlankTotal,thelocale,false,1));

	//Begin: added by Peng Zhu 2013-06-28 for remaining field
	var remaingTotal = $channelTotal.find('td input[name=remainingUnits]').val();
	if(typeof remaingTotal != 'undefined' && $.trim(remaingTotal) != '') remaingTotal = Globalize.parseInt(remaingTotal);
	else remaingTotal = 0;

	remaingTotal = remaingTotal- PlannedTotal;
	if(remaingTotal < 0) remaingTotal = 0;
	$channelTotal.find('td.Remainingcls').text(formatCurrentNumber(remaingTotal));
	//End: added by Peng Zhu 2013-06-28 for remaining field
}

function formatCurrentNumber(nums){
	return (Globalize.format(nums, 'n0'));
}
function matrixTotalTable(isFist){
	//console.log('matrixTotalTable isFist=' + isFist);

		var thelocale='other';

	if(/^de/.test(UserContext.locale)){
		thelocale = 'de';
	}
	if(!isFist){
		$('#matrixTabTotal .dataCelltd').each(function(indx){

			var $this = $(this), this_val = $.trim($this.text());

			//Begin: added by Peng Zhu 2013-06-28 for format number -- avoid 350.000 --> 350
			this_val =  Globalize.parseInt(this_val);
			//End: added by Peng Zhu 2013-06-28

			this_val = $.isNumeric(this_val) ? parseFloat(this_val) : 0;
			if($this.hasClass('Plannedincls')){
				this_val = Globalize.format(this_val, 'n1')+'%';
			}else{
				this_val = formatCurrentNumber(this_val);
			}
			$this.text(this_val);

		});

		barChart();

		return;
	}

	var list_cls = [], _cMap = new customMap();

//	_cMap.put('Plannedincls', 0);
	_cMap.put('Totalcls', 0); _cMap.put('Plannedcls', 0); _cMap.put('Gaincls', 0);
	_cMap.put('Buildcls', 0); _cMap.put('Defendcls', 0); _cMap.put('Observecls', 0);
	_cMap.put('Maintaincls', 0); _cMap.put('Blankcls', 0); _cMap.put('Thresholdcls', 0); _cMap.put('NotSelectclsHideU', 0);

	_cMap.put('Remainingcls', 0);

	list_cls = _cMap.keys();

	//calculate the total value for budget overview table
	for(var i=0; i<list_cls.length; i++){
		var clsName = list_cls[i], $find_th = $(('#matrixTabTotal tbody td.' + clsName)), th_v = _cMap.get(clsName);
		if(clsName == 'NotSelectclsHideU'){
			$find_th = $(('#matrixTabTotal tbody td .' + clsName));
		}
		$find_th.each(function(){
			var $this = $(this),$thistext = $this.text();
			if(clsName == 'NotSelectclsHideU') {
				$thistext = $this.val();
			}
			var this_v = Globalize.parseInt($.trim($thistext) == '' ? 0 : $.trim($thistext));
			th_v += this_v;
		});
		_cMap.remove(clsName);
		_cMap.put(clsName, th_v);
	}

	//populate the total value for budget overview table
	for(var i=0; i<list_cls.length; i++){
		var clsName = list_cls[i], find_th = ('#matrixTabTotal tfoot tr th.' + clsName), th_v = _cMap.get(clsName);
		if(clsName == 'NotSelectclsHideU'){
			find_th = ('#matrixTabTotal tfoot tr th .' + clsName);
			$(find_th).val(FormatNums(parseInt(th_v),thelocale,false,1));
		}else{
			$(find_th).text(FormatNums(parseInt(th_v),thelocale,false,1));
		}
	}
	list_cls = [];


	//Calculate the total "Planned in %"
	var totals = _cMap.get('Totalcls'), planned = _cMap.get('Plannedcls'), plannedIn = 0;
	//Why add this statement?
	//totals = parseInt($('#matrixTabTotal tfoot tr th input[name="totalUnits"]').val());

	//added by Peng Zhu 2013-05-27 for avoiding Infinity result when totals is zero
	if(totals && totals != 0 && planned != 0){
		plannedIn = accMul( (planned/totals), 100 );
	}

	//$('#matrixTabTotal tfoot tr th.Plannedincls').text(Globalize.format(plannedIn, 'n1')+' %');
	_cMap.clear();
	barChart();
}


function waitDeferred(dtd, list_cell, _channelType){
	var tasks = function(){
		cell_sum(list_cell, _channelType);
		dtd.resolve();
	};
	setTimeout(tasks,800);
	return dtd.promise();
}
function waitDeferred2(dtd, channelId){
	var tasks = function(){
		//BudgetChannelTotal(channelId);
		filterCombinationNameList_change($("select[id$='theFilterCombinationNameList']").val());

		dtd.resolve();
	};
	setTimeout(tasks,800);
	return dtd.promise();
}

function waitDeferredTipsTotal(dtd, $matrixData_selected){
	var tasks = function(){
		selectedCell_sum($matrixData_selected);
		dtd.resolve();
	};
	setTimeout(tasks,500);
	return dtd.promise();
}


function selectedCell_sum($matrixData_selected){
	var lens = $matrixData_selected.length, ProductRx=0, MarketRx=0, phys=0, marketTotal=0,
		$physId = $('#physId'), $marketRXId = $('#marketRXId'), $productRxId = $('#productRxId'), $marketShareId = $('#marketShareId'),
		$tipsAverageDivId = $('#tipsAverageDivId');

	//Begin: added by Peng Zhu
	var reportUrl = '';
	//End: added by Peng Zhu

	if(lens == 1){
		//Begin: added by Peng Zhu
		reportUrl = matrixDrillDownReportUrl;
		if($matrixData_selected.attr('id') && $.trim($matrixData_selected.attr('id')) != ''){
			//reportUrl += '?pv0=' + $.trim($matrixData_selected.attr('id')).substring(0, 15);  // commented out by Peng Zhu 2015-03-16 for report url parameter

			// Begin: added by Peng Zhu 2015-03-16 for report url parameter
			reportUrl += '?' + reportPVofMatrixCell + '=' + $.trim($matrixData_selected.attr('id')).substring(0, 15);
			// End: added by Peng Zhu 2015-03-16 for report url parameter
		}

		$("a[name='drillDownReport']").attr('href', reportUrl);
		//End: added by Peng Zhu
		phys = $.trim($matrixData_selected.attr('phys'));
		phys = isUndefinedTag(phys) && phys != '' ? parseInt(phys) : 0;
		$physId.text(Globalize.format(phys, 'n0'));

		ProductRx = $.trim($matrixData_selected.attr('ProductRx'));
		ProductRx = isUndefinedTag(ProductRx) && ProductRx != '' ? parseInt(ProductRx) : 0;


		MarketRx = $.trim($matrixData_selected.attr('MarketRx'));
		MarketRx = isUndefinedTag(MarketRx) && MarketRx != '' ? parseInt(MarketRx) : 0;

		if($tipsAverageDivId.hasClass('inbg') && phys != 0){	//avg
			MarketRx = MarketRx/phys;
			ProductRx = ProductRx/phys;
		}

		$productRxId.text(Globalize.format(ProductRx, 'n0'));
		$marketRXId.text(Globalize.format(MarketRx, 'n0'));

		marketTotal = $.trim($matrixData_selected.attr('TotalMarketShare'));
		marketTotal = isUndefinedTag(marketTotal) && marketTotal != '' ? marketTotal : 0;

		$marketShareId.text(marketTotal + '%');
		return;
	}

	$matrixData_selected.each(function(){

		var $this = $(this);

		//Begin: added by Peng Zhu 2013-06-20 for report
		if($this.attr('id') && $.trim($this.attr('id')) != ''){
			reportUrl += $.trim($this.attr('id')).substring(0, 15) + ',';
		}
		//End: added by Peng Zhu 2013-06-20 for report

		productRxTmp = $.trim($this.attr('ProductRx')),
		marketRxTmp = $.trim($this.attr('MarketRx')),
		physTmp = $.trim($this.attr('phys'));

		productRxTmp = isUndefinedTag(productRxTmp) && productRxTmp != '' ? parseInt(productRxTmp) : 0;
		ProductRx = accAdd(productRxTmp, ProductRx);

		marketRxTmp = isUndefinedTag(marketRxTmp) && marketRxTmp != '' ? parseInt(marketRxTmp) : 0;
		MarketRx = accAdd(marketRxTmp, MarketRx);

		productRxTmp = isUndefinedTag(physTmp) && physTmp != '' ? parseInt(physTmp) : 0;
		phys = accAdd(physTmp, phys);
	});
	//Begin: added by Peng Zhu 2013-06-20 for report
	if(reportUrl != null && $.trim(reportUrl) != ''){
		reportUrl = $.trim(reportUrl);
		reportUrl = reportUrl.substring(0, reportUrl.length - 1);
    	//reportUrl = matrixDrillDownReportUrl + '?pv0=' + reportUrl; // commented out by Peng Zhu 2015-03-16 for report url parameter

    	// Begin: added by Peng Zhu 2015-03-16 for report url parameter
    	reportUrl = matrixDrillDownReportUrl + '?' + reportPVofMatrixCell + '=' + reportUrl;
    	// End: added by Peng Zhu 2015-03-16 for report url parameter
	}
	else{
		reportUrl = matrixDrillDownReportUrl;
	}
	$("a[name='drillDownReport']").attr('href', reportUrl);
	//End: added by Peng Zhu 2013-06-20 for report

	$physId.text(Globalize.format(phys, 'n0'));


	if($tipsAverageDivId.hasClass('inbg')  && phys != 0){	//avg
		MarketRx = MarketRx/phys;
		ProductRx = ProductRx/phys;
	}
	$productRxId.text(Globalize.format(ProductRx, 'n0'));
	$marketRXId.text(Globalize.format(MarketRx, 'n0'));
	if(ProductRx != 0){
		marketTotal = roundFn(accMul(ProductRx/MarketRx, 100), 2);
	}
	$marketShareId.text(marketTotal + '%');

}
function cell_sum(list_cell, _channelType){
	list_cell = $.unique(list_cell);
	for(var i=0, max=list_cell.length; i<max; i++){
		var cell = list_cell[i],
			cellcls = ('#matrixData li.' + cell),
			celltotal = ('#matrixData li.' + cell + '_total'),
			total_1 = 0, total_2 = 0;
		$(cellcls).each(function(){
			var $this = $(this),
				channel1 = $.trim($this.find('div:first').text()),
				channel2 = $.trim($this.find('div:last').text());
			total_1 = accAdd(channel1, total_1);
			total_2 = accAdd(channel2, total_2);
		});
		switch(_channelType){
			case '1' : $(celltotal).find('div:first').text(total_1); break;
			case '2' : $(celltotal).find('div:last').text(total_2); break;
			default :break;
		}


	}
	list_cell = [];

}

function barChart(){
	//************************-= Begin: added by Peng Zhu 2013-11-06 for multiple channel barchart =-**********************************
	$('#barChartId').html('');

	var list_ch2bdgt = [], list_chName = [], labee_config = labelConfig();
	//#matrixTabTotal tbody tr
	$("#matrixTabTotal tbody tr[id$='_id']").each(function(idx){
	    var $this = $(this), gain_v = 0, build_v = 0, defend_v = 0, observe_v = 0, maintain_v = 0, total_v = 0;
	    var gain_p = 0, build_p = 0, defend_p = 0, observe_p = 0, maintain_p = 0;

	 	list_chName[idx] = $.trim($this.find("td:first").text());

	    var $gain_td = $this.find("td.Gaincls"),
	        $build_td = $this.find("td.Buildcls"),
	        $defend_td = $this.find("td.Defendcls"),
	        $observe_td = $this.find("td.Observecls"),
	        $maintain_td = $this.find("td.Maintaincls");

	    if($gain_td){
	        gain_v = $.trim($gain_td.text());
	        gain_v = gain_v != '' ? Globalize.parseInt(gain_v) : 0;
	    }

	    if($build_td){
	        build_v = $.trim($build_td.text());
	        build_v = build_v != '' ? Globalize.parseInt(build_v) : 0;
	    }

	    if($defend_td){
	        defend_v = $.trim($defend_td.text());
	        defend_v = defend_v != '' ? Globalize.parseInt(defend_v) : 0;
	    }

	    if($observe_td){
	        observe_v = $.trim($observe_td.text());
	        observe_v = observe_v != '' ? Globalize.parseInt(observe_v) : 0;
	    }

	    if($maintain_td){
	        maintain_v = $.trim($maintain_td.text());
	        maintain_v = maintain_v != '' ? Globalize.parseInt(maintain_v) : 0;
	    }

	    total_v = gain_v + build_v + defend_v + observe_v + maintain_v;

	    if(total_v > 0){
	        gain_p = (Math.round((gain_v/total_v*1000)))/10;
	        build_p = (Math.round((build_v/total_v*1000)))/10;
	        defend_p = (Math.round((defend_v/total_v*1000)))/10;
	        observe_p = (Math.round((observe_v/total_v*1000)))/10;
	        maintain_p = (Math.round((maintain_v/total_v*1000)))/10;
	    }

	    list_ch2bdgt[idx] = [gain_p, build_p, defend_p, observe_p, maintain_p];

	    var error_p = Math.round((gain_p + build_p + defend_p + observe_p + maintain_p - 100)*100)/100;

	    if(error_p){
	        var max_idx = 0, max_v = 0;
	        for(var i = 0; i < list_ch2bdgt[idx].length; i++){
	            if(list_ch2bdgt[idx][i] > max_v){
	                max_v = list_ch2bdgt[idx][i];
	                max_idx = i;
	            }
	        }
	        if(max_v){
	            max_v = max_v - error_p;
	            list_ch2bdgt[idx][max_idx] = max_v;
	        }
	    }
	});

    var ticks_bdgt = [
    	labee_config.a, labee_config.b, labee_config.c, labee_config.d, labee_config.e
	],

 	series_bdgt = [];

	for(var i = 0; i < list_chName.length; i++){
		series_bdgt.push({label: list_chName[i]});
	}

	try{
 		var plot1 = $.jqplot('barChartId', list_ch2bdgt, {

 			seriesDefaults:{
 				renderer:$.jqplot.BarRenderer,
 				rendererOptions: {
 					barWidth: 10,
 					barPadding: 3,
 					fillToZero: true
 				},
 				pointLabels: { show: true }
 			},
 			animate: true,
 			cursor: { show: false, showTooltip:false },
 			series: series_bdgt,
	        legend: {
	            show: true,
	            placement: 'outsideGrid'
	        },
 			axes: {
 				xaxis: {
 					renderer: $.jqplot.CategoryAxisRenderer,
 					ticks: ticks_bdgt,
 					tickRenderer:$.jqplot.CanvasAxisTickRenderer,
 					tickOptions: {
 					}
 				},
 				yaxis: {
 					max:100,
 					padMin: 0,
 					tickOptions:{
 						formatString: '%d'
 					}
 				},
 				showMinorTicks:true
 			},
 			highlighter: {
 				showLabel: true,
 				showTooltip:false,
 				tooltipAxes: 'xy',
 				useAxesFormatters:true,
 				sizeAdjust: 0 ,
 				tooltipLocation : 'ne'
 			}
 		});
 	}catch(e){

 	}
	//************************-= End: added by Peng Zhu 2013-11-06 for multiple channel barchart =-**********************************
}

function channelContent_stop_fn(event, ui){	//sortable   channel goto channelContent
	tipsShowOption();
//	$(ui.item).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
}

function channel_stop_fn(event, ui){	//sortable   channelContent goto channel

	var $item = $(ui.item), itemId = $item.attr('id'),
		$channelItem1 = $('#channel1DivContent li'),
		$channelItem2 = $('#channel2DivContent li'),
		channelItemId1 = $.trim($channelItem1.attr('id')),
		channelItemId2 = $.trim($channelItem2.attr('id'));

	if(itemId == channelItemId1 || itemId == channelItemId2){
		$(ui.item).addClass('ui-state-disabled').css({'opacity': 0.4});
	}
	tipsShowOption();
}


function init_budget_overview(){
	$('#loading-curtain-div').show();
	$("select[id$='channel1Picklist']").children().each(function(){
	    var $this = $(this);
	    if($this.val()){
	        var cId = $this.val();
			var currentId = cId, $dragCurrent = $(('#channelDivId li#' + currentId ));
			var $dragTarget = $("#channel1DivContent").find('li'), targetId = $dragTarget.attr('id');
			if(typeof(targetId) != 'undefined' && targetId != '' && currentId != targetId){	//cleart old channel
				$(('#channelDivId li#'+targetId)).removeClass('ui-state-disabled').css({'opacity': 1});
			}

			$dragTarget.attr({'id': currentId, 'liUnit__c': $dragCurrent.attr('liUnit__c'), 'liCost_Rate__c': $dragCurrent.attr('liCost_Rate__c')}).addClass('pgover').html($dragCurrent.text()).removeClass('ui-state-disabled');
			channelPopAssignment('add', null, $dragTarget);
	    }
	});
	$('#loading-curtain-div').hide();
}
/**
 *  added by Peng Zhu 2013-10-28 for channel picklist change
 */
function channel_change_fn(chType, chId){
	if(chType){
		var $channel1 = $("select[id$='channel1Picklist']"), $channel2 = $("select[id$='channel2Picklist']");
        var channel1 = $channel1.get(0), channel2 = $channel2.get(0);

        var lastSelecValCh1 = $channel1.data('lastSelecVal'),
        	lastSelecTxtCh1 = $channel1.data('lastSelecTxt'),
        	lastSelecValCh2 = $channel2.data('lastSelecVal'),
        	lastSelecTxtCh2 = $channel2.data('lastSelecTxt');

		switch(chType){
			case '1':
				if (lastSelecValCh1 != null && lastSelecTxtCh1 != null) {
	                jsAddItemToSelect(channel2, lastSelecTxtCh1, lastSelecValCh1);
	            }
	            var selecval = channel1.value;
	            lastSelecValCh1 = selecval;
	            var selectxt = channel1.options[channel1.selectedIndex].text;
	            lastSelecTxtCh1 = selectxt;
	            var selecidx = channel1.selectedIndex;
	            if (selecval) {
	                jsRemoveItemFromSelect(channel2, selecval);
	            }

            	$channel1.data('lastSelecVal', lastSelecValCh1);
        	 	$channel1.data('lastSelecTxt', lastSelecTxtCh1);

				if(chId){
					var currentId = chId, $dragCurrent = $(('#channelDivId li#' + currentId ));
					var $dragTarget = $("#channel1DivContent").find('li'), targetId = $dragTarget.attr('id');
					if(typeof(targetId) != 'undefined' && targetId != '' && currentId != targetId){	//cleart old channel
						$(('#channelDivId li#'+targetId)).removeClass('ui-state-disabled').css({'opacity': 1});
					}

					$dragTarget.attr({'id': currentId, 'liUnit__c': $dragCurrent.attr('liUnit__c'), 'liCost_Rate__c': $dragCurrent.attr('liCost_Rate__c')}).addClass('pgover').html($dragCurrent.text()).removeClass('ui-state-disabled');
					channelPopAssignment('add', null, $dragTarget);
				}
				else{
					$(('#channel1DivContent li')).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
					channelPopAssignment('del', '1', null);
				}
				break;
			case '2':
	            if (lastSelecValCh2 != null && lastSelecTxtCh2 != null) {
	                jsAddItemToSelect(channel1, lastSelecTxtCh2, lastSelecValCh2);
	            }
	            var selecval = channel2.value;
	            lastSelecValCh2 = selecval;
	            var selectxt = channel2.options[channel2.selectedIndex].text;
	            lastSelecTxtCh2 = selectxt;
	            var selecidx = channel2.selectedIndex;
	            if (selecval) {
	                jsRemoveItemFromSelect(channel1, selecval);
	            }
        	 	$channel2.data('lastSelecVal', lastSelecValCh2);
        	 	$channel2.data('lastSelecTxt', lastSelecTxtCh2);

				if(chId){
					var currentId = chId, $dragCurrent = $(('#channelDivId li#' + currentId ));
					var $dragTarget = $("#channel2DivContent").find('li'), targetId = $dragTarget.attr('id');
					if(typeof(targetId) != 'undefined' && targetId != '' && currentId != targetId){	//cleart old channel
						$(('#channelDivId li#'+targetId)).removeClass('ui-state-disabled').css({'opacity': 1});
					}

					$dragTarget.attr({'id': currentId, 'liUnit__c': $dragCurrent.attr('liUnit__c'), 'liCost_Rate__c': $dragCurrent.attr('liCost_Rate__c')}).addClass('pgover').html($dragCurrent.text()).removeClass('ui-state-disabled');
					channelPopAssignment('add', null, $dragTarget);
				}
				else{
					$(('#channel2DivContent li')).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
					channelPopAssignment('del', '2', null);
				}
				break;
			default: break;
		}
	}
}


function select_table_stop_fn(event,ui){
	var dtd = $.Deferred(),  $selectedLis = $('#matrixData li.ui-selected'), d = waitDeferredTipsTotal(dtd, $selectedLis);

	$.when(d)	//# Phys: //Market Rx:	//Product Rx:	//Market Share:
		.done(function(){
			channelPopAssignment('add', null, null);
			tipsShowOption($selectedLis);
		//	$('#colorbody').fadeIn();
		}).fail(function(){ if(window.console) console.log('fail waitDeferredTipsTotal');});
}

var h = 95, h_one = 157;
function e_over_click_overMenu(e){
	var $this = $(this), $channelDropDiv = $('#channelDropDiv'), $chartDiv = $('#chartDivId');
	switch(e.type){
		case 'mouseenter':
			overHidenTable($this);
			if(h == null){
				h = $channelDropDiv.height()-15;
			}
			break;
		case 'click':
			if($this.hasClass('hide')){
				$channelDropDiv.stop(true, true).animate({height: 20}, 1200, function(){
					$(this).find('.show').stop(true, true).fadeIn();
					$chartDiv.css({'line-height':'0px'});//.find('img').css({'top': 2});
				});//.show();
			}else{
				$channelDropDiv.stop(true, true).animate({height: h}, 1200, function(){
					$(this).removeAttr('style').find('.hide').stop(true, true).fadeIn();
					$chartDiv.css({'line-height': '90px'});//.find('img').css({'top': '45%'});
				});//.show();
			}
			$this.hide().find('img').removeClass('over');
			break;
		default : overHidenTable($this); break;
	}
}
function e_over_overTable(e){
	var $this = $(this), $overHide = $('#overTableHideId');
	switch(e.type){
	case 'mouseenter':
		//$overHide.fadeIn();
		$overHide.css("visibility","visible");
		break;
	default :
		if($this.height() > 30){
			//$overHide.fadeOut();
			$overHide.css("visibility","hidden");
		}
		break;
	}
}
function e_over_click_overTable(e){
	var $this = $(this), $matrixTableId = $('#matrixTotalClsId');
	switch(e.type){
	case 'mouseenter':
		overHidenTable($this);
		if(h_one == null){ h_one = $matrixTableId.height(); }
		break;
	case 'click':
		if($this.hasClass('hide')){
			$this.fadeOut(function(){
				$matrixTableId.stop(true, true).animate({height:25}, 1200, function(){
					$matrixTableId.find('div.tmpCls').hide();
					$this.addClass('show').removeClass('hide').attr({'title': 'show'}).show().find('img').removeClass('downDownCls').addClass('downUpCls');
				});
			});
		}else{
			$this.fadeOut(function(){
				$matrixTableId.find('div.tmpCls').show();
				$matrixTableId.stop(true, true).animate({height:h_one}, 1200, function(){
					$matrixTableId.removeAttr('style');
					$this.addClass('hide').removeClass('show').attr({'title': 'hide'}).fadeIn().find('img').removeClass('downUpCls').addClass('downDownCls');
				});
			});
		}
	//	$this.hide().find('img').removeClass('over');
		break;
	default : overHidenTable($this); break;
	}
}

function overHidenTable(_$this){
	if(_$this.hasClass('pgover')){
		_$this.removeClass('pgover').find('img').removeClass('over');
	}else{
		_$this.addClass('pgover').find('img').addClass('over');
	}
}

function e_over_channelDropDivId(e){
	var $this = $(this);
	switch(e.type){
		case 'mouseenter':
				if($this.height() > 50){
					$this.find('.hide').stop(true, true).fadeIn();//.show();
				}
			break;
		default :
				$this.removeClass('pgover').find('.hide').stop(true, true).fadeOut();//.hide();
			break;
	}
}



function e_over_draggable(e){
	var $this = $(this);
	switch(e.type){
		case 'mouseenter': $this.addClass('pgover'); break;
		default : $this.removeClass('pgover'); break;
	}
}

function showHeader_click(target){	//AppBodyHeader
	$('#loading-curtain-div').show();
	var $this = $(target), thisVal = 'ShowHeader', t = headerH, displayt = '';

	if($.trim($this.val()) == 'HideHeader'){
		thisVal = 'ShowHeader';
		displayt = 'none';
		t = 0;
	}else{
		thisVal = 'HideHeader';
	}
	$this.val(thisVal);
	$('#AppBodyHeader').stop(true, true).animate({height: t}, 1200, function(){
		$(this).find('#phHeader').css({'display': displayt});
		$('#loading-curtain-div').hide();
	});
}

;function eventHandle(event){
	var customMouser = {mouseenter: arguments[1],mouseleave: arguments[2]};
	customMouser[event.type].call(this, event);customMouser = {};
}
;function eventHandle1(event){
	var customMouser = {mouseenter: arguments[1],mouseleave: arguments[2],click: arguments[3]};
	customMouser[event.type].call(this, event);customMouser = {};
}
;function isUndefinedTag(elementTag){
	if(typeof(elementTag) != 'undefined'){
		return true;
	}
	return false;
}
function toObject(a){	//a[i][0]   [0] is first prodcut code
	var o = {};
	for (var i=0, j=a.length; i<j; i=i+1) { o[$.trim(a[i][0])] = a[i]; }
	return o;
}
function keys(t){
	var o = toObject(t), a=[], i;
	for (i in o) { if (o.hasOwnProperty(i)) { a.push(o[i]); } }
	return a;
}
function uniqArrays(x){ return (keys(x)); }

function killEnter(evt,el){
	if(evt.keyCode == 13){
		if(evt.preventDefault)
			evt.preventDefault();
		else
			event.returnValue = false;

		if(evt.stopPropagation)
			evt.stopPropagation();
		else
			event.cancelBubble = true;
		var $el = $(el);
		$el.next().focus().blur();
		if($el.hasClass('ipt_all')){
			applyAllQty(evt,$el.next().get(0));
		}
	}
}

/**
 * Show budget overview table
 * Added by Peng Zhu 2013-06-20
 */
function showBudgetOverviewTable(){
	if(!$('#overTableHideId').hasClass('hide')){
		$('#overTableHideId').fadeOut(function(){
			$('#matrixTotalClsId').find('div.tmpCls').show();
			$('#matrixTotalClsId').stop(true, true).animate({height:h_one}, 1200, function(){
				$('#matrixTotalClsId').removeAttr('style');
				$('#overTableHideId').addClass('hide').removeClass('show').attr({'title': 'hide'}).fadeIn().find('img').removeClass('downUpCls').addClass('downDownCls');
			});
		});
	}
}

function roundNumToKM(num,locale,kLevel,digits){

	if(isNaN(num)){
		if(!num)
		return 0;
		else return num;
	}
	var flag = num < 0 ? '-':'',startLevel = kLevel;
	num = Math.abs(num);
	if(!startLevel) startLevel = 1000;

	if(num<startLevel){
		num=FormatNums(num,locale,false,digits);
	}
	else if(num<1000000){
			num=Math.round(num/100)/10;
			//num=Math.round(num/1000);
			num=FormatNums(num,locale);
			num+='K';
	}
	else if(num<1000000000){
			num=Math.round(num/100000)/10;
			//num=Math.round(num/1000000);
			num=FormatNums(num,locale);
			num+='M';
	}else if(num<1000000000000){
			num=Math.round(num/100000000)/10;
			//num=Math.round(num/1000000000);
			num=FormatNums(num,locale);
			num+='B';
	}else if(num<1000000000000000){
			num=Math.round(num/100000000000)/10;
			//num=Math.round(num/1000000000);
			num=FormatNums(num,locale);
			num+='T';
	}
	else{
		alert('Meet with too large number : ' + num);
		num='';
	}
	if(num !== ''){
		num = flag + num;
	}
	return num;
}

function FormatNums(num,locale,flag,digits){
	locale=(''+locale).toLowerCase();
	if(num===''||(num&&num.charCodeAt&&num.substr)||isNaN(num)) {
		if(flag){
			if(locale === 'de'){
			 	unformateNum(num,'.',',',2);
			 }else{
			 	unformateNum(num,',','.',2);
			 }
		}else{
			unformateNum(num,locale,false); // change into number with function unformateNum.
		}
	}
	var isNegative = false;
	if(isNaN(digits))
		num = num.toFixed(1);
	else{
		isNegative = num < 0 ? true : false;
		num = parseFloat(num, 10);
		num = num.toFixed(digits);
	}
	var spt,deci,numint,numdec,numlen,result,position;
	if(locale=='de'){
		spt='.',deci=',',result='';
	}else{
		spt=',',deci='.',result='';
	}
	if(isNegative) {
		result = '-';
		num = num.replace('-','');
	}
	position=num.lastIndexOf('.');
	if(position>=0){
		numdec=num.substr(position+1);
		if(position>0)numint=num.substring(0,position);
	}else{
		numint=num;
	}

	numlen=numint.length;
	while(numlen>3){
		var deduct=numlen%3;
		deduct=(deduct==0?3:deduct);
			result+=numint.substr(0,deduct)+spt;
			numint=numint.substr(deduct);
			numlen=numlen-deduct;
	}
	result+=numint;
	if(position>=0){
		result=result+deci+numdec;
	}
	return result;
}

function unformateNum(num,seperator,decimal,setScale,locale){
	num=''+num;
	var regEndWithK=/[K,k]{1}$/;
	var regEndWithM=/[M,m]{1}$/;
	var regEndWithB=/[B,b]{1}$/;
	var regEndWithT=/[T,t]{1}$/;
	var isEndWithK=regEndWithK.test(num);
	var isEndWithM=regEndWithM.test(num);
	var isEndWithB=regEndWithB.test(num);
	var isEndWithT=regEndWithT.test(num);
	var otherchar;

	otherchar=/[^\d\,]/g ;
	num=num.replace(otherchar,'');
	if(locale=='de'){
		num=num.replace(',','.');//replace with decimal
	}
	num=parseFloat(num);
	if(isNaN(num)) num=0;
	if(isEndWithK){
		num*=1000;
	}else if(isEndWithM){
		num*=1000000;
	}else if(isEndWithB){
		num*=1000000000;
	}else if(isEndWithT){
		num*=1000000000000;
	}
	return num;
}

/**
 *  Added by Peng Zhu 2013-10-31
 */
function waitDeferredChannel(dtd, channelId){
	var tasks = function(){
		$("select[id$='channel1Picklist']").val(channelId);
		$("select[id$='channel1Picklist']").change();
		matrixTotalTable(false);
		dtd.resolve();
	};
	setTimeout(tasks,800);
	return dtd.promise();
}

function initBudgetOverviewTable(chArr){
	if($('#loading-curtain-div').is(':hidden')) $('#loading-curtain-div').show();
	if(chArr && chArr.length){
		var channelId = chArr.pop();
		var dtd = $.Deferred(), d = waitDeferredChannel(dtd, channelId);
		$.when(d)
		.done(function(){
			initBudgetOverviewTable(chArr);
		}).fail(function(){ if(window.console)console.log('fail list_rows'); });
	}
	else{

	    if($("select[id$='channel2Picklist']").val()){
	    	$("select[id$='channel2Picklist']").change();
	    }

		$('#loading-curtain-div').hide();
	}
}

/**
 *  Function for select
 */
function jsSelectIsExitItem(objSelect, objItemValue) {
    var isExit = false;
    if (objSelect) {
    for (var i = 0; i < objSelect.options.length; i++) {
        if (objSelect.options[i].value == objItemValue) {
            isExit = true;
            break;
        }
    }
    }
    return isExit;
}


function jsAddItemToSelect(objSelect, objItemText, objItemValue) {
	if (objSelect){
    if (jsSelectIsExitItem(objSelect, objItemValue)) {
    } else {
        var varItem = new Option(objItemText, objItemValue);
        objSelect.options.add(varItem);
    }
    }
}

function jsRemoveItemFromSelect(objSelect, objItemValue) {
	if (objSelect){
    if (jsSelectIsExitItem(objSelect, objItemValue)) {
        for (var i = 0; i < objSelect.options.length; i++) {
            if (objSelect.options[i].value == objItemValue) {
                objSelect.options.remove(i);
                break;
            }
        }
    } else {
    }
    }
}

/**
 *  added by Peng Zhu 2013-11-14
 */
function filterCombinationNameList_change(idx){
    hide0Values();
	var segmentArr = ['Gain', 'Build', 'Defend', 'Observe', 'Maintain','Blank', 'Threshold'];
	var channelIdArr = [];
	var summarySegment = {'Gain':0, 'Build':0, 'Defend':0, 'Observe':0,'Maintain':0 ,'Blank':0, 'Threshold':0 };
	var summaryPotentialPercent = {'Gain_Potential':0, 'Build_Potential':0, 'Defend_Potential':0, 'Observe_Potential':0, 'Maintain_Potential':0, 'Blank_Potential':0, 'Threshold_Potential':0}
	var summaryAdoptionPercent = {'Gain_Adoption':0, 'Build_Adoption':0, 'Defend_Adoption':0, 'Observe_Adoption':0, 'Maintain_Adoption':0, 'Blank_Adoption':0, 'Threshold_Adoption':0}
	var _P = '_Potential', _A = '_Adoption', _C='_Cust';
	var totalPotential = 0, totalAdoption = 0;
	var GainCust = 0, BuildCust = 0, DefendCust = 0, ObserveCust = 0, MaintainCust = 0, BlankCust = 0, ThresholdCust = 0;
	var GainCustS = 0, BuildCustS = 0, DefendCustS = 0, ObserveCustS = 0, MaintainCustS = 0, BlankCustS = 0, ThresholdCustS = 0;
	var thelocale= Globalize.parseInt('1.0') == 1 ? 'en_US' : 'de';
	/**if(/^de/.test(UserContext.locale)){
		thelocale = 'de';
	}*/

	var $li = $('#matrixData li.ui-selected');
	var isSelected = false;
	var selectLimap = {};

	if($li.length > 0 ) {
		isSelected = true;
	}
	$li.each(function() {
		var cls = $(this).attr('class');
		var col = /column_\d+/.exec(cls);
		var row = /row_\d+/.exec(cls);
		if(!!!selectLimap[col]){
			selectLimap[col] = {};
		}
		selectLimap[col][row] = true;
	});

	//var reportMcIdArray = {'GainMcIds':'','BuildMcIds':'','DefendMcIds':'','ObserveMcIds':'','MaintainMcIds':'','BlankMcIds':'', 'ThresholdMcIds':''};
	var reportMcIdArray = {};
	for (var i = JSON_CHANNELS.length - 1; i >= 0; i--) {
		var o = JSON_CHANNELS[i];
		var oCust = o.customers == 'null' ? 0 : parseInt(o.customers);
		var oCustSelect = 0;
		var oAllocatedUnits = o.allocatedUnits == 'null' ? 0 : parseInt(o.allocatedUnits);


		var oPotential = o.potential == 'null' ? 0 : parseInt(o.potential);
		var oPotentialSelect = 0;

		var oAdoption = o.intimacy == 'null' ? 0 : parseInt(o.intimacy);
		var oAdoptionSelect = 0;
		var custChannel = 0;


		if(isSelected) {
			if(selectLimap["column_" + o.cellColumn] && selectLimap["column_" + o.cellColumn]["row_" + o.cellRow]){
				oPotentialSelect = o.potential == 'null' ? 0 : parseInt(o.potential);
				oAdoptionSelect = o.intimacy == 'null' ? 0 : parseInt(o.intimacy);
				oCustSelect = o.customers == 'null' ? 0 : parseInt(o.customers);
				//added for r121
				if (!reportMcIdArray[o.segment + "McIds"]) {
					reportMcIdArray[o.segment + "McIds"] = '';
				}
				var mcIds = reportMcIdArray[o.segment + "McIds"];

				var omcid = o.mcId;
				if(omcid && omcid.length && omcid.length > 15) omcid = omcid.substring(0, 15);

				//if (mcIds.indexOf(o.mcId) == -1) {
				//
				//	mcIds = mcIds == '' ? o.mcId : mcIds + ',' + o.mcId;
				//}
				if (mcIds.indexOf(omcid) == -1) {

					mcIds = mcIds == '' ? omcid : mcIds + ',' + omcid;
				}
				reportMcIdArray[o.segment + "McIds"] = mcIds;
				
			}
		}else{
			oPotentialSelect  = o.potential == 'null' ? 0 : parseInt(o.potential);
			oAdoptionSelect = o.intimacy == 'null' ? 0 : parseInt(o.intimacy);
			oCustSelect = o.customers == 'null' ? 0 : parseInt(o.customers);
			//added for r121
				if (!reportMcIdArray[o.segment + "McIds"]) {
					reportMcIdArray[o.segment + "McIds"] = '';
				}
				var mcIds = reportMcIdArray[o.segment + "McIds"];
                
				var omcid = o.mcId;
				if(omcid && omcid.length && omcid.length > 15) omcid = omcid.substring(0, 15);

				if (mcIds.indexOf(omcid) == -1) {
					mcIds = mcIds == '' ? omcid : mcIds + ',' + omcid;
				}
				//if (mcIds.indexOf(o.mcId) == -1) {
				//	mcIds = mcIds == '' ? o.mcId : mcIds + ',' + o.mcId;
				//}
				reportMcIdArray[o.segment + "McIds"] = mcIds;
		}

		if (o.channelId && ($.trim(o.channelId) != '') && (segmentArr.indexOf(o.segment) > -1)) {

			if (!channelIdArr[o.channelId]){
				channelIdArr[o.channelId] = {'Gain':0, 'Build':0, 'Defend':0, 'Observe':0,'Maintain':0,'Blank':0, 'Threshold':0,
											'GainSelect':0, 'BuildSelect':0, 'DefendSelect':0, 'ObserveSelect':0,'MaintainSelect':0,'BlankSelect':0, 'ThresholdSelect':0,
											'GainChannel':0, 'BuildChannel':0, 'DefendChannel':0, 'ObserveChannel':0,'MaintainChannel':0,'BlankChannel':0, 'ThresholdChannel':0,
											'Gain_Cust':0,'Build_Cust':0,'Defend_Cust':0,'Observe_Cust':0,'Maintain_Cust':0,'Blank_Cust':0, 'Threshold_Cust':0,
											'TotalPlan':0};
			}

			var seg_v = parseInt(channelIdArr[o.channelId][o.segment]), seg_vt = 0, channelCustomer=0, totalPlan = parseInt(channelIdArr[o.channelId]['TotalPlan']), seg_custChannel;

			//calculate avg
			var totalQty = 0, totalCust = 0, qtyChannel=0;
			//var qty = o.map_index_cmcdo[i].qty == null ? 0 : o.map_index_cmcdo[].qty;
			for(var j in o.map_index_cmcdo){
				var j_qty = o.map_index_cmcdo[j].qty == null ? 0 : o.map_index_cmcdo[j].qty;
				var j_cust = o.map_index_cmcdo[j].accountNum == null ? 0 : o.map_index_cmcdo[j].accountNum;

				if(isSelected) {
					if(selectLimap["column_" + o.cellColumn] && selectLimap["column_" + o.cellColumn]["row_" + o.cellRow]){
						totalQty += parseInt(j_qty) * parseInt(j_cust);
						totalCust += parseInt(j_cust);

						if(parseInt(j_qty) > 0)
							qtyChannel+=parseInt(j_cust);
					}
				}else{
					totalQty += parseInt(j_qty) * parseInt(j_cust);
					totalCust += parseInt(j_qty);
					if(parseInt(j_qty) > 0)
						qtyChannel+=parseInt(j_cust);
				}
			}

			if(totalQty > 0){
				seg_vt = totalQty;//parseFloat(totalQty/totalCust).toFixed(0);
				channelCustomer = qtyChannel;
			}

			seg_v = seg_vt;//parseInt(seg_vt) * parseInt(oAllocatedUnits);
			totalPlan += seg_v;//parseInt(seg_vt) * parseInt(seg_v);

			channelIdArr[o.channelId][o.segment] += seg_v;
			channelIdArr[o.channelId][o.segment+_C] += oCust;
			channelIdArr[o.channelId][o.segment + "Select"] += oCustSelect;
			//Sum up total customer with at least one detail of channel assigned
			channelIdArr[o.channelId][o.segment+'Channel'] += channelCustomer;
			channelIdArr[o.channelId]['TotalPlan'] = totalPlan;

			//jescobar: Sum up the total potential by segment and the total potential for the matrix
			summaryPotentialPercent[o.segment+_P] += oPotentialSelect;
			summaryAdoptionPercent[o.segment+_A] += oAdoptionSelect;
			totalPotential += oPotential;
			totalAdoption += oAdoption;
		};
	};

	var totalGain =0, totalBuild = 0, totalDefend = 0, totalObserve = 0, totalMaintain = 0, totalBlank = 0, totalThreshold = 0, totalAllocatedBudget = 0, PlannedclsT = 0; totalNotSelect = 0; totalNoselctnum = 0;
	var totalGainU =0, totalBuildU = 0, totalDefendU = 0, totalObserveU = 0, totalMaintainU = 0, totalBlankU = 0, totalThresholdU = 0;

	var isfirstnotselect = false;
	if($.trim($("#matrixTabTotal tfoot tr").find(".totalNotSelect").text()) === '') {
		isfirstnotselect = true;
	}

	var totalPlannedcls = 0;

	$("#matrixTabTotal tbody tr[id$='_id']").each(function(){
		var PlannedTotal = 0, GainTotal = 0, BuildTotal = 0, DefendTotal = 0, ObserveTotal = 0, MaintainTotal = 0,BlankTotal = 0, ThresholdTotal = 0, Plannedin = 0;
		var $tr = $(this), trId = $tr.attr('id'), channelId = trId.split('_')[0];

		var UnitCosts = $tr.find("input[name = UnitCosts]").val() ? parseFloat($tr.find("input[name = UnitCosts]").val()) : 0;
		if(channelIdArr[channelId]){
			PlannedTotal = channelIdArr[channelId]['TotalPlan'];
			totalPlannedcls += PlannedTotal;

			GainTotal = channelIdArr[channelId]['Gain'];
			BuildTotal = channelIdArr[channelId]['Build'];
			DefendTotal = channelIdArr[channelId]['Defend'];
			ObserveTotal = channelIdArr[channelId]['Observe'];
			MaintainTotal = channelIdArr[channelId]['Maintain'];
			BlankTotal = channelIdArr[channelId]['Blank'];
			ThresholdTotal = channelIdArr[channelId]['Threshold'];
        
			//jescobar: Get total customer by Segment
			GainCust = (channelIdArr[channelId]['Gain'+_C]);
			BuildCust = (channelIdArr[channelId]['Build'+_C]);
			DefendCust = (channelIdArr[channelId]['Defend'+_C]);
			ObserveCust = (channelIdArr[channelId]['Observe'+_C]);
			MaintainCust = (channelIdArr[channelId]['Maintain'+_C]);
			BlankCust = (channelIdArr[channelId]['Blank'+_C]);
			ThresholdCust = (channelIdArr[channelId]['Threshold'+_C]);

			//jescobar: Get total customer by Segment
			GainCustS = (channelIdArr[channelId]['GainSelect']);
			BuildCustS = (channelIdArr[channelId]['BuildSelect']);
			DefendCustS = (channelIdArr[channelId]['DefendSelect']);
			ObserveCustS = (channelIdArr[channelId]['ObserveSelect']);
			MaintainCustS = (channelIdArr[channelId]['MaintainSelect']);
			BlankCustS = (channelIdArr[channelId]['BlankSelect']);
			ThresholdCustS = (channelIdArr[channelId]['ThresholdSelect']);

			//Customers with at least one channel assigned
			GainChannel = (channelIdArr[channelId]['GainChannel']);
			BuildChannel = (channelIdArr[channelId]['BuildChannel']);
			DefendChannel = (channelIdArr[channelId]['DefendChannel']);
			ObserveChannel = (channelIdArr[channelId]['ObserveChannel']);
			MaintainChannel = (channelIdArr[channelId]['MaintainChannel']);
			BlankChannel = (channelIdArr[channelId]['BlankChannel']);
			ThresholdChannel = (channelIdArr[channelId]['ThresholdChannel']);
		};

		var channelTotal = $.trim($tr.find('td input[name=totalUnits]').val());
		channelTotal = channelTotal == '' ? 0 : parseInt(channelTotal);


		var AvaBudcls =$.trim($tr.find('input[name=totalUnitnum]').val());
		AvaBudcls = AvaBudcls == '' ? 0 : parseInt(AvaBudcls);

		$tr.find('td.Plannedcls').text(FormatNums(PlannedTotal,thelocale,false,1));

		var allocatedBudget = PlannedTotal * UnitCosts;
		$tr.find('td.AllocatedBudgetcls').text(FormatNums(parseInt(allocatedBudget),thelocale,false,0));

		if(AvaBudcls != 0){
			Plannedin = accMul((PlannedTotal / AvaBudcls), 100);
		}
		//console.log(5);
		/*
		$tr.find('td.Plannedincls').text(Globalize.format(Plannedin, 'n1')+' %');
		$tr.find('td.Gaincls').text(FormatNums(GainTotal,thelocale,false,1));
		$tr.find('td.Buildcls').text(FormatNums(BuildTotal,thelocale,false,1));
		$tr.find('td.Defendcls').text(FormatNums(DefendTotal,thelocale,false,1));
		$tr.find('td.Observecls').text(FormatNums(ObserveTotal,thelocale,false,1));
		$tr.find('td.Maintaincls').text(FormatNums(MaintainTotal,thelocale,false,1));
		$tr.find('td.Blankcls').text(FormatNums(BlankTotal,thelocale,false,1));
		$tr.find('td.Thresholdcls').text(FormatNums(ThresholdTotal,thelocale,false,1));

		$tr.find('td .NotSelectclsHideU').val(FormatNums(GainTotal + BuildTotal + DefendTotal + ObserveTotal + MaintainTotal + BlankTotal + ThresholdTotal,thelocale,false,1));
		if(isfirstnotselect) {
			$tr.find('td.NotSelectcls').text(FormatNums(0,thelocale,false,1));
		}
		*/

		$tr.find('td.Plannedincls').text(Globalize.format(Plannedin, 'n1')+' %');
		$tr.find('td.Gaincls').text(FormatNums(parseInt(GainTotal),thelocale,false,0));
		$tr.find('td.Buildcls').text(FormatNums(parseInt(BuildTotal),thelocale,false,0));
		$tr.find('td.Defendcls').text(FormatNums(parseInt(DefendTotal),thelocale,false,0));
		$tr.find('td.Observecls').text(FormatNums(parseInt(ObserveTotal),thelocale,false,0));
		$tr.find('td.Maintaincls').text(FormatNums(parseInt(MaintainTotal),thelocale,false,0));
		$tr.find('td.Blankcls').text(FormatNums(parseInt(BlankTotal),thelocale,false,0));
		$tr.find('td.Thresholdcls').text(FormatNums(parseInt(ThresholdTotal),thelocale,false,0));

		//Customer channels assigned
		$tr.find('td.GainChannel').text(FormatNums(parseInt(GainChannel),thelocale,false,0));
		$tr.find('td.BuildChannel').text(FormatNums(parseInt(BuildChannel),thelocale,false,0));
		$tr.find('td.DefendChannel').text(FormatNums(parseInt(DefendChannel),thelocale,false,0));
		$tr.find('td.ObserveChannel').text(FormatNums(parseInt(ObserveChannel),thelocale,false,0));
		$tr.find('td.MaintainChannel').text(FormatNums(parseInt(MaintainChannel),thelocale,false,0));
		$tr.find('td.BlankChannel').text(FormatNums(parseInt(BlankChannel),thelocale,false,0));
		$tr.find('td.ThresholdChannel').text(FormatNums(parseInt(ThresholdChannel),thelocale,false,0));

		$tr.find('td .NotSelectclsHideU').val(FormatNums(parseInt(GainTotal + BuildTotal + DefendTotal + ObserveTotal + MaintainTotal + BlankTotal + ThresholdTotal),thelocale,false,0));
		if(isfirstnotselect) {
			$tr.find('td.NotSelectcls').text(FormatNums(0,thelocale,false,0));
		}

		//jescobar: Set Detail Average Customer
		var gainAvgCust = GainTotal * UnitCosts;
		totalGain += gainAvgCust;
		totalGainU += GainTotal;
		var buildAvgCust = BuildTotal * UnitCosts;
		totalBuild += buildAvgCust;
		totalBuildU += BuildTotal;
		var defendAvgCust = DefendTotal * UnitCosts;
		totalDefend += defendAvgCust;
		totalDefendU += DefendTotal;
		var observeAvgCust = ObserveTotal * UnitCosts;
		totalObserve += observeAvgCust;
		totalObserveU += ObserveTotal;
		var maintainAvgCust = MaintainTotal * UnitCosts;
		totalMaintain += maintainAvgCust;
		totalMaintainU += MaintainTotal;
		var blankAvgCust = BlankTotal * UnitCosts;
		totalBlank += blankAvgCust;
		totalBlankU += BlankTotal;
		var thresholdAvgCust = ThresholdTotal * UnitCosts;
		totalThreshold += thresholdAvgCust;
		totalThresholdU += ThresholdTotal;

		var NotSelect = (GainTotal + BuildTotal + DefendTotal + ObserveTotal + MaintainTotal + BlankTotal + ThresholdTotal) * UnitCosts;
		totalNotSelect += NotSelect;
		totalNoselctnum += GainTotal + BuildTotal + DefendTotal + ObserveTotal + MaintainTotal + BlankTotal + ThresholdTotal;
		PlannedclsT += GainTotal + BuildTotal + DefendTotal + ObserveTotal + MaintainTotal + BlankTotal + ThresholdTotal;
		totalAllocatedBudget += gainAvgCust + buildAvgCust + defendAvgCust + observeAvgCust + maintainAvgCust + blankAvgCust + thresholdAvgCust;
		$tr.find('td.AvgCustGain').text(FormatNums(gainAvgCust,thelocale,false,1));
		$tr.find('td.AvgCustBuild').text(FormatNums(buildAvgCust,thelocale,false,1));
		$tr.find('td.AvgCustDefend').text(FormatNums(defendAvgCust,thelocale,false,1));
		$tr.find('td.AvgCustObserve').text(FormatNums(observeAvgCust,thelocale,false,1));
		$tr.find('td.AvgCustMaintain').text(FormatNums(maintainAvgCust,thelocale,false,1));
		$tr.find('td.AvgCustBlank').text(FormatNums(blankAvgCust,thelocale,false,1));
		$tr.find('td.AvgCustThreshold').text(FormatNums(thresholdAvgCust,thelocale,false,1));

		if(isfirstnotselect) {
			$tr.find(".AvgCustOtherMatrice").text(FormatNums($tr.find(".AvgCustOtherMatrice").text(),thelocale,false,1));
			//console.log(':: Others: ', $tr.find(".AvgCustOtherMatrice").text());
			//$tr.find(".OtherMatricecls").text(FormatNums($tr.find(".OtherMatricecls").text(),thelocale,false,0));
			$tr.find('td.AvgCustNotSelect').text(FormatNums(0,thelocale,false,1));
		}

		//Begin: added by Peng Zhu 2013-06-28 for remaining field
		var remaingTotal = $tr.find('td input[name=remainingUnits]').val();
		if(typeof remaingTotal != 'undefined' && $.trim(remaingTotal) != '')
			remaingTotal = Globalize.parseInt(remaingTotal);
		else
			remaingTotal = 0;

		remaingTotal = channelTotal - PlannedTotal;
		if(remaingTotal < 0)
			remaingTotal = 0;

		$tr.find('td.Remainingcls').text(formatCurrentNumber(remaingTotal));
		//End: added by Peng Zhu 2013-06-28 for remaining field

		var AvaBudcls =$.trim($tr.find('input[name=totalUnitnum]').val());
		AvaBudcls = AvaBudcls == '' ? 0 : parseInt(AvaBudcls);

		var Plannedcls =$.trim($tr.find('.Plannedcls').text());
		Plannedcls = Plannedcls == '' ? 0 : Globalize.parseInt(Plannedcls);

		var totalNotSelect =$.trim($tr.find('.NotSelectcls').text());
		totalNotSelect = totalNotSelect == '' ? 0 : Globalize.parseInt(totalNotSelect);

		var OtherMatriceclsU =$.trim($tr.find('.OtherMatricecls').text());

		OtherMatriceclsU = OtherMatriceclsU == '' ? 0 : Globalize.parseInt(OtherMatriceclsU);

		$tr.find(".AvaBudcls").text(FormatNums(parseInt(AvaBudcls - Plannedcls - totalNotSelect - OtherMatriceclsU),thelocale,false,0));//TODO

	});

	var channelTotal = $.trim($("#matrixTabTotal tfoot").find('th input[name=totalUnits]').val());
		channelTotal = channelTotal == '' ? 0 : parseInt(channelTotal);
	if(channelTotal == 0){
		totalPlannedin = accMul(0, 100);
	} else {
		totalPlannedin = accMul((PlannedclsT / channelTotal), 100);
	}

	$("#matrixTabTotal tfoot .Plannedcls").text(FormatNums(totalPlannedcls,thelocale,false,1));
	$("#matrixTabTotal tfoot .totalGain").text(FormatNums(totalGain,thelocale,false,1));
	$("#matrixTabTotal tfoot .totalBuild").text(FormatNums(totalBuild,thelocale,false,1));
	$("#matrixTabTotal tfoot .totalDefend").text(FormatNums(totalDefend,thelocale,false,1));
	$("#matrixTabTotal tfoot .totalObserve").text(FormatNums(totalObserve,thelocale,false,1));
	$("#matrixTabTotal tfoot .totalMaintain").text(FormatNums(totalMaintain,thelocale,false,1));
	$("#matrixTabTotal tfoot .totalBlank").text(FormatNums(totalBlank,thelocale,false,1));
	$("#matrixTabTotal tfoot .totalThreshold").text(FormatNums(totalThreshold,thelocale,false,1));

	/*
	$("#matrixTabTotal tfoot .Gaincls").text(FormatNums(totalGainU,thelocale,false,1));
	$("#matrixTabTotal tfoot .Buildcls").text(FormatNums(totalBuildU,thelocale,false,1));
	$("#matrixTabTotal tfoot .Defendcls").text(FormatNums(totalDefendU,thelocale,false,1));
	$("#matrixTabTotal tfoot .Observecls").text(FormatNums(totalObserveU,thelocale,false,1));
	$("#matrixTabTotal tfoot .Maintaincls").text(FormatNums(totalMaintainU,thelocale,false,1));
	$("#matrixTabTotal tfoot .Blankcls").text(FormatNums(totalBlankU,thelocale,false,1));
	$("#matrixTabTotal tfoot .Thresholdcls").text(FormatNums(totalThresholdU,thelocale,false,1));

	*/

	$("#matrixTabTotal tfoot .Gaincls").text(FormatNums(parseInt(totalGainU),thelocale,false,0));
	$("#matrixTabTotal tfoot .Buildcls").text(FormatNums(parseInt(totalBuildU),thelocale,false,0));
	$("#matrixTabTotal tfoot .Defendcls").text(FormatNums(parseInt(totalDefendU),thelocale,false,0));
	$("#matrixTabTotal tfoot .Observecls").text(FormatNums(parseInt(totalObserveU),thelocale,false,0));
	$("#matrixTabTotal tfoot .Maintaincls").text(FormatNums(parseInt(totalMaintainU),thelocale,false,0));
	$("#matrixTabTotal tfoot .Blankcls").text(FormatNums(parseInt(totalBlankU),thelocale,false,0));
	$("#matrixTabTotal tfoot .Thresholdcls").text(FormatNums(parseInt(totalThresholdU),thelocale,false,0));

	$("#matrixTabTotal tfoot .NotSelectclsHideB").val(totalNotSelect);
	$("#matrixTabTotal tfoot .OtherMatricecls").text(FormatNums($("#matrixTabTotal tfoot").find(".OtherMatriceclsHide").val(),thelocale,false,1));

	if(isfirstnotselect) {
		$("#matrixTabTotal tfoot .OtherMatriceTd").text(FormatNums(parseInt($("#matrixTabTotal tfoot .OtherMatriceTd").text()),thelocale,false,0));
		$("#matrixTabTotal tfoot .NotSelectcls").text(FormatNums(0,thelocale,false,0));
		$("#matrixTabTotal tfoot .totalNotSelect").text(FormatNums(0,thelocale,false,1));

		var OtherMatrice =$.trim($("#matrixTabTotal tfoot").find('th .OtherMatriceclsU').val());
		OtherMatrice = OtherMatrice == '' ? 0 : parseInt(OtherMatrice);
		$("#matrixTabTotal tfoot .AvaBudcls").text(FormatNums(parseInt(channelTotal - Plannedcls - OtherMatrice - 0),thelocale,false,0));//TODO
	}

	$("#matrixTabTotal tfoot .AllocatedBudgetcls").text(FormatNums(parseInt(totalAllocatedBudget),thelocale,false,0));
	//console.log(1);
	$("#matrixTabTotal tfoot .Plannedincls").text(Globalize.format(totalPlannedin, 'n1')+' %');
	//@jescobar: Potential segments summary table
	$("#matrixTabTotal tbody tr[id$='_seg']").each(function(){
		var GainPotentialP = 0, BuidlPotentialP = 0, DefendPotentialP = 0, ObservePotentialP = 0, MaintainPotentialP = 0, BlankPotentialP = 0, ThresholdkPotentialP = 0;
		var GainAdoptionP = 0, BuidlAdoptionP = 0, DefendAdoptionP = 0, ObserveAdoptionP = 0, MaintainAdoptionP = 0, BlankAdoptionP = 0, ThresholdAdoptionP = 0;
		var $tr = $(this);
            
		//////console.log(':: Gain Cust: ' + GainCust);
		//jescobar: Set data for total customer by Segment
		/*
		$tr.find('td.GainclsCustomer').text(FormatNums(GainCustS,thelocale,false,1));
		$tr.find('td.BuildclsCustomer').text(FormatNums(BuildCustS,thelocale,false,1));
		$tr.find('td.DefendclsCustomer').text(FormatNums(DefendCustS,thelocale,false,1));
		$tr.find('td.ObserveclsCustomer').text(FormatNums(ObserveCustS,thelocale,false,1));
		$tr.find('td.MaintainclsCustomer').text(FormatNums(MaintainCustS,thelocale,false,1));
		$tr.find('td.BlankclsCustomer').text(FormatNums(BlankCustS,thelocale,false,1));
		$tr.find('td.ThresholdclsCustomer').text(FormatNums(ThresholdCustS,thelocale,false,1));
		*/

		//for r121
		//reportMcIdArray[o.segment + "McIds"] = mcIds;
		$tr.find('td.GainclsCustomer').empty();
		var gainStr = '<a title="Report" href="' + matrixDrillDownReportUrl + '?' + reportPVofMatrixCell +'=' + (reportMcIdArray["GainMcIds"] || '') + '" name="drillDownReport" target="_blank"><span id="physId">'+ FormatNums(parseInt(GainCustS),thelocale,false,0) + '</span>' +
                                				'<img src="/s.gif" class="imgClass" alt="Report" title="Report" style="background-image:url(/img/sprites/master.png); background-position:0 -282px; width: 16px; height: 11px;margin-left: 5px;" /></a>';
		$tr.find('td.GainclsCustomer').append(gainStr);
		$tr.find('td.BuildclsCustomer').empty();
		var buildStr = '<a title="Report" href="' + matrixDrillDownReportUrl + '?' + reportPVofMatrixCell +'=' + (reportMcIdArray["BuildMcIds"] || '') + '" name="drillDownReport" target="_blank"><span id="physId">'+ FormatNums(parseInt(BuildCustS),thelocale,false,0) + '</span>' +
                                				'<img src="/s.gif" class="imgClass" alt="Report" title="Report" style="background-image:url(/img/sprites/master.png); background-position:0 -282px; width: 16px; height: 11px;margin-left: 5px;" /></a>';
		$tr.find('td.BuildclsCustomer').append(buildStr);

		$tr.find('td.DefendclsCustomer').empty();
		var defendStr = '<a title="Report" href="' + matrixDrillDownReportUrl + '?' + reportPVofMatrixCell +'=' + (reportMcIdArray["DefendMcIds"] || '') + '" name="drillDownReport" target="_blank"><span id="physId">'+ FormatNums(parseInt(DefendCustS),thelocale,false,0) + '</span>' +
                                				'<img src="/s.gif" class="imgClass" alt="Report" title="Report" style="background-image:url(/img/sprites/master.png); background-position:0 -282px; width: 16px; height: 11px;margin-left: 5px;" /></a>';
		$tr.find('td.DefendclsCustomer').append(defendStr);

		$tr.find('td.ObserveclsCustomer').empty();
		var observeStr = '<a title="Report" href="' + matrixDrillDownReportUrl + '?' + reportPVofMatrixCell +'=' + (reportMcIdArray["ObserveMcIds"] || '') + '" name="drillDownReport" target="_blank"><span id="physId">'+ FormatNums(parseInt(ObserveCustS),thelocale,false,0) + '</span>' +
                                				'<img src="/s.gif" class="imgClass" alt="Report" title="Report" style="background-image:url(/img/sprites/master.png); background-position:0 -282px; width: 16px; height: 11px;margin-left: 5px;" /></a>';
		$tr.find('td.ObserveclsCustomer').append(observeStr);

		$tr.find('td.MaintainclsCustomer').empty();
		var maintainStr = '<a title="Report" href="' + matrixDrillDownReportUrl + '?' + reportPVofMatrixCell +'=' + (reportMcIdArray["MaintainMcIds"] || '') + '" name="drillDownReport" target="_blank"><span id="physId">'+ FormatNums(parseInt(MaintainCustS),thelocale,false,0) + '</span>' +
                                				'<img src="/s.gif" class="imgClass" alt="Report" title="Report" style="background-image:url(/img/sprites/master.png); background-position:0 -282px; width: 16px; height: 11px;margin-left: 5px;" /></a>';
		$tr.find('td.MaintainclsCustomer').append(maintainStr);

		$tr.find('td.BlankclsCustomer').empty();
		var blankStr = '<a title="Report" href="' + matrixDrillDownReportUrl + '?' + reportPVofMatrixCell +'=' + (reportMcIdArray["BlankMcIds"] || '') + '" name="drillDownReport" target="_blank"><span id="physId">'+ FormatNums(parseInt(BlankCustS),thelocale,false,0) + '</span>' +
                                				'<img src="/s.gif" class="imgClass" alt="Report" title="Report" style="background-image:url(/img/sprites/master.png); background-position:0 -282px; width: 16px; height: 11px;margin-left: 5px;" /></a>';
		$tr.find('td.BlankclsCustomer').append(blankStr);

		$tr.find('td.ThresholdclsCustomer').empty();
		var thresholdStr = '<a title="Report" href="' + matrixDrillDownReportUrl + '?' + reportPVofMatrixCell +'=' + (reportMcIdArray["ThresholdMcIds"] || '') + '" name="drillDownReport" target="_blank"><span id="physId">'+ FormatNums(parseInt(ThresholdCustS),thelocale,false,0) + '</span>' +
                                				'<img src="/s.gif" class="imgClass" alt="Report" title="Report" style="background-image:url(/img/sprites/master.png); background-position:0 -282px; width: 16px; height: 11px;margin-left: 5px;" /></a>';
		$tr.find('td.ThresholdclsCustomer').append(thresholdStr);

		if($("#matrixData li.ui-selected").length > 0) {
			$tr.find('td.NotSelectclsCustomer').text(FormatNums(parseInt(GainCust + BuildCust + DefendCust + ObserveCust + MaintainCust + BlankCust + ThresholdCust - (GainCustS + BuildCustS + DefendCustS + ObserveCustS + MaintainCustS + BlankCustS + ThresholdCustS)),thelocale,false,0));
		}else{
			$tr.find('td.NotSelectclsCustomer').text(FormatNums(0,thelocale,false,0));
		}

		/**jescobar: Get potential and adoption total by Segment*/
		//Potential
		GainPotentialP = summaryPotentialPercent['Gain'+_P];
		BuidlPotentialP = summaryPotentialPercent['Build'+_P];
		DefendPotentialP = summaryPotentialPercent['Defend'+_P];
		ObservePotentialP = summaryPotentialPercent['Observe'+_P];
		MaintainPotentialP = summaryPotentialPercent['Maintain'+_P];
		BlankPotentialP = summaryPotentialPercent['Blank'+_P];
		ThresholdPotentialP = summaryPotentialPercent['Threshold'+_P];

		//Adoption
		GainAdoptionP = summaryAdoptionPercent['Gain'+_A];
		BuidlAdoptionP = summaryAdoptionPercent['Build'+_A];
		DefendAdoptionP = summaryAdoptionPercent['Defend'+_A];
		ObserveAdoptionP = summaryAdoptionPercent['Observe'+_A];
		MaintainAdoptionP = summaryAdoptionPercent['Maintain'+_A];
		BlankAdoptionP = summaryAdoptionPercent['Blank'+_A];
		ThresholdAdoptionP = summaryAdoptionPercent['Threshold'+_A];

		//@jescobar: Calculate the potential % for each segment and channel
		var gainPP = (totalPotential == null || totalPotential== 0) ? 0 : accMul((GainPotentialP/totalPotential), 100);
		var buildPP = (totalPotential == null || totalPotential== 0) ? 0 : accMul((BuidlPotentialP/totalPotential),100);
		var defendPP = (totalPotential == null || totalPotential== 0) ? 0 : accMul((DefendPotentialP/totalPotential), 100);
		var observePP = (totalPotential == null || totalPotential== 0) ? 0 : accMul((ObservePotentialP/totalPotential), 100);
		var maintainPP = (totalPotential == null || totalPotential== 0) ? 0 : accMul((MaintainPotentialP/totalPotential),100);
		var blankPP = (totalPotential == null || totalPotential== 0) ? 0 : accMul((BlankPotentialP/totalPotential),100);
		var thresholdPP = (totalPotential == null || totalPotential== 0) ? 0 : accMul((ThresholdPotentialP/totalPotential),100);

		//@jescobar: Calculate the adoption % for each segment and channel
		var gainAP = (totalAdoption == null || totalAdoption== 0) ? 0 : accMul((GainAdoptionP/totalAdoption), 100);
		var buildAP = (totalAdoption == null || totalAdoption== 0) ? 0 : accMul((BuidlAdoptionP/totalAdoption),100);
		var defendAP = (totalAdoption == null || totalAdoption== 0) ? 0 : accMul((DefendAdoptionP/totalAdoption), 100);
		var observeAP = (totalAdoption == null || totalAdoption== 0) ? 0 : accMul((ObserveAdoptionP/totalAdoption), 100);
		var maintainAP = (totalAdoption == null || totalAdoption== 0) ? 0 : accMul((MaintainAdoptionP/totalAdoption),100);
		var blankAP = (totalAdoption == null || totalAdoption== 0) ? 0 : accMul((BlankAdoptionP/totalAdoption),100);
		var thresholdAP = (totalAdoption == null || totalAdoption== 0) ? 0 : accMul((ThresholdAdoptionP/totalAdoption),100);

		//jescobar: Set data for Percent of potential by Segment
		$tr.find('td.GainclsPotential').text(Globalize.format(gainPP,'n1') +' %');
		$tr.find('td.BuildclsPotential').text(Globalize.format(buildPP,'n1') +' %');
		$tr.find('td.DefendclsPotential').text(Globalize.format(defendPP,'n1') +' %');
		$tr.find('td.ObserveclsPotential').text(Globalize.format(observePP,'n1') +' %');
		$tr.find('td.MaintainclsPotential').text(Globalize.format(maintainPP,'n1') +' %');
		$tr.find('td.BlankclsPotential').text(Globalize.format(blankPP,'n1') +' %');
		$tr.find('td.ThresholdclsPotential').text(Globalize.format(thresholdPP,'n1') +' %');

		if($("#matrixData li.ui-selected").length > 0) {
			$tr.find('td.NotSelectcclsPotential').text(Globalize.format(100 - (gainPP + buildPP + defendPP + observePP + maintainPP + blankPP),'n1') +' %');
		}else{
			$tr.find('td.NotSelectcclsPotential').text(Globalize.format(0,'n1') +' %');
		}

		//jescobar: Set data for Percent of adoption by Segment
		$tr.find('td.GainclsAdoption').text(Globalize.format(gainAP,'n1') +' %');
		$tr.find('td.BuildclsAdoption').text(Globalize.format(buildAP,'n1') +' %');
		$tr.find('td.DefendclsAdoption').text(Globalize.format(defendAP,'n1') +' %');
		$tr.find('td.ObserveclsAdoption').text(Globalize.format(observeAP,'n1') +' %');
		$tr.find('td.MaintainclsAdoption').text(Globalize.format(maintainAP,'n1') +' %');
		$tr.find('td.BlankclsAdoption').text(Globalize.format(blankAP,'n1') +' %');
		$tr.find('td.ThresholdclsAdoption').text(Globalize.format(thresholdAP,'n1') +' %');

		if($("#matrixData li.ui-selected").length > 0) {
			$tr.find('td.NotSelectcclsAdoption').text(Globalize.format(100 - (gainAP + buildAP + defendAP + observeAP + maintainAP + blankAP + thresholdAP),'n1') +' %');
		}else{
			$tr.find('td.NotSelectcclsAdoption').text(Globalize.format(0,'n1') +' %');
		}
	});

	var totalNotSelect = $.trim($("#matrixTabTotal tfoot tr").find(".totalNotSelect").text());
	totalNotSelect = totalNotSelect == '' ? 0 : Globalize.parseInt(totalNotSelect);

	var AvaBudcls =$.trim($("#matrixTabTotal tfoot").find('th input[name=totalUnits]').val());
	AvaBudcls = AvaBudcls == '' ? 0 : parseInt(AvaBudcls);

	var Plannedcls =$.trim($("#matrixTabTotal tfoot").find('.Plannedcls').text());
	Plannedcls = Plannedcls == '' ? 0 : Globalize.parseInt(Plannedcls);

	var totalNotSelect =$.trim($("#matrixTabTotal tfoot").find('th.NotSelectcls').text());
	totalNotSelect = totalNotSelect == '' ? 0 : Globalize.parseInt(totalNotSelect);
	var OtherMatriceclsU =$.trim($("#matrixTabTotal tfoot").find('th .OtherMatriceclsU').val());
	OtherMatriceclsU = OtherMatriceclsU == '' ? 0 : parseInt(OtherMatriceclsU);
	$("#matrixTabTotal tfoot .AvaBudcls").text(FormatNums(parseInt(AvaBudcls - Plannedcls - totalNotSelect - OtherMatriceclsU),thelocale,false,0));//TODO
}
