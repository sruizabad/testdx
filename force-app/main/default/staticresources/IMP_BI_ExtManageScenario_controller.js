function register_event(){	//all event
    //added by Peng 2013-06-07
    j$("#matriDivId table select[name$='theMTSelecrList']").each(function(){
    	j$(this).attr("title", j$("option[value=" + j$(this).val() + "]", j$(this)).text());

		if(!j$(getParent(j$(this).get(0), 'TABLE')).hasClass("finalMatrix"))
			populateColumnsForLaunchTemplate(j$(this), true);
    });

    //Begin: modified by Peng Zhu 2013-06-21 for copying the rows and columns from template
    j$("#matriDivId table select[name$='theMTSelecrList']").change(function(){
    	j$(this).attr("title", j$("option[value=" + j$(this).val() + "]", j$(this)).text());
    	populateColumnsForLaunchTemplate(j$(this), false);
    });

    //Set info matrix from matrix template already created
	//End: modified by Peng Zhu 2013-06-21

	j$('#specialDivId .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#specialDivId .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');

	j$('#AccMSDivBody .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#AccMSDivBody .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');
	j$('#specialDivBody .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#specialDivBody .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');
	j$('#SCRoleDivBody  .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#SCRoleDivBody  .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');
	j$('#DepTDivBody  .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#DepTDivBody  .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');

	var sortable_options = {containment : '#theTopDiv', cancel : '.ui-state-disabled', cursor : 'move'},
		droppable_options = {hoverClass : 'hoverTarget', activeClass : 'dropTarget', cursor : 'move'};

	var special_drop = j$('#specialDivId ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
    var ams_drop = j$('#AccMSDivBody ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
    var sm_drop = j$('#specialDivBody ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
    var cr_drop = j$('#SCRoleDivBody ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
    var dt_drop = j$('#DepTDivBody ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
	j$("div[id^='matrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
	});


		});
	j$("div[id^='matrix']").not(".finalMatrix").droppable({
        accept:"#specialDivId .draggable, .specialCls .matrixDraggable",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#specialDivId",
        drop : droppable_drop_fn
    });

    //new
	j$("div[id^='AMSmatrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
		});
	});


	j$("div[id^='AMSmatrix']").not(".finalMatrix").droppable({
        accept:"#AccMSDivBody .draggable, .AMSmatrixCls .matrixDraggable",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#AccMSDivBody",
        drop : droppable_drop_fn
    });
    //new
	j$("div[id^='Smatrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
		});
	});


	j$("div[id^='Smatrix']").not(".finalMatrix").droppable({
        accept:"#specialDivBody .draggable",//, .SmatrixCls .matrixDraggable
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#specialDivBody",
        drop : droppable_drop_fn
    });
    //new
	j$("div[id^='CRmatrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
		});
	});


	j$("div[id^='CRmatrix']").not(".finalMatrix").droppable({
        accept:"#SCRoleDivBody .draggable",//, CRmatrixCls .matrixDraggable
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#SCRoleDivBody",
        drop : droppable_drop_fn
    });
    //new
	j$("div[id^='DTmatrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
		});
	});


	j$("div[id^='DTmatrix']").not(".finalMatrix").droppable({
        accept:"#DepTDivBody .draggable",//, DTmatrixCls .matrixDraggable
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#DepTDivBody",
        drop : droppable_drop_fn
    });

	j$("#specialDivId").droppable({
        accept:".specialCls .matrixDraggable, .specialCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .specialCls",
        drop : droppable_drop_fn
    });
	j$("#AccMSDivBody").droppable({
        accept:".AMSmatrixCls .matrixDraggable, .AMSmatrixCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .AMSmatrixCls",
        drop : droppable_drop_fn
    });
	j$("#specialDivBody").droppable({
        accept:".SmatrixCls .matrixDraggable, .SmatrixCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .SmatrixCls",
        drop : droppable_drop_fn
    });
	j$("#SCRoleDivBody").droppable({
        accept:".CRmatrixCls .matrixDraggable, .CRmatrixCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .CRmatrixCls",
        drop : droppable_drop_fn
    });
	j$("#DepTDivBody").droppable({
        accept:".DTmatrixCls .matrixDraggable, .DTmatrixCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .DTmatrixCls",
        drop : droppable_drop_fn
    });

	j$('.matrixDraggable').not(".ui-state-disabled").off('mouseleave mouseenter mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	showTextHelp();
}



/**
* Set info for the matrix from any matrix template already created
*
* @param obj DOM object select list of matrix template
*/
function setMatrixTemplateInfo(obj){
  var j$this = j$(obj);
  var template = jsonMatrixTemplate[j$(obj).val()];
  var j$table = j$(getParent(j$this.get(0), 'TABLE'));
  var j$cycleTemplate = j$("select[name$='theMTSelecrList']", j$table);

  var j$potentialLabel = j$("input[name$='matrixPotentialLabel']", j$table);
  var j$adoptionLabel = j$("input[name$='matrixAdoptionLabel']", j$table);

  if(template){
    j$cycleTemplate.val(template.Lifecycle_Template_BI__c);
    populateColumnsForLaunchTemplate(j$cycleTemplate, false);

    setSpecialtiesFromTemplate(j$this.get(0),template);
    if(template.Potential_Data_Label_BI__c!=null && j$.trim(j$potentialLabel.val())=='') j$potentialLabel.val(template.Potential_Data_Label_BI__c);
    if(template.Adoption_Data_Label_BI__c!=null && j$.trim(j$adoptionLabel.val())=='') j$adoptionLabel.val(template.Adoption_Data_Label_BI__c);

    //var j$inputRow = j$("input[id^='theRowInput']", j$table);
    //var j$inputCol = j$("input[id^='theColumnInput']", j$table);
    //j$inputRow.val(template.Row_BI__c);
    //j$inputCol.val(template.Column_BI__c);
  }else{

  }
}

function e_over_draggable(e){
  var j$this = j$(this);

  switch(e.type){
    case 'mouseenter': j$this.removeClass('pgclass').addClass('pgover'); break;
    case 'mouseleave' : j$this.removeClass('pgover').addClass('pgclass');if(isdown){j$this.addClass('ui-selected');isdown=false;} break;
    case 'mousedown' :if(j$this.attr("class").indexOf('ui-state-disabled') < 0){if(e.ctrlKey){if(j$this.attr("class").indexOf('ui-selected') > -1){j$this.removeClass('ui-selected');}else{j$this.addClass('ui-selected');}}else{j$this.removeClass('pgover');isdown = true;}};break;
    case 'mouseup' : if(j$this.attr("class").indexOf('ui-state-disabled') < 0){if(!e.ctrlKey){if(j$this.attr("class").indexOf('ui-selected') == -1){j$(".ui-selected").removeClass('ui-selected');j$this.addClass('ui-selected');}else{j$(".ui-selected").removeClass('ui-selected');}isdown = false;}};break;
  }
}


function droppable_drop_fn(drag,drop){
	//console.log('In drop....');
	var j$dragCurrent = j$(drop.helper.context), conditionId,
	parentId = j$.trim(j$dragCurrent.parent().parent().attr('id'));
	var map_id_name = {};
	var selectedArr = j$dragCurrent.parent().find(".ui-selected");
	var ulli = j$(this).find("li");
	var map_matrx_sle = {"AccMSDivBody":"AMSmatrix","specialDivBody":"Smatrix","SCRoleDivBody":"CRmatrix","DepTDivBody":"DTmatrix"};
	w:for(var i = 0; i < selectedArr.length; i++) {
		var selectLi = selectedArr[i];
		if(selectLi.id && selectLi.id.indexOf('\,') < 0 && !map_id_name[selectLi.id]){
			if(map_matrx_sle[parentId]){
				for(var j = 0; j < ulli.length; j++) {
					var li = j$(ulli[j]);
					if(li.attr("id") == selectLi.id) {continue w;};
				}
			}
			map_id_name[selectLi.id] = selectLi.id;
			j$dragCurrent = j$(selectLi);
			currentId = j$.trim(j$dragCurrent.attr('id'));
			if(typeof(parentId) != 'undefined' && j$.trim(parentId) != '' && (parentId.indexOf('matrix') > -1)){
				conditionId = 'matrixDivId';
			}
			else{
				conditionId = parentId;
			}


			var targetId = j$(drag.target).attr('id');
			 //console.log('Current : ' + j$.trim(j$dragCurrent.parent().parent().attr('id')));

			if((targetId.indexOf('matrix') > -1) && (parentId.indexOf('matrix') > -1)){
				conditionId = 'horizontalMove';
			}

			if(typeof(targetId) != 'undefined' && typeof(parentId) != 'undefined' && j$.trim(targetId) != '' && j$.trim(parentId) != '' && targetId != parentId){
				 //console.log('In cal....',conditionId);
				switch(conditionId){
					case 'horizontalMove' :
          var hasFinded = false;
          var j$dragTarget, targetId;

          j$(drag.target).find('li').each(function(){
            if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
              j$dragTarget = j$(this), targetId = j$(this).attr('id');
              return false;
            }
          });
          j$(drag.target).find('li').each(function(){
            if (j$(this).attr('id') == currentId) {
              hasFinded = true;
            }
          });

          if (hasFinded) {
            break;
          }
          //if has not find any available li, do nothing
          if(typeof(j$dragTarget) != 'undefined'){
            j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.text()})
              .addClass('pgclass').html(j$dragCurrent.text()).removeClass('ui-state-disabled');


            j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
          }
          //Begin: added by Peng Zhu 2013-08-30
          else{
            j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable  pgclass" name="'+j$dragCurrent.text()+'">' + j$dragCurrent.text() + '</li>');
            j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');

            j$(drag.target).find('ul').append(j$dragTarget);
          }
          //End: added by Peng Zhu 2013-08-30
          //j$dragTarget.off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
          break;
					case 'matrixDivId' :
						var duplicateCounter = 0;
		    			//j$("div[name='matrixSpecial'] li").each(function(){
						//	var liId = j$(this).attr('Id');
			       		//	if(liId && liId == currentId) duplicateCounter ++;
			    		//});

						//console.log('duplicateCounter : ' + duplicateCounter);
						//console.log('currentId : ' + currentId);
						var j$lis = j$(drag.target).find('li');
			        	j$lis.each(function(){
			        		if(currentId == j$(this).attr('id')){
			        			if(duplicateCounter < 2){
			        				j$(this).removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
				        			j$(this).off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
			        			}
			        			j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
			        		}
			        	});
						break;

					case 'specialDivId' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

            j$(drag.target).find('li').each(function(){
              if (j$(this).attr('id') == currentId) {
                hasFinded = true;
              }
            });
            if (hasFinded) {
              //j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              break;
            }

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;

					case 'AccMSDivBody' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;

					case 'specialDivBody' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});//@jescobar: disable specialties pattern2 matrix
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});//@jescobar: disable specialties pattern2 matrix
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;

					case 'SCRoleDivBody' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;

					case 'DepTDivBody' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;
					default :  break;
				}
			}
			checkAllMatrixSpecialDiv();
		}
	}
}
/*
function e_over_draggable(e){
var j$this = j$(this);
case 'mouseenter': j$this.addClass('pgover');
switch(e.type){break;
default : j$this.removeClass('pgover'); break;
}
}


function droppable_drop_fn(drag,drop){
//console.log('In drop....');
var j$dragCurrent = j$(drop.helper.context), conditionId,
currentId = j$.trim(j$dragCurrent.attr('id')),
parentId = j$.trim(j$dragCurrent.parent().parent().attr('id'));

if(typeof(parentId) != 'undefined' && j$.trim(parentId) != '' && (parentId.indexOf('matrix') > -1)){
conditionId = 'matrixDivId';
}
else{
conditionId = parentId;
}


var targetId = j$(drag.target).attr('id');
//console.log('Current : ' + j$.trim(j$dragCurrent.parent().parent().attr('id')));
//console.log('targetId : ' + targetId);

if((targetId.indexOf('matrix') > -1) && (parentId.indexOf('matrix') > -1)){
conditionId = 'horizontalMove';
}
//console.log(conditionId);
if(typeof(targetId) != 'undefined' && typeof(parentId) != 'undefined' && j$.trim(targetId) != '' && j$.trim(parentId) != '' && targetId != parentId){
//console.log('In cal....');
switch(conditionId){
case 'horizontalMove' :
//console.log('horizontalMove');
var hasFinded = false;
var j$dragTarget, targetId;

j$(drag.target).find('li').each(function(){
if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){  //cleart old channel
if(!hasFinded){
hasFinded = true;
j$dragTarget = j$(this), targetId = j$(this).attr('id');
}
}
});

//if has not find any available li, do nothing
if(typeof(j$dragTarget) != 'undefined'){
j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.text()})
.addClass('pgclass').addClass('ui-selected').html(j$dragCurrent.text()).removeClass('ui-state-disabled');


j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
}
//Begin: added by Peng Zhu 2013-08-30
else{
j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgover" name="'+j$dragCurrent.text()+'">' + j$dragCurrent.text() + '</li>');
j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');

j$(drag.target).find('ul').append(j$dragTarget);
}
//End: added by Peng Zhu 2013-08-30
break;

case 'matrixDivId' :
var duplicateCounter = 0;
j$("div[name='matrixSpecial'] li").each(function(){
var liId = j$(this).attr('Id');
if(liId && liId == currentId) duplicateCounter ++;
});

//console.log('duplicateCounter : ' + duplicateCounter);
//console.log('currentId : ' + currentId);
var j$lis = j$(drag.target).find('li');
j$lis.each(function(){
if(currentId == j$(this).attr('id')){
if(duplicateCounter < 2){
j$(this).removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
j$(this).off('mouseenter mouseleave').on('mouseleave mouseenter', e_over_draggable);
}
j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
}
});
break;

case 'specialDivId' :
//find a available li -- id is null
var hasFinded = false;
var j$dragTarget, targetId;
var j$divTarget = j$(drag.target).find('li.pgover');


//added by Peng Zhu 2013-06-25 for overflowHidden
var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
if(j$div.hasClass('overflowHidden')){
j$div.removeClass('overflowHidden');
}

j$(drag.target).find('li').each(function(){
if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){  //cleart old channel
if(!hasFinded){
hasFinded = true;
j$dragTarget = j$(this), targetId = j$(this).attr('id');
}
}
});
//if has not find any available li, do nothing
if(typeof(j$dragTarget) != 'undefined'){
j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')})
.addClass('pgover').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled');
j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave');
}
//Begin: added by Peng Zhu 2013-08-30
else{
j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgover" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>');
j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave');
j$(drag.target).find('ul').append(j$dragTarget);
}
//End: added by Peng Zhu 2013-08-30

break;
default :  break;
}
}
checkAllMatrixSpecialDiv();
}
*/
function removeMatrix(){
  j$("#matriDivId table:last-child").remove();
  var matrixIndex = parseInt(j$('#matrixIndex').val());
  if(matrixIndex > 0){
    j$('#matrixIndex').val(matrixIndex - 1);
  }
}

/**
* collect the matrixs values on page
* return : JSON Data
*/
function collectMatrixValues(toCalculate){
  var counter = 0, matrix = {}, arr = [], mtIdArr = [], sArrAll = [];

  j$("#matriDivId table").each(function(){

    var matrixEditId = j$(this).attr('id');
    if(matrixEditId && j$.trim(matrixEditId) != ''){
      matrixEditId = matrixEditId.split('-')[1];
    }
    //var isCopy = j$(this).data('iscopy');
    //var oringinalId = j$(this).data('orginid');
    //var scennum = j$(this).data('scennum');

    var isCopy = j$(this).attr('data-iscopy') == 'true' ? true : false;
    var oringinalId = j$(this).attr('data-orginid');
    var scennum = j$(this).attr('data-scennum');


    var isCurrent = j$(this).find("[id$='currentCheckbox']").is(':checked') ? true : false;
    var matrixTemplateId = j$("select[name$='matrixTemplate']", j$(this)).val();
    var name = j$("input[name='matrixName']", j$(this)).val();
    var scenDes = j$('#scenarioDes', j$(this)).val();
    //var dpa = false;
    //console.log(dpa);
    //console.log(j$("input[id$='matrixDPA']", j$(this)).get(0).checked);
    /*if(j$("input[id$='matrixDPA']", j$(this)).get(0).checked){
    dpa = true;
  } */
  var matrixphase = j$(this).find("[id$='matrixLaunchPhase']").is(':checked') ? true : false;
  var allCustomers = false;
  if(j$("input[id$='matrixAllCustomers']", j$(this)) && j$("input[id$='matrixAllCustomers']", j$(this)).get(0).checked){
    allCustomers = true;
  }
  var matrixStatus = j$(this).find("input[name$='matrixStatus']").val();
  var sMtId = j$("select[name$='theMTSelecrList']", j$(this)).val();
  if(typeof(sMtId) != 'undefined' && j$.trim(sMtId) != ''){
    mtIdArr.push(sMtId);
  }
  var adoptionLabel = j$("input[id$='matrixAdoptionLabel']", j$(this)).val();
  var potentialLabel = j$("input[id$='matrixPotentialLabel']", j$(this)).val();
  adoptionLabel = (adoptionLabel == null || adoptionLabel == '') ? 'Prod Rx' : adoptionLabel;
  potentialLabel = (potentialLabel == null || potentialLabel == '') ? 'Market Rx' : potentialLabel;
  var needCalculate = (toCalculate) ? j$(this).find("#checkCalculation").get(0).checked : false;
  if (matrixStatus != 'Draft') {
    needCalculate = false;
  }

  var special = '';
  var ams = '';
  var sm = '';
  var crm = '';
  var dtm = '';
  var sArr = [];

  //************************special*******************************//
  if(!(accountMatrix && AccountMatrixTypeValue == 'HCO only')){
    j$("div[name='matrixSpecial'] li", j$(this)).each(function(){
      var temp = j$.trim(j$(this).text());
      if(typeof(temp) != 'undefined' && temp != ''){
        special += temp + ';';
      }

      var sId= j$(this).attr('id');
      if(accountMatrix)
        sId = j$(this).attr('name');

      if(sId && j$.trim(sId) != ''){
        sArr.push(j$.trim(sId));
        sArrAll.push(j$.trim(sId));
      }
    });
    special = j$.trim(special);
    if(typeof(special) != 'undefined' && special != ''){
      special = special.substring(0, special.length - 1);
    }
  }else{
    //************************special*******************************//

    //************************ams*******************************//

    j$("div[name='matrixAMS'] li", j$(this)).each(function(){
      var temp = j$.trim(j$(this).text());
      if(typeof(temp) != 'undefined' && temp != ''){
        ams += temp + ';';
      }
    });

    ams = j$.trim(ams);
    if(typeof(ams) != 'undefined' && ams != ''){
      ams = ams.substring(0, ams.length - 1);
    }
    //************************ams*******************************//

    //************************sm*******************************//

    j$("div[name='matrixSm'] li", j$(this)).each(function(){
      var temp = j$.trim(j$(this).text());
      if(typeof(temp) != 'undefined' && temp != ''){
        sm += temp + ';';
      }

      var sId= j$(this).attr('id');
      //else sid = j$(this).attr('id');
      //console.log( j$(this).textContent );
      //console.log( sid );
      if(sId && j$.trim(sId) != ''){
        sArr.push(j$.trim(sId));
        sArrAll.push(j$.trim(sId));
      }
    });

    sm = j$.trim(sm);
    if(typeof(sm) != 'undefined' && sm != ''){
      sm = sm.substring(0, sm.length - 1);
    }
    //************************sm*******************************//

    //************************crm*******************************//

    j$("div[name='matrixCR'] li", j$(this)).each(function(){
      var temp = j$.trim(j$(this).text());
      if(typeof(temp) != 'undefined' && temp != ''){
        crm += temp + ';';
      }
    });

    crm = j$.trim(crm);
    if(typeof(crm) != 'undefined' && crm != ''){
      crm = crm.substring(0, crm.length - 1);
    }
    //************************crm*******************************//

    //************************dtm*******************************//

    j$("div[name='matrixDT'] li", j$(this)).each(function(){
      var temp = j$.trim(j$(this).text());
      if(typeof(temp) != 'undefined' && temp != ''){
        dtm += temp + ';';
      }
    });

    dtm = j$.trim(dtm);
    if(typeof(dtm) != 'undefined' && dtm != ''){
      dtm = dtm.substring(0, dtm.length - 1);
    }
  }
  //************************dtm*******************************//
  var row = j$("input[id^='theRowInput']", j$(this)).val();
  if(typeof(row) != 'undefined' && j$.trim(row) != ''){
    row = parseInt(row);
  }
  else{
    row = 0;
  }
  var column = j$("input[id^='theColumnInput']", j$(this)).val();
  if(typeof(column) != 'undefined' && j$.trim(column) != ''){
    column = parseInt(column);
  }
  else{
    column = 0;
  }

  if(typeof(name) != 'undefined' && name != ''){
    var cm = {};

    if(matrixEditId && j$.trim(matrixEditId) != '') cm.mid = matrixEditId;
    cm.name = name;
    cm.special = special;
    cm.ams = ams;
    cm.sm = sm;
    cm.crm = crm;
    cm.dtm = dtm;
    cm.tid = sMtId;
    cm.row = row;
    cm.column = column;
    cm.set_sId = sArr;
    console.log('ams: ', ams);
    console.log('sm: ', sm);
    console.log('crm: ', crm);
    console.log('dtm: ', dtm);
    console.log('sArr: ', sArr);
    //cm.dpa = dpa;
    cm.allCustomers = allCustomers;
    cm.adoptionLabel = adoptionLabel;
    cm.potentialLabel = potentialLabel;
    //cm.toCalculate = isCalculated;
    cm.matrixTemplateId = (typeof(matrixTemplateId) != 'undefined' && j$.trim(matrixTemplateId) != '') ? matrixTemplateId : null;
    cm.isCopy = isCopy;
    cm.oringinalId = oringinalId;
    cm.scennum = scennum;
    cm.isCurrent = isCurrent;
    cm.scenDes = scenDes;
    cm.needCalculate = needCalculate;
    cm.cellId = j$(this).attr('data-cellid');

    arr.push(cm);
  }
});

matrix.productId = j$("select[id$='theProductList']").val();
matrix.cid = cycleId;
matrix.list_cm = arr;
matrix.set_mtIds = mtIdArr;
matrix.set_sId = sArrAll;
matrix.countryId = countryId;
matrix.countryCode = countryCode;
matrix.countryCodeRegion = countryCodeRegion;
matrix.AccountMatrixTypeValue = AccountMatrixTypeValue;
matrix.accountMatrix = accountMatrix;
matrix.accountMatrixtype = (matrix.accountMatrix == true) ? AccountMatrixTypeValue : null;
matrix.firstScenId = j$("[id$='firstScen']").val();
//console.log('sArrAll:' + sArrAll);
return JSON.stringify(matrix);
}

function showTextHelp(){
  j$(function() {
    j$("img[class^='budgetHelpText']").hover(
      function () {
        j$(this).removeClass("budgetHelpText").addClass("budgetHelpTextOn");
      },
      function () {
        j$(this).removeClass("budgetHelpTextOn").addClass("budgetHelpText");
      }
    );
  });
}



function addMatrix(that){
  //TODO: get matrix index
  if (parseInt(j$('#matrixIndex').val()) == 5) {
    alert('There are already 5 matrix in this scenario!');
    return false;
  }
  j$('#loading-curtain-div').show();
  if (confirm('A matrix will be copied. Are you sure?')) {
    var $table = j$(that).closest('table');
    var copyId = $table.attr('id').split('-')[1];
    var cellId = $table.data('cellid') + '';



    IMP_BI_ExtManageScenarios.copyMatrice(copyId,cellId, function(data){
      j$('#htmlEntity').html(data);
      data = j$('#htmlEntity').text();
      data = j$.parseJSON( data );

      if(data && !data.success){
        j$("div[id='errorMsg']").html(data.message);
        j$("div[id='errorMsg']").show();
        j$('#loading-curtain-div').hide();
      }
      else{
        var matrixIndex = parseInt(j$('#matrixIndex').val()) + 1;
        j$("#needBatch").val('yes');
        //create table




        //console.log($table.find('[id$="matrixTemplate"]').val());

        var tableStr = '<table style="display:inline-block;" class="matrixInnerTable" id="mit-' + data.matrixId +'" data-scennum="'+matrixIndex+'" data-iscopy="true" data-orginid="'+data.matrixId+'" data-cellid="'+data.matrixCellId+'" data-iscal="'+$table.data('iscal')+'">';
        tableStr += $table.html() + '</table>';
        j$('#matriDivId').append(tableStr);

        var $newTable = j$('#mit-'+data.matrixId);

        $newTable.find('#matriceName').val($table.find('#matriceName').val());
        $newTable.find('#checkCalculation').get(0).checked = $table.find('#checkCalculation').get(0).checked;
        $newTable.find('[id$="matrixTemplate"]').val($table.find('[id$="matrixTemplate"]').val());
        $newTable.find('#scenarioDes').val($table.find('#scenarioDes').val());
        //$newTable.find('#matrixDPA').get(0).checked = $table.find('#matrixDPA').get(0).checked;
        $newTable.find('#matrixLaunchPhase').get(0).checked = $table.find('#matrixLaunchPhase').get(0).checked;
        $newTable.find('#matrixAllCustomers').get(0).checked = $table.find('#matrixAllCustomers').get(0).checked;
        $newTable.find('[id$="theMTSelecrList"]').val($table.find('[id$="theMTSelecrList"]').val());
        $newTable.find('#matrixPotentialLabel').val($table.find('#matrixPotentialLabel').val());
        $newTable.find('#matrixAdoptionLabel').val($table.find('#matrixAdoptionLabel').val());
        $newTable.find('[id^="theRowInput"]').val($table.find('[id^="theRowInput"]').val());
        $newTable.find('[id^="theColumnInput"]').val($table.find('[id^="theColumnInput"]').val());

        //console.log($newTable);



        $newTable.find('.tableScenTitle').text('Scenario ' + matrixIndex);
        $newTable.find('[name="matrixSpecial"]').attr('id', 'matrix'+matrixIndex);
        $newTable.find("[id$='currentCheckbox']").get(0).checked = false;
        var cuCheck = $newTable.find("[id$='currentCheckbox']").get(0);
        cuCheck.checked = false;
        $newTable.find("[id$='currentCheckbox']").prop('checked', false);
        $newTable.attr('id', 'mit-' + data.matrixId);
        /*
        var mydata = $newTable.data();
        mydata.scennum = matrixIndex;
        mydata.iscopy = true;
        mydata.orginid = data.matrixId;
        mydata.cellid = data.matrixCellId;
        */
        $newTable.attr('data-scennum', matrixIndex);
        $newTable.attr('data-iscopy', true);
        $newTable.attr('data-orginid', data.matrixId);
        $newTable.attr('data-cellid', data.matrixCellId);

        $newTable.get(0).dataset.scennum = matrixIndex;
        $newTable.get(0).dataset.iscopy = true;
        $newTable.get(0).dataset.orginid = data.matrixId;
        $newTable.get(0).dataset.cellid = data.matrixCellId;
        //$newTable.data('iscopy', true);
        //$newTable.data('orginid', data.matrixId);
        //$newTable.data('cellid', data.matrixCellId);



        //console.log($newTable.html());



        //append table to page


        var table2 = j$('#matriDivId').find('#mit-' + data.matrixId);


        //table2.remove();

        //console.log(j$('#' + 'mit-' + data.matrixId));
        j$('#' + 'mit-' + data.matrixId).find("[id$='currentCheckbox']").get(0).checked = false;
        j$('#' + 'mit-' + data.matrixId).find("[id$='currentCheckbox']").prop('checked', false);
        j$('#' + 'mit-' + data.matrixId).find("[id$='currentCheckbox']").data('iscu',false);
        j$('#matrixIndex').val(matrixIndex);
        //checkMatrix();
        goThroughAllCalCheck();
        register_event();
        j$('.matrixDraggable').not(".ui-state-disabled").off('mouseleave mouseenter mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
        j$('#loading-curtain-div').hide();
      }
    });

  } else {
    j$('#loading-curtain-div').hide();
  }

}


function matrix_stop_fn(event, ui){
  var parentUl = j$(this);
  var counter = 0;
  // //console.log(parentUl.attr('class'));
  // //console.log('tagName : ' + j$(this).get(0).tagName);

  var tagName = j$(this).get(0).tagName;
  if(tagName == 'DIV'){
    parentUl = j$("ul", j$(this));
  }


  j$("li", parentUl).each(function(){
    if(!j$(this).text().trim()){
      j$(this).remove();
      counter ++;
    }
  });

  for(var i = 0; i < counter; i++){
    parentUl.append('<li id="" class="matrixDraggable ui-state-disabled"></li>');
  }

  checkAllMatrixSpecialDiv();
}

//Begin: added by Peng Zhu
function matrix_sort_start_fn(event, ui){
  var j$this = j$(this);
  //console.log('tagName : ' + j$this.get(0).tagName);
  var j$div = j$(getParent(j$this.get(0), 'DIV'));
  if(!j$div.hasClass('overflowHidden')){
    //console.log('dk');
    j$div.addClass('overflowHidden');
  }
}

function removeGivenMatrix(obj){
  if (confirm('A matrix will be deleted. Are you sure?')) {
    var $table = j$(obj).closest('table');
    var objId = $table.attr('id').replace('mit-','');
    //var objScen = $table.data('scennum');
    var objScen = $table.attr('data-scennum');//jquery data doesn't work for change
    //console.log($table.data('scennum'));
    //console.log($table.attr('data-scennum'));
    var removed = j$('#removedMatrixId').val();
    if (matrixUsedId.indexOf(objId) > -1){
      alert('This matrix is used in portfolio. It is not allowed to delete!');
      j$('#loading-curtain-div').hide();
      return false;
    }


    var onlyScen1 = false;
    if (objScen == '1') {
      if (j$('#matriDivId').find('table').length == 1) {
        onlyScen1 = true;
      } else {
        alert('It is not allowed to delete the first scenario matrice!');
        j$('#loading-curtain-div').hide();
        return false;
      }
    }
    j$('#loading-curtain-div').show();
    if (objId != '') {

      if ($table.find("[id$='currentCheckbox']").is(":checked")) {
        if (objScen != j$('#matrixIndex').val()) {
          alert('It is not allowed to delete the current matrice, if there are other matrix existing!');
          j$('#loading-curtain-div').hide();
          return false;
        }
      }




      IMP_BI_ExtManageScenarios.deleteMatrix(objId, function(data){
        j$('#htmlEntity').html(data);
        data = j$('#htmlEntity').text();
        data = j$.parseJSON( data );

        if(data && !data.success){
          j$("div[id='errorMsg']").html(data.message);
          j$("div[id='errorMsg']").show();
          j$('#loading-curtain-div').hide();
        }
        else{
          if ($table.data('iscopy') != 'true') {
            j$("#needBatch").val('yes');
          }
          if (removed) {
            if (removed.indexOf(objId) == -1) {
              j$('#removedMatrixId').val(removed + ';' + objId);
            }
          } else {
            j$('#removedMatrixId').val(objId);
          }
          if (!onlyScen1) {
            $table.remove();

            var matrixIndex = parseInt(j$('#matrixIndex').val()) - 1;
            j$('#matrixIndex').val(matrixIndex);

            refreshMatrixIndex();
            goThroughAllCalCheck();
            j$('#loading-curtain-div').hide();
          } else {
            unBindBeforeunload();
            window.location.href = '/' + cycleId;
          }
        }
      });

    }
  }
}

function matrix_stop_fn_void(){}

function refreshMatrixIndex(){
  var matrixIndex = 0;
  j$("#matriDivId table").each(function(){
    matrixIndex ++;
    j$(this).attr('data-scennum',matrixIndex);
    j$(this).find('.tableScenTitle').text('Scenario ' + matrixIndex);
  });
}

/**
* reset the div "#matrixDivId" width when brower size change
*
@author  Peng Zhu
@created 2013-06-21
@version 1.0
*
@changelog
* 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
* - Created
*/
function resetMatriDivIdSize(){
  //#matriDivId
  var windowWidth = 0;

  if (window.innerWidth)
  windowWidth = window.innerWidth;
  else if ((document.body) && (document.body.clientWidth))
  windowWidth = document.body.clientWidth;
  else windowWidth = window.screen.width;

  if(windowWidth < 800) windowWidth = 800;

  var mainDivWidth = windowWidth - j$("#matriDivId").offset().left -80;//

  j$('#matriDivId').css("max-width", mainDivWidth);
  j$('#specialDivId').css("max-width", mainDivWidth);
}

//** assist function to find element's parent with specified tag name **//
function getParent(el,p){
  do{
    el = el.parentNode;
  }while(el && el.nodeName != p)
  return el;
}

/**
* This function is used to validate and save all the matrix info
*
@author  Peng Zhu
@created 2013-06-21
@version 1.0
*
@changelog
* 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
* - Created
*/
function savaMatrixsInPage(){
  showLoading();
  if(checkAllRequiredFields(true)){
    var delId = j$("#removedMatrixId").val();
    if (!delId) {
      delId = '';
    }
    j$("#isSave").val('yes');
    IMP_BI_ExtManageScenarios.saveMatrixData(collectMatrixValues(false), function(data){
      j$('#htmlEntity').html(data);
      data = j$('#htmlEntity').text();
      data = j$.parseJSON( data );

      if(data && !data.success){
        j$("div[id='errorMsg']").html(data.message);
        j$("div[id='errorMsg']").show();
        j$('#loading-curtain-div').hide();
      }
      else{
        if (j$("#needBatch").val() == 'yes') {
          j$("[id$='scenCell']").val(data.scenCell);
          callBatch();
        } else {
          unBindBeforeunload();
          cancel();

        }

        //cancel();
      }
    });
  }else{
    j$('#loading-curtain-div').hide();
  }
}

/**
* This function is used to validate, save all the matrix info and then invoke the calculate matrix function in heroku
*
@author  Peng Zhu
@created 2013-06-21
@version 1.0
*
@changelog
* 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
* - Created
*
* 2014-04-01 Jefferson Escobar <jescobar@omegacrmconsulting.com>
* - Modified
*
*/
function savaAndCalMatrixsInPage(){
  showLoading();
  if(checkAllRequiredFields(true)){

    //Chekc if there are not matrices to calculate
    var qMatrices = 0;
    j$("#matriDivId table").each(function(){
      if(!j$(this).data('iscal') && j$(this).find("[id$='checkCalculation']").get(0).checked){
        qMatrices++;
      }
    });
    var delId = j$("#removedMatrixId").val();
    if (!delId) {
      delId = '';
    }
    if(qMatrices == 0){
      if(confirm(msgMatrix2CalculateWarn)){
        IMP_BI_ExtManageScenarios.saveMatrixData(collectMatrixValues(true), function(data){
          j$('#htmlEntity').html(data);
          data = j$('#htmlEntity').text();
          data = j$.parseJSON( data );

          if(data && !data.success){
            j$("div[id='errorMsg']").html(data.message);
            j$("div[id='errorMsg']").show();
            j$('#loading-curtain-div').hide();
          }
          else{
            if (j$("#needBatch").val() == 'yes') {
              j$("[id$='scenCell']").val(data.scenCell);
              callBatch2();
            } else {
              cancel();
            }

            //cancel();
          }
        });
      }else{
        j$('#loading-curtain-div').hide();
      }
    }else{
      IMP_BI_ExtManageScenarios.saveMatrixData(collectMatrixValues(true), function(data){
        j$('#htmlEntity').html(data);
        data = j$('#htmlEntity').text();
        data = j$.parseJSON( data );

        if(data && !data.success){
          j$("div[id='errorMsg']").html(data.message);
          j$("div[id='errorMsg']").show();
          j$('#loading-curtain-div').hide();
        }
        else{
          if (j$("#needBatch").val() == 'yes') {
            j$("[id$='scenCell']").val(data.scenCell);
            callBatch();
          } else {
            //cancel();
            checkWhetherCalculate();
          }

        }
      });
    }
  }else{
    j$('#loading-curtain-div').hide();
  }
}


/**
* This function is used to populate columns value for Launch template
*
@author  Peng Zhu
@created 2013-06-21
@version 1.0
*
@changelog
* 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
* - Created
*/
function populateColumnsForLaunchTemplate(obj, isInit){
  //console.log('isInit : ' + isInit);
  var j$this = j$(obj);

  var jsonMTObj = jsonMTofLaunch[j$this.val()];
  var j$table = j$(getParent(j$this.get(0), 'TABLE'));
  var j$inputRow = j$("input[id^='theRowInput']", j$table);
  var j$inputColumn = j$("input[id^='theColumnInput']", j$table);
  var j$inputLaunch = j$("input[id^='matrixLaunchPhase']", j$table);
  //var j$inputDPA = j$("input[name$='matrixDPA']", j$table);
  var j$inputAllCust = j$("input[name$='matrixAllCustomers']", j$table);

  if(jsonMTObj){
    //console.log('lctId : ' + jsonMTObj.lctId + ', isLaunch : ' + jsonMTObj.isLaunch + ', Row : ' + jsonMTObj.lctRow + ', Column : ' + jsonMTObj.lctColumn);
    //j$inputRow.val(jsonMTObj.lctRow);
    //j$inputColumn.val(jsonMTObj.lctColumn);
    if(!isInit || j$.trim(j$inputRow.val()) == '') j$inputRow.val(jsonMTObj.lctRow);
    if(!isInit || j$.trim(j$inputColumn.val()) == '') j$inputColumn.val(jsonMTObj.lctColumn);

    //j$inputRow.val(jsonMTObj.lctRow);
    //j$inputColumn.val(jsonMTObj.lctColumn);

    if(jsonMTObj.isLaunch){
      //j$inputColumn.val(jsonMTObj.lctColumn);
      //j$inputRow.attr('disabled', 'disabled');
      //j$inputColumn.attr('disabled', 'disabled');
      j$inputColumn.attr('title', errMsg_Launch);
      j$inputLaunch.prop('checked',true);
      //j$inputDPA.prop('checked',false);
      //j$inputDPA.attr('disabled', 'disabled');
      j$inputAllCust.removeAttr('disabled');
    }
    else{
      //  j$inputRow.removeAttr('disabled');
      //j$inputColumn.removeAttr('disabled');
      j$inputColumn.removeAttr('title');
      j$inputLaunch.prop('checked',false);
      //j$inputDPA.removeAttr('disabled');
    }
  }
  else{
    //  j$inputRow.removeAttr('disabled');
    //j$inputColumn.removeAttr('disabled');
    j$inputColumn.removeAttr('title');
  }

  hideErrMsgWhenChangeMt(j$this.get(0));
}

/**
* This function is used to check if each matrix speciality has at least one value
*
@author  Peng Zhu
@created 2013-06-21
@version 1.0
*
@changelog
* 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
* - Created
*/
function checkAllMatrixSpecialDiv(){
  j$("div[name='matrixSpecial']").not(".finalMatrix").each(function(){
    var j$this = j$(this);
    var isNull = true;

    j$("li", j$this).each(function(){
      var liId = j$(this).attr('id');
      if(liId && j$.trim(liId) != '' ){ isNull = false;}
    });

    //get DPA checkbox value
    /*
    var j$table = j$(getParent(j$this.get(0), 'TABLE'));
    var dpa = j$table.find("input[name$='matrixDPA']").get(0);
    if(dpa && dpa.checked){
    isNull = false;
  }
  */
  if( j$("[id$='AccountMatrixCK']").prop('checked') ) isNull = false;//added by leijun

  if(isNull){
    j$this.siblings('.erMsg').find('font').text(errMsg_Special).parent().show();
    j$this.children('.erMsg').find('font').text(errMsg_Special).parent().show();
  }else{
    j$this.siblings('.erMsg').hide();
    j$this.children('.erMsg').hide();
  }
});
}

function checkDpaForAllCustomer(obj){
  if(obj){
    var j$allCustomers = j$(getParent(obj, 'TBODY')).find('input[name$=matrixAllCustomers]');
    var j$lauchPhase = j$(getParent(obj, 'TBODY')).find("input[id^='matrixLaunchPhase']");
    var j$divCurrSpecial = j$(getParent(obj, 'TBODY')).find('div.specialCls');
    var j$selectMT = j$(getParent(obj, 'TBODY')).find('select[name$=matrixTemplate]');

    if(obj.checked){

      j$("li.pgover", j$divCurrSpecial).each(function(){
        var duplicateCounter = 0, currentId = j$(this).attr('id'), j$this = j$(this);

        //j$("div[name='matrixSpecial'] li").each(function(){
        //  var liId = j$(this).attr('Id');
        //  if(liId && liId == currentId) duplicateCounter ++;
        //});


        var j$lis = j$("#specialDivId").find('li');
        j$lis.each(function(){
          if(currentId == j$(this).attr('id')){
            if(duplicateCounter < 2){
              j$(this).removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
              j$(this).off('mouseenter mouseleave').on('mouseleave mouseenter', e_over_draggable);
            }
            j$this.attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
          }
        });
      });

      //j$allCustomers.removeAttr('checked').attr('disabled', 'disabled');
      //j$lauchPhase.prop('checked',false);
      //j$divCurrSpecial.droppable({ disabled: true });//Set disabled droppable div
      j$selectMT.attr('disabled', 'disabled');
      j$selectMT.val('');
    }
    else{
      //j$allCustomers.removeAttr('disabled');
      //j$divCurrSpecial.droppable({ disabled: false });//Set enable droppable div
      j$selectMT.removeAttr('disabled', 'disabled').val('');
    }
  }
}


/**
* Check all the matrices for being calculated
* @param obj is checked or isn't for checking all the matrices in the page
* @return
*/
function checkAllMatrices(obj){
  if(obj && obj.checked){//Checked all matrices
    j$("input[name='matrixChecked']").each(function(){
      j$(this).prop('checked',true);
    });
  }else{//Unchecked all matrices
    j$("input[name='matrixChecked']").each(function(){
      j$(this).prop('checked',false);
    });
  }
}


/**
* Fill out specialties on matrix from matrix template selected
*
* @param obj Picklist matrix template
* @param template info record selected
*/
function setSpecialtiesFromTemplate(obj,template){
  var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.specialCls'), targetId, j$dragTarget, j$dragCurrent;
  var specialties  = (template.Specialty_Ids_BI__c!=null&&j$.trim(template.Specialty_Ids_BI__c)!='') ? template.Specialty_Ids_BI__c : null;
  var isIncluded = false;
  var spAllIncluded = 0;

  if(specialties){
    //Check if there are specialties inclued in the matrix definition
    j$divTarget.find('li').each(function(){
      var spId = j$(this).attr('Id');

      if(j$.trim(spId) != '' && specialties.indexOf(spId) > -1){
        alert(msgSpecialtiesIncluded);
        isIncluded = true;
        return false;
        //console.log(':: Specilaty: ' + j$(this).html());
      }
    });

    j$("li:not(.special-state-disabled)", j$("#specialDivId")).each(function(){
      j$dragCurrent = j$(this);
      var currentId = j$dragCurrent.attr('id');
      var hasFinded = false;
      var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
      var countSPIncluded = 0;

      if(specialties.indexOf(currentId) > -1){
        //console.log(':: Specialty: ' + currentId);
        if(j$div.hasClass('overflowHidden')){
          j$div.removeClass('overflowHidden');
        }

        j$divTarget.find('li').each(function(){
          if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){  //cleart old channel
            if(!hasFinded){
              hasFinded = true;
              j$dragTarget = j$(this), targetId = j$(this).attr('id');
            }
          }
        });
        //if has not find any available li, do nothing
        if(typeof(j$dragTarget) != 'undefined'){
          j$dragTarget.attr({'id': currentId})
          .addClass('pgover').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled');
          j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
          j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave');
        }
        //Begin: added by Peng Zhu 2013-08-30
        else{
          j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgover">' + j$dragCurrent.find('span').attr('title') + '</li>');
          j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
          j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave');
          j$(drag.target).find('ul').append(j$dragTarget);
        }
      }
    });

    //Validate if there are specialties available in the template to fill out the form
    spAllIncluded = j$divTarget.find('li.pgover').length;
    if(spAllIncluded == 0){
      alert('All the specialties for this template have  already included in others definitions');
    }
  }
}


function checkAllCustomers(obj){
  //get true or false
  if(obj){
    //var accountMatrix = j$("[id$='AccountMatrixCK']").prop('checked');
    //if(accountMatrix == true) var accountMatrixtype = j$("[id*='AMTSelect']").val();
    var j$lauchPhase = j$(getParent(obj, 'TBODY')).find("input[id^='matrixLaunchPhase']");
    var targetId = undefined, j$dragTarget = undefined, j$dragCurrent = undefined;

    if(obj.checked){
      if(!(accountMatrix && AccountMatrixTypeValue == 'HCO only')){
        var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.specialCls');
        var currentSpecialties = [];

        j$("li", j$divTarget).each(function(){//check specialties already include in the matrix
            currentSpecialties.push(j$(this).attr("Id"));
        });

        j$("li", j$("#specialDivId")).each(function(){
          j$dragCurrent = j$(this);
          var currentId = j$dragCurrent.attr('id');
          var hasFinded = false;
          var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));//added by Peng Zhu 2013-06-25 for overflowHiddenç
          var specialtyIn = j$.inArray(currentId, currentSpecialties);

          if(specialtyIn >= 0 )//Including specialty as long as it hasn't been already included in the matrix
            return;

          if(j$div.hasClass('overflowHidden')){
            j$div.removeClass('overflowHidden');
          }

          j$dragTarget = undefined;
          j$divTarget.find('li').each(function(){
            if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){  //cleart old channel
              if(!hasFinded){
                hasFinded = true;
                j$dragTarget = j$(this), targetId = j$(this).attr('id');
              }
            }
          });
          //if has not find any available li, do nothing
          if(typeof(j$dragTarget) != 'undefined'){
            j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
              .addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
            //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
            //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
          }
          //Begin: added by Peng Zhu 2013-08-30
          else{
            j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
            //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
            //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
            j$divTarget.find('.targerDropUlCls').append(j$dragTarget);
          }
          //End: added by Peng Zhu 2013-08-30
        });
        j$divTarget.find('.erMsg').hide();
      }else{
        var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.AMSmatrixCls ');
        var currentSpecialties = [];

        j$("li", j$divTarget).each(function(){//check specialties already include in the matrix
            currentSpecialties.push(j$(this).attr("Id"));
        });

        j$("li:not(.special-state-disabled)", j$("#AccMSDivBody")).each(function(){
          j$dragCurrent = j$(this);
          var currentId = j$dragCurrent.attr('id');
          var hasFinded = false;
          var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));//added by Peng Zhu 2013-06-25 for overflowHidden
          var specialtyIn = j$.inArray(currentId, currentSpecialties);

          if(specialtyIn >= 0 )//Including specialty as long as it hasn't been already included in the matrix
            return;

          if(j$div.hasClass('overflowHidden')){
            j$div.removeClass('overflowHidden');
          }

            j$dragTarget = undefined;
          j$divTarget.find('li').each(function(){
            if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){  //cleart old channel
              if(!hasFinded){
                hasFinded = true;
                j$dragTarget = j$(this), targetId = j$(this).attr('id');
              }
            }
          });
          //if has not find any available li, do nothing
          if(typeof(j$dragTarget) != 'undefined'){
            j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
              .addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
            //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
            //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
          }
          //Begin: added by Peng Zhu 2013-08-30
          else{
            j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
            //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
            //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
            j$divTarget.find('ul').append(j$dragTarget);
          }
          //End: added by Peng Zhu 2013-08-30
        });
        j$divTarget.find('.erMsg').hide();
        var targetId = undefined, j$dragTarget = undefined, j$dragCurrent = undefined;
        var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.SmatrixCls');
        var ulli = j$divTarget.find("li");
        j$("li:not(.special-state-disabled)", j$("#specialDivBody")).each(function(){
          var isRepeat = false;
          for(var i = 0; i< ulli.length; i++){
            var li = ulli[i];
            if(j$(li).attr("id") == j$(this).attr('id')){
              isRepeat = true;
              break;
            }
          }
          if(!isRepeat){
            j$dragCurrent = j$(this);
            var currentId = j$dragCurrent.attr('id');

            var hasFinded = false;

            //added by Peng Zhu 2013-06-25 for overflowHidden
            var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));

            if(j$div.hasClass('overflowHidden')){
              j$div.removeClass('overflowHidden');
            }

              j$dragTarget = undefined;
            j$divTarget.find('li').each(function(){
              if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){  //cleart old channel
                if(!hasFinded){
                  hasFinded = true;
                  j$dragTarget = j$(this), targetId = j$(this).attr('id');
                }
              }
            });
            //if has not find any available li, do nothing
            if(typeof(j$dragTarget) != 'undefined'){
              j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
                .addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
              //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
            }
            //Begin: added by Peng Zhu 2013-08-30
            else{
              j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
              //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              j$divTarget.find('ul').append(j$dragTarget);
            }
            //End: added by Peng Zhu 2013-08-30
          }
        });
        j$divTarget.find('.erMsg').hide();
        var targetId = undefined, j$dragTarget = undefined, j$dragCurrent = undefined;
        var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.CRmatrixCls');
        var ulli = j$divTarget.find("li");
        j$("li:not(.special-state-disabled)", j$("#SCRoleDivBody")).each(function(){
          var isRepeat = false;
          for(var i = 0; i< ulli.length; i++){
            var li = ulli[i];
            if(j$(li).attr("id") == j$(this).attr('id')){
              isRepeat = true;
              break;
            }
          }
          if(!isRepeat){
            j$dragCurrent = j$(this);
            var currentId = j$dragCurrent.attr('id');

            var hasFinded = false;

            //added by Peng Zhu 2013-06-25 for overflowHidden
            var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));

            if(j$div.hasClass('overflowHidden')){
              j$div.removeClass('overflowHidden');
            }

              j$dragTarget = undefined;
            j$divTarget.find('li').each(function(){
              if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){  //cleart old channel
                if(!hasFinded){
                  hasFinded = true;
                  j$dragTarget = j$(this), targetId = j$(this).attr('id');
                }
              }
            });
            //if has not find any available li, do nothing
            if(typeof(j$dragTarget) != 'undefined'){
              j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
                .addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
              //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
            }
            //Begin: added by Peng Zhu 2013-08-30
            else{
              j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
              //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              j$divTarget.find('ul').append(j$dragTarget);
            }
            //End: added by Peng Zhu 2013-08-30
          }
        });
        j$divTarget.find('.erMsg').hide();
        var targetId = undefined, j$dragTarget = undefined, j$dragCurrent = undefined;
        var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.DTmatrixCls');
        var ulli = j$divTarget.find("li");
        j$("li:not(.special-state-disabled)", j$("#DepTDivBody")).each(function(){
          var isRepeat = false;
          for(var i = 0; i< ulli.length; i++){
            var li = ulli[i];
            if(j$(li).attr("id") == j$(this).attr('id')){
              isRepeat = true;
              break;
            }
          }
          if(!isRepeat){
            j$dragCurrent = j$(this);
            var currentId = j$dragCurrent.attr('id');

            var hasFinded = false;

            //added by Peng Zhu 2013-06-25 for overflowHidden
            var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));

            if(j$div.hasClass('overflowHidden')){
              j$div.removeClass('overflowHidden');
            }
              j$dragTarget = undefined;
            j$divTarget.find('li').each(function(){
              if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){  //cleart old channel
                if(!hasFinded){
                  hasFinded = true;
                  j$dragTarget = j$(this), targetId = j$(this).attr('id');
                }
              }
            });
            //if has not find any available li, do nothing
            if(typeof(j$dragTarget) != 'undefined'){
              j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
                .addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
              //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
            }
            //Begin: added by Peng Zhu 2013-08-30
            else{
              j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              //j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
              //j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
              j$divTarget.find('ul').append(j$dragTarget);
            }
            //End: added by Peng Zhu 2013-08-30
          }
        });
        j$divTarget.find('.erMsg').hide();
      }
    }
  }
}

/**
* This function is used to check if each matrix speciality has at least one value
*
@author  Peng Zhu
@created 2013-06-21
@version 1.0
*
@changelog
* 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
* - Created
*/
function getLaunchTemplateValue(j$obj){
  j$this = j$obj;

  var j$table = j$(getParent(j$this.get(0), 'TABLE'));
  var j$mtSelect = j$("select[name$='theMTSelecrList']" ,j$table);
  var jsonMTObj = jsonMTofLaunch[j$mtSelect.val()];
  if(jsonMTObj && jsonMTObj.isLaunch){
    return jsonMTObj.lctColumn;
  }

  return '';
}
/**
* This function is used to hide the column errmsg when template select changed
*
@author  Peng Zhu
@created 2013-06-21
@version 1.0
*
@changelog
* 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
* - Created
*/
function hideErrMsgWhenChangeMt(obj){
  j$this = j$(obj);

  var j$table = j$(getParent(j$this.get(0), 'TABLE'));

  var j$inputColumn = j$("input[id^='theColumnInput']", j$table), j$errMsg = j$inputColumn.siblings('.erMsg');

  j$errMsg.hide();

  var v = j$inputColumn.val();

  if(typeof v == 'undefined') v = '';
  v = j$.trim(v.replace(/[^\d]/g, ''));

  if(v != ''){
    v = parseInt(v);

    if(v && v < 1 || v > 21){
      j$errMsg.find('font').text(errMsg_Column).parent().show();
    }
  }
  else{
    j$errMsg.find('font').text(errMsg_Column_Null).parent().show();
  }

  j$inputColumn.val(v);
}

/**
* Set disabled matrix final fields
*
@author  Peng Zhu
@created 2013-06-21
@version 1.0
*
@changelog
* 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
* - Created
*/
//
function disableAllFinalMatrix(){
  //j$("#matriDivId table.finalMatrix input[name$='matrixDPA']").attr('disabled', 'disabled');
  j$("#matriDivId table.finalMatrix input[name$='matrixAllCustomers']").attr('disabled', 'disabled');
  j$("#matriDivId table.finalMatrix input[name$='matrixAdoptionLabel']").attr('disabled', 'disabled');
  j$("#matriDivId table.finalMatrix input[name$='matrixPotentialLabel']").attr('disabled', 'disabled');
}
