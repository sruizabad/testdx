function register_event(){	//all event
    //added by Peng 2013-06-07
    j$("#matriDivId table select[name$='theMTSelecrList']").each(function(){
    	j$(this).attr("title", j$("option[value=" + j$(this).val() + "]", j$(this)).text());

		if(!j$(getParent(j$(this).get(0), 'TABLE')).hasClass("finalMatrix"))
			populateColumnsForLaunchTemplate(j$(this), true);
    });

    //Begin: modified by Peng Zhu 2013-06-21 for copying the rows and columns from template
    j$("#matriDivId table select[name$='theMTSelecrList']").change(function(){
    	j$(this).attr("title", j$("option[value=" + j$(this).val() + "]", j$(this)).text());
    	populateColumnsForLaunchTemplate(j$(this), false);
    });

    //Set info matrix from matrix template already created
	//End: modified by Peng Zhu 2013-06-21

	j$('#specialDivId .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#specialDivId .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');

	j$('#AccMSDivBody .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#AccMSDivBody .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');
	j$('#specialDivBody .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#specialDivBody .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');
	j$('#SCRoleDivBody  .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#SCRoleDivBody  .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');
	j$('#DepTDivBody  .draggable').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	j$('#DepTDivBody  .ui-state-disabled').off('mouseenter mouseleave mousedown mouseup');

	var sortable_options = {containment : '#theTopDiv', cancel : '.ui-state-disabled', cursor : 'move'},
		droppable_options = {hoverClass : 'hoverTarget', activeClass : 'dropTarget', cursor : 'move'};

	var special_drop = j$('#specialDivId ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
    var ams_drop = j$('#AccMSDivBody ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
    var sm_drop = j$('#specialDivBody ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
    var cr_drop = j$('#SCRoleDivBody ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
    var dt_drop = j$('#DepTDivBody ul.dropUlCls').sortable({
			opacity: 0.7,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn
			//stop: matrix_stop_fn_void
    });
	j$("div[id^='matrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
	});


		});
	j$("div[id^='matrix']").not(".finalMatrix").droppable({
        accept:"#specialDivId .draggable, .specialCls .matrixDraggable",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#specialDivId",
        drop : droppable_drop_fn
    });

    //new
	j$("div[id^='AMSmatrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
		});
	});


	j$("div[id^='AMSmatrix']").not(".finalMatrix").droppable({
        accept:"#AccMSDivBody .draggable, .AMSmatrixCls .matrixDraggable",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#AccMSDivBody",
        drop : droppable_drop_fn
    });
    //new
	j$("div[id^='Smatrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
		});
	});


	j$("div[id^='Smatrix']").not(".finalMatrix").droppable({
        accept:"#specialDivBody .draggable",//, .SmatrixCls .matrixDraggable
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#specialDivBody",
        drop : droppable_drop_fn
    });
    //new
	j$("div[id^='CRmatrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
		});
	});


	j$("div[id^='CRmatrix']").not(".finalMatrix").droppable({
        accept:"#SCRoleDivBody .draggable",//, CRmatrixCls .matrixDraggable
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#SCRoleDivBody",
        drop : droppable_drop_fn
    });
    //new
	j$("div[id^='DTmatrix']").not(".finalMatrix").each(function(){
		j$(this).sortable({
			items: "li:not(.ui-state-disabled)",
			opacity: 0.3,
			revert: true,
			cursor : sortable_options.cursor,
			cancel: sortable_options.cancel,
			containment: sortable_options.containment,
			helper:"clone",
			start : matrix_sort_start_fn,
			stop: matrix_stop_fn
		});
	});


	j$("div[id^='DTmatrix']").not(".finalMatrix").droppable({
        accept:"#DepTDivBody .draggable",//, DTmatrixCls .matrixDraggable
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#DepTDivBody",
        drop : droppable_drop_fn
    });

	j$("#specialDivId").droppable({
        accept:".specialCls .matrixDraggable, .specialCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .specialCls",
        drop : droppable_drop_fn
    });
	j$("#AccMSDivBody").droppable({
        accept:".AMSmatrixCls .matrixDraggable, .AMSmatrixCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .AMSmatrixCls",
        drop : droppable_drop_fn
    });
	j$("#specialDivBody").droppable({
        accept:".SmatrixCls .matrixDraggable, .SmatrixCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .SmatrixCls",
        drop : droppable_drop_fn
    });
	j$("#SCRoleDivBody").droppable({
        accept:".CRmatrixCls .matrixDraggable, .CRmatrixCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .CRmatrixCls",
        drop : droppable_drop_fn
    });
	j$("#DepTDivBody").droppable({
        accept:".DTmatrixCls .matrixDraggable, .DTmatrixCls .ui-selected",
        hoverClass: droppable_options.hoverClass,
        activeClass: droppable_options.activeClass,
        liveSelector:"#matriDivId .DTmatrixCls",
        drop : droppable_drop_fn
    });

	j$('.matrixDraggable').not(".ui-state-disabled").off('mouseleave mouseenter mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	showTextHelp();
}


/**
 * Set info for the matrix from any matrix template already created
 *
 * @param obj DOM object select list of matrix template
 */
function setMatrixTemplateInfo(obj){
	var j$this = j$(obj);
	var template = jsonMatrixTemplate[j$(obj).val()];
	var j$table = j$(getParent(j$this.get(0), 'TABLE'));
	var j$cycleTemplate = j$("select[name$='theMTSelecrList']", j$table);

	var j$potentialLabel = j$("input[name$='matrixPotentialLabel']", j$table);
	var j$adoptionLabel = j$("input[name$='matrixAdoptionLabel']", j$table);

	if(template){
		j$cycleTemplate.val(template.Lifecycle_Template_BI__c);
		populateColumnsForLaunchTemplate(j$cycleTemplate, false);
		if(template.Specialty_Ids_BI__c){
			j$table.find(".erMsg").hide();
		}
		setSpecialtiesFromTemplate(j$this.get(0),template);
		if(template.Potential_Data_Label_BI__c!=null && j$.trim(j$potentialLabel.val())=='') j$potentialLabel.val(template.Potential_Data_Label_BI__c);
		if(template.Adoption_Data_Label_BI__c!=null && j$.trim(j$adoptionLabel.val())=='') j$adoptionLabel.val(template.Adoption_Data_Label_BI__c);

		//var j$inputRow = j$("input[id^='theRowInput']", j$table);
		//var j$inputCol = j$("input[id^='theColumnInput']", j$table);
		//j$inputRow.val(template.Row_BI__c);
		//j$inputCol.val(template.Column_BI__c);
	}else{

	}
}

function e_over_draggable(e){
	var j$this = j$(this);
	switch(e.type){
		case 'mouseenter': j$this.addClass('pgover'); break;
		case 'mouseleave' : j$this.removeClass('pgover');if(isdown){j$this.addClass('ui-selected');isdown=false;} break;
		case 'mousedown' :if(j$this.attr("class").indexOf('ui-state-disabled') < 0){if(e.ctrlKey){if(j$this.attr("class").indexOf('ui-selected') > -1){j$this.removeClass('ui-selected');}else{j$this.addClass('ui-selected');}}else{j$this.removeClass('pgover');isdown = true;}};break;
		case 'mouseup' : if(j$this.attr("class").indexOf('ui-state-disabled') < 0){if(!e.ctrlKey){if(j$this.attr("class").indexOf('ui-selected') == -1){j$(".ui-selected").removeClass('ui-selected');j$this.addClass('ui-selected');}else{j$(".ui-selected").removeClass('ui-selected');}isdown = false;}};break;
	}
}


function droppable_drop_fn(drag,drop){
	//console.log('In drop....');
	var j$dragCurrent = j$(drop.helper.context), conditionId,
	parentId = j$.trim(j$dragCurrent.parent().parent().attr('id'));
	var map_id_name = {};
	var selectedArr = j$dragCurrent.parent().find(".ui-selected");
	var ulli = j$(this).find("li");
	var map_matrx_sle = {"AccMSDivBody":"AMSmatrix","specialDivBody":"Smatrix","SCRoleDivBody":"CRmatrix","DepTDivBody":"DTmatrix"};
	w:for(var i = 0; i < selectedArr.length; i++) {
		var selectLi = selectedArr[i];
		if(selectLi.id && selectLi.id.indexOf('\,') < 0 && !map_id_name[selectLi.id]){
			if(map_matrx_sle[parentId]){
				for(var j = 0; j < ulli.length; j++) {
					var li = j$(ulli[j]);
					if(li.attr("id") == selectLi.id) {continue w;};
				}
			}
			map_id_name[selectLi.id] = selectLi.id;
			j$dragCurrent = j$(selectLi);
			currentId = j$.trim(j$dragCurrent.attr('id'));
			if(typeof(parentId) != 'undefined' && j$.trim(parentId) != '' && (parentId.indexOf('matrix') > -1)){
				conditionId = 'matrixDivId';
			}
			else{
				conditionId = parentId;
			}


			var targetId = j$(drag.target).attr('id');
			 //console.log('Current : ' + j$.trim(j$dragCurrent.parent().parent().attr('id')));

			if((targetId.indexOf('matrix') > -1) && (parentId.indexOf('matrix') > -1)){
				conditionId = 'horizontalMove';
			}

			if(typeof(targetId) != 'undefined' && typeof(parentId) != 'undefined' && j$.trim(targetId) != '' && j$.trim(parentId) != '' && targetId != parentId){
				 //console.log('In cal....',conditionId);
				switch(conditionId){
					case 'horizontalMove' :
						var hasFinded = false;
						var j$dragTarget, targetId;

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});

						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.text()}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.text()).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);


							j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="'+j$dragCurrent.text()+'">' + j$dragCurrent.text() + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');

							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30
						break;

					case 'matrixDivId' :
						var duplicateCounter = 0;
		    			//j$("div[name='matrixSpecial'] li").each(function(){
						//	var liId = j$(this).attr('Id');
			       		//	if(liId && liId == currentId) duplicateCounter ++;
			    		//});

						//console.log('duplicateCounter : ' + duplicateCounter);
						//console.log('currentId : ' + currentId);
						var j$lis = j$(drag.target).find('li');
			        	j$lis.each(function(){
			        		if(currentId == j$(this).attr('id')){
			        			if(duplicateCounter < 2){
			        				j$(this).removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
				        			j$(this).off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
			        			}
			        			j$(('#' + parentId + ' #'+currentId)).attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
			        		}
			        	});
						break;

					case 'specialDivId' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;

					case 'AccMSDivBody' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;

					case 'specialDivBody' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});//@jescobar: disable specialties pattern2 matrix
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});//@jescobar: disable specialties pattern2 matrix
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;

					case 'SCRoleDivBody' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;

					case 'DepTDivBody' :
						//find a available li -- id is null
						var hasFinded = false;
						var j$dragTarget = undefined, targetId;
						var j$divTarget = j$(drag.target).find('li.pgover');

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

						j$(drag.target).find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 0.4});
							j$dragCurrent.removeClass('pgover').removeClass('ui-selected').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$(drag.target).find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30

						break;
					default :  break;
				}
			}
			checkAllMatrixSpecialDiv();
		}
	}
}

function removeMatrix(){
	j$("#matriDivId table:last-child").remove();
	var matrixIndex = parseInt(j$('#matrixIndex').val());
	if(matrixIndex > 0){
		j$('#matrixIndex').val(matrixIndex - 1);
	}
}

/**
 * collect the matrixs values on page
 * return : JSON Data
 */
function collectMatrixValues(toCalculate){
	var counter = 0, matrix = {}, arr = [], mtIdArr = [], sArrAll = [];

	  var accountMatrix = j$("[id$='AccountMatrixCK']").prop('checked');
	  if(accountMatrix == true) var accountMatrixtype = j$("[id*='AMTSelect']").val();
	j$("#matriDivId table").not('.finalMatrix').each(function(){

		var matrixEditId = j$(this).attr('id');
		if(matrixEditId && j$.trim(matrixEditId) != ''){
			matrixEditId = matrixEditId.split('-')[1];
		}

	    var name = j$("input[name='matrixName']", j$(this)).val();
	    var adoptionLabel = j$("input[name$='matrixAdoptionLabel']", j$(this)).val();
	    var potentialLabel = j$("input[name$='matrixPotentialLabel']", j$(this)).val();
	    var isChecked = (j$("input[name$='matrixChecked']", j$(this)) && j$("input[name$='matrixChecked']", j$(this)).get(0).checked) ? true : false ;

	    adoptionLabel = (adoptionLabel == null || adoptionLabel == '') ? 'Prod Rx' : adoptionLabel;
	    potentialLabel = (potentialLabel == null || potentialLabel == '') ? 'Market Rx' : potentialLabel;
	    var isCalculated = (toCalculate) ? isChecked : false;
	    var special = '';

	    var ams = '';
	    var sm = '';
	    var crm = '';
	    var dtm = '';
	    var sArr = [];

	    //************************special*******************************//
	    if(!(accountMatrix && accountMatrixtype == 'HCO only')){
			j$("div[name='matrixSpecial'] li", j$(this)).each(function(){
				var temp = j$.trim(j$(this).text());
		       	if(typeof(temp) != 'undefined' && temp != ''){
		           	special += temp + '; ';
		       	}

		       	var sId= j$(this).attr('id');
		       	if( j$("[id$='AccountMatrixCK']").prop('checked') ) sId = j$(this).attr('name');
		       	//else sid = j$(this).attr('id');
		       	//console.log( j$(this).textContent );
		       	//console.log( sid );
		       	if(sId && j$.trim(sId) != ''){
		       		sArr.push(j$.trim(sId));
		       		sArrAll.push(j$.trim(sId));
		       	}
            //console.log(':: sArr: ' + sArr);
            //console.log(':: sArrAll: ' + sArrAll);
		    });
		    //console.log("yaoxiongyu", sArr);
		    special = j$.trim(special);
		    if(typeof(special) != 'undefined' && special != ''){
		        special = special.substring(0, special.length - 1);
		    }
	    }else{
	    //************************special*******************************//

	    //************************ams*******************************//

		j$("div[name='matrixAMS'] li", j$(this)).each(function(){
			var temp = j$.trim(j$(this).text());
	       	if(typeof(temp) != 'undefined' && temp != ''){
	           	ams += temp + ';';
	       	}
	    });

	    ams = j$.trim(ams);
	    if(typeof(ams) != 'undefined' && ams != ''){
	        ams = ams.substring(0, ams.length - 1);
	    }
	    //************************ams*******************************//

	    //************************sm*******************************//

		j$("div[name='matrixSm'] li", j$(this)).each(function(){
			var temp = j$.trim(j$(this).text());
	       	if(typeof(temp) != 'undefined' && temp != ''){
	           	sm += temp + ';';
	       	}
	       	var sId= j$(this).attr('id');
	       	//else sid = j$(this).attr('id');
	       	//console.log( j$(this).textContent );
	       	console.log(':: Specialty: ', sId, ' N: ',  sm);
	       	if(sId && j$.trim(sId) != ''){
	       		sArr.push(j$.trim(sId));
	       		sArrAll.push(j$.trim(sId));
	       	}
	    });

	    sm = j$.trim(sm);
	    if(typeof(sm) != 'undefined' && sm != ''){
	        sm = sm.substring(0, sm.length - 1);
	    }
	    //************************sm*******************************//

	    //************************crm*******************************//

		j$("div[name='matrixCR'] li", j$(this)).each(function(){
			var temp = j$.trim(j$(this).text());
	       	if(typeof(temp) != 'undefined' && temp != ''){
	           	crm += temp + ';';
	       	}
	    });

	    crm = j$.trim(crm);
	    if(typeof(crm) != 'undefined' && crm != ''){
	        crm = crm.substring(0, crm.length - 1);
	    }
	    //************************crm*******************************//

	    //************************dtm*******************************//

		j$("div[name='matrixDT'] li", j$(this)).each(function(){
			var temp = j$.trim(j$(this).text());
	       	if(typeof(temp) != 'undefined' && temp != ''){
	           	dtm += temp + '; ';
	       	}
	    });

	    dtm = j$.trim(dtm);
	    if(typeof(dtm) != 'undefined' && dtm != ''){
	        dtm = dtm.substring(0, dtm.length - 1);
	    }
	    }
	    //************************dtm*******************************//


	    var row = j$("input[id^='theRowInput']", j$(this)).val();
	    if(typeof(row) != 'undefined' && j$.trim(row) != ''){
	    	row = parseInt(row);
	    }
	    else{
	    	row = 0;
	    }

	    var dpa = false;
	    if(j$("input[name$='matrixDPA']", j$(this)) && j$("input[name$='matrixDPA']", j$(this)).get(0).checked){
	    	dpa = true;
	    }

	    var allCustomers = false;
	    if(j$("input[name$='matrixAllCustomers']", j$(this)) && j$("input[name$='matrixAllCustomers']", j$(this)).get(0).checked){
	    	allCustomers = true;
	    }

	    var column = j$("input[id^='theColumnInput']", j$(this)).val();
	    if(typeof(column) != 'undefined' && j$.trim(column) != ''){
	    	column = parseInt(column);
	    }
	    else{
	    	column = 0;
	    }

	    //var lifeCycle = j$("select[name$='theLCSelectList']", j$(this)).val();
	    var sMtId = j$("select[name$='theMTSelecrList']", j$(this)).val();
	    if(typeof(sMtId) != 'undefined' && j$.trim(sMtId) != ''){
	    	mtIdArr.push(sMtId);
	    }
	    var matrixTemplateId = j$("select[name$='matrixTemplate']", j$(this)).val();
      //console.log( ':: sArr: ', sArr);
	    //console.log( ':: sArrAll: ', sArrAll);
	    if(typeof(name) != 'undefined' && name != ''){
	        var cm = {};
	        if(matrixEditId && j$.trim(matrixEditId) != '') cm.mid = matrixEditId;
	        cm.name = name;
	        cm.special = special;
	        cm.ams = ams;
	        cm.sm = sm;
	        cm.crm = crm;
	        cm.dtm = dtm;
	        //cm.lifeCycle = lifeCycle;
	        cm.tid = sMtId;
	        cm.row = row;
	        cm.column = column;
	        cm.set_sId = sArr;
	        cm.dpa = dpa;
	        cm.allCustomers = allCustomers;
	        cm.adoptionLabel = adoptionLabel;
	        cm.potentialLabel = potentialLabel;
	        cm.toCalculate = isCalculated;
	        cm.matrixTemplateId = (typeof(matrixTemplateId) != 'undefined' && j$.trim(matrixTemplateId) != '') ? matrixTemplateId : null;

	        arr.push(cm);
	    }
	});

	matrix.productId = j$("select[id$='theProductList']").val();
	matrix.cid = cycleId;
	matrix.list_cm = arr;
	matrix.AccountMatrixTypeValue = j$("[id*='AMTSelect']").val();
	matrix.set_mtIds = mtIdArr;
	matrix.set_sId = sArrAll;
	matrix.countryId = countryId;
	matrix.countryCode = countryCode;
	matrix.countryCodeRegion = countryCodeRegion;
	matrix.countryTestId = countryTestId;
	matrix.mHCOId = mHCOId;
	//add by leijun
	matrix.accountMatrix = j$("[id$='AccountMatrixCK']").prop('checked');
	if(matrix.accountMatrix == true)  matrix.accountMatrixtype = j$("[id*='AMTSelect']").val();
	else matrix.accountMatrixtype = null;
	//die
	return JSON.stringify(matrix);
}

function showTextHelp(){
	j$(function() {
		j$("img[class^='budgetHelpText']").hover(
			function () {
				j$(this).removeClass("budgetHelpText").addClass("budgetHelpTextOn");
			},
			function () {
				j$(this).removeClass("budgetHelpTextOn").addClass("budgetHelpText");
			}
		);
	  });
}


function addMatrix(){
    //TODO: get matrix index
    var matrixIndex = parseInt(j$('#matrixIndex').val()) + 1;
    var accountMatrix = j$("[id$='AccountMatrixCK']").prop('checked');
    if(accountMatrix == true) var accountMatrixtype = j$("[id*='AMTSelect']").val();
    var stylen = 'display : block;';
    var styley = 'display : none;';
    if(accountMatrix && accountMatrixtype == 'HCO only'){
    	styley = 'display : block;';
    	stylen = 'display : none;';
    }
    //show dpa for hcp matrix
    var showDPA = (accountMatrix == true) ? 'display : none;' : '';
    //create table
    var tableStr = '';
    tableStr += '<table style="display:inline-block;" class="matrixInnerTable">' +
                            '<tr>' +
                                '<td> <span style="float:left;"> <input type="checkbox" value="1" id="matrixChecked-'+matrixIndex+'" id="matrixChecked" name="matrixChecked"/> </span> <img src="/s.gif" class="budgetHelpText" title="'+selectMatrix_TextHelp+'" style="float: left;margin: 3px 5px;" /> '+
                                	'<img src="/img/icon/service_contracts24.png" title="'+matrixTemplateInfo+'" style="cursor: pointer; float: right;" onclick="openMatrixTemplate(this);"/>'+
                            	 	label_Matrix + '<span class="matrixIndexSpan">' + matrixIndex  + '</span>' + '<img title="delete this Matrix" class="deleteMatrixImg" style="cursor:pointer; margin-left:5px; vertical-align:middle; height:15px; width:15px;" onclick="removeGivenMatrix(this);" src="/img/func_icons/util/recycle.gif"></td>' +
                            '</tr>' +
                            '<tr>'+
	                            '<td style="text-align: left;">'+
	                                '<span class="matrixTableLabel">Matrix Template:&nbsp;</span>'+
	                                '<span style="float:right;">'+
	                                	matrixTemplateList+
	                                '</span>'+
	                            '</td>'+
                            '</tr>'+
                            '<tr>' +
                                '<td style="text-align: left;">' +
                                '<span class="matrixTableLabel">' + label_Name + ':&nbsp;</span><span style="float:right;">' +
                                '<div class="requiredInput"><div class="requiredBlock"></div>' +
                                '<input name="matrixName" type="text" style="width:100px;" onchange="checkMatrixName(this);" onkeyup="if(this.value.match(/\'/)) this.value=this.value.replace(/[\']/g,\'\')"/>' +
                                '<div class="erMsg" style="display:none; font-size:12px;">' +
                                        '<font color="#C00 !important">Must not be null</font>' +
                                    '</div>' +
                                '</div></span></td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="text-align: left;'+showDPA+'">' +
                                	'<span class="matrixTableLabel">' + label_Dpa + ':&nbsp;</span>' +
                                	'<span style="float:right;">' +
                                		'<input name="matrixDPA" type="checkbox" style="width:100px;" onclick="checkAllMatrixSpecialDiv();checkDpaForAllCustomer(this);"/>' +
                                	'</span></td>' +
                            '</tr>' +
                            '<tr>' +
                            '<td style="text-align: left;">' +
                            	'<span class="matrixTableLabel">' + label_LaunchPhase + ':&nbsp;</span>' +
                            	'<span style="float:right;">' +
                            		'<input id="matrixLaunchPhase" type="checkbox" disabled="true" style="width:100px;" />' +
                            	'</span></td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="text-align: left;">' +
                                	'<span class="matrixTableLabel">' + label_AllCustomers + ':&nbsp;'+
                                	'</span>' +
                                	'<span style="float:right;">' +
                                		'<input name="matrixAllCustomers" type="checkbox" style="width:100px;" onclick="checkAllCustomers(this);"/>' +
                                	'</span></td>' +
                            '</tr>' +
							              '<tr>' +
                                '<td style="text-align: left;">' +
                                '<span class="matrixTableLabel">' + label_Status + ':&nbsp;</span><span style="float:right;">' +
                                '<div class="requiredInput"><div class="requiredBlock"></div>' +
                                '<input name="matrixStatus" type="text" style="width:100px;" disabled="disabled" value="' + value_Draft + '"/>' +
                                '<div class="erMsg" style="display:none; font-size:12px;">' +
                                        '<font color="#C00 !important">Must not be null</font>' +
                                    '</div>' +
                                '</div></span></td>' +
                            '</tr>' +
                            '<tr>' +
								'<td style="text-align: left;">' +
                            	'<span class="matrixTableLabel">Template: </span><span style="float:right;">' +
                                '<div class="requiredInput">' +
									'<div class="requiredBlock">' +
									'</div>'
	                                + mtSelectList +
								    '<div class="erMsg" style="display:none; font-size:12px;">' +
								        '<font color="#C00 !important"><strong>Error: </strong>You must select one Product!</font>' +
								    '</div>' +
								'</div></span>' +
								'</td>' +
                            '</tr>' +
							'<tr>'+
								'<td style="text-align: left;">'+
									'<span class="matrixTableLabel">'+  label_Potential +'</span>'+
									'<span style="float:right;">'+
										'<div class="requiredInput">'+
											'<span style="float:right;">'+
												'<input type="text" style="width:100px;" size="20"  maxlength="25" name="matrixPotentialLabel" placeholder="Market Rx" />'+
											'</span>'+
											'<div class="erMsg" style="display:none; font-size:12px;">'+
												'<font color="#C00 !important"><strong>Error: </strong>Must not be null!</font>'+
											'</div>'+
										'</div>'+
									'</span>'+
								'</td>'+
							'</tr>'+
							'<tr>'+
								'<td style="text-align: left;">'+
									'<span class="matrixTableLabel">'+ label_Adoption +'</span>'+
									'<span style="float:right;">'+
										'<div class="requiredInput">'+
											'<span style="float:right;">'+
												'<input type="text" style="width:100px;" size="20" name="matrixAdoptionLabel" maxlength="25" placeholder="Prod RX"/>'+
											'</span>'+
											'<div class="erMsg" style="display:none; font-size:12px;">'+
												'<font color="#C00 !important"><strong>Error: </strong>Must not be null!</font>'+
											'</div>'+
										'</div>'+
									'</span>'+
								'</td>'+
							'</tr>'+
                            //begin: --------------row and column-----------------
                            '<tr>'+
								'<td style="text-align:left;">' +
									'<span style="float:left;">' +
									'<label for="theRowInput' + matrixIndex + '">' + label_Rows + ':</label></span>' +
									'<div class="requiredInput" style="float:right;"><div class="requiredBlock"></div>' +
							        	'<input type="text" style="width:100px" maxlength="2" disabled="disabled" id="theRowInput' + matrixIndex + '" onchange="checkSizeInput(this);" onkeyup="checkSizeInput(this);" onpaste="checkSizeInput(this);" ondragenter="checkSizeInput(this);" oncontextmenu="return false;" />' +
										'<div class="erMsg" style="display:none; font-size:12px;">' +
                                        	'<font color="#C00 !important">Must not be null</font>' +
                                    	'</div>' +
                                	'</div>' +
                             	'</td>' +
                             '</tr>' +
                             '<tr>' +
                             	'<td style="text-align:left;">' +
									'<span style="float:left;">' +
									'<label for="theColumnInput' + matrixIndex + '">' + label_Column + ':</label></span>' +
									'<div class="requiredInput" style="float:right;"><div class="requiredBlock"></div>' +
							        	'<input type="text" style="width:100px;" maxlength="2" disabled="disabled" id="theColumnInput' + matrixIndex + '" onchange="checkSizeInput(this);" onkeyup="checkSizeInput(this);" onpaste="checkSizeInput(this);" ondragenter="checkSizeInput(this);" oncontextmenu="return false;" />' +
							        	'<div class="erMsg" style="display:none; font-size:12px;">' +
                                        	'<font color="#C00 !important">Must not be null</font>' +
                                    	'</div>' +
									'</div>' +
								'</td>' +
							'</tr>' +
							//end: -------------row and column------------------
                            '<tr>' +
                                '<td>' +
                                	'<div style="text-align:left;'+stylen+'"> '+label_Specialization+':</div>'+
                                    '<div id="matrix' + matrixIndex + '" name="matrixSpecial" style="'+stylen+' height: 300px; width: 250px; border: 1px solid #000; overflow:auto;color: #fff;" class="specialCls heightCls ui-droppable">' +
                                        '<div class="erMsg" style="display:none; font-size:12px;">' +
								        		'<font color="#C00 !important"><strong>Error: </strong>Please select at least one ' + label_Specialization + '!</font>' +
								    		'</div>' +
                                        '<ul class="targerDropUlCls matrixDropUlCls ui-sortable">';
                               var counter = parseInt(specialCounter);
                               for(var i =  0; i < counter; i++){
                                   tableStr += '<li id="" class="matrixDraggable ui-state-disabled"></li>';
                               }

                                        tableStr += '</ul>' +

                                    '</div>' +
                                	'<div class="tabs" style="text-align:left;'+styley+'"><ul><li><a href="#AMSmatrix' + matrixIndex + '">'+label_Specialization+'</a></li><li><a href="#Smatrix' + matrixIndex + '">Speciality</a></li><li><a href="#CRmatrix' + matrixIndex + '">Customer Role</a></li><li><a href="#DTmatrix' + matrixIndex + '">Department Type</a></li></ul>'+
                                    '<div id="AMSmatrix' + matrixIndex + '" name="matrixAMS" style="'+styley+' height: 300px; width: 250px; border: 1px solid #000; overflow:auto;color: #fff;" class="AMSmatrixCls heightCls ui-droppable">' +
                                        '<div class="erMsg" style="display:none; font-size:12px;">' +
								        		'<font color="#C00 !important"><strong>Error: </strong>Please select at least one ' + label_Specialization + '!</font>' +
								    		'</div>' +
                                        '<ul class="targerDropUlCls matrixDropUlCls ui-sortable">';
                               var counter = parseInt(specialCounter);
                               for(var i =  0; i < counter; i++){
                                   tableStr += '<li id="" class="matrixDraggable ui-state-disabled"></li>';
                               }

                                        tableStr += '</ul>' +

                                    '</div>' +
                                    '<div id="Smatrix' + matrixIndex + '" name="matrixSm" style="'+styley+' height: 300px; width: 250px; border: 1px solid #000; overflow:auto;color: #fff;" class="SmatrixCls heightCls ui-droppable">' +
                                        '<div class="erMsg" style="display:none; font-size:12px;">' +
								        		'<font color="#C00 !important"><strong>Error: </strong>Please select at least one ' + label_Specialization + '!</font>' +
								    		'</div>' +
                                        '<ul class="targerDropUlCls matrixDropUlCls ui-sortable">';
                               var counter = parseInt(specialCounter);
                               for(var i =  0; i < counter; i++){
                                   tableStr += '<li id="" class="matrixDraggable ui-state-disabled"></li>';
                               }

                                        tableStr += '</ul>' +

                                    '</div>' +
                                    '<div id="CRmatrix' + matrixIndex + '" name="matrixCR" style="'+styley+' height: 300px; width: 250px; border: 1px solid #000; overflow:auto;color: #fff;" class="CRmatrixCls heightCls ui-droppable">' +
                                        '<div class="erMsg" style="display:none; font-size:12px;">' +
								        		'<font color="#C00 !important"><strong>Error: </strong>Please select at least one ' + label_Specialization + '!</font>' +
								    		'</div>' +
                                        '<ul class="targerDropUlCls matrixDropUlCls ui-sortable">';
                               var counter = parseInt(specialCounter);
                               for(var i =  0; i < counter; i++){
                                   tableStr += '<li id="" class="matrixDraggable ui-state-disabled"></li>';
                               }

                                        tableStr += '</ul>' +

                                    '</div>' +
                                    '<div id="DTmatrix' + matrixIndex + '" name="matrixDT" style="'+styley+' height: 300px; width: 250px; border: 1px solid #000; overflow:auto;color: #fff;" class="DTmatrixCls heightCls ui-droppable">' +
                                        '<div class="erMsg" style="display:none; font-size:12px;">' +
								        		'<font color="#C00 !important"><strong>Error: </strong>Please select at least one ' + label_Specialization + '!</font>' +
								    		'</div>' +
                                        '<ul class="targerDropUlCls matrixDropUlCls ui-sortable">';
                               var counter = parseInt(specialCounter);
                               for(var i =  0; i < counter; i++){
                                   tableStr += '<li id="" class="matrixDraggable ui-state-disabled"></li>';
                               }

                                        tableStr += '</ul>' +

                                    '</div></div>' +

                                '</td>' +
                            '</tr>' +
                        '</table>';

    //append table to page
    j$('#matriDivId').append(tableStr);
    j$('.matrixDraggable').not(".ui-state-disabled").off('mouseleave mouseenter mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);

    j$('#matrixIndex').val(matrixIndex);
    j$(".tabs").tabs();
}


function matrix_stop_fn(event, ui){
	var parentUl = j$(this);
	var counter = 0;
	// //console.log(parentUl.attr('class'));
	// //console.log('tagName : ' + j$(this).get(0).tagName);

	var tagName = j$(this).get(0).tagName;
	if(tagName == 'DIV'){
		parentUl = j$("ul", j$(this));
	}


	j$("li", parentUl).each(function(){
		if(!j$(this).text().trim()){
            j$(this).remove();
            counter ++;
        }
	});

	for(var i = 0; i < counter; i++){
		 parentUl.append('<li id="" class="matrixDraggable ui-state-disabled"></li>');
	}

	checkAllMatrixSpecialDiv();
}

//Begin: added by Peng Zhu
function matrix_sort_start_fn(event, ui){
	var map_id_name = {};
	var j$this = j$(this);
	var selectArr = j$this.find(".ui-selected:not(.pgover)");
	var selectId = [];
	var selectName = [];
	for(var i =0; i < selectArr.length; i++) {
		var selectLi = j$(selectArr[i]);
		if(selectLi.attr("id") && selectLi.attr("id").indexOf('\,') < 0 && !map_id_name[selectLi.attr("id")]){
			map_id_name[selectLi.attr("id")] = selectLi.attr("id");
			selectId[selectId.length] = selectLi.attr("id");
			selectName[selectName.length] = selectLi.html();
		}
	}
	j$(j$this.find('li')[j$this.find('li').length - 1]).attr("id", selectId.toString()).html(selectName.toString());
	var map_matrx_sle = {"AccMSDivBody":"AMSmatrix","specialDivBody":"Smatrix","SCRoleDivBody":"CRmatrix","DepTDivBody":"DTmatrix"};
	if(map_matrx_sle[j$this.parent().attr("id")]){
		j$("a[href^='#"+map_matrx_sle[j$this.parent().attr("id")]+"']").click();
	}
}

function removeGivenMatrix(obj){
	var j$table = j$(obj).parent().parent().parent().parent();

	j$("li", j$table).each(function(){
		if(j$(this).attr('id')){
			var liId = j$(this).attr('id');
			j$("#specialDivId li[id='" + liId + "']").removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
			j$("#specialDivId li[id='" + liId + "']").off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
			j$("#AccMSDivBody li[id='" + liId + "']").removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
			j$("#AccMSDivBody li[id='" + liId + "']").off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
			j$("#specialDivBody li[id='" + liId + "']").removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
			j$("#specialDivBody li[id='" + liId + "']").off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
			j$("#SCRoleDivBody li[id='" + liId + "']").removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
			j$("#SCRoleDivBody li[id='" + liId + "']").off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
			j$("#DepTDivBody li[id='" + liId + "']").removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
			j$("#DepTDivBody li[id='" + liId + "']").off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
		}
	});

	j$table.remove();

	var matrixIndex = parseInt(j$('#matrixIndex').val()) - 1;
	j$('#matrixIndex').val(matrixIndex);

	refreshMatrixIndex();
}

function matrix_stop_fn_void(){}

function refreshMatrixIndex(){
	var matrixIndex = 0;
	j$("#matriDivId table span[class='matrixIndexSpan']").each(function(){
		matrixIndex ++;
		j$(this).text(matrixIndex);
	});
}

/**
 * reset the div "#matrixDivId" width when brower size change
 *
 @author  Peng Zhu
 @created 2013-06-21
 @version 1.0
 *
 @changelog
 * 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 */
function resetMatriDivIdSize(){
	//#matriDivId
	if(j$('#matriDivId')[0] && j$('#specialDivId')[0]) {
		var windowWidth = 0;

		if (window.innerWidth)
			windowWidth = window.innerWidth;
		else if ((document.body) && (document.body.clientWidth))
			windowWidth = document.body.clientWidth;
		else windowWidth = window.screen.width;

		if(windowWidth < 800) windowWidth = 800;

		var mainDivWidth = windowWidth - j$("#matriDivId").offset().left -80;//

		j$('#matriDivId').css("max-width", mainDivWidth - 200);
		j$('#specialDivId').css("max-width", mainDivWidth);
	}
}

//** assist function to find element's parent with specified tag name **//
function getParent(el,p){
	do{
		el = el.parentNode;
	}while(el && el.nodeName != p)
	return el;
}

/**
 * This function is used to validate and save all the matrix info
 *
 @author  Peng Zhu
 @created 2013-06-21
 @version 1.0
 *
 @changelog
 * 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 */
function savaMatrixsInPage(){
    showLoading();
    if(checkAllRequiredFields(true)){
       IMP_BI_ExtMatrixDefinition.saveMatrixData(collectMatrixValues(false), function(data){
            j$('#htmlEntity').html(data);
            data = j$('#htmlEntity').text();
            data = j$.parseJSON( data );

            if(data && !data.success){
                j$("div[id='errorMsg']").html(data.message);
                j$("div[id='errorMsg']").show();
                j$('#loading-curtain-div').hide();
            }
            else{
                cancel();
            }
        });
    }else{
        j$('#loading-curtain-div').hide();
    }
}

/**
 * This function is used to validate, save all the matrix info and then invoke the calculate matrix function in heroku
 *
 @author  Peng Zhu
 @created 2013-06-21
 @version 1.0
 *
 @changelog
 * 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 *
 * 2014-04-01 Jefferson Escobar <jescobar@omegacrmconsulting.com>
 * - Modified
 *
 */
function savaAndCalMatrixsInPage(){
    showLoading();
    if(checkAllRequiredFields(true)){

    	//Chekc if there are not matrices to calculate
    	var qMatrices = 0;
    	j$("input[name='matrixChecked']").each(function(){
    		if(j$(this).is(':checked')){
    			qMatrices++;
    		}
		});

    	if(qMatrices == 0){
             if(confirm(msgMatrix2CalculateWarn)){
            	 IMP_BI_ExtMatrixDefinition.saveMatrixData(collectMatrixValues(true), function(data){
                     j$('#htmlEntity').html(data);
                     data = j$('#htmlEntity').text();
                     data = j$.parseJSON( data );

                     if(data && !data.success){
                         j$("div[id='errorMsg']").html(data.message);
                         j$("div[id='errorMsg']").show();
                         j$('#loading-curtain-div').hide();
                     }
                     else{
                         cancel();
                     }
                 });
             }else{
            	 j$('#loading-curtain-div').hide();
             }
    	}else{
    		IMP_BI_ExtMatrixDefinition.saveMatrixData(collectMatrixValues(true), function(data){
                j$('#htmlEntity').html(data);
                data = j$('#htmlEntity').text();
                data = j$.parseJSON( data );
                if(data && !data.success){
                    j$("div[id='errorMsg']").html(data.message);
                    j$("div[id='errorMsg']").show();
                    j$('#loading-curtain-div').hide();
                }
                else{
                    calculateMatrixInPage();
                }
            });
    	}
    }else{
        j$('#loading-curtain-div').hide();
    }
}


/**
 * This function is used to populate columns value for Launch template
 *
 @author  Peng Zhu
 @created 2013-06-21
 @version 1.0
 *
 @changelog
 * 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 */
function populateColumnsForLaunchTemplate(obj, isInit){
	//console.log('isInit : ' + isInit);
	var j$this = j$(obj);

	var jsonMTObj = jsonMTofLaunch[j$this.val()];
	var j$table = j$(getParent(j$this.get(0), 'TABLE'));
	var j$inputRow = j$("input[id^='theRowInput']", j$table);
	var j$inputColumn = j$("input[id^='theColumnInput']", j$table);
	var j$inputLaunch = j$("input[id^='matrixLaunchPhase']", j$table);
	var j$inputDPA = j$("input[name$='matrixDPA']", j$table);
	var j$inputAllCust = j$("input[name$='matrixAllCustomers']", j$table);

	if(jsonMTObj){
		//console.log('lctId : ' + jsonMTObj.lctId + ', isLaunch : ' + jsonMTObj.isLaunch + ', Row : ' + jsonMTObj.lctRow + ', Column : ' + jsonMTObj.lctColumn);
		//j$inputRow.val(jsonMTObj.lctRow);
		//j$inputColumn.val(jsonMTObj.lctColumn);
		if(!isInit || j$.trim(j$inputRow.val()) == '') j$inputRow.val(jsonMTObj.lctRow);
		if(!isInit || j$.trim(j$inputColumn.val()) == '') j$inputColumn.val(jsonMTObj.lctColumn);

		//j$inputRow.val(jsonMTObj.lctRow);
		//j$inputColumn.val(jsonMTObj.lctColumn);

		if(jsonMTObj.isLaunch){
			//j$inputColumn.val(jsonMTObj.lctColumn);
			//j$inputRow.attr('disabled', 'disabled');
			//j$inputColumn.attr('disabled', 'disabled');
			j$inputColumn.attr('title', errMsg_Launch);
			j$inputLaunch.prop('checked',true);
			//j$inputDPA.prop('checked',false);
			//j$inputDPA.attr('disabled', 'disabled');
			j$inputAllCust.removeAttr('disabled');
		}
		else{
		//	j$inputRow.removeAttr('disabled');
			//j$inputColumn.removeAttr('disabled');
			j$inputColumn.removeAttr('title');
			j$inputLaunch.prop('checked',false);
			//j$inputDPA.removeAttr('disabled');
		}
	}
	else{
	//	j$inputRow.removeAttr('disabled');
		j$inputColumn.removeAttr('disabled');
		j$inputColumn.removeAttr('title');
	}

	hideErrMsgWhenChangeMt(j$this.get(0));
}

/**
 * This function is used to check if each matrix speciality has at least one value
 *
 @author  Peng Zhu
 @created 2013-06-21
 @version 1.0
 *
 @changelog
 * 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 */
function checkAllMatrixSpecialDiv(){
	j$("div[name='matrixSpecial']").not(".finalMatrix").each(function(){
        var j$this = j$(this);
        var isNull = true;

        j$("li", j$this).each(function(){
        	var liId = j$(this).attr('id');
        	if(liId && j$.trim(liId) != '' ){ isNull = false;}
        });

        //get DPA checkbox value
		    var j$table = j$(getParent(j$this.get(0), 'TABLE'));
        var dpa = j$table.find("input[name$='matrixDPA']").get(0);
        //@jescobar 23-02-2017: Removing DPA skip validation
        /**if(dpa && dpa.checked){
            isNull = false;
        }*/
        if( j$("[id$='AccountMatrixCK']").prop('checked') ) isNull = false;//added by leijun

        if(isNull){
        	j$this.siblings('.erMsg').find('font').text(errMsg_Special).parent().show();
        	j$this.children('.erMsg').find('font').text(errMsg_Special).parent().show();
        }else{
        	j$this.siblings('.erMsg').hide();
        	j$this.children('.erMsg').hide();
        }
    });
	j$("div[name='matrixAMS']").not(".finalMatrix").each(function(){
        var j$this = j$(this);
        var isNull = true;

        j$("li", j$this).each(function(){
        	var liId = j$(this).attr('id');
        	if(liId && j$.trim(liId) != '' ){ isNull = false;}
        });

        //get DPA checkbox value
		    var j$table = j$(getParent(j$this.get(0), 'TABLE'));
        var dpa = j$table.find("input[name$='matrixDPA']").get(0);
        //@jescobar 23-02-2017: Removing DPA skip validation
        /**if(dpa && dpa.checked){
            isNull = false;
        }*/
        if( j$("[id$='AccountMatrixCK']").prop('checked') ) isNull = false;//added by leijun

        if(isNull){
        	j$this.siblings('.erMsg').find('font').text(errMsg_Special).parent().show();
        	j$this.children('.erMsg').find('font').text(errMsg_Special).parent().show();
        }else{
        	j$this.siblings('.erMsg').hide();
        	j$this.children('.erMsg').hide();
        }
    });
	j$("div[name='matrixSm']").not(".finalMatrix").each(function(){
        var j$this = j$(this);
        var isNull = true;

        j$("li", j$this).each(function(){
        	var liId = j$(this).attr('id');
        	if(liId && j$.trim(liId) != '' ){ isNull = false;}
        });

        //get DPA checkbox value
		    var j$table = j$(getParent(j$this.get(0), 'TABLE'));
        var dpa = j$table.find("input[name$='matrixDPA']").get(0);
        //@jescobar 23-02-2017: Removing DPA skip validation
        /**if(dpa && dpa.checked){
            isNull = false;
        }*/
        if( j$("[id$='AccountMatrixCK']").prop('checked') ) isNull = false;//added by leijun

        if(isNull){
        	j$this.siblings('.erMsg').find('font').text(errMsg_Special).parent().show();
        	j$this.children('.erMsg').find('font').text(errMsg_Special).parent().show();
        }else{
        	j$this.siblings('.erMsg').hide();
        	j$this.children('.erMsg').hide();
        }
    });
	j$("div[name='matrixCR']").not(".finalMatrix").each(function(){
        var j$this = j$(this);
        var isNull = true;

        j$("li", j$this).each(function(){
        	var liId = j$(this).attr('id');
        	if(liId && j$.trim(liId) != '' ){ isNull = false;}
        });

        //get DPA checkbox value
		    var j$table = j$(getParent(j$this.get(0), 'TABLE'));
        var dpa = j$table.find("input[name$='matrixDPA']").get(0);
        //@jescobar 23-02-2017: Removing DPA skip validation
        /**if(dpa && dpa.checked){
            isNull = false;
        }*/
        if( j$("[id$='AccountMatrixCK']").prop('checked') ) isNull = false;//added by leijun

        if(isNull){
        	j$this.siblings('.erMsg').find('font').text(errMsg_Special).parent().show();
        	j$this.children('.erMsg').find('font').text(errMsg_Special).parent().show();
        }else{
        	j$this.siblings('.erMsg').hide();
        	j$this.children('.erMsg').hide();
        }
    });
	j$("div[name='matrixDT']").not(".finalMatrix").each(function(){
        var j$this = j$(this);
        var isNull = true;

        j$("li", j$this).each(function(){
        	var liId = j$(this).attr('id');
        	if(liId && j$.trim(liId) != '' ){ isNull = false;}
        });

        //get DPA checkbox value
		    var j$table = j$(getParent(j$this.get(0), 'TABLE'));
        var dpa = j$table.find("input[name$='matrixDPA']").get(0);
        //@jescobar 23-02-2017: Removing DPA skip validation
        /**if(dpa && dpa.checked){
            isNull = false;
        }*/
        if( j$("[id$='AccountMatrixCK']").prop('checked') ) isNull = false;//added by leijun

        if(isNull){
        	j$this.siblings('.erMsg').find('font').text(errMsg_Special).parent().show();
        	j$this.children('.erMsg').find('font').text(errMsg_Special).parent().show();
        }else{
        	j$this.siblings('.erMsg').hide();
        	j$this.children('.erMsg').hide();
        }
    });
}

function checkDpaForAllCustomer(obj){
	if(obj){
		var j$allCustomers = j$(getParent(obj, 'TBODY')).find('input[name$=matrixAllCustomers]');
		var j$lauchPhase = j$(getParent(obj, 'TBODY')).find("input[id^='matrixLaunchPhase']");
		var j$divCurrSpecial = j$(getParent(obj, 'TBODY')).find('div.specialCls');
		var j$selectMT = j$(getParent(obj, 'TBODY')).find('select[name$=matrixTemplate]');

		if(obj.checked){

			j$("li.pgover", j$divCurrSpecial).each(function(){
				var duplicateCounter = 0, currentId = j$(this).attr('id'), j$this = j$(this);

				//j$("div[name='matrixSpecial'] li").each(function(){
				//	var liId = j$(this).attr('Id');
	       		//	if(liId && liId == currentId) duplicateCounter ++;
	    		//});


				var j$lis = j$("#specialDivId").find('li');
	        	j$lis.each(function(){
	        		if(currentId == j$(this).attr('id')){
	        			if(duplicateCounter < 2){
	        				j$(this).removeClass('ui-state-disabled').removeClass('special-state-disabled').css({'opacity': 1});
		        			j$(this).off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
	        			}
	        			j$this.attr({'id':''}).text('').addClass('ui-state-disabled').removeClass('pgover');
	        		}
	        	});
			});

			//j$allCustomers.removeAttr('checked').attr('disabled', 'disabled');
			//j$lauchPhase.prop('checked',false);
			j$selectMT.attr('disabled', 'disabled');
			j$selectMT.val('');
		}
		else{
			//j$allCustomers.removeAttr('disabled');
			//j$divCurrSpecial.droppable({ disabled: false });//Set enable droppable div
			j$selectMT.removeAttr('disabled', 'disabled').val('');
		}
	}
}


/**
 * Check all the matrices for being calculated
 * @param obj is checked or isn't for checking all the matrices in the page
 * @return
 */
function checkAllMatrices(obj){
	if(obj && obj.checked){//Checked all matrices
		j$("input[name='matrixChecked']").each(function(){
			j$(this).prop('checked',true);
		});
	}else{//Unchecked all matrices
		j$("input[name='matrixChecked']").each(function(){
			j$(this).prop('checked',false);
		});
	}
}


/**
 * Fill out specialties on matrix from matrix template selected
 *
 * @param obj Picklist matrix template
 * @param template info record selected
 */
function setSpecialtiesFromTemplate(obj,template){
	var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.specialCls'), targetId, j$dragTarget, j$dragCurrent;
	var specialties  = (template.Specialty_Ids_BI__c!=null&&j$.trim(template.Specialty_Ids_BI__c)!='') ? template.Specialty_Ids_BI__c : null;
	var isIncluded = false;
	var spAllIncluded = 0;

	if(specialties){
		//Check if there are specialties inclued in the matrix definition
		j$divTarget.find('li').each(function(){
			var spId = j$(this).attr('Id');

			if(j$.trim(spId) != '' && specialties.indexOf(spId) > -1){
				alert(msgSpecialtiesIncluded);
				isIncluded = true;
				return false;
				//console.log(':: Specilaty: ' + j$(this).html());
			}
		});

		j$("li:not(.special-state-disabled)", j$("#specialDivId")).each(function(){
			j$dragCurrent = j$(this);
			var currentId = j$dragCurrent.attr('id');
			var hasFinded = false;
			var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));
			var countSPIncluded = 0;

			if(specialties.indexOf(currentId) > -1){
				//console.log(':: Specialty: ' + currentId);
				if(j$div.hasClass('overflowHidden')){
					j$div.removeClass('overflowHidden');
				}

				j$divTarget.find('li').each(function(){
					if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
						if(!hasFinded){
							hasFinded = true;
							j$dragTarget = j$(this), targetId = j$(this).attr('id');
						}
					}
				});
    			j$('.matrixDraggable').not(".ui-state-disabled").off('mouseleave mouseenter mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
				//if has not find any available li, do nothing
				if(typeof(j$dragTarget) != 'undefined'){
					j$dragTarget.attr({'id': currentId})
						.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
					j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
					j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
				}
				//Begin: added by Peng Zhu 2013-08-30
				else{
					j$dragTarget = j$('<li id="' + currentId + '" class="matrixDraggable pgclass">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
					j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
					j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
					j$(drag.target).find('ul').append(j$dragTarget);
				}
			}
		});

		//Validate if there are specialties available in the template to fill out the form
		spAllIncluded = j$divTarget.find('li.pgclass').length;
		if(spAllIncluded == 0){
			alert('All the specialties for this template have  already included in others definitions');
		}
	}
}


function checkAllCustomers(obj){
	//get true or false
	if(obj){
		var accountMatrix = j$("[id$='AccountMatrixCK']").prop('checked');
		if(accountMatrix == true) var accountMatrixtype = j$("[id*='AMTSelect']").val();
		var j$dpa = j$(getParent(obj, 'TBODY')).find('input[name$=matrixDPA]');
		var j$lauchPhase = j$(getParent(obj, 'TBODY')).find("input[id^='matrixLaunchPhase']");

		var targetId = undefined, j$dragTarget = undefined, j$dragCurrent = undefined;

		if(obj.checked){
			if(!(accountMatrix && accountMatrixtype == 'HCO only')){
				var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.specialCls');
				j$("li:not(.special-state-disabled)", j$("#specialDivId")).each(function(){
					j$dragCurrent = j$(this);
					var currentId = j$dragCurrent.attr('id');

					var hasFinded = false;

					//added by Peng Zhu 2013-06-25 for overflowHidden
					var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));

					if(j$div.hasClass('overflowHidden')){
						j$div.removeClass('overflowHidden');
					}

					j$dragTarget = undefined;
					j$divTarget.find('li').each(function(){
						if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
							if(!hasFinded){
								hasFinded = true;
								j$dragTarget = j$(this), targetId = j$(this).attr('id');
							}
						}
					});
					//if has not find any available li, do nothing
					if(typeof(j$dragTarget) != 'undefined'){
						j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
							.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
						j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
					}
					//Begin: added by Peng Zhu 2013-08-30
					else{
						j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
						j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						j$divTarget.find('.targerDropUlCls').append(j$dragTarget);
					}
					//End: added by Peng Zhu 2013-08-30
				});
				j$divTarget.find('.erMsg').hide();
			}else{
				var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.AMSmatrixCls ');
				j$("li:not(.special-state-disabled)", j$("#AccMSDivBody")).each(function(){
					j$dragCurrent = j$(this);
					var currentId = j$dragCurrent.attr('id');

					var hasFinded = false;

					//added by Peng Zhu 2013-06-25 for overflowHidden
					var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));

					if(j$div.hasClass('overflowHidden')){
						j$div.removeClass('overflowHidden');
					}

						j$dragTarget = undefined;
					j$divTarget.find('li').each(function(){
						if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
							if(!hasFinded){
								hasFinded = true;
								j$dragTarget = j$(this), targetId = j$(this).attr('id');
							}
						}
					});
					//if has not find any available li, do nothing
					if(typeof(j$dragTarget) != 'undefined'){
						j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
							.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
						j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
					}
					//Begin: added by Peng Zhu 2013-08-30
					else{
						j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
						j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						j$divTarget.find('ul').append(j$dragTarget);
					}
					//End: added by Peng Zhu 2013-08-30
				});
				j$divTarget.find('.erMsg').hide();
				var targetId = undefined, j$dragTarget = undefined, j$dragCurrent = undefined;
				var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.SmatrixCls');
				var ulli = j$divTarget.find("li");
				j$("li:not(.special-state-disabled)", j$("#specialDivBody")).each(function(){
					var isRepeat = false;
					for(var i = 0; i< ulli.length; i++){
						var li = ulli[i];
						if(j$(li).attr("id") == j$(this).attr('id')){
							isRepeat = true;
							break;
						}
					}
					if(!isRepeat){
						j$dragCurrent = j$(this);
						var currentId = j$dragCurrent.attr('id');

						var hasFinded = false;

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));

						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

							j$dragTarget = undefined;
						j$divTarget.find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});//@jescobar: disable specialties pattern2 matrix
							j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});//@jescobar: disable specialties pattern2 matrix
							j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$divTarget.find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30
					}
				});
				j$divTarget.find('.erMsg').hide();
				var targetId = undefined, j$dragTarget = undefined, j$dragCurrent = undefined;
				var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.CRmatrixCls');
				var ulli = j$divTarget.find("li");
				j$("li:not(.special-state-disabled)", j$("#SCRoleDivBody")).each(function(){
					var isRepeat = false;
					for(var i = 0; i< ulli.length; i++){
						var li = ulli[i];
						if(j$(li).attr("id") == j$(this).attr('id')){
							isRepeat = true;
							break;
						}
					}
					if(!isRepeat){
						j$dragCurrent = j$(this);
						var currentId = j$dragCurrent.attr('id');

						var hasFinded = false;

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));

						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}

							j$dragTarget = undefined;
						j$divTarget.find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
							j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
							j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$divTarget.find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30
					}
				});
				j$divTarget.find('.erMsg').hide();
				var targetId = undefined, j$dragTarget = undefined, j$dragCurrent = undefined;
				var j$divTarget = j$(getParent(obj, 'TBODY')).find('div.DTmatrixCls');
				var ulli = j$divTarget.find("li");
				j$("li:not(.special-state-disabled)", j$("#DepTDivBody")).each(function(){
					var isRepeat = false;
					for(var i = 0; i< ulli.length; i++){
						var li = ulli[i];
						if(j$(li).attr("id") == j$(this).attr('id')){
							isRepeat = true;
							break;
						}
					}
					if(!isRepeat){
						j$dragCurrent = j$(this);
						var currentId = j$dragCurrent.attr('id');

						var hasFinded = false;

						//added by Peng Zhu 2013-06-25 for overflowHidden
						var j$div = j$(getParent(j$dragCurrent.get(0), 'DIV'));

						if(j$div.hasClass('overflowHidden')){
							j$div.removeClass('overflowHidden');
						}
							j$dragTarget = undefined;
						j$divTarget.find('li').each(function(){
							if(typeof(j$(this).attr('id')) == 'undefined' || j$(this).attr('id') == ''){	//cleart old channel
								if(!hasFinded){
									hasFinded = true;
									j$dragTarget = j$(this), targetId = j$(this).attr('id');
								}
							}
						});
						//if has not find any available li, do nothing
						if(typeof(j$dragTarget) != 'undefined'){
							j$dragTarget.attr({'id': currentId}).attr({'name': j$dragCurrent.find('span').attr('title')}).attr({'num':j$.trim(j$dragCurrent.attr('num'))})
								.addClass('pgclass').html(j$dragCurrent.find('span').attr('title')).removeClass('ui-state-disabled').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
							j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
						}
						//Begin: added by Peng Zhu 2013-08-30
						else{
							j$dragTarget = j$('<li id="' + currentId + '" num="' + j$.trim(j$dragCurrent.attr('num')) + '" class="matrixDraggable pgclass" name="' + j$dragCurrent.find('span').attr('title') + '">' + j$dragCurrent.find('span').attr('title') + '</li>').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							//j$dragCurrent.addClass('ui-state-disabled').addClass('special-state-disabled').css({'opacity': 1});
							j$dragCurrent.removeClass('pgover').off('mouseenter mouseleave mousedown mouseup').on('mouseleave mouseenter mousedown mouseup', e_over_draggable);
							j$divTarget.find('ul').append(j$dragTarget);
						}
						//End: added by Peng Zhu 2013-08-30
					}
				});
				j$divTarget.find('.erMsg').hide();
			}
		}
	}

	//if true -- get all available special and set them to current matrix

	//DPA disable

}

/**
 * This function is used to check if each matrix speciality has at least one value
 *
 @author  Peng Zhu
 @created 2013-06-21
 @version 1.0
 *
 @changelog
 * 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 */
function getLaunchTemplateValue(j$obj){
	j$this = j$obj;

   	var j$table = j$(getParent(j$this.get(0), 'TABLE'));
	var j$mtSelect = j$("select[name$='theMTSelecrList']" ,j$table);
	var jsonMTObj = jsonMTofLaunch[j$mtSelect.val()];
	if(jsonMTObj && jsonMTObj.isLaunch){
		return jsonMTObj.lctColumn;
	}

	return '';
}
/**
 * This function is used to hide the column errmsg when template select changed
 *
 @author  Peng Zhu
 @created 2013-06-21
 @version 1.0
 *
 @changelog
 * 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 */
function hideErrMsgWhenChangeMt(obj){
	j$this = j$(obj);

   	var j$table = j$(getParent(j$this.get(0), 'TABLE'));

   	var j$inputColumn = j$("input[id^='theColumnInput']", j$table), j$errMsg = j$inputColumn.siblings('.erMsg');

   	j$errMsg.hide();

   	var v = j$inputColumn.val();

	if(typeof v == 'undefined') v = '';
	v = j$.trim(v.replace(/[^\d]/g, ''));

    if(v != ''){
		v = parseInt(v);

		if(v && v < 1 || v > 21){
			j$errMsg.find('font').text(errMsg_Column).parent().show();
		}
    }
    else{
       j$errMsg.find('font').text(errMsg_Column_Null).parent().show();
    }

    j$inputColumn.val(v);
}

/**
 * Set disabled matrix final fields
 *
 @author  Peng Zhu
 @created 2013-06-21
 @version 1.0
 *
 @changelog
 * 2013-06-21 Peng Zhu <peng.zhu@itbconsult.com>
 * - Created
 */
//
function disableAllFinalMatrix(){
	j$("#matriDivId table.finalMatrix input[name$='matrixDPA']").attr('disabled', 'disabled');
	j$("#matriDivId table.finalMatrix input[name$='matrixAllCustomers']").attr('disabled', 'disabled');
	j$("#matriDivId table.finalMatrix input[name$='matrixAdoptionLabel']").attr('disabled', 'disabled');
	j$("#matriDivId table.finalMatrix input[name$='matrixPotentialLabel']").attr('disabled', 'disabled');
}
