window.onload = setTabName;
        
var parentTabId;
var childTabId;
function setTabName() 
{
    if(sforce.console.isInConsole()){
        try{
            sforce.console.getEnclosingPrimaryTabId(assignTabId);
        }catch(ex){}
    }
}
          
var assignTabId = function assignTabId(result){
    parentTabId = result.id;
              
    sforce.console.getEnclosingTabId(assignChildTabId);
};
        
var assignChildTabId = function assignTabId(result){
    childTabId = result.id;
};
        
function openURL(url){
    window.location = url;
}
        
function openTab(url, name){
    sforce.console.openSubtab(parentTabId, url, true, name, null, openSuccess);
        
    if(parentTabId == 'null' || parentTabId == null || parentTabId == ''){
        sforce.console.openPrimaryTab(null, url, true, name);
    }
}
        
function refreshSubtabAndCloseKnowledge(refreshTabId){
    sforce.console.refreshSubtabById(refreshTabId, true, refreshSuccess);
}
        
var refreshSuccess = function refreshSuccess(result) { 
    try{
        sforce.console.closeTab(childTabId);
    }catch(ex){}
};
   
var openSuccess = function openSuccess(result) {};	