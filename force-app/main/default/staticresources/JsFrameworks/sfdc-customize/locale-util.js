/**
 * Locale Utils for dealing with lcoale problems in SFDC, like Date, Time Zone and Numbers.
 * Author: Lingjun Jiang (lingjun.jiang@itbconsult.com)
 * Created: 2014-01-22
 * Version: 1.0
 * 
 * Dependencies: accountingjs, datejs
 * To use this tool, please expose below code to view state of your controllers,
 * Decimal value = 1000.10;
 * String formattedValue = value.format();
 * String thousandSep = formattedValue.substring(1,2);
 * String decimalSep = formattedValue.substring(5,6);
 * Integer timeZoneOffset = -UserInfo.getTimeZone().getOffset(Date.today());
 * 
 * And create a new LocaleUtil instance with below example code.
 * var localeUtil = new LocaleUtil({
 *   localeThousandSep: "{!thousandSep}",
 *   localeDecimalSep: "{!decimalSep}",
 *   localeDateFormat: UserContext.dateFormat,
 *   localeDateTimeFormat: UserContext.dateTimeFormat,
 *   localeTimeFormat: UserContext.timeFormat,
 *   localeTimeZoneOffset: {!timeZoneOffset},
 *   browserTimeZoneOffset: new Date().getTimezoneOffset()*60*1000
 * });
 *
 */

LocaleUtil = function(options) {
    options = options ? _.clone(options) : {};
    this.precision = options.precision ? options.precision : 2;
    this.localeThousandSep = options.localeThousandSep ? options.localeThousandSep : ".";
    this.localeDecimalSep = options.localeDecimalSep ? options.localeDecimalSep : ",";
    this.standardDateFormat = options.standardDateFormat ? options.standardDateFormat : "yyyy-MM-dd";
    this.localeDateFormat = options.localeDateFormat ? options.localeDateFormat : "dd.MM.yyyy";
    this.localeDateTimeFormat = options.localeDateTimeFormat ? options.localeDateTimeFormat : "dd.MM.yyyy HH:mm";
    this.localeTimeFormat = options.localeTimeFormat ? options.localeTimeFormat : "HH:mm";
    this.localeTimeZoneOffset = options.localeTimeZoneOffset ? options.localeTimeZoneOffset : "0";
    this.browserTimeZoneOffset = options.browserTimeZoneOffset ? options.browserTimeZoneOffset : "0";
};

LocaleUtil.prototype.formatNumber = function(num) {
    return accounting.formatNumber(num, this.precision, this.localeThousandSep, this.localeDecimalSep);
};

LocaleUtil.prototype.unformatNumber = function(s) {
    return accounting.unformat(s, this.localeDecimalSep);
};

LocaleUtil.prototype.isValidNumberFormat = function(s) {
    // Number: Currency amount (cents mandatory) Optional thousands separators; mandatory two-digit fraction
    // ^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*\.[0-9]{2}$
    // Number: Currency amount (cents optional) Optional thousands separators; optional two-digit fraction
    // ^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$
    // Number: Currency amount US & EU (cents optional) Can use US-style 123,456.78 notation and European-style 123.456,78 notation. Optional thousands separators; optional two-digit fraction
    // ^[+-]?[0-9]{1,3}(?:[0-9]*(?:[.,][0-9]{2})?|(?:,[0-9]{3})*(?:\.[0-9]{2})?|(?:\.[0-9]{3})*(?:,[0-9]{2})?)$
    // For EU only
    // ^[+-]?[0-9]{1,3}(?:[0-9]*(?:[,][0-9]{0,2})?|(?:\.[0-9]{3})*(?:,[0-9]{0,2})?)$
    // For US only
    // ^[+-]?[0-9]{1,3}(?:[0-9]*(?:[.][0-9]{0,2})?|(?:,[0-9]{3})*(?:\.[0-9]{0,2})?)$
    return s.match(new RegExp("^[+-]?[0-9]{1,3}(?:[0-9]*(?:[" + this.localeDecimalSep + "][0-9]{0," + this.precision + "})?|(?:\\" + this.localeThousandSep + "[0-9]{3})*(?:\\" + this.localeDecimalSep + "[0-9]{0," + this.precision + "})?)$")) != null;
};

LocaleUtil.prototype.parseDate = function(s) {
    return Date.parseExact(s, this.localeDateFormat);
}

LocaleUtil.prototype.formatDate = function(d) {
    return d.toString(this.localeDateFormat);
}

LocaleUtil.prototype.formatDateInStandardFmr = function(d) {
    return d.toString(this.localeDateFormat);
}

LocaleUtil.prototype.locale2BrowserDate = function(dateInMs) {
    return new Date(dateInMs + (this.localeTimeZoneOffset - this.browserTimeZoneOffset));
}

LocaleUtil.prototype.formatLocale2Browser = function(dateInMs) {
    return this.formatDate(this.locale2BrowserDate(dateInMs));
}