$('div.collapsible div.plan-name').click(function(e) {            
    var $this = $(this);
    $this.parent().children('.collapsible').toggle();            
    $this.parent().find('i.icon-folder-open').first().toggle();
    $this.parent().find('i.icon-folder-close').first().toggle();
    e.stopPropagation();
});

$('.icon-briefcase').click(function(e) {
    var modalId = $(this).parent().attr('href');
    console.log(modalId);
    $(modalId).modal('show');
    console.log(this);
    e.stopPropagation();
});

$('.plan-link').click(function(e){
    e.stopPropagation();
});

$(function(){
    $('.briefcase-link').tooltip({
        'placement':'bottom',
        'trigger':'hover'
    });
});