trigger AP_ASSIGNED_TO_OWNERID on Account_Plan_vod__c (before insert) {

    //assigned to ownerid
    for(Account_Plan_vod__c ap : trigger.new)
    {
       ap.Assigned_To_BI__c = ap.OwnerId;
    }
    
}