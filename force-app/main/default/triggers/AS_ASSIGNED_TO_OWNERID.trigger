trigger AS_ASSIGNED_TO_OWNERID on Account_Tactic_vod__c (before insert) {

    //assigned to ownerid
    for(Account_Tactic_vod__c at : trigger.new)
    {
       if (at.Assigned_To_BI__c == NULL) {at.Assigned_To_BI__c = at.OwnerId;}
    }
    
}