trigger AS_ASSIGNED_TO_OWNERID_CALL_OBJECTIVE on Call_Objective_vod__c (before insert) {

    //assigned to ownerid
    for(Call_Objective_vod__c at : trigger.new)
    {
       if (at.Assigned_To_BI__c == NULL) {at.Assigned_To_BI__c = at.OwnerId;}
    }
    
}