/*
 *  AttachmentMVN
 *  Created By:     Roman Lerman
 *  Created Date:   4/8/2013
 *  Description:    This is a generic Attachment trigger used for calling any Attachment logic
 */
trigger AttachmentMVN on Attachment (before insert, before update, before delete) {
	String parentId = 'ParentId';
	String objectName = 'Case';
	String lockedFieldName = 'IsClosed';
	String parentKeyPrefix = Schema.SobjectType.Case.getKeyPrefix();
	String error = Label.Cannot_Modify_Record_on_Closed_Case;
	
	String parentId2 = 'ParentId';
	String objectName2 = 'Fulfillment_MVN__c';
	String lockedFieldName2 = 'Is_Closed_MVN__c';
	String parentKeyPrefix2 = Schema.SobjectType.Fulfillment_MVN__c.getKeyPrefix();
	String error2 = Label.Cannot_Modify_Record_on_Closed_Fulfillment;
	
	new TriggersMVN()
		.bind(TriggersMVN.Evt.beforeinsert, new LockRelatedRecordsMVN(parentId, objectName, lockedFieldName, parentKeyPrefix, error))
        .bind(TriggersMVN.Evt.beforeupdate, new LockRelatedRecordsMVN(parentId, objectName, lockedFieldName, parentKeyPrefix, error))
        .bind(TriggersMVN.Evt.beforedelete, new LockRelatedRecordsMVN(parentId, objectName, lockedFieldName, parentKeyPrefix, error))
        
        .bind(TriggersMVN.Evt.beforeinsert, new LockRelatedRecordsMVN(parentId2, objectName2, lockedFieldName2, parentKeyPrefix2, error2))
        .bind(TriggersMVN.Evt.beforeupdate, new LockRelatedRecordsMVN(parentId2, objectName2, lockedFieldName2, parentKeyPrefix2, error2))
        .bind(TriggersMVN.Evt.beforedelete, new LockRelatedRecordsMVN(parentId2, objectName2, lockedFieldName2, parentKeyPrefix2, error2))
        .manage();
}