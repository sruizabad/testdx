trigger BI_ATL_ChangeAfterUpdateVODAccounts on Account_Territory_Loader_vod__c (after update, after insert) {
    BI_ATL_BO.getInstance().createGASHistoryByATLChanged(Trigger.new, Trigger.oldMap);
}