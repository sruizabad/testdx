trigger BI_CM_Insight_comment_Trigger on BI_CM_Insight_Comment__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	BI_CM_TriggerFactory.createHandler(BI_CM_Insight_Comment__c.sObjectType); 
}