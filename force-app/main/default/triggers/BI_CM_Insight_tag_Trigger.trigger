trigger BI_CM_Insight_tag_Trigger on BI_CM_Insight_tag__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    BI_CM_TriggerFactory.createHandler(BI_CM_Insight_tag__c.sObjectType); 
}