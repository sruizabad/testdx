/*
Name: BI_MM_Budget
Requirement ID: Budget Trigger
Description: Trigger to update budget records.
Version | Author-Email | Date | Comment
1.0 | Mukesh | 18.01.2016 | initial version
*/
trigger BI_MM_Budget on BI_MM_Budget__c (before insert, after insert, before update, after update, after delete) {

    BI_MM_BudgetHandler objBudgetHandler = new BI_MM_BudgetHandler();

    if(Trigger.isInsert && Trigger.isAfter) {
        if(BI_MM_RecursionHandler.isInsertSpainTrigger) {
            BI_MM_RecursionHandler.isInsertSpainTrigger = false;
            objBudgetHandler.onAfterInsert(Trigger.OldMap, Trigger.NewMap);
        }
    }

    if(Trigger.isUpdate && Trigger.isBefore) {

        //if(BI_MM_RecursionHandler.isBeforeUpdateBudgetTrigger) {
            //BI_MM_RecursionHandler.isBeforeUpdateBudgetTrigger = false;
            objBudgetHandler.onBeforeUpdate(Trigger.NewMap);
        //}
    }

    if(Trigger.isUpdate && Trigger.isAfter) {

        if(BI_MM_RecursionHandler.isAfterUpdateBudgetTrigger) {
            BI_MM_RecursionHandler.isAfterUpdateBudgetTrigger = false;
            objBudgetHandler.onAfterUpdate(Trigger.OldMap, Trigger.NewMap);
        }
    }

    if(Trigger.isDelete && Trigger.isAfter) {

        if(BI_MM_RecursionHandler.isDeleteTrigger) {
            BI_MM_RecursionHandler.isDeleteTrigger = false;
            objBudgetHandler.onAfterDelete(Trigger.OldMap, Trigger.NewMap);
        }
    }
}