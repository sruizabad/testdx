/*
Name: BI_MM_BudgetEvent
Requirement ID: collaboration
Description: This is trigger is used to collaboration functinality
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari |  02/01/2016 | initial version
*/
trigger BI_MM_BudgetEvent on BI_MM_BudgetEvent__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    new BI_MM_BudgetEventHandler().run();
    /**BI_MM_BudgetEventHandler objBudgetEventHandler = new BI_MM_BudgetEventHandler();
    BI_MM_BudgetExpenseHandler objBudgetExpenseHandler = new BI_MM_BudgetExpenseHandler();

    if(Trigger.isUpdate && Trigger.isBefore) {
        objBudgetEventHandler.onBeforeUpdate(Trigger.OldMap, Trigger.NewMap);
    }

    if(Trigger.isUpdate && Trigger.isAfter){
        if(BI_MM_RecursionHandler.isUpdateBudgetEvenTrigger) {
           BI_MM_RecursionHandler.isUpdateBudgetEvenTrigger = false;
           System.debug('====Buget Event trigger=====3===');
           objBudgetEventHandler.onAfterUpdate(Trigger.OldMap, Trigger.NewMap);
        }
        //objBudgetExpenseHandler.onAfterUpdate(Trigger.OldMap, Trigger.NewMap);
    }*/
}