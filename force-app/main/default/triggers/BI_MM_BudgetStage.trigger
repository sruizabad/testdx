/* 
Name: BI_MM_BudgetStage
Requirement ID: Budget Stage Trigger
Description: Trigger to update budget records.
Version | Author-Email | Date | Comment 
1.0 | Mukesh | 14.12.2015 | initial version 
*/
trigger BI_MM_BudgetStage on BI_MM_BudgetStage__c (after insert, before insert, after update) {

    BI_MM_BudgetStageHandler objBI_MM_BudgetStageHandler = new BI_MM_BudgetStageHandler();
    
    if(Trigger.isInsert && Trigger.isAfter){
        
        System.debug('======insert Trigger=======');
        objBI_MM_BudgetStageHandler.onAfterInsert(Trigger.new);        
    }
    
    if(Trigger.isInsert && Trigger.isBefore){
        objBI_MM_BudgetStageHandler.onBeforeInsert(Trigger.new);
    }
    
    if(Trigger.isUpdate && Trigger.isAfter){
        
        System.debug('======Update Trigger=======');
        objBI_MM_BudgetStageHandler.onAfterUpdate(Trigger.oldMap, Trigger.newMap);
    }
    
}