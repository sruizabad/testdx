/*
Name: BI_MM_MedicalEvent
Requirement ID: Collaboration Type 1
Description: This Class is used to create event budget on creation of Medical event.
Version | Author-Email | Date | Comment
1.0 | Mukesh Tiwari | 29.12.2015 | initial version
*/
trigger BI_MM_MedicalEvent on Medical_Event_vod__c (before insert, after insert, before update, after update, after delete, before delete) {

    BI_MM_MedicalEventHandler objBI_MM_MedicalEventHandler = new BI_MM_MedicalEventHandler();

    /**
     * This will call the method to check the event program
     *
     *
     * Author Omega CRM
     */

    if(Trigger.isInsert && Trigger.isBefore){
        System.debug('***BeforeInsert Trigger');
        objBI_MM_MedicalEventHandler.onBeforeInsert(Trigger.new);
    }
    

    if(Trigger.isInsert && Trigger.isAfter){

        if(BI_MM_RecursionHandler.isInsertTrigger) {
            BI_MM_RecursionHandler.isInsertTrigger = false;
            objBI_MM_MedicalEventHandler.onAfterInsert(Trigger.new);
        }
    }

    if(Trigger.isUpdate && Trigger.isBefore) {
      system.debug(':: Medical Event Trigger :: BeforeUpdate');
      objBI_MM_MedicalEventHandler.onBeforeUpdate(Trigger.OldMap, Trigger.NewMap);
    }

    if(Trigger.isUpdate && Trigger.isAfter) {
      //if(BI_MM_RecursionHandler.isUpdateMedicalEventTrigger && BI_MM_RecursionHandler.entries  < 2)
        //{
          //BI_MM_RecursionHandler.entries+=1;
          system.debug(':: Medical Event Trigger :: ');
          objBI_MM_MedicalEventHandler.onAfterUpdate(Trigger.OldMap, Trigger.NewMap);
        //}
    }
    /**
     * This will call the method to retrieve the events budget of the Medical Event to be deleted
     *
     *
     * Author Omega CRM
     */
    if(Trigger.isBefore && Trigger.isDelete){
        objBI_MM_MedicalEventHandler.onBeforeDelete(Trigger.old);
    }
    /**
     * This will call the method to delete the events budget of the Medical Event deleted
     *
     *
     * Author Omega CRM
     */
    if(Trigger.isDelete && Trigger.isAfter){
      objBI_MM_MedicalEventHandler.onAfterDelete(Trigger.OldMap);
    }
}