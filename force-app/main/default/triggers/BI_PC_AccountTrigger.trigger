/***********************************************************************************************************
* @date 01/08/2018 (dd/mm/yyyy)
* @description PC Account Tigger (MA-DOTS)
************************************************************************************************************/
trigger BI_PC_AccountTrigger on BI_PC_Account__c (after update) {

	BI_PC_AccountTriggerHandler handler = new BI_PC_AccountTriggerHandler(Trigger.isExecuting, Trigger.size);
		
	if(Trigger.isUpdate && Trigger.isAfter){
		handler.OnAfterUpdate(Trigger.new, Trigger.oldMap);
	}	
}