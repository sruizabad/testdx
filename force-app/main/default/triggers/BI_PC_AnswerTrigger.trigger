trigger BI_PC_AnswerTrigger on BI_PC_Answer__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

	BI_PC_AnswerTriggerHandler handler = new BI_PC_AnswerTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isInsert && Trigger.isBefore){
		handler.OnBeforeInsert(Trigger.new);
	} else if(Trigger.isDelete && Trigger.isAfter){
		handler.OnAfterDelete(Trigger.old);
	}
	
}