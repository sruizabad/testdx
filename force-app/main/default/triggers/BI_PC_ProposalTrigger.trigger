trigger BI_PC_ProposalTrigger on BI_PC_Proposal__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

	BI_PC_ProposalTriggerHandler handler = new BI_PC_ProposalTriggerHandler(Trigger.isExecuting, Trigger.size);
		
	if (Trigger.isInsert && Trigger.isBefore) {
		handler.OnBeforeInsert(Trigger.new);
	} else if(Trigger.isUpdate && Trigger.isBefore){
		handler.OnBeforeUpdate(Trigger.new, Trigger.oldMap);
	} else if(Trigger.isUpdate && Trigger.isAfter){
		handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
	}	
}