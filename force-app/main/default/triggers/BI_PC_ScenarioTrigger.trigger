trigger BI_PC_ScenarioTrigger on BI_PC_Scenario__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

	BI_PC_ScenarioTriggerHandler handler = new BI_PC_ScenarioTriggerHandler(Trigger.isExecuting, Trigger.size);
	
	
	/*if (Trigger.isInsert && Trigger.isBefore) {
		//handler.OnBeforeInsert(Trigger.new);
	}
 	else if (Trigger.isInsert && Trigger.isAfter) {
		//handler.OnAfterInsert(Trigger.new);
		//BI_PC_ScenarioTriggerHandler.OnAfterInsertAsync(Trigger.newMap.keySet());
	}
	else if(Trigger.isUpdate && Trigger.isBefore){
		handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
	}
	else
	*/
	
	if(Trigger.isUpdate && Trigger.isAfter){
		handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
		//BI_PC_ScenarioTriggerHandler.OnAfterUpdateAsync(Trigger.newMap.keySet());
	}
	/*
	else if(Trigger.isDelete && Trigger.isBefore){
		//handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
	}
	else if(Trigger.isDelete && Trigger.isAfter){
		//handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
		//BI_PC_ScenarioTriggerHandler.OnAfterDeleteAsync(Trigger.oldMap.keySet());
	}
	
	else if(Trigger.isUnDelete){
		//handler.OnUndelete(Trigger.new);	
	}
	*/
	
}