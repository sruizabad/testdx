trigger BI_PL_BusinessRuleTrigger on BI_PL_Business_rule__c (before insert, before update) {
    BI_PL_TriggerFactory.createHandler(BI_PL_Business_rule__c.sObjectType);
}