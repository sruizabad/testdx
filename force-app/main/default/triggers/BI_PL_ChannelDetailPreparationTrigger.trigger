trigger BI_PL_ChannelDetailPreparationTrigger on BI_PL_Channel_detail_preparation__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {

	BI_PL_TriggerFactory.createHandler(BI_PL_Channel_detail_preparation__c.sObjectType);
}