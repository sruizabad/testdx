trigger BI_PL_CycleTrigger on BI_PL_Cycle__c (before insert) {
	BI_PL_TriggerFactory.createHandler(BI_PL_Cycle__c.sObjectType);
}