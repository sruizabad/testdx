trigger BI_PL_DetailPreparationTrigger on BI_PL_Detail_preparation__c (before insert, before update) {
	BI_PL_TriggerFactory.createHandler(BI_PL_Detail_preparation__c.sObjectType);
}