trigger BI_PL_PositionCycleUserTrigger on BI_PL_Position_cycle_user__c (
    before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete) {
        BI_PL_TriggerFactory.createHandler(BI_PL_Position_cycle_user__c.sObjectType);
}