trigger BI_PL_PositionTrigger on BI_PL_Position__c (before insert) {
	BI_PL_TriggerFactory.createHandler(BI_PL_Position__c.sObjectType);
}