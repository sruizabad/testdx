trigger BI_PL_PreparationActionTrigger on BI_PL_Preparation_action__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

		BI_PL_TriggerFactory.createHandler(BI_PL_Preparation_action__c.sObjectType);
}