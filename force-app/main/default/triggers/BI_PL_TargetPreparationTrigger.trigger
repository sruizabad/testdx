trigger BI_PL_TargetPreparationTrigger on BI_PL_Target_preparation__c (before insert, before update) {
	BI_PL_TriggerFactory.createHandler(BI_PL_Target_preparation__c.sObjectType);
}