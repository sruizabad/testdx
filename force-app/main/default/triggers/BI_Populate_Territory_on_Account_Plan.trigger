/**************************************************************************************************
On create  get the createdBy User's  territory (or territories)  and pass it to the Territory_BI__c

Author: Raphael Krausz <raphael.krausz>
Date: 2014-01-30

**************************************************************************************************/
trigger BI_Populate_Territory_on_Account_Plan on Account_Plan_vod__c (before insert) {

    Id userId = UserInfo.getUserId();

    List<UserTerritory> userTerritories =
        [
            SELECT TerritoryId FROM UserTerritory WHERE UserId = :userId
        ];

    if ( userTerritories.isEmpty() ) {
        System.debug('No territories found!');
        return;
    }

    Id territoryId = userTerritories[0].TerritoryId;

    String territoryName = [SELECT Name FROM Territory WHERE Id = :territoryId].Name;

    for (Account_Plan_vod__c accountPlan : Trigger.new) {
        accountPlan.Territory_BI__c = territoryName;
    }
}