/*
 * BI_Populate_Territory_on_Event_Management trigger
 *
 * Author: Raphael Krausz <raphael.krausz@veeva.com>
 * Date:   2014-12-01
 * Description:
 *    Populate the Territory_BI__c field on a medical event with the owner's territory's name
 *    - only where available
 *    - i.e. it will not do so for System Administrators and other users that don't have a territory
 *
 *
 * Modified by: Raphael Krausz <raphael.krausz@veeva.com>
 * Date:        2014-12-12
 * Description:
 *      Changed trigger from "before insert, before update" to be just "before insert"
 *      The client request this trigger populate values only on create, not on modify.
 *
 */

trigger BI_Populate_Territory_on_Event_Management on Medical_Event_vod__c (before insert) {

    Set<Id> ownerIdSet = new Set<Id>();
    Map<Id, Id> userIdToTerritoryIdMap = new Map<Id, Id>();
    Map<Id, String> territoryIdToNameMap = new Map<Id, String>();

    for (Medical_Event_vod__c medicalEvent : Trigger.new) {
        ownerIdSet.add(medicalEvent.OwnerId);
    }

    List<UserTerritory> userTerritories =
        [
            SELECT Id, UserId, TerritoryId
            FROM UserTerritory
            WHERE UserId in :ownerIdSet
        ];

    for (UserTerritory theUserTerritory : userTerritories) {
        Id userId = theUserTerritory.UserId;
        Id territoryId = theUserTerritory.TerritoryId;
        userIdToTerritoryIdMap.put(userId, territoryId);
    }

    Set<Id> territoryIdSet = new Set<Id>();
    territoryIdSet.addAll( userIdToTerritoryIdMap.values() );

    List<Territory> territories =
        [
            SELECT Id, Name
            FROM Territory
            WHERE Id IN :territoryIdSet
        ];

    for (Territory theTerritory : territories) {
        Id territoryId = theTerritory.Id;
        String territoryName = theTerritory.Name;
        territoryIdToNameMap.put(territoryId, territoryName);
    }

    for (Medical_Event_vod__c medicalEvent : Trigger.new) {
        Id userId = medicalEvent.OwnerId;

        if ( ! userIdToTerritoryIdMap.containsKey(userId) ) {
            continue;
        }

        Id territoryId = userIdToTerritoryIdMap.get(userId);

        if ( ! territoryIdToNameMap.containsKey(territoryId) ) {
            String message =
                'Unable to find territory name (territory Id: ' + territoryId + ')'
                + ' - for medical event ID: ' + medicalEvent.Id
                + ' (with user ID '  + userId + ')'
                ;
            System.debug(message);
            continue;
        }

        String territoryName = territoryIdToNameMap.get(territoryId);

        medicalEvent.Territory_BI__c = territoryName;
    }

}