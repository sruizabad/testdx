trigger BI_SAP_Trigger_PositionSharing on SAP_Preparation_BI__c (before insert, after insert, before update, after update)  { 

    list<SAP_Preparation_BI__c> sap_toUpdate = new list<SAP_Preparation_BI__c>();
    list<SAP_Preparation_BI__share> shareList = new list<SAP_Preparation_BI__share>();
    list<User> users = new list<User> ();
    set<string> roleList = new set<String>();
    map<string, Id> rs = new map<string, Id>();
    map<string, string> rolebyName = new map<string, string>();
    map<string, Id> groupmap = new map<string, Id>();
    map<string, Id> user_to_role = new map<string, Id>();


    for(SAP_Preparation_BI__c s : Trigger.New) roleList.add(s.Territory_BI__c);
    system.debug('********** roleList: ' + roleList );

    for(UserRole r : [Select Name, developerName, Id From UserRole WHERE Name IN: roleList]){
        rs.put(r.developerName, r.Id);
        system.debug('********** ROLES: ' + r.Name + ' / DEVNAME:  ' + r.DeveloperName);
        roleByName.put(r.name, r.DeveloperName);
    }

    for(Group g : [SELECT Id, DeveloperName FROM Group WHERE developerName IN: rs.keySet() AND Type = 'Role']){
        groupmap.put(g.developerName, g.Id);
    }

    system.debug('******** Roles values: ' + rs.values());
    users = [SELECT Id, Name, UserRole.name, userRole.DeveloperName FROM User WHERE userRoleId IN: rs.values() and isActive = true];
    system.debug('******** USERS By roles: ' + users);
    
    for(User u : users){
        system.debug('******* User: '+ u.Name + ' / Role:  ' + u.UserRole.DeveloperName);
        user_to_role.put(u.userRole.Name, u.Id);
        roleByName.put(u.UserRole.name, u.userRole.DeveloperName);
    }

    //BEFORE INSERT
    if (Trigger.isBefore) { 
        System.debug('isBefore-> '+ Trigger.isBefore);    
        for(SAP_Preparation_BI__c s : Trigger.New){
            if(user_to_role.get(s.territory_BI__c) != null){
                s.OwnerId = user_to_role.get(s.territory_BI__c);        
                System.debug('New OwnerId-> '+ s.OwnerId);
            }
        }
    }   


    //AFTER INSERT
    if (Trigger.isAfter) {  

        for(SAP_Preparation_BI__c s : Trigger.New){
            system.debug('***** territory: ' + s.Territory_Bi__c);
            system.debug('******* roleByName: ' + rolebyName.get(s.Territory_BI__c));
            system.debug('******* groupmap: ' + groupmap.get(rolebyName.get(s.Territory_BI__c)));
            
            if(groupmap.get(rolebyName.get(s.Territory_BI__c)) != null){
                  SAP_Preparation_BI__share sshare = new SAP_Preparation_BI__share();
                  sshare.parentId = s.Id;
                  sshare.AccessLevel = 'Edit';
                  sshare.UserOrGroupId = groupmap.get(rolebyName.get(s.Territory_BI__c));
                  shareList.add(sshare);        
            }
        }
    
        System.debug('shareList-> '+ shareList);
        insert shareList;

    }   

}