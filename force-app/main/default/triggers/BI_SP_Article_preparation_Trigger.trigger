trigger BI_SP_Article_preparation_Trigger on BI_SP_Article_preparation__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	BI_SP_TriggerFactory.createHandler(BI_SP_Article_preparation__c.sObjectType);
}