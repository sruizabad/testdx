trigger BI_SP_Preparation_period_Trigger on BI_SP_Preparation_period__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	BI_SP_TriggerFactory.createHandler(BI_SP_Preparation_period__c.sObjectType);
}