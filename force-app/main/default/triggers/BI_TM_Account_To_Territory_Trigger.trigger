trigger BI_TM_Account_To_Territory_Trigger on BI_TM_Account_To_Territory__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    BI_TM_TriggerDispatcher.Run(new BI_TM_Account_To_Territory_Handler());
}