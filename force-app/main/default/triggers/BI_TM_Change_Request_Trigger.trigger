trigger BI_TM_Change_Request_Trigger on BI_TM_Change_Request__c (after delete, after insert, after undelete,
after update, before delete, before insert, before update) {
  // This should be used in conjunction with the TriggerHandlerComprehensive.cls template
  // The origin of this pattern is http://www.embracingthecloud.com/2010/07/08/ASimpleTriggerTemplateForSalesforce.aspx

  BI_TM_Change_Request_Trigger_Handler handler = new BI_TM_Change_Request_Trigger_Handler(Trigger.isExecuting, Trigger.size);

  if(Trigger.isInsert && Trigger.isBefore){
    handler.OnBeforeInsert(Trigger.new);
  }
  else if(Trigger.isInsert && Trigger.isAfter){
    handler.OnAfterInsert(Trigger.new);
    //BI_TM_Change_Request_Trigger_Handler.OnAfterInsertAsync(Trigger.newMap.keySet());
  }

  else if(Trigger.isUpdate && Trigger.isBefore){
    handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
  }
  else if(Trigger.isUpdate && Trigger.isAfter){
    handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    //BI_TM_Change_Request_Trigger_Handler.OnAfterUpdateAsync(Trigger.newMap.keySet());
  }

  // else if(Trigger.isDelete && Trigger.isBefore){
  //   handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
  // }
  // else if(Trigger.isDelete && Trigger.isAfter){
  //   handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
  //   //BI_TM_Change_Request_Trigger_Handler.OnAfterDeleteAsync(Trigger.oldMap.keySet());
  // }
  //
  // else if(Trigger.isUnDelete){
  //   handler.OnUndelete(Trigger.new);
  // }
}