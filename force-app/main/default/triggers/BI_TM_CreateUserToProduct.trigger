/********************************************************************************
Name:  BI_TM_CreateUserToProduct
Copyright ? 2015  Capgemini India Pvt Ltd
=================================================================
=================================================================
Creates User To Product record when Territory is assigned to a User
=================================================================
=================================================================
History  -------
VERSION            AUTHOR              DATE         DETAIL
 1.0 -               Kiran             1/21/2016    INITIAL DEVELOPMENT
*********************************************************************************/

trigger BI_TM_CreateUserToProduct on BI_TM_User_territory__c (before insert, after insert, before update,after update) {

   /* Instantiate handler class */
  BI_TM_CreateUserToProductHandler objBI_TM_CreateUserToProductHandler = new BI_TM_CreateUserToProductHandler();

  //Code for primary assignment's start date and end date overlap validation
 if(trigger.isBefore && (trigger.isInsert || Trigger.isUpdate)){
    objBI_TM_CreateUserToProductHandler.checkassignmenttype(Trigger.new);
    objBI_TM_CreateUserToProductHandler.validateDateOverlap(Trigger.new);

  }

  if(Trigger.isUpdate && Trigger.isAfter) {
    BI_TM_UserToPosition_PrimaryPosition primaryPositionHandler = new BI_TM_UserToPosition_PrimaryPosition();
    primaryPositionHandler.updatePrimaryPosition(trigger.newMap);
    objBI_TM_CreateUserToProductHandler.updateUserManagment(trigger.oldMap,trigger.newMap,false);
  }

  if(Trigger.isInsert && Trigger.IsAfter){
    BI_TM_UserToPosition_PrimaryPosition primaryPositionHandler = new BI_TM_UserToPosition_PrimaryPosition();
    primaryPositionHandler.updatePrimaryPosition(trigger.newMap);
    objBI_TM_CreateUserToProductHandler.updateUserManagment(trigger.oldMap,trigger.newMap,true);
  }
}