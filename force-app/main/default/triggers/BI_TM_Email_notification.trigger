trigger BI_TM_Email_notification on BI_TM_BIDS__c (after insert,after update, before update) {

  if(Trigger.isBefore && Trigger.isUpdate){
    for(BI_TM_BIDS__c bu : trigger.new){
      if(bu.BI_TM_Status__c == 'To be Update'){
        Set<String> fieldsChanges = BI_TM_Emailnotification_Handler.checkFieldChanges(BI_TM_Emailnotification_Handler.relevantFields, bu.Id);
        if(fieldsChanges.size() == 0){
          bu.BI_TM_Status__c = (String)trigger.oldMap.get(bu.Id).get('BI_TM_Status__c');
        }
      }
    }
  }

     if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
            BI_TM_Emailnotification_Handler handler = new BI_TM_Emailnotification_Handler();
            handler.sendEmail(trigger.new);
        }
    }
}