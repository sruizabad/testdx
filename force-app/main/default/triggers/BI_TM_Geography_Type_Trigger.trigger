trigger BI_TM_Geography_Type_Trigger on BI_TM_Geography_type__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    BI_TM_TriggerDispatcher.Run(new BI_TM_Geography_Type_Handler());
}