/******************************************************************************** 
Name:  BI_TM_MysetupprodInsertion
Copyright ? 2015  Capgemini India Pvt Ltd
================================================================= 
================================================================= 
Used for creating mysetupproducts
=================================================================
================================================================= 
History  -------
VERSION            AUTHOR              DATE         DETAIL                
   1.0 -               Kiran           08/02/2016    INITIAL DEVELOPMENT
*********************************************************************************/



trigger BI_TM_MysetupprodInsertion on BI_TM_User_to_product__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

BI_TM_TriggerDispatcher.Run(new BI_TM_User_to_productHandler());

   /* if(trigger.isInsert && trigger.isAfter){
        
        BI_TM_User_to_productHandler usrttoprod = new BI_TM_User_to_productHandler(trigger.newmap.keyset());
        //usrttoprod .BI_TM_User_to_productHandler(trigger.newmap.keyset());
    }
*/
}