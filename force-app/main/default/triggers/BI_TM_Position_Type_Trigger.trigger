trigger BI_TM_Position_Type_Trigger on BI_TM_Position_Type__c  (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    BI_TM_TriggerDispatcher.Run(new BI_TM_Position_Type_Handler());
}