trigger BI_TM_PostalCode_Trigger on BI_TM_PostalCode__c(before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    BI_TM_TriggerDispatcher.Run(new BI_TM_PostalCode_Handler());
}