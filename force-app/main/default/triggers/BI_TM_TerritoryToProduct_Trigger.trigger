trigger BI_TM_TerritoryToProduct_Trigger on BI_TM_Territory_to_Product_ND__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
  BI_TM_TriggerDispatcher.Run(new BI_TM_Territory2Product_Trigger_Handler());
}