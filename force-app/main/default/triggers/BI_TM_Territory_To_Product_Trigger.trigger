trigger BI_TM_Territory_To_Product_Trigger on BI_TM_Territory_to_product__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    BI_TM_TriggerDispatcher.Run(new BI_TM_Territory_To_Product_Handler());
}