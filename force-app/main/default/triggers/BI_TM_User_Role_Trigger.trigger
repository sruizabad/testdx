trigger BI_TM_User_Role_Trigger on BI_TM_User_role__c (before insert, before update, before delete, after insert,after update,after delete, after undelete) 
{
    BI_TM_TriggerDispatcher.Run(new BI_TM_User_Role_Handler());
}