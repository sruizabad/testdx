trigger BI_TM_User_mgmt_Trigger on BI_TM_User_mgmt__c(before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{

   if(System.isFuture()) {
return;
}
    //if(system.isBatch() || system.isFuture()){
    BI_TM_TriggerDispatcher.Run(new BI_TM_User_mgmt_Handler());
    //}
}