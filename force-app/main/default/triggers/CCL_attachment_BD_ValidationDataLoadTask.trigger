/***************************************************************************************************************************
Apex Trigger Name : CCL_attachment_BD_ValidationDataLoadTask
Version : 1.0
Created Date : 11/03/2015	
Function :  Prevent the deletion of attachments on the Data Load object. With the additional criteria that a the prevention
			only takes place when the task has been submitted.  

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                            		Date                            		Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Robin Wijnen					  		11/03/2015      		        			Initial Creation
* Wouter Jacops							27/03/2018									Added check on Status field
***************************************************************************************************************************/

trigger CCL_attachment_BD_ValidationDataLoadTask on Attachment (before delete) {
    
	if(Trigger.isDelete && Trigger.isBefore) {
     	
        // Get the prefix of object DataLoadTask
        Schema.DescribeSobjectResult CCL_sObject = CCL_DataLoadJob_DataLoadTask__c.sObjectType.getDescribe();
        String CCL_keyPrefix = CCL_sObject.getKeyPrefix();
        
        Map<Id,Attachment> CCL_dlTaskIds = new Map<Id,Attachment>();
        
        // Iterate over the list of attachments
        for(Attachment CCL_attach : Trigger.oldMap.values()) {
            
            String CCL_parentId = (String) CCL_attach.ParentId;
            
            if(CCL_parentId.startsWith(CCL_keyPrefix)) {
                    CCL_dlTaskIds.put(CCL_attach.ParentId,CCL_attach);
            }
        }
        
        // Get List of relevant DataloadTasks
        List<CCL_DataLoadJob_DataLoadTask__c> CCL_DLTasksList = [SELECT ID, CCL_Status__c FROM CCL_DataLoadJob_DataLoadTask__c WHERE ID IN: CCL_dlTaskIds.keySet() ];
        
        // Add Error to relevant DataloadTasks
        for(CCL_DataLoadJob_DataLoadTask__c CCL_dlTask : CCL_DLTasksList) {
            if(!(CCL_dlTask.CCL_Status__c == 'New')){
                CCL_dlTaskIds.get(CCL_dlTask.Id).addError(label.CCL_Prevent_Attachment_Deletion);
            }
        }
        
	}
}