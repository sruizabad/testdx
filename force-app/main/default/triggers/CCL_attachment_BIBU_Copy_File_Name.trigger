/***************************************************************************************************************************
Apex Trigger Name : CCL_attachment_BIBU_Copy_File_Name
Version : 1.0
Created Date : 27/03/2018   
Function :  Copy Name of the attachment to field on Task record  

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                                 Date                                       Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Wouter Jacops                          27/03/2018                                  Initial Creation
* Wouter Jacops							 22/05/2018									 Added check on file size
* Wouter Jacops							 12/09/2018									 Added check on CCL object
***************************************************************************************************************************/
trigger CCL_attachment_BIBU_Copy_File_Name on Attachment (before insert, before update) {
    
    if((Trigger.isInsert && Trigger.isBefore)||(Trigger.isInsert && Trigger.isBefore)) {
        
        Boolean isCCLObject = false;
        //Check for parent object type
        for(Attachment CCL_attach : Trigger.new) {
            if(String.valueOf(CCL_attach.ParentId.getSobjectType()).substring(0,3) == 'CCL'){
                isCCLObject = true;
            }
        }
        
        if(isCCLObject){
            // Get the prefix of object DataLoadTask
            Schema.DescribeSobjectResult CCL_sObject = CCL_DataLoadJob_DataLoadTask__c.sObjectType.getDescribe();
            String CCL_keyPrefix = CCL_sObject.getKeyPrefix();
            
            Map<Id,String> CCL_dlTaskIds = new Map<Id,String>();
            
            if(Test.isRunningTest() && CCL_DataLoader__c.getOrgDefaults() == null) {
                // Insert the settings
                CCL_DataLoader__c CCL_setting = new CCL_DataLoader__c(Name='Test', CCL_Max_Size_Attachments__c = 95);
                insert CCL_setting;
            }
            
            try {
                Decimal sizeLimit = 1000000 * CCL_DataLoader__c.getOrgDefaults().CCL_Max_Size_Attachments__c;
                
                // Iterate over the list of attachments
                for(Attachment CCL_attach : Trigger.new) {               
                    
                    if(CCL_attach.Body.size() > sizelimit) {
                        CCL_attach.addError(label.CCL_Attachment_Max_Size + ' (' + CCL_DataLoader__c.getOrgDefaults().CCL_Max_Size_Attachments__c + ' Mb).');
                    } 
                    
                    String CCL_parentId = (String) CCL_attach.ParentId;
                    String CCL_fileName = CCL_attach.Name.substring(0,CCL_attach.Name.length()-4);
                    
                    if(CCL_parentId.startsWith(CCL_keyPrefix)) {
                        //Check if attachment is not the errorLog
                        if(!CCL_fileName.contains('errorLog')){
                            CCL_dlTaskIds.put(CCL_attach.ParentId,CCL_fileName);
                        }
                    }
                }
                
                // Get List of relevant DataloadTasks (with status 'New')
                List<CCL_DataLoadJob_DataLoadTask__c> CCL_DLTasksList = [SELECT ID, CCL_Name__c FROM CCL_DataLoadJob_DataLoadTask__c WHERE ID IN: CCL_dlTaskIds.keySet() AND CCL_Status__c = 'New'];
                
                // Copy Attachment Name to DataloadTasks
                for(CCL_DataLoadJob_DataLoadTask__c CCL_dlTask : CCL_DLTasksList) {
                    CCL_dlTask.CCL_Name__c = CCL_dlTaskIds.get(CCL_dlTask.Id);
                }
                
                // Updating DataloadTask records
                update CCL_DLTasksList;
            } catch (Exception CCL_e) {
                System.Debug('CCL_attachment_BIBU_Copy_File_Name ---> EXCEPTION: ' + CCL_e.getMessage() + ', ' + CCL_e.getStackTraceString());
            }
        }
    }
}