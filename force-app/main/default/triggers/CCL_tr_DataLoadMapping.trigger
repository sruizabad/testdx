/***************************************************************************************************************************
Apex Trigger Name : CCL_tr_DataLoadMapping
Version : 1.0
Created Date : 19/10/2015   
Function :  Trigger to help calculate the hidden fields such as the object name, mapped field, ... after inserting or updating the Maps. 

Modification Log:
-------------------------------------------------------------------------------------------------------------------------------------------------
* Developer                                 Date                                    Description
* -----------------------------------------------------------------------------------------------------------------------------------------------
* Joris Artois                              19/10/2015                              Initial Creation
***************************************************************************************************************************/

trigger CCL_tr_DataLoadMapping on CCL_DataLoadInterface_DataLoadMapping__c (before insert, before update) {
    
    // Instantiate the Trigger Handler
    CCL_trHandler_DataLoadMapping handler = new CCL_trHandler_DataLoadMapping();
    
    /* Before Insert */
    if (trigger.isInsert && trigger.isBefore) {
        handler.beforeInsert(trigger.new, trigger.newmap);  
    }
    
    /* Before Update */
    if (trigger.isUpdate && trigger.isBefore) {
        handler.beforeUpdate(trigger.old, trigger.oldMap, trigger.new, trigger.newmap);
    }
}