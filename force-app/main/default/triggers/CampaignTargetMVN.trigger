trigger CampaignTargetMVN on Campaign_Target_vod__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	new TriggersMVN()
		.bind(TriggersMVN.Evt.afterupdate, new DeleteOrdersOnUnjoinTriggerMVN())
        .bind(TriggersMVN.Evt.afterdelete, new DeleteOrdersOnUnjoinTriggerMVN())
        .manage();
}