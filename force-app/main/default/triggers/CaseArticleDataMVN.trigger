/*
 *  CaseArticleDataMVN
 *  Created By:     Roman Lerman
 *  Created Date:   4/18/2013
 *  Description:    This is a generic Case Article Data trigger used for calling any Attachment logic
 */
trigger CaseArticleDataMVN on Case_Article_Data_MVN__c (before insert, before update, before delete) {
	new TriggersMVN()
		.bind(TriggersMVN.Evt.beforeinsert, new CaseArticleCopyTriggerMVN())
        .bind(TriggersMVN.Evt.beforedelete, new CaseArticleDeleteTriggerMVN())
        .manage();
}