/*
 *  CaseMVN
 *  Created By:     Roman Lerman
 *  Created Date:   3/1/2013
 *  Description:    This is a generic Case trigger used for calling any Case logic 
 */
trigger CaseMVN on Case (before insert, before update, after update) {
    new TriggersMVN()
        .bind(TriggersMVN.Evt.beforeinsert, new CreateInteractionForRequestMVN())
        .bind(TriggersMVN.Evt.beforeupdate, new ChangeCaseOwnershipMVN())
        .bind(TriggersMVN.Evt.afterupdate, new UpdateRequestsWhenInteractionChangedMVN())
        
     
        .bind(TriggersMVN.Evt.afterupdate, new CloseMedicalInqryWhenInteractClosedMVN())
        //.bind(TriggersMVN.Evt.afterupdate, new MITSCloseMedicalInqryWhenInteractClosed())
        .bind(TriggersMVN.Evt.afterupdate, new ReopenParentCaseWhenChildReopenedMVN())
        
        .manage();
        //Added this code for Sharing of Cases belonging to same country
        User userprofile= [Select Id,Profile.Name from User where Id = :UserInfo.getUserId()];
        
        if(userprofile.Profile.Name <>'MITS-BASIC MEDICAL INFORMATION KNOWLEDGE ADMINISTRATOR' && userprofile.Profile.Name <> 'MITS- BASIC MEDICAL INFORMATION USER' && userprofile.Profile.Name <> 'MITS-GLOBAL BUSINESS ADMINISTRATOR' && userprofile.Profile.Name <> 'MITS-REGIONAL MEDICAL INFORMATION USER' && userprofile.Profile.Name <> 'MITS-REGIONAL MEDICAL INFORMATION KNOWLEDGE ADMINISTRATOR') 
        {
        if(Trigger.isAfter)
        {
        if (Trigger.isUpdate) {
             if(!System.isBatch()){
    Case_Share_CAP cs=new Case_Share_CAP();
    cs.share(Trigger.new);
         system.debug('INSIDE CASE SHARE LOGIC');
  }
        }
  }
 } 
}