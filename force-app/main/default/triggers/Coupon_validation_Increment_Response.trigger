/*
 *  Coupon_validation_Increment_Response
 *  Created By:     Garima Agarwal
 *  Created Date:   7/12/2014
 *  Description:    This is a case trigger to validate the coupon entered by the user and allowing only Patients to Redeem coupons.
 *                  Increment Response on VAs Transaction once a case is created.
 */
trigger Coupon_validation_Increment_Response on Case (before update, after update)
{
if (Trigger.isBefore&&Trigger.isUpdate)
{Coupon_Validation_cap c=new Coupon_Validation_cap();
c.validate(Trigger.new);  
}
if(Trigger.isAfter&&Trigger.isUpdate)
{Vas_Response_cap c=new Vas_Response_cap();
//list<case>caselist=[select id,Coupon_Code__c, Status, account__c, product_MVN__c,Product_MVN__r.Name from case where id in :trigger.new and RecordType.Name='Request'];
if(BI_Gecco_RecursionHandler.isBool){
BI_Gecco_RecursionHandler.isBool = false;

list<case> caserectype = [select id,Coupon_Code__c,Coupon__c,Status, account__c,RecordTypeId,Effective_Call__c,product_MVN__c,Product_MVN__r.Name from case where id in :trigger.new];
for (case c12: caserectype )
{
system.debug('@@@@@Rec'+c12.Status);

}

list<case>caselist=[select id,Coupon_Code__c,Coupon__c,Status, account__c,Effective_Call__c,Effective_Call_cap__c,product_MVN__c,Product_MVN__r.Name from case where id in :trigger.new and status = 'Closed' and Effective_Call__c=false ];
system.debug(caselist);
c.updateresponse(caselist); 
}
}
}