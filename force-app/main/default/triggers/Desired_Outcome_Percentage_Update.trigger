/**
 * Account_Plan_Percentage_Update
 * Updates the percentage of a plan complete when a Desired Outcome is added/modified/deleted/undeleted
 * 
 * Author: David Bajor <david.bajor@veeva.com>
 * Date: 2015-11-19
 */

trigger Desired_Outcome_Percentage_Update on Outcome_BI__c (after insert, after update, after delete, after undelete) {

  Set<Id> plan_ids = new Set<Id>();
  
  Set<Outcome_BI__c> theOutcomes = new Set<Outcome_BI__c>();

  // If this is a delete, then Trigger.new will be null
  // If this is an insert, then Trigger.old will be null
  // If this is an upsert, then the tactic may have moved plans, so both should captured.
  // We use sets, to avoid duplicates
  
  if (Trigger.old != null) theOutcomes.addAll(Trigger.old);
  if (Trigger.new != null) theOutcomes.addAll(Trigger.new);
  
  for(Outcome_BI__c Outcome : theOutcomes) {
    plan_ids.add(Outcome.Account_Plan__c);
  }
  
  List<Account_Plan_vod__c> plans = [
    SELECT Id, Completed_Desired_Outcome_BI__c
    FROM Account_plan_vod__c
    WHERE Id in :plan_ids
  ];
  
  List<Outcome_BI__c> outcomes = [
    SELECT Id, Account_Plan__c, Completed_BI__c
    FROM Outcome_BI__c
    WHERE Account_Plan__c in :plan_ids
    AND IsDeleted = false
  ];
  

  Map<Id, Integer> totals   = new Map<Id, Integer>();
  Map<Id, Integer> completed  = new Map<Id, Integer>();
  
  
  for (Account_Plan_vod__c plan : plans) {
    totals.put(plan.Id, 0);
    completed.put(plan.Id, 0);
  }
  if(totals.size()>0){
    for (Outcome_BI__c Outcome : outcomes) {
      Id plan = Outcome.Account_Plan__c;
      //If the AS (Outcome_BI__c) was updated from null Account_Plan__c 
      //skip it, the instance from trigger.new will take care of the update
      if (plan == null) continue; 
      totals.put(plan, totals.get(plan)+1);
      if (Outcome.Completed_BI__c)
        completed.put(plan, completed.get(plan)+1);
    }
  
    for (Integer i = 0; i < plans.size(); i++) {
      Integer total = totals.get(plans[i].Id);
      Integer complete = completed.get(plans[i].Id);
      Integer percent_complete = 0;
      // Adding the 0.5 at the end of the calculation ensures the number is rounded correctly when casting back to an Integer
      if(total>0){
        percent_complete = (Integer) (100.0 * (Double) complete / (Double) total + 0.5);
        plans[i].Completed_Desired_Outcome_BI__c = percent_complete;
      }
      else if(total==0 || total==null){
        plans[i].Completed_Desired_Outcome_BI__c = percent_complete;
      }
    }
  }
  
  
  update plans;
}