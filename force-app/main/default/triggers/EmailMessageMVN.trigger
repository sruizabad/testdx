/*
 *  EmailMessageMVN
 *  Created By:     Roman Lerman
 *  Created Date:   6/24/2013
 *  Description:    This is a generic EmailMessage trigger used for calling any EmailMessage logic
 */
trigger EmailMessageMVN on EmailMessage (before insert, before update, before delete) {
	String parentId = 'ParentId';
	String objectName = 'Case';
	String lockedFieldName = 'IsClosed';
	String parentKeyPrefix = Schema.SobjectType.Case.getKeyPrefix();
	String error = Label.Cannot_Modify_Record_on_Closed_Case;
	
	new TriggersMVN()
		.bind(TriggersMVN.Evt.beforeinsert, new LockRelatedRecordsMVN(parentId, objectName, lockedFieldName, parentKeyPrefix, error))
        .bind(TriggersMVN.Evt.beforeupdate, new LockRelatedRecordsMVN(parentId, objectName, lockedFieldName, parentKeyPrefix, error))
        .bind(TriggersMVN.Evt.beforedelete, new LockRelatedRecordsMVN(parentId, objectName, lockedFieldName, parentKeyPrefix, error))
        .manage();
}