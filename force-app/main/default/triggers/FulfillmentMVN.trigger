trigger FulfillmentMVN on Fulfillment_MVN__c (after update) {
	new TriggersMVN()
    .bind(TriggersMVN.Evt.afterupdate, new ReopenParentCaseWhenFulfillReopenedMVN())
    .manage();
}