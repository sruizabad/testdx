/**
 *  This trigger is for Cycle_Data_BI__c object 
 *
 @author 	Hely
 @created 	2015-03-26
 @version 	1.0
 @since 	25.0 (Force.com ApiVersion)
 *
 @changelog
 *
 * 2015-03-26 Hely <hely.lin@itbconsult.com>
 * - Created
 */
trigger IMP_BI_CycleDataTrigger on Cycle_Data_BI__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	//************************* BEGIN Pre-Processing **********************************************
	
	//************************* END Pre-Processing ************************************************
	
	//************************* BEGIN Before Trigger **********************************************
	if(Trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) {
		
		set<Id> set_cycleDataMcId = new set<Id>();
		for(Cycle_Data_BI__c cycleData : trigger.new){
			if(cycleData.Matrix_Cell_1_BI__c !=null ){
				set_cycleDataMcId.add(cycleData.Matrix_Cell_1_BI__c);
			}
			if(cycleData.Matrix_Cell_2_BI__c != null){
				set_cycleDataMcId.add(cycleData.Matrix_Cell_2_BI__c);
			}
			if(cycleData.Matrix_Cell_3_BI__c != null){
				set_cycleDataMcId.add(cycleData.Matrix_Cell_3_BI__c);
			}
			if(cycleData.Matrix_Cell_4_BI__c != null){
				set_cycleDataMcId.add(cycleData.Matrix_Cell_4_BI__c);
			}
			if(cycleData.Matrix_Cell_5_BI__c != null){
				set_cycleDataMcId.add(cycleData.Matrix_Cell_5_BI__c);
			}
		}
		
		map<Id, Id> map_mcId_mId = new map<Id, Id>();
		for(Matrix_Cell_BI__c mc : [SELECT Matrix_BI__c, Id FROM Matrix_Cell_BI__c where Id in :set_cycleDataMcId]){
			map_mcId_mId.put(mc.Id, mc.Matrix_BI__c);
		}
		
		for(Cycle_Data_BI__c cycleData : trigger.new){
			cycleData.Matrix_1_BI__c = map_mcId_mId.get(cycleData.Matrix_Cell_1_BI__c);
			cycleData.Matrix_2_BI__c = map_mcId_mId.get(cycleData.Matrix_Cell_2_BI__c);
			cycleData.Matrix_3_BI__c = map_mcId_mId.get(cycleData.Matrix_Cell_3_BI__c);
			cycleData.Matrix_4_BI__c = map_mcId_mId.get(cycleData.Matrix_Cell_4_BI__c);
			cycleData.Matrix_5_BI__c = map_mcId_mId.get(cycleData.Matrix_Cell_5_BI__c);
		}
		
	}
	//************************* END Before Trigger **********************************************
	
	
	//************************* BEGIN After Trigger ***********************************************
	//************************* END After Trigger ************************************************

}