/**
 *  This trigger is used to the Strategic_Weight_BI__c field assignment after insert Matrix_Cell_BI__c
 *
 @author 	Hely
 @created 	2015-03-03
 @version 	1.0
 @since 	25.0 (Force.com ApiVersion)
 *
 @changelog
 * 2015-03-03  Hely <hely.lin@itbconsult.com>
 * - Migrated from old system
 * - Added ...
 *
 * 2015-03-03 Hely <hely.lin@itbconsult.com>
 * - Created
 */
trigger IMP_BI_MatrixCellTrigger on Matrix_Cell_BI__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	//************************* BEGIN Pre-Processing **********************************************
	
	//************************* END Pre-Processing ************************************************
	
	//************************* BEGIN Before Trigger **********************************************
	if(Trigger.isBefore && trigger.isInsert) {
		set<Id> set_matrixId = new set<Id>();
		for(Matrix_Cell_BI__c mc : trigger.new){
			set_matrixId.add(mc.Matrix_BI__c);
		}
		
		map<Id, Matrix_BI__c> map_id_matrix = new map<Id, Matrix_BI__c>([SELECT Id, Row_BI__c, Column_BI__c, Lifecycle_Template_BI__c,Apply_Minimum_Threshold_BI__c 
		 																	FROM Matrix_BI__c WHERE Id in :set_matrixId]);
		
		set<Id> set_lifecycleTemplateId = new set<Id>();
		
		for(Matrix_BI__c matrix : map_id_matrix.values()){
			set_lifecycleTemplateId.add(matrix.Lifecycle_Template_BI__c);
		}
		
		map<Id, Lifecycle_Template_BI__c> map_id_lifecycleTemplate = new map<Id, Lifecycle_Template_BI__c>([SELECT Id
																	, Row_BI__c
																	, Column_BI__c
																	, Type_BI__c
																	, Potential_Factor_Numbers_BI__c
																	, Adoption_Factor_Numbers_BI__c
																	, Potential_Weight_Factor_BI__c
																	, Adoption_Weight_Factor_BI__c 
																	FROM Lifecycle_Template_BI__c WHERE ID in :set_lifecycleTemplateId]);
		
		for(Matrix_Cell_BI__c mc : trigger.new){
			Matrix_BI__c matrix = map_id_matrix.get(mc.Matrix_BI__c);
			Lifecycle_Template_BI__c lifecycle = map_id_lifecycleTemplate.get(matrix.Lifecycle_Template_BI__c);
			Decimal strategicWeight = IMP_BI_ClsMatrixUtil.calcStrategicWeight(matrix, mc, lifecycle);
	    	mc.Strategic_Weight_BI__c = strategicWeight!=null ? strategicWeight : 0;
		}
			
	}
	//************************* END Before Trigger *************************************************

	//************************* BEGIN After Trigger ***********************************************
	if(Trigger.isAfter) {
		System.debug('************************* IMP_BI_MatrixCellTrigger : After Trigger ************');
	}
	//************************* END After Trigger ************************************************
	
}