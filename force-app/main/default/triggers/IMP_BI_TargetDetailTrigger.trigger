/**
* ===================================================================================================================================
*                                   IMMPaCT BI
* ===================================================================================================================================
*  Decription:      Trigger on target detail object
*  @author:         Jefferson Escobar
*  @created:        17-Sep-2016
*  @version:        1.0
*  @see:            Salesforce IMMPaCT
*  @since:              37.0 (Force.com ApiVersion)
* ===================================================================================================================================
*       Version     Date                        Developer                   Notes
*       1.0         17-Sep-2016                 jescobar                      Construction of the trigger.
*/
trigger IMP_BI_TargetDetailTrigger on Target_Detail_BI__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
  //Execute Target Detail Handler Operations
  new IMP_BI_TargetDetailHandler().run();
}