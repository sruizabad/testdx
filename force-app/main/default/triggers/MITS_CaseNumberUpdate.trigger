trigger MITS_CaseNumberUpdate on Case (after insert) {

List<MITSROPUCountryCodes__c> ropusettings = MITSROPUCountryCodes__c.getAll().Values();
system.debug('Ropu countries are*****'+ropusettings);

 User usercc=[SELECT Country_Code_BI__c,UserRole.Name, Id FROM User WHERE Id = : UserInfo.getUserId()];
 system.debug('User Role--->'+usercc.UserRole.Name);
 //Case cs=new Case();
   List<Case> cs=[Select Id, CaseNumber,Case_Number_New_MITS__c,Country_MITS__c,Case_Source_MITS__c from Case where Id IN:Trigger.newMap.keySet()];
  
for(Case csval : cs) {
      csval.Country_MITS__c=usercc.Country_Code_BI__c;
   for(MITSROPUCountryCodes__c settings:ropusettings){
   system.debug('Custom Setting Name*****'+settings.Name);
   if(usercc.UserRole.Name==settings.Name){
       csval.Case_Number_New_MITS__c= settings.MITS_ROPU__c + Date.Today().Year() + csval.CaseNumber;
             system.debug('Case Number *****'+csval.Case_Number_New_MITS__c);
             system.debug('in 1st If Loop');
             update csval; 
             }
             
         if(csval.Case_Number_New_MITS__c==null && usercc.UserRole.Name!=settings.Name){
         csval.Case_Number_New_MITS__c= usercc.Country_Code_BI__c+ Date.Today().Year() + csval.CaseNumber;
         update csval; 
         }
          
             
         } 
        
   }
   
   
 
}