trigger MITS_MedicalInquiryUpdatetoClosed on Case (before update) {

List<Medical_Inquiry_vod__c> closedMIs= new list<Medical_Inquiry_vod__c>();
Map<Case,Medical_Inquiry_vod__c> MIsinsert = new map<Case,Medical_Inquiry_vod__c>();  

User user = [SELECT id, email,UserName,Profile.Name FROM User WHERE id = :UserInfo.getUserId()];
  if(user.Profile.Name.Startswith('MITS')){
for(Case cs : Trigger.new){

 if(cs.Medical_Inquiry_MVN__c==null){
 
 Medical_Inquiry_vod__c mi= new Medical_Inquiry_vod__c(
        CaseNumber_BI__c = cs.CaseNumber,
        //Inquiry_Text__c = cs.Inquiry_MITS__c,
        Delivery_Method_vod__c = cs.Delivery_Method_MVN__c,
        Status_vod__c = cs.Status
      );
      system.debug('Status in MI is---->'+mi.Status_vod__c);
       MIsinsert.put(cs,mi);
       system.debug('Status in MI updated to---->'+MIsinsert);
   }
   else {
   
   Medical_Inquiry_vod__c mi = new Medical_Inquiry_vod__c(
        ID = cs.Medical_Inquiry_MVN__c,
        CaseNumber_BI__c = cs.Case_Number_New_MITS__c,
        //Inquiry_Text__c = cs.Inquiry_MITS__c,
        Delivery_Method_vod__c = cs.Delivery_Method_MVN__c,
        Status_vod__c = cs.Status
      );
      closedMIs.add(mi);   
   system.debug('Status after in MI updated to---->'+closedMIs);
      }
    Database.SaveResult[] srList = Database.insert(MIsinsert.values(), false);
}

for(Case C : MIsinsert .keySet()){
        C.Medical_Inquiry_MVN__c = MIsinsert.get(C).Id;
    }
 System.debug('To update size: ' + closedMIs.size());
    Database.SaveResult[] updateList = Database.update(closedMIs, false);
  }
}