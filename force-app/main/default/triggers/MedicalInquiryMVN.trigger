/*
 *  CaseMVN
 *  Created By:     Kai Chen
 *  Created Date:   7/28/2013
 *  Description:    This is a generic Medical Inquiry trigger used for calling any Medical Inquiry logic
 */
trigger MedicalInquiryMVN on Medical_Inquiry_vod__c (after insert, after update) {
    new TriggersMVN()
        .bind(TriggersMVN.Evt.afterinsert, new CreateCasesForMedicalInquiryMVN())
        .bind(TriggersMVN.Evt.afterupdate, new CreateCasesForMedicalInquiryMVN())
        //.bind(TriggersMVN.Evt.afterinsert, new MITSCreateCasesForMedicalInquiryMVN())
          .bind(TriggersMVN.Evt.afterinsert, new MITSCreateCasesForCustomerMITSrequest())
          .bind(TriggersMVN.Evt.afterupdate, new MITSCreateCasesForCustomerMITSrequest())        
        //.bind(TriggersMVN.Evt.afterupdate, new MITSCreateCasesForMedicalInquiryMVN())
        
        //.bind(TriggersMVN.Evt.afterupdate, new MITSCreateCasesForCustomerMITSrequest())
       
        .manage();
}