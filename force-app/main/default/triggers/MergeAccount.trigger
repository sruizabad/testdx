trigger MergeAccount on Account_Merge_BI__c (after insert) {
    Map<Id, Account> mapAccounts = new Map<Id, Account>();
    for(Account_Merge_BI__c merged : Trigger.new){
        mapAccounts.put(merged.Winner_Account_BI__c, null);
    }

    for(Account acc : [SELECT Id FROM Account WHERE Id IN: mapAccounts.keySet()]){
        mapAccounts.put(acc.Id, acc);
    }
    for(Account_Merge_BI__c merged : Trigger.new){
        merge mapAccounts.get(merged.Winner_Account_BI__c) merged.Loser_Account_BI__c;
    }
}