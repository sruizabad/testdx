trigger PlanExpenseUpdateForFixedAlloc on Fixed_Allocation_BI__c (after insert,after update,before delete) {

    if(Trigger.isafter && trigger.isInsert){
    
        PlanExpenseUpdate_FATriggerHandler FAinstobj= new PlanExpenseUpdate_FATriggerHandler();
        FAinstobj.FixedAllocationCreation(Trigger.new);
    }
    
    else if(Trigger.isAfter && Trigger.isUpdate){
    
        PlanExpenseUpdate_FATriggerHandler FAUpdtobj= new PlanExpenseUpdate_FATriggerHandler();
        FAUpdtobj.FixedAllocationUpdation(Trigger.newmap);
    }
    
    else if(Trigger.isbefore && Trigger.isDelete){
    
        PlanExpenseUpdate_FATriggerHandler FADelobj= new PlanExpenseUpdate_FATriggerHandler();
        FADelobj.FixedAllocationDeletion(Trigger.oldmap);
    
    }

}