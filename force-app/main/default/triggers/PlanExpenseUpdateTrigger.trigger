trigger PlanExpenseUpdateTrigger on Event_Expenses_BI__c (after insert, after update,before delete) {

    if(Trigger.isAfter && trigger.isInsert){
    
        PlanExpenseUpdateTriggerHandler obj = new PlanExpenseUpdateTriggerHandler();
        obj.EventExpenseCreation(trigger.new);
    }
    
    else if(Trigger.isAfter && Trigger.isUpdate){
    
        PlanExpenseUpdateTriggerHandler obj1 = new PlanExpenseUpdateTriggerHandler();
        obj1.EventExpenseUpdation(trigger.newmap);
    
    }
    
    else if(Trigger.isbefore && Trigger.isDelete){
    
        PlanExpenseUpdateTriggerHandler obj2 = new PlanExpenseUpdateTriggerHandler();
        obj2.EventExpenseDeletion(trigger.oldmap);
    
    }
    
}