trigger Update_PlanExpense_On_Plan on Plan_Expense_BI__c (after insert,after update) {

    if((Trigger.isInsert || Trigger.isUpdate)  && Trigger.isAfter){
        
        Update_PlanExpense_On_PlanHandler handler = new Update_PlanExpense_On_PlanHandler();
        handler.onAfterInsert(Trigger.newMap);
        
    }

}