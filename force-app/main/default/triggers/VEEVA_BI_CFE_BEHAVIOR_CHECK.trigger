trigger VEEVA_BI_CFE_BEHAVIOR_CHECK on Coaching_Report_vod__c (before update) {
    
	system.debug('VEEVA_BI_CFE_BEHAVIOR_CHECK is starting...');  
	system.debug('Attila User: ' + Userinfo.getUserId());
    
    Id SYS_ADMIN_PROFILE_ID = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1].Id;
    Id UserProfile = UserInfo.getProfileId();
    
    for (Coaching_Report_vod__c cr : trigger.new){  
        
        if (cr.Recordtype.developerName != 'Quarterly_Report_BI') return;
        
        Id CFEId = cr.Id;
        system.debug('Attila Coaching Report: ' + CFEId);
        if (cr.Status__c == 'Approved' && UserProfile != SYS_ADMIN_PROFILE_ID)
        {
			List<CFE_Report_Behavior_BI__c > behaviourList =
              [
				SELECT Id, Name, Behavior_BI__c, Coaching_Report_BI__c, IsDeleted,Rating_BI__c
 				FROM CFE_Report_Behavior_BI__c
 				WHERE Coaching_Report_BI__c = :CFEId
 				AND IsDeleted = FALSE	
				//LIMIT 100
              ];
             system.debug('Attila behaviourList: ' + behaviourList);
             for (CFE_Report_Behavior_BI__c behavior : behaviourList) {
			 	if (behavior.Rating_BI__c == '-' || behavior.Rating_BI__c == '' || behavior.Rating_BI__c == null)
                {
					cr.addError('One or more behaviors haven\'t been rated yet!');
                    return;
                }
             }	

        }

    }    
    
}