/**
 * Trigger: VEEVA_BI_CFE_FILL_PRECOACHINGNOTES
 * Object:  Coaching_Report_vod__c
 * Author:  Raphael Krausz <raphael.krausz@veeva.com>
 * Date:    2015-09-23
 * Description:
 *
 *      Create new trigger on Coaching_Report_vod__c. Check the most recent coaching record where
 *      the managerID and employeeID are the same and copy the string from post coaching notes into
 *      the current record's pre-coaching notes.
 *
 *      Find the last coaching report for this employee and manager. Pre-populate the pre-call notes
 *      with the post-call notes from the last coaching report.
 *
 *      Order the coaching reports by coaching start date. Use that to determine the last coaching report.
 *
 *
 * Modified by: Raphael Krausz <raphael.krausz@veeva.com>
 * Date: 2015-09-24
 * Description:
 *      Fixed bug - ensuring the coaching report is the one that occurs
 *
 *
 *
 *   Apex trigger VEEVA_BI_CFE_FILL_PRECOACHINGNOTES caused an unexpected exception, contact your administrator: VEEVA_BI_CFE_FILL_PRECOACHINGNOTES: execution of BeforeInsert caused by: System.NullPointerException: Attempt to de-reference a null object: Trigger.VEEVA_BI_CFE_FILL_PRECOACHINGNOTES: line 95, column 1
 */

trigger VEEVA_BI_CFE_FILL_PRECOACHINGNOTES on Coaching_Report_vod__c (before insert) {

    Set<Id> managerIds  = new Set<Id>();
    Set<Id> employeeIds = new Set<Id>();

    Set<String> compoundIdSet = new Set<String>();
    Map<String, Coaching_Report_vod__c> newCoachingReportMap = new Map<String, Coaching_Report_vod__c>();

    for (Coaching_Report_vod__c coachingReport : Trigger.new) {
        Id managerId   = coachingReport.Manager_vod__c;
        Id employeeId  = coachingReport.Employee_vod__c;
        Date startDate = coachingReport.Review_Date__c;

        managerIds.add(managerId);
        employeeIds.add(employeeId);

        String compoundId = makeCompoundId(coachingReport);
        compoundIdSet.add(compoundId);
        newCoachingReportMap.put(compoundId, coachingReport);
    }

    Date maxOldestDate = Date.today().addYears(-2);

    Integer numberOfCoachingReports = compoundIdSet.size();
    // Alternatively, the number of CFEs in the trigger

    List<Coaching_Report_vod__c> previousCoachingReports =
        [
            SELECT
            Id,
            Manager_vod__c,
            Employee_vod__c,
            Review_Date__c,
            Comments__c
            FROM Coaching_Report_vod__c
            WHERE Manager_vod__c IN :managerIds
            AND Employee_vod__c IN :employeeIds
            AND Review_Date__c > :maxOldestDate
            ORDER BY Review_Date__c DESC
        ];

    // Map to match compound ID of a coaching report with its previous version
    Map<String, Coaching_Report_vod__c> previousCoachingReportMap = new Map<String, Coaching_Report_vod__c>();

    for (Coaching_Report_vod__c coachingReport : previousCoachingReports) {
        String compoundId = makeCompoundId(coachingReport);

        if ( ! compoundIdSet.contains(compoundId) ) {
            // If this record doesn't match any of the CFEs in the trigger, ignore it
            continue;
        }

        if ( previousCoachingReportMap.containsKey(compoundId) ) {
            // If we've already stored this compound ID, then we already have the most recent
            // previous version of the coaching report.
            // (They are ordered by Review_Date__c descending.)
            continue;
        }



        Coaching_Report_vod__c newCoachingReport = newCoachingReportMap.get(compoundId);
        if (newCoachingReport == null) {
            // This should never happen!
            System.debug('newCoachingReport NOT found!!! This shouldn\'t happen!');
            continue;
        }

        Date previousReviewDate = coachingReport.Review_Date__c;
        Date newReviewDate      = newCoachingReport.Review_Date__c;

        if (previousReviewDate < newReviewDate) {
            previousCoachingReportMap.put(compoundId, coachingReport);
            
            if (previousCoachingReportMap.size() == numberOfCoachingReports) {
                // We have already found all the previous versions of the coaching reports and don't
                // need to look further
                // (This may never come, but it's not bad to check)
                break;
            }
        }
    }

    for (Coaching_Report_vod__c coachingReport : Trigger.new) {

        String compoundId = makeCompoundId(coachingReport);

        Coaching_Report_vod__c previousCoachingReport = previousCoachingReportMap.get(compoundId);

        if (previousCoachingReport == null) {
            // There is no previous coaching report
            // Therefore there is nothing to copy across - so we continue to the next
            System.debug(
                'No previous coaching report for the coaching report with an employee Id of '
                + coachingReport.Employee_vod__c
                + ', and a manager ID of ' + coachingReport.Manager_vod__c
                + ', coaching report ID: ' + coachingReport.Id
            );
            continue;
        }

        String lastReviewNotes = previousCoachingReport.Comments__c;

        String existingPreCoachingNotes = coachingReport.Pre_Coaching_Notes_BI__c;

        if ( String.isBlank(existingPreCoachingNotes) ) {
            coachingReport.Pre_Coaching_Notes_BI__c = lastReviewNotes;
        } else {
            coachingReport.Pre_Coaching_Notes_BI__c = existingPreCoachingNotes + '\n' + lastReviewNotes;
        }

    }


    private String makeCompoundId(Coaching_Report_vod__c aCoachingReport) {
        Id managerId   = aCoachingReport.Manager_vod__c;
        Id employeeId  = aCoachingReport.Employee_vod__c;

        String compoundId = ((String) managerId) + '::' + ((String) employeeId);
        return compoundId;
    }

}