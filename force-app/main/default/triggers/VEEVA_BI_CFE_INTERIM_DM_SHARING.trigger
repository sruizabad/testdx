/**
 * Trigger: VEEVA_BI_CFE_INTERIM_DM_SHARING
 * Object:  Coaching_Report_vod__c
 * Author:  Raphael Krausz <raphael.krausz@veeva.com>
 * Date:    2015-09-23
 * Description:
 *      Add ReadWrite permissions in the Coaching For Excellence Share (Coaching_Report_vod__Share)
 *      records for the creator of a Coaching For Excellence (Coaching_Report_vod__c) record.
 *
 */
 
trigger VEEVA_BI_CFE_INTERIM_DM_SHARING on Coaching_Report_vod__c (after insert) {

    List<Coaching_Report_vod__Share> sharesToAdd = new List<Coaching_Report_vod__Share>();

    for (Coaching_Report_vod__c coachingReport : trigger.new) {
        Coaching_Report_vod__Share coachingReportShare = new Coaching_Report_vod__Share();
        coachingReportShare.ParentId = coachingReport.Id;
        coachingReportShare.AccessLevel = 'Edit';
        coachingReportShare.UserOrGroupId = coachingReport.CreatedById;
        //Don't let the user to create a sharing for his/her self - Attila 
        If (coachingReport.CreatedById == coachingReport.Employee_vod__c) {
            coachingReport.addError('Please select a different Employee for the coaching report!');
            return;
        }
        else {
            coachingReportShare.UserOrGroupId = coachingReport.CreatedById;
        }
        sharesToAdd.add(coachingReportShare);
    }

    // Check if there are shares to add, and if so add them
    if ( ! sharesToAdd.isEmpty() ) {
        insert sharesToAdd;
    }


}