trigger VEEVA_BI_CHC_Approval_History on Order_Approval_BI__c (before insert, before update, after insert, after update) {
    system.debug('VEEVA_BI_CRC_Approval_History STARTED');
    set<Id> OACids = new set<Id>();
    list<Order_Approval_BI__c> OAs = new list<Order_Approval_BI__c>();
    set<Id> Oids = new set<Id>();
    
    for (Integer i = 0; i < Trigger.size; i++)
    {
    	/******* CSABA 2015.07.01.  skip  Order approval for Soft_Limit_Criteria ****************/ 
    	System.Debug('CSABA ApprovalCriteriaType__c = ' + trigger.new[i].ApprovalCriteriaType__c);
    	if(trigger.new[i].ApprovalCriteriaType__c == 'Soft_Limit_Criteria')
    	   continue;
    	/******* CSABA 2015.07.01.  skip  Order approval for Soft_Limit_Criteria ****************/   
    	
        //ENTRY CRITERIA - status have changed
        if(Trigger.isUpdate)
        {
            if(Trigger.old[i].Status_BI__c==Trigger.new[i].Status_BI__c) continue;
            if(Trigger.old[i].Status_BI__c!='Approved by District Manager' && Trigger.new[i].Status_BI__c=='Approved by National Sales Manager') 
            	Trigger.new[i].addError('The Order Approval needs to be approved by the District Manager first!');
        }
        //otherwise get relevant rule
        OACids.add(Trigger.new[i].Order_Approval_Criteria_BI__c);
        Oids.add(Trigger.new[i].Order_BI__c);
        OAs.add(Trigger.new[i]);
        
    }
    
    //Get the Rule tresholds
    map<Id, Order_Approval_Criteria_BI__c> OACmap = new map<Id, Order_Approval_Criteria_BI__c>();
    for(Order_Approval_Criteria_BI__c OAC : [SELECT Id, 
    												First_Treshold_BI__c, 
    												Second_Threshold_BI__c,
    												Order_Record_Type_BI__c,
    												Approval_Levels_BI__c,
    												Rule_Type_BI__c 
    												FROM Order_Approval_Criteria_BI__c 
    												WHERE Id in :OACids]){
        OACmap.put(OAC.Id, OAC);
    }
    system.debug('OAC size: '+OACmap.size());
    //Get the Orders
    map<Id, Order_vod__c> Omap = new map<Id, Order_vod__c>();
    for(Order_vod__c O : [SELECT Id, 
    							Override_Lock_vod__c, 
    							Status_vod__c,
    							Wholesaler_vod__r.Primary_Email_BI__c, 
    							Wholesaler_vod__r.Secondary_Email_BI__c, 
    							Wholesaler_vod__r.Additional_Email_BI__c,
    							Createdby.Backoffice_Email_BI__c  
    							FROM Order_vod__c 
    							WHERE Id in :Oids]){
        Omap.put(O.Id, O);
    }
    system.debug('O size: '+Omap.size());
    //prepare template IDs
    Id tempid = [SELECT Id, 
    					DeveloperName 
    					FROM EmailTemplate 
    					WHERE DeveloperName = 'CHC_Approval' 
    					LIMIT 1].Id;
    Id rejectTemp = [SELECT Id, 
    					DeveloperName 
    					FROM EmailTemplate 
    					WHERE DeveloperName = 'CHC_Rejection' 
    					LIMIT 1].Id;
    /*Id externalTemp = [SELECT Id, 
    					DeveloperName 
    					FROM EmailTemplate 
    					WHERE DeveloperName = 'CHC_Approved_External' 
    					LIMIT 1].Id;*/
    map<Id, Order_vod__c> AppOmap = new map<Id, Order_vod__c>();
    list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
    //Cycle through the Approvals and do what needs to be done
    for(Order_Approval_BI__c OA : OAs){
        
        Decimal rule1; 
        Decimal rule2;
        if(OA.Order_Approval_Criteria_BI__c!=null){
            rule1 = OACmap.get(OA.Order_Approval_Criteria_BI__c).First_Treshold_BI__c != null ? OACmap.get(OA.Order_Approval_Criteria_BI__c).First_Treshold_BI__c : 0;
            rule2 = OACmap.get(OA.Order_Approval_Criteria_BI__c).Second_Threshold_BI__c != null ? OACmap.get(OA.Order_Approval_Criteria_BI__c).Second_Threshold_BI__c : 0;
        }else{
            rule1 = 0;
            rule2 = 0;
        }
        
        //if pricing rule based criteria check the number of approval levels necessary
        if(OACmap.get(OA.Order_Approval_Criteria_BI__c).Rule_Type_BI__c != 'Treshold'
           &&OACmap.get(OA.Order_Approval_Criteria_BI__c).Approval_Levels_BI__c == '1'){
        	rule1 = 0;
            rule2 = 9001; //It's over 9000!!!
        }else if(OACmap.get(OA.Order_Approval_Criteria_BI__c).Rule_Type_BI__c != 'Treshold'
        		 &&OACmap.get(OA.Order_Approval_Criteria_BI__c).Approval_Levels_BI__c == '2'){
        	rule1 = 0;
            rule2 = 0;
        }
        
        system.debug('Processing: ' + OA.Name + ' rule1: ' + rule1 + ' rule2: ' + rule2 + ' status: ' + OA.Status_BI__c + ' discount: ' +OA.Order_Discount_BI__c);
        Order_vod__c O = Omap.get(OA.Order_BI__c);
        
        if(Trigger.isBefore){
            system.debug('isBefore');
            //fill email addresses
            OA.Primary_Account_Email_BI__c = O.Wholesaler_vod__r.Primary_Email_BI__c;
            OA.Secondary_Email_BI__c = O.Wholesaler_vod__r.Secondary_Email_BI__c;
            OA.Additional_Email_BI__c = O.Wholesaler_vod__r.Additional_Email_BI__c;
            OA.Backoffice_Email_BI__c = O.Createdby.Backoffice_Email_BI__c  ;
            //fill final approved checkbox
            if((OA.Order_Discount_BI__c >= rule1 && OA.Order_Discount_BI__c >= rule2 && OA.Status_BI__c=='Approved by National Sales Manager')||
               (OA.Order_Discount_BI__c >= rule1 && OA.Order_Discount_BI__c < rule2 && OA.Status_BI__c=='Approved by District Manager')  ){
                OA.Final_Approved_BI__c = true;
            }
        }
        
        if(Trigger.isAfter){
            system.debug('isAfter');
            //discount larger than rule one 
            if(OA.Order_Discount_BI__c > rule1 && OA.Status_BI__c=='Pending'){
                system.debug('DM notif: '+ OA.Approving_District_Manager_BI__c);
                if(OA.Approving_District_Manager_BI__c==null)OA.Approving_District_Manager_BI__c.addError('The district manager is missing and cannot be notified.');
                else{
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(OA.Approving_District_Manager_BI__c);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateID(tempid);
                    mail.setReplyTo(OA.Lastmodifiedby.Email);
                    mail.setSenderDisplayName(System.Label.CHC_SenderName);
                    mail.setWhatId(OA.Id);
                    mails.add(mail);
                }
            }
            //discount larger than rule two with status 
            if(OA.Order_Discount_BI__c > rule2 && OA.Status_BI__c=='Approved by District Manager'){
                system.debug('NM notif');
                if(OA.Approving_National_Sales_Manager_BI__c==null)OA.Approving_National_Sales_Manager_BI__c.addError('The national manager is missing and cannot be notified.');
                else{
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(OA.Approving_National_Sales_Manager_BI__c);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateID(tempid);
                    mail.setReplyTo(OA.Lastmodifiedby.Email);
                    mail.setSenderDisplayName(System.Label.CHC_SenderName);
                    mail.setWhatId(OA.Id);
                    mails.add(mail);    
                }
            }
            //status new is rejected
            if(OA.Status_BI__c=='Rejected'){
                system.debug('Rep reject');
                //Void the order
                O.Override_Lock_vod__c = true;
                O.Status_vod__c = 'Voided_vod';
                AppOmap.put(O.Id, O);
                //break up with rep in email, it's not her fault
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(OA.Createdbyid);
                //System.debug('Targeted: '+T);
                mail.setSaveAsActivity(false);
                mail.setTemplateID(rejectTemp);
                mail.setReplyTo(OA.Lastmodifiedby.Email);
                mail.setSenderDisplayName(System.Label.CHC_SenderName);
                mail.setWhatId(OA.Id);
                mails.add(mail);
            }
            //last needed approval was done
            if((OA.Order_Discount_BI__c >= rule1 && OA.Order_Discount_BI__c >= rule2 && OA.Status_BI__c=='Approved by National Sales Manager')||
               (OA.Order_Discount_BI__c >= rule1 && OA.Order_Discount_BI__c < rule2 && OA.Status_BI__c=='Approved by District Manager')  ){
                system.debug('Everything approved notif');
                //send notif to rep, district manager
               /* set<Id> targets = new set<Id>();
                
                //targets.add(OA.LastmodifiedbyId); //rep
                if(OA.Approving_District_Manager_BI__c!=null) targets.add(OA.Approving_District_Manager_BI__c);//manager
                else OA.Approving_District_Manager_BI__c.addError('VEEVA_BI_CRC_Approval_History: The district manager is missing and cannot be notified.');
                for(Id T : targets){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(T);
                    //System.debug('Targeted: '+T);
                    mail.setSaveAsActivity(false);
                    mail.setTemplateID(externalTemp);
                    mail.setReplyTo(OA.CreatedBy.Email);
                    mail.setSenderDisplayName(System.Label.CHC_SenderName);
                    mail.setWhatId(OA.Id);
                    mails.add(mail);
                }*/
                //change order status to approved
                O.Override_Lock_vod__c = true;
                O.Approval_Status_BI__c = 'Approved';
                AppOmap.put(O.Id, O);   
            }   
        }
    }
    system.debug('Orders to update: ' + AppOmap.size());
    system.debug('Mails to send: ' + mails.size());
    try{
        update AppOmap.values();
        Messaging.sendEmail(mails);
    }catch (Exception e){
        Trigger.new[0].addError('There was an error sending required notification mails within VEEVA_BI_CRC_Approval_History trigger: ' + e);
    }
    system.debug('VEEVA_BI_CRC_Approval_History FINISHED');
    
}