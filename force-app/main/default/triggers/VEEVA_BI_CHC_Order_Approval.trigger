/*
Changelog by G 2014-07-01
1. the approval has to be raised in the case of differences between the following fields in Order_Line_vod:
- Free_Goods_Rule_vod__c and Free_Goods_vod__c
- Line_Discount_Rule_vod__c and Line_Discount_vod__c
- Payment_Rule_vod__c and Payment_vod__c

2. in addition to the fields listed above, the approval must be raised in case of difference between Payment_Rule_vod__c and Payment_vod__c in Order_vod

3. the code must also consider the new Order_Record_Type_BI field in Order Approval Criteria to determine if the Order Type has a valid criteria; this allows
to limit the approval to only transfer orders (which has been requested) and foresees the support of different criteria by order type in the future.


Modified: 2015-09-30
Author:   Raphael Krausz <raphael.krausz@veeva.com>
Description:
    Still process an order if Send_for_Approval_BI__c is set to true, even if there are no
    lines for approval


*/
trigger VEEVA_BI_CHC_Order_Approval on Order_vod__c (before insert, before update, after insert, after update) {
    system.debug('VEEVA_BI_Order_Approval TRIGGER STARTING.');
    list<Order_Approval_BI__c> OAs = new list<Order_Approval_BI__c>();

    list<Order_vod__c> Os = new list<Order_vod__c>();
    set<Id> Oids = new set<Id>();


    /************************* Discound dispacther ******************/
    //if(trigger.isBefore && trigger.isUpdate &&
    //   trigger.old[0].recordtypeid != trigger.new[0].recordtypeid &&
    //   trigger.old[0].recordtypeid == '012K00000000pqd'
    //   )
    //OLD trigger.new[0].id == 'a1FK0000004RT5hMAG' &&
    Integer sizu = Trigger.new.size();
    for (Integer i = 0; i < sizu; i++) {
        System.Debug('HELLO Order' + trigger.new[i]);
        if (trigger.isUpdate == true && trigger.new[i].Buying_Group_Master_Order_BI__c ==  true &&
                trigger.old[i].Status_vod__c != 'Submitted_vod' && trigger.new[i].Status_vod__c == 'Submitted_vod') {
            System.Debug('HELLO MELLO');
            VEEVA_OrderConsolidation.PassDiscount2Child(trigger.new[i]);
        }
    }

    /************************* Discound dispacther ******************/

    //ENTRY CRITERIA
    for (Order_vod__c O : Trigger.new) {
        if (O.Status_vod__c != 'Submitted_vod') {
            system.debug('Not Submitted_vod: ' + O.Id);
            continue;
        }
        Oids.add(O.id);
        Os.add(O);
    }

    if (Oids.size() == 0) {
        System.debug('None met submitted criteria.');
        return;
    }

    User U = [SELECT Id, ManagerId, National_Sales_Manager_BI__c, Country_Code_BI__c, Username
              FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

    Order_Approval_Criteria_BI__c OAC = new Order_Approval_Criteria_BI__c();
    list<Order_Approval_Criteria_BI__c> OACs = new list<Order_Approval_Criteria_BI__c>();

    //get the OACs
    OACs = [SELECT Id,
            First_Treshold_BI__c,
            Second_Threshold_BI__c,
            Rule_Type_BI__c,
            Order_Record_Type_BI__c
            FROM Order_Approval_Criteria_BI__c
            WHERE Country_Code_BI__c = :U.Country_Code_BI__c ];

    if (OACs.size() == 0) {
        system.debug('The user '  + U.Username + ' does not have the relevant order approval criteria record. ' + U.Country_Code_BI__c);
        return;
    }

    //Check if orders have been handled
    set<Id> handledOs = new set<Id>();
    for (Order_Approval_BI__c OA : [SELECT External_Id_BI__c
                                    FROM Order_Approval_BI__c
                                    WHERE External_Id_BI__c in :Oids]) {
        handledOs.add(OA.External_Id_BI__c);
    }

    //prepare rectype devname map
    map<Id, Recordtype> RTmap = new map<Id, Recordtype>([SELECT Id, Developername FROM Recordtype WHERE sobjecttype = 'Order_vod__c']);

    /************** CSABA 2015.06.30. Soft Limit Approval START ****************************************/
    System.Debug('CSABA START');
    set<ID> setHandledbySoftCriteria = new set<ID>();
    //set<ID> setPendingParent = new Set<ID>();
    set<ID> setSplittedchildtoSkip =  new Set<ID>();
    for (Order_vod__c O : Os) {
        if (handledOs.contains(O.Id)) {
            system.debug('This order already has an OA record, skipping. ' + O.Id + '/' + O.Name);
            continue;
        }


        //set the child ordr  status  to pending

        //get order rectype name
        String ORTName = RTmap.get(O.RecordtypeId).Developername;

        boolean OACfound = false;
        System.Debug('This order already has an OA record, skipping. OAC' + OACs.size()  + ' Looking  for Criteria for ORTName = ' + ORTName + ' and Order ' + O.id + '/' + O.Name);
        for (Order_Approval_Criteria_BI__c OACc : OACs) {
            System.Debug('CSABA OACc.Order_Record_Type_BI__c = ' + OAcc.Order_Record_Type_BI__c);
            System.Debug('CSABA Check Order status here ' + O.Approval_Status_BI__c);
            if (OAcc.Order_Record_Type_BI__c == ORTName ) {
                OAC = OACc;
                OACfound = true;
                if (
                    Trigger.isBefore
                    && O.Approval_Status_BI__c != 'Rejected'
                    && O.Approval_Status_BI__c != 'Approved'
                    && (O.HasLineWithApproval__c > 0 || O.Send_for_Approval_BI__c == true)
                ) {
                    System.Debug('CSABA setting Status to Pending');
                    O.Approval_Status_BI__c = 'Pending';
                }
                setHandledbySoftCriteria.add(O.id);
            } else {
                System.Debug('CSABA. No match. Sorry ');
            }
        }

        if (!OACfound) {
            System.debug('No appropriate type of criteria was found: ' + ORTName);
            continue;
        }
        
        //Attila - 2016.01.05. 
        //In case of Treshhold and Pricing rule email sending don't quit from Trigger 
        //if (O.HasLineWithApproval__c == 0 && O.Send_for_Approval_BI__c == false) {
        //    system.debug('This order does not have any lines for Aproval and is not flagged for approval. Skip it ' + O.Id + ' O.Name = ' +  O.Name);
            //O.Approval_Status_BI__c = 'N/A';
        //    continue;
        //}
        
        

        if (O.HasLineWithApproval__c == 0 && O.Send_for_Approval_BI__c == false && OACs[0].Rule_Type_BI__c == 'Soft Limit') {
            system.debug('This order does not have any lines for Aproval and is not flagged for approval. Skip it ' + O.Id + ' O.Name = ' +  O.Name);
            //O.Approval_Status_BI__c = 'N/A';
            continue;
        }

        if (O.Parent_Order_vod__c != NULL) {
            system.debug('This order is a child order with splitted children. Skip it . BUT DO NOT LET  VIKTOR TO HANDLE' + O.Id);
            setSplittedchildtoSkip.add(O.id);
            continue;
        }

        //if we are here we  have  an Order with lines for approval set to true  AND we have a criteria  for  this Order Type. Lets  create Order_Approval_BI__c record
        system.debug('Creating object based on Order: ' + O.Name + ', ' + O.Id + ' User: ' + U.Username + ' Criteria: ' + OAC.Id + ' Discount: ' + O.Order_Max_Discount_CHC__c);
        Order_Approval_BI__c OA = new Order_Approval_BI__c(
            External_Id_BI__c = O.Id, //IT  is OK  only  if there is  ony  1  Criteria  / country
            Order_Approval_Criteria_BI__c = OAC.Id,
            Account_BI__c = O.Account_vod__c,
            Approving_District_Manager_BI__c = U.ManagerId,
            Approving_National_Sales_Manager_BI__c = U.National_Sales_Manager_BI__c,
            Status_BI__c = 'Pending',
            DateTime_BI__c = O.DateTime_vod__c,
            List_Amount_BI__c = O.List_Amount_vod__c,
            Net_Amount_BI__c = O.Net_Amount_vod__c,
            Order_BI__c = O.id,
            //Final_Approved_BI__c=true,
            Order_Date_BI__c = O.Order_Date_vod__c,
            Order_Discount_BI__c = O.Order_Max_Discount_CHC__c,
            Order_Net_Amount_BI__c = O.Order_Net_Amount_vod__c
        );
        OAs.add(OA);
    }

    if (OAs.size() > 0) {
        system.debug('Order approvals upserting: ' + OAs.size());
        upsert OAs External_Id_BI__c;  //CSABA 2015.06.30.  My OA  is also  upserted  here
        OAs.clear();
    }
    /**********************************************************************************************
    in case of splitted order do not forget  to set the child  orders  approval  status  to Pending
    for(Order_vod__c O : Os)
       {
        ID  parentu = O.Parent_Order_vod__c;
        if(parentu == NULL)
           continue;

        if(setPendingParent.contains(parentu))
           o.Approval_Status_BI__c = 'Pending';
        }

    return;
    }

    if(setSplittedchildtoSkip.size() > 0)
       return;
     ***********************/

    System.Debug('CSABA END OAS size = ' +  OAs.size());
    /************** CSABA 2015.06.30. Soft Limit Approval END ****************************************/


    //Collect the OLs in case it is a Pricing rule based OAC
    map<Id, list<Order_Line_vod__c>> OLmap = new map<Id, list<Order_Line_vod__c>>();  ////CSABA 2015.06.30. moved here from few lines below.

    for (Order_Line_vod__c OL : [SELECT Id,
                                 Free_Goods_Rule_vod__c,
                                 Free_Goods_vod__c,
                                 Line_Discount_Rule_vod__c,
                                 Line_Discount_vod__c,
                                 Payment_Rule_vod__c,
                                 Payment_vod__c,
                                 Order_vod__c
                                 FROM Order_Line_vod__c
                                 WHERE Order_vod__c in :Oids]) {

        list<Order_Line_vod__c> OLlist = new list<Order_Line_vod__c>();

        if (OLmap.containsKey(OL.Order_vod__c)) {
            OLlist = OLmap.get(OL.Order_vod__c);
            OLlist.add(OL);
            OLmap.put(OL.Order_vod__c, OLlist);
        } else {
            OLlist.add(OL);
            OLmap.put(OL.Order_vod__c, OLlist);
        }
    }//Order lines collected by O.Id

    //prepare rectype devname map
    //CSABA 2015.06.30. moved few lines above. map<Id, Recordtype> RTmap = new map<Id, Recordtype>([SELECT Id, Developername FROM Recordtype WHERE sobjecttype = 'Order_vod__c']);

    //go through orders
    for (Order_vod__c O : Os) {
        //CSABA 2015.07.01. If  the order  was alrady  handled  by soft Criteria skip.
        //Attila - 2016.01.05. 
        //Treshhold and Pricing rule email sending  
        //if (setHandledbySoftCriteria.contains(O.id)) {
         if (setHandledbySoftCriteria.contains(O.id) && (OAC.Rule_Type_BI__c <> 'Treshold' && OAC.Rule_Type_BI__c <> 'Pricing Rule')) {
            system.debug('This order was process by soft Criteria, skipping. ' + O.Id);
            system.debug('OAC.Rule_Type_BI__c ' + OAC.Rule_Type_BI__c);
            continue;
        }
        //CSABA 2015.07.01. If  the order  was alrady  handled  by soft Criteria skip.

    if (handledOs.contains(O.Id)) {
            system.debug('This order already has an OA record, skipping. ' + O.Id);
            continue;
        }
        //get order rectype name
        String ORTName = RTmap.get(O.RecordtypeId).Developername;

        boolean OACfound = false;
        //get the relevant approval criteria - OAC
        //Request G3 2017-07-01
        for (Order_Approval_Criteria_BI__c OACc : OACs) {
            if (OAcc.Order_Record_Type_BI__c == ORTName ) {
                OAC = OACc;
                OACfound = true;
                if (Trigger.isBefore) {
                    System.Debug('CSABa call Viktor, CSABA Call Viktor  Hello Mello');
                    O.Approval_Status_BI__c = 'Pending';
                }
            }
        }

        if (!OACfound) {
            System.debug('No approprate type of criteria was found: ' + ORTName);
            continue;
        }

        if (OAC.Rule_Type_BI__c == 'Treshold') {
            Decimal discount =  O.Order_Max_Discount_CHC__c == null ? 0 : O.Order_Max_Discount_CHC__c;
            Decimal treshold = OAC.First_Treshold_BI__c == null ? 0 : OAC.First_Treshold_BI__c;

            if (discount < treshold) {
                system.debug('Treshold not hit: ' + O.id);
                if (Trigger.isBefore) {
                    O.Approval_Status_BI__c = 'Approved';
                    system.debug('V1. Creating object based on Order: ' + O.Name + ', ' + O.Id + ' User: ' + U.Username + ' Criteria: ' + OAC.Id + ' Discount: ' + O.Order_Max_Discount_CHC__c);
                    Order_Approval_BI__c OA = new Order_Approval_BI__c(
                        External_Id_BI__c = O.Id,
                        Order_Approval_Criteria_BI__c = OAC.Id,
                        Account_BI__c = O.Account_vod__c,
                        Approving_District_Manager_BI__c = U.ManagerId,
                        Approving_National_Sales_Manager_BI__c = U.National_Sales_Manager_BI__c,
                        Status_BI__c = 'Approved',
                        DateTime_BI__c = O.DateTime_vod__c,
                        List_Amount_BI__c = O.List_Amount_vod__c,
                        Net_Amount_BI__c = O.Net_Amount_vod__c,
                        Order_BI__c = O.id,
                        Final_Approved_BI__c = true,
                        Order_Date_BI__c = O.Order_Date_vod__c,
                        Order_Discount_BI__c = O.Order_Max_Discount_CHC__c,
                        Order_Net_Amount_BI__c = O.Order_Net_Amount_vod__c );
                    OAs.add(OA);
                }//isBefore
            }//treshold hit

            if (Trigger.isAfter) {
                //treshold not hit
                //Create object
                system.debug('V2. Creating object based on Order: ' + O.Name + ', ' + O.Id + ' User: ' + U.Username + ' Criteria: ' + OAC.Id + ' Discount: ' + O.Order_Max_Discount_CHC__c);
                Order_Approval_BI__c OA = new Order_Approval_BI__c(
                    External_Id_BI__c = O.Id,
                    Order_Approval_Criteria_BI__c = OAC.Id,
                    Account_BI__c = O.Account_vod__c,
                    Approving_District_Manager_BI__c = U.ManagerId,
                    Approving_National_Sales_Manager_BI__c = U.National_Sales_Manager_BI__c,
                    DateTime_BI__c = O.DateTime_vod__c,
                    List_Amount_BI__c = O.List_Amount_vod__c,
                    Net_Amount_BI__c = O.Net_Amount_vod__c,
                    Order_BI__c = O.id,
                    Order_Date_BI__c = O.Order_Date_vod__c,
                    Order_Discount_BI__c = O.Order_Max_Discount_CHC__c,
                    Order_Net_Amount_BI__c = O.Order_Net_Amount_vod__c );
                OAs.add(OA);
            }//isAfter
            //end of Treshold handling
        } else {
            //Pricing rule based logic
            //check the pricing criterias
            boolean approvalNeeded = false;
            //G2 criteria:
            system.debug('O.Payment_vod__c: ' +  O.Payment_vod__c);
            system.debug('O.Payment_Rule_vod__c: ' +  O.Payment_Rule_vod__c);

            if (O.Payment_vod__c != O.Payment_Rule_vod__c)
                approvalNeeded = true;
            //G1 criteria:
            //go through the associated order lines
            for (Order_Line_vod__c OL : OLmap.get(O.Id)) {
                system.debug(' OL.Free_Goods_Rule_vod__c: ' +  OL.Free_Goods_Rule_vod__c);
                system.debug(' OL.Free_Goods_vod__c: ' +  OL.Free_Goods_vod__c);
                system.debug(' OL.Line_Discount_Rule_vod__c: ' +  OL.Line_Discount_Rule_vod__c);
                system.debug(' OL.Line_Discount_vod__c: ' +  OL.Line_Discount_vod__c);
                system.debug(' OL.Payment_vod__c: ' +  OL.Payment_vod__c);
                system.debug(' OL.Payment_Rule_vod__c: ' +  OL.Payment_Rule_vod__c);
                Decimal fgr =  OL.Free_Goods_Rule_vod__c == null ? 0 : OL.Free_Goods_Rule_vod__c;
                Decimal fg =  OL.Free_Goods_vod__c == null ? 0 : OL.Free_Goods_vod__c;
                Decimal ldr =  OL.Line_Discount_Rule_vod__c == null ? 0 : OL.Line_Discount_Rule_vod__c;
                Decimal ld =  OL.Line_Discount_vod__c == null ? 0 : OL.Line_Discount_vod__c;

                if (fgr != fg) approvalNeeded = true;
                if (ldr != ld) approvalNeeded = true;
                if (OL.Payment_vod__c != OL.Payment_Rule_vod__c) approvalNeeded = true;

            }
            system.debug('final approvalNeeded: ' + approvalNeeded);

            if (!approvalNeeded) {
                system.debug('approvalNeeded not: ' + O.id);
                if (Trigger.isBefore) {
                    O.Approval_Status_BI__c = 'Approved';
                    system.debug('V3. Creating object based on Order: ' + O.Name + ', ' + O.Id + ' User: ' + U.Username + ' Criteria: ' + OAC.Id + ' Discount: ' + O.Order_Max_Discount_CHC__c);
                    Order_Approval_BI__c OA = new Order_Approval_BI__c(
                        External_Id_BI__c = O.Id,
                        Order_Approval_Criteria_BI__c = OAC.Id,
                        Account_BI__c = O.Account_vod__c,
                        Approving_District_Manager_BI__c = U.ManagerId,
                        Approving_National_Sales_Manager_BI__c = U.National_Sales_Manager_BI__c,
                        Status_BI__c = 'Approved',
                        DateTime_BI__c = O.DateTime_vod__c,
                        List_Amount_BI__c = O.List_Amount_vod__c,
                        Net_Amount_BI__c = O.Net_Amount_vod__c,
                        Order_BI__c = O.id,
                        Final_Approved_BI__c = true,
                        Order_Date_BI__c = O.Order_Date_vod__c,
                        Order_Discount_BI__c = O.Order_Max_Discount_CHC__c,
                        Order_Net_Amount_BI__c = O.Order_Net_Amount_vod__c );
                    OAs.add(OA);
                }
            }
            //if not handled, add a new approval
            if (Trigger.isAfter) {
                //Create object
                system.debug('V4. Creating object based on Order: ' + O.Name + ', ' + O.Id + ' User: ' + U.Username + ' Criteria: ' + OAC.Id + ' Discount: ' + O.Order_Max_Discount_CHC__c);
                Order_Approval_BI__c OA = new Order_Approval_BI__c(
                    External_Id_BI__c = O.Id,
                    Order_Approval_Criteria_BI__c = OAC.Id,
                    Account_BI__c = O.Account_vod__c,
                    Approving_District_Manager_BI__c = U.ManagerId,
                    Approving_National_Sales_Manager_BI__c = U.National_Sales_Manager_BI__c,
                    DateTime_BI__c = O.DateTime_vod__c,
                    List_Amount_BI__c = O.List_Amount_vod__c,
                    Net_Amount_BI__c = O.Net_Amount_vod__c,
                    Order_BI__c = O.id,
                    Order_Date_BI__c = O.Order_Date_vod__c,
                    Order_Discount_BI__c = O.Order_Max_Discount_CHC__c,
                    Order_Net_Amount_BI__c = O.Order_Net_Amount_vod__c );
                OAs.add(OA);
            }//isAfter
        }//end of Pricing rule handling
    }//order cycle

    system.debug('Order approvals upserting: ' + OAs.size());
    upsert OAs External_Id_BI__c;  //CSABA 2015.06.30.  My OA  is also  upserted  here

    system.debug('VEEVA_BI_Order_Approval TRIGGER FINISHED.');
}