/****************************************************************************************************************************
Trigger operate  only  if the  ApprovalCriteriaType__c is SoftLimit

when Record  is  created  send mail.   do this  only  once
when record  is approved  send mail.   do this ony once
****************************************************************************************************************************/
trigger VEEVA_BI_CHC_SoftApproval_Handling on Order_Approval_BI__c (after insert, before update) 
{
    //ID  MyUserID = [Select Id  from User where userName = 'csaba.matyas-peter@bi.mvs' limit 1].id;     
    //we must  build  a map  of  Createby to  managerId
    Map<ID,ID> mapUser2ManagerId = new Map<Id,ID>();
    set<ID> setUsers =  new Set<ID>();
    for(Integer i = 0; i < Trigger.size; i++)
        setUsers.add(trigger.new[i].CreatedbyId); 
    
    //List<User> UsersandManagers = [Select Id, ManagerId from User where id in :setUsers];
    for(User U  : [Select Id, ManagerId from User where id in :setUsers])
        mapUser2ManagerId.put(U.id,U.ManagerId);
    
    Set<ID> Order2UpdateIDs = new Set<ID>();   //load here the ID  of those  Orders where we have  to  set Approved status
    Map<ID,String> mapOrder_Status =  new Map<Id,String>();
    
    
    //Collect Templates     
    Id approvedtempid = [SELECT Id, 
                        DeveloperName 
                        FROM EmailTemplate 
                        WHERE DeveloperName = 'CHC_Approval' 
                        LIMIT 1].Id;
    Id rejectTemp = [SELECT Id, 
                        DeveloperName 
                        FROM EmailTemplate 
                        WHERE DeveloperName = 'CHC_Rejection' 
                        LIMIT 1].Id;
                                    
    list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();            
    Boolean sendMail = false;       
    for(Integer i = 0; i < Trigger.size; i++)
    {
        if(trigger.new[i].ApprovalCriteriaType__c != 'Soft_Limit_Criteria')
           continue;    
           
        //2015.09.22.  Inset Levent mail banning logic
        
        //2015.09.22.  Inset Levent mail banning logic          
          
        //if after insert  send mail  to  Approver 
        if(trigger.isAfter)
           {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    System.Debug('CSABA 1.1  get manager id  for ' + Trigger.new[i].CreatedbyId);
                    ID testID = mapUser2ManagerId.get(Trigger.new[i].CreatedbyId);
                    mail.setTargetObjectId(mapUser2ManagerId.get(Trigger.new[i].CreatedbyId)); //MyUserID
                    //m;
                    /////mail.setTargetObjectId(MyUserID);
                    mail.setSaveAsActivity(false);
                    system.Debug('CSABA 1.3 ' + approvedtempid);
                    mail.setTemplateID(approvedtempid);
                    
                    system.Debug('CSABA 1.4 ' + Trigger.new[i].Lastmodifiedby.Email);
                    mail.setReplyTo(Trigger.new[i].Lastmodifiedby.Email);
                    
                    system.Debug('CSABA 1.5 ' + System.Label.CHC_SenderName);
                    mail.setSenderDisplayName(System.Label.CHC_SenderName);
                    
                    system.Debug('CSABA 1.6 whatID ' + Trigger.new[i].Id);
                    mail.setWhatId(Trigger.new[i].Id);
                    mails.add(mail);    //put it outside the for 
                    system.Debug('CSABA mail obj: ' + mail);   
                    
                    sendMail = true;    
                    System.Debug('CSABA  sending mail to ' + testID  + '   mails: ' + mail ); 
           }   
       
            
        //if  before update  check  is  it  was  just approve/rejected  and act  accordingly. Send mail  to  Creator 
        //and in case wholeseller email uncked and is APPROVED send to wholeseller (Wholesaler_Account_Partner_vod__c  --->)
        if(trigger.isBefore)
        {
            if(Trigger.old[i].Status_BI__c==Trigger.new[i].Status_BI__c) 
               continue;
            
            if(Trigger.old[i].Status_BI__c != 'Approved by District Manager' && Trigger.new[i].Status_BI__c == 'Approved by District Manager') 
            {
                System.Debug('Halleluje  brothers. APPROVED');
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(Trigger.new[i].Createdbyid); //MyUserID
                ////mail.setTargetObjectId(MyUserID);
                mail.setSaveAsActivity(false);
                mail.setTemplateID(approvedtempid);
                mail.setReplyTo(Trigger.new[i].Lastmodifiedby.Email);
                mail.setSenderDisplayName(System.Label.CHC_SenderName);
                mail.setWhatId(Trigger.new[i].Id);
                mails.add(mail);                
                
                sendMail = true;
                
                Order2UpdateIDs.add(trigger.new[i].Order_BI__c);
                mapOrder_Status.put(trigger.new[i].Order_BI__c,'Approved');
                
                //2015.08.04  set Final_Approved_BI__c to true. Wgy?  Levent and zoltan said
                Trigger.new[i].Final_Approved_BI__c =  true;
                
                System.Debug('CSABA  sending mail to ' + Trigger.new[i].Createdbyid +  '   mails: ' + mail ); 
            }
            else if(Trigger.old[i].Status_BI__c != 'Rejected' && Trigger.new[i].Status_BI__c == 'Rejected')  
            {
                System.Debug('Halleluje  brothers. REJECT');
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(Trigger.new[i].Createdbyid); //MyUserID
                /////mail.setTargetObjectId(MyUserID);
                mail.setSaveAsActivity(false);
                mail.setTemplateID(rejectTemp);
                mail.setReplyTo(Trigger.new[i].Lastmodifiedby.Email);
                mail.setSenderDisplayName(System.Label.CHC_SenderName);
                mail.setWhatId(Trigger.new[i].Id);
                mails.add(mail);                
                
                sendMail = true;
                
                Order2UpdateIDs.add(trigger.new[i].Order_BI__c);
                mapOrder_Status.put(trigger.new[i].Order_BI__c,'Rejected');
                
                System.Debug('CSABA  sending mail to ' + Trigger.new[i].Createdbyid + '   mails: ' + mail );            
            }       
        }

    }
    
    if(sendMail && mails.size() > 0) //2015.08.06.  add mails.size() > 0
    {
       //2015.09.22. OVERRULE. DO NOT SEND MAILS. Messaging.sendEmail(mails);   //put it outside the for
    }  
       
    //collect Orders  and set their status
    System.Debug('CSABA If we are here we update the Order records status');
    List<Order_vod__c> Os = [Select  Id,Approval_Status_BI__c ,Parent_Order_vod__c, Name, Override_Lock_vod__c
                             from Order_vod__c 
                             where  id in :Order2UpdateIDs or Parent_Order_vod__c in :Order2UpdateIDs
                            ];
                            
    System.Debug('CSABA If we are here we update the Order records status for nor  of Orders :' + Os.size());                           

   for(Integer i=0; i < OS.size(); i++)
   {
    String status = mapOrder_Status.get(Os[i].id);
    System.Debug('CSABA Status ' + status);
    
    if(status == NULL)  //in case of splitted child orders the status might be empty.
       status = mapOrder_Status.get(Os[i].Parent_Order_vod__c);
       
    System.Debug('CSABA we are going to change  status ' + Os[i].Approval_Status_BI__c +  ' to status = ' + status + ' for order ' + Os[i].Name);   
    Os[i].Approval_Status_BI__c = status;
    
    OS[i].Override_Lock_vod__c = true; //2015.07.17.  set override lock  because the Order might have been submitted
   }
   
   Database.Saveresult[] SaveResult = Database.update(Os);
   
   String ErrorMessage = '';
   if(SaveResult != null)
           {
            Integer i = 0;
            for(Database.Saveresult result : SaveResult)
               {
               if(!result.isSuccess())
                   {
                    Database.Error[] errs = result.getErrors();
                    for(Database.Error err: errs)
                        {                               
                            ErrorMessage = ErrorMessage + ' AddrID' + Os[i].id + ' ' + err.getStatusCode() + ' : ' + err.getMessage() + ' fields: ' + err.getFields() + '\r';
                        }     
                   }                                 
                   i++;
               }//end for   
            }//end if error   
                              
}