/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This trigger is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: trigger checks if there's overlapping TOT record for the user JIRA: DM-5022
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
**/
trigger VEEVA_BI_CHECK_OVERLAPPING_TOT on Time_Off_Territory_vod__c (before insert, before update) {
    
    string userCountry = [SELECT id, Country_Code_BI__c FROM User WHERE id =:UserInfo.getUserId() LIMIT 1].Country_Code_BI__c;
    if (userCountry != 'US') return;
    
    if (trigger.isUpdate){
        if (Trigger.new[0].Name == Trigger.old[0].Name) return;
    }
    
    boolean allowOverlap = Trigger.new[0].Allow_overlapping_TOT_BI__c;
    boolean hasOverlap = false;
    date startDate = Trigger.new[0].Date_vod__c;
    string start;
    integer startInt;
    integer hours;
    DateTime startDateTime;
    DateTime newStartDateTime;
    DateTime newEndDateTime;
    
    if (Trigger.new[0].Time_vod__c != 'All Day'){
        start = Trigger.new[0].Start_Time_vod__c.substringBefore(':');
        startInt = integer.valueOf(start);    
        hours = (integer)Trigger.new[0].Hours_vod__c;
        startDateTime = startDate;
        newStartDateTime = startDateTime.addHours(startInt);
        newEndDateTime = newStartDateTime.addHours(hours);
    }
    
    for (Time_Off_Territory_vod__c t : [SELECT Id, Name, Date_vod__c, Start_Time_vod__c, Hours_vod__c,Time_vod__c FROM Time_Off_Territory_vod__c WHERE OwnerId =:Trigger.new[0].OwnerId]){
        date oldStartDate = t.Date_vod__c;
        string oldStart;
        integer oldStartInt;
        integer oldhours;
        DateTime oldStartDateTime;
        DateTime newStartDateTimeOld;
        DateTime newEndDateTimeOld;
        
        if (t.Time_vod__c != 'All Day' && t.Start_Time_vod__c != null){
            oldStart = t.Start_Time_vod__c.substringBefore(':');
            oldStartInt = integer.valueOf(oldStart);
            oldhours = (integer)t.Hours_vod__c;
            oldStartDateTime = oldStartDate;
            newStartDateTimeOld = oldStartDateTime.addHours(oldStartInt);
            newEndDateTimeOld = newStartDateTimeOld.addHours(oldhours);
        }
        
        if (t.Time_vod__c == 'All Day'){
            if (oldStartDate == startDate && !allowOverlap)
            	Trigger.new[0].addError('Please note that you have overlapping TOT record ('+ t.Name +'). If you wish to continue please check the Allow overlapping TOT box below and hit Save.'); 
        }else{
            if (newStartDateTime >= newStartDateTimeOld && newEndDateTime <= newEndDateTimeOld && !allowOverlap)
            	Trigger.new[0].addError('Please note that you have overlapping TOT record ('+ t.Name +'). If you wish to continue please check the Allow overlapping TOT box below and hit Save.'); 
        }
    }

}