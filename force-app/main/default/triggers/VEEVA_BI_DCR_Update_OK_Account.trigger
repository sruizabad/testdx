/*******************************************************************************
The trigger updates HCP&HCO Accounts, Child Accounts and Addresses with 
OK external IDs so the OK update will upsert them correctly 
(in case of addresses replace not upsert).

Created by: Viktor Fasi
Last modified date: 2013.10.01

TODO: -
********************************************************************************/
trigger VEEVA_BI_DCR_Update_OK_Account on V2OK_Data_Change_Request__c (after update) {
    system.debug('Starting VEEVA_BI_DCR_Update_OK_Account.');
    list<Account> HCPs = new list<Account>();
    list<Account> HCOs = new list<Account>();
    list<Child_Account_vod__c> ACTs = new list<Child_Account_vod__c>();
    set<Address_vod__c> ADDRs = new set<Address_vod__c>();
    
    //map the recordtypeids for later use
    map<String, Id> RTs = new map<String, Id>();
    for(RecordType RT : [SELECT Id, Name, DeveloperName FROM RecordType WHERE IsActive = true AND SobjectType = 'V2OK_Data_Change_Request__c' ]){
        RTs.put(RT.DeveloperName, RT.Id);
    }
  	//create maps for duplicate check
  	Map<Id,String>HCPmap = new Map<Id,String>();
  	Map<Id,String>HCOmap = new Map<Id,String>();
  	Map<Id,String>ACTmap = new Map<Id,String>();
  	
  	//fill up the maps
    for(Integer i=0; i < Trigger.size ; i++){
        V2OK_Data_Change_Request__c DCR = Trigger.new[i];
        //skip if not cegedim response arrival  
        if(!(Trigger.old[i].Data_Request_Response__c == null && DCR.Data_Request_Response__c!=null)){
            continue;
        }
		//HCP check
        if(HCPmap.get(DCR.Professional_DS__c)==null||HCPmap.get(DCR.Professional_DS__c)=='Rejected'){
	        if(DCR.OK_Validated_IND_ID__c!=null && DCR.Data_Request_Response__c.contains('VERIFIED')){
	            if(DCR.HCP_Temp_External_ID_BI__c!=null)HCPmap.put(DCR.HCP_Temp_External_ID_BI__c,DCR.OK_Validated_IND_ID__c);
	        }else if(DCR.Data_Request_Response__c.contains('REJECTED')
	        		&& (DCR.RecordTypeId == RTs.get('New_Professional_at_New_Workplace') || DCR.RecordTypeId == RTs.get('New_Professional_at_Existing_Workplace')) ){
	        	if(DCR.HCP_Temp_External_ID_BI__c!=null)HCPmap.put(DCR.HCP_Temp_External_ID_BI__c,'Rejected');
	        }
    	}else{
           /* Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

			String adminEmailAddress = Data_Connect_Setting__c.getInstance('AdminEmail').Value__c;
				
			mail.setToAddresses(new String[] {adminEmailAddress});
			mail.setSenderDisplayName('VR load warning');
			mail.setSubject('An account occured multiple times in the VR');

			String plainTextBody = '';
			plainTextBody += 'Check out the following DCRs: ' + DCR.Id;
			plainTextBody += '\n HCP Account ID: ' + DCR.Professional_DS__c;
			plainTextBody += '\n HCP Account OK ID: ' + DCR.OK_Validated_IND_ID__c;
			plainTextBody += '\n Response: ' + DCR.Data_Request_Response__c;
			mail.setPlainTextBody(plainTextBody);
			
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
        }//end of HCP
		//HCO check
		if(HCOmap.get(DCR.Organisation_Account__c)==null||HCOmap.get(DCR.Organisation_Account__c)=='Rejected'){
	        if(DCR.OK_Validated_WKP_ID__c!=null && DCR.Data_Request_Response__c.contains('VERIFIED')){ 
	            if(DCR.HCO_Temp_External_ID_BI__c!=null)HCOmap.put(DCR.HCO_Temp_External_ID_BI__c,DCR.OK_Validated_WKP_ID__c);
	        }else if(DCR.Data_Request_Response__c.contains('REJECTED')
	        		&& (DCR.RecordTypeId == RTs.get('New_Professional_at_New_Workplace') || DCR.RecordTypeId == RTs.get('Existing_Professional_at_New_Workplace'))){
	        	if(DCR.HCO_Temp_External_ID_BI__c!=null)HCOmap.put(DCR.HCO_Temp_External_ID_BI__c,'Rejected');
	        }
		}else{
            /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

			String adminEmailAddress = Data_Connect_Setting__c.getInstance('AdminEmail').Value__c;
				
			mail.setToAddresses(new String[] {adminEmailAddress});
			mail.setSenderDisplayName('VR load warning');
			mail.setSubject('An account occured multiple times in the VR');

			String plainTextBody = '';
			plainTextBody += 'Check out the following DCRs: ' + DCR.Id;
			plainTextBody += '\n HCO Account ID: ' + DCR.Organisation_Account__c;
			plainTextBody += '\n HCO Account OK ID: ' + DCR.OK_Validated_WKP_ID__c;
			plainTextBody += '\n Response: ' + DCR.Data_Request_Response__c;
			mail.setPlainTextBody(plainTextBody);
			
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
		}//end of HCO
        //ACT check - if created by Acc trigger AND validated by Onekey
        if(ACTmap.get(DCR.ACT_Temp_External_ID_BI__c)==null||ACTmap.get(DCR.ACT_Temp_External_ID_BI__c)=='Rejected'){
	        if(DCR.OK_Validated_ACT_ID__c!=null && DCR.ACT_Temp_External_ID_BI__c != null && DCR.Data_Request_Response__c.contains('VERIFIED')){
	            if(DCR.ACT_Temp_External_ID_BI__c!=null)ACTmap.put(DCR.ACT_Temp_External_ID_BI__c,DCR.OK_Validated_IND_ID__c + DCR.OK_Validated_WKP_ID__c);
	        }else if(DCR.Data_Request_Response__c.contains('REJECTED')
	        		&& (DCR.RecordTypeId == RTs.get('New_Professional_at_New_Workplace') 
	        			|| DCR.RecordTypeId == RTs.get('Existing_Professional_at_New_Workplace') 
	        			|| DCR.RecordTypeId == RTs.get('New_Professional_at_Existing_Workplace')) ){
	        	if(DCR.ACT_Temp_External_ID_BI__c!=null)ACTmap.put(DCR.ACT_Temp_External_ID_BI__c,'Rejected');
	        }
        }else{
            /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

			String adminEmailAddress = Data_Connect_Setting__c.getInstance('AdminEmail').Value__c;
				
			mail.setToAddresses(new String[] {adminEmailAddress});
			mail.setSenderDisplayName('VR load warning');
			mail.setSubject('A CA occured multiple times in the VR');

			String plainTextBody = '';
			plainTextBody += 'Check out the following DCRs: ' + DCR.Id;
			plainTextBody += '\n CA ID: ' + DCR.ACT_Temp_External_ID_BI__c;
			plainTextBody += '\n CA OK ID: ' + DCR.OK_Validated_IND_ID__c + DCR.OK_Validated_WKP_ID__c;
			plainTextBody += '\n Response: ' + DCR.Data_Request_Response__c;
			mail.setPlainTextBody(plainTextBody);
			
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
		}
        //add to addr to del if DCR is verified and  Existing_Professional_at_New_Workplace
        if((DCR.RecordTypeId == RTs.get('Professional_Update_Delete') || 
        	DCR.RecordTypeId == RTs.get('New_Professional_at_New_Workplace') || 
        	DCR.RecordTypeId == RTs.get('Existing_Professional_at_New_Workplace') || 
        	DCR.RecordTypeId == RTs.get('Workplaces'))
           && DCR.ADDR_Temp_External_ID_BI__c!= null && DCR.ADDR_Temp_External_ID_BI__c!='' ){
            Address_vod__c ADDR = new Address_vod__c(
                Id = DCR.ADDR_Temp_External_ID_BI__c
            );
            ADDRs.add(ADDR);
        }
        
    }//end of map cycle
    
    //based on the maps define the objects to be updated
    for(Id HCPid : HCPmap.keySet()){
		Account HCP = new Account();
	            HCP.Id = HCPid;
	            if(HCPmap.get(HCPid)=='Rejected') HCP.OK_Status_Code_BI__c = 'Rejected'; 
	            else HCP.OK_External_Id_BI__c = HCPmap.get(HCPid);
	        HCPs.add(HCP);	
    }
    for(Id HCOid : HCOmap.keySet()){
		Account HCO = new Account();
	            HCO.Id = HCOid;
	            if(HCOmap.get(HCOid)=='Rejected') HCO.OK_Status_Code_BI__c = 'Rejected'; 
	            else HCO.OK_External_Id_BI__c = HCOmap.get(HCOid);
	        HCOs.add(HCO);	
    }
    for(Id ACTid : ACTmap.keySet()){
		Child_Account_vod__c ACT = new Child_Account_vod__c();
	            ACT.Id = ACTid;
	            if(ACTmap.get(ACTid)=='Rejected') ACT.OK_Status_Code_BI__c = 'Rejected'; 
	            else ACT.External_ID2__c = ACTmap.get(ACTid);
	        ACTs.add(ACT);	
    }
      
    //add the set of ADDR to a list to be able to DML them
    list<Address_vod__c> ADDRslist = new list<Address_vod__c>();
    ADDRslist.addall(ADDRs);
    
    //update records
    Database.SaveResult[] srList1 = Database.update(HCPs, false);
    Database.SaveResult[] srList2 = Database.update(HCOs, false);
    Database.SaveResult[] srList3 = Database.update(ACTs, false);
    if(ADDRslist.size()>0) Database.DeleteResult[] srList4 = Database.delete(ADDRslist, false);
	
	/***log results***
    for (Database.SaveResult sr : srList1) {
        if (sr.isSuccess()) {
            System.debug('Successful HCP update: ' + sr.getId());
        }
        else {     
            for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred during HCP update: ' + err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Fields that affected this error: ' + err.getFields());
            }
        }
    }
    for (Database.SaveResult sr : srList2) {
        if (sr.isSuccess()) {
            System.debug('Successful HCO update: ' + sr.getId());
        }
        else {     
            for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred during HCO update: ' + err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Fields that affected this error: ' + err.getFields());
            }
        }
    }
    for (Database.SaveResult sr : srList3) {
        if (sr.isSuccess()) {
            System.debug('Successful ACT update: ' + sr.getId());
        }
        else {     
            for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred during ACT update: ' + err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Fields that affected this error: ' + err.getFields());
            }
        }
    }
    for (Database.DeleteResult sr : srList4) {
        if (sr.isSuccess()) {
            System.debug('Successful address delete: ' + sr.getId());
        }
        else {     
            for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred during address delete: ' + err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Fields that affected this error: ' + err.getFields());
            }
        }
    }
    /*** ***/
}