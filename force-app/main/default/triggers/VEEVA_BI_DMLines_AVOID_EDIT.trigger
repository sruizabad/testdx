trigger VEEVA_BI_DMLines_AVOID_EDIT on Diagnostic_Management_Lines_BI__c (before update, before delete) {
    
    system.debug('VEEVA_BI_DMLines_AVOID_EDIT started!');
    
    set<id> DMLIDs = new set<id>();
    map<id, boolean> DMLMap = new map<id, boolean>();
    
    for (Diagnostic_Management_Lines_BI__c d : trigger.new)
    {     
    	DMLIDs.add(d.id);
    }	    
    
    if (DMLIDs.size() > 0) 
    {
        for (Diagnostic_Management_Lines_BI__c d : [SELECT Id, Name, Diagnostic_Management_BI__r.Letter_to_send_to_Lab_BI__c, Laboratory_BI__c
                                                    FROM Diagnostic_Management_Lines_BI__c Where id in:DMLIDs]) {
			DMLMap.put(d.id,d.Diagnostic_Management_BI__r.Letter_to_send_to_Lab_BI__c);	            
        }
    }
    
    for (Diagnostic_Management_Lines_BI__c d : trigger.new)
    {     
        if (DMLMap.get(d.Id) == true) {
            d.addError('Record is locked!');
        }	
    }

}