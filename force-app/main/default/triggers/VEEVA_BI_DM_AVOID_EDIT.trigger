trigger VEEVA_BI_DM_AVOID_EDIT on Diagnostic_Management_BI__c (before update, before delete) {
    
    system.debug('VEEVA_BI_DM_AVOID_EDIT started!');
    
    for (integer i = 0; i<Trigger.new.size(); i++) {
        if (Trigger.old[i].Letter_to_send_to_Lab_BI__c == true && Trigger.new[i].Letter_to_send_to_Lab_BI__c == true) {
            Trigger.new[i].addError('Record is locked!');
        }
    }
    

}