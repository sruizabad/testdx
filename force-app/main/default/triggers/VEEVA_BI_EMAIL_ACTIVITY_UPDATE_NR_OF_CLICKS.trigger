trigger VEEVA_BI_EMAIL_ACTIVITY_UPDATE_NR_OF_CLICKS on Email_Activity_vod__c (after insert, after update) {
    
    system.debug('VEEVA_BI_EMAIL_ACTIVITY_UPDATE_NR_OF_CLICKS trigger starting... '); 
    
    List<Sent_Fragment_vod__c> frags = new List<Sent_Fragment_vod__c>();

       for (Email_Activity_vod__c e : trigger.new)
        {   
            if (e.Event_type_vod__c == 'Clicked_vod')
            {
                system.debug('Attila Sent_Email_vod__c: ' + e.Sent_Email_vod__c);
				system.debug('Attila Approved_Document_vod__c: ' + e.Approved_Document_vod__c);
                
                // if there's no Approved_Document_vod__c or Sent_Email_vod__c quit from trigger
                if (e.Approved_Document_vod__c == null || e.Sent_Email_vod__c == null) return;
                
                List<Sent_Fragment_vod__c > fraglist =
                  [
                    SELECT id, name, Number_of_Clicks_BI__c, Sent_Fragment_vod__c, Sent_Email_vod__c 
                          FROM Sent_Fragment_vod__c 
                          WHERE Sent_Fragment_vod__c =:e.Approved_Document_vod__c
                          AND Sent_Email_vod__c =:e.Sent_Email_vod__c	
                  ];  
                
                system.debug('Attila fraglist: ' + fraglist);
                
                decimal nrofclicks;
                
                for (Sent_Fragment_vod__c s : fraglist) 
                {
                    if (s.Number_of_Clicks_BI__c == null) nrofclicks = 1;
                    else nrofclicks = s.Number_of_Clicks_BI__c + 1;
                    
                    frags.add(new Sent_Fragment_vod__c(Number_of_Clicks_BI__c = nrofclicks, id = s.Id ));
                }                 
                
            }

       }
    system.debug('Attila updatelist: ' + frags);
    if (frags.size() > 0) update frags;
    
    }