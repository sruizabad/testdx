//Created by Erik Dozsa | 2015/07/01
//This trigger was developed to support the proper working of VEEVA_BI_ORDER_CONFIRMATION trigger.
//VEEVA_BI_ORDER_CONFIRMATION sends e-mail confirmation to the customer about the submitted order against it.
//In case of splitted order we must ensure that the e-mail is only sent out when the system generated all the
//Child Order type of order records - so the e-mail could be sent out due to the proper availability of the data.

trigger VEEVA_BI_FLAG_CHILD_ORDERS_ON_PARENT on Order_vod__c (after insert, after update) 
{
    decimal parentQty;
    set<id> masterOrderIds = new set<id>();
    list<Order_vod__c> parentOrders = new list<Order_vod__c>();
    list<Order_vod__c> childOrders = new list<Order_vod__c>();
    list<Order_vod__c> childOrdersToUpdate = new list<Order_vod__c>();
    list<Order_vod__c> ordersToUpdate = new list<Order_vod__c>();
    
    if(trigger.isAfter && trigger.isUpdate)
    {
        for(Order_vod__c o : trigger.new)
        {
            if(o.Master_Order_vod__c == false && o.Parent_Order_vod__c != null)   
            //if(o.Order_Category_BI__c == 'Child Order')
            {
                masterOrderIds.add(o.Parent_Order_vod__c);
            }
        }
        
        if(masterOrderIds.isEmpty())
        {
            system.debug('ERIK RETURN');
            return;
        }
             
        system.debug('ERIK CONTINUE');
        parentOrders = [ select id, Order_Total_Quantity_vod__c, Order_Confirmation_Check_BI__c, Order_Confirmation_Sent_BI__c, Override_Lock_vod__c from Order_vod__c where ID in: masterOrderIds ];
        
        for(Order_vod__c po : parentOrders)
        {   
            if(po.Order_Confirmation_Sent_BI__c == true)
                return;
            
            childOrders = [ select id, Order_Total_Quantity_vod__c, Order_Confirmation_Lines_Check_BI__c from Order_vod__c where Parent_Order_vod__c =: po.id ];
            decimal qty = 0;
            decimal linesQty = 0;
            
            for(Order_vod__c poc : childOrders)
            {
                system.debug('ERIK TOTAL QTY: ' + poc.Order_Total_Quantity_vod__c);
                system.debug('ERIK TOTAL QTY ROLLUP: ' + poc.Order_Confirmation_Lines_Check_BI__c);
                
                //if(poc.Order_Total_Quantity_vod__c == poc.Order_Confirmation_Lines_Check_BI__c)
                if(poc.Order_Total_Quantity_vod__c == null)
                    qty = qty + 0;
                else
                    qty = qty + poc.Order_Total_Quantity_vod__c;
                    
                linesQty = linesQty + poc.Order_Confirmation_Lines_Check_BI__c;
            }
            
            system.debug('ERIK QTY: ' + qty + ' PARENT QTY: ' + po.Order_Total_Quantity_vod__c);
            system.debug('ERIK LINES QTY: ' + linesQty);
                                    
            if(qty == po.Order_Total_Quantity_vod__c && linesQty == po.Order_Total_Quantity_vod__c)
            {
                po.Order_Confirmation_Check_BI__c = true;
                po.Override_Lock_vod__c = true;
                ordersToUpdate.add(po);
                system.debug('ERIK ORDER CONF CHECK: ' + po.Order_Confirmation_Check_BI__c);
            }
        }
        
        system.debug('ERIK ORDERS TO UPD: ' + ordersToUpdate.size());
        if(ordersToUpdate.size() != 0)
        {
            update ordersToUpdate;
        }
        
        
        //NO NEED TO USE FUTURE CALL
        //call future method
        //VEEVA_BI_ORDER_CONF_HELPER.updateOrders(masterOrderIds);
    }
    
    
}