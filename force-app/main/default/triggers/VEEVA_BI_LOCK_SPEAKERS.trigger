/**
 * Trigger VEEVA_BI_LOCK_SPEAKERS on Event_Attendee_vod__c
 *
 * Original author: Csaba Matyas-Peter
 *
 * Modified by: Raphael Krausz <raphael.krausz@veeva.com>
 * Date: 		2015-07-27
 * Description:
 *	- You may not add any attendee after the event has completed.
 * 	- You may not modify any attendee after the event has completed
 * 		EXCEPTION: Status_vod__c field (and other specified ignored fields)
 *	- You may not add any speaker after compliance approval.
 * 	- You may not modify any speaker after compliance approval.
 * 		EXCEPTION: Status_vod__c field (and other specified ignored fields)
 * 	- You may not delete any speaker after compliance approval.
 *
 *
 * Modified by: Raphael Krausz <raphael.krausz@veeva.com>
 * Date: 		2015-07-27
 * Description:
 * 	Code clean-up and made it so that the Trigger can accept batches of size > 1
 *
 *
 * Modified by: Raphael Krausz <raphael.krausz@veeva.com>
 * Date: 		2015-07-28
 * Description:
 * 	Added new requirement.
 *  	- You many not delete an attendee after the event has completed.
 *
 *
 * Modified by: Raphael Krausz <raphael.krausz@veeva.com>
 * Date: 		2015-07-29
 * Description:
 * 	Added new requirement.
 *  	- You many not change an attendee to/from being a speaker after approval.
 *
 *
 */

Trigger VEEVA_BI_LOCK_SPEAKERS on Event_Attendee_vod__c (before delete, before insert, before update) {


	//check if the Profile  of the user/owner  fall in the  white List
	{
		Id profileId = Userinfo.getProfileId();

		Event_Management_Lock_Settings__c eventManagementLockSettings
		    = Event_Management_Lock_Settings__c.getInstance(profileId);

		Boolean isBanned = Boolean.valueOf(eventManagementLockSettings.isBanned__c);

		if (isBanned == NULL || isBanned == false)
			return;
	}


	List<Event_Attendee_vod__c> eventAttendeeTriggerList;

	if (Trigger.isDelete) {
		eventAttendeeTriggerList = Trigger.old;
	} else {
		eventAttendeeTriggerList = Trigger.new;
	}


	Map<Id, Medical_Event_vod__c> medicalEventMap;
	{
		Set<Id> medicalEventIdSet = new Set<Id>();
		for (Event_Attendee_vod__c eventAttendee : eventAttendeeTriggerList) {
			medicalEventIdSet.add(eventAttendee.Medical_Event_vod__c);
		}

		medicalEventMap = new Map<Id, Medical_Event_vod__c>(
		    [
		        SELECT Id, Event_Status_BI__c
		        FROM Medical_Event_vod__c
		        WHERE Id IN :medicalEventIdSet
		    ]
		);
	}

	for (Event_Attendee_vod__c eventAttendee : eventAttendeeTriggerList) {

		Boolean isSpeaker = eventAttendee.Speaker_BI__c;

		Id medicalEventId = eventAttendee.Medical_Event_vod__c;
		String eventStatus = medicalEventMap.get(medicalEventId).Event_Status_BI__c;


		System.debug(
		    'Event Attendee ID: ' + eventAttendee.Id
		    + ', isSpeaker: ' + isSpeaker
		    + ', eventStatus: ' + eventStatus
		);


		String completedStatus = 'Completed / Closed';
		Set<String> validStatusSet = new Set<String> { 'Under Approval', 'Approved', completedStatus };

		Boolean eventHasCompleted = eventStatus == completedStatus;
		Boolean attendeeIsSpeakerOrEventHasCompleted = isSpeaker || eventHasCompleted;

		System.debug(
		    'Event status: ' + eventStatus
		    + '; eventHasCompleted: ' + eventHasCompleted
		    + '; attendeeIsSpeakerOrEventHasCompleted: ' + attendeeIsSpeakerOrEventHasCompleted
		);


		if (validStatusSet.contains(eventStatus)) {

			if (
			    Trigger.isInsert
			    && attendeeIsSpeakerOrEventHasCompleted
			) {

				String errorMessage;
				if (eventHasCompleted) {
					System.debug('Error: Label.EVENT_MANAGEMENT_COMPLETED_NO_NEW_ATTENDEE');
					errorMessage = Label.EVENT_MANAGEMENT_COMPLETED_NO_NEW_ATTENDEE;
				} else {
					System.debug('Error: Label.EVENT_MANAGEMENT_APPROVED_NO_SPEAKER_CHANGES');
					errorMessage = Label.EVENT_MANAGEMENT_APPROVED_NO_SPEAKER_CHANGES;
				}

				eventAttendee.addError(errorMessage);

				continue;

			}


			if (Trigger.isDelete) {

				if (isSpeaker) {
					System.debug('Error: Label.EVENT_MANAGEMENT_APPROVED_NO_SPEAKER_DELETES');
					String errorMessage = Label.EVENT_MANAGEMENT_APPROVED_NO_SPEAKER_DELETES;
					eventAttendee.addError(errorMessage);
					continue;
				}

				if (eventHasCompleted) {
					System.debug('Error: Label.EVENT_MANAGEMENT_COMPLETED_NO_ATTENDEE_DELETES');
					String errorMessage = Label.EVENT_MANAGEMENT_COMPLETED_NO_ATTENDEE_DELETES;
					eventAttendee.addError(errorMessage);
					continue;
				}

			} else {

				Boolean illegalFieldChange = false;

				if (Trigger.isUpdate) {

					// Need to check if the speaker field has been changed
					// this is because if may missed being checked under other circumstances

					Event_Attendee_vod__c oldEventAttendee = Trigger.oldMap.get(eventAttendee.Id);

					if (eventAttendee.Speaker_BI__c != oldEventAttendee.Speaker_BI__c) {
						System.debug('Speaker field changed!');
						illegalFieldChange = true;
					}

				}



				if (attendeeIsSpeakerOrEventHasCompleted) {

					// the attendee is a speaker or the event has completed

					// Check which fields have been modified - if the status is the only field which has changed
					// (excluding the admin fields) then it is permitted


					Set<String> ignoreFields = new Set<String> {
						// Please use *only* lowercase in ignoreFields!!!!!!
						'status_vod__c',
						'lastmodifieddate',
						'lastmodifiedbyid',
						'systemmodstamp',
						'incurred_expense_vod__c',
						'expense_attendee_type_vod__c'
					};


					Map<String, Schema.SObjectField> schemaFieldMap
					    = Schema.SObjectType.Event_Attendee_vod__c.fields.getMap();

					Event_Attendee_vod__c oldEventAttendee = Trigger.oldMap.get(eventAttendee.Id);

					for (String field : schemaFieldMap.keySet()) {
						System.debug('Field being examined: "' + field + '"');
						if (ignoreFields.contains(field.toLowerCase())) {
							continue;
						}
						if (eventAttendee.get(field) != oldEventAttendee.get(field)) {
							System.debug('Field changed: "' + field + '"');
							illegalFieldChange = true;
							break;
						}
					}

				}

				if (illegalFieldChange) {
					String errorMessage;
					if (eventHasCompleted) {
						System.debug('Error: Label.EVENT_MANAGEMENT_COMPLETED_NO_ATTENDEE_CHANGES');
						errorMessage = Label.EVENT_MANAGEMENT_COMPLETED_NO_ATTENDEE_CHANGES;
					} else {
						System.debug('Error: Label.EVENT_MANAGEMENT_APPROVED_NO_SPEAKER_UPDATES');
						errorMessage = Label.EVENT_MANAGEMENT_APPROVED_NO_SPEAKER_UPDATES;
					}
					eventAttendee.addError(errorMessage);
				}
			}
		}
	}
}