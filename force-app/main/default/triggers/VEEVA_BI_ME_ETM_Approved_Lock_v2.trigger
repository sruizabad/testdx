trigger VEEVA_BI_ME_ETM_Approved_Lock_v2 on Event_Team_Member_BI__c (before delete, before insert, before update) {
    //check for profile and possibly skip it all
    String profileName = [SELECT Id, Name FROM Profile WHERE Id = :Userinfo.getProfileId() LIMIT 1].Name;
    System.debug('profile: '+profileName);
    if(profileName.toUpperCase().contains('DATA')||
       profileName.toUpperCase().contains('ADMIN')||
       profileName.toUpperCase().contains('MARKETING')){
        system.debug('returning');
        return;
    }
    
    //or do what you have to do
    map<Id, String> MEmap = new map<Id, String>();
    if(Trigger.isDelete||Trigger.isUpdate){
        for(Event_Team_Member_BI__c ETM : Trigger.old){
            MEmap.put(ETM.Event_Management_BI__c, '');
        }
    }else{
        for(Event_Team_Member_BI__c ETM : Trigger.new){
            MEmap.put(ETM.Event_Management_BI__c, '');
        }
    }
    
    
    for(Medical_Event_vod__c ME : [SELECT Id, Event_Status_BI__c FROM Medical_Event_vod__c WHERE Id in :MEmap.keySet()]){
        MEmap.put(ME.Id,Me.Event_Status_BI__c);
    }
    
    //if field modified is on the list, let it go
    Map <String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get('Event_Team_Member_BI__c').getDescribe().fields.getMap();
    
    //get the custom setting values (all fields'), add them to a set
    Map <String, Schema.SObjectField> settingFields = Schema.getGlobalDescribe().get('Event_Management_Lock_Settings__c').getDescribe().fields.getMap();
    Event_Management_Lock_Settings__c EL = Event_Management_Lock_Settings__c.getInstance();
    set<String> excludedFields = new set<String>();
    
    for(Schema.SObjectField sfield : settingFields.Values()){
        String fname = sfield.getDescribe().getName();
        String settingValue;
        
        settingValue = string.valueOf(EL.get(fname));
        if(settingValue!=null) excludedFields.addAll(settingValue.split(','));
    }
    
    system.debug('excludedFields: '+excludedFields);
    
    for (Integer i = 0; i < Trigger.size; i++) 
    {
        if(Trigger.isUpdate)
        {

            // Added 2015-02-05 - error should only be thrown on approved. Raphael Krausz
            System.debug('MEmap.get(trigger.old[i].Event_Management_BI__c: ' + MEmap.get(trigger.old[i].Event_Management_BI__c));
            if(MEmap.get(trigger.old[i].Event_Management_BI__c) == 'Approved')
            {
                //go through all the fields, and match them
                for(Schema.SObjectField sfield : objectFields.Values())
                {
                    String fname = sfield.getDescribe().getName();
                    system.debug('checking: ' + fname);
                    //exclude fields defined in the settings
                    if(excludedFields.contains(fname)) continue;                        
                    
                    if(trigger.old[i].get(fname)!=trigger.new[i].get(fname))
                    {
                        //throw error message
                        system.debug('Adding error for: '+fname + ' details: '+sfield);
                        trigger.new[i].addError( System.Label.Medical_Event_Locked );
                    }
                }
            }       
        }
        else if(Trigger.isInsert)
        {
            if(MEmap.get(trigger.new[i].Event_Management_BI__c) == 'Approved')
            {
                system.debug('ETM lock');
                trigger.new[i].addError(System.Label.Medical_Event_Locked);
            }
        }
        else if(Trigger.isDelete){
            if(MEmap.get(trigger.old[i].Event_Management_BI__c) == 'Approved')
            {
                system.debug('ETM lock');
                trigger.old[i].addError(System.Label.Medical_Event_Locked);
            }
        }
    }

}