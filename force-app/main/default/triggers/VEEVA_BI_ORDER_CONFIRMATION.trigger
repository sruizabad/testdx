trigger VEEVA_BI_ORDER_CONFIRMATION on Order_vod__c (after insert, after update, before insert, before update) 
{
    User currentUser = [select name, profile.name, phone, email from user where id =: UserInfo.getUserId()];
    
    set<ID> rtIDs = new set<ID>();
    map<Id, String> rtMap = new map<Id, String>();
    
    //Getting record owner info
    map<Id, User> userInfoMap = new map<Id, User>();
    List<Id> userIds = new List<Id>();
    
    //get owner's user ids
    for(Order_vod__c o : trigger.new)
    {
    	userIds.add(o.OwnerId);
    }
    
    //get user info for userids
    for(User user: [SELECT name, profile.name, phone, email FROM user WHERE id IN: userIds])
    {
    	userInfoMap.put(user.id, user);
    }
    
    
    for(RecordType rt : [select id, name from recordtype where sobjecttype = 'Order_vod__c'])
    {
    	rtMap.put(rt.id, rt.name);
    }
       
    string profilename; //[select name from profile where id =: UserInfo.getProfileId()].name;
    string repName; //[select Name from User where ID =: UserInfo.getUserId()].Name;
    string repPhone; //[select Phone from User where ID =: UserInfo.getUserId()].Phone;
    string repMail; //[select email from User where ID =: UserInfo.getUserId()].email;
    
    profilename = currentUser.profile.name;
    repName = currentUser.name;
    repPhone = currentUser.phone;
    repMail = currentUser.email;    
    
    system.debug('ERIK PROFILE NAME: ' + profilename);
    if(profilename <> 'System Administrato')
    {

        //list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
          
        //send mail only when the record is updated/created     
        if(trigger.isBefore && trigger.isUpdate)
        {   
                 
            for(Order_vod__c o : trigger.new)
            {

                if(o.Order_Confirmation_Sent_BI__c != true)
                {
                    /*
                    if(o.Wholesaler_Account_Partner_vod__c != null)
                    {
                        Account_Partner_vod__c accPartner = [select id, Wholesaler_BI__c from Account_Partner_vod__c where Id =: o.Wholesaler_Account_Partner_vod__c limit 1];
                        if(accPartner.Wholesaler_BI__c != null)
                        {
                            Account payerWholesaler = [select id, Primary_Email_BI__c from Account where Id =: accPartner.Wholesaler_BI__c limit 1];
                                    
                                if(payerWholesaler.Primary_Email_BI__c != null || payerWholesaler.Primary_Email_BI__c != '')
                                    //system.debug('ERIK Wholesaler_Partner_Email_BI__c is populated by Workflow');
                                    o.Wholesaler_Partner_Email_BI__c = payerWholesaler.Primary_Email_BI__c;
                         }
                    }
                    
                    if(o.Payer_vod__c != null)
                    {
                        Account_Partner_vod__c curentAccountPartner = [select id, Account_vod__c from Account_Partner_vod__c where Id =: o.Payer_vod__c limit 1];
                        if(curentAccountPartner.Account_vod__c != null)
                        {
                            Account currentAccount = [select id, Primary_Email_BI__c from Account where Id =: curentAccountPartner.Account_vod__c]; 
                            if(currentAccount.Primary_Email_BI__c != null)
                            {
                                //system.debug('ERIK Payer_Primary_Email_BI__c is populated by Workflow');
                                o.Payer_Primary_Email_BI__c = currentAccount.Primary_Email_BI__c;
                            }
                        }
                    }*/
                    
                    //set record owner's info 
                    o.Rep_s_Email_BI__c = userInfoMap.get(o.OwnerId).email; //repMail;
                    o.Rep_s_Name_BI__c = userInfoMap.get(o.OwnerId).name;  //repName;
                    o.Rep_s_Phone_BI__c = userInfoMap.get(o.OwnerId).phone; //repPhone; 
                    
                    String recTypeName = rtMap.get(o.RecordTypeId);
                    o.Order_Type_BI__c = recTypeName;
                     
                    
                    //Splitted Orders
                    if(o.Order_Confirmation_BI__c == true && 
                       //o.Status_vod__c == 'Submitted_vod' && 
                       o.Master_Order_vod__c == true &&
                       o.Parent_Order_vod__c == null && 
                       o.Order_Confirmation_Check_BI__c == true && 
                      (o.Approval_Status_BI__c == 'N/A' || o.Approval_Status_BI__c == 'Approved')
                       /*&& o.Order_Confirmation_Sent_BI__c == false*/ )
                    {
                        //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        //String[] toAddress = new List<String>();
                        
                        o.Order_Confirmation_To_Send_BI__c = true;
              
                        //make sure only one mail is sent | VOD triggers run multiple times which could cause multiple e-mail sending
                        o.Order_Confirmation_Sent_BI__c = true;
                        o.Override_Lock_vod__c = true;
                        //system.debug('ALL IS OK MAIL SHOULD BE SENT FOR SPLITTED!');
                        
                    }
                    
                    //Standalone Orders
                    else if(o.Order_Confirmation_BI__c == true &&
                            //o.Order_Category_BI__c == 'Standalone Order' && 
                            o.Master_Order_vod__c == false &&
                            o.Parent_Order_vod__c == null &&
                            o.Status_vod__c == 'Submitted_vod' &&
                           (o.Approval_Status_BI__c == 'N/A' || o.Approval_Status_BI__c == 'Approved') &&
                            o.Order_Confirmation_Sent_BI__c == false)
                    {
                        //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        //String[] toAddress = new List<String>();
                        
                        o.Order_Confirmation_To_Send_BI__c = true;
                        
                        //make sure only one mail is sent | VOD triggers run multiple times which could cause multiple e-mail sending
                        o.Order_Confirmation_Sent_BI__c = true;
                        o.Override_Lock_vod__c = true;
                        
                    }
                }
                else
                    system.debug('ERIK MAIL ALREADY SENT');
            }
            
            
            try
            {
                system.debug('Mail sending');
                //Messaging.sendEmail(mails);
            }
            catch (Exception e)
            {
                Trigger.new[0].addError('There was an error sending required notification mails within VEEVA_BI_ORDER_CONFIRMATION trigger: ' + e);
            }
        }
    }
    
    else
    {
        system.debug('ERIK NO LOOPS AS WE ARE SYS ADMIN');
    }
    
    
    
    
}