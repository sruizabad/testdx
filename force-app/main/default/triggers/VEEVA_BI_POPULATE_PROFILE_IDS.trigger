/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This trigger is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: trigger translates the selected profile names into a delimited list of SFDC id's. 
*				Store this list in Profile_IDs_1_BI__c (the extra two are for overflow in case a large number of profiles is selected)
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
*
* @Updates: Attila 2018.09.13. - populate text fields with profile names instead of Ids	
**/
trigger VEEVA_BI_POPULATE_PROFILE_IDS on HTML_Report_vod__c (before insert, before update) {
	
    //map<string, id> profileMap = new map<string, id>();
    //list<Profile> profileList = [SELECT Id, Name FROM Profile];
    string delimiter = ';';
    
    /*for (Profile p :profileList){
        profileMap.put(p.Name, p.Id);
    }*/
    
    for (Integer i=0; i<trigger.new.size(); i++) {
        
        if (trigger.new[i].Profile_Names_BI__c != null) {
            
            string[] tmpString = trigger.new[i].Profile_Names_BI__c.split(';');
            string profileIds1 = '';
            string profileIds2 = '';
            string profileIds3 = '';
            
            if (tmpString.size() > 1){
                for (string s :tmpString){
                    if (profileIds1.length() < 206){
                        //profileIds1 += profileMap.get(s) + delimiter;
                        profileIds1 += s + delimiter;
                    }else if (profileIds2.length() < 206){
                        //profileIds2 += profileMap.get(s) + delimiter;
                        profileIds2 += s + delimiter;
                    }else{
                        //profileIds3 += profileMap.get(s) + delimiter;
                        profileIds3 += s + delimiter;
                    } 
                }
            }else{
                //profileIds1 = profileMap.get(tmpString[0]);    
                profileIds1 = tmpString[0];    
            }    
            
            trigger.new[i].Profile_IDs_1_BI__c = profileIds1;
            trigger.new[i].Profile_IDs_2_BI__c = profileIds2;
            trigger.new[i].Profile_IDs_3_BI__c = profileIds3;
            
        }else{
            trigger.new[i].Profile_IDs_1_BI__c = '';
            trigger.new[i].Profile_IDs_2_BI__c = '';
            trigger.new[i].Profile_IDs_3_BI__c = '';
        }
    }
}