trigger VEEVA_BI_RDT_AVOID_EDIT on Requested_Diagnostic_Test_BI__c  (before update, before delete) {
    
    system.debug('VEEVA_BI_RDT_AVOID_EDIT started!');
    
    set<id> RDTIDs = new set<id>();
    map<id, boolean> RDTMap = new map<id, boolean>();
    boolean b = false;

    for (Requested_Diagnostic_Test_BI__c r : trigger.new)
    {     
    	RDTIDs.add(r.id);
    }	    
    
    if (RDTIDs.size() > 0) 
    {
        for (Requested_Diagnostic_Test_BI__c r : [SELECT Id, Diagnostic_Mananagement_Line_BI__r.Diagnostic_Management_BI__r.Letter_to_send_to_Lab_BI__c 
                                                  FROM Requested_Diagnostic_Test_BI__c Where id in:RDTIDs]) {
			RDTMap.put(r.id,r.Diagnostic_Mananagement_Line_BI__r.Diagnostic_Management_BI__r.Letter_to_send_to_Lab_BI__c);	            
        }        
    }    
        
    for(Requested_Diagnostic_Test_BI__c r : Trigger.new) 
    {
        Requested_Diagnostic_Test_BI__c oldRDT = Trigger.oldMap.get(r.Id);
        if (RDTMap.get(r.Id) == true) {
            if (oldRDT.Name != r.Name || oldRDT.Diagnostic_Management_Lab_Test_BI__c != r.Diagnostic_Management_Lab_Test_BI__c || 
                oldRDT.Diagnostic_Mananagement_Line_BI__c != r.Diagnostic_Mananagement_Line_BI__c ||
                oldRDT.Who_Pays_Dx_BI__c != r.Who_Pays_Dx_BI__c ||
                oldRDT.Who_Pays_Kit_BI__c != r.Who_Pays_Kit_BI__c ||
                oldRDT.Estimated_Cost_of_Test_BI__c != r.Estimated_Cost_of_Test_BI__c ||
                oldRDT.Final_Cost_of_Test_BI__c != r.Final_Cost_of_Test_BI__c ||
                oldRDT.Estimated_Cost_of_Kit_BI__c != r.Estimated_Cost_of_Kit_BI__c ||
                oldRDT.Final_Cost_of_Kit_BI__c != r.Final_Cost_of_Kit_BI__c) 
            {
                    r.addError('Record is locked!');
                }
        }        
    }

}