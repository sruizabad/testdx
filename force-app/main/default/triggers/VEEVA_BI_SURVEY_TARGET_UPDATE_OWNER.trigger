/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This trigger is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: trigger updates the Ownerid field of Survey_Target_vod__c with Survey_vod__r.Ownerid
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
**/
trigger VEEVA_BI_SURVEY_TARGET_UPDATE_OWNER on Survey_Target_vod__c (after insert) {
    
    system.debug('VEEVA_BI_SURVEY_TARGET_UPDATE_OWNER trigger starting... ');    
    
    List<Survey_Target_vod__c> ListSurvey = new List<Survey_Target_vod__c>();
    
    for (Survey_Target_vod__c s : trigger.new){    
        
        string channels = s.Channels_vod__c;
        
        id Surveyownerid = [SELECT Survey_vod__r.Ownerid, Name FROM Survey_Target_vod__c Where id =:s.id limit 1].Survey_vod__r.Ownerid;
        
        if (channels != null && channels.contains('Approved_Email'))
            ListSurvey.add(new Survey_Target_vod__c (Ownerid = Surveyownerid, id = s.id));
    }
    
    if (ListSurvey.size() > 0) update ListSurvey;
}