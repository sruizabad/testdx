/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This trigger is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: trigger avoids changing the name or create new territories with existing names
        or delete record if it's active in BITMAN Jira: DM-6947. 
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
*/
trigger VEEVA_BI_TERRITORY_VALIDATION on Territory (before insert, before update, before delete) {

    Set<id> terrIds = new Set<id>();
    Map <string, id> terrMap = new Map <string, id>();
    
    if (trigger.isInsert){
        system.debug('Attila isInsert!');
        for (Integer i=0; i<Trigger.new.size(); i++) {
            terrMap.put(Trigger.new[i].Name, Trigger.new[i].Id);
        }

        for (Territory t : [SELECT Id, Name FROM Territory WHERE Name IN:terrMap.keySet()]){
            terrIds.add(terrMap.get(t.Name));
        }

        for (Territory t : trigger.new){     
            if (terrIds.contains(t.id)) 
                t.addError('The territory name ' + '"' + t.name + '"' + ' already exists!');
        }         
    }
    
    if (trigger.isUpdate) {
    system.debug('Attila isUpdate!');        
        for (Integer i=0; i<Trigger.new.size(); i++) {
            //check if name is changed
            if (Trigger.new[i].Name != Trigger.old[i].Name)
            Trigger.new[i].addError('The territory name cannot be changed!');
                //terrMap.put(Trigger.new[i].Name, Trigger.new[i].Id);
        }

        /*for (Territory t : [SELECT Id, Name FROM Territory WHERE Name IN:terrMap.keySet()]){
            terrIds.add(terrMap.get(t.Name));
        }

        for (Territory t : trigger.new){     
            if (terrIds.contains(t.id)) 
              t.addError('The territory name ' + '"' + t.name + '"' + ' already exists!');
        } */
    }
    
    if (trigger.isDelete) {
    system.debug('Attila isDelete!');
        for (Integer i=0; i<Trigger.old.size(); i++) {
            terrMap.put(Trigger.old[i].Name, Trigger.old[i].Id);
        }

        //shouldn't delete records which are active in BITMAN
        for (BI_TM_Territory__c t : [SELECT BI_TM_TerritoryID__c, BI_TM_Is_Active__c 
                            FROM BI_TM_Territory__c WHERE BI_TM_TerritoryID__c IN:terrMap.values()]){
          if (t.BI_TM_Is_Active__c == true)
                terrIds.add(t.BI_TM_TerritoryID__c);
        }    
        
        for (Territory t : trigger.old){     
            if (terrIds.contains(t.id)) 
                t.addError('The territory is still active in BITMAN!');
        } 
        
    }
}