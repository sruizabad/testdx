/*
 * VEEVA_BI_TimeStamp_ChildAccount trigger
 *
 * Author: Raphael Krausz <raphael.krausz@veeva.com>
 * Date: 2015-02-24
 * Description:
 *
 *  When a primary parent is changed on an account ensure the
 *  Child Accounts have their LastModifiedDateTime timestamp updated.
 *
 *  As the Primary_vod__c flag on the Child_Account_vod__c object is a formula
 *  field the Child Account object doesn't actually get updated when an Account
 *  object has a change of primary parent. Hence, downstream processes cannot
 *  tell the change has occured simply from the Child Account object.
 *
 *  By manually updating the Child Account object we will update it's admin timestamp
 *  fields; thereby the downstream processes will be able to notice the change of primary.
 *
 *  NOTE: All Child Account objects have an external ID - External_ID_vod__c - comprised of
 *      the parent account Id + '__' + the child account Id
 *
 *  We also need to ensure that we update not only the Child Account record of the associated
 *  with the new primary parent, but also we need to update the Child Account record associated
 *  with the old primary parent.
 *
 */

trigger VEEVA_BI_TimeStamp_ChildAccount on Account (after update) {

    Set<String> childAccountExternalIds = new Set<String>();

    for (Account newAccount : Trigger.new) {
        Account oldAccount = Trigger.oldMap.get(newAccount.Id);

        if ( newAccount.Primary_Parent_vod__c != oldAccount.Primary_Parent_vod__c ) {

            if (! String.isBlank(oldAccount.Primary_Parent_vod__c)) {
                String oldExternalId = oldAccount.Primary_Parent_vod__c + '__' + oldAccount.Id;
                childAccountExternalIds.add(oldExternalId);
            }

            if (! String.isBlank(newAccount.Primary_Parent_vod__c)) {
                String newExternalId = newAccount.Primary_Parent_vod__c + '__' + newAccount.Id;
                childAccountExternalIds.add(newExternalId);
            }
        }
    }

    if ( childAccountExternalIds.isEmpty() ) {
        // None of the accounts had a change of primary
        // Nothing to do - so let's exit
        return;
    }

    System.debug('childAccountExternalIds: ' + childAccountExternalIds);

    List<Child_Account_vod__c> childAccounts =
        [
            SELECT Id
            FROM Child_Account_vod__c
            WHERE External_ID_vod__c IN :childAccountExternalIds
        ];


    if ( ! childAccounts.isEmpty() ) {
        update childAccounts;
    }

}