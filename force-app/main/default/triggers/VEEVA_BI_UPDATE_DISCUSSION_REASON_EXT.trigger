trigger VEEVA_BI_UPDATE_DISCUSSION_REASON_EXT on Call2_Discussion_vod__c (after insert) {
   
    system.debug('VEEVA_BI_UPDATE_DISCUSSION_REASON_EXT trigger is starting...');
    
    set<id> Calls = new set<id>();    
    List<Call2_Discussion_vod__c> updateDISCList = new List<Call2_Discussion_vod__c>();
    
    for (Call2_Discussion_vod__c c : trigger.new){ 
        Calls.add(c.Call2_vod__c);
    }
    
    for (Call2_Discussion_vod__c cd : [SELECT Id, Name, Initial_Reason_BI__c, Call2_vod__c FROM Call2_Discussion_vod__c 
                                       WHERE Call2_vod__c in:Calls ORDER BY Name LIMIT 1]) {
    	updateDISCList.add(new Call2_Discussion_vod__c (Initial_Reason_BI__c = true, id = cd.id));
    }          
    
    if (updateDISCList.size() > 0 ) update updateDISCList;
}