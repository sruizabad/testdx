trigger VEEVA_BI_UPDATE_EXPENSE_ATTENDEE_TYPE on Event_Attendee_vod__c (after insert) {
  
    system.debug('VEEVA_BI_UPDATE_EXPENSE_ATTENDEE_TYPE trigger starting... ');  
   
    ID UserID = Userinfo.getUserId();
    system.debug('UserID: ' + UserID); 
    String UserCountry = [SELECT Id, Name, Country_Code_BI__c FROM User Where id = :UserID limit 1].Country_Code_BI__c;
    system.debug('User Country: ' + UserCountry);
    
    if (UserCountry == '') return;
    
    List<Event_Attendee_vod__c> Attendee = new List<Event_Attendee_vod__c>();
    
    for (Event_Attendee_vod__c e : trigger.new){      

        if (UserCountry == 'JP') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'JPVEEVA', id = e.id)); 
        } else if (UserCountry == 'ES') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'ESVEEVA', id = e.id)); 
        } else if (UserCountry == 'MX') {
            if (e.Attendee_Type_vod__c == 'User') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SYSEMP', id = e.id)); 
            } else if (e.Attendee_Type_vod__c == 'Account') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'MXVEEVA', id = e.id)); 
            }
        } else if (UserCountry == 'GT') {
            if (e.Attendee_Type_vod__c == 'User') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SYSEMP', id = e.id)); 
            } else if (e.Attendee_Type_vod__c == 'Account') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'GTVEEVA', id = e.id)); 
            }
        } else if (UserCountry == 'HN') {
            if (e.Attendee_Type_vod__c == 'User') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SYSEMP', id = e.id)); 
            } else if (e.Attendee_Type_vod__c == 'Account') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'HNVEEVA', id = e.id)); 
            }
        } else if (UserCountry == 'CR') {
            if (e.Attendee_Type_vod__c == 'User') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SYSEMP', id = e.id)); 
            } else if (e.Attendee_Type_vod__c == 'Account') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'CRVEEVA', id = e.id)); 
            } 
        } else if (UserCountry == 'SV') {
            if (e.Attendee_Type_vod__c == 'User') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SYSEMP', id = e.id)); 
            } else if (e.Attendee_Type_vod__c == 'Account') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SVVEEVA', id = e.id)); 
            }
        } else if (UserCountry == 'PA') {
            if (e.Attendee_Type_vod__c == 'User') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SYSEMP', id = e.id)); 
            } else if (e.Attendee_Type_vod__c == 'Account') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'PAVEEVA', id = e.id)); 
            }
        } else if (UserCountry == 'NI') {
            if (e.Attendee_Type_vod__c == 'User') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SYSEMP', id = e.id)); 
            } else if (e.Attendee_Type_vod__c == 'Account') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'NIVEEVA', id = e.id)); 
            }
        } else if (UserCountry == 'DO') {
            if (e.Attendee_Type_vod__c == 'User') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SYSEMP', id = e.id)); 
            } else if (e.Attendee_Type_vod__c == 'Account') {
                Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'DOVEEVA', id = e.id)); 
            }                       
        } else if (UserCountry == 'IN') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'INVEEVA', id = e.id)); 
        } else if (UserCountry == 'ID') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'IDVEEVA', id = e.id)); 
        } else if (UserCountry == 'MY') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'MYVEEVA', id = e.id)); 
        } else if (UserCountry == 'PH') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'PHVEEVA', id = e.id)); 
        } else if (UserCountry == 'SG') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'SGVEEVA', id = e.id)); 
        } else if (UserCountry == 'KR') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'KRVEEVA', id = e.id)); 
        } else if (UserCountry == 'TH') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'THVEEVA', id = e.id)); 
        } else if (UserCountry == 'VN') {
          Attendee.add(new Event_Attendee_vod__c (Expense_Attendee_Type_vod__c = 'VNVEEVA', id = e.id)); 
        } 
    }
    
    if (Attendee.size() > 0) update Attendee;

}