/**
* @description: update Interaction_Medium_BI__c on child call if Parent_Call_vod__c is populated - CR 1349 - BISM 8014451
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
*/
trigger VEEVA_BI_UPDATE_INTERACTION_MEDIUM on Call2_vod__c (before insert, before update) {
   
    system.debug('VEEVA_BI_UPDATE_INTERACTION_MEDIUM is starting...');
    
    //list<Call2_vod__c> callsToUpdate = new list<Call2_vod__c>();
    set<id> parentIds = new set<id>();
    map<id, string> parentMap = new Map<id, string>();
     
    for (integer i = 0; i < Trigger.new.size(); i++) {
        if (Trigger.new[i].Parent_Call_vod__c != null && Trigger.new[i].Status_vod__c != 'Submitted_vod'){
            parentIds.add(Trigger.new[i].Parent_Call_vod__c);
        }
    }
    
    system.debug('Attila parentIds: ' + parentIds);
    
    if (parentIds.size() > 0) {
        for (Call2_vod__c call : [SELECT Id, Interaction_Medium_BI__c FROM Call2_vod__c WHERE Id IN :parentIds]){
            parentMap.put(call.Id, call.Interaction_Medium_BI__c);                    
        }
        system.debug('Attila parentMap: ' + parentMap);
        
        for (Call2_vod__c c : [SELECT Id, Parent_Call_vod__c,Account_vod__c, Interaction_Medium_BI__c FROM Call2_vod__c WHERE Parent_Call_vod__c IN :parentIds]){
            string medium = parentMap.get(c.Parent_Call_vod__c);                                                     
            system.debug('Attila medium: ' + medium);
            if (medium != c.Interaction_Medium_BI__c){
                c.Interaction_Medium_BI__c = medium;
                //-Attila- changed the trigger to before so it's not needed                    
                //callsToUpdate.add(new Call2_vod__c(id = c.Id, Interaction_Medium_BI__c = medium));                
            }
        }
    }
    
    //-Attila- changed the trigger to before so it's not needed
    /*
    system.debug('Attila callsToUpdate: ' + callsToUpdate);
    if (callsToUpdate.size() > 0){
        update callsToUpdate;           
    }*/
}