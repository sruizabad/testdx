/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This trigger is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: update My_Target_vod__c based on start and end date on cycle_plan_vod__c JIRA: DM-7491
* @trigger handler: VEEVA_BI_UPDATE_MY_TARGET_HANDLER
* @Author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
**/
trigger VEEVA_BI_UPDATE_MY_TARGET on TSF_vod__c (after insert, after update) {
    
    if(VEEVA_BI_UPDATE_MY_TARGET_HANDLER.hasExecuted){
        return; // prevent recursive re-entry
    }

    VEEVA_BI_UPDATE_MY_TARGET_HANDLER.hasExecuted = true;

    VEEVA_BI_UPDATE_MY_TARGET_HANDLER handler = new VEEVA_BI_UPDATE_MY_TARGET_HANDLER(trigger.isExecuting);
    
    map<id, id> cyclePlanTFSMap = new Map<id, id>();
    map<id, id> accountTSFMap = new Map<id, id>();
    map<id, date> TSFActivityMap = new Map<id, date>();
    map<id, TSF_vod__c> TSFMap = new Map<id, TSF_vod__c>();
    
    for (integer i = 0; i < Trigger.new.size(); i++) {
        accountTSFMap.put(Trigger.new[i].Account_vod__c, Trigger.new[i].id);
        TSFActivityMap.put(Trigger.new[i].id, Trigger.new[i].Last_Activity_Date_vod__c);
    }
    
    for (Cycle_Plan_Target_vod__c c : [SELECT Id, Cycle_Plan_vod__c, Cycle_Plan_Account_vod__c, Planned_Calls_vod__c FROM Cycle_Plan_Target_vod__c WHERE Planned_Calls_vod__c > 0 AND Cycle_Plan_Account_vod__c IN:accountTSFMap.keySet()]){
        cyclePlanTFSMap.put(c.Cycle_Plan_vod__c, accountTSFMap.get(c.Cycle_Plan_Account_vod__c));   
    }
    
    for (Cycle_Plan_vod__c cp : [SELECT Id, Start_Date_vod__c, End_Date_vod__c FROM Cycle_Plan_vod__c WHERE id IN:cyclePlanTFSMap.keySet()]){
        id TSFId = cyclePlanTFSMap.get(cp.Id);
        date TSFActDate = TSFActivityMap.get(TSFId);
        if (TSFActDate >= cp.Start_Date_vod__c && TSFActDate <= cp.End_Date_vod__c){
            if (!TSFMap.containsKey(TSFId)){
                TSF_vod__c tsf = new TSF_vod__c();
                tsf.id = TSFId;
                tsf.My_Target_vod__c = true;
                TSFMap.put(TSFId, tsf);
            }
        }
    }
    
    if (TSFMap.size() > 0) handler.updateGroup(TSFMap);
}