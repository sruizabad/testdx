/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This trigger is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: trigger updates Public_Group_Helper_BI__c to mark records for VEEVA_BI_BATCH_UPDATE_USER_PG batch class
* @author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
**/
trigger VEEVA_BI_UPDATE_USER_PG_HELPER on User (before insert, before update) {

    if (trigger.isUpdate){
        for (Integer i=0; i<trigger.new.size(); i++) {
            if (trigger.new[i].Profile_Name_vod__c != trigger.old[i].Profile_Name_vod__c && trigger.new[i].isActive == true){
                trigger.new[i].Public_Group_Helper_BI__c = 'update_' + trigger.old[i].Profile_Name_vod__c;
            }
        }        
    }    

    if (trigger.isInsert){
        for (Integer i=0; i<trigger.new.size(); i++) {
            if (trigger.new[i].isActive == true){
                trigger.new[i].Public_Group_Helper_BI__c = 'insert_' + trigger.new[i].Profile_Name_vod__c;
            }
        } 
    }
}