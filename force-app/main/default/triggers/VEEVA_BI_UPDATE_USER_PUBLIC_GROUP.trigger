/**
* Copyright (c) 2018 Veeva Systems Inc.  All Rights Reserved. This trigger is based on pre-existing content developed and owned by Veeva Systems Inc. 
  and may only be used in connection with the deliverable with which it was provided to Customer.  

* @description: Adds User to a Public Group where that Public Group Name matches the User's Profile Name + '_MyInsights'
*               In case of profile change, removes user from old Public Group whose name is that of a old Profile name + '_MyInsights', 
* @trigger handler: VEEVA_BI_UPDATE_PUBLIC_GROUP_HANDLER
* @author: Attila Hagelmayer (attila.hagelmayer@veeva.com)
**/
trigger VEEVA_BI_UPDATE_USER_PUBLIC_GROUP on User (after insert, after update) {
    
    if(VEEVA_BI_UPDATE_PUBLIC_GROUP_HANDLER.hasExecuted){
        return; // prevent recursive re-entry
    }

    VEEVA_BI_UPDATE_PUBLIC_GROUP_HANDLER.hasExecuted = true;

    VEEVA_BI_UPDATE_PUBLIC_GROUP_HANDLER handler = new VEEVA_BI_UPDATE_PUBLIC_GROUP_HANDLER(trigger.isExecuting);

    map<string, User> objMap = new map<string, User>(); 
    string concat = '_MyInsights';
    
    if (trigger.isUpdate) {
    
        set<id> userSet = new set<id>();
        set<string> groupSet = new set<string>();
        for (Integer i=0; i<trigger.new.size(); i++) {
            string newProfile = trigger.new[i].Profile_Name_vod__c;
            string oldProfile = trigger.old[i].Profile_Name_vod__c;
            //in case of profile change and only for active users
            //if (newProfile != oldProfile && trigger.new[i].isActive == true){
                userSet.add(trigger.new[i].id);
                groupSet.add(oldProfile + concat);       
                objMap.put(newProfile + concat, trigger.new[i]);
            //}            
        }    
        //system.debug('Attila delete userSet: ' + userSet + 'groupSet: ' + groupSet);
        if (!userSet.isEmpty() && !groupSet.isEmpty()) handler.deleteFromGroup(userSet, groupSet);
        system.debug('Attila update objMap: ' + objMap);
        if (!objMap.isEmpty()) handler.insertGroup(objMap); 
    }
    
    if (trigger.isInsert) {
        
        for (Integer i=0; i<trigger.new.size(); i++) {
            string newProfile = trigger.new[i].Profile_Name_vod__c;
            if (trigger.new[i].isActive == true){
                objMap.put(newProfile + concat, trigger.new[i]);
            }
        }        
        
        if (!objMap.isEmpty()) handler.insertGroup(objMap); 
    }

}