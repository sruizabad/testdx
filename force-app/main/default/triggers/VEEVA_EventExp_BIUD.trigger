/************************************************************************************************

Original author: Csaba Matyas-Peter
Original date: unknown

Modified by: Raphael Krausz <raphael.krausz@veeva.com>
Date: 2015-07-27
Description:
	When:
	Event_Attendee_BI__r.speaker_bi__c - true
	AND Event_Team_Member_BI__r.Event_Management_bi__r.event_status_bi__c IN
			Under Approval
			Approved
			Completed
	AND Event_Team_Member_BI__r.Event_Management_bi__r.Program_BI__r.Expense_Freeze_bi__c - true

	Then throw an error - the record is locked.

Modified by: Raphael Krausz <raphael.krausz@veeva.com>
Date: 2015-07-27
Description:
	Converted code from maximum batch size of 1, to be batchable

************************************************************************************************/
trigger VEEVA_EventExp_BIUD on Event_Expenses_BI__c (before delete, before insert, before update) {

	//check if the Profile  of the user/owner  fall in the  white List
	{
		Id profileId = Userinfo.getProfileId();

		Event_Management_Lock_Settings__c eventManagementLockSettings
		    = Event_Management_Lock_Settings__c.getInstance(profileId);

		Boolean isBanned = Boolean.valueOf(eventManagementLockSettings.isBanned__c);

		if (isBanned == NULL || isBanned ==  false)
			return;
	}


	Set<Id> eventTeamMemberIdSet = new Set<Id>();
	Set<Id> eventAttendeeIdSet   = new Set<Id>();

	List<Event_Expenses_BI__c> eventExpensesTriggerList;
	if (Trigger.isDelete) {
		eventExpensesTriggerList = Trigger.old;
	} else {
		eventExpensesTriggerList = Trigger.new;
	}

	for (Event_Expenses_BI__c eventExpenses : eventExpensesTriggerList) {
		eventTeamMemberIdSet.add(eventExpenses.Event_Team_Member_BI__c);
		eventAttendeeIdSet.add(eventExpenses.Event_Attendee_BI__c);
	}

	Map<Id, Event_Team_Member_BI__c> eventTeamMemberMap = new Map<Id, Event_Team_Member_BI__c>(
	    [
	        SELECT
	        Id,
	        Event_Management_BI__r.Expense_Freeze_BI__c,
	        Event_Management_BI__r.Event_Status_BI__c
	        FROM Event_Team_Member_BI__c
	        WHERE Id IN :eventTeamMemberIdSet
	    ]
	);

	Map<Id, Event_Attendee_vod__c> eventAtendeeMap = new Map<Id, Event_Attendee_vod__c>(
	    [
	        SELECT Id, Speaker_BI__c
	        FROM Event_Attendee_vod__c
	        WHERE Id IN :eventAttendeeIdSet
	    ]
	);

	Set<String> lockedStatuses = new Set<String> {
		'Under Approval',
		'Approved',
		'Completed / Closed'
	};

	String errorMessage = Label.EVENT_MANAGEMENT_APPROVED_NO_SPEAKER_EXPENSE_CHANGES;


	for (Event_Expenses_BI__c eventExpenses : eventExpensesTriggerList) {

		Event_Attendee_vod__c eventAtendee = eventAtendeeMap.get(eventExpenses.Event_Attendee_BI__c);
		Event_Team_Member_BI__c eventTeamMember = eventTeamMemberMap.get(eventExpenses.Event_Team_Member_BI__c);

		// The event attendee field is optional on event expenses - so we need to check it first
		Boolean isSpeaker;
		if (eventAtendee == null) {
			isSpeaker = false;
		} else {
			isSpeaker = eventAtendee.Speaker_BI__c;
		}

		Boolean expensesAreFrozen = eventTeamMember.Event_Management_BI__r.Expense_Freeze_BI__c;
		String  status = eventTeamMember.Event_Management_BI__r.Event_Status_BI__c;


		if (isSpeaker && expensesAreFrozen && lockedStatuses.contains(status)) {
			system.Debug('CSABA FREEZU = ' +  expensesAreFrozen + '  statusu = ' + status);

			eventExpenses.addError(errorMessage);
		}

	}

}