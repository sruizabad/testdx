trigger VPRO_Call_Objective_Trigger on Call_Objective_vod__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    new VPRO_Call_Objective_Trigger_Handler().run();
}